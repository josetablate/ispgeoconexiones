<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'phpDocumentor' => array($vendorDir . '/phpdocumentor/fileset/src', $vendorDir . '/phpdocumentor/fileset/tests/unit', $vendorDir . '/phpdocumentor/graphviz/src', $vendorDir . '/phpdocumentor/graphviz/tests/unit', $vendorDir . '/phpdocumentor/phpdocumentor/src', $vendorDir . '/phpdocumentor/phpdocumentor/tests/unit', $vendorDir . '/phpdocumentor/reflection/src', $vendorDir . '/phpdocumentor/reflection/tests/unit', $vendorDir . '/phpdocumentor/reflection/tests/mocks', $vendorDir . '/phpdocumentor/reflection-docblock/src'),
    'Umpirsky\\' => array($vendorDir . '/umpirsky/twig-php-function/src'),
    'Twig_' => array($vendorDir . '/twig/twig/lib'),
    'Pimple' => array($vendorDir . '/pimple/pimple/lib'),
    'PhpOption\\' => array($vendorDir . '/phpoption/phpoption/src'),
    'PhpCollection' => array($vendorDir . '/phpcollection/phpcollection/src'),
    'Parsedown' => array($vendorDir . '/erusev/parsedown'),
    'PHPExcel' => array($vendorDir . '/phpoffice/phpexcel/Classes'),
    'PEAR2\\Net\\Transmitter\\' => array($vendorDir . '/pear2/net_routeros/vendor/pear2/net_transmitter/src', $vendorDir . '/pear2/net_transmitter/src'),
    'PEAR2\\Net\\RouterOS\\' => array($vendorDir . '/pear2/net_routeros/src'),
    'PEAR2\\Console\\Color' => array($vendorDir . '/pear2/net_routeros/vendor/pear2/console_color/src'),
    'PEAR2\\Cache\\SHM' => array($vendorDir . '/pear2/cache_shm/src', $vendorDir . '/pear2/net_routeros/vendor/pear2/cache_shm/src'),
    'Metadata\\' => array($vendorDir . '/jms/metadata/src'),
    'KevinGH\\Version' => array($vendorDir . '/kherge/version/src/lib'),
    'JakubOnderka\\PhpConsoleHighlighter' => array($vendorDir . '/jakub-onderka/php-console-highlighter/src'),
    'JakubOnderka\\PhpConsoleColor' => array($vendorDir . '/jakub-onderka/php-console-color/src'),
    'JMS\\Serializer' => array($vendorDir . '/jms/serializer/src'),
    'JMS\\' => array($vendorDir . '/jms/parser-lib/src'),
    'Herrera\\Phar\\Update' => array($vendorDir . '/herrera-io/phar-update/src/lib'),
    'Herrera\\Json' => array($vendorDir . '/herrera-io/json/src/lib'),
    'Doctrine\\Common\\Lexer\\' => array($vendorDir . '/doctrine/lexer/lib'),
    'Detection' => array($vendorDir . '/mobiledetect/mobiledetectlib/namespaced'),
    'Cilex\\Provider\\Console' => array($vendorDir . '/cilex/console-service-provider/src'),
    'Cilex\\Provider' => array($vendorDir . '/phpdocumentor/phpdocumentor/src'),
    'Cilex' => array($vendorDir . '/cilex/cilex/src'),
    'Aptoma' => array($vendorDir . '/aptoma/twig-markdown/src'),
);
