#comando utiles

#creo las base de datos
mysql -u root -e "create database ispbraindb"
mysql -u root -e "create database ispbrain_mikrotik_ipfija_ta"
mysql -u root -e "create database ispbrain_mikrotik_pppoe_ta"
mysql -u root -e "create database ispbrain_mikrotik_dhcp_ta"

#migracion

bin/cake migrations migrate
bin/cake migrations migrate -s Migrations/mikrotik_ipfija_ta -c ispbrain_mikrotik_ipfija_ta
bin/cake migrations migrate -s Migrations/mikrotik_pppoe_ta -c ispbrain_mikrotik_pppoe_ta
bin/cake migrations migrate -s Migrations/mikrotik_dhcp_ta -c ispbrain_mikrotik_dhcp_ta

#inserto data inicial

bin/cake migrations seed --seed UsersSeed --source Seeds/main_initial
bin/cake migrations seed --seed ClasesSeed  --source Seeds/main_initial
bin/cake migrations seed --seed ActionsSystemSeed  --source Seeds/main_initial
bin/cake migrations seed --seed RolesSeed  --source Seeds/main_initial
bin/cake migrations seed --seed RolesUsersSeed  --source Seeds/main_initial
bin/cake migrations seed --seed ActionsSystemRolesSeed --source Seeds/main_initial
bin/cake migrations seed --seed AccountsSeed  --source Seeds/main_initial
bin/cake migrations seed --seed CountriesSeed  --source Seeds/main_initial
bin/cake migrations seed --seed ProvincesSeed  --source Seeds/main_initial
bin/cake migrations seed --seed CitiesSeed  --source Seeds/main_initial
bin/cake migrations seed --seed AreasSeed  --source Seeds/main_initial
bin/cake migrations seed --seed MessageTemplatesSeed --source Seeds/main_initial
bin/cake migrations seed --seed PackagesSeed --source Seeds/main_initial
bin/cake migrations seed --seed ArticlesSeed --source Seeds/main_initial
bin/cake migrations seed --seed CashEntitiesSeed --source Seeds/main_initial
bin/cake migrations seed --seed StoresSeed --source Seeds/main_initial
bin/cake migrations seed --seed ArticlesStoresSeed --source Seeds/main_initial
bin/cake migrations seed --seed ProductsSeed --source Seeds/main_initial
bin/cake migrations seed --seed StatusTicketsSeed  --source Seeds/main_initial
bin/cake migrations seed --seed CategoriesTicketsSeed  --source Seeds/main_initial
bin/cake migrations seed --seed LabelsSeed  --source Seeds/main_initial
bin/cake migrations seed --seed DiscountsSeed  --source Seeds/main_initial
bin/cake migrations seed --seed PaymentsConceptsSeed  --source Seeds/main_initial

#insert data para testing faker
bin/cake migrations seed --seed CustomersSeed --source  Seeds/faker


#tareas programadas

vim /etc/cron.d/ispbrain (local)


#*/5 *   * * *   www-data /var/www/html/ispbrain/bin/cake DiffControllers
#*/1 *   * * *   www-data /var/www/html/ispbrain/bin/cake BackupDropbox
#*/1 *   * * *   www-data /var/www/html/ispbrain/bin/cake AutomaticConnectionBlocking
#0   0   1 * *   www-data /var/www/html/ispbrain/bin/cake DebtMonthControl 
#*/1 *   * * *   www-data /var/www/html/ispbrain/bin/cake CobroDigitalControl
#*/1 *   * * *   www-data /var/www/html/ispbrain/bin/cake MercadoPagoControl
#*/1 *   * * *   www-data /var/www/html/ispbrain/bin/cake CuentaDigitalControl
#*/1 *   * * *   www-data /var/www/html/ispbrain/bin/cake PaymentCommitmentControl


#ejecutar shell script 

bin/cake Validation 
bin/cake SendInfo
bin/cake DebtMonthControl
bin/cake BackupDropbox 
bin/cake MercadoPagoControl
bin/cake CobroDigitalControl
bin/cake CuentaDigitalControl
bin/cake PaymentCommitmentControl
bin/cake AutomaticConnectionBlocking
bin/cake DiffControllers


#limpiar logs


echo '' > logs/cli-debug.log
echo '' > logs/cli-error.log
echo '' > logs/error.log
echo '' > logs/debug.log
echo '' > logs/AutomaticConnectionBlocking.log
echo '' > logs/action.log
echo '' > logs/backup.log
echo '' > logs/shell.log
echo '' > logs/cobro_digital.log
echo '' > logs/sync_controllers.log



#limpiar la cache del ORM
bin/cake orm_cache clear

