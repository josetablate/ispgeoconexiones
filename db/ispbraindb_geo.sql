-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 30-04-2020 a las 03:31:22
-- Versión del servidor: 10.4.12-MariaDB-log
-- Versión de PHP: 7.2.11

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ispbraindb_geo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accounts`
--

CREATE TABLE `accounts` (
  `code` int(9) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `saldo` decimal(10,2) NOT NULL DEFAULT 0.00,
  `title` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actions_system`
--

CREATE TABLE `actions_system` (
  `id` int(11) NOT NULL,
  `action` varchar(45) NOT NULL,
  `display_name` varchar(100) NOT NULL,
  `clase_id` int(2) NOT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actions_system_roles`
--

CREATE TABLE `actions_system_roles` (
  `id` int(11) NOT NULL,
  `role_id` int(2) NOT NULL,
  `action_system_id` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `action_log`
--

CREATE TABLE `action_log` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `detail` text NOT NULL,
  `user_id` int(4) NOT NULL,
  `action` varchar(255) NOT NULL,
  `customer_code` int(11) DEFAULT NULL,
  `main` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `areas`
--

CREATE TABLE `areas` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `city_id` int(11) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `business_billing_default` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `code` varchar(45) NOT NULL,
  `name` varchar(80) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `total_amount` int(8) UNSIGNED NOT NULL DEFAULT 0,
  `exist_snid` tinyint(1) NOT NULL DEFAULT 0,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `enabled` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articles_stores`
--

CREATE TABLE `articles_stores` (
  `id` int(11) NOT NULL,
  `current_amount` int(8) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `store_id` int(4) NOT NULL,
  `article_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auto_debits_accounts`
--

CREATE TABLE `auto_debits_accounts` (
  `id` int(11) NOT NULL,
  `firstname` varchar(150) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `cbu` varchar(45) NOT NULL,
  `cuit` varchar(45) NOT NULL,
  `customer_code` int(11) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `payment_getway_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cash_entities`
--

CREATE TABLE `cash_entities` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `cash` decimal(10,2) NOT NULL DEFAULT 0.00,
  `comments` varchar(200) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `open` tinyint(1) NOT NULL DEFAULT 0,
  `user_id` int(4) NOT NULL,
  `account_code` int(11) DEFAULT NULL,
  `cash_other` decimal(10,2) NOT NULL DEFAULT 0.00,
  `contado` decimal(10,2) NOT NULL DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cash_entities_transactions`
--

CREATE TABLE `cash_entities_transactions` (
  `id` int(11) NOT NULL,
  `user_origin_id` int(11) NOT NULL,
  `user_destination_id` int(11) NOT NULL,
  `value` decimal(10,2) NOT NULL DEFAULT 0.00,
  `type` varchar(45) DEFAULT NULL,
  `concept` varchar(200) DEFAULT NULL,
  `acepted` datetime DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `is_table` int(11) NOT NULL DEFAULT 1,
  `seating_number` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories_tickets`
--

CREATE TABLE `categories_tickets` (
  `id` int(11) NOT NULL,
  `name` varchar(40) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `ordering` int(4) DEFAULT NULL,
  `color_back` varchar(7) NOT NULL DEFAULT '',
  `color_text` varchar(7) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `province_id` int(11) NOT NULL DEFAULT 1,
  `cp` varchar(45) NOT NULL DEFAULT '1',
  `enabled` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clases`
--

CREATE TABLE `clases` (
  `id` int(11) NOT NULL,
  `clase` varchar(45) NOT NULL,
  `display_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cobrodigital_accounts`
--

CREATE TABLE `cobrodigital_accounts` (
  `id` int(11) NOT NULL,
  `barcode` varchar(45) DEFAULT NULL,
  `electronic_code` varchar(45) DEFAULT NULL,
  `used` tinyint(1) NOT NULL DEFAULT 0,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `id_comercio` varchar(10) DEFAULT NULL,
  `customer_code` int(11) DEFAULT NULL,
  `date_exported` datetime DEFAULT NULL,
  `firstname` varchar(150) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `cbu` varchar(45) DEFAULT NULL,
  `cuit` varchar(45) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `payment_getway_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `asigned` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cobrodigital_transactions`
--

CREATE TABLE `cobrodigital_transactions` (
  `id` int(11) NOT NULL,
  `id_transaccion` varchar(255) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `codigo_barras` varchar(45) DEFAULT NULL,
  `identificador` varchar(255) DEFAULT '0',
  `nro_boleta` varchar(255) DEFAULT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `info` varchar(150) DEFAULT NULL,
  `concepto` varchar(150) DEFAULT NULL,
  `bruto` decimal(10,2) DEFAULT 0.00,
  `comision` decimal(10,2) DEFAULT 0.00,
  `neto` decimal(10,2) DEFAULT 0.00,
  `saldo_acumulado` decimal(10,2) DEFAULT 0.00,
  `estado` tinyint(1) NOT NULL DEFAULT 0,
  `comments` varchar(255) DEFAULT NULL,
  `bloque_id` int(11) DEFAULT NULL,
  `customer_code` int(11) DEFAULT NULL,
  `receipt_number` varchar(255) DEFAULT NULL,
  `payment_getway_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `receipt_id` varchar(255) DEFAULT NULL,
  `fecha_pago` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `concepts`
--

CREATE TABLE `concepts` (
  `id` int(11) NOT NULL,
  `type` varchar(45) NOT NULL,
  `code` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT 0.00,
  `quantity` decimal(10,2) NOT NULL DEFAULT 0.00,
  `sum_price` decimal(10,2) NOT NULL DEFAULT 0.00,
  `sum_tax` decimal(10,2) NOT NULL DEFAULT 0.00,
  `discount` decimal(10,2) DEFAULT 0.00,
  `total` decimal(10,2) NOT NULL DEFAULT 0.00,
  `unit` varchar(45) NOT NULL,
  `comprobante_id` int(11) NOT NULL,
  `tax` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `concepts_credit`
--

CREATE TABLE `concepts_credit` (
  `id` int(11) NOT NULL,
  `type` varchar(45) NOT NULL,
  `code` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT 0.00,
  `quantity` decimal(10,2) NOT NULL DEFAULT 0.00,
  `sum_price` decimal(10,2) NOT NULL DEFAULT 0.00,
  `tax` int(11) NOT NULL DEFAULT 1,
  `sum_tax` decimal(10,2) NOT NULL DEFAULT 0.00,
  `discount` decimal(10,2) DEFAULT 0.00,
  `total` decimal(10,2) NOT NULL DEFAULT 0.00,
  `unit` varchar(45) NOT NULL,
  `credit_note_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `concepts_debit`
--

CREATE TABLE `concepts_debit` (
  `id` int(11) NOT NULL,
  `type` varchar(45) NOT NULL,
  `code` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT 0.00,
  `quantity` decimal(10,2) NOT NULL DEFAULT 0.00,
  `sum_price` decimal(10,2) NOT NULL DEFAULT 0.00,
  `tax` int(11) NOT NULL DEFAULT 1,
  `sum_tax` decimal(10,2) NOT NULL DEFAULT 0.00,
  `discount` decimal(10,2) DEFAULT 0.00,
  `total` decimal(10,2) NOT NULL DEFAULT 0.00,
  `unit` varchar(45) NOT NULL,
  `debit_note_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `connections`
--

CREATE TABLE `connections` (
  `id` int(11) NOT NULL,
  `address` varchar(150) NOT NULL,
  `lat` float DEFAULT NULL,
  `lng` float DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `customer_code` int(11) NOT NULL,
  `user_id` int(4) NOT NULL,
  `area_id` int(4) NOT NULL,
  `controller_id` int(4) NOT NULL,
  `service_id` int(4) NOT NULL,
  `ip` varchar(15) DEFAULT NULL,
  `comments` text DEFAULT NULL,
  `mac` varchar(17) DEFAULT NULL,
  `diff` tinyint(1) NOT NULL DEFAULT 0,
  `portal_sync` tinyint(1) NOT NULL DEFAULT 0,
  `clave_wifi` varchar(45) DEFAULT NULL,
  `ports` varchar(45) DEFAULT NULL,
  `ip_alt` varchar(45) DEFAULT NULL,
  `ing_traffic` varchar(200) DEFAULT NULL,
  `force_debt` tinyint(1) NOT NULL DEFAULT 1,
  `status` varchar(45) DEFAULT NULL,
  `discount_total_month` int(11) DEFAULT NULL,
  `discount_month` int(11) DEFAULT NULL,
  `discount_value` decimal(10,2) DEFAULT 0.00,
  `discount_description` varchar(200) DEFAULT NULL,
  `discount_code` varchar(45) DEFAULT NULL,
  `discount_alicuot` int(11) DEFAULT NULL,
  `discount_always` tinyint(1) DEFAULT 0,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `debt_month` decimal(10,2) NOT NULL DEFAULT 0.00,
  `payment_method_id` int(11) NOT NULL DEFAULT 1,
  `invoices_no_paid` int(11) NOT NULL DEFAULT 0,
  `before_ip` varchar(15) DEFAULT NULL,
  `queue_name` varchar(45) DEFAULT NULL,
  `pppoe_name` varchar(45) DEFAULT NULL,
  `pppoe_pass` varchar(45) DEFAULT NULL,
  `debt_period_generated` text DEFAULT NULL,
  `cobrodigital_card` tinyint(1) NOT NULL DEFAULT 0,
  `cobrodigital_auto_debit` tinyint(1) NOT NULL DEFAULT 0,
  `chubut_auto_debit` tinyint(1) NOT NULL DEFAULT 0,
  `debt_total` decimal(10,2) NOT NULL DEFAULT 0.00,
  `payu_card` tinyint(1) NOT NULL DEFAULT 0,
  `visa_auto_debit` tinyint(1) NOT NULL DEFAULT 0,
  `mastercard_auto_debit` tinyint(1) NOT NULL DEFAULT 0,
  `cuentadigital` tinyint(1) NOT NULL DEFAULT 0,
  `blocking_date` datetime DEFAULT NULL,
  `enabling_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `connections_auto_debits`
--

CREATE TABLE `connections_auto_debits` (
  `id` int(11) NOT NULL,
  `connection_id` int(11) NOT NULL,
  `auto_debit_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `connections_cobrodigital_cards`
--

CREATE TABLE `connections_cobrodigital_cards` (
  `id` int(11) NOT NULL,
  `connection_id` int(11) NOT NULL,
  `cobrodigital_card_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `connections_debts_month`
--

CREATE TABLE `connections_debts_month` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `business_billing` varchar(45) NOT NULL,
  `period` varchar(45) NOT NULL,
  `total` decimal(10,2) DEFAULT 0.00,
  `totalx` decimal(10,2) DEFAULT 0.00,
  `connection_id` int(11) NOT NULL,
  `debt_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `connections_has_message_templates`
--

CREATE TABLE `connections_has_message_templates` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `connections_id` int(6) NOT NULL,
  `message_template_id` int(11) NOT NULL,
  `ip` varchar(45) NOT NULL,
  `type` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `connections_labels`
--

CREATE TABLE `connections_labels` (
  `id` int(11) NOT NULL,
  `connection_id` int(11) NOT NULL,
  `label_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `connections_traffic`
--

CREATE TABLE `connections_traffic` (
  `id` int(11) NOT NULL,
  `time` datetime NOT NULL,
  `packets_up` int(11) NOT NULL,
  `bytes_up` int(11) NOT NULL,
  `packets_down` int(11) NOT NULL,
  `bytes_down` int(11) NOT NULL,
  `ip_address` bigint(20) NOT NULL,
  `user` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `connections_transactions`
--

CREATE TABLE `connections_transactions` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `customer_code` int(11) NOT NULL,
  `connection_id` int(11) NOT NULL,
  `data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `controllers`
--

CREATE TABLE `controllers` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `template` varchar(45) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `lat` float DEFAULT NULL,
  `lng` float DEFAULT NULL,
  `connect_to` varchar(255) NOT NULL,
  `port` int(11) DEFAULT 8728,
  `username` varchar(45) NOT NULL,
  `password` varchar(50) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `trademark` varchar(45) NOT NULL,
  `message_method` varchar(45) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `radius` int(11) DEFAULT NULL,
  `color` varchar(7) DEFAULT NULL,
  `port_grap` int(11) DEFAULT NULL,
  `use_tls` tinyint(1) NOT NULL DEFAULT 0,
  `port_ssl` int(11) DEFAULT 8729,
  `apply_config_avisos` tinyint(1) NOT NULL DEFAULT 0,
  `apply_config_queue_graph` tinyint(1) NOT NULL DEFAULT 0,
  `apply_config_https` tinyint(1) NOT NULL DEFAULT 0,
  `apply_config_temp_access` tinyint(1) NOT NULL DEFAULT 0,
  `apply_certificate_apissl` tinyint(1) NOT NULL DEFAULT 0,
  `apply_certificate_wwwssl` tinyint(1) NOT NULL DEFAULT 0,
  `counter_unsync` varchar(11) DEFAULT NULL,
  `last_control_diff` datetime DEFAULT NULL,
  `last_status` tinyint(1) NOT NULL DEFAULT 0,
  `integration` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `credit_notes`
--

CREATE TABLE `credit_notes` (
  `id` int(11) NOT NULL,
  `concept_type` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `pto_vta` int(11) NOT NULL,
  `num` int(11) DEFAULT NULL,
  `tipo_comp` varchar(45) NOT NULL,
  `sum_tax` decimal(10,2) NOT NULL DEFAULT 0.00,
  `subtotal` decimal(10,2) NOT NULL DEFAULT 0.00,
  `discount` decimal(10,2) DEFAULT 0.00,
  `total` decimal(10,2) NOT NULL DEFAULT 0.00,
  `company_name` varchar(145) NOT NULL,
  `company_address` varchar(200) NOT NULL,
  `company_cp` varchar(5) NOT NULL,
  `company_city` varchar(145) NOT NULL,
  `company_phone` varchar(45) NOT NULL,
  `company_fax` varchar(45) DEFAULT NULL,
  `company_ident` varchar(45) NOT NULL,
  `company_email` varchar(255) DEFAULT NULL,
  `company_web` varchar(45) DEFAULT NULL,
  `customer_code` int(11) NOT NULL,
  `customer_name` varchar(145) NOT NULL,
  `customer_address` varchar(145) DEFAULT NULL,
  `customer_cp` varchar(45) DEFAULT NULL,
  `customer_city` varchar(45) DEFAULT NULL,
  `customer_country` varchar(45) DEFAULT NULL,
  `customer_doc_type` int(11) DEFAULT NULL,
  `customer_ident` varchar(45) DEFAULT NULL,
  `customer_responsible` int(11) DEFAULT NULL,
  `cond_vta` varchar(45) DEFAULT NULL,
  `comments` varchar(200) DEFAULT NULL,
  `user_id` int(4) NOT NULL,
  `cae` varchar(100) DEFAULT NULL,
  `vto` datetime DEFAULT NULL,
  `barcode` varchar(255) DEFAULT NULL,
  `seating_number` varchar(100) DEFAULT NULL,
  `duedate` datetime DEFAULT NULL,
  `printed` varchar(225) DEFAULT NULL,
  `used` datetime DEFAULT NULL,
  `resto` decimal(10,2) NOT NULL DEFAULT 0.00,
  `business_id` int(11) NOT NULL,
  `company_ing_bruto` varchar(45) DEFAULT NULL,
  `company_init_act` varchar(45) DEFAULT NULL,
  `connection_id` int(11) DEFAULT NULL,
  `company_responsible` int(11) DEFAULT NULL,
  `tax` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentadigital_accounts`
--

CREATE TABLE `cuentadigital_accounts` (
  `id` int(11) NOT NULL,
  `barcode` varchar(50) DEFAULT NULL,
  `payment_getway_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `customer_code` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `image_src` varchar(255) DEFAULT NULL,
  `account_number` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentadigital_transactions`
--

CREATE TABLE `cuentadigital_transactions` (
  `id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `fecha_cobro` varchar(10) DEFAULT NULL,
  `hhmmss` varchar(10) DEFAULT NULL,
  `neto` decimal(10,2) DEFAULT 0.00,
  `neto_original` decimal(10,2) DEFAULT 0.00,
  `barcode` varchar(50) DEFAULT NULL,
  `info` varchar(150) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 0,
  `comentario` varchar(255) DEFAULT NULL,
  `bloque_id` int(11) DEFAULT NULL,
  `customer_code` int(11) DEFAULT NULL,
  `receipt_number` varchar(255) DEFAULT NULL,
  `receipt_id` int(11) DEFAULT NULL,
  `payment_getway_id` int(11) DEFAULT NULL,
  `id_transaccion` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customers`
--

CREATE TABLE `customers` (
  `code` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  `name` varchar(200) NOT NULL,
  `address` varchar(150) NOT NULL,
  `responsible` int(11) NOT NULL DEFAULT 5,
  `doc_type` int(11) NOT NULL DEFAULT 96,
  `ident` varchar(45) DEFAULT NULL,
  `phone` varchar(45) NOT NULL,
  `phone_alt` varchar(45) DEFAULT NULL,
  `asked_router` tinyint(1) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `comments` text DEFAULT NULL,
  `daydue` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `clave_portal` varchar(45) DEFAULT '1234',
  `seller` varchar(45) DEFAULT NULL,
  `area_id` int(4) DEFAULT NULL,
  `user_id` int(4) NOT NULL,
  `account_code` int(11) DEFAULT NULL,
  `old` varchar(45) DEFAULT NULL,
  `cond_venta` int(11) NOT NULL DEFAULT 96,
  `is_presupuesto` tinyint(1) NOT NULL DEFAULT 1,
  `denomination` tinyint(1) NOT NULL DEFAULT 1,
  `valor` decimal(10,2) DEFAULT 0.00,
  `lat` float DEFAULT NULL,
  `lng` float DEFAULT NULL,
  `portal_sync` tinyint(1) NOT NULL DEFAULT 0,
  `province_id` varchar(2) DEFAULT '0',
  `country_id` varchar(2) DEFAULT '0',
  `business_billing` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `payment_method_default_id` int(11) DEFAULT NULL,
  `debt_month` decimal(10,2) NOT NULL DEFAULT 0.00,
  `debt_total` decimal(10,2) NOT NULL DEFAULT 0.00,
  `invoices_no_paid` int(11) NOT NULL DEFAULT 0,
  `acb_enabled` tinyint(1) NOT NULL DEFAULT 0,
  `acb_due_debt` decimal(10,2) NOT NULL DEFAULT 0.00,
  `acb_invoice_no_paid` int(11) NOT NULL DEFAULT 0,
  `acb_type` varchar(45) NOT NULL DEFAULT 'connection',
  `acb_surcharge_enabled` tinyint(1) NOT NULL DEFAULT 0,
  `billing_for_service` tinyint(1) NOT NULL DEFAULT 0,
  `payment_commitment` datetime DEFAULT NULL,
  `super_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `last_emails` datetime DEFAULT NULL,
  `last_sms` datetime DEFAULT NULL,
  `value_service` decimal(10,2) DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customers_accounts`
--

CREATE TABLE `customers_accounts` (
  `id` int(11) NOT NULL,
  `customer_code` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `payment_getway_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customers_has_discounts`
--

CREATE TABLE `customers_has_discounts` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `code` varchar(45) NOT NULL,
  `description` varchar(145) NOT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT 0.00,
  `sum_price` decimal(10,2) NOT NULL DEFAULT 0.00,
  `sum_tax` decimal(10,2) NOT NULL DEFAULT 0.00,
  `total` decimal(10,2) NOT NULL DEFAULT 0.00,
  `debt_id` int(11) DEFAULT NULL,
  `user_id` int(4) NOT NULL,
  `customer_code` int(11) NOT NULL,
  `seating_number` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `connection_id` int(11) DEFAULT NULL,
  `period` varchar(45) DEFAULT NULL,
  `tax` int(11) NOT NULL,
  `tipo_comp` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customers_labels`
--

CREATE TABLE `customers_labels` (
  `id` int(11) NOT NULL,
  `customer_code` int(11) NOT NULL,
  `label_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customers_message_templates`
--

CREATE TABLE `customers_message_templates` (
  `id` int(11) NOT NULL,
  `category` varchar(45) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `created` datetime NOT NULL,
  `html` text DEFAULT NULL,
  `send_date` datetime DEFAULT NULL,
  `priority` int(11) NOT NULL,
  `portal_sync` tinyint(1) NOT NULL DEFAULT 0,
  `active` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `custom_logs`
--

CREATE TABLE `custom_logs` (
  `id` int(11) NOT NULL,
  `type` varchar(45) NOT NULL,
  `message` text NOT NULL,
  `stack` text NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `debit_notes`
--

CREATE TABLE `debit_notes` (
  `id` int(11) NOT NULL,
  `concept_type` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `pto_vta` int(11) NOT NULL,
  `num` int(11) DEFAULT NULL,
  `tipo_comp` varchar(45) NOT NULL,
  `sum_tax` decimal(10,2) NOT NULL DEFAULT 0.00,
  `subtotal` decimal(10,2) NOT NULL DEFAULT 0.00,
  `discount` decimal(10,2) DEFAULT 0.00,
  `total` decimal(10,2) NOT NULL DEFAULT 0.00,
  `company_name` varchar(145) NOT NULL,
  `company_address` varchar(200) NOT NULL,
  `company_cp` varchar(5) NOT NULL,
  `company_city` varchar(145) NOT NULL,
  `company_phone` varchar(45) NOT NULL,
  `company_fax` varchar(45) DEFAULT NULL,
  `company_ident` varchar(45) NOT NULL,
  `company_email` varchar(255) DEFAULT NULL,
  `company_web` varchar(45) DEFAULT NULL,
  `customer_code` int(11) NOT NULL,
  `customer_name` varchar(145) NOT NULL,
  `customer_address` varchar(145) DEFAULT NULL,
  `customer_cp` varchar(45) DEFAULT NULL,
  `customer_city` varchar(45) DEFAULT NULL,
  `customer_country` varchar(45) DEFAULT NULL,
  `customer_doc_type` int(11) DEFAULT NULL,
  `customer_ident` varchar(45) DEFAULT NULL,
  `customer_responsible` int(11) NOT NULL,
  `cond_vta` varchar(45) DEFAULT NULL,
  `comments` varchar(200) DEFAULT NULL,
  `user_id` int(4) NOT NULL,
  `cae` varchar(100) DEFAULT NULL,
  `vto` datetime DEFAULT NULL,
  `barcode` varchar(255) DEFAULT NULL,
  `seating_number` varchar(100) DEFAULT NULL,
  `duedate` datetime NOT NULL,
  `printed` varchar(225) DEFAULT NULL,
  `used` datetime DEFAULT NULL,
  `business_id` int(11) NOT NULL,
  `company_ing_bruto` varchar(45) DEFAULT NULL,
  `company_init_act` varchar(45) DEFAULT NULL,
  `connection_id` int(11) DEFAULT NULL,
  `company_responsible` int(11) DEFAULT NULL,
  `tax` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `debts`
--

CREATE TABLE `debts` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `duedate` datetime NOT NULL,
  `type` varchar(45) NOT NULL,
  `code` varchar(45) NOT NULL,
  `description` varchar(145) NOT NULL,
  `quantity` decimal(10,2) NOT NULL DEFAULT 0.00,
  `unit` varchar(45) NOT NULL DEFAULT '0',
  `price` decimal(10,2) NOT NULL DEFAULT 0.00,
  `sum_price` decimal(10,2) NOT NULL DEFAULT 0.00,
  `sum_tax` decimal(10,2) NOT NULL DEFAULT 0.00,
  `discount` decimal(10,2) DEFAULT 0.00,
  `total` decimal(10,2) NOT NULL DEFAULT 0.00,
  `dues` varchar(45) DEFAULT NULL,
  `tipo_comp` varchar(45) NOT NULL,
  `user_id` int(4) NOT NULL,
  `customer_code` int(11) NOT NULL,
  `seating_number` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `connection_id` int(11) DEFAULT NULL,
  `period` varchar(45) DEFAULT NULL,
  `tax` int(11) NOT NULL DEFAULT 1,
  `presale_id` int(11) DEFAULT NULL,
  `sale_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `discounts`
--

CREATE TABLE `discounts` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `concept` varchar(200) NOT NULL,
  `comments` varchar(200) DEFAULT NULL,
  `value` decimal(10,2) UNSIGNED NOT NULL DEFAULT 0.00,
  `always` tinyint(1) NOT NULL DEFAULT 0,
  `duration` int(11) NOT NULL DEFAULT 1,
  `user_id` int(11) NOT NULL,
  `code` varchar(45) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `type` varchar(45) NOT NULL DEFAULT 'generic'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `error_log`
--

CREATE TABLE `error_log` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `msg` varchar(255) NOT NULL,
  `data` text DEFAULT NULL,
  `user_id` int(4) NOT NULL,
  `type` varchar(45) NOT NULL,
  `view` datetime DEFAULT NULL,
  `event` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `firewall_address_lists`
--

CREATE TABLE `firewall_address_lists` (
  `id` int(11) NOT NULL,
  `list` varchar(45) NOT NULL,
  `address` varchar(15) NOT NULL,
  `comment` varchar(100) NOT NULL,
  `controller_id` int(4) NOT NULL,
  `api_id` varchar(45) DEFAULT NULL,
  `connection_id` int(6) NOT NULL,
  `before_address` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `general_parameters`
--

CREATE TABLE `general_parameters` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instalations`
--

CREATE TABLE `instalations` (
  `id` int(11) NOT NULL,
  `comments` varchar(200) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `connection_id` int(6) NOT NULL,
  `package_id` int(4) NOT NULL,
  `user_id` int(4) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `customer_code` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instalations_articles`
--

CREATE TABLE `instalations_articles` (
  `id` int(11) NOT NULL,
  `amount` int(4) UNSIGNED NOT NULL,
  `created` datetime NOT NULL,
  `article_id` int(4) NOT NULL,
  `instalation_id` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `int_out_cashs_entities`
--

CREATE TABLE `int_out_cashs_entities` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `type` varchar(45) NOT NULL,
  `concept` varchar(200) NOT NULL,
  `in_value` decimal(10,2) UNSIGNED NOT NULL DEFAULT 0.00,
  `out_value` decimal(10,2) UNSIGNED NOT NULL DEFAULT 0.00,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `data` varchar(45) DEFAULT NULL,
  `saldo_contado` decimal(10,2) UNSIGNED NOT NULL DEFAULT 0.00,
  `number_part` varchar(45) DEFAULT NULL,
  `cash_entity_id` int(4) NOT NULL,
  `seating_number` int(11) DEFAULT NULL,
  `payment_method_id` int(11) DEFAULT NULL,
  `is_table` int(11) DEFAULT NULL,
  `saldo_other` decimal(10,2) NOT NULL DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `invoices`
--

CREATE TABLE `invoices` (
  `id` int(11) NOT NULL,
  `concept_type` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `duedate` datetime NOT NULL,
  `tipo_comp` varchar(45) NOT NULL,
  `pto_vta` int(11) NOT NULL,
  `num` int(11) DEFAULT NULL,
  `subtotal` decimal(10,2) NOT NULL DEFAULT 0.00,
  `sum_tax` decimal(10,2) NOT NULL DEFAULT 0.00,
  `discount` decimal(10,2) DEFAULT 0.00,
  `total` decimal(10,2) NOT NULL DEFAULT 0.00,
  `cae` varchar(100) DEFAULT NULL,
  `vto` datetime DEFAULT NULL,
  `barcode` varchar(255) DEFAULT NULL,
  `company_name` varchar(145) NOT NULL,
  `company_address` varchar(200) NOT NULL,
  `company_cp` varchar(5) NOT NULL,
  `company_city` varchar(145) NOT NULL,
  `company_phone` varchar(45) NOT NULL,
  `company_fax` varchar(45) DEFAULT NULL,
  `company_ident` varchar(45) NOT NULL,
  `company_email` varchar(255) DEFAULT NULL,
  `company_web` varchar(45) DEFAULT NULL,
  `company_responsible` int(11) DEFAULT NULL,
  `customer_code` int(11) NOT NULL,
  `customer_name` varchar(145) DEFAULT NULL,
  `customer_address` varchar(145) DEFAULT NULL,
  `customer_cp` varchar(45) DEFAULT NULL,
  `customer_city` varchar(45) DEFAULT NULL,
  `customer_country` varchar(45) DEFAULT NULL,
  `customer_ident` varchar(45) DEFAULT NULL,
  `customer_doc_type` int(11) DEFAULT NULL,
  `customer_responsible` int(11) DEFAULT NULL,
  `cond_vta` varchar(45) NOT NULL,
  `comments` text NOT NULL DEFAULT '',
  `user_id` int(4) NOT NULL,
  `paid` datetime DEFAULT NULL,
  `seating_number` varchar(100) DEFAULT NULL,
  `printed` varchar(225) DEFAULT NULL,
  `resto` decimal(10,2) NOT NULL DEFAULT 0.00,
  `cd_nro_boleta` int(11) DEFAULT NULL,
  `auto_debit` tinyint(1) NOT NULL DEFAULT 0,
  `cd_exported` tinyint(1) NOT NULL DEFAULT 0,
  `cd_date_exported` datetime DEFAULT NULL,
  `cd_duedate` datetime DEFAULT NULL,
  `comp_asociados` varchar(45) DEFAULT NULL,
  `portal_sync` tinyint(1) NOT NULL DEFAULT 0,
  `connection_id` int(11) DEFAULT NULL,
  `business_id` int(11) NOT NULL,
  `company_ing_bruto` varchar(45) DEFAULT NULL,
  `company_init_act` varchar(45) DEFAULT NULL,
  `period` varchar(45) DEFAULT NULL,
  `cd_auto_debit_date_exported` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `anulated` datetime DEFAULT NULL,
  `manual` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `labels`
--

CREATE TABLE `labels` (
  `id` int(11) NOT NULL,
  `text` varchar(45) NOT NULL,
  `entity` int(11) NOT NULL,
  `color_back` varchar(7) NOT NULL DEFAULT '',
  `color_text` varchar(7) NOT NULL DEFAULT '',
  `enabled` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mass_emails_blocks`
--

CREATE TABLE `mass_emails_blocks` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `confirm_date` datetime DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `template_name` varchar(40) NOT NULL DEFAULT '',
  `email_account` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mass_emails_sms`
--

CREATE TABLE `mass_emails_sms` (
  `id` int(11) NOT NULL,
  `message` text NOT NULL,
  `subject` varchar(50) NOT NULL DEFAULT '',
  `customer_code` int(11) DEFAULT NULL,
  `connections_id` text DEFAULT NULL,
  `invoices_id` text DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `bloque_id` int(11) DEFAULT NULL,
  `business_billing` int(11) DEFAULT NULL,
  `invoice_attach` tinyint(1) NOT NULL DEFAULT 0,
  `archived` tinyint(1) NOT NULL DEFAULT 0,
  `invoice_periode` varchar(45) DEFAULT NULL,
  `invoice_impagas` tinyint(1) NOT NULL DEFAULT 0,
  `email` varchar(255) DEFAULT NULL,
  `receipt_attach` tinyint(1) NOT NULL DEFAULT 0,
  `receipts_id` text DEFAULT NULL,
  `receipt_periode` varchar(45) DEFAULT NULL,
  `account_summary_attach` tinyint(1) NOT NULL DEFAULT 0,
  `email_id` varchar(255) DEFAULT NULL,
  `payment_link_mercadopago` tinyint(1) NOT NULL DEFAULT 0,
  `template_id` int(11) DEFAULT NULL,
  `payment_link_cuentadigital` tinyint(1) NOT NULL DEFAULT 0,
  `cd_venc` int(11) DEFAULT NULL,
  `cd_saldo` int(11) DEFAULT NULL,
  `cd_efectivo` tinyint(1) NOT NULL DEFAULT 0,
  `cd_credit_card` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mass_emails_templates`
--

CREATE TABLE `mass_emails_templates` (
  `id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL DEFAULT '',
  `subject` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(200) DEFAULT NULL,
  `message` text NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `business_billing` int(11) DEFAULT NULL,
  `invoice_attach` tinyint(1) NOT NULL DEFAULT 0,
  `receipt_attach` tinyint(1) NOT NULL DEFAULT 0,
  `email_id` int(11) DEFAULT NULL,
  `account_summary_attach` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mastercard_auto_debit_accounts`
--

CREATE TABLE `mastercard_auto_debit_accounts` (
  `id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `asigned` datetime DEFAULT NULL,
  `card_number` varchar(200) DEFAULT NULL,
  `customer_code` int(11) DEFAULT NULL,
  `payment_getway_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mastercard_auto_debit_records`
--

CREATE TABLE `mastercard_auto_debit_records` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `card_number` varchar(200) NOT NULL,
  `customer_name` varchar(200) NOT NULL,
  `customer_code` int(11) NOT NULL,
  `customer_doc_type` int(11) NOT NULL,
  `customer_ident` varchar(45) DEFAULT NULL,
  `invoice_num` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `comment` varchar(200) DEFAULT NULL,
  `duedate` datetime NOT NULL,
  `total` decimal(10,2) NOT NULL DEFAULT 0.00,
  `customer_city` varchar(45) DEFAULT NULL,
  `business_id` int(11) NOT NULL,
  `payment_getway_id` int(11) NOT NULL,
  `status` int(2) NOT NULL DEFAULT -1,
  `account_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mastercard_auto_debit_transactions`
--

CREATE TABLE `mastercard_auto_debit_transactions` (
  `id` int(11) NOT NULL,
  `id_transaction` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `card_number` varchar(2000) DEFAULT NULL,
  `total` decimal(10,2) DEFAULT 0.00,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `comment` varchar(255) DEFAULT NULL,
  `bloque_id` int(11) DEFAULT NULL,
  `customer_code` int(11) DEFAULT NULL,
  `payment_getway_id` int(11) DEFAULT NULL,
  `receipt_number` varchar(255) DEFAULT NULL,
  `receipt_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `message_templates`
--

CREATE TABLE `message_templates` (
  `id` int(11) NOT NULL,
  `type` varchar(45) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `html` text DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `send_date` datetime DEFAULT NULL,
  `user_id` int(4) NOT NULL,
  `priority` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migration_from_excel`
--

CREATE TABLE `migration_from_excel` (
  `id` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `tipo_doc` varchar(255) DEFAULT NULL,
  `ident` varchar(255) DEFAULT NULL,
  `responsable` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `phone_alt` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `mail` varchar(255) DEFAULT NULL,
  `is_presupuesto` varchar(255) DEFAULT NULL,
  `cod_barra_co_di` varchar(255) DEFAULT NULL,
  `cod_cu_di` varchar(255) DEFAULT NULL,
  `nro_cu_di` varchar(255) DEFAULT NULL,
  `cod_payu` varchar(255) DEFAULT NULL,
  `saldo` varchar(255) DEFAULT NULL,
  `empresa` varchar(255) DEFAULT NULL,
  `dueday` varchar(255) DEFAULT NULL,
  `valor_servicio` varchar(255) DEFAULT NULL,
  `fecha_inst` varchar(255) DEFAULT NULL,
  `nodo` varchar(255) DEFAULT NULL,
  `plan` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `mac` varchar(255) DEFAULT NULL,
  `pppoe_name` varchar(255) DEFAULT NULL,
  `pppoe_pass` varchar(255) DEFAULT NULL,
  `free` varchar(255) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `error` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mp_transactions`
--

CREATE TABLE `mp_transactions` (
  `id` int(11) NOT NULL,
  `payment_id` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_approved` datetime DEFAULT NULL,
  `date_last_updated` datetime DEFAULT NULL,
  `money_release_date` datetime DEFAULT NULL,
  `operation_type` varchar(20) DEFAULT NULL,
  `collector_id` varchar(10) DEFAULT NULL,
  `payer_email` varchar(254) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `transaction_amount` decimal(10,2) DEFAULT 0.00,
  `total_paid_amount` decimal(10,0) DEFAULT 0,
  `currency_id` varchar(4) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `status_detail` varchar(255) DEFAULT NULL,
  `payment_type_id` varchar(20) DEFAULT NULL,
  `comment` varchar(254) DEFAULT NULL,
  `local_status` tinyint(1) NOT NULL DEFAULT 0,
  `comision` decimal(10,2) DEFAULT 0.00,
  `net_received_amount` decimal(10,2) DEFAULT 0.00,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `external_reference` varchar(255) DEFAULT NULL,
  `customer_code` int(11) DEFAULT NULL,
  `connection_id` int(11) DEFAULT NULL,
  `receipt_number` varchar(255) DEFAULT NULL,
  `receipt_id` int(11) DEFAULT NULL,
  `bloque_id` int(11) DEFAULT NULL,
  `payment_getway_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `observations`
--

CREATE TABLE `observations` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `comment` text NOT NULL,
  `customer_code` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `packages`
--

CREATE TABLE `packages` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT 0.00,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `account_code` int(11) DEFAULT NULL,
  `aliquot` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `packages_articles`
--

CREATE TABLE `packages_articles` (
  `id` int(11) NOT NULL,
  `amount` int(4) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `article_id` int(4) NOT NULL,
  `package_id` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `packages_sales`
--

CREATE TABLE `packages_sales` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `customer_code` int(5) NOT NULL,
  `package_id` int(4) NOT NULL,
  `instalation_id` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `concept` varchar(200) NOT NULL,
  `import` decimal(10,2) UNSIGNED NOT NULL DEFAULT 0.00,
  `customer_code` int(11) NOT NULL,
  `cash_entity_id` int(11) DEFAULT NULL,
  `user_id` int(4) NOT NULL,
  `payment_method_id` int(2) NOT NULL,
  `seating_number` varchar(100) DEFAULT NULL,
  `receipt_id` int(11) DEFAULT NULL,
  `anulated` datetime DEFAULT NULL,
  `cd_auto_debit` tinyint(1) DEFAULT 0,
  `cd_id_transaccion` varchar(128) DEFAULT NULL,
  `tp_id_transaccion` varchar(20) DEFAULT NULL,
  `mp_id_transaccion` varchar(20) DEFAULT NULL,
  `connection_id` int(11) DEFAULT NULL,
  `cuenta` varchar(80) DEFAULT NULL,
  `bank` varchar(80) DEFAULT NULL,
  `payment_date` varchar(45) DEFAULT NULL,
  `number_part` varchar(45) DEFAULT NULL,
  `number_transaction` text DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payments_concepts`
--

CREATE TABLE `payments_concepts` (
  `id` int(11) NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payment_commitment`
--

CREATE TABLE `payment_commitment` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `customer_code` int(11) NOT NULL,
  `connection_id` int(11) DEFAULT NULL,
  `duedate` datetime NOT NULL,
  `comment` text NOT NULL,
  `import` decimal(10,2) DEFAULT NULL,
  `ok` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payment_getway_parameters`
--

CREATE TABLE `payment_getway_parameters` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payu_accounts`
--

CREATE TABLE `payu_accounts` (
  `id` int(11) NOT NULL,
  `barcode` varchar(50) DEFAULT NULL,
  `id_comercio` varchar(10) DEFAULT NULL,
  `customer_code` int(11) DEFAULT NULL,
  `payment_getway_id` int(11) NOT NULL,
  `used` tinyint(1) NOT NULL DEFAULT 0,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `date_exported` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `asigned` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payu_transactions`
--

CREATE TABLE `payu_transactions` (
  `id` int(11) NOT NULL,
  `fecha_pago` datetime NOT NULL,
  `id_orden` varchar(255) DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT 0.00,
  `pais` varchar(20) DEFAULT NULL,
  `cuenta` varchar(20) DEFAULT NULL,
  `numero_pago` varchar(20) DEFAULT NULL,
  `id_cupon` varchar(20) DEFAULT NULL,
  `referencia_venta` varchar(20) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 0,
  `medio_pago` varchar(20) DEFAULT NULL,
  `tipo_creacion` varchar(50) DEFAULT NULL,
  `multiple_pagos` tinyint(1) NOT NULL DEFAULT 0,
  `frecuencia_recordatorio` varchar(50) DEFAULT NULL,
  `fecha_recordatorio` datetime DEFAULT NULL,
  `nombre_cliente` varchar(200) DEFAULT NULL,
  `concepto` varchar(100) DEFAULT NULL,
  `fecha_expiracion` datetime DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `comments` varchar(100) DEFAULT NULL,
  `bloque_id` int(11) DEFAULT NULL,
  `customer_code` int(11) DEFAULT NULL,
  `receipt_number` varchar(255) DEFAULT NULL,
  `payment_getway_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `receipt_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `phinxlog`
--

CREATE TABLE `phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `portal_parameters`
--

CREATE TABLE `portal_parameters` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `presales`
--

CREATE TABLE `presales` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `customer_code` int(11) NOT NULL,
  `comments` varchar(200) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `presupuestos`
--

CREATE TABLE `presupuestos` (
  `id` int(11) NOT NULL,
  `concept_type` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `duedate` datetime NOT NULL,
  `tipo_comp` varchar(45) NOT NULL,
  `pto_vta` int(11) NOT NULL,
  `num` int(11) DEFAULT NULL,
  `subtotal` decimal(10,2) NOT NULL DEFAULT 0.00,
  `sum_tax` decimal(10,2) NOT NULL DEFAULT 0.00,
  `discount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `total` decimal(10,2) NOT NULL DEFAULT 0.00,
  `company_name` varchar(145) NOT NULL,
  `company_address` varchar(200) NOT NULL,
  `company_cp` varchar(5) NOT NULL,
  `company_city` varchar(145) NOT NULL,
  `company_phone` varchar(45) NOT NULL,
  `company_fax` varchar(45) DEFAULT NULL,
  `company_ident` varchar(45) NOT NULL,
  `company_email` varchar(45) DEFAULT NULL,
  `company_web` varchar(45) DEFAULT NULL,
  `company_responsible` int(11) DEFAULT NULL,
  `customer_code` int(11) NOT NULL,
  `customer_name` varchar(145) DEFAULT NULL,
  `customer_address` varchar(145) DEFAULT NULL,
  `customer_cp` varchar(45) DEFAULT NULL,
  `customer_city` varchar(45) DEFAULT NULL,
  `customer_country` varchar(45) DEFAULT NULL,
  `customer_ident` varchar(45) DEFAULT NULL,
  `customer_doc_type` int(11) DEFAULT NULL,
  `customer_responsible` int(11) DEFAULT NULL,
  `cond_vta` varchar(45) NOT NULL,
  `comments` varchar(200) DEFAULT NULL,
  `user_id` int(4) NOT NULL,
  `printed` varchar(225) DEFAULT NULL,
  `connection_id` int(11) DEFAULT NULL,
  `business_id` int(11) NOT NULL,
  `company_ing_bruto` varchar(45) DEFAULT NULL,
  `company_init_act` varchar(45) DEFAULT NULL,
  `period` varchar(45) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `presupuesto_concepts`
--

CREATE TABLE `presupuesto_concepts` (
  `id` int(11) NOT NULL,
  `type` varchar(45) NOT NULL,
  `code` varchar(45) NOT NULL,
  `description` varchar(250) NOT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT 0.00,
  `quantity` decimal(10,2) NOT NULL DEFAULT 0.00,
  `sum_price` decimal(10,2) NOT NULL DEFAULT 0.00,
  `sum_tax` decimal(10,2) NOT NULL DEFAULT 0.00,
  `discount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `total` decimal(10,2) NOT NULL DEFAULT 0.00,
  `unit` varchar(45) NOT NULL,
  `presupuesto_id` int(11) NOT NULL,
  `tax` int(11) NOT NULL DEFAULT 5
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `unit_price` decimal(10,2) NOT NULL DEFAULT 0.00,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `article_id` int(4) NOT NULL,
  `account_code` int(11) DEFAULT NULL,
  `aliquot` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provinces`
--

CREATE TABLE `provinces` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `country_id` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `real_time_speed`
--

CREATE TABLE `real_time_speed` (
  `id` int(11) NOT NULL,
  `up` decimal(10,2) DEFAULT 0.00,
  `down` decimal(10,2) DEFAULT 0.00,
  `connection_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `receipts`
--

CREATE TABLE `receipts` (
  `id` int(11) NOT NULL,
  `concept_type` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `pto_vta` int(11) NOT NULL,
  `num` int(11) DEFAULT NULL,
  `tipo_comp` varchar(45) NOT NULL,
  `concept` varchar(250) DEFAULT NULL,
  `total` decimal(10,2) NOT NULL DEFAULT 0.00,
  `company_name` varchar(145) NOT NULL DEFAULT '0',
  `company_address` varchar(200) NOT NULL,
  `company_cp` varchar(5) NOT NULL,
  `company_city` varchar(145) NOT NULL,
  `company_phone` varchar(45) NOT NULL,
  `company_fax` varchar(45) DEFAULT NULL,
  `company_ident` varchar(45) NOT NULL,
  `company_email` varchar(255) DEFAULT NULL,
  `company_web` varchar(45) DEFAULT NULL,
  `customer_code` int(11) NOT NULL,
  `customer_name` varchar(145) NOT NULL,
  `customer_address` varchar(145) DEFAULT NULL,
  `customer_cp` varchar(45) DEFAULT NULL,
  `customer_city` varchar(45) DEFAULT NULL,
  `customer_country` varchar(45) DEFAULT NULL,
  `customer_doc_type` int(11) DEFAULT NULL,
  `customer_ident` varchar(45) DEFAULT NULL,
  `customer_responsible` int(11) NOT NULL,
  `payment_method` varchar(45) NOT NULL,
  `comments` varchar(200) DEFAULT NULL,
  `user_id` int(4) NOT NULL,
  `cae` varchar(100) DEFAULT NULL,
  `vto` datetime DEFAULT NULL,
  `barcode` varchar(255) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `duedate` datetime DEFAULT NULL,
  `printed` varchar(225) DEFAULT NULL,
  `used` datetime DEFAULT NULL,
  `resto` decimal(10,2) NOT NULL DEFAULT 0.00,
  `portal_sync` tinyint(1) NOT NULL DEFAULT 0,
  `observations` text DEFAULT '',
  `business_id` int(11) NOT NULL,
  `company_ing_bruto` varchar(45) DEFAULT NULL,
  `company_init_act` varchar(45) DEFAULT NULL,
  `company_responsible` int(11) DEFAULT NULL,
  `connection_id` int(11) DEFAULT NULL,
  `moves` text DEFAULT NULL,
  `barcode_alt` varchar(50) DEFAULT NULL,
  `electronic_code` varchar(50) DEFAULT NULL,
  `id_comercio` varchar(10) DEFAULT NULL,
  `firstname` varchar(150) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `cbu` varchar(45) DEFAULT NULL,
  `cuit` varchar(45) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `road_map`
--

CREATE TABLE `road_map` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `customer_code` int(11) NOT NULL,
  `user_id` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles_users`
--

CREATE TABLE `roles_users` (
  `id` int(11) NOT NULL,
  `user_id` int(4) NOT NULL,
  `role_id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seating`
--

CREATE TABLE `seating` (
  `number` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `concept` varchar(200) DEFAULT NULL,
  `comments` varchar(200) DEFAULT NULL,
  `type` varchar(1) NOT NULL DEFAULT 'a',
  `user_id` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seating_detail`
--

CREATE TABLE `seating_detail` (
  `id` int(11) NOT NULL,
  `debe` decimal(10,2) UNSIGNED NOT NULL DEFAULT 0.00,
  `haber` decimal(10,2) UNSIGNED NOT NULL DEFAULT 0.00,
  `seating_number` int(11) NOT NULL,
  `account_code` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT 0.00,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `account_code` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `aliquot` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `services_has_discounts`
--

CREATE TABLE `services_has_discounts` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `discount_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_pending`
--

CREATE TABLE `service_pending` (
  `id` int(11) NOT NULL,
  `presale_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `discount_id` int(11) DEFAULT NULL,
  `customer_code` int(11) NOT NULL,
  `used` datetime DEFAULT NULL,
  `address` varchar(200) NOT NULL,
  `lat` float DEFAULT NULL,
  `lng` float DEFAULT NULL,
  `installation_date` datetime NOT NULL,
  `availability` varchar(45) DEFAULT NULL,
  `service_name` varchar(200) DEFAULT NULL,
  `service_total` decimal(10,2) DEFAULT NULL,
  `discount_name` varchar(200) DEFAULT NULL,
  `discount_total` decimal(10,2) DEFAULT NULL,
  `payment_method_id` int(2) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `pay_with_account` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(40) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `data` longblob DEFAULT NULL,
  `expires` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sms`
--

CREATE TABLE `sms` (
  `id` int(11) NOT NULL,
  `message` varchar(200) NOT NULL,
  `customer_code` int(11) NOT NULL,
  `status` varchar(255) DEFAULT 'PENDIENTE',
  `bloque_id` int(11) NOT NULL,
  `business_billing` int(11) DEFAULT NULL,
  `archived` tinyint(1) NOT NULL DEFAULT 0,
  `phone` varchar(45) DEFAULT NULL,
  `getway_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sms_bloques`
--

CREATE TABLE `sms_bloques` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `confirm_date` datetime DEFAULT NULL,
  `status` varchar(45) NOT NULL DEFAULT 'PENDIENTE',
  `user_id` int(11) NOT NULL,
  `template_name` varchar(255) NOT NULL DEFAULT '',
  `platform` varchar(50) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `device_id` varchar(20) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `sms_offset` int(11) DEFAULT NULL,
  `sms_limit` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sms_templates`
--

CREATE TABLE `sms_templates` (
  `id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL,
  `description` varchar(160) DEFAULT NULL,
  `message` varchar(200) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `business_billing` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status_tickets`
--

CREATE TABLE `status_tickets` (
  `id` int(11) NOT NULL,
  `name` varchar(40) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `ordering` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stores`
--

CREATE TABLE `stores` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `address` varchar(100) NOT NULL,
  `lat` float DEFAULT NULL,
  `lng` float DEFAULT NULL,
  `comments` varchar(200) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `user_id` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `template_resources`
--

CREATE TABLE `template_resources` (
  `id` int(11) NOT NULL,
  `type` varchar(45) NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tickets`
--

CREATE TABLE `tickets` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `status` int(1) DEFAULT 0,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `user_id` int(4) NOT NULL,
  `connection_id` int(6) DEFAULT NULL,
  `asigned_user_id` int(6) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `category` int(2) DEFAULT 0,
  `start_task` datetime NOT NULL,
  `duedate` datetime DEFAULT NULL,
  `label` int(11) DEFAULT NULL,
  `service_pending_id` int(11) DEFAULT NULL,
  `archived` tinyint(1) NOT NULL DEFAULT 0,
  `title` varchar(100) NOT NULL,
  `area_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tickets_records`
--

CREATE TABLE `tickets_records` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `user_id` int(4) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `short_description` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tp_transactions`
--

CREATE TABLE `tp_transactions` (
  `id` int(11) NOT NULL,
  `RESULTMESSAGE` varchar(256) DEFAULT NULL,
  `DATETIME` datetime DEFAULT NULL,
  `OPERATIONID` varchar(20) DEFAULT NULL,
  `CURRENCYCODE` varchar(3) DEFAULT NULL,
  `AMOUNT` decimal(10,2) DEFAULT 0.00,
  `CREDITEDAMOUNT` decimal(10,2) DEFAULT 0.00,
  `AMOUNTBUYER` decimal(10,2) DEFAULT 0.00,
  `BANKID` varchar(3) DEFAULT NULL,
  `PROMOTIONID` varchar(10) DEFAULT NULL,
  `TYPE` varchar(40) DEFAULT NULL,
  `INSTALLMENTPAYMENTS` varchar(2) DEFAULT NULL,
  `CUSTOMEREMAIL` varchar(255) DEFAULT NULL,
  `IDENTIFICATIONTYPE` varchar(4) DEFAULT NULL,
  `IDENTIFICATION` varchar(10) DEFAULT NULL,
  `CARDNUMBER` varchar(20) DEFAULT NULL,
  `CARDHOLDERNAME` varchar(200) DEFAULT NULL,
  `TICKETNUMBER` varchar(4) DEFAULT NULL,
  `PAYMENTMETHODCODE` varchar(4) DEFAULT NULL,
  `PAYMENTMETHODNAME` varchar(20) DEFAULT NULL,
  `PAYMENTMETHODTYPE` varchar(20) DEFAULT NULL,
  `OPERATIONNUMBER` varchar(20) DEFAULT NULL,
  `AUTHORIZATIONKEY` varchar(256) DEFAULT NULL,
  `CFT` varchar(10) DEFAULT NULL,
  `TEA` varchar(10) DEFAULT NULL,
  `STATUS` tinyint(1) NOT NULL DEFAULT 0,
  `COMMENT` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `description` varchar(200) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visa_auto_debit_accounts`
--

CREATE TABLE `visa_auto_debit_accounts` (
  `id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `asigned` datetime DEFAULT NULL,
  `card_number` varchar(200) DEFAULT NULL,
  `customer_code` int(11) DEFAULT NULL,
  `payment_getway_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visa_auto_debit_records`
--

CREATE TABLE `visa_auto_debit_records` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `card_number` varchar(200) NOT NULL,
  `customer_name` varchar(200) NOT NULL,
  `customer_code` int(11) NOT NULL,
  `customer_doc_type` int(11) NOT NULL,
  `customer_ident` varchar(45) DEFAULT NULL,
  `invoice_num` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `comment` varchar(200) DEFAULT NULL,
  `duedate` datetime NOT NULL,
  `total` decimal(10,2) NOT NULL DEFAULT 0.00,
  `customer_city` varchar(45) DEFAULT NULL,
  `business_id` int(11) NOT NULL,
  `payment_getway_id` int(11) NOT NULL,
  `status` int(2) NOT NULL DEFAULT -1,
  `account_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visa_auto_debit_transactions`
--

CREATE TABLE `visa_auto_debit_transactions` (
  `id` int(11) NOT NULL,
  `id_transaction` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `card_number` varchar(2000) DEFAULT NULL,
  `total` decimal(10,2) DEFAULT 0.00,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `comment` varchar(255) DEFAULT NULL,
  `bloque_id` int(11) DEFAULT NULL,
  `customer_code` int(11) DEFAULT NULL,
  `payment_getway_id` int(11) DEFAULT NULL,
  `receipt_number` varchar(255) DEFAULT NULL,
  `receipt_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `web_proxy_access`
--

CREATE TABLE `web_proxy_access` (
  `id` int(11) NOT NULL,
  `src_address` varchar(15) NOT NULL,
  `dst_port` int(4) NOT NULL DEFAULT 80,
  `action` varchar(45) NOT NULL DEFAULT 'deny',
  `redirect_to` varchar(45) NOT NULL,
  `comment` varchar(45) NOT NULL,
  `controller_id` int(4) NOT NULL,
  `api_id` varchar(45) DEFAULT NULL,
  `connection_id` int(6) NOT NULL,
  `type` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`code`);

--
-- Indices de la tabla `actions_system`
--
ALTER TABLE `actions_system`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `actions_system_roles`
--
ALTER TABLE `actions_system_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `action_system_id` (`action_system_id`),
  ADD KEY `role_id` (`role_id`);

--
-- Indices de la tabla `action_log`
--
ALTER TABLE `action_log`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `articles_stores`
--
ALTER TABLE `articles_stores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `article_id` (`article_id`),
  ADD KEY `store_id` (`store_id`);

--
-- Indices de la tabla `auto_debits_accounts`
--
ALTER TABLE `auto_debits_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cash_entities`
--
ALTER TABLE `cash_entities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `account_code` (`account_code`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `account_code_2` (`account_code`);

--
-- Indices de la tabla `cash_entities_transactions`
--
ALTER TABLE `cash_entities_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categories_tickets`
--
ALTER TABLE `categories_tickets`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `clases`
--
ALTER TABLE `clases`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cobrodigital_accounts`
--
ALTER TABLE `cobrodigital_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cobrodigital_transactions`
--
ALTER TABLE `cobrodigital_transactions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_transaccion` (`id_transaccion`);

--
-- Indices de la tabla `concepts`
--
ALTER TABLE `concepts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `concepts_credit`
--
ALTER TABLE `concepts_credit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `credit_note_id` (`credit_note_id`);

--
-- Indices de la tabla `concepts_debit`
--
ALTER TABLE `concepts_debit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `debit_note_id` (`debit_note_id`);

--
-- Indices de la tabla `connections`
--
ALTER TABLE `connections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_code` (`customer_code`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `controller_id` (`controller_id`),
  ADD KEY `service_id` (`service_id`),
  ADD KEY `area_id` (`area_id`);

--
-- Indices de la tabla `connections_auto_debits`
--
ALTER TABLE `connections_auto_debits`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `connections_cobrodigital_cards`
--
ALTER TABLE `connections_cobrodigital_cards`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `connections_debts_month`
--
ALTER TABLE `connections_debts_month`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `connection_id` (`connection_id`,`period`);

--
-- Indices de la tabla `connections_has_message_templates`
--
ALTER TABLE `connections_has_message_templates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `connections_id` (`connections_id`),
  ADD KEY `message_template_id` (`message_template_id`);

--
-- Indices de la tabla `connections_labels`
--
ALTER TABLE `connections_labels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `connection_id` (`connection_id`),
  ADD KEY `label_id` (`label_id`);

--
-- Indices de la tabla `connections_traffic`
--
ALTER TABLE `connections_traffic`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ip_address` (`ip_address`);

--
-- Indices de la tabla `connections_transactions`
--
ALTER TABLE `connections_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `controllers`
--
ALTER TABLE `controllers`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `credit_notes`
--
ALTER TABLE `credit_notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `seating_number` (`seating_number`);

--
-- Indices de la tabla `cuentadigital_accounts`
--
ALTER TABLE `cuentadigital_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cuentadigital_transactions`
--
ALTER TABLE `cuentadigital_transactions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_transaccion` (`id_transaccion`);

--
-- Indices de la tabla `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`code`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `account_code` (`account_code`),
  ADD KEY `area_id` (`area_id`);

--
-- Indices de la tabla `customers_accounts`
--
ALTER TABLE `customers_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `customers_has_discounts`
--
ALTER TABLE `customers_has_discounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `customer_code` (`customer_code`),
  ADD KEY `seating_number` (`seating_number`),
  ADD KEY `invoice_id` (`invoice_id`),
  ADD KEY `connection_id` (`connection_id`);

--
-- Indices de la tabla `customers_labels`
--
ALTER TABLE `customers_labels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_code` (`customer_code`),
  ADD KEY `label_id` (`label_id`);

--
-- Indices de la tabla `customers_message_templates`
--
ALTER TABLE `customers_message_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `custom_logs`
--
ALTER TABLE `custom_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `debit_notes`
--
ALTER TABLE `debit_notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `seating_number` (`seating_number`);

--
-- Indices de la tabla `debts`
--
ALTER TABLE `debts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `customer_code` (`customer_code`),
  ADD KEY `seating_number` (`seating_number`),
  ADD KEY `invoice_id` (`invoice_id`),
  ADD KEY `presale_id` (`presale_id`);

--
-- Indices de la tabla `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `error_log`
--
ALTER TABLE `error_log`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `firewall_address_lists`
--
ALTER TABLE `firewall_address_lists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `controller_id` (`controller_id`),
  ADD KEY `connection_id` (`connection_id`);

--
-- Indices de la tabla `general_parameters`
--
ALTER TABLE `general_parameters`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `instalations`
--
ALTER TABLE `instalations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `connection_id` (`connection_id`),
  ADD KEY `package_id` (`package_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `customer_code` (`customer_code`);

--
-- Indices de la tabla `instalations_articles`
--
ALTER TABLE `instalations_articles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `instalation_id` (`instalation_id`),
  ADD KEY `article_id` (`article_id`);

--
-- Indices de la tabla `int_out_cashs_entities`
--
ALTER TABLE `int_out_cashs_entities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cash_entity_id` (`cash_entity_id`),
  ADD KEY `seating_number` (`seating_number`);

--
-- Indices de la tabla `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `seating_number` (`seating_number`);

--
-- Indices de la tabla `labels`
--
ALTER TABLE `labels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `entity` (`entity`);

--
-- Indices de la tabla `mass_emails_blocks`
--
ALTER TABLE `mass_emails_blocks`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mass_emails_sms`
--
ALTER TABLE `mass_emails_sms`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mass_emails_templates`
--
ALTER TABLE `mass_emails_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mastercard_auto_debit_accounts`
--
ALTER TABLE `mastercard_auto_debit_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mastercard_auto_debit_records`
--
ALTER TABLE `mastercard_auto_debit_records`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mastercard_auto_debit_transactions`
--
ALTER TABLE `mastercard_auto_debit_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `message_templates`
--
ALTER TABLE `message_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migration_from_excel`
--
ALTER TABLE `migration_from_excel`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mp_transactions`
--
ALTER TABLE `mp_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `observations`
--
ALTER TABLE `observations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `account_code` (`account_code`),
  ADD KEY `account_code_2` (`account_code`);

--
-- Indices de la tabla `packages_articles`
--
ALTER TABLE `packages_articles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `article_id` (`article_id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indices de la tabla `packages_sales`
--
ALTER TABLE `packages_sales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_code` (`customer_code`),
  ADD KEY `package_id` (`package_id`),
  ADD KEY `instalation_id` (`instalation_id`);

--
-- Indices de la tabla `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cash_entity_id` (`cash_entity_id`),
  ADD KEY `customer_code` (`customer_code`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `payment_method_id` (`payment_method_id`),
  ADD KEY `seating_number` (`seating_number`),
  ADD KEY `receipt_id` (`receipt_id`);

--
-- Indices de la tabla `payments_concepts`
--
ALTER TABLE `payments_concepts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `payment_commitment`
--
ALTER TABLE `payment_commitment`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `payment_getway_parameters`
--
ALTER TABLE `payment_getway_parameters`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `payu_accounts`
--
ALTER TABLE `payu_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `payu_transactions`
--
ALTER TABLE `payu_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `phinxlog`
--
ALTER TABLE `phinxlog`
  ADD PRIMARY KEY (`version`);

--
-- Indices de la tabla `portal_parameters`
--
ALTER TABLE `portal_parameters`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `presales`
--
ALTER TABLE `presales`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `presupuestos`
--
ALTER TABLE `presupuestos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `presupuesto_concepts`
--
ALTER TABLE `presupuesto_concepts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `account_code` (`account_code`),
  ADD KEY `article_id` (`article_id`),
  ADD KEY `account_code_2` (`account_code`);

--
-- Indices de la tabla `provinces`
--
ALTER TABLE `provinces`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_id` (`country_id`);

--
-- Indices de la tabla `real_time_speed`
--
ALTER TABLE `real_time_speed`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `receipts`
--
ALTER TABLE `receipts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `road_map`
--
ALTER TABLE `road_map`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_code` (`customer_code`),
  ADD KEY `user_id` (`user_id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles_users`
--
ALTER TABLE `roles_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indices de la tabla `seating`
--
ALTER TABLE `seating`
  ADD PRIMARY KEY (`number`);

--
-- Indices de la tabla `seating_detail`
--
ALTER TABLE `seating_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `seating_number` (`seating_number`),
  ADD KEY `account_code` (`account_code`);

--
-- Indices de la tabla `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `account_code` (`account_code`),
  ADD KEY `account_code_2` (`account_code`);

--
-- Indices de la tabla `services_has_discounts`
--
ALTER TABLE `services_has_discounts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `service_pending`
--
ALTER TABLE `service_pending`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sms`
--
ALTER TABLE `sms`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sms_bloques`
--
ALTER TABLE `sms_bloques`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sms_templates`
--
ALTER TABLE `sms_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `status_tickets`
--
ALTER TABLE `status_tickets`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD KEY `user_id_2` (`user_id`);

--
-- Indices de la tabla `template_resources`
--
ALTER TABLE `template_resources`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `connection_id` (`connection_id`),
  ADD KEY `asigned_user_id` (`asigned_user_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `status` (`status`),
  ADD KEY `category` (`category`);

--
-- Indices de la tabla `tickets_records`
--
ALTER TABLE `tickets_records`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ticket_id` (`ticket_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indices de la tabla `tp_transactions`
--
ALTER TABLE `tp_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indices de la tabla `visa_auto_debit_accounts`
--
ALTER TABLE `visa_auto_debit_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `visa_auto_debit_records`
--
ALTER TABLE `visa_auto_debit_records`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `visa_auto_debit_transactions`
--
ALTER TABLE `visa_auto_debit_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `web_proxy_access`
--
ALTER TABLE `web_proxy_access`
  ADD PRIMARY KEY (`id`),
  ADD KEY `controller_id` (`controller_id`),
  ADD KEY `connection_id` (`connection_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `actions_system`
--
ALTER TABLE `actions_system`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `actions_system_roles`
--
ALTER TABLE `actions_system_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `action_log`
--
ALTER TABLE `action_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `areas`
--
ALTER TABLE `areas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `articles_stores`
--
ALTER TABLE `articles_stores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `auto_debits_accounts`
--
ALTER TABLE `auto_debits_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cash_entities`
--
ALTER TABLE `cash_entities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cash_entities_transactions`
--
ALTER TABLE `cash_entities_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `categories_tickets`
--
ALTER TABLE `categories_tickets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `clases`
--
ALTER TABLE `clases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cobrodigital_accounts`
--
ALTER TABLE `cobrodigital_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cobrodigital_transactions`
--
ALTER TABLE `cobrodigital_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `concepts`
--
ALTER TABLE `concepts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `concepts_credit`
--
ALTER TABLE `concepts_credit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `concepts_debit`
--
ALTER TABLE `concepts_debit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `connections`
--
ALTER TABLE `connections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `connections_auto_debits`
--
ALTER TABLE `connections_auto_debits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `connections_cobrodigital_cards`
--
ALTER TABLE `connections_cobrodigital_cards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `connections_debts_month`
--
ALTER TABLE `connections_debts_month`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `connections_has_message_templates`
--
ALTER TABLE `connections_has_message_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `connections_labels`
--
ALTER TABLE `connections_labels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `connections_traffic`
--
ALTER TABLE `connections_traffic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `connections_transactions`
--
ALTER TABLE `connections_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `controllers`
--
ALTER TABLE `controllers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `credit_notes`
--
ALTER TABLE `credit_notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cuentadigital_accounts`
--
ALTER TABLE `cuentadigital_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cuentadigital_transactions`
--
ALTER TABLE `cuentadigital_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `customers`
--
ALTER TABLE `customers`
  MODIFY `code` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `customers_accounts`
--
ALTER TABLE `customers_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `customers_has_discounts`
--
ALTER TABLE `customers_has_discounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `customers_labels`
--
ALTER TABLE `customers_labels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `customers_message_templates`
--
ALTER TABLE `customers_message_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `custom_logs`
--
ALTER TABLE `custom_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `debit_notes`
--
ALTER TABLE `debit_notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `debts`
--
ALTER TABLE `debts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `discounts`
--
ALTER TABLE `discounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `error_log`
--
ALTER TABLE `error_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `firewall_address_lists`
--
ALTER TABLE `firewall_address_lists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `general_parameters`
--
ALTER TABLE `general_parameters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `instalations`
--
ALTER TABLE `instalations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `instalations_articles`
--
ALTER TABLE `instalations_articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `int_out_cashs_entities`
--
ALTER TABLE `int_out_cashs_entities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `labels`
--
ALTER TABLE `labels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mass_emails_blocks`
--
ALTER TABLE `mass_emails_blocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mass_emails_sms`
--
ALTER TABLE `mass_emails_sms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mass_emails_templates`
--
ALTER TABLE `mass_emails_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mastercard_auto_debit_accounts`
--
ALTER TABLE `mastercard_auto_debit_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mastercard_auto_debit_records`
--
ALTER TABLE `mastercard_auto_debit_records`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mastercard_auto_debit_transactions`
--
ALTER TABLE `mastercard_auto_debit_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `message_templates`
--
ALTER TABLE `message_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migration_from_excel`
--
ALTER TABLE `migration_from_excel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mp_transactions`
--
ALTER TABLE `mp_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `observations`
--
ALTER TABLE `observations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `packages_articles`
--
ALTER TABLE `packages_articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `packages_sales`
--
ALTER TABLE `packages_sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `payments_concepts`
--
ALTER TABLE `payments_concepts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `payment_commitment`
--
ALTER TABLE `payment_commitment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `payment_getway_parameters`
--
ALTER TABLE `payment_getway_parameters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `payu_accounts`
--
ALTER TABLE `payu_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `payu_transactions`
--
ALTER TABLE `payu_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `portal_parameters`
--
ALTER TABLE `portal_parameters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `presales`
--
ALTER TABLE `presales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `presupuestos`
--
ALTER TABLE `presupuestos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `presupuesto_concepts`
--
ALTER TABLE `presupuesto_concepts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `provinces`
--
ALTER TABLE `provinces`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `real_time_speed`
--
ALTER TABLE `real_time_speed`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `receipts`
--
ALTER TABLE `receipts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `road_map`
--
ALTER TABLE `road_map`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `roles_users`
--
ALTER TABLE `roles_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `seating`
--
ALTER TABLE `seating`
  MODIFY `number` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `seating_detail`
--
ALTER TABLE `seating_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `services_has_discounts`
--
ALTER TABLE `services_has_discounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `service_pending`
--
ALTER TABLE `service_pending`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sms`
--
ALTER TABLE `sms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sms_bloques`
--
ALTER TABLE `sms_bloques`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sms_templates`
--
ALTER TABLE `sms_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `status_tickets`
--
ALTER TABLE `status_tickets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `stores`
--
ALTER TABLE `stores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `template_resources`
--
ALTER TABLE `template_resources`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tickets_records`
--
ALTER TABLE `tickets_records`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tp_transactions`
--
ALTER TABLE `tp_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `visa_auto_debit_accounts`
--
ALTER TABLE `visa_auto_debit_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `visa_auto_debit_records`
--
ALTER TABLE `visa_auto_debit_records`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `visa_auto_debit_transactions`
--
ALTER TABLE `visa_auto_debit_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `web_proxy_access`
--
ALTER TABLE `web_proxy_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
