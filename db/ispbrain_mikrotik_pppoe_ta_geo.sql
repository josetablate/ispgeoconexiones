-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 30-04-2020 a las 03:32:19
-- Versión del servidor: 10.4.12-MariaDB-log
-- Versión de PHP: 7.2.11

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ispbrain_mikrotik_pppoe_ta_geo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `controllers`
--

CREATE TABLE `controllers` (
  `id` int(11) NOT NULL,
  `local_address` varchar(15) DEFAULT NULL,
  `dns_server` varchar(15) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `queue_default` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ip_excluded`
--

CREATE TABLE `ip_excluded` (
  `id` int(11) NOT NULL,
  `ip` varchar(45) NOT NULL,
  `controller_id` int(11) NOT NULL,
  `comments` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `phinxlog`
--

CREATE TABLE `phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plans`
--

CREATE TABLE `plans` (
  `id` int(11) NOT NULL,
  `controller_id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `pool_id` int(11) DEFAULT NULL,
  `service_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pools`
--

CREATE TABLE `pools` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `addresses` varchar(18) NOT NULL,
  `min_host` varchar(15) NOT NULL,
  `max_host` varchar(15) NOT NULL,
  `next_pool_id` int(11) DEFAULT NULL,
  `controller_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pppoe_servers`
--

CREATE TABLE `pppoe_servers` (
  `id` int(11) NOT NULL,
  `interface` varchar(45) NOT NULL,
  `one_session_per_host` varchar(45) NOT NULL,
  `authentication` varchar(45) NOT NULL,
  `service_name` varchar(45) NOT NULL,
  `disabled` varchar(45) NOT NULL,
  `controller_id` int(11) NOT NULL,
  `api_id` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profiles`
--

CREATE TABLE `profiles` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `local_address` varchar(15) DEFAULT NULL,
  `dns_server` varchar(45) DEFAULT NULL,
  `down` varchar(6) DEFAULT NULL,
  `up` varchar(6) DEFAULT NULL,
  `down_burst` varchar(45) DEFAULT NULL,
  `up_burst` varchar(45) DEFAULT NULL,
  `down_threshold` varchar(45) DEFAULT NULL,
  `up_threshold` varchar(45) DEFAULT NULL,
  `down_time` varchar(45) DEFAULT NULL,
  `up_time` varchar(45) DEFAULT NULL,
  `priority` varchar(45) DEFAULT NULL,
  `down_at_limit` varchar(45) DEFAULT NULL,
  `up_at_limit` varchar(45) DEFAULT NULL,
  `up_queue_type` varchar(45) DEFAULT NULL,
  `down_queue_type` varchar(45) DEFAULT NULL,
  `controller_id` int(11) NOT NULL,
  `api_id` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `queues`
--

CREATE TABLE `queues` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `target` bigint(20) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `connection_id` int(11) NOT NULL,
  `controller_id` int(11) NOT NULL,
  `api_id` varchar(45) DEFAULT NULL,
  `pool_id` int(11) DEFAULT NULL,
  `profile_id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secrets`
--

CREATE TABLE `secrets` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `service` varchar(45) NOT NULL,
  `local_address` int(11) DEFAULT NULL,
  `remote_address` bigint(20) NOT NULL,
  `last_logged_out` varchar(45) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `connection_id` int(11) NOT NULL,
  `controller_id` int(11) NOT NULL,
  `api_id` varchar(45) DEFAULT NULL,
  `profile` varchar(45) DEFAULT 'default',
  `caller_id` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `controllers`
--
ALTER TABLE `controllers`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ip_excluded`
--
ALTER TABLE `ip_excluded`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `phinxlog`
--
ALTER TABLE `phinxlog`
  ADD PRIMARY KEY (`version`);

--
-- Indices de la tabla `plans`
--
ALTER TABLE `plans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `controller_id` (`controller_id`),
  ADD KEY `profile_id` (`profile_id`),
  ADD KEY `pool_id` (`pool_id`);

--
-- Indices de la tabla `pools`
--
ALTER TABLE `pools`
  ADD PRIMARY KEY (`id`),
  ADD KEY `next_pool_id` (`next_pool_id`),
  ADD KEY `controller_id` (`controller_id`);

--
-- Indices de la tabla `pppoe_servers`
--
ALTER TABLE `pppoe_servers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `controller_id` (`controller_id`);

--
-- Indices de la tabla `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `controller_id` (`controller_id`);

--
-- Indices de la tabla `queues`
--
ALTER TABLE `queues`
  ADD PRIMARY KEY (`id`),
  ADD KEY `controller_id` (`controller_id`),
  ADD KEY `pool_id` (`pool_id`),
  ADD KEY `profile_id` (`profile_id`),
  ADD KEY `plan_id` (`plan_id`);

--
-- Indices de la tabla `secrets`
--
ALTER TABLE `secrets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `controller_id` (`controller_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `controllers`
--
ALTER TABLE `controllers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ip_excluded`
--
ALTER TABLE `ip_excluded`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `plans`
--
ALTER TABLE `plans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pools`
--
ALTER TABLE `pools`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pppoe_servers`
--
ALTER TABLE `pppoe_servers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `queues`
--
ALTER TABLE `queues`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `secrets`
--
ALTER TABLE `secrets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
