$(document).ready(function(){
	// HTML markup implementation, overlap mode
	$( '#menu' ).multilevelpushmenu({
		direction: 'rtl',
		backItemIcon: 'fas fa-angle-right',
		groupIcon: 'fas fa-plus'
	});
});