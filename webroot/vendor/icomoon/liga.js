/* A polyfill for browsers that don't support ligatures. */
/* The script tag referring to this file must be placed before the ending body tag. */

/* To provide support for elements dynamically added, this script adds
   method 'icomoonLiga' to the window object. You can pass element references to this method.
*/
(function () {
    'use strict';
    function supportsProperty(p) {
        var prefixes = ['Webkit', 'Moz', 'O', 'ms'],
            i,
            div = document.createElement('div'),
            ret = p in div.style;
        if (!ret) {
            p = p.charAt(0).toUpperCase() + p.substr(1);
            for (i = 0; i < prefixes.length; i += 1) {
                ret = prefixes[i] + p in div.style;
                if (ret) {
                    break;
                }
            }
        }
        return ret;
    }
    var icons;
    if (!supportsProperty('fontFeatureSettings')) {
        icons = {
            'home': '&#xe900;',
            'house': '&#xe900;',
            'office': '&#xe903;',
            'buildings': '&#xe903;',
            'pencil': '&#xe905;',
            'write': '&#xe905;',
            'pencil2': '&#xe906;',
            'write2': '&#xe906;',
            'image': '&#xe90d;',
            'picture': '&#xe90d;',
            'images': '&#xe90e;',
            'pictures': '&#xe90e;',
            'bullhorn': '&#xe91a;',
            'megaphone': '&#xe91a;',
            'connection': '&#xe91b;',
            'wifi': '&#xe91b;',
            'podcast': '&#xe91c;',
            'broadcast': '&#xe91c;',
            'profile': '&#xe923;',
            'file2': '&#xe923;',
            'file-empty': '&#xe924;',
            'file3': '&#xe924;',
            'files-empty': '&#xe925;',
            'files': '&#xe925;',
            'file-text2': '&#xe926;',
            'file4': '&#xe926;',
            'file-picture': '&#xe927;',
            'file5': '&#xe927;',
            'stack': '&#xe92e;',
            'layers': '&#xe92e;',
            'folder-download': '&#xe933;',
            'directory5': '&#xe933;',
            'barcode': '&#xe937;',
            'cart': '&#xe93a;',
            'purchase': '&#xe93a;',
            'coin-dollar': '&#xe93b;',
            'money': '&#xe93b;',
            'address-book': '&#xe944;',
            'contact': '&#xe944;',
            'envelop': '&#xe945;',
            'mail': '&#xe945;',
            'location': '&#xe947;',
            'map-marker': '&#xe947;',
            'location2': '&#xe948;',
            'map-marker2': '&#xe948;',
            'map': '&#xe94b;',
            'guide': '&#xe94b;',
            'map2': '&#xe94c;',
            'guide2': '&#xe94c;',
            'history': '&#xe94d;',
            'time': '&#xe94d;',
            'alarm': '&#xe950;',
            'time4': '&#xe950;',
            'calendar': '&#xe953;',
            'date': '&#xe953;',
            'printer': '&#xe954;',
            'print': '&#xe954;',
            'floppy-disk': '&#xe962;',
            'save2': '&#xe962;',
            'database': '&#xe964;',
            'db': '&#xe964;',
            'undo2': '&#xe967;',
            'left': '&#xe967;',
            'user-plus': '&#xe973;',
            'user2': '&#xe973;',
            'user-minus': '&#xe974;',
            'user3': '&#xe974;',
            'user-check': '&#xe975;',
            'user4': '&#xe975;',
            'user-tie': '&#xe976;',
            'user5': '&#xe976;',
            'search': '&#xe986;',
            'magnifier': '&#xe986;',
            'lock': '&#xe98f;',
            'secure': '&#xe98f;',
            'unlocked': '&#xe990;',
            'lock-open': '&#xe990;',
            'wrench': '&#xe991;',
            'tool': '&#xe991;',
            'cog': '&#xe994;',
            'gear': '&#xe994;',
            'cogs': '&#xe995;',
            'gears': '&#xe995;',
            'hammer': '&#xe996;',
            'tool2': '&#xe996;',
            'magic-wand': '&#xe997;',
            'wizard': '&#xe997;',
            'pie-chart': '&#xe99a;',
            'stats': '&#xe99a;',
            'stats-dots': '&#xe99b;',
            'stats2': '&#xe99b;',
            'stats-bars': '&#xe99c;',
            'stats3': '&#xe99c;',
            'bin': '&#xe9ac;',
            'trashcan': '&#xe9ac;',
            'truck': '&#xe9b0;',
            'transit': '&#xe9b0;',
            'road': '&#xe9b1;',
            'asphalt': '&#xe9b1;',
            'switch': '&#xe9b6;',
            'power-cord': '&#xe9b7;',
            'plugin': '&#xe9b7;',
            'clipboard': '&#xe9b8;',
            'board': '&#xe9b8;',
            'list-numbered': '&#xe9b9;',
            'options': '&#xe9b9;',
            'cloud': '&#xe9c1;',
            'weather': '&#xe9c1;',
            'cloud-check': '&#xe9c4;',
            'cloud4': '&#xe9c4;',
            'link': '&#xe9cb;',
            'chain': '&#xe9cb;',
            'attachment': '&#xe9cd;',
            'paperclip': '&#xe9cd;',
            'eye': '&#xe9ce;',
            'views': '&#xe9ce;',
            'eye-plus': '&#xe9cf;',
            'views2': '&#xe9cf;',
            'eye-minus': '&#xe9d0;',
            'views3': '&#xe9d0;',
            'eye-blocked': '&#xe9d1;',
            'views4': '&#xe9d1;',
            'bookmark': '&#xe9d2;',
            'ribbon': '&#xe9d2;',
            'shocked2': '&#xe9f2;',
            'emoticon20': '&#xe9f2;',
            'plus': '&#xea0a;',
            'add': '&#xea0a;',
            'minus': '&#xea0b;',
            'subtract': '&#xea0b;',
            'blocked': '&#xea0e;',
            'forbidden': '&#xea0e;',
            'cross': '&#xea0f;',
            'cancel': '&#xea0f;',
            'checkmark': '&#xea10;',
            'tick': '&#xea10;',
            'enter': '&#xea13;',
            'signin': '&#xea13;',
            'exit': '&#xea14;',
            'signout': '&#xea14;',
            'loop2': '&#xea2e;',
            'repeat2': '&#xea2e;',
            'arrow-right': '&#xea34;',
            'right3': '&#xea34;',
            'arrow-down-right': '&#xea35;',
            'down-right': '&#xea35;',
            'arrow-left': '&#xea38;',
            'left3': '&#xea38;',
            'arrow-right2': '&#xea3c;',
            'right4': '&#xea3c;',
            'arrow-down-right2': '&#xea3d;',
            'down-right2': '&#xea3d;',
            'tab': '&#xea45;',
            'arrows': '&#xea45;',
            'sort-alpha-asc': '&#xea48;',
            'arrange': '&#xea48;',
            'sort-alpha-desc': '&#xea49;',
            'arrange2': '&#xea49;',
            'sort-numeric-asc': '&#xea4a;',
            'arrange3': '&#xea4a;',
            'sort-numberic-desc': '&#xea4b;',
            'arrange4': '&#xea4b;',
            'opt': '&#xea51;',
            'option': '&#xea51;',
            'checkbox-checked': '&#xea52;',
            'checkbox': '&#xea52;',
            'checkbox-unchecked': '&#xea53;',
            'checkbox2': '&#xea53;',
            'filter': '&#xea5b;',
            'funnel': '&#xea5b;',
            'terminal': '&#xea81;',
            'console': '&#xea81;',
            'whatsapp': '&#xea93;',
            'brand13': '&#xea93;',
            'file-pdf': '&#xeadf;',
            'file10': '&#xeadf;',
            'file-excel': '&#xeae2;',
            'file13': '&#xeae2;',
          '0': 0
        };
        delete icons['0'];
        window.icomoonLiga = function (els) {
            var classes,
                el,
                i,
                innerHTML,
                key;
            els = els || document.getElementsByTagName('*');
            if (!els.length) {
                els = [els];
            }
            for (i = 0; ; i += 1) {
                el = els[i];
                if (!el) {
                    break;
                }
                classes = el.className;
                if (/icon-/.test(classes)) {
                    innerHTML = el.innerHTML;
                    if (innerHTML && innerHTML.length > 1) {
                        for (key in icons) {
                            if (icons.hasOwnProperty(key)) {
                                innerHTML = innerHTML.replace(new RegExp(key, 'g'), icons[key]);
                            }
                        }
                        el.innerHTML = innerHTML;
                    }
                }
            }
        };
        window.icomoonLiga();
    }
}());
