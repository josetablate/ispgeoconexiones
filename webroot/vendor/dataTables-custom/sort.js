/* *   
    <td>2016-11-15</td>

    $('#example').dataTable( {
       columnDefs: [
         { type: 'date-custom', targets: 0 }
       ]
    });
*/


jQuery.extend( jQuery.fn.dataTableExt.oSort, {"date-custom-pre": function ( a ) {

        if ($.trim(a) != '') {
            var frDatea = a.split('/');  // d/m-Y
            var x = new Date(frDatea[2],frDatea[1]-1,frDatea[0],0,0,0);
        } else {
            var x = new Date(0,0,0,0,0,0);
        }
        return x;
        },
        "date-custom-asc": function ( a, b ) {  return a > b; },
        "date-custom-desc": function ( a, b ) {  return b > a; }
    });
    
/* *   
    <td>2016-11-15 20:15:00</td>

    $('#example').dataTable( {
       columnDefs: [
         { type: 'datetime-custom', targets: 0 }
       ]
    });
*/


jQuery.extend( jQuery.fn.dataTableExt.oSort, {"datetime-custom-pre": function ( a ) {
    
        if ($.trim(a) != '') {
                        
            var frDatea = $.trim(a).split(' ');
            var frTimea = frDatea[1].split(':'); // hh:mm
            var frDatea2 = frDatea[0].split('/'); // d-m-Y 
            
            var x = new Date(frDatea2[2], (frDatea2[1] - 1), frDatea2[0], frTimea[0], frTimea[1], 0);
       
        } else {
            var x = new Date(0,0,0,0,0,0);
        }
        return x;
        },
        "datetime-custom-asc": function ( a, b ) {  return a > b; },
        "datetime-custom-desc": function ( a, b ) {  return b > a; }
    });
    
    
/* *   
    <td>-385,45</td>
    <td>1000,45</td>

    $('#example').dataTable( {
       columnDefs: [
         { type: 'numeric-comma', targets: 0 }
       ]
    });
*/

    
jQuery.extend( jQuery.fn.dataTableExt.oSort, {
	"numeric-comma-pre": function ( a ) {
	    a = a.toString();
	    var b = a.replace('.', '');
	    var c = b.replace(',', ".");
		return parseFloat( c);
	},

	"numeric-comma-asc": function ( a, b ) {
	     a = a.toString();
	     b = b.toString();
		return ((a < b) ? -1 : ((a > b) ? 1 : 0));
	},

	"numeric-comma-desc": function ( a, b ) {
	     a = a.toString();
	     b = b.toString();
		return ((a < b) ? 1 : ((a > b) ? -1 : 0));
	}
} );

/* *   
    <td>10.200.45.1</td>
    <td>10.200.45.10</td>
    <td>10.200.45.2</td>
  

    $('#example').dataTable( {
       columnDefs: [
         { type: 'ip-address', targets: 0 }
       ]
    });
*/

jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "ip-address-pre": function ( a ) {
        
        console.log(a);
        
        var i, item;
        var m = a.split("."),
            n = a.split(":"),
            x = "",
            xa = "";
 
        if (m.length == 4) {
            // IPV4
            for(i = 0; i < m.length; i++) {
                item = m[i];
 
                if(item.length == 1) {
                    x += "00" + item;
                }
                else if(item.length == 2) {
                    x += "0" + item;
                }
                else {
                    x += item;
                }
            }
        }
        else if (n.length > 0) {
            // IPV6
            var count = 0;
            for(i = 0; i < n.length; i++) {
                item = n[i];
 
                if (i > 0) {
                    xa += ":";
                }
 
                if(item.length === 0) {
                    count += 0;
                }
                else if(item.length == 1) {
                    xa += "000" + item;
                    count += 4;
                }
                else if(item.length == 2) {
                    xa += "00" + item;
                    count += 4;
                }
                else if(item.length == 3) {
                    xa += "0" + item;
                    count += 4;
                }
                else {
                    xa += item;
                    count += 4;
                }
            }
 
            // Padding the ::
            n = xa.split(":");
            var paddDone = 0;
 
            for (i = 0; i < n.length; i++) {
                item = n[i];
 
                if (item.length === 0 && paddDone === 0) {
                    for (var padding = 0 ; padding < (32-count) ; padding++) {
                        x += "0";
                        paddDone = 1;
                    }
                }
                else {
                    x += item;
                }
            }
        }
 
        return x;
    },
 
    "ip-address-asc": function ( a, b ) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },
 
    "ip-address-desc": function ( a, b ) {
        return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    }
});


/* *   
    <td>1.</td>
    <td>1.1.</td>
    <td>1.2.</td>
    <td>1.10</td>
    <td>1.12</td>
    <td>2.</td>
    <td>2.1</td>
  

    $('#example').dataTable( {
       columnDefs: [
         { type: 'code-account', targets: 0 }
       ]
    });
*/

// function pad (str, max) {
//   str = str.toString();
//   return str.length < max ? pad("0" + str, max) : str;
// }

// jQuery.extend( jQuery.fn.dataTableExt.oSort, {
// 	"code-account-pre": function ( a ) {
	    
// 	    var array = a.split('.');
	    
// 	    var r = '';
// 	    var i = 0;
// 	    for(i; i < array.length - 1; i++){
// 	        r += pad(array[i],5)  + '.';
// 	    }
	    
// 		return r;
// 	},

// 	"code-account-asc": function ( a, b ) {
// 		return ((a < b) ? -1 : ((a > b) ? 1 : 0));
// 	},

// 	"code-account-desc": function ( a, b ) {
// 		return ((a < b) ? 1 : ((a > b) ? -1 : 0));
// 	}
// } );