var arrMenu = [
  {
    title: 'ISP GEO',
    id: 'menuID',
    icon: 'fa fa-reorder',
   
    items: [
    
      {
        name: 'Home',
        id: 'itemID',
        icon: 'fa fa-home',
        link: '/ispbrain/Home/dashboard',
      },
     
      {
        name: 'Clientes',
        icon: 'fa fa-users',
        link: '#',
        items: [
          {
            title: 'Clientes',
            icon: 'fa fa-users',
            items: [
              {
                name: 'Agregar Nuevo',
                link: '/ispbrain/Customers/add',
                icon: 'fa fa-user-plus'
              },
              {
                name: 'Ver Lista',
                link: '/ispbrain/Customers/index',
                icon: 'fa fa-table'
              },
              {
                name: 'Cargar Pago',
                link: '/ispbrain/Payments/add',
                icon: 'fa fa-dollar'
              },
              {
                name: 'Vender Paquete',
                link: '/ispbrain/Debts/sellingPackage',
                icon: 'fa fa-shopping-cart '
              },
              {
                name: 'Vender Producto',
                link: '/ispbrain/Debts/sellingProduct',
                icon: 'fa fa-shopping-cart '
              },
              {
                name: 'Deudas Mensuales',
                link: '/ispbrain/Debts/addDebtServices/',
                icon: 'fa fa-magic',
                class: 'pepe'
              },
               {
                name: 'Ajustar Saldo',
                link: '/ispbrain/customers/fixSaldo',
                icon: 'fa fa-calculator'
              },
            ]
          }
        ]
      },
      
      {
        name: 'Conexiones',
        icon: 'fa fa-plug',
        link: '#',
        items: [
          {
            title: 'Conexiones',
            icon: 'fa fa-plug',
            items: [
              {
                name: 'Agregar Nueva',
                link: '/ispbrain/Connections/add',
                icon: 'fa fa-plus',
              },
              {
                name: 'Ver Lista',
                link: '/ispbrain/Connections/index',
                icon: 'fa fa-table',
              },
               {
                name: 'Avisos',
                link: '/ispbrain/Messages/index',
                icon: 'fa fa-bullhorn',
              },
               {
                name: 'IP Recicladas',
                link: '/ispbrain/FreePoolIps/index',
                icon: 'fa fa-recycle'
              },
            ]
          }
        ]
      },
      
      {
        name: 'Instalaciones',
        icon: 'fa fa-wrench',
        link: '#',
        items: [
          {
            title: 'Instalaciones',
            icon: 'fa fa-wrench',
            items: [
              {
                name: 'Rendir',
                link: '/ispbrain/Instalations/add',
                icon: 'fa fa-plus',
              },
              {
                name: 'Ver Lista',
                link: '/ispbrain/Instalations/index',
                icon: 'fa fa-table',
              },
              {
                name: 'Hoja de Ruta',
                link: '/ispbrain/RoadMap/index',
                icon: 'fa fa-road',
              },
            ]
          }
        ]
      },
      
      {
        name: 'Tickets',
        icon: 'fa fa-ticket',
        link: '#',
        items: [
          {
            title: 'Tickets',
            icon: 'fa fa-ticket',
            items: [
              {
                name: 'Nuevo',
                link: '/ispbrain/tickets/add',
                icon: 'fa fa-plus',
              },
              {
                name: 'Lista',
                link: '/ispbrain/tickets/index',
                icon: 'fa fa-table',
              },
            ]
          }
        ]
      },
      
      {
        name: 'Stock',
        icon: 'fa fa-archive',
        link: '#',
        items: [
          {
            title: 'Stock',
            icon: 'fa fa-archive',
            items: [
              {
                name: 'Stock Depostios',
                link: '/ispbrain/StockStores/index',
                icon: 'fa fa-table',
              },
              {
                name: 'Mover Stock',
                link: '/ispbrain/StockStores/move',
                icon: 'fa fa-random',
              },
              {
                name: 'Actualizar Stock',
                link: '/ispbrain/StockStores/add',
                icon: 'fa fa-plus',
              },
              {
                name: 'Artículos',
                link: '/ispbrain/Stock/index',
                icon: 'fa fa-cube',
              },
              {
                name: 'Despositos',
                link: '/ispbrain/Stores/index',
                icon: 'fa fa-building-o',
              },
            ]
          }
        ]
      },
    
      
      {
        name: 'Administración',
        icon: 'fa fa-clipboard',
        link: '#',
        items: [
        {
          title: 'Administración',
          icon: 'fa fa-clipboard',
          items: [
             {
                  name: 'Contable',
                  icon: 'fa fa-calculator',
                  link: '#',
                  items: [
                      {
                          title: 'Contable',
                          icon: 'fa fa-calculator',
                          items: [
                              {
                                name: 'Facturardor',
                                icon: 'fa fa-pencil-square-o',
                                link: '/ispbrain/Docs/generate',
                              },
                              {
                                name: 'Facturas',
                                icon: 'fa fa-calendar',
                                link: '/ispbrain/Docs/index',
                              },
                              {
                                name: 'Nota de Creditos',
                                icon: 'fa fa-sticky-note-o',
                                link: '/ispbrain/Docs/indexcredits'
                              },
                               {
                                name: 'Historicos',
                                icon: 'fa fa-history',
                                link: '/ispbrain/Debts/index'
                              },
                              {
                                name: 'Cajas',
                                icon: 'fa fa-cubes',
                                link: '#',
                                items: [
                                    {
                                      title: 'Cajas',
                                      icon: 'fa fa-cubes',
                                      items: [
                                          {
                                            name: 'Mi Caja',
                                            icon: 'fa fa-cube',
                                            link: '/ispbrain/CashEntities/pointSaleView',
                                          },
                                          {
                                            name: 'Lista',
                                            icon: 'fa fa-table',
                                            link: '/ispbrain/CashEntities/index',
                                          }
                                      ]
                                    }
                                ]
                              }
                          ]
                      }
                  ]
              },
              {
                name: 'Varios',
                icon: 'fa fa-briefcase',
                link: '#',
                items: [
                  {
                    title: 'Varios',
                    icon: 'fa fa-briefcase',
                    items: [
                        {
                          name: 'Servicios',
                          link: '/ispbrain/Services/index',
                          icon: 'fa fa-signal',
                        },
                        {
                          name: 'Productos',
                          link: '/ispbrain/Products/index',
                          icon: 'fa fa-tag',
                        },
                        {
                          name: 'Paquetes',
                          link: '/ispbrain/Packages/index',
                          icon: 'fa fa-tags',
                        },
                        {
                          name: 'Metodos de pago',
                          link: '/ispbrain/PaymentMethods/index',
                          icon: 'fa fa-credit-card',
                        },
                        {
                          name: 'Ciudades',
                          link: '/ispbrain/Cities/index',
                          icon: 'fa fa-map-o',
                        },
                    ]
                  }
                ]
              },
            ]
          }
        ]
      },
      
      {
        name: 'Sistema',
        icon: 'fa fa-cogs',
        link: '#',
        items: [
          {
            title: 'Sistema',
            icon: 'fa fa-cogs',
            items: [
              {
                name: 'Usuarios',
                link: '/ispbrain/users/index',
                icon: 'fa fa-user',
              },
              {
                name: 'Privilegios',
                link: '/ispbrain/Roles/index',
                icon: 'fa fa-unlock-alt',
              },
              {
                name: 'Routers',
                link: '/ispbrain/Nodes/index',
                icon: 'fa fa-wifi',
              },
              {
                name: 'Perfiles',
                link: '/ispbrain/Speeds/index',
                icon: 'fa fa-tachometer',
              },
              {
                name: 'Redes',
                link: '/ispbrain/Networks/index',
                icon: 'fa fa-cogs',
              },
              {
                name: 'Configuración',
                link: '/ispbrain/Paraments/edit1',
                icon: 'fa fa-sliders',
              },
              {
                name: 'Logs',
                link: '/ispbrain/Logs/index',
                icon: 'fa fa-history',
              },
              
            ]
          }
        ]
      },
      
      {
        name: 'Cerrar Sesión',
        icon: 'fa fa-sign-out',
        link: '/ispbrain/Users/logout',
      },
    ]
  }
];


$(document).ready(function(){
	
	var menuWidth = '13%';

	
	if(screen.width < 500 ||
	 navigator.userAgent.match(/Android/i) ||
	 navigator.userAgent.match(/webOS/i) ||
	 navigator.userAgent.match(/iPhone/i) ||
	 navigator.userAgent.match(/iPod/i)) {
	
		menuWidth = '50%';
	}
	else 	if(screen.width < 1200 ||
	 navigator.userAgent.match(/Android/i) ||
	 navigator.userAgent.match(/webOS/i) ||
	 navigator.userAgent.match(/iPhone/i) ||
	 navigator.userAgent.match(/iPod/i)) {
	
		menuWidth = '20%';
	}
	
	$( '#menu' ).multilevelpushmenu({
		containersToPush: [$( '.container-fuild' )],
		menuWidth: menuWidth,
		menuHeight: '100%',
		collapsed: true, 
		backText: 'Atras', 
		menu: arrMenu,
		preventItemClick: true,                                
    preventGroupItemClick: true, 
    onItemClick: function() {
        var event = arguments[0], $menuLevelHolder = arguments[1], $item = arguments[2], options = arguments[3];
        var itemHref = $item.find( 'a:first' ).attr( 'href' );
        if(itemHref.match(/addDebtServices/i)){
          
          bootbox.confirm('Esta seguro que desea generar la deuda mensual de todos los clientes ?   ', function(result) {
            if(result){
                window.open(itemHref, '_self');
              }
          });
        }
        else  if(itemHref.match(/logout/i)){
          bootbox.confirm('¿Salir del Sistema?   ', function(result) {
            if(result){
                window.open(itemHref, '_self');
              }
          }).find("div.modal-content").addClass("confirm-width-sm");
        }
        else{
           location.href = itemHref;
        }
    }
	});

$(window).resize(function () {
	$( '#menu' ).multilevelpushmenu( 'redraw' );
});


