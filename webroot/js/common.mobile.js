    function pad (str, max) {
        str = str.toString();
        return str.length < max ? pad("0" + str, max) : str;
    }

   /** 
    * notificaciones 
    * type: warning, error, information, success
    * text: html
    */
    function generateNoty(type, text, closeWith = ['click']) {

        var animationIn  = '';
        var animationOut  = '';

        switch (type) {
            case 'error':
                animationIn  = 'shake';
                animationOut  = 'fadeOut';
                break;
            case 'warning':
                animationIn  = 'fadeInDown';
                animationOut  = 'fadeOut';
            break;
            case 'success':
                animationIn  = 'bounceInRight';
                animationOut  = 'bounceOutLeft';
                break;
            case 'information': 
                animationIn  = 'bounceInRight';
                animationOut  = 'bounceOutLeft';
                break;
        }

        var n = new Noty({
            text        : text,
            type        : type,
            dismissQueue: true,
            layout      : 'topCenter', /*top, topLeft, topCenter, topRight, center, centerLeft, centerRight, bottom, bottomLeft, bottomCenter, bottomRight*/
            closeWith   : closeWith, /*['click', 'button', 'hover', 'backdrop']*/
            theme       : 'relax',
            maxVisible  : 5,
            animation   : {
                open  : 'animated '+animationIn,
                close : 'animated '+animationOut,
                easing: 'swing',
                speed : 500
            }
        });

        n.show();

        return n;
    }

    $(document).ready(function() {

        $('body').css('overflow', 'auto');

        $('.modal').on('shown.bs.modal', function() {
            //Make sure the modal and backdrop are siblings (changes the DOM)
            $(this).before($('.modal-backdrop'));
            //Make sure the z-index is higher than the backdrop
            $(this).css("z-index", parseInt($('.modal-backdrop').css('z-index')) + 1);
        });
    });

    function createModalHideColumn(table, modal) {

        $.each(table.columns()[0], function(i, val) {

            var column = table.column(i);
            var ckecked = column.visible() ? 'checked' : '';
            var name = $(column.header()).html();

            var checkbox = "";
            checkbox  = "<div class='col-3'>";
            checkbox += "   <label class='form-check-label'>";
            checkbox += "       <input type='checkbox' class='form-check-input toggle-hide-column' " + ckecked + " data-column='" + i + "' > " + name;            
            checkbox += "   </label>";
            checkbox += "</div>";

            $(modal + ' .checkbox-container').append(checkbox);
        });

        $('.toggle-hide-column').on( 'click', function (e) {

            var column =  table.column( $(this).data('column'));
            column.visible(  $(this).is(":checked") );
            table.columns.adjust().draw();
        });
    }

    function calulateIvaAndNeto(total, aliquot_per, quantity) {

        var neto = total / ( aliquot_per + 1);

        neto = neto.toFixed(2);

        var iva = total - neto;

        var iva2 =  neto * aliquot_per;

        iva = iva.toFixed(2);
        iva2 = iva2.toFixed(2);

        if (iva < iva2) {
            iva = iva2;
        }

        var price = neto / quantity;

        price = price.toFixed(2);

        var response = {
            quantity: quantity,
            price: parseFloat(price),
            sum_price: parseFloat(neto),
            sum_tax: parseFloat(iva),
            total: total
        }

        return response;
    }

    function number_format(number, showSymbol) {

        if (showSymbol) {
            return new Intl.NumberFormat("es-AR", {style: "currency", currency: "ARS", minimumFractionDigits: 2, maximumFractionDigits: 2}).format(number);
        }
        return new Intl.NumberFormat("es-AR",  {minimumFractionDigits: 2, maximumFractionDigits: 2}).format(number);
    }

    function autoScroll(intance_table) {
        var $scrollBody = $(intance_table.table().node()).parent();
        $scrollBody.scrollTop($scrollBody.get(0).scrollHeight);
    }

    function checkStatusFilter() {

        var apply = false;

        $.each($('input.column_search'), function() { 
            if ($(this).val().length > 0) {
                apply = true;
            }
        });

        if (apply) {
            $('.btn-filter-column').addClass('filter-apply');
        } else {
            $('.btn-filter-column').removeClass('filter-apply');
        }
    }

    function validateExistCBU(cbu, callback) {

        var request = $.ajax({
            url: "/ispbrain/CustomersAccounts/validateExistCBU.json",
            method: "POST",
            data: JSON.stringify({ cbu : cbu }),
            dataType: "json"
        });

        request.done(function(response) {
            if (!response.data.error) {
                callback(true);
            } else {
                callback(false);
                generateNoty('warning', response.data.msgError);
            }
        });

        request.fail(function(jqXHR) {
            if (jqXHR.status == 403) {

            	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            	setTimeout(function() { 
                    window.location.href = "/ispbrain";
            	}, 3000);

            } else {
                callback(false);
            	generateNoty('error','Error al verificar si está en uso el CBU: ' + cbu);
            }
        });
    }

    function editAccount(account, callback) {

        var request = $.ajax({
            url: "/ispbrain/CustomersAccounts/edit.json",
            method: "POST",
            data: JSON.stringify(account),
            dataType: "json"
        });

        request.done(function(response) {

            if (!response.data.error) {
                generateNoty(response.data.type, response.data.msg);
                callback(account);
            } else {
                generateNoty(response.data.type, response.data.msg);
            }
        });

        request.fail(function(jqXHR) {

            if (jqXHR.status == 403) {

            	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            	setTimeout(function() { 
                    window.location.href = "/ispbrain";
            	}, 3000);

            } else {
            	generateNoty('error', 'Error al editar la cuenta');
            }
        });
    }
