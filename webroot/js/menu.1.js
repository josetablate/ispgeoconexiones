 $(document).ready(function(){

  $('#menu2').multilevelpushmenu({
    menu: sessionPHP.menu,
    menuWidth: '100%',
    mode: 'cover',   
    // menuWidth: 0,                                              // Wrapper width (integer, '%', 'px', 'em').
    // menuHeight: 0, 
    container: $( '#menu2' )
  });
  	
});
 
$(window).resize(function () {
    $( '#menu2' ).multilevelpushmenu( 'redraw' );
});
