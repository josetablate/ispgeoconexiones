 
 	var menuWidth = '60%';
	var iscollapsed = true;
	
	var onExpandMenu = !iscollapsed;
	var currentlevelMenuExpand = !iscollapsed ? 0 : -1;
	
 
 $(document).ready(function(){

    	
    	$( '#menu' ).multilevelpushmenu({
    		menuWidth: menuWidth,
    		container: $( '#menu' ),
    		menuHeight: '100%',
    		menuID: 'multilevelpushmenu',                              // ID of the <nav> element.
            wrapperClass: 'multilevelpushmenu_wrapper',                // Wrapper CSS class.
            menuInactiveClass: 'multilevelpushmenu_inactive',   
        	collapsed: iscollapsed, 
    		backText: 'Atras', 
    		mode: 'cover',  
    		menu: sessionPHP.menu,
    		preventItemClick: true,                                
            preventGroupItemClick: true, 
            fullCollapse: false,  
            onItemClick: function() {
                var event = arguments[0], $menuLevelHolder = arguments[1], $item = arguments[2], options = arguments[3];
                
                var itemHref = $item.find( 'a:first' ).attr( 'href' );
                
                if(itemHref.match(/logout/i)){
                  
                  bootbox.confirm('¿Salir del Sistema?   ', function(result) {
                    if(result){
                        window.open(itemHref, '_self');
                      }
                  }).find("div.modal-content").addClass("confirm-width-sm");
                }
                else{
                   location.href = itemHref;
                }
            }, 
             onTitleItemClick: function() {
                if(!iscollapsed){
                    $('#menu').multilevelpushmenu('expand');
                }
            },
            onExpandMenuStart: function(event){

              $("body").attr("style", "overflow: hidden");
              $('.menu-modal').fadeIn();
              
              
              if(!onExpandMenu){
                onExpandMenu = true;
              }
              if(currentlevelMenuExpand < 1){
                currentlevelMenuExpand++;
              }
            },
            onCollapseMenuStart: function(){
              
              
              currentlevelMenuExpand--;
              if(currentlevelMenuExpand == -1){
                if(iscollapsed){
                  onExpandMenu = false;
                  
                      
                   $("body").attr("style", "overflow: auto");
                   $('.menu-modal').fadeOut();
                  
                  
                }
              }
            },
    	});  
     
  });
  
   $(window).resize(function () {
    	$( '#menu' ).multilevelpushmenu( 'redraw' );
   });
  
   $(document).keydown(function(e){
    	  
        if(e.keyCode==220){ //tecla arriba del tab
          if(onExpandMenu){
            if(iscollapsed){
               $('#menu').multilevelpushmenu('collapse');
               onExpandMenu = false;
            }
          }else{
            onExpandMenu = true;
            $('#menu').multilevelpushmenu('expand');
          }
        }
        
        if(e.keyCode==27){ //tecla ESC
    
          if(onExpandMenu){
            if(currentlevelMenuExpand == 0){
              if(iscollapsed){
                 $('#menu').multilevelpushmenu('collapse');
              }
            }
            else if(currentlevelMenuExpand > 0){

               $('#menu').multilevelpushmenu('collapse', currentlevelMenuExpand-1);
               
            }
          }
        }
    });