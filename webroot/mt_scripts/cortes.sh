/ip firewall address-list
add address=10.0.0.0/8 list="[LIST_PRIVADAS_NAME]"
add address=172.16.0.0/12 list="[LIST_PRIVADAS_NAME]"
add address=192.168.0.0/16 list="[LIST_PRIVADAS_NAME]"
add address=100.64.0.0/10 list="[LIST_PRIVADAS_NAME]" 
/ip firewall filter
add action=drop chain=forward comment="Bloqueo por corte - ISPBrain" dst-address-list=!"[LIST_PRIVADAS_NAME]" src-address-list="[LIST_CORTES_NAME]"
