/ip firewall address-list
add address=10.0.0.0/8 list="[LIST_PRIVADAS_NAME]"
add address=172.16.0.0/12 list="[LIST_PRIVADAS_NAME]"
add address=192.168.0.0/16 list="[LIST_PRIVADAS_NAME]"
add address=100.64.0.0/10 list="[LIST_PRIVADAS_NAME]" 
/ip firewall filter
add chain=input comment="Aceptaciones de avisos - ISPBrain" port=8085 protocol=tcp src-address-list="[LIST_AVISOS_NAME]" 
add chain=input port=8085 protocol=tcp src-address-list="[LIST_CORTES_NAME]" 
add chain=forward dst-port=53 protocol=udp src-address-list="[LIST_CORTES_NAME]" 
add chain=input dst-port=53 protocol=udp src-address-list="[LIST_CORTES_NAME]" 
add chain=forward port=81 protocol=tcp comment="[FORWARD]" 
add action=drop chain=forward comment="Bloqueo por corte - ISPBrain" dst-address-list=!"[LIST_PRIVADAS_NAME]" src-address-list="[LIST_CORTES_NAME]" 
/ip firewall nat
add action=redirect chain=dstnat comment="Redireccion avisos - ISPBrain"  dst-address-list=!"[LIST_PRIVADAS_NAME]" dst-port=80 protocol=tcp src-address-list="[LIST_AVISOS_NAME]" to-ports=8085
add action=redirect chain=dstnat comment="Redireccion cortes - ISPbrain"  dst-address-list=!"[LIST_PRIVADAS_NAME]" dst-port=80 protocol=tcp src-address-list="[LIST_CORTES_NAME]" to-ports=8085
/ip proxy 
set enabled=yes port=8085
