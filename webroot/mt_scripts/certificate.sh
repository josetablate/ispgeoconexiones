/certificate 
add name=[CERTIFICATE_NAME] common-name=[COMMON_NAME] key-usage="key-cert-sign,crl-sign"
:delay 3 
sign [CERTIFICATE_NAME] name=[COMMON_NAME]
/ip service 
set www-ssl disabled=no certificate=[COMMON_NAME] 
set api-ssl disabled=no certificate=[COMMON_NAME]
