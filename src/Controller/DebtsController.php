<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use App\Controller\Component\ComprobantesComponent;
use App\Controller\Component\PDFGeneratorComponent;
use Cake\Utility\Security;
use Cake\Event\Event;
use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Query;

/**
 * Debts Controller
 *
 * @property \App\Model\Table\DebtsTable $Debts
 */
class DebtsController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Accountant', [
            'className' => '\App\Controller\Component\Admin\Accountant'
        ]);

        $this->loadComponent('CurrentAccount', [
            'className' => '\App\Controller\Component\Admin\CurrentAccount'
        ]);

        $this->loadComponent('FiscoAfipComp', [
            'className' => '\App\Controller\Component\Admin\FiscoAfipComp'
        ]);

        $this->loadComponent('FiscoAfipCompPdf', [
            'className' => '\App\Controller\Component\Admin\FiscoAfipCompPdf'
        ]);
    }

    public function isAuthorized($user = null)
    {
        if ($this->request->getParam('action') == 'editAjax') {
            return true;
        }

        if ($this->request->getParam('action') == 'debtPreview') {
            return true;
        }

        if ($this->request->getParam('action') == 'getWithoutInvoice') {
            return true;
        }

        if ($this->request->getParam('action') == 'getWithoutInvoiceCustomer') {
            return true;
        }

        if ($this->request->getParam('action') == 'generate') {
            return true;
        }

        if ($this->request->getParam('action') == 'sellingService') {
            return true;
        }

        if ($this->request->getParam('action') == 'sellingServiceEdit') {
            return true;
        }

        if ($this->request->getParam('action') == 'sellingPackageEdit') {
            return true;
        }

        if ($this->request->getParam('action') == 'sellingProductEdit') {
            return true;
        }

        if ($this->request->getParam('action') == 'delete') {
            return true;
        }

        if ($this->request->getParam('action') == 'deleteMasive') {
            return true;
        }

        if ($this->request->getParam('action') == 'getPayments') {
            return true;
        }

        if ($this->request->getParam('action') == 'getNameMonth') {
            return true;
        }

        if ($this->request->getParam('action') == 'debtMonthControl') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow('debtMonthControl');
    }

    public function index()
    {
        $debts = $this->Debts->find()
            ->contain([
                'Users',
                'Invoices',
                'Customers'
            ]);

        $this->loadModel('Payments');

        $payments= $this->Payments->find()
            ->contain([
                'Users',
                'CashEntities',
                'Customers'
            ]);

        $paraments = $this->request->getSession()->read('paraments');

        foreach ($payments as $payment) {

            foreach ($paraments->payment_getway as $pg) {

                if ($pg->id == $payment->payment_method_id) {
                    $payment_method = new \stdClass;
                    $payment_method = $pg;
                    $payment->payment_method = $payment_method;
                }
            }
        }

        $this->set(compact('debts','payments'));
        $this->set('_serialize', ['debts','payments']);
    }

    /**
     * called from 
     *  Invoices/generate
     */
    public function getWithoutInvoice()
    {
       if ($this->request->is('ajax')) {

            $response =  new \stdClass();
            $response->draw = intval($this->request->getQuery('draw'));
            $response->recordsTotal = 0;
            $response->data = [];
            $response->recordsFiltered = 0;

            $response = $this->ServerSideDebtsMoves($response);

            $response = $this->ServerSideCustomersDiscountsMoves($response);

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    private function ServerSideDebtsMoves($response)
    {
        $this->loadModel('Debts');

        $where = []; ;

        $where += ['Debts.period' => $this->request->getQuery('period')];
        $where += ['Debts.invoice_id IS' => NULL];
        $where += ['Customers.deleted' => false];

        $response->recordsTotal += $this->Debts->find()->contain(['Customers'])->where($where)->count();

        $debts = $this->Debts->find('ServerSideData', [
            'params' => $this->request->getQuery(),
            'where' =>  $where
        ]);

        $response->recordsFiltered += $this->Debts->find('RecordsFiltered', [
            'params' => $this->request->getQuery(),
            'where' => $where
        ]);

        foreach ($debts as $debt) {
            $response->data[] = new Row($debt, 'debt');
        }

        return $response;
    } 

    private function ServerSideCustomersDiscountsMoves($response)
    {
        $this->loadModel('CustomersHasDiscounts');

        $where = [];

        $where += ['CustomersHasDiscounts.period' => $this->request->getQuery('period')];
        $where += ['CustomersHasDiscounts.invoice_id IS' => NULL];
        $where += ['CustomersHasDiscounts.debt_id IS' => NULL];
        $where += ['Customers.deleted' => false];

        $response->recordsTotal += $this->CustomersHasDiscounts->find()->contain(['Customers'])->where($where)->count();

        $customersHasDiscounts = $this->CustomersHasDiscounts->find('ServerSideData', [
            'params' => $this->request->getQuery(),
            'where' => $where
        ]);

        $response->recordsFiltered += $this->CustomersHasDiscounts->find('RecordsFiltered', [
            'params' => $this->request->getQuery(),
            'where' => $where
        ]);

        foreach ($customersHasDiscounts as $customerHasDiscount) {
            $response->data[] = new Row($customerHasDiscount, 'customer_has_discount'); 
        }

        return $response;
    } 

    public function getPayments()
    {
        if ($this->request->is('ajax')) {

            $payments = $this->Payments->find()
                ->contain([
                    'Users',
                    'CashEntities',
                    'Customers'
                ]);

            $paraments = $this->request->getSession()->read('paraments');

            foreach ($payments as $payment) {

                foreach ($paraments->payment_getway as $pg) {

                    if ($pg->id == $payment->payment_method_id) {
                        $payment_method = new \stdClass;
                        $payment_method = $pg;
                        $payment->payment_method = $payment_method;
                    }
                }
            }

            $this->set(compact('payments'));
            $this->set('_serialize', ['payments']);
        }
    }

    public function sellingProduct($from_presale = false)
    {
        $paraments = $this->request->getSession()->read('paraments');

        $user_id = $this->Auth->user()['id'];
        $session = $this->request->getSession();
        $connections = [];
        $flag = false;
        if (array_key_exists("data", $this->request->getData())) {
            $data = json_decode($this->request->getData('data'), true);
            if ($data['from'] == 'presale') {
                $flag = true;
            }
        }
        $presale = ($session->check('presale') && ($flag
                || $from_presale));
        if ($presale) {
            $presale = $session->read('presale');

            //verificar si es customer nuevo o existente
            if (isset($presale->customer->code)) {
                if ($presale->customer->billing_for_service) {
                    if (sizeof($presale->customer->connections) > 0) {
                        foreach ($presale->customer->connections as $connection) {

                            $connections[$connection->id] = $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address;
                        }
                    }
                }
            }
        }

        $this->loadModel('Discounts');
        $discounts = $this->Discounts->find()->where([
            'deleted' => false,
            'enabled' => true,
        ])->andWhere(function (QueryExpression $exp, Query $q) {
            return $exp->in('type', ['product', 'generic']);
        });

        $discounts_list = [];
        foreach ($discounts as $discount) {
            $discounts_list[$discount->id] = $discount->concept;
        }

        $this->loadModel('Stores');
        $this->loadModel('Accounts');

        if (!$this->Stores->find()->where(['user_id' => $user_id, 'enabled' => true, 'deleted' => false])->first()) {
            $this->Flash->warning(__('No tiene un depósito asigando.'));
            if ($presale) {
                return $this->redirect(['controller' => 'Presales', 'action' => 'add']);
            }
            return $this->redirect($this->referer());
        }

        $paraments = $this->request->getSession()->read('paraments');

        $debt_form = $this->Debts->newEntity();

        //para el vencimiento
        $today = Time::now();
        if ($paraments->accountant->type == '01') { //mes vencido
            $today->modify('+1 month');
        }
        $today->day($paraments->accountant->daydue);
        $debt_form->duedate = $today;

        if ($this->request->is('post')) {

            $request_data = json_decode($this->request->getData('data'), true);

            if (!$presale) {
                //verifica si se eligio un cliente
                if ($request_data['customer_code']  == '' ) {
                    $this->Flash->error(__('Debe selecionar un Cliente.'));
                    return $this->redirect(['action' => 'sellingProduct']);
                }
                $customer = $this->Debts->Customers->get($request_data['customer_code']);
            }

            $this->loadModel('ArticlesStores');

            //verifica si se eligio un producto
            if ($request_data['product_id'] == '') {
                $this->Flash->error(__('Debe selecionar un Producto.'));
                if ($presale) {
                    return $this->redirect(['controller' => 'Presales', 'action' => 'add']);
                }
                return $this->redirect(['action' => 'sellingProduct']);
            }

            //busco el articulo en los depositos
            $articleStores = $this->ArticlesStores->get($request_data['product_article_store_id']);

            //si la cantidad en deposito no es la suficiente
            if ($articleStores->current_amount < $request_data['amount']) {
                $this->Flash->error(__('La cantidad excede el article disponible.'));
                if ($presale) {
                    return $this->redirect(['controller' => 'Presales', 'action' => 'add']);
                }
                return $this->redirect(['action' => 'sellingProduct']);
            }

            //no guardo el descuento de la cantidad si es una venta recien se realizaria el descuento al confirmar la venta.
            if (!$presale) {
                //descuento la cantidad vendida
                $articleStores->current_amount -= $request_data['amount'];

                //actualizo el deposito
                if (!$this->ArticlesStores->save($articleStores)) {
                    $this->Flash->error(__('Error al actualizar el article.'));
                    if ($presale) {
                        return $this->redirect(['controller' => 'Presales', 'action' => 'add']);
                    }
                    return $this->redirect(['action' => 'sellingProduct']);
                }
            }

            $this->loadModel('Products');
            $product = $this->Products->get($request_data['product_id'] );
            
            if (!$product) {
                $this->Flash->error(__('No se encontro el Producto.'));
                if ($presale) {
                    return $this->redirect(['controller' => 'Presales', 'action' => 'add']);
                }
                return $this->redirect(['action' => 'sellingProduct']);
            }

            // if (!$presale) {
                $duedate = explode('/', $request_data['duedate']);
                $duedate = new Time($duedate[2] . '-' . $duedate[1] . '-' . $duedate[0]);
            // }

            if ($presale) {
                $product->amount = $request_data['amount'];
                $product->dues = $request_data['dues'];
                $product->product_total_due = $request_data['product_total_due'];
                $product->product_total = $request_data['product_total'];
                $product->product_article_store_id = $request_data['product_article_store_id'];
                $product->discount_id = $request_data['discount_id'];
                $product->discount_name = $request_data['discount_name'];
                $product->duedate = $duedate;
                $product->period =  Time::now()->format('m/Y');
                $product->connection_id = NULL;
                if (array_key_exists('connection_id', $request_data)) {
                    if ($request_data['connection_id'] != '') {
                        $product->connection_id = $request_data['connection_id'];
                        $connection = null;
                        foreach ($presale->customer->connections as $conn) {
                            if ($conn->id == $product->connection_id) {
                                $connection = $conn;
                                break;
                            }
                        }
                        if ($connection) {
                            $product->pay_with_connection = $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address;
                        }
                    }
                }
                array_push($presale->products, $product);
                $session->write('presale', $presale);
                $this->Flash->success( __('Se ha cargado con éxito el Producto'));

                if ($request_data['redirect'] == 'exit') {
                    return $this->redirect(["controller" => "presales", "action" => "add"]);
                }

                return $this->redirect(['controller' => 'Debts', 'action' => 'sellingProduct', true]);
            }

            //log de accion

            $action =  'Venta de Producto';
            $detail =  'Producto: ' . $product->name . PHP_EOL;
            $detail .= 'Cantidad: ' . $request_data['amount'] . PHP_EOL;
            $detail .= 'Cuotas: ' . $request_data['dues'] . PHP_EOL;
            $this->registerActivity($action, $detail, $customer->code);

            $ok = $this->CurrentAccount->addDebtProduct([
                'amount' => $request_data['amount'],
                'customer' => $customer,
                'product' => $product,
                'duedate' => $duedate,
                'dues' => $request_data['dues'],
                'discount_id' => $request_data['discount_id'],
                'redirect' => ['action' => 'sellingProduct'],
            ]);

            $session = $this->request->getSession();

            if (!$ok) {
                return $this->redirect(['action' => 'sellingProduct']);
            }

            $this->Flash->success(__('La venta se realizo con éxito'));

            return $this->redirect(['action' => 'sellingProduct']);
        }

        $dues_interest = $paraments->accountant->dues;

        $this->set(compact('debt_form', 'user_id', 'dues_interest', 'presale', 'discounts', 'discounts_list', 'connections'));
        $this->set('_serialize', ['debt_form']);
    }

    public function sellingProductEdit($id)
    {
        $paraments = $this->request->getSession()->read('paraments');

        $user_id = $this->Auth->user()['id'];
        $session = $this->request->getSession();
        $connections = [];
        $flag = false;
        if (array_key_exists("data", $this->request->getData())) {
            $data = json_decode($this->request->getData('data'), true);
            if ($data['from'] == 'presale') {
                $flag = true;
            }
        }

        $presale = $session->read('presale');

        //verificar si es customer nuevo o existente
        if (isset($presale->customer->code)) {
            if ($presale->customer->billing_for_service) {
                if (sizeof($presale->customer->connections) > 0) {
                    foreach ($presale->customer->connections as $connection) {

                        $connections[$connection->id] = $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address;
                    }
                }
            }
        }

        $this->loadModel('Discounts');
        $discounts = $this->Discounts->find()->where([
            'deleted' => false,
            'enabled' => true,
        ])->andWhere(function (QueryExpression $exp, Query $q) {
            return $exp->in('type', ['product', 'generic']);
        });

        $discounts_list = [];
        foreach ($discounts as $discount) {
            $discounts_list[$discount->id] = $discount->concept;
        }

        $this->loadModel('Stores');
        $this->loadModel('Accounts');

        if (!$this->Stores->find()->where(['user_id' => $user_id, 'enabled' => true, 'deleted' => false])->first()) {
            $this->Flash->warning(__('No tiene un depósito asigando.'));
            if ($presale) {
                return $this->redirect(['controller' => 'Presales', 'action' => 'add']);
            }
            return $this->redirect($this->referer());
        }

        $paraments = $this->request->getSession()->read('paraments');

        $debt_form = $this->Debts->newEntity();

        //para el vencimiento
        $today = Time::now();
        if ($paraments->accountant->type == '01') { //mes vencido
            $today->modify('+1 month');
        }
        $today->day($paraments->accountant->daydue);
        $debt_form->duedate = $today; 

        if ($this->request->is('post')) {

            $request_data = json_decode($this->request->getData('data'), true);

            $this->loadModel('ArticlesStores');

            //busco el articulo en los depositos
            $articleStores = $this->ArticlesStores->get($request_data['product_article_store_id']);

            //si la cantidad en deposito no es la suficiente
            if ($articleStores->current_amount < $request_data['amount']) {
                $this->Flash->error(__('La cantidad excede el article disponible.'));
                if ($presale) {
                    return $this->redirect(['controller' => 'Presales', 'action' => 'add']);
                }
                return $this->redirect(['action' => 'sellingProduct']);
            }

            $this->loadModel('Products');
            $product = $this->Products->get($request_data['product_id'] );

            if (!$product) {
                $this->Flash->error(__('No se encontro el Producto.'));
                if ($presale) {
                    return $this->redirect(['controller' => 'Presales', 'action' => 'add']);
                }
                return $this->redirect(['action' => 'sellingProduct']);
            }

            $duedate = explode('/', $request_data['duedate']);
            $duedate = new Time($duedate[2] . '-' . $duedate[1] . '-' . $duedate[0]);

            $product->amount = $request_data['amount'];
            $product->dues = $request_data['dues'];
            $product->product_total_due = $request_data['product_total_due'];
            $product->product_total = $request_data['product_total'];
            $product->product_article_store_id = $request_data['product_article_store_id'];
            $product->discount_id = $request_data['discount_id'];
            $product->discount_name = $request_data['discount_name'];
            $product->duedate = $duedate;
            $product->period =  Time::now()->format('m/Y');
            $product->connection_id = NULL;
            if (array_key_exists('connection_id', $request_data)) {
                if ($request_data['connection_id'] != '') {
                    $product->connection_id = $request_data['connection_id'];
                    $connection = null;
                    foreach ($presale->customer->connections as $conn) {
                        if ($conn->id == $product->connection_id) {
                            $connection = $conn;
                            break;
                        }
                    }
                    if ($connection) {

                        $product->pay_with_connection = $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address;
                    }
                }
            }
            unset($presale->products[$id]);
            array_push($presale->products, $product);
            $session->write('presale', $presale);
            $this->Flash->success( __('Se ha editado con éxito el Producto'));
            return $this->redirect(['controller' => 'Presales', 'action' => 'add']);
        }

        $dues_interest = $paraments->accountant->dues;

        $this->set(compact('debt_form', 'user_id', 'dues_interest', 'presale', 'discounts', 'discounts_list', 'connections', 'id'));
        $this->set('_serialize', ['debt_form']);
    }

    public function sellingPackage($from_presale = false)
    {
        $paraments = $this->request->getSession()->read('paraments');

        $debt_form = $this->Debts->newEntity();
        $session = $this->request->getSession();

        $this->loadModel('Discounts');
        $discounts = $this->Discounts->find()->where(function (QueryExpression $exp, Query $q) {
            return $exp->in('type', ['package', 'generic']);
        })->andWhere([
            'deleted' => false,
            'enabled' => true,
        ]);

        $discounts_list = [];
        foreach ($discounts as $discount) {
            $discounts_list[$discount->id] = $discount->concept;
        }

        $connections = [];

        $flag = false;
        if (array_key_exists("data", $this->request->getData())) {
            $data = json_decode($this->request->getData('data'), true);
            if ($data['from'] == 'presale') {
                $flag = true;
            }
        }
        $presale = ($session->check('presale') && ($flag
                || $from_presale));
        if ($presale) {
            $presale = $session->read('presale');

            //verificar si es customer nuevo o existente
            if (isset($presale->customer->code)) {
                if (isset($presale->customer) && sizeof($presale->customer->connections) > 0) {
                    foreach ($presale->customer->connections as $connection) {

                        $connections[$connection->id] = $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address;
                    }
                }
            }
        }

        //para el vencimiento
        $today = Time::now();
        if ($paraments->accountant->type == '01') { //mes vencido
            $today->modify('+1 month');
        }
        $today->day($paraments->accountant->daydue);
        $debt_form->duedate = $today;

        if ($this->request->is('post')) {

            $request_data = json_decode($this->request->getData('data'), true);

            if (!$presale) {
               if ($request_data['customer_code']  == '' ) {
                    $this->Flash->error(__('Debe selecionar un cliente.'));
                    return $this->redirect(['action' => 'sellingPackage']);
                }
                $customer = $this->Debts->Customers->get($request_data['customer_code']);
            }

            if ($request_data['package_id'] == '') {
                $this->Flash->error(__('Debe selecionar un paquete.'));
                if ($presale) {
                    return $this->redirect(['controller' => 'Presales', 'action' => 'add']);
                }
                return $this->redirect(['action' => 'sellingPackage']);
            }

            if (!$presale) {
                $duedate = explode('/', $request_data['duedate']);
                $duedate = new Time($duedate[2] . '-' . $duedate[1] . '-' . $duedate[0]);
            }

            $this->loadModel('Packages');
            $package = $this->Packages->get($request_data['package_id']);

            if (!$package) {
                $this->Flash->error(__('No se encontro el paquete.'));
                if ($presale) {
                    return $this->redirect(['controller' => 'Presales', 'action' => 'add']);
                }
                return $this->redirect(['action' => 'sellingPackage']);
            }

            $duedate = explode('/', $request_data['duedate']);
            $duedate = new Time($duedate[2] . '-' . $duedate[1] . '-' . $duedate[0]);

            if ($presale) {

                $package->package_total = $request_data['package_total'];
                $package->dues = $request_data['dues'];
                $package->package_total_due = $request_data['package_total_due'];
                $package->discount_id = $request_data['discount_id'];
                
                $package->duedate = $duedate;
                $package->period =  Time::now()->format('m/Y');
                
                $package->discount_name = $request_data['discount_name'];
                $package->discount_total = $request_data['discount_total'];
                $package->connection_id = NULL;
                if (array_key_exists('connection_id', $request_data)) {
                    if ($request_data['connection_id'] != '') {
                        $package->connection_id = $request_data['connection_id'];
                        $connection = null;
                        foreach ($presale->customer->connections as $conn) {
                            if ($conn->id == $package->connection_id) {
                                $connection = $conn;
                                break;
                            }
                        }
                        if ($connection) {

                            $package->pay_with_connection = $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address;
                        }
                    }
                }

                array_push($presale->packages, $package);

                $session->write('presale', $presale);

                $this->Flash->success( __('Se ha cargado con éxito el Paquete'));

                if ($request_data['redirect'] == 'exit') {
                    return $this->redirect(["controller" => "presales", "action" => "add"]);
                }

                return $this->redirect(['action' => 'sellingPackage', true]);
            }

            //log de accion

            $action =  'Venta de Paquete';
            $detail =  'Paquete: ' . $package->name . PHP_EOL;
            $detail .= 'Cuotas: ' . $request_data['dues'] . PHP_EOL;
            $this->registerActivity($action, $detail, $customer->code);

            $ok = $this->CurrentAccount->addDebtPackage([
                'amount' => 1,
                'customer' => $customer,
                'package' => $package,
                'duedate' => $duedate,
                'dues' => $request_data['dues'],
                'discount_id' => $request_data['discount_id'],
                'redirect' => ['action' => 'sellingPackage'],
            ]);

            if (!$ok) {
                return $this->redirect(['action' => 'sellingPackage']);
            }

            $this->Flash->success(__('El Paquete se agregó a la cuenta corriente del cliente'));

            return $this->redirect(['action' => 'sellingPackage']);
        }

        $dues_interest = $paraments->accountant->dues;

        $this->set(compact('debt_form', 'dues_interest', 'presale', 'discounts', 'discounts_list', 'paraments', 'connections'));
        $this->set('_serialize', ['debt_form']);
    }

    public function sellingPackageEdit($id)
    {
        $paraments = $this->request->getSession()->read('paraments');

        $debt_form = $this->Debts->newEntity();
        $session = $this->request->getSession();

        $this->loadModel('Discounts');
        $discounts = $this->Discounts->find()->where(function (QueryExpression $exp, Query $q) {
            return $exp->in('type', ['package', 'generic']);
        })->andWhere([
            'deleted' => false,
            'enabled' => true,
        ]);

        $discounts_list = [];
        foreach ($discounts as $discount) {
            $discounts_list[$discount->id] = $discount->concept;
        }

        $connections = [];

        $flag = false;
        if (array_key_exists("data", $this->request->getData())) {
            $data = json_decode($this->request->getData('data'), true);
            if ($data['from'] == 'presale') {
                $flag = true;
            }
        }

        $presale = $session->read('presale');

        //verificar si es customer nuevo o existente
        if (isset($presale->customer->code)) {
            if (isset($presale->customer) && sizeof($presale->customer->connections) > 0) {
                foreach ($presale->customer->connections as $connection) {

                    $connections[$connection->id] = $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address;
                }
            }
        }

        //para el vencimiento
        $today = Time::now();
        if ($paraments->accountant->type == '01') { //mes vencido
            $today->modify('+1 month');
        }
        $today->day($paraments->accountant->daydue);
        $debt_form->duedate = $today; 

        if ($this->request->is('post')) {

            $request_data = json_decode($this->request->getData('data'), true);

            $this->loadModel('Packages');
            $package = $this->Packages->get($request_data['package_id']);

            $package->package_total = $request_data['package_total'];
            $package->dues = $request_data['dues'];
            $package->package_total_due = $request_data['package_total_due'];
            $package->discount_id = $request_data['discount_id'];

            $duedate = explode('/', $request_data['duedate']);
            $duedate = new Time($duedate[2] . '-' . $duedate[1] . '-' . $duedate[0]);
            
            $package->duedate = $duedate;
            $package->period =  Time::now()->format('m/Y');

            $package->discount_name = $request_data['discount_name'];
            $package->discount_total = $request_data['discount_total'];
            $package->connection_id = NULL;
            if (array_key_exists('connection_id', $request_data)) {
                if ($request_data['connection_id'] != '') {
                    $package->connection_id = $request_data['connection_id'];
                    $connection = null;
                    foreach ($presale->customer->connections as $conn) {
                        if ($conn->id == $package->connection_id) {
                            $connection = $conn;
                            break;
                        }
                    }
                    if ($connection) {

                        $package->pay_with_connection = $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address;
                    }
                }
            }
            unset($presale->packages[$id]);
            array_push($presale->packages, $package);
            $session->write('presale', $presale);
            $this->Flash->success( __('Se ha editado con éxito el Paquete'));

            return $this->redirect(['controller' => 'Presales', 'action' => 'add']);
        }

        $dues_interest = $paraments->accountant->dues;

        $this->set(compact('debt_form', 'dues_interest', 'presale', 'discounts', 'discounts_list', 'paraments', 'connections', 'id'));
        $this->set('_serialize', ['debt_form']);
    }

    public function sellingService($from_presale = false)
    {
        $paraments = $this->request->getSession()->read('paraments');
        $banks = $this->request->getSession()->read('banks');

        $user_id = $this->Auth->user()['id'];
        $session = $this->request->getSession();

        $this->loadModel('Services');
        $services = $this->Services->find()->where(['deleted' => false]);

        $services_list = [];
        foreach ($services as $service) {
            $services_list[$service->id] = $service->name;
        }

        $this->loadModel('Discounts');
        $discounts = $this->Discounts->find()->where([
            'deleted' => false,
            'enabled' => true,
            'type' => 'service'
        ]);

        $discounts_list = [];
        foreach ($discounts as $discount) {
            $discounts_list[$discount->id] = $discount->concept;
        }

        $connections = [];

        $flag = false;
        if (array_key_exists("data", $this->request->getData())) {
            $data = json_decode($this->request->getData('data'), true);
            if ($data['from'] == 'presale') {
                $flag = true;
            }
        }
        $daydue = $paraments->accountant->daydue;
        $presale = ($session->check('presale') && ($flag
                || $from_presale));
        if ($presale) {
            $presale = $session->read('presale');
            $daydue = $presale->customer->daydue;
        }

        $range_hours_installation = $paraments->presale->range_hours_installation;

        $debt_form = $this->Debts->newEntity();

        if ($this->request->is('post')) {
            
            $this->request = $this->request->withParsedBody($data);
            
            if (!$presale) {
                //verifica si se eligio un cliente
                if ($this->request->getData('customer_code')  == '' ) {
                    $this->Flash->error(__('Debe selecionar un Cliente.'));
                    return $this->redirect(['action' => 'sellingService']);
                }
                $customer = $this->Debts->Customers->get($this->request->getData('customer_code'));
            }

            //verifica si se eligio un producto
            if ($this->request->getData('service_id') == '') {
                $this->Flash->error(__('Debe selecionar un Servicio.'));
                if ($presale) {
                    return $this->redirect(['controller' => 'Presales', 'action' => 'add']);
                }
                return $this->redirect(['action' => 'sellingService']);
            }

            $service = $this->Services->get($this->request->getData('service_id') );

            if (!$service) {
                $this->Flash->error(__('No se encontro el Servicio.'));
                if ($presale) {
                    return $this->redirect(['controller' => 'Presales', 'action' => 'add']);
                }
                return $this->redirect(['action' => 'sellingService']);
            }

            $installation_date = explode('/', $this->request->getData('installation_date'));
            $installation_date = new Time($installation_date[2] . '-' . $installation_date[1] . '-' . $installation_date[0]);

            if ($presale) {

                $service->service = $this->request->getData('service_id');
                $service->service_name = $this->request->getData('service_name');
                $service->service_total = $this->request->getData('service_total');
                $service->discount_id = $this->request->getData('discount_id');
                $service->discount_name = $this->request->getData('discount_name');
                $service->availability = $this->request->getData('range_hours_installation');
                $service->discount_total = $this->request->getData('discount_total');
                $service->lat = array_key_exists('lat', $this->request->getData()) ? $this->request->getData('lat') : null;
                $service->lng = array_key_exists('lng', $this->request->getData()) ? $this->request->getData('lng') : null;
                $service->address = $this->request->getData('address');
                $service->installation_date = $installation_date;
                $service->generate_ticket = $this->request->getData('generate_ticket');

                array_push($presale->services, $service);
                $session->write('presale', $presale);
                $this->Flash->success( __('Se ha cargado con éxito el Servicio'));

                if ($this->request->getData('redirect') == 'exit') {
                    return $this->redirect(["controller" => "presales", "action" => "add"]);
                }

                return $this->redirect(['controller' => 'Debts', 'action' => 'sellingService', true]);
            }
        }

        $this->set(compact('debt_form', 'user_id', 'presale', 'services', 'services_list', 'discounts', 'discounts_list', 'range_hours_installation', 'banks'));
        $this->set('_serialize', ['debt_form']);
    }

    public function sellingServiceEdit($id)
    {
        $paraments = $this->request->getSession()->read('paraments');
        $banks = $this->request->getSession()->read('banks');

        $user_id = $this->Auth->user()['id'];
        $session = $this->request->getSession();

        $this->loadModel('Services');
        $services = $this->Services->find()->where(['deleted' => false]);

        $services_list = [];
        foreach ($services as $service) {
            $services_list[$service->id] = $service->name;
        }

        $this->loadModel('Discounts');
        $discounts = $this->Discounts->find()->where([
            'deleted' => false,
            'enabled' => true,
            'type' => 'service'
        ]);

        $discounts_list = [];
        foreach ($discounts as $discount) {
            $discounts_list[$discount->id] = $discount->concept;
        }

        $connections = [];

        $flag = false;
        if (array_key_exists("data", $this->request->getData())) {
            $data = json_decode($this->request->getData('data'), true);
            if ($data['from'] == 'presale') {
                $flag = true;
            }
        }
        $daydue = $paraments->accountant->daydue;
        $presale = $session->read('presale');

        foreach ($presale->services as $key => $serv) {
            if ($key == $id) {
                $service = $serv;
            }
        }

        $daydue = $presale->customer->daydue;

        $range_hours_installation = $paraments->presale->range_hours_installation;

        $debt_form = $this->Debts->newEntity();

        if ($this->request->is('post')) {

            $this->request = $this->request->withParsedBody($data);

            $service = $this->Services->get($this->request->getData('service_id') );

            if (!$service) {
                $this->Flash->error(__('No se encontro el Servicio.'));
                return $this->redirect(['controller' => 'Presales', 'action' => 'add']);
            }

            $installation_date = explode('/', $this->request->getData('installation_date'));
            $installation_date = new Time($installation_date[2] . '-' . $installation_date[1] . '-' . $installation_date[0]);

            $service->service = $this->request->getData('service_id');
            $service->service_name = $this->request->getData('service_name');
            $service->service_total = $this->request->getData('service_total');
            $service->discount_id = $this->request->getData('discount_id');
            $service->discount_name = $this->request->getData('discount_name');
            $service->availability = $this->request->getData('range_hours_installation');
            $service->discount_total = $this->request->getData('discount_total');
            $service->lat = array_key_exists('lat', $this->request->getData()) ? $this->request->getData('lat') : null;
            $service->lng = array_key_exists('lng', $this->request->getData()) ? $this->request->getData('lng') : null;
            $service->address = $this->request->getData('address');
            $service->installation_date = $installation_date;
            $service->generate_ticket = $this->request->getData('generate_ticket');

            unset($presale->services[$id]);
            array_push($presale->services, $service);
            $session->write('presale', $presale);
            $this->Flash->success( __('Se ha editado con éxito el Servicio'));
            return $this->redirect(['controller' => 'Presales', 'action' => 'add']);
        }

        $this->set(compact('debt_form', 'user_id', 'presale', 'services', 'services_list', 'discounts', 'discounts_list', 'range_hours_installation', 'banks', 'service', 'id'));
        $this->set('_serialize', ['debt_form']);
    }

    public function editAjax()
    {
        if ($this->request->is('ajax')) {

            $data = $this->request->input('json_decode');

            $debt = $this->Debts->get($data->debt_id, ['contain' => ['Customers', 'Users']]);

            $duedate = $data->duedate;
            $duedate = explode('/',$duedate);
            $duedate = $duedate[2] . '-' . $duedate[1] . '-' . $duedate[0] . ' ' . Time::now()->format(' H:i:s');

            $debtData = [];

            $debt->duedate = new Time($duedate);
            $debt->tipo_comp = $data->tipo_comp;

            if ($this->Debts->save($debt)) {

                $customer = $this->loadModel('Customers');
                $customer = $this->Customers->get($debt->customer_code);
                $this->CurrentAccount->updateDebtMonth($debt->customer_code, $debt->connection_id);

            } else {
                $debt = false;
            }

            $this->set('debt', $debt);
        }
    }

    public function generateAll()
    {
        $paraments =  $this->request->getSession()->read('paraments');
        $afip_codes =  $this->request->getSession()->read('afip_codes');

        $debt_form = $this->Debts->newEntity();
        $debt_form->duedate = Time::now();

        if ($paraments->accountant->type == '01') { //mes vencido
          $debt_form->duedate->modify('+1 month');
        }

        $debt_form->duedate->day($paraments->accountant->daydue);
        $debt_form->initdate = Time::now();

        if ($this->Accountant->isEnabled()) {
            if (!$paraments->accountant->acounts_parent->services_free) {
            $this->Flash->warning(__('Configuración faltante. Debe especificar la cuenta para servicios bonificados'));
                 return $this->redirect(['action' => 'generateAll']);
            }
        }

        $debts_preview = [];

        $business = [];
        $business[''] = 'Seleccione Empresa';
        foreach ($paraments->invoicing->business as $b) {
            $business[$b->id] = $b->name . ' (' . $b->address. ')';
        }

        $areas = [];
        $this->loadModel('Areas');
        $areas_model = $this->Areas->find();
        foreach ($areas_model as $a) {
            $areas[$a->id] = $a->name;
        }

        $this->set(compact('debt_form', 'debts_preview', 'business', 'areas'));
    }

    /**
     * called from 
     *  debts/generateAll
    */
    public function debtPreview_old()
    {
        $paraments =  $this->request->getSession()->read('paraments');
        $afip_codes =  $this->request->getSession()->read('afip_codes');

        $data = $this->request->getQuery();

        $debts_preview = [];

        if (count($data) > 1) {

            $this->loadModel('Connections');
            $connections = $this->Connections->find()->contain([ 'Services', 'Customers', 'Debts']);

            foreach ($connections as $connection) {

                $flag = false;

                $date_temp = explode('/', $data['debt_month']);
                $date_temp = new Time($date_temp[1] . '-' . $date_temp[0] . '-1 00:00:00');
                $date_temp =  $date_temp->modify('+1 month');

                if ($connection->created > $date_temp) {
                    $flag = true;
                    continue;
                }
                
                if ($connection->debts) {

                    foreach ($connection->debts as $connection_debt) {

                        if ($connection_debt->period == $data['debt_month']) {
                            $flag = true;
                            break;
                        }
                    }
                }

                if (!$flag) { //no se econtro deuda de la conexion del mismo periodo

                    if ($connection->enabled) { 
                        $flag = true;
                    } else if ($data['connections_bloker'] && !$connection->enabled){
                        //conexion desabilitada pero se establecion que se toman en cuenta igual
                        $flag = true;
                    }

                    if ($flag) {

                        $debt = $this->Debts->newEntity();
                        $debt->customer = $connection->customer;

                        if ($data['customer_duedate_check']) {
                            $duedate_month = explode('/', $data['duedate_month']);
                            $debt->duedate = new Time($duedate_month[1] . '-' . $duedate_month[0] . '-' . $connection->customer->daydue);
                        } else {
                            $duedate = explode('/', $data['duedate']);
                            $debt->duedate = new Time($duedate[2] . '-' . $duedate[1] . '-' . $duedate[0]);
                        }

                        $debt->total = $connection->service->price;
                        
                        if ($data['custom_concept_check']) {
                            $debt->description = $data['concept'];
                        } else {
                            $debt->description = 'Abono ' . $data['debt_month'] . ' ' . $connection->service->name;
                        }

                        $debts_preview[] = $debt;
                    }
                }
            }
        }

        $this->set(compact('debts_preview'));
        $this->set('_serialize', ['debts_preview']);
    }

    private function getNameMonth($period)
    {
        $name_month['01'] = 'Enero ';
        $name_month['02'] = 'Febrero ';
        $name_month['03'] = 'Marzo ';
        $name_month['04'] = 'Abril ';
        $name_month['05'] = 'Mayo ';
        $name_month['06'] = 'Junio ';
        $name_month['07'] = 'Julio ';
        $name_month['08'] = 'Agosto ';
        $name_month['09'] = 'Septiembre ';
        $name_month['10'] = 'Octubre ';
        $name_month['11'] = 'Noviembre ';
        $name_month['12'] = 'Diciembre ';

        return $name_month[explode('/', $period)[0]] . explode('/', $period)[1];
    }

    public function debtPreview()
    {
       if ($this->request->is('ajax')) {

            $page = 1;

            $response = new \stdClass();

            $paraments = $this->request->getSession()->read('paraments');

            $afip_codes = $this->request->getSession()->read('afip_codes');

            $data = $this->request->getQuery('form_generate_data');

            $debts_preview = [];

            if (count($data) > 1) {

                //paginacion
                if ($this->request->getQuery()['length'] > 0) {

                    if ($this->request->getQuery()['start'] > 0) {
                        $page = $this->request->getQuery()['start'] / $this->request->getQuery()['length']; 
                        $page++;
                    }
                }
                $limit = $this->request->getQuery()['length']; 

                $this->loadModel('Invoices');

                $seach_value = [
                    'customer_code' => '',
                    'customer_idet' => '',
                    'customer_name' => '',
                    'f_impagas' => '',
                    'concept' => '',
                    'duedate' => '',
                    'total' => '',
                    'business' => '',
                    'tipo_comp' => '',
                    'area_id' => ''
                ];

                $order_value = [
                    'column' => intval($this->request->getQuery('order')[0]['column']),
                    'dir' => $this->request->getQuery('order')[0]['dir'],
                ];

                $seach_value['customer_code'] = $this->request->getQuery('columns')[1]['search']['value'];
                $seach_value['customer_idet'] = $this->request->getQuery('columns')[2]['search']['value'];
                $seach_value['customer_name'] = $this->request->getQuery('columns')[3]['search']['value'];
                $seach_value['f_impagas'] = $this->request->getQuery('columns')[5]['search']['value'];
                $seach_value['concept'] = $this->request->getQuery('columns')[6]['search']['value'];
                $seach_value['duedate'] = $this->request->getQuery('columns')[7]['search']['value'];
                $seach_value['total'] = $this->request->getQuery('columns')[8]['search']['value'];
                $seach_value['business_id'] = $this->request->getQuery('columns')[9]['search']['value'];
                $seach_value['tipo_comp'] = $this->request->getQuery('columns')[10]['search']['value'];
                $seach_value['connection_locker'] = $this->request->getQuery('columns')[11]['search']['value'];
                $seach_value['area_id'] = $this->request->getQuery('columns')[12]['search']['value'];

                if ($seach_value['duedate'] != '') {

                    $seach_value['duedate'] = explode('<>', $seach_value['duedate']);
                    $seach_value['duedate'][0] = explode('/', $seach_value['duedate'][0]);
                    $seach_value['duedate'][0] = new Time($seach_value['duedate'][0][2] . '-' . $seach_value['duedate'][0][1] . '-' . $seach_value['duedate'][0][0]);
                    $seach_value['duedate'][1] = explode('/', $seach_value['duedate'][1]);
                    $seach_value['duedate'][1] = new Time($seach_value['duedate'][1][2] . '-' . $seach_value['duedate'][1][1] . '-' . $seach_value['duedate'][1][0]);
                }

                if ($seach_value['f_impagas'] != '') {

                    $seach_value['f_impagas'] = explode('<>', $seach_value['f_impagas']); 

                    $seach_value['f_impagas'][0] = trim($seach_value['f_impagas'][0]);
                    $seach_value['f_impagas'][1] = trim($seach_value['f_impagas'][1]);

                    $seach_value['f_impagas'][0] = $seach_value['f_impagas'][0] != '' ? intval($seach_value['f_impagas'][0]) : '';
                    $seach_value['f_impagas'][1] = $seach_value['f_impagas'][1] != '' ? intval($seach_value['f_impagas'][1]) : '';

                    $seach_value['f_impagas']['from'] = $seach_value['f_impagas'][0];
                    $seach_value['f_impagas']['to'] = $seach_value['f_impagas'][1];
                }

                if ($seach_value['total'] != '') {

                    $seach_value['total'] = explode('<>', $seach_value['total']); 

                    $seach_value['total'][0] = trim($seach_value['total'][0]);
                    $seach_value['total'][1] = trim($seach_value['total'][1]);

                    $seach_value['total'][0] = $seach_value['total'][0] != '' ? intval($seach_value['total'][0]) : '';
                    $seach_value['total'][1] = $seach_value['total'][1] != '' ? intval($seach_value['total'][1]) : '';

                    $seach_value['total']['from'] = $seach_value['total'][0];
                    $seach_value['total']['to'] = $seach_value['total'][1];
                }

                $this->loadModel('Connections');
                $connections = $this->Connections->find()
                    ->contain([
                        'Areas',
                        'Services',
                        'Customers.Areas',
                        'ConnectionsDebtsMonth'
                    ])
                    ->where([
                        'Connections.deleted' => FALSE,
                        'Customers.deleted'   => FALSE
                    ]);

                $id = 1;

                foreach ($connections as $connection) {

                    $flag = FALSE;

                    $date_temp = explode('/', $data['debt_month']);
                    $date_temp = new Time($date_temp[1] . '-' . $date_temp[0] . '-1 00:00:00');
                    $date_temp =  $date_temp->modify('+1 month');

                    //si la fecha que se creo la conexion es mayor al dia 1 del mes siguiente. 

                    if ($connection->created > $date_temp) {
                        $flag = TRUE;
                        continue;
                    }

                    if (!$connection->enabled && !$data['generate_debt_blocking_connection']) {
                        continue;
                    }

                    foreach ($connection->connections_debts_month as $cdm) {
                        if ( $data['debt_month'] == $cdm->period) {
                            $flag = TRUE;
                            break;
                        }
                    }

                    if ($flag) {
                       continue;
                    }

                    $debt = $this->Debts->newEntity();
                    $debt->id = $id++;
                    $debt->customer = $connection->customer;

                    if ($data['customer_duedate_check']) {
                        $duedate_month = explode('/', $data['duedate_month']);
                        $debt->duedate = new Time($duedate_month[1] . '-' . $duedate_month[0] . '-' . $connection->customer->daydue);
                    } else {
                        $duedate = explode('/', $data['duedate']);
                        $debt->duedate = new Time($duedate[2] . '-' . $duedate[1] . '-' . $duedate[0]);
                    }

                    if ($paraments->gral_config->value_service_customer) {
                         $debt->total = $debt->customer->value_service;
                    } else {
                         $debt->total = $connection->service->price;
                    }

                    if ($data['custom_concept_check']) {
                        $debt->concept = $data['concept'];
                    } else {
                        $debt->concept = 'Abono ' . $connection->service->name . ' ' . $this->getNameMonth($data['debt_month']);
                    }

                    foreach ($paraments->invoicing->business as $business) {
                        if ($business->id == $connection->customer->business_billing ) {
                            $debt->business = $business;
                        }
                    }

                    if ($data['force_presupuesto']) {
                        $debt->tipo_comp = 'XXX';
                    } else {
                        if ($connection->customer->is_presupuesto) {
                            $debt->tipo_comp = 'XXX';
                        } else {
                            $debt->tipo_comp = $afip_codes['combinationsInvoices'][$debt->business->responsible][$connection->customer->responsible];
                        }
                    }

                    $debt->customers_has_discount = NULL;

                    if ($connection->discount_always) {

                        $debt->customers_has_discount = new \stdClass();
                        $debt->customers_has_discount->description = $connection->discount_description;

                        if ($paraments->gral_config->value_service_customer) {
                             $debt->customers_has_discount->total = $debt->customer->value_service * $connection->discount_value;
                        } else {
                             $debt->customers_has_discount->total = $connection->service->price * $connection->discount_value;
                        }

                    } else {

                        if ($connection->discount_month && $connection->discount_month > 0) {
                            $debt->customers_has_discount = new \stdClass();
                            $debt->customers_has_discount->description = $connection->discount_description;
                            $debt->customers_has_discount->description .= ' (Mes ' . ($connection->discount_total_month - $connection->discount_month + 1) . ' de ' . $connection->discount_total_month . ')';

                            if ($paraments->gral_config->value_service_customer) {
                                 $debt->customers_has_discount->total = $debt->customer->value_service * $connection->discount_value;
                            } else {
                                 $debt->customers_has_discount->total = $connection->service->price * $connection->discount_value;
                            }
                        }
                    }

                    $debt->connection_id = $connection->id;
                    $debt->connection_locker = !$connection->enabled;
                    $debt->force_debt = $connection->force_debt;
                    $debt->f_impagas = $this->Invoices->find()->where(['paid IS' => NULL, 'connection_id' => $connection->id])->count();

                    if ($paraments->gral_config->billing_for_service) {
                        $debt->area_id = $connection->customer->area_id;
                    } else {
                        $debt->area_id = $connection->area_id;
                    }

                    $debts_preview[] = $debt;
                }
            }

            $response->draw = intval($this->request->getQuery('draw'));

            $response->recordsTotal = count($debts_preview); 

            //filter

            $debts_preview_filter = [];

            $i = 0;

            foreach ($debts_preview as $debt) {

                if ($seach_value['customer_code'] != '' && $debt->customer->code != $seach_value['customer_code']) {
                    continue;
                }

                if ($seach_value['customer_idet'] != '' && $debt->customer->ident != $seach_value['customer_idet']) {
                    continue;
                }

                if ($seach_value['customer_name'] != '' && !strpos(strtoupper($debt->customer->name), strtoupper($seach_value['customer_name']))) {
                    continue;
                }

                if ($seach_value['concept'] != '' && !strpos(strtoupper($debt->concept), strtoupper($seach_value['concept']))) {
                    continue;
                }

                if ($seach_value['duedate'] != '') {

                    if ($debt->duedate < $seach_value['duedate'][0] || $debt->duedate > $seach_value['duedate'][1]) {
                        continue;
                    }
                }

                if ($seach_value['f_impagas'] != '') {

                    if (strval($seach_value['f_impagas']['from']) != '' && strval($seach_value['f_impagas']['to']) != '') {
                        if ($debt->f_impagas < $seach_value['f_impagas']['from'] || $debt->f_impagas > $seach_value['f_impagas']['to']) {
                            continue;
                        }
                    } else if (strval($seach_value['f_impagas']['from']) != '' && $seach_value['f_impagas']['to'] == '') {
                        if ($debt->f_impagas < $seach_value['f_impagas']['from']){
                            continue;
                        }
                    } else if ($seach_value['f_impagas']['from'] == '' && strval($seach_value['f_impagas']['to']) != '') {
                        if ($debt->f_impagas > $seach_value['f_impagas']['to']) {
                            continue;
                        }
                    }
                }

                if ($seach_value['total'] != '') {

                    if (strval($seach_value['total']['from']) != '' && strval($seach_value['total']['to']) != '') {
                        if ($debt->total < $seach_value['total']['from'] || $debt->total > $seach_value['total']['to']) {
                            continue;
                        }
                    } else if (strval($seach_value['total']['from']) != '' && $seach_value['total']['to'] == '') {
                        if ($debt->total < $seach_value['total']['from']) {
                            continue;
                        }
                    } else if ($seach_value['total']['from'] == '' && strval($seach_value['total']['to']) != '') {
                        if ($debt->total > $seach_value['total']['to']) {
                            continue;
                        }
                    }
                }

                if (strval($seach_value['connection_locker']) != '' && $debt->connection_locker != $seach_value['connection_locker']) {
                    continue;
                }

                if ($seach_value['business_id'] != '' && $debt->business->id != $seach_value['business_id']) {
                    continue;
                }

                if ($seach_value['tipo_comp'] != '' && $debt->tipo_comp != $seach_value['tipo_comp']) {
                    continue;
                }

                if ($seach_value['area_id'] != '' && $debt->area_id != $seach_value['area_id']) {
                    continue;
                }

                $debts_preview_filter[] = $debt;
            }

            //end filter

            $cmp_total_asc = function($a, $b)
            {
                return $a->total - $b->total;
            };

            $cmp_total_desc = function($a, $b)
            {
                return $b->total - $a->total;
            };

            $cmp_code_asc = function($a, $b)
            {
                return $a->customer->code - $b->customer->code;
            };

            $cmp_code_desc = function($a, $b)
            {
                return $b->customer->code - $a->customer->code;
            };

            $cmp_name_asc = function($a, $b)
            {
                return strcmp($a->customer->name, $b->customer->name);
            };

            $cmp_name_desc = function($a, $b)
            {
                return strcmp($b->customer->name, $a->customer->name);
            };

            $cmp_business_asc = function($a, $b)
            {
                return strcmp($a->business->name, $b->business->name);
            };

            $cmp_business_desc = function($a, $b)
            {
                return strcmp($b->business->name, $a->business->name);
            };

            $cmp_tipocomp_asc = function($a, $b)
            {
                return strcmp($a->tipo_comp, $b->tipo_comp);
            };

            $cmp_tipocomp_desc = function($a, $b)
            {
                return strcmp($b->tipo_comp, $a->tipo_comp);
            };

            $cmp_fimpagas_asc = function($a, $b)
            {
                return $a->f_impagas - $b->f_impagas;
            };

            $cmp_fimpagas_desc = function($a, $b)
            {
                return $b->f_impagas - $a->f_impagas;
            };

            $cmp_concept_asc = function($a, $b)
            {
                return strcmp($a->concept, $b->concept);
            };

            $cmp_concept_desc = function($a, $b)
            {
                return strcmp($b->concept, $a->concept);
            };

            $cmp_duedate_asc = function($a, $b)
            {
                if ($a->duedate < $b->duedate) {
                    return -1;
                } else if ($a->duedate > $b->duedate) {
                    return 1;
                } else {
                    return 0;
                }
            };

            $cmp_duedate_desc = function($a, $b)
            {
                if ($a->duedate < $b->duedate) {
                    return 1;
                } else if ($a->duedate > $b->duedate) {
                    return -1;
                } else {
                    return 0;
                }
            };

            $cmp_area_asc = function($a, $b)
            {
                if ($paraments->gral_config->billing_for_service) {
                    return strcmp($a->customer->area->name, $b->customer->area->name);
                } else {
                    return strcmp($a->customer->area->name, $b->customer->area->name);
                }
            };

            $cmp_area_desc = function($a, $b)
            {
                if ($paraments->gral_config->billing_for_service) {
                    return strcmp($b->connection->area->name, $a->connection->area->name);
                } else {
                    return strcmp($b->connection->area->name, $a->connection->area->name);
                }
            };

            switch ($order_value['column']) {

                case 12: //area_id

                    if ($order_value['dir'] == 'asc') {
                        usort($debts_preview_filter, $cmp_area_asc);
                    } else {
                        usort($debts_preview_filter, $cmp_area_desc);
                    }
                    break;

                case 11: //tipo_comp

                    if ($order_value['dir'] == 'asc') {
                        usort($debts_preview_filter, $cmp_tipocomp_asc);
                    } else {
                        usort($debts_preview_filter, $cmp_tipocomp_desc);
                    }
                    break;

                 case 10: //empresa

                    if ($order_value['dir'] == 'asc') {
                        usort($debts_preview_filter, $cmp_business_asc);
                    } else {
                        usort($debts_preview_filter, $cmp_business_desc);
                    }
                    break;

                case 9: //total

                    if ($order_value['dir'] == 'asc') {
                        usort($debts_preview_filter, $cmp_total_asc);
                    } else {
                        usort($debts_preview_filter, $cmp_total_desc);
                    }
                    break;

                case 8: //duedate

                    if ($order_value['dir'] == 'asc') {
                        usort($debts_preview_filter, $cmp_duedate_asc);
                    } else {
                        usort($debts_preview_filter, $cmp_duedate_desc);
                    }
                    break;

                case 7: //concepto

                    if ($order_value['dir'] == 'asc') {
                        usort($debts_preview_filter, $cmp_concept_asc);
                    } else {
                        usort($debts_preview_filter, $cmp_concept_desc);
                    }
                    break;

                case 6: //f_impagas

                    if ($order_value['dir'] == 'asc') {
                        usort($debts_preview_filter, $cmp_fimpagas_asc);
                    } else {
                        usort($debts_preview_filter, $cmp_fimpagas_desc);
                    }
                    break;

                case 4: //name

                    if ($order_value['dir'] == 'asc') {
                        usort($debts_preview_filter, $cmp_name_asc);
                    } else {
                        usort($debts_preview_filter, $cmp_name_desc);
                    }
                    break;

                default: //customer.code

                    if ($order_value['dir'] == 'asc') {
                        usort($debts_preview_filter, $cmp_code_asc);
                    } else {
                        usort($debts_preview_filter, $cmp_code_desc);
                    }
            }

            $response->recordsFiltered = count($debts_preview_filter);

            if (sizeof($debts_preview_filter) > 0 && $limit != -1) {
                $debts_preview_filter = array_chunk($debts_preview_filter, $limit);
                $debts_preview_filter = $debts_preview_filter[--$page];
            }

            $response->data = $debts_preview_filter;

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function generate()
    {
       if ($this->request->is('ajax')) {

            $response = NULL;

            $paraments = $this->request->getSession()->read('paraments');

            $afip_codes = $this->request->getSession()->read('afip_codes');

            $data = $this->request->input('json_decode')->ids;
            $data_confirm = $this->request->input('json_decode')->data_confirm;


            $debts_preview = [];

            if (count((array)$data) > 0) {

                $this->LoadModel('Invoices');

                $this->loadModel('Connections');
                $connections = $this->Connections->find()
                    ->contain([
                        'Areas',
                        'Services',
                        'Customers.Areas',
                        'ConnectionsDebtsMonth'
                    ])
                    ->where([
                        'Connections.deleted' => FALSE,
                        'Customers.deleted'   => FALSE
                    ]);

                $i = 1;

                $ids_selected = json_decode(json_encode($data->ids_selected), TRUE);

                $action = 'Generación Deudas Mensual';
                $detail = '';
                $this->registerActivity($action, $detail, NULL, TRUE);

                $action = 'Información de generación de deudas mensual';
                $detail ='Parámetros: ' . PHP_EOL;
                $detail .='Período: ' . $data_confirm->confirm_period . PHP_EOL;
                $detail .='Vencimiento individual: ' . $data_confirm->confirm_customer_duedate_check . PHP_EOL;
                $detail .='Vencimiento: ' . $data_confirm->confirm_duedate . PHP_EOL;
                $detail .='Concepto: ' . $data_confirm->confirm_concept . PHP_EOL;
                $detail .='Generar Deudas Conexiones Bloqueadas: ' . $data_confirm->confirm_generate_debt_blocking_connection . PHP_EOL;
                $detail .='Forzar Presupuesto: ' . $data_confirm->confirm_force_presupuesto . PHP_EOL;
                $detail .='Total deudas a generar: ' . $data_confirm->super_total_debts . PHP_EOL . PHP_EOL;
                $detail .='Se generará: ' . PHP_EOL;
                $build_total = (array) $data_confirm->build_total;

                foreach ($build_total as $key => $enterprise) {

                    $enterprise = (array) $enterprise;

                    if ($key != 'total') {
                        if (array_key_exists('name', $enterprise)) {
                            $detail .= 'Empresa: ' . $enterprise['name'] . ' (' . $enterprise['address'] . ')' . PHP_EOL;
                        }
                        if (array_key_exists('XXX', $enterprise)) {
                            $detail .='>> FX: $' . number_format((float) $enterprise['XXX'] , 2, ',', '') . PHP_EOL;
                        }
                        if (array_key_exists('001', $enterprise)) {
                            $detail .='>> FA: $' . number_format((float) $enterprise['001'] , 2, ',', '') . PHP_EOL;
                        }
                        if (array_key_exists('006', $enterprise)) {
                            $detail .='>> FB: $' . number_format((float) $enterprise['006'] , 2, ',', '') . PHP_EOL;
                        }
                        if (array_key_exists('011', $enterprise)) {
                            $detail .='>> FC: $' . number_format((float) $enterprise['011'] , 2, ',', '') . PHP_EOL;
                        }
                        if (array_key_exists('total', $enterprise)) {
                            $detail .='Total: $' . number_format((float) $enterprise['total'] , 2, ',', '') . PHP_EOL;
                        }
                        $detail .= PHP_EOL;
                    }
                }
                $detail .= '----------------------------------------------' . PHP_EOL;
                $detail .= 'Total Completo: $' . number_format((float) $build_total['total']->total , 2, ',', '') . PHP_EOL;
                $this->registerActivity($action, $detail, NULL);

                foreach ($connections as $connection) {

                    $flag = FALSE;

                    $time_now = Time::now();
                    $hms = $time_now->i18nFormat('HH:mm:ss');

                    $date_temp = explode('/', $data->debt_month);
                    $date_temp = new Time($date_temp[1] . '-' . $date_temp[0] . '-1 ' . $hms);
                    $date_temp = $date_temp->modify('+1 month');

                    if ($connection->created > $date_temp) {
                        $flag = TRUE;
                        continue;
                    }

                    foreach ($connection->connections_debts_month as $cdm) {

                        if ($data->debt_month == $cdm->period) {
                            $flag = TRUE;
                            break;
                        }
                    }

                    if ($flag) {
                        continue;
                    }

                    if (!$connection->enabled && !$data->generate_debt_blocking_connection) {
                        continue;
                    }

                    if (array_key_exists($i, $ids_selected)) {

                        $debt = $this->Debts->newEntity();
                        $debt->customer = $connection->customer;

                        if ($data->customer_duedate_check) {
                            $duedate_month = explode('/', $data->duedate_month);
                            $debt->duedate = new Time($duedate_month[1] . '-' . $duedate_month[0] . '-' . $connection->customer->daydue);
                        } else {
                            $duedate = explode('/', $data->duedate);
                            $debt->duedate = new Time($duedate[2] . '-' . $duedate[1] . '-' . $duedate[0]);
                        }

                        if ($paraments->gral_config->value_service_customer){ 
                             $debt->total = $debt->customer->value_service;
                        } else {
                             $debt->total = $connection->service->price;
                        }

                        if ($data->custom_concept_check) {
                            $debt->description = $data->concept;
                        } else {
                            $debt->description = 'Abono ' .  $connection->service->name . ' ' . $this->getNameMonth($data->debt_month);
                        }

                        foreach ($paraments->invoicing->business as $business) {
                            if ($business->id == $connection->customer->business_billing ) {
                                $debt->business = $business;
                            }
                        }

                        if ($data->force_presupuesto) {
                            $debt->tipo_comp = 'XXX';
                        } else {
                            if ($connection->customer->is_presupuesto) {
                                $debt->tipo_comp = 'XXX';
                            } else {
                                $debt->tipo_comp = $afip_codes['combinationsInvoices'][$debt->business->responsible][$connection->customer->responsible];
                            }
                        }

                        $debt->service = $connection->service;

                        $debt->service->price = $debt->total;

                        $debt->connection = $connection;
                        $debt->period = $data->debt_month;

                        $debt->connection_id = $connection->id;

                        $debt->force_debt = $connection->force_debt;

                        $debts_preview[] = $debt;
                    }

                    $i++;

                } //end foreach connections

                if (count($debts_preview) > 0) {

                    $response = true;

                    $debts_per_service = [];
                    $seating = NULL;

                    //si la contabilidada esta activada registro el asientop
                    if ($this->Accountant->isEnabled()) {
                        $seating = $this->Accountant->addSeating([
                            'concept' => 'Generación Deuda Mensual',
                            'comments' => '',
                            'seating' => NULL,
                            'account_debe' => NULL,
                            'account_haber' => NULL,
                            'redirect' => ['action' => 'generateAll']
                        ]);
                    }

                   foreach ($debts_preview as $debt_preview) {

                        if (!$debt_preview->force_debt) {
                            continue;
                        }

                        // $seating = NULL; ////?

                        $debt = $this->CurrentAccount->addDebtService([
                            'concept' => $debt_preview->description,
                            'seating' => $seating,
                            'customer' => $debt_preview->customer,
                            'service' => $debt_preview->service,
                            'calculate_proporcional' => FALSE,
                            'period' => $debt_preview->period,
                            'connection' => $debt_preview->connection,
                            'duedate' => $debt_preview->duedate,
                            'redirect' => ['action' => 'generateAll'],
                            'tipo_comp' => $debt_preview->tipo_comp
                        ], FALSE);

                        if (!$debt) {
                           $response = FALSE;
                           break;
                        }

                        //si la contabilidada esta activada registro el asientop
                        if ($this->Accountant->isEnabled()) {

                            $seating = $this->Accountant->addSeating([
                                'concept' => '', 
                                'comments' => '',
                                'seating' => $seating,
                                'account_debe' => $debt_preview->customer->account_code,
                                'account_haber' => NULL,
                                'value' => $debt->total,
                                'redirect' => ['action' => 'generateAll']
                            ]);

                            if (!$seating) {
                                $response = FALSE;
                                break;
                            }
                        }

                        if (!array_key_exists($debt_preview->service->id, $debts_per_service)) {
                            $debts_per_service[$debt_preview->service->id] = ['service' => $debt_preview->service, 'total' => 0]; 
                        }

                        $debts_per_service[$debt_preview->service->id]['total'] += $debt->total;

                        $cdm = $this->Connections->ConnectionsDebtsMonth->newEntity();

                        $cdm->created =  Time::now();
                        $cdm->business_billing = $debt_preview->customer->business_billing;
                        $cdm->period = $debt_preview->period;
                        $cdm->debt_id = $debt->id;

                        if ($debt_preview->tipo_comp == 'XXX') {
                             $cdm->totalx = $debt->total;
                        } else {
                             $cdm->total = $debt->total;
                        }
                        $cdm->connection_id = $debt_preview->connection->id;
                        $this->Connections->ConnectionsDebtsMonth->save($cdm);
                    }

                    $command = ROOT . "/bin/cake DebtMonthControl > /dev/null &";
                    $result = exec($command);

                    //si la contabilidada esta activada registro el asientop
                    if ($this->Accountant->isEnabled()) {

                        if (count($debts_per_service) > 0) {

                           foreach ($debts_per_service as $debt) {

                                $seating_total = $this->Accountant->addSeating([
                                    'concept' => '', 
                                    'comments' => '',
                                    'seating' => $seating,
                                    'account_debe' => NULL,
                                    'account_haber' => $debt['service']->account_code,
                                    'value' => $debt['total'],
                                    'redirect' => ['action' => 'generateAll']
                                ]);

                                if (!$seating_total) {
                                    $response = FALSE;
                                    break;
                                }
                           }
                        }
                    }
                }
            }

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function import()
    {
        $debt = $this->Debts->newEntity();

        if ($this->request->is('post')) {

            $this->loadModel('Paraments');
            $this->loadModel('Customers');
            $this->loadModel('Docs');
            $paraments = $this->Paraments->get(1);
            
            $types_tax = [
                'm' => ['c' => 'FC', 'm' => 'FC', 'r' => 'FC', 'e' => 'FC'],
                'r' => ['c' => 'FB', 'm' => 'FB', 'r' => 'FA', 'e' => 'FA']
            ];

            if ($_FILES['csv']) {

                $error = false;

                $handle = fopen($_FILES['csv']['tmp_name'], "r");

                $first = true;
                $counter = 0;

                while ($data = fgetcsv($handle, 999999, ";")) {

                    if ($first) {
                        $first = false;
                        continue;
                    }

                    $mov_data = [
                        'customer_code' => intval(trim($data[0])),
                        'type' => trim($data[1]),
                        'concept' => utf8_decode(trim($data[2])),
                        'value' => trim($data[3]),
                        'duedate' => trim($data[4]),
                        'other_account' => intval(trim($data[5])),
                        'comments' => utf8_decode(trim($data[6])),
                        'user_id' => utf8_decode(trim($data[7])),
                    ];

                    if ($mov_data['duedate']) {
                        $mov_data['duedate'] =  explode('/',$mov_data['duedate']);
                        $mov_data['duedate'] = $mov_data['duedate'][2] . '-' . $mov_data['duedate'][1] . '-' . $mov_data['duedate'][0];   
                    }

                    $customer = $this->Customers->find()->where(['code' => $mov_data['customer_code']])->first();

                    if (!$customer) {
                        $error = true;
                        Debug($data);
                        break;
                    }

                    if ($mov_data['type'] == 'd') {

                        $debt = $this->Debts->newEntity();

                        $debt->type_bill = 'FX';
                        if ($customer->type_bill == 'f') {
                            $debt->type_bill = $types_tax[$paraments->business_type_tax][$customer->type_tax];
                        }

                        $mov_data['duedate'] .=  Time::now()->format(' H:i:s');
                        $duedate = new Time($mov_data['duedate']);

                        $debt->created = Time::now();
                        $debt->duedate = $duedate;
                        $debt->amount = 1;
                        $debt->concept = $mov_data['concept'];
                        $debt->pricexunit = $mov_data['value'];
                        $debt->subtotal = $mov_data['value'];
                        $debt->user_id = $mov_data['user_id'];
                        $debt->customer_code = $customer->code;

                        if ($this->Accountant->isEnabled()) {
                            
                            $seating = $this->Accountant->addSeating([
                                'concept' => 'Venta de Servicio', 
                                'comments' => '',
                                'seating' => null,
                                'account_debe' => $customer->account_code,
                                'account_haber' => $mov_data['other_account'],
                                'value' => $mov_data['value'],
                                'redirect' => null,
                                'seating_type' => 'm'
                            ]);

                            $debt->seating_number = $seating->number;

                        }

                        if (!$this->Debts->save($debt)) {
                            $error = true;
                            Debug($data);
                            break;
                        }

                    } else {

                        $doc_cx = $this->Docs->newEntity();
                        
                        $doc_cx->created = Time::now();
                        $doc_cx->point_sale = $paraments->point_sale;
                        
                        $lastcount =  $this->Docs->find()->where(['type_comp' => 'CX'])->order(['number' => 'DESC'])->first();

                        if ($lastcount) {
                            $doc_cx->number = $lastcount->number + 1;
                        } else {
                            $doc_cx->number = 1;
                        }

                        $doc_cx->type_comp = 'CX'; // Nota de credito X
                        $doc_cx->concept = $mov_data['concept']; 
                        $doc_cx->date_start = $doc_cx->created;
                        $doc_cx->date_end = $doc_cx->created;
                        $doc_cx->total = $mov_data['value'];
                        $doc_cx->duedate = $doc_cx->created;
                        $doc_cx->user_id = $mov_data['user_id'];
                        $doc_cx->customer_code = $customer->code;

                        if ($this->Accountant->isEnabled()) {

                            $seating = $this->Accountant->addSeating([
                                'concept' => $mov_data['concept'],
                                'comments' => $mov_data['comments'],
                                'seating' => null,
                                'account_debe' => $mov_data['other_account'],
                                'account_haber' => $customer->account_code,
                                'value' => $mov_data['value'],
                                'redirect' => null,
                                'seating_type' => 'm'
                            ]);

                            $doc_cx->seating_number = $seating->number;
                        }

                        if (!$this->Docs->save($doc_cx)) {
                            $error = true;
                            Debug($data);
                            break;
                        }
                    }

                    $counter++;
                }

                if ($error) {
                    $this->Flash->error(__('Error al cargar'));
                } else {
                    $this->Flash->success(__('Se cargaron ' . $counter . ' Movimientos'));
                }

                fclose($handle);
            }
        }

        $this->set(compact('debt'));
        $this->set('_serialize', ['debt']);
    }

    public function delete()
    {
        if ($this->request->is('ajax')) {

            $data = $this->request->input('json_decode');

            $debt = $this->Debts->get($data->id);

            $this->loadModel('CustomersHasDiscounts');

            $customerHasDicount = $this->CustomersHasDiscounts->find()->where(['debt_id' => $debt->id])->first();

            $ok = false;

            if ($this->CurrentAccount->deleteDebt($debt)) {

                $this->loadModel('ConnectionsDebtsMonth');
                $connection_debt_month = $this->ConnectionsDebtsMonth
                    ->find()
                    ->where([
                        'debt_id' => $debt->id
                    ])->first();

                if ($connection_debt_month) {
                    if ($this->ConnectionsDebtsMonth->delete($connection_debt_month)) {
                        $ok = true;
                    }
                } else {
                    $ok = true;
                }

                if ($customerHasDicount) {
                    if ($this->CurrentAccount->deleteDiscount($customerHasDicount)) {
                        $ok = true;
                    }
                } else {
                    $ok = true;
                }
            }

            if ($ok) {
                if ($this->Accountant->isEnabled()) {
                  $this->Accountant->generateContrasiento($debt->seating_number);   
                }
            }

            //log
            $afip_codes = $this->request->getSession()->read('afip_codes');
            $customer_code = $debt->customer_code;
            $created = $debt->created->format('d/m/Y');
            $duedate = $debt->duedate->format('d/m/Y');
            $description = $debt->description;
            $tipo_comp = $afip_codes['comprobantes'][$debt->tipo_comp];
            $total = number_format($debt->total, 2, ',', '.');
            $detail = "";
            $detail .= 'Fecha: ' . $created . PHP_EOL;
            $detail .= 'Vencimiento: ' . $duedate . PHP_EOL;
            $detail .= 'Descripción: ' . $description . PHP_EOL;
            $detail .= 'Destino: ' . $tipo_comp . PHP_EOL;
            $detail .= 'Total: ' . $total . PHP_EOL;
            $action = 'Eliminación de Deuda';
            $this->registerActivity($action, $detail, $customer_code);

            $this->set("ok", $ok);
        }
    }

    public function deleteMasive()
    {
        if ($this->request->is('ajax')) {

            $data = $this->request->input('json_decode');

            $afip_codes = $this->request->getSession()->read('afip_codes');

            $action = 'Eliminación Masiva de Deudas';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            foreach ($data as $id) {

                $debt = $this->Debts->get($id);

                $this->loadModel('CustomersHasDiscounts');

                $customerHasDicount = $this->CustomersHasDiscounts->find()->where(['debt_id' => $id])->first();

                $ok = false;

                $customer_code = $debt->customer_code;
                $created = $debt->created->format('d/m/Y');
                $duedate = $debt->duedate->format('d/m/Y');
                $description = $debt->description;
                $tipo_comp = $afip_codes['comprobantes'][$debt->tipo_comp];
                $total = number_format($debt->total, 2, ',', '.');

                if ($this->CurrentAccount->deleteDebt($debt)) {

                    $detail = "";
                    $detail .= 'Fecha: ' . $created . PHP_EOL;
                    $detail .= 'Vencimiento: ' . $duedate . PHP_EOL;
                    $detail .= 'Descripción: ' . $description . PHP_EOL;
                    $detail .= 'Destino: ' . $tipo_comp . PHP_EOL;
                    $detail .= 'Total: ' . $total . PHP_EOL;
                    $action = 'Eliminación de Deuda';
                    $this->registerActivity($action, $detail, $customer_code);

                    $this->loadModel('ConnectionsDebtsMonth');
                    $connection_debt_month = $this->ConnectionsDebtsMonth
                        ->find()
                        ->where([
                            'debt_id' => $id
                        ])->first();

                    if ($connection_debt_month) {
                        if ($this->ConnectionsDebtsMonth->delete($connection_debt_month)) {
                            $ok = true;
                        }
                    } else {
                        $ok = true;
                    }

                    if ($customerHasDicount) {
                        if ($this->CurrentAccount->deleteDiscount($customerHasDicount)) {
                            $ok = true;
                        }
                    } else {
                        $ok = true;
                    }
                }

                if ($ok) {
                    if ($this->Accountant->isEnabled()) {
                        $this->Accountant->generateContrasiento($debt->seating_number);   
                    }
                }

                $this->set("ok", $ok);
            }
        }
    }

    public function debtMonthControl($verify_payment_commitment = FALSE)
    {
        $action = 'Control de deudas (Cron)';
        $detail = '';
        $this->registerActivity($action, $detail, NULL, TRUE);

        // Control de deudas en clientes

        $this->loadModel('Customers');
        $customers = $this->Customers
            ->find()
            ->contain([
                'Connections'
            ])->toArray();

        $customer_saldo_recalculado = 0;
        $connection_saldo_recalculado = 0;

        foreach ($customers as $customer) {

            $customer_saldo_recalculado++;
            $code = $customer->code;

            foreach ($customer->connections as $connection) {

                $connection_saldo_recalculado++;
                $this->CurrentAccount->updateDebtMonth($code, $connection->id, true);
            }

            $this->CurrentAccount->updateDebtMonth($code, null, true);

            if ($verify_payment_commitment) {

                if ($customer->payment_commitment != NULL) {

                    $controllerClass = 'App\Controller\PaymentCommitmentController';

                    $payemnt_commitment = new $controllerClass;
                    if ($payemnt_commitment->verifyCompliance($code)) {

                        $created = Time::now()->format('d/m/Y H:i');
                        $user_id = NULL;

                        if (!isset($this->request->getSession()->read('Auth.User')['id'])) {
                            $user_id = 100;
                        } else {
                            $user_id = $this->request->getSession()->read('Auth.User')['id'];
                        }

                        $action = 'Cumplío Compromiso de Pago';
                        $detail = 'Cliente: ' . $customer->name . PHP_EOL;

                        $detail .= '---------------------------' . PHP_EOL;

                        $this->registerActivity($action, $detail, $code, TRUE);

                        // Agregado del Compromiso de Pago en Observaciones
                        $this->loadModel('Observations');

                        $observation = $this->Observations->newEntity();
                        $observation->customer_code = $code;
                        $observation->comment = 'Cumplío Compromiso de Pago' . ' - Fecha de pago: ' . $created;
                        $observation->user_id = $user_id;

                        $this->Observations->save($observation);
                    }
                }
            }
        }

        // Control de deudas en conexiones

        /*$this->loadModel('Connections');
        $connections = $this->Connections
            ->find()
            ->where([
                'Connections.deleted' => FALSE
            ])
            ->toArray();

        $connection_saldo_recalculado = 0;

        foreach ($connections as $connection) {

            $connection_saldo_recalculado++;

            $customer = $this->CurrentAccount->updateDebtMonth($connection->customer_code, $connection->id, true);
        }*/

        $action = 'Resultado de proceso control de deudas (Cron)';
        $detail = 'Cantidad de clientes afectados: ' . $customer_saldo_recalculado . PHP_EOL;
        $detail .= 'Cantidad de conexiones afectadas: ' . $connection_saldo_recalculado . PHP_EOL;
        $this->registerActivity($action, $detail, NULL);
    }

    public function debtMonthControlAfterActionShell($ids, $verify_payment_commitment = FALSE)
    {
        $action = 'Control de deudas (Acción)';
        $detail = '';
        $this->registerActivity($action, $detail, NULL, TRUE);

        $customer_saldo_recalculado = 0;
        $connection_saldo_recalculado = 0;

        $ids = explode('.', $ids);

        if (sizeof($ids) > 0) {

            // Control de deudas en clientes

            $this->loadModel('Customers');
            $customers = $this->Customers
                ->find()
                ->contain([
                    'Connections'
                ])->where([
                    'Customers.code IN' => $ids
                ])->toArray();

            foreach ($customers as $customer) {

                $customer_saldo_recalculado++;
                $code = $customer->code;

                foreach ($customer->connections as $connection) {

                    $connection_saldo_recalculado++;
                    $this->CurrentAccount->updateDebtMonth($code, $connection->id, true);
                }

                $this->CurrentAccount->updateDebtMonth($code, null, true);

                if ($verify_payment_commitment) {

                    if ($customer->payment_commitment != NULL) {

                        $controllerClass = 'App\Controller\PaymentCommitmentController';

                        $payemnt_commitment = new $controllerClass;
                        if ($payemnt_commitment->verifyCompliance($code)) {

                            $created = Time::now()->format('d/m/Y H:i');
                            $user_id = NULL;

                            if (!isset($this->request->getSession()->read('Auth.User')['id'])) {
                                $user_id = 100;
                            } else {
                                $user_id = $this->request->getSession()->read('Auth.User')['id'];
                            }

                            $action = 'Cumplío Compromiso de Pago';
                            $detail = 'Cliente: ' . $customer->name . PHP_EOL;

                            $detail .= '---------------------------' . PHP_EOL;

                            $this->registerActivity($action, $detail, $code, TRUE);

                            // Agregado del Compromiso de Pago en Observaciones
                            $this->loadModel('Observations');

                            $observation = $this->Observations->newEntity();
                            $observation->customer_code = $code;
                            $observation->comment = 'Cumplío Compromiso de Pago' . ' - Fecha de pago: ' . $created;
                            $observation->user_id = $user_id;

                            $this->Observations->save($observation);
                        }
                    }
                }
            }
        }

        $action = 'Resultado de proceso control de deudas después de pagos (Acción)';
        $detail = 'Cantidad de clientes afectados: ' . $customer_saldo_recalculado . PHP_EOL;
        $detail .= 'Cantidad de conexiones afectadas: ' . $connection_saldo_recalculado . PHP_EOL;
        $this->registerActivity($action, $detail, NULL);
    }
}

class Row {

    public $id;
    public $created;
    public $duedate;
    public $code;
    public $description;
    public $quantity;
    public $unit;

    public $price;
    public $sum_price;
    public $tax;
    public $sum_tax;
    public $discount;
    public $total;
    public $tipo_comp;
    public $customer;
    public $user;
    public $seating_number;

    public $connection;
    public $customers_has_discount;

    public $saldo;

    public $type_move;

    public function __construct($data, $type) {

        switch ($type) {

            case 'debt':

                $this->id = $data->id;
                $this->created = $data->created;
                $this->duedate = $data->duedate;
                $this->code = $data->code;
                $this->description = $data->description;
                $this->quantity = $data->quantity;
                $this->unit = $data->unit;
                $this->price = $data->price;
                $this->sum_price = $data->sum_price;
                $this->tax = $data->tax;
                $this->sum_tax = $data->sum_tax;
                $this->discount = $data->discount;
                $this->total = $data->total; 
                $this->tipo_comp = $data->tipo_comp;
                $this->customer = $data->customer;
                $this->user = $data->user;
                $this->seating_number = $data->seating_number;
                $this->connection = $data->connection;
                $this->customers_has_discount = $data->customers_has_discount;

                $this->type_move = $type;
                break;

            case 'customer_has_discount':

                $this->id = $data->id;
                $this->created = $data->created;
                $this->duedate = '';
                $this->code = $data->code;
                $this->description = $data->description;
                $this->quantity = 1;
                $this->unit = 7;
                $this->price = -$data->price;
                $this->sum_price = -$data->sum_price;
                $this->tax = $data->tax;
                $this->sum_tax = -$data->sum_tax;
                $this->discount = 0;
                $this->total = -$data->total; 
                $this->tipo_comp = $data->tipo_comp;
                $this->customer = $data->customer;
                $this->user = $data->user;
                $this->seating_number = $data->seating_number;
                $this->connection = $data->connection;
                $this->customers_has_discount = '';

                $this->type_move = $type;
                break;
        }
    }
} 
