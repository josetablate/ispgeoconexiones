<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Event\Event;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\Filesystem\Folder;
use Cake\Event\EventManager;

use App\Controller\Component\ComprobantesComponent;
use App\Controller\Component\AccountantComponent;
use App\Controller\Component\PDFGeneratorComponent;

class PayuController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Accountant', [
             'className' => '\App\Controller\Component\Admin\Accountant'
        ]);

        $this->loadComponent('CurrentAccount', [
             'className' => '\App\Controller\Component\Admin\CurrentAccount'
        ]);

        $this->loadComponent('FiscoAfipComp', [
             'className' => '\App\Controller\Component\Admin\FiscoAfipComp'
        ]);

        $this->loadComponent('IntegrationRouter', [
             'className' => '\App\Controller\Component\Integrations\IntegrationRouter'
        ]);

        $this->initEventManager();
    }

    public function initEventManager()
    {
        EventManager::instance()->on(
            'PayuController.Error',
            function ($event, $msg, $data, $flash = false) {

                $this->loadModel('ErrorLog');
                $log = $this->ErrorLog->newEntity();
                $log->user_id = $this->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Error';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->ErrorLog->save($log);

                if ($flash) {
                    $this->Flash->error(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'PayuController.Warning',
            function ($event, $msg, $data, $flash = false) {

                $this->loadModel('ErrorLog');
                $log = $this->ErrorLog->newEntity();
                $log->user_id = $this->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Warning';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->ErrorLog->save($log);

                if ($flash) {
                    $this->Flash->warning(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'PayuController.Log',
            function ($event, $msg, $data) {
                
            }
        );

        EventManager::instance()->on(
            'PayuController.Notice',
            function ($event, $msg, $data) {

            }
        );
    }

    public function isAuthorized($user = null) 
    {
        if ($this->request->getParam('action') == 'retrieves') {
            return true;
        }

        if ($this->request->getParam('action') == 'getCards') {
            return true;
        }

        if ($this->request->getParam('action') == 'assignTransaction') {
            return true;
        }

        if ($this->request->getParam('action') == 'processTransactions') {
            return true;
        }

        if ($this->request->getParam('action') == 'generatePayment') {
            return true;
        }

        if ($this->request->getParam('action') == 'strToDeciaml') {
            return true;
        }

        if ($this->request->getParam('action') == 'enableConnection') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function index()
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');
        $account_enabled = $this->Accountant->isEnabled();

        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Configuración PayU';
            $detail = "";
            $this->registerActivity($action, $detail, NULL, TRUE);

            $payment_getway->config->payu->enabled = $this->request->getData('enabled');
            $payment_getway->config->payu->cash = $this->request->getData('cash');
            $payment_getway->config->payu->portal = $this->request->getData('portal');
            $payment_getway->config->payu->api_key = $this->request->getData('api_key');
            $payment_getway->config->payu->api_login = $this->request->getData('api_login');
            $payment_getway->config->payu->merchant_id = $this->request->getData('merchant_id');
            $payment_getway->config->payu->language = $this->request->getData('language');
            $payment_getway->config->payu->is_test = $this->request->getData('is_test');
            $payment_getway->config->payu->automatic = $this->request->getData('automatic');

            $action = 'Configuración Guardada' . PHP_EOL;
            $detail = 'Habilitar: ' . ($payment_getway->config->payu->enabled ? 'Si' : 'No') . PHP_EOL;
            $detail .= 'API KEY: ' . $payment_getway->config->payu->api_key . PHP_EOL;
            $detail .= 'API LOGIN: ' . $payment_getway->config->payu->api_login . PHP_EOL;
            $detail .= 'ID COMERCIO: ' . $payment_getway->config->payu->merchant_id . PHP_EOL;
            $detail .= 'LENGUAJE: ' . $payment_getway->config->payu->language . PHP_EOL;
            $detail .= 'TESTING: ' . $payment_getway->config->payu->is_test . PHP_EOL;

            if ($account_enabled) {
                $payment_getway->config->payu->account = $this->request->getData('account');
                $detail .= 'Cuenta contable: ' . $payment_getway->config->payu->account . PHP_EOL;
            }

            $this->registerActivity($action, $detail, NULL);

            $this->savePaymentGetwayParaments($payment_getway);
        }

        $this->loadModel('Accounts');
        $accountsTitles = $this->Accounts->find()->where(['title' => TRUE])->order(['code' => 'ASC']);
        $accounts = $this->Accounts
            ->find()
            ->order([
                'code' => 'ASC'
            ]);

        $this->set(compact('payment_getway', 'accountsTitles', 'accounts', 'account_enabled'));
        $this->set('_serialize', ['payment_getway', 'accounts']);
    }

    public function markUsedCard()
    {
        if ($this->request->is('post')) {

            $payment_getway = $this->request->getSession()->read('payment_getway');
            $paraments = $this->request->getSession()->read('paraments');
            $data = $this->request->getData();

            if ($payment_getway->config->payu->enabled) {

                // if ($payment_getway->config->payu->api_key != ""
                //     && $payment_getway->config->payu->api_login != ""
                //     && $payment_getway->config->payu->merchant_id != ""
                //     && $payment_getway->config->payu->language != ""
                //     && $payment_getway->config->payu->is_test != "") {

                $card_id = $data['payu_barcode'];
                $customer_code = $data['customer_code'];

                $this->loadModel('CustomersAccounts');
                $customers_accounts_payu_card = $this->CustomersAccounts
                    ->find()
                    ->where([
                        'customer_code'     => $customer_code,
                        'deleted'           => FALSE,
                        'payment_getway_id' => 7,
                    ]);

                $where = [
                    'customer_code' => $customer_code,
                    'deleted'       => FALSE,
                ];

                $auto_debit = FALSE;
                $customers_accounts_cobrodigital_auto_debit = [];

                foreach ($payment_getway->methods as $pg) {
                    $config = $pg->config;
                    if ($payment_getway->config->$config->enabled) {
                        if ($pg->auto_debit) {
                            $auto_debit = TRUE;

                            if (!array_key_exists('OR', $where)) {
                                $where['OR'] = [];
                            }

                            $where['OR'][] = [
                                'payment_getway_id' => $pg->id
                            ];
                        }
                    }
                }

                if ($auto_debit) {
                    $customers_accounts_auto_debit = $this->CustomersAccounts
                        ->find()
                        ->where($where)
                        ->toArray();
                }

                if ($customers_accounts_payu_card->count() > 0) {
                    $this->Flash->warning(__('Únicamente puede tener una Cuenta por Cliente. Este Cliente posee una Cuenta del mismo tipo activa, en caso de querer crear una nueva Cuenta deberá "deshabilitar" la actual Cuenta activa del Cliente'));
                    return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
                }

                if (sizeof($customers_accounts_auto_debit) > 0) {
                    $this->Flash->warning(__('El Cliente tiene una Cuenta de Débito Automático activa, esto inhabilita la creación de otra Cuenta, para poder crear otras Cuentas deberá "deshabilitar" dicha Cuenta de Débito Automático.'));
                    return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
                }

                $this->loadModel('PayuAccounts');

                $payu_account = $this->PayuAccounts
                    ->find()
                    ->where([
                        'customer_code'     => $customer_code,
                        'payment_getway_id' => 7,
                        'deleted'           => FALSE
                    ]);

                if ($payu_account->count() > 0) {
                    $this->Flash->warning(__('Únicamente puede tener una Tarjeta de Cobranza de PayU por Cliente.'));
                    return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
                }

                $card = $this->PayuAccounts->get($card_id);
                $card->used = TRUE;
                $card->customer_code = $customer_code;
                $card->asigned = Time::now();

                if ($this->PayuAccounts->save($card)) {

                    $barcode = $card->barcode;

                    $detail = "";
                    $detail .= 'Cód. Barra: ' . $barcode . PHP_EOL;
                    $action = 'Asignación de Tarjeta de Cobranza - PayU';
                    $this->registerActivity($action, $detail, $customer_code);

                    $this->Flash->success(__('Tarjeta asiganada correctamente.'));

                    $this->loadModel('CustomersAccounts');
                    $cutomer_account = $this->CustomersAccounts->newEntity();
                    $cutomer_account->customer_code = $customer_code;
                    $cutomer_account->account_id = $card->id;
                    $cutomer_account->payment_getway_id = 7;
                    $this->CustomersAccounts->save($cutomer_account);

                    if ($paraments->gral_config->billing_for_service) {

                        $this->loadModel('Customers');
                        $customer = $this->Customers->get($customer_code, [
                            'contain' => []
                        ]);

                        if (sizeof($customer->connections) > 0) {

                            $this->loadModel('Connections');

                            foreach ($customer->connections as $connection) {
                                $connection->payu_card = TRUE;
                                $this->Connections->save($connection);
                            }
                        }
                    }
                }

                // } else {
                //     $this->Flash->warning(__('Debe cargar las credenciales de Payu.'));
                // }
            } else {
                $this->Flash->warning(__('Debe habilitar PayU.'));
            }
        }

        return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
    }

    public function uploadCards()
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');

        if ($payment_getway->config->payu->enabled) {

            if ($this->request->is('post')) {

                $error = $this->isReloadPage();

                if (!$error) {

                    if ($_FILES['csv']) {

                        $handle = fopen($_FILES['csv']['tmp_name'], "r");
                        $this->loadModel('PayuAccounts');
                        $gut_count = 0;
                        $no_gut = 0;
                        $count = 0;
                        $repeat = 0;

                        while ($data = fgetcsv($handle, 999999, ";")) {
    
                            if ($count != 0) {

                                $card = $this->PayuAccounts->find()->where(['barcode' => $data[0]])->first();

                                if (empty($card)) {

                                    $card = $this->PayuAccounts->newEntity();
                                    $id_comercio = $payment_getway->config->payu->merchant_id;

                                    $card_data = [
                                        'barcode'           => trim($data[0]),
                                        'used'              => false,
                                        'deleted'           => false,
                                        'id_comercio'       => $id_comercio,
                                        'payment_getway_id' => 7
                                    ];

                                    $cardEntity = $this->PayuAccounts->patchEntity($card, $card_data);

                                    if ($this->PayuAccounts->save($cardEntity)) {
                                        $gut_count++;
                                    } else {
                                        $no_gut++;
                                        $file_path = LOGS . 'payu_error_import_card.log';
                                        $main_error = "Error al guardar.";
                                        $message = "Mensaje: " . $main_error . "\n";
                                        $message .= "columna 1: " . $data[0] . "\n";
                                        $message .= "objeto: " . json_encode($card->getErrors()) . "\n\n";

                                        file_put_contents($file_path, $message, FILE_APPEND | LOCK_EX);
                                    }
                                } else {
                                    $repeat++;
                                }
                            }
                            $count++;
                        }
                        $this->Flash->success(__('Cantidad de tarjetas cargadas correctamente: ' . $gut_count . '\n' . 'Cantidad de tarjetas no cargadas: ' . $no_gut . '\n' . 'Cantidad de tarjetas previamente cargadas: ' . $repeat));
                    }
                } else {

                    $this->Flash->warning(__('La misma acción se intento realizar varias veces.'));
                    return $this->redirect(['action' => 'uploadCards']);
                }
            }
        } else {
            $this->Flash->warning('Debe habilitar PayU');
            return $this->redirect(['controller' => 'Payu', 'action' => 'index']);
        }
        $this->set(compact('payment_getway'));
        $this->set('_serialize', ['payment_getway']);
    }

    public function cards()
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');

        if (!$payment_getway->config->payu->enabled) {
            $this->Flash->warning('Debe habilitar PayU');
            return $this->redirect(['controller' => 'PaymentMethods', 'action' => 'index']);
        }
    }

    public function retrieves()
    {
        if ($this->request->is('ajax')) { //Ajax Detection 

            $payment_getway = $this->request->getSession()->read('payment_getway');
            $this->loadModel('PayuAccounts');

            $payment_getway_id = $this->request->getQuery()['payment_getway_id'];
            $used = $this->request->getQuery()['used'];
            $deleted = $this->request->getQuery()['deleted'];
            $date_exported = $this->request->getQuery()['date_exported'] ? $this->request->getQuery()['date_exported'] : NULL;

            $where = [
                'PayuAccounts.deleted'           => $deleted,
                'PayuAccounts.id_comercio'       => $payment_getway->config->payu->merchant_id,
                'PayuAccounts.date_exported IS'  => $date_exported,
                'PayuAccounts.payment_getway_id' => $payment_getway_id,
                'PayuAccounts.used'              => $used
            ];

            $this->loadModel('PayuAccounts');
            $response = new \stdClass();

            $response->draw = intval($this->request->getQuery('draw'));

            $response->recordsTotal = $this->PayuAccounts->find()->where($where)->count();

            $response->data = $this->PayuAccounts->find('ServerSideData', [
                'params' => $this->request->getQuery(),
                'where'  => $where
            ]);

            $response->recordsFiltered = $this->PayuAccounts->find('RecordsFiltered', [
                'params' => $this->request->getQuery(),
                'where'  => $where
            ]);

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function usedCards()
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');

        if (!$payment_getway->config->payu->enabled) {

            $this->Flash->warning('Debe habilitar PayU');
            return $this->redirect(['controller' => 'PaymentMethods', 'action' => 'index']);
        }
    }

    public function deletedCards()
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');

        if (!$payment_getway->config->payu->enabled) {

            $this->Flash->warning('Debe habilitar PayU');
            return $this->redirect(['controller' => 'PaymentMethods', 'action' => 'index']);
        }
    }

    public function deleteCard($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $this->loadModel('PayuAccounts');
        $this->loadModel('CustomersAccounts');

        $customer_account = $this->CustomersAccounts
            ->find()
            ->where([
                'account_id' => $id
            ])->first();

        if ($customer_account) {
            $this->CustomersAccounts->delete($customer_account);
        }

        $card = $this->PayuAccounts->get($id);
        $customer_code = $card->customer_code;
        $barcode = $card->barcode;

        $card->used = FALSE;
        $card->customer_code = NULL;
        $card->date_exported = NULL;
        $card->deleted = TRUE;

        if ($this->PayuAccounts->save($card)) {
            $detail = "";
            $detail .= 'Cód. Barra: ' . $barcode . PHP_EOL;
            $action = 'Eliminación de Tarjeta de Cobranza - PayU';
            $this->registerActivity($action, $detail, $customer_code);
            $this->Flash->success(__('Se ha eliminado la Tarjeta correctamente.'));
        } else {
            $this->Flash->error(__('No se ha eliminado la Tarjeta. por favor, intente nuevamente.'));
        }

        return $this->redirect(['action' => 'cards']);
    }

    public function restoreCard($id = null)
    {
        $this->loadModel('PayuAccounts');

        $card = $this->PayuAccounts->get($id, [
            'contain' => []
        ]);

        $data = $this->request->getData();
        $data['deleted'] = FALSE;

        $card = $this->PayuAccounts->patchEntity($card, $data);
        $card->asigned = Time::now();

        if ($this->PayuAccounts->save($card)) {
            $barcode = $card->barcode;
            $customer_code = $card->barcode;
            $detail = "";
            $detail .= 'Cód. Barra: ' . $barcode . PHP_EOL;
            $action = 'Restauración de Tarjeta de Cobranza - PayU';
            $this->registerActivity($action, $detail, $customer_code);
            $this->Flash->success(__('Tarjeta Restaurada.'));
        } else {
            $this->Flash->error(__('Error al restaurar la Tarjeta.'));
        }

        return $this->redirect(['action' => 'deletedCards']);
    }

    public function freedCard($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $this->loadModel('CustomersAccounts');
        $paraments = $this->request->getSession()->read('paraments');
        $data = $this->request->getData();

        $customer_account = $this->CustomersAccounts
            ->find()
            ->where([
                'customer_code'     => $data['customer_code'],
                'account_id'        => $id,
                'payment_getway_id' => 7
            ])->first();

        if ($this->CustomersAccounts->delete($customer_account)) {

            $this->loadModel('PayuAccounts');

            $card = $this->PayuAccounts->get($id);
            $barcode = $card->barcode;
            $customer_code = $card->customer_code;
            $card->used = FALSE;
            $card->deleted = FALSE;
            $card->customer_code = NULL;
            $card->date_exported = NULL;

            if ($this->PayuAccounts->save($card)) {

                $detail = "";
                $detail .= 'Cód. Barra: ' . $barcode . PHP_EOL;
                $action = 'Liberación de Tarjeta de Cobranza - PayU';
                $this->registerActivity($action, $detail, $customer_code);

                if ($paraments->gral_config->billing_for_service) {

                    $this->loadModel('Customers');
                    $customer = $this->Customers->get($customer_code, [
                        'contain' => ['Connections']
                    ]);

                    if (sizeof($customer->connections) > 0) {

                        $this->loadModel('Connections');

                        foreach ($customer->connections as $connection) {
                            $connection->payu_card = FALSE;
                            $this->Connections->save($customer);
                        }
                    }
                }
                $this->Flash->success(__('Se ha liberado la Tarjeta correctamente.'));
            } else {
                $this->Flash->error(__('No se ha liberado la Tarjeta. por favor, intente nuevamente.'));
            }
        } else {
            $this->Flash->error(__('No se ha liberado la Tarjeta. por favor, intente nuevamente.'));
        }

        return $this->redirect(['action' => 'cards']);
    }

    public function getCards()
    {
        $data = [
            'status' => 400
        ];

        $msg = '';

        if ($this->request->is('ajax')) {

            $payment_getway = $this->request->getSession()->read('payment_getway');

            if ($payment_getway->config->payu->enabled) {

                $id_comercio = $payment_getway->config->payu->merchant_id;

                $cards = [];

                $this->loadModel('PayuAccounts');

                $cards = $this->PayuAccounts->find()->where([
                    'used'              => FALSE, 
                    'deleted'           => FALSE,
                    'id_comercio'       => $id_comercio,
                    'payment_getway_id' => 7
                ])->toArray();

                $data['cards'] = $cards;
                $data['status'] = 200;

            } else {
                $msg = 'Debe habilitar PayU';
            }
        }
        $data['message'] = $msg;

        $this->set(compact('data'));
        $this->set('_serialize', ['data']);
    }

    public function loadpayment()
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');

        if ($payment_getway->config->payu->enabled) {

            if ($this->request->is('post')) {

                $error = $this->isReloadPage();

                if (!$error) {

                    $action = 'Importación de Pagos por Excel de PayU';
                    $detail = '';
                    $this->registerActivity($action, $detail, NULL, TRUE);

                    if ($_FILES['csv']) {

                        $handle = fopen($_FILES['csv']['tmp_name'], "r");
                        $this->loadModel('PayuAccounts');
                        $this->loadModel('PayuTransactions');
                        $gut_count = 0;
                        $no_gut = 0;
                        $count = 0;
                        $repeat = 0;

                        $customers_codes = "";
                        $first = TRUE;

                        while ($data = fgetcsv($handle, 999999, ";")) {

                            if ($count != 0) {

                                $payu_transaction = $this->PayuTransactions
                                    ->find()
                                    ->where([
                                        'id_orden' => $data[1]]
                                    )->first();

                                $bloque_id = time();
                                $customer = NULL;

                                if (empty($payu_transaction)) {

                                    $numero_pago = trim($data[5]);
                                    $customer_code = NULL;
                                    $comments = "";
                                    $user_sistema = 100;
                                    $payuAccount = NULL;

                                    if (!empty($numero_pago)) {

                                        //traer cliente
                                        $payuAccount = $this->PayuAccounts
                                            ->find()
                                            ->contain([
                                                'Customers.Connections'
                                            ])
                                            ->where([
                                                'barcode'           => $numero_pago,
                                                'payment_getway_id' => 7
                                            ])->first();

                                        if ($payuAccount) {

                                            $customer = $payuAccount->customer;

                                            if ($customer == NULL) {

                                                $comments .= " Tarjeta sin asignar cliente.";
                                            }
                                        } else {
                                            $comments .= " No existe tarjeta.";
                                        }
                                        
                                    } else {
                                        $comments .= "No tiene cód. barra.";
                                    }

                                    $payu_transaction = $this->PayuTransactions->newEntity();

                                    //fecha pago
                                    $fecha_pago_format = trim($data[0]);
                                    $fecha_pago_format = explode(' ', $fecha_pago_format);

                                    if (sizeof($fecha_pago_format) == 1) {

                                        $date1 = $fecha_pago_format[0];
                                        $date1 = explode('/', $date1);

                                        $fecha_pago = $date1[2] . '-' . $date1[1] . '-' . $date1[0];
                                    } else {

                                        $date1 = $fecha_pago_format[0];
                                        $date1 = explode('/', $date1);

                                        $date2 = $fecha_pago_format[1];
                                        $date2 = explode(':', $date2);

                                        $hour = empty($date2[0]) ? '00' : $date2[0];
                                        $minute = empty($date2[1]) ? '00' : $date2[1];

                                        $fecha_pago = $date1[2] . '-' . $date1[1] . '-' . $date1[0] . ' ' . $hour . ':' . $minute . ':00';
                                    }

                                    //fecha recordatorio
                                    $fecha_recordatorio = trim($data[13]);

                                    if ($fecha_recordatorio == '') {

                                        $fecha_recordatorio = NULL;
                                    } else {

                                        $fecha_recordatorio_format = trim($data[0]);
                                        $fecha_recordatorio_format = explode(' ', $fecha_recordatorio_format);
                                        
                                        if (sizeof($fecha_recordatorio_format) == 1) {

                                            $date1_recordatorio = $fecha_recordatorio_format[0];
                                            $date1_recordatorio = explode('/', $date1_recordatorio);

                                            $fecha_recordatorio = $date1_recordatorio[2] . '-' . $date1_recordatorio[1] . '-' . $date1_recordatorio[0];
                                        } else {

                                            $date1_recordatorio = $fecha_recordatorio_format[0];
                                            $date1_recordatorio = explode('/', $date1_recordatorio);

                                            $date2_recordatorio = $fecha_recordatorio_format[1];
                                            $date2_recordatorio = explode(':', $date2_recordatorio);

                                            $hour_recordatorio = empty($date2_recordatorio[0]) ? '00' : $date2_recordatorio[0];
                                            $minute_recordatorio = empty($date2_recordatorio[1]) ? '00' : $date2_recordatorio[1];

                                            $fecha_recordatorio = $date1_recordatorio[2] . '-' . $date1_recordatorio[1] . '-' . $date1_recordatorio[0] . ' ' . $hour_recordatorio . ':' . $minute_recordatorio . ':00';
                                        }

                                    }

                                    //fecha expiracion
                                    $fecha_expiracion = trim($data[16]);

                                    if ($fecha_expiracion == '') {

                                        $fecha_expiracion = NULL;
                                    } else {

                                        $fecha_expiracion_format = trim($data[0]);
                                        $fecha_expiracion_format = explode(' ', $fecha_expiracion_format);
                                        
                                        if (sizeof($fecha_expiracion_format) == 1) {

                                            $date1_expiracion = $fecha_expiracion_format[0];
                                            $date1_expiracion = explode('/', $date1_expiracion);

                                            $fecha_expiracion = $date1_expiracion[2] . '-' . $date1_expiracion[1] . '-' . $date1_expiracion[0];
                                        } else {

                                            $date1_expiracion = $fecha_expiracion_format[0];
                                            $date1_expiracion = explode('/', $date1_expiracion);

                                            $date2_expiracion = $fecha_expiracion_format[1];
                                            $date2_expiracion = explode(':', $date2_expiracion);

                                            $hour_expiration = empty($date2_expiracion[0]) ? '00' : $date2_expiracion[0];
                                            $minute_expiration = empty($date2_expiracion[1]) ? '00' : $date2_expiracion[1];

                                            $fecha_expiracion = $date1_expiracion[2] . '-' . $date1_expiracion[1] . '-' . $date1_expiracion[0] . ' ' . $hour_expiration . ':' . $minute_expiration . ':00';
                                        }
                                    }

                                    $multiple_pago = trim($data[11]) == "true" ? TRUE : FALSE;

                                    $payu_transaction_data = [
                                        'fecha_pago'              => new Time($fecha_pago),
                                        'id_orden'                => trim($data[1]),
                                        'valor'                   => $this->strToDeciaml(trim($data[2])),
                                        'pais'                    => trim($data[3]),
                                        'cuenta'                  => trim($data[4]),
                                        'numero_pago'             => trim($data[5]),
                                        'id_cupon'                => trim($data[6]),
                                        'referencia_venta'        => trim($data[7]),
                                        'estado'                  => 0,
                                        'medio_pago'              => utf8_decode(trim($data[9])),
                                        'tipo_creacion'           => utf8_decode(trim($data[10])),
                                        'multiple_pagos'          => $multiple_pago,
                                        'frecuencia_recordatorio' => trim($data[12]),
                                        'fecha_recordatorio'      => new Time($fecha_recordatorio),
                                        'nombre_cliente'          => utf8_decode(trim($data[14])),
                                        'concepto'                => utf8_decode(trim($data[15])),
                                        'fecha_expiracion'        => new Time($fecha_expiracion),
                                        'email'                   => trim($data[17]),
                                        'created'                 => Time::now(),
                                        'comments'                => $comments,
                                        'bloque_id'               => $bloque_id,
                                        'customer_code'           => $customer == NULL ? $customer : $customer->code,
                                        'receipt_number'          => NULL,
                                        'payment_getway_id'       => 7
                                    ];

                                    $payuTransactionEntity = $this->PayuTransactions->patchEntity($payu_transaction, $payu_transaction_data);

                                    if ($this->PayuTransactions->save($payuTransactionEntity)) {
                                        if ($payuAccount != NULL && $customer != NULL) {
                                            if ($this->processTransactions($payuAccount, $payu_transaction, 0, $user_sistema)) {
                                                $gut_count++;
                                                if ($first) {
                                                    $customers_codes .= $customer->code;
                                                    $first = FALSE;
                                                } else {
                                                    $customers_codes .= '.' . $customer->code;
                                                }
                                            }
                                        } else {
                                            $no_gut++;
                                        }
                                    } else {
                                        $no_gut++;
                                        $file_path = LOGS . 'payu_error_import_card.log';
                                        $main_error = "Error al guardar.";
                                        $message = "Mensaje: " . $main_error . "\n";
                                        $message .= "columna 1: " . $data[0] . "\n";
                                        $message .= "columna 2: " . $data[1] . "\n";
                                        $message .= "objeto: " . json_encode($payuTransactionEntity->getErrors()) . "\n\n";

                                        file_put_contents($file_path, $message, FILE_APPEND | LOCK_EX);
                                    }
                                } else {
                                    $repeat++;
                                }
                            }
                            $count++;
                        }
                        $this->Flash->success(__('Cantidad de pagos cargadas correctamente: ' . $gut_count . '\n' . 'Cantidad de pagos no cargadas: ' . $no_gut . '\n' . 'Cantidad de pagos previamente cargadas: ' . $repeat));
                        if ($customers_codes != "") {
                            $command = ROOT . "/bin/cake debtMonthControlAfterAction $customers_codes 1 > /dev/null &";
                            $result = exec($command);
                        }
                    }
                } else {

                    $this->Flash->warning(__('La misma acción se intento realizar varias veces.'));
                    return $this->redirect(['action' => 'loadpayment']);
                }
            }
        } else {
            $this->Flash->warning('Debe habilitar PayU');
            return $this->redirect(['controller' => 'Payu', 'action' => 'index']);
        }

        $this->set(compact('payment_getway'));
        $this->set('_serialize', ['payment_getway']);
    }

    private function processTransactions($payu_account, $payu_transaction, $register = 0, $user_sistema = 100)
    {
        $this->loadModel('PayuTransactions');
        $this->loadModel('Connections');

        $customer = $payu_account->customer;

        //cliente con facturacion separa por servicio
        if ($customer->billing_for_service) {

            $where = [
                'customer_code'       => $customer->code,
                'Connections.deleted' => FALSE
            ];

            if ($payu_transaction->payment_getway_id == 7) {
                $where['payu_card'] = TRUE;
            }

            $connections = $this->Connections
                ->find()
                ->Contain([
                    'Customers'
                ])
                ->where($where)
                ->order([
                    'Connections.debt_month' => 'DESC'
                ]);

            $bruto = $payu_transaction->valor;

            if ($bruto == 0) {
                return 0;
            }

            $connections_size = $connections->count();
            if ($connections_size == 0) {
                $payu_transaction->customer_code = $customer->code;
                $register += $this->generatePayment($payu_transaction, $bruto, $register, $payu_account, NULL);

                return $register;
            }

            if ($connections_size == 1) {

                $connection = $connections->first();

                $payu_transaction->customer_code = $customer->code;
                $register += $this->generatePayment($payu_transaction, $bruto, $register, $payu_account, $connection->id);

                return $register;
            }

            $connections_debts = [];
            $connections_without_debts = [];

            // primer recorrido busco los que tienen deudas y los que no tienen deudas
            foreach ($connections as $key => $connection) {

                $payu_transaction->customer_code = $connection->customer_code;
                $this->PayuTransactions->save($payu_transaction);

                if ($connection->debt_month > 0) {
                    array_push($connections_debts, $connection);
                } else {
                    array_push($connections_without_debts, $connection);
                }
            }

            //array donde cargo los pagos
            $generate_payments = [];
            //bruto que voy descontando las deudas de las conexiones
            $bruto_aux = $bruto;

            // cargo en un array de pagos las conexiones que pueden pagarse
            // a partir de la conexiones con deudas
            if (sizeof($connections_debts) > 0) {

                foreach ($connections_debts as $connections_debt) {

                    // la deuda de la conexiones es menor al bruto
                    if ($connections_debt->debt_month <= $bruto_aux) {
                        $bruto_aux -= $connections_debt->debt_month;

                        $connections_debt->imports = [];
                        array_push($connections_debt->imports, $connections_debt->debt_month);
                        array_push($generate_payments, $connections_debt);
                    } else {
                        // se genera un pago a la conexion con el valor de bruto 
                        $connections_debt->imports = [];
                        array_push($connections_debt->imports, $bruto_aux);
                        $bruto_aux = 0;
                        array_push($generate_payments, $connections_debt);
                        break;
                    }
                }
            }

            // si quedo valor en bruto_aux, puede ser que no exista conexiones con deudas
            if ($bruto_aux > 0) {
                // debe existir conexiones sin deudas
                if (sizeof($connections_without_debts) > 0) {
                    // se asigna el pago a la primer conexion
                    // $connection_without_debt = $connections_without_debts[0];

                    $connection_without_debt_clone = new \sdtClass;
                    $connection_without_debt_clone->id = $connections_without_debts[0]->id;  

                    $connection_without_debt_clone->imports = [];
                    array_push($connection_without_debt_clone->imports, $bruto_aux);
                    $bruto_aux = 0;

                    array_push($generate_payments, $connection_without_debt_clone);

                } else {
                    // se asigna el pago a la primer conexion (con deuda)
                    // $connections_debt = $connections_debts[0];

                    $connections_debt_clone = new \sdtClass;
                    $connections_debt_clone->id = $connections_debts[0]->id;  

                    $connections_debt_clone->imports = [];
                    array_push($connections_debt_clone->imports, $bruto_aux);
                    $bruto_aux = 0;

                    array_push($generate_payments, $connections_debt_clone);
                }
            }

            // genero los pagos de las conexiones que tienen deudas
            foreach ($generate_payments as $key => $generate_payment) {

                foreach ($generate_payment->imports as $key => $import) {

                    $register += $this->generatePayment($payu_transaction, $import, $register, $payu_account, $generate_payment->id);
                }
            }

        } else {

            $bruto = $payu_transaction->valor;

            if ($bruto == 0) {
                return $register;
            }

            $register = $this->generatePayment($payu_transaction, $bruto, $register, $payu_account);
        }

        return $register;
    }

    private function generatePayment($payu_transaction, $import, $register, $payu_account, $connection_id = NULL, $user_sistema = 100)
    {
        $this->loadModel('Payments');
        $this->loadModel('PayuTransactions');
        $payment_getway = $this->request->getSession()->read('payment_getway');
        $paraments = $this->request->getSession()->read('paraments');
        $seating = NULL;

        $customer = $payu_account->customer;

        //si la contabilidada esta activada registro el asiento
        if ($this->Accountant->isEnabled()) {

            $seating = $this->Accountant->addSeating([
                'concept'       => 'Por Cobranza (PayU)',
                'comments'      => '',
                'seating'       => NULL, 
                'account_debe'  => $payment_getway->config->payu->account,
                'account_haber' => $customer->account_code,
                'value'         => $import,
                'user_id'       => $user_sistema,
                'created'       => Time::now()
            ]);
        }

        $data_payment = [
            'import'                => $import,
            'created'               => $payu_transaction->fecha_pago,
            'cash_entity_id'        => NULL,
            'user_id'               => $user_sistema,
            'seating_number'        => $seating ? $seating->number : NULL,
            'customer_code'         => $customer->code,
            'payment_method_id'     => $payu_account->payment_getway_id, //id = 7 metodo de pago -> tarjeta de cobranza payu
            'concept'               => 'Pago por PayU',
            'connection_id'         => $connection_id,
            'number_transaction'    => $payu_transaction->id_orden,
            'account_id'            => $payu_account->id
        ];

        $payment = $this->Payments->newEntity();

        $payment = $this->Payments->patchEntity($payment, $data_payment);

        if ($payment) {

            if ($this->Payments->save($payment)) {

                //genero el recibo del pago
                $receipt = $this->FiscoAfipComp->receipt($payment, FALSE);

                if ($receipt) {

                    $register++;

                    $payu_transaction->estado = 1;
                    $payu_transaction->comments = 'procesado correctamente ';

                    if ($payu_transaction->receipt_number != NULL) {
                        $payu_transaction->receipt_number .= ',' . str_pad($receipt->pto_vta, 4, "0", STR_PAD_LEFT) . '-' . str_pad($receipt->num, 8, "0", STR_PAD_LEFT);
                        $payu_transaction->receipt_id .= ',' . $receipt->id;
                    } else {
                        $payu_transaction->receipt_number = str_pad($receipt->pto_vta, 4, "0", STR_PAD_LEFT) . '-' . str_pad($receipt->num, 8, "0", STR_PAD_LEFT);
                        $payu_transaction->receipt_id = $receipt->id;
                    }

                    $this->PayuTransactions->save($payu_transaction);

                    //habilito la conexion relacionada al serivicio que esta pagando
                    $this->enableConnection($payment, $customer);

                    $afip_codes = $this->request->getSession()->read('afip_codes');

                    $this->loadModel('Users');
                    $user = $this->Users->get($user_sistema);
                    $user_name = $user->name . ' - username: ' . $user->username;

                    $customer_name = $customer->name . ' - Código: ' . $customer->code . ' - ' . $afip_codes['doc_types'][$customer->doc_type] . ': ' . $customer->ident;
                    $customer_code = $customer->code;

                    if ($paraments->invoicing->email_emplate_receipt != "") {

                        $controllerClass = 'App\Controller\MassEmailsController';

                        $mass_email = new $controllerClass;
                        if ($mass_email->sendEmailReceipt($receipt->id, $paraments->invoicing->email_emplate_receipt)) {

                            $action = 'Envío recibo al cliente';
                            $detail = 'Cliente: ' . $customer_name . PHP_EOL;
                            $detail .= 'Usuario: ' . $user_name . PHP_EOL;

                            $detail .= '---------------------------' . PHP_EOL;

                            $this->registerActivity($action, $detail, $customer_code);
                        } else {

                            $action = 'No envío recibo al cliente';
                            $detail = 'Cliente: ' . $customer_name . PHP_EOL;
                            $detail .= 'Usuario: ' . $user_name . PHP_EOL;

                            $detail .= '---------------------------' . PHP_EOL;

                            $this->registerActivity($action, $detail, $customer_code);
                        }
                    }

                } else {

                    $data_error = new \stdClass; 
                    $data_error->request_data = $data_payment;

                    $event = new Event('PayuController.Error', $this, [
                        'msg' => __('Hubo un error al intentar generar el recibo. Verifique los valores cargados.'),
                        'data' => $data_error,
                        'flash' => true
                    ]);

                    $this->getEventManager()->dispatch($event);

                    Log::error($receipt->getErrors(), ['scope' => ['PayuController']]);

                    //rollback payment
                    $this->Payments->delete($payment);
                    //recibo
                    $this->FiscoAfipComp->rollbackReceipt($receipt, TRUE);
                    //rollback seating
                    //si la contabilidada esta activada registro el asientop
                    if ($this->Accountant->isEnabled()) {
                        if ($seating) {
                            $this->Accountant->deleteSeating();
                        }
                    }
                }
            } else {
                Log::error($payment->getErrors(), ['scope' => ['PayuController']]);
            }

        } else {
            Log::error($payment->getErrors(), ['scope' => ['PayuController']]);
        }

        return $register;
    }

    private function strToDeciaml($value)
    {
        $value = str_replace(" ARS", "", $value);
        $value = str_replace(".00", "", $value);
        $value = str_replace('.', '', $value);
        $value = str_replace(',', '.', $value);
        return floatval($value);
    }

    public function assignTransaction()
    {
        //ajax Detection
        if ($this->request->is('ajax')) {

            $transaction_id = $this->request->input('json_decode')->transaction_id;
            $customer_code = $this->request->input('json_decode')->customer_code;

            $response = [
                'type'    => "error",
                'message' => "No se ha asignado el pago al cliente.",
                'error'   => true
            ];

            $this->loadModel('Customers');
            $customer = $this->Customers
                ->find()
                ->where([
                    'code' => $customer_code
                ])->first();

            if ($customer) {

                $this->loadModel('PayuTransactions');
                $payu_transaction = $this->PayuTransactions
                    ->find()
                    ->where([
                        'id' => $transaction_id
                    ])->first();

                $this->loadModel('PayuAccounts');
                $payu_account = $this->PayuAccounts
                    ->find()
                    ->contain([
                        'Customers'
                    ])
                    ->where([
                        'customer_code' => $customer_code
                    ])->first();

                if ($payu_transaction) {

                    if ($this->processTransactions($payu_account, $payu_transaction)) {

                        $payment_getway_name = "PayU";
                        $data = "";
                        switch ($payu_account->payment_getway_id) {
                            case 7:
                                $payment_getway_name = "Tarjeta de Cobranza";
                                $data = "Cód. Barra: " . $payu_account->barcode;
                                break;
                        }

                        $detail = "";
                        $detail .= 'Medio: '. $payment_getway_name . PHP_EOL;
                        $detail .= $data . PHP_EOL; 
                        $action = 'Asignación de Pago Manual - PayU';
                        $this->registerActivity($action, $detail, $customer_code);

                        $response = [
                            'type'    => "success",
                            'message' => "Pago asignado correctamente.",
                            'error'   => false
                        ];

                        $command = ROOT . "/bin/cake debtMonthControlAfterAction $customer_code 1 > /dev/null &";
                        $result = exec($command);
                    }
                }
            }

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    private function enableConnection($payment, $customer)
    {
        $paraments = $this->request->getSession()->read('paraments');

        $this->loadModel('Connections');

        $connections = [];

        if ($customer->billing_for_service) {

             if ($payment->connection_id) {

                 $connections = $this->Connections->find()->contain(['Customers', 'Controllers', 'Services'])
                    ->where([
                        'Connections.id' => $payment->connection_id,
                        'Connections.deleted' => false,
                        'Connections.enabled' => false
                    ]);
             }

        } else {

            $connections = $this->Connections->find()->contain(['Customers', 'Controllers', 'Services'])
                ->where([
                    'Connections.customer_code' => $customer->code,
                    'Connections.deleted' => false,
                    'Connections.enabled' => false
                ]);
        }

        foreach ($connections as $connection) {

            if ($this->IntegrationRouter->enableConnection($connection, true)) {

                $connection->enabled = true;
                $connection->enabling_date = Time::now();

                if ($this->Connections->save($connection)) {

                    $this->loadModel('ConnectionsHasMessageTemplates');
                    $chmt =  $this->ConnectionsHasMessageTemplates->find()
                        ->where([
                            'connections_id' => $connection->id, 
                            'type' => 'Bloqueo'])
                        ->first();

                    if (!$this->ConnectionsHasMessageTemplates->delete($chmt)) {
                        //$this->Flash->error(__('No se pudo eliminar el aviso de bloqueo.'));
                    }

                    $customer_code = $connection->customer_code;

                    $detail = "";
                    $detail .= 'Conexión: '. $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL; 
                    $action = 'Habilitación de Conexión - Imputación de pago automático PayU';
                    $this->registerActivity($action, $detail, $customer_code);
                } else {
                    //$this->Flash->error(__('No se pudo Habilitar la conexión.'));
                }
            }
        }
    }

    public function createCard()
    {
        if ($this->request->is('post')) {

            $payment_getway = $this->request->getSession()->read('payment_getway');
            $paraments = $this->request->getSession()->read('paraments');

            if ($payment_getway->config->payu->enabled) {

                $data = $this->request->getData();
                $barcode = $data['barcode'];
                $customer_code = $data['customer_code'];

                $this->loadModel('CustomersAccounts');
                $customers_accounts_payu_card = $this->CustomersAccounts
                    ->find()
                    ->where([
                        'customer_code'     => $customer_code,
                        'deleted'           => FALSE,
                        'payment_getway_id' => 7,
                    ]);

                $where = [
                    'customer_code' => $customer_code,
                    'deleted'       => FALSE,
                ];

                $auto_debit = FALSE;
                $customers_accounts_cobrodigital_auto_debit = [];

                foreach ($payment_getway->methods as $pg) {
                    $config = $pg->config;
                    if ($payment_getway->config->$config->enabled) {
                        if ($pg->auto_debit) {
                            $auto_debit = TRUE;

                            if (!array_key_exists('OR', $where)) {
                                $where['OR'] = [];
                            }

                            $where['OR'][] = [
                                'payment_getway_id' => $pg->id
                            ];
                        }
                    }
                }

                if ($auto_debit) {
                    $customers_accounts_cobrodigital_auto_debit = $this->CustomersAccounts
                        ->find()
                        ->where($where)
                        ->toArray();
                }

                if ($customers_accounts_payu_card->count() > 0) {
                    $this->Flash->warning(__('Únicamente puede tener una Cuenta por Cliente. Este Cliente posee una Cuenta del mismo tipo activa, en caso de querer crear una nueva Cuenta deberá "deshabilitar" la actual Cuenta activa del Cliente'));
                    return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
                }

                if (sizeof($customers_accounts_cobrodigital_auto_debit) > 0) {
                    $this->Flash->warning(__('El Cliente tiene una Cuenta de Débito Automático activa, esto inhabilita la creación de otra Cuenta, para poder crear otras Cuentas deberá "deshabilitar" dicha Cuenta de Débito Automático.'));
                    return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
                }

                $this->loadModel('PayuAccounts');

                $payu_account = $this->PayuAccounts
                    ->find()
                    ->where([
                        'customer_code'     => $customer_code,
                        'payment_getway_id' => 7,
                        'deleted'           => FALSE
                    ]);

                if ($payu_account->count() > 0) {
                    $this->Flash->warning(__('Únicamente puede tener una Tarjeta de Cobranza de PayU por Cliente.'));
                    return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
                }

                $card = $this->PayuAccounts
                    ->find()
                    ->where([
                        'barcode' => $barcode
                    ])->first();

                if ($card == NULL) {
                    $card = $this->PayuAccounts->newEntity();
                    $card->barcode = $barcode;
                    $card->id_comercio = $payment_getway->config->payu->merchant_id;
                } else if ($card->used) {
                    $this->Flash->warning(__('El código barra: ' . $barcode . '. Está siendo utilizada por una cuenta actualmente.'));
                    return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
                }

                $card->used = TRUE;
                $card->customer_code = $customer_code;
                $card->asigned = Time::now();
                $card->payment_getway_id = 7;

                if ($this->PayuAccounts->save($card)) {

                    $detail = "";
                    $detail .= 'Cód. Barra: ' . $barcode . PHP_EOL;
                    $action = 'Creación de Tarjeta de Cobranza - PayU';
                    $this->registerActivity($action, $detail, $customer_code);

                    $this->Flash->success(__('Tarjeta asiganada correctamente.'));

                    $this->loadModel('CustomersAccounts');
                    $cutomer_account = $this->CustomersAccounts->newEntity();
                    $cutomer_account->customer_code = $customer_code;
                    $cutomer_account->account_id = $card->id;
                    $cutomer_account->payment_getway_id = 7;
                    $this->CustomersAccounts->save($cutomer_account);

                    if ($paraments->gral_config->billing_for_service) {

                        $this->loadModel('Customers');
                        $customer = $this->Customers->get($customer_code, [
                            'contain' => ['Connections']
                        ]);

                        if (sizeof($customer->connections) > 0) {

                            $this->loadModel('Connections');

                            foreach ($customer->connections as $connection) {
                                $connection->payu_card = TRUE;
                                $this->Connections->save($connection);
                            }
                        }
                    }

                } else {
                    $this->Flash->warning(__('Debe cargar al menos una cuenta de PayU.'));
                }
            } else {
                $this->Flash->warning(__('Debe habilitar PayU.'));
            }
        }

        return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
    }
}
