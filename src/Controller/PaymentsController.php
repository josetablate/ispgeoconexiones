<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use App\Controller\Component\Admin\CashRegister;
use Cake\Event\EventManager;
use Cake\Event\Event;

/**
 * Payments Controller
 *
 * @property \App\Model\Table\PaymentsTable $Payments
 */
class PaymentsController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('CurrentAccount', [
            'className' => '\App\Controller\Component\Admin\CurrentAccount'
        ]);

        $this->loadComponent('FiscoAfipComp', [
             'className' => '\App\Controller\Component\Admin\FiscoAfipComp'
        ]);

        $this->loadComponent('FiscoAfipCompPdf', [
             'className' => '\App\Controller\Component\Admin\FiscoAfipCompPdf'
        ]);

        $this->loadComponent('CashRegister', [
             'className' => '\App\Controller\Component\Admin\CashRegister'
        ]);

        $this->loadComponent('IntegrationRouter', [
             'className' => '\App\Controller\Component\Integrations\IntegrationRouter'
        ]);

        $this->loadComponent('Accountant', [
             'className' => '\App\Controller\Component\Admin\Accountant'
        ]);

        $this->initEventManager();
    }

    public function initEventManager()
    {
        EventManager::instance()->on(
            'PaymentsController.Error',
            function($event, $msg, $data, $flash = false){

                $this->loadModel('ErrorLog');
                $log = $this->ErrorLog->newEntity();
                $log->user_id = $this->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Error';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->ErrorLog->save($log);

                if ($flash) {
                    $this->Flash->error(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'PaymentsController.Warning',
            function ($event, $msg, $data, $flash = false) {

                $this->loadModel('ErrorLog');
                $log = $this->ErrorLog->newEntity();
                $log->user_id = $this->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Warning';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->ErrorLog->save($log);

                if ($flash) {
                    $this->Flash->warning(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'PaymentsController.Log',
            function ($event, $msg, $data) {
                
            }
        );

        EventManager::instance()->on(
            'PaymentsController.Notice',
            function ($event, $msg, $data) {

            }
        );
    }

    public function isAuthorized($user = null)
    {
        if ($this->request->getParam('action') == 'checkStatusCashEntity') {
            return true;
        }

        if ($this->request->getParam('action') == 'getPayments') {
            return true;
        }

        if ($this->request->getParam('action') == 'getPaymentsCustomer') {
            return true;
        }

        if ($this->request->getParam('action') == 'getPaymentsCustomer') {
            return true;
        }

        if ($this->request->getParam('action') == 'anulateAutomaticPaymentGetway') {
            return true;
        }

        if ($this->request->getParam('action') == 'uploadPayments') {
            return true;
        }

        if ($this->request->getParam('action') == 'checkStatusCashEntity') {
            return true;
        }

        if ($this->request->getParam('action') == 'getLetter') {
            return true;
        }

        if ($this->request->getParam('action') == 'enableConnection') {
            return true;
        }

        if ($this->request->getParam('action') == 'strToDeciaml') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function index()
    {
        $this->loadModel('CashEntities');
        $cash_entities = $this->CashEntities->find()->where(['open' => true]);

        $cash_entities_array = [];
        $cash_entities_array[''] = 'Seleccione';
        foreach ($cash_entities as $cash_entity) {
            $cash_entities_array[$cash_entity->id] = $cash_entity->name . ' ($ ' . ($cash_entity->contado + $cash_entity->cash_other) . ')';
        }

        $users = [];
        $this->loadModel('Users');
        $users_model = $this->Users->find();
        foreach ($users_model as $u) {
            $users[$u->name] = $u->name; 
        }

        $this->loadModel('MassEmailsTemplates');
        $mass_emails_templates_model = $this->MassEmailsTemplates
            ->find()
            ->where([
                'enabled' => TRUE
            ]);

        $mass_emails_templates = [
            '' => 'Seleccione'
        ];

        foreach ($mass_emails_templates_model as $mass_email_template_model) {
            $mass_emails_templates[$mass_email_template_model->id] = $mass_email_template_model->name;
        }

        $this->set('mass_emails_templates', $mass_emails_templates);
        $this->set('cash_entities', $cash_entities);
        $this->set('users', $users);
        $this->set('cash_entities_array', $cash_entities_array);
    }

    public function getPayments()
    {
        $response =  new \stdClass();

        $response->draw = intval($this->request->getQuery('draw'));

        $customer_ids_disabled = $this->getCustomersIdsBusinessDisabled();

        $where = [];
        if(count($customer_ids_disabled) > 0){
            $where += ['Payments.customer_code NOT IN' => $customer_ids_disabled];
        }

        if (null !== $this->request->getQuery('where')) {
            $where += $this->request->getQuery('where');
            $response->recordsTotal = $this->Payments->find()->where($where)->count();
        } else {
            $response->recordsTotal = $this->Payments->find()->where($where)->count();
        }

        $response->data = $this->Payments->find('ServerSideData', [
            'params' => $this->request->getQuery(),
            'where'  => count($where) > 0 ?  $where : false
        ]);

        $response->recordsFiltered = $this->Payments->find('RecordsFiltered', [
            'params' => $this->request->getQuery(),
            'where'  =>  count($where) > 0 ?  $where : false
        ]);

        $this->set(compact('response'));
        $this->set('_serialize', ['response']);
    }

    /**
     * called from:
     *  customers/view
    */
    public function getPaymentsCustomer()
    {
        $code = $this->request->getQuery('customer_code');
        $paraments = $this->request->getSession()->read('paraments');

        $payments = $this->Payments->find()
            ->contain([
                'Users',
                'CashEntities',
                'Receipts',
            ])
            ->where(['Payments.customer_code' => $code]);

        foreach ($payments as $payment) {

            foreach ($paraments->payment_getway as $pg) {

                if ($pg->id == $payment->payment_method_id) {
                    $payment_method = new \stdClass;
                    $payment_method = $pg;
                    $payment->payment_method = $payment_method;
                }
            }
        }

        $this->set(compact('payments'));
        $this->set('_serialize', ['payments']);
    }

    private function checkStatusCashEntity()
    {
        $this->loadModel('CashEntities');

        $user_id =  $this->Auth->user()['id'];
        $cash_entity = $this->CashEntities->find()->where(['user_id' => $user_id, 'deleted' => false])->first();

        if (!$cash_entity) {
            $this->Flash->warning(__('No tiene asiganado una caja.'));
            return false;
        }

        if (!$cash_entity->enabled) {
            $this->Flash->warning(__('Su caja está deshabilitada.'));
            return false;
        }

        if (!$cash_entity->open) {
            $this->Flash->open_cash(__('Su caja está cerrada.'));
            return false;
        }
        return $cash_entity;
    }

    public function add($receipt_id = null, $customer_code = null)
    {
        $customer_load = NULL;
        if ($customer_code != NULL) {
            $this->loadModel('Customers');
            $customer_load = $this->Customers
                ->find()
                ->select([
                    'code',
                    'name',
                    'ident',
                    'address',
                    'doc_type',
                    'phone',
                    'debt_month',
                    'business_billing',
                    'billing_for_service',
                    'comments',
                ])
                ->where([
                    'code' => $customer_code
                ])->first();
        }

        //armo lista de formas de pago
        $payment_getway = $this->request->getSession()->read('payment_getway');
        $paymentMethodsx = [];
        foreach ($payment_getway->methods as $pg) {
            $config = $pg->config;
            if ($payment_getway->config->$config->enabled && $payment_getway->config->$config->cash) {
                $paymentMethodsx[$pg->id] = $pg->name;
            }
        }
        $credentials = $payment_getway->config->cobrodigital->credentials;

        //armo lista de bancos para pago con cheques
        $banks = [];
        $banks[''] = 'Seleccione Banco';
        foreach ($payment_getway->config->cheque->banks as $bank) {
            $banks[$bank] = $bank;
        }

        $accounts =  [];
        $accounts[''] = 'Seleccione cuenta';

        foreach ($payment_getway->config->transferencia->banks as $bank) {
            $accounts[$bank] = $bank;
        }

        $cash_entity = $this->checkStatusCashEntity();

        if (!$cash_entity) {
            return $this->redirect($this->referer());
        }

        $this->loadModel('PaymentsConcepts');
        $paymentsConceptsList  = $this->PaymentsConcepts->find('list');
        $paymentsConcepts = [];
        $paymentsConcepts[0] = 'Ingresar un concepto.';
        foreach ($paymentsConceptsList as $key => $pcl) {
            $paymentsConcepts[$key] = $pcl;
        }

        $payment = $this->Payments->newEntity();

        $users = [];
        $this->loadModel('Users');
        $users_model = $this->Users->find();
        foreach ($users_model as $user) {
            $users[$user->id] = $user->name;
        }

        if ($this->request->is(['post'])) {

            $error = $this->isReloadPage();

            if (!$error) {

                $this->request = $this->request->withParsedBody(json_decode($this->request->getData('data'), true));

                $request_data = $this->request->getData();
                $request_data['number_transaction'] = trim($this->request->getData('number_transaction'));

                $paraments = $this->request->getSession()->read('paraments');

                //bandera para saber si se usa caja, esto es porque si carga un pago seleccionando una plataforma de pago
                // así como ingresando el nro de referencia, como resultado generará una transacción en la paltaforma de pago seleccionada
                $load_cash_entity = TRUE;

                //Condición para saber si es una plataforma de pago que genera transacciones y si se ingreso nro de referencia
                if ($request_data['number_transaction']
                    && in_array($request_data['payment_method_id'], [7, 99, 101, 104, 105])) {

                    $column_transaction_id = FALSE;

                    switch ($request_data['payment_method_id']) {

                        case 7:
                            $column_transaction_id = 'id_orden';
                            $model_transaction = $payment_getway->config->payu->transaction;
                            break;

                        case 99:
                        case 104:
                            $column_transaction_id = 'id_transaccion';
                            $model_transaction = $payment_getway->config->cobrodigital->transaction;
                            break;

                        case 101:
                            $column_transaction_id = 'payment_id';
                            $model_transaction = $payment_getway->config->mercadopago->transaction;
                            break;

                        case 105:
                            $column_transaction_id = 'id_transaccion';
                            $model_transaction = $payment_getway->config->cuentadigital->transaction;
                            break;
                    }

                    if ($column_transaction_id) {

                        $this->loadModel($model_transaction);
                        $transaction = $this->$model_transaction
                            ->find()
                            ->where([
                                $column_transaction_id => $request_data['number_transaction']
                            ])->first();

                        if ($transaction) {

                            $this->Flash->warning(__('La transacción con nro referencia: ' . $request_data['number_transaction'] . ' ya encuentra cargada.'));
                            return $this->redirect(['action' => 'add', 0, $request_data['customer_code']]);
                        }

                        //en caso de cumplirse con la bandera a false no registrará en caja el pago
                        $load_cash_entity = FALSE;
                    }
                }

                $this->loadModel('Customers');
                $customer = $this->Customers->get($this->request->getData('customer_code'), [
                    'contain' => ['Connections']
                ]);

                //valido si existe la empresa de facturacion para este cliente
                $exist_business_id = false;
                foreach ($paraments->invoicing->business as $b) {
                    if ($b->id == $customer->business_billing) {
                        $exist_business_id = true;
                    }
                }

                if (!$exist_business_id) {

                    $data_error = new \stdClass;
                    $data_error->request_data = $this->request->getData();

                    $event = new Event('PaymentsController.Error', $this, [
                        'msg' => __('No existe la configuración de la empresa de facturación para este cliente.'),
                        'data' => $data_error,
                        'flash' => true
                    ]);

                    $this->getEventManager()->dispatch($event);
                    return $this->redirect(['action' => 'add']);
                }

                //si No marco la opcion de credito, el vuelto se decuenta del importe
                if ($this->request->getData('is_credit') == '0') {
                    if ($this->request->getData('vuelto') > 0) {
                        $request_data['import'] -= $this->request->getData('vuelto');
                    }
                }

                //si esta en mondo migracion, guarda la fecha cargada
                if ($paraments->system->mode_migration) {

                    $created = $this->request->getData('created');
                    $created = explode('/', $created);
                    $created = $created[2] . '-' . $created[1] . '-' . $created[0] . Time::now()->format(' H:i:s');
                    $request_data['created'] = $created;
                } else {
                    $request_data['created'] = Time::now();
                }

                $request_data['cash_entity_id'] = NULL;
                $request_data['number_part'] = NULL;
                $request_data['moves'] = $this->request->getData('moves');
                $request_data['user_id'] = $this->Auth->user()['id'];

                //variable inventada para decir que el pago fue hecho desde cobranza
                $payment->from_cobranza = TRUE;

                //Evita la carga de numero de parte, ya que ingreso el nro de referencia y se seleccionó una plataforma de pago que genera transacciones
                if ($load_cash_entity) {

                    $request_data['cash_entity_id'] = $cash_entity->id;

                    $this->loadModel('IntOutCashsEntities');
                    $int_out_cash_entity = $this->IntOutCashsEntities
                        ->find()
                        ->where([
                            'type'           => 'APERTURA',
                            'cash_entity_id' => $cash_entity->id
                        ])
                        ->order([
                            'created' => 'DESC'
                        ])
                        ->first();

                    $request_data['number_part'] = $int_out_cash_entity->number_part;
                }

                $payment = $this->Payments->patchEntity($payment, $request_data);

                //valido valores cargados
                if (!$payment) {

                    $data_error = new \stdClass;
                    $data_error->request_data = $request_data;
                    $data_error->$payment;

                    $event = new Event('PaymentsController.Error', $this, [
                        'msg' => __('Hubo un error al intentar cargar el pago. Verifique los valores cargados.'),
                        'data' => $data_error,
                        'flash' => true
                    ]);

                    $this->getEventManager()->dispatch($event);
                    return $this->redirect(['action' => 'add']);
                }

                if (!$this->Payments->save($payment)) {

                    $data_error = new \stdClass;
                    $data_error->request_data = $request_data;
                    $data_error->$payment;

                    $event = new Event('PaymentsController.Error', $this, [
                        'msg' => __('Hubo un error al intentar cargar el pago. Error de base de datos.'),
                        'data' => $data_error,
                        'flash' => true
                    ]);

                    $this->getEventManager()->dispatch($event);
                    return $this->redirect(['action' => 'add']);
                }

                //actualizo el saldo del cliente
                foreach ($customer->connections as $connection) {

                    $this->CurrentAccount->updateDebtMonth($customer->code, $connection->id);
                }

                $this->CurrentAccount->updateDebtMonth($customer->code, $payment->connection_id);

                //obtengo el concepto
                $payment->concept = $payment->concept ? $paymentsConcepts[$payment->concept] : $payment->concept_custom;

                $iocash = NULL;

                $data = new Data();
                $data->created = $payment->created;
                $data->cash_entity = $cash_entity;
                $data->data = $payment->customer_code;
                $data->payment_method_id = $payment->payment_method_id;
                $data->seating_number = NULL;

                $data->is_table = ($payment->payment_method_id == 1) ? 1 : 2;

                //si la forma de pago es en efectivo, registro el movimiento en el acasa del cobrador
                if ($payment->payment_method_id == 1) {

                    //si la contabilidada esta activada registro el asiento
                    if ($this->Accountant->isEnabled()) {

                        $seating = $this->Accountant->addSeating([
                            'concept'       => 'Por Cobranza',
                            'comments'      => '',
                            'seating'       => NULL, 
                            'account_debe'  => $cash_entity->account_code,
                            'account_haber' => $customer->account_code,
                            'value'         => $payment->import,
                            'user_id'       => $cash_entity->user_id,
                            'created'       => Time::now()
                        ]);

                        if ($seating) {
                            $payment->seating_number = $seating->number;
                            $data->seating_number = $seating->number;
                        }
                    }

                    $data->cash_entity->contado += $payment->import;

                } else {

                    //si la contabilidada esta activada registro el asiento
                    if ($this->Accountant->isEnabled()) {

                        foreach ($payment_getway->methods as $pg) {

                            if ($pg->id == $payment->payment_method_id) {

                                $seating = $this->Accountant->addSeating([
                                    'concept'       => 'Por Cobranza',
                                    'comments'      => 'método ' . $pg->config,
                                    'seating'       => NULL,
                                    'account_debe'  => $payment_getway->config->{$pg->config}->account,
                                    'account_haber' => $customer->account_code,
                                    'value'         => $payment->import,
                                    'user_id'       => $cash_entity->user_id,
                                    'created'       => Time::now()
                                ]);

                                if ($seating) {
                                    $payment->seating_number = $seating->number;
                                    $data->seating_number = $seating->number;
                                }
                            }
                        }
                    }

                    $data->cash_entity->cash_other += $payment->import;
                }

                $data->type = 'COBRANZA';
                $data->in_value = $payment->import;
                $data->concept = $payment->concept;

                //Evita el registro en caja, ya que ingreso el nro de referencia y se seleccionó una plataforma de pago que genera transacciones
                if ($load_cash_entity) {

                    //registro el ingreso en caja
                    $iocash = $this->CashRegister->register($data, CashRegister::TYPE_IN);

                    if (!$iocash) {
                        $data_error = new \stdClass;
                        $data_error->request_data = $request_data;

                        $event = new Event('PaymentsController.Error', $this, [
                            'msg' => __('Hubo un error al intentar registrar en caja el pago. Verifique los valores cargados.'),
                            'data' => $data_error,
                            'flash' => true
                        ]);

                        $this->getEventManager()->dispatch($event);
                        return $this->redirect(['action' => 'add']);
                    }

                    //actualizo el monto de caja
                    if (!$this->CashEntities->save($data->cash_entity)) {

                        $data_error = new \stdClass;
                        $data_error->request_data = $request_data;

                        $event = new Event('PaymentsController.Error', $this, [
                            'msg' => __('Hubo un error al intentar actualizar la caja. Verifique los valores cargados.'),
                            'data' => $data_error,
                            'flash' => true
                        ]);

                        $this->getEventManager()->dispatch($event);

                        $data->cash_entity->cash -= $payment->import;
                        $this->CashEntities->save($data->cash_entity);
                        $this->Payments->delete($payment);

                        return $this->redirect(['action' => 'add']);
                    }
                }

                //genero el recibo del pago
                $receipt = $this->FiscoAfipComp->receipt($payment);

                if (!$receipt) {

                    $data_error = new \stdClass; 
                    $data_error->request_data = $request_data;

                    $event = new Event('PaymentsController.Error', $this, [
                        'msg' => __('Hubo un error al intentar generar el recibo. Verifique los valores cargados.'),
                        'data' => $data_error,
                        'flash' => true
                    ]);

                    $this->getEventManager()->dispatch($event);

                    //si el pago fue en efectivo, se resta el importe de caja y eliminar el mov de ingreso
                    if ($iocash) {
                        $this->CashRegister->remove($iocash);
                        $data->cash_entity->cash -= $payment->import;
                        $this->CashEntities->save($data->cash_entity);
                    }

                    $this->Payments->delete($payment);

                    return $this->redirect(['action' => 'add']);
                }

                //habilito la conexion relacionada al serivicio que esta pagando
                $this->enableConnection($payment, $customer);

                $user_id = $this->request->getSession()->read('Auth.User')['id'];
                $afip_codes = $this->request->getSession()->read('afip_codes');

                $this->loadModel('Users');
                $user = $this->Users->get($user_id);
                $user_name = $user->name . ' - username: ' . $user->username;

                $customer_name = $customer->name . ' - Código: ' . $customer->code . ' - ' . $afip_codes['doc_types'][$customer->doc_type] . ': ' . $customer->ident;
                $customer_code = $customer->code;

                $controllerClass = 'App\Controller\PaymentCommitmentController';

                $payemnt_commitment = new $controllerClass;
                if ($payemnt_commitment->verifyCompliance($customer->code)) {

                    $created = $payment->created->format('d/m/Y H:i');
                    $import = number_format($payment->import, 2, ',', '.');

                    $action = 'Cumplío Compromiso de Pago';
                    $detail = 'Cliente: ' . $customer_name . PHP_EOL;
                    $detail .= 'Usuario: ' . $user_name . PHP_EOL;

                    $detail .= '---------------------------' . PHP_EOL;

                    $this->registerActivity($action, $detail, $customer_code);

                    // Agregado del Compromiso de Pago en Observaciones
                    $this->loadModel('Observations');

                    $observation = $this->Observations->newEntity();
                    $observation->customer_code = $customer->code;
                    $observation->comment = 'Cumplío Compromiso de Pago' . ' - Fecha de pago: ' . $created . ' - Importe de pago: $' . $import;
                    $observation->user_id = $user_id;

                    $this->Observations->save($observation);
                }

                if ($paraments->invoicing->email_emplate_receipt != "") {

                    $controllerClass = 'App\Controller\MassEmailsController';

                    $mass_email = new $controllerClass;
                    if ($mass_email->sendEmailReceipt($receipt->id, $paraments->invoicing->email_emplate_receipt)) {

                        $action = 'Envío recibo al cliente';
                        $detail = 'Cliente: ' . $customer_name . PHP_EOL;
                        $detail .= 'Usuario: ' . $user_name . PHP_EOL;

                        $detail .= '---------------------------' . PHP_EOL;

                        $this->registerActivity($action, $detail, $customer_code);
                    } else {

                        $action = 'No envío recibo al cliente';
                        $detail = 'Cliente: ' . $customer_name . PHP_EOL;
                        $detail .= 'Usuario: ' . $user_name . PHP_EOL;

                        $detail .= '---------------------------' . PHP_EOL;

                        $this->registerActivity($action, $detail, $customer_code);
                    }
                }

                //si marco para impimir el recibo, envio el numero de recibo
                if ($this->request->getData('get_receipt')) {
                    return $this->redirect(['action' => 'add', $payment->receipt_id, $payment->customer_code]);
                } else {
                    return $this->redirect(['action' => 'add', 0, $payment->customer_code]);
                }
            } else {

                $this->Flash->warning(__('La misma acción se intento realizar varias veces.'));
                return $this->redirect(['action' => 'add']);
            }
        }

        $this->set(compact('payment', 'banks', 'paymentsConcepts', 'paymentMethodsx', 'receipt_id', 'customer_code', 'accounts', 'users', 'credentials', 'payment_getway', 'customer_load'));
        $this->set('_serialize', ['payment']);
    }

    public function printReceipt($id)
    {
        $data = new Data();
        $data->id = $id;

        $this->FiscoAfipCompPdf->receipt($data);
    }

    public function anulate()
    {
        if ($this->request->is('ajax')) {//Ajax Detection

            $response = new \stdClass;
            $response->error = false;
            $response->msg = 'Anulación de Pago Completa';
            $response->typeMsg = 'success';

            $payment_id = $this->request->input('json_decode')->id;
            $cash_entity_id = $this->request->input('json_decode')->cash_entity_id;
            $cash = $this->request->input('json_decode')->cash;

            $payment = $this->Payments->get($payment_id, ['contain' => [
                'Customers.Areas',
                'Receipts',
            ]]);

            if (!$payment->cash_entity_id && $cash) {

                $response->error = true;
                $response->msg = 'No se puede anular un pago que no se haya cargado a un caja';
                $response->typeMsg = 'error';

            } else {

                $this->loadModel('Customers');
                $customer = $this->Customers->get($payment->customer_code);

                if ($cash) {

                    $this->loadModel('CashEntities');
                    $cash_entity = $this->CashEntities->get($cash_entity_id);

                    $data = new Data();
                    $data->cash_entity = $cash_entity;
                    $data->data = $payment->customer_code;

                    if ($payment->payment_method_id == 1) {

                       $data->cash_entity->contado -= $payment->import; 
                       $data->is_table = 1;

                    } else {
                       $data->cash_entity->cash_other -= $payment->import;
                       $data->is_table = 2;
                    }

                    $data->seating_number = null;

                    if ($this->Accountant->isEnabled()) {

                        $seating = $this->Accountant->addSeating([
                            'concept'       => 'ANULACIÓN DE PAGO',
                            'comments'      => '',
                            'seating'       => NULL, 
                            'account_debe'  => $customer->account_code,
                            'account_haber' => $cash_entity->account_code,
                            'value'         => $payment->import,
                            'user_id'       => $this->request->getSession()->read('Auth.User')->id,
                            'created'       => Time::now()
                        ]);

                        if ($seating) {
                             $data->seating_number = $seating->number;
                        }
                    }

                    $data->type = 'ANULACIÓN DE PAGO';
                    $data->out_value = $payment->import;
                    $data->concept = 'Error de Cobranza. Recibo # ' . sprintf("%'.04d", $payment->receipt->pto_vta) . '-' . sprintf("%'.08d", $payment->receipt->num);

                    if ($this->CashRegister->register($data, CashRegister::TYPE_OUT)) {

                        $this->CashEntities->save($data->cash_entity);

                        $receipt = $payment->receipt;

                        $payment->anulated = Time::now();
                        $this->Payments->save($payment);

                        $payment->receipt->resto = 0;

                        if ($this->Payments->Receipts->save($receipt)) {

                            //log de accion

                            $afip_codes = $this->request->getSession()->read('afip_codes');

                            $tipo_comp = $this->getLetter($afip_codes['comprobantesLetter'], $receipt->tipo_comp);

                            $action = 'Anulación de Cobranza';
                            $detail = 'Tipo de Comp.: ' . $tipo_comp . PHP_EOL;
                            $detail .= 'Fecha: ' . $receipt->date->format('d/m/Y') . PHP_EOL;
                            $detail .= 'Número: ' . sprintf("%'.04d", $receipt->pto_vta) . '-' . sprintf("%'.08d", $receipt->num) . PHP_EOL;
                            $detail .= 'Total: ' . number_format($receipt->total, 2, ',', '.') . PHP_EOL;
                            $detail .= 'Caja: ' . $data->cash_entity->name . PHP_EOL;

                            $detail .= '---------------------------' . PHP_EOL;

                            $this->registerActivity($action, $detail, $payment->customer_code);

                            $this->CurrentAccount->updateDebtMonth($payment->customer_code, $payment->receipt->connection_id);
                        }
                    } else {

                        $response->error = true;
                        $response->msg = 'Error al intentar registra el egreso de caja';
                        $response->typeMsg = 'error';
                    }

                } else { // no se selecciono caja

                    $receipt = $payment->receipt;

                    $payment->anulated = Time::now();
                    $this->Payments->save($payment);

                    $payment->receipt->resto = 0;
                    
                    if ($this->Payments->Receipts->save($receipt)) {

                        $afip_codes = $this->request->getSession()->read('afip_codes');

                        $tipo_comp = $this->getLetter($afip_codes['comprobantesLetter'], $receipt->tipo_comp);

                        $action = 'Anulación de Cobranza';
                        $detail = 'Tipo de Comp.: ' . $tipo_comp . PHP_EOL;
                        $detail .= 'Fecha: ' . $receipt->date->format('d/m/Y') . PHP_EOL;
                        $detail .= 'Número: ' . sprintf("%'.04d", $receipt->pto_vta) . '-' . sprintf("%'.08d", $receipt->num) . PHP_EOL;
                        $detail .= 'Total: ' . number_format($receipt->total, 2, ',', '.') . PHP_EOL;
                        $detail .= 'Sin Caja' . PHP_EOL;

                        $detail .= '---------------------------' . PHP_EOL;

                        $this->registerActivity($action, $detail, $payment->customer_code);

                        $this->CurrentAccount->updateDebtMonth($payment->customer_code, $payment->receipt->connection_id);
                   
                    } else {

                        $response->error = true;
                        $response->msg = 'Error al anular el pago';
                        $response->typeMsg = 'error';
                    }
                }
            }

            $this->set('response', $response);
        }
    }

    public function anulateAutomaticPaymentGetway()
    {
        if ($this->request->is('ajax')) { //ajax Detection

            $response = new \stdClass;
            $response->error = false;
            $response->msg = 'Anulación de Pago Completa';
            $response->typeMsg = 'success';

            $payment_getway = $this->request->getSession()->read('payment_getway');

            $transaction_id = $this->request->input('json_decode')->transaction_id;
            $payment_getway_id = $this->request->input('json_decode')->payment_getway_id;

            foreach ($payment_getway->methods as $pg) {
                $config = $pg->config;
                if ($pg->id == $payment_getway_id) {
                    $transaction = $payment_getway->config->$config->transaction;
                    continue;
                }
            }

            $this->loadModel($transaction);
            $transaction_model = $this->$transaction
                ->find()
                ->contain([
                    'Customers'
                ])
                ->where([
                    'id' => $transaction_id
                ])->first();

            if ($transaction_model->receipt_id) {

                $receipts_ids = explode(",", $transaction_model->receipt_id);

                $payments = $this->Payments
                    ->find()
                    ->contain([
                        'Customers.Areas',
                        'Receipts'
                    ])
                    ->where([
                        'receipt_id IN' => $receipts_ids
                    ]);

                if ($payments->count() > 0) {

                    foreach ($payments as $payment) {

                        $customer = $payment->customer;

                        if ($this->Accountant->isEnabled()) {

                            $account_haber = NULL;

                            foreach ($payment_getway->methods as $key => $pg) {
                                if ($pg->id == $payment->payment_method_id) {
                                    $config = $pg->config;
                                    $account_haber = $payment_getway->config->$config->account;
                                }
                            }

                            if ($account_haber) {

                                $seating = $this->Accountant->addSeating([
                                    'concept'       => 'ANULACIÓN DE PAGO',
                                    'comments'      => '',
                                    'seating'       => NULL, 
                                    'account_debe'  => $customer->account_code,
                                    'account_haber' => $account_haber,
                                    'value'         => $payment->import,
                                    'user_id'       => $this->request->getSession()->read('Auth.User')->id,
                                    'created'       => Time::now()
                                ]);
                            }
                        }

                        $receipt = $payment->receipt;

                        $payment->anulated = Time::now();

                        if ($this->Payments->save($payment)) {

                            switch ($payment_getway_id) {

                                case 106:
                                    $this->loadModel('VisaAutoDebitRecords');
                                    $visa_auto_debit = $this->VisaAutoDebitRecords
                                        ->find()
                                        ->where([
                                            'id' => $transaction_model->id_transaction
                                        ])->first();
                                    $visa_auto_debit->status = 0;
                                    $this->VisaAutoDebitRecords->save($visa_auto_debit);
                                    $transaction_model->status = 0;
                                    $transaction_model->comment = "Transacción rechazada manualmente (edición)";
                                    break;

                                case 107:
                                    $this->loadModel('MastercardAutoDebitRecords');
                                    $mastercard_auto_debit = $this->MastercardAutoDebitRecords
                                        ->find()
                                        ->where([
                                            'id' => $transaction_model->id_transaction
                                        ])->first();
                                    $mastercard_auto_debit->status = 0;
                                    $this->MastercardAutoDebitRecords->save($mastercard_auto_debit);
                                    $transaction_model->status = 0;
                                    $transaction_model->comment = "Transacción rechazada manualmente (edición)";
                                    break;

                                default:
                                    $transaction_model->estado = 0;
                                    $transaction_model->comments = "Transacción rechazada manualmente (anulación de pago)";
                            }

                            $transaction_model->receipt_id = NULL;
                            $transaction_model->receipt_number = NULL;
                            $this->$transaction->save($transaction_model);

                            //log de accion

                            $afip_codes = $this->request->getSession()->read('afip_codes');

                            $tipo_comp = $this->getLetter($afip_codes['comprobantesLetter'], $receipt->tipo_comp);

                            $action = 'Anulación de Cobranza';
                            $detail = 'Tipo de Comp.: ' . $tipo_comp . PHP_EOL;
                            $detail .= 'Fecha: ' . $receipt->date->format('d/m/Y') . PHP_EOL;
                            $detail .= 'Número: ' . sprintf("%'.04d", $receipt->pto_vta) . '-' . sprintf("%'.08d", $receipt->num) . PHP_EOL;
                            $detail .= 'Total: ' . number_format($receipt->total, 2, ',', '.') . PHP_EOL;

                            $detail .= '---------------------------' . PHP_EOL;

                            $this->registerActivity($action, $detail, $payment->customer_code);

                            $this->CurrentAccount->updateDebtMonth($payment->customer_code, $payment->receipt->connection_id);
                        }
                    }

                } else {
                    $response->msg = 'La transacción no tiene asociado Pagos.';
                    $response->typeMsg = 'error';
                }

            } else {
                $response->msg = 'La transacción no tiene asociado Recibo.';
                $response->typeMsg = 'error';
            }

            $this->set('response', $response);
        }
    }

    private function getLetter($letters, $idnex)
    {
        foreach ($letters as $i =>  $letter) {

            if ($i == $idnex) {
                return $letter;
            }
        }
    }

    private function enableConnection($payment, $customer)
    {
        $paraments = $this->request->getSession()->read('paraments');

        $this->loadModel('Connections');

        $connections = [];

        if ($customer->billing_for_service) {

             if ($payment->connection_id) {

                 $connections = $this->Connections->find()->contain(['Customers', 'Controllers'])
                    ->where([
                        'Connections.id' => $payment->connection_id,
                        'Connections.deleted' => false,
                        'Connections.enabled' => false
                    ]);
             }

        } else {

            $connections = $this->Connections->find()->contain(['Customers', 'Controllers'])
                ->where([
                    'Connections.customer_code' => $customer->code,
                    'Connections.deleted' => false,
                    'Connections.enabled' => false
                ]);
        }

        foreach ($connections as $connection) {

            if ($this->IntegrationRouter->enableConnection($connection, true)) {

                $connection->enabled = true;
                $connection->enabling_date = Time::now();

                if ($this->Connections->save($connection)) {

                    $this->loadModel('ConnectionsHasMessageTemplates');
                    $chmt =  $this->ConnectionsHasMessageTemplates->find()
                        ->where([
                            'connections_id' => $connection->id, 
                            'type' => 'Bloqueo'])
                        ->first();

                    if (!$this->ConnectionsHasMessageTemplates->delete($chmt)) {
                        $this->Flash->error(__('No se pudo eliminar el aviso de bloqueo.'));
                    }
                } else {
                    $this->Flash->error(__('No se pudo Habilitar la conexión.'));
                }
            }
        }
    }

    public function uploadPayments()
    {
        $cash_entity = $this->checkStatusCashEntity();

        if (!$cash_entity) {
            return $this->redirect($this->referer());
        }

        if ($this->request->is('post')) {

            $error = $this->isReloadPage();

            if (!$error) {

                if ($_FILES['csv']) {

                    $action = 'Importador de pagos';
                    $detail = '';
                    $this->registerActivity($action, $detail, NULL, TRUE);

                    $handle = fopen($_FILES['csv']['tmp_name'], "r");

                    $this->loadModel('IntOutCashsEntities');
                    $this->loadModel('Customers');
                    $payment_getway = $this->request->getSession()->read('payment_getway');
                    $paraments = $this->request->getSession()->read('paraments');

                    $gut_count = 0;
                    $no_gut = 0;
                    $count = 0;

                    $user_id = $this->request->getSession()->read('Auth.User')['id'];

                    $customers_codes = "";
                    $first = TRUE;

                    while ($data = fgetcsv($handle, 999999, ";")) {

                        if ($count != 0) {

                            $seating = NULL;

                            // columnas
                            // 0- fecha de pago
                            // 1- código de cliente,
                            // 2- importe,
                            // 3- concepto (opcional),
                            // 4- método de pago (opcional),

                            $time_now = Time::now();
                            $hms = $time_now->i18nFormat('HH:mm:ss');

                            // columna 0 -> fecha de pago
                            if (!empty($data[0])) {
                                $created = explode('/', $data[0]);
                                $created = new Time($created[2] . '-' . $created[1] . '-' . $created[0] . ' ' . $hms);
                            } else {
                                $no_gut = 0;
                                continue;
                            }

                            // columna 1 -> código de cliente
                            if (!empty($data[1])) {

                                $customer_code = $data[1];
                                $customer = $this->Customers
                                    ->find()
                                    ->where([
                                        'code' => $customer_code
                                    ])->first();

                                if ($customer == NULL) {
                                    $no_gut = 0;
                                    continue;
                                }
                            } else {
                                $no_gut = 0;
                                continue;
                            }

                            // columna 2 -> importe
                            if (!empty($data[2]) && $data[2] > 0) {
                                $import = $this->strToDeciaml($data[2]);
                            } else {
                                $no_gut = 0;
                                continue;
                            }

                            // columna 3 -> concepto (opcional)
                            $concept = "pago";
                            if (!empty($data[3])) {
                                $concept = $data[3];
                            }

                            // columna 4 -> Método de pago (opcional)
                            $payment_method_id = 1;
                            if (!empty($data[4]) && is_integer(intval($data[4]))) {
                                $payment_method_id = $data[4];
                            }

                            $int_out_cash_entity = $this->IntOutCashsEntities
                                ->find()
                                ->where([
                                    'type'           => 'APERTURA',
                                    'cash_entity_id' => $cash_entity->id
                                ])
                                ->order([
                                    'created' => 'DESC'
                                ])
                                ->first();

                            $iocash = null;

                            $data = new Data();
                            $data->created = $created;
                            $data->cash_entity = $cash_entity;
                            $data->data = $customer_code;
                            $data->payment_method_id = $payment_method_id;
                            $data->seating_number = NULL;

                            $data->is_table = ($payment_method_id == 1) ? 1 : 2;

                            //si la contabilidada esta activada registro el asiento
                            if ($this->Accountant->isEnabled()) {

                                $account_debe = $cash_entity->account_code;

                                if ($payment_method_id != 1) {

                                    foreach ($payment_getway->methdos as $method) {
                                        if ($method->id == $payment_method_id) {
                                            $config = $method->config;
                                            $account_debe = $payment_getway->config->$config->account;
                                            continue;
                                        }
                                    }
                                }

                                $seating = $this->Accountant->addSeating([
                                    'concept'       => 'Pago desde importador',
                                    'comments'      => '',
                                    'seating'       => NULL, 
                                    'account_debe'  => $account_debe,
                                    'account_haber' => $customer->account_code,
                                    'value'         => $import,
                                    'user_id'       => $user_id,
                                    'created'       => Time::now()
                                ]);
                            }

                            if ($payment_method_id == 1) {
                                $data->cash_entity->contado += $import;
                            } else {
                                $data->cash_entity->cash_other += $import;
                            }

                            $data_payment = [
                                'import'                => $import,
                                'created'               => $created,
                                'cash_entity_id'        => $cash_entity->id,
                                'user_id'               => $user_id,
                                'seating_number'        => $seating ? $seating->number : NULL,
                                'customer_code'         => $customer->code,
                                'payment_method_id'     => $payment_method_id, //id = 99 metodo de pago -> cobrodigital
                                'concept'               => $concept,
                                'connection_id'         => NULL,
                                'number_transaction'    => NULL,
                                'account_id'            => NULL,
                                'number_part'           => $int_out_cash_entity->number_part,
                            ];

                            $payment = $this->Payments->newEntity();

                            $payment = $this->Payments->patchEntity($payment, $data_payment);

                            if ($payment) {

                                if ($this->Payments->save($payment)) {

                                    $data->type = 'COBRANZA';
                                    $data->in_value = $payment->import;
                                    $data->concept = $payment->concept;

                                    //registro el ingreso en caja
                                    $iocash = $this->CashRegister->register($data, CashRegister::TYPE_IN);

                                    if (!$iocash) {
                                        $data_error = new \stdClass; 
                                        $data_error->request_data = $request_data;

                                        $event = new Event('PaymentsController.Error', $this, [
                                            'msg' => __('Hubo un error al intentar registrar en caja el pago. Verifique los valores cargados.'),
                                            'data' => $data_error,
                                            'flash' => true
                                        ]);

                                        $this->getEventManager()->dispatch($event);
                                        $no_gut++;
                                    }

                                    //actualizo el monto de caja
                                    if (!$this->CashEntities->save($data->cash_entity)) {

                                        $data_error = new \stdClass;
                                        $data_error->request_data = $request_data;

                                        $event = new Event('PaymentsController.Error', $this, [
                                            'msg' => __('Hubo un error al intentar actualizar la caja. Verifique los valores cargados.'),
                                            'data' => $data_error,
                                            'flash' => true
                                        ]);

                                        $this->getEventManager()->dispatch($event);

                                        $data->cash_entity->cash -= $payment->import;
                                        $this->CashEntities->save($data->cash_entity);
                                        $this->Payments->delete($payment);

                                        $no_gut++;
                                    }

                                    //genero el recibo del pago
                                    $receipt = $this->FiscoAfipComp->receipt($payment);

                                    if ($receipt) {

                                        $gut_count++;

                                        //habilito la conexion relacionada al serivicio que esta pagando
                                        $this->enableConnection($payment, $customer);

                                        $user_id = $this->request->getSession()->read('Auth.User')['id'];
                                        $afip_codes = $this->request->getSession()->read('afip_codes');

                                        $this->loadModel('Users');
                                        $user = $this->Users->get($user_id);
                                        $user_name = $user->name . ' - username: ' . $user->username;

                                        $ident = $customer->ident ? $customer->ident : "";

                                        $customer_name = $customer->name . ' - Código: ' . $customer->code . ' - ' . $afip_codes['doc_types'][$customer->doc_type] . ': ' . $ident;
                                        $customer_code = $customer->code;

                                        $action = 'Generación pago desde Importador de Pagos';
                                        $detail = 'Cliente: ' . $customer_name . PHP_EOL;
                                        $detail .= 'Fecha: ' . $created . PHP_EOL;
                                        $detail .= 'Total: ' . $import . PHP_EOL;

                                        $detail .= '---------------------------' . PHP_EOL;

                                        $this->registerActivity($action, $detail, $customer_code);

                                        if ($first) {
                                            $customers_codes .= $customer_code;
                                            $first = FALSE;
                                        } else {
                                            $customers_codes .= '.' . $customer_code;
                                        }

                                        if ($paraments->invoicing->email_emplate_receipt != "") {

                                            $controllerClass = 'App\Controller\MassEmailsController';

                                            $mass_email = new $controllerClass;
                                            if ($mass_email->sendEmailReceipt($receipt->id, $paraments->invoicing->email_emplate_receipt)) {

                                                $action = 'Envío recibo al cliente';
                                                $detail = 'Cliente: ' . $customer_name . PHP_EOL;
                                                $detail .= 'Usuario: ' . $user_name . PHP_EOL;

                                                $detail .= '---------------------------' . PHP_EOL;

                                                $this->registerActivity($action, $detail, $customer_code);
                                            } else {

                                                $action = 'No envío recibo al cliente';
                                                $detail = 'Cliente: ' . $customer_name . PHP_EOL;
                                                $detail .= 'Usuario: ' . $user_name . PHP_EOL;

                                                $detail .= '---------------------------' . PHP_EOL;

                                                $this->registerActivity($action, $detail, $customer_code);
                                            }
                                        }

                                    } else {

                                        $no_gut++;
                                        $data_error = new \stdClass; 
                                        $data_error->request_data = $request_data;

                                        $event = new Event('PaymentsController.Error', $this, [
                                            'msg' => __('Hubo un error al intentar generar el recibo. Verifique los valores cargados.'),
                                            'data' => $data_error,
                                            'flash' => true
                                        ]);

                                        $this->getEventManager()->dispatch($event);

                                        //si el apgo fue en efectivo, se resta el importe de caja y eliminar el mov de ingreso
                                        if ($iocash) {
                                            $this->CashRegister->remove($iocash);
                                            $data->cash_entity->cash -= $payment->import;
                                            $this->CashEntities->save($data->cash_entity);
                                        }

                                        //rollback payment
                                        $this->Payments->delete($payment);
                                        //recibo
                                        $this->FiscoAfipComp->rollbackReceipt($receipt, TRUE);
                                        //rollback seating
                                        //si la contabilidada esta activada registro el asientop
                                        if ($this->Accountant->isEnabled()) {
                                            if ($seating) {
                                                $this->Accountant->deleteSeating();
                                            }
                                        }
                                    }
                                } else {
                                    $no_gut++;
                                }

                            } else {
                                $no_gut++;
                            }
                        }
                        $count++;
                    }

                    if ($customers_codes != "") {
                        $command = ROOT . "/bin/cake debtMonthControlAfterAction $customers_codes 1 > /dev/null &";
                        $result = exec($command);
                    }

                    $this->Flash->success(__('Cantidad de pagos cargadas correctamente: ' . $gut_count . '\n' . 'Cantidad de pagos no cargadas: ' . $no_gut));
                }
            } else {

                $this->Flash->warning(__('La misma acción se intento realizar varias veces.'));
                return $this->redirect(['action' => 'uploadPayments']);
            }
        }
    }

    private function strToDeciaml($value)
    {
       $value = str_replace('.', '', $value);
       $value = str_replace(',', '.', $value);
       return floatval($value);
    }
}

class Data {}
