<?php
namespace App\Controller;

use App\Controller\AppController;
use FPDF;
use Cake\I18n\Time;
use Cake\Datasource\ConnectionManager;


/**
 * DiaryBook Controller
 *
 * @property \App\Model\Table\DiaryBookTable $DiaryBook
 */
class DiaryBookController extends AppController
{
    
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Accountant', [
             'className' => '\App\Controller\Component\Admin\Accountant'
        ]);
    }

    public function isAuthorized($user = null)
    {
        if ($this->request->getParam('action') == 'getDiaryByFilter') {
            return true;
        }

        if ($this->request->getParam('action') == 'getMayorByFilter') {
            return true;
        }

        if ($this->request->getParam('action') == 'getSuma') {
            return true;
        }

        if ($this->request->getParam('action') == 'mayorAccount') {
            return true;
        }

        if ($this->request->getParam('action') == 'compAdmin') {
            return true;
        }

        if ($this->request->getParam('action') == 'getCompAdminCustomers') {
            return true;
        }

        if ($this->request->getParam('action') == 'delete') {
            return true;
        }

        if ($this->request->getParam('action') == 'updateAccountParent') {
            return true;
        }

        if ($this->request->getParam('action') == 'HeaderMayor') {
            return true;
        }

        if ($this->request->getParam('action') == 'FooterMayor') {
            return true;
        }

        if ($this->request->getParam('action') == 'HeaderSuma') {
            return true;
        }

        if ($this->request->getParam('action') == 'FooterSuma') {
            return true;
        }

        if ($this->request->getParam('action') == 'HeaderDiary') {
            return true;
        }

        if ($this->request->getParam('action') == 'FooterDiary') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function index($seating_number_preseleted = 0, $action_preselected = null)
    {
        if ($seating_number_preseleted == 0) {
            $seating_number_preseleted = null;
        }

        $this->set(compact('seating_number_preseleted', 'action_preselected'));
    }

    /**
     * called from
     *  DiaryBook index
    */
    public function getDiaryByFilter()
    {
        $filter_data = $this->request->getQuery('filter_data');
        $filter = new \stdClass;

        foreach ($filter_data as $data) {

            $filter->{$data['name']} = $data['value'];
        }

        $this->loadModel('Seating');
        $this->loadModel('SeatingDetail');
        $seatingsDetails = $this->SeatingDetail->find()->contain(['Seating', 'Accounts']);

        $seatingsDetailsFilter = $this->SeatingDetail->find()->contain(['Seating', 'Accounts']);

        $filter->from = explode('/', $filter->from);
        $filter->from = $filter->from[2] . '-' . $filter->from[1] . '-' . $filter->from[0] . ' 00:00:00';

        $filter->to = explode('/', $filter->to);
        $filter->to = $filter->to[2] . '-' . $filter->to[1] . '-' . $filter->to[0] . ' 23:59:59';

        if ($filter->number == '') {

            $seatingsDetailsFilter->where([
                'Seating.created >=' => $filter->from,
                'Seating.created <' => $filter->to,
            ]);

            if ($filter->concept != '') {
                $seatingsDetailsFilter->andWhere(['Seating.concept LIKE' => '%' . $filter->concept . '%']);
            }

            if ($filter->manual == 1) {
                $seatingsDetailsFilter->andWhere(['Seating.type' => 'm']);
            }

            if ($filter->account_code_parent != '') {

                $filter->account_code = str_pad($filter->account_code_parent, 4, "0", STR_PAD_RIGHT);
                $filter->account_code .= str_pad($filter->account_code_child, 5, "0", STR_PAD_LEFT);

                if ($filter->account_code != '') {
                    $seatingsDetailsFilter->andWhere(['account_code' => $filter->account_code]);
                }
            }

            if ($filter->account_name != '') {
                $seatingsDetailsFilter->andWhere(['Accounts.name LIKE' => '%' . $filter->account_name . '%']);
            }

        } else {

             $seatingsDetailsFilter->where(['Seating.number' => $filter->number]);
        }

        $is_result = false;

        foreach ($seatingsDetailsFilter as $sdf) {
            $is_result = true;
            $seatingsDetails->orWhere(['Seating.number' => $sdf->seating_number]);
        }

        if (!$is_result) {
            $seatingsDetails = [];
        } else {
            $seatingsDetails->order(['Seating.number' => 'ASC', 'SeatingDetail.debe' => 'DESC']); 
        }

        $this->set('seatingsDetails', $seatingsDetails);
    }

    /**
     * called from
     *  DiaryBook mayor
    */
    public function getMayorByFilter()
    {
        $seatingsDetailsArray = [];

        $filter_data = $this->request->getQuery('filter_data');

        if ($filter_data) {

            $filter = new \stdClass;

            foreach ($filter_data as $data) {
                $filter->{$data['name']} = $data['value'];
            }

            $filter->from = explode('/', $filter->from);
            $filter->from = $filter->from[2] . '-' . $filter->from[1] . '-' . $filter->from[0] . ' 00:00:00';

            $filter->to = explode('/', $filter->to);
            $filter->to = $filter->to[2] . '-' . $filter->to[1] . '-' . $filter->to[0] . ' 23:59:59';

            //asientos en el rango de las fechas
            $sql = "SELECT seating.number, seating.created, seating_detail.account_code, seating.concept,  
                seating_detail.debe, seating_detail.haber FROM seating_detail 
                inner join seating on seating_detail.seating_number = seating.number 
                WHERE seating.created >= '" .  $filter->from . "'
                AND seating.created <= '" . $filter->to . "'";

            //saldo anterior a la fecha inicial
            $sql2 = "SELECT SUM(seating_detail.debe) as debe, SUM(seating_detail.haber) as haber FROM seating_detail 
                inner join seating on seating_detail.seating_number = seating.number 
                WHERE seating.created < '" . $filter->from . "'";

            if ($filter->account_code_parent != '' ) {

                $filter->account_code = str_pad($filter->account_code_parent, 4, "0", STR_PAD_RIGHT);
                $filter->account_code .= str_pad($filter->account_code_child, 5, "0", STR_PAD_LEFT);

                $this->loadModel('Accounts');
                $account = $this->Accounts->find()->where(['code' => $filter->account_code])->first();

                $isTitle = false;

                if ($account) {

                    $sql .= "AND seating_detail.account_code = " . $filter->account_code;

                    $sql2 .= " AND seating_detail.account_code = " . $filter->account_code;

                    $database = ConnectionManager::get('default');
                    $seatingsDetails = $database->execute($sql)->fetchAll('assoc');
                    $sDSaldoAnterior = $database->execute($sql2)->fetchAll('assoc');

                    $saldo = 0;

                    foreach ($sDSaldoAnterior as $sa) {

                        $saldo += $sa['debe'];
                        $saldo -= $sa['haber'];

                        $saldo = round($saldo, 2);
                    }

                    foreach ($seatingsDetails as $sd) {

                        $data = new \stdClass;

                        $data->number = $sd['number'];
                        $data->created = $sd['created'];
                        $data->account_code = $sd['account_code'];
                        $data->concept = $sd['concept'];
                        $data->debe = $sd['debe'];
                        $data->haber = $sd['haber'];
                        $data->exist_in_admin = false;

                        $saldo += $data->debe;
                        $saldo -= $data->haber;

                        $saldo = round($saldo, 2);

                        $data->saldo = $saldo;

                        $seatingsDetailsArray[] = $data;
                    }
                }
            }
        }

        $this->loadModel('Customers');
        $this->loadModel('Debts');
        $this->loadModel('Invoices');
        $this->loadModel('Payments');
        $this->loadModel('DebitNotes');
        $this->loadModel('CreditNotes');
        $this->loadModel('CustomersHasDiscounts');

        foreach ($seatingsDetailsArray as $sda) {

            $customer = $this->Customers->find()->where(['account_code' =>  $sda->account_code])->first();

            if ($customer) {

                $debt = $this->Debts->find()->where(['seating_number' => $sda->number, 'customer_code' => $customer->code])->first();
                if ($debt) {
                    $sda->exist_in_admin = $debt->id;
                    continue;
                }
                $invoice = $this->Invoices->find()->where(['seating_number' => $sda->number, 'customer_code' => $customer->code])->first();
                if ($invoice) {
                    $sda->exist_in_admin = $invoice->id;
                    continue;
                }
                $payment = $this->Payments->find()->where(['seating_number' => $sda->number, 'customer_code' => $customer->code])->first();
                if ($payment) {
                    $sda->exist_in_admin = $payment->id;
                    continue;
                }
                $creditNote = $this->CreditNotes->find()->where(['seating_number' => $sda->number, 'customer_code' => $customer->code])->first();
                if ($creditNote) {
                    $sda->exist_in_admin = $creditNote->id;
                    continue;
                }
                $debitNote = $this->DebitNotes->find()->where(['seating_number' => $sda->number, 'customer_code' => $customer->code])->first();
                if ($debitNote) {
                    $sda->exist_in_admin = $debitNote->id;
                    continue;
                }
                $customersHasDiscount = $this->CustomersHasDiscounts->find()->where(['seating_number' => $sda->number, 'customer_code' => $customer->code])->first();
                if ($customersHasDiscount) {
                    $sda->exist_in_admin = $customersHasDiscount->id;
                    continue;
                }
            }
        }

        $this->set('seatingsDetailsArray', $seatingsDetailsArray);
    }

    public function mayorAccount($code)
    {
        $this->viewBuilder()->layout('layoutSimple');

        $this->loadModel('Accounts');
        $account = $this->Accounts->find()->where(['code' => $code])->first();

        if ($account) {

            $isTitle = $account->title;
            $data['account_code'] = $account->code;
            $data['name'] = $account->name;

        } else {
            $this->Flash->error(__('No se pudo encontra cuenta N° {0}', $code ));
            return $this->redirect(['action' => 'mayor']);
        }

        $data['from'] = Time::now()->day(1)->format('Y-m-d') . ' 00:00:00';
        $data['to'] = Time::now()->format('Y-m-d') . ' 23:59:59';

        //asientos en el rango de las fechas
        $sql = "SELECT seating.number, seating.created, seating_detail.account_code, seating.concept, 
            seating_detail.debe, seating_detail.haber FROM seating_detail 
            inner join seating on seating_detail.seating_number = seating.number 
            WHERE seating.created >= '". $data['from'] ."' 
            AND seating.created < '" . $data['to'] . "' ";

        //saldo anterior a la fecha inicial
        $sql2 = "SELECT SUM(seating_detail.debe) as debe, SUM(seating_detail.haber) as haber FROM seating_detail 
            inner join seating on seating_detail.seating_number = seating.number 
            WHERE seating.created < '" . $data['from'] . " 00:00:00' ";

        $isTitle = false;

        if ($isTitle) {

            $hrchy = '';
            $hrchy_temp = str_split(substr($code, 0, 4));
            foreach ($hrchy_temp as $h) {
                $hrchy .= ($h != 0) ? $h : '';
            }

            $sql .= " AND seating_detail.account_code LIKE '" . $hrchy . "%'";

            $sql2 .= " AND seating_detail.account_code LIKE '" . $hrchy . "%'";
        } else {
            $sql .= "AND seating_detail.account_code = ".$code;

            $sql2 .= " AND seating_detail.account_code = ".$code;
        }

        $database = ConnectionManager::get('default');
        $seatingsDetails = $database->execute($sql);
        $sDSaldoAnterior = $database->execute($sql2);
    
        $this->set(compact('seatingsDetails', 'data', 'sDSaldoAnterior'));
        $this->set('_serialize', ['seatingsDetails']);
    }

    public function mayor($account_code = null)
    {
        $this->loadModel('Accounts');

        $account_preseleted = null;

        if ($account_code) {
            $account_preseleted =  $this->Accounts->find()->where(['code' => $account_code])->first();
        }

        $this->set(compact('account_preseleted'));
    }

    public function suma()
    {}

    /**
     * called from
     *  DiaryBook suma
    */
    public function getSuma()
    {
        $filter_data = $this->request->getQuery('filter_data');

        $account = null;

        if ($filter_data) {

            $filter = new \stdClass;

            foreach ($filter_data as $data) {

                $filter->{$data['name']} = $data['value'];
            }

            $filter->from = explode('/',$filter->from);
            $filter->from = $filter->from[2] . '-' . $filter->from[1] . '-' . $filter->from[0] . ' 00:00:00';

            $filter->to = explode('/',$filter->to);
            $filter->to = $filter->to[2] . '-' . $filter->to[1] . '-' . $filter->to[0] . ' 23:59:59';

            $sumasSaldosArray = [];

            $sql_accounts = "SELECT * FROM accounts";

            $database = ConnectionManager::get('default');
            $accountsArray = $database->execute($sql_accounts)->fetchAll('assoc');

            foreach ($accountsArray as $accountA) {

                $data = new \stdClass;
                $data->code = $accountA['code'];
                $data->title = $accountA['title'];

                $hrchy = substr(strval($data->code), 0, 4);
                $hrchy = str_split($hrchy);

                $data->name = '';

                foreach ($hrchy as $h) {
                    if ($h != '0') {
                        $data->name .= '&nbsp;&nbsp;&nbsp'; 
                    }
                }
                if (!$data->title) {
                    $data->name .= '&nbsp;&nbsp;&nbsp;'; 
                }

                $data->name .= $accountA['name'];

                $data->saldo_acum = 0;

                $data->debe = 0;
                $data->haber = 0;

                $sumasSaldosArray[$data->code] = $data;
            }

            $sql = "SELECT 
                        accounts.code as code,
                        accounts.name as name,
                        accounts.title as title,
                        seating_detail.debe as debe,
                        seating_detail.haber as haber 
                    FROM seating_detail 
                        inner join seating on seating_detail.seating_number = seating.number 
                        inner join accounts on seating_detail.account_code = accounts.code 
                    WHERE seating.created >= '" .  $filter->from . "'
                        AND seating.created < '" . $filter->to . "'";

            $sql2 = "SELECT 
                        accounts.code as code,
                        accounts.name as name,
                        accounts.title as title,
                        seating_detail.debe as debe,
                        seating_detail.haber as haber 
                    FROM seating_detail 
                        inner join seating on seating_detail.seating_number = seating.number 
                        inner join accounts on seating_detail.account_code = accounts.code 
                    WHERE seating.created < '".  $filter->from ."'";

            $seatingsDetails = $database->execute($sql)->fetchAll('assoc');
            $sDSaldoAnterior = $database->execute($sql2)->fetchAll('assoc');

            foreach ($sDSaldoAnterior as $sDSaldoA) {

                $sumasSaldosArray[$sDSaldoA['code']]->debe += $sDSaldoA['debe'];
                $sumasSaldosArray[$sDSaldoA['code']]->haber += $sDSaldoA['haber'];
            }

            foreach ($seatingsDetails as $seatingDetail) {

                $sumasSaldosArray[$seatingDetail['code']]->debe += $seatingDetail['debe'];
                $sumasSaldosArray[$seatingDetail['code']]->haber += $seatingDetail['haber'];
            }

            $sumasSaldos = [];

            $this->updateAccountParent($sumasSaldosArray);

            $paraments = $this->request->getSession()->read('paraments');

            $customer_code_parent = substr($paraments->accountant->acounts_parent->customers, 0, 4);

            foreach ($sumasSaldosArray as $code => $sumaSaldo) {

                if ($filter->show_saldo == 1) {

                    if (abs($sumaSaldo->debe - $sumaSaldo->haber) == 0) {
                        continue;
                    }
                }

                if (substr($code, 0, 4) == $customer_code_parent) {

                    if ($filter->show_customers == 1 || $code == $paraments->accountant->acounts_parent->customers) {
                        $sumasSaldos[] = $sumaSaldo;
                    }

                } else {
                    $sumasSaldos[] = $sumaSaldo;
                }
            }

            $this->set('sumasSaldos', $sumasSaldos);
        }
    }

    private function updateAccountParent(&$accounts)
    {
        foreach ($accounts as $account) {

            $hrchy_temp = $account->code;
            $hrchy = '';
            $hrchy_temp = str_split(substr($hrchy_temp, 0, 4)); 
            foreach ($hrchy_temp as $h) {
                $hrchy .= ($h != 0) ? $h : '';
            }

            $i = strlen($hrchy);

            while ( $i > 0 ) {

                $account_parent = $accounts[str_pad($hrchy, 9, "0", STR_PAD_RIGHT)];

                if ($account_parent) {

                    $account_parent->debe += $account->debe;
                    $account_parent->haber += $account->haber;
                    $accounts[$account_parent->code] = $account_parent;
                }

                if ($i > 0)  $i--;

                $hrchy_temp = str_split(substr($hrchy, 0, $i));
                $hrchy = '';
                foreach ($hrchy_temp as $h) {
                    $hrchy .= ($h != 0) ? $h : '';
                }

                if ($i > 0) {
                    $i = strlen($hrchy);
                }
            }
        }

        return true;
    }

    public function add()
    {
      if ($this->request->is('post')) {

            $paraments = $this->request->getSession()->read('paraments');
            $afip_codes =  $this->request->getSession()->read('afip_codes');

            if (!array_key_exists('account', $this->request->getData())  
                || !array_key_exists('debe', $this->request->getData())
                || !array_key_exists('haber', $this->request->getData())) {

                $this->Flash->error(__('Debe ingresar la infomación del asiento'));
                return $this->redirect(['controller' => 'DiaryBook', 'action' => 'index']);
            }

            $amount_account = count($this->request->getData('account'));

            if ($amount_account < 2) {
                $this->Flash->error(__('Debe ingresar al menos 2 cuentas'));
                return $this->redirect(['controller' => 'DiaryBook', 'action' => 'index']);
            }

            $accounts =  [];

            $this->loadModel('Accounts');

            $debe = 0; $haber = 0; $count = 0;

            for ($i = 0; $i < $amount_account; $i++) {

                $d = ($this->request->getData('debe')[$i] != '') ? $this->request->getData('debe')[$i] : 0 ;
                $h = ($this->request->getData('haber')[$i] != '') ? $this->request->getData('haber')[$i] : 0;

                if ($d != 0 || $h != 0) {

                    $code = $this->request->getData('account')[$i];

                    if ($code != '') {

                        $a = ['code' => $code, 'debe' => 0, 'haber' => 0];

                        $a['debe'] =  ($this->request->getData('debe')[$i] != '') ? $this->request->getData('debe')[$i] : 0 ;
                        $a['haber'] = ($this->request->getData('haber')[$i] != '') ? $this->request->getData('haber')[$i] : 0;

                        $accounts[] = $a;

                        $count++;
                    }
                }
            }

            if ($count < 2) {
                $this->Flash->error(__('Debe ingresar al menos 2 cuentas'));
                return $this->redirect(['controller' => 'DiaryBook', 'action' => 'index']);
            }

            $seating = $this->Accountant->addSeating([
                'concept' => $this->request->getData('concept'),
                'comments' => $this->request->getData('comments'),
                'seating' => null,
                'account_debe' => null,
                'account_haber' => null,
                'value' => 0,
                'redirect' => ['controller' => 'DiaryBook', 'action' => 'index'],
                'seating_type' => 'm',
            ]);

            $this->loadModel('Customers');
            $this->loadModel('CashEntities');

            foreach ($accounts as $account) {

                if ($account['debe'] > 0) {

                    $seating = $this->Accountant->addSeating([
                        'concept' => null,
                        'comments' => null,
                        'seating' => $seating,
                        'account_debe' => $account['code'],
                        'account_haber' => null,
                        'value' => $account['debe'],
                        'seating_type' => 'm',
                    ]);

                    if (!$seating) {
                        return $this->redirect(['controller' => 'DiaryBook', 'action' => 'index']);
                    }
                }

                if ($account['haber'] > 0) {

                    $seating = $this->Accountant->addSeating([
                        'concept' => null,
                        'comments' => null,
                        'seating' => $seating,
                        'account_debe' => null,
                        'account_haber' => $account['code'],
                        'value' => $account['haber'],
                        'seating_type' => 'm',
                    ]);

                    if (!$seating) {
                        return $this->redirect(['controller' => 'DiaryBook', 'action' => 'index']);
                    }
                }
            }

            $this->Flash->success(__('Asiento  agregado correctamente.'));
            return $this->redirect(['controller' => 'DiaryBook', 'action' => 'index']);
        }
    }

    //pdf mayor

    public function printmayor()
    {
        if ($this->request->is('post')) {

            $data = $this->request->getData();

            $from = explode('/', $this->request->getData('from'));
            $data['from'] = $from[2] . '-' . $from[1] . '-' . $from[0] . ' 00:00:00';

            $to = explode('/', $this->request->getData('to'));
            $data['to'] = $to[2] . '-' . $to[1] . '-' . $to[0] . ' 23:59:59';

            //asientos en el rango de las fechas
            $sql = "SELECT seating.number, seating.created, seating_detail.account_code, seating.concept, 
                seating_detail.debe, seating_detail.haber FROM seating_detail 
                inner join seating on seating_detail.seating_number = seating.number 
                WHERE seating.created >= '". $data['from'] ."' 
                AND seating.created <= '" . $data['to'] . "'";

            //saldo anterior a la fecha inicial
            $sql2 = "SELECT SUM(seating_detail.debe) as debe, SUM(seating_detail.haber) as haber FROM seating_detail 
                inner join seating on seating_detail.seating_number = seating.number 
                WHERE seating.created < '" . $data['from'] . "'";

            if ($this->request->getData('account_code_parent') != '' && $this->request->getData('account_code_child') != '' ) {

                $data['account_code'] = str_pad($this->request->getData('account_code_parent'), 4, "0", STR_PAD_RIGHT);
                $data['account_code'] .= str_pad($this->request->getData('account_code_child'), 5, "0", STR_PAD_LEFT);

                $code = $data['account_code'];

                $this->loadModel('Accounts');
                $account = $this->Accounts->find()->where(['code' => $code])->first();

                if ($account) {

                    $data['account_code'] = $account->code;
                    $data['name'] = $account->name;

                    $sql .= " AND seating_detail.account_code = " . $code;

                    $sql2 .= " AND seating_detail.account_code = " . $code;

                    $database = ConnectionManager::get('default');

                    $seatingsDetails = $database->execute($sql);
                    $sDSaldoAnterior = $database->execute($sql2);

                    $saldo = 0;  

                    foreach ($sDSaldoAnterior as $value) {
                        $saldo += $value['debe'];
                        $saldo -= $value['haber'];
                        $saldo = round($saldo, 2);
                    }

                    //generate pdf

                    $this->response = $this->response->withCharset('UTF-8');
                    $this->response = $this->response->withType('application/pdf');

                    $FONT = 'Helvetica';

                    $pdf = new FPDF('P', 'mm', 'A4');
                    $pdf->AliasNbPages();

                    $middle_page = $pdf->GetPageWidth() / 2;

                    $pdf->SetTitle(utf8_decode('MAYOR'));

                    $pdf->AddPage();

                    $this->HeaderMayor($pdf, $data);
                    $this->FooterMayor($pdf);

                    $y = 32;
                    $font_size = 9;
                    $max_roe_per_page = 47;
                    $count_row = 0;

                    $count = count($seatingsDetails);
                    $counter = 0;

                    foreach ($seatingsDetails as $seatingsDetail) {

                        $counter++;
                        $saldo += $seatingsDetail['debe'];
                        $saldo -= $seatingsDetail['haber'];
                        $saldo = round($saldo,2);

                        $pdf->SetFont($FONT,'', $font_size);
                        $pdf->SetXY(10, $y);
                        $created = new Time($seatingsDetail['created']);
                        $pdf->MultiCell(20, 6, $created->format('d/m/Y'), null, 'C');

                        $pdf->SetFont($FONT,'', $font_size);
                        $pdf->SetXY(30, $y);
                        $pdf->MultiCell(20, 6, sprintf("%'.05d", $seatingsDetail['number']), null, 'C');

                        $pdf->SetFont($FONT,'', $font_size);

                        $pdf->Text(50, $y+4, utf8_decode(substr($seatingsDetail['concept'], 0, 55)), null, 'L');

                        $pdf->SetFont($FONT,'', $font_size);
                        $pdf->SetXY(140, $y);
                        $pdf->MultiCell(20, 6, number_format($seatingsDetail['debe'], 2, ',', '.'), null, 'R');

                        $pdf->SetFont($FONT,'', $font_size);
                        $pdf->SetXY(160, $y);
                        $pdf->MultiCell(20, 6, number_format($seatingsDetail['haber'], 2, ',', '.'), null, 'R');

                        $pdf->SetFont($FONT, '', $font_size);
                        $pdf->SetXY(180, $y);

                        if ($counter == $count) {
                            $pdf->SetFont($FONT,'', 10);
                        }

                        $pdf->MultiCell(20, 6, number_format($saldo, 2, ',', '.'), null, 'R');

                        $y += 5;

                        $count_row++;

                        if ($count_row >=$max_roe_per_page) {

                            $pdf->AddPage();
                            $this->HeaderMayor($pdf, $data);
                            $this->FooterMayor($pdf);
                            $pdf->SetXY(0, 0);
                            $y = 32;
                            $count_row = 0;
                        }
                    }

                    $pdf->Output('I', 'Mayor.pdf');
                }
            }
        }
    }

    private function HeaderMayor($pdf, $data)
    {
        $FONT = 'Helvetica';

        $middle_page = $pdf->GetPageWidth() / 2;

        $pdf->SetFont($FONT, '', 12);
        $pdf->Text($middle_page - 5, 10, utf8_decode('MAYOR'));

        $pdf->SetFont($FONT,'',10);
        $pdf->Text(10, 15, utf8_decode('Desde: '));
        $pdf->SetFont($FONT, '', 10);

        $from = new Time($data['from']);
        $pdf->Text(23, 15, $from->format('d/m/Y'));

        $pdf->SetFont($FONT, '', 10);
        $pdf->Text(10, 20, utf8_decode('Hasta: '));
        $pdf->SetFont($FONT, '', 10);

        $to = new Time($data['to']);
        $pdf->Text(23, 20, $to->format('d/m/Y'));

        $pdf->SetFont($FONT,'',10);
        $pdf->Text(45, 15, utf8_decode('N° Cuenta: '));
        $pdf->SetFont($FONT,'',10);
        $pdf->Text(65, 15, $data['account_code']);

        $pdf->SetFont($FONT, '', 10);
        $pdf->Text(85, 15, utf8_decode('Nombre de Cuenta: '));
        $pdf->SetFont($FONT, '', 10);
        $pdf->Text(119, 15, utf8_decode($data['name']));

        $pdf->SetFillColor(204, 204, 204);
        
        $y = 25;
            
        //cabecera del detalle
        $pdf->SetFont($FONT,'', 8);
        $pdf->SetXY(10,$y);
        $pdf->Cell(20, 6, utf8_decode('Fecha'), 1, 0, 'C', true);

        $pdf->SetFont($FONT,'', 8);
        $pdf->SetXY(30,$y);
        $pdf->Cell(20, 6, utf8_decode('N° Asiento'), 1, 0, 'C', true);

        $pdf->SetFont($FONT,'', 8);
        $pdf->SetXY(50,$y);
        $pdf->Cell(90, 6, utf8_decode('Concepto'), 1, 0, 'C', true);

        $pdf->SetFont($FONT,'', 8);
        $pdf->SetXY(140,$y);
        $pdf->Cell(20, 6, utf8_decode('Debe'), 1, 0, 'C', true);

        $pdf->SetFont($FONT,'', 8);
        $pdf->SetXY(160,$y);
        $pdf->Cell(20, 6, utf8_decode('Haber'), 1, 0, 'C', true);

        $pdf->SetFont($FONT,'', 8);
        $pdf->SetXY(180,$y);
        $pdf->Cell(20, 6, utf8_decode('Saldo'), 1, 0, 'C', true);
    }

    private function FooterMayor($pdf)
    {
        $FONT = 'Helvetica';

        // Posición: a 1,5 cm del final
        $pdf->SetXY(0,274);
        // Arial italic 8
        $pdf->SetFont($FONT,'',8);
        // Número de página
        $pdf->Cell(0, 2, utf8_decode('Página '.$pdf->PageNo().'/{nb}'), 0, 0, 'C');
    }

    // pdf suma

    public function printSuma()
    {
        if ($this->request->is('post')) {

            $filter_data = $this->request->getData();

            $account = null;

            $filter = new \stdClass;

            foreach ($filter_data as $key => $data) {

                $filter->{$key} = $data;
            }

            $filter->from = explode('/',$filter->from);
            $filter->from = $filter->from[2] . '-' . $filter->from[1] . '-' . $filter->from[0] . ' 00:00:00';

            $filter->to = explode('/',$filter->to);
            $filter->to = $filter->to[2] . '-' . $filter->to[1] . '-' . $filter->to[0] . ' 23:59:59';

            $sumasSaldosArray = [];

            $sql_accounts = "SELECT * FROM accounts";

            $database = ConnectionManager::get('default');
            $accountsArray = $database->execute($sql_accounts)->fetchAll('assoc');

            foreach ($accountsArray as $accountA) {

                $data = new \stdClass;
                $data->code = $accountA['code'];
                $data->title = $accountA['title'];

                $hrchy = substr(strval($data->code), 0, 4);
                $hrchy = str_split($hrchy);

                $data->name = '';

                foreach ($hrchy as $h) {
                    if ($h != '0') {
                        $data->name .= '   '; 
                    }
                }
                if (!$data->title) {
                    $data->name .= '   '; 
                }

                $data->name .= $accountA['name'];

                $data->debe = 0;
                $data->haber = 0;

                $sumasSaldosArray[$data->code] = $data;
            }

            $sql = "SELECT 
                        accounts.code as code,
                        accounts.name as name,
                        accounts.title as title,
                        seating_detail.debe as debe,
                        seating_detail.haber as haber 
                    FROM seating_detail 
                        inner join seating on seating_detail.seating_number = seating.number 
                        inner join accounts on seating_detail.account_code = accounts.code 
                    WHERE seating.created >= '" .  $filter->from . "'
                        AND seating.created <= '" . $filter->to . "'";

            $sql2 = "SELECT 
                        accounts.code as code,
                        accounts.name as name,
                        accounts.title as title,
                        seating_detail.debe as debe,
                        seating_detail.haber as haber 
                    FROM seating_detail 
                        inner join seating on seating_detail.seating_number = seating.number 
                        inner join accounts on seating_detail.account_code = accounts.code 
                    WHERE seating.created < '" .  $filter->from . "'";

            $seatingsDetails = $database->execute($sql)->fetchAll('assoc');
            $sDSaldoAnterior = $database->execute($sql2)->fetchAll('assoc');

            foreach ($sDSaldoAnterior as $sDSaldoA) {

                $sumasSaldosArray[$sDSaldoA['code']]->debe += $sDSaldoA['debe'];
                $sumasSaldosArray[$sDSaldoA['code']]->haber += $sDSaldoA['haber'];
            }

            foreach ($seatingsDetails as $seatingDetail) {

                $sumasSaldosArray[$seatingDetail['code']]->debe += $seatingDetail['debe'];
                $sumasSaldosArray[$seatingDetail['code']]->haber += $seatingDetail['haber'];
            }

            $sumasSaldos = [];

            $this->updateAccountParent($sumasSaldosArray);

            $paraments = $this->request->getSession()->read('paraments');

            $customer_code_parent = substr($paraments->accountant->acounts_parent->customers, 0, 4);

            foreach ($sumasSaldosArray as $code => $sumaSaldo) {

                if ($filter->show_saldo == 'true') {

                    if (abs($sumaSaldo->debe - $sumaSaldo->haber) == 0) {
                        continue;
                    }
                }

                if (substr($code, 0, 4) == $customer_code_parent) {

                    if ($filter->show_customers == 'true' || $code == $paraments->accountant->acounts_parent->customers) {
                        $sumasSaldos[] = $sumaSaldo;
                    }

                } else {
                    $sumasSaldos[] = $sumaSaldo;
                }
            }

            //generate pdf

            $this->response = $this->response->withCharset('UTF-8');
            $this->response = $this->response->withType('application/pdf');

            $FONT = 'Arial';

            $pdf = new FPDF('P', 'mm', 'A4');
            $pdf->AliasNbPages();

            $middle_page = $pdf->GetPageWidth() / 2;

            $pdf->SetTitle(utf8_decode('SUMAS Y SALDOS'));

            $pdf->AddPage();

            $this->HeaderSuma($pdf,$data);
            $this->FooterSuma($pdf);

            $y = 27;
            $font_size = 8;
            $max_roe_per_page = 47;
            $count_row = 0;

            //calculo de saldo anterior
          
            foreach ($sumasSaldos as $sumaSaldo) {

                $saldo = $sumaSaldo->debe - $sumaSaldo->haber;

                if ($sumaSaldo->title) {
                    $pdf->SetFont($FONT, '', $font_size);
                } else {
                    $pdf->SetFont($FONT, '', $font_size);
                }

                $deudor = 0;
                $acreedor = 0;

                if ($saldo > 0) {
                    $deudor = $saldo;
                } else if ($saldo < 0) {
                    $acreedor = $saldo;
                }

                $pdf->SetXY(10, $y);
                $pdf->MultiCell(17, 6, $sumaSaldo->code, null, 'L');

                $pdf->SetXY(27, $y);

                $name = '';

                $name .= $sumaSaldo->name;
                $pdf->MultiCell(63, 6, utf8_decode(substr(strtolower($name), 0, 45)) , null, 'L');

                $pdf->SetXY(112, $y);
                $pdf->MultiCell(22, 6, number_format($sumaSaldo->debe, 2, ',', '.'), null, 'R');

                $pdf->SetXY(134, $y);
                $pdf->MultiCell(22, 6, number_format($sumaSaldo->haber, 2, ',', '.'), null, 'R');
                
       
                $pdf->SetXY(156, $y);
                $pdf->MultiCell(22, 6, number_format($deudor, 2, ',', '.'), null, 'R');
              
        
                $pdf->SetXY(178, $y);
                $pdf->MultiCell(22, 6, number_format(abs($acreedor), 2, ',', '.'), null, 'R');

                $y += 5;

                $count_row++;

                if ($count_row >= $max_roe_per_page) {

                    $pdf->AddPage();
                    $this->HeaderSuma($pdf, $data);
                    $this->FooterSuma($pdf);
                    $pdf->SetXY(0, 0);
                    $y = 27;
                    $count_row = 0;
                }
            }

            $pdf->Output('I', 'Sumas y Saldos.pdf');
        }
    }

    private function HeaderSuma($pdf, $data)
    {
        $FONT = 'Helvetica';

        $middle_page = $pdf->GetPageWidth() / 2;

        $pdf->SetFont($FONT, '', 12);
        $pdf->Text($middle_page - 25, 10, utf8_decode('SUMAS Y SALDOS'));

        $pdf->Text(25, 15, 'CUENTAS');

        $pdf->SetFillColor(204, 204, 204);

        $y = 20;

        //cabecera del detalle
        $pdf->SetFont($FONT,'', 8);
        $pdf->SetXY(10, $y);
        $pdf->Cell(17, 6, utf8_decode('Cuenta'), 1, 0, 'C', true);

        $pdf->SetFont($FONT,'', 8);
        $pdf->SetXY(27, $y);
        $pdf->Cell(63, 6, utf8_decode('Nombre'), 1, 0, 'C', true);

        $pdf->SetFont($FONT,'', 8);
        $pdf->SetXY(112, $y);
        $pdf->Cell(22, 6, utf8_decode('Debe'), 1, 0, 'C', true);

        $pdf->SetFont($FONT,'', 8);
        $pdf->SetXY(134, $y);
        $pdf->Cell(22, 6, utf8_decode('Haber'), 1, 0, 'C', true);

        $pdf->SetFont($FONT,'', 8);
        $pdf->SetXY(156, $y);
        $pdf->Cell(22, 6, utf8_decode('Deudor'), 1, 0, 'C', true);

        $pdf->SetFont($FONT,'', 8);
        $pdf->SetXY(178, $y);
        $pdf->Cell(22, 6, utf8_decode('Acreedor'), 1, 0, 'C', true);
    }

    private function FooterSuma($pdf)
    {
        $FONT = 'Helvetica';

        // Posición: a 1,5 cm del final
        $pdf->SetXY(0, 274);
        // Arial italic 8
        $pdf->SetFont($FONT, '', 8);
        // Número de página
        $pdf->Cell(0, 2, utf8_decode('Página ' . $pdf->PageNo().'/{nb}'), 0, 0, 'C');
    }

    //pdf libro diario

    public function printdiary()
    {
        if ($this->request->is('post')) {

            $this->loadModel('SeatingDetail');

            $seatingsDetailsFilter = $this->SeatingDetail->find()->contain(['Seating','Accounts']);

            $data = $this->request->getData();

            $from = explode('/',$this->request->getData('from'));
            $data['from'] = $from[2] . '-' . $from[1] . '-' . $from[0] . ' 00:00:00';

            $to = explode('/',$this->request->getData('to'));
            $data['to'] = $to[2] . '-' . $to[1] . '-' . $to[0] . ' 23:59:59';

            $seatingsDetailsFilter->where([
                'Seating.created >=' =>  $data['from'],
                'Seating.created <=' =>  $data['to']
            ]);

            if ($this->request->getData('number') != '') {
                $seatingsDetailsFilter->andWhere(['Seating.number' => $this->request->getData('number')]);
            }
            if ($this->request->getData('concept') != '') {
                $seatingsDetailsFilter->andWhere(['Seating.concept LIKE' => '%'.$this->request->getData('concept').'%']);
            }
            if ($this->request->getData('manual') == 'true') {
                $seatingsDetailsFilter->andWhere(['Seating.type' => 'm']);
            }

            if ($this->request->getData('account_code_parent') != '' && $this->request->getData('account_code_child') != '' ) {

                $data['account_code'] = str_pad($this->request->getData('account_code_parent'), 4, "0", STR_PAD_RIGHT);
                $data['account_code'] .= str_pad($this->request->getData('account_code_child'), 5, "0", STR_PAD_LEFT);

                $seatingsDetailsFilter->andWhere(['account_code' => $data['account_code']]);
            }
            if ($this->request->getData('account_name') != '') {
                $seatingsDetailsFilter->andWhere(['Accounts.name LIKE' => '%' . $this->request->getData('account_name') . '%']);
            }

            $is_result = false;

            $this->loadModel('Seating');
            $seatings = $this->Seating->find()->contain(['SeatingDetail.Accounts']);

            foreach ($seatingsDetailsFilter as $sdf) {
                $is_result = true;
                $seatings->orWhere(['Seating.number' => $sdf->seating_number]);
            }

            $seatings->order(['Seating.number' => 'ASC']);

            if (!$is_result) {
                $seatings = null;
            }

            //generate pdf

            $this->response = $this->response->withCharset('UTF-8');
            $this->response = $this->response->withType('application/pdf');

            $FONT = 'Helvetica';

            $pdf = new FPDF('P', 'mm', 'A4');
            $pdf->AliasNbPages();

            $middle_page = $pdf->GetPageWidth() / 2;

            $pdf->SetTitle(utf8_decode('LIBRO DIARIO'));

            $pdf->AddPage();
            $this->HeaderDiary($pdf, $this->request->getData());
            $this->FooterDiary($pdf);

            $y = 20;
            $font_size = 9;
            $max_roe_per_page = 41;
            $count_row = 0;

            $newpage = true;

            if ($seatings) {

                foreach ($seatings as $seating) {

                    if (!$newpage) {
                        $y += 1;
                    }

                    $newpage = false;

                    if ($seating->type == 'm') {
                        $pdf->SetFillColor(137, 233, 255);
                    } else {
                        $pdf->SetFillColor(204, 204, 204);
                    }

                    $pdf->SetFont($FONT,'', $font_size);
                    $pdf->SetXY(10,$y);
                    $pdf->MultiCell(20, 4, utf8_decode($seating->created->format('d/m/Y')), 0, null, 'C', true);

                    $pdf->SetFont($FONT,'', $font_size);
                    $pdf->SetXY(30,$y);
                    $pdf->MultiCell(20, 4, sprintf("%'.05d", $seating->number), 0, null, 'C', true);

                    $pdf->SetFont($FONT, '', $font_size);
                    $pdf->SetXY(45,$y);
                    $pdf->MultiCell(155, 4,  utf8_decode($seating->concept) , 0, null, 'L', true);

                    $count_row++;

                    // if(!$newpage){
                        $y += 5;
                    // }

                    // $newpage = false;

                    foreach ($seating->seating_detail as $sd) {

                        // if($newpage){
                        //     $y -= 3;
                        // }

                        $newpage = false;

                        $pdf->SetFont($FONT,'', $font_size);
                        $pdf->SetXY(10, $y);
                        $pdf->MultiCell(20, 6,  $sd->account_code, null, 'L');

                        $pdf->SetFont($FONT,'', $font_size);
                        $pdf->SetXY(30, $y);
                        $pdf->MultiCell(140, 6, utf8_decode(substr(strtolower($sd->account->name), 0, 85)), null, 'L');

                        $pdf->SetFont($FONT, '', $font_size);
                        $pdf->SetXY($pdf->GetPageWidth() - 50,$y);
                        $pdf->MultiCell(20, 6, number_format($sd->debe, 2, ',', '.'), null, 'R');

                        $pdf->SetFont($FONT,'', $font_size);
                        $pdf->SetXY($pdf->GetPageWidth() - 30,$y);
                        $pdf->MultiCell(20, 6, number_format($sd->haber, 2, ',', '.'), null, 'R');

                        $y += 5;

                        $count_row++;

                        if ($count_row >= $max_roe_per_page) {

                            $newpage = true;

                            $count_row = 0;

                            $pdf->AddPage();
                            $this->HeaderDiary($pdf, $this->request->getData());
                            $this->FooterDiary($pdf);
                            $pdf->SetXY(0, 0);
                            $y = 20;
                        }
                    }

                    // $y += 5;

                    // if($count_row == $max_roe_per_page && !$newpage){

                    //     // $newpage = true;

                    //      $count_row = 0;

                    //     // $pdf->AddPage();
                    //     $this->HeaderDiary($pdf, $this->request->getData());
                    //     $this->FooterDiary($pdf);
                    //     $pdf->SetXY(0,0);
                    //     $y = 20;
                    // }
                }
            }
            
            $pdf->Output('I', 'Libro.pdf');
        }
    }

    private function HeaderDiary($pdf, $data)
    {
        $FONT = 'Helvetica';
    
        $middle_page = $pdf->GetPageWidth() / 2;

        $pdf->SetFont($FONT, '', 12);
        $pdf->Text($middle_page - 22, 10, utf8_decode('LIBRO DIARIO'));

        $pdf->SetFont($FONT, '', 10);
        $pdf->Text(10, 15, utf8_decode('Desde: '));
        $pdf->SetFont($FONT, '', 10);

        $data['from'] = explode("/", $data['from']);
        $data['from'] = $data['from'][2] . '-' . $data['from'][1] . '-' . $data['from'][0] . ' 00:00:00';

        $from = new Time($data['from']);
        $pdf->Text(23, 15, $from->format('d/m/Y'));

        $pdf->SetFont($FONT, '', 10);
        $pdf->Text(45, 15, utf8_decode('Hasta: '));
        $pdf->SetFont($FONT, '', 10);

        $data['to'] = explode("/", $data['to']);
        $data['to'] = $data['to'][2] . '-' . $data['to'][1] . '-' . $data['to'][0] . ' 00:00:00';

        $to = new Time($data['to']);
        $pdf->Text(57, 15, $to->format('d/m/Y'));

        $pdf->SetFillColor(204, 204, 204);

        $y = 20;

        $pdf->Line(10, 18, 200, 18);
    }

    private function FooterDiary($pdf)
    {
        $FONT = 'Helvetica';

        // Posición: a 1,5 cm del final
        $pdf->SetXY(0, 274);
        // Arial italic 8
        $pdf->SetFont($FONT, '', 8);
        // Número de página
        $pdf->Cell(0, 2, utf8_decode('Página ' . $pdf->PageNo().'/{nb}'), 0, 0, 'C');
    }

    public function compAdmin()
    {}

    public function getCompAdminCustomers()
    {
        if ($this->request->is('ajax')) {

            $this->loadModel('Customers');

            $customers = $this->Customers->find()
                ->select(['code', 'account_code', 'name',  'doc_type', 'ident', 'debt_total'])
                ->where(['deleted' => false]);

            $this->loadModel('SeatingDetail');

            $query = $this->SeatingDetail->find();

            $seatingDetailsSumatory =  $query->select([
                'debe' => $query->func()->sum('debe'),
                'haber' => $query->func()->sum('haber'),
                'account_code' => 'account_code'
            ])
            ->group('account_code');

            $customersArray = [];

            foreach ($customers as $customer) {
                $customersArray[$customer->account_code] = $customer;
            }

            $customersdiff = [];

            $epsilon = 0.001;

            foreach ($seatingDetailsSumatory as $sds) {

                if (array_key_exists($sds->account_code, $customersArray)) {

                    $customersArray[$sds->account_code]->saldo_account = $sds->debe - $sds->haber;

                    $diff = abs($customersArray[$sds->account_code]->saldo_account - $customersArray[$sds->account_code]->debt_total); // .001

                    if ($diff > $epsilon) {
                        $customersdiff[] = $customersArray[$sds->account_code];
                    }
                }  
            }

            $this->set(compact('customersdiff'));
            $this->set('_serialize', ['customersdiff']);
        }
    }

    public function delete()
    {
        if ($this->request->is('ajax')) {

            $data = $this->request->input('json_decode');

            $ok = $this->Accountant->deleteSeating($data->number);

            $this->set("ok", $ok);
        }
    }
}
