<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Event\Event;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\Filesystem\Folder;

use App\Controller\Component\ComprobantesComponent;
use App\Controller\Component\AccountantComponent;
use App\Controller\Component\PDFGeneratorComponent;

class ChequePaymentMethodController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Accountant', [
             'className' => '\App\Controller\Component\Admin\Accountant'
        ]);
    }

    public function isAuthorized($user = null) 
    {
        return parent::allowRol($user['id']);
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function index()
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');
        $account_enabled = $this->Accountant->isEnabled();

        if ($this->request->is(['patch', 'post', 'put'])) {

            $payment_getway->config->cheque->enabled = $this->request->getData('enabled');
            $payment_getway->config->cheque->cash = $this->request->getData('cash');
            $payment_getway->config->cheque->portal = $this->request->getData('portal');
            if ($account_enabled) {
                $payment_getway->config->cheque->account = $this->request->getData('account');
            }

            if ($this->request->data['banks'] != '') {

                $banks = explode(',', $this->request->data['banks']);

                $payment_getway->config->cheque->banks = [];

                foreach ($banks as $b) {
                    $payment_getway->config->cheque->banks[] = trim($b);
                }
            }

            //se guarda los cambios
            $this->savePaymentGetwayParaments($payment_getway);
        }

        $this->loadModel('Accounts');
        $accountsTitles = $this->Accounts->find()->where(['title' => true])->order(['code' => 'ASC']);
        $accounts = $this->Accounts->find()
            ->order([
                'code' => 'ASC'
            ]);

        $this->set(compact('accountsTitles', 'accounts', 'payment_getway', 'account_enabled'));
        $this->set('_serialize', ['payment_getway', 'accounts']);
    }
}
