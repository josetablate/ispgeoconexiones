<?php
namespace App\Controller;

use App\Controller\IspControllerMikrotikIpfija;


use App\Controller\MikrotikIpfijaUtils\RowAddressList;
use App\Controller\MikrotikIpfijaUtils\RowAddressListClient;
use App\Controller\MikrotikIpfijaUtils\RowArp;
use App\Controller\MikrotikIpfijaUtils\RowGateway;
use App\Controller\MikrotikIpfijaUtils\RowQueue;


use Cidr;
use Cake\Filesystem\File;
use Cake\I18n\Time;



class IspControllerMikrotikIpfijaTaController extends IspControllerMikrotikIpfija
{
    public function initialize() {
         
        parent::initialize();     
    }    
    
    
    //########CONFIG#########
    
    //controller

    public function edit($id = null) {         
        
        $this->loadModel('Controllers');

        $controller = $this->Controllers->get($id, [
            'contain' => []
        ]);

        $tcontroller = $this->IntegrationRouter->MikrotikIpfijaTa->getControllerTemplate($controller); 

        $controller->local_address = $tcontroller->local_address;
        $controller->dns_server = $tcontroller->dns_server;  
        $controller->queue_default = $tcontroller->queue_default;  
        $controller->arp = $tcontroller->arp;
        

        if ($this->request->is(['patch', 'post', 'put'])) {

            $request_data = $this->request->getData();

            $request_data['queue_default'] = ( array_key_exists('queue_default', $request_data)) ? $request_data['queue_default'] : $tcontroller->queue_default; 

            if ($this->IntegrationRouter->MikrotikIpfijaTa->editController($controller, $request_data)) {

                $controller =  $this->Controllers->patchEntity($controller, $request_data);

                if ($this->Controllers->save($controller)) {
                    $this->Flash->success(__('Los cambios se guardaron correctamente.'));
                } else {
                    $this->_ctrl->Flash->error(__('No se pudo editar el Controlador.'));
                }
            }
        }

        $controller->queue_default = ($controller->queue_default) ? $controller->queue_default : 'default-small' ; 
        $controller->trademark_name = $this->getTrademarksByTemplate($controller->template)[$controller->trademark];
        $controller->template_name = $this->getTemplateName($controller->template);
        $queue_types_array = $this->IntegrationRouter->MikrotikIpfijaTa->getQueueTypesApi($controller);

        $this->set(compact('controller', 'queue_types_array'));
        $this->set('_serialize', ['controller']);
    }
    
    public function config($id) {
        
        $this->loadModel('Controllers');
        $this->loadModel('Services');

        $controller = $this->Controllers->get($id, [
            'contain' => []
        ]);

        $tcontroller = $this->IntegrationRouter->MikrotikIpfijaTa->getControllerTemplate($controller); 

        $controller->local_address = $tcontroller->local_address;
        $controller->dns_server = $tcontroller->dns_server;  
        $controller->queue_default = $tcontroller->queue_default;  

        $controller->queue_default = ($controller->queue_default) ? $controller->queue_default : 'default-small' ; 
         
        $controller->interfaces = $this->IntegrationRouter->MikrotikIpfijaTa->getInterfacesApi($controller);
     
        $queue_types_array = $this->IntegrationRouter->MikrotikIpfijaTa->getQueueTypesApi($controller);

        $services = $this->Services->find()->where(['deleted' => false, 'enabled' => true]);
        $servicesArray = [];
        $servicesArray[''] = '';         
        foreach ($services as $service){
            $servicesArray[$service->id] = $service->name;
        }    

        $this->set(compact('controller', 'queue_types_array', 'servicesArray'));
        $this->set('_serialize', ['controller']);
    }

    public function sync($id) {
        
        $this->loadModel('Controllers');

        $controller = $this->Controllers->get($id);
        
        $this->clearQueues();
        $this->clearAddressLists();
        $this->clearAddressListsClient();
        $this->clearGateways();
        $this->clearArp();

        $this->set(compact('controller'));
        $this->set('_serialize', ['iController']);
    }
     
    //desde shell (control automatico)
    public function checkSync($id){
        
        $this->loadModel('Controllers');
        
        $controller = $this->Controllers->get($id);
        $controller->tcontroller = $this->IntegrationRouter->MikrotikIpfijaTa->getControllerTemplate($controller); 
         
        $paraments = $this->request->getSession()->read('paraments');                 
         
        if($controller->integration && $this->IntegrationRouter->MikrotikIpfijaTa->getStatus($controller)){
             
             $controller->counter_unsync = 0;        
             $controller->counter_unsync += $this->diffQueues($controller, true);
             $controller->counter_unsync += $this->diffAddressListsClient($controller, true);   
             
             if($controller->tcontroller->arp){
                 $controller->counter_unsync += $this->diffArp($controller, true);   
             }
             
             $controller->last_status = true;   
             
        }else{
             $controller->last_status = false;              
        }
        
        $controller->last_control_diff = Time::now();      
        $this->Controllers->save($controller);    
        
        return $controller->counter_unsync;
        
    }
    
    public function generateArpsFromMikrotik(){
        
        // if ($this->request->is('ajax')) {
            
        //     $controller_id = $this->request->input('json_decode')->controller_id;
            
        //     $response = false;
            
        //     $this->loadModel('Controllers');
        //     $controller = $this->Controllers->get($controller_id);
            
        //     $response = $this->IntegrationRouter->MikrotikIpfijaTa->generateArpsFromMikrotik($controller);
            
        //     $this->set('data', $response);
        // }
    }
     
     
    /*sobre acrag de fucntion en IspControllerMikrotik*/
    public function applyConfigAccessNewCustomer(){
        
        if ($this->request->is('ajax')) {
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            
            $response = false;
           
            $paraments = $this->request->getSession()->read('paraments');
            $ip_server = $paraments->system->server->ip;
             
            if($ip_server){
                
                $this->loadModel('Controllers');
                $controller = $this->Controllers->get($controller_id);                
                $response = $this->IntegrationRouter->MikrotikIpfijaTa->applyConfigAccessNewCustomer($controller, $ip_server);       
                
             }
            
            $this->set('data', $response);
        }
    }
    
  
    
    //plans
    
    public function getPlansByController(){

        $controller_id = $this->request->getQuery('controller_id');
        
        $plans = $this->IntegrationRouter->MikrotikIpfijaTa->getPlans($controller_id);
    
        $this->set(compact('plans'));
        $this->set('_serialize', ['plans']);
    }

    public function addPlan(){
        
      if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);

            $response = $this->IntegrationRouter->MikrotikIpfijaTa->addPlan($request_data);
            
            $this->set('response', $response);
        }
    }
     
    public function editPlan() {
        
        if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            $response = $this->IntegrationRouter->MikrotikIpfijaTa->editPlan($request_data);
            
            $this->set('response', $response);
        }
    }

    public function deletePlan() {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $data = $this->request->input('json_decode');
    
            $response = $this->IntegrationRouter->MikrotikIpfijaTa->deletePlan($data->id);

            $this->set('response', $response);
            
        }
    }
    
    
    //pools
    
    public function getPoolsByController(){

        $controller_id = $this->request->getQuery('controller_id');
        
        $pools = $this->IntegrationRouter->MikrotikIpfijaTa->getPools($controller_id);
    
        $this->set(compact('pools'));
        $this->set('_serialize', ['pools']);
    }
    
    public function addPool() {
        
        if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            $response = $this->IntegrationRouter->MikrotikIpfijaTa->addPool($request_data);
            
            $this->set('response', $response);
        }
    }
    
    public function deletePool() {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $data = $this->request->input('json_decode');
            
            $response = $this->IntegrationRouter->MikrotikIpfijaTa->deletePool( $data->id);

            $this->set('response', $response);
        }
    }

    public function editPool(){
        
        if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            $response = $this->IntegrationRouter->MikrotikIpfijaTa->editPool($request_data);
            
            $this->set('response', $response);
        }
    }
    
    public function getRangeNetwork()  {
         
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $addresses = $this->request->input('json_decode')->addresses;
            $range = Cidr::cidrToRange($addresses);
            $this->set('range', $range);
        }
    } 
    
    public function movePool(){
        
        if ($this->request->is('ajax')) {

            $data = $this->request->input('json_decode');
             
            $request_data = json_decode(json_encode($data), true);

            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($request_data['controller_id']);

            $response = $this->IntegrationRouter->MikrotikIpfijaTa->movePool($request_data);

            $this->set('response', $response);
        }
        
    }
    
    
    
    //gateways
    
    public function getGatewaysByController() {

        $controller_id = $this->request->getQuery('controller_id');
        
        $gateways = $this->IntegrationRouter->MikrotikIpfijaTa->getGateways($controller_id);
    
        $this->set(compact('gateways'));
        $this->set('_serialize', ['gateways']);
    }
    
    public function addGateway() {
        
        if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            $response = $this->IntegrationRouter->MikrotikIpfijaTa->addGateway($request_data);
            
            $this->set('response', $response);
        }
    }
    
    public function deleteGateway() {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $data = $this->request->input('json_decode');
            
            $response = $this->IntegrationRouter->MikrotikIpfijaTa->deleteGateway( $data->id);

            $this->set('response', $response);
        }
    }
    
    function editGateway() {
        
        if ($this->request->is('ajax')) {

            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);

            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($request_data['controller_id']);

            $response = $this->IntegrationRouter->MikrotikIpfijaTa->editGateway($controller, $request_data);

            $this->set('response', $response);
        }
    }
    
    
    //profiles
    
    public function getProfilesByController(){

        $controller_id = $this->request->getQuery('controller_id');
        
        $profiles = $this->IntegrationRouter->MikrotikIpfijaTa->getProfiles($controller_id);
    
        $this->set(compact('profiles'));
        $this->set('_serialize', ['profiles']);
    }
    
    public function addProfile(){
        
         if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
                 //$this->log($request_data, 'debug');
            
            $response = $this->IntegrationRouter->MikrotikIpfijaTa->addProfile($request_data);
            
            $this->set('response', $response);
        }

    }

    public function deleteProfile()    {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $data = $this->request->input('json_decode');
            
            $response = $this->IntegrationRouter->MikrotikIpfijaTa->deleteProfile( $data->id);

            $this->set('response', $response);
        }
    }

    public function editProfile()  {
        
         if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            $response = $this->IntegrationRouter->MikrotikIpfijaTa->editProfile($request_data);
            
            $this->set('response', $response);
        }
 
    }
  
    
    //ip excluida    
   
    public function getIpExcludedByController() {

        $controller_id = $this->request->getQuery('controller_id');
 
        $ip_excluded = $this->IntegrationRouter->MikrotikIpfijaTa->getIPExcluded($controller_id);
    
        $this->set(compact('ip_excluded'));
        $this->set('_serialize', ['ip_excluded']);
    }
    
    public function deleteIPExcluded() {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $data = $this->request->input('json_decode');
            
            $response = $this->IntegrationRouter->MikrotikIpfijaTa->deleteIPExcludedById( $data->id);

            $this->set('response', $response);
        }
    }
    
    public function addIPExcluded() {
        
        if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            $response = $this->IntegrationRouter->MikrotikIpfijaTa->addIPExcludedDirect($request_data);
            
            $this->set('response', $response);
        }
       
    } 
     
     
     
    
    
    //########SYNC#########   
    
    
    
     //sync queues

    public function syncQueues()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
      
            $connections_ids = $this->request->input('json_decode')->connections_ids;
            $delete_queue_side_b = $this->request->input('json_decode')->delete_queue_side_b;
            
            $this->loadModel('Connections');
            $success = true;
            
            $fileB = new File(WWW_ROOT . 'sync/mikrotik_ipfija_ta/queuesB.json', false, 0775);
            $json = $fileB->read($fileB, 'r');
            $fileB->close();
            $json =  json_decode($json);
            
            foreach($json->data as $row){
                
                if($row->marked_to_delete){
                    
                    if(!$row->other){ //son registro del controlador, que deben eliminarse,            
                    	
                    	if(!$this->IntegrationRouter->MikrotikIpfijaTa->deleteQueueInController($controller, $row->api_id->value)){
                             $success = false;
                             break;
                        }
                    	
                    }else if($row->other && $delete_queue_side_b){ //son registros agenos al controlador, y esta marcado para limpieza
                    	
                    	if(!$this->IntegrationRouter->MikrotikIpfijaTa->deleteQueueInController($controller, $row->api_id->value)){
                             $success = false;
                             break;
                        }
                    }
                }
            }
   
            
            
            foreach($connections_ids as $connection_id){
                
                $connection = $this->Connections->get($connection_id, ['contain' => ['Controllers', 'Customers']]);
                if(!$this->IntegrationRouter->MikrotikIpfijaTa->syncQueue($connection)){
                    $success = false;
                    break;
                }
            }
            
            $this->diffQueues($controller);

            $this->set('response', $success);
        }
    }
    
    public function syncQueueIndi()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
      
            $connections_ids = $this->request->input('json_decode')->connections_ids;
            
            $this->loadModel('Connections');
            $success = true;
         
            foreach($connections_ids as $connection_id){
                
                $connection = $this->Connections->get($connection_id, ['contain' => ['Controllers', 'Customers']]);
                if(!$this->IntegrationRouter->MikrotikIpfijaTa->syncQueue($connection)){
                    $success = false;
                    break;
                }
            }
            
         
            
            $this->diffQueues($controller);

            $this->set('response', $success);
        }
    }
  
    public function refreshQueues(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $ok = false;
            
            if($this->IntegrationRouter->MikrotikIpfijaTa->getStatus($controller)){
  
                $ok = true;
            }
            
            if($ok){
                $this->diffQueues($controller);
            }
            
            $this->set('data', $ok);
        }
        
    }
  
    public function deleteQueueInController(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
            $queue_api_id = $this->request->input('json_decode')->queue_api_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $success = true;
        
            if(!$this->IntegrationRouter->MikrotikIpfijaTa->deleteQueueInController($controller, $queue_api_id->value)){
                 $success = false;
            }            
            
            $this->diffQueues($controller);
            
            $this->set('data', $success);
        }
        
    }
    
    protected function diffQueues($controller, $file = true){
         
        if($file){
              $this->clearQueues();
        }

        $queuesA = $this->IntegrationRouter->MikrotikIpfijaTa->getQueuesAndConnectionsArray($controller);
        
        $queuesB = $this->IntegrationRouter->MikrotikIpfijaTa->getQueuesInController($controller);
        
        $tableA = [];
        $tableB = [];
        
        $count = 0;
        
        foreach ($queuesA as $queue_a) {
            
            $row = new RowQueue($queue_a, 'A');
             
            //existe en A pero no en B
            if(!array_key_exists($queue_a['name'], $queuesB)){
                $tableA[] = $row;
                $count++;
            }else{
                
                $diff = false;
                
                $queue_b = $queuesB[$queue_a['name']];
                
                if(strtoupper($queue_a['api_id']) != strtoupper($queue_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($queue_a['name'] != $queue_b['name']){
                    $row->name->state = false;
                     $diff = true;
                }
                
                if($queue_a['comment'] != $queue_b['comment']){
                    $row->comment->state = false;
                    $diff = true;
                }
                
                if($queue_a['target'] != $queue_b['target']){
                    $row->target->state = false;
                    $diff = true;
                }
                
            
                if($queue_a['max_limit']  != $queue_b['max_limit']){
                    $row->max_limit->state = false;
                    $diff = true;
                }
                
                 if($queue_a['limit_at']  != $queue_b['limit_at']){
                    $row->limit_at->state = false;
                    $diff = true;
                }
                
                 if($queue_a['burst_limit']  != $queue_b['burst_limit']){
                    $row->burst_limit->state = false;
                    $diff = true;
                }
                
                 if($queue_a['burst_threshold']  != $queue_b['burst_threshold']){
                    $row->burst_threshold->state = false;
                    $diff = true;
                }
                
                 if($queue_a['burst_time']  != $queue_b['burst_time']){
                    $row->burst_time->state = false;
                    $diff = true;
                }
                
                 if($queue_a['priority']  != $queue_b['priority']){
                    $row->priority->state = false;
                    $diff = true;
                }
                
                 if($queue_a['queue']  != $queue_b['queue']){
                    $row->queue->state = false;
                    $diff = true;
                }
                
              
                if( $diff){
                    $tableA[] = $row;
                    $count++;
                }
                
            }
        }
        
        foreach ($queuesB as $queue_b) {
            
            $row = new RowQueue($queue_b, 'B');
            
            if($queue_b['other'] == true){
            	$row->setNew();
            	$tableB[] = $row;
            	continue;
            }
            
            //existe en B pero no en A
            if(!array_key_exists($queue_b['name'], $queuesA)){
                $row->setNew();
                $tableB[] = $row;
                $count++;
                
            }else{
              
                $diff = false;
                
                $queue_a = $queuesA[$queue_b['name']];
            
                if(strtoupper($queue_a['api_id']) != strtoupper($queue_b['api_id'])){
                    $row->setNew();
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($queue_a['name'] != $queue_b['name']){
                    $row->name->state = false;
                     $diff = true;
                }
                
                if($queue_a['comment'] != $queue_b['comment']){
                    $row->comment->state = false;
                    $diff = true;
                }
               
                if($queue_a['target'] != $queue_b['target']){
                    $row->target->state = false;
                    $diff = true;
                }
                
                if($queue_a['max_limit']  != $queue_b['max_limit']){
                    $row->max_limit->state = false;
                    $diff = true;
                }
                
                 if($queue_a['limit_at']  != $queue_b['limit_at']){
                    $row->limit_at->state = false;
                    $diff = true;
                }
                
                 if($queue_a['burst_limit']  != $queue_b['burst_limit']){
                    $row->burst_limit->state = false;
                    $diff = true;
                }
                
                 if($queue_a['burst_threshold']  != $queue_b['burst_threshold']){
                    $row->burst_threshold->state = false;
                    $diff = true;
                }
                
                 if($queue_a['burst_time']  != $queue_b['burst_time']){
                    $row->burst_time->state = false;
                    $diff = true;
                }
                
                 if($queue_a['priority']  != $queue_b['priority']){
                    $row->priority->state = false;
                    $diff = true;
                }
                
                 if($queue_a['queue']  != $queue_b['queue']){
                    $row->queue->state = false;
                    $diff = true;
                }
                
              
                if( $diff){
                    $tableB[] = $row;
                    $count++;
                }
            }
            
        }
        
        if($file){
             
            $this->updateFile($tableA,'queuesA', 'mikrotik_ipfija_ta');
            $this->updateFile($tableB,'queuesB', 'mikrotik_ipfija_ta');
        }
        
        return $count;
    } 
    
    protected function clearQueues(){
        
        $this->updateFile([],'queuesA', 'mikrotik_ipfija_ta');
        $this->updateFile([],'queuesB', 'mikrotik_ipfija_ta');
    } 
    

    
    // sync address list Client

    public function syncAddressListsClient()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $ok = true;
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            $connection_ids = $this->request->input('json_decode')->connection_ids;
            $delete_address_lists_client_side_b = $this->request->input('json_decode')->delete_address_lists_client_side_b;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
                
            $fileB = new File(WWW_ROOT . 'sync/mikrotik_ipfija_ta/addressListsClientB.json', false, 0775);
            $json = $fileB->read($fileB, 'r');
            $json =  json_decode($json);
            
            
            foreach($json->data as $row){
                
                if($row->marked_to_delete){
                    
                    if(!$row->other){ //son registro del controlador, que deben eliminarse,            
                        
                        if(!$this->IntegrationRouter->MikrotikIpfijaTa->deleteAddressListClientInController($controller, $row->api_id->value)){
                            $ok = false;
                            break;
                        }
                    	
                    }else if($row->other && $delete_address_lists_client_side_b){ //son registros agenos al controlador, y esta marcado para limpieza
                    	
                    	if(!$this->IntegrationRouter->MikrotikIpfijaTa->deleteAddressListClientInController($controller, $row->api_id->value)){
                            $ok = false;
                            break;
                        }
                    }
                    
                }
            }
            
            $this->loadModel('Connections');
   
            if($ok){
                
                foreach($connection_ids as $connection_id){
                    
                    $connection = $this->Connections->get($connection_id, ['contain' => ['Controllers', 'Customers']]);
                    if(!$this->IntegrationRouter->MikrotikIpfijaTa->syncAddressListClient($connection)){
                        $ok = false;
                        break;
                    }
                }
            }
            
            
            $this->diffAddressListsClient($controller);

            $this->set('response', $ok);
        }
    }
    
    public function syncAddressListClientIndi()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $ok = true;
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            $connection_ids = $this->request->input('json_decode')->connection_ids;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
            
            $this->loadModel('Connections');
                
            foreach($connection_ids as $connection_id){
                
                $connection = $this->Connections->get($connection_id, ['contain' => ['Controllers', 'Customers']]);
                if(!$this->IntegrationRouter->MikrotikIpfijaTa->syncAddressListClient($connection)){
                    $ok = false;
                    break;
                }
            }
            
            $this->diffAddressListsClient($controller);

            $this->set('response', $ok);
        }
    }
  
    public function refreshAddressListsClient(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $ok = false;
            
            if($this->IntegrationRouter->MikrotikIpfijaTa->getStatus($controller)){
  
                $ok = true;
            }
            
            if($ok){
                $this->diffAddressListsClient($controller);
            }
          
            
            $this->set('data', $ok);
        }
        
    }
 
    public function deleteAddressListClientInController(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
            $address_list_client_api_id = $this->request->input('json_decode')->address_list_client_api_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $success = true;
          
            if(!$this->IntegrationRouter->MikrotikIpfijaTa->deleteAddressListClientInController($controller, $address_list_client_api_id->value)){
                 $success = false;
            }
            
            $this->diffAddressListsClient($controller);
            
            $this->set('data', $success);
        }
        
    }
    
    protected function diffAddressListsClient($controller, $file = true){
        
        if($file){
             $this->clearAddressListsClient();            
        }

        $addressListsA = $this->IntegrationRouter->MikrotikIpfijaTa->getAddressListsClientAndConnectionsArray($controller);
        
        // $this->log($addressListsA, 'debug');
        
        $addressListsB = $this->IntegrationRouter->MikrotikIpfijaTa->getAddressListsClientInController($controller);
        
        $tableA = [];
        $tableB = [];
        
        $count = 0;
        
        foreach ($addressListsA as $address_list_a) {
            
            $row = new RowAddressListClient($address_list_a, 'A');
             
            //existe en A pero no en B
            if(!array_key_exists($address_list_a['list'].':'.$address_list_a['address'], $addressListsB)){
                $tableA[] = $row;
                $count++;
            }else{
                
                $diff = false;
                
                $address_list_b = $addressListsB[$address_list_a['list'].':'.$address_list_a['address']];
                
                if(strtoupper($address_list_a['api_id']) != strtoupper($address_list_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($address_list_a['list'] != $address_list_b['list']){
                    $row->list->state = false;
                     $diff = true;
                }
                
                if($address_list_a['address'] != $address_list_b['address']){
                    $row->address->state = false;
                     $diff = true;
                }
                
                if($address_list_a['comment'] != $address_list_b['comment']){
                    $row->comment->state = false;
                    $diff = true;
                }
              
                if( $diff){
                    $tableA[] = $row;
                    $count++;
                }
                
            }
        }
        
        foreach ($addressListsB as $address_list_b) {
            
            $row = new RowAddressListClient($address_list_b, 'B');
            
            if($address_list_b['other'] == true){
            	$row->setNew();
            	$tableB[] = $row;
            	continue;
            }
            
            //existe en B pero no en A
            if(!array_key_exists($address_list_b['list'].':'.$address_list_b['address'], $addressListsA)){
                $row->setNew();
                $tableB[] = $row;
                $count++;
                
            }else{
                
                $diff = false;
                
                $address_list_a = $addressListsA[$address_list_b['list'].':'.$address_list_b['address']];
            
                if(strtoupper($address_list_a['api_id']) != strtoupper($address_list_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                    $row->setNew();
                }
                
                if($address_list_a['list'] != $address_list_b['list']){
                    $row->list->state = false;
                    $diff = true;
                }
                
                if($address_list_a['address'] != $address_list_b['address']){
                    $row->address->state = false;
                    $diff = true;
                }
                
                if($address_list_a['comment'] != $address_list_b['comment']){
                    $row->comment->state = false;
                     $diff = true;
                }
               
              
                if( $diff){
                    $tableB[] = $row;
                    $count++;
                }
            }
            
        }
        
        if($file){
             
            $this->updateFile($tableA,'addressListsClientA', 'mikrotik_ipfija_ta');
            $this->updateFile($tableB,'addressListsClientB', 'mikrotik_ipfija_ta');
        }
        
        return $count;
        
    } 
    
    protected function clearAddressListsClient() {
        
        $this->updateFile([],'addressListsClientA', 'mikrotik_ipfija_ta');
        $this->updateFile([],'addressListsClientB', 'mikrotik_ipfija_ta');
        
    }
    
    
    
     // sync address list

    public function syncAddressLists()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $ok = true;
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            $address_lists_ids = $this->request->input('json_decode')->address_lists_ids;
            $delete_address_list_side_b = $this->request->input('json_decode')->delete_address_lists_side_b;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
                
            $fileB = new File(WWW_ROOT . 'sync/mikrotik_ipfija_ta/addressListsB.json', false, 0775);
            $json = $fileB->read($fileB, 'r');
            $fileB->close();
            $json =  json_decode($json);
            
            
            foreach($json->data as $row){
                
                if($row->marked_to_delete){
                    
                    if(!$row->other){ //son registro del controlador, que deben eliminarse,            
	                    
	                    if(!$this->IntegrationRouter->MikrotikIpfijaTa->deleteAddressListInController($controller, $row->api_id->value)){
                            $ok = false;
                            break;
                        }
                    	
                    }else if($row->other && $delete_address_list_side_b){ //son registros agenos al controlador, y esta marcado para limpieza
                    	
                    	if(!$this->IntegrationRouter->MikrotikIpfijaTa->deleteAddressListInController($controller, $row->api_id->value)){
                            $ok = false;
                            break;
                        }
                    }
                }
            }
   
   
            if($ok){
                
                foreach($address_lists_ids as $address_list_id){
                    
                    if(!$this->IntegrationRouter->MikrotikIpfijaTa->syncAddressList($address_list_id)){
                        $ok = false;
                        break;
                    }
                }
                
            }
            
            $this->diffAddressLists($controller);

            $this->set('response', $ok);
        }
    }
    
    public function syncAddressListIndi()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $ok = true;
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            $address_lists_ids = $this->request->input('json_decode')->address_lists_ids;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
   
            foreach($address_lists_ids as $address_list_id){
                
                if(!$this->IntegrationRouter->MikrotikIpfijaTa->syncAddressList($address_list_id)){
                    $ok = false;
                    break;
                }
            }            
          
            $this->diffAddressLists($controller);

            $this->set('response', $ok);
        }
    }
  
    public function refreshAddressLists(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            
            $controller_id = $this->request->input('json_decode')->controller_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $ok = false;
            
            if($this->IntegrationRouter->MikrotikIpfijaTa->getStatus($controller)){
  
                $ok = true;
            }
            
            if($ok){
                $this->diffAddressLists($controller);
            }
            
            $this->set('data', $ok);
        }
        
    }
 
    public function deleteAddressListInController(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
            $address_list_api_id = $this->request->input('json_decode')->address_list_api_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $success = true;
          
            if(!$this->IntegrationRouter->MikrotikIpfijaTa->deleteAddressListInController($controller, $address_list_api_id->value)){
                 $success = false;
            }
            
            $this->diffAddressLists($controller);
            
            $this->set('data', $success);
        }
        
    }
    
    protected function diffAddressLists($controller, $file = true){
         
        if($file){
             $this->clearAddressLists();            
        }

        $addressListsA = $this->IntegrationRouter->MikrotikIpfijaTa->getAddressListsArray($controller->id);
        
        $addressListsB = $this->IntegrationRouter->MikrotikIpfijaTa->getAddressListsInController($controller);
        
        $tableA = [];
        $tableB = [];
        
        $count = 0;
        
        foreach ($addressListsA as $address_list_a) {
            
            $row = new RowAddressList($address_list_a, 'A');
             
            //existe en A pero no en B
            if(!array_key_exists($address_list_a['list'].':'.$address_list_a['address'], $addressListsB)){
                $tableA[] = $row;
                $count++;
            }else{
                
                $diff = false;
                
                $address_list_b = $addressListsB[$address_list_a['list'].':'.$address_list_a['address']];
                
                if(strtoupper($address_list_a['api_id']) != strtoupper($address_list_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($address_list_a['list'] != $address_list_b['list']){
                    $row->list->state = false;
                     $diff = true;
                }
                
                if($address_list_a['address'] != $address_list_b['address']){
                    $row->address->state = false;
                     $diff = true;
                }
                
                if($address_list_a['comment'] != $address_list_b['comment']){
                    $row->comment->state = false;
                    $diff = true;
                }
              
                if( $diff){
                    $tableA[] = $row;
                    $count++;
                }
                
            }
        }
        
        foreach ($addressListsB as $address_list_b) {
            
            $row = new RowAddressList($address_list_b, 'B');
            
            if($address_list_b['other'] == true){
            	$row->setNew();
            	$tableB[] = $row;
            	continue;
            }
            
            //existe en B pero no en A
            if(!array_key_exists($address_list_b['list'].':'.$address_list_b['address'], $addressListsA)){
                $row->setNew();
                $tableB[] = $row;
                $count++;
                
            }else{
                
                $diff = false;
                
                $address_list_a = $addressListsA[$address_list_b['list'].':'.$address_list_b['address']];
            
                if(strtoupper($address_list_a['api_id']) != strtoupper($address_list_b['api_id'])){
                    $row->setNew();
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($address_list_a['list'] != $address_list_b['list']){
                    $row->list->state = false;
                    $diff = true;
                }
                
                if($address_list_a['address'] != $address_list_b['address']){
                    $row->address->state = false;
                    $diff = true;
                }
                
                if($address_list_a['comment'] != $address_list_b['comment']){
                    $row->comment->state = false;
                     $diff = true;
                }
               
              
                if( $diff){
                    $tableB[] = $row;
                    $count++;
                }
            }
            
        }
        
        if($file){
             
            $this->updateFile($tableA,'addressListsA', 'mikrotik_ipfija_ta');
            $this->updateFile($tableB,'addressListsB', 'mikrotik_ipfija_ta');
        }
        
        return $count;
    } 
    
    protected function clearAddressLists(){
        
        $this->updateFile([],'addressListsA', 'mikrotik_ipfija_ta');
        $this->updateFile([],'addressListsB', 'mikrotik_ipfija_ta');
    } 
    
    
    
    //sync arp

    public function syncArps()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
      
            $connection_ids = $this->request->input('json_decode')->connection_ids;
            $delete_arp_side_b = $this->request->input('json_decode')->delete_arp_side_b;
            
            $this->loadModel('Connections');
            $success = true;
                
            $fileB = new File(WWW_ROOT . 'sync/mikrotik_ipfija_ta/arpB.json', false, 0644);
            $json = $fileB->read($fileB, 'r');
            $json =  json_decode($json);
            
            foreach($json->data as $row){
                
                if($row->marked_to_delete){
                    
                    
                    if(!$row->other){ //son registro del controlador, que deben eliminarse,            
	                    
	                    if(!$this->IntegrationRouter->MikrotikIpfijaTa->deleteArpInController($controller, $row->api_id->value)){
                            $success = false;
                            break;
                        }
                    	
                    }else if($row->other && $delete_arp_side_b){ //son registros agenos al controlador, y esta marcado para limpieza
                    	
                    	if(!$this->IntegrationRouter->MikrotikIpfijaTa->deleteArpInController($controller, $row->api_id->value)){
                            $success = false;
                            break;
                        }
                    }
                }
            }
            
            if($success){
                
                foreach($connection_ids as $connection_id){
                
                    $connection = $this->Connections->get($connection_id, ['contain' => ['Controllers', 'Customers']]);
                    if(!$this->IntegrationRouter->MikrotikIpfijaTa->syncArp($connection)){
                        $success = false;
                        break;
                    }
                }
            }
            
            $this->diffArp($controller);

            $this->set('data', $success);
        }
    }
    
    public function syncArpIndi()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
      
            $connection_ids = $this->request->input('json_decode')->connection_ids;
         
            $success = true;
            
            $this->loadModel('Connections');
            
            foreach($connection_ids as $connection_id){
               
               $connection = $this->Connections->get($connection_id, ['contain' => ['Controllers', 'Customers']]);
                if(!$this->IntegrationRouter->MikrotikIpfijaTa->syncArp($connection)){
                    $success = false;
                    break;
                }
            }
            
            $this->diffArp($controller);

            $this->set('data', $success);
        }
    }
  
    public function refreshArp(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $ok = false;
            
            if($this->IntegrationRouter->MikrotikIpfijaTa->getStatus($controller)){
  
                $ok = true;
            }
            
            if($ok){
                $this->diffArp($controller);
            }
           
            $this->set('data', $ok);
        }
        
    }
  
    public function deleteArpInController(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            
            // $this->log('deleteArpInController', 'debug');
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            $arp_api_id = $this->request->input('json_decode')->arp_api_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $success = true;
            
            if(!$this->IntegrationRouter->MikrotikIpfijaTa->deleteArpInController($controller, $arp_api_id->value)){
                 $success = false;
            }
            
            $this->diffArp($controller);
            
            $this->set('data', $success);
        }
        
    }
    
    protected function diffArp($controller, $file = true){
        
        if($file){
             $this->clearArp();            
        }

        $arpA = $this->IntegrationRouter->MikrotikIpfijaTa->getArpsAndConnectionsArray($controller);
        
        $arpB = $this->IntegrationRouter->MikrotikIpfijaTa->getArpsInController($controller);
    
        $tableA = [];
        $tableB = [];
        
        $count = 0;
        
        foreach ($arpA as $arp_a) {
            
            $row = new RowArp($arp_a, 'A');
             
            //existe en A pero no en B
            if(!array_key_exists($arp_a['address'], $arpB)){
                $tableA[] = $row;
                $count++;
            }else{
                
                $diff = false;
                
                $arp_b = $arpB[$arp_a['address']];
                
                if(strtoupper($arp_a['api_id']) != strtoupper($arp_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($arp_a['address'] != $arp_b['address']){
                    $row->address->state = false;
                     $diff = true;
                }
                
                if(strtoupper($arp_a['mac_address']) != $arp_b['mac_address']){
                    $row->mac_address->state = false;
                    $diff = true;
                }
                
                if($arp_a['interface'] != $arp_b['interface']){
                    $row->interface->state = false;
                    $diff = true;
                }
            
                if($arp_a['comment']  != $arp_b['comment']){
                    $row->comment->state = false;
                    $diff = true;
                }
                
              
                if( $diff){
                    $tableA[] = $row;
                    $count++;
                }
                
            }
        }
        
        foreach ($arpB as $arp_b) {
            
            $row = new RowArp($arp_b, 'B');
            
            
            if($arp_b['other'] == true){
            	$row->setNew();
            	$tableB[] = $row;
            	continue;
            }
            
            //existe en B pero no en A
            if(!array_key_exists($arp_b['address'], $arpA)){
                $row->setNew();
                $tableB[] = $row;
                $count++;
            }else{
                
                $diff = false;
                
                $arp_a = $arpA[$arp_b['address']];
            
                if(strtoupper($arp_a['api_id']) != strtoupper($arp_b['api_id'])){
                    $row->setNew();
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($arp_a['address'] != $arp_b['address']){
                    $row->address->state = false;
                     $diff = true;
                }
                
                if(strtoupper($arp_a['mac_address']) != $arp_b['mac_address']){
                    $row->mac_address->state = false;
                    $diff = true;
                }
               
                if($arp_a['interface'] != $arp_b['interface']){
                    $row->interface->state = false;
                    $diff = true;
                }
                
                if($arp_a['comment']  != $arp_b['comment']){
                    $row->comment->state = false;
                    $diff = true;
                }
                
              
                if( $diff){
                    $tableB[] = $row;
                    $count++;
                }
            }
            
        }
        
        if($file){
             
            $this->updateFile($tableA,'arpA', 'mikrotik_ipfija_ta');
            $this->updateFile($tableB,'arpB', 'mikrotik_ipfija_ta');
        }
        
        return $count;
        
    } 
    
    protected function clearArp() {
        
        $this->updateFile([],'arpA', 'mikrotik_ipfija_ta');
        $this->updateFile([],'arpB', 'mikrotik_ipfija_ta');
    }
    
    
    
    //gateways
    
    public function syncGateways()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $ok = true;
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            $gateways_ids = $this->request->input('json_decode')->gateways_ids;
            $delete_gateways_side_b = $this->request->input('json_decode')->delete_gateways_side_b;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
                
            $fileB = new File(WWW_ROOT . 'sync/mikrotik_ipfija_ta/gatewaysB.json', false, 0644);
            $json = $fileB->read($fileB, 'r');
            $json =  json_decode($json);
            
           foreach($json->data as $row){
                
                if($row->marked_to_delete){
                    
                    if(!$row->other){ //son registro del controlador, que deben eliminarse,   
                    
                        if(!$this->IntegrationRouter->MikrotikIpfijaTa->deleteGatewayInController($controller, $row->api_id->value)){
                            $ok = false;
                            break;
                        }
                    	
                    }else if($row->other && $delete_gateways_side_b){ //son registros agenos al controlador, y esta marcado para limpieza
                    	
                    	if(!$this->IntegrationRouter->MikrotikIpfijaTa->deleteGatewayInController($controller, $row->api_id->value)){
                            $ok = false;
                            break;
                        }
                    }
                }
            }
                
           foreach($gateways_ids as $gateways_id){

               if(!$this->IntegrationRouter->MikrotikIpfijaTa->syncGateway($gateways_id)){
                   $ok = false;
                   break;
               }
           }
            
            $this->diffGateways($controller);

            $this->set('response', $ok);
        }
    }
     
    public function syncGatewayIndi()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $ok = true;
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            $gateways_ids = $this->request->input('json_decode')->gateways_ids;      
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);       
                
           foreach($gateways_ids as $gateways_id){

               if(!$this->IntegrationRouter->MikrotikIpfijaTa->syncGateway($gateways_id)){
                   $ok = false;
                   break;
               }
           }
            
            $this->diffGateways($controller);

            $this->set('response', $ok);
        }
    }
  
    public function refreshGateways(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $ok = false;
            
            if($this->IntegrationRouter->MikrotikIpfijaTa->getStatus($controller)){
  
                $ok = true;
            }
            
            if($ok){
                $this->diffGateways($controller);
            }
            
            $this->set('data', $ok);
        }
        
    }
 
    public function deleteGatewayInController(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
            $gateway_api_id = $this->request->input('json_decode')->gateway_api_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $success = true;
          
            if(!$this->IntegrationRouter->MikrotikIpfijaTa->deleteGatewayInController($controller, $gateway_api_id->value)){
                 $success = false;
            }
            
            $this->diffGateways($controller);
            
            $this->set('data', true);
        }
        
    }
    
    protected function diffGateways($controller){
         
        $this->clearGateways();

        $gatewayA = $this->IntegrationRouter->MikrotikIpfijaTa->getGatewaysArray($controller->id);
        
        $gatewayB = $this->IntegrationRouter->MikrotikIpfijaTa->getGatewaysInController($controller);
        
        $tableA = [];
        $tableB = [];
        
        foreach ($gatewayA as $gateway_a) {
            
            $row = new RowGateway($gateway_a, 'A');
             
            //existe en A pero no en B
            if(!array_key_exists($gateway_a['address'], $gatewayB)){
                $tableA[] = $row;
            }else{
                
                $diff = false;
                
                $gateway_b = $gatewayB[$gateway_a['address']];
                
                if(strtoupper($gateway_a['api_id']) != strtoupper($gateway_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($gateway_a['address'] != $gateway_b['address']){
                    $row->address->state = false;
                     $diff = true;
                }
                
                if($gateway_a['interface'] != $gateway_b['interface']){
                    $row->interface->state = false;
                     $diff = true;
                }
                
                if($gateway_a['comment'] != $gateway_b['comment']){
                    $row->comment->state = false;
                     $diff = true;
                }
              
                if( $diff){
                    $tableA[] = $row;
                }
                
            }
        }
        
        foreach ($gatewayB as $gateway_b) {
            
            $row = new RowGateway($gateway_b, 'B');
            
            if($gateway_b['other'] == true){
            	$row->setNew();
            	$tableB[] = $row;
            	continue;
            }
            
            //existe en B pero no en A
            if(!array_key_exists($gateway_b['address'], $gatewayA)){
                $row->setNew();
                $tableB[] = $row;
                
            }else{
                
                $diff = false;
                
                $gateway_a = $gatewayA[$gateway_b['address']];
            
                if(strtoupper($gateway_a['api_id']) != strtoupper($gateway_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                    $row->marked_to_delete = true;
                }
                
                if($gateway_a['address'] != $gateway_b['address']){
                    $row->address->state = false;
                    $diff = true;
                }
                
                if($gateway_a['interface'] != $gateway_b['interface']){
                    $row->interface->state = false;
                     $diff = true;
                }
                
                if($gateway_a['comment'] != $gateway_b['comment']){
                    $row->comment->state = false;
                     $diff = true;
                }
              
                if( $diff){
                    $tableB[] = $row;
                }
            }
            
        }
        
        $this->updateFile($tableA,'gatewaysA', 'mikrotik_ipfija_ta');
        $this->updateFile($tableB,'gatewaysB', 'mikrotik_ipfija_ta');
   
    } 
    
    protected function clearGateways(){
        
        $this->updateFile([],'gatewaysA', 'mikrotik_ipfija_ta');
        $this->updateFile([],'gatewaysB', 'mikrotik_ipfija_ta');
    } 
    
    
    
    
    
    
    
   
    
   
    
}







