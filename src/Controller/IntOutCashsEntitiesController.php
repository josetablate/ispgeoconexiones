<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use FPDF;
use App\Controller\Component\Admin\CashRegister;

/**
 * IntOutCashsEntities Controller
 *
 * @property \App\Model\Table\IntOutCashsEntitiesTable $IntOutCashsEntities
 *
 * @method \App\Model\Entity\IntOutCashsEntity[] paginate($object = null, array $settings = [])
 */
class IntOutCashsEntitiesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('CashRegister', [
             'className' => '\App\Controller\Component\Admin\CashRegister'
        ]);
    }

    public function isAuthorized($user = null)
    {
        if ($this->request->getParam('action') == 'getAllByCashEntityContado') {
            return true;
        }

        if ($this->request->getParam('action') == 'getAllByCashEntityOther') {
            return true;
        }

        if ($this->request->getParam('action') == 'getAllByNumberContado') {
            return true;
        }

        if ($this->request->getParam('action') == 'getAllByNumberOther') {
            return true;
        }

        if ($this->request->getParam('action') == 'getAllParts') {
            return true;
        }

        if ($this->request->getParam('action') == 'addInOutCashContado') {
            return true;
        }

        if ($this->request->getParam('action') == 'addInOutCashOther') {
            return true;
        }

        if ($this->request->getParam('action') == 'HeaderCustom') {
            return true;
        }

        if ($this->request->getParam('action') == 'FooterCustom') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function viewParte($cash_entity_id, $number)
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');
        $payment_methods = [
            '' => 'Seleccione'
        ];
        foreach ($payment_getway->methods as $payment_method) {
            $payment_methods[$payment_method->name] = $payment_method->name;
        }
        $cashEntity = $this->CashEntities->get($cash_entity_id, ['contain' => ['Users']]);

        $this->set('payment_methods', $payment_methods);
        $this->set('cashEntity', $cashEntity);
        $this->set('number', $number);
    }

    public function getAllByNumberContado()
    {
        $number = $this->request->getQuery('number');

        $apertura = $this->IntOutCashsEntities
            ->find()
            ->where(['number_part' => $number, 'type' => 'APERTURA'])
            ->first();

        $cierre = $this->IntOutCashsEntities
            ->find()
            ->where(['number_part' => $number, 'type' => 'CIERRE'])
            ->first();

        if (!$apertura) {
            $this->Flash->warning(__('No se Encuentra el Parte.{0}', $number));
            return $this->redirect(['action' => 'index']);
        }

        $this->loadModel('CashEntities');
        $this->loadModel('Customers');

        $cashEntity = $this->CashEntities->get($apertura->cash_entity_id, ['contain' => ['Users']]);

        if (!$cashEntity) {
            $this->Flash->warning(__('La caja no existe.'));
            return $this->redirect(['action' => 'index']);
        }

        if (!$cashEntity->enabled) {
            $this->Flash->warning(__('La caja está deshabilitada.'));
            return $this->redirect(['action' => 'index']);
        }

        $movements = [];

        if ($cierre) {

             $movements = $this->IntOutCashsEntities->find()
                ->order(['id' => 'desc'])
                ->where([
                    'cash_entity_id' => $cashEntity->id,
                    'id >=' => $apertura->id, 'id <=' => $cierre->id,
                     'OR' => [['is_table' => 1] , ['is_table is' => null]]
                    ]);

        } else {

             $movements = $this->IntOutCashsEntities->find()
                ->order(['id' => 'desc'])
                ->where([
                    'cash_entity_id' => $cashEntity->id,
                    'id >=' => $apertura->id,
                     'OR' => [['is_table' => 1] , ['is_table is' => null]]
                    ]);

        }

        foreach ($movements as $movement) {
            $movement->customer_name = "";
            if ($movement->data) {
                $customer = $this->Customers
                    ->find()
                    ->select(['name'])
                    ->where(['code' => $movement->data])
                    ->first();
                $movement->customer_name = $customer->name;
            }
        }

        $this->set('movements', $movements);
    }

    public function getAllByNumberOther()
    {
        $number = $this->request->getQuery('number');

        $apertura = $this->IntOutCashsEntities
            ->find()
            ->where(['number_part' => $number, 'type' => 'APERTURA'])
            ->first();

        $cierre = $this->IntOutCashsEntities
            ->find()
            ->where(['number_part' => $number, 'type' => 'CIERRE'])
            ->first();

        if (!$apertura) {
            $this->Flash->warning(__('No se Encuentra el Parte.{0}', $number));
            return $this->redirect(['action' => 'index']);
        }

        $this->loadModel('CashEntities');
        $this->loadModel('Customers');

        $cashEntity = $this->CashEntities->get($apertura->cash_entity_id, ['contain' => ['Users']]);

        if (!$cashEntity) {
            $this->Flash->warning(__('La caja no existe.'));
            return $this->redirect(['action' => 'index']);
        }

        if (!$cashEntity->enabled) {
            $this->Flash->warning(__('La caja está deshabilitada.'));
            return $this->redirect(['action' => 'index']);
        }

        $movements = [];

        if ($cierre) {

             $movements = $this->IntOutCashsEntities->find()
                ->order(['id' => 'desc'])
                ->where([
                    'cash_entity_id' => $cashEntity->id,
                    'id >=' => $apertura->id, 'id <=' => $cierre->id,
                     'OR' => [['is_table !=' => 1] , ['is_table is' => null]]
                    ]);

        } else {

             $movements = $this->IntOutCashsEntities->find()
                ->order(['id' => 'desc'])
                ->where([
                    'cash_entity_id' => $cashEntity->id,
                    'id >=' => $apertura->id,
                     'OR' => [['is_table !=' => 1] , ['is_table is' => null]]
                    ]);

        }

        foreach ($movements as $movement) {
            $movement->customer_name = "";
            if ($movement->data) {
                $customer = $this->Customers
                    ->find()
                    ->select(['name'])
                    ->where(['code' => $movement->data])
                    ->first();
                $movement->customer_name = $customer->name;
            }
        }

        $this->set('movements', $movements);
    }

    public function printpart($number_part)
    {
        $apertura = $this->IntOutCashsEntities->find()
            ->where(['number_part' => $number_part, 'type' => 'APERTURA'])
            ->first();

        $cierre = $this->IntOutCashsEntities->find()
            ->where(['number_part' => $number_part, 'type' => 'CIERRE'])
            ->first();

        if (!$apertura) {
            $this->Flash->warning(__('No se Encuentra el Parte.'));
            return $this->redirect($this->referer());
        }

        $cashEntity = $this->CashEntities->get($apertura->cash_entity_id, ['contain' => ['Users']]);

        if (!$cashEntity) {
            $this->Flash->warning(__('La caja no existe.'));
            return $this->redirect($this->referer());
        }

        if (!$cashEntity->enabled) {
            $this->Flash->warning(__('La caja está deshabilitada.'));
            return $this->redirect($this->referer());
        }

        $movements = null;

        if ($cierre) {

             $movements = $this->IntOutCashsEntities->find()
            ->order(['id' => 'asc'])
            ->where(['cash_entity_id' => $cashEntity->id, 'id >=' => $apertura->id, 'id <=' => $cierre->id]);
                
        } else {

             $movements = $this->IntOutCashsEntities->find()
            ->order(['id' => 'asc'])
            ->where(['cash_entity_id' => $cashEntity->id, 'id >=' => $apertura->id]);
        }

        //generate pdf

        $this->response->charset('UTF-8');
        $this->response->type('application/pdf');

        $FONT = 'Helvetica';

        $pdf = new FPDF('P','mm','A4');
        $pdf->AliasNbPages();

        $middle_page = $pdf->GetPageWidth() / 2;

        $pdf->SetTitle(utf8_decode('Parte N° ' . $number_part));

        $pdf->AddPage();

        $this->HeaderCustom($pdf, $number_part, $cashEntity , $apertura);
        $this->FooterCustom($pdf);

        $y = 32;

        $font_size = 9;

        $max_roe_per_page = 26;
        $count_row = 0;

        foreach ($movements as $movement) {

            $pdf->SetFont($FONT, '', $font_size);
            $pdf->SetXY(10, $y);
            $pdf->MultiCell(20, 6, utf8_decode($movement->created->format('d/m/Y')), null, 'C');

            if ($movement->seating_number) {
                $pdf->SetFont($FONT, '', $font_size);
                $pdf->SetXY(30, $y);
                $pdf->MultiCell(20, 6, sprintf("%'.05d", $movement->seating_number), null, 'C');
            }

            if ($movement->data) {
                $pdf->SetFont($FONT, '', $font_size);
                $pdf->SetXY(50, $y);
                $pdf->MultiCell(20, 6, sprintf("%'.05d", $movement->data), null, 'C');
            }

            $h = 6;
            if (strlen($movement->concept) > 47) {
                $h = 4;
            }

            $pdf->SetFont($FONT, '', $font_size);
            $pdf->SetXY(70,$y);
            $pdf->MultiCell(70, $h, utf8_decode($movement->concept), null, 'L');

            $pdf->SetFont($FONT, '', $font_size);
            $pdf->SetXY(140, $y);
            $pdf->MultiCell(20, 6, number_format($movement->in_value, 2, ',', '.'), null, 'R');

            $pdf->SetFont($FONT, '', $font_size);
            $pdf->SetXY(160, $y);
            $pdf->MultiCell(20, 6, number_format($movement->out_value, 2, ',', '.'), null, 'R');

            $pdf->SetFont($FONT,'', $font_size);
            $pdf->SetXY(180, $y);
            $pdf->MultiCell(20, 6, number_format($movement->saldo_contado, 2, ',', '.'), null, 'R');

            $y += 9;

            $count_row++;

            if ($count_row % $max_roe_per_page == 0) {

                $pdf->AddPage();
                $this->HeaderCustom($pdf, $number_part, $cashEntity, $apertura);
                $pdf->SetXY(0, 0);
                $y = 32;
            }
        }

        $pdf->Output();
    }

    private function HeaderCustom($pdf, $number_part, $cashEntity, $apertura)
    {
        $FONT = 'Helvetica';

        //encabezado
        $pdf->SetFont($FONT, 'B', 12);
        $pdf->Text(10, 20, utf8_decode('Parte N° '));
        $pdf->SetFont($FONT, '', 12);
        $pdf->Text(28, 20, $number_part);

        $pdf->SetFont($FONT, 'B', 12);
        $pdf->Text(52, 20,  utf8_decode('CTA N°: '));
        $pdf->SetFont($FONT, '', 12);
        $pdf->Text(70, 20, $cashEntity->account_code);

        $pdf->SetFont($FONT, 'B', 12);
        $pdf->Text(93, 20, utf8_decode('CAJA: '));
        $pdf->SetFont($FONT, '', 12);
        $pdf->Text(108, 20, utf8_decode($cashEntity->name) );

        $pdf->SetFont($FONT, 'B', 12);
        $pdf->Text(152, 20, utf8_decode('APERTURA: '));
        $pdf->SetFont($FONT, '', 12);
        $pdf->Text(179, 20, $apertura->created->format('d/m/Y'));
        $pdf->Line(10, 22, $pdf->GetPageWidth() - 10, 22);

        $pdf->SetFillColor(204, 204, 204);

        $y = 25;

        //cabecera del detalle
        $pdf->SetFont($FONT, '', 8);
        $pdf->SetXY(10, $y);
        $pdf->Cell(20, 6, utf8_decode('Fecha'), 1, 0, 'C', true);

        $pdf->SetFont($FONT, '', 8);
        $pdf->SetXY(30, $y);
        $pdf->Cell(20, 6, utf8_decode('N° Asiento'), 1, 0, 'C', true);

        $pdf->SetFont($FONT, '', 8);
        $pdf->SetXY(50, $y);
        $pdf->Cell(20, 6, utf8_decode('Cliente'), 1, 0, 'C', true);

        $pdf->SetFont($FONT,'', 8);
        $pdf->SetXY(70, $y);
        $pdf->Cell(70, 6, utf8_decode('Concepto.'), 1, 0, 'C', true);

        $pdf->SetFont($FONT, '', 8);
        $pdf->SetXY(140, $y);
        $pdf->Cell(20, 6, utf8_decode('Ingreso.'), 1, 0, 'C', true);

        $pdf->SetFont($FONT, '', 8);
        $pdf->SetXY(160, $y);
        $pdf->Cell(20, 6, utf8_decode('Egreso'), 1, 0, 'C', true);

        $pdf->SetFont($FONT,'', 8);
        $pdf->SetXY(180, $y);
        $pdf->Cell(20, 6, utf8_decode('Saldo'), 1, 0, 'C', true);
    }

    // Pie de página
    private function FooterCustom($pdf)
    {
        $FONT = 'Helvetica';

        // Posición: a 1,5 cm del final
        $pdf->SetXY(0, 274);
        // Arial italic 8
        $pdf->SetFont($FONT, '', 8);
        // Número de página
        $pdf->Cell(0, 2, utf8_decode('Página ' . $pdf->PageNo() . '/{nb}'), 0, 0, 'C');
    }

    public function getAllByCashEntityContado()
    {
        $this->loadModel('Customers');
        $cash_entity_id = $this->request->getQuery('cash_entity_id');

        $lastApertura = $this->IntOutCashsEntities
            ->find()
            ->order(['IntOutCashsEntities.id' => 'desc'])
            ->where(['cash_entity_id' => $cash_entity_id, 'type' => 'APERTURA'])
            ->first();

        $movements = [];

        if ($lastApertura) {

            $movements = $this->IntOutCashsEntities
                ->find()
                ->order(['id' => 'desc'])
                ->where([
                    'cash_entity_id' => $cash_entity_id,
                    'id >=' => $lastApertura->id, 
                    'OR' => [['is_table' => 1] , ['is_table is' => null]]
                ]);
        } else {

            $movements = $this->IntOutCashsEntities
                ->find()
                ->order(['id' => 'desc'])
                ->where([
                    'cash_entity_id' => $cash_entity_id,
                    'OR' => [['is_table' => 1] , ['is_table is' => null]]
                ]);
        }

        foreach ($movements as $movement) {
            $movement->customer_name = "";
            if ($movement->data) {
                $customer = $this->Customers
                    ->find()
                    ->select(['name'])
                    ->where(['code' => $movement->data])
                    ->first();
                $movement->customer_name = $customer->name;
            }
        }

        $this->set('movements', $movements);
    }

    public function getAllByCashEntityOther()
    {
        $this->loadModel('Customers');
        $cash_entity_id = $this->request->getQuery('cash_entity_id');

        $lastApertura = $this->IntOutCashsEntities
            ->find()
            ->order(['IntOutCashsEntities.id' => 'desc'])
            ->where([
                'cash_entity_id' => $cash_entity_id,
                'type' => 'APERTURA'
            ])
            ->first();

        $movements = [];

        if ($lastApertura) {

            $movements = $this->IntOutCashsEntities
                ->find()
                ->order(['id' => 'desc'])
                ->where([
                    'cash_entity_id' => $cash_entity_id,
                    'id >=' => $lastApertura->id ,
                    'OR' => [['is_table !=' => 1] , ['is_table is' => null]]
                ]);

        } else {

            $movements = $this->IntOutCashsEntities
                ->find()
                ->order(['id' => 'desc'])
                ->where([
                    'cash_entity_id' => $cash_entity_id,
                    'OR' => [['is_table !=' => 1] , ['is_table is' => null]]
                ]);

            //$this->log($movements, 'debug');
        }

        foreach ($movements as $movement) {
            $movement->customer_name = "";
            if ($movement->data) {
                $customer = $this->Customers
                    ->find()
                    ->select(['name'])
                    ->where(['code' => $movement->data])
                    ->first();
                $movement->customer_name = $customer->name;
            }
        }

        $this->set('movements', $movements);
    }

    public function addInOutCashContado()
    {
        if ($this->request->is('ajax')) {

            $success = true;

            $request_data = $this->request->input('json_decode');

            $user_id = $this->Auth->user()['id'];

            $this->loadModel('CashEntities'); 

            $cash_entity = $this->CashEntities->find()->where(['user_id' => $user_id, 'deleted' => false])->first();

            $data = new \stdClass;
            $data->cash_entity = $cash_entity;

            $data->created = Time::now();
            $data->type = $request_data->type;
            $data->concept = $request_data->concept;
            $data->in_value = abs($request_data->in_value);
            $data->out_value = abs($request_data->out_value);
            $data->data = null;
            $data->seating_number = null;
            $data->is_table = 1;
            $data->payment_method_id = null;

            if ($data->in_value > 0 ) {

                $data->cash_entity->contado += $data->in_value;

                if ($success && !$this->CashRegister->register($data, CashRegister::TYPE_IN)) {
                    $success = false;
                }

                if ($success && !$this->CashEntities->save($cash_entity)) {
                    $success = false;
                }

            } else if ($data->out_value > 0 ) {

                $data->cash_entity->contado -= $data->out_value;

                if ($success && !$this->CashRegister->register($data, CashRegister::TYPE_OUT)) {
                    $success = false;
                }

                if ($success && !$this->CashEntities->save($cash_entity)) {
                    $success = false;
                }
            }

            if ($success) {
                $this->set('cashEntity', $cash_entity);
            } else {
                $this->set('cashEntity', $success);
            }
        }
    }

    public function addInOutCashOther()
    {
        if ($this->request->is('ajax')) {

            $success = true;

            $request_data = $this->request->input('json_decode');

            $user_id = $this->Auth->user()['id'];

            $this->loadModel('CashEntities'); 

            $cash_entity = $this->CashEntities->find()->where(['user_id' => $user_id, 'deleted' => false])->first();

            $data = new \stdClass;
            $data->cash_entity = $cash_entity;

            $data->created = Time::now();
            $data->type = $request_data->type;
            $data->concept = $request_data->concept;
            $data->in_value = abs($request_data->in_value);
            $data->out_value = abs($request_data->out_value);
            $data->data = null;
            $data->seating_number = null;
            $data->is_table = 2;
            $data->payment_method_id = null;

            if ($data->in_value > 0 ) {

                $data->cash_entity->cash_other += $data->in_value;

                if ($success && !$this->CashRegister->register($data, CashRegister::TYPE_IN)) {
                    $success = false;
                }

                if ($success && !$this->CashEntities->save($cash_entity)) {
                    $success = false;
                }

            } else if ($data->out_value > 0 ) {

                $data->cash_entity->cash_other -= $data->out_value;

                if ($success && !$this->CashRegister->register($data, CashRegister::TYPE_OUT)) {
                    $success = false;
                }

                if ($success && !$this->CashEntities->save($cash_entity)) {
                    $success = false;
                }
            }

            if ($success) {
                $this->set('cashEntity', $cash_entity);
            } else {
                $this->set('cashEntity', $success);
            }
        }
    }

    public function getAllParts()
    {
        $cash_entity_id = $this->request->getQuery('cash_entity_id');

        $apertures = $this->IntOutCashsEntities->find()
            ->order(['id' => 'desc'])
            ->where(['cash_entity_id' => $cash_entity_id, 'type' => 'APERTURA']);

        $this->set('apertures', $apertures);
    }
}
