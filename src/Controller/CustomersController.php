<?php
namespace App\Controller;

use App\Controller\AppController;
use PEAR2\Net\RouterOS;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Event\EventManager;

use Cidr;
use Cake\Datasource\ConnectionManager;
use PEAR2\Net\RouterOS\SocketException;
use PEAR2\Net\RouterOS\DataFlowException;
use Cake\I18n\Time;
use App\Controller\Component\ComprobantesComponent;
use App\Controller\Component\CashRegisterComponent;
use App\Controller\Component\PDFGeneratorComponent;
use App\Controller\Component\Admin\FiscoAfipComp;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

use Cake\Console\ShellDispatcher;

/**
 * Customers Controller
 *
 * @property \App\Model\Table\CustomersTable $Customers
 */
class CustomersController extends AppController
{
    public static $TECHNICAL_SUPPORT_CATEGORY = 0;
    public static $INSTALLATIONS_CATEGORY = 1;
    public static $INVOICING_CATEGORY = 2;
    public static $SALE_CATEGORY = 3;

    public static $TICKETS_CATEGORIES = [
        '' => 'Seleccione Categoría',
        0 => 'Soporte Técnico',
        1 => 'Instalación',
        2 => 'Facturación',
        3 => 'Venta',
    ];

    public static $AVAILABLE_STATUS = 0;
    public static $ASSIGNED_STATUS = 1;
    public static $FINISHED_STATUS = 2;

    public static $TICKETS_STATUS = [
        0 => 'Disponible',
        1 => 'Asignado',
        2 => 'Finalizado',
    ];

    public static $TICKETS_STATUS_COLORS = [
        0 => 'warning',
        1 => 'danger',
        2 => 'success',
    ];

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Accountant', [
             'className' => '\App\Controller\Component\Admin\Accountant'
        ]);

        $this->loadComponent('CurrentAccount', [
            'className' => '\App\Controller\Component\Admin\CurrentAccount'
        ]);

        $this->loadComponent('FiscoAfipComp', [
            'className' => '\App\Controller\Component\Admin\FiscoAfipComp'
        ]);

        $this->loadComponent('FiscoAfipCompPdf', [
             'className' => '\App\Controller\Component\Admin\FiscoAfipCompPdf'
        ]);

        $this->loadComponent('IntegrationRouter', [
             'className' => '\App\Controller\Component\Integrations\IntegrationRouter'
        ]);

        $this->loadComponent('FiscoAfip', [
            'className' => '\App\Controller\Component\Admin\FiscoAfip'
        ]);

        $this->initEventManager();
    }

    public function initEventManager()
    {
        EventManager::instance()->on(
            'Customers.Error',
            function ($event, $msg, $data, $flash = false) {

                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $user_id = null;

                if (!isset($this->_ctrl->request->getSession()->read('Auth.User')['id'])) {
                    $user_id = 100;
                } else {
                    $user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                }

                $log->user_id = $user_id;

                $log->type = 'Error';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);

                if ($flash) {
                    $this->_ctrl->Flash->error(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'Customers.Warning',
            function ($event, $msg, $data, $flash = false) {
    
                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();

                $user_id = null;

                if (!isset($this->_ctrl->request->getSession()->read('Auth.User')['id'])) {
                    $user_id = 100;
                } else {
                    $user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                }

                $log->user_id = $user_id;
                $log->type = 'Warning';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);

                if ($flash) {
                    $this->_ctrl->Flash->warning(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'Customers.Log',
            function ($event, $msg, $data) {

            }
        );

        EventManager::instance()->on(
            'Customers.Notice',
            function ($event, $msg, $data) {

            }
        );
    }

    public function isAuthorized($user = null) 
    {
        if ($this->request->getParam('action') == 'geCustomerAjax') {
            return true;
        }

        if ($this->request->getParam('action') == 'getCustomers') {
            return true;
        }

        if ($this->request->getParam('action') == 'getCustomersSimple') {
            return true;
        }

        if ($this->request->getParam('action') == 'getCustomersDeleted') {
            return true;
        }

        if ($this->request->getParam('action') == 'verifyExistOnCobrodigital') {
            return true;
        }

        if ($this->request->getParam('action') == 'freedCard') {
            return true;
        }

        if ($this->request->getParam('action') == 'editCustomerFromDebt') {
               return true;
        }

        if ($this->request->getParam('action') == 'changeLabels') {
            return true;
        }

        if ($this->request->getParam('action') == 'getMoves') {
            return true;
        }

        if ($this->request->getParam('action') == 'getWithoutInvoiceCustomer') {
            return true;
        }

        if ($this->request->getParam('action') == 'updateDebt') {
            return true;
        }

        if ($this->request->getParam('action') == 'importNetsion') {
            return true;
        }
        
        if ($this->request->getParam('action') == 'importAndros') {
            return true;
        }

        if ($this->request->getParam('action') == 'importEditCobroDigital') {
            return true;
        }
        
        if ($this->request->getParam('action') == 'importEditSaldo') {
            return true;
        }

        if ($this->request->getParam('action') == 'getSourceAdministrativeMovementFromAjax') {
            return true;
        }

        if ($this->request->getParam('action') == 'editAdministrativeMovement') {
            return true;
        }

        if ($this->request->getParam('action') == 'getDebtWithInvoice') {
            return true;
        }

        if ($this->request->getParam('action') == 'getMovesAll') {
            return true;
        }

        if ($this->request->getParam('action') == 'getPresupuestos') {
            return true;
        }
        
        if ($this->request->getParam('action') == 'applyMassiveDebt') {
            return true;
        }

        if ($this->request->getParam('action') == 'applyMassiveDiscount') {
            return true;
        }

        if ($this->request->getParam('action') == 'changeClave') {
            return true;
        }

        if ($this->request->getParam('action') == 'forgotClave') {
            return true;
        }

        if ($this->request->getParam('action') == 'cdRequestCard') {
            return true;
        }

        if ($this->request->getParam('action') == 'ServerSideMoves') {
            return true;
        }

        if ($this->request->getParam('action') == 'ServerSideInvoicesMoves') {
            return true;
        }

        if ($this->request->getParam('action') == 'ServerSidePaymentsMoves') {
            return true;
        }

        if ($this->request->getParam('action') == 'getDebtsWithoutInvoices') {
            return true;
        }

        if ($this->request->getParam('action') == 'editConnectionFromMovement') {
            return true;
        }

        if ($this->request->getParam('action') == 'getBusinessCube') {
            return true;
        }

        if ($this->request->getParam('action') == 'getTaxtypeCube') {
            return true;
        }

        if ($this->request->getParam('action') == 'syncConnections') {
            return true;
        }

        if ($this->request->getParam('action') == 'randomPassword') {
            return true;
        }

        if ($this->request->getParam('action') == 'ServerSideCustomersDiscountsMoves') {
            return true;
        }

        if ($this->request->getParam('action') == 'ServerSideDebtsMoves') {
            return true;
        }

        if ($this->request->getParam('action') == 'ServerSideCreditsMovesAll') {
            return true;
        }

        if ($this->request->getParam('action') == 'ServerSideDebitsMovesAll') {
            return true;
        }

        if ($this->request->getParam('action') == 'ServerSideDebitsMoves') {
            return true;
        }

		if ($this->request->getParam('action') == 'ServerSideCreditsMoves') {
            return true;
        }

		if ($this->request->getParam('action') == 'ServerSideInvoicesMovesAll') {
            return true;
        }

		if ($this->request->getParam('action') == 'ServerSidePaymentsMovesAll') {
            return true;
        }
        
        if ($this->request->getParam('action') == 'editMasive') {
            return true;
        }
        
        if ($this->request->getParam('action') == 'uploadFileEditMasive') {
            return true;
        }
        

        return parent::allowRol($user['id']);
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['changeClave', 'forgotClave', 'cdRequestCard']);
    }

    public function index()
    {
        $paraments = $this->request->getSession()->read('paraments');
        $payment_getway = $this->request->getSession()->read('payment_getway');

        $areas = [];
        $this->loadModel('Areas');
        $areas_model = $this->Areas->find();
        foreach ($areas_model as $a) {
            $areas[$a->id] = $a->name;
        }

        $services = [];
        $this->loadModel('Services');
        $services_model = $this->Services->find();
        foreach ($services_model as $a) {
            $services[$a->id] = $a->name;
        }

        $controllers = [];
        $this->loadModel('Controllers');
        $controllers_model = $this->Controllers
            ->find()->where([
        		'enabled' => TRUE,
        		'deleted' => FALSE
        	]);
        foreach ($controllers_model as $c) {
            $controllers[$c->id] = $c->name;
        }

        $users = [];
        $this->loadModel('Users');
        $users_model = $this->Users->find();
        foreach ($users_model as $user) {
            $users[$user->id] = $user->name;
        }

        $accounts = [];
        foreach ($payment_getway->methods as $pg) {
            $config = $pg->config;
            if ($payment_getway->config->$config->enabled && $pg->credential) {
                $accounts[$pg->id] = $pg->name;
            }
        }

        if (sizeof($accounts) == 0) {
            $accounts[""] = "Sin Cuentas";
        }

        $this->loadModel('Labels');
        $labels_array = $this->Labels->find()->where(['entity' => 0])->toArray();

        $labels = [];
        $labels[] = [
            'id' => 0,
            'text' => 'Sin etiquetas',
            'entity' => 0,
            'color_back' => '#000000',
            'color_text' => '#000000',
            'enabled' => 1,
        ];

        foreach ($labels_array as $label) {
            $labels[] = $label;
        }

        $this->set(compact('paraments', 'areas', 'services', 'users', 'labels', 'accounts', 'controllers'));
        $this->set('_serialize', ['customers']);
    }

    /**
     * called from 
     * Customers/index
    */
    public function getCustomers()
    {
        if ($this->request->is('ajax')) {

            $response = new \stdClass();

            $response->draw = intval($this->request->getQuery('draw'));

            $response->recordsTotal = 0;

            $params = $this->request->getQuery();

            $customer_ids_disabled = $this->getCustomersIdsBusinessDisabled();

            $where = [];
            $where += ['Customers.deleted' => FALSE];

            if(count($customer_ids_disabled) > 0){
                $where += ['Customers.code NOT IN' => $customer_ids_disabled];
            }

            // Cuentas
            if (array_key_exists(24, $params['columns'])) {

                if ($params['columns'][24]['search']['value'] != '') {

                    $payment_getway_id = $params['columns'][24]['search']['value'];

                    $response->recordsTotal = $this->Customers
                        ->find()
                        ->matching('CustomersAccounts', function ($q) use ($payment_getway_id) {
                            return $q->where([
                                'CustomersAccounts.payment_getway_id' => $payment_getway_id,
                                'CustomersAccounts.deleted'           => 0
                            ]);
                        })
                        ->where($where)->count();
                } else {
                    $response->recordsTotal = $this->Customers->find()->where($where)->count();
                }
            } else {
                $response->recordsTotal = $this->Customers->find()->where($where)->count();
            }

            $response->data = $this->Customers->find('ServerSideData', [
                'params' => $this->request->getQuery(),
                'where' => $where
            ]);

            $response->recordsFiltered = $this->Customers->find('RecordsFiltered', [
                'params' => $this->request->getQuery(),
                'where' => $where
            ]);

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

   /**
    * called from 
    * Element/customer_seekerx
    */
    public function getCustomersSimple() 
    {
        if ($this->request->is('ajax')) {

            $response =  new \stdClass();

            $response->draw = intval($this->request->getQuery('draw'));

            if ($this->request->getQuery('enableLoad') == 1) {

                $customer_ids_disabled = $this->getCustomersIdsBusinessDisabled();

                $where = [];
                $where += ['Customers.deleted' => FALSE];
    
                if(count($customer_ids_disabled) > 0){
                    $where += ['Customers.code NOT IN' => $customer_ids_disabled];
                }
    

                $response->recordsTotal = $this->Customers->find()->where($where)->count();

                $response->data = $this->Customers->find('ServerSideDataSimple', [
                    'params' => $this->request->getQuery(),
                    'where' => $where
                ]);

                $response->recordsFiltered = $this->Customers->find('RecordsFilteredSimple', [
                    'params' => $this->request->getQuery(),
                    'where' => $where
                ]);

            } else {

                $response->recordsTotal = 0;
                $response->data = [];
                $response->recordsFiltered = 0;
            }

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function getPresupuestos()
    {
        if ($this->request->is('ajax')) {

            $response = new \stdClass();

            $response->draw = intval($this->request->getQuery('draw'));

            $response->data = [];

            $response->connection_saldo_month = 0;

            $date_temp = Time::now()->day(1)->modify('+1 month')->hour(0)->minute(0)->second(0);

            $params = $this->request->getQuery();

            if ($params['load'] == 1) {

                if ($params['where']['connection_id'] != 0) {

                    if ($params['duedate'] == 0) {

                        $response->recordsTotal = $this->Presupuestos->find()->where([
                            'Presupuestos.customer_code' => $params['where']['customer_code'],
                            'Presupuestos.connection_id' => $params['where']['connection_id'],
                            'Presupuestos.duedate <' => $date_temp->format('Y-m-d H:i:s'),
                        ])->count();

                        $presupuestos = $this->Presupuestos->find('ServerSideDataCustomer',  [
                            'params' => $params,
                            'where' =>  [
                                'Presupuestos.customer_code' => $params['where']['customer_code'],
                                'Presupuestos.connection_id' => $params['where']['connection_id'],
                                'Presupuestos.duedate <' => $date_temp->format('Y-m-d H:i:s'),
                            ],
                        ]);

                        $response->recordsFiltered = $this->Presupuestos->find('RecordsFilteredCustomer', [
                            'params' => $params,
                            'where' =>  [
                                'Presupuestos.customer_code' => $params['where']['customer_code'],
                                'Presupuestos.connection_id' => $params['where']['connection_id'],
                                'Presupuestos.duedate <' => $date_temp->format('Y-m-d H:i:s'),
                            ],
                        ]);

                    } else {

                        $response->recordsTotal = $this->Presupuestos->find()->where([
                            'Presupuestos.customer_code' => $params['where']['customer_code'],
                            'Presupuestos.connection_id' => $params['where']['connection_id'],
                        ])->count();

                        $presupuestos = $this->Presupuestos->find('ServerSideDataCustomer',  [
                            'params' => $params,
                            'where' =>  [
                                'Presupuestos.customer_code' => $params['where']['customer_code'],
                                'Presupuestos.connection_id' => $params['where']['connection_id'],
                            ],
                        ]);

                        $response->recordsFiltered = $this->Presupuestos->find('RecordsFilteredCustomer', [
                            'params' => $params,
                            'where' =>  [
                                'Presupuestos.customer_code' => $params['where']['customer_code'],
                                'Presupuestos.connection_id' => $params['where']['connection_id'],
                            ],
                        ]);
                    }

                } else {

                    if ($params['duedate'] == 0) {

                        $response->recordsTotal = $this->Presupuestos->find()->where([
                            'Presupuestos.customer_code' => $params['where']['customer_code'],
                            'Presupuestos.connection_id IS' => NULL,
                            'Presupuestos.duedate <' => $date_temp->format('Y-m-d H:i:s'),
                        ])->count();

                        $presupuestos = $this->Presupuestos->find('ServerSideDataCustomer',  [
                            'params' => $params,
                            'where' =>  [
                                'Presupuestos.customer_code' => $params['where']['customer_code'],
                                'Presupuestos.connection_id IS' => NULL,
                                'Presupuestos.duedate <' => $date_temp->format('Y-m-d H:i:s'),
                            ],
                        ]);

                        $response->recordsFiltered = $this->Presupuestos->find('RecordsFilteredCustomer', [
                            'params' => $params,
                            'where' =>  [
                                'Presupuestos.customer_code' => $params['where']['customer_code'],
                                'Presupuestos.connection_id IS' => NULL,
                                'Presupuestos.duedate <' => $date_temp->format('Y-m-d H:i:s'),
                            ],
                        ]);
                    } else {

                        $response->recordsTotal = $this->Presupuestos->find()->where([
                            'Presupuestos.customer_code' => $params['where']['customer_code'],
                            'Presupuestos.connection_id IS' => NULL,
                        ])->count();

                        $presupuestos = $this->Presupuestos->find('ServerSideDataCustomer',  [
                            'params' => $params,
                            'where' =>  [
                                'Presupuestos.customer_code' => $params['where']['customer_code'],
                                'Presupuestos.connection_id IS' => NULL,
                            ],
                        ]);

                        $response->recordsFiltered = $this->Presupuestos->find('RecordsFilteredCustomer', [
                            'params' => $params,
                            'where' =>  [
                                'Presupuestos.customer_code' => $params['where']['customer_code'],
                                'Presupuestos.connection_id IS' => NULL,
                            ],
                        ]);
                    }
                }

            } else {
                 $response->recordsTotal = 0;
                 $presupuestos = [];
                 $response->recordsFiltered = 0;
            
            }

            foreach ($presupuestos as $presupuesto) {
                $response->data[] = $presupuesto;
            }

            $order_asc = function($a, $b)
            {
                  if ($a->date < $b->date) {
                      return -1;
                  } else if ($a->date > $b->date) {
                      return 1;
                  } else {
                      return 0;
                  }
            };

            usort($response->data, $order_asc);

            $newData = [];

            foreach ($response->data as $data) {
                $response->connection_saldo_month += $data->total;

                $data->saldo = $response->connection_saldo_month;

                $newData[] = $data;
            }

            $response->data = array_reverse($newData); 

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function getMoves()
    {
        if ($this->request->is('ajax')) {

            $response = new \stdClass();
            $response->draw = intval($this->request->getQuery('draw'));
            $response->recordsTotal = 0;
            $response->data = [];
            $response->data_next_nonth = [];
            $response->recordsFiltered = 0;
            $response->connection_saldo_month = 0;

            if ($this->request->getQuery()['where']['connection_id'] != '') {
                $response = $this->ServerSideMoves($response, $this->request->getQuery());
            }

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function ServerSideMoves($response, $params)
    {
        $response = $this->ServerSideInvoicesMoves($response, $params);

        $response = $this->ServerSidePaymentsMoves($response, $params);

        $response = $this->ServerSideDebitsMoves($response, $params);

        $response = $this->ServerSideCreditsMoves($response, $params);

        $order_asc = function($a, $b)
        {
              if ($a->date < $b->date) {
                  return -1;
              } else if ($a->date > $b->date) {
                  return 1;
              } else {
                  return 0;
              }
        };

        usort($response->data, $order_asc);

        $date_temp = Time::now()->day(1)->modify('+1 month')->hour(0)->minute(0)->second(0);

        $newData = [];

        foreach ($response->data as $data) {
            $response->connection_saldo_month += $data->total;

            $data->saldo = $response->connection_saldo_month;

            $newData[] = $data;

            if ($data->saldo == 0 && $params['complete'] == 0) {
              $newData = [];
            }
        }

        $response->data = array_reverse($newData); 

        return $response;
    }

    private function ServerSideInvoicesMoves($response, $params)
    {
        $this->loadModel('Invoices');

        $date_temp = Time::now()->day(1)->modify('+1 month')->hour(0)->minute(0)->second(0);

        if ($params['where']['connection_id'] != 0) {

            if ($params['duedate'] == 0) {

                $response->recordsTotal += $this->Invoices->find()->where([
                    'Invoices.customer_code' => $params['where']['customer_code'],
                    'Invoices.connection_id' => $params['where']['connection_id'],
                    'Invoices.duedate <' => $date_temp->format('Y-m-d H:i:s'),
                ])->count();

                $invoices = $this->Invoices->find('ServerSideDataMoves',  [
                    'params' => $params,
                    'where' =>  [
                        'Invoices.customer_code' => $params['where']['customer_code'],
                        'Invoices.connection_id' => $params['where']['connection_id'],
                        'Invoices.duedate <' => $date_temp->format('Y-m-d H:i:s'),
                    ],
                ]);

                $response->recordsFiltered += $this->Invoices->find('RecordsFilteredMoves', [
                    'params' => $params,
                    'where' =>  [
                        'Invoices.customer_code' => $params['where']['customer_code'],
                        'Invoices.connection_id' => $params['where']['connection_id'],
                        'Invoices.duedate <' => $date_temp->format('Y-m-d H:i:s'),
                    ],
                ]);
            } else {

                $response->recordsTotal += $this->Invoices->find()->where([
                    'Invoices.customer_code' => $params['where']['customer_code'],
                    'Invoices.connection_id' => $params['where']['connection_id'],
                ])->count();

                $invoices = $this->Invoices->find('ServerSideDataMoves',  [
                    'params' => $params,
                    'where' =>  [
                        'Invoices.customer_code' => $params['where']['customer_code'],
                        'Invoices.connection_id' => $params['where']['connection_id'],
                    ],
                ]);

                $response->recordsFiltered += $this->Invoices->find('RecordsFilteredMoves', [
                    'params' => $params,
                    'where' =>  [
                        'Invoices.customer_code' => $params['where']['customer_code'],
                        'Invoices.connection_id' => $params['where']['connection_id'],
                    ],
                ]);
            }

        } else {

            if ($params['duedate'] == 0) {

                $response->recordsTotal += $this->Invoices->find()->where([
                    'Invoices.customer_code' => $params['where']['customer_code'],
                    'Invoices.connection_id IS' => NULL,
                    'Invoices.duedate <' => $date_temp->format('Y-m-d H:i:s'),
                ])->count();

                $invoices = $this->Invoices->find('ServerSideDataMoves',  [
                    'params' => $params,
                    'where' =>  [
                        'Invoices.customer_code' => $params['where']['customer_code'],
                        'Invoices.connection_id IS' => NULL,
                        'Invoices.duedate <' => $date_temp->format('Y-m-d H:i:s'),
                    ],
                ]);

                $response->recordsFiltered += $this->Invoices->find('RecordsFilteredMoves', [
                    'params' => $params,
                    'where' =>  [
                        'Invoices.customer_code' => $params['where']['customer_code'],
                        'Invoices.connection_id IS' => NULL,
                        'Invoices.duedate <' => $date_temp->format('Y-m-d H:i:s'),
                    ],
                ]);
            } else {

                $response->recordsTotal += $this->Invoices->find()->where([
                    'Invoices.customer_code' => $params['where']['customer_code'],
                    'Invoices.connection_id IS' => NULL,
                ])->count();

                $invoices = $this->Invoices->find('ServerSideDataMoves',  [
                    'params' => $params,
                    'where' =>  [
                        'Invoices.customer_code' => $params['where']['customer_code'],
                        'Invoices.connection_id IS' => NULL,
                    ],
                ]);

                $response->recordsFiltered += $this->Invoices->find('RecordsFilteredMoves', [
                    'params' => $params,
                    'where' =>  [
                        'Invoices.customer_code' => $params['where']['customer_code'],
                        'Invoices.connection_id IS' => NULL,
                    ],
                ]);

            }
        }

        foreach ($invoices as $invoice) {
            $response->data[] = new Move($invoice, 'invoice'); 
        }

        return $response;
    } 

    private function ServerSidePaymentsMoves($response, $params)
    {
        $this->loadModel('Payments');

        if ($params['where']['connection_id'] != 0) {

            $response->recordsTotal += $this->Payments->find()->contain(['Receipts'])->where([
                'Payments.customer_code' => $params['where']['customer_code'],
                'Payments.connection_id' => $params['where']['connection_id'],
                'Payments.anulated IS' => NULL
            ])->count();

            $payments = $this->Payments->find('ServerSideDataMoves', [
                'params' => $params,
                'where' =>  [
                    'Payments.customer_code' => $params['where']['customer_code'],
                    'Payments.connection_id' => $params['where']['connection_id'],
                    'Payments.anulated IS' => NULL
                ],
            ]);

            $response->recordsFiltered += $this->Payments->find('RecordsFilteredMoves', [
                'params' => $params,
                'where' =>  [
                    'Payments.customer_code' => $params['where']['customer_code'],
                    'Payments.connection_id' => $params['where']['connection_id'],
                    'Payments.anulated IS' => NULL
                ],
            ]);

        } else {

            $response->recordsTotal += $this->Payments->find()->contain(['Receipts'])->where([
                'Payments.customer_code' => $params['where']['customer_code'],
                'Payments.connection_id IS' => NULL,
                'Payments.anulated IS' => NULL
            ])->count();

            $payments = $this->Payments->find('ServerSideDataMoves', [
                'params' => $params,
                'where' =>  [
                    'Payments.customer_code' => $params['where']['customer_code'],
                    'Payments.connection_id IS' => NULL,
                    'Payments.anulated IS' => NULL
                ],
            ]);

            $response->recordsFiltered += $this->Payments->find('RecordsFilteredMoves', [
                'params' => $params,
                'where' =>  [
                    'Payments.customer_code' => $params['where']['customer_code'],
                    'Payments.connection_id IS' => NULL,
                    'Payments.anulated IS' => NULL
                ],
            ]);
        }

        foreach ($payments as $payment) {

            $response->data[] = new Move($payment, 'payment'); 
        }

        return $response;
    }

    private function ServerSideDebitsMoves($response, $params)
    {
        $this->loadModel('DebitNotes');

        $date_temp = Time::now()->day(1)->modify('+1 month')->hour(0)->minute(0)->second(0);

        if ($params['where']['connection_id'] != 0) {

            if ($params['duedate'] == 0) {

                $response->recordsTotal += $this->DebitNotes->find()->where([
                    'DebitNotes.customer_code' => $params['where']['customer_code'],
                    'DebitNotes.connection_id' => $params['where']['connection_id'],
                    'DebitNotes.duedate <' => $date_temp->format('Y-m-d H:i:s'),
                ])->count();

                $debits = $this->DebitNotes->find('ServerSideDataMoves', [
                    'params' => $params,
                    'where' =>  [
                        'DebitNotes.customer_code' => $params['where']['customer_code'],
                        'DebitNotes.connection_id' => $params['where']['connection_id'],
                        'DebitNotes.duedate <' => $date_temp->format('Y-m-d H:i:s'),
                    ],
                ]);

                $response->recordsFiltered += $this->DebitNotes->find('RecordsFilteredMoves', [
                    'params' => $params,
                    'where' =>  [
                        'DebitNotes.customer_code' => $params['where']['customer_code'],
                        'DebitNotes.connection_id' => $params['where']['connection_id'],
                        'DebitNotes.duedate <' => $date_temp->format('Y-m-d H:i:s'),
                    ],
                ]);

            } else {

                $response->recordsTotal += $this->DebitNotes->find()->where([
                    'DebitNotes.customer_code' => $params['where']['customer_code'],
                    'DebitNotes.connection_id' => $params['where']['connection_id'],
                ])->count();

                $debits = $this->DebitNotes->find('ServerSideDataMoves',  [
                    'params' => $params,
                    'where' =>  [
                        'DebitNotes.customer_code' => $params['where']['customer_code'],
                        'DebitNotes.connection_id' => $params['where']['connection_id'],
                    ],
                ]);

                $response->recordsFiltered += $this->DebitNotes->find('RecordsFilteredMoves', [
                    'params' => $params,
                    'where' =>  [
                        'DebitNotes.customer_code' => $params['where']['customer_code'],
                        'DebitNotes.connection_id' => $params['where']['connection_id'],
                    ],
                ]);
            }
        } else {

            if ($params['duedate'] == 0) {

                $response->recordsTotal += $this->DebitNotes->find()->where([
                    'DebitNotes.customer_code' => $params['where']['customer_code'],
                    'DebitNotes.connection_id IS' => NULL,
                    'DebitNotes.duedate <' => $date_temp->format('Y-m-d H:i:s'),
                ])->count();

                $debits = $this->DebitNotes->find('ServerSideDataMoves',  [
                    'params' => $params,
                    'where' =>  [
                        'DebitNotes.customer_code' => $params['where']['customer_code'],
                        'DebitNotes.connection_id IS' => NULL,
                        'DebitNotes.duedate <' => $date_temp->format('Y-m-d H:i:s'),
                    ],
                ]);

                $response->recordsFiltered += $this->DebitNotes->find('RecordsFilteredMoves', [
                    'params' => $params,
                    'where' =>  [
                        'DebitNotes.customer_code' => $params['where']['customer_code'],
                        'DebitNotes.connection_id IS' => NULL,
                        'DebitNotes.duedate <' => $date_temp->format('Y-m-d H:i:s'),
                    ],
                ]);

            } else {

                $response->recordsTotal += $this->DebitNotes->find()->where([
                    'DebitNotes.customer_code' => $params['where']['customer_code'],
                    'DebitNotes.connection_id IS' => NULL,
                ])->count();

                $debits = $this->DebitNotes->find('ServerSideDataMoves', [
                    'params' => $params,
                    'where' =>  [
                        'DebitNotes.customer_code' => $params['where']['customer_code'],
                        'DebitNotes.connection_id IS' => NULL,
                    ],
                ]);

                $response->recordsFiltered += $this->DebitNotes->find('RecordsFilteredMoves', [
                    'params' => $params,
                    'where' =>  [
                        'DebitNotes.customer_code' => $params['where']['customer_code'],
                        'DebitNotes.connection_id IS' => NULL,
                    ],
                ]);
            }
        }

        foreach ($debits as $debit) {

            $response->data[] = new Move($debit, 'debit'); 
        }

        return $response;
    } 

    private function ServerSideCreditsMoves($response, $params)
    {
        $this->loadModel('CreditNotes');

        if ($params['where']['connection_id'] != 0) {
            
            $response->recordsTotal += $this->CreditNotes->find()->where([
                'CreditNotes.customer_code' => $params['where']['customer_code'],
                'CreditNotes.connection_id' => $params['where']['connection_id'],
            ])->count();

            $cedits = $this->CreditNotes->find('ServerSideDataMoves', [
                'params' => $params,
                'where' =>  [
                    'CreditNotes.customer_code' => $params['where']['customer_code'],
                    'CreditNotes.connection_id' => $params['where']['connection_id'],
                ],
            ]);

            $response->recordsFiltered += $this->CreditNotes->find('RecordsFilteredMoves', [
                'params' => $params,
                'where' =>  [
                    'CreditNotes.customer_code' => $params['where']['customer_code'],
                    'CreditNotes.connection_id' => $params['where']['connection_id'],
                ],
            ]);

        } else {

            $response->recordsTotal += $this->CreditNotes->find()->where([
                'CreditNotes.customer_code' => $params['where']['customer_code'],
                'CreditNotes.connection_id IS' => NULL,
                ] )->count();

            $cedits = $this->CreditNotes->find('ServerSideDataMoves',  [
                'params' => $params,
                'where' =>  [
                    'CreditNotes.customer_code' => $params['where']['customer_code'],
                    'CreditNotes.connection_id IS' => NULL,
                ],
            ]);

            $response->recordsFiltered += $this->CreditNotes->find('RecordsFilteredMoves', [
                'params' => $params,
                'where' =>  [
                    'CreditNotes.customer_code' => $params['where']['customer_code'],
                    'CreditNotes.connection_id IS' => NULL,
                ],
            ]);
        }

        foreach ($cedits as $cedit) {

            $response->data[] = new Move($cedit, 'credit'); 
        }

        return $response;
    } 

    public function getWithoutInvoiceCustomer()
    {
       if ($this->request->is('ajax')) {

            $response =  new \stdClass();
            $response->draw = intval($this->request->getQuery('draw'));
            $response->recordsTotal = 0;
            $response->data = [];
            $response->data_next_nonth = [];
            $response->recordsFiltered = 0;
            $response->connection_saldo_month = 0;

            $response = $this->ServerSideDebtsMoves($response, $this->request->getQuery());

            $response = $this->ServerSideCustomersDiscountsMoves($response, $this->request->getQuery());

            $order_asc = function($a, $b)
            {
                if ($a->date < $b->date) {
                    return -1;
                } else if ($a->date > $b->date) {
                    return 1;
                } else {
                    return 0;
                }
            };

            usort($response->data, $order_asc);

            $newData = [];

            foreach ($response->data as $data) {

                $response->connection_saldo_month += $data->total;

                if ($data->customers_has_discount) {
                    $response->connection_saldo_month -= $data->customers_has_discount->total;
                }

                $data->saldo = $response->connection_saldo_month;
                $newData[] = $data;
            }

            $response->data = array_reverse($newData); 

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function getMovesAll()
    {
        if ($this->request->is('ajax')) {

            $response = new \stdClass();
            $response->draw = intval($this->request->getQuery('draw'));
            $response->recordsTotal = 0;
            $response->data = [];
            $response->data_next_nonth = [];
            $response->recordsFiltered = 0;
            $response->connection_saldo_month = 0;

            $response = $this->ServerSideMovesAll($response, $this->request->getQuery());

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function ServerSideMovesAll($response, $params)
    {
        $response = $this->ServerSideInvoicesMovesAll($response, $params);

        $response = $this->ServerSidePaymentsMovesAll($response, $params);

        $response = $this->ServerSideDebitsMovesAll($response, $params);

        $response = $this->ServerSideCreditsMovesAll($response, $params);

        $order_asc = function($a, $b)
        {
              if ($a->date < $b->date) {
                  return -1;
              } else if ($a->date > $b->date) {
                  return 1;
              } else {
                  return 0;
              }
        };

        usort($response->data, $order_asc);

        $date_temp = Time::now()->day(1)->modify('+1 month')->hour(0)->minute(0)->second(0);

        return $response;
    }

    private function ServerSideInvoicesMovesAll($response, $params)
    {
        $this->loadModel('Invoices');

        $date_temp = Time::now()->day(1)->modify('+1 month')->hour(0)->minute(0)->second(0);
        $date = explode("/", $params['date']);
        $date = $date[2] . '-' . $date[1] . '-' . $date[0];

        if ($params['duedate'] == 0) {

            $response->recordsTotal += $this->Invoices->find()->where([
                'Invoices.duedate <' => $date_temp->format('Y-m-d H:i:s'),
            ])->count();

            $invoices = $this->Invoices->find('ServerSideDataMovesAll',  [
                'params' => $params,
                'where' => [
                    'Invoices.duedate <' => $date_temp->format('Y-m-d H:i:s'),
                    'Invoices.date LIKE' => $date . '%'
                ],
            ]);

            $response->recordsFiltered += $this->Invoices->find('RecordsFilteredMovesAll', [
                'params' => $params,
                'where' => [
                    'Invoices.duedate <' => $date_temp->format('Y-m-d H:i:s'),
                    'Invoices.date LIKE' => $date . '%'
                ],
            ]);
        } else {

            $response->recordsTotal += $this->Invoices->find()->count();

            $invoices = $this->Invoices->find('ServerSideDataMovesAll',  [
                'params' => $params,
                'where' => [],
            ]);

            $response->recordsFiltered += $this->Invoices->find('RecordsFilteredMovesAll', [
                'params' => $params,
                'where' => [],
            ]);

        }

        foreach ($invoices as $invoice) {
            $response->data[] = new MoveAll($invoice, 'invoice'); 
        }

        return $response;
    } 

    private function ServerSidePaymentsMovesAll($response, $params)
    {
        $this->loadModel('Payments');
        $date = explode("/", $params['date']);
        $date = $date[2] . '-' . $date[1] . '-' . $date[0];

        $response->recordsTotal += $this->Payments->find()->contain(['Receipts'])->where([
            'Payments.anulated IS' => NULL,
            'Payments.created LIKE' => $date . '%'
        ])->count();

        $payments = $this->Payments->find('ServerSideDataMovesAll', [
            'params' => $params,
            'where' => [
                'Payments.anulated IS' => NULL,
                'Payments.created LIKE' => $date . '%'
            ],
        ]);

        $response->recordsFiltered += $this->Payments->find('RecordsFilteredMovesAll', [
            'params' => $params,
            'where' => [
                'Payments.anulated IS' => NULL,
                'Payments.created LIKE' => $date . '%'
            ],
        ]);

        foreach ($payments as $payment) {

            $response->data[] = new MoveAll($payment, 'payment'); 
        }

        return $response;
    }

    private function ServerSideDebitsMovesAll($response, $params)
    {
        $this->loadModel('DebitNotes');

        $date_temp = Time::now()->day(1)->modify('+1 month')->hour(0)->minute(0)->second(0);
        $date = explode("/", $params['date']);
        $date = $date[2] . '-' . $date[1] . '-' . $date[0];

        if ($params['duedate'] == 0) {

            $response->recordsTotal += $this->DebitNotes->find()->where([
                'DebitNotes.duedate <' => $date_temp->format('Y-m-d H:i:s'),
            ])->count();

            $debits = $this->DebitNotes->find('ServerSideDataMovesAll',  [
                'params' => $params,
                'where' => [
                    'DebitNotes.duedate <' => $date_temp->format('Y-m-d H:i:s'),
                    'DebitNotes.date LIKE' => $date . '%'
                ],
            ]);

            $response->recordsFiltered += $this->DebitNotes->find('RecordsFilteredMoves', [
                'params' => $params,
                'where' => [
                    'DebitNotes.duedate <' => $date_temp->format('Y-m-d H:i:s'),
                    'DebitNotes.date LIKE' => $date . '%'
                ],
            ]);

        } else {

            $response->recordsTotal += $this->DebitNotes->find()->count();

            $debits = $this->DebitNotes->find('ServerSideDataMovesAll', [
                'params' => $params,
                'where' => [],
            ]);

            $response->recordsFiltered += $this->DebitNotes->find('RecordsFilteredMovesAll', [
                'params' => $params,
                'where' => [],
            ]);
        }

        foreach ($debits as $debit) {

            $response->data[] = new MoveAll($debit, 'debit'); 
        }

        return $response;
    } 

    private function ServerSideCreditsMovesAll($response, $params)
    {
        $this->loadModel('CreditNotes');
        $date = explode("/", $params['date']);
        $date = $date[2] . '-' . $date[1] . '-' . $date[0];

        $response->recordsTotal += $this->CreditNotes->find()->count();

        $cedits = $this->CreditNotes->find('ServerSideDataMovesAll',  [
            'params' => $params,
            'where' =>  [
                'CreditNotes.date LIKE' => $date . '%'
            ],
        ]);

        $response->recordsFiltered += $this->CreditNotes->find('RecordsFilteredMovesAll', [
            'params' => $params,
            'where' =>  [
                'CreditNotes.date LIKE' => $date . '%'
            ],
        ]);

        foreach ($cedits as $cedit) {

            $response->data[] = new MoveAll($cedit, 'credit'); 
        }

        return $response;
    }

    private function ServerSideDebtsMoves($response, $params)
    {
        $this->loadModel('Debts');
        $date_temp = Time::now()->day(1)->modify('+1 month')->hour(0)->minute(0)->second(0);

        if ($params['where']['connection_id'] != 0) {

            if ($params['duedate'] == 0) {

                $response->recordsTotal += $this->Debts->find()->where([
                    'Debts.customer_code' => $params['where']['customer_code'],
                    'Debts.connection_id' => $params['where']['connection_id'],
                    'Debts.duedate <' => $date_temp->format('Y-m-d H:i:s'),
                    'Debts.invoice_id IS' => NULL
                    ])->count();

                $debts = $this->Debts->find('ServerSideDataMoves', [
                    'params' => $params,
                    'where' => [
                        'Debts.customer_code' => $params['where']['customer_code'],
                        'Debts.connection_id' => $params['where']['connection_id'],
                        'Debts.duedate <' => $date_temp->format('Y-m-d H:i:s'),
                        'Debts.invoice_id IS' => NULL
                        ],
                    ]);

                $response->recordsFiltered += $this->Debts->find('RecordsFilteredMoves', [
                    'params' => $params,
                    'where' => [
                        'Debts.customer_code' => $params['where']['customer_code'],
                        'Debts.connection_id' => $params['where']['connection_id'],
                        'Debts.duedate <' => $date_temp->format('Y-m-d H:i:s'),
                        'Debts.invoice_id IS' => NULL
                        ],
                    ]);

            } else {

                $response->recordsTotal += $this->Debts->find()->where([
                    'Debts.customer_code' => $params['where']['customer_code'],
                    'Debts.connection_id' => $params['where']['connection_id'],
                    'Debts.invoice_id IS' => NULL
                ])->count();

                $debts = $this->Debts->find('ServerSideDataMoves', [
                    'params' => $params,
                    'where' => [
                        'Debts.customer_code' => $params['where']['customer_code'],
                        'Debts.connection_id' => $params['where']['connection_id'],
                        'Debts.invoice_id IS' => NULL
                    ],
                ]);

                $response->recordsFiltered += $this->Debts->find('RecordsFilteredMoves', [
                    'params' => $params,
                    'where' => [
                        'Debts.customer_code' => $params['where']['customer_code'],
                        'Debts.connection_id' => $params['where']['connection_id'],
                        'Debts.invoice_id IS' => NULL
                    ],
                ]);

            }

        } else {

             if ($params['duedate'] == 0) {

                $response->recordsTotal += $this->Debts->find()->where([
                    'Debts.customer_code' => $params['where']['customer_code'],
                    'Debts.connection_id IS' => NULL,
                    'Debts.duedate <' => $date_temp->format('Y-m-d H:i:s'),
                    'Debts.invoice_id IS' => NULL
                    ])->count();

                $debts = $this->Debts->find('ServerSideDataMoves',  [
                    'params' => $params,
                    'where' => [
                        'Debts.customer_code' => $params['where']['customer_code'],
                        'Debts.connection_id IS' => NULL,
                        'Debts.duedate <' => $date_temp->format('Y-m-d H:i:s'),
                        'Debts.invoice_id IS' => NULL
                        ],
                    ]);

                $response->recordsFiltered += $this->Debts->find('RecordsFilteredMoves', [
                    'params' => $params,
                    'where' => [
                        'Debts.customer_code' => $params['where']['customer_code'],
                        'Debts.connection_id IS' => NULL,
                        'Debts.duedate <' => $date_temp->format('Y-m-d H:i:s'),
                        'Debts.invoice_id IS' => NULL
                        ],
                    ]);

            } else {
                 
                $response->recordsTotal += $this->Debts->find()->where([
                    'Debts.customer_code' => $params['where']['customer_code'],
                    'Debts.connection_id IS' => NULL,
                    'Debts.invoice_id IS' => NULL
                ])->count();

                $debts = $this->Debts->find('ServerSideDataMoves', [
                    'params' => $params,
                    'where' => [
                        'Debts.customer_code' => $params['where']['customer_code'],
                        'Debts.connection_id IS' => NULL,
                        'Debts.invoice_id IS' => NULL
                    ],
                ]);

                $response->recordsFiltered += $this->Debts->find('RecordsFilteredMoves', [
                    'params' => $params,
                    'where' => [
                        'Debts.customer_code' => $params['where']['customer_code'],
                        'Debts.connection_id IS' => NULL,
                        'Debts.invoice_id IS' => NULL
                    ],
                ]);

             }
        }

        foreach ($debts as $debt) {
            $response->data[] = new MovewithoutInvoice($debt, 'debt'); 
        }

        return $response;
    }

    private function ServerSideCustomersDiscountsMoves($response, $params)
    {
        $this->loadModel('CustomersHasDiscounts');

        $date_temp = Time::now()->modify('+1 month')->day(1)->hour(0)->minute(0)->second(0);

        if ($params['where']['connection_id'] != 0) {

            $response->recordsTotal += $this->CustomersHasDiscounts->find()->where([
                'CustomersHasDiscounts.customer_code' => $params['where']['customer_code'],
                'CustomersHasDiscounts.connection_id' => $params['where']['connection_id'],
                'CustomersHasDiscounts.invoice_id IS' => NULL,
                'CustomersHasDiscounts.debt_id IS' => NULL
            ])->count();

            $customersHasDiscounts = $this->CustomersHasDiscounts->find('ServerSideDataMoves', [
                'params' => $params,
                'where' => [
                    'CustomersHasDiscounts.customer_code' => $params['where']['customer_code'],
                    'CustomersHasDiscounts.connection_id' => $params['where']['connection_id'],
                    'CustomersHasDiscounts.invoice_id IS' => NULL,
                    'CustomersHasDiscounts.debt_id IS' => NULL
                ],
            ]);

            $response->recordsFiltered += $this->CustomersHasDiscounts->find('RecordsFilteredMoves', [
                'params' => $params,
                'where' => [
                    'CustomersHasDiscounts.customer_code' => $params['where']['customer_code'],
                    'CustomersHasDiscounts.connection_id' => $params['where']['connection_id'],
                    'CustomersHasDiscounts.invoice_id IS' => NULL,
                    'CustomersHasDiscounts.debt_id IS' => NULL
                ],
            ]);
        } else {

            $response->recordsTotal += $this->CustomersHasDiscounts->find()->where([
                'CustomersHasDiscounts.customer_code' => $params['where']['customer_code'],
                'CustomersHasDiscounts.connection_id IS' => NULL,
                'CustomersHasDiscounts.invoice_id IS' => NULL,
                'CustomersHasDiscounts.debt_id IS' => NULL
            ])->count();

            $customersHasDiscounts = $this->CustomersHasDiscounts->find('ServerSideDataMoves', [
                'params' => $params,
                'where' => [
                    'CustomersHasDiscounts.customer_code' => $params['where']['customer_code'],
                    'CustomersHasDiscounts.connection_id IS' => NULL,
                    'CustomersHasDiscounts.invoice_id IS' => NULL,
                    'CustomersHasDiscounts.debt_id IS' => NULL
                ],
            ]);

            $response->recordsFiltered += $this->CustomersHasDiscounts->find('RecordsFilteredMoves', [
                'params' => $params,
                'where' => [
                    'CustomersHasDiscounts.customer_code' => $params['where']['customer_code'],
                    'CustomersHasDiscounts.connection_id IS' => NULL,
                    'CustomersHasDiscounts.invoice_id IS' => NULL,
                    'CustomersHasDiscounts.debt_id IS' => NULL
                ],
            ]);
        }

        foreach ($customersHasDiscounts as $customerHasDiscount) {

            $response->data[] = new MovewithoutInvoice($customerHasDiscount, 'customer_has_discount'); 
        }

        return $response;
    }

    /**
     * called from 
     * Customers/index
    */
    public function getCustomersDeleted()
    {
        if ($this->request->is('ajax')) {

            $response =  new \stdClass();

            $response->draw = intval($this->request->getQuery('draw'));

            $response->recordsTotal = $this->Customers
                ->find()
                ->where([
                    'Customers.deleted'       => TRUE,
                    'Customers.super_deleted' => FALSE
                ])->count();

            $response->data = $this->Customers->find('ServerSideData', [
                'params' => $this->request->getQuery(),
                'where' => [
                    'Customers.deleted'       => TRUE,
                    'Customers.super_deleted' => FALSE
                ] 
            ]);

            $response->recordsFiltered = $this->Customers->find('RecordsFiltered', [
                'params' => $this->request->getQuery(),
                'where' => [
                    'Customers.deleted'       => TRUE,
                    'Customers.super_deleted' => FALSE
                ] 
            ]);

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    /**
     * called from 
     * Element/customer_seeker
    */
    public function geCustomerAjax()
    {
        if ($this->request->is('ajax')) { //Ajax Detection

            $code = $this->request->input('json_decode')->code ? $this->request->input('json_decode')->code : null;
            $ident = $this->request->input('json_decode')->ident ? $this->request->input('json_decode')->ident : null;

            $wheres = ['deleted' => false];

            if ($code) {
               $wheres['code'] = $code;
            }

            if ($ident) {
               $wheres['ident'] = $ident;
            }

            $customer = $this->Customers
                ->find()
                ->contain([
                    'Labels'
                ])->where($wheres)
                ->first();

            if ($customer) {
                $this->set('customer', $customer);
            } else {
               $this->set('customer', false);
            }
        }
    }

    public function deleted()
    {
        $customers = $this->Customers
            ->find()
            ->contain([
                'Users',
                'Areas'
            ])
            ->where([
                'Customers.deleted' => TRUE
            ]);

        $paraments = $this->request->getSession()->read('paraments');

        $areas = [];
        $this->loadModel('Areas');
        $areas_model = $this->Areas->find();
        foreach ($areas_model as $a) {
            $areas[$a->id] = $a->name;
        }

        $services = [];
        $this->loadModel('Services');
        $services_model = $this->Services->find();
        foreach ($services_model as $a) {
            $services[$a->id] = $a->name;
        }

        $users = [];
        $this->loadModel('Users');
        $users_model = $this->Users->find();
        foreach ($users_model as $user) {
            $users[$user->id] = $user->name;
        }

        $this->loadModel('Labels');
        $labels_array = $this->Labels->find()->where(['entity' => 0])->toArray();

        $labels = [];
        $labels[] = [
            'id' => 0,
            'text' => 'Sin etiquetas',
            'entity' => 0,
            'color_back' => '#000000',
            'color_text' => '#000000',
            'enabled' => 1,
        ];

        foreach ($labels_array as $label) {
            $labels[] = $label;
        }

        $this->set(compact('customers', 'paraments', 'areas', 'services', 'users', 'labels'));
        $this->set('_serialize', ['customers']);
    }

    public function restore($id = null)
    {
        $customer = $this->Customers->get($id, [
            'contain' => []
        ]);

        $customer = $this->Customers->patchEntity($customer, ['deleted' => false]);

        if ($this->Customers->save($customer)) {
            $this->Flash->success(__('Cliente Restaurado.'));
        } else {
            $this->Flash->error(__('Error al restaurar el cliente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    private function randomPassword($len = 6)
    {
        $alphabet = "abcdefghijklmnopqrstuwxyz0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $len; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function add($from_presale = false)
    {
        $session = $this->request->getSession();
        $customer_data = null;

        $clave_portal = $this->randomPassword();

        if (strstr(strtolower($this->referer('/', true)), "/customers/add")) {
            $customer_data = $session->read('customer_data');
        } else {
            $session->delete('customer_data');
        }

        $customer = $this->Customers->newEntity();

        if ($customer_data) {
            $customer = $this->Customers->patchEntity($customer, $customer_data);
        }

        $paraments = $session->read('paraments');
        $presale = ($session->check('presale') && ((array_key_exists("btn-add", $this->request->getData()) && $this->request->getData()['btn-add'] == 'presale') 
                || $from_presale));

        $this->loadModel('Users');
        $users_add = $this->Users->find()->where(['deleted' => 0, 'id NOT IN' => $paraments->system->users]);

        if ($this->Accountant->isEnabled()) {

            if (!$paraments->accountant->acounts_parent->customers) {

                $this->Flash->warning(__('Configuración Faltante. Se debe especificar la cuenta padre de Clientes.'));

                if ($presale) {
                    return $this->redirect(['controller' => 'Presales', 'action' => 'add']);
                }
                return $this->redirect(['action' => 'index']);
            }
        }

        if ($this->request->is('post')) {

            $action = 'Agregado Cliente';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            if ($paraments->customer->doc_validate) {

                $other = $this->Customers->find()->where([
                    'doc_type' => $this->request->getData('doc_type'),
                    'ident' => $this->request->getData('ident'),
                    'super_deleted' => false
                ])
                ->first();

                if ($other) {

                    if ($other->deleted) {
                        $this->Flash->error(__('Existe un Cliente eliminado con el DNI: ' . $this->request->getData('ident') . '. Para recuperarlo ir a la sección Clientes Eliminados.' ));
                    } else {
                        $this->Flash->error(__('Existe un Cliente cargado con el DNI: ' . $this->request->getData('ident')));
                    }
                    $session->write(
                        'customer_data', 
                        $this->request->getData()
                    );
                    if ($presale) {
                        return $this->redirect(['action' => 'add', true]);
                    }
                    return $this->redirect(['action' => 'add']);
                }
            }
            
            $data = $this->request->getData();

            if (!$this->request->getData('ident')) {
                $data['ident'] = null;
            }

            if (!$this->request->getData('email')) {
                $data['email'] = null;
            }

            $data['name'] = strtoupper($this->request->getData('name'));

            if ($paraments->system->mode_migration) {

                $created = $this->request->getData('created');
                $created = explode('/', $created);
                $created = $created[2] . '-' . $created[1] . '-' . $created[0] . Time::now()->format(' H:i:s');

                $data['created'] = $created;
                $data['modified'] = $created;
            }

            $customer = $this->Customers->patchEntity($customer, $data);

            $customer->user_id = $this->Auth->user()['id'];

            if ($paraments->customer->email_validate) {

                if ($this->Customers->find()->where(['email' => $customer->email, 'deleted' => 0])->first()) {

                    $session->write(
                        'customer_data', 
                        $data
                    );

                    $this->Flash->error(__("Error, el correo electrónico ya existe."));

                    if ($presale) {
                        return $this->redirect(['action' => 'add', true]);
                    }

                    return $this->redirect(['action' => 'add']);
                }
            }

            $customer->billing_for_service = $paraments->gral_config->billing_for_service;

            if ($presale) {
                $session->delete('customer_data');
                $presale = $session->read('presale');
                $presale->customer = $customer;
                $presale->customer->labels = [];
                $session->write('presale', $presale);
                $this->Flash->success(__('Se ha cargado el cliente correctamente'));

                return $this->redirect(['controller' => 'Presales', 'action' => 'add']);
            }

            if ($this->Customers->save($customer)) {

                $session->delete('customer_data');

                //si la contabilidada esta activada registro el asientop
                if ($this->Accountant->isEnabled()) {
                    $account = $this->Accountant->createAccount([
                        'account_parent' => $paraments->accountant->acounts_parent->customers,
                        'name' => sprintf("%'.05d", $customer->code) . ' - ' .  str_replace("-", "", $customer->ident) . ' - ' . $customer->name,
                        'redirect' => ['action' => 'add']
                    ]);

                    $customer->account_code = $account->code;

                    if (!$this->Customers->save($customer)) {
                        $this->Flash->error(__('Error al actualizar la cuenta del Cliente agregado.'));
                    }
                }

                if ($paraments->invoicing->generate_ticket) {

                    $date_array = explode(' ', $this->request->getData('start_task'));
                    //primer parte -> dd/mm/yyyy
                    $start_task_1 = explode('/', $date_array[0]);
                    //primer parte -> hh:mm
                    $start_task_2 = explode(':', $date_array[1]);

                    $start_task = $start_task_1[2] . '-' . $start_task_1[1] . '-' . $start_task_1[0] . ' ' . $start_task_2[0] . ':' . $start_task_2[1];

                    $start_task = new Time($start_task);

                    $this->loadModel("Tickets");
                    $ticket = $this->Tickets->newEntity();
                    $ticket->title = $this->request->getData('ticket_title') ? $this->request->getData('ticket_title') : 'Nueva instalación. Ticket generado en la carga de Cliente.';
                    $ticket->description = $this->request->getData('ticket_description') ? $this->request->getData('ticket_description') : 'Ticket generado desde la carga de Cliente.';

                    //estado abierto
                    $ticket->status = 1;

                    //para asignar a alguien la instalacion es el mismo que realizo el flujo de venta
                    $ticket->user_id = $this->Auth->user()['id'];
                    $ticket->asigned_user_id = NULL;
                    $ticket->customer_id = $customer->code;
                    $ticket->area_id = $customer->area_id;
                    $ticket->asigned_user_id = $this->request->getData('asigned_user_id') == '' ? NULL : $this->request->getData('asigned_user_id');

                    //categoria instalacion
                    $ticket->category = 2;

                    //la fecha de inicio es la fecha que se pacto en la venta para la instalacion
                    $ticket->start_task = $start_task;
                    $this->Tickets->save($ticket);
                }

                $this->Flash->success(__('Cliente Agregado.'));

                if ($this->request->getData('btn-add') == 'exit') {
                    return $this->redirect(['controller' => 'customers',  'action' => 'index']);
                }

                if ($this->request->getData('btn-add') == 'connection') {
                    return $this->redirect(['controller' => 'connections', 'action' => 'add']);
                }

                return $this->redirect(['action' => 'add']);

            } else {
                
                $message = 'Error al agregar el Cliente.';
                if (array_key_exists('email', $customer->getErrors())) {
                    $message = "Error, el correo electrónico ya existe.";
                }
                $this->Flash->error(__($message));
            }
        }

        $customer->seller = $this->Auth->user()['username'];

        $this->loadModel('Services');
        $services = $this->Services->find('list')->where(['enabled' => true, 'deleted' => false]);

        $this->loadModel('Countries');
        $countries = $this->Countries->find()->contain(['Provinces.Cities.Areas']);

        $business = [];

        foreach ($paraments->invoicing->business as $b) {
            if($b->enable){
                $business[$b->id] = $b->name . ' (' . $b->address. ')';
            }            
        }

        $this->loadModel('Areas');
        $areas = [];
        $areas_model = $this->Areas
            ->find()
            ->where([
                'enabled' => 1,
                'deleted' => 0
            ]);

        foreach ($areas_model as $area) {
            $areas[$area->id] = $area;
        }

        $banks = $this->request->getSession()->read('banks');

        $this->set(compact('customer', 'services', 'countries', 'banks', 'paraments', 'presale', 'business', 'areas', 'users_add', 'clave_portal'));
        $this->set('_serialize', ['customer']);
    }

    public function edit($id = null)
    {
        $session = $this->request->getSession();
        $presale = ($session->check('presale') && (array_key_exists("btn-edit", $this->request->getData()) && in_array($this->request->getData('btn-edit'), ['presale', 'cobrodigital_presale']) 
                || strstr(strtolower($this->referer()), "presales")));

        if ($id) {
            $customer = $this->Customers->get($id, [
                'contain' => ['Users', 'Areas', 'labels']
            ]);
        } else {
            if ($presale) {
                $presale = $session->read('presale');
                $customer = $presale->customer;
            }
        }

        $paraments = $this->request->getSession()->read('paraments');

        if ($this->Accountant->isEnabled()) {

            if (!$paraments->accountant->acounts_parent->customers) {
                $this->Flash->error(__('Configuración Faltante. Se debe especificar la cuenta padre de Clientes.'));
                if ($presale) {
                    return $this->redirect(['controller' => 'Presales', 'action' => 'add']);
                }
                return $this->redirect(['action' => 'index']);
            }
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Edición Cliente';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            if ($this->request->getData('btn-edit') == 'customer' || $this->request->getData('btn-edit') == 'presale') {

                if ($paraments->customer->doc_validate) {

                    if ($id) {
                        $other = $this->Customers->find()->where([
                            'doc_type' => $this->request->getData('doc_type'),
                            'ident' => $this->request->getData('ident'),
                            'code !=' => $id, 
                            'super_deleted' => false
                        ])
                        ->first();
                    }

                    if ($other) {
                        if ($other->deleted) {
                            $this->Flash->error(__('Existe un cliente eliminado con el DNI: ' . $this->request->getData('ident') . '. Para recuperarlo comuníquese con el administrador.' ));
                        } else {
                            $this->Flash->error(__('Existe un cliente cargado con el DNI: ' . $this->request->getData('ident')));
                        }
                        if ($presale) {

                            return $this->redirect(['controller' => 'Presales', 'action' => 'add']);
                        }

                        return $this->redirect(['action' => 'edit', $id]);
                    }
                  
                }

                $data = $this->request->getData();

                if (!$this->request->getData('ident')) {
                    $data['ident'] = null;
                }

                if (!$this->request->getData('email')) {
                    $data['email'] = null;
                }

                $data['name'] = strtoupper($this->request->getData('name'));

                $editConnections =  strtoupper($customer->name) != strtoupper($this->request->getData('name'));

                $customer = $this->Customers->patchEntity($customer, $data);

                if ($paraments->customer->email_validate) {

                    if ($this->Customers->find()->where(['email' => $customer->email, 'code !=' => $id, 'deleted' => 0])->first()) {
                        $this->Flash->error(__("Error, el correo electrónico ya existe."));
                        if ($presale) {
                            return $this->redirect(['action' => 'edit', true]);
                        }

                        return $this->redirect(['action' => 'edit', $id]);
                    }
                }

                if ($presale) {
                    $presale = $session->read('presale');
                    $debts_total = $this->getDebtsWithoutInvoices($customer->code);
                    $presale->customer = $customer;
                    $presale->customer->debts_total = $debts_total->total;
                    $presale->lat = $customer->lat;
                    $presale->lng = $customer->lng;
                    $session->write('presale', $presale);
                    $this->Flash->success(__('Se ha editado el cliente correctamente'));

                    return $this->redirect(['controller' => 'Presales', 'action' => 'add']);
                }

                if ($this->Customers->save($customer)) {

                    //si la contabilidada esta activada registro el asientop
                    if ($this->Accountant->isEnabled()) {

                        //edio cuenta contable del cliente
                        $this->loadModel('Accounts');

                        $account = $this->Accounts->find()
                            ->where([
                                'code' =>  $customer->account_code,
                        ])->first();

                        $account->name = sprintf("%'.05d", $customer->code) . ' - ' . str_replace("-", "", $customer->ident) . ' - ' . $customer->name;

                        if (!$this->Accounts->save($account)) {
                            $this->Flash->error(__('No se pudo actualizar la cuenta.'));
                        }
                    }

                    if ($editConnections) {
                        $this->syncConnections($customer->code);
                    }

                    $this->Flash->success(__('Datos actualizados.'));

                    $session = $this->request->getSession();

                    return $this->redirect(['action' => 'index']);

                } else {
                    $this->Flash->error(__('Error al editar el Cliente.'));
                }
            }
        }

        $this->loadModel('Services');
        $servicesAll = $this->Services->find();

        $services = [];
        foreach ($servicesAll as $service) {
            if ($customer->plan_ask == $service->id || (!$service->deleted  && $service->enabled)) {
                $services[$service->id] = $service->name; 
            }
        }

        $this->loadModel('Countries');
        $countries = $this->Countries->find()->contain(['Provinces.Cities.Areas']);

        $business = [];

        foreach ($paraments->invoicing->business as $b) {
            if($b->enable){
                $business[$b->id] = $b->name . ' (' . $b->address. ')';
            }
            
        }

        $this->loadModel('Areas');
        $areas = [];
        $areas_model = $this->Areas
            ->find()
            ->where([
                'enabled' => 1,
                'deleted' => 0
            ]);

        foreach ($areas_model as $area) {
            $areas[$area->id] = $area;
        }

        $banks = $this->request->getSession()->read('banks');

        $this->set(compact('customer', 'services', 'banks', 'paraments', 'countries', 'presale', 'business', 'areas'));
        $this->set('_serialize', ['customer']);
    }

    public function editCustomerFromDebt()
    {
        if ($this->request->is(['ajax'])) {

            $paraments = $this->request->getSession()->read('paraments');

            $data = $this->request->input('json_decode');

            $customer = $this->Customers->get($data->code);
            $customer->responsible = $data->responsible;
            $customer->daydue = $data->daydue;
            $customer->business_billing = $data->business_billing;
            $customer->code = $data->code;
            $customer->is_presupuesto = $data->is_presupuesto;

            if ($paraments->gral_config->value_service_customer) {
                $customer->value_service = $data->value_service;
            }

            $action = 'Edición Cliente desde Deuda';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            if ($this->Customers->save($customer)) {
                $customer = true;
            } else {
                $customer = false;
            }

            $this->set('customer', $customer);
        }
    }

    private function syncConnections($customer_code)
    {
        $this->loadModel('Connections');
        $connections = $this->Connections->find()->contain(['Controllers', 'Customers'])->where(['customer_code' => $customer_code]);

        foreach ($connections as $connection) {

            if(!$connection->deleted){

                if (!$this->IntegrationRouter->syncConnection($connection)) {
                    $this->Flash->error(__('Error al intentar actualizar las conexiones.'));
                    return false;
                }

            }           
        }

        return true;
    }

    public function view($code = null, $tab = 'profile') 
    {
        $paraments = $this->request->getSession()->read('paraments');
        $payment_getway = $this->request->getSession()->read('payment_getway');

        $this->loadModel('Areas');
        $areas = $this->Areas->find('list')->where(['deleted' => 0])->toArray();

        $this->loadModel('MassEmailsTemplates');
        $mass_emails_templates_model = $this->MassEmailsTemplates
            ->find()
            ->where([
                'enabled' => TRUE
            ]);

        $mass_emails_templates = [
            '' => 'Seleccione'
        ];

        foreach ($mass_emails_templates_model as $mass_email_template_model) {
            $mass_emails_templates[$mass_email_template_model->id] = $mass_email_template_model->name;
        }

        $this->loadModel('Connections');

        $payment_methods = [];
        foreach ($payment_getway->methods as $payment_method) {
            $config = $payment_method->config;
            if ($payment_getway->config->$config->enabled) {
                $payment_methods[$payment_method->id] = $payment_method->name;
            }
        }

        $customer = $this->Customers->get($code, [
            'contain' => [
                'Users',
                'Cities',
                'Connections'
            ]
        ]);

        $debt_month_facturados_connection = 0;
        $debt_month_sin_facturar_connection = 0;
        $debt_month_facturados_no_connection = $this->CurrentAccount->getDebtMonthNoConnectionFacturado($customer->code);
        $debt_month_sin_facturar_no_connection = $this->CurrentAccount->getDebtMonthNoConnectionSinFacturado($customer->code);
        $otras_ventas = 0;

        if ($paraments->gral_config->billing_for_service) {
            foreach ($customer->connections as $connection) {
                $debt_month_facturados_connection += $this->CurrentAccount->getDebtMonthFacturado($connection->id);
                $debt_month_sin_facturar_connection += $this->CurrentAccount->getDebtMonthSinFacturado($connection->id);
            }
            $otras_ventas = $debt_month_facturados_no_connection + $debt_month_sin_facturar_no_connection;
        }

        $debt_month_facturados =  $debt_month_facturados_no_connection + $debt_month_facturados_connection;
        $debt_month_sin_facturar = $debt_month_sin_facturar_no_connection + $debt_month_sin_facturar_connection;
        $customer->debt_total = $this->CurrentAccount->calulateCustomerDebtTotal($customer->code);

        $customer->from = Time::now()->day(1)->hour(0)->minute(0)->second(0);
        $customer->to = Time::now()->modify('+1 month')->day(1)->modify('-1 day')->hour(23)->minute(59)->second(59);

        $this->loadModel('Users');
        $users = $this->Users->find('list')->where(['deleted' => 0, 'id NOT IN' => $paraments->system->users]);

        $users_logs = [];
        $users_logs[0] = "Sin asignar";
        foreach ($users as $key => $user) {
            $users_logs[$key] = $user;
        }

        $this->loadModel('Controllers');
        $controllers = $this->Controllers->find('list')->where(['deleted' => 0]);

        $this->loadModel('Services');
        $services = $this->Services->find('list')->where(['deleted' => 0]);

        $this->loadModel('CashEntities');
        $cash_entities = $this->CashEntities->find()->where(['open' => true]);

        $cash_entities_array = [];
        $cash_entities_array[''] = 'Seleccione';
        foreach ($cash_entities as $cash_entity) {
            $cash_entities_array[$cash_entity->id] = $cash_entity->name . ' ($ ' . ($cash_entity->contado + $cash_entity->cash_other) . ')';
        }

        $connectionsArray = $this->getSourceAdministrativeMovement($code);

        $this->loadModel('StatusTickets');
        $tickets_status = $this->StatusTickets->find()->where(['deleted' => false])->order(['ordering' => 'ASC'])->toArray();
        $last_status = array_shift($tickets_status);
        array_push($tickets_status, $last_status);

        $status = [];
        foreach ($tickets_status as $st) {
            $status_ti[$st->name] = $st->name;
        }

        $this->loadModel('CategoriesTickets');
        $tickets_categories = $this->CategoriesTickets->find()->where(['deleted' => false])->order(['ordering' => 'ASC'])->toArray();

        $categories = [];
        $categorias = [];
        foreach ($tickets_categories as $cat) {
            $categories[$cat->name] = $cat->name;
            $categorias[$cat->id] = $cat->name;
        }

        $business = [];
        foreach ($paraments->invoicing->business as $b) {
            $business[$b->id] = $b->name;
        }

        // cuentas del cliente
        $accounts = [];
        $this->loadModel('CustomersAccounts');
        $customers_accounts = $this->CustomersAccounts
            ->find()
            ->where([
                'customer_code' => $code,
            ])
            ->order([
                'deleted' => 'ASC'
            ]);

        foreach ($customers_accounts as $customer_account) {

            foreach ($payment_getway->methods as $key => $pg) {

                if ($pg->id == $customer_account->payment_getway_id) {

                    $config = $pg->config;
                    $model = $payment_getway->config->$config->model;
                    $this->loadModel($model);
                    $account = $this->$model->find()->where([
                        'payment_getway_id' => $customer_account->payment_getway_id,
                        'id'                => $customer_account->account_id
                    ])->first();

                    if ($paraments->gral_config->billing_for_service) {

                        $connections = $this->Connections->find()
                            ->contain([
                                'Services'
                            ])->where([
                                'customer_code' => $customer->code
                            ]);

                        $connectionxs = [];

                        if ($connections->count() > 0) {

                            foreach ($connections as $conn) {

                                if ($conn->$key) {

                                    $connectionxs[$conn->id] = $conn->created->format('d/m/Y') . ' - ' . $conn->service->name . ' - ' . $conn->address;
                                }
                            }

                            $account->connections = $connectionxs;
                        }
                    }

                    array_push($accounts, $account);
                }
            }
        }

        //pasarela de pago
        $payment_methods = [];
        $payment_methods[""] = "Seleccionar";
        foreach ($payment_getway->methods as $pg) {
            $config = $pg->config;
            if ($payment_getway->config->$config->enabled && $pg->credential) {

                $payment_methods[$pg->id] = $pg->name;
            }
        }

        $credentials = $payment_getway->config->cobrodigital->credentials;
        $id_comercios = [];

        foreach ($credentials as $credential) {
            if ($credential->enabled) {
                $id_comercios[$credential->idComercio] = '(' . $credential->idComercio . ') ' . $credential->name;
            }
        }

        $banks = $this->request->getSession()->read('banks');

        $this->set(compact('customer', 'services', 'controllers', 'users', 'users_logs', 'status_ti', 'categories', 'categorias', 'connectionsArray', 'business', 'banks', 'credentials', 'payment_getway', 'accounts', 'id_comercios', 'tab', 'payment_methods', 'mass_emails_templates', 'areas', 'debt_month_facturados', 'debt_month_sin_facturar', 'otras_ventas'));
        $this->set('_serialize', ['customer']);

        $this->set('cash_entities', $cash_entities);
        $this->set('cash_entities_array', $cash_entities_array);
    }

    public function indexMoves() 
    {
        $paraments = $this->request->getSession()->read('paraments');
        $payment_getway = $this->request->getSession()->read('payment_getway');

        $this->loadModel('Areas');
        $areas = $this->Areas->find('list')->where(['deleted' => 0])->toArray();

        $this->loadModel('MassEmailsTemplates');
        $mass_emails_templates_model = $this->MassEmailsTemplates
            ->find()
            ->where([
                'enabled' => TRUE
            ]);

        $mass_emails_templates = [
            '' => 'Seleccione'
        ];

        foreach ($mass_emails_templates_model as $mass_email_template_model) {
            $mass_emails_templates[$mass_email_template_model->id] = $mass_email_template_model->name;
        }

        $this->loadModel('Connections');

        $payment_methods = [];
        foreach ($payment_getway->methods as $payment_method) {
            $config = $payment_method->config;
            if ($payment_getway->config->$config->enabled) {
                $payment_methods[$payment_method->id] = $payment_method->name;
            }
        }

        $this->loadModel('Users');
        $users = $this->Users->find('list')->where(['deleted' => 0, 'id NOT IN' => $paraments->system->users]);

        $this->loadModel('Controllers');
        $controllers = $this->Controllers->find('list')->where(['deleted' => 0]);

        $this->loadModel('Services');
        $services = $this->Services->find('list')->where(['deleted' => 0]);

        $this->loadModel('CashEntities');
        $cash_entities = $this->CashEntities->find()->where(['open' => true]);

        $cash_entities_array = [];
        $cash_entities_array[''] = 'Seleccione';
        foreach ($cash_entities as $cash_entity) {
            $cash_entities_array[$cash_entity->id] = $cash_entity->name . ' ($ ' . ($cash_entity->contado + $cash_entity->cash_other) . ')';
        }

        $business = [];
        foreach ($paraments->invoicing->business as $b) {
            $business[$b->id] = $b->name;
        }

        //pasarela de pago
        $payment_methods = [];
        $payment_methods[""] = "Seleccionar";
        foreach ($payment_getway->methods as $pg) {
            $config = $pg->config;
            if ($payment_getway->config->$config->enabled && $pg->credential) {

                $payment_methods[$pg->id] = $pg->name;
            }
        }

        $credentials = $payment_getway->config->cobrodigital->credentials;
        $id_comercios = [];

        foreach ($credentials as $credential) {
            if ($credential->enabled) {
                $id_comercios[$credential->idComercio] = '(' . $credential->idComercio . ') ' . $credential->name;
            }
        }

        $banks = $this->request->getSession()->read('banks');

        $this->set(compact('services', 'controllers', 'users', 'business', 'banks', 'credentials', 'payment_getway', 'accounts', 'id_comercios', 'tab', 'payment_methods', 'mass_emails_templates', 'areas'));
        $this->set('_serialize', ['customer']);

        $this->set('cash_entities', $cash_entities);
        $this->set('cash_entities_array', $cash_entities_array);
    }

    public function printresume($code, $from = null, $to = null)
    {
        if (count($paraments = $this->request->getSession()->read('paraments')->invoicing->business) > 0) {

            $data =  new Data();
            $data->code = $code;

            if ($from && $to) {
                $data->from = new Time($from . ' 00:00:00');
                $data->to =  new Time($to . ' 23:59:59');
            }
            else {
                $data->from = Time::now()->day(1);
                $data->to =  Time::now()->modify('+1 month')->day(1)->modify('-1 day')->hour(23)->minute(59)->second(59);
            }

            $this->PDFGenerator->generate($data, PDFGeneratorComponent::TYPE_ACCOUNT_RESUME);

        } else {
            $this->Flash->error(__('Configuración incompleta. Faltan datos de la empresa'));
            return $this->redirect(['action' => 'index']);
        }
    }

    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);

        $action = 'Archivado de Cliente';
        $detail = '';
        $this->registerActivity($action, $detail, NULL, TRUE);

        $customer = $this->Customers->get($_POST['id'], ['contain' => ['Connections']]);

        $connections_count = $this->Customers->Connections
            ->find()
            ->where([
                'deleted'       => false, 
                'customer_code' => $customer->code
            ])->count();

        if ($connections_count > 0) {

            $this->Flash->warning(__('No se puede archivar un cliente con conexiones activas. Primero debería eliminar sus conexiones.'));
            return $this->redirect(['action' => 'index']);
        }

        $customer->deleted = true;

        if ($this->Customers->save($customer)) {
            $this->Flash->success(__('Cliente archivado.'));
        } else {
            $this->Flash->error(__('Error al intentar archivar el cliente.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function superDelete()
    {
        $this->request->allowMethod(['post', 'delete']);

        $action = 'Eliminación de Cliente';
        $detail = '';
        $this->registerActivity($action, $detail, NULL, TRUE);

        $customer = $this->Customers->get($_POST['id']);

        $customer->super_deleted = TRUE;

        if ($this->Customers->save($customer)) {
            $this->Flash->success(__('Cliente Eliminado.'));
        } else {
            $this->Flash->error(__('Error al intentar eliminar el cliente.'));
        }
        return $this->redirect(['action' => 'deleted']);
    }

    public function importNetsion()
    {
        $customer = $this->Customers->newEntity();

        if ($this->request->is('post')) {

            $this->loadModel('Accounts');
            $paraments = $this->request->session()->read('paraments');

            if ($_FILES['csv']) {

                $error =  false;

                $handle = fopen($_FILES['csv']['tmp_name'], "r");

                $first = true;
                $counter = 0;
                $error_counter = 0;

                while ($data = fgetcsv($handle, 999999, ";")) {

                    if ($first) {
                        $first = false;
                        continue;
                    }

                    // Zarate,Zona2
                    // Zarate
                    $area_id = '1';
                    if (strpos(trim($data[5]), 'Zarate,Zona2') !== false) {
                        $area_id = '2';
                    }

                    //fecha de inicio
                    $created = Time::now();
                    $modified = Time::now();

                    if (!empty(trim($data[15]))) {
                        $created = new Time($data[15] . ' 00:00:00');
                        $modified = new Time($data[15] . ' 00:00:00');
                    }

                    //discriminacion por empresa en el cube
                    $business_billing = 1526130279;
                    if (trim($data[25]) == '2') {
                        $business_billing = 1528595019;
                    }

                    $phone = trim($data[9]) ? trim($data[9]) : 'sin telefono';

                    $user_id = 106;
                    $seller = "CYM Internet";

                    if (!empty(trim($data[10]))) {

                        if (strpos(trim($data[10]), 'Carlos Alberto Paladino') !== false) {
                            $user_id = 103;
                            $seller = "Carlos Alberto Paladino";
                        }

                        if (strpos(trim($data[10]), 'Miguel Paladino') !== false) {
                            $user_id = 104;
                            $seller = "Miguel Paladino";
                        }

                        if (strpos(trim($data[10]), 'Marcelo Ruben Di santo') !== false) {
                            $user_id = 105;
                            $seller = "Marcelo Ruben Di santo";
                        }

                        if (strpos(trim($data[10]), 'CYM Internet') !== false) {
                            $user_id = 106;
                            $seller = "CYM Internet";
                        }
                    }

                    $customer_data = [
                        'code'                      => intval(trim($data[0])),
                        'name'                      => utf8_decode(trim($data[1])),
                        'address'                   => utf8_decode(trim($data[4])),
                    	'doc_type'                  => '96',
                    	'status'                    => trim($data[24]),
                    	'ident'                     => trim($data[18]),
                    	'responsible'               => '5',
                    	'is_presupuesto'            => '1',
                    	'phone'                     => $phone,
                    	'country_id'                => '1',
                    	'province_id'               => '4',
                    	'city_id'                   => '1',
                    	'area_id'                   => $area_id,
                    	'payment_method_default_id' => '1',
                    	'denomination'              => '1',
                    	'cond_venta'                => '96',
                    	'business_billing'          => intval($business_billing),
                    	'phone_alt'                 => '',
                    	'daydue'                    => intval(trim($data[8])) == 0 ? 10 : intval(trim($data[8])),
                    	'email'                     => trim($data[17]),
                    	'seller'                    => $seller,
                    	'user_id'                   => $user_id,
                    	'created'                   => $created,
                    	'modified'                  => $modified,
                    	'comments'                  => 'deuda=' . trim($data[6]) . ',' . 'webpass=' . trim($data[14]) . ',' . 'ip=' . trim($data[29]) . ',' . 'plan=' . trim($data[2]) . ',' . 'payment_code=' . trim($data[21]),
                    ];

                    if (!$customer_data['code'] 
                        || !$customer_data['name'] 
                        || !$customer_data['ident'] 
                        || !$customer_data['address']) {
                         Debug($data);
                        $error_counter++;
                        continue;
                    }

                    // if (!$customer_data['code'] 
                    //     || !$customer_data['name'] 
                    //     || !$customer_data['ident'] 
                    //     || !$customer_data['address']
                    //     || !$customer_data['phone']) {
                    //      Debug($data);
                    //     $error_counter++;
                    //     continue;
                    // }

                    $customer = $this->Customers->newEntity();
                    $customer = $this->Customers->patchEntity($customer, $customer_data);
                    $customer->code = $customer_data['code'] ;

                    $account = $this->Accountant->createAccount([
                        'account_parent' => $paraments->accountant->acounts_parent->customers,
                        'name' => sprintf("%'.05d", $customer_data['code']) . ' - ' .  str_replace("-", "", $customer->ident) . ' - ' . $customer->name,
                        'redirect' => null
                    ]);

                    $customer->account_code = $account->code;

                    if (!$this->Customers->save($customer)) {
                        $error = true;
                        Debug($customer); die();
                        break;
                    }

                    $counter++;
                }

                if ($error) {
                    $this->Flash->error(__('Error al cargar'));
                } else {
                    $this->Flash->success(__('Se cargaron ' . $counter . ' Clientes'));
                    $this->Flash->error(__('Clientes faltan datos: ' . $error_counter));
                }

                fclose($handle);
            }
        }
        
        $this->render('import');

        $this->set(compact('customer'));
        $this->set('_serialize', ['customer']);
    }
    
    public function importAndros()
    {
        $customer = $this->Customers->newEntity();

        if ($this->request->is('post')) {

            $this->loadModel('Accounts');
            $this->loadModel('CustomersLabels');
            $paraments = $this->request->session()->read('paraments');

            if ($_FILES['csv']) {

                $error =  false;

                $handle = fopen($_FILES['csv']['tmp_name'], "r");

                $first = true;
                $counter = 0;
                $error_counter = 0;

                while ($data = fgetcsv($handle, 999999, ";")) {

                    if ($first) {
                        $first = false;
                        continue;
                    }

                    //discriminacion por empresa en el cube
                    $business_billing = 1526130279;
                    if (trim($data[15]) == 'Andros') {
                        $business_billing = 1539103136;
                    }

                    $phone = trim($data[8]) ? trim(utf8_encode($data[8])) : 'sin teléfono';

                    $user_id = 100;
                    $seller = "Sistema";

                    $customer_data = [
                        'code'                      => intval(trim($data[1])),
                        'name'                      => utf8_decode(trim($data[2])),
                        'address'                   => utf8_decode(trim($data[7])),
                    	'doc_type'                  => '96',
                    	'status'                    => NULL,
                    	'ident'                     => trim(str_replace(".", "", $data[4])),
                    	'responsible'               => '5',
                    	'is_presupuesto'            => trim($data[13]) == "SI" ? FALSE : TRUE,
                    	'phone'                     => $phone,
                    	'country_id'                => '1',
                    	'province_id'               => '2',
                    	'city_id'                   => '1',
                    	'area_id'                   => '1',
                    	'payment_method_default_id' => '1',
                    	'denomination'              => '1',
                    	'cond_venta'                => '96',
                    	'business_billing'          => intval($business_billing),
                    	'phone_alt'                 => trim($data[9]),
                    	'daydue'                    => trim($data[14]),
                    	'email'                     => trim(str_replace(",", "", utf8_decode($data[10]))),
                    	'seller'                    => $seller,
                    	'user_id'                   => $user_id,
                    	'comments'                  => 'deuda=' . trim($data[6]) . '#' . 'nodo=' . trim($data[0]) . '#' . 'bar_code_cd=' . trim($data[17]) . '#' . 'columna_1=' . trim($data[18]) . '#' . 'columna_n=' . trim($data[19]),
                    ];

                    if (!$customer_data['code'] 
                        || !$customer_data['name'] 
                        // || !$customer_data['ident'] 
                        || !$customer_data['address']) {
                         Debug($data);
                        $error_counter++;
                        continue;
                    }

                    // if (!$customer_data['code'] 
                    //     || !$customer_data['name'] 
                    //     || !$customer_data['ident'] 
                    //     || !$customer_data['address']
                    //     || !$customer_data['phone']) {
                    //      Debug($data);
                    //     $error_counter++;
                    //     continue;
                    // }

                    $customer = $this->Customers->newEntity();
                    $customer = $this->Customers->patchEntity($customer, $customer_data);
                    $customer->code = $customer_data['code'] ;
                    
                    $c = $this->Customers->find()->where(['code' => $customer_data['code']])->first();
                    
                    if ($c) {
                        Debug($c); 
                    }

                    $account = $this->Accountant->createAccount([
                        'account_parent' => $paraments->accountant->acounts_parent->customers,
                        'name' => sprintf("%'.05d", $customer_data['code']) . ' - ' .  str_replace("-", "", $customer->ident) . ' - ' . $customer->name,
                        'redirect' => null
                    ]);

                    $customer->account_code = $account->code;

                    if ($this->Customers->save($customer)) {
                        
                          $counter++;
                        
                        $label_id = "";
                        switch (trim($data[16])) {
                            case 'Bonificado':
                                $label_id = 3;
                                break;
                            case 'Chequar IP':
                                $label_id = 4;
                                break;
                            case 'Proxima Instalacion':
                                $label_id = 5;
                                break;
                            case 'Solicito Baja Desinstalar':
                                $label_id = 6;
                                break;
                            case 'Suspendido':
                                $label_id = 7;
                                break;
                            case 'Suspendido/desinstalar':
                                $label_id = 8;
                                break;
                        }
                        if ($label_id != "") {
                            $customer_label = $this->CustomersLabels->newEntity();
                            $customer_label->customer_code = $customer->code;
                            $customer_label->label_id = $label_id;
                            $this->CustomersLabels->save($customer_label);
                        }
                    } else {
                        $error = true;
                        Debug($customer); die();
                        break;
                    }
                }

                if ($error) {
                    $this->Flash->error(__('Error al cargar'));
                } else {
                    $this->Flash->success(__('Se cargaron ' . $counter . ' Clientes'));
                    $this->Flash->error(__('Clientes faltan datos: ' . $error_counter));
                }

                fclose($handle);
            }
        }
        
        $this->render('import');

        $this->set(compact('customer'));
        $this->set('_serialize', ['customer']);
    }

    function getTaxtypeCube($type)
    {
        
        switch ($type) {
            
            case 1: //ri
                return 1;
            case 2: //mo
                return 6;
            case 3: //ex
                return 4;
            case 4: //cf
                return 5;
            
        }
    } 

    function getBusinessCube($companynum)
    {
        switch ($companynum) {
            
            case 1: //edu (mo)
                return 1526130279;
            case 2: //guille (mo)
                return 1540071841;
            case 3: //ri
                return 1540071951;
        }
    }
    
    public function import()
    {
        $customer = $this->Customers->newEntity();

        if ($this->request->is('post')) {

            $this->loadModel('Accounts');
            $paraments = $this->request->session()->read('paraments');

            if ($_FILES['csv']) {

                $error =  false;

                $handle = fopen($_FILES['csv']['tmp_name'], "r");

                $first = true;
                $counter = 0;
                $error_counter = 0;

                while ($data = fgetcsv($handle, 999999, ",")) {

                    if ($first) {
                        $first = false;
                        continue;
                    }
                    
                    $name = utf8_decode(trim($data[1]));

                    if ($name) {
                         $c = $this->Customers->find()->where(['name like' => '%'.$name.'%'])->first();
                         
                         if (!$c) {
                             Debug($data);
                         }
                    }
                    
                }

                fclose($handle);
            }
        }
        
        $this->render('import');

        $this->set(compact('customer'));
        $this->set('_serialize', ['customer']);
    }

    public function importPatagoniaDBCube()
    {
        $customer = $this->Customers->newEntity();

        if ($this->request->is('post')) {

            $this->loadModel('Accounts');
            $this->loadModel('CustomersLabels');
            $paraments = $this->request->session()->read('paraments');

            $error =  false;

            $first = true;
            $counter = 0;
            $error_counter = 0;
            
            $connection_patagonia_cube = ConnectionManager::get('244');
            
            $customersCube = $connection_patagonia_cube->execute('SELECT * FROM customer')->fetchAll('assoc');
            
            $this->loadModel('Users');
            $this->loadModel('Observations');

             foreach ($customersCube as $customerCube) {
                 
                 $sin_telefono= false;
                 $code = intval(trim($customerCube['idcustomer']));

                $phone1 = $customerCube['phone'];
                $phone2 = $customerCube['mobile'];
                
                
                $parseUsers = [
                    1 => 120, 
                    2 => 121,
                    100 => 122, 
                    103 => 123,
                    107 => 124,
                    108 => 125
                ];
                
                $user_id = $customerCube['saleop'] == 100 ? 111 : $customerCube['saleop'];
                
                if (!$user_id) {
                    $user_id = 101;
                }
                
                if ($user_id == 102) {
                    $user_id = 107;
                }
                
                $user = $this->Users->get(120);

                $created = Time::now();

                if ($customerCube['intdate']) {
                    $created = new Time($customerCube['intdate']. ' 00:00:00');
                }

                $customer_data = [
                    'code'                      => intval(trim($customerCube['idcustomer'])),
                    'comments'                  => $customerCube['idcustomer'],
                    'name'                      => trim($customerCube['name']),
                    'address'                   => trim($customerCube['address']),
                	'doc_type'                  => $customerCube['doctype'] == 1 ? 80 : 96  ,
                	'status'                    => NULL,
                	'ident'                     => trim(str_replace(".", "", $customerCube['id'])),
                	'responsible'               =>  $this->getTaxtypeCube($customerCube['taxtype']),
                	'is_presupuesto'            => $customerCube['custtype'] == 1 ? FALSE : TRUE,
                	'phone'                     => $phone1,
                	'phone_alt'                 => $phone2,
                	'country_id'                => '1',
                	'province_id'               => '5',
                	'city_id'                   => $customerCube['postalcod'] == 3 ? 1 : $customerCube['postalcod'],
                	'area_id'                   => $customerCube['postalcod'] == 3 ? 1 : $customerCube['postalcod'],
                	'payment_method_default_id' => '1',
                	'denomination'              => '1',
                	'cond_venta'                => '96',
                	'business_billing'          => 1526130279,
                	'daydue'                    => 15,
                	'email'                     => trim(str_replace(",", "", $customerCube['mail'])),
                	'seller'                    => $user->username,
                	'user_id'                   => $user->id,
                	'lat'                   => $customerCube['lat'],
                	'lng'                   => $customerCube['lng'],
                	'deleted'                   => ($customerCube['custstatus'] == 5 ) ? true : false,
                	'created'                   => $created,
                	'modified'                   => $created,
                	'debt_month'                  => floatval($customerCube['debt']),
                ];

                if (strpos($customerCube['idcustomer'], 'ZZ') !== false) {
                       $customer_data['deleted'] = true; 
                }

                if (!$customer_data['name']) {
                     Debug($customer_data);
                    $error_counter++;
                    continue;
                }
                
                if (!$customer_data['phone']) {
                    
                    $customersDataCube = $connection_patagonia_cube->execute("SELECT * FROM customer_data where type = 'phone' and idcustomer = '" . $customerCube['idcustomer']."'" )->fetchAll('assoc');
                    
                    $f = true;
                    
                    $customer_data['phone'] = '';
                    
                    foreach ( $customersDataCube as $phone) {
                        
                        if ($f) {
                            $f = true;
                            $customer_data['phone'] .= $phone['value'];
                        } else {
                           $customer_data['phone_alt'] .= $phone['value']; 
                        }
                    }
                    
                    if (!$customer_data['phone']) {
                         $customer_data['phone'] ='sin-telefono';
                         $sin_telefono = true;
                    }
                    
                }
                
                if ($customer_data['address'] == '') {
                    
                    $sin_telefono = true;
                    $customer_data['address'] ='sin-domicilio';
                    
                }

                // if (!$customer_data['code'] 
                //     || !$customer_data['name'] 
                //     || !$customer_data['ident'] 
                //     || !$customer_data['address']
                //     || !$customer_data['phone']) {
                //      Debug($data);
                //     $error_counter++;
                //     continue;
                // }

                $customer = $this->Customers->newEntity();
                $customer = $this->Customers->patchEntity($customer, $customer_data);
                $customer->code = $customer_data['code'] ;
                
                $c = $this->Customers->find()->where(['code' => $customer_data['code']])->first();
  
                if ($c) {
                    Debug($c); 
               
                    $customer->code = null;
                    continue;
                }

                if ($this->Customers->save($customer)) {
                    
                    if ($sin_telefono) {
                        
                        $customer_label = $this->CustomersLabels->newEntity();
                        $customer_label->customer_code = $customer->code;
                        $customer_label->label_id = 1;
                        $this->CustomersLabels->save($customer_label);
                    }

                    if ($customerCube['management']) {
                        
                        // if($customer->code != 1198){

                            $gestiones = explode('>', $customerCube['management']);

                            $first = true;
                            
                            $users_ids = [
                                'marcos' => 120, 
                                'marcela' => 121, 
                                'facundo' => 122, 
                                'Esteban' => 123, 
                                'crapatagonia' => 124, 
                                'Cobrador' => 125, 
                            ];

                            foreach ($gestiones as $gestion) {
                                
                                if ($first) {
                                    
                                    $first = false; 
                                    continue;
                                }
                                
                                 $obs = $this->Observations->newEntity();
                                
                                $gestion = explode(')',$gestion);
                                $date = explode('(',$gestion[0]);
                                
                                $obs->created = new Time($date[0].' '. $date[1]);
                       
                                $obs->modified = $obs->created ;  

                                $obs->user_id = 100;
                                
                                foreach($users_ids as $username => $user_id){
                                    
                                    if(strpos($gestion[1], $username) !== false){
                                        
                                         $obs->user_id = $user_id;
                                         break;
                                    }
                                }
                           
                                $obs->model = 'Customers';
                                $obs->model_id = $customer->code;
                                
                                $obs->comment = $gestion[1];
                                $obs->customer_code = $customer->code;
                                
                                $this->Observations->save($obs);
                                
                            }

                    }

                    $counter++;

                    
                } else {
                    $error = true;
                    Debug($customer); die();
                    break;
                }

            }

            if ($error) {
                $this->Flash->error(__('Error al cargar'));
            } else {
                $this->Flash->success(__('Se cargaron ' . $counter . ' Clientes'));
                $this->Flash->error(__('Clientes faltan datos: ' . $error_counter));
            }
           
        }
        
        $this->render('import');

        $this->set(compact('customer'));
        $this->set('_serialize', ['customer']);
    }
   
    public function importPatagoniaDBCubeSaldos($block = 0)
    {
        $salto = 300 * $block;
        
        $paraments = $this->request->session()->read('paraments');

        $error =  false;

        $first = true;
        $counter = 0;
        $error_counter = 0;
        
        $customers = $this->Customers->find()->contain(['Cities', 'Areas'])->where(['debt_month !=' => 0])->toArray();
    
        $customers = array_slice($customers, $salto);
        
        $i = 0;
        
        foreach ($customers as $customer) {
            
            if ($i > 300) {
                break;
            }
            $i++;
             
            $saldo = $customer->debt_month;
            
            $concept = new \stdClass;
            $concept->type        = 'S';
            $concept->code        = 99;
            $concept->description = 'Saldo al mes de Octubre 2018';
            $concept->quantity    = 1;
            $concept->unit        = 7;
            $concept->price       = abs($saldo);
            $concept->discount    = 0;
            $concept->sum_price   = 0;
            $concept->tax         = 1;
            $concept->sum_tax     = 0;
            $concept->total       = abs($saldo);
            $concepts[0] = $concept;

            if ($saldo > 0) { //deuda

                $invoice  = [
                    'customer'      => $customer,
                    'concept_type'  => 3,
                    'date'          => new Time('2018-11-01 00:00:00'),
                    'date_start'    => new Time('2018-11-01 00:00:00'),
                    'date_end'      => new Time('2018-11-30 00:00:00'),
                    'duedate'       => new Time('2018-11-15 00:00:00'),
                    'debts'         => $concepts,
                    'discounts'     => [],
                    'tipo_comp'     => 'XXX',
                    'comments'      => 'Deuda Noviembre 2018',
                    'manual'        => true,
                    'connection_id' => null
                ];

                $invoice = $this->FiscoAfipComp->invoice($invoice);
                
                if (!$invoice) {
                    $error_counter++;
                } else {
                    $counter++;
                }

            } else if ($saldo < 0) { //cedito

                $creditNote  = [
                    'customer'     => $customer,
                    'concept_type' => 3,
                    'date'         => Time::now(),
                    'date_start'   => Time::now(),
                    'date_end'     => Time::now(),
                    'duedate'      => Time::now(),
                    'concepts'     => $concepts,
                    'tipo_comp'    => 'NCX',
                    'comments'     => 'Saldo a favor Noviembre 2018',
                    'manual'       => true,
                    'invoice'      => null,
                    'connection_id' => null
                ];


                $creditNote = $this->FiscoAfipComp->creditNote($creditNote);
                
                if (!$creditNote) {
                    $error_counter++;
                } else {
                    $counter++;
                }
            }
        }

        die($block . " cantidad: " . $counter);
        
        $this->render('import');

        $this->set(compact('customer'));
        $this->set('_serialize', ['customer']);
    }


    public function importaAireNet($block = 0){

        $salto = 300 * $block;

        $customers =  $customers = $this->Customers->find()
        ->contain([
            'Cities', 'Areas'
            ])
        ->where([
            'debt_month !=' => 0
            ])
        ->toArray();   

        $customers = array_slice($customers, $salto);
        
        $counter = 0;
        $error_counter = 0;

        $i = 0;
        
        foreach ($customers as $customer) {
            
            if ($i > 300) {
                break;
            }
            $i++;
            
            $saldo = $customer->debt_month;
            
            $concept = new \stdClass;
            $concept->type        = 'S';
            $concept->code        = 99;
            $concept->description = 'Octubre 2019';
            $concept->quantity    = 1;
            $concept->unit        = 7;
            $concept->price       = abs($saldo);
            $concept->discount    = 0;
            $concept->sum_price   = 0;
            $concept->tax         = 1;
            $concept->sum_tax     = 0;
            $concept->total       = abs($saldo);
            $concepts[0] = $concept;

            if ($saldo > 0) { //deuda

                $invoice  = [
                    'customer'      => $customer,
                    'concept_type'  => 3,
                    'date'          => new Time('2019-10-01 00:00:00'),
                    'date_start'    => new Time('2019-10-01 00:00:00'),
                    'date_end'      => new Time('2019-10-1 00:00:00'),
                    'duedate'       => new Time('2019-10-15 00:00:00'),
                    'debts'         => $concepts,
                    'discounts'     => [],
                    'tipo_comp'     => 'XXX',
                    'comments'      => 'Octubre 2019',
                    'manual'        => true,
                    'connection_id' => null
                ];

                $invoice = $this->FiscoAfipComp->invoice($invoice);
                
                if (!$invoice) {
                    $error_counter++;
                } else {
                    $counter++;
                }

            } else if ($saldo < 0) { //cedito

                $creditNote  = [
                    'customer'     => $customer,
                    'concept_type' => 3,
                    'date'         => Time::now(),
                    'date_start'   => Time::now(),
                    'date_end'     => Time::now(),
                    'duedate'      => Time::now(),
                    'concepts'     => $concepts,
                    'tipo_comp'    => 'NCX',
                    'comments'     => 'Octubre 2019',
                    'manual'       => true,
                    'invoice'      => null,
                    'connection_id' => null
                ];


                $creditNote = $this->FiscoAfipComp->creditNote($creditNote);
                
                if (!$creditNote) {
                    $error_counter++;
                } else {
                    $counter++;
                }
            }

        }

    }

    public function importEditOriginal()
    {
        $customer = $this->Customers->newEntity();

        if ($this->request->is('post')) {

            if ($_FILES['csv']) {

                $error =  false;

                $handle = fopen($_FILES['csv']['tmp_name'], "r");

                $first = true;
                $counter = 0;
                $error_counter = 0;

                while ($data = fgetcsv($handle, 999999, ";")) {

                    if ($first) {
                        $first = false;
                        continue;
                    }
                    
                    $customer = $this->Customers->find()->where(['code' => $data[0]])->first();
                    
                    if (!$customer) {
                        $this->Flash->error(__('no se econtro el cleinte {0}', $data[0] ));
                        Debug($customer); die();
                    }
                    
                    //aca lo que se quire editar !!!
                  
                    $customer = $this->Customers->patchEntity($customer, [
                        'is_presupuesto' => false
                    ]);

                    if (!$this->Customers->save($customer)) {
                        Debug($customer); die();
                        break;
                    }

                    $counter++;
                }
                
                $this->Flash->success(__('Se editaron ' . $counter . ' Clientes'));

                fclose($handle);
            }
        }

        $this->set(compact('customer'));
        $this->set('_serialize', ['customer']);
    }
    
    public function importEditCobroDigital()
    {
        
        $customer = $this->Customers->newEntity();

        if ($this->request->is('post')) {

            if ($_FILES['csv']) {

                $this->loadModel('CobrodigitalCards');
                $this->loadModel('Connections');
                $this->loadModel('ConnectionsCobrodigitalCards');

                $error =  false;

                $handle = fopen($_FILES['csv']['tmp_name'], "r");

                $first = true;
                $counterCD = 0;
                $counterPY = 0;
                $error_counter = 0;

                while ($data = fgetcsv($handle, 999999, ";")) {

                    if ($first) {
                        $first = false;
                        continue;
                    }

                    $comments = '';
                    
                    if ($data[1] == '') {
                        continue;
                    }
                    
                    $customer = $this->Customers->find()->where(['code' => $data[1]])->first();
                    
                    if (!$customer) {
                        $this->Flash->error(__('no se econtro el cliente {0}', $data[1] ));
                        Debug($data); 
                        continue;
                    }
                    
                    $bar_code = trim($data[17]);

                    if (strlen($bar_code) == 29) {
                        
                        $card = $this->CobrodigitalCards->find()->where(['barcode' => $bar_code])->first();
                    
                        if ($card) {
                            
                            $connections = $this->Connections->find()->where(['customer_code' => $customer->code]);
                            
                            foreach ($connections as $connection) {
                                
                                $connCobroCard = $this->ConnectionsCobrodigitalCards->newEntity();
                                
                                $connCobroCard->connection_id = $connection->id;
                                $connCobroCard->cobrodigital_card_id = $card->id;
                                
                                if (!$this->ConnectionsCobrodigitalCards->save($connCobroCard)) {
                                    Debug($connCobroCard); 
                                }
                                
                                $connection->payment_method_id  = 99;
                                
                                if (!$this->Connections->save($connection)) {
                                    Debug($connection); 
                                }
                                
                                $card->used = true;
                                $card->customer_code = $customer->code;
                                
                                if (!$this->CobrodigitalCards->save($card)) {
                                    Debug($card); 
                                }
                                
                                $counterCD++;
                            }
                        } else {
                            Debug('card no encontrado');
                            Debug($bar_code);
                        }
                    } else if ($bar_code != '') {
                        $customer->comments = '#Payu: '. $bar_code . "\n";
                        $counterPY++;
                    }
              
                    if (!$this->Customers->save($customer)) {
                        Debug($customer);
                        break;
                    }
                }
                
                $this->Flash->success(__('cobro digital: ' . $counterCD ));
                $this->Flash->success(__('payu: ' . $counterPY ));

                fclose($handle);
            }
        }
        
        $this->render('importEdit');

        $this->set(compact('customer'));
        $this->set('_serialize', ['customer']);
    }
    
    public function importEditSaldo()
    {
        $customer = $this->Customers->newEntity();

        if ($this->request->is('post')) {

            if ($_FILES['csv']) {

                $this->loadModel('CobrodigitalCards');
                $this->loadModel('Connections');
                $this->loadModel('ConnectionsCobrodigitalCards');

                $error =  false;

                $handle = fopen($_FILES['csv']['tmp_name'], "r");

                $first = true;
                $counter = 0;
                $error_counter = 0;

                while ($data = fgetcsv($handle, 999999, ";")) {

                    if ($first) {
                        $first = false;
                        continue;
                    }

                    $saldo = floatval(trim($data[6]));

                    if (!$saldo) {
                        continue;
                    }
                    
                    $customer = $this->Customers->find()->contain(['Cities', 'Areas'])->where(['code' => $data[1]])->first();
                    
                    if (!$customer) {
                        $this->Flash->error(__('no se econtro el cliente {0}', $data[1] ));
                        Debug($data); die();
                    }
                    
                    $connection = $this->Connections->find()->where(['customer_code' => $customer->code])->first();
                    
                    $concept = new \stdClass;
                    $concept->type        = 'S';
                    $concept->code        = 99;
                    $concept->description = 'Saldo al mes de Octubre 2018';
                    $concept->quantity    = 1;
                    $concept->unit        = 7;
                    $concept->price       = abs($saldo);
                    $concept->discount    = 0;
                    $concept->sum_price   = 0;
                    $concept->tax         = 1;
                    $concept->sum_tax     = 0;
                    $concept->total       = abs($saldo);
                    $concepts[0] = $concept;
        
                    if ($saldo > 0) { //deuda
        
                        $invoice  = [
                            'customer'      => $customer,
                            'concept_type'  => 3,
                            'date'          => Time::now(),
                            'date_start'    => Time::now(),
                            'date_end'      => Time::now(),
                            'duedate'       => Time::now(),
                            'debts'         => $concepts,
                            'discounts'     => [],
                            'tipo_comp'     => 'XXX',
                            'comments'      => 'Octubre 2018',
                            'manual'        => true,
                            'connection_id' => $connection ? $connection->id : null
                        ];

                        $invoice = $this->Comprobantes->generate($invoice, ComprobantesComponent::TYPE_INVOICE);
        
                        $counter++;
        
                    } else if ($customer->saldo_cube < 0) { //cedito
        
                        $creditNote  = [
                            'customer'     => $customer,
                            'concept_type' => 3,
                            'date'         => Time::now(),
                            'date_start'   => Time::now(),
                            'date_end'     => Time::now(),
                            'duedate'      => Time::now(),
                            'concepts'     => $concepts,
                            'tipo_comp'    => 'NCX',
                            'comments'     => 'Octubre 2018',
                            'manual'       => true,
                            'invoice'      => null,
                            'connection_id' => $connection ? $connection->id : null
                        ];

                        $creditNote = $this->Comprobantes->generate($creditNote, ComprobantesComponent::TYPE_CREDIT_NOTE);

                        $counter++;

                    }

                }
                
                $this->Flash->success(__('Se editaron ' . $counter . ' Clientes'));

                fclose($handle);
            }
        }

        $this->set(compact('customer'));
        $this->set('_serialize', ['customer']);
        
        $this->render('importEdit');
    }

    public function verifyExistOnCobrodigital()
    {
        if ($this->request->is('post')) {
            $code = $this->request->getData('code');

            $this->loadComponent('CobroDigital');
            $key    = 'Id';
            $value  = $this->request->getData('code');
            $result = $this->CobroDigital->verificarExistenciaPagador($key, $value);
            if ($result->ejecucion_correcta) {
                $message = "";
                foreach ($result->log as $l) {
                    $message .= $l . '\n';
                }
                $this->Flash->success($message);
            } else {
                $message = "";
                foreach ($result->log as $l) {
                    $message .= $l . '\n';
                }
                $this->Flash->warning($message);
            }
        }
        return $this->redirect(['action' => 'index']);
    }

    public function freedCard($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $customer = $this->Customers->get($id);
        if ($customer) {

            $this->loadModel('CobrodigitalCards');
            $card = $this->CobrodigitalCards->find()->where(['barcode' => $customer->cd_barcode, 'electronic_code' => $customer->cd_electronic_code])->first();
            $card->used = false;

            if ($this->CobrodigitalCards->save($card)) {
                $this->Flash->success(__('Se ha liberado la Tarjeta correctamente.'));
                $this->loadModel('Customers');

                $customer->cd_barcode = null;
                $customer->cd_electronic_code = null;
                $this->Customers->save($customer);

            } else {
                $this->Flash->error(__('No se ha liberado la Tarjeta. por favor, intente nuevamente.'));
            }
        } else {
            $this->Flash->error(__('No se ha liberado la Tarjeta. por favor, intente nuevamente.'));
        }

        return $this->redirect(['action' => 'edit', $customer->code]);
    }

    public function changeClave($code)
    {
        if ($this->request->is(['put'])) {
            $data = $this->request->input('json_decode', true);

            if (array_key_exists("token", $data) && ($data['token'] == Configure::read('project.token'))) {

                if (array_key_exists("code", $data)) {
                    $customer = $this->Customers->get($data['code']);

                    if (array_key_exists("password", $data)) {

                        $customer->clave_portal = $data['password'];

                        if ($this->Customers->save($customer)) {
                            $data['message'] = "Cambio con éxito";
                            $data['status'] = 200;
                        } else {
                            $data['message'] = "No se ha podido realizar el cambio. Por favor intente nuevamente.";
                            $data['status'] = 400;
                        }

                    } else {
                        $data['message'] = "Falta el campo a cambiar";
                        $data['status'] = 400;
                    }
                } else {
                    $data['message'] = "Falta el campo code para realizar la busqueda";
                    $data['status'] = 400;
                }

            } else {
                $data['message'] = "Token inválido";
                $data['status'] = 401;
            }

            $this->set([
                'data'       => $data,
                '_serialize' => ['data']
            ]);
        }
    }

    public function forgotClave()
    {
        if ($this->request->is(['put'])) {
            $data = $this->request->input('json_decode', true);

            if (array_key_exists("token", $data) && ($data['token'] == Configure::read('project.token'))) {

                if (array_key_exists("email", $data)) {
                    $customer = $this->Customers->find()->where(['email' => $data['email']])->first();

                    if ($customer) {
                        $customer->clave_portal = $this->randomPassword();

                        if ($this->Customers->save($customer)) {
                            $data['password'] = $customer->clave_portal;
                            $data['message'] = "Clave restablecida con éxito";
                            $data['status'] = 200;
                        } else {
                            $data['message'] = "No se ha podido restablecer la Clave. Por favor intente nuevamente.";
                            $data['status'] = 400;
                        }
                    } else {
                        $data['message'] = "El Correo enviado no se encuentra registrado";
                        $data['status'] = 400;
                    }
                } else {
                    $data['message'] = "Falta el campo Correo para realizar la busqueda";
                    $data['status'] = 400;
                }

            } else {
                $data['message'] = "Token inválido";
                $data['status'] = 401;
            }

            $this->set([
                'data'       => $data,
                '_serialize' => ['data']
            ]);
        }
    }

    public function cdRequestCard()
    {
        if ($this->request->is(['post'])) {
            $data = $this->request->input('json_decode', true);
            if (array_key_exists("token", $data) && ($data['token'] == Configure::read('project.token'))) {
                if (array_key_exists("code", $data)) {
                    $customer = $this->Customers->get($data['code']);
                    $this->loadModel('CobrodigitalCards');
                    $card = $this->CobrodigitalCards->find()->where(['used' => false, 'deleted' => false])->limit(1)->first();
                    if ($card) {
                        $customer->cobrodigital = true;
                        $customer->cd_barcode = $card->barcode;
                        $customer->cd_electronic_code = $card->electronic_code;
                        $this->Customers->save($customer);
                        $card->used = true;
                        $this->CobrodigitalCards->save($card);

                        $data['message'] = "Asignación de Tarjeta de Pago exitosa.";
                        $data['cobrodigital'] = true;
                        $data['barcode'] = $card->barcode;
                        $data['electronic_code'] = $card->electronic_code;
                        $data['status'] = 200;
                    } else {
                        $data['message'] = "No hay Tarjetas de Pagos disponibles.";
                        $data['cobrodigital'] = false;
                        $data['status'] = 200;
                    }
                } else {
                    $data['message'] = "Falta el campo code para realizar la busqueda";
                    $data['status'] = 400;
                }
            } else {
                $data['message'] = "Token inválido";
                $data['status'] = 401;
            }

            $this->set([
                'data'       => $data,
                '_serialize' => ['data']
            ]);
        }
    }

    public function changeLabels()
    {
        if ($this->request->is('ajax')) {

            $data_input = $this->request->input('json_decode');

            $customer = $this->Customers->get($data_input->customer_code, ['contain' => ['Labels']]);
            
            $data = $this->request->getData();
            $data['labels'] = [];
            $data['labels']['_ids'] = $data_input->labels;

            $customer = $this->Customers->patchEntity($customer, $data);

            if ($this->Customers->save($customer)) {
               $customer = $this->Customers->get($data_input->customer_code, ['contain' => ['Connections', 'Labels', 'CustomersAccounts']]);
            } else {
                $customer = false;
            }

            $this->set('customer', $customer);
        }
    }

    public function fixSaldoDebt($customer_code = NULL)
    {
        $customer = $this->Customers->newEntity();
        $customer_load = NULL;
        if ($customer_code != NULL) {
            $this->loadModel('Customers');
            $customer_load = $this->Customers
                ->find()
                ->select([
                    'code',
                    'name',
                    'ident',
                    'address',
                    'doc_type',
                    'debt_month',
                    'business_billing',
                    'billing_for_service',
                    'comments',
                ])
                ->where([
                    'code' => $customer_code
                ])->first();
        }

        if ($this->request->is('post')) {

            $action = 'Deudas extras';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $customer = $this->Customers->get($this->request->getData('customer_code_selected_from'));

            $duedate = $this->request->getData('duedate');
            $duedate = explode('/', $duedate);
            $duedate = $duedate[2] . '-' . $duedate[1] . '-' . $duedate[0] . ' 00:00:00';

            $data = [
                'concept' => $this->request->getData('concept'),
                'seating' => null,
                'customer' => $customer,
                'value' => $this->request->getData('value'),
                'alicuot' => $this->request->getData('alicuot'),
                'duedate' => new Time($duedate),
                'period' => $this->request->getData('period'),
                'connection_id' => $this->request->getData('connection_id') != 0 ? $this->request->getData('connection_id') : null,
            ];

            if ($this->CurrentAccount->addDebt($data)) {

                $this->Flash->success(__('Deuda generada correctamente.'));
                return $this->redirect(['action' => 'fixSaldoDebt', $customer->code]);
            } else {
                $this->Flash->error(__('Error al intentar generar la deuda.'));
            }
        }

        $paraments = $this->request->getSession()->read('paraments');

        //para el vencimiento
        $today = Time::now();
        if ($paraments->accountant->type == '01') { //mes vencido
            $today->modify('+1 month');
        }
        $today->day($paraments->accountant->daydue);
        $customer->duedate = $today; 

        $this->set(compact('customer', 'customer_code', 'customer_load'));
    }

    public function fixSaldoDiscount($customer_code = NULL)
    {
        $customer = $this->Customers->newEntity();
        $customer_load = NULL;
        if ($customer_code != NULL) {
            $this->loadModel('Customers');
            $customer_load = $this->Customers
                ->find()
                ->select([
                    'code',
                    'name',
                    'ident',
                    'address',
                    'doc_type',
                    'debt_month',
                    'business_billing',
                    'billing_for_service',
                    'comments',
                ])
                ->where([
                    'code' => $customer_code
                ])->first();
        }

        if ($this->request->is('post')) {

            $action = 'Descuentos extras';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $customer = $this->Customers->get($this->request->getData('customer_code_selected_from'));

            $data = [
                'concept' => $this->request->getData('concept'),
                'seating' => null,
                'customer' => $customer,
                'value' => $this->request->getData('value'),
                'alicuot' => $this->request->getData('alicuot'),
                'period' => $this->request->getData('period'),
                'connection_id' => $this->request->getData('connection_id') != 0 ? $this->request->getData('connection_id') : null,
            ];

            if ($this->CurrentAccount->addDiscountWithoutDebt($data)) {
                $this->Flash->success(__('Descuento generado correctamente.'));

                return $this->redirect(['action' => 'fixSaldoDiscount', $customer->code]);
            } else {
                $this->Flash->error(__('Error al intentar generar el Descuento.'));
            }
        }

        $this->set(compact('customer', 'customer_code', 'customer_load'));
    }

    public function updateDebt($code)
    {
        $action = 'Actualización Saldo Cliente';
        $detail = '';
        $this->registerActivity($action, $detail, NULL, TRUE);

        $customer = $this->Customers->get($code, ['contain' => ['Connections']]);

        foreach ($customer->connections as $connection) {

            $this->CurrentAccount->updateDebtMonth($code, $connection->id);
        }

        $this->CurrentAccount->updateDebtMonth($code);

        $this->Flash->success(__('Saldos recalculados.'));

        return $this->redirect(['action' => 'view', $code]);
    }

    public function fixDoc()
    {
        $this->loadModel('Customers');

        if ($this->request->is('post')) {

            $action = 'Corrección Documento Cliente';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $customs = json_decode($this->request->getData('customers'));
            $no_customers = json_decode($this->request->getData('no_customers'));
            $count = 0;
            foreach ($customs as $custom) {

                $other = $this->Customers->find()->where([
                    'doc_type' => $custom->doc_type,
                    'ident' => $custom->ident
                ])
                ->first();

                if ($other) {

                    if ($other->deleted) {
                        $error_msg = 'Existe un Cliente eliminado con el mismo doc.' . ' Para recuperarlo ir a la sección Clientes Eliminados';
                    } else {
                        $error_msg = 'Existe un Cliente cargado con el mismo doc.';
                    }

                    $no_customer = new \stdClass;
                    $no_customer->id = $custom->id;
                    $no_customer->doc_type = $custom->doc_type;
                    $no_customer->ident = $custom->ident . ' | Mensaje: ' . $error_msg;
                    array_push($no_customers, $no_customer);
                } else {
                    $customer = $this->Customers->get($custom->id);
                    $customer->doct_type = $custom->doc_type;
                    $customer->ident = $custom->ident;

                    $this->Customers->save($customer);
                    $count++;
                }
            }
            if (!empty($no_customers)) {
                $message = "Cliente cargados: $count - Clientes no cargados: ";
                foreach ($no_customers as $no_customer) {
                    $message .= 'Código: ' . $no_customer->id . ' | ' . $this->request->getSession()->read('afip_codes')['doc_types'][$no_customer->doc_type] . ': ' . $no_customer->ident;
                }
                $this->Flash->warning($message);
            } else {
                $this->Flash->success(__("Se ha modificado correctamente $count Documentos"));
            }
        }

        $customers_complete = $this->Customers
            ->find()
            ->contain([
                'Areas'
            ])->where([
                'Customers.deleted'       => false,
                'Customers.super_deleted' => false
            ]);
        $customers = [];
        foreach ($customers_complete as $customer_complete) {
            if ($this->verifyDoc($customer_complete->ident)) {
                array_push($customers, $customer_complete);
            }
        }

        $this->set(compact('customers'));
        $this->set('_serialize', ['customers']);
    }

    function verifyDoc($ident)
    {
        if ($ident == NULL || strlen($ident) < 7 || strpos($ident, '.') !== false || strpos($ident, '-') !== false) {
            return true;
        }

        return !is_numeric($ident);
    }

    private function getSourceAdministrativeMovement($code)
    {
        $this->loadModel('Connections');
        $connections = $this->Connections
            ->find()
            ->contain([
                'Users', 
                'Controllers', 
                'Services'
            ])->where([
                'customer_code' => $code
            ])->order([
                'Connections.deleted' => 'ASC'
            ]);

        $connectionsArray = [];

        foreach ($connections as $connection ) {

            if ($connection->deleted && $connection->debt_month == 0) {
                continue;
            }

            $connectionsArray[$connection->id] =  (($connection->deleted) ? '(Eliminado) ' : '') . $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address;
        }
        $connectionsArray[0] = 'Otras ventas';
        return $connectionsArray;
    }

    public function getSourceAdministrativeMovementFromAjax()
    {
        if ($this->request->is('ajax')) {

            $data_input = $this->request->input('json_decode');
            $data = [];

            if ($data_input->customer_code) {
                $data = $this->getSourceAdministrativeMovement($data_input->customer_code);
            }

            $this->set(compact('data'));
            $this->set('_serialize', ['data']);
        }
    }

    public function editAdministrativeMovement()
    {
        if ($this->request->is('ajax')) {

            $action = 'Edición movimiento (conexión)';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $data = [
                'type' => 'error',
                'msg'  => 'Error, no se ha editado correctamente'
            ];

            $data_input = $this->request->input('json_decode');
            $model = $data_input->model;
            $model_id = $data_input->model_id;
            $customer_code = $data_input->customer_code;
            $connection_id = $data_input->connection_id == 0 ? NULL : $data_input->connection_id;

            switch ($model) {

                case 'Debts':
                    $this->loadModel('CustomersHasDiscounts');
                    $customers_has_discounts = $this->CustomersHasDiscounts
                        ->find()
                        ->where([
                            'debt_id' => $model_id
                        ]);

                    if ($customers_has_discounts->count() > 0) {
                        foreach ($customers_has_discounts as $customer_has_discount) {
                            $this->editConnectionFromMovement('CustomersHasDiscounts', $customer_has_discount->id, $connection_id);
                        }
                    }
                    break;

                case 'Invoices':
                    $this->loadModel('Debts');
                    $debts = $this->Debts
                        ->find()
                        ->where([
                            'invoice_id' => $model_id
                        ]);

                    if ($debts->count() > 0) {
                        foreach ($debts as $debt) {
                            $this->editConnectionFromMovement('Debts', $debt->id, $connection_id);
                        }
                    }

                    $this->loadModel('CustomersHasDiscounts');
                    $customers_has_discounts = $this->CustomersHasDiscounts
                        ->find()
                        ->where([
                            'invoice_id' => $model_id
                        ]);

                    if ($customers_has_discounts->count() > 0) {
                        foreach ($customers_has_discounts as $customer_has_discount) {
                            $this->editConnectionFromMovement('CustomersHasDiscounts', $customer_has_discount->id, $connection_id);
                        }
                    }
                    break;

                case 'Payments':
                    $this->loadModel('Payments');
                    $payment = $this->Payments
                        ->find()
                        ->where([
                            'receipt_id' => $model_id
                        ])->first();

                    $this->editConnectionFromMovement('Receipts', $model_id, $connection_id);
                    $model_id = $payment->id;
                    break;
            }

            if ($this->editConnectionFromMovement($model, $model_id, $connection_id)) {
                $data['type'] = 'success';
                $data['msg'] = 'Se ha editado correctamente';
                $this->CurrentAccount->updateDebtMonth($customer_code, $connection_id);
            }

            $this->set('data', $data);
        }
    }

    private function editConnectionFromMovement($model, $model_id, $connection_id)
    {
        $flag = FALSE;
        $this->loadModel($model);
        $source = $this->$model
            ->find()
            ->where([
                'id' => $model_id
            ])->first();

        if ($source) {
            $source->connection_id = $connection_id;
            if ($this->$model->save($source)) {
                $flag = TRUE;
            } else {

                $data_error = new \stdClass;
                $data_error->connection_id = $connection_id;
                $data_error->model_id = $model_id;

                $event = new Event('Customers.Error', $this, [
                	'msg'   => __($model . ' error al editar connection_id de movimiento adminsitrativo.'),
                	'data'  => $data_error,
                	'flash' => false
                ]);

                $this->_ctrl->getEventManager()->dispatch($event);
            }
        } else {

            $data_error = new \stdClass;
            $data_error->connection_id = $connection_id;
            $data_error->model_id = $model_id;

            $event = new Event('Customers.Error', $this, [
            	'msg'   => __($model . ' error al editar connection_id de movimiento adminsitrativo.'),
            	'data'  => $data_error,
            	'flash' => false
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);
        }
        return $flag;
    }

    public function observations()
    {
        $paraments = $this->request->session()->read('paraments');
        $this->loadModel('Users');
        $users = $this->Users->find('list')->where(['deleted' => 0, 'id NOT IN' => $paraments->system->users]);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * Devuelve las deudas sin facturar esta mal el nombre
     */
    public function getDebtWithInvoice()
    {
        if ($this->request->is('ajax')) { //ajax detection

            $session = $this->request->getSession();
            $presale = ($session->check('presale') && strstr(strtolower($this->referer()), "presales"));

            $customer_code = $this->request->input('json_decode')->customer_code;
            $debts = $this->getDebtsWithoutInvoices($customer_code);

            if ($presale) {
                $presale = $session->read('presale');
                $debts_total = 0;
                if ($debts) {
                    $debts_total = $debts->total;
                }

                $presale->customer->debts_total = $debts_total;
                $session->write('presale', $presale);
            }

            $this->set('debts', $debts);
        }
    }

    /**
     * 
     */
    private function getDebtsWithoutInvoices($customer_code)
    {
        $this->loadModel('Debts');

        $date = Time::now();
        $date->day(1);
        $date->modify('+1 month');

        $debts = $this->Debts
            ->find()
            ->where([
                'customer_code' => $customer_code,
                'invoice_id IS' => NULL,
                'duedate <'     => $date
            ]);
        $debts = $debts->select(['total' => $debts->func()->sum('total')])->first();
        return $debts;
    }

    public function accountSummaryPDF($customer_code)
    {
        if ($this->request->is('post')) {

            $paraments = $this->request->session()->read('paraments');

            $html = $this->FiscoAfipCompPdf->account_summary($customer_code, FALSE, FALSE, NULL, $paraments->invoicing->add_cobrodigital_account_summary, $paraments->invoicing->add_payu_account_summary, $paraments->invoicing->add_cuentadigital_account_summary);

            $mpdf = new \Mpdf\Mpdf([
            	'margin_left'   => 20,
            	'margin_right'  => 15,
            	'margin_top'    => 48,
            	'margin_bottom' => 25,
            	'margin_header' => 10,
            	'margin_footer' => 10
            ]);

            $mpdf->SetProtection(array('print'));
            $pdf_title = "resumen-" . $customer_code;
            $mpdf->SetTitle($pdf_title);
            $mpdf->SetDisplayMode('fullpage');
            $mpdf->SetJS('this.print();');
            $mpdf->allow_charset_conversion = TRUE;
            $mpdf->charset_in = 'UTF-8';
            $html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');
            $mpdf->WriteHTML($html);
            $mpdf->Output($pdf_title . '.pdf', \Mpdf\Output\Destination::INLINE);

            $this->redirect($this->referer());
        }
    }

    public function lockeds()
    {
        $paraments = $this->request->getSession()->read('paraments');
        $payment_getway = $this->request->getSession()->read('payment_getway');

        $business = [];

        foreach ($paraments->invoicing->business as $b) {
            $business[$b->id] = $b->name . ' (' . $b->address. ')';
        }

        $areas = [];
        $this->loadModel('Areas');
        $areas_model = $this->Areas->find();
        foreach ($areas_model as $a) {
            $areas[$a->id] = $a->name;
        }

        $services = [];
        $this->loadModel('Services');
        $services_model = $this->Services->find();
        foreach ($services_model as $a) {
            $services[$a->id] = $a->name;
        }

        $controllers = [];
        $this->loadModel('Controllers');
        $controllers_model = $this->Controllers
            ->find()
            ->where([
        		'enabled' => TRUE,
        		'deleted' => FALSE
        	]);
        foreach ($controllers_model as $c) {
            $controllers[$c->id] = $c->name;
        }

        $users = [];
        $this->loadModel('Users');
        $users_model = $this->Users->find();
        foreach ($users_model as $user) {
            $users[$user->id] = $user->name;
        }

        $accounts = [];
        foreach ($payment_getway->methods as $pg) {
            $config = $pg->config;
            if ($payment_getway->config->$config->enabled && $pg->credential) {
                $accounts[$pg->id] = $pg->name;
            }
        }

        if (sizeof($accounts) == 0) {
            $accounts[""] = "Sin Cuentas";
        }

        $this->loadModel('Labels');
        $labels_array = $this->Labels->find()->where(['entity' => 0])->toArray();

        $labels = [];
        $labels[] = [
            'id' => 0,
            'text' => 'Sin etiquetas',
            'entity' => 0,
            'color_back' => '#000000',
            'color_text' => '#000000',
            'enabled' => 1,
        ];

        foreach ($labels_array as $label) {
            $labels[] = $label;
        }

        $this->set(compact('paraments', 'areas', 'services', 'users', 'labels', 'accounts', 'controllers', 'business'));
        $this->set('_serialize', ['customers']);
    }

    public function applyMassiveDebt()
    {
        if ($this->request->is('ajax')) {

            $paraments = $this->request->getSession()->read('paraments');
            $afip_codes = $this->request->getSession()->read('afip_codes');

            $response = new \stdClass;
            $response->error = false;
            $response->msg = '';
            $response->typeMsg = 'success';

            $data = $this->request->input('json_decode');
            $ids = $data->ids;

            $amount = count($ids);

            $success = 0;
            $error = 0;

            $this->loadModel('DebitNotes');

            $action = 'Aplicación Masiva de Deudas';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $customers_codes = "";
            $first = TRUE;

            foreach ($ids as $id) {

                $customer = $this->Customers->get($id, [
                    'contain' => ['Cities']
                ]);

                switch ($data->surcharge_type) {

                    case 'next_month':

                        $duedate = explode("/", $data->duedate);
                        $duedate = $duedate[2] . '-' . $duedate[1] . '-' . $duedate[0] . '00:00:00';
                        $duedate = new Time($duedate);

                        $debt_data = [
                            'concept'              => $data->concept,
                            'seating'              => NULL,
                            'customer'             => $customer,
                            'value'                => $data->importe,
                            'alicuot'              => 5,
                            'duedate'              => $duedate,
                            'connection_id'        => NULL,
                            'is_presupuesto_force' => $data->presupuesto,
                            'period'               => $data->periode,
                        ];

                        if ($this->CurrentAccount->addDebt($debt_data, FALSE)) {
                            $success++;
                        } else {
                            $error++;
                        }
                        break;

                    case 'this_month':

                        $tipo_comp = 'NDX';

                        if (!$data->presupuesto) {

                            if ($customer->is_presupuesto) {

                                $tipo_comp ='NDX';

                            } else {

                                foreach ($paraments->invoicing->business as $business) {

                                    if ($business->id == $customer->business_billing) {
                                        $customer->business = $business;
                                    }
                                }

                                $tipo_comp = $afip_codes['combinationsDebitNotes'][$customer->business->responsible][$customer->responsible];
                            }

                        }

                        $row = $this->CurrentAccount->calulateIvaAndNeto($data->importe, $afip_codes['alicuotas_percectage'][5], 1);

                        $concepts = [];

                        $concept = new \stdClass;
                        $concept->type        = 'S';
                        $concept->code        = 99;
                        $concept->description = $data->concept;
                        $concept->quantity    = 1;
                        $concept->unit        = 7;
                        $concept->price       = $row->price;
                        $concept->discount    = 0;
                        $concept->sum_price   = $row->sum_price;;
                        $concept->tax         = 5;
                        $concept->sum_tax     = $row->sum_tax;
                        $concept->total       = $row->total;
                        $concepts[0] = $concept;

                        $debitNote  = [
                            'customer'      => $customer,
                            'concept_type'  => 2,
                            'date'          => Time::now(),
                            'date_start'    => Time::now(),
                            'date_end'      => Time::now(),
                            'duedate'       => Time::now(),
                            'concepts'      => $concepts,
                            'tipo_comp'     => $tipo_comp,
                            'comments'      => $data->concept,
                            'manual'        => TRUE,
                            'business_id'   => $customer->business_billing,
                            'connection_id' => NULL
                        ];

                        $debitNote = $this->FiscoAfipComp->debitNote($debitNote, FALSE);

                        if ($debitNote) {

                           //informo afip si no es de tipo X

                           if ($this->FiscoAfip->informar($debitNote, FiscoAfipComp::TYPE_DEBIT_NOTE)) {

                               if ($this->DebitNotes->save($debitNote)) {
                                   $success++;
                                   if ($first) {
                                        $customers_codes .= $id;
                                        $first = FALSE;
                                    } else {
                                        $customers_codes .= '.' . $id;
                                    }
                               } else {
                                   $error++;
                               }
                           } else {
                               $this->FiscoAfipComp->rollbackInvoice($debitNote, TRUE);
                               $error++;
                           }

                        } else {
                            $this->FiscoAfipComp->rollbackInvoice($debitNote, TRUE);
                            $error++;
                        }
                        break;
                }
            }

            if ($customers_codes != "") {
                $command = ROOT . "/bin/cake debtMonthControlAfterAction $customers_codes 0 > /dev/null &";
                $result = exec($command);
            }

            $response->msg = __('{0} Clientes seleccionados. <br> {1} Clientes generaron recargos. <br> {2} Clientes no generaron recargos.', $amount, $success, $error);

            $this->set('response', $response);
        }
    }

    public function applyMassiveDiscount()
    {
        if ($this->request->is('ajax')) {

            $paraments = $this->request->getSession()->read('paraments');
            $afip_codes = $this->request->getSession()->read('afip_codes');

            $response = new \stdClass;
            $response->error = false;
            $response->msg = '';
            $response->typeMsg = 'success';

            $data = $this->request->input('json_decode');
            $ids = $data->ids;

            $amount = count($ids);

            $success = 0;
            $error = 0;

            $this->loadModel('CreditNotes');

            $action = 'Aplicación Masiva de Descuentos';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $customers_codes = "";
            $first = TRUE;

            foreach ($ids as $id) {

                $customer = $this->Customers->get($id, [
                    'contain' => ['Cities']
                ]);

                switch ($data->surcharge_type) {

                    case 'next_month':

                        $discount_data = [
                            'concept'              => $data->concept,
                            'seating'              => NULL,
                            'customer'             => $customer,
                            'value'                => $data->importe,
                            'alicuot'              => 5,
                            'connection_id'        => NULL,
                            'is_presupuesto_force' => $data->presupuesto,
                            'period'               => $data->periode,
                        ];

                        if ($this->CurrentAccount->addDiscountWithoutDebt($discount_data, FALSE)) {
                            $success++;
                        } else {
                            $error++;
                        }
                        break;

                    case 'this_month':

                        $tipo_comp = 'NCX';

                        if (!$data->presupuesto) {

                            if ($customer->is_presupuesto) { 

                                $tipo_comp ='NCX';

                            } else {

                                foreach ($paraments->invoicing->business as $business) {

                                    if ($business->id == $customer->business_billing) {
                                        $customer->business = $business;
                                    }
                                }

                                $tipo_comp = $afip_codes['combinationsCreditNotes'][$customer->business->responsible][$customer->responsible];
                            }

                        }

                        $row = $this->CurrentAccount->calulateIvaAndNeto($data->importe, $afip_codes['alicuotas_percectage'][5], 1);

                        $concepts = [];

                        $concept = new \stdClass;
                        $concept->type        = 'S';
                        $concept->code        = 99;
                        $concept->description = $data->concept;
                        $concept->quantity    = 1;
                        $concept->unit        = 7;
                        $concept->price       = $row->price;
                        $concept->discount    = 0;
                        $concept->sum_price   = $row->sum_price;;
                        $concept->tax         = 5;
                        $concept->sum_tax     = $row->sum_tax;
                        $concept->total       = $row->total;
                        $concepts[0] = $concept;

                        $creditNote  = [
                            'customer'      => $customer,
                            'concept_type'  => 2,
                            'date'          => Time::now(),
                            'date_start'    => Time::now(),
                            'date_end'      => Time::now(),
                            'duedate'       => Time::now(),
                            'concepts'      => $concepts,
                            'tipo_comp'     => $tipo_comp,
                            'comments'      => $data->concept,
                            'manual'        => TRUE,
                            'business_id'   => $customer->business_billing,
                            'connection_id' => NULL
                        ];

                        $creditNote = $this->FiscoAfipComp->creditNote($creditNote, FALSE);

                        if ($creditNote) {

                           //informo afip si no es de tipo X

                           if ($this->FiscoAfip->informar($creditNote, FiscoAfipComp::TYPE_CREDIT_NOTE)) {

                               if ($this->CreditNotes->save($creditNote)) {
                                   $success++;
                                   if ($first) {
                                        $customers_codes .= $id;
                                        $first = FALSE;
                                    } else {
                                        $customers_codes .= '.' . $id;
                                    }
                               } else {
                                   $error++;
                               }
                           } else {
                               $this->FiscoAfipComp->rollbackInvoice($creditNote, TRUE);
                               $error++;
                           }

                        } else {
                            $this->FiscoAfipComp->rollbackInvoice($creditNote, TRUE);
                            $error++;
                        }
                        break;
                }
            }

            if ($customers_codes != "") {
                $command = ROOT . "/bin/cake debtMonthControlAfterAction $customers_codes 0 > /dev/null &";
                $result = exec($command);
            }

            $response->msg = __('{0} Clientes seleccionados. <br> {1} Clientes generaron descuentos. <br> {2} Clientes no generaron descuentos.', $amount, $success, $error);

            $this->set('response', $response);
        }
    }

    public function editMasive()
    {
        if ($this->request->is('post')) {

            $helper = new Helper\Sample();

            $inputFileName = WWW_ROOT . 'customers/clientes.xlsx';

            $spreadsheet = IOFactory::load($inputFileName);

            $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

            $this->loadModel('Customers');
            $this->loadModel('Cities');
            $this->loadModel('Areas');

            // var_dump($sheetData);

            $i = 0;
            $total = 0;
            $ok = 0;
            $error = 0;

            foreach($sheetData as $row) {

                $i++;

                if ($i < 3) { //para saltar header u description del excel
                    continue;
                }
                $total++;

                //busco el cliente ...

                $customer = $this->Customers->find()->where(['code' => intval(trim($row['A']))])->first();

                if (!$customer) {
                    // Debug($row); die('cliente no econtrado');
                    continue;
                }

                // c - codigo
                // d - editeca

                $doc_type = intval(trim($row['D']));

                $ident = intval(trim($row['E']));
                $ident = str_replace('-', '', $ident);

                $city = $this->Cities->find()->where(['cp' => trim($row['I'])])->first();

                if (!$city) {

                    Debug($row); die('codigo postal no encontrado.');
                }

                $area = $this->Areas->find()->where(['city_id' => $city->id])->first();

                $codigo_cobro_digital = trim($row['L']);
                $codigo_cuenta_digital = trim($row['M']);
                $codigo_payu_digital = trim($row['N']);

                $saldo = floatval($row['O']);

                // $business_billing_id = 1526130279;

                $phone = trim($row['G']);

                $data = [
                    'code'                      => intval($row['A']),
                	'name'                      => trim($row['B']),
                	'address'                   => trim($row['C']),
                	'doc_type'                  => $doc_type,
                	'ident'                     => $ident,
                	'responsible'               => intval(trim($row['F'])),
                	'phone'                     => $phone ? $phone : 'Incompleto',
                	'phone_alt'                 => trim($row['H']),
                	'city_id'                   => $city->id,
                	'province_id'               => $city->province_id,
                	'country_id'                => '1',
                	'area_id'                   => $area->id,
                	'email'                     => trim($row['J']),
                	'is_presupuesto'            => intval(trim($row['K'])),
                	'payment_method_default_id' => '1',
                	'lat'                       => null,
                	'lng'                       => null,
                	'denomination'              => $doc_type != 99 ? true : false,
                // 	'cond_venta'                => '96',
                // 	'business_billing'          => $business_billing_id,
                	'daydue'                    => 10,
                	'seller'                    => 'Admin',
                	'comments'                  => '',
                	'user_id'                   => 100,
                	'billing_for_service'       => false
                ];

                $customer = $this->Customers->newEntity();
                $customer = $this->Customers->patchEntity($customer, $data);

                if (count($customer->errors()) > 0) {
                    Debug($customer);
                    $error++;
                }

                if ($save) {

                    if ($this->Customers->save($customer)) {

                        $ok++;

                    } else {
                        $error++;
                    }
                }
            }

            if ($save) {
                die(__('SAVE: TRUE. total: {0}. clientes agregados: {1}. clientes no cargados: {2} ', $total, $ok, $error));
                $this->Flash->success(__("Archivos cargado"));
            } else {
                die(__('SAVE: FALSE. total: {0}. errores: {1} ', $total, $error));
            }

            return $this->redirect(['action' => 'editMasive']);
        }
    }

    public function uploadFileEditMasive()
    {
        $c = 0;

        //$files = array_filter($_FILES['upload']['name']); //something like that to be used before processing files.

        // Count # of uploaded files in array
        $total = count($_FILES['upload']['name']);

        // Loop through each file
        for ($i=0 ; $i < $total ; $i++) {

          //Get the temp file path
          $tmpFilePath = $_FILES['upload']['tmp_name'][$i];

          //Make sure we have a file path
          if ($tmpFilePath != "") {
            //Setup our new file path
            $newFilePath = WWW_ROOT . "customers/" . $_FILES['upload']['name'][$i];

            //Upload the file into the temp dir
            if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                $c++;
            }
          }
        }

        $this->Flash->success(__("Archivos cargado"));

        return $this->redirect(['action' => 'editMasive']);
    }

    public function massiveEdition()
    {
        if ($this->request->is('post')) {

            $error = $this->isReloadPage();

            if (!$error) {

                $data = json_decode($this->request->getData('data'));
                $ids = $data->ids;

                $i = 0;
                $c = count($ids);
                $customers = $this->Customers->find()->where(['code IN' => $ids]);

                foreach($customers as $customer){

                    if ($data->area !='') {
                        $customer->area_id =  $data->area;
                    }

                    if ($data->doc_type !='') {
                        $customer->doc_type =  $data->doc_type;
                    }

                    if ($data->responsible != '') {
                        $customer->responsible =  $data->responsible == '1';
                    }

                    if ($data->is_presupuesto != '') {
                        $customer->is_presupuesto =  $data->is_presupuesto == '1';
                    }

                    if ($data->business_billing != '') {
                        $customer->business_billing =  $data->business_billing;
                    }

                    if ($this->Customers->save($customer)) {
                        $i++;
                    }
                
                }

                $this->Flash->success(__("Se editaron $i/$c clientes"));
                return $this->redirect($this->referer());
            } else {

                $this->Flash->warning(__('La misma acción se intento realizar varias veces.'));
                return $this->redirect(['action' => 'lockeds']);
            }
        }
    }
}

class data {}

class CuitValidator {
	static function isValid($cuit) {
		$digits = array();
		if (strlen($cuit) != 13) return false;
		for ($i = 0; $i < strlen($cuit); $i++) {
			if ($i == 2 or $i == 11) {
				if ($cuit[$i] != '-') return false;
			} else {
				if (!ctype_digit($cuit[$i])) return false;
				if ($i < 12) {
					$digits[] = $cuit[$i];
				}
			}
		}
		$acum = 0;
		foreach (array(5, 4, 3, 2, 7, 6, 5, 4, 3, 2) as $i => $multiplicador) {
			$acum += $digits[$i] * $multiplicador;
		}
		$cmp =  ($acum % 11);
		if ($cmp == 11) $cmp = 0;
		if ($cmp == 10) $cmp = 9;
		return ($cuit[12] == $cmp);
	}
}

class Move {

    public $id;
    public $date;
    public $tipo_comp;
    public $destino;
    public $pto_vta;
    public $num;
    public $cae;
    public $quantity;
    public $subtotal;
    public $sum_tax;
    public $total; 
    public $paid; 
    public $duedate;
    public $description;
    public $saldo;
    public $username;

    public $connection_id;

    public $customers_has_discount;

    public $type_move;

    public $customer;

    public $seating_number;
    public $anulated;

    public function __construct($data, $type) {

        switch ($type) {

            case 'invoice':

                $this->id = $data->id;
                $this->date = $data->date;
                $this->tipo_comp = $data->tipo_comp;
                $this->destino = '';
                $this->pto_vta = $data->pto_vta;
                $this->num = $data->num;
                $this->cae = $data->cae;
                $this->quantity = '';
                $this->subtotal = $data->subtotal;
                $this->sum_tax = $data->sum_tax;
                $this->total = $data->total;
                $this->paid = $data->paid;
                $this->duedate = $data->duedate;
                $this->description = $data->comments;
                $this->saldo = 0;
                $this->username = $data->user->username;
                $this->cash_entity_name = '';

                $this->connection_id = $data->connection_id;

                $this->type_move = $type;
                $this->model = 'Invoices';

                $this->customer = $data->customer;

                $this->seating_number = $data->seating_number;
                $this->anulated = $data->anulated;

                break;

            case 'payment':

                $this->id = $data->receipt->id;
                $this->date = $data->created;
                $this->tipo_comp = $data->receipt->tipo_comp;
                $this->destino = '';
                $this->pto_vta = $data->receipt->pto_vta;
                $this->num = $data->receipt->num;
                $this->cae = '';
                $this->quantity = '';
                $this->subtotal = '';
                $this->sum_tax = '';
                $this->total = -$data->import;
                $this->paid = '';
                $this->duedate = null;
                $this->description = $data->concept;
                $this->saldo = 0;
                $this->username = $data->user->username;
                $this->cash_entity_name = '';
                $this->payment_method_id = $data->payment_method_id;
                $this->payment_id = $data->id;

                if (!empty($data->cash_entity)) {
                    $this->cash_entity_name = $data->cash_entity->name;
                }

                $this->connection_id = $data->receipt->connection_id;

                $this->customer = $data->customer;

                $this->type_move = $type;
                $this->model = 'Payments';

                $this->seating_number = $data->seating_number;
                $this->anulated = "";

                break;

            case 'debit':

                $this->id = $data->id;
                $this->date = $data->date;
                $this->tipo_comp = $data->tipo_comp;
                $this->destino = '';
                $this->pto_vta = $data->pto_vta;
                $this->num = $data->num;
                $this->cae = $data->cae;
                $this->subtotal = $data->subtotal;
                $this->sum_tax = $data->sum_tax;
                $this->total = $data->total;
                $this->paid = '';
                $this->duedate = $data->duedate;
                $this->description = $data->comments;
                $this->saldo = 0;
                $this->username = $data->user->username;

                $this->cash_entity_name = null;

                $this->connection_id = $data->connection_id;

                $this->customer = $data->customer;

                $this->type_move = $type;
                $this->model = 'DebitNotes';

                $this->seating_number = $data->seating_number;
                $this->anulated = "";

                break;

            case 'credit':

                $this->id = $data->id;
                $this->date = $data->date;
                $this->tipo_comp = $data->tipo_comp;
                $this->destino = '';
                $this->pto_vta = $data->pto_vta;
                $this->num = $data->num;
                $this->cae = $data->cae;
                $this->subtotal = $data->subtotal;
                $this->sum_tax = $data->sum_tax;
                $this->total = -$data->total;
                $this->paid = '';
                $this->duedate = null;
                $this->description = $data->comments;
                $this->saldo = 0;
                $this->username = $data->user->username;

                $this->cash_entity_name = null;

                $this->connection_id = $data->connection_id;

                $this->customer = $data->customer;

                $this->type_move = $type;
                $this->model = 'CreditNotes';

                $this->seating_number = $data->seating_number;
                $this->anulated = "";

                break;
        }
    }
}

class MovewithoutInvoice {

    public $id;
    public $date;
    public $destino;
    public $quantity;
    public $subtotal;
    public $sum_tax;
    public $total; 
    public $paid; 
    public $duedate;
    public $description;
    public $saldo;
    public $username;
    public $period;
    public $seating_number;

    public $connection_id;

    public $customers_has_discount;

    public $type_move;

    public $customer;

    public function __construct($data, $type) {

        switch ($type) {

            case 'debt':

                $this->id = $data->id;
                $this->date = $data->created;
                $this->tipo_comp = $data->tipo_comp;
                $this->destino = $data->tipo_comp;
                $this->quantity = $data->quantity;
                $this->subtotal = $data->sum_price;
                $this->sum_tax = $data->sum_tax;
                $this->total = $data->total;
                $this->duedate = $data->duedate;
                $this->description = $data->description;
                $this->saldo = 0;
                $this->username = $data->user->username;
                $this->period = $data->period;

                $this->connection_id = $data->connection_id;

                $this->customers_has_discount = $data->customers_has_discount;

                $this->type_move = $type;
                $this->model = 'Debts';

                $this->customer = $data->customer;

                $this->seating_number = $data->seating_number;

                break;

            case 'customer_has_discount':

                $this->id = $data->id;
                $this->date = $data->created;
                $this->tipo_comp = $data->tipo_comp;
                $this->destino = $data->tipo_comp;
                $this->quantity = 1;
                $this->subtotal = $data->sum_price;
                $this->sum_tax = $data->sum_tax;
                $this->total = -$data->total;
                $this->duedate = '';
                $this->description = $data->description;
                $this->saldo = 0;
                $this->username = $data->user->username;
                $this->period = $data->period;

                $this->connection_id = $data->connection_id;

                $this->customer = $data->customer;

                $this->type_move = $type;
                $this->model = 'CustomersHasDiscounts';

                $this->seating_number = $data->seating_number;

                break;
        }
    }
}

class MoveAll {

    public $id;
    public $date;
    public $customer_code;
    public $customer_name;
    public $customer_ident;
    public $customer_doc_type;
    public $tipo_comp;
    public $destino;
    public $pto_vta;
    public $num;
    public $cae;
    public $quantity;
    public $subtotal;
    public $sum_tax;
    public $total; 
    public $paid; 
    public $duedate;
    public $description;
    public $saldo;
    public $username;

    public $connection_id;

    public $customers_has_discount;

    public $type_move;

    public $customer;

    public $seating_number;
    public $anulated;

    public function __construct($data, $type) {

        switch ($type) {

            case 'invoice':

                $this->id = $data->id;
                $this->date = $data->date;
                $this->customer_code = $data->customer_code;
                $this->customer_name = $data->customer_name;
                $this->customer_ident = $data->customer_ident;
                $this->customer_doc_type = $data->customer_doc_type;
                $this->tipo_comp = $data->tipo_comp;
                $this->destino = '';
                $this->pto_vta = $data->pto_vta;
                $this->num = $data->num;
                $this->cae = $data->cae;
                $this->quantity = '';
                $this->subtotal = $data->subtotal;
                $this->sum_tax = $data->sum_tax;
                $this->total = $data->total;
                $this->paid = $data->paid;
                $this->duedate = $data->duedate;
                $this->description = $data->comments;
                $this->saldo = 0;
                $this->username = $data->user->username;
                $this->cash_entity_name = '';

                $this->connection_id = $data->connection_id;

                $this->type_move = $type;
                $this->model = 'Invoices';

                $this->customer = $data->customer;

                $this->seating_number = $data->seating_number;
                $this->anulated = $data->anulated;

                break;

            case 'payment':

                $this->id = $data->receipt->id;
                $this->date = $data->created;
                $this->customer_code = $data->customer->code;
                $this->customer_name = $data->customer->name;
                $this->customer_ident = $data->customer->ident;
                $this->customer_doc_type = $data->customer->doc_type;
                $this->tipo_comp = $data->receipt->tipo_comp;
                $this->destino = '';
                $this->pto_vta = $data->receipt->pto_vta;
                $this->num = $data->receipt->num;
                $this->cae = '';
                $this->quantity = '';
                $this->subtotal = '';
                $this->sum_tax = '';
                $this->total = -$data->import;
                $this->paid = '';
                $this->duedate = null;
                $this->description = $data->concept;
                $this->saldo = 0;
                $this->username = $data->user->username;
                $this->cash_entity_name = '';
                $this->payment_method_id = $data->payment_method_id;
                $this->payment_id = $data->id;

                if (!empty($data->cash_entity)) {
                    $this->cash_entity_name = $data->cash_entity->name;
                }

                $this->connection_id = $data->receipt->connection_id;

                $this->customer = $data->customer;

                $this->type_move = $type;
                $this->model = 'Payments';

                $this->seating_number = $data->seating_number;
                $this->anulated = "";

                break;

            case 'debit':

                $this->id = $data->id;
                $this->date = $data->date;
                $this->customer_code = $data->customer_code;
                $this->customer_name = $data->customer_name;
                $this->customer_ident = $data->customer_ident;
                $this->customer_doc_type = $data->customer_doc_type;
                $this->tipo_comp = $data->tipo_comp;
                $this->destino = '';
                $this->pto_vta = $data->pto_vta;
                $this->num = $data->num;
                $this->cae = $data->cae;
                $this->subtotal = $data->subtotal;
                $this->sum_tax = $data->sum_tax;
                $this->total = $data->total;
                $this->paid = '';
                $this->duedate = $data->duedate;
                $this->description = $data->comments;
                $this->saldo = 0;
                $this->username = $data->user->username;

                $this->cash_entity_name = null;

                $this->connection_id = $data->connection_id;

                $this->customer = $data->customer;

                $this->type_move = $type;
                $this->model = 'DebitNotes';

                $this->seating_number = $data->seating_number;
                $this->anulated = "";

                break;

            case 'credit':

                $this->id = $data->id;
                $this->date = $data->date;
                $this->customer_code = $data->customer_code;
                $this->customer_name = $data->customer_name;
                $this->customer_ident = $data->customer_ident;
                $this->customer_doc_type = $data->customer_doc_type;
                $this->tipo_comp = $data->tipo_comp;
                $this->destino = '';
                $this->pto_vta = $data->pto_vta;
                $this->num = $data->num;
                $this->cae = $data->cae;
                $this->subtotal = $data->subtotal;
                $this->sum_tax = $data->sum_tax;
                $this->total = -$data->total;
                $this->paid = '';
                $this->duedate = null;
                $this->description = $data->comments;
                $this->saldo = 0;
                $this->username = $data->user->username;

                $this->cash_entity_name = null;

                $this->connection_id = $data->connection_id;

                $this->customer = $data->customer;

                $this->type_move = $type;
                $this->model = 'CreditNotes';

                $this->seating_number = $data->seating_number;
                $this->anulated = "";

                break;
        }
    }
}
