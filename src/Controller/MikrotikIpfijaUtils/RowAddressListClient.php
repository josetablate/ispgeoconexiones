<?php
namespace App\Controller\MikrotikIpfijaUtils;

use App\Controller\Utils\Attr;

class RowAddressListClient {
   
  
    public $marked_to_delete;
    
    public $id;
    
    public $connection_id;
    
    public $api_id;
    public $list;
    public $address;
    public $comment;
    
    public $other;
    
    public function __construct($addressList, $side) {
        
        $this->other = $addressList['other'];
        
        $this->marked_to_delete = false;
        
        if($side == 'A'){
            $this->id = $addressList['id'];
            $this->connection_id = $addressList['connection']['id'];
            
        }
        
        $this->api_id = new Attr($addressList['api_id'], true);
        $this->list = new Attr($addressList['list'], true);
        $this->address = new Attr($addressList['address'], true);
    
        
        if($side == 'A'){
            $this->comment = new Attr($addressList['comment'], true);
        }
        else{
            $this->comment = new Attr($this->convert_to($addressList['comment'], "UTF-8"), true);
        }
     
    }
    
    
    public function setNew(){
        
        $this->marked_to_delete = true;
        
        $this->api_id->state = false;
        $this->list->state = false;
        $this->address->state = false;
        $this->comment->state = false;
    }
    
    private function convert_to( $source, $target_encoding )  {
        
        if($source != ''){
            
            // detect the character encoding of the incoming file
            $encoding = mb_detect_encoding( $source, "auto" );
               
            // escape all of the question marks so we can remove artifacts from
            // the unicode conversion process
            $target = str_replace( "?", "[question_mark]", $source );
               
            // convert the string to the target encoding
            $target = mb_convert_encoding( $target, $target_encoding, $encoding);
               
            // remove any question marks that have been introduced because of illegal characters
            $target = str_replace( "?", "", $target );
               
            // replace the token string "[question_mark]" with the symbol "?"
            $target = str_replace( "[question_mark]", "?", $target );
           
            return $target;
        }
        
         return $source;
    }
    
}
