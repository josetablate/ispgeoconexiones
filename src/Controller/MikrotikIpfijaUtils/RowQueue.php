<?php
namespace App\Controller\MikrotikIpfijaUtils;

use App\Controller\Utils\Attr;

class RowQueue {
   
   
    public $queue_id;
    public $connection_id;
    
    public $marked_to_delete;
    
    public $api_id;
    public $name;
    public $comment;
    
    public $target;
    public $max_limit;
    public $limit_at;
    public $burst_limit;
    public $burst_threshold;
    public $burst_time;
    public $priority;
    public $queue;
    
    public $other;
    
    public function __construct($queue, $side) {
        
        $this->other = $queue['other'];
        
        $this->marked_to_delete = false;
        
        if($side == 'A'){
            $this->queue_id = $queue['id'];
            $this->connection_id = $queue['connection']['id'];
        }
 
        $this->api_id = new Attr($queue['api_id'], true);     
        
        $this->target = new Attr($queue['target'], true);
        
        $this->max_limit = new Attr($queue['max_limit'], true);
        $this->limit_at = new Attr($queue['limit_at'], true);
        $this->burst_limit = new Attr($queue['burst_limit'], true);
        $this->burst_threshold = new Attr($queue['burst_threshold'], true);
        $this->burst_time = new Attr($queue['burst_time'], true);
        $this->priority = new Attr($queue['priority'], true);
        $this->queue = new Attr($queue['queue'], true);
         
        if($side == 'A'){
            $this->name = new Attr($queue['name'], true);
            $this->comment = new Attr($queue['comment'], true);
        }else{
            $this->name = new Attr($this->convert_to($queue['name'], "UTF-8"), true);
            $this->comment = new Attr($this->convert_to($queue['comment'], "UTF-8"), true);
        }
    
    }
    
    
    public function setNew(){
        
        $this->marked_to_delete = true;
        
        $this->api_id->state = false;
        $this->name->state = false;
        $this->target->state = false;
        $this->comment->state = false;
        $this->max_limit->state = false;
        $this->limit_at->state = false;
        $this->burst_limit->state = false;
        $this->burst_threshold->state = false;
        $this->burst_time->state = false;
        $this->priority->state = false;
        $this->queue->state = false;

    }
     
     private function convert_to( $source, $target_encoding )  {
        
        if($source != ''){
            
            // detect the character encoding of the incoming file
            $encoding = mb_detect_encoding( $source, "auto" );
               
            // escape all of the question marks so we can remove artifacts from
            // the unicode conversion process
            $target = str_replace( "?", "[question_mark]", $source );
               
            // convert the string to the target encoding
            $target = mb_convert_encoding( $target, $target_encoding, $encoding);
               
            // remove any question marks that have been introduced because of illegal characters
            $target = str_replace( "?", "", $target );
               
            // replace the token string "[question_mark]" with the symbol "?"
            $target = str_replace( "[question_mark]", "?", $target );
           
            return $target;
        }
        
         return $source;
    }
    
}
