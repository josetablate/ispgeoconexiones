<?php
namespace App\Controller\MikrotikIpfijaUtils;

use App\Controller\Utils\Attr;

class RowArp {
   
   
    public $arp_id;
    public $connection_id;
    
    public $marked_to_delete;
    
    public $api_id;
    public $address;
    public $mac_address;
    
    public $interface;
    public $comment;
    
    public $other;

    
    public function __construct($arp, $side) {
        
        $this->other = $arp['other'];
        
        $this->marked_to_delete = false;
        
        if($side == 'A'){
            $this->arp_id = $arp['id'];
            $this->connection_id = $arp['connection']['id'];
        }
       
        $this->api_id = new Attr(strtoupper($arp['api_id']), true);
        $this->address = new Attr($arp['address'], true);
        $this->mac_address = new Attr($arp['mac_address'], true);
        
        if($side == 'A'){
            $this->comment = new Attr($arp['comment'], true);
        }else{
            $this->comment = new Attr($this->convert_to($arp['comment'], "UTF-8"), true);
            
        }
        
        $this->interface = new Attr($arp['interface'], true);
    }
    
    
    public function setNew(){
        
        $this->marked_to_delete = true;
        
        $this->api_id->state = false;
        $this->address->state = false;
        $this->mac_address->state = false;
        $this->interface->state = false;
        $this->comment->state = false;

    }
    
    private function convert_to( $source, $target_encoding )  {
        
        if($source != ''){
            
            // detect the character encoding of the incoming file
            $encoding = mb_detect_encoding( $source, "auto" );
               
            // escape all of the question marks so we can remove artifacts from
            // the unicode conversion process
            $target = str_replace( "?", "[question_mark]", $source );
               
            // convert the string to the target encoding
            $target = mb_convert_encoding( $target, $target_encoding, $encoding);
               
            // remove any question marks that have been introduced because of illegal characters
            $target = str_replace( "?", "", $target );
               
            // replace the token string "[question_mark]" with the symbol "?"
            $target = str_replace( "[question_mark]", "?", $target );
           
            return $target;
        }
        
         return $source;
    }
    
}