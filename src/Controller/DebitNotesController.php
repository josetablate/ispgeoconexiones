<?php
namespace App\Controller;
 
use App\Controller\AppController;
use App\Controller\Component\Admin\FiscoAfipComp;
use App\Controller\Component\Admin\FiscoAfipCompPdf;
use Cake\I18n\Time;

/**
 * DebitNotes Controller
 *
 * @property \App\Model\Table\DebitNotesTable $DebitNotes
 */
class DebitNotesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('FiscoAfip', [
             'className' => '\App\Controller\Component\Admin\FiscoAfip'
        ]);

        $this->loadComponent('FiscoAfipComp', [
             'className' => '\App\Controller\Component\Admin\FiscoAfipComp'
        ]);

        $this->loadComponent('FiscoAfipCompPdf', [
             'className' => '\App\Controller\Component\Admin\FiscoAfipCompPdf'
        ]);

        $this->loadComponent('Accountant', [
             'className' => '\App\Controller\Component\Admin\Accountant'
        ]);

        $this->loadComponent('CurrentAccount', [
            'className' => '\App\Controller\Component\Admin\CurrentAccount'
        ]);
    }

    public function isAuthorized($user = null)
    {
        if ($this->request->getParam('action') == 'getDebitNotes') {
            return true;
        }

        if ($this->request->getParam('action') == 'getDebitnotesCustomer') {
            return true;
        }

        if ($this->request->getParam('action') == 'delete') {
            return true;
        }

        if ($this->request->getParam('action') == 'addRecargos') {
            return true;
        }

        if ($this->request->getParam('action') == 'apply') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function index()
    {
        $debitNotes = $this->DebitNotes->find()->contain(['Users']);

        $this->loadModel('MassEmailsTemplates');
        $mass_emails_templates_model = $this->MassEmailsTemplates
            ->find()
            ->where([
                'enabled' => TRUE
            ]);

        $mass_emails_templates = [
            '' => 'Seleccione'
        ];

        foreach ($mass_emails_templates_model as $mass_email_template_model) {
            $mass_emails_templates[$mass_email_template_model->id] = $mass_email_template_model->name;
        }

        $this->set(compact('debitNotes', 'mass_emails_templates'));
        $this->set('_serialize', ['debitNotes']);
    }

    public function getAll_OLd()
    {
        $debitnotes = $this->DebitNotes->find()->contain(['Customers','Users']);

        $this->loadModel('CompAsociados');

        $this->set(compact('debitnotes'));
        $this->set('_serialize', ['debitnotes']);
    }

    public function getDebitNotes()
    {
        if ($this->request->is('ajax')) {

            $response =  new \stdClass();

            $response->draw = intval($this->request->getQuery('draw'));

            $customer_ids_disabled = $this->getCustomersIdsBusinessDisabled();

            $where = [];
            if(count($customer_ids_disabled) > 0){
                $where += ['DebitNotes.customer_code NOT IN' => $customer_ids_disabled];
            }

            if (null !== $this->request->getQuery('where')) {
                $where += $this->request->getQuery('where');
                $response->recordsTotal = $this->DebitNotes->find()->where($where)->count();
            } else {
                $response->recordsTotal = $this->DebitNotes->find()->where($where)->count();
            }

            $response->data = $this->DebitNotes->find('ServerSideData', [
                'params' => $this->request->getQuery(),
                'where' => count($where) > 0 ?  $where : false
            ]);

            $response->recordsFiltered = $this->DebitNotes->find('RecordsFiltered', [
                'params' => $this->request->getQuery(),
                'where' =>  count($where) > 0 ?  $where : false
            ]);

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function getDebitnotesCustomer()
    {
        if ($this->request->is('ajax')) {

            $code = $this->request->getQuery('customer_code');

            $debitnotes = $this->DebitNotes->find()->contain(['Users'])->where(['customer_code' => $code]);

            $this->loadModel('CompAsociados');

            foreach ($debitnotes as $debitnote) {

                $debitnote->comp_asociados = $this->CompAsociados->find()
                    ->where([
                        'tipo_comp' => $debitnote->tipo_comp,
                        'pto_vta' => $debitnote->pto_vta,
                        'num' => $debitnote->num,
                    ]);
              
            }

            $this->set(compact('debitnotes'));
            $this->set('_serialize', ['debitnotes']);
        }
    }

    public function showprint($id)
    {
        $data = new \stdClass;
        $data->id = $id;
        $this->FiscoAfipCompPdf->debitNote($data);
    }

    public function apply()
    {
        if ($this->request->is('ajax')) {

            $success = true;

            $data = $this->request->input('json_decode');

            $debitNote = $this->DebitNotes->get($data->debit_note_id);

            if (!$debitNote) {
                $success = false;
            } else {

                $this->loadModel('Invoices');

                $invoice = $this->Invoices->get($data->invoice_id);

                if (!$invoice) {
                    $success = false;
                } else {

                    $this->loadModel('CompAsociados');

                    $ca = $this->CompAsociados->newEntity();

                    $ca->tipo_comp1 = $debitNote->tipo_comp;
                    $ca->pto_vta1 = $debitNote->pto_vta;
                    $ca->num1 = $debitNote->num;

                    $ca->tipo_comp2 = $invoice->tipo_comp;
                    $ca->pto_vta2 = $invoice->pto_vta;
                    $ca->num2 = $invoice->num;

                    $ca->total = $debitNote->total;

                    if (!$this->CompAsociados->save($ca)) {
                        $success = false;
                    }
                }
            }

            $this->set('apply', $success);
        }
    }

    public function delete()
    {
        if ($this->request->is('ajax')) {

            $data = $this->request->input('json_decode');

            $debitNote = $this->DebitNotes->get($data->id);

            $ok = false;

            if ($this->FiscoAfipComp->rollbackDebitNote($debitNote)) {
                $ok = true;
            }

            $this->set("ok", $ok);
        }
    }

    public function addRecargos()
    {
        if ($this->request->is('ajax')) {

            $paraments = $this->request->getSession()->read('paraments');
            $afip_codes = $this->request->getSession()->read('afip_codes');

            $response['error'] = TRUE;
            $response['msg'] = "Error, al intentar generar el Recargo."; 

            $data = $this->request->input('json_decode');

            $this->loadModel('Customers');
            $customer = $this->Customers
                ->find()
                ->contain([
                    'Cities'
                ])
                ->where([
                    'code' => $data->customer_code
                ])->first();

            if ($customer) {

                $tax = $data->type == "012" ? 1 : 5;
                $tax_porcentaje = $afip_codes['alicuotas_percectage'][$tax];

                $result = $this->CurrentAccount->calulateIvaAndNeto($data->total, $tax_porcentaje, 1);

                $concepts = [];

                $concept = new \stdClass;
                $concept->type        = 'S';
                $concept->code        = 99;
                $concept->description = $data->concept;
                $concept->quantity    = 1;
                $concept->unit        = 7; //Un.
                $concept->price       = $result->price;
                $concept->discount    = 0;
                $concept->sum_price   = $result->sum_price;
                $concept->tax         = $tax;
                $concept->sum_tax     = $result->sum_tax;
                $concept->total       = $result->total;
                $concepts[0]          = $concept;

                $now = Time::now();
                $debitNote  = [
                    'customer'       => $customer,
                    'concept_type'   => 3, // Productos y Servicios
                    'date'           => $now,
                    'date_start'     => $now,
                    'date_end'       => $now,
                    'duedate'        => $now,
                    'concepts'       => $concepts,
                    'tipo_comp'      => $data->type,
                    'comments'       => $data->concept,
                    'manual'         => TRUE,
                    'business_id'    => $customer->business_billing,
                    'connection_id'  => $data->connection_id,
                    'seating_number' => NULL,
                ];

                $debitNote = $this->FiscoAfipComp->debitNote($debitNote);

                if ($debitNote) {

                   //creo asiento

                   if ($this->Accountant->isEnabled()) {

                        $seating = $this->Accountant->addSeating([
                            'concept'       => 'Recargo',
                            'comments'      => '',
                            'seating'       => NULL,
                            'account_debe'  => $customer->account_code,
                            'account_haber' => $paraments->accountant->acounts_parent->surcharge,
                            'value'         => $debitNote->total,
                            'redirect'      => NULL
                        ]);

                        $debitNote->seating_number = $seating->number;

                        if ($seating) {
                            $debitNote->seating_number = $seating->number;
                            $this->DebitNotes->Save($debitNote);
                        }
                    }

                    //informo afip si no es de tipo X

                    if ($this->FiscoAfip->informar($debitNote, FiscoAfipComp::TYPE_DEBIT_NOTE)) {

                        if ($this->DebitNotes->save($debitNote)) {
                            $response['error'] = FALSE;
                            $response['msg'] = "Se ha generado correctamente el recargo"; 
                        }

                    } else {
                        $this->FiscoAfipComp->rollbackDebitNote($debitNote, true);
                    }
                } else {
                    $this->FiscoAfipComp->rollbackDebitNote($debitNote, true);
                }
            }
            $this->set('data', $response);
        }
     }
}
