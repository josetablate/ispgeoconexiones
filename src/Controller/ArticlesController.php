<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Articles Controller
 *
 * @property \App\Model\Table\ArticlesTable $Articles
 */
class ArticlesController extends AppController
{
    public function isAuthorized($user = null) 
    {
        if ($this->request->getParam('action') == 'getAllCodeAjax') {
            return true;
        }

        if ($this->request->getParam('action') == 'getAllCodeWithoutAjax') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function index()
    {
        $articles = $this->Articles->find()->contain(['ArticlesStores'])->where(['deleted' => false]);

        foreach ($articles as $article) {

            $article->current_amount = 0;

            foreach ($article->articles_stores as $ss) {

                if ($article->id == $ss->article_id) {
                    $article->current_amount += $ss->current_amount;
                }
            }
        }

        $this->set(compact('articles'));
        $this->set('_serialize', ['articles']);
    }

    public function add()
    {
        $article = $this->Articles->newEntity();
        if ($this->request->is('post')) {

            $action = 'Agregado de Artículo';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            if ($this->Articles->find()->where(['code' => $this->request->getData('code'), 'deleted' => false])->first()) {
                $this->Flash->warning(__('Existe un artículo con este código.'));
                return $this->redirect(['action' => 'add']);
            }

            $data = $this->request->getData();
            $data['total_amount'] = 0;
            $data['enabled'] = true;

            $article = $this->Articles->patchEntity($article, $data);

            if ($this->Articles->save($article)) {
                $action = 'Artículo agregado';
                $detail = '';
                $detail .= 'Código: ' . $article->code . PHP_EOL;
                $detail .= 'Nombre: ' . $article->name . PHP_EOL;
                $detail .= 'Cantidad: ' . $article->total_amount . PHP_EOL;
                $detail .= 'Habilitado: ' . ($article->enabled ? 'Si' : 'No') . PHP_EOL;
                $this->registerActivity($action, $detail, NULL);
                $this->Flash->success(__('Artículo agregado.'));
                return $this->redirect(['action' => 'add']);
            } else {
                $this->Flash->error(__('Error agregar el artículo.'));
            }
        }
 
        $this->set(compact('article', 'stores'));
        $this->set('_serialize', ['article']);
    }

    public function getAllCodeAjax()
    {

        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $codes = $this->Articles->find()->select(['id','code','name', 'exist_snid'])->where(['enabled' => true,'deleted' => false])->toArray(); 
            $this->set('codes',$codes );
        }
    } 

    public function getAllCodeWithoutAjax()
    {
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $codes = $this->Articles->find()->select(['id','code','name'])->where(['enabled' => true, 'deleted' => false])->toArray(); 
            $this->set('codes',$codes );
        }
    }

    public function edit()
    {
         if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Edición de Artículo';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $article = $this->Articles->get($_POST['id'], [
                'contain' => ['Stores']
            ]);

            if ($this->Articles->find()->where(['code' => $this->request->getData('code'), 'deleted' => false, 'id !=' => $_POST['id']])->first()) {
                $this->Flash->warning(__('Existe un artículo con este código.'));
                return $this->redirect(['action' => 'index']);
            }

            $article->enabled = true;

            $article = $this->Articles->patchEntity($article, $this->request->getData());

            if ($this->Articles->save($article)) {
                $action = 'Artículo editado';
                $detail = '';
                $detail .= 'Código: ' . $article->code . PHP_EOL;
                $detail .= 'Nombre: ' . $article->name . PHP_EOL;
                $detail .= 'Cantidad: ' . $article->total_amount . PHP_EOL;
                $detail .= 'Habilitado: ' . ($article->enabled ? 'Si' : 'No') . PHP_EOL;
                $this->registerActivity($action, $detail, NULL);
                $this->Flash->success(__('Datos actualizados.'));
            } else {

                $this->Flash->error(__('Error al actualizar los datos.'));
            }

            return $this->redirect(['action' => 'index']);
        }
    }

    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);
        $action = 'Eliminación de Artículo';
        $detail = '';
        $this->registerActivity($action, $detail, NULL, TRUE);

        $article = $this->Articles->get($_POST['id']);

        $this->loadModel('ArticlesStores');
        $articlesStores = $this->ArticlesStores->find()->where(['current_amount >' => 0, 'article_id' => $_POST['id']]);

        $count = 0;

        foreach ($articlesStores as $ss) {

            $store = $this->ArticlesStores->Stores->get($ss->store_id);

            if ($store && !$store->deleted) {
                $this->Flash->error(__('No se puede eliminar por que el depósito ' . $store->name.' contiene ' . $ss->current_amount . ' del artículo ' . $article->name));
                return $this->redirect(['action' => 'index']);
            }
        }

        $article->deleted = true;
        $article_code = $article->code;
        $article_name = $article->name;
        $article_total_amount = $article->total_amount;
        $article_enabled = ($article->enabled ? 'Si' : 'No');

        if ($this->Articles->save($article)) {
            $action = 'Artículo eliminado';
            $detail = '';
            $detail .= 'Código: ' . $article_code . PHP_EOL;
            $detail .= 'Nombre: ' . $article_name . PHP_EOL;
            $detail .= 'Cantidad: ' . $article_total_amount. PHP_EOL;
            $detail .= 'Habilitado: ' . $article_enabled . PHP_EOL;
            $this->registerActivity($action, $detail, NULL);
            $this->Flash->success(__('Artículo eliminado.'));
        } else {
            $this->Flash->error(__('Error al eliminar el artíclo.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
