<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 */
class ProductsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Accountant', [
            'className' => '\App\Controller\Component\Admin\Accountant'
        ]);
    }
    
    public function isAuthorized($user = null) 
    {
        if ($this->request->getParam('action') == 'getAllByUserAjax') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $products = $this->Products->find()
            ->contain([
                'Accounts', 
                'Articles'
            ])
            ->where([
                'Products.deleted' => false
            ]);

        $this->set(compact('products'));
        $this->set('_serialize', ['products']);
    }
    
    public function getAllByUserAjax() 
    {
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $user_id = $this->request->input('json_decode')->user_id;

            //busco el deposito del usuario selecionado
            $this->loadModel('Stores');
            $store = $this->Stores->find()->where(['user_id' => $user_id, 'Stores.deleted' => false, 'Stores.enabled' => true])->first();

            //si exite el deposito
            if ($store) {

                //busco todos los productos que 
                $products = $this->Products->find()->contain('Articles')->where(['Products.enabled' => true, 'Products.deleted' => false])->toArray(); 
                $result = [];

                foreach ($products as $p) {
                    $articleStore = $this->checkArticlesProductByStore($p->article_id, $store->id);
                    if ($articleStore != null && $articleStore->current_amount > 0) {
                        $p->amount = $articleStore->current_amount;
                        $p->product_article_store_id = $articleStore->id;
                        $result[] = $p; 
                    }
                }

                if (count($result) > 0) {
                    $this->set('products',$result );
                    $this->set('store_id',$store->id );
                } else {
                    $this->set('products', false);
                }

            } else {
                $this->set('products', false);
            }
        }
    } 

    private function checkArticlesProductByStore($article_id, $store_id)
    {
        $this->loadModel('ArticlesStores');
        $ss = $this->ArticlesStores->find()->where([
                'article_id' => $article_id,
                'store_id' => $store_id,
                'current_amount >' => 0
            ])
            ->first();
        if ($ss) {
            return $ss;
        }
        return null;
    }

    /**
     * View method
     *
     * @param string|null $id Product id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $product = $this->Products->get($id, [
            'contain' => ['Articles']
        ]);

        $this->set('product', $product);
        $this->set('_serialize', ['product']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $product = $this->Products->newEntity();
        $parament = $this->request->getSession()->read('paraments');

        //si la contabilidada esta activada registro el asientop
        if ($this->Accountant->isEnabled()) {
            if (!$parament->accountant->acounts_parent->products) {
                $this->Flash->default(__('Error de Configuración. Falta cuenta padre de los Productos'));
                return $this->redirect(['action' => 'add']);
            }
        }

        if ($this->request->is('post')) {

            if ($this->request->getData('article_id') == '') {
                $this->Flash->warning(__('Ingreso el artículo de forma manual. Debe ingresar el articulo, presionando * en el campo: "Artículo" para poder visualizar la lista de artículos y de allí seleccionar el artículo.'));
                return $this->redirect(['action' => 'add']);
            }

            if ($this->Products->find()->where(['name' => $this->request->getData('name'), 'deleted' => false])->first()) {
                 $this->Flash->error(__('Ya existe un producto con este nombre.'));
                 return $this->redirect(['action' => 'add']);
            }

            $request_data = $this->request->getData();
            $request_data['account_code'] = NULL;

            //si la contabilidada esta activada registro el asientop
            if ($this->Accountant->isEnabled()) {

                $account = $this->Accountant->createAccount([
                    'account_parent' => $parament->accountant->acounts_parent->products,
                    'name' => $this->request->getData('name'),
                    'redirect' => ['action' => 'add']
                ]);
                $request_data['account_code'] = $account->code;
            }

            $product = $this->Products->patchEntity($product, $request_data);

            if ($this->Products->save($product)) {
                $this->Flash->success(__('Producto agregado.'));

                return $this->redirect(['action' => 'add']);
            } else {
                //si la contabilidada esta activada registro el asientop
                if ($this->Accountant->isEnabled()) {
                    $this->Accountant->deleteAccount($account);
                    $this->Flash->error(__('No se pudo crear el producto.'));
                }
            }
        }

        $this->set(compact('product'));
        $this->set('_serialize', ['product']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Product id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if ($this->request->is(['patch', 'post', 'put'])) {

            $product = $this->Products->get($_POST['id'], [
                'contain' => ['Articles']
            ]);

            //si la contabilidada esta activada registro el asientop
            if ($this->Accountant->isEnabled()) {

                $this->loadModel('Accounts');

                $account = $this->Accounts->find()->where(['code' => $this->request->getData('account_code')])->first();
    
                if (!$account) {
                    $this->Flash->warning(__('No existe el número de cuenta {0}.', $this->request->getData('account_code')));
                    return $this->redirect(['action' => 'index']);
                }
                
                //edito cuenta contable del producto
                $this->loadModel('Accounts');

                $account = $this->Accounts->find()
                    ->where([
                        'code' =>  $product->account_code,
                ])->first();

                $account->name = $product->name;

                if (!$this->Accounts->save($account)) {
                    $this->Flash->error(__('No se pudo actualizar la cuenta.'));
                }
            }

            if ($this->Products->find()
                ->where(['name' => $this->request->getData('name'),
                    'deleted' => false,
                    'id != ' => $id
                ])
                ->first()) {
                     $this->Flash->error(__('Ya existe un producto con este nombre.'));
                     return $this->redirect(['action' => 'index'
                 ]);
            }

            $product = $this->Products->patchEntity($product, $this->request->getData());

            if ($this->Products->save($product)) {
                $this->Flash->success(__('Datos actualizados.'));
            } else {
                $this->Flash->error(__('Error al actualizar los datos.'));
            }
            return $this->redirect(['action' => 'index']);
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Product id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);
        $product = $this->Products->get($_POST['id']);

        $product->deleted = true;

        if ($this->Products->save($product)) {
            $this->Flash->success(__('Producto eliminado.'));
        } else {
            $this->Flash->error(__('Error al eliminar el producto.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
