<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Event\Event;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\Filesystem\Folder;
use Cake\Event\EventManager;

use App\Controller\Component\ComprobantesComponent;
use App\Controller\Component\AccountantComponent;
use App\Controller\Component\PDFGeneratorComponent;

class CuentadigitalController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Accountant', [
             'className' => '\App\Controller\Component\Admin\Accountant'
        ]);

        $this->loadComponent('CurrentAccount', [
             'className' => '\App\Controller\Component\Admin\CurrentAccount'
        ]);

        $this->loadComponent('FiscoAfipComp', [
             'className' => '\App\Controller\Component\Admin\FiscoAfipComp'
        ]);

        $this->loadComponent('IntegrationRouter', [
             'className' => '\App\Controller\Component\Integrations\IntegrationRouter'
        ]);

        $this->initEventManager();
    }

    public function initEventManager()
    {
        EventManager::instance()->on(
            'CuentadigitalController.Error',
            function ($event, $msg, $data, $flash = false) {

                $this->loadModel('ErrorLog');
                $log = $this->ErrorLog->newEntity();
                $log->user_id = $this->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Error';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->ErrorLog->save($log);

                if ($flash) {
                    $this->Flash->error(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'CuentadigitalController.Warning',
            function ($event, $msg, $data, $flash = false) {

                $this->loadModel('ErrorLog');
                $log = $this->ErrorLog->newEntity();
                $log->user_id = $this->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Warning';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->ErrorLog->save($log);

                if ($flash) {
                    $this->Flash->warning(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'CuentadigitalController.Log',
            function ($event, $msg, $data) {

            }
        );

        EventManager::instance()->on(
            'CuentadigitalController.Notice',
            function ($event, $msg, $data) {

            }
        );
    }

    public function isAuthorized($user = null)
    {
        if ($this->request->getParam('action') == 'getCredentials') {
            return true;
        }

        if ($this->request->getParam('action') == 'assignTransaction') {
            return true;
        }

        if ($this->request->getParam('action') == 'ticketControl') {
            return true;
        }

        if ($this->request->getParam('action') == 'getAccounts') {
            return true;
        }

        if ($this->request->getParam('action') == 'processTransactions') {
            return true;
        }

        if ($this->request->getParam('action') == 'generatePayment') {
            return true;
        }

        if ($this->request->getParam('action') == 'strToDeciaml') {
            return true;
        }

        if ($this->request->getParam('action') == 'enableConnection') {
            return true;
        }

        if ($this->request->getParam('action') == 'test') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function index()
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');
        $paraments = $this->request->getSession()->read('paraments');
        $account_enabled = $this->Accountant->isEnabled();

        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Configuración Cuenta Digital';
            $detail = "";
            $this->registerActivity($action, $detail, NULL, TRUE);

            $payment_getway->config->cuentadigital->control_number = $this->request->getData('control_number');
            $payment_getway->config->cuentadigital->enabled = $this->request->getData('enabled');
            $payment_getway->config->cuentadigital->cash = $this->request->getData('cash');
            $payment_getway->config->cuentadigital->portal = $this->request->getData('portal');
            $payment_getway->config->cuentadigital->period = $this->request->getData('period');
            $payment_getway->config->cuentadigital->hours_execution = $this->request->getData('hours_execution');
            $payment_getway->config->cuentadigital->account_number = $this->request->getData('account_number');
            $payment_getway->config->cuentadigital->automatic = $this->request->getData('automatic');

            if ($payment_getway->config->cuentadigital->enabled) {
                $paraments->invoicing->add_cuentadigital_account_summary = TRUE;
            } else {
                $paraments->invoicing->add_cuentadigital_account_summary = FALSE;
            }

            $action = 'Configuración Guardada' . PHP_EOL;
            $detail = 'Habilitar: ' . ($payment_getway->config->cuentadigital->enabled ? 'Si' : 'No') . PHP_EOL;
            $detail .= 'Control: ' . $payment_getway->config->cuentadigital->control_number . PHP_EOL;
            $detail .= 'Nro de Cuenta: ' . $payment_getway->config->cuentadigital->account_number . PHP_EOL;
            $detail .= 'Horario de ejecución: ' . $payment_getway->config->cuentadigital->hours_execution . PHP_EOL;
            $detail .= 'Fecha: ' . $payment_getway->config->cuentadigital->period . PHP_EOL;

            //se guarda los cambios
            $this->saveGeneralParaments($paraments);

            if ($account_enabled) {
                $payment_getway->config->cuentadigital->account = $this->request->getData('account');
                $detail .= 'Cuenta contable: ' . $payment_getway->config->cuentadigital->account . PHP_EOL;
            }

            $this->registerActivity($action, $detail, NULL);

            $this->savePaymentGetwayParaments($payment_getway);
        }

        $this->loadModel('Accounts');
        $accountsTitles = $this->Accounts->find()->where(['title' => TRUE])->order(['code' => 'ASC']);
        $accounts = $this->Accounts
            ->find()
            ->order([
                'code' => 'ASC'
            ]);

        $user_id = $this->request->getSession()->read('Auth.User')['id'];
        $this->set(compact('payment_getway', 'accountsTitles', 'accounts', 'account_enabled', 'user_id'));
        $this->set('_serialize', ['payment_getway', 'accounts']);
    }

    public function getCredentials()
    {
        if ($this->request->is(['ajax'])) {
            $payment_getway = $this->request->getSession()->read('payment_getway');
            $credentials = $payment_getway->config->cuentadigital->credentials;
            $this->set(compact('credentials'));
            $this->set('_serialize', ['credentials']);
        }
    }

    public function addCredential()
    {
        if ($this->request->is(['post'])) {

            $action = 'Agregado de Cuenta - Cuenta Digital';
            $detail = "";
            $this->registerActivity($action, $detail, NULL, TRUE);
            
            $payment_getway = $this->request->getSession()->read('payment_getway');

            $credential = new \stdClass;
            $credential->id             = time();
            $credential->name           = $this->request->getData('name');
            $credential->control_number = $this->request->getData('control_number');
            $credential->account_number = $this->request->getData('account_number');
            $credential->enabled        = $this->request->getData('enabled') == '1' ? TRUE : FALSE;

            $payment_getway->config->cuentadigital->credentials[] = $credential;

            //se guarda los cambios
            $this->savePaymentGetwayParaments($payment_getway);

            $action = 'Cuenta Guardada' . PHP_EOL;
            $detail = 'Habilitar: ' . ($credential->enabled ? 'Si' : 'No') . PHP_EOL;
            $detail .= 'Nombre: ' . $credential->name . PHP_EOL;
            $detail .= 'Control: ' . $credential->control_number . PHP_EOL;
            $detail .= 'Nro de Cuenta: ' . $credential->account_number . PHP_EOL;

            $this->registerActivity($action, $detail, NULL);
        }
        return $this->redirect(['action' => 'index']);
    }

    public function editCredential()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Edición de Cuenta - Cuenta Digital';
            $detail = "";
            $this->registerActivity($action, $detail, NULL, TRUE);

            $payment_getway = $this->request->getSession()->read('payment_getway');

            foreach ($payment_getway->config->cuentadigital->credentials as $credential) {
                if ($credential->id == $this->request->getData('id')) {
                    $credential->name               = $this->request->getData('name');
                    $credential->control_number     = $this->request->getData('control_number');
                    $credential->account_number     = $this->request->getData('account_number');
                    $credential->enabled            = $this->request->getData('enabled') == '1' ? TRUE : FALSE;
                    break;
                }
            }
            //se guarda los cambios
            $this->savePaymentGetwayParaments($payment_getway);

            $action = 'Cuenta Guardada' . PHP_EOL;
            $detail = 'Habilitar: ' . ($credential->enabled ? 'Si' : 'No') . PHP_EOL;
            $detail .= 'Nombre: ' . $credential->name . PHP_EOL;
            $detail .= 'Control: ' . $credential->control_number . PHP_EOL;
            $detail .= 'Nro de Cuenta: ' . $credential->account_number . PHP_EOL;

            $this->registerActivity($action, $detail, NULL);

            return $this->redirect(['action' => 'index']);
        }
    }

    public function generateBarcode()
    {
        if ($this->request->is('post')) {

            $payment_getway = $this->request->getSession()->read('payment_getway');
            $paraments = $this->request->getSession()->read('paraments');

            if ($payment_getway->config->cuentadigital->enabled) {

                $data = $this->request->getData();
                $customer_code = $data['customer_code'];
                $credential_id = $data['credential_id'];

                $account_number = NULL;

                foreach ($payment_getway->config->cuentadigital->credentials as $credential) {

                    if ($credential->id == $credential_id) {

                        $account_number = $credential->account_number;
                    }
                }

                $this->loadModel('CustomersAccounts');
                $customers_accounts_cuentadigital = $this->CustomersAccounts
                    ->find()
                    ->where([
                        'customer_code'     => $customer_code,
                        'deleted'           => FALSE,
                        'payment_getway_id' => 105,
                    ]);

                $where = [
                    'customer_code' => $customer_code,
                    'deleted'       => FALSE,
                ];

                $auto_debit = FALSE;
                $customers_accounts_cobrodigital_auto_debit = [];

                foreach ($payment_getway->methods as $pg) {
                    $config = $pg->config;
                    if ($payment_getway->config->$config->enabled) {
                        if ($pg->auto_debit) {
                            $auto_debit = TRUE;

                            if (!array_key_exists('OR', $where)) {
                                $where['OR'] = [];
                            }

                            $where['OR'][] = [
                                'payment_getway_id' => $pg->id
                            ];
                        }
                    }
                }

                if ($auto_debit) {
                    $customers_accounts_cobrodigital_auto_debit = $this->CustomersAccounts
                        ->find()
                        ->where($where)
                        ->toArray();
                }

                if ($customers_accounts_cuentadigital->count() > 0) {
                    $this->Flash->warning(__('Únicamente puede tener una Cuenta por Cliente. Este Cliente posee una Cuenta del mismo tipo activa, en caso de querer crear una nueva Cuenta deberá "deshabilitar" la actual Cuenta activa del Cliente'));
                    return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
                }

                if (sizeof($customers_accounts_cobrodigital_auto_debit) > 0) {
                    $this->Flash->warning(__('El Cliente tiene una Cuenta de Débito Automático activa, esto inhabilita la creación de otra Cuenta, para poder crear otras Cuentas deberá "deshabilitar" dicha Cuenta de Débito Automático.'));
                    return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
                }

                $this->loadModel('CuentadigitalAccounts');

                $cuentadigital_account = $this->CuentadigitalAccounts
                    ->find()
                    ->where([
                        'customer_code'     => $customer_code,
                        'payment_getway_id' => 105,
                        'deleted'           => FALSE
                    ]);

                if ($cuentadigital_account->count() > 0) {
                    $this->Flash->warning(__('Únicamente puede tener un código de barra por Cliente.'));
                    return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
                }

                $this->loadComponent('CuentaDigital');
                $image_src = $this->CuentaDigital->generateBarcode($customer_code, $account_number);

                $barcode = explode("=", $image_src);

                $cuentadigital = $this->CuentadigitalAccounts->newEntity();
                $cuentadigital->image_src = $image_src;
                $cuentadigital->barcode = $barcode[1];
                $cuentadigital->customer_code = $customer_code;
                $cuentadigital->account_number = $account_number;
                $cuentadigital->payment_getway_id = 105;

                if ($this->CuentadigitalAccounts->save($cuentadigital)) {

                    $this->Flash->success(__('Código de barra asiganado correctamente.'));

                    $this->loadModel('CustomersAccounts');
                    $cutomer_account = $this->CustomersAccounts->newEntity();
                    $cutomer_account->customer_code = $customer_code;
                    $cutomer_account->account_id = $cuentadigital->id;
                    $cutomer_account->payment_getway_id = 105;
                    $this->CustomersAccounts->save($cutomer_account);

                    if ($paraments->gral_config->billing_for_service) {

                        $this->loadModel('Customers');
                        $customer = $this->Customers->get($customer_code, [
                            'contain' => ['Connections']
                        ]);

                        if (sizeof($customer->connections) > 0) {

                            $this->loadModel('Connections');

                            foreach ($customer->connections as $connection) {
                                $connection->cuentadigital = TRUE;
                                $this->Connections->save($connection);
                            }
                        }
                    }
                }
            }
        }
        return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
    }

    public function createCard()
    {
        if ($this->request->is('post')) {

            $payment_getway = $this->request->getSession()->read('payment_getway');
            $paraments = $this->request->getSession()->read('paraments');

            if ($payment_getway->config->cuentadigital->enabled) {

                $data = $this->request->getData();
                $customer_code = $data['customer_code'];
                $barcode = $data['barcode'];
                $credential_id = $data['credential_id'];

                $account_number = NULL;

                foreach ($payment_getway->config->cuentadigital->credentials as $credential) {

                    if ($credential->id == $credential_id) {

                        $account_number = $credential->account_number;
                    }
                }

                $this->loadModel('CustomersAccounts');
                $customers_accounts_cuentadigital = $this->CustomersAccounts
                    ->find()
                    ->where([
                        'customer_code'     => $customer_code,
                        'deleted'           => FALSE,
                        'payment_getway_id' => 105,
                    ]);

                $where = [
                    'customer_code' => $customer_code,
                    'deleted'       => FALSE,
                ];

                $auto_debit = FALSE;
                $customers_accounts_cobrodigital_auto_debit = [];

                foreach ($payment_getway->methods as $pg) {
                    $config = $pg->config;
                    if ($payment_getway->config->$config->enabled) {
                        if ($pg->auto_debit) {
                            $auto_debit = TRUE;

                            if (!array_key_exists('OR', $where)) {
                                $where['OR'] = [];
                            }

                            $where['OR'][] = [
                                'payment_getway_id' => $pg->id
                            ];
                        }
                    }
                }

                if ($auto_debit) {
                    $customers_accounts_cobrodigital_auto_debit = $this->CustomersAccounts
                        ->find()
                        ->where($where)
                        ->toArray();
                }

                if ($customers_accounts_cuentadigital->count() > 0) {
                    $this->Flash->warning(__('Únicamente puede tener una Cuenta por Cliente. Este Cliente posee una Cuenta del mismo tipo activa, en caso de querer crear una nueva Cuenta deberá "deshabilitar" la actual Cuenta activa del Cliente'));
                    return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
                }

                if (sizeof($customers_accounts_cobrodigital_auto_debit) > 0) {
                    $this->Flash->warning(__('El Cliente tiene una Cuenta de Débito Automático activa, esto inhabilita la creación de otra Cuenta, para poder crear otras Cuentas deberá "deshabilitar" dicha Cuenta de Débito Automático.'));
                    return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
                }

                $this->loadModel('CuentadigitalAccounts');

                $cuentadigital_account = $this->CuentadigitalAccounts
                    ->find()
                    ->where([
                        'customer_code'     => $customer_code,
                        'payment_getway_id' => 105,
                        'deleted'           => FALSE
                    ]);

                if ($cuentadigital_account->count() > 0) {
                    $this->Flash->warning(__('Únicamente puede tener un código de barra por Cliente.'));
                    return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
                }

                if (!empty(trim($barcode))) {

                    $cuenta = $this->CuentadigitalAccounts
                        ->find()
                        ->where([
                            'barcode LIKE' => '%' . $barcode . '%'
                        ])->first();

                    if ($cuenta) {
                        $this->Flash->warning(__('El código barra: ' . $barcode . '. Está siendo utilizada por una cuenta actualmente.'));
                        return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
                    }
                }

                $cuentadigital = $this->CuentadigitalAccounts->newEntity();
                $cuentadigital->barcode = $barcode;
                $cuentadigital->account_number = $account_number;

                $url_image = $payment_getway->config->cuentadigital->url_image;
                $cuentadigital->image_src = $url_image . $barcode;

                if (strlen($barcode) == 14) {
                    $barcode = str_replace("0190", "", $barcode);
                    $barcode = substr($barcode, 0, -2);
                    $cuentadigital->image_src = $url_image . $barcode;
                }

                $cuentadigital->customer_code = $customer_code;
                $cuentadigital->payment_getway_id = 105;

                if ($this->CuentadigitalAccounts->save($cuentadigital)) {

                    $detail = "";
                    $detail .= 'Cód. Barra: ' . $barcode . PHP_EOL;
                    $action = 'Creación Manual de cuenta - Cuenta Digital';
                    $this->registerActivity($action, $detail, $customer_code, TRUE);

                    $this->Flash->success(__('Cuenta cargada correctamente.'));

                    $this->loadModel('CustomersAccounts');
                    $cutomer_account = $this->CustomersAccounts->newEntity();
                    $cutomer_account->customer_code = $customer_code;
                    $cutomer_account->account_id = $cuentadigital->id;
                    $cutomer_account->payment_getway_id = 105;
                    $this->CustomersAccounts->save($cutomer_account);

                    if ($paraments->gral_config->billing_for_service) {

                        $this->loadModel('Customers');
                        $customer = $this->Customers->get($customer_code, [
                            'contain' => ['Connections']
                        ]);

                        if (sizeof($customer->connections) > 0) {

                            $this->loadModel('Connections');

                            foreach ($customer->connections as $connection) {
                                $connection->cuentadigital = TRUE;
                                $this->Connections->save($connection);
                            }
                        }
                    }
                }
            }
        }
        return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
    }

    public function ticketControl()
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');

        //CONSULTAR A CUENTA DIGITAL LAS TRANSACCIONES
        $this->loadComponent('CuentaDigital');

        $data = [
            'code'    => 200,
            'message' => 'datos general'
        ];

        $this->loadModel('Users');
        $this->loadModel('Customers');
        $this->loadModel('CuentadigitalTransactions');
        $this->loadModel('CuentadigitalAccounts');

        $credentials_aux = $payment_getway->config->cuentadigital->credentials;
        $credentials = [];
        foreach ($credentials_aux as $credential_aux) {
            if ($credential_aux->enabled) {
                array_push($credentials, $credential_aux);
            }
        }

        $action = 'Control de Pagos de Cuenta Digital Cron';
        $detail = '';
        $this->registerActivity($action, $detail, NULL, TRUE);

        foreach ($credentials as $credential) {

            $result = $this->CuentaDigital->consultarTransacciones($credential->control_number, $credential->account_number);

            if ($result != "") {

                if (strpos($result, '|') !== FALSE) {

                    $user_sistema = 100;

                    //Explicación y estructura de su archivo con la configuración actual

                    //Ejemplo de una linea del archivo:
                    //20081230|221500|1000,50|999,50|01901111111122|cliente9879|PagoFacil|23a0f1c7b636c08660abbe4f02360633-de70dbb3f907c613ddce6566667f92c6|1
                    //Explicación de la estructura
                    //Fecha del cobro|Horario de la operacion (HHmmss)|Monto neto original|Monto neto recibido|Codigo de barras cobrado|Su referencia|Medio de pago usado|Codigo interno unico de operacion|Cobro numero 1 del archivo

                    $register = 0;
                    $bloque_id = time();

                    //armar array de transacciones
                    $datos = explode(PHP_EOL, $result);
                    //$datos = explode("\n", $result);//cuando es windows
                    array_pop($datos);

                    $customers_codes = "";
                    $first = TRUE;

                    foreach ($datos as $dato) {

                        $dato = explode("|", $dato);

                        $trasaccion = $this->CuentadigitalTransactions
                            ->find()
                            ->select(['id_transaccion'])
                            ->where([
                                'id_transaccion' => $dato[7]
                            ])->first();

                        //si la transaccion ya esta registra pasamo a la siguiente

                        if ($trasaccion) {
                            continue;
                        }

                        //sino existe la registro
                        //fecha de cobro
                        $fecha = new Time($dato[0] . ' ' . $dato[1]);

                        $cuentadigital_transaction = $this->CuentadigitalTransactions->newEntity();
                        $cuentadigital_transaction->id_transaccion    = $dato[7];
                        $cuentadigital_transaction->fecha             = new Time($dato[0] . ' ' . $dato[1]);
                        $cuentadigital_transaction->fecha_cobro       = $dato[0];
                        $cuentadigital_transaction->hhmmss            = $dato[1];
                        $cuentadigital_transaction->neto_original     = $this->strToDeciaml($dato[2]);
                        $cuentadigital_transaction->neto              = $this->strToDeciaml($dato[3]);
                        $cuentadigital_transaction->barcode           = $dato[4];
                        $cuentadigital_transaction->info              = $dato[6];
                        $cuentadigital_transaction->comentario        = 'Cobro numero ' . $dato[8] . ' del archivo.';
                        $cuentadigital_transaction->estado            = 0;
                        $cuentadigital_transaction->bloque_id         = $bloque_id;
                        $cuentadigital_transaction->payment_getway_id = 105;

                        $cuentadigital_account = NULL;

                        if (strpos($dato[5], 'ispbrain') !== false) {

                            $ident_array = explode('-', $dato[5]);

                            $cuentadigital_account = $this->CuentadigitalAccounts
                                ->find()
                                ->contain([
                                    'Customers.Connections'
                                ])
                                ->where([
                                    'customer_code' => $ident_array[1]
                                ])->first();
                        }

                        if ($cuentadigital_account == NULL) {

                            $barcode = str_replace("0190", "", $dato[4] . '');
                            $barcode = mb_substr($barcode, 0, -2);

                            $cuentadigital_account = $this->CuentadigitalAccounts
                                ->find()
                                ->contain([
                                    'Customers.Connections'
                                ])
                                ->where([
                                    'barcode LIKE' => '%'. $barcode . '%'
                                ])->first();
                        } else {

                            $cuentadigital_transaction->customer_code = $cuentadigital_account->customer_code;
                        }

                        $customerx = NULL;

                        // no coincide el codigo de barra de la transaccion con el codigo de barra de las tarjetas.
                        if (empty($cuentadigital_account)) {

                            $cuentadigital_transaction->comentario .= ' No existe cliente con este código de barras.';

                            try {

                                $this->CuentadigitalTransactions->save($cuentadigital_transaction);
                            } catch(Exception $e) {

                                $data_error = new \stdClass;
                                $data_error->errors = $e->getMessage();

                                $event = new Event('CuentadigitalController.Error', $this, [
                                    'msg'   => __('Hubo al guardar la transacción. Posiblemente relacionado a la duplicación de la transacción.'),
                                    'data'  => $data_error,
                                    'flash' => true
                                ]);

                                $this->getEventManager()->dispatch($event);
                                Log::write('error', $e->getMessage());
                            }

                        } else {

                            $cuentadigital_transaction->payment_getway_id = 105;
                            $customerx = $cuentadigital_account->customer;
                        }

                        //proceso la transaccion ....

                        if ($customerx != NULL && $cuentadigital_account != NULL) {

                            if ($first) {
                                $customers_codes .= $customerx->code;
                                $first = FALSE;
                            } else {
                                $customers_codes .= '.' . $customerx->code;
                            }

                            $register += $this->processTransactions($customerx, $cuentadigital_account, $cuentadigital_transaction, $register, $user_sistema);
                        }
                    }

                    if ($customers_codes != "") {
                        $command = ROOT . "/bin/cake debtMonthControlAfterAction $customers_codes 1 > /dev/null &";
                        $result = exec($command);
                    }

                    $data = [
                        'code'    => 200,
                        'message' => 'Pagos registrados: ' . $register
                    ];

                } else {

                    Log::error($result, ['scope' => ['CuentadigitalController']]);
                    $data = [
                        'code'    => 400,
                        'message' => $result
                    ];
                }

            } else {

                Log::info("No se registraron pagos en la fecha", ['scope' => ['CuentadigitalController']]);
                $data = [
                    'code'    => 200,
                    'message' => "No se registraron pagos en la fecha"
                ];
            }
        }

        return json_encode($data);
    }

    private function processTransactions($customer, $cuentadigital_account, $cuentadigital_transaction, $register = 0, $user_sistema = 100)
    {
        $this->loadModel('CuentadigitalTransactions');
        $this->loadModel('Connections');

        //cliente con facturacion separa por servicio
        if ($customer->billing_for_service) {

            $where = [
                'customer_code'       => $customer->code,
                'Connections.deleted' => FALSE
            ];

            if ($cuentadigital_transaction->payment_getway_id == 105) {
                $where['cuentadigital'] = TRUE;
            }

            $connections = $this->Connections
                ->find()
                ->Contain([
                    'Customers'
                ])
                ->where($where)
                ->order([
                    'Connections.debt_month' => 'DESC'
                ]);

            $bruto = $cuentadigital_transaction->neto_original;

            if ($bruto == 0) {
                return 0;
            }

            $connections_size = $connections->count();
            if ($connections_size == 0) {
                $cuentadigital_transaction->customer_code = $customer->code;
                $register += $this->generatePayment($customer, $cuentadigital_transaction, $bruto, $register, $cuentadigital_account, NULL);

                return $register;
            }

            if ($connections_size == 1) {

                $connection = $connections->first();

                $cuentadigital_transaction->customer_code = $customer->code;
                $register += $this->generatePayment($customer, $cuentadigital_transaction, $bruto, $register, $cuentadigital_account, $connection->id);

                return $register;
            }

            $connections_debts = [];
            $connections_without_debts = [];

            // primer recorrido busco los que tienen deudas y los que no tienen deudas
            foreach ($connections as $key => $connection) {

                $cuentadigital_transaction->customer_code = $connection->customer_code;

                try {

                    $this->CuentadigitalTransactions->save($cuentadigital_transaction);
                } catch(Exception $e) {

                    $data_error = new \stdClass; 
                    $data_error->errors = $e->getMessage();

                    $event = new Event('CuentadigitalController.Error', $this, [
                        'msg'   => __('Hubo al guardar la transacción. Posiblemente relacionado a la duplicación de la transacción.'),
                        'data'  => $data_error,
                        'flash' => true
                    ]);

                    $this->getEventManager()->dispatch($event);
                    Log::write('error', $e->getMessage());
                }

                if ($connection->debt_month > 0) {
                    array_push($connections_debts, $connection);
                } else {
                    array_push($connections_without_debts, $connection);
                }
            }

            //array donde cargo los pagos
            $generate_payments = [];
            //bruto que voy descontando las deudas de las conexiones
            $bruto_aux = $bruto;

            // cargo en un array de pagos las conexiones que pueden pagarse
            // a partir de la conexiones con deudas
            if (sizeof($connections_debts) > 0) {

                foreach ($connections_debts as $connections_debt) {

                    // la deuda de la conexiones es menor al bruto
                    if ($connections_debt->debt_month <= $bruto_aux) {

                        $bruto_aux -= $connections_debt->debt_month;

                        $connections_debt->imports = [];
                        array_push($connections_debt->imports, $connections_debt->debt_month);
                        array_push($generate_payments, $connections_debt);
                    } else {
                        // se genera un pago a la conexion con el valor de bruto 
                        $connections_debt->imports = [];
                        array_push($connections_debt->imports, $bruto_aux);
                        $bruto_aux = 0;
                        array_push($generate_payments, $connections_debt);
                        break;
                    }
                }
            }

            // si quedo valor en bruto_aux, puede ser que no exista conexiones con deudas
            if ($bruto_aux > 0) {
                // debe existir conexiones sin deudas
                if (sizeof($connections_without_debts) > 0) {
                    // se asigna el pago a la primer conexion
                    // $connection_without_debt = $connections_without_debts[0];

                    $connection_without_debt_clone = new \sdtClass;
                    $connection_without_debt_clone->id = $connections_without_debts[0]->id;  

                    $connection_without_debt_clone->imports = [];
                    array_push($connection_without_debt_clone->imports, $bruto_aux);
                    $bruto_aux = 0;

                    array_push($generate_payments, $connection_without_debt_clone);

                } else {
                    // se asigna el pago a la primer conexion (con deuda)
                    // $connections_debt = $connections_debts[0];

                    $connections_debt_clone = new \sdtClass;
                    $connections_debt_clone->id = $connections_debts[0]->id;  

                    $connections_debt_clone->imports = [];
                    array_push($connections_debt_clone->imports, $bruto_aux);
                    $bruto_aux = 0;

                    array_push($generate_payments, $connections_debt_clone);
                }
            }

            // genero los pagos de las conexiones que tienen deudas
            foreach ($generate_payments as $key => $generate_payment) {

                foreach ($generate_payment->imports as $key => $import) {

                    $register += $this->generatePayment($customer, $cuentadigital_transaction, $import, $register, $cuentadigital_account, $generate_payment->id);
                }
            }

        } else {

            $bruto = $cuentadigital_transaction->neto_original;

            if ($bruto == 0) {
                return 0;
            }

            $cuentadigital_transaction->customer_code = $customer->code;
            $register = $this->generatePayment($customer, $cuentadigital_transaction, $bruto, $register, $cuentadigital_account);
        }

        return $register;
    }

    private function generatePayment($customer, $cuentadigital_transaction, $import, $register, $cuentadigital_account, $connection_id = NULL, $user_sistema = 100)
    {
        $this->loadModel('Payments');
        $this->loadModel('CuentadigitalTransactions');
        $payment_getway = $this->request->getSession()->read('payment_getway');
        $paraments = $this->request->getSession()->read('paraments');
        $seating = NULL;

        //si la contabilidada esta activada registro el asiento
        if ($this->Accountant->isEnabled()) {

            $seating = $this->Accountant->addSeating([
                'concept'       => 'Por Cobranza (Cuenta Digital)',
                'comments'      => '',
                'seating'       => NULL, 
                'account_debe'  => $payment_getway->config->cuentadigital->account,
                'account_haber' => $customer->account_code,
                'value'         => $import,
                'user_id'       => $user_sistema,
                'created'       => Time::now()
            ]);
        }

        $data_payment = [
            'import'                => $import,
            'created'               => $cuentadigital_transaction->fecha,
            'cash_entity_id'        => NULL,
            'user_id'               => $user_sistema,
            'seating_number'        => $seating ? $seating->number : NULL,
            'customer_code'         => $customer->code,
            'payment_method_id'     => $cuentadigital_account->payment_getway_id, //id = 99 metodo de pago -> cobrodigital
            'concept'               => 'Por Cobranza (Cuenta Digital)',
            'connection_id'         => $connection_id,
            'number_transaction'    => NULL,
            'account_id'            => $cuentadigital_account->id
        ];

        $payment = $this->Payments->newEntity();

        $payment = $this->Payments->patchEntity($payment, $data_payment);

        if ($payment) {

            if ($this->Payments->save($payment)) {

                //genero el recibo del pago
                $receipt = $this->FiscoAfipComp->receipt($payment, FALSE);

                if ($receipt) {

                    $register++;

                    $cuentadigital_transaction->commentario .= ' - procesado correctamente ';
                    $cuentadigital_transaction->estado = 1;

                    if ($cuentadigital_transaction->receipt_number != NULL) {
                        $cuentadigital_transaction->receipt_number .= ',' . str_pad($receipt->pto_vta, 4, "0", STR_PAD_LEFT) . '-' . str_pad($receipt->num, 8, "0", STR_PAD_LEFT);
                        $cuentadigital_transaction->receipt_id .= ',' . $receipt->id;
                    } else {
                        $cuentadigital_transaction->receipt_number = str_pad($receipt->pto_vta, 4, "0", STR_PAD_LEFT) . '-' . str_pad($receipt->num, 8, "0", STR_PAD_LEFT);
                        $cuentadigital_transaction->receipt_id = $receipt->id;
                    }

                    try {

                        $this->CuentadigitalTransactions->save($cuentadigital_transaction);
                    } catch(Exception $e) {

                        $data_error = new \stdClass; 
                        $data_error->errors = $e->getMessage();

                        $event = new Event('CuentadigitalController.Error', $this, [
                            'msg'   => __('Hubo al guardar la transacción. Posiblemente relacionado a la duplicación de la transacción.'),
                            'data'  => $data_error,
                            'flash' => true
                        ]);

                        $this->getEventManager()->dispatch($event);
                        Log::write('error', $e->getMessage());
                    }

                    //habilito la conexion relacionada al serivicio que esta pagando
                    $this->enableConnection($payment, $customer);

                    $afip_codes = $this->request->getSession()->read('afip_codes');

                    $this->loadModel('Users');
                    $user = $this->Users->get($user_sistema);
                    $user_name = $user->name . ' - username: ' . $user->username;

                    $customer_name = $customer->name . ' - Código: ' . $customer->code . ' - ' . $afip_codes['doc_types'][$customer->doc_type] . ': ' . $customer->ident;
                    $customer_code = $customer->code;

                    if ($paraments->invoicing->email_emplate_receipt != "") {

                        $controllerClass = 'App\Controller\MassEmailsController';

                        $mass_email = new $controllerClass;
                        if ($mass_email->sendEmailReceipt($receipt->id, $paraments->invoicing->email_emplate_receipt)) {

                            $action = 'Envío recibo al cliente';
                            $detail = 'Cliente: ' . $customer_name . PHP_EOL;
                            $detail .= 'Usuario: ' . $user_name . PHP_EOL;

                            $detail .= '---------------------------' . PHP_EOL;

                            $this->registerActivity($action, $detail, $customer_code, TRUE);
                        } else {

                            $action = 'No envío recibo al cliente';
                            $detail = 'Cliente: ' . $customer_name . PHP_EOL;
                            $detail .= 'Usuario: ' . $user_name . PHP_EOL;

                            $detail .= '---------------------------' . PHP_EOL;

                            $this->registerActivity($action, $detail, $customer_code, TRUE);
                        }
                    }

                } else {

                    $data_error = new \stdClass; 
                    $data_error->request_data = $data_payment;

                    $event = new Event('CuentadigitalController.Error', $this, [
                        'msg' => __('Hubo un error al intentar generar el recibo. Verifique los valores cargados.'),
                        'data' => $data_error,
                        'flash' => true
                    ]);

                    $this->getEventManager()->dispatch($event);

                    Log::error($receipt->getErrors(), ['scope' => ['CuentadigitalController']]);

                    //rollback payment
                    $this->Payments->delete($payment);
                    //recibo
                    $this->FiscoAfipComp->rollbackReceipt($receipt, TRUE);
                    //rollback seating
                    //si la contabilidada esta activada registro el asientop
                    if ($this->Accountant->isEnabled()) {
                        if ($seating) {
                            $this->Accountant->deleteSeating();
                        }
                    }
                }
            } else {
                Log::error($payment->getErrors(), ['scope' => ['CuentadigitalController']]);
            }

        } else {
            Log::error($payment->getErrors(), ['scope' => ['CuentadigitalController']]);
        }
        return $register;
    }

    private function strToDeciaml($value)
    {
       $value = str_replace('.', '', $value);
       $value = str_replace(',', '.', $value);
       return floatval($value);
    }

    public function assignTransaction()
    {
        //ajax Detection
        if ($this->request->is('ajax')) {

            $transaction_id = $this->request->input('json_decode')->transaction_id;
            $customer_code = $this->request->input('json_decode')->customer_code;

            $response = [
                'type'    => "error",
                'message' => "No se ha asignado el pago al cliente.",
                'error'   => true
            ];

            $this->loadModel('Customers');
            $customer = $this->Customers
                ->find()
                ->where([
                    'code' => $customer_code
                ])->first();

            if ($customer) {

                $this->loadModel('CuentadigitalTransactions');
                $cuentadigital_transaction = $this->CuentadigitalTransactions
                    ->find()
                    ->where([
                        'id' => $transaction_id
                    ])->first();

                $cuentadigital_account = NULL;

                $this->loadModel('CuentadigitalAccounts');
                $cuentadigital_account = $this->CuentadigitalAccounts
                    ->find()
                    ->contain([
                        'Customers'
                    ])
                    ->where([
                        'customer_code' => $customer_code
                    ])->first();

                if ($cuentadigital_transaction) {

                    if ($this->processTransactions($customer, $cuentadigital_account, $cuentadigital_transaction)) {

                        $payment_getway_name = "Cuenta Digital";
                        $data = "";
                        switch ($cuentadigital_account->payment_getway_id) {
                            case 105:
                                $payment_getway_name = "Código de barra abierto";
                                $data = "Cód. Barra: " . $cuentadigital_account->barcode;
                                break;
                        }

                        $detail = "";
                        $detail .= 'Medio: '. $payment_getway_name . PHP_EOL;
                        $detail .= $data . PHP_EOL; 
                        $action = 'Asignación de Pago Manual - Cuenta Digital';
                        $this->registerActivity($action, $detail, $customer_code, TRUE);
                        
                        $response = [
                            'type'    => "success",
                            'message' => "Pago asignado correctamente.",
                            'error'   => false
                        ];

                        $command = ROOT . "/bin/cake debtMonthControlAfterAction $customer_code 1 > /dev/null &";
                        $result = exec($command);
                    }
                }
            }

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function accounts()
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');

        if (!$payment_getway->config->cuentadigital->enabled) {

            $this->Flash->warning('Debe habilitar Cuenta digital');
            return $this->redirect(['controller' => 'PaymentMethods', 'action' => 'index']);
        }

        $cuenta_digital_credentials = [];
        foreach ($payment_getway->config->cuentadigital->credentials as $credential) {
            $cuenta_digital_credentials[$credential->account_number] = $credential->name . ' (' . $credential->account_number . ')';
        }

        $this->set(compact('cuenta_digital_credentials'));
        $this->set('_serialize', ['cuenta_digital_credentials']);
    }

    public function getAccounts()
    {
        if ($this->request->is('ajax')) { //Ajax Detection 

            $payment_getway = $this->request->getSession()->read('payment_getway');
            $this->loadModel('CuentadigitalAccounts');

            $payment_getway_id = $this->request->getQuery()['payment_getway_id'];
            $deleted = $this->request->getQuery()['deleted'];

            $where = [
                'CuentadigitalAccounts.deleted'           => $deleted,
                'CuentadigitalAccounts.payment_getway_id' => $payment_getway_id
            ];

            $response = new \stdClass();

            $response->draw = intval($this->request->getQuery('draw'));

            $response->recordsTotal = $this->CuentadigitalAccounts->find()->where($where)->count();

            $response->data = $this->CuentadigitalAccounts->find('ServerSideData', [
                'params' => $this->request->getQuery(),
                'where'  => $where
            ]);

            $response->recordsFiltered = $this->CuentadigitalAccounts->find('RecordsFiltered', [
                'params' => $this->request->getQuery(),
                'where'  => $where
            ]);

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function accountsDisabled()
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');

        if (!$payment_getway->config->cuentadigital->enabled) {

            $this->Flash->warning('Debe habilitar Cuenta digital');
            return $this->redirect(['controller' => 'PaymentMethods', 'action' => 'index']);
        }

        $cuenta_digital_credentials = [];
        foreach ($payment_getway->config->cuentadigital->credentials as $credential) {
            $cuenta_digital_credentials[$credential->account_number] = $credential->name . ' (' . $credential->account_number . ')';
        }

        $this->set(compact('cuenta_digital_credentials'));
        $this->set('_serialize', ['cuenta_digital_credentials']);
    }

    private function enableConnection($payment, $customer)
    {
        $paraments = $this->request->getSession()->read('paraments');

        $this->loadModel('Connections');

        $connections = [];

        if ($customer->billing_for_service) {

             if ($payment->connection_id) {

                 $connections = $this->Connections->find()->contain(['Customers', 'Controllers', 'Services'])
                    ->where([
                        'Connections.id' => $payment->connection_id,
                        'Connections.deleted' => false,
                        'Connections.enabled' => false
                    ]);
             }

        } else {

            $connections = $this->Connections->find()->contain(['Customers', 'Controllers', 'Services'])
                ->where([
                    'Connections.customer_code' => $customer->code,
                    'Connections.deleted' => false,
                    'Connections.enabled' => false
                ]);
        }

        foreach ($connections as $connection) {

            if ($this->IntegrationRouter->enableConnection($connection, true)) {

                $connection->enabled = true;
                $connection->enabling_date = Time::now();

                if ($this->Connections->save($connection)) {

                    $this->loadModel('ConnectionsHasMessageTemplates');
                    $chmt =  $this->ConnectionsHasMessageTemplates->find()
                        ->where([
                            'connections_id' => $connection->id, 
                            'type' => 'Bloqueo'])
                        ->first();

                    if (!$this->ConnectionsHasMessageTemplates->delete($chmt)) {
                        //$this->Flash->error(__('No se pudo eliminar el aviso de bloqueo.'));
                    }

                    $customer_code = $connection->customer_code;

                    $detail = "";
                    $detail .= 'Conexión: '. $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL; 
                    $action = 'Habilitación de Conexión - Imputación de pago automático Cuenta Digital';
                    $this->registerActivity($action, $detail, $customer_code, TRUE);
                } else {
                    //$this->Flash->error(__('No se pudo Habilitar la conexión.'));
                }
            }
        }
    }

    public function disabled($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $this->loadModel('CuentadigitalAccounts');
        $this->loadModel('CustomersAccounts');

        $card = $this->CuentadigitalAccounts->get($id);
        $barcode = $card->barcode;
        $customer_code = $card->customer_code;
        $card->deleted = TRUE;

        if ($this->CuentadigitalAccounts->save($card)) {

            $detail = "";
            $detail .= 'Cód. Barra: ' . $barcode . PHP_EOL;
            $action = 'Deshabilitación de Cuenta - Cuenta Digital';
            $this->registerActivity($action, $detail, $customer_code);

            $this->Flash->success(__('Se ha deshabilitado la cuenta correctamente.'));
        } else {
            $this->Flash->error(__('No se ha deshabilitado la cuenta. por favor, intente nuevamente.'));
        }

        return $this->redirect(['action' => 'accounts']);
    }

    public function test()
    {
        if ($this->request->is('ajax')) {

            $payment_getway = $this->request->getSession()->read('payment_getway');
            $this->log($payment_getway, 'debug');
            //CONSULTAR A CUENTA DIGITAL LAS TRANSACCIONES
            $this->loadComponent('CuentaDigital');

            $credentials = $payment_getway->config->cuentadigital->credentials;
            $id = $this->request->input('json_decode')->id;
            $credential_selected = NULL;

            foreach ($credentials as $credential) {
                if ($id == $credential->id) {
                    $credential_selected = $credential;
                }
            }

            $result = $this->CuentaDigital->consultarTransacciones($credential_selected->control_number, $credential_selected->account_number);
            $result = empty($result) ? 'OK' : $result;

            $this->set('result', $result);
        }
    }

    public function forceExecution()
    {
        if ($this->request->is('post')) {

            $payment_getway = $this->request->getSession()->read('payment_getway');

            //CONSULTAR A CUENTA DIGITAL LAS TRANSACCIONES
            $this->loadComponent('CuentaDigital');

            $this->loadModel('Users');
            $this->loadModel('Customers');
            $this->loadModel('CuentadigitalTransactions');
            $this->loadModel('CuentadigitalAccounts');

            $credentials_aux = $payment_getway->config->cuentadigital->credentials;
            $credentials = [];
            foreach ($credentials_aux as $credential_aux) {
                if ($credential_aux->enabled) {
                    array_push($credentials, $credential_aux);
                }
            }

            //20190314
            $date = $_POST['date'];
            $date = explode('/', $date);
            $date = $date[2] . $date[1] . $date[0];

            $action = 'Ejecución forzada de control de Pagos - Cuenta Digital';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            foreach ($credentials as $credential) {

                $result = $this->CuentaDigital->consultarTransacciones($credential->control_number, $credential->account_number, $date);

                if ($result != "") {

                    if (strpos($result, '|') !== FALSE) {

                        $user_sistema = 100;

                        //Explicación y estructura de su archivo con la configuración actual

                        //Ejemplo de una linea del archivo:
                        //20081230|221500|1000,50|999,50|01901111111122|cliente9879|PagoFacil|23a0f1c7b636c08660abbe4f02360633-de70dbb3f907c613ddce6566667f92c6|1
                        //Explicación de la estructura
                        //Fecha del cobro|Horario de la operacion (HHmmss)|Monto neto original|Monto neto recibido|Codigo de barras cobrado|Su referencia|Medio de pago usado|Codigo interno unico de operacion|Cobro numero 1 del archivo

                        $register = 0;
                        $bloque_id = time();

                        //armar array de transacciones
                        $datos = explode(PHP_EOL, $result);
                        //$datos = explode("\n", $result);
                        array_pop($datos);

                        $customers_codes = "";
                        $first = TRUE;

                        foreach ($datos as $dato) {

                            $dato = explode("|", $dato);

                            $trasaccion = $this->CuentadigitalTransactions
                                ->find()
                                ->select(['id_transaccion'])
                                ->where([
                                    'id_transaccion' => $dato[7]
                                ])->first();

                            //si la transaccion ya esta registra pasamo a la siguiente
                            $this->log("11", 'debug');
                            if ($trasaccion) {
                                continue;
                            }
                            $this->log("2", 'debug');
                            //sino existe la registro
                            //fecha de cobro
                            $fecha = new Time($dato[0] . ' ' . $dato[1]);

                            $cuentadigital_transaction = $this->CuentadigitalTransactions->newEntity();
                            $cuentadigital_transaction->id_transaccion    = $dato[7];
                            $cuentadigital_transaction->fecha             = new Time($dato[0] . ' ' . $dato[1]);
                            $cuentadigital_transaction->fecha_cobro       = $dato[0];
                            $cuentadigital_transaction->hhmmss            = $dato[1];
                            $cuentadigital_transaction->neto_original     = $this->strToDeciaml($dato[2]);
                            $cuentadigital_transaction->neto              = $this->strToDeciaml($dato[3]);
                            $cuentadigital_transaction->barcode           = $dato[4];
                            $cuentadigital_transaction->info              = $dato[6];
                            $cuentadigital_transaction->comentario        = 'Cobro numero ' . $dato[8] . ' del archivo.';
                            $cuentadigital_transaction->estado            = 0;
                            $cuentadigital_transaction->bloque_id         = $bloque_id;
                            $cuentadigital_transaction->payment_getway_id = 105;

                            $cuentadigital_account = NULL;

                            if (strpos($dato[5], 'ispbrain') !== false) {

                                $ident_array = explode('-', $dato[5]);
                            
                                $cuentadigital_account = $this->CuentadigitalAccounts
                                    ->find()
                                    ->contain([
                                        'Customers.Connections'
                                    ])
                                    ->where([
                                        'customer_code' => $ident_array[1]
                                    ])->first();
                            }

                            if ($cuentadigital_account == NULL) {

                                $barcode = str_replace("0190", "", $dato[4] . '');
                                $barcode = mb_substr($barcode, 0, -2);

                                $cuentadigital_account = $this->CuentadigitalAccounts
                                    ->find()
                                    ->contain([
                                        'Customers.Connections'
                                    ])
                                    ->where([
                                        'barcode LIKE' => '%'. $barcode . '%'
                                    ])->first();
                            } else {

                                $cuentadigital_transaction->customer_code = $cuentadigital_account->customer_code;
                            }

                            $customerx = NULL;

                            // no coincide el codigo de barra de la transaccion con el codigo de barra de las tarjetas.
                            if (empty($cuentadigital_account)) {

                                $cuentadigital_transaction->comentario .= ' No existe cliente con este código de barras.';

                                try {

                                    $this->CuentadigitalTransactions->save($cuentadigital_transaction);
                                } catch(Exception $e) {

                                    $data_error = new \stdClass;
                                    $data_error->errors = $e->getMessage();

                                    $event = new Event('CuentadigitalController.Error', $this, [
                                        'msg'   => __('Hubo al guardar la transacción. Posiblemente relacionado a la duplicación de la transacción.'),
                                        'data'  => $data_error,
                                        'flash' => true
                                    ]);

                                    $this->getEventManager()->dispatch($event);
                                    Log::write('error', $e->getMessage());
                                }
                            } else {

                                $cuentadigital_transaction->payment_getway_id = 105;
                                $customerx = $cuentadigital_account->customer;
                            }

                            //proceso la transaccion ....

                            if ($customerx != NULL && $cuentadigital_account != NULL) {

                                if ($first) {
                                    $customers_codes .= $customerx->code;
                                    $first = FALSE;
                                } else {
                                    $customers_codes .= '.' . $customerx->code;
                                }

                                $register += $this->processTransactions($customerx, $cuentadigital_account, $cuentadigital_transaction, $register, $user_sistema);
                            }
                        }

                        if ($customers_codes != "") {
                            $command = ROOT . "/bin/cake debtMonthControlAfterAction $customers_codes 1 > /dev/null &";
                            $result = exec($command);
                        }

                        $this->Flash->success(__('Pagos registrados: ' . $register));

                    } else {

                        $this->Flash->error(__('Al realizar la consulta de pagos.'));
                    }

                } else {
                    $this->Flash->success(__('No se registraron pagos en la fecha.'));
                }
            }

            return  $this->redirect($this->referer());
        }
    }

    public function cleanTransactions()
    {
        if ($this->request->is('post')) {

            $action = 'Borrado de transacciones con estado sin asignar - Cuenta Digital';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $this->loadModel('CuentadigitalTransactions');
            if ($this->CuentadigitalTransactions->deleteAll(['estado' => 0])) {
                $this->Flash->success(__('Se han borrado correctamente las transacciones con estado: sin asignar.'));
            } else {
                $this->Flash->error(__('No se han podido borrar las transacciones con estaod: sin asignar.'));
            }

            return  $this->redirect($this->referer());
        }
    }
}
