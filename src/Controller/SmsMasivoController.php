<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Event\Event;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\Filesystem\Folder;

/**
 * Accounts Controller
 *
 * @property \App\Model\Table\AccountsTable $Accounts
 */
class SmsMasivoController extends AppController
{
    public function isAuthorized($user = null) 
    {
        if ($this->request->getParam('action') == 'getMessages') {
            return true;
        }
        
        if ($this->request->getParam('action') == 'credentialControl') {
            return true;
        }

        if ($this->request->getParam('action') == 'getCustomers') {
            return true;
        }

        if ($this->request->getParam('action') == 'getCustomersWithArchived') {
            return true;
        }

        if ($this->request->getParam('action') == 'ajaxValidateAccount') {
            return true;
        }

        if ($this->request->getParam('action') == 'getDevices') {
            return true;
        }

        if ($this->request->getParam('action') == 'controlDefaultPlatform') {
            return true;
        }

        if ($this->request->getParam('action') == 'controlMessages') {
            return true;
        }

        if ($this->request->getParam('action') == 'verifySMSMasivePhone') {
            return true;
        }

        if ($this->request->getParam('action') == 'validateAccount') {
            return true;
        }

        if ($this->request->getParam('action') == 'sef') {
            return true;
        }

        if ($this->request->getParam('action') == 'controlDefaultPlatform') {
            return true;
        }

        if ($this->request->getParam('action') == 'controlMessages') {
            return true;
        }

        if ($this->request->getParam('action') == 'verifySMSMasivePhone') {
            return true;
        }

        if ($this->request->getParam('action') == 'sef') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function initialize()
    {
        parent::initialize();

        $this->loadModel('SmsTemplates');
        $this->loadComponent('SmsMasivo');
        $this->loadComponent('SmsGetway');

        $this->loadComponent('CurrentAccount', [
            'className' => '\App\Controller\Component\Admin\CurrentAccount'
        ]);
    }

    public function credentialControl($sms)
    {
        $paraments = $this->request->getSession()->read('paraments');
        switch ($sms) {

            case 'smsmasivo':

                if ($paraments->sms->platform->smsmasivo->smsusuario == '' 
                    && $paraments->sms->platform->sms_getway->smsclave == '') {
                    return FALSE;
                }
                break;

            case 'sms_getway':
                if ($paraments->sms->platform->sms_getway->token == '') {
                    return FALSE;
                }
                break;
        }
        return TRUE;
    }

    public function index()
    {
        $paraments = $this->request->getSession()->read('paraments');

        $templates = $this->SmsTemplates->find();

        $this->set(compact('templates'));
        $this->set('_serialize', ['templates']);
    }

    public function config()
    {
        $paraments = $this->request->getSession()->read('paraments');

        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Edición Configuración de SMS';
            $detail = "";
            $this->registerActivity($action, $detail, NULL, TRUE);

            if ($this->request->getData('form') == 'sms_masivo') {
                $paraments->sms->platform->smsmasivo->smsusuario = $this->request->getData('sms_masivo.smsusuario');
                $paraments->sms->platform->smsmasivo->smsclave = $this->request->getData('sms_masivo.smsclave');
                if (!$this->request->getData('sms_masivo.enabled')) {
                    $this->controlDefaultPlatform('smsmasivo');
                }
                $paraments->sms->platform->smsmasivo->enabled = $this->request->getData('sms_masivo.enabled') ? true : false;
                $paraments->sms->platform->smsmasivo->fulltime = $this->request->getData('sms_masivo.fulltime') ? true : false;

                $action = 'Configuración SMS Masivo Guardada' . PHP_EOL;
                $detail = 'Usuario: ' . $paraments->sms->platform->smsmasivo->smsusuario . PHP_EOL;
                $detail .= 'Clave: ' . $paraments->sms->platform->smsmasivo->smsclave . PHP_EOL;
                $detail .= 'Habilitar: ' . ($paraments->sms->platform->smsmasivo->enabled ? 'Si' : 'No') . PHP_EOL;
                $detail .= '24h: ' . ($paraments->sms->platform->smsmasivo->fulltime ? 'Si' : 'No') . PHP_EOL;
                $this->registerActivity($action, $detail, NULL);
            }

            if ($this->request->getData('form') == 'sms_getway') {
                $paraments->sms->platform->sms_getway->token = $this->request->getData('sms_getway.token');
                $paraments->sms->platform->sms_getway->device_id = $this->request->getData('sms_getway.device_id');
                if (!$this->request->getData('sms_getway.enabled')) {
                    $this->controlDefaultPlatform('sms_getway');
                }
                $paraments->sms->platform->sms_getway->enabled = $this->request->getData('sms_getway.enabled') ? true : false;

                $action = 'Configuración SMS Getway Guardada' . PHP_EOL;
                $detail = 'TOKEN: ' . $paraments->sms->platform->sms_getway->token . PHP_EOL;
                $detail .= 'Dispositivo por defecto: ' . $paraments->sms->platform->sms_getway->device_id . PHP_EOL;
                $this->registerActivity($action, $detail, NULL);
            }

            if ($this->request->getData('form') == 'sms') {
                $paraments->sms->default_platform = $this->request->getData('sms.default_platform');

                $default_platform = "";

                switch ($paraments->sms->default_platform) {
                    case 'smsmasivo':
                        $default_platform = 'SMS Masivo';
                        break;
                    case 'sms_getway':
                        $default_platform = 'SMS Getway';
                        break;
                    default:
                        $default_platform = '';
                }

                $action = 'Configuración SMS Gral' . PHP_EOL;
                $detail .= 'Plataforma por defecto: ' . $default_platform . PHP_EOL;
                $this->registerActivity($action, $detail, NULL);
            }

            //se guarda los cambios
            $this->saveGeneralParaments($paraments);
        }

        $platform = [
            'seleccionar Plataforma'
        ];
        $default_platform = '';
        foreach ($paraments->sms->platform as $k => $s) {
            if ($s->enabled && $this->credentialControl($k)) {
                $platform[$k] = $s->name;
            }
        }

        $sms_getway_devices[''] = 'Seleccione dispositivo predeterminado';
        foreach ($paraments->sms->platform->sms_getway->devices as $key => $device) {
            $sms_getway_devices[$key] = $device->name;
        }

        $this->set(compact('paraments', 'platform', 'sms_getway_devices'));
        $this->set('_serialize', ['paraments', 'platform', 'sms_getway_devices']);
    }

    private function controlDefaultPlatform($platform)
    {
        $paraments = $this->request->getSession()->read('paraments');
        foreach ($paraments->sms->platform as $k => $s) {
            if ($platform == $k && $s->enabled) {
                $paraments->sms->default_platform = "";
            }
        }

        //se guarda los cambios

        return $this->saveGeneralParaments($paraments);
    }

    public function addTemplate()
    {
        $template = $this->SmsTemplates->newEntity();
        $paraments = $this->request->getSession()->read('paraments');
        $business = [];
        $business[''] = 'Seleccione Empresa';
        foreach ($paraments->invoicing->business as $b) {
            $business[$b->id] = $b->name;
        }
        $num_characters = $paraments->sms->num_characters;

        if ($this->request->is('post')) {

            $action = 'Agregado Plantilla SMS';
            $detail = "";
            $this->registerActivity($action, $detail, NULL, TRUE);

            $msg_count = strlen($this->request->getData('message'));

            if (strpos($this->request->getData('message'), '%cliente_nombre%') !== false) {
                $msg_count += 4;
            }

            if (strpos($this->request->getData('message'), '%cliente_codigo%') !== false) {
                $msg_count -= 11;
            }

            if (strpos($this->request->getData('message'), '%cliente_documento%') !== false) {
                $msg_count -= 3;
            }

            if (strpos($this->request->getData('message'), '%cliente_usuario_portal%') !== false) {
                $msg_count -= 13;
            }

            if (strpos($this->request->getData('message'), '%cliente_clave_portal%') !== false) {
                $msg_count -= 12;
            }

            if (strpos($this->request->getData('message'), '%cliente_saldo_total%') !== false) {
                $msg_count -= 11;
            }

            if (strpos($this->request->getData('message'), '%cliente_saldo_mes%') !== false) {
                $msg_count -= 9;
            }

            if (strpos($this->request->getData('message'), '%empresa_nombre%') !== false) {
                $msg_count += 4;
            }

            if (strpos($this->request->getData('message'), '%empresa_domicilio%') !== false) {
                $msg_count += 1;
            }

            if (strpos($this->request->getData('message'), '%empresa_web%') !== false) {
                $msg_count += 7;
            }

            if ($msg_count > 160) {
        	    $this->Flash->warning(__('El mensaje supera 160 caracteres, debe ser menor o igual a 160 caracteres'));
        	    return $this->redirect(['action' => 'addTemplate']);
        	}

            $template = $this->SmsTemplates->find()->where(['name' => $this->request->getData('name')])->first();

            if ($template) {
                $this->Flash->warning(__('Ya existe una Plantilla con este Nombre'));
                return $this->redirect(['action' => 'addTemplate']);
            }
            $template = $this->SmsTemplates->newEntity();
            $template = $this->SmsTemplates->patchEntity($template, $this->request->getData());

            if ($this->SmsTemplates->save($template)) {

                $business_name = "";

                foreach ($paraments->invoicing->business as $b) {
                    if ($b->id == $template->business_billing) {
                        $business_name = $b->name . ' (' . $b->address . ')';
                    }
                }

                $action = 'Plantilla SMS Guardada';
                $detail = "";
                $detail .= "Nombre: " . $template->name . PHP_EOL;
                $detail .= "Descripción: " . $template->description . PHP_EOL;
                $detail .= "Mensaje: " . $template->messsage . PHP_EOL;
                $detail .= "Habilitado: " . ($template->enabled ? 'Si' : 'No') . PHP_EOL;
                $detail .= "Empresa: " . $business_name;
                $this->registerActivity($action, $detail, NULL);
                $this->Flash->success(__('Se ha guardado correctamente la Plantilla.'));

                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(__('No se ha podido guardar la Plantilla. Por favor, intente nuevamente.'));
        }

        $this->set(compact('template', 'num_characters', 'business'));
        $this->set('_serialize', ['template']);
    }

    public function editTemplate($id)
    {
        $template = $this->SmsTemplates->get($id);
        $paraments = $this->request->getSession()->read('paraments');
        $business = [];
        $business[''] = 'Seleccione Empresa';
        foreach ($paraments->invoicing->business as $b) {
            $business[$b->id] = $b->name;
        }
        $num_characters = $paraments->sms->num_characters;

        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Edición Plantilla SMS';
            $detail = "";
            $this->registerActivity($action, $detail, NULL, TRUE);

            $msg_count = strlen(utf8_decode($this->request->getData('message')));

            if (strpos($template->message, '%cliente_nombre%') !== false) {
                $msg_count += 4;
            }

            if (strpos($template->message, '%cliente_codigo%') !== false) {
                $msg_count -= 11;
            }

            if (strpos($template->message, '%cliente_documento%') !== false) {
                $msg_count -= 3;
            }

            if (strpos($template->message, '%cliente_usuario_portal%') !== false) {
                $msg_count -= 13;
            }

            if (strpos($template->message, '%cliente_clave_portal%') !== false) {
                $msg_count -= 12;
            }

            if (strpos($template->message, '%cliente_saldo_total%') !== false) {
                $msg_count -= 11;
            }

            if (strpos($template->message, '%cliente_saldo_mes%') !== false) {
                $msg_count -= 9;
            }

            if (strpos($template->message, '%empresa_nombre%') !== false) {
                $msg_count += 4;
            }

            if (strpos($template->message, '%empresa_domicilio%') !== false) {
                $msg_count += 1;
            }

            if (strpos($template->message, '%empresa_web%') !== false) {
                $msg_count += 7;
            }

            if ($msg_count > 160) {
        	    $this->Flash->warning(__('El mensaje supera 160 caracteres, debe ser menor o igual a 160 caracteres'));
        	} else {
        	    if ($this->SmsTemplates->find()->where(['name' => $this->request->getData('name'), 'id !=' => $id])->first()) {
                   $this->Flash->warning(__('Existe una Plantilla con este Nombre.'));
                } else {
                    $template = $this->SmsTemplates->patchEntity($template, $this->request->getData());

                    if ($this->SmsTemplates->save($template)) {

                        $business_name = "";

                        foreach ($paraments->invoicing->business as $b) {
                            if ($b->id == $template->business_billing) {
                                $business_name = $b->name . ' (' . $b->address . ')';
                            }
                        }

                        $action = 'Plantilla SMS Guardada';
                        $detail = "";
                        $detail .= "Nombre: " . $template->name . PHP_EOL;
                        $detail .= "Descripción: " . $template->description . PHP_EOL;
                        $detail .= "Mensaje: " . $template->messsage . PHP_EOL;
                        $detail .= "Habilitado: " . ($template->enabled ? 'Si' : 'No') . PHP_EOL;
                        $detail .= "Empresa: " . $business_name;
                        $this->registerActivity($action, $detail, NULL);

                        $this->Flash->success(__('Datos actualizados.'));
                        return $this->redirect(['action' => 'index']);
                    } else {
                        $this->Flash->error(__('Error al actualizar los datos.'));
                    }
                }
        	}
        }

        $this->set(compact('template', 'num_characters', 'business'));
        $this->set('_serialize', ['template']);
    }

    public function enableTemplate()
    {
        $template = $this->SmsTemplates->get($_POST['id']);

        $template->enabled = true;

        if ($this->SmsTemplates->save($template)) {

            $action = 'Habilitación Plantilla SMS';
            $detail = "";
            $detail .= "Nombre: " . $template->name;
            $this->registerActivity($action, $detail, NULL, TRUE);

            $this->Flash->success(__('La Plantilla ' . $template->name . ' se encuentra habilitado'));
        } else {
            $this->Flash->error(__('Error al habilitar la Plantilla: ' . $template->name . '. Por favor, intente nuevamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function disableTemplate()
    {
        $template = $this->SmsTemplates->get($_POST['id']);

        $template->enabled = false;

        if ($this->SmsTemplates->save($template)) {

            $action = 'Deshabilitación Plantilla SMS';
            $detail = "";
            $detail .= "Nombre: " . $template->name;
            $this->registerActivity($action, $detail, NULL);

            $this->Flash->success(__('La Plantilla ' . $template->name . ' se encuentra deshabilitado'));
        } else {
            $this->Flash->error(__('Error al deshabilitar la Plantilla: ' . $template->name . '. Por favor, intente nuevamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function selectCustomers($template_id = NULL)
    {
        $paraments = $this->request->getSession()->read('paraments');
        $payment_getway = $this->request->getSession()->read('payment_getway');

        $template = null;
        $business = [];
        foreach ($paraments->invoicing->business as $b) {
            $business[$b->id] = 'Empresa: ' . $b->name;
        }

        $platform = [
            '' => 'Seleccionar Plataforma'
        ];
        $default_platform = $paraments->sms->default_platform;
        $default_device = ''; 
        $available_send = TRUE;
        foreach ($paraments->sms->platform as $k => $s) {
            if ($s->enabled && $this->credentialControl($k)) {
                $platform[$k] = $s->name;
            }
        }

        if (sizeof($platform) < 2) {
            $this->Flash->warning('Debe habilitar alguna plataforma de SMS.');
            return $this->redirect(['action' => 'config']);
        }

        $areas = [];
        $this->loadModel('Areas');
        $areas_model = $this->Areas->find();
        foreach ($areas_model as $a) {
            $areas[$a->id] = $a->name;
        }

        $controllers = [];
        $this->loadModel('Controllers');
        $controllers_model = $this->Controllers
            ->find()
            ->where([
        		'enabled' => TRUE,
        		'deleted' => FALSE
        	]);
        foreach ($controllers_model as $c) {
            $controllers[$c->id] = $c->name;
        }

        $services = [];
        $this->loadModel('Services');
        $services_model = $this->Services->find();
        foreach ($services_model as $s) {
            $services[$s->id] = $s->name;
        }

        $accounts = [];
        foreach ($payment_getway->methods as $pg) {
            $config = $pg->config;
            if ($payment_getway->config->$config->enabled && $pg->credential) {
                $accounts[$pg->id] = $pg->name;
            }
        }

        if (sizeof($accounts) == 0) {
            $accounts[""] = "Sin Cuentas";
        }

        $sms_getway_devices = [];
        $vencimiento_paquete = '';
        $disponibles = '';
        $fecha_servidor = '';

        if ($paraments->sms->platform->smsmasivo->enabled) {

            if ($paraments->sms->platform->smsmasivo->smsusuario == '' 
                || $paraments->sms->platform->smsmasivo->smsusuario == '') {

                $this->Flash->default(__('La credencial de la plataforma SMSMasivo es necesaria. Por favor verifique la configuración.'));
                $available_send = FALSE;
            }

            if ($default_platform != '' && !$this->validateAccount($default_platform)) {

                $this->Flash->default(__('No se pudo establecer comunicación con el servidor de SMS Masivo (posible motivo las credenciales de la Cuenta).'));
                $available_send = FALSE;
            }

            $vencimiento_paquete = $this->SmsMasivo->consultarVencimientoPaquete();

            $disponibles = $this->SmsMasivo->consultarSaldo();

            $vencimiento_paquete = new Time($vencimiento_paquete);
            $vencimiento_paquete = $vencimiento_paquete->day . '/' . $vencimiento_paquete->month . '/' . $vencimiento_paquete->year;
            $fecha_servidor = $this->SmsMasivo->consultarFechaServidor();
            $fecha_servidor = new Time($fecha_servidor);
            $hora_fecha_servidor = $fecha_servidor->hour;
            $fecha_servidor = $fecha_servidor->format('H:i:s');

            if ($paraments->sms->platform->smsmasivo->fulltime 
                    || $hora_fecha_servidor < 22 
                    && $hora_fecha_servidor > 8) {
                } else {
                    $this->Flash->warning(__('NOTA: Recuerde que, predeterminadamente, todos los usuarios de SMS Masivos cuentan con una franja horaria habilitada para realizar los envíos (desde las 08:00hs hasta las 22:00hs). Si precisa enviar mensajes las 24hs, por favor, póngase en contacto con el departamento de soporte.'));
                    $available_send = FALSE;
                }
            }

        if ($paraments->sms->platform->sms_getway->enabled) {

            if ($default_platform != '' && !$this->validateAccount($default_platform)) {

                $this->Flash->default(__('La credencial no pertenecen a una cuenta válida. Por favor verifique la configuración.'));
                $available_send = FALSE;
            }

            if ($paraments->sms->platform->sms_getway->token == '') {

                $this->Flash->default(__('API Token de la plataforma SMS Getway es necesario. Por favor verifique la configuración.'));
                $available_send = FALSE;
            }

            if (sizeof($paraments->sms->platform->sms_getway->devices) < 1) {

                $this->Flash->default(__('SMS Getway es necesario cargar los dispositivos de su cuenta. Por favor verifique la configuración.'));
                $available_send = FALSE;
            } else {

                foreach ($paraments->sms->platform->sms_getway->devices as $key => $device) {
                    $sms_getway_devices[$key] = $device->name;
                }
                $default_device = $paraments->sms->platform->sms_getway->device_id ? $paraments->sms->platform->sms_getway->device_id : '';
            }
        }

        $this->loadModel('SmsBloques');
        $block_next_id = $this->SmsBloques->find()->select(['id'])->order(['id' => 'DESC'])->first();
        $block_next_id = empty($block_next_id) ? 1 : ++$block_next_id->id;

        $templates_model = $this->SmsTemplates
            ->find()
            ->where([
                'enabled' => TRUE
            ]);

        if ($templates_model->count() < 1) {
            $this->Flash->warning('Debe crear una plantilla.');
            return $this->redirect(['action' => 'addTemplate']);
        }

        $templates = [];
        foreach ($templates_model as $tem) {
            $templates[$tem->id] = 'Plantilla: ' .  $tem->name;
            if ($template_id != NULL) {
                if ($tem->id == $template_id) {
                    $template = $tem;
                }
            } else {
                $template = $tem;
            }
        }

        $this->loadModel('Labels');
        $labels_array = $this->Labels->find()->where(['entity' => 0])->toArray();

        $labels = [];
        $labels[] = [
            'id' => 0,
            'text' => 'Sin etiquetas',
            'entity' => 0,
            'color_back' => '#000000',
            'color_text' => '#000000',
            'enabled' => 1,
        ];

        foreach ($labels_array as $label) {
            $labels[] = $label;
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            $error = $this->isReloadPage();

            if (!$error) {

                $this->loadModel('Customers');
                $this->loadModel('Sms');
                $result_blocks = [];

                $paraments = $this->request->getSession()->read('paraments');

                $data = json_decode($this->request->getData('data'));
                $platform_selected = $data->platform;
                $afip_codes = $this->request->getSession()->read('afip_codes');
                foreach ($paraments->invoicing->business as $b) {
                    if ($b->id == $data->business_billing) {
                        $enterprise = $b;
                    }
                }

                foreach ($templates_model as $tem) {
                    if ($tem->id == $data->template_id) {
                        $template = $tem;
                    }
                }

                $data_ids[] = $data->ids;

                if ($platform_selected == 'smsmasivo') {
                    $data_ids = array_chunk($data->ids, 50);
                }

                foreach ($data_ids as $ids) {

                    $bloque = $this->SmsBloques->newEntity();
                    $bloque->user_id = $this->Auth->user()['id'];
                    $bloque->template_name = $template->name;
                    $bloque->platform = $platform_selected;

                    $bloqueString = NULL;
                    switch ($platform_selected) {

                        case 'smsmasivo':
                            $bloqueString = "";
                            break;

                        case 'sms_getway':
                            $bloqueString = [];
                            $bloque->device_id = $data->device_id;
                            $bloque->token = $paraments->sms->platform->$platform_selected->token;
                            break;
                    }

                    $this->SmsBloques->save($bloque);
                    $smses = [];

                    $ok = 0;
                    $error = 0;

                    foreach ($ids as $id) {

                        $customer = $this->Customers->get($id, [
                            'contain' => []
                        ]);

                        $debts_total = $this->CurrentAccount->calulateCustomerDebtTotal($customer->code);

                        $customer_name = $this->sef($customer->name);
                        $tag_customer_name = substr($customer_name, 0, 20);
                        $temp = str_replace("%cliente_nombre%", $tag_customer_name, $template->message);

                        $tag_customer_code = substr($customer->code, 0, 11);
                        $temp = str_replace("%cliente_codigo%", $tag_customer_code, $temp);

                        $doc_customer = $afip_codes['doc_types'][$customer->doc_type] . ' ' . $customer->ident;
                        $tag_doc_customer = substr($doc_customer, 0, 16);
                        $temp = str_replace("%cliente_documento%", $tag_doc_customer, $temp);

                        $tag_customer_ident = substr($customer->ident, 0, 11);
                        $temp = str_replace("%cliente_usuario_portal%", $tag_customer_ident, $temp);

                        $tag_customer_clave_portal = substr($customer->clave_portal, 0, 10);
                        $temp = str_replace("%cliente_clave_portal%", $tag_customer_clave_portal, $temp);

                        $tag_deb_total = substr($debts_total, 0, 10);
                        $temp = str_replace("%cliente_saldo_total%", $tag_deb_total, $temp);

                        $tag_deb_mes = substr($customer->debt_month, 0, 10);
                        $temp = str_replace("%cliente_saldo_mes%", $tag_deb_mes, $temp);

                        $enterprise_name = $this->sef($enterprise->name);
                        $tag_enterprise_name = substr($enterprise_name, 0, 20);
                        $temp = str_replace("%empresa_nombre%", $tag_enterprise_name, $temp);

                        $enterprise_address = $this->sef($enterprise->address);
                        $tag_enterprise_address = substr($enterprise_address, 0, 20);
                        $temp = str_replace("%empresa_domicilio%", $tag_enterprise_address, $temp);

                        $tag_enterprise_web = substr($enterprise->web, 0, 20);
                        $temp = str_replace("%empresa_web%", $tag_enterprise_web, $temp);

                        $sms = $this->Sms->newEntity();

                        $temp = $this->sef($temp);

                        $data_sms = [
                            'message'          => $temp,
                            'customer_code'    => $customer->code,
                            'bloque_id'        => $bloque->id,
                            'business_billing' => $enterprise->id,
                            'phone'            => $customer->phone
                        ];

                        $sms = $this->Sms->patchEntity($sms, $data_sms);
                        $this->Sms->save($sms);

                        $smses[$sms->phone] = $sms;

                        switch ($platform_selected) {

                            case 'sms_getway':
                                $message = new \stdClass;
                                $message->phone_number = $customer->phone;
                                $message->message = $temp;
                                $message->device_id = $data->device_id;
                                array_push($bloqueString, $message);
                                break;

                            case 'smsmasivo':
                                //bloque=XXX%2C1161880001%2CTexto+1%0D%0AYYY%2C1161880002%2CTexto+2%0D%0A
                                $bloqueString .= $sms->id . ',' . $customer->phone . ',' . $temp . PHP_EOL;
                        }

                        $customer->last_sms = Time::now();
                        $this->Customers->save($customer);
                    }

                    $component = $paraments->sms->platform->$platform_selected->component;

                    $smsrespuesta = $this->$component->enviarBloque($bloqueString);

                    switch ($platform_selected) {

                        case 'sms_getway':

                            $smsrespuesta = json_decode($smsrespuesta);
                            if (!empty($smsrespuesta) && !isset($smsrespuesta->status)) {

                                $sms_size = sizeof($smsrespuesta);

                                foreach ($smsrespuesta as $k => $s) {

                                    if ($k == 0) {
                                        $bloque->sms_offset = $s->id;
                                    }

                                    if ($sms_size == ($k + 1)) {
                                        $bloque->sms_limit = $s->id;
                                    }

                                    $sms = $smses[$s->phone_number];
                                    $sms->getway_id = $s->id;
                                    $this->Sms->save($sms);
                                }
                                $bloque->amount = $sms_size;
                                if ($this->SmsBloques->save($bloque)) {
                                    $ok++;
                                    $result_blocks[$bloque->id] = TRUE;
                                } else {
                                    $result_blocks[$bloque->id] = FALSE;
                                    $error++;
                                }
                            } else {
                                $result_blocks[$bloque->id] = FALSE;
                                //$this->log('Error al enviar mensajes con SMS Getway', 'debug');
                                $error++;
                            }
                            break;

                        case 'smsmasivo':

                            if ($smsrespuesta == 'OK') {
                                $result_blocks[$bloque->id] = TRUE;
                                $ok++;
                            } else {
                                $result_blocks[$bloque->id] = FALSE;
                                //$this->log('Error al enviar mensajes con SMS Masivo', 'debug');
                                $error++;
                            }
                            break;
                    }
                }

                $this->loadModel('Users');
                $user = $this->Users->get($this->Auth->user()['id']);
                $user_name = $user->name . ' - username: ' . $user->username;

                $action =  'Envío masivo de SMS';

                $result = "";
                $i = 0;
                foreach ($result_blocks as $ey => $result_block) {
                    $flag = $result_block ? 'OK' : 'ERROR';
                    $limit = " - ";
                    if ($i + 1 == sizeof($result_blocks)) {
                        $limit = "";
                    }
                    $result .= "Bloque nº " . $ey . ': ' . $flag . $limit . PHP_EOL;
                    $i++;
                }

                $now = Time::now();

                $detail = $result;
                $detail .= 'Fecha: ' . $now->format('d/m/Y') . PHP_EOL;
                $detail .=  'Plantilla: ' . $template->name . PHP_EOL;
                $detail .=  'Empresa: ' . $enterprise->name . PHP_EOL;
                $detail .=  'Plataforma: ' . $platform_selected . PHP_EOL;
                $detail .=  'Usuario: ' . $user_name . PHP_EOL;
                $detail .=  'Cantidad SMS OK: ' . $ok . PHP_EOL;
                $detail .=  'Cantidad SMS Error: ' . $error . PHP_EOL;
                $detail .=  'Cantidad total SMS: ' . sizeof($ids) . PHP_EOL;
                $detail .= '---------------------------' . PHP_EOL;

                $this->registerActivity($action, $detail, NULL);

                if ($data->redirect == 'message') {
                    $result = "";
                    $i = 0;
                    foreach ($result_blocks as $ey => $result_block) {
                        $flag = $result_block ? 'OK' : 'ERROR';
                        $limit = " - ";
                        if ($i + 1 == sizeof($result_blocks)) {
                            $limit = "";
                        }
                        $result .= "Bloque nº " . $ey . ': ' . $flag . $limit;
                        $i++;
                    }
                    $this->Flash->success(__($result));
                    //$this->log($result_blocks, 'debug');
                    return $this->redirect(['action' => $data->redirect]);
                } else {
                    $result = "";
                    $i = 0;
                    foreach ($result_blocks as $ey => $result_block) {
                        $flag = $result_block ? 'OK' : 'ERROR';
                        $limit = " - ";
                        if ($i + 1 == sizeof($result_blocks)) {
                            $limit = "";
                        }
                        $result .= "Bloque nº " . $ey . ': ' . $flag . $limit;
                        $i++;
                    }

                    $this->Flash->success(__($result));
                    //$this->log($result_blocks, 'debug');
                    return $this->redirect(['action' => $data->redirect, $template_id]);
                }
            } else {

                $this->Flash->warning(__('La misma acción se intento realizar varias veces.'));
                return $this->redirect(['action' => 'selectCustomers']);
            }
        }

        $this->set(compact('templates', 'template', 'disponibles', 'vencimiento_paquete', 'fecha_servidor', 'business', 'block_next_id', 'labels', 'platform', 'available_send', 'sms_getway_devices', 'areas', 'templates_model', 'default_platform', 'default_device', 'controllers', 'services', 'accounts'));
        $this->set('_serialize', ['template']);
    }

    public function selectCustomersWithArchived($template_id = NULL)
    {
        $paraments = $this->request->getSession()->read('paraments');
        $payment_getway = $this->request->getSession()->read('payment_getway');

        $template = null;
        $business = [];
        foreach ($paraments->invoicing->business as $b) {
            $business[$b->id] = 'Empresa: ' . $b->name;
        }

        $platform = [
            '' => 'Seleccionar Plataforma'
        ];
        $default_platform = $paraments->sms->default_platform;
        $default_device = ''; 
        $available_send = TRUE;
        foreach ($paraments->sms->platform as $k => $s) {
            if ($s->enabled && $this->credentialControl($k)) {
                $platform[$k] = $s->name;
            }
        }

        if (sizeof($platform) < 2) {
            $this->Flash->warning('Debe habilitar alguna plataforma de SMS.');
            return $this->redirect(['action' => 'config']);
        }

        $areas = [];
        $this->loadModel('Areas');
        $areas_model = $this->Areas->find();
        foreach ($areas_model as $a) {
            $areas[$a->id] = $a->name;
        }

        $controllers = [];
        $this->loadModel('Controllers');
        $controllers_model = $this->Controllers
            ->find()
            ->where([
        		'enabled' => TRUE,
        		'deleted' => FALSE
        	]);
        foreach ($controllers_model as $c) {
            $controllers[$c->id] = $c->name;
        }

        $services = [];
        $this->loadModel('Services');
        $services_model = $this->Services->find();
        foreach ($services_model as $s) {
            $services[$s->id] = $s->name;
        }

        $accounts = [];
        foreach ($payment_getway->methods as $pg) {
            $config = $pg->config;
            if ($payment_getway->config->$config->enabled && $pg->credential) {
                $accounts[$pg->id] = $pg->name;
            }
        }

        if (sizeof($accounts) == 0) {
            $accounts[""] = "Sin Cuentas";
        }

        $sms_getway_devices = [];
        $vencimiento_paquete = '';
        $disponibles = '';
        $fecha_servidor = '';

        if ($paraments->sms->platform->smsmasivo->enabled) {

            if ($paraments->sms->platform->smsmasivo->smsusuario == '' 
                || $paraments->sms->platform->smsmasivo->smsusuario == '') {

                $this->Flash->default(__('La credencial de la plataforma SMSMasivo es necesaria. Por favor verifique la configuración.'));
                $available_send = FALSE;
            }

            if ($default_platform != '' && !$this->validateAccount($default_platform)) {

                $this->Flash->default(__('No se pudo establecer comunicación con el servidor de SMS Masivo (posible motivo las credenciales de la Cuenta).'));
                $available_send = FALSE;
            }

            $now = Time::now();

            if ($paraments->sms->platform->smsmasivo->fulltime 
                || $now->hour < 22 
                && $now->hour > 8) {
            } else {
                $this->Flash->warning(__('NOTA: Recuerde que, predeterminadamente, todos los usuarios de SMS Masivos cuentan con una franja horaria habilitada para realizar los envíos (desde las 08:00hs hasta las 22:00hs). Si precisa enviar mensajes las 24hs, por favor, póngase en contacto con el departamento de soporte.'));
                $available_send = FALSE;
            }

            $vencimiento_paquete = $this->SmsMasivo->consultarVencimientoPaquete();

            $disponibles = $this->SmsMasivo->consultarSaldo();

            $vencimiento_paquete = new Time($vencimiento_paquete);
            $vencimiento_paquete = $vencimiento_paquete->day . '/' . $vencimiento_paquete->month . '/' . $vencimiento_paquete->year;
            $fecha_servidor = $this->SmsMasivo->consultarFechaServidor();
            $fecha_servidor = new Time($fecha_servidor);
            $fecha_servidor = $fecha_servidor->format('H:i:s');
        }

        if ($paraments->sms->platform->sms_getway->enabled) {

            if ($default_platform != '' && !$this->validateAccount($default_platform)) {

                $this->Flash->default(__('La credencial no pertenecen a una cuenta válida. Por favor verifique la configuración.'));
                $available_send = FALSE;
            }

            if ($paraments->sms->platform->sms_getway->token == '') {

                $this->Flash->default(__('API Token de la plataforma SMS Getway es necesario. Por favor verifique la configuración.'));
                $available_send = FALSE;
            }

            if (sizeof($paraments->sms->platform->sms_getway->devices) < 1) {

                $this->Flash->default(__('SMS Getway es necesario cargar los dispositivos de su cuenta. Por favor verifique la configuración.'));
                $available_send = FALSE;
            } else {

                foreach ($paraments->sms->platform->sms_getway->devices as $key => $device) {
                    $sms_getway_devices[$key] = $device->name;
                }
                $default_device = $paraments->sms->platform->sms_getway->device_id ? $paraments->sms->platform->sms_getway->device_id : '';
            }
        }

        $this->loadModel('SmsBloques');
        $block_next_id = $this->SmsBloques->find()->select(['id'])->order(['id' => 'DESC'])->first();
        $block_next_id = empty($block_next_id) ? 1 : ++$block_next_id->id;

        $templates_model = $this->SmsTemplates
            ->find()
            ->where([
                'enabled' => TRUE
            ]);

        if ($templates_model->count() < 1) {
            $this->Flash->warning('Debe crear una plantilla.');
            return $this->redirect(['action' => 'addTemplate']);
        }

        $templates = [];
        foreach ($templates_model as $tem) {
            $templates[$tem->id] = 'Plantilla: ' .  $tem->name;
            if ($template_id != NULL) {
                if ($tem->id == $template_id) {
                    $template = $tem;
                }
            } else {
                $template = $tem;
            }
        }

        $this->loadModel('Labels');
        $labels_array = $this->Labels->find()->where(['entity' => 0])->toArray();

        $labels = [];
        $labels[] = [
            'id' => 0,
            'text' => 'Sin etiquetas',
            'entity' => 0,
            'color_back' => '#000000',
            'color_text' => '#000000',
            'enabled' => 1,
        ];

        foreach ($labels_array as $label) {
            $labels[] = $label;
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            $error = $this->isReloadPage();

            if (!$error) {

                $this->loadModel('Customers');
                $this->loadModel('Sms');
                $result_blocks = [];

                $paraments = $this->request->getSession()->read('paraments');

                $data = json_decode($this->request->getData('data'));
                $platform_selected = $data->platform;
                $afip_codes = $this->request->getSession()->read('afip_codes');
                foreach ($paraments->invoicing->business as $b) {
                    if ($b->id == $data->business_billing) {
                        $enterprise = $b;
                    }
                }

                foreach ($templates_model as $tem) {
                    if ($tem->id == $data->template_id) {
                        $template = $tem;
                    }
                }

                $data_ids[] = $data->ids;

                if ($platform_selected == 'smsmasivo') {
                    $data_ids = array_chunk($data->ids, 50);
                }

                foreach ($data_ids as $ids) {

                    $bloque = $this->SmsBloques->newEntity();
                    $bloque->user_id = $this->Auth->user()['id'];
                    $bloque->template_name = $template->name;
                    $bloque->platform = $platform_selected;

                    $bloqueString = NULL;
                    switch ($platform_selected) {

                        case 'smsmasivo':
                            $bloqueString = "";
                            break;

                        case 'sms_getway':
                            $bloqueString = [];
                            $bloque->device_id = $data->device_id;
                            $bloque->token = $paraments->sms->platform->$platform_selected->token;
                            break;
                    }

                    $this->SmsBloques->save($bloque);
                    $smses = [];

                    $ok = 0;
                    $error = 0;

                    foreach ($ids as $id) {

                        $customer = $this->Customers->get($id, [
                            'contain' => []
                        ]);

                        $debts_total = $this->CurrentAccount->calulateCustomerDebtTotal($customer->code);

                        $customer_name = $this->sef($customer->name);
                        $tag_customer_name = substr($customer_name, 0, 20);
                        $temp = str_replace("%cliente_nombre%", $tag_customer_name, $template->message);

                        $tag_customer_code = substr($customer->code, 0, 11);
                        $temp = str_replace("%cliente_codigo%", $tag_customer_code, $temp);

                        $doc_customer = $afip_codes['doc_types'][$customer->doc_type] . ' ' . $customer->ident;
                        $tag_doc_customer = substr($doc_customer, 0, 16);
                        $temp = str_replace("%cliente_documento%", $tag_doc_customer, $temp);

                        $tag_customer_ident = substr($customer->ident, 0, 11);
                        $temp = str_replace("%cliente_usuario_portal%", $tag_customer_ident, $temp);

                        $tag_customer_clave_portal = substr($customer->clave_portal, 0, 10);
                        $temp = str_replace("%cliente_clave_portal%", $tag_customer_clave_portal, $temp);

                        $tag_deb_total = substr($debts_total, 0, 10);
                        $temp = str_replace("%cliente_saldo_total%", $tag_deb_total, $temp);

                        $tag_deb_mes = substr($customer->debt_month, 0, 10);
                        $temp = str_replace("%cliente_saldo_mes%", $tag_deb_mes, $temp);

                        $enterprise_name = $this->sef($enterprise->name);
                        $tag_enterprise_name = substr($enterprise_name, 0, 20);
                        $temp = str_replace("%empresa_nombre%", $tag_enterprise_name, $temp);

                        $enterprise_address = $this->sef($enterprise->address);
                        $tag_enterprise_address = substr($enterprise_address, 0, 20);
                        $temp = str_replace("%empresa_domicilio%", $tag_enterprise_address, $temp);

                        $tag_enterprise_web = substr($enterprise->web, 0, 20);
                        $temp = str_replace("%empresa_web%", $tag_enterprise_web, $temp);

                        $sms = $this->Sms->newEntity();

                        $temp = $this->sef($temp);

                        $data_sms = [
                            'message'          => $temp,
                            'customer_code'    => $customer->code,
                            'bloque_id'        => $bloque->id,
                            'business_billing' => $enterprise->id,
                            'phone'            => $customer->phone
                        ];

                        $sms = $this->Sms->patchEntity($sms, $data_sms);
                        $this->Sms->save($sms);

                        $smses[$sms->phone] = $sms;

                        switch ($platform_selected) {

                            case 'sms_getway':
                                $message = new \stdClass;
                                $message->phone_number = $customer->phone;
                                $message->message = $temp;
                                $message->device_id = $data->device_id;
                                array_push($bloqueString, $message);
                                break;

                            case 'smsmasivo':
                                //bloque=XXX%2C1161880001%2CTexto+1%0D%0AYYY%2C1161880002%2CTexto+2%0D%0A
                                $bloqueString .= $sms->id . ',' . $customer->phone . ',' . $temp . PHP_EOL;
                        }
                    }

                    $component = $paraments->sms->platform->$platform_selected->component;

                    $smsrespuesta = $this->$component->enviarBloque($bloqueString);

                    switch ($platform_selected) {

                        case 'sms_getway':

                            $smsrespuesta = json_decode($smsrespuesta);
                            if (!empty($smsrespuesta) && !isset($smsrespuesta->status)) {

                                $sms_size = sizeof($smsrespuesta);

                                foreach ($smsrespuesta as $k => $s) {

                                    if ($k == 0) {
                                        $bloque->sms_offset = $s->id;
                                    }

                                    if ($sms_size == ($k + 1)) {
                                        $bloque->sms_limit = $s->id;
                                    }

                                    $sms = $smses[$s->phone_number];
                                    $sms->getway_id = $s->id;
                                    $this->Sms->save($sms);
                                }
                                $bloque->amount = $sms_size;
                                if ($this->SmsBloques->save($bloque)) {
                                    $ok++;
                                    $result_blocks[$bloque->id] = TRUE;
                                } else {
                                    $result_blocks[$bloque->id] = FALSE;
                                    $error++;
                                }
                            } else {
                                $result_blocks[$bloque->id] = FALSE;
                                //$this->log('Error al enviar mensajes con SMS Getway', 'debug');
                                $error++;
                            }
                            break;

                        case 'smsmasivo':

                            if ($smsrespuesta == 'OK') {
                                $result_blocks[$bloque->id] = TRUE;
                                $ok++;
                            } else {
                                $result_blocks[$bloque->id] = FALSE;
                                //$this->log('Error al enviar mensajes con SMS Masivo', 'debug');
                                $error++;
                            }
                            break;
                    }
                }

                $this->loadModel('Users');
                $user = $this->Users->get($this->Auth->user()['id']);
                $user_name = $user->name . ' - username: ' . $user->username;

                $action =  'Envío masivo de SMS';

                $result = "";
                $i = 0;
                foreach ($result_blocks as $ey => $result_block) {
                    $flag = $result_block ? 'OK' : 'ERROR';
                    $limit = " - ";
                    if ($i + 1 == sizeof($result_blocks)) {
                        $limit = "";
                    }
                    $result .= "Bloque nº " . $ey . ': ' . $flag . $limit . PHP_EOL;
                    $i++;
                }

                $now = Time::now();

                $detail = $result;
                $detail .= 'Fecha: ' . $now->format('d/m/Y') . PHP_EOL;
                $detail .=  'Plantilla: ' . $template->name . PHP_EOL;
                $detail .=  'Empresa: ' . $enterprise->name . PHP_EOL;
                $detail .=  'Plataforma: ' . $platform_selected . PHP_EOL;
                $detail .=  'Usuario: ' . $user_name . PHP_EOL;
                $detail .=  'Cantidad SMS OK: ' . $ok . PHP_EOL;
                $detail .=  'Cantidad SMS Error: ' . $error . PHP_EOL;
                $detail .=  'Cantidad total SMS: ' . sizeof($ids) . PHP_EOL;
                $detail .= '---------------------------' . PHP_EOL;

                $this->registerActivity($action, $detail, NULL);

                if ($data->redirect == 'message') {
                    $result = "";
                    $i = 0;
                    foreach ($result_blocks as $ey => $result_block) {
                        $flag = $result_block ? 'OK' : 'ERROR';
                        $limit = " - ";
                        if ($i + 1 == sizeof($result_blocks)) {
                            $limit = "";
                        }
                        $result .= "Bloque nº " . $ey . ': ' . $flag . $limit;
                        $i++;
                    }
                    $this->Flash->success(__($result));
                    //$this->log($result_blocks, 'debug');
                    return $this->redirect(['action' => $data->redirect]);
                } else {
                    $result = "";
                    $i = 0;
                    foreach ($result_blocks as $ey => $result_block) {
                        $flag = $result_block ? 'OK' : 'ERROR';
                        $limit = " - ";
                        if ($i + 1 == sizeof($result_blocks)) {
                            $limit = "";
                        }
                        $result .= "Bloque nº " . $ey . ': ' . $flag . $limit;
                        $i++;
                    }

                    $this->Flash->success(__($result));
                    //$this->log($result_blocks, 'debug');
                    return $this->redirect(['action' => $data->redirect, $template_id]);
                }
            } else {

                $this->Flash->warning(__('La misma acción se intento realizar varias veces.'));
                return $this->redirect(['action' => 'selectCustomersWithArchived']);
            }
        }

        $this->set(compact('templates', 'template', 'disponibles', 'vencimiento_paquete', 'fecha_servidor', 'business', 'block_next_id', 'labels', 'platform', 'available_send', 'sms_getway_devices', 'areas', 'templates_model', 'default_platform', 'default_device', 'controllers', 'services', 'accounts'));
        $this->set('_serialize', ['template']);
    }

    public function messages()
    {
        $paraments = $this->request->getSession()->read('paraments');

        $users = [];
        $this->loadModel('Users');
        $users_model = $this->Users->find();
        foreach ($users_model as $u) {
            $users[$u->id] = $u->name; 
        }

        $templates = [];
        $templates_model = $this->SmsTemplates->find();
        foreach ($templates_model as $temp) {
            $templates[$temp->name] = $temp->name;
        }

        $platforms = [];
        foreach ($paraments->sms->platform as $key => $plat) {
            $platforms[$key] = $plat->name;
        }

        $this->controlMessages();

        $this->set(compact('templates', 'platforms', 'users'));
    }

    public function getMessages()
    {
        if ($this->request->is('ajax')) {

            $response = new \stdClass();

            $response->draw = intval($this->request->getQuery('draw'));
            $archived = filter_var($this->request->getQuery('archived'), FILTER_VALIDATE_BOOLEAN);
            $response->recordsTotal = 0;
            $response->data = [];
            $response->recordsFiltered = 0;

            $this->loadModel('Sms');

            $response->recordsTotal = $this->Sms
                ->find()
                ->contain([
                    'SmsBloques', 
                    'Customers'
                ])->where([
                    'Sms.archived' => $archived
                ])->count();

            $response->data = $this->Sms->find('ServerSideData', [
                'params' => $this->request->getQuery(),
                'where' => ['Sms.archived' => $archived] 
            ]);

            $response->recordsFiltered = $this->Sms->find('RecordsFiltered', [
                'params' => $this->request->getQuery(),
                'where' => ['Sms.archived' => $archived] 
            ]);

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function archivedMessages()
    {
        $paraments = $this->request->getSession()->read('paraments');

        $users = [];
        $this->loadModel('Users');
        $users_model = $this->Users->find();
        foreach ($users_model as $u) {
            $users[$u->id] = $u->name; 
        }

        $templates = [];
        $templates_model = $this->SmsTemplates->find();
        foreach ($templates_model as $temp) {
            $templates[$temp->name] = $temp->name;
        }

        $platforms = [];
        foreach ($paraments->sms->platform as $key => $plat) {
            $platforms[$key] = $plat->name;
        }

        $this->controlMessages();

        $this->set(compact('templates', 'platforms', 'users'));
    }

    public function archiveMessages()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->loadModel('Sms');

            $data = json_decode($this->request->getData('data'));

            foreach ($data->ids as $id) {
                $sms = $this->Sms->get($id);
                $sms->archived = true;
                $this->Sms->save($sms);
            }

            $this->Flash->success(__('Mensajes archivados correctamente.'));
            return $this->redirect(['action' => $data->redirect]);
        }
    }

    private function controlMessages()
    {
        $paraments = $this->request->getSession()->read('paraments');

        foreach ($paraments->sms->platform as $key => $platform) {

            if ($platform->enabled) {

                switch ($key) {

                    case 'smsmasivo':

                        $this->loadModel('Sms');

                        $bloques = $this->Sms->SmsBloques->find()->where(['platform' => $key, 'status' => 'PENDIENTE']);

                        foreach ($bloques as $bloque) {
    
                            $time = $bloque->created->format('YmdHis');

                            $respuesta = $this->SmsMasivo->consultarBloqueFecha($time);

                            $confirm_date = null;

                            if ($respuesta != 'PENDIENTE') {

                                $bloque->status = 'VALIDADO';

                                $respuesta = explode(PHP_EOL, $respuesta);

                                foreach ($respuesta as $res) {

                                    if (!empty($res)) {

                                        $res = explode("\t", $res);

                                        if (sizeof($res) > 2) {

                                            $sms = $this->Sms
                                                ->find()
                                                ->contain([
                                                    'SmsBloques'
                                                ])
                                                ->where([
                                                    'Sms.id' => $res[0]
                                                ])->first();

                                            if ($sms) {

                                                $sms->status = utf8_decode(trim($res[2]));

                                                if (!$confirm_date) {

                                                    $year = substr($res[1], 0, -10);
                                                    $month = substr($res[1], 4, -8);
                                                    $day = substr($res[1], 6, -6);
                                                    $hour = substr($res[1], 8, -4);
                                                    $minute = substr($res[1], 10, -2);
                                                    $second = substr($res[1], 12, 2);
                                                    $confirm_date = $year . '-' . $month . '-' . $day . ' ' . $hour . ':' . $minute . ':' . $second;
                                                }

                                                $this->Sms->save($sms);
                                            }
                                        }
                                    }
                                }

                                $bloque->confirm_date = $confirm_date;
                                $this->Sms->SmsBloques->save($bloque);
                            }
                        }
                        break;
                    case 'sms_getway':

                        $this->loadModel('Sms');
                        $this->loadModel('SmsBloques');

                        $bloques = $this->SmsBloques
                            ->find()
                            ->where([
                                'platform'  => 'sms_getway',
                                'status !=' => 'VALIDADO'
                            ]);

                        $bloques->select(['device_id', 'id', 'sms_offset', 'sms_limit', 'amount']);

                        if ($bloques->count() > 0) {

                            foreach ($bloques as $bloque) {

                                $offset = 0;

                                $limit = $bloque->amount;

                                $filter_search = new \stdClass;

                                $filters = [
                                    [
                                        [
                                            'field' => 'device_id',
                                            'operator' => '=',
                                            'value' => $bloque->device_id
                                        ],
                                        [
                                            'field' => 'status',
                                            'operator' => '!=',
                                            'value' => 'received'
                                        ],
                                        [
                                            'field' => 'id',
                                            'operator' => '>=',
                                            'value' => $bloque->sms_offset
                                        ],
                                        [
                                            'field' => 'id',
                                            'operator' => '<=',
                                            'value' => $bloque->sms_limit
                                        ],
                                    ],
                                ];

                                $order_by = [
                                    [
                                        'field' => 'created_at',
                                        'direction' => 'DESC'
                                    ],
                                    [
                                        'field' => 'status',
                                        'direction' => 'ASC'
                                    ]
                                ];

                                $filter_search->filters = $filters;
                                $filter_search->order_by = $order_by;
                                $filter_search->limit = $limit;
                                $filter_search->offset = $offset;
     
                                $result = $this->SmsGetway->consultarBloque($filter_search);
                                $result = json_decode($result);

                                if (!empty($result) && !property_exists($result, "status")) {

                                    if ($result->count > 0) {

                                        foreach ($result->results as $dev) {

                                            $sms = $this->Sms
                                                ->find()
                                                ->contain([
                                                    'SmsBloques'
                                                ])
                                                ->where([
                                                    'SmsBloques.platform'  => $key,
                                                    'SmsBloques.device_id' => $bloque->device_id,
                                                    'Sms.getway_id'        => $dev->id,
                                                ])->first();

                                            if ($sms) {

                                                switch ($dev->status) {
                                                    case 'sent':
                                                        $status = 'OK';
                                                        break;
                                                    case 'pending':
                                                        $status = 'PENDIENTE';
                                                        break;
                                                    case 'failed':
                                                        $status = 'ERROR';
                                                        break;
                                                    default:
                                                        $status = 'ENCOLADO';
                                                }

                                                $sms->status = $status;
                                                $this->Sms->save($sms);
                                            }
                                        }

                                        $count_sms = $this->Sms
                                            ->find()
                                            ->where([
                                                'bloque_id' => $bloque->id
                                            ])
                                            ->andWhere([
                                                'OR'=>[
                                                    ['status' => 'OK'],
                                                    ['status' => 'ERROR']
                                                ]
                                            ])
                                            ->count();

                                        if ($count_sms == $bloque->amount) {

                                            $bloque->status = 'VALIDADO';
                                            $bloque->confirm_date = Time::now();
                                            $this->Sms->SmsBloques->save($bloque);
                                        }
                                    }
                                }
                            }
                        }
                        break;
                }
            }
        }
    }

    public function getCustomers()
    {
        if ($this->request->is('ajax')) {

            $paraments = $this->request->getSession()->read('paraments');
            $now = Time::now();
            $response = new \stdClass();
            $response->recordsTotal = 0;
            $response->data = [];
            $response->recordsFiltered = [];
            $response->draw = intval($this->request->getQuery('draw'));
            $type = $this->request->getQuery('type');

            if ($type != '') {
                $this->loadModel('Customers');

                $customers_total = $this->Customers->find()
                    ->contain([
                        'Connections'
                    ])->where([
                        'deleted' => FALSE
                    ]);

                $params = $this->request->getQuery();

                if ($params['columns'][10]['search']['value'] != '') {

                    $enabled = $params['columns'][10]['search']['value'];

                    $query = $query->matching('Connections', function ($q) use ($enabled) {
                        return $q->where([
                            'Connections.enabled' => $enabled
                        ]);
                    });
                }

                // Cuentas
                if ($params['columns'][24]['search']['value'] != '') {

                    $payment_getway_id = $params['columns'][24]['search']['value'];

                    $customers_total = $this->Customers
                        ->find()
                        ->matching('CustomersAccounts', function ($q) use ($payment_getway_id) {
                            return $q->where([
                                'CustomersAccounts.payment_getway_id' => $payment_getway_id,
                                'CustomersAccounts.deleted'           => 0
                            ]);
                        })
                        ->where([
                            'Customers.deleted' => FALSE
                        ]);
                }

                // Servicio
                if ($params['columns'][9]['search']['value'] != '') {

                    $customers_ok = [];

                    foreach ($customers_total as $customer) {

                        if (sizeof($customer->connections) > 0) {

                            foreach ($customer->connections as $connection) {

                                $enabled = $params['columns'][9]['search']['value'] ? TRUE : FALSE;

                                if ($connection->deleted == FALSE && $connection->enabled == $enabled) {
                                    array_push($customers_ok, $customer);
                                }
                            }
                        }
                    }
                    $customers_total = $customers_ok;
                }

                // controlador
                if ($params['columns'][22]['search']['value'] != '') {

                    $customers_ok = [];

                    foreach ($customers_total as $customer) {

                        if (sizeof($customer->connections) > 0) {

                            foreach ($customer->connections as $connection) {

                                $controller_id = $params['columns'][22]['search']['value'];

                                if ($connection->deleted == FALSE && $connection->controller_id == $controller_id) {
    
                                    array_push($customers_ok, $customer);
                                }
                            }
                        }
                    }
                    $customers_total = $customers_ok;
                }

                // servicio
                if ($params['columns'][23]['search']['value'] != '') {

                    $customers_ok = [];

                    foreach ($customers_total as $customer) {

                        if (sizeof($customer->connections) > 0) {

                            foreach ($customer->connections as $connection) {

                                $service_id = $params['columns'][23]['search']['value'];

                                if ($connection->deleted == FALSE && $connection->service_id == $service_id) {

                                    array_push($customers_ok, $customer);
                                }
                            }
                        }
                    }
                    $customers_total = $customers_ok;
                }

                foreach ($customers_total as $customer_total) {
                    if ($type == 'smsmasivo'
                        && !$this->verifySMSMasivePhone($customer_total->phone)) {
                        $response->recordsTotal++;
                    }
                }

                $response->data = $this->Customers->find('ServerSideDataCheckSelection', [
                    'params' => $this->request->getQuery(),
                    'where'  => ['Customers.deleted' => FALSE],
                    'type'   => $type
                ]);

                $response->recordsFiltered = $this->Customers->find('RecordsFilteredCheckSelection', [
                    'params' => $this->request->getQuery(),
                    'where'  => ['Customers.deleted' => FALSE] ,
                    'type'   => $type
                ]);
            }

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function getCustomersWithArchived()
    {
        if ($this->request->is('ajax')) {

            $paraments = $this->request->getSession()->read('paraments');
            $now = Time::now();
            $response = new \stdClass();
            $response->recordsTotal = 0;
            $response->data = [];
            $response->recordsFiltered = [];
            $response->draw = intval($this->request->getQuery('draw'));
            $type = $this->request->getQuery('type');

            if ($type != '') {
                $this->loadModel('Customers');

                $customers_total = $this->Customers->find()
                    ->contain([
                        'Connections'
                    ])->where([
                        'super_deleted' => FALSE
                    ]);

                $params = $this->request->getQuery();

                if ($params['columns'][10]['search']['value'] != '') {

                    $enabled = $params['columns'][10]['search']['value'];

                    $query = $query->matching('Connections', function ($q) use ($enabled) {
                        return $q->where([
                            'Connections.enabled' => $enabled
                        ]);
                    });
                }

                // Cuentas
                if ($params['columns'][24]['search']['value'] != '') {

                    $payment_getway_id = $params['columns'][24]['search']['value'];

                    $customers_total = $this->Customers
                        ->find()
                        ->matching('CustomersAccounts', function ($q) use ($payment_getway_id) {
                            return $q->where([
                                'CustomersAccounts.payment_getway_id' => $payment_getway_id,
                                'CustomersAccounts.deleted'           => 0
                            ]);
                        })
                        ->where([
                            'Customers.super_deleted' => FALSE
                        ]);
                }

                // Servicio
                if ($params['columns'][9]['search']['value'] != '') {

                    $customers_ok = [];

                    foreach ($customers_total as $customer) {

                        if (sizeof($customer->connections) > 0) {

                            foreach ($customer->connections as $connection) {

                                $enabled = $params['columns'][9]['search']['value'] ? TRUE : FALSE;

                                if ($connection->deleted == FALSE && $connection->enabled == $enabled) {
                                    array_push($customers_ok, $customer);
                                }
                            }
                        }
                    }
                    $customers_total = $customers_ok;
                }

                // controlador
                if ($params['columns'][22]['search']['value'] != '') {

                    $customers_ok = [];

                    foreach ($customers_total as $customer) {

                        if (sizeof($customer->connections) > 0) {

                            foreach ($customer->connections as $connection) {

                                $controller_id = $params['columns'][22]['search']['value'];

                                if ($connection->deleted == FALSE && $connection->controller_id == $controller_id) {
    
                                    array_push($customers_ok, $customer);
                                }
                            }
                        }
                    }
                    $customers_total = $customers_ok;
                }

                // servicio
                if ($params['columns'][23]['search']['value'] != '') {

                    $customers_ok = [];

                    foreach ($customers_total as $customer) {

                        if (sizeof($customer->connections) > 0) {

                            foreach ($customer->connections as $connection) {

                                $service_id = $params['columns'][23]['search']['value'];

                                if ($connection->deleted == FALSE && $connection->service_id == $service_id) {

                                    array_push($customers_ok, $customer);
                                }
                            }
                        }
                    }
                    $customers_total = $customers_ok;
                }

                foreach ($customers_total as $customer_total) {
                    if ($type == 'smsmasivo' 
                        && !$this->verifySMSMasivePhone($customer_total->phone)) {
                        $response->recordsTotal++;
                    }
                }

                $response->data = $this->Customers->find('ServerSideDataCheckSelectionWithArchived', [
                    'params' => $this->request->getQuery(),
                    'where'  => ['Customers.super_deleted' => FALSE],
                    'type'   => $type
                ]);

                $response->recordsFiltered = $this->Customers->find('RecordsFilteredCheckSelectionWithArchived', [
                    'params' => $this->request->getQuery(),
                    'where'  => ['Customers.super_deleted' => FALSE],
                    'type'   => $type
                ]);
            }

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function fixCustomers()
    {
        $this->loadModel('Customers');

        if ($this->request->is('post')) {
            $action = 'Corrección de Teléfonos';
            $detail = "";
            $this->registerActivity($action, $detail, NULL, TRUE);

            $customs = json_decode($this->request->getData('customers'));
            $no_customers = json_decode($this->request->getData('no_customers'));
            $count = 0;
            foreach ($customs as $custom) {
                $customer = $this->Customers->get($custom->id);
                $customer->phone = $custom->phone;
                $this->Customers->save($customer);
                $count++;
            }
            if (!empty($no_customers)) {
                $message = "Cliente cargados: $count - Clientes no cargados: ";
                foreach ($no_customers as $no_customer) {
                    $message .= 'Código: ' . $no_customer->id . ' | Tel.: ' . $no_customer->phone . ' - ';
                }
                $this->Flash->warning($message);
            } else {
                $this->Flash->success(__("Se ha modificado correctamente $count Teléfonos"));
            }
            return $this->redirect(['action' => 'fixCustomers']);
        }

        $customers_complete = $this->Customers
            ->find()
            ->contain([
                'Areas'
            ])
            ->where([
                'Customers.deleted'       => FALSE,
                'Customers.super_deleted' => FALSE
            ]);
        $customers = [];
        foreach ($customers_complete as $customer_complete) {
            if ($this->verifySMSMasivePhone($customer_complete->phone)) {
                array_push($customers, $customer_complete);
            }
        }

        $this->set(compact('customers'));
        $this->set('_serialize', ['customers']);
    }

    function verifySMSMasivePhone($phone)
    {
        if ($phone == NULL || strlen($phone) < 1) {
            return TRUE;
        }

        $white_space = strpos($phone, ' ');
        $white_space = is_int($white_space);

        $plus = strpos($phone, '+');
        $plus = is_int($plus);

        $dash = strpos($phone, '-');
        $dash = is_int($plus);

        $size = strlen($phone) != 10;

        $zero_initial = $phone[0] == 0;

        return ($white_space || $plus || $dash || !is_numeric($phone) || $size || $zero_initial);
    }

    public function validateAccount($platform)
    {
        $paraments = $this->request->getSession()->read('paraments');
        $component = $paraments->sms->platform->$platform->component;
        return $this->$component->validateAccount();
    }

    public function ajaxValidateAccount()
    {
        if ($this->request->is('ajax')) {
            $paraments = $this->request->getSession()->read('paraments');
            $platform = $this->request->input('json_decode')->platform;
            $component = $paraments->sms->platform->$platform->component;
            $data['error'] = true;
            $data['type'] = 'warning';
            $data['msg'] = 'La credenciales no pertenecen a una cuenta válida. Por favor verifique la configuración.';
            if ($this->$component->validateAccount()) {
                $data['error'] = false;
            }
            $this->set(compact('data'));
            $this->set('_serialize', ['data']);
        }
    }

    public function getDevices()
    {
        if ($this->request->is('ajax')) {

            $paraments = $this->request->getSession()->read('paraments');
            $platform = $this->request->input('json_decode')->platform;
            $token = $this->request->input('json_decode')->token;

            $data['error'] = true;
            $data['type'] = 'warning';

            $component = $paraments->sms->platform->$platform->component;
            $data['msg'] = 'Error al consultar los dispositivos de la cuenta';
            $result = $this->$component->getDevices($token);
            $result = json_decode($result);

            if (!empty($result) && !property_exists($result, "status")) {

                $data['error'] = false;

                if ($result->count > 0) {

                    $devices = [];
                    $paraments->sms->platform->sms_getway->device_id = "";
                    $paraments->sms->platform->sms_getway->devices = [];

                    foreach ($result->results as $dev) {

                        $device = new \stdClass;
                        $device->name =  'Device ID: ' . $dev->id . ' - ' . $dev->type;
                        $device->id = $dev->id;
                        $devices[$dev->id] = $device;
                        $paraments->sms->platform->sms_getway->devices[$dev->id] = $device;
                    }
                    $data['devices'] = $devices;

                    $this->saveGeneralParaments($paraments);

                } else {

                    $data['msg'] = 'La cuenta no tiene ningun dispositivo asociado.';
                }
            }
            $this->set(compact('data'));
            $this->set('_serialize', ['data']);
        }
    }

    public function sef($cadena)
    {
        $sef = trim($cadena);
        $sef = strtr($sef, [
            'á' => 'a',
            'é' => 'e',
            'í' => 'i',
            'ó' => 'o',
            'ú' => 'u',
            'Á' => 'A',
            'É' => 'E',
            'Í' => 'I',
            'Ó' => 'O',
            'Ú' => 'U',
            'ñ' => 'n',
            'Ñ' => 'N',
            'ç' => 'c',
            'Ç' => 'C'
        ]);
        return $sef;
    }
}
