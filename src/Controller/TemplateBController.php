<?php
namespace App\Controller;

use App\Controller\AppController;
use Cidr;
use Cake\Log\Log;

use Cake\Filesystem\File;

class TemplateBController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Integration');
    }

    public function isAuthorized($user = null) 
    {
        return parent::allowRol($user['id'], 'Controllers', 'admin');
    }

    public function edit($id = null) {
        
        $this->loadModel('Controllers');

        $controller = $this->Controllers->get($id, [
            'contain' => []
        ]);

        $tcontroller = $this->Integration->TemplateB->getControllerTemplate($controller); 

        $controller->local_address = $tcontroller->local_address;
        $controller->dns_server = $tcontroller->dns_server;  
        $controller->queue_default = $tcontroller->queue_default;  
        $controller->arp = $tcontroller->arp;
        

        if ($this->request->is(['patch', 'post', 'put'])) {

            $request_data = $this->request->getData();

            $request_data['queue_default'] = ( array_key_exists('queue_default', $request_data)) ? $request_data['queue_default'] : $tcontroller->queue_default; 

            if ($this->Integration->TemplateB->editController($controller, $request_data)) {

                $controller =  $this->Controllers->patchEntity($controller, $request_data);

                if ($this->Controllers->save($controller)) {
                    
                    $this->Flash->success(__('Los cambios se guardaron correctamente.'));
                } else {
                    $this->_ctrl->Flash->error(__('COD #013. No se pudo editar el Controlador.'));
                }
            }
        }


        $controller->queue_default = ($controller->queue_default) ? $controller->queue_default : 'default-small' ; 
        
        // $controller->trademark_name = $this->Integration->getTrademarksByTemplate($controller->template)[$controller->trademark];
        // $controller->template_name = $this->Integration->getTemplates()[$controller->template];
        
        $controller->trademark_name = $this->Integration->getTrademarksByTemplate($controller->template)[$controller->trademark];
        $controller->template_name = $this->Integration->getTemplateName($controller->template);
        
        
        $messageMethods = $this->Integration->getMessageMethods();
        $queue_types_array = $this->Integration->TemplateB->getQueueTypes($controller);

        $this->set(compact('controller', 'messageMethods', 'queue_types_array'));
        $this->set('_serialize', ['controller']);
    }
    
    public function config($id) {
        
        $this->loadModel('Controllers');
        $this->loadModel('Services');

        $controller = $this->Controllers->get($id, [
            'contain' => []
        ]);

        $tcontroller = $this->Integration->TemplateB->getControllerTemplate($controller); 

        $controller->local_address = $tcontroller->local_address;
        $controller->dns_server = $tcontroller->dns_server;  
        $controller->queue_default = $tcontroller->queue_default;  

        $controller->queue_default = ($controller->queue_default) ? $controller->queue_default : 'default-small' ; 

        // $controller->trademark_name = $this->Integration->getTrademarksByTemplate($controller->template)[$controller->trademark];
        // $controller->template_name = $this->Integration->getTemplates()[$controller->template];

        $controller->interfaces = $this->Integration->getInterfaces($controller);
     
        $queue_types_array = $this->Integration->TemplateB->getQueueTypes($controller);

        $services = $this->Services->find()->where(['deleted' => false, 'enabled' => true]);
        $servicesArray = [];
        $servicesArray[''] = '';
        foreach ($services as $service){
            $servicesArray[$service->id] = $service->name;
        }
    

        $this->set(compact('controller', 'queue_types_array', 'servicesArray'));
        $this->set('_serialize', ['controller']);
    }

    public function sync($id) {
        
        $this->loadModel('Controllers');

        $controller = $this->Controllers->get($id);


        $this->clearQueues();
        $this->clearAddressLists();
        $this->clearAddressListsCliente();
        $this->clearGateways();
        $this->clearArp();
        
        
        // $this->diffQueues($controller);
        // $this->diffAddressLists($controller);
        // $this->diffAddressListsCliente($controller);
        
        
        

        $this->set(compact('controller'));
        $this->set('_serialize', ['controller']);
    }
    
    


    //sync queues

    private function clearQueues() {
        
        $tableA = [];
        $tableB = [];
        
        $json = new \stdClass;
        $json->data = $tableA;
        
        $json = json_encode($json, JSON_PRETTY_PRINT);
        $fileA = new File(WWW_ROOT . 'sync/b/queuesA.json', false, 0644);
        $fileA->write($json, 'w', true);
        
        $json = new \stdClass;
        $json->data = $tableB;
        
        
        $json = json_encode($json, JSON_PRETTY_PRINT);
        $fileB = new File(WWW_ROOT . 'sync/b/queuesB.json', false, 0644);
        $fileB->write($json, 'w', true);
        
    }

    public function syncQueues()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
      
            $connections_ids = $this->request->input('json_decode')->connections_ids;
            $delete_queue_side_b = $this->request->input('json_decode')->delete_queue_side_b;
            
            $this->loadModel('Connections');
            $success = true;
            
             if($delete_queue_side_b){
                
                $fileB = new File(WWW_ROOT . 'sync/b/queuesB.json', false, 0644);
                $json = $fileB->read($fileB, 'r');
                $json =  json_decode($json);
                
                foreach($json->data as $row){
                    
                    if($row->marked_to_delete){
                        if(!$this->Integration->TemplateB->deleteQueueInController($controller, $row->api_id->value)){
                             $success = false;
                             break;
                        }
                    }
                }
            }
            
            
            foreach($connections_ids as $connection_id){
                
                $connection = $this->Connections->get($connection_id, ['contain' => ['Controllers', 'Customers']]);
                if(!$this->Integration->TemplateB->syncQueue($connection)){
                    $success = false;
                    break;
                }
            }
            
           
           
            $this->clearQueues();
            
            $this->diffQueues($controller);

            $this->set('data', $success);
        }
    }
  
    public function refreshQueues(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $this->clearQueues();
            
            $this->diffQueues($controller);
            
            $this->set('data', true);
        }
        
    }
  
    public function deleteQueueInController(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
            $queue_api_id = $this->request->input('json_decode')->queue_api_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $success = true;
            
            // $this->log( $queue_api_id , 'debug');
            
            if(!$this->Integration->TemplateB->deleteQueueInController($controller, $queue_api_id->value)){
                 $success = false;
            }
            
            $this->clearQueues();
            
            $this->diffQueues($controller);
            
            $this->set('data', true);
        }
        
    }
    
    private function diffQueues($controller){

        $queuesA = $this->Integration->TemplateB->getQueuesAndConnectionsArray($controller);
        
        $queuesB = $this->Integration->TemplateB->getQueuesInController($controller);
        
        // $this->log($queuesB, 'debug');
        
        $tableA = [];
        $tableB = [];
        
        foreach ($queuesA as $queue_a) {
            
            $row = new RowQueue($queue_a, 'A');
             
            //existe en A pero no en B
            if(!array_key_exists($queue_a['name'], $queuesB)){
                $tableA[] = $row;
            }else{
                
                $diff = false;
                
                $queue_b = $queuesB[$queue_a['name']];
                
                if(strtoupper($queue_a['api_id']) != strtoupper($queue_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($queue_a['name'] != $queue_b['name']){
                    $row->name->state = false;
                     $diff = true;
                }
                
                if($queue_a['comment'] != $queue_b['comment']){
                    $row->comment->state = false;
                    $diff = true;
                }
                
                if($queue_a['target'] != $queue_b['target']){
                    $row->target->state = false;
                    $diff = true;
                }
            
                if($queue_a['max_limit']  != $queue_b['max_limit']){
                    $row->max_limit->state = false;
                    $diff = true;
                }
                
                 if($queue_a['limit_at']  != $queue_b['limit_at']){
                    $row->limit_at->state = false;
                    $diff = true;
                }
                
                 if($queue_a['burst_limit']  != $queue_b['burst_limit']){
                    $row->burst_limit->state = false;
                    $diff = true;
                }
                
                 if($queue_a['burst_threshold']  != $queue_b['burst_threshold']){
                    $row->burst_threshold->state = false;
                    $diff = true;
                }
                
                 if($queue_a['burst_time']  != $queue_b['burst_time']){
                    $row->burst_time->state = false;
                    $diff = true;
                }
                
                 if($queue_a['priority']  != $queue_b['priority']){
                    $row->priority->state = false;
                    $diff = true;
                }
                
                 if($queue_a['queue']  != $queue_b['queue']){
                    $row->queue->state = false;
                    $diff = true;
                }
                
              
                if( $diff){
                    $tableA[] = $row;
                }
                
            }
        }
        
        
        $i = 0;
        
        foreach ($queuesB as $queue_b) {
            
            $row = new RowQueue($queue_b, 'B');
            //existe en B pero no en A
            if(!array_key_exists($queue_b['name'], $queuesA)){
                $row->setNew();
                $tableB[] = $row;
                
            }else{
                
                $diff = false;
                
                $queue_a = $queuesA[$queue_b['name']];
            
                if(strtoupper($queue_a['api_id']) != strtoupper($queue_b['api_id'])){
                    $row->setNew();
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($queue_a['name'] != $queue_b['name']){
                    $row->name->state = false;
                     $diff = true;
                }
                
                if($queue_a['comment'] != $queue_b['comment']){
                    $row->comment->state = false;
                    $diff = true;
                }
               
                if($queue_a['target'] != $queue_b['target']){
                    $row->target->state = false;
                    $diff = true;
                }
                
                if($queue_a['max_limit']  != $queue_b['max_limit']){
                    $row->max_limit->state = false;
                    $diff = true;
                }
                
                 if($queue_a['limit_at']  != $queue_b['limit_at']){
                    $row->limit_at->state = false;
                    $diff = true;
                }
                
                 if($queue_a['burst_limit']  != $queue_b['burst_limit']){
                    $row->burst_limit->state = false;
                    $diff = true;
                }
                
                 if($queue_a['burst_threshold']  != $queue_b['burst_threshold']){
                    $row->burst_threshold->state = false;
                    $diff = true;
                }
                
                 if($queue_a['burst_time']  != $queue_b['burst_time']){
                    $row->burst_time->state = false;
                    $diff = true;
                }
                
                 if($queue_a['priority']  != $queue_b['priority']){
                    $row->priority->state = false;
                    $diff = true;
                }
                
                 if($queue_a['queue']  != $queue_b['queue']){
                    $row->queue->state = false;
                    $diff = true;
                }
                
              
                if( $diff){
                    $tableB[] = $row;
                }
            }
            
        }
        
        
        $json = new \stdClass;
        $json->data = $tableA;
        
        $json = json_encode($json, JSON_PRETTY_PRINT);
        
        $this->getLastErrorJson($tableA);
        
        
        $fileA = new File(WWW_ROOT . 'sync/b/queuesA.json', false, 0644);
        $fileA->write($json, 'w', true);
        
        $json = new \stdClass;
        $json->data = $tableB;
        
        
        $json = json_encode($json, JSON_PRETTY_PRINT);
        
        $this->getLastErrorJson($tableB);
        
        
        $fileB = new File(WWW_ROOT . 'sync/b/queuesB.json', false, 0644);
        $fileB->write($json, 'w', true);
        
    } 
    

    
    //sync arp

    private function clearArp() {
        
        $tableA = [];
        $tableB = [];
        
        $json = new \stdClass;
        $json->data = $tableA;
        
        $json = json_encode($json, JSON_PRETTY_PRINT);
        $fileA = new File(WWW_ROOT . 'sync/b/arpA.json', false, 0644);
        $fileA->write($json, 'w', true);
        
        $json = new \stdClass;
        $json->data = $tableB;
        
        
        $json = json_encode($json, JSON_PRETTY_PRINT);
        $fileB = new File(WWW_ROOT . 'sync/b/arpB.json', false, 0644);
        $fileB->write($json, 'w', true);
    }

    public function syncArp()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
      
            $connections_ids = $this->request->input('json_decode')->connections_ids;
            $delete_arp_side_b = $this->request->input('json_decode')->delete_arp_side_b;
            
            $this->loadModel('Connections');
            $success = true;
            
             if($delete_arp_side_b){
                
                $fileB = new File(WWW_ROOT . 'sync/b/arpB.json', false, 0644);
                $json = $fileB->read($fileB, 'r');
                $json =  json_decode($json);
                
                foreach($json->data as $row){
                    
                    if($row->marked_to_delete){
                        if(!$this->Integration->TemplateB->deleteArpInController($controller, $row->api_id->value)){
                             $success = false;
                             break;
                        }
                    }
                }
            }
            
            
            foreach($connections_ids as $connection_id){
                
                $connection = $this->Connections->get($connection_id, ['contain' => ['Controllers', 'Customers']]);
                if(!$this->Integration->TemplateB->syncArpByConnection($connection)){
                    $success = false;
                    break;
                }
            }
            
           
           
            $this->clearArp();
            
            $this->diffArp($controller);

            $this->set('data', $success);
        }
    }
  
    public function refreshArp(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $this->clearArp();
            
            $this->diffArp($controller);
            
            $this->set('data', true);
        }
        
    }
  
    public function deleteArpInController(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            
            $this->log('deleteArpInController', 'debug');
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            $arp_api_id = $this->request->input('json_decode')->arp_api_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
          
            $this->Integration->TemplateB->deleteArpInController($controller, $arp_api_id->value);
            
            $this->clearArp();
            
            $this->diffArp($controller);
            
            $this->set('data', true);
        }
        
    }
    
    private function diffArp($controller){

        $arpA = $this->Integration->TemplateB->getArpAndConnectionsArray($controller);
        
        $arpB = $this->Integration->TemplateB->getArpInController($controller);
    
        $tableA = [];
        $tableB = [];
        
        foreach ($arpA as $arp_a) {
            
            $row = new RowArp($arp_a, 'A');
             
            //existe en A pero no en B
            if(!array_key_exists($arp_a['address'], $arpB)){
                $tableA[] = $row;
            }else{
                
                $diff = false;
                
                $arp_b = $arpB[$arp_a['address']];
                
                if(strtoupper($arp_a['api_id']) != strtoupper($arp_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($arp_a['address'] != ip2long($arp_b['address'])){
                    $row->address->state = false;
                     $diff = true;
                }
                
                if(strtoupper(implode(':', str_split($arp_a['mac_address'],2))) != $arp_b['mac_address']){
                    $row->mac_address->state = false;
                    $diff = true;
                }
                
                if($arp_a['interface'] != $arp_b['interface']){
                    $row->interface->state = false;
                    $diff = true;
                }
            
                if($arp_a['comment']  != $arp_b['comment']){
                    $row->comment->state = false;
                    $diff = true;
                }
                
              
                if( $diff){
                    $tableA[] = $row;
                }
                
            }
        }
        
        
        $i = 0;
        
        foreach ($arpB as $arp_b) {
            
            $row = new RowArp($arp_b, 'B');
            //existe en B pero no en A
            if(!array_key_exists(ip2long($arp_b['address']), $arpA)){
                $row->setNew();
                $tableB[] = $row;
                
            }else{
                
                $diff = false;
                
                $arp_a = $arpA[ip2long($arp_b['address'])];
            
                if(strtoupper($arp_a['api_id']) != strtoupper($arp_b['api_id'])){
                    $row->setNew();
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($arp_a['address'] != ip2long($arp_b['address'])){
                    $row->address->state = false;
                     $diff = true;
                }
                
                if(strtoupper(implode(':', str_split($arp_a['mac_address'],2))) != $arp_b['mac_address']){
                    $row->mac_address->state = false;
                    $diff = true;
                }
               
                if($arp_a['interface'] != $arp_b['interface']){
                    $row->interface->state = false;
                    $diff = true;
                }
                
                if($arp_a['comment']  != $arp_b['comment']){
                    $row->comment->state = false;
                    $diff = true;
                }
                
              
                if( $diff){
                    $tableB[] = $row;
                }
            }
            
        }
        
        
        $json = new \stdClass;
        $json->data = $tableA;
        
        $json = json_encode($json, JSON_PRETTY_PRINT);
        
        $this->getLastErrorJson($tableA);
        
        
        $fileA = new File(WWW_ROOT . 'sync/b/arpA.json', false, 0644);
        $fileA->write($json, 'w', true);
        
        $json = new \stdClass;
        $json->data = $tableB;
        
        
        $json = json_encode($json, JSON_PRETTY_PRINT);
        
        $this->getLastErrorJson($tableB);
        
        
        $fileB = new File(WWW_ROOT . 'sync/b/arpB.json', false, 0644);
        $fileB->write($json, 'w', true);
        
    } 
    
    
    
    private function getLastErrorJson($data){
        
        $error = false;
        
        switch(json_last_error()) {
            case JSON_ERROR_NONE:
                $error = false;
            break;
            case JSON_ERROR_DEPTH:
                $error =  ' - Excedido tamaño máximo de la pila';
            break;
            case JSON_ERROR_STATE_MISMATCH:
                $error =  ' - Desbordamiento de buffer o los modos no coinciden';
            break;
            case JSON_ERROR_CTRL_CHAR:
                $error =  ' - Encontrado carácter de control no esperado';
            break;
            case JSON_ERROR_SYNTAX:
                $error =  ' - Error de sintaxis, JSON mal formado';
            break;
            case JSON_ERROR_UTF8:
                $error =  ' - Caracteres UTF-8 malformados, posiblemente codificados de forma incorrecta';
            break;
            default:
                $error =  ' - Error desconocido';
            break;
        }
        
        if($error){
            
            $this->loadModel('ErrorLog');
            $log = $this->ErrorLog->newEntity();
            $log->user_id = $this->request->getSession()->read('Auth.User')['id'];
            $log->type = 'Error';
            $log->msg = __($error);
            $log->data = null;
            $log->event = 'TemplateBController.getLastErrorJson';
            $this->ErrorLog->save($log);
        }
            
    }
    
    
    // sync address list
    
    private function clearAddressLists() {
        
        $tableA = [];
        $tableB = [];
        
        $json = new \stdClass;
        $json->data = $tableA;
        
        $json = json_encode($json, JSON_PRETTY_PRINT);
        $fileA = new File(WWW_ROOT . 'sync/b/addressListsA.json', false, 0644);
        $fileA->write($json, 'w', true);
        
        $json = new \stdClass;
        $json->data = $tableB;
        
        
        $json = json_encode($json, JSON_PRETTY_PRINT);
        $fileB = new File(WWW_ROOT . 'sync/b/addressListsB.json', false, 0644);
        $fileB->write($json, 'w', true);
        
    }

    public function syncAddressLists()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $ok = true;
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            $address_lists_ids = $this->request->input('json_decode')->address_lists_ids;
            $delete_address_list_side_b = $this->request->input('json_decode')->delete_address_lists_side_b;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
            
            if($delete_address_list_side_b){
                
                $fileB = new File(WWW_ROOT . 'sync/b/addressListsB.json', false, 0644);
                $json = $fileB->read($fileB, 'r');
                $json =  json_decode($json);
                
                
                foreach($json->data as $row){
                    
                    if($row->marked_to_delete){
                        
                        if(!$this->Integration->TemplateB->deleteAddressListInController($controller, $row->api_id->value)){
                        
                            $ok = false;
                            break;
                        }
                    }
                }
            }
   
            if($ok){
                
                foreach($address_lists_ids as $address_list_id){
                    
                    if(!$this->Integration->TemplateB->syncFirewallAddressList($address_list_id)){
                        $ok = false;
                        break;
                    }
                }
                
            }
            
            $this->clearAddressLists();
            
            $this->diffAddressLists($controller);

            $this->set('response', $ok);
        }
    }
  
    public function refreshAddressLists(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $this->clearAddressLists();
            
            $this->diffAddressLists($controller);
            
            $this->set('data', true);
        }
        
    }
 
    public function deleteAddressListInController(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
            $address_list_api_id = $this->request->input('json_decode')->address_list_api_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $success = true;
          
            if(!$this->Integration->TemplateB->deleteAddressListInController($controller, $address_list_api_id->value)){
                 $success = false;
            }
            
            $this->clearAddressLists();
            
            $this->diffAddressLists($controller);
            
            $this->set('data', true);
        }
        
    }
    
    private function diffAddressLists($controller){

        $addressListsA = $this->Integration->TemplateB->getFirewallAddressListsArray($controller->id);
        
        $addressListsB = $this->Integration->TemplateB->getFirewallAddressListsInController($controller);
        
        $tableA = [];
        $tableB = [];
        
        foreach ($addressListsA as $address_list_a) {
            
            $row = new RowAddressList($address_list_a, 'A');
             
            //existe en A pero no en B
            if(!array_key_exists($address_list_a['list'].':'.$address_list_a['address'], $addressListsB)){
                $tableA[] = $row;
            }else{
                
                $diff = false;
                
                $address_list_b = $addressListsB[$address_list_a['list'].':'.$address_list_a['address']];
                
                if(strtoupper($address_list_a['api_id']) != strtoupper($address_list_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($address_list_a['list'] != $address_list_b['list']){
                    $row->list->state = false;
                     $diff = true;
                }
                
                if($address_list_a['address'] != $address_list_b['address']){
                    $row->address->state = false;
                     $diff = true;
                }
                
                if($address_list_a['comment'] != $address_list_b['comment']){
                    $row->comment->state = false;
                    $diff = true;
                }
              
                if( $diff){
                    $tableA[] = $row;
                }
                
            }
        }
        
        
        $i = 0;
        
        foreach ($addressListsB as $address_list_b) {
            
            $row = new RowAddressList($address_list_b, 'B');
            //existe en B pero no en A
            if(!array_key_exists($address_list_b['list'].':'.$address_list_b['address'], $addressListsA)){
                $row->setNew();
                $tableB[] = $row;
                
            }else{
                
                $diff = false;
                
                $address_list_a = $addressListsA[$address_list_b['list'].':'.$address_list_b['address']];
            
                if(strtoupper($address_list_a['api_id']) != strtoupper($address_list_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                    $row->setNew();
                }
                
                if($address_list_a['list'] != $address_list_b['list']){
                    $row->list->state = false;
                    $diff = true;
                }
                
                if($address_list_a['address'] != $address_list_b['address']){
                    $row->address->state = false;
                    $diff = true;
                }
                
                if($address_list_a['comment'] != $address_list_b['comment']){
                    $row->comment->state = false;
                     $diff = true;
                }
               
              
                if( $diff){
                    $tableB[] = $row;
                }
            }
            
        }
        
        
        $json = new \stdClass;
        $json->data = $tableA;
        
        $json = json_encode($json, JSON_PRETTY_PRINT);
        $fileA = new File(WWW_ROOT . 'sync/b/addressListsA.json', false, 0644);
        $fileA->write($json, 'w', true);
        
        $json = new \stdClass;
        $json->data = $tableB;
        
        $json = json_encode($json, JSON_PRETTY_PRINT);
        $fileB = new File(WWW_ROOT . 'sync/b/addressListsB.json', false, 0644);
        $fileB->write($json, 'w', true);
        
    } 
    
    
    // sync gateway
    
    private function clearGateways() {
        
        $tableA = [];
        $tableB = [];
        
        $json = new \stdClass;
        $json->data = $tableA;
        
        $json = json_encode($json, JSON_PRETTY_PRINT);
        $fileA = new File(WWW_ROOT . 'sync/b/gatewayA.json', false, 0644);
        $fileA->write($json, 'w', true);
        
        $json = new \stdClass;
        $json->data = $tableB;
        
        
        $json = json_encode($json, JSON_PRETTY_PRINT);
        $fileB = new File(WWW_ROOT . 'sync/b/gatewayB.json', false, 0644);
        $fileB->write($json, 'w', true);
        
    }

    public function syncGateways()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $ok = true;
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            $gateway_ids = $this->request->input('json_decode')->gateway_ids;

            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
           
   
            if($ok){
                
                foreach($gateway_ids as $gateway_id){
                    
                    if(!$this->Integration->TemplateB->syncGateway($gateway_id)){
                        $ok = false;
                        break;
                    }
                }
                
            }
            
            $this->clearGateways();
            
            $this->diffGateways($controller);

            $this->set('response', $ok);
        }
    }
  
    public function refreshGateways(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $this->clearGateways();
            
            $this->diffGateways($controller);
            
            $this->set('data', true);
        }
        
    }
 
    public function deleteGatewayInController(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
            $gateway_api_id = $this->request->input('json_decode')->gateway_api_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
          
            $this->Integration->TemplateB->deleteGatewayInController($controller, $gateway_api_id->value);
          
        
            $this->clearGateways();
            
            $this->diffGateways($controller);
            
            $this->set('data', true);
        }
        
    }
    
    private function diffGateways($controller){

        $gatewayA = $this->Integration->TemplateB->getGatewaysArray($controller->id);
        
        $gatewayB = $this->Integration->TemplateB->getGatewaysInController($controller);
        
        $tableA = [];
        $tableB = [];
        
        foreach ($gatewayA as $gateway_a) {
            
            $row = new RowGateway($gateway_a, 'A');
             
            //existe en A pero no en B
            if(!array_key_exists($gateway_a['address'], $gatewayB)){
                $tableA[] = $row;
            }else{
                
                $diff = false;
                
                $gateway_b = $gatewayB[$gateway_a['address']];
                
                if(strtoupper($gateway_a['api_id']) != strtoupper($gateway_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($gateway_a['address'] != $gateway_b['address']){
                    $row->address->state = false;
                     $diff = true;
                }
                
                if($gateway_a['interface'] != $gateway_b['interface']){
                    $row->interface->state = false;
                     $diff = true;
                }
                
                if($gateway_a['comment'] != $gateway_b['comment']){
                    $row->comment->state = false;
                    $diff = true;
                }
              
                if( $diff){
                    $tableA[] = $row;
                }
                
            }
        }
        
        
        $i = 0;
        
        foreach ($gatewayB as $gateway_b) {
            
            $row = new RowGateway($gateway_b, 'B');
            //existe en B pero no en A
            if(!array_key_exists($gateway_b['address'], $gatewayA)){
                $row->setNew();
                $tableB[] = $row;
                
            }else{
                
                $diff = false;
                
                $gateway_a = $gatewayA[$gateway_b['address']];
            
                if(strtoupper($gateway_a['api_id']) != strtoupper($gateway_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                    $row->setNew();
                }
                
                if($gateway_a['address'] != $gateway_b['address']){
                    $row->address->state = false;
                    $diff = true;
                }
                
                if($gateway_a['interface'] != $gateway_b['interface']){
                    $row->interface->state = false;
                    $diff = true;
                }
              
                
                if($gateway_a['comment'] != $gateway_b['comment']){
                    $row->comment->state = false;
                     $diff = true;
                }
               
              
                if( $diff){
                    $tableB[] = $row;
                }
            }
            
        }
        
        
        $json = new \stdClass;
        $json->data = $tableA;
        
        $json = json_encode($json, JSON_PRETTY_PRINT);
        $fileA = new File(WWW_ROOT . 'sync/b/gatewayA.json', false, 0644);
        $fileA->write($json, 'w', true);
        
        $json = new \stdClass;
        $json->data = $tableB;
        
        $json = json_encode($json, JSON_PRETTY_PRINT);
        $fileB = new File(WWW_ROOT . 'sync/b/gatewayB.json', false, 0644);
        $fileB->write($json, 'w', true);
        
    } 
    
    
    
    
    // sync address list Cliente
    
    private function clearAddressListsCliente() {
        
        $tableA = [];
        $tableB = [];
        
        $json = new \stdClass;
        $json->data = $tableA;
        
        $json = json_encode($json, JSON_PRETTY_PRINT);
        $fileA = new File(WWW_ROOT . 'sync/b/addressListsClienteA.json', false, 0644);
        $fileA->write($json, 'w', true);
        
        $json = new \stdClass;
        $json->data = $tableB;
        
        
        $json = json_encode($json, JSON_PRETTY_PRINT);
        $fileB = new File(WWW_ROOT . 'sync/b/addressListsClienteB.json', false, 0644);
        $fileB->write($json, 'w', true);
        
    }

    public function syncAddressListsCliente()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $ok = true;
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            $address_lists_cliente_ids = $this->request->input('json_decode')->address_lists_cliente_ids;
            $delete_address_list_cliente_side_b = $this->request->input('json_decode')->delete_address_lists_cliente_side_b;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
            
            if($delete_address_list_cliente_side_b){
                
                $fileB = new File(WWW_ROOT . 'sync/b/addressListsClienteB.json', false, 0644);
                $json = $fileB->read($fileB, 'r');
                $json =  json_decode($json);
                
                
                foreach($json->data as $row){
                    
                    if($row->marked_to_delete){
                        
                        if(!$this->Integration->TemplateB->deleteAddressListClienteInController($controller, $row->api_id->value)){
                        
                            $ok = false;
                            break;
                        }
                    }
                }
            }
   
            if($ok){
                
                foreach($address_lists_cliente_ids as $address_list_cliente_id){
                    
                    if(!$this->Integration->TemplateB->syncFirewallAddressListCliente($address_list_cliente_id)){
                        $ok = false;
                        break;
                    }
                }
                
            }
            
            
            $this->clearAddressListsCliente();
            
            $this->diffAddressListsCliente($controller);

            $this->set('response', $ok);
        }
    }
  
    public function refreshAddressListsCliente(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $this->clearAddressListsCliente();
            
            $this->diffAddressListsCliente($controller);
            
            $this->set('data', true);
        }
        
    }
 
    public function deleteAddressListClienteInController(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
            $address_list_cliente_api_id = $this->request->input('json_decode')->address_list_cliente_api_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $success = true;
          
            if(!$this->Integration->TemplateB->deleteAddressListClienteInController($controller, $address_list_cliente_api_id->value)){
                 $success = false;
            }
            
            $this->clearAddressListsCliente();
            
            $this->diffAddressListsCliente($controller);
            
            $this->set('data', true);
        }
        
    }
    
    private function diffAddressListsCliente($controller){

        $addressListsA = $this->Integration->TemplateB->getFirewallAddressListsClienteArray($controller->id);
        
        $addressListsB = $this->Integration->TemplateB->getFirewallAddressListsClienteInController($controller);
        
        $tableA = [];
        $tableB = [];
        
        foreach ($addressListsA as $address_list_a) {
            
            $row = new RowAddressList($address_list_a, 'A');
             
            //existe en A pero no en B
            if(!array_key_exists($address_list_a['list'].':'.$address_list_a['address'], $addressListsB)){
                $tableA[] = $row;
            }else{
                
                $diff = false;
                
                $address_list_b = $addressListsB[$address_list_a['list'].':'.$address_list_a['address']];
                
                if(strtoupper($address_list_a['api_id']) != strtoupper($address_list_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($address_list_a['list'] != $address_list_b['list']){
                    $row->list->state = false;
                     $diff = true;
                }
                
                if($address_list_a['address'] != $address_list_b['address']){
                    $row->address->state = false;
                     $diff = true;
                }
                
                if($address_list_a['comment'] != $address_list_b['comment']){
                    $row->comment->state = false;
                    $diff = true;
                }
              
                if( $diff){
                    $tableA[] = $row;
                }
                
            }
        }
        
        
        $i = 0;
        
        foreach ($addressListsB as $address_list_b) {
            
            // $this->log($address_list_b, 'debug');
            
            $row = new RowAddressList($address_list_b, 'B');
            //existe en B pero no en A
            if(!array_key_exists($address_list_b['list'].':'.$address_list_b['address'], $addressListsA)){
                $row->setNew();
                $tableB[] = $row;
                
            }else{
                
                $diff = false;
                
                $address_list_a = $addressListsA[$address_list_b['list'].':'.$address_list_b['address']];
            
                if(strtoupper($address_list_a['api_id']) != strtoupper($address_list_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                    $row->setNew();
                }
                
                if($address_list_a['list'] != $address_list_b['list']){
                    $row->list->state = false;
                    $diff = true;
                }
                
                if($address_list_a['address'] != $address_list_b['address']){
                    $row->address->state = false;
                    $diff = true;
                }
                
                if($address_list_a['comment'] != $address_list_b['comment']){
                    $row->comment->state = false;
                     $diff = true;
                }
               
              
                if( $diff){
                    $tableB[] = $row;
                }
            }
            
        }
        
        
        $json = new \stdClass;
        $json->data = $tableA;
        
        $json = json_encode($json, JSON_PRETTY_PRINT);
        $fileA = new File(WWW_ROOT . 'sync/b/addressListsClienteA.json', false, 0644);
        $fileA->write($json, 'w', true);
        
        $json = new \stdClass;
        $json->data = $tableB;
        
        
        $json = json_encode($json, JSON_PRETTY_PRINT);
        $fileB = new File(WWW_ROOT . 'sync/b/addressListsClienteB.json', false, 0644);
        $fileB->write($json, 'w', true);
        
    } 
    
    
    
  
  
  
  
    //CONFIG  
    
    /**
     * called from: 
     *      controller/templateB/config/plans
    */
    public function getPlansByController(){

        $controller_id = $this->request->getQuery('controller_id');
        
        $plans = $this->Integration->TemplateB->getPlans($controller_id);
    
        $this->set(compact('plans'));
        $this->set('_serialize', ['plans']);
    }

    public function addPlan()
    {
      if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);

            $response = $this->Integration->TemplateB->addPlan($request_data);
            
            $this->set('response', $response);
        }
    }

    public function editPlan()
    {
        if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            $response = $this->Integration->TemplateB->editPlan($request_data);
            
            $this->set('response', $response);
        }
    }

    public function deletePlan()
    {
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $data = $this->request->input('json_decode');
    
            $response = $this->Integration->TemplateB->deletePlan($data->id);

            $this->set('response', $response);
            
        }
    }
    
    /**
     * called from: 
     *      controller/templateB/config/pools
    */
    public function getPoolsByController(){

        $controller_id = $this->request->getQuery('controller_id');
        
        $pools = $this->Integration->TemplateB->getPools($controller_id);
    
        $this->set(compact('pools'));
        $this->set('_serialize', ['pools']);
    }
    
    public function addPool()
    {
        if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            
            $request_data = json_decode(json_encode($data), true);
            
            $response = $this->Integration->TemplateB->addPool($request_data);
            
            $this->set('response', $response);
        }
    }
    
    public function deletePool()
    {
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $data = $this->request->input('json_decode');
            
            $response = $this->Integration->TemplateB->deletePool( $data->id);

            $this->set('response', $response);
        }
    }

    public function editPool()
    {
        if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            $response = $this->Integration->TemplateB->editPool($request_data);
            
            $this->set('response', $response);
        }
    }
    
    public function getRangeNetwork()
    {
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $addresses = $this->request->input('json_decode')->addresses;
            $range = Cidr::cidrToRange($addresses);
            $this->set('range', $range);
        }
    } 
    
    
     /**
     * called from: 
     *      controller/templateB/config/profiles
    */
    public function getProfilesByController(){

        $controller_id = $this->request->getQuery('controller_id');
        
        $profiles = $this->Integration->TemplateB->getProfiles($controller_id);
    
        $this->set(compact('profiles'));
        $this->set('_serialize', ['profiles']);
    }
    
    public function addProfile()
    {
         if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            $response = $this->Integration->TemplateB->addProfile($request_data);
            
            $this->set('response', $response);
        }

    }

    public function deleteProfile()
    {
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $data = $this->request->input('json_decode');
            
            $response = $this->Integration->TemplateB->deleteProfile( $data->id);

            $this->set('response', $response);
        }
    }

    public function editProfile()
    {
        
         if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            $controller_id = $this->request->getData('controller_id');
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
            
            $response = $this->Integration->TemplateB->editProfile($controller, $request_data);
            
            $this->set('response', $response);
        }
 
    }
    
    
    /**
     * called from: 
     *      controller/TemplateB/config/ip-excluded
    */
    public function getIpExcludedByController() {

        $controller_id = $this->request->getQuery('controller_id');
 
        $ip_excluded = $this->Integration->TemplateB->getIPExcluded($controller_id);
    
        $this->set(compact('ip_excluded'));
        $this->set('_serialize', ['ip_excluded']);
    }
    
    public function deleteIPExcluded() {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $data = $this->request->input('json_decode');
            
            $response = $this->Integration->TemplateB->deleteIPExcludedById( $data->id);

            $this->set('response', $response);
        }
    }
    
    public function addIPExcluded() {
        
        if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            $response = $this->Integration->TemplateB->addIPExcludedDirect($request_data);
            
            $this->set('response', $response);
        }
       
    }
    

    
    
    public function applyConfigAvisos(){
        
        if ($this->request->is('ajax')) {
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            
            $response = false;
           
            $paraments = $this->request->getSession()->read('paraments');
            $ip_server = $paraments->system->server->ip;
             
            if($ip_server){
                
                $this->loadModel('Controllers');
                $controller = $this->Controllers->get($controller_id);
                
                $response = $this->Integration->TemplateB->applyConfigAvisos($controller, $ip_server);
                 
             }
            
            $this->set('response', $response);
        }
    } 
    
    public function applyConfigQueuesGraph(){
        
        if ($this->request->is('ajax')) {
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            
            $response = false;
         
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
            $response = $this->Integration->TemplateB->applyConfigQueuesGraph($controller);
             
        
            $this->set('response', $response);
        }
        
    } 
    
    public function applyConfigHttps(){
        
        if ($this->request->is('ajax')) {
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            
            $response  = false;
            
            $paraments = $this->request->getSession()->read('paraments');
            $ip_server = $paraments->system->server->ip;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
            $response = $this->Integration->TemplateB->applyConfigHttps($controller,$ip_server);
             
        
            $this->set('response', $response);
        }
        
    } 
    

    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);

        $id = $_POST['id'];

        //verificar referencias y si no hay eliminar

        $this->loadModel('Connections');
        $count = $this->Connections->find()->where(['controller_id' => $id])->count();

        if ($count > 0) {
            $this->Flash->warning(__('No se puede Eliminar. Existen {0} Conexiones relacionados a este controlador. ', $count));
            return $this->redirect(['action' => 'edit', $id]);
        }

        $this->loadModel('Controllers');
        $inController = $this->Controllers->get($_POST['id']);
        $inController->deleted = true;

        if ($this->Controllers->save($inController)) {
            $this->Flash->success(__('El controlador se elimino correctamente.'));
        } else {
            $this->Flash->error(__('No se pudo eliminar el controlador.'));
        }

        return $this->redirect(['controller' => 'controllers', 'action' => 'index']);
    }
}



class Attr {
    
    public $value;
    public $state;
 
    public function __construct($_value, $_state) {
        $this->value = $_value;
        $this->state = $_state;
    }
    
}


class RowQueue {
   
   
    public $queue_id;
    public $connection_id;
    
    public $marked_to_delete;
    
    public $api_id;
    public $name;
    public $comment;
    
    public $target;
    public $max_limit;
    public $limit_at;
    public $burst_limit;
    public $burst_threshold;
    public $burst_time;
    public $priority;
    public $queue;

    
    public function __construct($queue, $side) {
        
        $this->marked_to_delete = false;
        
        if($side == 'A'){
            $this->queue_id = $queue['id'];
            $this->connection_id = $queue['connection']['id'];
        }
        // else{
        //     $this->queue_id = str_replace('*','',$queue['api_id']);
        // }
        
        $this->api_id = new Attr($queue['api_id'], true);
        
        if($side == 'A'){
            
            $this->name = new Attr($queue['name'], true);
            $this->comment = new Attr($queue['comment'], true);
        }
        else{
        
            $this->name = new Attr($this->convert_to($queue['name'], "UTF-8"), true);
            $this->comment = new Attr($this->convert_to($queue['comment'], "UTF-8"), true);
            
        }
        
        if($side == 'A'){
            $this->target = new Attr($queue['target'], true);
        }else{
            $this->target = new Attr($queue['target'], true);
        }
        
        $this->max_limit = new Attr($queue['max_limit'], true);
        $this->limit_at = new Attr($queue['limit_at'], true);
        $this->burst_limit = new Attr($queue['burst_limit'], true);
        $this->burst_threshold = new Attr($queue['burst_threshold'], true);
        $this->burst_time = new Attr($queue['burst_time'], true);
        $this->priority = new Attr($queue['priority'], true);
        $this->queue = new Attr($queue['queue'], true);
       
    
    }
    
    private function convert_to( $source, $target_encoding )  {
        
        if($source != ''){
            
            // detect the character encoding of the incoming file
            $encoding = mb_detect_encoding( $source, "auto" );
               
            // escape all of the question marks so we can remove artifacts from
            // the unicode conversion process
            $target = str_replace( "?", "[question_mark]", $source );
               
            // convert the string to the target encoding
            $target = mb_convert_encoding( $target, $target_encoding, $encoding);
               
            // remove any question marks that have been introduced because of illegal characters
            $target = str_replace( "?", "", $target );
               
            // replace the token string "[question_mark]" with the symbol "?"
            $target = str_replace( "[question_mark]", "?", $target );
           
            return $target;
        }
        
         return $source;
    }
    
    
    public function setNew(){
        
        $this->marked_to_delete = true;
        
        $this->api_id->state = false;
        $this->name->state = false;
        $this->target->state = false;
        $this->comment->state = false;
        $this->max_limit->state = false;
        $this->limit_at->state = false;
        $this->burst_limit->state = false;
        $this->burst_threshold->state = false;
        $this->burst_time->state = false;
        $this->priority->state = false;
        $this->queue->state = false;

    }
    
}

// class RowProfile {
   
  
//     public $marked_to_delete;
    
//     public $id;
//     public $api_id;
//     public $name;
//     public $local_address;
//     public $dns_server;
//     public $rate_limit_string;
//     public $queue_type;

    
//     public function __construct($profile, $side) {
        
//         $this->marked_to_delete = false;
        
//         if($side == 'A'){
//             $this->id = $profile['id'];
//         }
        
//         $this->api_id = new Attr($profile['api_id'], true);
//         $this->name = new Attr($profile['name'], true);
//         $this->local_address = new Attr($profile['local_address'], true);
//         $this->dns_server = new Attr($profile['dns_server'], true);
//         $this->rate_limit_string = new Attr($profile['rate_limit_string'], true);
//         $this->queue_type = new Attr($profile['queue_type'], true);
//     }
    
    
//     public function setNew(){
        
//         $this->marked_to_delete = true;
        
//         $this->api_id->state = false;
//         $this->name->state = false;
//         $this->local_address->state = false;
//         $this->dns_server->state = false;
//         $this->rate_limit_string->state = false;
//         $this->queue_type->state = false;
//     }
    
// }

class RowAddressList {
   
  
    public $marked_to_delete;
    
    public $id;
    public $api_id;
    public $list;
    public $address;
    public $comment;
    
    public function __construct($addressList, $side) {
        
        $this->marked_to_delete = false;
        
        if($side == 'A'){
            $this->id = $addressList['id'];
        }
        
        $this->api_id = new Attr($addressList['api_id'], true);
        $this->list = new Attr($addressList['list'], true);
        $this->address = new Attr($addressList['address'], true);
    
        
        if($side == 'A'){
            $this->comment = new Attr($addressList['comment'], true);
        }
        else{
            $this->comment = new Attr(utf8_decode($addressList['comment']), true);
        }
     
    }
    
    
    public function setNew(){
        
        $this->marked_to_delete = true;
        
        $this->api_id->state = false;
        $this->list->state = false;
        $this->address->state = false;
        $this->comment->state = false;
    }
    
}

class RowGateway {
    
    public $id;
    public $api_id;
    public $interface;
    public $address;
    public $comment;
    
    public $marked_to_delete;
    
    public function __construct($gateway, $side) {
        
        $this->marked_to_delete = false;
        
        if($side == 'A'){
            $this->id = $gateway['id'];
        }
        
        $this->api_id = new Attr($gateway['api_id'], true);
        $this->interface = new Attr($gateway['interface'], true);
        $this->address = new Attr($gateway['address'], true);

        
        if($side == 'A'){
            $this->comment = new Attr($gateway['comment'], true);
        }
        else{
            $this->comment = new Attr(utf8_decode($gateway['comment']), true);
        }
        
     
    }
    
    
    public function setNew(){
        
        $this->marked_to_delete = true;
        
        $this->api_id->state = false;
        $this->interface->state = false;
        $this->address->state = false;
        $this->comment->state = false;
    }
    
}

class RowArp {
   
   
    public $arp_id;
    public $connection_id;
    
    public $marked_to_delete;
    
    public $api_id;
    public $address;
    public $mac_address;
    
    public $interface;
    public $comment;

    
    public function __construct($arp, $side) {
        
        $this->marked_to_delete = false;
        
        if($side == 'A'){
            $this->arp_id = $arp['id'];
            $this->connection_id = $arp['connection']['id'];
        }
       
        $this->api_id = new Attr(strtoupper($arp['api_id']), true);
        
        if($side == 'A'){
            $this->address = new Attr(long2ip($arp['address']), true);
        }else{
            $this->address = new Attr($arp['address'], true);
        }
        
        if($side == 'A'){
            $this->comment = new Attr($arp['comment'], true);
        }
        else{
            $this->comment = new Attr(utf8_decode($arp['comment']), true);
        }
        
        if($side == 'A'){
        
            $this->mac_address = new Attr(strtoupper (implode(':', str_split($arp['mac_address'],2))), true);
        }else{
            $this->mac_address = new Attr($arp['mac_address'], true);
        }
        
        
        
        $this->interface = new Attr($arp['interface'], true);
    }
    
    
    public function setNew(){
        
        $this->marked_to_delete = true;
        
        $this->api_id->state = false;
        $this->address->state = false;
        $this->mac_address->state = false;
        $this->interface->state = false;
        $this->comment->state = false;

    }
    
}






