<?php
namespace App\Controller;

use App\Controller\AppController;
use PEAR2\Net\RouterOS;
use Cake\Core\Configure;
use Cidr;
use Cake\Datasource\ConnectionManager;
use PEAR2\Net\RouterOS\SocketException;
use PEAR2\Net\RouterOS\DataFlowException;
use Cake\I18n\Time;
use Cake\Event\Event;
use FPDF;
use App\Controller\Component\PDFGeneratorComponent;

/**
 * Portal Controller
 *
 */
class PortalController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->Auth->config('loginAction', [
            'controller' => 'Portal',
            'action' => 'login'
        ]);

        $this->Auth->allow(['forgotPassword', 'randomPassword', 'sendMailForgotPassword']);

        if ($this->request->getSession()->read('type') != 'portal' ) {
            if ($this->Auth) {
                $this->Auth->logout();
            }
        }

        $this->loadModel('Customers');
        $this->loadComponent('PDFGenerator');
    }

    public function isAuthorized($user)
    {

        if ($this->request->getParam('action') == 'login') {
            return true;
        }

        if ($this->request->getParam('action') == 'index') {
            return true;
        }

        if ($this->request->getParam('action') == 'resumes') {
            return true;
        }

        if ($this->request->getParam('action') == 'receipts') {
            return true;
        }

        if ($this->request->getParam('action') == 'profile') {
            return true;
        }

        if ($this->request->getParam('action') == 'printresume') {
            return true;
        }

        if ($this->request->getParam('action') == 'printreceipt') {
            return true;
        }

        if ($this->request->getParam('action') == 'logout') {
            return true;
        }

        if ($this->request->getParam('action') == 'forgotPassword') {
            return true;
        }

        if ($this->request->getParam('action') == 'randomPassword') {
            return true;
        }

        if ($this->request->getParam('action') == 'sendMailForgotPassword') {
            return true;
        }

        return false;
    }

    public function index()
    {
        $code = $this->Auth->user()['code'];
        $customer = $this->Customers->get($code, [
            'contain' => []
        ]);

        $this->loadModel('Connections');
        $customer->connections = $this->Connections->find()->contain(['Users', 'Controllers', 'Services'])->where(['customer_code' => $code]);

        $this->loadModel('Debts');
        $customer->debts = $this->Debts->find()->contain(['Users'])->where(['invoice_id IS' => NULL, 'customer_code' => $code]);

        $this->loadModel('Invoices');
        $customer->invoices = $this->Invoices->find()->contain(['Users', 'Concepts'])->where(['customer_code' => $code]);

        $this->loadModel('Payments');
        $customer->payments = $this->Payments->find()->contain(['Users', 'CashEntities', 'Receipts'])->where(['Payments.customer_code' => $code]);

        $paraments = $this->request->getSession()->read('paraments');

        foreach ($customer->payments as $payment) {

            foreach ($paraments->payment_getway as $pg) {

                if ($pg->id == $payment->payment_method_id) {
                    $payment_method = new \stdClass;
                    $payment_method = $pg;
                    $payment->payment_method = $payment_method;
                }
            }
        }

        $this->loadModel('CreditNotes');
        $customer->creditNotes = $this->CreditNotes->find()->contain(['Users'])->where(['customer_code' => $code]);

        $this->loadModel('DebitNotes');
        $customer->debitNotes = $this->DebitNotes->find()->contain(['Users'])->where(['customer_code' => $code]);

        $next_month = Time::now()->modify('+1 month')->day(1)->hour(0)->minute(0)->second(0);

        $customer->debt_total = 0;

        foreach ($customer->debts as $debt) {
            $customer->debt_total += $debt->total;
        }

        foreach ($customer->invoices as $invoice) {
            $customer->debt_total += $invoice->total;
        }
        
        foreach ($customer->debitNotes as $debitNote) {
            $customer->debt_total += $debitNote->total;
        }

        foreach ($customer->payments as $pay) {
            $customer->debt_total -= $pay->import;
        }

        foreach ($customer->creditNotes as $creditNote) {
            $customer->debt_total -= $creditNote->total;
        }

        $customer->from = Time::now()->day(1)->hour(0)->minute(0)->second(0);
        $customer->to = Time::now()->modify('+1 month')->day(1)->hour(0)->minute(0)->second(0);

        $this->set(compact('customer'));
        $this->set('_serialize', ['customer']);
    }

    public function resumes()
    {
        $code = $this->Auth->user()['code'];
        $data = new \stdClass;
        $data->code = $code;
        $data->from = Time::now()->day(1);
        $data->to =  Time::now()->modify('+1 month')->day(1);
        $resumes = $this->PDFGenerator->generate($data, PDFGeneratorComponent::TYPE_ACCOUNT_RESUME, true);
        $customer = $resumes->customer;

        $general = new \stdClass;
        $general->date = Time::now()->format('d/m/Y');
        $general->saldo = $resumes->saldo;
        $duedate = new Time();
        $duedate->year($resumes->to->year);
        $duedate->month($resumes->to->month);
        $duedate->day($resumes->customer->daydue);
        $general->duedate = $duedate->format('d/m/Y');
        $general->customer_name = strtoupper($resumes->customer->name);
        $general->customer_doc_type = $this->doc_types[$resumes->customer->doc_type] .' '. $resumes->customer->ident;
        $general->customer_code = $code;
        $general->periode = $resumes->from->format('d/m/Y') . ' - ' . $resumes->to->format('d/m/Y');

        $this->set(compact('customer', 'resumes', 'general'));
        $this->set('_serialize', ['customer']);
    }

    public function receipts()
    {
        $code = $this->Auth->user()['code'];
        $customer = $this->Customers->get($code, [
            'contain' => []
        ]);
        $this->loadModel('Payments');
        $customer->payments = $this->Payments->find()->contain(['Receipts'])->where(['Payments.customer_code' => $code]);
        $this->set(compact('customer'));
        $this->set('_serialize', ['customer']);
    }

    public function profile()
    {
        $code = $this->Auth->user()['code'];
        $customer = $this->Customers->get($code, [
            'contain' => []
        ]);
        $this->set(compact('customer'));
        $this->set('_serialize', ['customer']);
    }

    public function login()
    {

        if ($this->request->getSession()->check('Auth.User')) {
            $this->Flash->warning(__('Ya se encuentra iniciada su sesión.'));
            return $this->redirect(['action' => 'index']);
        }

        if ($this->request->is('post')) {

            $this->loadModel('Customers');

            $customer = $this->Customers->find()->where([
                'ident' => $this->request->getData('num'),
                'clave_portal' => $this->request->getData('psw'),
                'deleted' => false
            ])->first();

            if ($customer) {

                if ($customer->deleted) {

                    $this->Flash->warning(__('No se encuentra el usuario. Puede que se haya eliminado.'));
                    return $this->redirect(['action' => 'login']);
                }

                $this->Auth->setUser($customer);
            
                $this->request->getSession()->write('type', 'portal');
                $customer = $this->request->getSession()->read('Auth.User');
                //ver a donde redirigir
                return $this->redirect(['action' => 'index', $customer->code]);
            } 
            else {
                $this->Flash->error(__('El usuario o la clave son incorrectas.'));
            }
        }
    }

    public function logout()
    {
     
        $this->Auth->logout();
        $this->request->getSession()->write('type', '');
        return $this->redirect(['action' => 'login']);
    }

    private function randomPassword($len = 6)
    {
        $alphabet = "abcdefghijklmnopqrstuwxyz0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $len; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    private function sendMailForgotPassword($customer, $email)
    {

        $paraments = $this->request->getSession()->read('paraments');
        $company_name = $paraments->invoicing->company->name;
    	$titulo    = 'Restablecimiento de contraseña';

    	$html = "
        <html>
    		<head>
    			<title>" . $titulo . "</title>
    		</head>
    		<body>
    			<p>Tu contrase se restablecio. Usa esta nueva contraseña para ingresar.</p>
    			<br>
    			<br>
    			<p>Tu nueva contraseña: <b>" . $customer->password . "</b></p>
    		</body>
    	</html>";

    	$headers = "MIME-Version: 1.0" . "\r\n";
    	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

    	// More headers
    	$headers .= "From: <contacto@$company_name.com>" . "\r\n";

        return mail($email, $titulo, $html, $headers);
    }

    public function changePwd()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {

            $this->loadModel('Customers');

            $id = $this->Auth->user()['id'];

            $customer = $this->Customers->get($id, [
                'contain' => []
            ]);

            $customer->password = $this->request->getData('newclave');

            if ($this->Customers->save($customer)) {
                $this->Flash->success(__('Contraseña actualizada.'));
            } else {
                $this->Flash->error(__('Error al intentar cambiar la contraseña.'));
            }

            return $this->redirect(['action' => 'edit']);
        }
    }

    public function forgotPassword()
    {

        if ($this->request->is(['patch', 'post', 'put'])) {

            $this->loadModel('Customers');

            $email = $this->request->getData('email');

            $customer = $this->Customers->find()->where(['email' => $email])->first();
            if (!$customer) {
                 $this->Flash->warning(__('El correo no esta registrada.', $email));
                 return $this->redirect(['action' => 'login']);
            }

            $customer->password = $this->randomPassword();
            if ($this->Customers->save($customer)) {

                if ($this->sendMailForgotPassword($customer, $customer->email)) {
                    $this->Flash->success(__('Se envió a tu correo una nueva clave.'));
                } else {
                     $this->Flash->error(__('Error al intentar enviar el email.'));
                }

            } else {
                $this->Flash->error(__('Error al intentar restablecer la contraseña.'));
            }

            return $this->redirect(['action' => 'login']);
        }
    }

    public function dashboard($code)
    {
        $this->loadModel('Customers');

        $customer = $this->Customers->get($code, [
            'contain' => [
                'Users',
                'Cities',
                'Services',
                'Connections.Services',
                'Debts.Users',
                'Payments.CashEntities',
                'Docs.Debts',
                'Docs.Payments'
            ]
        ]);

        $next_month = Time::now();
        $next_month->modify('+1 month');
        $next_month->day(1);

        $customer->debt_total = 0;
        $customer->debt_month = 0;

        foreach ($customer->debts as $debt) {
            if (!$debt->deleted && !$debt->doc_id) { // no esta eliminada y no esta facturado
                $customer->debt_total += $debt->subtotal;
                if ($debt->duedate < $next_month) {
                    $customer->debt_month += $debt->subtotal;
                }
            }
        }

        foreach ($customer->docs as $doc) {
            if (!$doc->deleted) { // no esta eliminado
                if ($doc->type_comp == 'FX' || $doc->type_comp == 'FA' || $doc->type_comp == 'FB' || $doc->type_comp == 'FC') {
                    $customer->debt_total += $doc->total;
                    if ($doc->duedate < $next_month) {
                        $customer->debt_month += $doc->total;
                    }
                } else if ($doc->type_comp == 'CX') {
                    if ($doc->created < $next_month) {
                        $customer->debt_total -= $doc->total;
                        $customer->debt_month -= $doc->total;
                    }
                    
                }
            }
        }

        foreach ($customer->payments as $pay) {
            if (!$pay->deleted) { // no esta eliminado
                $customer->debt_total -= $pay->import;
                if ($pay->created < $next_month) {
                    $customer->debt_month -= $pay->import;
                }
            }
        }

        $date = Time::now();
        $this->loadModel('Paraments');
        $paraments = $this->Paraments->get(1);

        $this->set('customer', $customer);
        $this->set('date', $date);
        $this->set('paraments', $paraments);
        $this->set('_serialize', ['customer']);
    }

    public function view($code = null)
    {

        $customer = $this->Customers->get($code, [
            'contain' => [
                'Users',
                'Cities',
                'Services',
                'Connections.Controllers',
                'Connections.Services',
                'Debts.Users',
                'Payments.Users',
                'Payments.CashEntities',
                'Docs.Debts',
                'Docs.Users'
            ]
        ]);

        $this->getResume($customer);

        $next_month = Time::now();
        $next_month->modify('+1 month');
        $next_month->day(1);

        $debt_total = 0;
        $debt_month = 0;

        foreach ($customer->debts as $debt) {
            if (!$debt->deleted && !$debt->doc_id) { // no esta eliminada y no esta facturado
                $debt_total += $debt->subtotal;
                if ($debt->duedate < $next_month) {
                    $debt_month += $debt->subtotal;
                }
            }
        }

        foreach ($customer->docs as $doc) {
            if (!$doc->deleted) { // no esta eliminado
                if ($doc->type_comp == 'FX' || $doc->type_comp == 'FA' || $doc->type_comp == 'FB' || $doc->type_comp == 'FC') {
                    $debt_total += $doc->total;
                    if ($doc->duedate < $next_month) {
                        $debt_month += $doc->total;
                    }
                } else if ($doc->type_comp == 'CX') {
                    $debt_total -= $doc->total;
                    $debt_month -= $doc->total;
                }
            }
        }

        foreach ($customer->payments as $pay) {
            if (!$pay->deleted) { // no esta eliminado
                $debt_total -= $pay->import;
                if ($pay->created < $next_month) {
                    $debt_month -= $pay->import;
                }
            }
        }

        //logs

        $this->loadModel('Logs');

        $logs_customer = $this->Logs->find()->contain([
            'Users',
            'Customers',
            'Connections',
            'Services',
            'Products',
            'Packages'
        ])->where([
            'Logs.user_id != ' => 0,
            'Logs.customer_code' => $code
        ]);

        $this->set(compact('debt_total','debt_month', 'logs_customer'));
        $this->set('customer', $customer);
        $this->set('_serialize', ['customer', 'debt_total', 'debt_month']);
    }

    public function printresume($code)
    {

        $data =  new \stdClass;
        $data->code = $code;

        $data->from = Time::now()->day(1);
        $data->to =  Time::now()->modify('+1 month')->day(1);

        $this->PDFGenerator->generate($data, PDFGeneratorComponent::TYPE_ACCOUNT_RESUME);
    }

    public function printreceipt($id)
    {

        $data =  new \stdClass;
        $data->id = $id;

        $this->PDFGenerator->view($data, PDFGeneratorComponent::TYPE_RECEIPT);
    }
}
