<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ConnectionsHasMessageTemplates Controller
 *
 * @property \App\Model\Table\ConnectionsHasMessageTemplatesTable $ConnectionsHasMessageTemplates
 */
class ConnectionsHasMessageTemplatesController extends AppController
{
    public function isAuthorized($user = null)
    {
        return parent::allowRol($user['id'], 'AvisoHttp', 'admin');
    }

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('IntegrationRouter', [
             'className' => '\App\Controller\Component\Integrations\IntegrationRouter'
        ]);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $chmts = $this->ConnectionsHasMessageTemplates->find()
            ->contain(['Connections.Customers', 'Connections.Areas', 'MessageTemplates'])
            ->where(['MessageTemplates.type' => 'Aviso']);

        $this->set(compact('chmts'));
        $this->set('_serialize', ['chmts']);
    }

    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);

        $chmts_selected = $_POST['chmts_selected'];

        $counter = 0;
        $counter_unsync = 0;
        $controllers_unsync = '';

        if ($chmts_selected) {

            $chmts_selected = explode(',', $chmts_selected);

            foreach ($chmts_selected as $id) {

                $chmt = $this->ConnectionsHasMessageTemplates->get($id, ['contain' => ['Connections.Controllers']]);

                if (!$this->ConnectionsHasMessageTemplates->delete($chmt)) {
                    $this->Flash->error(__('Error al intenatr eliminar.'));
                    return $this->redirect(['action' => 'index']);   
                }

                //cuando ya no quedan mas avisos con esta ip

                if ($this->ConnectionsHasMessageTemplates->find()->where(['ip' => $chmt->ip, 'type' => 'Aviso'])->count() == 0) {

                    $chmt->connection =  $this->IntegrationRouter->removeAvisoConnection($chmt->connection, true);

                    $counter++;

                    if ($chmt->connection->diff) {
                         $counter_unsync++;   
                         $controllers_unsync .= $chmt->connection->controller->name . ', ';
                    }
                }
            }

            $this->Flash->success(__('Avisos eliminados {0} .', $counter));

            if ($counter_unsync > 0) {
                  $this->Flash->warning(__('Avisos no eliminados en los controladores {0}.', $counter_unsync));  
                  $this->Flash->warning(__('Controladores desconectados: {0}.', $controllers_unsync));  
             } 
        }

        return $this->redirect(['action' => 'index']);
    }
}
