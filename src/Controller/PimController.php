<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Event\Event;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\Filesystem\Folder;

use App\Controller\Component\ComprobantesComponent;
use App\Controller\Component\AccountantComponent;
use App\Controller\Component\PDFGeneratorComponent;

class PimController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Accountant', [
             'className' => '\App\Controller\Component\Admin\Accountant'
        ]);
    }

    public function isAuthorized($user = null) 
    {
        return parent::allowRol($user['id']);
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function index()
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');
        $account_enabled = $this->Accountant->isEnabled();

        if ($this->request->is(['patch', 'post', 'put'])) {

            $payment_getway->config->pim->enabled = $this->request->getData('enabled');
            $payment_getway->config->pim->cash = $this->request->getData('cash');
            $payment_getway->config->pim->portal = $this->request->getData('portal');

            if ($account_enabled) {
                $payment_getway->config->pim->account = $this->request->getData('account');
            }

            $this->savePaymentGetwayParaments($payment_getway);
        }

        $this->loadModel('Accounts');
        $accountsTitles = $this->Accounts->find()->where(['title' => TRUE])->order(['code' => 'ASC']);
        $accounts = $this->Accounts
            ->find()
            ->order([
                'code' => 'ASC'
            ]);

        $this->set(compact('accountsTitles', 'accounts', 'payment_getway', 'account_enabled'));
        $this->set('_serialize', ['payment_getway', 'accounts']);
    }
}
