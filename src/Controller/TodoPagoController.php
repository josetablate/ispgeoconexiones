<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Event\Event;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\Filesystem\Folder;

use App\Controller\Component\ComprobantesComponent;
use App\Controller\Component\AccountantComponent;
use App\Controller\Component\PDFGeneratorComponent;

class TodoPagoController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Accountant', [
             'className' => '\App\Controller\Component\Admin\Accountant'
        ]);
    }

    public function isAuthorized($user = null) 
    {
        if ($this->request->getParam('action') == 'operationsControl') {
            return true;
        }

        if ($this->request->getParam('action') == 'strToDeciaml') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow('operationsControl');
    }

    public function index()
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');
        $account_enabled = $this->Accountant->isEnabled();

        if ($this->request->is(['patch', 'post', 'put'])) {

            $payment_getway->config->todopago->MODO = $this->request->getData('payment_getway.todopago.MODO');
            $payment_getway->config->todopago->test->merchant_id = $this->request->getData('test-merchant_id');
            $payment_getway->config->todopago->test->api_keys = $this->request->getData('test-api_keys');
            $payment_getway->config->todopago->prod->merchant_id = $this->request->getData('prod-merchant_id');
            $payment_getway->config->todopago->prod->api_keys = $this->request->getData('prod-api_keys');
            $payment_getway->config->todopago->enabled = $this->request->getData('enabled');
            $payment_getway->config->todopago->cash = $this->request->getData('cash');
            $payment_getway->config->todopago->portal = $this->request->getData('portal');
            $payment_getway->config->todopago->hours_execution = $this->request->getData('hours_execution');

            if ($account_enabled) {
                $payment_getway->config->todopago->account = $this->request->getData('account');
            }

            $this->savePaymentGetwayParaments($payment_getway);

            // PORTAL BEGIN

            // $portal = $this->request->getSession()->read('portal');

            // $portal->payment_getway->todopago->MODO = $this->request->getData('payment_getway.todopago.MODO');
            // $portal->payment_getway->todopago->test->merchant_id = $this->request->getData('test-merchant_id');
            // $portal->payment_getway->todopago->test->api_keys = $this->request->getData('test-api_keys');
            // $portal->payment_getway->todopago->prod->merchant_id = $this->request->getData('prod-merchant_id');
            // $portal->payment_getway->todopago->prod->api_keys = $this->request->getData('prod-api_keys');
            // $portal->payment_getway->todopago->enabled = $this->request->getData('enabled');

            // $this->savePortalParaments($payment_getway);

            // PORTAL END
        }
        
        $this->loadModel('Accounts');
        $accountsTitles = $this->Accounts->find()->where(['title' => true])->order(['code' => 'ASC']);
        $accounts = $this->Accounts->find()
            ->order([
                'code' => 'ASC'
            ]);

        $this->set(compact('payment_getway', 'accountsTitles', 'accounts', 'account_enabled'));
        $this->set('_serialize', ['payment_getway']);
    }

    public function operationsControl()
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');

        //CONSULTAR A TODO PAGO LAS TRANSACCIONES
        $this->loadComponent('TodoPago');

        //CONSULTA RANGO DE TIEMPO
        $from = Time::now()->modify($payment_getway->config->todopago->period)->format('Y-m-d'); //'d/m/Y'

        $to = Time::now()->format('Y-m-d');

        $data = [
            'code'  => 200
        ];
        $message = "";

        for ($page_number = 1; $page_number < 6; $page_number++) {
            $result = $this->TodoPago->getByRangeDateTime($from, $to, $page_number);

            if (array_key_exists('Operations', $result)) {

                $this->loadModel('Users');
                $this->loadModel('Customers');
                $this->loadModel('Payments');
                $this->loadModel('TpTransactions');

                $this->loadComponent('Accountant');
                $this->loadComponent('Comprobantes');
                $this->loadComponent('PDFGenerator');

                $user_sistema = 100;

                $operations = $result['Operations'];

                //ESTRUCTURA DE DATO
                /*'Operations' => [
            		(int) 0 => [
            			'RESULTCODE' => '99995',
            			'RESULTMESSAGE' => 'Tu tarjeta no autorizó tu compra. Iniciala nuevamente utilizando otro medio de pago.',
            			'DATETIME' => '2018-03-27T14:54:03.670-03:00',
            			'OPERATIONID' => '15221732861',
            			'CURRENCYCODE' => '32',
            			'AMOUNT' => '1.00',
            			'FEEAMOUNT' => [
            				'@attributes' => [
            					'nil' => 'true'
            				]
            			],
            			'TAXAMOUNT' => [
            				'@attributes' => [
            					'nil' => 'true'
            				]
            			],
            			'SERVICECHARGEAMOUNT' => [
            				'@attributes' => [
            					'nil' => 'true'
            				]
            			],
            			'CREDITEDAMOUNT' => '1.00',
            			'AMOUNTBUYER' => '1.00',
            			'FEEAMOUNTBUYER' => [
            				'@attributes' => [
            					'nil' => 'true'
            				]
            			],
            			'TAXAMOUNTBUYER' => [
            				'@attributes' => [
            					'nil' => 'true'
            				]
            			],
            			'BANKID' => '7',
            			'PROMOTIONID' => '2706',
            			'TYPE' => 'compra_online',
            			'INSTALLMENTPAYMENTS' => '1',
            			'CUSTOMEREMAIL' => 'juan@yopmail.com',
            			'IDENTIFICATIONTYPE' => 'DNI',
            			'IDENTIFICATION' => '1231231',
            			'CARDNUMBER' => '4242XXXXXXXX4242',
            			'CARDHOLDERNAME' => 'Coco Loco',
            			'TICKETNUMBER' => '0',
            			'AUTHORIZATIONCODE' => [
            				'@attributes' => [
            					'nil' => 'true'
            				]
            			],
            			'BARCODE' => [
            				'@attributes' => [
            					'nil' => 'true'
            				]
            			],
            			'COUPONEXPDATE' => [
            				'@attributes' => [
            					'nil' => 'true'
            				]
            			],
            			'COUPONSECEXPDATE' => [
            				'@attributes' => [
            					'nil' => 'true'
            				]
            			],
            			'COUPONSUBSCRIBER' => [
            				'@attributes' => [
            					'nil' => 'true'
            				]
            			],
            			'PAYMENTMETHODCODE' => '42',
            			'PAYMENTMETHODNAME' => 'VISA',
            			'PAYMENTMETHODTYPE' => 'Crédito',
            			'OPERATIONNUMBER' => '50545296',
            			'AUTHORIZATIONKEY' => 'ce9ad15a-6b33-cd72-5000-0b3446f9173c',
            			'REFUNDED' => [
            				'@attributes' => [
            					'nil' => 'true'
            				]
            			],
            			'PUSHNOTIFYMETHOD' => [
            				'@attributes' => [
            					'nil' => 'true'
            				]
            			],
            			'PUSHNOTIFYENDPOINT' => [
            				'@attributes' => [
            					'nil' => 'true'
            				]
            			],
            			'PUSHNOTIFYSTATES' => [
            				'@attributes' => [
            					'nil' => 'true'
            				]
            			],
            			'IDCONTRACARGO' => '0',
            			'FECHANOTIFICACIONCUENTA' => [
            				'@attributes' => [
            					'nil' => 'true'
            				]
            			],
            			'ESTADOCONTRACARGO' => [
            				'@attributes' => [
            					'nil' => 'true'
            				]
            			],
            			'COMISION' => [
            				'@attributes' => [
            					'nil' => 'true'
            				]
            			],
            			'CFT' => '0.00',
            			'TEA' => '0.00',
            			'REFUNDS' => [],
            			'PHONENUMBER' => 'null',
            			'ADDRESS' => 'null',
            			'POSTALCODE' => 'null',
            			'CUSTOMERID' => 'null',
            			'ITEMS' => []
            		],
            		'Status' => '00000',
                ];*/

                $register = 0;

                foreach ($operations as $operation) {

                    $trasaccion = $this->TpTransactions
                        ->find()
                        ->select(['OPERATIONID', 'STATUS'])
                        ->where(['OPERATIONID' => $operation['OPERATIONID']])->first();

                    //si la  transaccion ya esta registra pasamo a la siguiente

                    if ($trasaccion) {
                        $message .= 'Page ' . $page_number . ' - OPERATIONID: ' . $operation['OPERATIONID'] . ', ya existe.';
                        continue;
                    }

                    //sino existe la registro

                    $tp_transaction = $this->TpTransactions->newEntity();
                    $tp_transaction->RESULTMESSAGE       = $operation['RESULTMESSAGE'];
                    $tp_transaction->OPERATIONID         = $operation['OPERATIONID'];
                    $tp_transaction->DATETIME            = new Time($operation['DATETIME'] . ', America/Argentina/Buenos_Aires');
                    $tp_transaction->CURRENCYCODE        = intval($operation['CURRENCYCODE']);
                    $tp_transaction->AMOUNT              = $this->strToDeciaml($operation['AMOUNT']);
                    $tp_transaction->CREDITEDAMOUNT      = $this->strToDeciaml($operation['CREDITEDAMOUNT']);
                    $tp_transaction->AMOUNTBUYER         = $this->strToDeciaml($operation['AMOUNTBUYER']);
                    $tp_transaction->BANKID              = $operation['BANKID'];
                    $tp_transaction->PROMOTIONID         = $operation['PROMOTIONID'];
                    $tp_transaction->TYPE                = $operation['TYPE'];
                    $tp_transaction->INSTALLMENTPAYMENTS = $operation['INSTALLMENTPAYMENTS'];
                    $tp_transaction->CUSTOMEREMAIL       = $operation['CUSTOMEREMAIL'];
                    $tp_transaction->IDENTIFICATIONTYPE  = $operation['IDENTIFICATIONTYPE'];
                    $tp_transaction->IDENTIFICATION      = $operation['IDENTIFICATION'];
                    $tp_transaction->CARDNUMBER          = $operation['CARDNUMBER'];
                    $tp_transaction->CARDHOLDERNAME      = $operation['CARDHOLDERNAME'];
                    $tp_transaction->TICKETNUMBER        = $operation['TICKETNUMBER'];
                    $tp_transaction->PAYMENTMETHODCODE   = $operation['PAYMENTMETHODCODE'];
                    $tp_transaction->PAYMENTMETHODNAME   = $operation['PAYMENTMETHODNAME'];
                    $tp_transaction->PAYMENTMETHODTYPE   = $operation['PAYMENTMETHODTYPE'];
                    $tp_transaction->OPERATIONNUMBER     = $operation['OPERATIONNUMBER'];
                    $tp_transaction->AUTHORIZATIONKEY    = $operation['AUTHORIZATIONKEY'];
                    $tp_transaction->CFT                 = $operation['CFT'];
                    $tp_transaction->TEA                 = $operation['TEA'];
                    $tp_transaction->STATUS              = 0;

                    //OPERATIONID = time() . '-' . customer->code; //time() valor entero de longitud 10 digitos.
                    $operation_id_array = explode("-", $tp_transaction->OPERATIONID);
                    $code = $operation_id_array[1];
                    //OTRA OPCION PARA RECUPERAR EL CODE DEL CUSTOMER
                    //$code = substr($operation['OPERATIONID'], 10);

                    $customer = $this->Customers
                        ->find()
                        ->where([
                            'code'     => $code,
                            'deleted'  => false])
                        ->first();

                    //si no existe un cliente al cual asociar el pago de esta transaccion no se pude procesar

                    if (empty($customer)) {

                        $tp_transaction->COMMENT = 'No existe cliente o cliente eliminado.';
                        $this->TpTransactions->save($tp_transaction);
                        continue;
                    }

                    //proceso la transaccion ....

                    //TRAER FACTURAS NO PAGADAS
                    $this->loadModel('Invoices');
                    $invoice = $this->Invoices->find()
                        ->contain(['Users', 'Customers'])
                        ->where([
                            'id' => $operation['OPERATIONID']
                        ])->first();
                    //TRAER FACTURAS NO PAGADAS

                    $seating = $this->Accountant->addSeating([
                        'concept'       => 'Por Cobranza (todopago)',
                        'comments'      => '',
                        'seating'       => null, 
                        'account_debe'  => $payment_getway->config->todopago->account,
                        'account_haber' => $customer->account_code,
                        'value'         => $tp_transaction->AMOUNTBUYER,
                        'user_id'       => $user_sistema,
                        'created'       => Time::now()
                    ]);

                    $data_payment = [
                        'import'                => $tp_transaction->AMOUNTBUYER,
                        'created'               => $tp_transaction->DATETIME,
                        'cash_entity_id'        => null,
                        'user_id'               => $user_sistema,
                        'seating_number'        => $seating->number,
                        'customer_code'         => $customer->code,
                        'payment_method_id'     => 100, //id = 99 metodo de pago -> todopago
                        'concept'               => "Pago online (todopago)",
                        'invoice_selected'      => !empty($invoice) ? $invoice->id : null,
                        'tp_id_transaccion'     => $tp_transaction->OPERATIONID
                    ];

                    $payment = $this->Payments->newEntity();

                    $payment = $this->Payments->patchEntity($payment, $data_payment);

                    if ($payment) {

                        if ($this->Payments->save($payment)) {

                            $this->Accountant->updateDebtMonth($customer->code);

                            $receipt = $this->Comprobantes->generate($payment, ComprobantesComponent::TYPE_RECEIPT);

                            if ($receipt) {

                                if ($this->PDFGenerator->generate($receipt, PDFGeneratorComponent::TYPE_RECEIPT)) {

                                    $this->enableConnection($customer);
                                    $register++;

                                    $tp_transaction->COMMENT = 'procesado correctamente';
                                    $tp_transaction->STATUS = 1;
                                    $this->TpTransactions->save($tp_transaction);

                                } else {
                                    //rollback Receipts
                                    $this->Comprobantes->deleteReceipt($payment);
                                    //rollback payment
                                    $this->Payments->delete($payment);
                                    //rollback seating
                                    $this->Accountant->deleteSeating($seating);
                                    Log::error('Error al generar el PDF del Recibo', ['scope' => ['TodoPagoController']]);
                                }

                            } else {

                                Log::error($receipt->getErrors(), ['scope' => ['TodoPagoController']]);

                                //rollback payment
                                $this->Payments->delete($payment);
                                //rollback seating
                                $this->Accountant->deleteSeating($seating);

                            }
                        } else {
                            Log::error($pament->getErrors(), ['scope' => ['TodoPagoController']]);
                        }
                    } else {

                        Log::error($pament->getErrors(), ['scope' => ['TodoPagoController']]);
                    }

                }

                $message .= 'Pagos registrados: ' . $register;

            } else {
                $message .= "Page " . $page_number . ": no hay datos";
                Log::info($message, ['scope' => ['TodoPagoController']]);
                break;
            }
        }

        $data['message'] = $message;

        $this->set([
            'data'       => $data,
            '_serialize' => ['data']
        ]);
    }

    private function strToDeciaml($value)
    {
       $value = str_replace('.', '', $value);
       $value = str_replace(',', '.', $value);
       return floatval($value);
    }
}