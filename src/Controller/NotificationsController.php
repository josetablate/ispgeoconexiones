<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

/**
 * ErrorLog Controller
 *
 * @property \App\Model\Table\ErrorLogTable $ErrorLog
 *
 * @method \App\Model\Entity\ErrorLog[] paginate($object = null, array $settings = [])
 */
class NotificationsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }

    public function isAuthorized($user = null) 
    {
        if ($this->request->getParam('action') == 'verify') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function index()
    {
        $paraments = $this->request->getSession()->read('paraments');

        $notifications = [];
        $current_day = Time::now()->day;

        foreach ($paraments->notificators as $key => $notificator) {

            foreach ($notificator->notifications as $k => $notification) {
                $val = new \stdClass;
                $val->id = $notification->id;
                $val->notificator_id = $notificator->id;
                $val->name = $notification->name;
                $val->start = $notification->start;
                $val->dueday = $notification->dueday;
                $val->message = $notification->message;
                $val->checked = $notification->checked;
                $val->enabled = $notification->enabled;
                if ($current_day >= $notification->start) {
                    array_push($notifications, $val);
                }
            }
        }

        $this->set(compact('notifications'));
        $this->set('_serialize', ['notifications']);
    }

    public function config()
    {
        $paraments = $this->request->getSession()->read('paraments');

        if ($this->request->is(['patch', 'post', 'put'])) {

            foreach ($paraments->notificators as $key => $notificator) {
                foreach ($notificator->notifications as $k => $notication) {
                    $notificator_id = $notificator->id;
                    $notification_id = $notication->id;
                    $notication->enabled = $this->request->getData("notifications.$notificator_id.$notification_id.enabled") ? TRUE : FALSE;
                    $notication->start = $this->request->getData("notifications.$notificator_id.$notification_id.start");
                    $notication->dueday = $this->request->getData("notifications.$notificator_id.$notification_id.dueday");
                    $notication->message = $this->request->getData("notifications.$notificator_id.$notification_id.message");
                }
            }

            //se guarda los cambios
            $this->saveGeneralParaments($paraments);
        }

        $this->set(compact('parament'));
        $this->set('_serialize', ['parament']);
    }

    public function add()
    {
        if ($this->request->is('post')) {

            $paraments = $this->request->getSession()->read('paraments');

            foreach ($paraments->notificators as $key => $notificator) {
                if ($notificator->name == $this->request->getData('name')) {
                    $this->Flash->warning(__('Ya existe un Notificador con el nombre: ' . $this->request->getData('name')));
                    return $this->redirect(['controller' => 'Notifications', 'action' => 'config']);
                }
            }

            $notificator = new \stdClass;
            $notificator->id            = Time::now()->toUnixString();
            $notificator->name          = $this->request->getData('name');
            $notificator->notifications = [];

            $paraments->notificators[] = $notificator;

            //se guarda los cambios
            $this->saveGeneralParaments($paraments);
            return $this->redirect(['controller' => 'Notifications', 'action' => 'config']);
        }
    }

    public function edit()
    {
        if ($this->request->is('post')) {

            $paraments = $this->request->getSession()->read('paraments');

            foreach ($paraments->notificators as $key => $notificator) {
                if ($notificator->id != $this->request->getData('id') && $notificator->name == $this->request->getData('name')) {
                    $this->Flash->warning(__('Ya existe un Notificador con el nombre: ' . $this->request->getData('name')));
                    return $this->redirect(['controller' => 'Notifications', 'action' => 'config']);
                }
            }

            foreach ($paraments->notificators as $key => $notificator) {
                $notificator->name = $this->request->getData('name');
            }

            //se guarda los cambios
            $this->saveGeneralParaments($paraments);
            return $this->redirect(['controller' => 'Notifications', 'action' => 'config']);
        }
    }

    public function addNotification()
    {
        if ($this->request->is('post')) {

            $paraments = $this->request->getSession()->read('paraments');

            $notificator_id = $this->request->getData('notificator_id');

            foreach ($paraments->notificators as $notificator) {
                foreach ($notificator->notifications as $key => $notification) {
                    if ($notificator_id == $notificator->id && $notification->name == $this->request->getData('name')) {
                        $this->Flash->warning(__('Ya existe una notificación con el nombre: ' . $this->request->getData('name')));
                        return $this->redirect(['controller' => 'Notifications', 'action' => 'config']);
                    }
                }
            }
            $notificator = $this->request->getData('notificator');

            $notification = new \stdClass;
            $notification->id = Time::now()->toUnixString();
            $notification->name = $this->request->getData('name');
            $notification->start = 1;
            $notification->dueday = 10;
            $notification->message = "";
            $notification->enabled = false;
            $notification->checked = false;

            foreach ($paraments->notificators as $notificator) {
                if ($notificator->id == $notificator_id) {
                    $notificator->notifications[] = $notification;
                    break;
                }
            }

            //se guarda los cambios
            $this->saveGeneralParaments($paraments);
            return $this->redirect(['Notifications' => 'Settings', 'action' => 'config']);
        }
    }

    public function editNotification()
    {
        if ($this->request->is('post')) {

            $paraments = $this->request->getSession()->read('paraments');

            $notificator_id = $this->request->getData('notificator_id');
            $id = $this->request->getData('id');

            foreach ($paraments->notificators as $notificator) {
                foreach ($notificator->notifications as $key => $notification) {
                    if ($notification->id != $id && $notificator_id == $notificator->id && $notification->name == $this->request->getData('name')) {
                        $this->Flash->warning(__('Ya existe una notificación con el nombre: ' . $this->request->getData('name')));
                        return $this->redirect(['controller' => 'Notifications', 'action' => 'config']);
                    }
                }
            }

            foreach ($paraments->notificators as $notificator) {
                foreach ($notificator->notifications as $key => $notification) {
                    if ($notification->id == $id && $notificator_id == $notificator->id) {
                        $notification->name = $this->request->getData('name');
                    }
                }
            }

            //se guarda los cambios
            $this->saveGeneralParaments($paraments);
            return $this->redirect(['Notifications' => 'Settings', 'action' => 'config']);
        }
    }

    public function verify()
    {
        //Ajax Detection
        if ($this->request->is('Ajax')) {

            $current_day = Time::now()->day;

            $paraments = $this->request->getSession()->read('paraments');

            $notifications = [];

            if (isset($paraments->notificators) && count($paraments->notificators) > 0) {
                foreach ($paraments->notificators as $key => $notificator) {
                    foreach ($notificator->notifications as $k => $notification) {
                        if ($notification->enabled && ($current_day >= $notification->start)) {
                            if (!$notification->checked) {
                                $value = new \stdClass;
                                $value->id             = $notification->id;
                                $value->notificator_id = $notificator->id;
                                $value->message        = $notification->message;
                                $value->name           = $notification->name;
                                $value->start          = $notification->start;
                                $value->dueday         = $notification->dueday;
                                $value->name           = $notification->name;
                                $value->controller     = $notificator->name;

                                array_push($notifications, $value);
                            }
                        } else {
                            $notification->checked = false;
                            //se guarda los cambios
                            $this->saveGeneralParaments($paraments);
                        }
                    }
                }
            }

            $this->set('notifications', $notifications);
        }
    }

    public function check()
    {
        if ($this->request->is('post')) {

            $paraments = $this->request->getSession()->read('paraments');

            $data = json_decode($this->request->getData('data'));

            foreach ($data as $d) {
                foreach ($paraments->notificators as $notificator) {
                    if ($notificator->id != $d->notificator_id) {
                        continue;
                    }
                    foreach ($notificator->notifications as $notification) {
                        if ($notification->id == $d->id) {
                            $notification->checked = true;
                        }
                    }
                }
            }

            $this->saveGeneralParaments($paraments);
        }
        $this->redirect($this->referer());
    }

    public function unCheck()
    {
        if ($this->request->is('post')) {

            $paraments = $this->request->getSession()->read('paraments');

            $data = json_decode($this->request->getData('data'));

            foreach ($data as $d) {
                foreach ($paraments->notificators as $notificator) {
                    if ($notificator->id != $d->notificator_id) {
                        continue;
                    }
                    foreach ($notificator->notifications as $notification) {
                        if ($notification->id == $d->id) {
                            $notification->checked = false;
                        }
                    }
                }
            }

            $this->saveGeneralParaments($paraments);
        }
        $this->redirect($this->referer());
    }

    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);
        $notificator_id = $this->request->getData('notificator_id');

        $paraments = $this->request->getSession()->read('paraments');

        foreach ($paraments->notificators as $key => $notificator) {
            if ($notificator->id == $notificator_id) {
                unset($paraments->notificators[$key]);
            }
        }

        //se guarda los cambios
        $this->saveGeneralParaments($paraments);
        return $this->redirect(['controller' => 'Notifications', 'action' => 'config']);
    }

    public function deleteNotification()
    {
        $this->request->allowMethod(['post', 'delete']);

        $id = $this->request->getData('id');
        $notificator_id = $this->request->getData('notificator_id');

        $paraments = $this->request->getSession()->read('paraments');

        foreach ($paraments->notificators as $key => $notificator) {
            foreach ($notificator->notifications as $k => $notification) {
                if ($notificator->id == $notificator_id && $notification->id == $id) {
                    unset($paraments->notificators[$key]->notifications[$k]);
                }
            }
        }

        //se guarda los cambios
        $this->saveGeneralParaments($paraments);
        return $this->redirect($this->referer());
    }
}
