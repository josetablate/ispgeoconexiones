<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use App\Controller\Component\ComprobantesComponent;

/**
 * Seating Controller
 *
 * @property \App\Model\Table\SeatingTable $Seating
 */
class SeatingController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Accountant', [
             'className' => '\App\Controller\Component\Admin\Accountant'
        ]);

        $this->loadComponent('Comprobantes');
    }

    public function isAuthorized($user = null)
    {
        if ($this->request->getParam('action') == 'get_interest') {
            return true;
        }

        if ($this->request->getParam('action') == 'geCustomerAjax') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function geCustomerAjax()
    {
        if ($this->request->is('Ajax')) { //Ajax Detection

            $code = $this->request->input('json_decode')->code ? $this->request->input('json_decode')->code : null;
            $account = $this->request->input('json_decode')->account ? $this->request->input('json_decode')->account : null;
            $doc_type = $this->request->input('json_decode')->doc_type ? $this->request->input('json_decode')->doc_type : null;
            $ident = $this->request->input('json_decode')->ident ? $this->request->input('json_decode')->ident : null;

            if ($account) {
                $paraments = $this->request->getSession()->read('paraments');
                $account = substr($paraments->accountant->acounts_parent->customers, 0, 4) . sprintf("%'.05d", $account);
            }

            $wheres = ['deleted' => false];

            if ($code) {
                $wheres['code'] = $code;
            }

            if ($account) {
                $Seating['account_code'] = $account;
            }

            if ($ident) {
               $wheres['doc_type'] = $doc_type;
               $wheres['ident'] = $ident;
            }

            $this->loadModel('Customers');
            $customer = $this->Customers->find()->where($wheres)->first();

            $now = Time::now();
            $now->modify('+1 month')->day(1)->hour(0)->minute(0)->second(0);

            $customer->debt_month = 0;

            $this->loadModel('Debts');
            $debts =  $this->Debts->find()->where(['customer_code' => $customer->code, 'invoice_id IS' => NULL, 'duedate <' => $now->format('Y-m-d H:i:s')]);
            $debts = $debts->select(['total' => $debts->func()->sum('total')])->first();

            $this->loadModel('DebitNotes');
            $debitNotes =  $this->DebitNotes->find()->where(['customer_code' => $customer->code, 'duedate <' => $now->format('Y-m-d H:i:s')]);
            $debitNotes = $debitNotes->select(['total' => $debitNotes->func()->sum('total')])->first();

            $this->loadModel('Invoices');
            $invoices =  $this->Invoices->find()->where(['customer_code' => $customer->code, 'tipo_comp IN' => ['XXX', '001', '006', '011'] , 'duedate <' => $now->format('Y-m-d H:i:s')]);
            $invoices = $invoices->select(['total' => $invoices->func()->sum('total')])->first();

            $this->loadModel('Payments');
            $payments =  $this->Payments->find()->where(['customer_code' => $customer->code]);
            $payments = $payments->select(['total' => $payments->func()->sum('import')])->first();

            $this->loadModel('CreditNotes');
            $creditNotes =  $this->CreditNotes->find()->where(['customer_code' => $customer->code]);
            $creditNotes = $creditNotes->select(['total' => $creditNotes->func()->sum('total')])->first();

            $customer->debt_month += $debts->total;
            $customer->debt_month += $invoices->total;
            $customer->debt_month += $debitNotes->total;
            $customer->debt_month -= $payments->total;
            $customer->debt_month -= $creditNotes->total;

            if ($customer) {
                $this->set('customer', $customer);
            } else {
                $this->set('customer', false);
            }
        }
    } 

    public function edit()
    {
        if ($this->request->is('post')) {

            $seating = $this->Seating->get($_POST['id']);
            $seating->comments = $this->request->getData('comments');

            if (!$this->Seating->save($seating)) {
                $this->Flash->error(__('No se pudo actualizar el comentario'));
                return $this->redirect(['controller' => 'DiaryBook', 'action' => 'index']);
            }

            $this->Flash->success(__('El comentario se actualizo correctamente'));
            return $this->redirect(['controller' => 'DiaryBook', 'action' => 'index']);
        }
    }
}
