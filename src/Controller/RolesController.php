<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Roles Controller
 *
 * @property \App\Model\Table\RolesTable $Roles
 */
class RolesController extends AppController
{
    public function isAuthorized($user = null)
    {
        if ($this->request->getParam('action') == 'getAllActionsSystem') {
            return true;
        }

        return parent::allowRol($user['id']);     
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $parament = $this->request->getSession()->read('paraments');

        $roles = $this->Roles->find()->contain(['ActionsSystem.Clases'])
        ->where(['deleted' => false, 'id NOT IN' => $parament->system->users]);

        $this->set(compact('roles'));
        $this->set('_serialize', ['roles']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $role = $this->Roles->newEntity();
        if ($this->request->is('post')) {

            $action = 'Agregado de Privilegio';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            if ($this->Roles->find()->where(['name' => $this->request->getData('name'), 'deleted' => false])->first()) {
                $this->Flash->error(__('Existe un Privilegios con mismo nombre.'));
                return $this->redirect(['action' => 'add']);
            }

            $request_data = $this->request->getData();

            $request_data['actions_system'] = [];
            $request_data['actions_system']['_ids'] = explode(',', $this->request->getData('actions_system._ids') );

            $role = $this->Roles->patchEntity($role, $request_data);

            if ($this->Roles->save($role)) {

                $action = 'Privilegio de creado';
                $detail = 'Privilegio: ' . $role->name;

                $this->registerActivity($action, $detail);

                $this->Flash->success(__('Privilegio agregado.'));
                return $this->redirect(['action' => 'add']);
            } else {
                $this->Flash->error(__('Error al agregar el privilegio.'));
            }
        }

        $this->set(compact('role'));
        $this->set('_serialize', ['role']);
    }

    public function getAllactionsSystem()
    {
        $actionsSystem = $this->Roles->ActionsSystem->find()->contain('Clases'); 

        $this->set(compact('actionsSystem'));
        $this->set('_serialize', ['actionsSystem']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Role id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $role = $this->Roles->get($id, [
            'contain' => ['ActionsSystem.Clases', 'Users']
        ]);

        $action = 'Edición de Privilegio';
        $detail = '';
        $this->registerActivity($action, $detail, NULL, TRUE);

        $parament = $this->request->getSession()->read('paraments');

        if (in_array($role->id, $parament->system->users)) {
            
            $this->Flash->warning(__('Este privilegio no puede editarse.'));
            return $this->redirect(['action' => 'index']);
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            if ($this->Roles->find()->where(['name' => $this->request->getData('name'), 'deleted' => false, 'id != ' => $id])->first()) {
                $this->Flash->error(__('Existe un Privilegios con mismo nombre.'));
                return $this->redirect(['action' => 'edit', $id ]);
            }

            $request_data = $this->request->getData();

            $request_data['actions_system'] = [];
            $request_data['actions_system']['_ids'] = explode(',',$this->request->getData('actions_system._ids') );

            $role = $this->Roles->patchEntity($role, $request_data);

            if ($this->Roles->save($role)) {

                $action = 'Privilegio Editado';
                $detail = 'Privilegio: ' . $role->name;
                $this->registerActivity($action, $detail);

                $this->Flash->success(__('Datos actualizados.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Error al actualizar los datos.'));
            }
        }

        $this->set(compact('role'));
        $this->set('_serialize', ['role']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Role id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);

        $action = 'Eliminación de Privilegio';
        $detail = '';
        $this->registerActivity($action, $detail, NULL, TRUE);

        $role = $this->Roles->get($_POST['id']);

        // $this->loadModel('Users');

        // $this->Users->find()->contain(['Users'])->where(['RolesUsers.role_id' => $role, 'Users.deleted' => false]);

        $role->deleted = true;

        if ($this->Roles->save($role)) {

            $action = 'Privilegio Eliminado';
            $detail = 'Privilegio: ' . $role->name;
            $this->registerActivity($action, $detail);

            $this->Flash->success(__('Privilegio eliminado.'));
        } else {
            $this->Flash->error(__('Error al eliminar el privilegio'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
