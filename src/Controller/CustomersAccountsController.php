<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;

/**
 * CustomersAccounts Controller
 *
 *
 * @method \App\Model\Entity\CustomersAccount[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CustomersAccountsController extends AppController
{
    public function isAuthorized($user = null) 
    {
        if ($this->request->getParam('action') == 'validateExistCBU') {
            return true;
        }

        if ($this->request->getParam('action') == 'getAccounts') {
            return true;
        }

        if ($this->request->getParam('action') == 'existCBU') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $customersAccounts = $this->paginate($this->CustomersAccounts);

        $this->set(compact('customersAccounts'));
    }

    /**
     * View method
     *
     * @param string|null $id Customers Account id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $customersAccount = $this->CustomersAccounts->get($id, [
            'contain' => []
        ]);

        $this->set('customersAccount', $customersAccount);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $customersAccount = $this->CustomersAccounts->newEntity();
        if ($this->request->is('post')) {
            $customersAccount = $this->CustomersAccounts->patchEntity($customersAccount, $this->request->getData());
            if ($this->CustomersAccounts->save($customersAccount)) {
                $this->Flash->success(__('The customers account has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The customers account could not be saved. Please, try again.'));
        }
        $this->set(compact('customersAccount'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Customers Account id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $data = $this->request->getData();

        $payment_getway = $this->request->getSession()->read('payment_getway');
        $cbu = $data['cbu'];
        $total = $this->existCBU($cbu, $data['account_id']);

        if ($total > 0) {
            $this->Flash->warning(__("El CBU: $cbu ya existe."));
        } else {
            foreach ($payment_getway->methods as $pg) {
                $config = $pg->config;
                if ($pg->id == $data['payment_getway_id']) {
                    $model = $payment_getway->config->$config->model;
                    continue;
                }
            }

            $this->loadModel($model);

            if ($this->$model->find()->where(['cuit' => $data['cuit'], 'customer_code !=' => $data['customer_code'], 'deleted' => FALSE])->first()) {
                $this->Flash->warning(__('Existe otra cuenta de Débito Automático con el mismo CUIT.'));
                return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
            }

            $account = $this->$model
                ->find()
                ->where([
                    'customer_code'     => $data['customer_code'],
                    'id'                => $data['account_id'],
                    'payment_getway_id' => $data['payment_getway_id']
                ])->first();

            if ($account) {
                foreach ($data as $k => $d) {
                    $account->$k = $d;
                }
                $this->$model->save($account);
                $this->Flash->success(__('Se ha editado correctamente la cuenta.'));
            } else {
                $this->Flash->warning(__('No se encontro la cuenta.'));
            }
        }

        return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
    }

    public function delete()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Eliminación de Cuenta de Plataforma de Pago';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $data = $this->request->getData();
            $payment_getway = $this->request->getSession()->read('payment_getway');
            $paraments = $this->request->getSession()->read('paraments');
            $account = NULL;

            foreach ($payment_getway->methods as $key => $pg) {

                if ($pg->id == $data['payment_getway_id']) {

                    $config = $pg->config;
                    $model = $payment_getway->config->$config->model;
                    $this->loadModel($model);
                    $account = $this->$model
                        ->find()
                        ->where([
                            'customer_code'     => $data['customer_code'],
                            'id'                => $data['account_id'],
                            'payment_getway_id' => $data['payment_getway_id']
                        ])->first();

                    if ($account) {

                        if ($this->$model->delete($account)) {

                            $detail = "";

                            switch ($data['payment_getway_id']) {

                                case 7:
                                    $barcode = $account->barcode;
                                    $detail .= 'Cód. Barra: ' . $barcode . PHP_EOL;
                                    $final_action = "de Tarjeta de Cobranza - PayU";
                                    break;

                                case 99:
                                    $barcode = $account->barcode;
                                    $electronic_code = $account->electronic_code;
                                    $id_comercio = $account->id_comercio;
                                    $detail .= 'Cód. Barra: ' . $barcode . PHP_EOL;
                                    $detail .= 'Cód. Electrónico: ' . $electronic_code . PHP_EOL;
                                    $detail .= 'ID Comercio: ' . $id_comercio . PHP_EOL;
                                    $final_action = "de Tarjeta de Cobranza - Cobro Digital";
                                    break;

                                case 104:
                                    $id_comercio = $account->id_comercio;
                                    $firstname = $account->firstname;
                                    $lastname = $account->lastname;
                                    $cuit = $account->cuit;
                                    $cbu = $account->cbu;
                                    $detail .= 'Nombre: ' . $firstname . PHP_EOL;
                                    $detail .= 'Apellido: ' . $lastname . PHP_EOL;
                                    $detail .= 'CUIT: ' . $cuit . PHP_EOL;
                                    $detail .= 'CBU: ' . $cbu . PHP_EOL;
                                    $detail .= 'ID Comercio: ' . $id_comercio . PHP_EOL;
                                    $final_action = "de Débito Automático Banco - Cobro Digital";
                                    break;

                                case 105:
                                    $barcode = $account->barcode;
                                    $detail .= 'Cód. Barra: ' . $barcode . PHP_EOL;
                                    $final_action = "de Código de Barra Abierto - Cuenta Digital";
                                    break;

                                case 106:
                                    $card_number = $account->card_number;
                                    $detail .= 'Nro: ' . $card_number . PHP_EOL;
                                    $final_action = "de Débito Automático - MasterCard";
                                    break;

                                case 106:
                                    $card_number = $account->card_number;
                                    $detail .= 'Nro: ' . $card_number . PHP_EOL;
                                    $final_action = "de Débito Automático - Visa";
                                    break;
                            }

                            $action = 'Cuenta eliminada ' . $final_action;
                            $this->registerActivity($action, $detail, $data['customer_code']);

                            $this->loadModel('CustomersAccounts');
                            $customer_account = $this->CustomersAccounts
                                ->find()
                                ->where([
                                    'customer_code'     => $data['customer_code'],
                                    'account_id'        => $data['account_id'],
                                    'payment_getway_id' => $data['payment_getway_id']
                                ])
                                ->first();

                            if ($this->CustomersAccounts->delete($customer_account)) {

                                if ($paraments->gral_config->billing_for_service) {

                                    $this->loadModel('Customers');
                                    $customer = $this->Customers->get($data['customer_code'], [
                                        'contain' => ['Connections']
                                    ]);

                                    if (sizeof($customer->connections) > 0) {

                                        $this->loadModel('Connections');
                                        foreach ($payment_getway->methods as $key => $pg) {
                                            if ($pg->id == $data['payment_getway_id']) {
                                                $attribute = $key;
                                            }
                                        }

                                        foreach ($customer->connections as $connection) {
                                            $connection->$attribute = FALSE;
                                            $this->Connections->save($connection);
                                        }
                                    }
                                }
                                $this->Flash->success(__('Se ha eliminado correctamente'));
                            } else {
                                $this->Flash->warning(__('Error, al eliminado. Por favor intente nuevamente.'));
                            }
                        }
                    } else {
                        $this->Flash->warning(__('Error, al eliminado. no se encontró la cuenta que desea eliminar.'));
                    }
                    break;
                }
            }

            return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
        }
    }

    public function validateExistCBU()
    {
        if ($this->request->is(['ajax'])) {

            $data = $this->request->input('json_decode');
            $cbu = $data->cbu;

            $total = $this->existCBU($cbu);

            $datax = [
                'error'    => FALSE,
                'msgError' => ''
            ];

            if ($total > 0) {
                $datax['error']    = TRUE;
                $datax['msgError'] = "El CBU: $cbu ya existe."; 
            }

            $this->set('data', $datax); 
        }
    }

    private function existCBU($cbu, $id = NULL)
    {
        $this->loadModel('AutoDebitsAccounts');
        $this->loadModel('CobrodigitalAccounts');

        $where1 = [
            'cbu'               => $cbu,
            'payment_getway_id' => 102,
            'deleted'           => FALSE
        ];

        $where2 = [
            'cbu'               => $cbu,
            'payment_getway_id' => 104,
            'deleted'           => FALSE
        ];

        if (!empty($id)) {
            $where1['id !='] = $id;
            $where2['id !='] = $id;
        }

        $result_1 = $this->AutoDebitsAccounts
            ->find()
            ->where($where1)->count();

        $result_2 = $this->CobrodigitalAccounts
            ->find()
            ->where($where2)->count();

        return $result_1 + $result_2;
    }

    public function darBaja()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {

            $data = $this->request->getData();
            $payment_getway = $this->request->getSession()->read('payment_getway');
            $paraments = $this->request->getSession()->read('paraments');
            $account = NULL;

            foreach ($payment_getway->methods as $key => $pg) {

                if ($pg->id == $data['payment_getway_id']) {

                    $config = $pg->config;
                    $model = $payment_getway->config->$config->model;
                    $this->loadModel($model);
                    $account = $this->$model
                        ->find()
                        ->where([
                            'customer_code'     => $data['customer_code'],
                            'id'                => $data['account_id'],
                            'payment_getway_id' => $data['payment_getway_id']
                        ])->first();
                    $account->deleted = TRUE;
                    if ($this->$model->save($account)) {
                        $detail = "";
                        switch ($data['payment_getway_id']) {
                            case 7:
                                $barcode = $account->barcode;
                                $detail .= 'Cód. Barra: ' . $barcode . PHP_EOL;
                                $final_action = "de Tarjeta de Cobranza - PayU";
                                break;
                            case 99:
                                $barcode = $account->barcode;
                                $electronic_code = $account->electronic_code;
                                $id_comercio = $account->id_comercio;
                                $detail .= 'Cód. Barra: ' . $barcode . PHP_EOL;
                                $detail .= 'Cód. Electrónico: ' . $electronic_code . PHP_EOL;
                                $detail .= 'ID Comercio: ' . $id_comercio . PHP_EOL;
                                $final_action = "de Tarjeta de Cobranza - Cobro Digital";
                                break;
                            case 104:
                                $id_comercio = $account->id_comercio;
                                $firstname = $account->firstname;
                                $lastname = $account->lastname;
                                $cuit = $account->cuit;
                                $cbu = $account->cbu;
                                $detail .= 'Nombre: ' . $firstname . PHP_EOL;
                                $detail .= 'Apellido: ' . $lastname . PHP_EOL;
                                $detail .= 'CUIT: ' . $cuit . PHP_EOL;
                                $detail .= 'CBU: ' . $cbu . PHP_EOL;
                                $detail .= 'ID Comercio: ' . $id_comercio . PHP_EOL;
                                $final_action = "de Débito Automático Banco - Cobro Digital";
                                break;
                            case 105:
                                $barcode = $account->barcode;
                                $detail .= 'Cód. Barra: ' . $barcode . PHP_EOL;
                                $final_action = "de Código de Barra Abierto - Cuenta Digital";
                                break;
                            case 106:
                                $card_number = $account->card_number;
                                $detail .= 'Nro: ' . $card_number . PHP_EOL;
                                $final_action = "de Débito Automático - MasterCard";
                                break;
                            case 106:
                                $card_number = $account->card_number;
                                $detail .= 'Nro: ' . $card_number . PHP_EOL;
                                $final_action = "de Débito Automático - Visa";
                                break;
                        }
                        $action = 'Deshabilitación ' . $final_action;
                        $this->registerActivity($action, $detail, $data['customer_code']);
                    }
                    break;
                }
            }

            $this->loadModel('CustomersAccounts');

            $customer_account = $this->CustomersAccounts
                ->find()
                ->where([
                    'customer_code'     => $data['customer_code'],
                    'account_id'        => $data['account_id'],
                    'payment_getway_id' => $data['payment_getway_id']
                ])
                ->first();

            $customer_account->deleted = TRUE;

            if ($this->CustomersAccounts->save($customer_account)) {

                if ($paraments->gral_config->billing_for_service) {

                    $this->loadModel('Customers');
                    $customer = $this->Customers->get($data['customer_code'], [
                        'contain' => ['Connections']
                    ]);

                    if (sizeof($customer->connections) > 0) {

                        $this->loadModel('Connections');
                        foreach ($payment_getway->methods as $key => $pg) {
                            if ($pg->id == $data['payment_getway_id']) {
                                $attribute = $key;
                            }
                        }

                        foreach ($customer->connections as $connection) {
                            $connection->$attribute = FALSE;
                            $this->Connections->save($connection);
                        }
                    }
                }
                $this->Flash->success(__('Se ha deshabilitar correctamente'));
            } else {
                $this->Flash->warning(__('Error, al deshabilitar. Por favor intente nuevamente.'));
            }

            return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
        }
    }

    public function darAlta()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {

            $payment_getway = $this->request->getSession()->read('payment_getway');
            $paraments = $this->request->getSession()->read('paraments');

            $data = $this->request->getData();

            $this->loadModel('CustomersAccounts');

            $customers_accounts_diff_auto_debit = [];
            $customers_accounts_auto_debit = [];

            $auto_debits = [];
            foreach ($payment_getway->methods as $pg) {
                $config = $pg->config;
                if ($payment_getway->config->$config->enabled) {
                    if ($pg->auto_debit) {
                        array_push($auto_debits, $pg->id);
                    }
                }
            }

            if (in_array($data['payment_getway_id'], $auto_debits)) {
                $customers_accounts_auto_debit = $this->CustomersAccounts
                    ->find()
                    ->where([
                        'customer_code' => $data['customer_code'],
                        'deleted'       => FALSE,
                    ])
                    ->toArray();
            } else {

                $where = [
                    'customer_code' => $data['customer_code'],
                    'deleted'       => FALSE,
                    'OR' => [['payment_getway_id' => $data['payment_getway_id']]],
                ];

                foreach ($payment_getway->methods as $pg) {
                    $config = $pg->config;
                    if ($payment_getway->config->$config->enabled) {
                        if ($pg->auto_debit) {
                            $where['OR'][] = [
                                'payment_getway_id' => $pg->id
                            ];
                        }
                    }
                }

                $customers_accounts_diff_auto_debit = $this->CustomersAccounts
                    ->find()
                    ->where($where)
                    ->toArray();
            }

            if (sizeof($customers_accounts_auto_debit) > 0) {
                $this->Flash->warning(__('El Cliente tiene Cuenta/s, esto inhabilita la creación de la cuenta, para poder crear una cuenta de Débito Automático deberá "deshabilitar" dicha/s Cuenta/s.'));
                return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
            }

            if (sizeof($customers_accounts_diff_auto_debit) > 0) {
                $this->Flash->warning(__('Únicamente puede tener una cuenta por Cliente. Este Cliente posee una Cuenta del mismo tipo activa, en caso de querer "habilitar" está Cuenta deberá de "deshabilitar" la actual Cuenta activa del Cliente'));
                return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
            }

            $account = NULL;

            foreach ($payment_getway->methods as $key => $pg) {

                if ($pg->id == $data['payment_getway_id']) {

                    $config = $pg->config;
                    $model = $payment_getway->config->$config->model;
                    $this->loadModel($model);
                    $account = $this->$model
                        ->find()
                        ->where([
                            'customer_code'     => $data['customer_code'],
                            'id'                => $data['account_id'],
                            'payment_getway_id' => $data['payment_getway_id']
                        ])->first();
                    $account->deleted = FALSE;
                    $account->asigned = Time::now();
                    if ($this->$model->save($account)) {
                        $detail = "";
                        switch ($data['payment_getway_id']) {
                            case 7:
                                $barcode = $account->barcode;
                                $detail .= 'Cód. Barra: ' . $barcode . PHP_EOL;
                                $final_action = "de Tarjeta de Cobranza - PayU";
                                break;
                            case 99:
                                $barcode = $account->barcode;
                                $electronic_code = $account->electronic_code;
                                $id_comercio = $account->id_comercio;
                                $detail .= 'Cód. Barra: ' . $barcode . PHP_EOL;
                                $detail .= 'Cód. Electrónico: ' . $electronic_code . PHP_EOL;
                                $detail .= 'ID Comercio: ' . $id_comercio . PHP_EOL;
                                $final_action = "de Tarjeta de Cobranza - Cobro Digital";
                                break;
                            case 104:
                                $id_comercio = $account->id_comercio;
                                $firstname = $account->firstname;
                                $lastname = $account->lastname;
                                $cuit = $account->cuit;
                                $cbu = $account->cbu;
                                $detail .= 'Nombre: ' . $firstname . PHP_EOL;
                                $detail .= 'Apellido: ' . $lastname . PHP_EOL;
                                $detail .= 'CUIT: ' . $cuit . PHP_EOL;
                                $detail .= 'CBU: ' . $cbu . PHP_EOL;
                                $detail .= 'ID Comercio: ' . $id_comercio . PHP_EOL;
                                $final_action = "de Débito Automático Banco - Cobro Digital";
                                break;
                            case 105:
                                $barcode = $account->barcode;
                                $detail .= 'Cód. Barra: ' . $barcode . PHP_EOL;
                                $final_action = "de Código de Barra Abierto - Cuenta Digital";
                                break;
                            case 106:
                                $card_number = $account->card_number;
                                $detail .= 'Nro: ' . $card_number . PHP_EOL;
                                $final_action = "de Débito Automático - MasterCard";
                                break;
                            case 106:
                                $card_number = $account->card_number;
                                $detail .= 'Nro: ' . $card_number . PHP_EOL;
                                $final_action = "de Débito Automático - Visa";
                                break;
                        }
                        $action = 'Habilitación ' . $final_action;
                        $this->registerActivity($action, $detail, $data['customer_code']);
                    }
                    break;
                }
            }

            $customer_account = $this->CustomersAccounts
                ->find()
                ->where([
                    'customer_code'     => $data['customer_code'],
                    'account_id'        => $data['account_id'],
                    'payment_getway_id' => $data['payment_getway_id']
                ])
                ->first();

            $customer_account->deleted = FALSE;

            if ($this->CustomersAccounts->save($customer_account)) {

                if ($paraments->gral_config->billing_for_service) {

                    $this->loadModel('Customers');
                    $customer = $this->Customers->get($data['customer_code'], [
                        'contain' => ['Connections']
                    ]);

                    if (sizeof($customer->connections) > 0) {

                        $this->loadModel('Connections');
                        foreach ($payment_getway->methods as $key => $pg) {
                            if ($pg->id == $data['payment_getway_id']) {
                                $attribute = $key;
                            }
                        }

                        foreach ($customer->connections as $connection) {
                            $connection->$attribute = TRUE;
                            $this->Connections->save($connection);
                        }
                    }
                }
                $this->Flash->success(__('Se ha habilitado correctamente'));
            } else {
                $this->Flash->warning(__('Error, al habilitar. Por favor intente nuevamente.'));
            }
            return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
        }
    }

    public function freedCard($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $data = $this->request->getData();
        $payment_getway = $this->request->getSession()->read('payment_getway');
        $paraments = $this->request->getSession()->read('paraments');

        $customer_account = $this->CustomersAccounts
            ->find()
            ->where([
                'customer_code'     => $data['customer_code'],
                'account_id'        => $data['account_id'],
                'payment_getway_id' => $data['payment_getway_id'],
            ])->first();

        if ($this->CustomersAccounts->delete($customer_account)) {

            if ($data['payment_getway_id'] == 99) {

                $this->loadModel('CobrodigitalAccounts');

                $card = $this->CobrodigitalAccounts->get($data['account_id']);
                $customer_code = $card->customer_code;

                $barcode = $card->barcode;
                $electronic_code = $card->electronic_code;
                $id_comercio = $card->id_comercio;

                $card->used = FALSE;
                $card->deleted = FALSE;
                $card->customer_code = NULL;
                $card->date_exported = NULL;

                if ($this->CobrodigitalAccounts->save($card)) {

                    $detail = "";
                    $detail .= 'Cód. Barra: ' . $barcode . PHP_EOL;
                    $detail .= 'Cód. Electrónico: ' . $electronic_code . PHP_EOL;
                    $detail .= 'ID Comercio: ' . $id_comercio . PHP_EOL;
                    $action = 'Liberación de Tarjeta de Cobranza - Cobro Digital';
                    $this->registerActivity($action, $detail, $customer_code);

                    if ($paraments->gral_config->billing_for_service) {

                        $this->loadModel('Customers');
                        $customer = $this->Customers->get($customer_code, [
                            'contain' => ['Connections']
                        ]);

                        if (sizeof($customer->connections) > 0) {

                            $this->loadModel('Connections');

                            foreach ($customer->connections as $connection) {
                                $connection->cobrodigital_card = FALSE;
                                $this->Connections->save($connection);
                            }
                        }
                    }
                    $this->Flash->success(__('Se ha liberado la Tarjeta correctamente.'));
                } else {
                    $this->Flash->error(__('No se ha liberado la Tarjeta. por favor, intente nuevamente.'));
                }
            } else if ($data['payment_getway_id'] == 7) {

                $this->loadModel('PayuAccounts');

                $card = $this->PayuAccounts->get($data['account_id']);
                $customer_code = $card->customer_code;
                $barcode = $card->barcode;
                $card->used = FALSE;
                $card->deleted = FALSE;
                $card->customer_code = NULL;

                if ($this->PayuAccounts->save($card)) {

                    $detail = "";
                    $detail .= 'Cód. Barra: ' . $barcode . PHP_EOL;
                    $action = 'Liberación de Tarjeta de Cobranza - PayU';
                    $this->registerActivity($action, $detail, $customer_code);

                    if ($paraments->gral_config->billing_for_service) {

                        $this->loadModel('Customers');
                        $customer = $this->Customers->get($customer_code, [
                            'contain' => ['Connections']
                        ]);

                        if (sizeof($customer->connections) > 0) {

                            $this->loadModel('Connections');

                            foreach ($customer->connections as $connection) {
                                $connection->payu_card = FALSE;
                                $this->Connections->save($connection);
                            }
                        }
                    }
                    $this->Flash->success(__('Se ha liberado la Tarjeta correctamente.'));
                } else {
                    $this->Flash->error(__('No se ha liberado la Tarjeta. por favor, intente nuevamente.'));
                }
            }
        } else {
            $this->Flash->warning(__('No se ha liberado la Tarjeta. por favor, intente nuevamente.'));
        }

        return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
    }

    public function getAccounts()
    {
        if ($this->request->is('ajax')) {

            $this->loadModel('Connections');

            $payment_getway = $this->request->getSession()->read('payment_getway');
            $paraments = $this->request->getSession()->read('paraments');

            $data = $this->request->input('json_decode');
            $customers_accounts = $this->CustomersAccounts
                ->find()
                ->where([
                    'customer_code' => $data->customer_code,
                    'deleted'       => FALSE
                ]);

            $response = [
                'accounts' => NULL,
            ];
            $accounts = [];

            if ($customers_accounts) {

                foreach ($customers_accounts as $customer_account) {

                    $model = NULL;
                    $column_connection = "";

                    foreach ($payment_getway->methods as $key => $pg) {

                        $config = $pg->config;

                        if ($payment_getway->config->$config->enabled && $payment_getway->config->$config->cash) {

                            if ($pg->id == $customer_account->payment_getway_id) {
                                $column_connection = $key;
                                $model = $payment_getway->config->$config->model;
                                continue;
                            }
                        }
                    }

                    if ($model) {

                        $this->loadModel($model);
                        $account_model = $this->$model
                            ->find()
                            ->where([
                                'customer_code'     => $data->customer_code,
                                'deleted'           => FALSE,
                                'payment_getway_id' => $customer_account->payment_getway_id
                            ])->first();

                        if ($paraments->gral_config->billing_for_service) {

                            $connections = $this->Connections
                                ->find()
                                ->contain([
                                    'Services'
                                ])
                                ->where([
                                    'customer_code' => $data->customer_code
                                ]);

                            $connectionxs = [];

                            if ($connections->count() > 0) {

                                foreach ($connections as $conn) {

                                    if ($conn->$column_connection) {

                                        $connectionxs[$conn->id] = $conn->created->format('d/m/Y') . ' - ' . $conn->service->name . ' - ' . $conn->address;
                                    }
                                }

                                $account_model->connections = $connectionxs;
                            }
                        }

                        if ($account_model) {
                            array_push($accounts, $account_model);
                        }
                    }
                }
                if (sizeof($accounts) > 0) {
                    $response['accounts'] = $accounts;
                }
            }
        }
        $this->set(compact('response'));
        $this->set('_serialize', ['response']);
    }
}
