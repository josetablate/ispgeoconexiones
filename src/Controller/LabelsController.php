<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Labels Controller
 *
 * @property \App\Model\Table\LabelsTable $Labels
 *
 * @method \App\Model\Entity\Label[] paginate($object = null, array $settings = [])
 */
class LabelsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }

    public function isAuthorized($user = null)
    {
        if ($this->request->getParam('action') == 'tickets') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function indexCustomers()
    {
        $labels = $this->Labels->find()->where(['entity' => 0]);

        $label = $this->Labels->newEntity();

        $this->set(compact('labels', 'label'));
        $this->set('_serialize', ['labels', 'label']);
    }

    public function indexConnections()
    {
        $labels = $this->Labels->find()->where(['entity' => 1]);

        $label = $this->Labels->newEntity();

        $this->set(compact('labels', 'label'));
        $this->set('_serialize', ['labels', 'label']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->request->is('post')) {

            $label = $this->Labels->newEntity();
            $label = $this->Labels->patchEntity($label, $this->request->getData());

            if ($this->Labels->save($label)) {

                $this->Flash->success(__('Etiqueta agregada correctamente.'));
             } else {
                $this->Flash->error(__('Error al intentar agregar la etiqueta.'));
            }
        }

        switch ($label->entity) {
            case 0:
                $controller = 'Labels';
                $action = 'index_customers';
                break;
            case 1:
                $controller = 'Labels';
                $action = 'index_connections';
                break;
            case 2:
                $controller = 'Tickets';
                $action = 'config';
                break;
        }
        return $this->redirect(['controller' => $controller, 'action' => $action]);
    }

    /**
     * Edit method
     *
     * @param string|null $id Label id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if ($this->request->is(['patch', 'post', 'put'])) {

            $label = $this->Labels->get($this->request->getData('id'));
            $label = $this->Labels->patchEntity($label, $this->request->getData());
            if ($this->Labels->save($label)) {
                $this->Flash->success(__('Lo cambios se guardaron correctamente'));
            } else {
                $this->Flash->error(__('Error al intentar editar la etiquete'));
            }
        }

        switch ($label->entity) {
            case 0:
                $controller = 'Labels';
                $action = 'index_customers';
                break;
            case 1:
                $controller = 'Labels';
                $action = 'index_connections';
                break;
            case 2:
                $controller = 'Tickets';
                $action = 'config';
                break;
        }
        return $this->redirect(['controller' => $controller, 'action' => $action]);
    }

    /**
     * Delete method
     *
     * @param string|null $id Label id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $label = $this->Labels->get($_POST['id']);

        switch ($label->entity) {
            case 0:
                $controller = 'Labels';
                $action = 'index_customers';
                $this->loadModel('CustomersLabels');
                $this->CustomersLabels->deleteAll(['label_id' => $label->id]);
                break;
            case 1:
                $controller = 'Labels';
                $action = 'index_connections';
                $this->loadModel('ConnectionsLabels');
                $this->ConnectionsLabels->deleteAll(['label_id' => $label->id]);
                break;
            case 2:
                $controller = 'Tickets';
                $action = 'config';
                break;
        }

        if ($this->Labels->delete($label)) {
            $this->Flash->success(__('Etiqueta eliminada.'));
        } else {
            $this->Flash->error(__('Error al eliminar la Etiqueta.'));
        }

        return $this->redirect(['controller' => $controller, 'action' => $action]);
    }

    public function tickets()
    {
        if ($this->request->is('ajax')) {

            $labels = $this->Labels->find()->where(['entity' => 2]);

            $label = $this->Labels->newEntity();

            $this->set(compact('labels', 'label'));
            $this->set('_serialize', ['labels', 'label']);
        }
    }
}
