<?php
namespace App\Controller;

use App\Controller\IspControllerMikrotikDhcp;

use App\Controller\MikrotikDhcpUtils\RowAddressList;
use App\Controller\MikrotikDhcpUtils\RowDhcpServer;
use App\Controller\MikrotikDhcpUtils\RowGateway;
use App\Controller\MikrotikDhcpUtils\RowLease;
use App\Controller\MikrotikDhcpUtils\RowNetwork;
use App\Controller\MikrotikDhcpUtils\RowPool;
use App\Controller\MikrotikDhcpUtils\RowQueue;

use Cidr;
use Cake\Filesystem\File;
use Cake\I18n\Time;



class IspControllerMikrotikDhcpTaController extends IspControllerMikrotikDhcp
{
    public function initialize() {
         
        parent::initialize();     
    } 
     
    //########CONFIG#########
    
    //controller
     
    public function edit($id = null) {
        
        $this->loadModel('Controllers');

        $controller = $this->Controllers->get($id, [
            'contain' => []
        ]);
        $tcontroller = $this->IntegrationRouter->MikrotikDhcpTa->getControllerTemplate($controller); 

        $controller->local_address = $tcontroller->local_address;
        $controller->dns_server = $tcontroller->dns_server;  
        $controller->queue_default = $tcontroller->queue_default;  
        $controller->address_list_name = $tcontroller->address_list_name;  
        

        if ($this->request->is(['patch', 'post', 'put'])) {

            $request_data = $this->request->getData();

            $request_data['queue_default'] = ( array_key_exists('queue_default', $request_data)) ? $request_data['queue_default'] : $tcontroller->queue_default; 

            if ($this->IntegrationRouter->MikrotikDhcpTa->editController($controller, $request_data)) {

                $controller =  $this->Controllers->patchEntity($controller, $request_data);

                if ($this->Controllers->save($controller)) {
                    $this->Flash->success(__('Los cambios se guardaron correctamente.'));
                } else {
                    $this->_ctrl->Flash->error(__('No se pudo editar el Controlador.'));
                }
            }
        }

        $controller->queue_default = ($controller->queue_default) ? $controller->queue_default : 'default-small' ;     
        $controller->trademark_name = $this->getTrademarksByTemplate($controller->template)[$controller->trademark];
        $controller->template_name = $this->getTemplateName($controller->template);      
        $queue_types_array = $this->IntegrationRouter->MikrotikDhcpTa->getQueueTypesApi($controller);
        
        $this->set(compact('controller', 'queue_types_array'));
        $this->set('_serialize', ['controller']);
    }
     
    public function config($id) {
        
        $this->loadModel('Controllers');
        $this->loadModel('Services');

        $controller = $this->Controllers->get($id, [
            'contain' => []
        ]);

        $tcontroller = $this->IntegrationRouter->MikrotikDhcpTa->getControllerTemplate($controller); 

        $controller->local_address = $tcontroller->local_address;
        $controller->dns_server = $tcontroller->dns_server;  
        $controller->queue_default = $tcontroller->queue_default;  

        $controller->queue_default = ($controller->queue_default) ? $controller->queue_default : 'default-small' ; 

        $controller->interfaces = $this->IntegrationRouter->MikrotikDhcpTa->getInterfacesApi($controller);
     
        $queue_types_array = $this->IntegrationRouter->MikrotikDhcpTa->getQueueTypesApi($controller);

        $services = $this->Services->find()->where(['deleted' => false, 'enabled' => true]);
        $servicesArray = [];
        $servicesArray[''] = '';
        foreach ($services as $service){
            $servicesArray[$service->id] = $service->name;
        }
    

        $this->set(compact('controller', 'queue_types_array', 'servicesArray'));
        $this->set('_serialize', ['controller']);
    }
    
    public function sync($id) {
        
        $this->loadModel('Controllers');

        $controller = $this->Controllers->get($id);

        $this->clearQueues();
        $this->clearLeases();
        $this->clearAddressLists();
        $this->clearNetworks();
        $this->clearGateways();
        $this->clearDhcpServers();
        $this->clearPools();
        

        $this->set(compact('controller'));
        $this->set('_serialize', ['controller']);
    }
 
     
   //desde shell (control automatico)
    public function checkSync($id){
        
        $this->loadModel('Controllers');
        
        $controller = $this->Controllers->get($id);
         
        $paraments = $this->request->getSession()->read('paraments');                 
         
        if($controller->integration && $this->IntegrationRouter->MikrotikDhcpTa->getStatus($controller)){
             
             $controller->counter_unsync = 0;        
             $controller->counter_unsync += $this->diffQueues($controller, false);
             $controller->counter_unsync += $this->diffLeases($controller, false);
             $controller->counter_unsync += $this->diffAddressLists($controller, false);
             $controller->last_status = true;                  
        }else{
             $controller->last_status = false;              
        }
        
        $controller->last_control_diff = Time::now();      
        $this->Controllers->save($controller);  
        
        return $controller->counter_unsync;
        
    }

   
     
   //plans
   
    public function getPlansByController(){

        $controller_id = $this->request->getQuery('controller_id');
        
        $plans = $this->IntegrationRouter->MikrotikDhcpTa->getPlans($controller_id);
    
        $this->set(compact('plans'));
        $this->set('_serialize', ['plans']);
    }

    public function addPlan(){
        
      if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);

            $response = $this->IntegrationRouter->MikrotikDhcpTa->addPlan($request_data);
            
            $this->set('response', $response);
        }
    }
     
    public function editPlan() {
        
        if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            $response = $this->IntegrationRouter->MikrotikDhcpTa->editPlan($request_data);
            
            $this->set('response', $response);
        }
    }
    
    public function movePlan(){
        
        if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            /*
            [service_id_destino] => 2
            [controller_id] => 1
            [id] => 1
            */
            
            //$this->log($request_data, 'debug');

            $response = $this->IntegrationRouter->MikrotikDhcpTa->movePlan($request_data);
            
            $this->set('response', $response);
        }
    }
    

    public function deletePlan() {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $data = $this->request->input('json_decode');
    
            $response = $this->IntegrationRouter->MikrotikDhcpTa->deletePlan($data->id);

            $this->set('response', $response);
            
        }
    }
     
     
    //networks
   
    public function getNetworksByController() {

        $controller_id = $this->request->getQuery('controller_id');
        
        $networks = $this->IntegrationRouter->MikrotikDhcpTa->getNetworks($controller_id);
    
        $this->set(compact('networks'));
        $this->set('_serialize', ['networks']);
    }
    
    public function addNetwork() {
        
        if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            $response = $this->IntegrationRouter->MikrotikDhcpTa->addNetwork($request_data);
            
            $this->set('response', $response);
        }
    }
    
    public function deleteNetwork() {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $data = $this->request->input('json_decode');
            
            $response = $this->IntegrationRouter->MikrotikDhcpTa->deleteNetwork( $data->id);

            $this->set('response', $response);
        }
    }
    
    public function getRangeNetwork() {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $address = $this->request->input('json_decode')->address;
            $range = Cidr::cidrToRange($address);
            $this->set('range', $range);
        }
    } 
    
    public function editNetwork() {
        
        if ($this->request->is('ajax')) {

            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);

            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($request_data['controller_id']);

            $response = $this->IntegrationRouter->MikrotikDhcpTa->editNetwork($controller, $request_data);

            $this->set('response', $response);
        }
    }
    
    public function moveNetwork(){
        
        if ($this->request->is('ajax')) {

            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);

            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($request_data['controller_id']);

            $response = $this->IntegrationRouter->MikrotikDhcpTa->moveNetwork($request_data);

            $this->set('response', $response);
        }
        
    }
     
     
     
    //gateways
    
    public function getGatewaysByController() {

        $controller_id = $this->request->getQuery('controller_id');
        
        $gateways = $this->IntegrationRouter->MikrotikDhcpTa->getGateways($controller_id);
    
        $this->set(compact('gateways'));
        $this->set('_serialize', ['gateways']);
    }
    
    public function addGateway() {
        
        if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            $response = $this->IntegrationRouter->MikrotikDhcpTa->addGateway($request_data);
            
            $this->set('response', $response);
        }
    }
    
    public function deleteGateway() {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $data = $this->request->input('json_decode');
            
            $response = $this->IntegrationRouter->MikrotikDhcpTa->deleteGateway( $data->id);

            $this->set('response', $response);
        }
    }
    
    function editGateway() {
        
        if ($this->request->is('ajax')) {

            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);

            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($request_data['controller_id']);

            $response = $this->IntegrationRouter->MikrotikDhcpTa->editGateway($controller, $request_data);

            $this->set('response', $response);
        }
    }
    
     
    //pools
   
    public function getPoolsByController() {

        $controller_id = $this->request->getQuery('controller_id');
        
        $pools = $this->IntegrationRouter->MikrotikDhcpTa->getPools($controller_id);
    
        $this->set(compact('pools'));
        $this->set('_serialize', ['pools']);
    }
    
    public function addPool() {
        
        if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            $response = $this->IntegrationRouter->MikrotikDhcpTa->addPool($request_data);
            
            $this->set('response', $response);
        }
    }
    
    public function deletePool() {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $data = $this->request->input('json_decode');
            
            $response = $this->IntegrationRouter->MikrotikDhcpTa->deletePool( $data->id);

            $this->set('response', $response);
        }
    }
    
    function editPool() {
        
        if ($this->request->is('ajax')) {

            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);

            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($request_data['controller_id']);

            $response = $this->IntegrationRouter->MikrotikDhcpTa->editPool($controller, $request_data);

            $this->set('response', $response);
        }
    }
     
     
     
    //profiles
  
    public function getProfilesByController() {

        $controller_id = $this->request->getQuery('controller_id');
        
        $profiles = $this->IntegrationRouter->MikrotikDhcpTa->getProfiles($controller_id);
    
        $this->set(compact('profiles'));
        $this->set('_serialize', ['profiles']);
    }
    
    public function addProfile() { 
        
         if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            $response = $this->IntegrationRouter->MikrotikDhcpTa->addProfile($request_data);
            
            $this->set('response', $response);
        }

    }

    public function deleteProfile() {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $data = $this->request->input('json_decode');
            
            $response = $this->IntegrationRouter->MikrotikDhcpTa->deleteProfile( $data->id);

            $this->set('response', $response);
        }
    }

    public function editProfile() {
        
         if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            $controller_id = $request_data['controller_id'];
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
            
            $response = $this->IntegrationRouter->MikrotikDhcpTa->editProfile($controller, $request_data);
            
            $this->set('response', $response);
        }
 
    }
    
   
    //dhcp server
    
    public function getDhcpServersByController(){

        $controller_id = $this->request->getQuery('controller_id');
 
        $dhcpServers = $this->IntegrationRouter->MikrotikDhcpTa->getDhcpServers($controller_id);
    
        $this->set(compact('dhcpServers'));
        $this->set('_serialize', ['dhcpServers']);
    }
    
    public function addDhcpServer() {
        
        if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            $response = $this->IntegrationRouter->MikrotikDhcpTa->addDhcpServer($request_data);
            
            $this->set('response', $response);
        }
       
    }
    
    public function editDhcpServer() {
        
        if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            $response = $this->IntegrationRouter->MikrotikDhcpTa->editDhcpServer($request_data);
            
            $this->set('response', $response);
        }
    }

    public function deleteDhcpServer() {
         
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $data = $this->request->input('json_decode');
            
            $response = $this->IntegrationRouter->MikrotikDhcpTa->deleteDhcpServer( $data->id);

            $this->set('response', $response);
        }
    }     
     
     
     //ip excluida    
 
    public function getIpExcludedByController() {

        $controller_id = $this->request->getQuery('controller_id');
 
        $ip_excluded = $this->IntegrationRouter->MikrotikDhcpTa->getIPExcluded($controller_id);
    
        $this->set(compact('ip_excluded'));
        $this->set('_serialize', ['ip_excluded']);
    }
    
    public function deleteIPExcluded() {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $data = $this->request->input('json_decode');
            
            $response = $this->IntegrationRouter->MikrotikDhcpTa->deleteIPExcludedById( $data->id);

            $this->set('response', $response);
        }
    }
    
    public function addIPExcluded() {
        
        if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            
                 //$this->log($request_data, 'debug');
         
            $response = $this->IntegrationRouter->MikrotikDhcpTa->addIPExcludedDirect($request_data);
         
            $this->set('response', $response);
        }
       
    }
    
   
     
    //########SYNC#########   
     
     
    //sync DHCP Servers

    public function syncDhcpServers()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
      
            $ids = $this->request->input('json_decode')->ids;
            $delete_dhcpserver_side_b = $this->request->input('json_decode')->delete_dhcpserver_side_b;
            
            $this->loadModel('Connections');
            $success = true;
            
                
            $fileB = new File(WWW_ROOT . 'sync/mikrotik_dhcp_ta/dhcpserversB.json', false, 0644);
            $json = $fileB->read($fileB, 'r');
            $json =  json_decode($json);
            
            foreach($json->data as $row){
                
                if($row->marked_to_delete){
                    
                    if(!$row->other){ //son registro del controlador, que deben eliminarse, 
                                            
                    	if(!$this->IntegrationRouter->MikrotikDhcpTa->deleteDhcpServerInController($controller, $row->api_id->value)){
                    		 $success = false;
                    		 break;
                    	}
                    	
                    }else if($row->other && $delete_dhcpserver_side_b){ //son registros agenos al controlador, y esta marcado para limpieza
                    
                    	if(!$this->IntegrationRouter->MikrotikDhcpTa->deleteDhcpServerInController($controller, $row->api_id->value)){
                    		 $success = false;
                    		 break;
                    	}
                    	
                    }
                }
            }
            
            foreach($ids as $id){
               
                if(!$this->IntegrationRouter->MikrotikDhcpTa->syncDhcpServer($id)){
                    $success = false;
                    break;
                }
            }      
       
            
            $this->diffDhcpServers($controller);

            $this->set('data', $success);
        }
    }
     
    public function syncDhcpServerIndi()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
      
            $ids = $this->request->input('json_decode')->ids;         
            
            $this->loadModel('Connections');
            $success = true;   
            
            foreach($ids as $id){
               
                if(!$this->IntegrationRouter->MikrotikDhcpTa->syncDhcpServer($id)){
                    $success = false;
                    break;
                }
            }     
       
            
            $this->diffDhcpServers($controller);

            $this->set('data', $success);
        }
    }
  
    public function refreshDhcpServers(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
            
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $ok = false;
            
            if($this->IntegrationRouter->MikrotikDhcpTa->getStatus($controller)){
  
                $ok = true;
            }
            
            if($ok){
              $this->diffDhcpServers($controller);  
            }
            
            
            $this->set('data', $ok);
        }
        
    }
  
    public function deleteDhcpServerInController(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
            $dhcpserver_api_id = $this->request->input('json_decode')->dhcpserver_api_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $success = true;
            
            if(!$this->IntegrationRouter->MikrotikDhcpTa->deleteDhcpServerInController($controller, $dhcpserver_api_id->value)){
                 $success = false;
            }
            
            $this->diffDhcpServers($controller);
            
            $this->set('data', true);
        }
        
    }
    
    protected function diffDhcpServers($controller){
         
        $this->clearDhcpServers();

        $dhcpServersA = $this->IntegrationRouter->MikrotikDhcpTa->getDhcpServersArray($controller->id);
        $dhcpServersB = $this->IntegrationRouter->MikrotikDhcpTa->getDhcpServersInController($controller);

        $tableA = [];
        $tableB = [];
        
        foreach ($dhcpServersA as $dhcpserver_a) {
            
            $row = new RowDhcpServer($dhcpserver_a, 'A');
             
            //existe en A pero no en B
            if(!array_key_exists($dhcpserver_a['name'], $dhcpServersB)){
                $tableA[] = $row;
            }else{
                
                $diff = false;
                
                $dhcpserver_b = $dhcpServersB[$dhcpserver_a['name']];
                
                if(strtoupper($dhcpserver_a['api_id']) != strtoupper($dhcpserver_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($dhcpserver_a['name'] != $dhcpserver_b['name']){
                    $row->name->state = false;
                     $diff = true;
                }
                
                if($dhcpserver_a['interface'] != $dhcpserver_b['interface']){
                    $row->interface->state = false;
                    $diff = true;
                }
                
                if($dhcpserver_a['address_pool'] != $dhcpserver_b['address_pool']){
                    $row->address_pool->state = false;
                    $diff = true;
                }
                
                if($dhcpserver_a['leases_time'] != $dhcpserver_b['leases_time']){
                    $row->leases_time->state = false;
                    $diff = true;
                }
                
                if($dhcpserver_a['disabled'] != $dhcpserver_b['disabled']){
                    $row->disabled->state = false;
                    $diff = true;
                }
                
                if( $diff){
                    $tableA[] = $row;
                }
                
            }
        }
        
        foreach ($dhcpServersB as $dhcpserver_b) {
            
            $row = new RowDhcpServer($dhcpserver_b, 'B');
            
            if($dhcpserver_b['other'] == true){
            	$row->setNew();
            	$tableB[] = $row;
            	continue;
            }
            
            //existe en B pero no en A
            if(!array_key_exists($dhcpserver_b['name'], $dhcpServersA)){
                $row->setNew();
                $tableB[] = $row;
                
            }else{
                
                $diff = false;
                
                $dhcpserver_a = $dhcpServersA[$dhcpserver_b['name']];
            
                if(strtoupper($dhcpserver_a['api_id']) != strtoupper($dhcpserver_b['api_id'])){
                    $row->setNew();
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($dhcpserver_a['name'] != $dhcpserver_b['name']){
                    $row->name->state = false;
                     $diff = true;
                }
                
                if($dhcpserver_a['interface'] != $dhcpserver_b['interface']){
                    $row->interface->state = false;
                    $diff = true;
                }
                
                if($dhcpserver_a['address_pool'] != $dhcpserver_b['address_pool']){
                    $row->address_pool->state = false;
                    $diff = true;
                }
                
                if($dhcpserver_a['leases_time'] != $dhcpserver_b['leases_time']){
                    $row->leases_time->state = false;
                    $diff = true;
                }
               
                if($dhcpserver_a['disabled'] != $dhcpserver_b['disabled']){
                    $row->disabled->state = false;
                    $diff = true;
                }
              
                if( $diff){
                    $tableB[] = $row;
                }
            }
            
        }
        
        $this->updateFile($tableA,'dhcpserversA', 'mikrotik_dhcp_ta');
        $this->updateFile($tableB,'dhcpserversB', 'mikrotik_dhcp_ta');
        
    } 
    
    protected function clearDhcpServers(){

        $this->updateFile([],'dhcpserversA', 'mikrotik_dhcp_ta');
        $this->updateFile([],'dhcpserversB', 'mikrotik_dhcp_ta');
    } 
    


    //sync queues

    public function syncQueues()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
      
            $connections_ids = $this->request->input('json_decode')->connections_ids;
            $delete_queue_side_b = $this->request->input('json_decode')->delete_queue_side_b;
            
            $this->loadModel('Connections');
            $success = true;
            
            $fileB = new File(WWW_ROOT . 'sync/mikrotik_dhcp_ta/queuesB.json', false, 0644);
            $json = $fileB->read($fileB, 'r');
            $json =  json_decode($json);
            
            foreach($json->data as $row){
                
                if($row->marked_to_delete){
                    
                    if(!$row->other){ //son registro del controlador, que deben eliminarse, 
                        
                        if(!$this->IntegrationRouter->MikrotikDhcpTa->deleteQueueInController($controller, $row->api_id->value)){
                             $success = false;
                             break;
                        }
                        
                    }else if($row->other && $delete_queue_side_b){ //son registros agenos al controlador, y esta marcado para limpieza
                    
                        if(!$this->IntegrationRouter->MikrotikDhcpTa->deleteQueueInController($controller, $row->api_id->value)){
                             $success = false;
                             break;
                        }
                        
                    }
                }
            }
            
            foreach($connections_ids as $connection_id){
                
                $connection = $this->Connections->get($connection_id, ['contain' => ['Controllers', 'Customers']]);
                if(!$this->IntegrationRouter->MikrotikDhcpTa->syncQueue($connection)){
                    $success = false;
                    break;
                }
            }
            
            $this->diffQueues($controller);

            $this->set('data', $success);
        }
    }
     
    public function syncQueueIndi()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
      
            $connections_ids = $this->request->input('json_decode')->connections_ids;         
            
            $this->loadModel('Connections');
            $success = true;
            
            foreach($connections_ids as $connection_id){
                
                $connection = $this->Connections->get($connection_id, ['contain' => ['Controllers', 'Customers']]);
                if(!$this->IntegrationRouter->MikrotikDhcpTa->syncQueue($connection)){
                    $success = false;
                    break;
                }
            }
            
            $this->diffQueues($controller);

            $this->set('data', $success);
        }
    }
  
    public function refreshQueues(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $ok = false;
            
            if($this->IntegrationRouter->MikrotikDhcpTa->getStatus($controller)){
  
                $ok = true;
            }
            
            if($ok){
               $this->diffQueues($controller); 
            }
            
            $this->set('data', $ok);
        }
        
    }
  
    public function deleteQueueInController(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
            $queue_api_id = $this->request->input('json_decode')->queue_api_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $success = true;     
            
            if(!$this->IntegrationRouter->MikrotikDhcpTa->deleteQueueInController($controller, $queue_api_id->value)){
                 $success = false;
            }
            
            $this->diffQueues($controller);
            
            $this->set('data', true);
        }
        
    }
    
    protected function diffQueues($controller, $file = true){
         
        if($file){
            $this->clearQueues(); 
        }     

        $queuesA = $this->IntegrationRouter->MikrotikDhcpTa->getQueuesAndConnectionsArray($controller);
        
        $queuesB = $this->IntegrationRouter->MikrotikDhcpTa->getQueuesInController($controller);
        
        $tableA = [];
        $tableB = [];
        
        $count = 0;
        
        foreach ($queuesA as $queue_a) {
            
            $row = new RowQueue($queue_a, 'A');
             
            //existe en A pero no en B
            if(!array_key_exists($queue_a['name'], $queuesB)){
                $tableA[] = $row;
                $count++;
                
            }else{
                
                $diff = false;
                
                $queue_b = $queuesB[$queue_a['name']];
                
                if(strtoupper($queue_a['api_id']) != strtoupper($queue_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($queue_a['name'] != $queue_b['name']){
                    $row->name->state = false;
                     $diff = true;
                }
                
                if($queue_a['comment'] != $queue_b['comment']){
                    $row->comment->state = false;
                    $diff = true;
                }
                
                if($queue_a['target'] != $queue_b['target']){
                    $row->target->state = false;
                    $diff = true;
                }
            
                if($queue_a['max_limit']  != $queue_b['max_limit']){
                    $row->max_limit->state = false;
                    $diff = true;
                }
                
                 if($queue_a['limit_at']  != $queue_b['limit_at']){
                    $row->limit_at->state = false;
                    $diff = true;
                }
                
                 if($queue_a['burst_limit']  != $queue_b['burst_limit']){
                    $row->burst_limit->state = false;
                    $diff = true;
                }
                
                 if($queue_a['burst_threshold']  != $queue_b['burst_threshold']){
                    $row->burst_threshold->state = false;
                    $diff = true;
                }
                
                 if($queue_a['burst_time']  != $queue_b['burst_time']){
                    $row->burst_time->state = false;
                    $diff = true;
                }
                
                 if($queue_a['priority']  != $queue_b['priority']){
                    $row->priority->state = false;
                    $diff = true;
                }
                
                 if($queue_a['queue']  != $queue_b['queue']){
                    $row->queue->state = false;
                    $diff = true;
                }
                
              
                if( $diff){
                    $tableA[] = $row;
                    $count++;
                }
                
            }
        }
        
        foreach ($queuesB as $queue_b) {
            
            $row = new RowQueue($queue_b, 'B');
            
             if($queue_b['other'] == true){
                $row->setNew();
                $tableB[] = $row;
                continue;
                
            }
            
            //existe en B pero no en A
            if(!array_key_exists($queue_b['name'], $queuesA)){
                $row->setNew();
                $tableB[] = $row;
                $count++;
                
            }else{
                
                $diff = false;
                
                $queue_a = $queuesA[$queue_b['name']];
            
                if(strtoupper($queue_a['api_id']) != strtoupper($queue_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($queue_a['name'] != $queue_b['name']){
                    $row->name->state = false;
                     $diff = true;
                }
                
                if($queue_a['comment'] != $queue_b['comment']){
                    $row->comment->state = false;
                    $diff = true;
                }
               
                if($queue_a['target'] != $queue_b['target']){
                    $row->target->state = false;
                    $diff = true;
                }
                
                if($queue_a['max_limit']  != $queue_b['max_limit']){
                    $row->max_limit->state = false;
                    $diff = true;
                }
                
                 if($queue_a['limit_at']  != $queue_b['limit_at']){
                    $row->limit_at->state = false;
                    $diff = true;
                }
                
                 if($queue_a['burst_limit']  != $queue_b['burst_limit']){
                    $row->burst_limit->state = false;
                    $diff = true;
                }
                
                 if($queue_a['burst_threshold']  != $queue_b['burst_threshold']){
                    $row->burst_threshold->state = false;
                    $diff = true;
                }
                
                 if($queue_a['burst_time']  != $queue_b['burst_time']){
                    $row->burst_time->state = false;
                    $diff = true;
                }
                
                 if($queue_a['priority']  != $queue_b['priority']){
                    $row->priority->state = false;
                    $diff = true;
                }
                
                 if($queue_a['queue']  != $queue_b['queue']){
                    $row->queue->state = false;
                    $diff = true;
                }
                
              
                if( $diff){
                    $tableB[] = $row;
                    $count++;
                }
            }
            
        }
        
        if($file){
            $this->updateFile($tableA,'queuesA', 'mikrotik_dhcp_ta');
            $this->updateFile($tableB,'queuesB', 'mikrotik_dhcp_ta');
        }
        
        return $count;
    } 
    
    protected function clearQueues(){
        
        $this->updateFile([],'queuesA', 'mikrotik_dhcp_ta');
        $this->updateFile([],'queuesB', 'mikrotik_dhcp_ta');
    } 
    
    
    
    // sync address list

    public function syncAddressLists()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $ok = true;
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            $address_lists_ids = $this->request->input('json_decode')->address_lists_ids;
            $delete_address_list_side_b = $this->request->input('json_decode')->delete_address_lists_side_b;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
                
            $fileB = new File(WWW_ROOT . 'sync/mikrotik_dhcp_ta/addressListsB.json', false, 0644);
            $json = $fileB->read($fileB, 'r');
            $json =  json_decode($json);
            
            
            foreach($json->data as $row){
                
                if($row->marked_to_delete){
                    
                    if(!$row->other){ //son registro del controlador, que deben eliminarse,     
                    
                    	if(!$this->IntegrationRouter->MikrotikDhcpTa->deleteAddressListInController($controller, $row->api_id->value)){
                            $ok = false;
                            break;
                        }
                        	
                    }else if($row->other && $delete_address_list_side_b){ //son registros agenos al controlador, y esta marcado para limpieza
                    	
                    	if(!$this->IntegrationRouter->MikrotikDhcpTa->deleteAddressListInController($controller, $row->api_id->value)){
                            $ok = false;
                            break;
                        }
                    }
                }
            }
             
            foreach($address_lists_ids as $address_list_id){
                    
               if(!$this->IntegrationRouter->MikrotikDhcpTa->syncAddressList($address_list_id)){
                   $ok = false;
                   break;
               }
            }

          
            
            $this->diffAddressLists($controller);

            $this->set('response', $ok);
        }
    }
     
    public function syncAddressListIndi()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $ok = true;
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            $address_lists_ids = $this->request->input('json_decode')->address_lists_ids;        
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);    
                
           foreach($address_lists_ids as $address_list_id){

               if(!$this->IntegrationRouter->MikrotikDhcpTa->syncAddressList($address_list_id)){
                   $ok = false;
                   break;
               }
           }
         
            
            $this->diffAddressLists($controller);

            $this->set('response', $ok);
        }
    }
  
    public function refreshAddressLists(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $ok = false;
            
            if($this->IntegrationRouter->MikrotikDhcpTa->getStatus($controller)){
  
                $ok = true;
            }
            
            if($ok){
                $this->diffAddressLists($controller);
            }
            
            $this->set('data', $ok);
        }
        
    }
 
    public function deleteAddressListInController(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
            $address_list_api_id = $this->request->input('json_decode')->address_list_api_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $success = true;
          
            if(!$this->IntegrationRouter->MikrotikDhcpTa->deleteAddressListInController($controller, $address_list_api_id->value)){
                 $success = false;
            }
            
            $this->diffAddressLists($controller);
            
            $this->set('data', true);
        }
        
    }
    
    protected function diffAddressLists($controller, $file = true){
         
        if($file){
             $this->clearAddressLists();  
        }

        $addressListsA = $this->IntegrationRouter->MikrotikDhcpTa->getAddressListsArray($controller->id);
        
        $addressListsB = $this->IntegrationRouter->MikrotikDhcpTa->getAddressListsInController($controller);
        
        $tableA = [];
        $tableB = [];
        
        $count = 0;
        
        foreach ($addressListsA as $address_list_a) {
            
            $row = new RowAddressList($address_list_a, 'A');
             
            //existe en A pero no en B
            if(!array_key_exists($address_list_a['list'].':'.$address_list_a['address'], $addressListsB)){
                $tableA[] = $row;
                $count++;
            }else{
                
                $diff = false;
                
                $address_list_b = $addressListsB[$address_list_a['list'].':'.$address_list_a['address']];
                
                if(strtoupper($address_list_a['api_id']) != strtoupper($address_list_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($address_list_a['list'] != $address_list_b['list']){
                    $row->list->state = false;
                     $diff = true;
                }
                
                if($address_list_a['address'] != $address_list_b['address']){
                    $row->address->state = false;
                     $diff = true;
                }
                
                if($address_list_a['comment'] != $address_list_b['comment']){
                    $row->comment->state = false;
                    $diff = true;
                }
              
                if( $diff){
                    $tableA[] = $row;
                    $count++;
                }
                
            }
        }
        
        foreach ($addressListsB as $address_list_b) {
            
            $row = new RowAddressList($address_list_b, 'B');
            
            if($address_list_b['other'] == true){
                $row->setNew();
                $tableB[] = $row;
                continue;
                
            }
            
            //existe en B pero no en A
            if(!array_key_exists($address_list_b['list'].':'.$address_list_b['address'], $addressListsA)){
                $row->setNew();
                $tableB[] = $row;
                $count++;
                
            }else{
                
                $diff = false;
                
                $address_list_a = $addressListsA[$address_list_b['list'].':'.$address_list_b['address']];
            
                if(strtoupper($address_list_a['api_id']) != strtoupper($address_list_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                    $row->marked_to_delete = true;
                }
                
                if($address_list_a['list'] != $address_list_b['list']){
                    $row->list->state = false;
                    $diff = true;
                }
                
                if($address_list_a['address'] != $address_list_b['address']){
                    $row->address->state = false;
                    $diff = true;
                }
                
                if($address_list_a['comment'] != $address_list_b['comment']){
                    $row->comment->state = false;
                     $diff = true;
                }
               
              
                if( $diff){
                    $tableB[] = $row;
                    $count++;
                }
            }
            
        }
        
        if($file){
            
            $this->updateFile($tableA,'addressListsA', 'mikrotik_dhcp_ta');
            $this->updateFile($tableB,'addressListsB', 'mikrotik_dhcp_ta');
        }
        
        return $count;
        
        
        
    } 
    
    protected function clearAddressLists(){
        
        $this->updateFile([],'addressListsA', 'mikrotik_dhcp_ta');
        $this->updateFile([],'addressListsB', 'mikrotik_dhcp_ta');
        
    } 
    
    
    
    // sync networks

    public function syncNetworks()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $ok = true;
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            $networks_ids = $this->request->input('json_decode')->networks_ids;
            $delete_networks_side_b = $this->request->input('json_decode')->delete_networks_side_b;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
                
            $fileB = new File(WWW_ROOT . 'sync/mikrotik_dhcp_ta/networksB.json', false, 0644);
            $json = $fileB->read($fileB, 'r');
            $json =  json_decode($json);
            
            
            foreach($json->data as $row){
                
                if($row->marked_to_delete){
                    
                    if(!$row->other){ //son registros del controlador, que deben eliminarse,    
                    
                        if(!$this->IntegrationRouter->MikrotikDhcpTa->deleteNetworksInController($controller, $row->api_id->value)){
                            $ok = false;
                            break;
                        }
                    }else if($row->other && $delete_networks_side_b){ //son registros agenos al controlador, y esta marcado para limpieza
                    	
                    	if(!$this->IntegrationRouter->MikrotikDhcpTa->deleteNetworksInController($controller, $row->api_id->value)){
                            $ok = false;
                            break;
                        }
                    }
                }
            }  
                
            foreach($networks_ids as $networks_id){
                    
               if(!$this->IntegrationRouter->MikrotikDhcpTa->syncNetwork($networks_id)){
                   $ok = false;
                   break;
               }
            }
            
            $this->diffNetworks($controller);

            $this->set('response', $ok);
        }
    }
     
    public function syncNetworkIndi()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $ok = true;
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            $networks_ids = $this->request->input('json_decode')->networks_ids;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);        
                
            foreach($networks_ids as $networks_id){
                    
               if(!$this->IntegrationRouter->MikrotikDhcpTa->syncNetwork($networks_id)){
                   $ok = false;
                   break;
               }
            }
            
            $this->diffNetworks($controller);

            $this->set('response', $ok);
        }
    }
  
    public function refreshNetworks(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $ok = false;
            
            if($this->IntegrationRouter->MikrotikDhcpTa->getStatus($controller)){
  
                $ok = true;
            }
            
            if($ok){
                $this->diffNetworks($controller);
            }
            
            $this->set('data', $ok);
        }
        
    }
 
    public function deleteNetworksInController(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
            $network_api_id = $this->request->input('json_decode')->network_api_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $success = true;
          
            if(!$this->IntegrationRouter->MikrotikDhcpTa->deleteNetworksInController($controller, $network_api_id->value)){
                 $success = false;
            }
            
            $this->diffNetworks($controller);
            
            $this->set('data', true);
        }
        
    }
    
    protected function diffNetworks($controller){
        
        $this->clearNetworks(); 
        
        $networksA = $this->IntegrationRouter->MikrotikDhcpTa->getNetworksArray($controller->id);
        
        $networksB = $this->IntegrationRouter->MikrotikDhcpTa->getNetworksInController($controller);
        
        $tableA = [];
        $tableB = [];
        
        foreach ($networksA as $network_a) {
            
            $row = new RowNetwork($network_a, 'A');
             
            //existe en A pero no en B
            if(!array_key_exists($network_a['comment'], $networksB)){
                $tableA[] = $row;
            }else{
                
                $diff = false;
                
                $network_b = $networksB[$network_a['comment']];
                
                if(strtoupper($network_a['api_id']) != strtoupper($network_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($network_a['comment'] != $network_b['comment']){
                    $row->comment->state = false;
                     $diff = true;
                }
                
                if($network_a['address'] != $network_b['address']){
                    $row->address->state = false;
                     $diff = true;
                }
                
                if($network_a['gateway'] != $network_b['gateway']){
                    $row->gateway->state = false;
                    $diff = true;
                }
                
                if($network_a['dns_server'] != $network_b['dns_server']){
                    $row->dns_server->state = false;
                    $diff = true;
                }
              
                if( $diff){
                    $tableA[] = $row;
                }
                
            }
        }
        
        foreach ($networksB as $network_b) {
            
            $row = new RowNetwork($network_b, 'B');
            
            if($network_b['other'] == true){
            	$row->setNew();
            	$tableB[] = $row;
            	continue;
            }
            
            //existe en B pero no en A
            if(!array_key_exists($network_b['comment'], $networksA)){
                $row->setNew();
                $tableB[] = $row;
                
            }else{
                
                $diff = false;
                
                $network_a = $networksA[$network_b['comment']];
            
                if(strtoupper($network_a['api_id']) != strtoupper($network_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                    $row->marked_to_delete = true;
                }
                
                if($network_a['comment'] != $network_b['comment']){
                    $row->comment->state = false;
                    $diff = true;
                }
                
                if($network_a['address'] != $network_b['address']){
                    $row->address->state = false;
                    $diff = true;
                }
                
                if($network_a['gateway'] != $network_b['gateway']){
                    $row->gateway->state = false;
                     $diff = true;
                }
                
                if($network_a['dns_server'] != $network_b['dns_server']){
                    $row->dns_server->state = false;
                     $diff = true;
                }
               
              
                if( $diff){
                    $tableB[] = $row;
                }
            }
            
        }
        
        $this->updateFile($tableA,'networksA', 'mikrotik_dhcp_ta');
        $this->updateFile($tableB,'networksB', 'mikrotik_dhcp_ta');
    } 
    
    protected function clearNetworks(){
        
        $this->updateFile([],'networksA', 'mikrotik_dhcp_ta');
        $this->updateFile([],'networksB', 'mikrotik_dhcp_ta');
    } 
    
    
    
    //pools
    
    public function syncPools()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $ok = true;
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            $pools_ids = $this->request->input('json_decode')->pools_ids;
            $delete_pools_side_b = $this->request->input('json_decode')->delete_pools_side_b;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
            
            $fileB = new File(WWW_ROOT . 'sync/mikrotik_dhcp_ta/poolsB.json', false, 0644);
            $json = $fileB->read($fileB, 'r');
            $json =  json_decode($json);
            
            
           foreach($json->data as $row){
                
                if($row->marked_to_delete){
                    
                    if(!$row->other){ //son registro del controlador, que deben eliminarse,                        
                        
                        if(!$this->IntegrationRouter->MikrotikDhcpTa->deletePoolInController($controller, $row->api_id->value)){
                            $ok = false;
                            break;
                        }
                    	
                    }else if($row->other && $delete_pools_side_b){ //son registros agenos al controlador, y esta marcado para limpieza
                    	
                    	if(!$this->IntegrationRouter->MikrotikDhcpTa->deletePoolInController($controller, $row->api_id->value)){
                            $ok = false;
                            break;
                        }
                    }
                }
            }  
                
           foreach($pools_ids as $pool_id){

               if(!$this->IntegrationRouter->MikrotikDhcpTa->syncPool($pool_id)){
                   $ok = false;
                   break;
               }
           }
            
            
            $this->diffPools($controller);

            $this->set('response', $ok);
        }
    }
     
    public function syncPoolIndi()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $ok = true;
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            $pools_ids = $this->request->input('json_decode')->pools_ids;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
                
           foreach($pools_ids as $pool_id){

               if(!$this->IntegrationRouter->MikrotikDhcpTa->syncPool($pool_id)){
                   $ok = false;
                   break;
               }
           }
            
            
            $this->diffPools($controller);

            $this->set('response', $ok);
        }
    }
  
    public function refreshPools(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $ok = false;
            
            if($this->IntegrationRouter->MikrotikDhcpTa->getStatus($controller)){
  
                $ok = true;
            }
            
            if($ok){
                $this->diffPools($controller);
            }
            
            $this->set('data', $ok);
        }
        
    }
 
    public function deletePoolsInController(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
            $pool_api_id = $this->request->input('json_decode')->pool_api_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $success = true;
          
            if(!$this->IntegrationRouter->MikrotikDhcpTa->deletePoolInController($controller, $pool_api_id->value)){
                 $success = false;
            }
            
            $this->diffPools($controller);
            
            $this->set('data', true);
        }
        
    }
    
    protected function diffPools($controller){
         
        $this->clearPools();
         
        $poolsA = $this->IntegrationRouter->MikrotikDhcpTa->getPoolsArray($controller->id);
        
        $poolsB = $this->IntegrationRouter->MikrotikDhcpTa->getPoolsInController($controller);
        
        $tableA = [];
        $tableB = [];
        
        foreach ($poolsA as $pool_a) {
            
            $row = new RowPool($pool_a, 'A');
             
            //existe en A pero no en B
            if(!array_key_exists($pool_a['name'], $poolsB)){
                $tableA[] = $row;
            }else{
                
                $diff = false;
                
                $pool_b = $poolsB[$pool_a['name']];
                
                if(strtoupper($pool_a['api_id']) != strtoupper($pool_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($pool_a['name'] != $pool_b['name']){
                    $row->name->state = false;
                     $diff = true;
                }
                
                if($pool_a['ranges'] != $pool_b['ranges']){
                    $row->ranges->state = false;
                     $diff = true;
                }
              
                if( $diff){
                    $tableA[] = $row;
                }
                
            }
        }
        
        foreach ($poolsB as $pool_b) {
            
            $row = new RowPool($pool_b, 'B');
            
            if($pool_b['other'] == true){
            	$row->setNew();
            	$tableB[] = $row;
            	continue;
            }
            
            
            //existe en B pero no en A
            if(!array_key_exists($pool_b['name'], $poolsA)){
                $row->setNew();
                $tableB[] = $row;
                
            }else{
                
                $diff = false;
                
                $pool_a = $poolsA[$pool_b['name']];
            
                if(strtoupper($pool_a['api_id']) != strtoupper($pool_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                    $row->marked_to_delete = true;
                }
                
                if($pool_a['name'] != $pool_b['name']){
                    $row->name->state = false;
                    $diff = true;
                }
                
                if($pool_a['ranges'] != $pool_b['ranges']){
                    $row->ranges->state = false;
                    $diff = true;
                }
              
                if( $diff){
                    $tableB[] = $row;
                }
            }
            
        }
        
        $this->updateFile($tableA,'poolsA', 'mikrotik_dhcp_ta');
        $this->updateFile($tableB,'poolsB', 'mikrotik_dhcp_ta');
        
    } 
    
    protected function clearPools(){
        
        $this->updateFile([],'poolsA', 'mikrotik_dhcp_ta');
        $this->updateFile([],'poolsB', 'mikrotik_dhcp_ta');
    } 
    

    
    
    //gateways
    
    public function syncGateways()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $ok = true;
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            $gateways_ids = $this->request->input('json_decode')->gateways_ids;
            $delete_gateways_side_b = $this->request->input('json_decode')->delete_gateways_side_b;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
                
            $fileB = new File(WWW_ROOT . 'sync/mikrotik_dhcp_ta/gatewaysB.json', false, 0644);
            $json = $fileB->read($fileB, 'r');
            $json =  json_decode($json);
            
           foreach($json->data as $row){
                
                if($row->marked_to_delete){
                    
                    if(!$row->other){ //son registro del controlador, que deben eliminarse,   
                    
                        if(!$this->IntegrationRouter->MikrotikDhcpTa->deleteGatewayInController($controller, $row->api_id->value)){
                            $ok = false;
                            break;
                        }
                    	
                    }else if($row->other && $delete_gateways_side_b){ //son registros agenos al controlador, y esta marcado para limpieza
                    	
                    	if(!$this->IntegrationRouter->MikrotikDhcpTa->deleteGatewayInController($controller, $row->api_id->value)){
                            $ok = false;
                            break;
                        }
                    }
                }
            }
                
           foreach($gateways_ids as $gateways_id){

               if(!$this->IntegrationRouter->MikrotikDhcpTa->syncGateway($gateways_id)){
                   $ok = false;
                   break;
               }
           }
            
            $this->diffGateways($controller);

            $this->set('response', $ok);
        }
    }
     
    public function syncGatewayIndi()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $ok = true;
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            $gateways_ids = $this->request->input('json_decode')->gateways_ids;      
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);       
                
           foreach($gateways_ids as $gateways_id){

               if(!$this->IntegrationRouter->MikrotikDhcpTa->syncGateway($gateways_id)){
                   $ok = false;
                   break;
               }
           }
            
            $this->diffGateways($controller);

            $this->set('response', $ok);
        }
    }
  
    public function refreshGateways(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $ok = false;
            
            if($this->IntegrationRouter->MikrotikDhcpTa->getStatus($controller)){
  
                $ok = true;
            }
            
            if($ok){
                $this->diffGateways($controller);
            }
            
            $this->set('data', $ok);
        }
        
    }
 
    public function deleteGatewayInController(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
            $gateway_api_id = $this->request->input('json_decode')->gateway_api_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $success = true;
          
            if(!$this->IntegrationRouter->MikrotikDhcpTa->deleteGatewayInController($controller, $gateway_api_id->value)){
                 $success = false;
            }
            
            $this->diffGateways($controller);
            
            $this->set('data', true);
        }
        
    }
    
    protected function diffGateways($controller){
         
        $this->clearGateways();

        $gatewayA = $this->IntegrationRouter->MikrotikDhcpTa->getGatewaysArray($controller->id);
        
        $gatewayB = $this->IntegrationRouter->MikrotikDhcpTa->getGatewaysInController($controller);
        
        $tableA = [];
        $tableB = [];
        
        foreach ($gatewayA as $gateway_a) {
            
            $row = new RowGateway($gateway_a, 'A');
             
            //existe en A pero no en B
            if(!array_key_exists($gateway_a['address'], $gatewayB)){
                $tableA[] = $row;
            }else{
                
                $diff = false;
                
                $gateway_b = $gatewayB[$gateway_a['address']];
                
                if(strtoupper($gateway_a['api_id']) != strtoupper($gateway_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($gateway_a['address'] != $gateway_b['address']){
                    $row->address->state = false;
                     $diff = true;
                }
                
                if($gateway_a['interface'] != $gateway_b['interface']){
                    $row->interface->state = false;
                     $diff = true;
                }
                
                if($gateway_a['comment'] != $gateway_b['comment']){
                    $row->comment->state = false;
                     $diff = true;
                }
              
                if( $diff){
                    $tableA[] = $row;
                }
                
            }
        }
        
        foreach ($gatewayB as $gateway_b) {
            
            $row = new RowGateway($gateway_b, 'B');
            
            if($gateway_b['other'] == true){
            	$row->setNew();
            	$tableB[] = $row;
            	continue;
            }
            
            //existe en B pero no en A
            if(!array_key_exists($gateway_b['address'], $gatewayA)){
                $row->setNew();
                $tableB[] = $row;
                
            }else{
                
                $diff = false;
                
                $gateway_a = $gatewayA[$gateway_b['address']];
            
                if(strtoupper($gateway_a['api_id']) != strtoupper($gateway_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                    $row->marked_to_delete = true;
                }
                
                if($gateway_a['address'] != $gateway_b['address']){
                    $row->address->state = false;
                    $diff = true;
                }
                
                if($gateway_a['interface'] != $gateway_b['interface']){
                    $row->interface->state = false;
                     $diff = true;
                }
                
                if($gateway_a['comment'] != $gateway_b['comment']){
                    $row->comment->state = false;
                     $diff = true;
                }
              
                if( $diff){
                    $tableB[] = $row;
                }
            }
            
        }
        
        $this->updateFile($tableA,'gatewaysA', 'mikrotik_dhcp_ta');
        $this->updateFile($tableB,'gatewaysB', 'mikrotik_dhcp_ta');
   
    } 
    
    protected function clearGateways(){
        
        $this->updateFile([],'gatewaysA', 'mikrotik_dhcp_ta');
        $this->updateFile([],'gatewaysB', 'mikrotik_dhcp_ta');
    } 
   
   
    
    
    //leases
    
    public function syncLeases()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
      
            $connections_ids = $this->request->input('json_decode')->connections_ids;
            $delete_leases_side_b = $this->request->input('json_decode')->delete_leases_side_b;
            
            $this->loadModel('Connections');
            $success = true;           
       
                
           $fileB = new File(WWW_ROOT . 'sync/mikrotik_dhcp_ta/leasesB.json', false, 0644);
           $json = $fileB->read($fileB, 'r');
           $json =  json_decode($json);

           foreach($json->data as $row){

                if($row->marked_to_delete){

                    if(!$row->other){ //son registro del controlador, que deben eliminarse,   

                        if(!$this->IntegrationRouter->MikrotikDhcpTa->deleteLeaseInController($controller, $row->api_id->value)){
                            $success = false;
                            break;
                       }

                    }else if($row->other && $delete_leases_side_b){ //son registros agenos al controlador, y esta marcado para limpieza

                         if(!$this->IntegrationRouter->MikrotikDhcpTa->deleteLeaseInController($controller, $row->api_id->value)){
                                 $success = false;
                                 break;
                            }
                    }
                }
            }
           
             
            foreach($connections_ids as $connection_id){
                
                $connection = $this->Connections->get($connection_id, ['contain' => ['Controllers', 'Customers']]);
                if(!$this->IntegrationRouter->MikrotikDhcpTa->syncLease($connection)){
                    $success = false;
                    break;
                }
            }
             
            $this->diffLeases($controller);

            $this->set('data', $success);
        }
    }
     
    public function syncLeaseIndi()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
      
            $connections_ids = $this->request->input('json_decode')->connections_ids;       
            
            $this->loadModel('Connections');
            $success = true;   
             
            foreach($connections_ids as $connection_id){
                
                $connection = $this->Connections->get($connection_id, ['contain' => ['Controllers', 'Customers']]);
                if(!$this->IntegrationRouter->MikrotikDhcpTa->syncLease($connection)){
                    $success = false;
                    break;
                }
            }
             
            $this->diffLeases($controller);

            $this->set('data', $success);
        }
    }
  
    public function refreshLeases(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $ok = false;
            
            if($this->IntegrationRouter->MikrotikDhcpTa->getStatus($controller)){
  
                $ok = true;
            }
            
            if($ok){
                $this->diffLeases($controller);
            }
            
            $this->set('data', $ok);
        }
        
    }
  
    public function deleteLeaseInController(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
            $lease_api_id = $this->request->input('json_decode')->lease_api_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $success = true;
            
            // $this->log( $lease_api_id , 'debug');
            
            if(!$this->IntegrationRouter->MikrotikDhcpTa->deleteLeaseInController($controller, $lease_api_id->value)){
                 $success = false;
            }
            
            $this->diffLeases($controller);
            
            $this->set('data', true);
        }
        
    }
    
    protected function diffLeases($controller, $file = true){
         
        if($file){
             $this->clearLeases();
        }

        $leasesA = $this->IntegrationRouter->MikrotikDhcpTa->getLeasesAndConnectionsArray($controller);
        
        $leasesB = $this->IntegrationRouter->MikrotikDhcpTa->getLeasesInController($controller);
        
        $tableA = [];
        $tableB = [];
        
        $count = 0;
        
        foreach ($leasesA as $lease_a) {
            
            $row = new RowLease($lease_a, 'A');
             
            //existe en A pero no en B
            if(!array_key_exists($lease_a['address'], $leasesB)){
                $tableA[] = $row;
                $count++;
            }else{
                
                $diff = false;
                
                $lease_b = $leasesB[$lease_a['address']];
                
                if(strtoupper($lease_a['api_id']) != strtoupper($lease_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($lease_a['address'] != $lease_b['address']){
                    $row->address->state = false;
                     $diff = true;
                }
                
                if($lease_a['mac_address'] != $lease_b['mac_address']){
                    $row->mac_address->state = false;
                    $diff = true;
                }
                
                if($lease_a['server'] != $lease_b['server']){
                    $row->server->state = false;
                    $diff = true;
                }
            
                if($lease_a['comment']  != $lease_b['comment']){
                    $row->comment->state = false;
                    $diff = true;
                }
                
                if($lease_a['address_lists']  != $lease_b['address_lists']){
                    $row->address_lists->state = false;
                    $diff = true;
                }
                
                if($lease_a['lease_time']  != $lease_b['lease_time']){
                    $row->lease_time->state = false;
                    $diff = true;
                }
              
                if( $diff){
                    $tableA[] = $row;
                    $count++;
                }
                
            }
        }
        
        foreach ($leasesB as $lease_b) {
            
            $row = new RowLease($lease_b, 'B');
            
            if($lease_b['other'] == true){
                $row->setNew();
                $tableB[] = $row;
                continue;
                
            }
            
            //existe en B pero no en A
            if(!array_key_exists($lease_b['address'], $leasesA)){
                $row->setNew();
                $tableB[] = $row;
                $count++;
                
            }else{
                
                $diff = false;
            
                $lease_a = $leasesA[$lease_b['address']];
            
                if(strtoupper($lease_a['api_id']) != strtoupper($lease_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($lease_a['address'] != $lease_b['address']){
                    $row->address->state = false;
                    $diff = true;
                }
                
                if($lease_a['mac_address'] != $lease_b['mac_address']){
                    $row->mac_address->state = false;
                    $diff = true;
                }
               
                if($lease_a['server'] != $lease_b['server']){
                    $row->server->state = false;
                    $diff = true;
                }
                
                if($lease_a['comment']  != $lease_b['comment']){
                    $row->comment->state = false;
                    $diff = true;
                }
                
                if($lease_a['address_lists']  != $lease_b['address_lists']){
                    $row->address_lists->state = false;
                    $diff = true;
                }
                
                if($lease_a['lease_time']  != $lease_b['lease_time']){
                    $row->lease_time->state = false;
                    $diff = true;
                }
             
                if( $diff){
                    $tableB[] = $row;
                    $count++;
                }
                
            }
            
        }
        
        if($file){
            
            $this->updateFile($tableA,'leasesA', 'mikrotik_dhcp_ta');
            $this->updateFile($tableB,'leasesB', 'mikrotik_dhcp_ta');
            
        }
        
        return $count; 
        
    } 
    
    protected function clearLeases(){
        
        $this->updateFile([],'leasesA', 'mikrotik_dhcp_ta');
        $this->updateFile([],'leasesB', 'mikrotik_dhcp_ta');
    } 
    
     
    
}







