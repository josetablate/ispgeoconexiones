<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;

use App\Controller\Component\Admin\CashRegister;

/**
 * CashEntities Controller
 *
 * @property \App\Model\Table\CashEntitiesTable $CashEntities
 */
class CashEntitiesController extends AppController
{
     public function isAuthorized($user = null)
     {
        if ($this->request->getParam('action') == 'checkStatusCashEntity') {
            return true;
        }

        if ($this->request->getParam('action') == 'getAll') {
            return true;
        }

        if ($this->request->getParam('action') == 'addTransactionContado') {
            return true;
        }

        if ($this->request->getParam('action') == 'addTransactionOther') {
            return true;
        }

        if ($this->request->getParam('action') == 'confirmTransaction') {
            return true;
        }

        if ($this->request->getParam('action') == 'getCashEntityToatales') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Accountant', [
             'className' => '\App\Controller\Component\Admin\Accountant'
        ]);

        $this->loadComponent('CashRegister', [
             'className' => '\App\Controller\Component\Admin\CashRegister'
        ]);
    }
 
    public function index()
    {}

    public function add()
    {
        $cashEntity = $this->CashEntities->newEntity();
        if ($this->request->is('post')) {

            $parament =  $this->request->getSession()->read('paraments');

            //si la contabilidada esta activada registro el asientop
            if ($this->Accountant->isEnabled()) {
                if (!$parament->accountant->acounts_parent->cashs ) {
                    $this->Flash->warning(__('Debe espeficar la cuenta padre de Cajas.'));
                    return $this->redirect(['action' => 'add']);
                }
            }

            if ($this->CashEntities->find()->where(['name' => $this->request->getData('name'), 'deleted' => false])->first()) {
                $this->Flash->warning(__('Existe una caja con el mismo nombres.'));
                return $this->redirect(['action' => 'add']);
            }

            if ($this->CashEntities->find()->where(['user_id' => $this->request->getData('user_id'), 'deleted' => false])->first()) {
                $this->Flash->warning(__('El usuario ya tiene una caja asignada.'));
                return $this->redirect(['action' => 'add']);
            }

            $request_data = $this->request->getData();
            $request_data['account_code'] = NULL;

            //si la contabilidada esta activada registro el asientop
            if ($this->Accountant->isEnabled()) {
                $account = $this->Accountant->createAccount([
                    'account_parent' => $parament->accountant->acounts_parent->cashs,
                    'name' =>  $this->request->getData('name'),
                    'redirect' => ['action' => 'add']
                ]);
                $request_data['account_code'] = $account->code;
            }

            $cashEntity = $this->CashEntities->patchEntity($cashEntity, $request_data);

            if ($this->CashEntities->save($cashEntity)) {

                $this->Flash->success(__('Caja agregada.'));
                return $this->redirect(['action' => 'add']);
            } else {
                //si la contabilidada esta activada registro el asientop
                if ($this->Accountant->isEnabled()) {
                    $this->Accountant->deleteAccount($account);
                    $this->Flash->error(__('Error al agregar la caja.'));
                }
            }
        }

        $parament = $this->request->getSession()->read('paraments');

        $users =  $this->CashEntities->Users->find('list')
            ->where(['id NOT IN' => $parament->system->users, 'deleted' => false, 'enabled' => true]);

        $this->set(compact('cashEntity', 'users'));
        $this->set('_serialize', ['cashEntity']);
    }

    public function edit($id = null)
    {
        $parament = $this->request->getSession()->read('paraments');

        $cashEntity = $this->CashEntities->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {

            if ($this->CashEntities->find()->where(['name' => $this->request->getData('name'), 'deleted' => false, 'id !=' => $id])->first()) {
                $this->Flash->warning(__('Existe una caja con el mismo nombres.'));
                return $this->redirect(['action' => 'edit', $id]);
            }

            if ($this->CashEntities->find()->where(['user_id' => $this->request->getData('user_id'), 'deleted' => false, 'id !=' => $id])->first()) {
                $this->Flash->warning(__('El usuario ya tiene una caja asignada.'));
                return $this->redirect(['action' => 'edit', $id]);
            }

            $cashEntity = $this->CashEntities->patchEntity($cashEntity, $this->request->getData());
            if ($this->CashEntities->save($cashEntity)) {
                //si la contabilidada esta activada registro el asientop
                if ($this->Accountant->isEnabled()) {

                    $this->loadModel('Accounts');

                    $account = $this->Accounts->find()
                        ->where([
                            'code' =>  $cashEntity->account_code,
                        ])->first();

                    $account->name = $cashEntity->name;

                    if (!$this->Accounts->save($account)) {
                        $this->Flash->error(__('No se pudo actualizar la cuenta.'));
                    }
                }

                $this->Flash->success(__('Datos actualizados.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Error al actualizar los datos.'));
            }
        }

        $parament = $this->request->getSession()->read('paraments');

        $users = $this->CashEntities->Users->find('list')
            ->where(['id NOT IN' => $parament->system->users]);

        $this->set(compact('cashEntity', 'users'));
        $this->set('_serialize', ['cashEntity']);
    }

    public function view($id = null)
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');
        $payment_methods = [
            '' => 'Seleccione'
        ];
        foreach ($payment_getway->methods as $payment_method) {
            $payment_methods[$payment_method->name] = $payment_method->name;
        }

        $cash_entity = $this->CashEntities->find()->contain(['Users'])->where(['CashEntities.id' => $id, 'CashEntities.deleted' => false])->first();

        if (!$cash_entity) {
            $this->Flash->warning(__('La caja no existe.'));
            return $this->redirect(['action' => 'index']);
        }

        if (!$cash_entity->enabled) {

            $this->Flash->warning(__('La caja está deshabilitada.'));
            return $this->redirect(['action' => 'index']);
        }

        $this->loadModel('IntOutCashsEntities');

        $apertures = $this->IntOutCashsEntities->find()
            ->order(['id' => 'desc'])
            ->where(['cash_entity_id' => $cash_entity->id, 'type' => 'APERTURA']);

        $lastApertura = $this->IntOutCashsEntities->find()
            ->order(['id' => 'desc'])
            ->where(['cash_entity_id' => $cash_entity->id, 'type' => 'APERTURA' ])
            ->first();

        $movements = null;
        $lastApertureNumber = sprintf("%'.03d", $cash_entity->id)  . '-' . sprintf("%'.05d", 0); 

        if ($lastApertura) {
            $lastApertureNumber = $lastApertura->number_part; 
        }

        $this->set('apertures', $apertures);
        $this->set('lastApertureNumber', $lastApertureNumber);
        $this->set('cash_entity', $cash_entity);
        $this->set('payment_methods', $payment_methods);
        $this->set('_serialize', ['cash_entity', 'movements']);
    }

    public function delete()
    {
        $parament = $this->request->getSession()->read('paraments');

        $this->request->allowMethod(['post', 'delete']);
        $cashEntity = $this->CashEntities->get($_POST['id']);

        if ($cashEntity->cash > 0) {
            $this->Flash->warning(__('La caja debe estar vacía para ser eliminada.'));
            return $this->redirect(['action' => 'index']);
        }

        $cashEntity->deleted = true;

        if ($this->CashEntities->save($cashEntity)) {
            $this->Flash->success(__('Caja eliminada.'));
        } else {
            $this->Flash->error(__('Error al eliminar la caja.'));
        }

        return  $this->redirect($this->referer());
    }

    public function pointSaleView()
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');
        $payment_methods = [
            '' => 'Seleccione'
        ];
        foreach ($payment_getway->methods as $payment_method) {
            $payment_methods[$payment_method->name] = $payment_method->name;
        }

        $user_id =  $this->Auth->user()['id'];
        $cash_entity = $this->CashEntities->find()->contain(['Users'])->where(['user_id' => $user_id, 'CashEntities.deleted' => false])->first();

        if (!$cash_entity) {
            $this->Flash->warning(__('No tiene asiganado una caja.'));
            return $this->redirect($this->referer());
        }

        if (!$cash_entity->enabled) {
            
            $this->Flash->warning(__('Su caja está deshabilitada.'));
            return $this->redirect($this->referer());
        }

        $this->loadModel('IntOutCashsEntities');

        $lastApertura = $this->IntOutCashsEntities->find()
            ->order(['IntOutCashsEntities.id' => 'desc'])
            ->where(['cash_entity_id' => $cash_entity->id, 'type' => 'APERTURA' ])
            ->first();

        $lastApertureNumber = sprintf("%'.03d", $cash_entity->id)  . '-' . sprintf("%'.05d", 0); 

        if ($lastApertura) {
            $lastApertureNumber = $lastApertura->number_part; 
        }
 
        $movement_form =  $this->IntOutCashsEntities->newEntity();
        $movement_form->user_id = $this->Auth->user()['id'];
        $movement_form->cash_entity_id = $cash_entity->id;

        $this->loadModel('Users');
        $users_destinations =  $this->Users->find()
            ->innerJoinWith('CashEntities')
            ->where([
                'Users.deleted' => false, 'Users.enabled' => true,
                'CashEntities.deleted' => false, 'CashEntities.enabled' => true,
                'Users.id !=' => $this->Auth->user()['id']
            ]);

        $users_destinations_list = [];
        $users_destinations_list[''] = 'Seleccione';
        foreach ($users_destinations as $user) {
            $users_destinations_list[$user->id] = $user->name;
        }

        $this->set('users_destinations_list', $users_destinations_list);
        $this->set('lastApertureNumber', $lastApertureNumber);
        $this->set('cash_entity', $cash_entity);
        $this->set('movement_form', $movement_form);
        $this->set('payment_methods', $payment_methods);
        $this->set('_serialize', ['cash_entity']);
    }

    public function openCash()
    {
        $user_id = $this->Auth->user()['id'];
        $cash_entity = $this->CashEntities->find()->where(['user_id' => $user_id, 'deleted' => false])->first();

        $data =  new Data();
        $data->cash_entity = $cash_entity;

        if ($this->CashRegister->register($data, CashRegister::TYPE_OPEN)) {

            $cash_entity->open = true;

            if (!$this->CashEntities->save($cash_entity)) {

                $this->Flash->error(__('Error, no se puedo abrir la caja.'));
                return $this->redirect(['action' => 'pointSaleView']);
            }

            $this->Flash->success(__('La Apertura de la caja se realizó correctamente.'));
            return $this->redirect(['action' => 'pointSaleView']);
        }
    }

    public function closeCash()
    {
        $user_id = $this->Auth->user()['id'];
        $cash_entity = $this->CashEntities->find()->where(['user_id' => $user_id, 'deleted' => false])->first();

        $data =  new Data();
        $data->cash_entity = $cash_entity;

        if ($this->CashRegister->register($data, CashRegister::TYPE_CLOSE)) {

            $cash_entity->open = false;

            if (!$this->CashEntities->save($cash_entity)) {

                $this->Flash->error(__('Error, no se puedo cerrar la caja.'));
                return $this->redirect(['action' => 'pointSaleView']);
            }

            $this->Flash->success(__('El cierre de caja se realizó correctamente.'));
            return $this->redirect(['action' => 'pointSaleView']);
        }
    }

    private function checkStatusCashEntity()
    {
        $user_id =  $this->Auth->user()['id'];
        $cash_entity = $this->CashEntities->find()->where(['user_id' => $user_id, 'deleted' => false])->first();

        if (!$cash_entity) {
            $this->Flash->warning(__('No tiene asiganado una caja.'));
            return false;
        }

        if (!$cash_entity->enabled) {
            $this->Flash->warning(__('Su caja está deshabilitada.'));
            return false;
        }

        if (!$cash_entity->open) {
            $this->Flash->open_cash(__('Su caja está cerrada.'));
            return false;
        }

        return true;
    }

    public function getAll()
    {
        $cashEntities = $this->CashEntities->find()
            ->contain(['Users'])
            ->Where(['user_id !=' => 0, 'CashEntities.deleted' => false])
            ->order(['CashEntities.id' => 'DESC']);
     
        $this->set('cashentities', $cashEntities);
    }

    public function addTransactionContado()
    {
        $action = 'Transferencia entre cajas (contado)';
        $detail = '';
        $this->registerActivity($action, $detail, NULL, TRUE);

        $this->loadModel('CashEntitiesTransactions');
        $cashEntitiesTransaction = $this->CashEntitiesTransactions->newEntity();

        if ($this->request->is('post')) {

            $cashEntitiesTransaction = $this->CashEntitiesTransactions->patchEntity($cashEntitiesTransaction, $this->request->getData());

            $user_id = $this->Auth->user()['id'];
            $cash_entity = $this->CashEntities
                ->find()
                ->where([
                    'user_id' => $user_id,
                    'deleted' => FALSE
                ])->first();

            $cash_entity_destiny = $this->CashEntities
                ->find()
                ->where([
                    'user_id' => $cashEntitiesTransaction->user_destination_id,
                    'deleted' => FALSE
                ])->first();

            if (!$cash_entity->open) {
                $this->Flash->warning(__('La caja debe estar abierta para realizar transacciones.'));
                return $this->redirect(['action' => 'pointSaleView']);
            }

            if ($cash_entity->contado < $cashEntitiesTransaction->value) {
                $this->Flash->warning(__('Saldo insuficiente para realizar la transacción.'));
                return $this->redirect(['action' => 'pointSaleView']);
            }

            $data = new \stdClass;
            $data->cash_entity = $cash_entity;
            $data->cash_entity_destiny = $cash_entity_destiny;

            $cashEntitiesTransaction->created = Time::now();
            $cashEntitiesTransaction->user_origin_id = $user_id;
            $cashEntitiesTransaction->type = 'TRANSFERENCIA';
            $cashEntitiesTransaction->is_table = 1;

            $data->created =  Time::now();
            $data->type = $cashEntitiesTransaction->type;
            $data->concept = $cashEntitiesTransaction->concept;
            $data->in_value = 0;
            $data->out_value = abs($cashEntitiesTransaction->value);
            $data->data = null;
            $data->seating_number = null;
            $data->is_table = 1;
            $data->cash_entity->contado -= $data->out_value;

            if (!$this->CashRegister->register($data, CashRegister::TYPE_OUT)) {
                $this->Flash->error(__('Error al intentar registrar el egreso de caja.'));
                return $this->redirect(['action' => 'pointSaleView']);
            }

            if (!$this->CashEntities->save($cash_entity)) {
                $this->Flash->error(__('Error al intentar actualizar la caja.'));
                return $this->redirect(['action' => 'pointSaleView']);
            }

            if ($this->CashEntitiesTransactions->save($cashEntitiesTransaction)) {

                $action = 'Parámetros de transferencia';
                $detail = 'Caja origen: ' . $data->cash_entity->name . PHP_EOL;
                $detail .= 'Caja destino: ' . $data->cash_entity_destiny->name . PHP_EOL;
                $detail .= 'Valor: ' . number_format($data->out_value, 2, ',', '.') . PHP_EOL;
                $detail .= 'Concepto: ' . $data->concept . PHP_EOL;
                $detail .= 'Tipo: ' . $data->type . PHP_EOL;
                $detail .= 'Medio: ' . (($data->is_table == 1) ? 'Contado' : 'Otros' ) . PHP_EOL;

                $this->registerActivity($action, $detail, NULL);

                $this->Flash->success(__('Transacción registrada correctamente.'));
                return $this->redirect(['action' => 'pointSaleView']);
            }

            $this->Flash->error(__('Error al intentar registrar la transacción.'));
            return $this->redirect(['action' => 'pointSaleView']);
        }
    }

    public function addTransactionOther()
    {
        $action = 'Transferencia entre cajas (otros medios)';
        $detail = '';
        $this->registerActivity($action, $detail, NULL, TRUE);

        $this->loadModel('CashEntitiesTransactions');
        $cashEntitiesTransaction = $this->CashEntitiesTransactions->newEntity();

        if ($this->request->is('post')) {

            $cashEntitiesTransaction = $this->CashEntitiesTransactions->patchEntity($cashEntitiesTransaction, $this->request->getData());

            $user_id = $this->Auth->user()['id'];
            $cash_entity = $this->CashEntities
                ->find()
                ->where([
                    'user_id' => $user_id,
                    'deleted' => FALSE
                ])->first();

            $cash_entity_destiny = $this->CashEntities
                ->find()
                ->where([
                    'user_id' => $cashEntitiesTransaction->user_destination_id,
                    'deleted' => FALSE
                ])->first();

            if (!$cash_entity->open) {
                $this->Flash->warning(__('La caja debe estar abierta para realizar transacciones.'));
                return $this->redirect(['action' => 'pointSaleView']);
            }

            if ($cash_entity->cash_other < $cashEntitiesTransaction->value) {
                $this->Flash->warning(__('Saldo insuficiente para realizar la transacción.'));
                return $this->redirect(['action' => 'pointSaleView']);
            }

            $data = new \stdClass;
            $data->cash_entity = $cash_entity;
            $data->cash_entity_destiny = $cash_entity_destiny;

            $cashEntitiesTransaction->created = Time::now();
            $cashEntitiesTransaction->user_origin_id = $user_id;
            $cashEntitiesTransaction->type = 'TRANSFERENCIA';
            $cashEntitiesTransaction->is_table = 2;

            $data->created = Time::now();
            $data->type = $cashEntitiesTransaction->type;
            $data->concept = $cashEntitiesTransaction->concept;
            $data->in_value = 0;
            $data->out_value = abs($cashEntitiesTransaction->value);
            $data->data = null;
            $data->seating_number = null;
            $data->is_table = 2;
            $data->cash_entity->cash_other -= $data->out_value;

            if (!$this->CashRegister->register($data, CashRegister::TYPE_OUT)) {
                $this->Flash->error(__('Error al intentar registrar el egreso de caja.'));
                return $this->redirect(['action' => 'pointSaleView']);
            }

            if (!$this->CashEntities->save($cash_entity)) {
                $this->Flash->error(__('Error al intentar actualizar la caja.'));
                return $this->redirect(['action' => 'pointSaleView']);
            }

            if ($this->CashEntitiesTransactions->save($cashEntitiesTransaction)) {

                $action = 'Parámetros de transferencia';
                $detail = 'Caja origen: ' . $data->cash_entity->name . PHP_EOL;
                $detail .= 'Caja destino: ' . $data->cash_entity_destiny->name . PHP_EOL;
                $detail .= 'Valor: ' . number_format($data->out_value, 2, ',', '.') . PHP_EOL;
                $detail .= 'Concepto: ' . $data->concept . PHP_EOL;
                $detail .= 'Tipo: ' . $data->type . PHP_EOL;
                $detail .= 'Medio: ' . (($data->is_table == 1) ? 'Contado' : 'Otros' ) . PHP_EOL;

                $this->registerActivity($action, $detail, NULL);

                $this->Flash->success(__('Transacción registrada correctamente.'));
                return $this->redirect(['action' => 'pointSaleView']);
            }

            $this->Flash->error(__('Error al intentar registrar la transacción.'));
            return $this->redirect(['action' => 'pointSaleView']);
        }
    }

    public function confirmTransaction()
    {
        if ($this->request->is('ajax')) {

            $action = 'Confirmación de transferencia';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $response = new \stdClass;
            $response->error = false;
            $response->msg = '';

            $data = $this->request->input('json_decode');

            $this->loadModel('CashEntitiesTransactions');

            $cashEntitiesTransaction = $this->CashEntitiesTransactions->get($data->transaction_id);

            $user_id = $this->Auth->user()['id'];
            $cash_entity = $this->CashEntities
                ->find()
                ->where([
                    'user_id' => $cashEntitiesTransaction->user_destination_id,
                    'deleted' => FALSE
                ])->first();

            $cash_entity_origen = $this->CashEntities
                ->find()
                ->where([
                    'user_id' => $cashEntitiesTransaction->user_origin_id,
                    'deleted' => FALSE
                ])->first();

            if ($cash_entity->open) {

                $dataCashRegister = new \stdClass;
                $dataCashRegister->cash_entity = $cash_entity;
                $dataCashRegister->created =  Time::now();
                $dataCashRegister->type = $cashEntitiesTransaction->type;
                $dataCashRegister->concept = $cashEntitiesTransaction->concept;
                $dataCashRegister->in_value = abs($cashEntitiesTransaction->value);
                $dataCashRegister->out_value = 0;
                $dataCashRegister->data = null;
                $dataCashRegister->seating_number = null;
                $dataCashRegister->payment_method_id = null;

                $dataCashRegister->is_table = $cashEntitiesTransaction->is_table;

                if ($dataCashRegister->is_table == 1) {
                    $dataCashRegister->cash_entity->contado += $dataCashRegister->in_value;
                } else {
                    $dataCashRegister->cash_entity->cash_other += $dataCashRegister->in_value;
                }

                if ($this->CashRegister->register($dataCashRegister, CashRegister::TYPE_IN)) {

                    if ($this->CashEntities->save($cash_entity)) {

                        $action = 'Parámetros de transferencia aceptada';
                        $detail = 'Caja origen: ' . $cash_entity_origen->name . PHP_EOL;
                        $detail .= 'Caja destino: ' . $cash_entity->name . PHP_EOL;
                        $detail .= 'Valor: ' . number_format($dataCashRegister->in_value, 2, ',', '.') . PHP_EOL;
                        $detail .= 'Concepto: ' . $dataCashRegister->concept . PHP_EOL;
                        $detail .= 'Tipo: ' . $dataCashRegister->type . PHP_EOL;
                        $detail .= 'Medio: ' . (($dataCashRegister->is_table == 1) ? 'Contado' : 'Otros' ) . PHP_EOL;
                        $this->registerActivity($action, $detail, NULL);

                        $cashEntitiesTransaction->acepted = Time::now();

                        if ($this->CashEntitiesTransactions->save($cashEntitiesTransaction)) {

                               //si la contabilidada esta activada registro el asiento
                                if ($this->Accountant->isEnabled()) {

                                    $seating = $this->Accountant->addSeating([
                                        'concept'       => 'Transferencia entre cajas',
                                        'comments'      => '',
                                        'seating'       => NULL,
                                        'account_debe'  => $cash_entity->account_code,
                                        'account_haber' => $cash_entity_origen->account_code,
                                        'value'         => abs($cashEntitiesTransaction->value),
                                        'user_id'       => $cash_entity->user_id,
                                        'created'       => Time::now()
                                    ]);

                                    if ($seating) {
                                         $cashEntitiesTransaction->seating_number = $seating->number;
                                         $this->CashEntitiesTransactions->save($cashEntitiesTransaction);
                                    }
                                }

                            $response->error = false;
                            $response->msg .= 'La transferencia se confirmo correctamente.';
                        } else {
                            $response->error = true;
                            $response->msg .= 'No se pudo confirmar la transacción.';
                        }
                    } else {
                        $response->error = true;
                        $response->msg .= 'No se pudo actualizar el monto de la caja.';
                    }
                } else {
                    $response->error = true;
                    $response->msg .= 'No se pudo registrar el ingreso a caja.';
                }
            } else {
                $response->error = true;
                $response->msg .= 'Su caja está cerrada. Para recibir transferencias debe abrir su caja.';
            }

            $this->set('response', $response);
        }
    }

    public function getCashEntityToatales()
    {
        if ($this->request->is('Ajax')) { //Ajax Detection

            $cash_entity_id = $this->request->input('json_decode')->cash_entity_id;

            $this->loadModel('CashEntities');
            $cash_entity = $this->CashEntities->get($cash_entity_id);

            $this->set('cash_entity', $cash_entity);
        }
    }
}

class Data {}
