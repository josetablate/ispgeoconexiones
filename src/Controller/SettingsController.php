<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Event\Event;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Core\Configure;

class SettingsController extends AppController
{
    public function initialize()
    {
        parent::initialize();

         $this->loadComponent('FiscoAfip', [
             'className' => '\App\Controller\Component\Admin\FiscoAfip'
        ]);
    }

    public function isAuthorized($user = null) 
    {
        if ($this->request->getParam('action') == 'getBusiness') {
            return true;
        }

        if ($this->request->getParam('action') == 'downloadPaymentGetway') {
            return true;
        }

        if ($this->request->getParam('action') == 'download') {
            return true;
        }

        if ($this->request->getParam('action') == 'downloadPortal') {
            return true;
        }

        if ($this->request->getParam('action') == 'setConfigDuedateCustomers') {
            return true;
        }

        if ($this->request->getParam('action') == 'backup') {
            return true;
        }

        if ($this->request->getParam('action') == 'validateIdent') {
            return true;
        }

        if ($this->request->getParam('action') == 'setVisibilityChat') {
            return true;
        }

        if ($this->request->getParam('action') == 'enableBusiness') {
            return true;
        }

        if ($this->request->getParam('action') == 'disableBusiness') {
            return true;
        }

        if ($this->request->getParam('action') == 'forceDebtMonthControl') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function testLoginAFIP()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {

            $paraments = $this->request->getSession()->read('paraments');

            $business_selected = NULL;

            foreach ($paraments->invoicing->business as $b) {
                if ($b->id == $this->request->getData('id')) {
                    $business_selected = $b;
                }
            }

            if ($business_selected) {

                if ($this->FiscoAfip->testLoginAFIP($business_selected)) {
                    $this->Flash->success(__('Login OK.'));
                } else {
                    $this->Flash->error(__('Login error.'));
                }
            }
            return $this->redirect(['action' => 'index']);
        }
    }

    public function index($tab = 'bills')
    {
        $paraments = $this->request->getSession()->read('paraments');
        $payment_getway = $this->request->getSession()->read('payment_getway');
        $portal = $this->request->getSession()->read('portal');
        $session_timeout = Configure::read('Session')['timeout'];
        $debug_mode = Configure::read('debug');

        $business = [];
        $emails = [];
        $emails[''] = 'Seleccione Cuenta de Correo';
        foreach ($paraments->mass_emails->emails as $email) {
            if ($email->enabled) {
                $emails[$email->id] = $email->contact;
            }
        }

        $providers[''] = 'Seleccionar Proveedor';
        foreach ($paraments->emails_accounts->providers as $provider) {
            if ($provider->enabled) {
                $providers[$provider->name] = $provider->name;
            }
        }

        $this->loadModel('MassEmailsTemplates');
        $mass_emails_templates = $this->MassEmailsTemplates
            ->find()
            ->where([
                'enabled' => TRUE
            ]);

        $emails_emplates[''] = 'Seleccione Plantilla';
        foreach ($mass_emails_templates as $mass_email_template) {
            $emails_emplates[$mass_email_template->id] = $mass_email_template->name;
        }

        $this->loadModel('Countries');
        $countries = $this->Countries->find()->contain(['Provinces.Cities']);

        $this->loadModel('Provinces');
        $provinces_billing_setting = $this->Provinces->find('list')->toArray();

        //path: prueba local para ello crear la carpeta crons-sites y agregar los archivos de crons
        //$file = new File('C:' . DIRECTORY_SEPARATOR . 'laragon' . DIRECTORY_SEPARATOR . 'www' . DIRECTORY_SEPARATOR . 'crons-sites' . DIRECTORY_SEPARATOR . 'androsnet', false, 0644);
        $subdomain = $paraments->gral_config->enterprise_name;
        $file = new File(DIRECTORY_SEPARATOR . 'var' . DIRECTORY_SEPARATOR . 'www' . DIRECTORY_SEPARATOR . 'html' . DIRECTORY_SEPARATOR . 'crons-sites' . DIRECTORY_SEPARATOR . $subdomain, false, 0644);
        $cron = $file->read(true, 'r');
        $cron_posta = [];

        if ($cron) {

            $cron = preg_split('/\n|\r\n?/', $cron);

            foreach ($cron as $job) {

                $job_posta = new \stdClass;
                $job_posta->enabled = !(strpos($job, '#') !== false);
                $name_aux = trim(explode('bin/cake', $job)[1]);

                $time = trim(explode('www-data', $job)[0]);
                $time = str_replace("#", "", $time);

                $add = true;

                switch ($name_aux) {

                    case 'Validation':
                        $job_posta->id = "Validation";
                        $job_posta->name = "Validación";
                        $job_posta->description = "Control con el Brainminator. El Sistema envia información al Brainminator.";
                        $job_posta->next_execution = $time;
                        $job_posta->job = $job;
                        break;
                    case 'DiffControllers':
                        $job_posta->id = "DiffControllers";
                        $job_posta->name = "Diferencia en los controladores";
                        $job_posta->description = "Controla si hay diferencia de datos entre los controladores del Sistema y el Router.";
                        $job_posta->next_execution = $time;
                        $job_posta->job = $job;
                        break;
                    case 'BackupDropbox':
                        $job_posta->id = "BackupDropbox";
                        $job_posta->name = "Backup";
                        $job_posta->description = "Backup de DropBox.";
                        $job_posta->next_execution = $time;
                        $job_posta->job = $job;
                        break;
                    case 'AutomaticConnectionBlocking':
                        $job_posta->id = "AutomaticConnectionBlocking";
                        $job_posta->name = "Bloqueo de Conexiones Automático";
                        $job_posta->description = "Ejecución de bloqueo automático de conexiones.";
                        $job_posta->next_execution = $time;
                        $job_posta->job = $job;
                        break;
                    case 'DebtMonthControl':
                        $job_posta->id = "DebtMonthControl";
                        $job_posta->name = "Control de Deudas Mes";
                        $job_posta->description = "Recalculo de los saldos de mes de los clientes.";
                        $job_posta->next_execution = $time;
                        $job_posta->job = $job;
                        break;
                    case 'CobroDigitalControl':
                        $job_posta->id = "CobroDigitalControl";
                        $job_posta->name = "Control de pagos de Cobro Digital";
                        $job_posta->description = "Comunicación hacia Cobro Digital para la bajada de pagos.";
                        $job_posta->next_execution = $time;
                        $job_posta->job = $job;
                        break;
                    case 'MercadoPagoControl':
                        $job_posta->id = "MercadoPagoControl";
                        $job_posta->name = "Control de pagos de Mercado Pago";
                        $job_posta->description = "Comunicación hacia Mercado Pago para la bajada de pagos.";
                        $job_posta->next_execution = $time;
                        $job_posta->job = $job;
                        break;
                    case 'CuentaDigitalControl':
                        $job_posta->id = "CuentaDigitalControl";
                        $job_posta->name = "Control de pagos de Cuenta Digital";
                        $job_posta->description = "Comunicación hacia Cuenta Digital para la bajada de pagos.";
                        $job_posta->next_execution = $time;
                        $job_posta->job = $job;
                        break;
                    case 'PaymentCommitmentControl':
                        $job_posta->id = "PaymentCommitmentControl";
                        $job_posta->name = "Control de Compromiso de Pago";
                        $job_posta->description = "Verifica los compromiso de pagos vencidos en el día y bloquea a los mismos si no cumplen con la regla de corte definidas en la configuración de Corte Automático de Conexiones.";
                        $job_posta->next_execution = $time;
                        $job_posta->job = $job;
                        break;
                    default:
                        $add = false;
                }

                if ($add) {
                    array_push($cron_posta, $job_posta);
                }
            }
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            if ($this->request->getData('form') == 'bills') {

                $action = 'Configuración pestaña: Facturación';
                $detail = "";
                $this->registerActivity($action, $detail, NULL, TRUE);

                $afip_codes = $this->request->getSession()->read('afip_codes');

                $paraments->customer->doc_type = $this->request->getData('customer.doc_type');
                $paraments->customer->responsible = $this->request->getData('customer.responsible');
                $paraments->customer->denomination = intval($this->request->getData('customer.denomination'));
                $paraments->customer->cond_venta = $afip_codes['cond_venta'][intval($this->request->getData('customer.cond_venta'))];

                $paraments->invoicing->is_presupuesto = $this->request->getData('invoicing.is_presupuesto') ? true : false;
                $paraments->invoicing->print_duplicate_receipt = $this->request->getData('invoicing.print_duplicate_receipt') ? true : false;
                $paraments->invoicing->auto_print = $this->request->getData('invoicing.auto_print') ?  true : false;
                $paraments->invoicing->resume_receipt = $this->request->getData('invoicing.resume_receipt') ? true : false;
                $paraments->invoicing->business_billing_default = $this->request->getData('invoicing.business_billing_default');
                $paraments->invoicing->observations_receipt = $this->request->getData('invoicing.observations_receipt');
                $paraments->invoicing->account_summary_complete = $this->request->getData('invoicing.account_summary_complete') ? true : false;
                $paraments->invoicing->email_emplate_receipt = intval($this->request->getData('invoicing.email_emplate_receipt'));
                $paraments->invoicing->email_emplate_invoice = intval($this->request->getData('invoicing.email_emplate_invoice'));

                $paraments->invoicing->add_cobrodigital_account_summary = $this->request->getData('invoicing.add_cobrodigital_account_summary') ? true : false;
                $paraments->invoicing->add_payu_account_summary = $this->request->getData('invoicing.add_payu_account_summary') ? true : false;
                $paraments->invoicing->add_cuentadigital_account_summary = $this->request->getData('invoicing.add_cuentadigital_account_summary') ? true : false;

                $paraments->invoicing->connection_auto_selected_cobranza = $this->request->getData('invoicing.connection_auto_selected_cobranza') ? true : false;
                $paraments->invoicing->show_comprobante_from_facturador_manual_after_generation = $this->request->getData('invoicing.show_comprobante_from_facturador_manual_after_generation') ? true : false;
                $paraments->invoicing->generate_ticket = $this->request->getData('invoicing.generate_ticket') ? true : false;
                $paraments->invoicing->use_cash_for_payment_anulate = $this->request->getData('invoicing.use_cash_for_payment_anulate') ? true : false;

                $paraments->accountant->type = $this->request->getData('accountant.type');
                $paraments->accountant->daydue = intval($this->request->getData('accountant.daydue'));
                $paraments->accountant->service_debt = $this->request->getData('accountant.service_debt') ? true : false;
                $paraments->accountant->service_debt_change = $this->request->getData('accountant.service_debt_change') ? true : false;

                $paraments->accountant->dues = $this->request->getData('accountant.dues', ["0","0","0","0","0","0"]);

                $action = 'Configuración por defecto guardada' . PHP_EOL;
                $detail .= 'Tipo de documento: ' . $paraments->customer->doc_type . PHP_EOL;
                $detail .= 'Tipo de cliente: ' . $paraments->customer->responsible . PHP_EOL;
                $detail .= 'Denominación: ' . $paraments->customer->denomination . PHP_EOL;
                $detail .= 'Condición de venta: ' . $paraments->customer->cond_venta . PHP_EOL;

                $detail .= 'Presupuesto: ' . ($paraments->invoicing->is_presupuesto ? 'Si' : 'No') . PHP_EOL;
                $detail .= 'Recibo por duplicado: ' . ($paraments->invoicing->print_duplicate_receipt ? 'Si' : 'No') . PHP_EOL;
                $detail .= 'Anular Recibo con Caja: ' . ($paraments->invoicing->use_cash_for_payment_anulate ? 'Si' : 'No') . PHP_EOL;
                $detail .= 'Auto imprimir recibo: ' . ($paraments->invoicing->resume_receipt ? 'Si' : 'No') . PHP_EOL;
                $detail .= 'Mostrar resumen de cuenta en recibo: ' . ($paraments->invoicing->print_duplicate_receipt ? 'Si' : 'No') . PHP_EOL;

                $business_name = "";
                foreach ($paraments->invoicing->business as $b) {
                    if ($b == $paraments->invoicing->business_billing_default) {
                        $business_name = $b->name . ' (' . $b->address . ')';
                    }
                }

                $detail .= 'Facturar con Empresa p/facturar por defecto a los clientes: ' . $business_name . PHP_EOL;
                $detail .= 'Observación en Recibo: ' . ($paraments->invoicing->observations_receipt ? 'Si' : 'No') . PHP_EOL;
                $detail .= 'Mostrar resumen de cuenta completo: ' . ($paraments->invoicing->account_summary_complete ? 'Si' : 'No') . PHP_EOL;

                $receipt_email_template = "";
                $invoice_email_template = "";
                foreach ($mass_emails_templates as $mass_email_template) {
                    if ($mass_email_template->id == $paraments->invoicing->email_emplate_receipt) {
                        $receipt_email_template = $mass_email_template->name;
                    }
                    if ($mass_email_template->id == $paraments->invoicing->email_emplate_invoice) {
                        $invoice_email_template = $mass_email_template->name;
                    }
                }

                $detail .= 'Enviar recibo (cobranza): ' . $receipt_email_template . PHP_EOL;
                $detail .= 'Enviar por Correo luego de la facturación masiva: ' . $invoice_email_template . PHP_EOL;

                $detail .= 'Agregar cód. barra Cobro Digital (PDF): ' . ($paraments->invoicing->add_cobrodigital_account_summary ? 'Si' : 'No') . PHP_EOL;
                $detail .= 'Agregar cód. barra PayU (PDF): ' . ($paraments->invoicing->add_payu_account_summary ? 'Si' : 'No') . PHP_EOL;
                $detail .= 'Agregar cód. barra Cuenta Digital (PDF): ' . ($paraments->invoicing->add_cuentadigital_account_summary ? 'Si' : 'No') . PHP_EOL;

                if ($paraments->gral_config->billing_for_service) {
                    $detail .= 'Auto selección de conexión (Cobranza): ' . ($paraments->invoicing->connection_auto_selected_cobranza ? 'Si' : 'No') . PHP_EOL;
                }

                $detail .= 'Mostrar comprobante luego de su generación: ' . ($paraments->invoicing->show_comprobante_from_facturador_manual_after_generation ? 'Si' : 'No') . PHP_EOL;
                $detail .= 'Agregar Ticket (Agregar Cliente): ' . ($paraments->invoicing->generate_ticket ? 'Si' : 'No') . PHP_EOL;

                $accountant_types = $afip_codes['accountant_types'];

                $detail .= 'Tipo de vencimiento: ' . ($accountant_types[$paraments->accountant->type] ? 'Si' : 'No') . PHP_EOL;
                $detail .= 'Día de vencimiento: ' . $paraments->accountant->daydue . PHP_EOL;
                $detail .= 'Deuda por servicio nuevo (proporcional): ' . ($paraments->accountant->service_debt ? 'Si' : 'No') . PHP_EOL;

                $detail .= 'Interés Cuotas: ' . PHP_EOL;

                foreach ($paraments->accountant->dues as $key => $value) {
                    $idx = $key + 1;
                    $detail .= $key . ': ' . $value . PHP_EOL;
                }

                $this->registerActivity($action, $detail, NULL);
            }

            if ($this->request->getData('form') == 'duedates') {

                $action = 'Configuración Bloqueo Automático Conexiones';
                $detail = "";
                $this->registerActivity($action, $detail, NULL, TRUE);

                if ($this->request->getData('automatic_connection_blocking.enabled')) {

                    // bloqueo automatico de conexiones 

                    $paraments->automatic_connection_blocking->enabled = $this->request->getData('automatic_connection_blocking.enabled') ? true : false;
                    $paraments->automatic_connection_blocking->day = intval($this->request->getData('automatic_connection_blocking.day'));
                    $paraments->automatic_connection_blocking->hours_execution = $this->request->getData('automatic_connection_blocking.hours_execution');
                    $paraments->automatic_connection_blocking->invoice_no_paid = intval($this->request->getData('automatic_connection_blocking.invoice_no_paid'));
                    $paraments->automatic_connection_blocking->due_debt = floatval($this->request->getData('automatic_connection_blocking.due_debt'));
                    $paraments->automatic_connection_blocking->type = $paraments->gral_config->billing_for_service ? 'connection' : 'customers';
                    $paraments->automatic_connection_blocking->force = $this->request->getData('automatic_connection_blocking.force') ? true : false;

                    $tipo_recargo = [
                        'next_month' => 'Deuda c/venc. mes próximo', 
                        'this_month' => 'Nota de Débito p/mes en curso'
                    ];

                    if ($this->request->getData('automatic_connection_blocking.surcharge_enabled')) {

                        $paraments->automatic_connection_blocking->surcharge_enabled = $this->request->getData('automatic_connection_blocking.surcharge_enabled') ? true : false;
                        $paraments->automatic_connection_blocking->surcharge_value = floatval($this->request->getData('automatic_connection_blocking.surcharge_value'));
                        $paraments->automatic_connection_blocking->surcharge_concept = $this->request->getData('automatic_connection_blocking.surcharge_concept');
                        $paraments->automatic_connection_blocking->surcharge_type = $this->request->getData('automatic_connection_blocking.surcharge_type');
                        $paraments->automatic_connection_blocking->surcharge_presupuesto = $this->request->getData('automatic_connection_blocking.surcharge_presupuesto') ? true : false;

                    } else {
                        $paraments->automatic_connection_blocking->surcharge_enabled = $this->request->getData('automatic_connection_blocking.surcharge_enabled') ? true : false;
                    }

                    $action = 'Configuración de Bloqueo Automático General' . PHP_EOL;

                    $detail .= 'Configuración Guardada' . PHP_EOL;
                    $detail .= 'Habilitar: ' . ($paraments->automatic_connection_blocking->enabled ? 'Si' : 'No') . PHP_EOL;
                    $detail .= 'Día: ' . $paraments->automatic_connection_blocking->day . PHP_EOL;
                    $detail .= 'Facturas impagas: ' . $paraments->automatic_connection_blocking->invoice_no_paid . PHP_EOL;
                    $detail .= 'Deuda vencida: ' . $paraments->automatic_connection_blocking->due_debt . PHP_EOL;
                    $detail .= 'Configuración a nivel: ' . ($paraments->automatic_connection_blocking->force ? 'General' : 'Individual por cliente') . PHP_EOL;
                    $detail .= 'Recargo por reconexión automático' . PHP_EOL;
                    $detail .= 'Aplicar regargo: ' . ($paraments->automatic_connection_blocking->surcharge_enabled ? 'Si' : 'No') . PHP_EOL;

                    if ($paraments->automatic_connection_blocking->surcharge_enabled) {
                        $detail .= 'Importe: ' . $paraments->automatic_connection_blocking->surcharge_value . PHP_EOL;
                        $detail .= 'Concepto: ' . $paraments->automatic_connection_blocking->surcharge_concept . PHP_EOL;
                        $detail .= 'Tipo de recargo: ' . $tipo_recargo[$paraments->automatic_connection_blocking->surcharge_type] . PHP_EOL;
                        $detail .= 'Forzar presupuesto: ' . ($paraments->automatic_connection_blocking->surcharge_presupuesto ? 'Si' : 'No') . PHP_EOL;
                    }

                    $this->registerActivity($action, $detail, NULL);

                } else {
                    $enabled_before = $paraments->automatic_connection_blocking->enabled ? 'Si' : 'No';
                    $paraments->automatic_connection_blocking->enabled = $this->request->getData('automatic_connection_blocking.enabled') ? true : false;
                    $enabled_after = $paraments->automatic_connection_blocking->enabled ? 'Si' : 'No';

                    $action = 'Configuración de Bloqueo Automático General';
                    $detail = 'Habilitar: ' . $enabled_before . ' => ' . $enabled_after . PHP_EOL; 
                    $this->registerActivity($action, $detail, NULL);
                }

                if (!$paraments->automatic_connection_blocking->force) {

                    $this->loadModel('Customers');

                    $this->Customers->updateAll(
                        [
                            'acb_enabled'           => $paraments->automatic_connection_blocking->enabled,
                            'acb_due_debt'          => $paraments->automatic_connection_blocking->due_debt,
                            'acb_invoice_no_paid'   => $paraments->automatic_connection_blocking->invoice_no_paid,
                            'acb_type'              => $paraments->gral_config->billing_for_service ? 'connection' : 'customers',
                            'acb_surcharge_enabled' => $paraments->automatic_connection_blocking->surcharge_enabled
                        ], 
                        [
                            'code IS NOT'           => NULL
                        ]);

                    $action = 'Configuración Bloqueo Automático Cliente';

                    $detail = 'Configuración Guardada' . PHP_EOL; 
                    $detail .= 'Habilitar: ' . ($paraments->automatic_connection_blocking->enabled ? 'Si' : 'No') . PHP_EOL; 
                    $detail .= 'Deuda Vencida: ' . $paraments->automatic_connection_blocking->due_debt . PHP_EOL; 
                    $detail .= 'Facturas impagas: ' . $paraments->automatic_connection_blocking->invoice_no_paid . PHP_EOL;
                    $detail .= 'Aplicar regargo: ' . ($paraments->automatic_connection_blocking->surcharge_enabled ? 'Si' : 'No') . PHP_EOL;
                    $this->registerActivity($action, $detail, NULL);
                }
            }

            if ($this->request->getData('form') == 'contable') {
                $paraments->accountant->account_enabled = $this->request->getData('accountant.account_enabled') ? TRUE : FALSE;
                $paraments->accountant->acounts_parent->cashs = intval($this->request->getData('accountant_acounts_parent_cashs'));
                $paraments->accountant->acounts_parent->customers = intval($this->request->getData('accountant_acounts_parent_customers'));
                $paraments->accountant->acounts_parent->services = intval($this->request->getData('accountant_acounts_parent_services'));
                $paraments->accountant->acounts_parent->packages = intval($this->request->getData('accountant_acounts_parent_packages'));
                $paraments->accountant->acounts_parent->products = intval($this->request->getData('accountant_acounts_parent_products'));
                $paraments->accountant->acounts_parent->surcharge = intval($this->request->getData('accountant_acounts_parent_surcharge'));
                $paraments->accountant->acounts_parent->bonus = intval($this->request->getData('accountant_acounts_parent_bonus'));
                $paraments->accountant->acounts_parent->services_free = intval($this->request->getData('accountant_acounts_parent_services_free'));
                $paraments->accountant->acounts_parent->cobrodigital = intval($this->request->getData('accountant_acounts_parent_cobrodigital'));
                $paraments->accountant->acounts_parent->todopago = intval($this->request->getData('accountant_acounts_parent_todopago'));
                $paraments->accountant->acounts_parent->mercadopago = intval($this->request->getData('accountant_acounts_parent_mercadopago'));
            }

            if ($this->request->getData('form') == 'system') {

                $action = 'Configuración pestaña: Sistema';
                $detail = "";
                $this->registerActivity($action, $detail, NULL, TRUE);

                $paraments->customer->doc_validate = $this->request->getData('customer.doc_validate') ? true : false;
                $paraments->customer->email_validate = $this->request->getData('customer.email_validate') ? true : false;
                $paraments->customer->search_list_customers = $this->request->getData('customer.search_list_customers') ? true : false;
                $paraments->customer->country_default = $this->request->getData('customer.country_default');
                $paraments->customer->province_default = $this->request->getData('customer.province_default');
                $paraments->customer->city_default = $this->request->getData('customer.city_default');

                $paraments->connection->pppoe_credential = $this->request->getData('connection.pppoe_credential') ? true : false;
                $paraments->connection->pool_plan = $this->request->getData('connection.pool_plan') ? true : false;

                $paraments->system->map->lng = floatval($this->request->getData('system.map.lng'));
                $paraments->system->map->lat = floatval($this->request->getData('system.map.lat'));
                $paraments->system->map->zoom = floatval($this->request->getData('system.map.zoom'));
                $paraments->system->server->ip = $this->request->getData('system.server.ip');
                $paraments->system->uri_http = $this->request->getData('system.uri_http');

                if ($this->request->getData('session_timeout') > 0) {

                    $session_time_before = Configure::read('Session')['timeout'];
                    $session_time_new = intval($this->request->getData('session_timeout'));
                    $SessionTimeShell = new \App\Shell\SessionTimeShell;
                    $SessionTimeShell->change($session_time_before, $session_time_new);
                    $session_timeout = $session_time_new;
                } else {
                    $this->Flash->warning(__('El tiempo de sesión debe ser mayor a 1 minutos.'));
                }

                $paraments->system->mode_migration = $this->request->getData('system.mode_migration') ? true : false;
                $paraments->system->show_error_log = $this->request->getData('system.show_error_log') ? true : false;

                $action = 'Configuración por defecto guardada' . PHP_EOL;
                $detail .= 'Validación por DNI : ' . ($paraments->customer->doc_validate ? 'Si' : 'No') . PHP_EOL;
                $detail .= 'Validar Correo Electónico: ' . ($paraments->customer->email_validate ? 'Si' : 'No') . PHP_EOL;
                $detail .= 'Búsqueda de cliente (tabla): ' . ($paraments->customer->search_list_customers ? 'Si' : 'No') . PHP_EOL;

                $country_value = "";
                $country_selected = NULL;
                foreach ($countries as $country) {
                    if ($paraments->customer->country_default == $country->id) {
                        $country_value = $country->name;
                        $country_selected = $country;
                    }
                }

                $detail .= 'País: ' . $country_value . PHP_EOL;

                $province_value = "";
                $province_selected = NULL;
                if ($country_selected != NULL) {
                    foreach ($country_selected->provinces as $province) {
                        if ($paraments->customer->province_default == $province->id) {
                            $province_value = $province->name;
                            $province_selected = $province;
                        }
                    }
                }

                $detail .= 'Provincia: ' . $province_value . PHP_EOL;

                $city_value = "";
                if ($province_selected != NULL) {
                    foreach ($province_selected->cities as $city) {
                        if ($paraments->customer->city_default == $city->id) {
                            $city_value = $city->name;
                        }
                    }
                }

                $detail .= 'Ciudad: ' . $city_value . PHP_EOL;
                $detail .= 'Conexión con los Controladores: ' . ($paraments->system->integration ? 'Si' : 'No') . PHP_EOL;
                $detail .= 'Credenciales PPPoE Automático: ' . ($paraments->connection->pppoe_credential ? 'Si' : 'No') . PHP_EOL;
                $detail .= 'Relación de redes con servicios: ' . ($paraments->connection->pool_plan ? 'Si' : 'No') . PHP_EOL;
                $detail .= 'Mapa lat: ' . $paraments->system->map->lat . PHP_EOL;
                $detail .= 'Mapa lng: ' . $paraments->system->map->lng . PHP_EOL;
                $detail .= 'Mapa zoom: ' . $paraments->system->map->zoom . PHP_EOL;
                $detail .= 'Server IP: ' . $paraments->system->server->ip . PHP_EOL;
                $detail .= 'uri http: ' . $paraments->system->uri_http . PHP_EOL;         
                $detail .= 'Mostrar contador de errores: ' . ($paraments->system->show_error_log ? 'Si' : 'No') . PHP_EOL;
                $this->registerActivity($action, $detail, NULL);
            }

            if ($this->request->getData('form') == 'download_upload' ) {

                 if ($this->request->getData('upload_parament.name')) {

                    $file_upload = $this->request->getData('upload_parament');

                    $file = new File($file_upload['tmp_name'], false, 0644);
                    $file_data = $file->read(true, 'r');
                    $paraments = json_decode($file_data);
                    $file->close();

                    $this->loadModel('GeneralParameters');
                    $general_paraments_db = $this->GeneralParameters->find()->first();

                    if ($general_paraments_db) {

                        $general_paraments_db->modified = Time::now();
                        $general_paraments_db->data = serialize($paraments);

                        if (!$this->GeneralParameters->save($general_paraments_db)) {
                           $this->Flash->error(__('Error al intentar cargar la configuración'));
                        }
                    }
                }

                if ($this->request->getData('upload_payment_getway.name')) {

                    $file_upload = $this->request->getData('upload_payment_getway');

                    $file = new File($file_upload['tmp_name'], false, 0644);
                    $file_data = $file->read(true, 'r');
                    $payment_getway = json_decode($file_data);
                    $file->close();

                    $this->loadModel('PaymentGetwayParameters');
                    $payment_getway_paraments_db = $this->PaymentGetwayParameters->find()->first();

                    if ($payment_getway_paraments_db) {

                        $payment_getway_paraments_db->modified = Time::now();
                        $payment_getway_paraments_db->data = serialize($payment_getway);

                        if (!$this->PaymentGetwayParameters->save($payment_getway_paraments_db)) {
                           $this->Flash->error(__('Error al intentar cargar la configuración'));
                        }
                    }
                    $this->savePaymentGetwayParaments($payment_getway);
                }
            }

            if ($this->request->getData('form') == 'gral_config') {

                $action = 'Configuración pestaña: Gral Config';
                $detail = "";
                $this->registerActivity($action, $detail, NULL, TRUE);

                $paraments->gral_config->billing_for_service = intval($this->request->getData('gral_config.billing_for_service'));
                if (!$paraments->gral_config->billing_for_service) {
                    $paraments->automatic_connection_blocking->type = "customers";
                }
                $this->loadModel('Customers');
                $this->Customers->updateAll(
                    [
                        'billing_for_service' => $paraments->gral_config->billing_for_service
                    ],
                    [
                        'billing_for_service' => !$paraments->gral_config->billing_for_service
                    ]);

                $paraments->gral_config->value_service_customer = intval($this->request->getData('gral_config.value_service_customer'));
                $paraments->gral_config->using_privilegios = intval($this->request->getData('gral_config.using_privilegios'));
                $paraments->gral_config->using_avisos = $this->request->getData('gral_config.using_avisos') ? true : false;

                $paraments->gral_config->enterprise_name = $this->request->getData('gral_config.enterprise_name');
                $paraments->gral_config->using_chat = intval($this->request->getData('gral_config.using_chat'));
                $paraments->gral_config->telegram_group_link = $this->request->getData('gral_config.telegram_group_link');

                $mode_before = intval(Configure::read('debug'));
                $mode_new = intval($this->request->getData('debug_mode'));
                $DebugShell = new \App\Shell\DebugShell;
                $DebugShell->change($mode_before, $mode_new);
                $debug_mode = $mode_new;

                $action = 'Configuración por defecto guardada' . PHP_EOL;
                $detail .= 'Facturación por servicio: ' . ($paraments->gral_config->billing_for_service ? 'Si' : 'No') . PHP_EOL;
                $detail .= 'Usar privilegios: ' . ($paraments->gral_config->using_privilegios ? 'Si' : 'No') . PHP_EOL;
                $detail .= 'Nombre de Empresa: ' . $paraments->gral_config->enterprise_name . PHP_EOL;
                $detail .= 'Usar chat: ' . ($paraments->gral_config->using_chat ? 'Si' : 'No') . PHP_EOL;
                $detail .= 'Link Grupo Telegram: ' . $paraments->gral_config->telegram_group_link . PHP_EOL;
                $detail .= 'Debug: ' . ($debug_mode ? 'Si' : 'No') . PHP_EOL;
                $this->registerActivity($action, $detail, NULL);
            }

            if ($this->request->getData('form') == 'payment_commitment' ) {

                $paraments->customer->payment_commitment = $this->request->getData('customer.payment_commitment');
            }

            if ($this->request->getData('form') == 'emails_accounts') {

                if ($this->request->getData('sub_form') == 'config') {

                    $paraments->emails_accounts->provider = $this->request->getData('provider');
                }

                if ($this->request->getData('sub_form') == 'sendgrid') {

                    $paraments->emails_accounts->providers->sendgrid->api_key = $this->request->getData('api_key');
                    $paraments->emails_accounts->providers->sendgrid->enabled = $this->request->getData('enabled');

                    if ($paraments->emails_accounts->providers->sendgrid->enabled) {

                        foreach ($paraments->emails_accounts->providers as $key => $provider) {

                            if ($key != 'sendgrid') {

                                $provider->enabled = FALSE;
                            }
                        }
                    } else {

                        if ($paraments->emails_accounts->provider == 'sendgrid') {

                            $paraments->emails_accounts->provider = "";
                        }
                    }
                }
            }

            if ($this->request->getData('form') == 'telegram') {

                $paraments->telegram->url = $this->request->getData('telegram.url');
                $paraments->telegram->token = $this->request->getData('telegram.token');
                $paraments->telegram->chat_id = $this->request->getData('telegram.chat_id');
            }

            if ($this->request->getData('form') == 'billing') {

                $afip_codes = $this->request->getSession()->read('afip_codes');

                $paraments->billing->razon_social = $this->request->getData('billing.razon_social');
                $paraments->billing->province = $this->request->getData('billing.province_billing');
                $paraments->billing->localidad = $this->request->getData('billing.localidad_billing');
                $paraments->billing->address = $this->request->getData('billing.address');
                $paraments->billing->email = $this->request->getData('billing.email');
                //$paraments->billing->doc_type = $this->request->getData('billing.doc_type');
                $paraments->billing->ident = $this->request->getData('billing.ident');
                $paraments->billing->responsible = $this->request->getData('billing.responsible');
                $paraments->billing->confirm = $this->request->getData('billing.confirm') ? true : false;

                $province = $provinces_billing_setting[$paraments->billing->province];

                $action = 'Datos de facturación guardados';
                $detail = 'Razón social: ' . $paraments->billing->razon_social . PHP_EOL; 
                $detail .= 'Condición frente al IVA: ' . $afip_codes['responsibles'][$paraments->billing->responsible] . PHP_EOL; 
                $detail .= 'Provincia: ' . $province . PHP_EOL;
                $detail .= 'Localidad: ' . $paraments->billing->localidad . PHP_EOL;
                $detail .= 'Domicilio: ' . $paraments->billing->address . PHP_EOL;
                $detail .= 'Correo Electrónico: ' . $paraments->billing->email . PHP_EOL;
                $detail .= 'CUIT: ' . $paraments->billing->ident . PHP_EOL;
                $detail .= 'Confirmado: ' . ($paraments->billing->confirm ? 'Si' : 'No') . PHP_EOL;
                $this->registerActivity($action, $detail, NULL);
            }

            //se guarda los cambios
            $this->saveGeneralParaments($paraments);
        }

        $business = ["" => "Seleccionar Empresa"];
        foreach ($paraments->invoicing->business as $b) {
            $business[$b->id] = $b->name;
        }

        $this->loadModel('Accounts');
        $accountsTitles = $this->Accounts->find()->where(['title' => true])->order(['code' => 'ASC']);
        $accounts = $this->Accounts->find()
            ->order([
                'code' => 'ASC'
            ]);

        if (array_key_exists('form', $this->request->getData())) {
            $tab = array_key_exists('form', $this->request->getData()) ? $this->request->getData('form') : 'bills';
        }

        $this->set('user_id', $this->request->getSession()->read('Auth.User')['id']);
        $this->set(compact('paraments', 'accountsTitles', 'accounts', 'tab', 'session_timeout', 'business', 'portal', 'emails', 'payment_getway', 'providers', 'countries', 'emails_emplates', 'cron_posta', 'debug_mode', 'provinces_billing_setting'));
        $this->set('_serialize', ['paraments', 'accounts', 'portal']);
    }

    public function enableJob()
    {
        if ($this->request->is('post')) {

            $paraments = $this->request->getSession()->read('paraments');

            $id = $_POST['id'];
            $job = $_POST['job'];

            $job_new = str_replace("#", "", $job);

            //nube
            $subdomain = $paraments->gral_config->enterprise_name;
            $file = DIRECTORY_SEPARATOR . 'var' . DIRECTORY_SEPARATOR . 'www' . DIRECTORY_SEPARATOR . 'html' . DIRECTORY_SEPARATOR . 'crons-sites' . DIRECTORY_SEPARATOR . $subdomain;

            //local
            //$file = 'C:' . DIRECTORY_SEPARATOR . 'laragon' . DIRECTORY_SEPARATOR . 'www' . DIRECTORY_SEPARATOR . 'crons-sites' . DIRECTORY_SEPARATOR . 'androsnet';

            //read the entire string
            $str = file_get_contents($file);

            //replace something in the file string - this is a VERY simple example
            $str = str_replace($job, $job_new, $str);

            //write the entire string
            file_put_contents($file, $str);

            $this->Flash->success(__('Habiltiado correctamente.'));

            return $this->redirect(['action' => 'index', 'gral_config']);
        }
    }

    public function disableJob()
    {
        if ($this->request->is('post')) {

            $paraments = $this->request->getSession()->read('paraments');

            $id = $_POST['id'];
            $job = $_POST['job'];

            //nube
            $job_new =  '#' . $job;
            $subdomain = $paraments->gral_config->enterprise_name;
            $file = DIRECTORY_SEPARATOR . 'var' . DIRECTORY_SEPARATOR . 'www' . DIRECTORY_SEPARATOR . 'html' . DIRECTORY_SEPARATOR . 'crons-sites' . DIRECTORY_SEPARATOR . $subdomain;
            //local
            //$file = 'C:' . DIRECTORY_SEPARATOR . 'laragon' . DIRECTORY_SEPARATOR . 'www' . DIRECTORY_SEPARATOR . 'crons-sites' . DIRECTORY_SEPARATOR . 'androsnet';

            //read the entire string
            $str = file_get_contents($file);

            //replace something in the file string - this is a VERY simple example
            $str = str_replace($job, $job_new, $str);

            //write the entire string
            file_put_contents($file, $str);

            $this->Flash->success(__('Deshabilitado correctamente.'));

            return $this->redirect(['action' => 'index', 'gral_config']);
        }
    }

    public function editJob()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {

            $paraments = $this->request->getSession()->read('paraments');

            $job = $_POST['job'];
            $job_old = $_POST['job_old'];

            //nube
            $subdomain = $paraments->gral_config->enterprise_name;
            $file = DIRECTORY_SEPARATOR . 'var' . DIRECTORY_SEPARATOR . 'www' . DIRECTORY_SEPARATOR . 'html' . DIRECTORY_SEPARATOR . 'crons-sites' . DIRECTORY_SEPARATOR . $subdomain;

            //local
            //$file = 'C:' . DIRECTORY_SEPARATOR . 'laragon' . DIRECTORY_SEPARATOR . 'www' . DIRECTORY_SEPARATOR . 'crons-sites' . DIRECTORY_SEPARATOR . 'androsnet';


            //read the entire string
            $str = file_get_contents($file);

            //replace something in the file string - this is a VERY simple example
            $str = str_replace($job_old, $job, $str);

            //write the entire string
            file_put_contents($file, $str);

            return $this->redirect(['action' => 'index', 'gral_config']);
        }
    }

    public function download()
    {
        $this->loadModel('GeneralParameters');
        $general_paraments_db = $this->GeneralParameters->find()->first();

        $parament_cliente = null;

        if ($general_paraments_db) {
            $parament_cliente = json_encode(unserialize($general_paraments_db->data), JSON_PRETTY_PRINT);
        }

        $response = $this->response;
        $response = $response->withStringBody($parament_cliente);
        $response = $response->withType('json');

        return $response->withDownload('paraments-' . date('YmdHis') . '.json');
    }

    public function downloadPaymentGetway()
    {
        $this->loadModel('PaymentGetwayParameters');
        $payment_getway_paraments_db = $this->PaymentGetwayParameters->find()->first();

        $payment_getway_cliente = null;

        if ($payment_getway_paraments_db) {
            $payment_getway_cliente = json_encode(unserialize($payment_getway_paraments_db->data), JSON_PRETTY_PRINT);
        }

        $response = $this->response;
        $response = $response->withStringBody($payment_getway_cliente);
        $response = $response->withType('json');

        return $response->withDownload('payment-getway-' . date('YmdHis') . '.json');
    }

    public function downloadPortal()
    {
        $this->loadModel('PortalParameters');
        $portal_paraments_db = $this->PortalParameters->find()->first();

        $portal_cliente = null;

        if ($portal_paraments_db) {
            $portal_cliente = json_encode(unserialize($portal_paraments_db->data), JSON_PRETTY_PRINT);
        }

        $response = $this->response;
        $response = $response->withStringBody($portal_cliente);
        $response = $response->withType('json');

        return $response->withDownload('portal-' . date('YmdHis') . '.json');
    }

    public function getBusiness()
    {
        $paraments = $this->request->getSession()->read('paraments');

        $business  = $paraments->invoicing->business;

        $this->set(compact('business'));
        $this->set('_serialize', ['business']);
    }

    public function downloadCSR($id)
    {
        $paraments = $this->request->getSession()->read('paraments');

        $business = null;

        foreach ($paraments->invoicing->business as $b) {
            if ($b->id == $id) {
                $business = $b;
                break;
            }
        }

        $this->response->file(WWW_ROOT . 'wsfe/' . $business->cuit . '-certificado.tar.gz', array(
            'download' => true,
            'name' => $business->cuit . '-certificado.tar.gz',
        ));
        return $this->response;
    }

    public function generateCSR($id)
    {
        $paraments = $this->request->getSession()->read('paraments');

        $business = null;

        foreach ($paraments->invoicing->business as $b) {
            if ($b->id == $id) {
                $business = $b;
                break;
            }
        }

        if (!$this->validateIdent($business->cuit)) {
             $this->Flash->warning(__('Debe ingresar un CUIT Válido.'));
             return $this->redirect(['action' => 'index']);
        }

        //generar el csr con el shell
        $webServiceAFIPCSRShell = new \App\Shell\WebServiceAFIPCSRShell;
        $webServiceAFIPCSRShell->generate($business);

        $cuit = str_replace('-', '', $business->cuit);

        $business->webservice_afip->key = $cuit . '.key';
        $business->webservice_afip->csr = $cuit . '.csr';

        //se guarda los cambios
        $this->saveGeneralParaments($paraments);

        $this->response->file(WWW_ROOT . 'wsfe/' . $cuit . '-certificado.tar.gz', array(
            'download' => true,
            'name' => $cuit.'-certificado.tar.gz',
        ));
        return $this->response;
    }

    public function addBusiness()
    {
        $paraments = $this->request->getSession()->read('paraments');

        if ($this->request->is(['patch', 'post', 'put'])) {

            $webservice_afip = new \stdClass;
            $webservice_afip->testing = false;
            $webservice_afip->log = false;
            $webservice_afip->alias = '';
            $webservice_afip->key = '';
            $webservice_afip->csr = '';
            $webservice_afip->crt = '';

            $color = new \stdClass;
            $color->hex = $this->request->getData('hex');
            $color->rgb = $this->request->getData('rgb');

            $newlogoPath = '';

            if ($this->request->getData('logo.name')) {

                $file = $this->request->getData('logo');

                $ext = explode('.', $file['name']);

                $newlogoPath = 'logo-' . time() . '-' . $ext[0] . '.' . end($ext);
                $from = $file['tmp_name'];
                $to = WWW_ROOT . 'img/'. $newlogoPath ;

                if (!move_uploaded_file($from, $to)) {
                     $this->Flash->error(__('Error al intentar agregar la la imagen'));
                }
            }

            $b =  [
                'id' => Time::now()->toUnixString(),
                'pto_vta' => $this->request->getData('pto_vta'),
                'name' => $this->request->getData('name'),
                'fantasy_name' => $this->request->getData('fantasy_name'),
                'address' => $this->request->getData('address'),
                'cp' => $this->request->getData('cp'),
                'city' => $this->request->getData('city'),
                'color' => $color,
                'logo' => $newlogoPath,
                'copias' => $this->request->getData('copias'),
                'phone' => $this->request->getData('phone'),
                'cuit' => $this->request->getData('cuit'),
                'ing_bruto' => $this->request->getData('ing_bruto'),
                'init_act' => $this->request->getData('init_act'),
                'responsible' => $this->request->getData('responsible'),
                'enable' => true,             
                'webservice_afip' =>  $webservice_afip,
                'email' => $this->request->getData('email'),
                'web' => $this->request->getData('web'),
                'direccion_comercio_interior' => $this->request->getData('direccion_comercio_interior'),
            ];

            $paraments->invoicing->business[] = json_decode(json_encode($b)); 
        }

        //se guarda los cambios
        $this->saveGeneralParaments($paraments);
        return $this->redirect(['action' => 'index', 'empresas']);
    }  

    public function editBusiness()
    {
        $paraments = $this->request->getSession()->read('paraments');

        if ($this->request->is(['patch', 'post', 'put'])) {

            foreach ($paraments->invoicing->business as $business) {

                if ($business->id == $this->request->getData('id')) {

                    $color = new \stdClass;
                    $color->hex = $this->request->getData('hex');
                    $color->rgb = $this->request->getData('rgb');

                    if ($this->request->getData('logo.name')) {

                        $file = $this->request->getData('logo');

                        $ext = explode('.', $file['name']);

                        $newlogoPath = 'logo-' . time() . '-' . $ext[0] . '.' .  end($ext);
                        $from = $file['tmp_name'];
                        $to = WWW_ROOT . 'img/' . $newlogoPath ;

                        if (move_uploaded_file($from, $to)) {
                            $business->logo = $newlogoPath;
                        } else {
                            $this->Flash->warning(__('Error al intentar agregar la la imagen'));
                        }
                    }

                    $business->pto_vta = $this->request->getData('pto_vta');
                    $business->name = $this->request->getData('name');
                    $business->fantasy_name = $this->request->getData('fantasy_name');
                    $business->address =  $this->request->getData('address');
                    $business->cp = $this->request->getData('cp');
                    $business->city = $this->request->getData('city');

                    $business->color = $color;

                    $business->copias = $this->request->getData('copias');
                    $business->phone = $this->request->getData('phone');

                    $business->cuit = $this->request->getData('cuit');
                    $business->ing_bruto = $this->request->getData('ing_bruto');
                    $business->init_act = $this->request->getData('init_act');
                    $business->responsible = $this->request->getData('responsible');
                    $business->enable = $this->request->getData('enable') ? true : false;
                    $business->web = $this->request->getData('web');
                    $business->email = $this->request->getData('email');
                    $business->direccion_comercio_interior = $this->request->getData('direccion_comercio_interior');

                    $business->webservice_afip->testing = $this->request->getData('testing') ? true : false;
                    $business->webservice_afip->log = $this->request->getData('log') ? true : false;
                    $business->webservice_afip->alias = $this->request->getData('alias');

                    if ($this->request->getData('crt') != '' && $this->request->getData('crt.name')) {

                        $file = $this->request->getData('crt');
                        $file_name = $business->cuit . '.crt';

                        $from = $file['tmp_name'];
                        $to = WWW_ROOT . 'wsfe/'. $file_name;

                        if (move_uploaded_file($from, $to)) {
                             $business->webservice_afip->crt = $file_name;
                        }
                    }
                }
            }
        }

        //se guarda los cambios
        $this->saveGeneralParaments($paraments);
        return $this->redirect(['action' => 'index', 'empresas']);
    }  

    public function deleteBusiness()
    {
        $paraments = $this->request->getSession()->read('paraments');

        if ($this->request->is(['patch', 'post', 'put'])) {

            $new_array = [];

            foreach ($paraments->invoicing->business as $key => $business) {
                if ($business->id != $this->request->getData('id')) {
                    $new_array[] = $business;
                }
            }

            $paraments->invoicing->business = $new_array;
        }

        //se guarda los cambios
        $this->saveGeneralParaments($paraments);
        return $this->redirect(['action' => 'index', 'empresas']);
    } 

    public function downloadCRT($id)
    {
        $paraments = $this->request->getSession()->read('paraments');

        $business = null;

        foreach ($paraments->invoicing->business as $b) {
            if ($b->id == $id) {
                $business = $b;
                break;
            }
        }

        if (file_exists(WWW_ROOT . 'wsfe/' . $business->cuit . '.crt')) {
            $this->response->file(WWW_ROOT . 'wsfe/' . $business->cuit . '.crt', array(
                'download' => true,
                'name' => $business->cuit . '.crt',
            ));
            return $this->response;
        }

        $this->Flash->warning(__('No se subio el archivo CRT.'));
        $url = explode('/', $this->referer());
        $controller = $url[4];
        $url = $url[0] . '//' . $url[2] . '/' . $url[3] . '/' . $url[4] . '/' . $url[5];
        $url .= '/bills';
        return $this->redirect($url);
    }

    private function validateIdent($ident)
    {
        if (strpos($ident, ' ')) {
            $ident = str_replace(" ", "", $ident);
        }

        $ident = str_replace("-", "", $ident);
 
        if (strlen($ident) < 10) { // lo tiene l largo de un cuit
            return false;
        } else {
            $ident = substr($ident, 0, 2) . '-' . substr($ident, 2, 8) . '-' . substr($ident, 10);
            return CuitValidator::isValid($ident);
        }
    }

    public function backup()
    {
        $paraments = $this->request->getSession()->read('paraments');

        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Configuración de Backup';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $paraments->backup->enabled = $this->request->getData('enabled') ? true : false;
            $paraments->backup->hours_execution = $this->request->getData('hours_execution');
            $paraments->backup->folder_name = $this->request->getData('folder_name');
            $paraments->backup->amount_files = $this->request->getData('amount_files');
            $paraments->backup->app_key = $this->request->getData('app_key');
            $paraments->backup->app_secret = $this->request->getData('app_secret');
            $paraments->backup->token = $this->request->getData('token');

            $action = 'Configuración de Backup guardada';
            $detail = 'Habilitado: ' . ($paraments->backup->enabled ? 'Si' : 'No') . PHP_EOL;
            $detail .= 'Horario de ejecución: ' . $paraments->backup->hours_execution . PHP_EOL;
            $detail .= 'Nombre de Carpeta: ' . $paraments->backup->folder_name . PHP_EOL;
            $detail .= 'Número archivos: ' . $paraments->backup->amount_files . PHP_EOL;
            $detail .= 'App Key: ' . $paraments->backup->app_key . PHP_EOL;
            $detail .= 'App Secret: ' . $paraments->backup->app_secret . PHP_EOL;
            $detail .= 'Token: ' . $paraments->backup->token . PHP_EOL;
            $this->registerActivity($action, $detail, NULL);

            $this->saveGeneralParaments($paraments);
        }

        $this->set(compact('paraments'));
        $this->set('_serialize', ['paraments']);
    }

    public function setConfigDuedateCustomers()
    {
        $paraments = $this->request->getSession()->read('paraments');
        $this->loadModel('Customers');
        $this->Customers->updateAll(
            [
                'acb_enabled'           => $paraments->automatic_connection_blocking->enabled,
                'acb_due_debt'          => $paraments->automatic_connection_blocking->due_debt,
                'acb_invoice_no_paid'   => $paraments->automatic_connection_blocking->invoice_no_paid,
                'acb_type'              => $paraments->automatic_connection_blocking->type,
                'acb_surcharge_enabled' => $paraments->automatic_connection_blocking->surcharge_enabled
            ], 
            [
                'code IS NOT'           => NULL
            ]);
        $tab = 'duedates';
        $this->Flash->success(__('Se ha aplicado la configuración a todos los Clientes correctamente.'));
        return $this->redirect(['action' => 'index', $tab]);
    }

    public function setVisibilityChat()
    {
        if ($this->request->is(['ajax'])) {
            $show_chat = $this->request->input('json_decode')->show_chat;
            $user = $this->request->getSession()->read('Auth.User');
            $user['show_chat'] = $show_chat;
            $this->request->getSession()->write('Auth.User', $user);
        }
    }

    public function enableBusiness()
    {
        if ($this->request->is(['ajax'])) {

            $paraments = $this->request->getSession()->read('paraments');

            $busines_id = $this->request->input('json_decode')->busines_id;

            foreach ($paraments->invoicing->business as $key => $business) {
                if ($business->id == $busines_id) {
                    $business->enable = true;
                    $this->saveGeneralParaments($paraments);
                }
            }
        }
    }

    public function disableBusiness()
    {
        if ($this->request->is(['ajax'])) {

            $paraments = $this->request->getSession()->read('paraments');

            $busines_id = $this->request->input('json_decode')->busines_id;

            foreach ($paraments->invoicing->business as $key => $business) {
                if ($business->id == $busines_id) {
                    $business->enable = false;
                    $this->saveGeneralParaments($paraments);
                }
            }
        }
    }

    public function billing()
    {
        if ($this->request->is(['post'])) {

            $this->loadModel('Provinces');
            $provinces = $this->Provinces->find('list')->toArray();

            $paraments = $this->request->getSession()->read('paraments');
            $afip_codes = $this->request->getSession()->read('afip_codes');
            $razon_social = $this->request->getData('razon_social');
            $responsible = $this->request->getData('responsible');
            $province = $this->request->getData('province_billing');
            $localidad = $this->request->getData('localidad_billing');
            $address = $this->request->getData('address');
            $email = $this->request->getData('email');
            $ident = $this->request->getData('ident');
            //$doc_type = $this->request->getData('doc_type');

            $province_text = $provinces[$province];

            $this->loadModel('Customers');
            $customers_count = $this->Customers
                ->find()
                ->where([
                    'deleted'       => FALSE,
                    'super_deleted' => FALSE
                ])->count();

            // if ($paraments->billing->confirm) {
            //     $action = 'Edición de datos de facturación';
            //     $detail = '';
            //     $this->registerActivity($action, $detail, NULL, TRUE);

            //     $action = 'Datos de facturación';
            //     $detail = 'Razón social: ' . $razon_social . PHP_EOL; 
            //     $detail .= 'Condición frente al IVA: ' . $afip_codes['responsibles'][$responsible] . PHP_EOL; 
            //     $detail .= 'Domicilio: ' . $address . PHP_EOL;
            //     $detail .= 'Correo Electrónico: ' . $email . PHP_EOL;
            //     $detail .= $afip_codes['doc_types'][$doc_type] . ': ' . $ident . PHP_EOL;
            //     $detail .= 'Cant. Clientes: ' . $customers_count . PHP_EOL;
            //     $detail .= 'Versión Sistema: ' . Configure::Read('project.version') . PHP_EOL;
            //     $this->registerActivity($action, $detail, NULL);

            //     //TELEGRAM
            //     $controllerClass = 'App\Controller\TelegramController';

            //     $telegram = new $controllerClass;

            //     $message = "\nDatos internos: " . $paraments->gral_config->enterprise_name;
            //     $message .= "\nToken: " . Configure::read('project.token');
            //     $message .= "\nEmpresa: " . $paraments->gral_config->enterprise_name;
            //     $message .= "\nVersión Sistema: " . Configure::Read('project.version');
            //     $message .= "\n------------------------------------------";
            //     $message .= "\nCambio de Datos de facturación";
            //     $message .= "\nRazón social: " . $razon_social; 
            //     $message .= "\nCondición frente al IVA: " . $afip_codes['responsibles'][$responsible]; 
            //     $message .= "\nDomicilio: " . $address;
            //     $message .= "\nCorreo Electrónico: " . $email;
            //     $message .= "\n" . $afip_codes['doc_types'][$doc_type] . ': ' . $ident;
            //     $message .= "\nCant. Clientes: " . $customers_count;

            //     $telegram->sendMessage($message, '-316189272');

            //     //EMAIL
            //     $controllerClass = 'App\Controller\MassEmailsController';

            //     $mass_email = new $controllerClass;

            //     $email_data = new \stdClass;
            //     $email_data->responsible = $afip_codes['responsibles'][$responsible];
            //     $email_data->address = $address;
            //     $email_data->doc_type = $afip_codes['doc_types'][$doc_type];
            //     $email_data->ident = $ident;
            //     $email_data->razon_social = $razon_social;
            //     $email_data->email = $email;
            //     $email_data->confirm = TRUE;

            //     $mass_email->sendConfimationEmail($email_data);
            //     $this->Flash->success(__('Se editaron los datos de facturación correctamente.'));

            // } else {
                $action = 'Confirmación de uso';
                $detail = '';
                $this->registerActivity($action, $detail, NULL, TRUE);

                $action = 'Datos de facturación'; 
                $detail = 'Razón social: ' . $razon_social . PHP_EOL; 
                $detail .= 'Condición frente al IVA: ' . $afip_codes['responsibles'][$responsible] . PHP_EOL;
                $detail .= 'Provincia: ' . $province_text . PHP_EOL;
                $detail .= 'Localidad: ' . $localidad . PHP_EOL;
                $detail .= 'Domicilio: ' . $address . PHP_EOL;
                $detail .= 'Correo Electrónico: ' . $email . PHP_EOL;
                $detail .= 'CUIT: ' . $ident . PHP_EOL;
                $detail .= 'Cant. Clientes: ' . $customers_count . PHP_EOL;
                $detail .= 'Versión Sistema: ' . Configure::Read('project.version') . PHP_EOL;
                $this->registerActivity($action, $detail, NULL);

                //TELEGRAM
                $controllerClass = 'App\Controller\TelegramController';

                $telegram = new $controllerClass;

                $message = "\nDatos internos: " . $paraments->gral_config->enterprise_name;
                $message .= "\nToken: " . Configure::read('project.token');
                //$message .= "\nEmpresa: " . $paraments->gral_config->enterprise_name;
                $message .= "\nVersión Sistema: " . Configure::Read('project.version');
                $message .= "\n------------------------------------------";
                $message .= "\nConfirmación!!!! - Datos de facturación";
                $message .= "\nRazón social: " . $razon_social; 
                $message .= "\nCondición frente al IVA: " . $afip_codes['responsibles'][$responsible];
                $message .= "\nProvincia: " . $province_text;
                $message .= "\nLocalidad: " . $localidad;
                $message .= "\nDomicilio: " . $address;
                $message .= "\nCorreo Electrónico: " . $email;
                $message .= "\n" . 'CUIT: ' . $ident;
                $message .= "\nCant. Clientes: " . $customers_count;

                $telegram->sendMessage($message, '-316189272');

                //EMAIL
                $controllerClass = 'App\Controller\MassEmailsController';

                $mass_email = new $controllerClass;

                $email_data = new \stdClass;
                $email_data->responsible = $afip_codes['responsibles'][$responsible];
                $email_data->province = $province_text;
                $email_data->localidad = $localidad;
                $email_data->address = $address;
                //$email_data->doc_type = $afip_codes['doc_types'][$doc_type];
                $email_data->ident = $ident;
                $email_data->razon_social = $razon_social;
                $email_data->email = $email;
                $email_data->confirm = FALSE;

                $mass_email->sendConfimationEmail($email_data);

                $paraments->billing->confirm = TRUE;
                $this->Flash->success(__('Se confirmo correctamente.'));
            // }

            $paraments->billing->razon_social = $razon_social;
            $paraments->billing->email = $email;
            $paraments->billing->responsible = $responsible;
            $paraments->billing->province = $province;
            $paraments->billing->localidad = $localidad;
            $paraments->billing->address = $address;
            $paraments->billing->ident = $ident;
            //$paraments->billing->doc_type = $doc_type;
            $this->saveGeneralParaments($paraments);
        }
        return  $this->redirect($this->referer());
    }

    public function forceDebtMonthControl()
    {
        if ($this->request->is('post')) {

            $action = 'Ejecución de recalculo de saldo forzada';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $command = ROOT . "/bin/cake DebtMonthControl > /dev/null &";
            $result = exec($command, $output);

            $this->Flash->success(__('Recalculo de saldo forzada ejecutada.'));

            return  $this->redirect($this->referer());
        }
    }
}

class CuitValidator {
	static function isValid($cuit) {
		$digits = array();
		if (strlen($cuit) != 13) return false;
		for ($i = 0; $i < strlen($cuit); $i++) {
			if ($i == 2 or $i == 11) {
				if ($cuit[$i] != '-') return false;
			} else {
				if (!ctype_digit($cuit[$i])) return false;
				if ($i < 12) {
					$digits[] = $cuit[$i];
				}
			}
		}
		$acum = 0;
		foreach (array(5, 4, 3, 2, 7, 6, 5, 4, 3, 2) as $i => $multiplicador) {
			$acum += $digits[$i] * $multiplicador;
		}
		$cmp = 11 - ($acum % 11);
		if ($cmp == 11) $cmp = 0;
		if ($cmp == 10) $cmp = 9;
		return ($cuit[12] == $cmp);
    }
}
