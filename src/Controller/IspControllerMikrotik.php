<?php
namespace App\Controller;

use App\Controller\IspControllerController;
use Cidr;


abstract class IspControllerMikrotik extends IspControllerController
{
    public function initialize()
    {
        parent::initialize();       
     
    }    
    
    
    //scripts
    
    public function applyConfigAvisos(){
        
        if ($this->request->is('ajax')) {
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            
            $response = false;
           
            $paraments = $this->request->getSession()->read('paraments');
            $ip_server = $paraments->system->server->ip;
             
            if($ip_server){                
                $this->loadModel('Controllers');
                $controller = $this->Controllers->get($controller_id);                
                $response = $this->IntegrationRouter->applyConfigAvisos($controller, $ip_server);                  
             }
            
            $this->set('data', $response);
        }     
        
    } 
    
    public function applyConfigQueuesGraph(){
        
        if ($this->request->is('ajax')) {
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            
            $response = false;
         
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
            $response = $this->IntegrationRouter->applyConfigQueuesGraph($controller);   
        
            $this->set('data', $response);
        }
        
    } 
    
    public function generateCertificateAndApply(){
        
        if ($this->request->is('ajax')) {
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            
            $response  = false;
            
            $paraments = $this->request->getSession()->read('paraments');
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
            $response = $this->IntegrationRouter->generateCertificateAndApply($controller);
          
            $this->set('data', $response);
        }        
    } 
    
   
    
    
    public function applyConfigAccessNewCustomer(){
        
        if ($this->request->is('ajax')) {
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            
            $response = false;
           
            $paraments = $this->request->getSession()->read('paraments');
            $ip_server = $paraments->system->server->ip;
             
            if($ip_server){
                
                $this->loadModel('Controllers');
                $controller = $this->Controllers->get($controller_id);
                
                $response = $this->IntegrationRouter->applyConfigAccessNewCustomer($controller, $ip_server);             
                $this->log($response, 'debug');
             }
            
            $this->set('data', $response);
        }
    }
     
    
}



