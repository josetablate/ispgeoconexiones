<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use PEAR2\Net\RouterOS;
use PEAR2\Net\RouterOS\SocketException;
use PEAR2\Net\RouterOS\DataFlowException;
use Cake\I18n\Time;

/**
 * Home Controller
 *
 * @property \App\Model\Table\HomeTable $Home
 */
class HomeController extends AppController
{
    public function isAuthorized($user = null) 
    {
        if ($this->request->getParam('action') == 'dashboard') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function  beforeFilter(Event $event)
    {}

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('IntegrationRouter', [
             'className' => '\App\Controller\Component\Integrations\IntegrationRouter'
        ]);
    }

    public function index()
    {}

    public function dashboard()
    {
        $this->loadModel('Customers');
        $this->loadModel('Connections');
        $this->loadModel('Tickets');
        $this->loadModel('Controllers');

        $customer_ids_disabled = $this->getCustomersIdsBusinessDisabled();

        if (count($customer_ids_disabled) == 0) {
            $customer_ids_disabled = [-1];
        }

        $session = $this->request->getSession();
        $paraments = $session->read('paraments');

        $amount_customers = $this->Customers->find()->where(['deleted'=> false, 'super_deleted' => false, 'Customers.code NOT IN' => $customer_ids_disabled])->count();
        $amount_connections = $this->Connections->find()->where(['deleted' => false, 'Connections.customer_code NOT IN' => $customer_ids_disabled])->count();
        $amount_connections_lock = $this->Connections->find()->where(['enabled' => false, 'deleted' => false, 'Connections.customer_code NOT IN' => $customer_ids_disabled])->count();

        $tickets_opened = $this->Tickets->find()->where(['status' => 1])->count();
        $tickets_opened_asign = $this->Tickets->find()->where(['status' => 2])->count();

        $data = null;
        $connections_coords = null;
        $customers_coords = null;
        $controllers_coords = null;

        $connections_coords = $this->Connections->find()
            ->select(['Connections.id', 'Connections.ip', 'Connections.lat', 'Connections.lng', 'Connections.address', 'Customers.name', 'Customers.code', 'Customers.doc_type', 'Customers.ident', 'Services.name', 'Controllers.name'])
            ->contain(['Customers', 'Services', 'Controllers'])
            ->where(['Connections.deleted' => FALSE,  'Connections.customer_code NOT IN' => $customer_ids_disabled])
            ->toArray();

        $customers_coords = [];

        $controllers_coords = $this->Controllers->find()
            ->select(['id', 'lat', 'lng', 'name', 'template', 'connect_to', 'port', 'radius', 'color'])
            ->where(['deleted' => 0])
            ->toArray();

        $now = Time::now()->day(1)->hour(0)->minute(0)->second(0)->modify('+1 month');

        $this->loadModel('Payments');
        $payments = $this->Payments
            ->find()
            ->contain([
                'Customers'
            ])
            ->where([
                'anulated IS'        => NULL,
                'Payments.created >' => Time::now()->day(1)->hour(0)->minute(0)->second(0), 
                'Customers.deleted'  => FALSE,
                'Payments.customer_code NOT IN' => $customer_ids_disabled
            ]);
        $payments = $payments->select(['total' => $payments->func()->sum('import')])->first();

        $this->loadModel('ConnectionsDebtsMonth');

        $bussinesDisabledIds = $this->getBusinessDisabled();

        if (count($bussinesDisabledIds) == 0) {
            $bussinesDisabledIds = [-1];
        }

        $collect_month_service = 0;

        $period = $this->ConnectionsDebtsMonth
            ->find()
            ->select([
                'id',
                'period'
            ])
            ->order([
                'id' => 'DESC'
            ])
            ->where([
                'ConnectionsDebtsMonth.business_billing NOT IN' => $bussinesDisabledIds
            ])
            ->first();

        if ($period) {

            $connections_debts_month = $this->ConnectionsDebtsMonth
                ->find()
                ->where([
                    'ConnectionsDebtsMonth.period' => $period->period
                ]);
            $connections_debts_month_totalx = $connections_debts_month->select(['totalx' => $connections_debts_month->func()->sum('totalx')])->first();
            $connections_debts_month_total = $connections_debts_month->select(['total' => $connections_debts_month->func()->sum('total')])->first();

            $collect_month_service = $connections_debts_month_totalx->totalx + $connections_debts_month_total->total;
        }

        $data = [
            'total_customers' => $amount_customers,
            'customers_new'   => 0,

            'total_connections'      => $amount_connections,
            'total_connections_lock' => $amount_connections_lock,
            'connections_new'        => 0,

            'tickets_opened'       => $tickets_opened,
            'tickets_opened_asign' => $tickets_opened_asign,

            'collect_month_service' => $collect_month_service,
            'collected_month'       => $payments->total,
            'collect_pending'       => 0,
        ];

        $now = Time::now();

        $Last1Month = Time::now();
        $Last1Month->day(1)->hour(0)->minute(0)->second(0);

        $customers = $this->Customers->find()->where(['deleted' => FALSE, 'super_deleted' => FALSE, 'Customers.code NOT IN' => $customer_ids_disabled]);

        foreach ($customers as $customer) {

            $data['collect_pending'] += $customer->debt_month;

            if ($customer->created >= $Last1Month) {
                $data['customers_new']++;
            }
        }

        $connections = $this->Connections
            ->find()
            ->where([
                'enabled' => TRUE,
                'deleted' => FALSE,
                'Connections.customer_code NOT IN' => $customer_ids_disabled
            ]);

        foreach ($connections as $connection) {
            if ($connection->created >= $Last1Month) {
                $data['connections_new']++;
            }
        }

        $zoom = $paraments->system->map->zoom;

        $this->set('data', $data);
        $this->set('connections_coords', $connections_coords);
        $this->set('customers_coords', $customers_coords);
        $this->set('controllers_coords', $controllers_coords);
        $this->set('zoom', $zoom);
    }
}
