<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\Component\Admin\FiscoAfipComp;
use App\Controller\Component\Admin\FiscoAfipCompPdf;
use Cake\I18n\Time;

/**
 * CreditNotes Controller
 *
 * @property \App\Model\Table\CreditNotesTable $CreditNotes
 */
class CreditNotesController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('FiscoAfip', [
             'className' => '\App\Controller\Component\Admin\FiscoAfip'
        ]);

        $this->loadComponent('FiscoAfipComp', [
             'className' => '\App\Controller\Component\Admin\FiscoAfipComp'
        ]);

        $this->loadComponent('FiscoAfipCompPdf', [
             'className' => '\App\Controller\Component\Admin\FiscoAfipCompPdf'
        ]);

        $this->loadComponent('Accountant', [
             'className' => '\App\Controller\Component\Admin\Accountant'
        ]);

        $this->loadComponent('CurrentAccount', [
            'className' => '\App\Controller\Component\Admin\CurrentAccount'
        ]);
    }

    public function isAuthorized($user = null) 
    {
        if ($this->request->getParam('action') == 'getCreditnotesCustomer') {
            return true;
        }

        if ($this->request->getParam('action') == 'getCreditNotes') {
            return true;
        }

        if ($this->request->getParam('action') == 'delete') {
            return true;
        }

        if ($this->request->getParam('action') == 'addCreditos') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function index()
    {
        $this->loadModel('MassEmailsTemplates');
        $mass_emails_templates_model = $this->MassEmailsTemplates
            ->find()
            ->where([
                'enabled' => TRUE
            ]);

        $mass_emails_templates = [
            '' => 'Seleccione'
        ];

        foreach ($mass_emails_templates_model as $mass_email_template_model) {
            $mass_emails_templates[$mass_email_template_model->id] = $mass_email_template_model->name;
        }
        $this->set(compact('mass_emails_templates'));
        $this->set('_serialize', ['mass_emails_templates']);
    }

    public function getCreditNotes()
    {
        if ($this->request->is('ajax')) {

            $response =  new \stdClass();

            $response->draw = intval($this->request->getQuery('draw'));

            $customer_ids_disabled = $this->getCustomersIdsBusinessDisabled();

            $where = [];
            if(count($customer_ids_disabled) > 0){
                $where += ['CreditNotes.customer_code NOT IN' => $customer_ids_disabled];
            }

            if (null !== $this->request->getQuery('where')) {
                $where += $this->request->getQuery('where');
                $response->recordsTotal = $this->CreditNotes->find()->where($where)->count();
            } else {
                $response->recordsTotal = $this->CreditNotes->find()->where($where)->count();
            }

            $response->data = $this->CreditNotes->find('ServerSideData', [
                'params' => $this->request->getQuery(),
                'where' => count($where) > 0 ?  $where : false
            ]);

            $response->recordsFiltered = $this->CreditNotes->find('RecordsFiltered', [
                'params' => $this->request->getQuery(),
                'where' =>  count($where) > 0 ?  $where : false
            ]);

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

   /**
    * called from: 
    *  customer/view
    */
    public function getCreditnotesCustomer()
    {
        if ($this->request->is('ajax')) {

            $code = $this->request->getQuery('customer_code');

            $creditnotes = $this->CreditNotes->find()->contain(['Users'])->where(['customer_code' => $code]);

            $this->loadModel('CompAsociados');

            foreach ($creditnotes as $creditNote) {

                $creditNote->comp_asociados = $this->CompAsociados->find()
                    ->where([
                        'tipo_comp' => $creditNote->tipo_comp,
                        'pto_vta' => $creditNote->pto_vta,
                        'num' => $creditNote->num,
                    ]);
            }

            $this->set(compact('creditnotes'));
            $this->set('_serialize', ['creditnotes']);
        }
    }

    public function showprint($id)
    {
        $data =  new \stdClass;
        $data->id = $id;
        $this->FiscoAfipCompPdf->creditNote($data);
    }

    public function delete()
    {
        if ($this->request->is('ajax')) {

            $data = $this->request->input('json_decode');

            $creditNote = $this->CreditNotes->get($data->id);

            $ok = false;

            if ($this->FiscoAfipComp->rollbackCreditNote($creditNote)) {
                $ok = true;
            }

            $this->set("ok", $ok);
        }
    }

    public function addCreditos()
    {
        if ($this->request->is('ajax')) {

            $paraments = $this->request->getSession()->read('paraments');
            $afip_codes = $this->request->getSession()->read('afip_codes');

            $response['error'] = TRUE;
            $response['msg'] = "Error, al intentar generar el Crédito."; 

            $data = $this->request->input('json_decode');

            $this->loadModel('Customers');
            $customer = $this->Customers
                ->find()
                ->contain([
                    'Cities'
                ])
                ->where([
                    'code' => $data->customer_code
                ])->first();

            if ($customer) {

                $tax = $data->type == "013" ? 1 : 5;
                $tax_porcentaje = $afip_codes['alicuotas_percectage'][$tax];

                $result = $this->CurrentAccount->calulateIvaAndNeto($data->total, $tax_porcentaje, 1);

                $concepts = [];

                $concept = new \stdClass;
                $concept->type        = 'S';
                $concept->code        = 99;
                $concept->description = $data->concept;
                $concept->quantity    = 1;
                $concept->unit        = 7; //Un.
                $concept->price       = $result->price;
                $concept->discount    = 0;
                $concept->sum_price   = $result->sum_price;
                $concept->tax         = $tax;
                $concept->sum_tax     = $result->sum_tax;
                $concept->total       = $result->total;
                $concepts[0]          = $concept;

                $now = Time::now();
                $creditNote  = [
                    'customer'       => $customer,
                    'concept_type'   => 3, // Productos y Servicios
                    'date'           => $now,
                    'date_start'     => $now,
                    'date_end'       => $now,
                    'duedate'        => $now,
                    'concepts'       => $concepts,
                    'tipo_comp'      => $data->type,
                    'comments'       => $data->concept,
                    'manual'         => TRUE,
                    'business_id'    => $customer->business_billing,
                    'connection_id'  => $data->connection_id,
                    'seating_number' => NULL,
                ];

                $creditNote = $this->FiscoAfipComp->creditNote($creditNote);

                if ($creditNote) {

                   //creo asiento

                   if ($this->Accountant->isEnabled()) {

                        $seating = $this->Accountant->addSeating([
                            'concept'       => 'Crédito',
                            'comments'      => '',
                            'seating'       => NULL,
                            'account_debe'  => $customer->account_code,
                            'account_haber' => $paraments->accountant->acounts_parent->surcharge,
                            'value'         => $creditNote->total,
                            'redirect'      => NULL
                        ]);

                        $creditNote->seating_number = $seating->number;

                        if ($seating) {
                            $creditNote->seating_number = $seating->number;
                            $this->CreditNotes->Save($creditNote);
                        }
                    }

                    //informo afip si no es de tipo X

                    if ($this->FiscoAfip->informar($creditNote, FiscoAfipComp::TYPE_CREDIT_NOTE)) {

                        if ($this->CreditNotes->save($creditNote)) {
                            $response['error'] = FALSE;
                            $response['msg'] = "Se ha generado correctamente el crédito"; 
                        }

                    } else {
                        $this->FiscoAfipComp->rollbackCreditNote($creditNote, true);
                    }
                } else {
                    $this->FiscoAfipComp->rollbackCreditNote($creditNote, true);
                }
            }
            $this->set('data', $response);
        }
     }
}
