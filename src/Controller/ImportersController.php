<?php
namespace App\Controller;

use Cake\I18n\Time;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use App\Controller\Component\ComprobantesComponent;
use App\Controller\Component\PDFGeneratorComponent;
use Cidr;

/**
 * ImportersController Controller
 *
 */
class ImportersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Accountant');
        $this->loadComponent('Integration');
    }

    public function isAuthorized($user = null) 
    {
        return true;
        return ($user['id'] == 100) ? true : false;
    }

    public function index()
    {
        $paraments = $this->request->getSession()->read('paraments');

        if (!file_exists('Importers/customer_data.json')) {
            $this->cleanDataJson('customer_data');
        }

        if (!file_exists('Importers/customer_without_data.json')) {
            $this->cleanDataJson('customer_without_data');
        }

        if (!file_exists('Importers/connection_data.json')) {
            $this->cleanDataJson('connection_data');
        }

        if (!file_exists('Importers/connection_without_data.json')) {
            $this->cleanDataJson('connection_without_data');
        }

        if (!file_exists('Importers/debt_data.json')) {
            $this->cleanDataJson('debt_data');
        }

        if (!file_exists('Importers/debt_without_data.json')) {
            $this->cleanDataJson('debt_without_data');
        }

        // Import customers data
        $customerImport = $this->request->getSession()->read('customerImport');

        if (!$customerImport) {
            $customerImport = [
            	'code'           => 'A',
            	'name'           => 'B',
            	'doc_type'       => 'C',
            	'ident'          => 'D',
            	'plan_ask'       => 'E',
            	'address'        => 'F',
            	'city_name'      => 'G',
            	'phone'          => 'H',
            	'email'          => 'I',
            	'responsiblex'   => 'J',
            	'cond_ventas'    => 'K',
            	'clave_portal'   => 'L',
            	'created'        => 'M',
            	'seller'         => 'N',
            	'additional'     => '',
            	'csv'            => '',
            	'doc_types'      => $paraments->customer->doc_type,
            	'responsible'    => $paraments->customer->responsible,
            	'cond_venta'     => '1',
            	'is_presupuesto' => 1
            ];
        }

        // Import connections data
        $connectionImport = $this->request->getSession()->read('connectionImport');

        if (!$connectionImport) {
            $connectionImport = [
            	'address'       => 'A',
            	'lat'           => 'B',
            	'lng'           => 'C',
            	'customer_code' => 'D',
            	'ip'            => 'E',
            	'node'          => 'F',
            	'plan'          => 'G',
            	'mac'           => 'H',
            	'date'          => 'I',
            	'csv'           => '',
            	'additional'    => '',
            ];
        }

        // Import debts data
        $debtImport = $this->request->getSession()->read('debtImport');

        if (!$debtImport) {
            $debtImport = [
            	'code'           => 'A',
            	'ident'            => 'B',
            	'name'           => 'C',
            	'total'          => 'D',
            	'concept'        => 'E',
            	'date'           => 'F',
            	'date_start'     => 'G',
            	'date_end'       => 'H',
            	'duedate'        => 'I',
            ];
        }

        $this->set(compact('paraments', 'customerImport', 'connectionImport', 'debtImport'));
    }

    public function customersPreview()
    {

        if ($this->request->is('post')) {
            
            $this->request->getSession()->write('customerImport', $this->request->getData());

            if ($_FILES['csv']) {
                $this->loadModel('Areas');
                $this->loadModel('Services');

                $abc = $this->createColumnsArray('ZZ');

                $this->request = $this->request->withParsedBody($this->transformUppercase($this->request->getData()));

                $col_code = array_search($this->request->getData('code'), $abc);
                $col_name = array_search($this->request->getData('name'), $abc);

                if ($this->request->getData('doc_type') == '') {
                    $col_doc_type = -1;
                } else {
                    $col_doc_type = array_search($this->request->getData('doc_type'), $abc);
                }

                $col_ident = array_search($this->request->getData('ident'), $abc);

                if ($this->request->getData('plan_ask') == '') {
                    $col_plan_ask = -1;
                } else {
                    $col_plan_ask = array_search($this->request->getData('plan_ask'), $abc);
                }

                $col_address = array_search($this->request->getData('address'), $abc);
                $col_city_name = array_search($this->request->getData('city_name'), $abc);
                $col_phone = array_search($this->request->getData('phone'), $abc);  

                if ($this->request->getData('email') == '') {
                    $col_email = -1;
                } else {
                    $col_email = array_search($this->request->getData('email'), $abc);
                }

                if ($this->request->getData('responsiblex') == '') {
                    $col_responsible = -1;
                } else {
                    $col_responsible = array_search($this->request->getData('responsiblex'), $abc);
                }

                if ($this->request->getData('cond_ventas') == '') {
                    $col_cond_ventas = -1;
                } else {
                    $col_cond_ventas = array_search($this->request->getData('cond_ventas'), $abc);
                }

                if ($this->request->getData('clave_portal') == '') {
                    $clave_portal = -1;
                } else {
                    $clave_portal = array_search($this->request->getData('clave_portal'), $abc);
                }

                if ($this->request->getData()['created'] == '') {
                    $col_created = -1;
                } else {
                    $col_created = array_search($this->request->getData('created'), $abc);
                }

                if ($this->request->getData('seller') == '') {
                    $col_seller = -1;
                } else {
                    $col_seller = array_search($this->request->getData('seller'), $abc);
                }

                $additional = [];
                if ($this->request->getData()['additional'] != '') {
                    $additional_letters = explode(",", $this->request->getData('additional'));
                    foreach ($additional_letters as $additional_letter) {
                        $index = array_search($additional_letter, $abc);
                        if ($index) {
                            array_push($additional, $index);
                        }
                    }
                }

                $handle = fopen($_FILES['csv']['tmp_name'], "r");

                $first = true;

                $customers_data = [];
                $customers_datax = [];
                $customers_without_data = [];

                while ($data = fgetcsv($handle, 999999, ";")) {
                    $message_error = "";

                    if ($first) {
                        $first = false;
                        continue;
                    }

                    // Verifica si existe el indice, en caso que no se le asigan null.
                    $code = array_key_exists($col_code, $data) ? $data[$col_code] : null;
                    if ($code == null) {
                        $message_error .= "Cargar el código. ";
                    }
                    $name = array_key_exists($col_name, $data) ? $data[$col_name] : null;
                    if ($name == null) {
                        $message_error .= "Cargar el nombre. ";
                    }
                    if ($col_doc_type == -1) {
                        $doc_type = $this->request->getData('doc_types');
                    } else {
                        $doc_type = array_key_exists($col_doc_type, $data) ? $data[$col_doc_type] : null;
                        if ($doc_type == null) {
                            $message_error .= "Cargar el type doc. ";
                        }
                    }

                    $ident_exist = false;
                    $ident = array_key_exists($col_ident, $data) ? $data[$col_ident] : null;

                    if ($ident == null || $ident == '') {
                        $message_error .= "Cargar el nro de documento. ";
                    } else {
                        $ident = str_replace ('.', '', $ident);
                        if (is_numeric($ident)) {
                            foreach ($customers_data as $cus) {
                                if ($cus['ident'] == $ident) {
                                    $ident_exist = true;
                                    $message_error .= 'ident: ' . $ident . ', ya existe. ';
                                }
                            }
                        } else {
                            $message_error .= "Nro de documento, debe ser numérico. ";
                        }
                    }

                    if ($col_plan_ask == -1) {
                        $plan_ask = null;
                    } else {
                        $plan_ask = array_key_exists($col_plan_ask , $data) ? $data[$col_plan_ask] : null;
                        if ($plan_ask != null) {
                            $plan_ask = $this->Services->find('all', [
                                'conditions' => ['Services.name LIKE' => '%' . $plan_ask . '%']
                            ])->first();
                            if ($plan_ask) {
                                $plan_ask = $plan_ask->id;
                            } else {
                                $plan_ask = null; 
                            }
                        } else {
                            $plan_ask = null;
                        }
                    }

                    $address = array_key_exists($col_address, $data) ? $data[$col_address] : null;
                    if ($address == null) {
                        $message_error .= "Cargar el domicilio. ";
                    }

                    $city_name = array_key_exists($col_city_name, $data) ? $data[$col_city_name] : null;

                    if ($city_name != null) {
                        $area = $this->Areas->find('all', [
                            'conditions' => ['Areas.name LIKE' => '%' . $city_name . '%']
                        ])->first();
                        if ($area) {
                            $area_id = $area->id;
                        } else {
                            $area_id = null;
                            $message_error .= "Cargar la ciudad $city_name en la base de datos. ";
                        }
                    } else {
                        $area_id = null;
                        $message_error .= "Cargar la ciudad $city_name en la base de datos. ";
                    }

                    $phone = array_key_exists($col_phone, $data) ? $data[$col_phone] : null;
                    if ($phone == null) {
                        $phone = '0000000000';
                    }

                    $email_exist = false;
                    if ($col_email == -1) {
                        $email = null;
                    } else {
                        $email = array_key_exists($col_email, $data) ? $data[$col_email] : null;
                        if ($email == '') {
                            $email = null;
                        } else {
                            foreach ($customers_data as $cus) {
                                if ($cus['email'] == $email) {
                                    $email_exist = true;
                                    $message_error .= $email . ', ya existe. ';
                                }
                            }
                        }
                    }

                    if ($col_cond_ventas == -1) {
                         $cond_ventas = $this->request->getData('cond_venta');
                    } else {
                        $cond_ventas = array_key_exists($col_cond_ventas, $data) ? $data[$col_cond_ventas] : null;
                        if ($cond_ventas == null) {
                            $message_error .= "Cargar cond_venta. ";
                        }
                    }

                    if ($col_responsible == -1) {
                         $responsible = $this->request->getData('responsible');
                    } else {
                        $responsible = array_key_exists($col_responsible, $data) ? $data[$col_responsible] : null;
                        if ($responsible == null) {
                            $message_error .= "Cargar la responsabilidad ante IVA. ";
                        }
                    }

                    $comments = "";
                    if (!empty($additional)) {
                        $numItems = count($additional);
                        $i = 0;
                        foreach ($additional as $a) {
                            $comments .= $data[$a];
                            //para que agregue la coma al final del atributo si es el ultimo no agrega la coma
                            if (++$i != $numItems) {
                                $comments .= "#";
                            }
                        }
                    }

                    if ($clave_portal == -1) {
                         $clave_portal = $this->randomPassword();
                    } else {
                        $clave_portal = array_key_exists($clave_portal, $data) ? $data[$clave_portal] : null;
                        if ($clave_portal == null || $clave_portal == '') {
                            $clave_portal = $this->randomPassword();
                        }
                    }
                    $created = null;
                    if ($col_created == -1) {
                        $created = null;
                    } else {
                        $created = array_key_exists($col_created, $data) ? $data[$col_created] : null;
                        if ($created == '') {
                            $created = null;
                        }
                    }

                    $seller = null;
                    if ($col_seller == -1) {
                        $seller = null;
                    } else {
                        $seller = array_key_exists($col_seller, $data) ? $data[$col_seller] : null;
                        if ($seller == '') {
                            $seller = null;
                        }
                    }

                    $customer_data = [
                        'code'           => intval(trim($code)),
                        'name'           => $this->cleanString(trim($name)),
                        'doc_type'       => trim($doc_type),
                        'ident'          => trim($ident),
                        'plan_ask'       => trim($plan_ask),
                        'address'        => $this->cleanString(trim($address)),
                        'zone_id'        => $area_id,
                        'phone'          => $this->cleanString(trim($phone)),
                        'email'          => !empty($email) ? trim($email) : null,
                        'cond_venta'     => trim($cond_ventas),
                        'responsible'    => $responsible,
                        'user_id'        => 100,
                        'status'         => 'CP',
                        'comments'       => $this->cleanStringBasic(trim($comments)),
                        'is_presupuesto' => $this->request->getData('is_presupuesto'),
                        'clave_portal'   => $this->cleanString(trim($clave_portal)),
                        'created'        => $this->cleanStringBasic(trim($created)),
                        'seller'         => !empty($seller) ? $this->cleanStringBasic(trim($seller)) : 'admin',
                        'error'          => $message_error,
                    ];

                    //Condicion para guardar los clientes con datos completos y los clientes con datos incompletos.
                    if (!$customer_data['code'] 
                        || !$customer_data['name'] 
                        || (!$customer_data['ident'] || !is_numeric($customer_data['ident'])) 
                        || !$customer_data['address']
                        || !$customer_data['phone']
                        || !$customer_data['zone_id']
                        || !$customer_data['responsible']
                        || $email_exist
                        || $ident_exist) {
 
                        array_push($customers_without_data, $customer_data);
                    } else {

                        array_push($customers_data, $customer_data);
                    }
                }

                $customers_datatable = new \stdClass;
                $customers_datatable->data = $customers_data;

                $customers_json = json_encode($customers_datatable, JSON_PRETTY_PRINT);

                $file = new File('Importers/customer_data.json', false, 0644);

                if ($file->write($customers_json, 'w', true)) {

                } else {
                    $this->Flash->error(__('Error al crear json customers_data.'));
                }

                $customers_without_datatable = new \stdClass;
                $customers_without_datatable->data = $customers_without_data;
                $customers_without_json = json_encode($customers_without_datatable, JSON_PRETTY_PRINT);

                $file = new File('Importers/customer_without_data.json', false, 0644);

                if ($file->write($customers_without_json, 'w', true)) {

                } else {
                    $this->Flash->error(__('Error al crear json customers_without_data.'));
                }

                $this->Flash->success(__('Clientes con datos completos: ' . count($customers_data) . ' - Clientes con datos faltantes: ' . count($customers_without_data)));

                fclose($handle);
            }
        }

        return $this->redirect(['action' => 'index', ]);
    }

    public function customersImport()
    {
        $this->loadModel('Customers');

        $paraments = $this->request->getSession()->read('paraments');
        $counter = 0;
        $error =  false;

        $file = new File('Importers/customer_data.json', false, 0644);
        $json = $file->read(true, 'r');
        $customers_data =  json_decode($json, true);

        foreach ($customers_data['data'] as $customer_data) {
            $customer = $this->Customers->newEntity();
            $customer = $this->Customers->patchEntity($customer, $customer_data);

            $account = $this->Accountant->createAccount([
                'account_parent' => $paraments->accountant->acounts_parent->customers,
                'name' => sprintf("%'.05d", $customer->code) . ' - ' . $customer->ident . ' - ' . $customer->name,
                'redirect' => null
            ]);

            if (!empty($customer->created)) {
                $created = explode(" ", $customer->created);
                $customer->created = new Time($created[0] . '-' . $created[1] . '-' . $created[2] . ' 00:00:00');
            }

            $customer->account_code = $account->code;
            $customer->daydue = $paraments->accountant->daydue;
            $customer->code = $customer_data['code'];

            if (!$this->Customers->save($customer)) {
                $error = true;
                Debug($customer); die();
                break;
            }
            $counter++;
        }

        if ($error) {
            $this->Flash->error(__('Error al cargar'));
        } else {
            $this->Flash->success(__('Se cargaron ' . $counter . ' Clientes'));
        }
        return $this->redirect(['action' => 'index', ]);
    }

    public function connectionsPreview()
    {
         if ($this->request->is('post')) {
            $this->request->getSession()->write('connectionImport', $this->request->getData());

            if ($_FILES['csv']) {
                
                $this->loadModel('Areas');
                $this->loadModel('Services');
                $this->loadModel('Controllers');
                $this->loadModel('Customers');
                $this->loadModel('Connections');
                $this->loadModel('TBPools');
                $this->loadModel('TDNetworks');
                $this->loadModel('TBPlans');
                $this->loadModel('TDPlans');

                $abc = $this->createColumnsArray('ZZ');

                $this->request = $this->request->withParsedBody($this->transformUppercase($this->request->getData()));

                $col_address = array_search($this->request->getData('address'), $abc);

                if ($this->request->getData('lat') == '') {
                    $col_lat = -1;
                } else {
                    $col_lat = array_search($this->request->getData('lat'), $abc);
                }

                if ($this->request->getData('lng') == '') {
                    $col_lng = -1;
                } else {
                    $col_lng = array_search($this->request->getData('lng'), $abc);
                }

                if ($this->request->getData('customer_code') == '') {
                    $col_customer_code = -1;
                } else {
                    $col_customer_code = array_search($this->request->getData('customer_code'), $abc);
                }

                if ($this->request->getData('city_name') == '') {
                    $col_city_name = -1;
                } else {
                    $col_city_name = array_search($this->request->getData('city_name'), $abc);
                }

                $col_node = array_search($this->request->getData('node'), $abc);
                $col_plan = array_search($this->request->getData('plan'), $abc);

                if ($this->request->getData('mac') == '') {
                    $col_mac = -1;
                } else {
                    $col_mac = array_search($this->request->getData('mac'), $abc);
                }

                if ($this->request->getData('date') == '') {
                    $col_date = -1;
                } else {
                    $col_date = array_search($this->request->getData('date'), $abc);
                }

                $additional = [];
                if ($this->request->getData('additional') != '') {
                    $additional_letters = explode(",", $this->request->getData('additional'));
                    foreach ($additional_letters as $additional_letter) {
                        $index = array_search($additional_letter, $abc);
                        if ($index) {
                            array_push($additional, $index);
                        }
                    }
                }

                $handle = fopen($_FILES['csv']['tmp_name'], "r");

                $first = true;

                $connections_data = [];
                $connections_without_data = [];
                $number_row = 1;

                while ($data = fgetcsv($handle, 999999, ";")) {
                    $message_error = "";

                    if ($first) {
                        $first = false;
                        continue;
                    }
                    $customer = null;
                    $customer_code = array_key_exists($col_customer_code, $data) ? $data[$col_customer_code] : null;
                    if ($customer_code == null || $customer_code == '') {
                        $message_error .= "Cargar customer_code. ";
                    } else {
                        $customer = $this->Customers->find()->contain(['Areas'])->where(['code' => $customer_code])->first();
                    }

                    if (!empty($customer)) {

                        // Verifica si existe el indice, en caso que no se le asigan null.
                        $address = array_key_exists($col_address, $data) ? $data[$col_address] : null;
                        if ($address == null || $address == '') {
                            $address = $customer->address;
                        }

                        if ($col_lat == -1) {
                            $lat = null;
                        } else {
                            $lat = array_key_exists($col_lat, $data) ? $data[$col_lat] : null;
                        }

                        if ($col_lng == -1) {
                            $lng = null;
                        } else {
                            $lng = array_key_exists($col_lng, $data) ? $data[$col_lng] : null;
                        }

                        if ($col_ip == -1) {
                            $ip = null;
                        } else {
                            $ip = array_key_exists($col_ip, $data) ? $data[$col_ip] : null;
                        }

                        $controller_id = null;
                        $controller = null;
                        $public_ip = '0';
                        if (!empty($ip)) {
                            // IP Publica
                            if (in_array($ip, ['181.174.202.5', '181.174.202.74', '181.174.202.2'])) {
                                $public_ip = '1';
                                $controller_id = 25;
                            } else {
                                // IP Fija, recorro los pooles y pregunto con Cidr si la ip de la conexion pertenece al pool si es asi obtengo controller_id
                                //$pools = $this->TBPools->find();

                                // DHCP, recorro los pooles y pregunto con Cidr si la ip de la conexion pertenece al pool si es asi obtengo controller_id
                                $pools = $this->TDNetworks->find();
                                foreach ($pools as $pool) {
                                    //IP Fija
                                    //if (Cidr::cidr_match($ip, $pool->addresses)) {

                                    //DHCP
                                    if (Cidr::cidr_match($ip, $pool->address)) {
                                        $controller_id = $pool->controller_id;
                                        $controller = $this->Controllers->find('all', [
                                            'conditions' => ['Controllers.id' => $controller_id
                                        ]])->first();

                                        if (!$controller) {
                                            $message_error .= "Cargar el controlador $node en la base de datos. ";
                                        }
                                    }
                                }

                                if (!$controller_id) {
                                    $message_error .= "IP: $ip sin red.";
                                }
                            }
                        }

                        // $node = array_key_exists($col_node, $data) ? $data[$col_node] : null;
                        // $controller = null;
                        // if ($node != null) {
                        //     $controller = $this->Controllers->find('all', [
                        //         'conditions' => ['Controllers.description' => $node
                        //     ]])->first();

                        //     if ($controller) {
                        //         $controller_id = $controller->id;
                        //     } else {
                        //         $controller_id = null;
                        //         $message_error .= "Cargar el controlador $node en la base de datos. ";
                        //     }
                        // } else {
                        //     $controller_id = null;
                        //     $message_error .= "Cargar el controlador $node en la base de datos. ";
                        // }

                        $plan = array_key_exists($col_plan, $data) ? $data[$col_plan] : null;
                        $service_id = null;
                        if ($plan != null) {
                            $service = $this->Services->find('all', [
                                'conditions' => ['Services.description' => $plan]
                            ])->first();
                            if ($service) {
                                //IP Fijate
                                //$tplan = $this->TBPlans->find()->where(['service_id' => $service->id])->first();

                                //DHCP
                                $tplan = $this->TDPlans->find()->where(['service_id' => $service->id, 'controller_id' => $controller_id])->first();

                                if ($tplan) {
                                    $service_id = $tplan->id;
                                } else {
                                    $service_name = $service->name;
                                    $message_error .= "No hay tplan del service: $service_name en la base de datos.";
                                }
                            } else {
                                $message_error .= "Cargar el plan $plan en la base de datos.";
                            }
                        } else {
                            $message_error .= "Cargar el plan $plan en la base de datos.";
                        }

                        if ($col_mac == -1) {
                            $mac = null;
                        } else {
                            $mac = array_key_exists($col_mac, $data) ? $data[$col_mac] : null;
                        }

                        if ($col_date == -1) {
                            $date = null;
                        } else {
                            $date = array_key_exists($col_date, $data) ? $data[$col_date] : null;
                        }

                        $comments = "";
                        if (!empty($additional)) {
                            $numItems = count($additional);
                            $i = 0;
                            foreach ($additional as $a) {
                                $comments .= $data[$a];
                                //para que agregue la coma al final del atributo si es el ultimo no agrega la coma
                                if (++$i != $numItems) {
                                    $comments .= ", ";
                                }
                            }
                        }

                        // template b -> ip fija (queue)
                        /*$connection_data = [
                            'number_row'         => $number_row++,
                        	'controller_id'      => trim($controller_id),
                        	'plan_id'            => trim($service_id),
                        	'ip'                 => $ip ? trim($ip) : $ip,
                        	'proportional'       => '0',
                        	'public_ip'          => $public_ip,
                        	'mac'                => $mac,
                        	'customer_name'      => $customer->name,
                        	'customer_code'      => trim($customer_code),
                        	'address'            => $this->cleanString(trim(utf8_encode($address))),
                        	'area_id'            => $customer->area_id,
                        	'clave_wifi'         => '',
                        	'ports'              => '',
                        	'ip_alt'             => '',
                        	'ing_traffic'        => '',
                        	'date'               => trim($date),
                        	'comments'           => $comments,
                        	'service_pending_id' => '',
                        	'lat'                => trim($lat),
                        	'lng'                => trim($lng),
                        	'last_conn'          => NULL,
                        	'error'              => $message_error,
                        	'controller'         => json_encode($controller),
                        	'service'            => !empty($service) ? json_encode($service) : null,
                            'customer'           => json_encode($customer),
                        ];*/

                        // template c ppoe con graficas (pppoe)
                        /*$connection_data = [
                            'number_row'         => $number_row++,
                        	'controller_id'      => trim($controller_id),
                        	'plan_id'            => trim($service_id),
                        	'ip'                 => $ip ? trim($ip) : $ip,
                        	'name'               => '00105', //?? hay que ver que logica usan
                        	'password'           => '00010-5', //?? hay que ver que logica usan
                        	'public_ip'          => '0',
                        	'mac'                => $mac,
                        	'customer_name'      => $customer->name,
                        	'customer_code'      => trim($customer_code),
                        	'address'            => $this->cleanString(trim(utf8_encode($address))),
                        	'area_id'            => $customer->area_id,
                        	'clave_wifi'         => '',
                        	'ports'              => '',
                        	'ip_alt'             => '',
                        	'ing_traffic'        => '',
                        	'date'               => trim($date),
                        	'comments'           => $comments,
                        	'service_pending_id' => '',
                        	'lat'                => trim($lat),
                        	'lng'                => trim($lng),
                        	'last_conn'          => NULL,
                        	'error'              => $message_error,
                        	'controller'         => json_encode($controller),
                        	'service'            => !empty($service) ? json_encode($service) : null,
                            'customer'           => json_encode($customer),
                        ];*/

                        // template d -> DHCP
                        $connection_data = [
                            'number_row'         => $number_row++,
                        	'controller_id'      => trim($controller_id),
                        	'plan_id'            => trim($service_id),
                        	'proportional'       => '0',
                        	'ip'                 => $ip ? trim($ip) : $ipp,
                        	'public_ip'          => $public_ip,
                        	'mac'                => $mac,
                        	'customer_name'      => $customer->name,
                        	'customer_code'      => trim($customer_code),
                        	'address'            => $this->cleanString(trim(utf8_encode($address))),
                        	'area_id'            => $customer->area_id,
                        	'clave_wifi'         => '',
                        	'ports'              => '',
                        	'ip_alt'             => '',
                        	'ing_traffic'        => '',
                        	'date'               => trim($date),
                        	'comments'           => '',
                        	'service_pending_id' => '',
                        	'lat'                => trim($lat),
                        	'lng'                => trim($lng),
                        	'last_conn'          => NULL,
                        	'error'              => $message_error,
                        	'controller'         => json_encode($controller),
                        	'service'            => !empty($service) ? json_encode($service) : null,
                            'customer'           => json_encode($customer),
                        ];
                    }

                    if (!empty($connection_data)) {

                        // Condicion para guardar las conexiones con datos completos y las conexiones con datos incompletos.
                        
                        // En DHCP es obligatorio la MAC, pero para IP Fija no es olbigatorio la MAC

                        if (!$connection_data['address'] 
                            || !$connection_data['customer_code'] 
                            || !$connection_data['controller_id'] 
                            || !$connection_data['plan_id']
                            || !$connection_data['mac']) {
                            array_push($connections_without_data, $connection_data);
                        } else {

                            array_push($connections_data, $connection_data);
                        }
                    }
                }

                if (!empty($connection_data)) {
                    $connections_datatable = new \stdClass;
                    $connections_datatable->data = $connections_data;
                    $connections_json = json_encode($connections_datatable, JSON_PRETTY_PRINT);
    
                    $file = new File('Importers/connection_data.json', false, 0644);
    
                    if ($file->write($connections_json, 'w', true)) {
    
                    } else {
                        $this->Flash->error(__('Error al crear json connections_data.'));
                    }
    
                    $connections_without_datatable = new \stdClass;
                    $connections_without_datatable->data = $connections_without_data;
                    $connections_without_json = json_encode($connections_without_datatable, JSON_PRETTY_PRINT);
    
                    $file = new File('Importers/connection_without_data.json', false, 0644);
    
                    if ($file->write($connections_without_json, 'w', true)) {
    
                    } else {
                        $this->Flash->error(__('Error al crear json connection_without_data.'));
                    }
    
                    $this->Flash->success(__('Conexiones con datos completos: ' . count($connections_data) . ' - Conexiones con datos faltantes: ' . count($connections_without_data)));
                } else {
                    $this->Flash->error(__('Debe cargar primero los clientes.'));
                }

                fclose($handle);
            }
        }

        return $this->redirect(['action' => 'index', ]);
    }

    public function connectionsImport()
    {
        $this->loadModel('Connections');
        $this->loadModel('Controllers');
        $this->loadModel('Customers');
        $this->loadModel('Services');

        $counter = 0;
        $count_error = 0;
        $ip_error = false;
        $connections_without_data = [];

        $file = new File('Importers/connection_data.json', false, 0644);
        $json = $file->read(true, 'r');
        $connections_data =  json_decode($json, true);

        $message_error = "";

        foreach ($connections_data['data'] as $connection_data) {

            $connection = $this->Connections->newEntity();

            $controller = $this->Controllers->get($connection_data['controller_id']);
            
            $connection_data['last_conn'] = $this->Connections->find()
                ->where(['customer_code' => $connection_data['customer_code']])
                ->order(['id' => 'DESC'])->first();
            

            if ($connection_data['mac'] != '') {

                $connection_data['mac'] = str_replace(':', '', $connection_data['mac']);
                $connection_data['mac'] = str_replace(' ', '', $connection_data['mac']);
                $connection_data['mac'] = str_replace('-', '', $connection_data['mac']);
            }

            //$connection_data['public_ip'] = 0;
            $connection_data['ip'] = $connection_data['ip'];

            $date = explode("/", $connection_data['date']);
            $date = new Time($date[2] . '-' . $date[1] . '-' . $date[0] . ' 00:00:00');

            $connection_data['created'] = $date;
            $connection_data['modified'] = $date;

            $connection = $this->Integration->addConnection($controller, $connection_data);

            if ($connection) {
                $counter++;
            } else {
                array_push($connections_without_data, $connection_data);
                $count_error++;
            }
            
            // die();
        }

        $connections_without_datatable = new \stdClass;
        $connections_without_datatable->data = $connections_without_data;
        $connections_without_json = json_encode($connections_without_datatable, JSON_PRETTY_PRINT);

        $file = new File('Importers/connection_without_datax.json', false, 0644);

        if ($file->write($connections_without_json, 'w', true)) {

        }

        $message = 'Conexiones cargadas: ' . $counter;

        if ($count_error > 0) {
            $message .= ' - Conexiones no cargadas: ' . $count_error;

            if ($ip_error) {
                $message .= '. Error: COD #029. No existe ip libre para esta conexión.'; 
            }
        }

        $this->Flash->success(__($message . $message_error));

        return $this->redirect(['action' => 'index', ]);
    }

    public function debtsPreview()
    {
        if ($this->request->is('post')) {
            $this->request->getSession()->write('debtImport', $this->request->getData());

            if ($_FILES['csv']) {
                $this->loadModel('Customers');

                $paraments = $this->request->getSession()->read('paraments');

                $abc = $this->createColumnsArray('ZZ');
                
                $this->request = $this->request->withParsedBody($this->transformUppercase($this->request->getData()));

                $col_code = array_search($this->request->getData('code'), $abc);
                $col_name = array_search($this->request->getData('name'), $abc);
                $col_ident = array_search($this->request->getData('ident'), $abc);
                $col_total = array_search($this->request->getData('total'), $abc);
                $col_concept = array_search($this->request->getData('concept'), $abc);
                $col_date = array_search($this->request->getData('date'), $abc);
                $col_date_start = array_search($this->request->getData('date_start'), $abc);
                $col_date_end = array_search($this->request->getData('date_end'), $abc);
                $col_duedate = array_search($this->request->getData('duedate'), $abc);

                $error = false;

                $handle = fopen($_FILES['csv']['tmp_name'], "r");

                $first = true;

                $debts_data = [];
                $debts_without_data = [];

                while ($data = fgetcsv($handle, 999999, ";")) {
                    $message_error = "";

                    if ($first) {
                        $first = false;
                        continue;
                    }

                    // Verifica si existe el indice, en caso que no se le asigan null.
                    $code = array_key_exists($col_code, $data) ? $data[$col_code] : null;
                    if ($code == null) {
                        $message_error .= "Cargar el código. ";
                    }
                    $name = array_key_exists($col_name, $data) ? $data[$col_name] : null;
                    if ($name == null) {
                        $message_error .= "Cargar el nombre. ";
                    }
                    $ident = array_key_exists($col_ident, $data) ? $data[$col_ident] : null;
                    if ($ident == null) {
                        $message_error .= "Cargar el número de doc. ";
                    }
                    $total = array_key_exists($col_total, $data) ? $data[$col_total] : null;
                    if ($total == null) {
                        $message_error .= "Cargar el saldo. ";
                    }
                    $concept = array_key_exists($col_concept, $data) ? $data[$col_concept] : null;
                    if ($concept == null) {
                        $message_error .= "Cargar el concepto. ";
                    }
                    $date = array_key_exists($col_date, $data) ? $data[$col_date] : null;
                    if ($date == null) {
                        $message_error .= "Cargar la fecha. ";
                    }
                    $date_start = array_key_exists($col_date_start, $data) ? $data[$col_date_start] : null;
                    if ($date_start == null) {
                        $message_error .= "Cargar el periodo inicio. ";
                    }
                    $date_end = array_key_exists($col_date_end, $data) ? $data[$col_date_end] : null;
                    if ($date_end == null) {
                        $message_error .= "Cargar el periodo fin. ";
                    }
                    $duedate = array_key_exists($col_duedate, $data) ? $data[$col_duedate] : null;
                    if ($duedate == null) {
                        $message_error .= "Cargar el vencimiento. ";
                    }
                    $total = str_replace("$", "", $total);
                    $total = trim($total);

                    if ($total < 0) {
                        $total = abs($total);
                        $total = '-' . number_format($total, 2, '.', '');
                    } else {
                        $total = abs($total);
                        $total = number_format($total, 2, '.', '');
                    }

                    $debt_data = [
                        'code'       => intval(trim($code)),
                        'name'       => utf8_decode(trim($name)),
                        'ident'      => trim($ident),
                        'total'      => $total,
                        'concept'    => utf8_decode(trim($concept)),
                        'date'       => trim($date),
                        'date_start' => trim($date_start),
                        'date_end'   => trim($date_end),
                        'duedate'    => trim($duedate),
                        'error'      => $message_error,
                    ];

                    // Condicion para guardar los clientes con datos completos y los clientes con datos incompletos.
                    if (!$debt_data['code']
                        || !$debt_data['name']
                        || !$debt_data['ident']
                        || !$debt_data['total']
                        || !$debt_data['concept']
                        || !$debt_data['date']
                        || !$debt_data['duedate']) {
 
                        array_push($debts_without_data, $debt_data);
                    } else {

                        array_push($debts_data, $debt_data);
                    }
                }

                $debts_datatable = new \stdClass;
                $debts_datatable->data = $debts_data;

                $debts_json = json_encode($debts_datatable, JSON_PRETTY_PRINT);

                $file = new File('Importers/debt_data.json', false, 0644);

                if ($file->write($debts_json, 'w', true)) {

                } else {
                    $this->Flash->error(__('Error al crear json debs_data.'));
                }

                $debts_without_datatable = new \stdClass;
                $debts_without_datatable->data = $debts_without_data;
                $debts_without_json = json_encode($debts_without_datatable, JSON_PRETTY_PRINT);

                $file = new File('Importers/debt_without_data.json', false, 0644);

                if ($file->write($debts_without_json, 'w', true)) {

                } else {
                    $this->Flash->error(__('Error al crear json debs_without_data.'));
                }

                $this->Flash->success(__('Deudas con datos completos: ' . count($debts_data) . ' - Deudas con datos faltantes: ' . count($debts_without_data)));

                fclose($handle);
            }
        }

        return $this->redirect(['action' => 'index', ]);
    }

    public function debtsImport()
    {
        $this->loadModel('Customers');
        $this->loadComponent('Comprobantes');
        $this->loadComponent('PDFGenerator');

        $paraments = $this->request->getSession()->read('paraments');
        $counter = 0;
        $error =  0;
        $debts_error_data = [];

        $file = new File('Importers/debt_data.json', false, 0644);
        $json = $file->read(true, 'r');
        $debts_data = json_decode($json);

        foreach ($debts_data->data as $debt_data) {

            $customer = $this->Customers->find()->contain(['Areas'])->where(['code' => $debt_data->code])->first();

            if ($customer) {
                $date = explode("/", $debt_data->date);
                $date = new Time($date[0] . '-' . $date[1] . '-' . $date[2] . ' 00:00:00');

                $date_start = null;
                if (!empty($debt_data->date_start)) {
                    $date_start = explode("/", $debt_data->date_start);
                    $date_start = new Time($date_start[0] . '-' . $date_start[1] . '-' . $date_start[2] . ' 00:00:00');
                }

                $date_end = null;
                if (!empty($debt_data->date_end)) {
                    $date_end = explode("/", $debt_data->date_end);
                    $date_end = new Time($date_end[0] . '-' . $date_end[1] . '-' . $date_end[2] . ' 00:00:00');
                }

                $duedate = explode("/", $debt_data->duedate);
                $duedate = new Time($duedate[0] . '-' . $duedate[1] . '-' . $duedate[2] . ' 00:00:00');
                
                $concept = new \stdClass;
                $concept->type        = 'S';
                $concept->code        = 99;
                $concept->description = $debt_data->concept;
                $concept->quantity    = 1;
                $concept->unit        = 7;
                $concept->price       = abs($debt_data->total);
                $concept->discount    = 0;
                $concept->sum_price   = 0;
                $concept->tax         = 1;
                $concept->sum_tax     = 0;
                $concept->total       = abs($debt_data->total);

                $concepts[0] = $concept;

                //Deuda > 0 and nota de credito < 0
                if ($debt_data->total > 0) {
                    $invoice  = [
                        'customer'     => $customer,
                        'concept_type' => 3,
                        'date'         => $date,
                        'date_start'   => !empty($date_start) ? $date_start : $date,
                        'date_end'     => !empty($date_end) ? $date_end : $date,
                        'duedate'      => $duedate,
                        'concepts'     => $concepts,
                        'tipo_comp'    => 'XXX',
                        'comments'     => 'Factura generada con el importador',
                        'manual'       => true,
                    ];

                    $invoice = $this->Comprobantes->generate($invoice, ComprobantesComponent::TYPE_INVOICE);

                    if (!$invoice) {
                        $error++;
                    } else {
                        $counter++;
                        $this->PDFGenerator->generate($invoice, PDFGeneratorComponent::TYPE_INVOICE);
                    }
                } else {
                    $creditNote  = [
                        'customer'     => $customer,
                        'concept_type' => 3,
                        'date'         => $date,
                        'date_start'   => $date_start,
                        'date_end'     => $date_end,
                        'duedate'      => $duedate,
                        'concepts'     => $concepts,
                        'tipo_comp'    => 'NCX',
                        'comments'     => 'Nota de Crédito generada con el importador',
                        'manual'       => true,
                        'invoice'      => null,
                    ];

                    $creditNote = $this->Comprobantes->generate($creditNote, ComprobantesComponent::TYPE_CREDIT_NOTE);

                    if (!$creditNote) {
                        $error++;
                        array_push($debts_error_data, $debt_data);
                    } else {
                        $counter++;
                        $this->PDFGenerator->generate($creditNote, PDFGeneratorComponent::TYPE_CREDIT_NOTE);
                    }
                }
            } else {
                $error++;
                $debt_data->error = "No existe Customer, debe cargar los clientes.";
                array_push($debts_error_data, $debt_data);
            }
        }

        $debts_error_datatable = new \stdClass;
        $debts_error_datatable->data = $debts_error_data;
        $debts_error_json = json_encode($debts_error_datatable, JSON_PRETTY_PRINT);

        $file = new File('Importers/debt_error_data.json', false, 0644);

        if ($file->write($debts_error_json, 'w', true)) {

        } else {
            $this->Flash->error(__('Error al crear json debt_error_data.'));
        }

        $this->Flash->success(__('Se cargaron: ' . $counter . ' - Sin cargar: ' . $error));

        return $this->redirect(['action' => 'index', ]);
    }

    public function cleanDatatableData()
    {
        if ($this->request->is('post')) {
            $this->cleanDataJson($this->request->getData('file_name'));
        }
        return $this->redirect(['action' => 'index']);
    }

    private function transformUppercase($data)
    {
        foreach ($data as $key => $item) {
            if (is_string($item)) {
                $data[$key] =  strtoupper ($item);
            }
        }
        return $data;
    }

   /**
    * This function creates an array with column names up until the column
    * you specified.
    */
    private function createColumnsArray($end_column, $first_letters = '')
    {
        $columns = array();
        $length = strlen($end_column);
        $letters = range('A', 'Z');

        // Iterate over 26 letters.
        foreach ($letters as $letter) {
            // Paste the $first_letters before the next.
            $column = $first_letters . $letter;

            // Add the column to the final array.
            $columns[] = $column;

            // If it was the end column that was added, return the columns.
            if ($column == $end_column)
              return $columns;
        }

        // Add the column children.
        foreach ($columns as $column) {
            // Don't itterate if the $end_column was already set in a previous itteration.
            // Stop iterating if you've reached the maximum character length.
            if (!in_array($end_column, $columns) && strlen($column) < $length) {
                $new_columns = $this->createColumnsArray($end_column, $column);
                // Merge the new columns which were created with the final columns array.
                $columns = array_merge($columns, $new_columns);
            }
        }

        return $columns;
    }

    private function randomPassword($len = 6) 
    {
        $alphabet = "abcdefghijklmnopqrstuwxyz0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $len; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    private function cleanStringBasic($cadena)
    {
        $srting = (preg_replace('([^A-Za-z0-9])', ' ', $cadena));
        return $srting;
    }

    private function cleanString($cadena) 
    {
        $srting = (preg_replace('([^A-Za-z0-9])', ' ', $cadena));
        if (is_array($srting)) {
            foreach ($srting as $key => $value) {
                $srting[$key] = utf8ize($value);
            }
        } else if (is_string ($srting)) {
            return iconv('UTF-8', 'UTF-8//IGNORE', utf8_encode($srting));
        }
        return $srting;
    }

    private function cleanDataJson($file_name)
    {
        $datatable = new \stdClass;
        $datatable->data = [];
        $datatable_json = json_encode($datatable, JSON_PRETTY_PRINT);
        $file = new File('Importers/' . $file_name . '.json', false, 0644);
        $file->write($datatable_json, 'w', true);
    }
}
