<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Exception\PDOException;
use Cake\Datasource\ConnectionManager;

/**
 * Accounts Controller
 *
 * @property \App\Model\Table\AccountsTable $Accounts
 */
class AccountsController extends AppController
{
    public function isAuthorized($user = null)
    {
        if ($this->request->getParam('action') == 'getAllParents') {
            return true;
        }

        if ($this->request->getParam('action') == 'getByParent') {
            return true;
        }

        if ($this->request->getParam('action') == 'getAllAjax') {
            return true;
        }

        if ($this->request->getParam('action') == 'validateFormAjax') {
            return true;
        }

        if ($this->request->getParam('action') == 'getAccountByCodeAjax') {
            return true;
        }

        if ($this->request->getParam('action') == 'getAllAccountsCustomersAjax') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function index()
    {}

     /**
     * called from 
     * Element/accounts_seeker
    */
    public function getAllParents()
    {
        if ($this->request->is('Ajax')) {
          
            $paraments = $this->request->getSession()->read('paraments');

            $parents = $this->Accounts->find()
                ->where([
                    'title' => true,
                    // 'code !=' => $paraments->accountant->acounts_parent->customers,
                ]);

            $this->set('parents', $parents);
        }
    }

    /**
     * called from 
     * Element/accounts_seeker
    */
    public function getByParent()
    {
        $account_title = $this->request->getQuery('parent');
        // $customer_account = $this->request->getQuery('customer_account');

        $accounts = [];

        if ($account_title != '') {

            $hrchy = '';
            $hrchy_temp = str_split(substr($account_title, 0, 4));
            foreach ($hrchy_temp as $h) {
                $hrchy .= ($h != 0) ? $h : '';
            }

            $sql = "SELECT code, name, description, saldo, title FROM accounts 
                WHERE code LIKE '$hrchy%' AND code != " . str_pad($hrchy, 9, "0", STR_PAD_RIGHT) . " order by code ASC"; 

            $database = ConnectionManager::get('default');
            $accountsDB = $database->execute($sql)->fetchAll('assoc');

            foreach ($accountsDB as $a) {

                $account = new \stdClass;
                $account->code = $a['code'];
                $account->title = $a['title'];

                $account->name = '';

                $hrchy = substr(strval($account->code), 0, 4);
                $hrchy = str_split($hrchy);
                
                foreach ($hrchy as $h) {
                    if ($h != '0') {
                        $account->name .= '&nbsp;&nbsp;&nbsp;'; 
                    }
                }
                if (!$account->title) {
                    $account->name .= '&nbsp;&nbsp;&nbsp;'; 
                }

                $account->name .= $a['name'];

                $account->description = $a['description'];
                $account->saldo = $a['saldo'];

                $accounts[] = $account;
            }

        } else {

            $paraments = $this->request->getSession()->read('paraments');
            $hrchy_customers = substr($paraments->accountant->acounts_parent->customers, 0, 4);
            $sql = "SELECT code, name, description, saldo, title FROM accounts WHERE code NOT LIKE '$hrchy_customers%' order by code ASC"; 

            $database = ConnectionManager::get('default');
            $accountsDB = $database->execute($sql)->fetchAll('assoc');;

            foreach ($accountsDB as $a) {

                $account = new \stdClass;
                $account->code = $a['code'];
                $account->title = $a['title'];

                $account->name = '';

                $hrchy = substr(strval($account->code), 0, 4);
                $hrchy = str_split($hrchy);

                foreach ($hrchy as $h) {
                    if ($h != '0') {
                        $account->name .= '&nbsp;&nbsp;&nbsp;'; 
                    }
                }
                
                if (!$account->title) {
                    $account->name .= '&nbsp;&nbsp;&nbsp;'; 
                }

                $account->name .= $a['name'];

                $account->description = $a['description'];
                $account->saldo = $a['saldo'];

                $accounts[] = $account;
            }
        }

        $this->set('accounts', $accounts);
    }

    /**
     * called from 
     * DiaryBook/index
    */
    public function getAccountByCodeAjax()
    {
        if ($this->request->is('Ajax')) { //Ajax Detection
            $code = $this->request->input('json_decode')->code;

            $pos = strpos($code, '-');

            $account_code = $code;

            if ($pos) {

                $code =  explode('-', $code);
                $account_code = str_pad($code[0], 4, "0", STR_PAD_RIGHT);
                $account_code .= str_pad($code[1], 5, "0", STR_PAD_LEFT);
            }

            $account = $this->Accounts->find()->where(['code' => $account_code])->first();

            if ($account) {
               $this->set('account', $account); 
            } else {
                $this->set('account', false); 
            }
        }
    }

    /**
     * called from 
     * Accounts/index
    */
    public function validateFormAjax()
    {
        if ($this->request->is('Ajax')) {
            $code = $this->request->input('json_decode')->code;

            $account = $this->Accounts->find()->where(['code' => $code])->first();

            $data = ['error' => false, 'msgError' => ''];

            if ($account) {
                $data['error'] = true;
                $data['msgError'] = "El código $code ya existe."; 
            }

            $this->set('data', $data);
        }
    }

    public function add()
    {
        if ($this->request->is('post')) {

            $account = $this->Accounts->newEntity();
            $account = $this->Accounts->patchEntity($account, $this->request->getData());

            $account->status = true;

            if ($this->Accounts->save($account)) {
                $this->Flash->success(__('Cuenta Agregada.'));
            } else {
                $this->Flash->error(__('No se pudo agregar la cuenta.'));
            }

            return $this->redirect(['action' => 'index']);
        }
    }

    public function edit()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {

            $account = $this->Accounts->find()->where(['code' => $this->request->getData('code')])->first();

            if (!$account) {
                $this->Flash->error(__('No se ecuentra la cuenta {0}', $this->request->getData('code')));
                return $this->redirect(['action' => 'index']);
            }

            $data = $this->request->getData();
            $data['status'] = true;

            $account = $this->Accounts->patchEntity($account, $this->request->getData());

            if ($this->Accounts->save($account)) {
                $this->Flash->success(__('Cuenta editada.'));
            } else {
                $this->Flash->warning(__('No se pudo editar la cuenta. Puede que el código de cuenta este asociado a un Entidad'));
            }
        }

        return $this->redirect(['action' => 'index']);
    }
}
