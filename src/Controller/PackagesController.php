<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Packages Controller
 *
 * @property \App\Model\Table\PackagesTable $Packages
 */
class PackagesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Accountant', [
            'className' => '\App\Controller\Component\Admin\Accountant'
        ]);
    }

    public function isAuthorized($user = null)
    {
        if ($this->request->getParam('action') == 'getAllAjax') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $packages = $this->Packages->find()->contain(['Accounts'])->where(['deleted' => false]);

        $this->set(compact('packages'));
        $this->set('_serialize', ['packages']);
    }

    public function getAllAjax()
    {
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $packages = $this->Packages->find()->where(['enabled' => true, 'deleted' => false]); 
            
            if (count($packages) > 0) {
                $this->set('packages',$packages );
            } else {
                $this->set('packages', false);
            }
        }
    } 

    public function view($id = null)
    {
        $package = $this->Packages->get($id, [
            'contain' => ['PackagesArticles.Articles']
        ]);

        $this->set('package', $package);
        $this->set('_serialize', ['package']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $package = $this->Packages->newEntity();

        $parament = $this->request->getSession()->read('paraments');

        //si la contabilidada esta activada registro el asientop
        if ($this->Accountant->isEnabled()) {
            if (!$parament->accountant->acounts_parent->packages) {
                $this->Flash->default(__('Error de Configuración. Falta cuenta padre de los Paquetes de Instalación'));
                return $this->redirect(['action' => 'add']);
            }
        }

        if ($this->request->is('post')) {
            
            if ($this->Packages->find()->where(['name' => $this->request->getData('name'), 'deleted' => false])->first()) {
                $this->Flash->warning(__('Existe un paquete con el mismo nombre.'));
                 return $this->redirect(['action' => 'add']);
            }
            
            $data  = $this->request->getData();

            //si la contabilidada esta activada registro el asientop
            if ($this->Accountant->isEnabled()) {

                $account = $this->Accountant->createAccount([
                    'account_parent' => $parament->accountant->acounts_parent->packages,
                    'name' => $this->request->getData('name'),
                    'redirect' => ['action' => 'add']
                ]);

                
                $data['account_code'] = $account->code;
            }

            $package = $this->Packages->patchEntity($package, $data);

            if ($this->Packages->save($package)) {

                $this->Flash->success(__('Paquete agregado.'));
                return $this->redirect(['action' => 'add']);
            } else {
                //si la contabilidada esta activada registro el asientop
                if ($this->Accountant->isEnabled()) {
                    $this->Accountant->deleteAccount($account);
                    $this->Flash->error(__('Error al agregar el paquete.'));
                }
            }
        }

        $articles = null;

        $this->set(compact('package', 'articles'));
        $this->set('_serialize', ['package']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Package id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $package = $this->Packages->get($id, [
            'contain' => ['Articles']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {

            if ($this->Packages->find()->where(['name' => $this->request->getData('name'), 'deleted' => false, 'id !=' => $id])->first()) {
                $this->Flash->warning(__('Existe un paquete con el mismo nombre.'));
                 return $this->redirect(['action' => 'edit', $package->id]);
            }

            //si la contabilidada esta activada registro el asientop
            if ($this->Accountant->isEnabled()) {
                $this->loadModel('Accounts');

                $account = $this->Accounts->find()->where(['code' => $this->request->getData('account_code')])->first();

                if (!$account) {
                    $this->Flash->warning(__('No existe el número de cuenta {0}.', $this->request->getData('account_code')));
                    return $this->redirect(['action' => 'edit', $package->id]);
                }
                
                 //edito cuenta contable del servicio
                $this->loadModel('Accounts');

                $account = $this->Accounts->find()
                    ->where([
                        'code' =>  $package->account_code,
                ])->first();

                $account->name = $package->name;

                if (!$this->Accounts->save($account)) {
                    $this->Flash->error(__('No se pudo actualizar la cuenta.'));
                }
        
                
            }

            $package = $this->Packages->patchEntity($package, $this->request->getData());

            if ($this->Packages->save($package)) {

                $this->Flash->success(__('Datos actualizados.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Error al actualizar los datos.'));
                 return $this->redirect(['action' => 'edit', $package->id]);
            }
        }

        $articles = $this->Packages->Articles->find()->toArray();

        $packagesArticles = $this->Packages->PackagesArticles->find()->where(['package_id' => $package->id]); 

        foreach ($articles as $a) {

            $a['amount'] = 0;

            foreach ($packagesArticles as $pa) {

              if ($a['id'] == $pa->article_id) {
                $a['amount'] = $pa->amount;
              }
          }
        }
 
        $this->set(compact('package', 'articles'));
        $this->set('_serialize', ['package']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Package id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);
        $package = $this->Packages->get($_POST['id']);

        $package->deleted = true;

        if ($this->Packages->save($package)) {
            $this->Flash->success(__('Paquete eliminado.'));
        } else {
            $this->Flash->error(__('Error al eliminar el paquete.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
