<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Event\Event;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\Filesystem\Folder;
use Cake\Event\EventManager;

use App\Controller\Component\ComprobantesComponent;
use App\Controller\Component\AccountantComponent;
use App\Controller\Component\PDFGeneratorComponent;

class MercadoPagoController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Accountant', [
             'className' => '\App\Controller\Component\Admin\Accountant'
        ]);

        $this->loadComponent('CurrentAccount', [
             'className' => '\App\Controller\Component\Admin\CurrentAccount'
        ]);

        $this->loadComponent('FiscoAfipComp', [
             'className' => '\App\Controller\Component\Admin\FiscoAfipComp'
        ]);

        $this->loadComponent('FiscoAfipCompPdf', [
             'className' => '\App\Controller\Component\Admin\FiscoAfipCompPdf'
        ]);

        $this->loadComponent('IntegrationRouter', [
             'className' => '\App\Controller\Component\Integrations\IntegrationRouter'
        ]);

        $this->initEventManager();
    }

    public function initEventManager()
    {
        EventManager::instance()->on(
            'MercadoPagoController.Error',
            function ($event, $msg, $data, $flash = false) {

                $this->loadModel('ErrorLog');
                $log = $this->ErrorLog->newEntity();
                $log->user_id = $this->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Error';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->ErrorLog->save($log);

                if ($flash) {
                    $this->Flash->error(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'MercadoPagoController.Warning',
            function ($event, $msg, $data, $flash = false) {

                $this->loadModel('ErrorLog');
                $log = $this->ErrorLog->newEntity();
                $log->user_id = $this->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Warning';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->ErrorLog->save($log);

                if ($flash) {
                    $this->Flash->warning(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'MercadoPagoController.Log',
            function ($event, $msg, $data) {
            }
        );

        EventManager::instance()->on(
            'MercadoPagoController.Notice',
            function ($event, $msg, $data) {
            }
        );
    }

    public function isAuthorized($user = null)
    {
        if ($this->request->getParam('action') == 'assignTransaction') {
            return true;
        }

        if ($this->request->getParam('action') == 'operacionesControl') {
            return true;
        }

        if ($this->request->getParam('action') == 'strToDeciaml') {
            return true;
        }

        if ($this->request->getParam('action') == 'generatePayment') {
            return true;
        }

		if ($this->request->getParam('action') == 'enableConnection') {
            return true;
        }

        if ($this->request->getParam('action') == 'test') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow('operacionesControl', 'syncPayment');
    }

    public function index()
    {
        $paraments = $this->request->getSession()->read('paraments');
        $payment_getway = $this->request->getSession()->read('payment_getway');
        $account_enabled = $this->Accountant->isEnabled();

        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Configuración Mercado Pago';
            $detail = "";
            $this->registerActivity($action, $detail, NULL, TRUE);

            $payment_getway->config->mercadopago->installments = $this->request->getData('payment_getway.mercadopago.installments');
            $payment_getway->config->mercadopago->mode = $this->request->getData('payment_getway.mercadopago.mode');
            $payment_getway->config->mercadopago->sandbox->public_key = $this->request->getData('sandbox-public_key');
            $payment_getway->config->mercadopago->sandbox->access_token = $this->request->getData('sandbox-access_token');
            $payment_getway->config->mercadopago->prod->public_key = $this->request->getData('prod-public_key');
            $payment_getway->config->mercadopago->prod->access_token = $this->request->getData('prod-access_token');
            $payment_getway->config->mercadopago->enabled = $this->request->getData('enabled');
            $payment_getway->config->mercadopago->cash = $this->request->getData('cash');
            $payment_getway->config->mercadopago->portal = $this->request->getData('portal');
            $payment_getway->config->mercadopago->hours_execution = $this->request->getData('hours_execution');

            $payment_getway->config->mercadopago->excluded_payment_methods = [];

            $action = 'Configuración Guardada' . PHP_EOL;
            $detail = 'Habilitar: ' . ($payment_getway->config->mercadopago->enabled ? 'Si' : 'No') . PHP_EOL;
            $detail .= 'Cuotas: ' . $payment_getway->config->mercadopago->installments . PHP_EOL;
            $detail .= 'Modo: ' . ($payment_getway->config->mercadopago->mode == 'sandbox' ? 'Testing' : 'Producción') . PHP_EOL;
            $detail .= 'Public Key (SANDBOX): ' . $payment_getway->config->mercadopago->sandbox->public_key . PHP_EOL;
            $detail .= 'Access Token (SANDBOX): ' . $payment_getway->config->mercadopago->sandbox->access_token . PHP_EOL;
            $detail .= 'Public Key (PROD): ' . $payment_getway->config->mercadopago->prod->public_key . PHP_EOL;
            $detail .= 'Access Token (PROD): ' . $payment_getway->config->mercadopago->prod->access_token . PHP_EOL;
            $detail .= 'Horario de ejecución: ' . $payment_getway->config->mercadopago->hours_execution . PHP_EOL;
            $detail .= 'Métodos de Pagos Excluídos: ' . $payment_getway->config->mercadopago->hours_execution . PHP_EOL;

            foreach ($this->request->getData('payment_getway.mercadopago.excluded_payment_methods') as $key => $excluded_payment_method) {
                if ($excluded_payment_method) {
                    $detail .= $payment_getway->config->mercadopago->payment_methods->$key . PHP_EOL;
                    array_push($payment_getway->config->mercadopago->excluded_payment_methods, $key);
                }
            }

            $detail .= 'Tipos de Pagos Excluídos: ' . PHP_EOL;

            $payment_getway->config->mercadopago->excluded_payment_types = [];

            foreach ($this->request->getData('payment_getway.mercadopago.excluded_payment_types') as $key => $excluded_payment_type) {
                if ($excluded_payment_type) {
                    $detail .= $payment_getway->config->mercadopago->payment_types->$key . PHP_EOL;
                    array_push($payment_getway->config->mercadopago->excluded_payment_types, $key);
                }
            }

            $payment_getway->config->mercadopago->CLIENT_ID = $this->request->getData('CLIENT_ID');
            $payment_getway->config->mercadopago->CLIENT_SECRET = $this->request->getData('CLIENT_SECRET');

            $detail .= 'CLIENT_ID: ' . $payment_getway->config->mercadopago->CLIENT_ID . PHP_EOL;
            $detail .= 'CLIENT_SECRET: ' . $payment_getway->config->mercadopago->CLIENT_SECRET . PHP_EOL;

            $payment_getway->config->mercadopago->automatic = $this->request->getData('automatic');
            if ($account_enabled) {
                $payment_getway->config->mercadopago->account = $this->request->getData('account');
                $detail .= 'Cuenta contable: ' . $payment_getway->config->mercadopago->account . PHP_EOL;
            }

            $this->registerActivity($action, $detail, NULL);

            //se guarda los cambios
            $this->savePaymentGetwayParaments($payment_getway);

            // PORTAL BEGIN
            // $portal = $this->request->getSession()->read('portal');

            // $portal->payment_getway->mercadopago->mode = $this->request->getData('payment_getway.mercadopago.mode');
            // $portal->payment_getway->mercadopago->sandbox->public_key = $this->request->getData('sandbox-public_key');
            // $portal->payment_getway->mercadopago->sandbox->access_token = $this->request->getData('sandbox-access_token');
            // $portal->payment_getway->mercadopago->prod->public_key = $this->request->getData('prod-public_key');
            // $portal->payment_getway->mercadopago->prod->access_token = $this->request->getData('prod-access_token');
            // $portal->payment_getway->mercadopago->enabled = $this->request->getData('portal');

            // $portal->payment_getway->mercadopago->CLIENT_ID = $this->request->getData('CLIENT_ID');
            // $portal->payment_getway->mercadopago->CLIENT_SECRET = $this->request->getData('CLIENT_SECRET');
            // $portal->portal_sync = false;

            // $this->savePortalParaments($payment_getway);
            // PORTAL END
        }
        
        $this->loadModel('Accounts');
        $accountsTitles = $this->Accounts->find()->where(['title' => true])->order(['code' => 'ASC']);
        $accounts = $this->Accounts->find()
            ->order([
                'code' => 'ASC'
            ]);

        $this->set(compact('paraments', 'accountsTitles', 'accounts', 'payment_getway', 'account_enabled'));

        $this->set('_serialize', ['paraments', 'payment_getway']);
    }

    public function operacionesControl()
    {
        $paraments = $this->request->getSession()->read('paraments');
        $payment_getway = $this->request->getSession()->read('payment_getway');

        //CONSULTAR A MERCADO PAGO LAS TRANSACCIONES
        $this->loadComponent('MercadoPago');

        $data = [
            'code'  => 200,
            'message' => 'datos general'
        ];

        $result = $this->MercadoPago->search_payment();

        $action = 'Control de Pagos MercadoPago Cron';
        $detail = '';
        $this->registerActivity($action, $detail, NULL, TRUE);

        if (array_key_exists('status', $result)) {

            if ($result['status'] == 200 || $result['status'] == 201) {

                if (array_key_exists('response', $result)) {

                    if (array_key_exists('results', $result['response'])) {

                        if (count($result['response']['results']) > 0) {

                            $this->loadModel('Users');
                            $this->loadModel('Customers');
                            $this->loadModel('Connections');
                            $this->loadModel('MpTransactions');

                            $user_sistema = 100;

                            $operations = $result['response']['results'];

                            $message = "";

                            /*{
                            	"paging": {
                            		"total": 70,
                            		"limit": 10,
                            		"offset": 0
                            	},
                            	"results": [
                            		{
                            			"id": PAYMENT_ID,
                            			"date_created": "2011-09-20T00:00:00.000-04:00",
                            			"date_approved": null,
                            			"date_last_updated": "2011-10-19T16:44:34.000-04:00",
                            			"money_release_date": "2011-10-04T17:32:49.000-04:00",
                            			"operation_type": "regular_payment",
                            			"collector_id": USER_ID,
                            			"payer": {
                            				"id": "PAYER_ID",
                            				"first_name": "Payer First name",
                            				"last_name": "Payer Last name",
                            				"phone": {
                            					"area_code": "0123",
                            					"number": "4567890",
                            					"extension": null
                            				},
                            				"email": "payer@email.com"
                            			},
                            			"external_reference": "Seller reference",
                            			"description": "Payment description",
                            			"transaction_amount": 2,
                            			"transaction_details": {
                            				"total_paid_amount": 2,
                            			},
                            			"currency_id": "ARS",
                            			"status": "cancelled",
                            			"status_detail": "expired",
                            			"payment_type_id": "ticket",
                            			...
                            		},
                            		{
                            			...
                            		}
                            	]
                            }*/

                            $bloque_id = time();

                            $customers_codes = "";
                            $first = TRUE;

                            foreach ($operations as $operation) {

                                $trasaccion = $this->MpTransactions
                                    ->find()
                                    ->select([
                                        'id',
                                        'payment_id',
                                        'status'
                                    ])
                                    ->where([
                                        'payment_id' => $operation['id']
                                    ])->first();

                                $comment_status = "";

                                switch ($operation['status']) {
                                    case 'pending':
                                        $comment_status = 'pendiente';
                                        break;
                                    case 'approved':
                                        $comment_status = 'aprobado';
                                        break;
                                    case 'authorized':
                                        $comment_status = 'autorizado';
                                        break;
                                    case 'in_process':
                                        $comment_status = 'en proceso';
                                        break;
                                    case 'in_mediation':
                                        $comment_status = 'en mediación';
                                        break;
                                    case 'rejected':
                                        $comment_status = 'rechazado';
                                        break;
                                    case 'cancelled':
                                        $comment_status = 'cancelado';
                                        break;
                                    case 'refunded':
                                        $comment_status = 'reintegrado';
                                        break;
                                    case 'charged_back':
                                        $comment_status = 'recargo';
                                        break;
                                }

                                if ($trasaccion == NULL) {

                                    $mp_transaction = $this->MpTransactions->newEntity();
                                } else {

                                    //si existe la transaccion corroboro el estado

                                    $status_mp = [
                                        'approved',
                                        'rejected',
                                        'cancelled',
                                        'refunded',
                                        'charged_back',
                                    ];

                                    // Si tiene cualquiera de los estados que están en el array status_mp
                                    // son transacciones cerradas, pero sino la transacción sigue en proceso
                                    // y se debe actualizar el estado de dicha transacción
                                    if (in_array($trasaccion->status, $status_mp)) {
                                        $message .= 'payment_id: ' . $operation['id'] . ', Estado: ' . $comment_status;
                                        continue;
                                    }

                                    // Si sigue los posibles estados de la transacciones son:
                                    // pendiente, autorizado, en proceso, en mediación

                                    $mp_transaction = $trasaccion;
                                }

                                $mp_transaction->payment_id          = $operation['id'];
                                $mp_transaction->date_created        = new Time($operation['date_created'] . ', America/Argentina/Buenos_Aires');
                                $mp_transaction->date_approved       = $operation['date_approved'] ? new Time($operation['date_approved'] . ', America/Argentina/Buenos_Aires') : NULL;
                                $mp_transaction->date_last_updated   = $operation['date_last_updated'] ? new Time($operation['date_last_updated'] . ', America/Argentina/Buenos_Aires') : NULL;
                                $mp_transaction->money_release_date  = $operation['money_release_date'] ? new Time($operation['money_release_date'] . ', America/Argentina/Buenos_Aires') : NULL;
                                $mp_transaction->operation_type      = $operation['operation_type'];
                                if (array_key_exists('payer', $operation)) {
                                    $mp_transaction->payer_email = $operation['payer']['email'];
                                } else {
                                    $mp_transaction->payer_email = NULL;
                                }
                                $mp_transaction->description         = $operation['description'];
                                $mp_transaction->transaction_amount  = $this->strToDeciaml($operation['transaction_amount']);
                                $mp_transaction->total_paid_amount   = $this->strToDeciaml($operation['transaction_details']['total_paid_amount']);
                                $mp_transaction->currency_id         = $operation['currency_id'];
                                $mp_transaction->status              = $operation['status'];
                                $mp_transaction->status_detail       = $operation['status_detail'];
                                $mp_transaction->payment_type_id     = $operation['payment_type_id'];
                                $mp_transaction->external_reference  = $operation['external_reference'];
                                $comision = 0;
                                if (count($operation['fee_details']) > 0) {
                                    $comision = $operation['fee_details'][0]['amount'];
                                }
                                $mp_transaction->comision            = $this->strToDeciaml($comision);
                                $mp_transaction->net_received_amount = $this->strToDeciaml($operation['transaction_details']['net_received_amount']);
                                $mp_transaction->local_status        = FALSE;
                                $mp_transaction->bloque_id           = $bloque_id;
                                $mp_transaction->payment_getway_id   = $payment_getway->methods->mercadopago->id;
                                $this->MpTransactions->save($mp_transaction);

                                $connection_id = NULL;

                                if ($operation['external_reference'] == NULL) {
                                    $mp_transaction->comment = "No existe referencia de cliente - Estado: " . $comment_status;
                                    $this->MpTransactions->save($mp_transaction);
                                    continue;
                                }

                                if ($paraments->gral_config->billing_for_service) {

                                    $reference = explode("-", $mp_transaction->external_reference);
                                    if ($reference[0] == 'servicio') {

                                        $connection_id = $reference[1];

                                        if (empty($connection_id)) {
                                            $mp_transaction->comment = "No existe referencia de cliente - Estado: " . $comment_status;
                                            $this->MpTransactions->save($mp_transaction);
                                            continue;
                                        }

                                        $connection = $this->Connections
                                            ->find()
                                            ->contain([
                                                'Customers'
                                            ])
                                            ->where([
                                                'id' => $connection_id
                                            ])
                                            ->first();

                                        if ($connection) {

                                            if ($connection->deleted) {

                                                $connection_id = NULL;
                                                $customer = $connection->customer;

                                                if (empty($customer)) {

                                                    $mp_transaction->comment = 'No existe cliente o cliente eliminado - Estado: ' . $comment_status;
                                                    $this->MpTransactions->save($mp_transaction);
                                                    continue;
                                                }
                                            }

                                            $customer = $connection->customer;

                                        } else {
                                            $mp_transaction->comment = "No existe referencia de cliente - Estado: " . $comment_status;
                                            $this->MpTransactions->save($mp_transaction);
                                            continue;
                                        }

                                        if ($mp_transaction->status != 'approved') {
                                            continue;
                                        }

                                        $this->generatePayment($customer, $mp_transaction, $connection_id, $user_sistema);

                                    } else {

                                        $customer_code = array_key_exists(1, $reference) ? $reference[1] : FALSE;;

                                        if (empty($customer_code)) {
                                            $mp_transaction->comment = "No existe referencia de cliente - Estado: " . $comment_status;
                                            $this->MpTransactions->save($mp_transaction);
                                            continue;
                                        }

                                        $customer = $this->Customers
                                            ->find()
                                            ->where([
                                                'code' => $customer_code
                                            ])
                                            ->first();

                                        //si no existe un cliente al cual asociar el pago de esta transaccion no se pude procesar

                                        if (empty($customer)) {
                                            $mp_transaction->comment = 'No existe cliente o cliente eliminado -  Estado: ' . $comment_status;
                                            $this->MpTransactions->save($mp_transaction);
                                            continue;
                                        }

                                        if ($mp_transaction->status != 'approved') {
                                            $customer_name = str_pad($customer->code, 5, "0", STR_PAD_LEFT) . ' ' . $customer->name;
                                            $mp_transaction->comment = 'Cliente: ' . $customer_name . ' - Estado: ' . $comment_status;
                                            $this->MpTransactions->save($mp_transaction);
                                            continue;
                                        }

                                        $this->generatePayment($customer, $mp_transaction, $connection_id, $user_sistema);
                                    }
                                } else {

                                    $customer_code  = $mp_transaction->external_reference;

                                    if (empty($customer_code)) {
                                        $mp_transaction->comment = "No existe referencia de cliente - Estado: " . $comment_status;
                                        $this->MpTransactions->save($mp_transaction);
                                        continue;
                                    }

                                    if (!is_numeric($customer_code)) {

                                        $mp_transaction->comment = "No existe referencia de cliente - Estado: " . $comment_status;
                                        $this->MpTransactions->save($mp_transaction);
                                        continue;
                                    }

                                    $customer = $this->Customers
                                        ->find()
                                        ->contain([
                                            'Connections'
                                        ])
                                        ->where([
                                            'code' => $customer_code
                                        ])
                                        ->first();

                                    //si no existe un cliente al cual asociar el pago de esta transaccion no se pude procesar

                                    if (empty($customer)) {

                                        $mp_transaction->comment = 'No existe cliente o cliente eliminado - Estado: ' . $comment_status;
                                        $this->MpTransactions->save($mp_transaction);
                                        continue;
                                    }

                                    if ($mp_transaction->status != 'approved') {
                                        $customer_name = str_pad($customer->code, 5, "0", STR_PAD_LEFT) . ' ' . $customer->name;
                                        $mp_transaction->comment = 'Cliente: ' . $customer_name . ' - Estado: ' . $comment_status;
                                        $this->MpTransactions->save($mp_transaction);
                                        continue;
                                    }

                                    $this->generatePayment($customer, $mp_transaction, $connection_id, $user_sistema);
                                }

                                if ($first) {
                                    $customers_codes .= $customer_code;
                                    $first = FALSE;
                                } else {
                                    $customers_codes .= '.' . $customer_code;
                                }
                            }

                            $message .= 'Pagos registrados';

                        } else {
                            $message .= "No hay datos";
                            Log::info($message, ['scope' => ['mercadopago']]);
                        }
                    } else {
                        $message .= "Error index results.";
                        Log::info($message, ['scope' => ['mercadopago']]);
                    }
                } else {
                    $message .= "Error index response.";
                    Log::info($message, ['scope' => ['mercadopago']]);
                }
            } else {
                $message .= "Error status != 200 y status != 201";
                Log::info($message, ['scope' => ['mercadopago']]);
            }
        } else {
            $message .= "Error sin status";
            Log::info($message, ['scope' => ['mercadopago']]);
        }

        if ($customers_codes != "") {
            $command = ROOT . "/bin/cake debtMonthControlAfterAction $customers_codes 1 > /dev/null &";
            $result = exec($command);
        }

        $data['message'] = $message;

        return json_encode($data);
    }

    private function generatePayment($customer, $mp_transaction, $connection_id = NULL, $user_sistema = 100)
    {
        $this->loadModel('Payments');
        $this->loadModel('MpTransactions');
        $payment_getway = $this->request->getSession()->read('payment_getway');
        $paraments = $this->request->getSession()->read('paraments');

        $seating = NULL;

        //proceso la transaccion ....

        //si la contabilidada esta activada registro el asiento
        if ($this->Accountant->isEnabled()) {

            $seating = $this->Accountant->addSeating([
                'concept'       => 'Por Cobranza (mercadopago)',
                'comments'      => '',
                'seating'       => NULL, 
                'account_debe'  => $payment_getway->config->mercadopago->account,
                'account_haber' => $customer->account_code,
                'value'         => $mp_transaction->total_paid_amount,
                'user_id'       => $user_sistema,
                'created'       => Time::now()
            ]);
        }

        $data_payment = [
            'import'             => $mp_transaction->total_paid_amount,
            'created'            => $mp_transaction->date_created,
            'cash_entity_id'     => NULL,
            'user_id'            => $user_sistema,
            'seating_number'     => $seating ? $seating->number : NULL,
            'customer_code'      => $customer->code,
            'payment_method_id'  => $payment_getway->methods->mercadopago->id, //id = 99 metodo de pago -> cobrodigital
            'concept'            => $mp_transaction->description ? $mp_transaction->description : 'Pago desde MercadoPago',
            'connection_id'      => $connection_id,
            'number_transaction' => $mp_transaction->payment_id,
            'account_id'         => NULL
        ];

        $payment = $this->Payments->newEntity();

        $payment = $this->Payments->patchEntity($payment, $data_payment);

        if ($payment) {

            if ($this->Payments->save($payment)) {

                //genero el recibo del pago
                $receipt = $this->FiscoAfipComp->receipt($payment, FALSE);

                if ($receipt) {

                    $mp_transaction->comment .= ' - procesado correctamente ';
                    $mp_transaction->local_status = TRUE;
                    $mp_transaction->receipt_number = str_pad($receipt->pto_vta, 4, "0", STR_PAD_LEFT) . '-' . str_pad($receipt->num, 8, "0", STR_PAD_LEFT);;
                    $mp_transaction->receipt_id = $receipt->id;
                    $mp_transaction->customer_code = $customer->code;
                    $mp_transaction->connection_id = $connection_id;

                    $this->MpTransactions->save($mp_transaction);

                    //habilito la conexion relacionada al serivicio que esta pagando
                    $this->enableConnection($payment, $customer);

                    $afip_codes = $this->request->getSession()->read('afip_codes');

                    $this->loadModel('Users');
                    $user = $this->Users->get($user_sistema);
                    $user_name = $user->name . ' - username: ' . $user->username;

                    $customer_name = $customer->name . ' - Código: ' . $customer->code . ' - ' . $afip_codes['doc_types'][$customer->doc_type] . ': ' . $customer->ident;
                    $customer_code = $customer->code;

                    if ($paraments->invoicing->email_emplate_receipt != "") {

                        $controllerClass = 'App\Controller\MassEmailsController';

                        $mass_email = new $controllerClass;
                        if ($mass_email->sendEmailReceipt($receipt->id, $paraments->invoicing->email_emplate_receipt)) {

                            $action = 'Envío recibo al cliente';
                            $detail = 'Cliente: ' . $customer_name . PHP_EOL;
                            $detail .= 'Usuario: ' . $user_name . PHP_EOL;

                            $detail .= '---------------------------' . PHP_EOL;

                            $this->registerActivity($action, $detail, $customer_code);
                        } else {

                            $action = 'No envío recibo al cliente';
                            $detail = 'Cliente: ' . $customer_name . PHP_EOL;
                            $detail .= 'Usuario: ' . $user_name . PHP_EOL;

                            $detail .= '---------------------------' . PHP_EOL;

                            $this->registerActivity($action, $detail, $customer_code);
                        }
                    }

                } else {

                    $data_error = new \stdClass; 
                    $data_error->request_data = $data_payment;

                    $event = new Event('MercadoPagoController.Error', $this, [
                        'msg' => __('Hubo un error al intentar generar el recibo. Verifique los valores cargados.'),
                        'data' => $data_error,
                        'flash' => true
                    ]);

                    $this->getEventManager()->dispatch($event);

                    Log::error($receipt->getErrors(), ['scope' => ['mercadopago']]);

                    //rollback payment
                    $this->Payments->delete($payment);
                    //rollback seating
                    $this->Accountant->deleteSeating($seating);

                }
            } else {
                Log::error($payment->getErrors(), ['scope' => ['mercadopago']]);
            }
        } else {

            Log::error($payment->getErrors(), ['scope' => ['mercadopago']]);
        }
    }

    public function assignTransaction()
    {
        //ajax Detection
        if ($this->request->is('ajax')) {

            $transaction_id = $this->request->input('json_decode')->transaction_id;
            $customer_code = $this->request->input('json_decode')->customer_code;
            $paraments = $this->request->getSession()->read('paraments');

            $response = [
                'type'    => "error",
                'message' => "No se ha asignado el pago al cliente.",
                'error'   => true
            ];

            $this->loadModel('Customers');
            $customer = $this->Customers
                ->find()
                ->where([
                    'code' => $customer_code
                ])->first();

            if ($customer) {

                $this->loadModel('MpTransactions');
                $mp_transaction = $this->MpTransactions
                    ->find()
                    ->where([
                        'id' => $transaction_id
                    ])->first();

                if ($mp_transaction) {

                    $connection_id = NULL;

                    if ($paraments->gral_config->billing_for_service) {

                        $reference = explode("-", $mp_transaction->external_reference);

                        if ($reference[0] == 'servicio') {
                            $connection_id = $reference[1];
                        }
                    }

                    $this->generatePayment($customer, $mp_transaction, $connection_id);

                    $detail = "";
                    $detail .= 'Medio: Mercado Pago' . PHP_EOL;
                    $detail .= $data . PHP_EOL; 
                    $action = 'Asignación de Pago Manual - Mercado Pago';
                    $this->registerActivity($action, $detail, $customer_code, TRUE);

                    $response = [
                        'type'    => "success",
                        'message' => "Pago asignado correctamente.",
                        'error'   => false
                    ];

                    $command = ROOT . "/bin/cake debtMonthControlAfterAction $customer_code 1 > /dev/null &";
                    $result = exec($command);
                }
            }

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    private function strToDeciaml($value)
    {
       return floatval($value);
    }

    private function enableConnection($payment, $customer)
    {
        $paraments = $this->request->getSession()->read('paraments');

        $this->loadModel('Connections');

        $connections = [];

        if ($customer->billing_for_service) {

             if ($payment->connection_id) {

                 $connections = $this->Connections->find()->contain(['Customers', 'Controllers', 'Services'])
                    ->where([
                        'Connections.id' => $payment->connection_id,
                        'Connections.deleted' => false,
                        'Connections.enabled' => false
                    ]);
             }

        } else {

            $connections = $this->Connections->find()->contain(['Customers', 'Controllers', 'Services'])
                ->where([
                    'Connections.customer_code' => $customer->code,
                    'Connections.deleted' => false,
                    'Connections.enabled' => false
                ]);
        }

        foreach ($connections as $connection) {

            if ($this->IntegrationRouter->enableConnection($connection, true)) {

                $connection->enabled = true;
                $connection->enabling_date = Time::now();

                if ($this->Connections->save($connection)) {

                    $this->loadModel('ConnectionsHasMessageTemplates');
                    $chmt =  $this->ConnectionsHasMessageTemplates->find()
                        ->where([
                            'connections_id' => $connection->id, 
                            'type' => 'Bloqueo'])
                        ->first();

                    if (!$this->ConnectionsHasMessageTemplates->delete($chmt)) {
                        //$this->Flash->error(__('No se pudo eliminar el aviso de bloqueo.'));
                    }

                    $customer_code = $connection->customer_code;

                    $detail = "";
                    $detail .= 'Conexión: '. $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL; 
                    $action = 'Habilitación de Conexión - Imputación de pago automático MercadoPago';
                    $this->registerActivity($action, $detail, $customer_code);
                } else {
                    //$this->Flash->error(__('No se pudo Habilitar la conexión.'));
                }
            }
        }
    }

    public function test()
    {
        //CONSULTAR A MERCADO PAGO LAS TRANSACCIONES
        $this->loadComponent('MercadoPago');

        $messagemp = "No pudo comunicarse";

        $result = $this->MercadoPago->search_payment();

        if (array_key_exists('status', $result)) {

            if ($result['status'] == 200 || $result['status'] == 201) {

                if (array_key_exists('response', $result)) {

                    if (array_key_exists('results', $result['response'])) {

                        $messagemp = "OK";
                    }
                }
            }
        }

        $this->set('messagemp', $messagemp);
    }

    public function forceExecution()
    {
        if ($this->request->is('post')) {

            $action = 'Ejecución de control de pagos forzada - Mercado Pago';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $command = ROOT . "/bin/cake MercadoPagoControl -f > /dev/null &";
            $result = exec($command, $output);
            $this->Flash->success(__('Consulta ejecutada. ' . $result));
            return  $this->redirect($this->referer());
        }
    }

    public function cleanTransactions()
    {
        if ($this->request->is('post')) {

            $action = 'Borrado de transacciones con estado sin asignar - Mercado Pago';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $this->loadModel('MpTransactions');
            if ($this->MpTransactions->deleteAll(['local_status' => 0])) {
                $this->Flash->success(__('Se han borrado correctamente las transacciones con estado: sin asignar.'));
            } else {
                $this->Flash->error(__('No se han podido borrar las transacciones con estaod: sin asignar.'));
            }
            
            return  $this->redirect($this->referer());
        }
    }
}
