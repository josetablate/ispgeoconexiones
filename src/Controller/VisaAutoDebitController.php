<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Event\Event;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\Filesystem\Folder;
use Cake\Event\EventManager;

use App\Controller\Component\ComprobantesComponent;
use App\Controller\Component\AccountantComponent;
use App\Controller\Component\PDFGeneratorComponent;

class VisaAutoDebitController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Accountant', [
             'className' => '\App\Controller\Component\Admin\Accountant'
        ]);

        $this->loadComponent('CurrentAccount', [
             'className' => '\App\Controller\Component\Admin\CurrentAccount'
        ]);

        $this->loadComponent('FiscoAfipComp', [
             'className' => '\App\Controller\Component\Admin\FiscoAfipComp'
        ]);

        $this->loadComponent('IntegrationRouter', [
             'className' => '\App\Controller\Component\Integrations\IntegrationRouter'
        ]);

        $this->initEventManager();
    }

    public function initEventManager()
    {
        EventManager::instance()->on(
            'VisaAutoDebitController.Error',
            function ($event, $msg, $data, $flash = false) {

                $this->loadModel('ErrorLog');
                $log = $this->ErrorLog->newEntity();
                $log->user_id = $this->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Error';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->ErrorLog->save($log);

                if ($flash) {
                    $this->Flash->error(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'VisaAutoDebitController.Warning',
            function ($event, $msg, $data, $flash = false) {

                $this->loadModel('ErrorLog');
                $log = $this->ErrorLog->newEntity();
                $log->user_id = $this->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Warning';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->ErrorLog->save($log);

                if ($flash) {
                    $this->Flash->warning(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'VisaAutoDebitController.Log',
            function ($event, $msg, $data) {
                
            }
        );

        EventManager::instance()->on(
            'VisaAutoDebitController.Notice',
            function ($event, $msg, $data) {

            }
        );
    }

    public function isAuthorized($user = null) 
    {
        if ($this->request->getParam('action') == 'retrieves') {
            return true;
        }

        if ($this->request->getParam('action') == 'retrievesDebts') {
            return true;
        }

        if ($this->request->getParam('action') == 'acceptPayment') {
            return true;
        }

        if ($this->request->getParam('action') == 'editAcceptPayment') {
            return true;
        }

        if ($this->request->getParam('action') == 'rejectPayment') {
            return true;
        }

        if ($this->request->getParam('action') == 'editRejectPayment') {
            return true;
        }

        if ($this->request->getParam('action') == 'assignTransaction') {
            return true;
        }

        if ($this->request->getParam('action') == 'processTransactions') {
            return true;
        }

        if ($this->request->getParam('action') == 'generatePayment') {
            return true;
        }

        if ($this->request->getParam('action') == 'strToDeciaml') {
            return true;
        }

        if ($this->request->getParam('action') == 'enableConnection') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function index()
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');
        $account_enabled = $this->Accountant->isEnabled();

        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Configuración Visa Débito Automático';
            $detail = "";
            $this->registerActivity($action, $detail, NULL, TRUE);

            $payment_getway->config->visa_auto_debit->enabled = $this->request->getData('enabled');
            $payment_getway->config->visa_auto_debit->cash = $this->request->getData('cash');
            $payment_getway->config->visa_auto_debit->portal = $this->request->getData('portal');
            $payment_getway->config->visa_auto_debit->automatic = $this->request->getData('automatic');

            $action = 'Configuración Guardada' . PHP_EOL;
            $detail = 'Habilitar: ' . ($payment_getway->config->visa_auto_debit->enabled ? 'Si' : 'No') . PHP_EOL;

            if ($account_enabled) {
                $payment_getway->config->visa_auto_debit->account = $this->request->getData('account');
                $detail .= 'Cuenta contable: ' . $payment_getway->config->visa_auto_debit->account . PHP_EOL;
            }
            
            $this->registerActivity($action, $detail, NULL);

            $this->savePaymentGetwayParaments($payment_getway);
        }

        $this->loadModel('Accounts');
        $accountsTitles = $this->Accounts->find()->where(['title' => TRUE])->order(['code' => 'ASC']);
        $accounts = $this->Accounts
            ->find()
            ->order([
                'code' => 'ASC'
            ]);

        $this->set(compact('payment_getway', 'accountsTitles', 'accounts', 'account_enabled'));
        $this->set('_serialize', ['payment_getway', 'accounts']);
    }

    public function createAccount()
    {
        if ($this->request->is('post')) {

            $payment_getway = $this->request->getSession()->read('payment_getway');
            $paraments = $this->request->getSession()->read('paraments');

            if ($payment_getway->config->visa_auto_debit->enabled) {

                $data = $this->request->getData();
                $card_number = $data['card_number'];
                $customer_code = $data['customer_code'];

                $this->loadModel('CustomersAccounts');
                $customers_accounts_visa_auto_debit = $this->CustomersAccounts
                    ->find()
                    ->where([
                        'customer_code'     => $customer_code,
                        'deleted'           => FALSE,
                        'payment_getway_id' => 106,
                    ]);

                $where = [
                    'customer_code' => $customer_code,
                    'deleted'       => FALSE,
                ];

                $auto_debit = FALSE;
                $customers_accounts_auto_debit = [];

                foreach ($payment_getway->methods as $pg) {
                    $config = $pg->config;
                    if ($payment_getway->config->$config->enabled) {
                        if ($pg->auto_debit) {
                            $auto_debit = TRUE;

                            if (!array_key_exists('OR', $where)) {
                                $where['OR'] = [];
                            }

                            $where['OR'][] = [
                                'payment_getway_id' => $pg->id
                            ];
                        }
                    }
                }

                if ($auto_debit) {
                    $customers_accounts_auto_debit = $this->CustomersAccounts
                        ->find()
                        ->where($where)
                        ->toArray();
                }

                if ($customers_accounts_visa_auto_debit->count() > 0) {
                    $this->Flash->warning(__('Únicamente puede tener una Cuenta por Cliente. Este Cliente posee una Cuenta del mismo tipo activa, en caso de querer crear una nueva Cuenta deberá "deshabilitar" la actual Cuenta activa del Cliente'));
                    return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
                }

                if (sizeof($customers_accounts_auto_debit) > 0) {
                    $this->Flash->warning(__('El Cliente tiene una Cuenta de Débito Automático activa, esto inhabilita la creación de otra Cuenta, para poder crear otras Cuentas deberá "deshabilitar" dicha Cuenta de Débito Automático.'));
                    return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
                }

                $this->loadModel('VisaAutoDebitAccounts');

                $visa_auto_debit_account = $this->VisaAutoDebitAccounts
                    ->find()
                    ->where([
                        'customer_code'     => $customer_code,
                        'payment_getway_id' => 106,
                        'deleted'           => FALSE
                    ]);

                if ($visa_auto_debit_account->count() > 0) {
                    $this->Flash->warning(__('Únicamente puede tener una Cuenta de Visa Débito Automático por Cliente.'));
                    return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
                }

                $card = $this->VisaAutoDebitAccounts
                    ->find()
                    ->where([
                        'card_number' => $card_number
                    ])->first();

                if ($card == NULL) {
                    $card = $this->VisaAutoDebitAccounts->newEntity();
                    $card->card_number = $card_number;
                } else if ($card->used) {
                    $this->Flash->warning(__('El número de tarjeta: ' . $card_number . '. Está siendo utilizada por una cuenta actualmente.'));
                    return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
                }

                $card->customer_code = $customer_code;
                $card->asigned = Time::now();
                $card->payment_getway_id = 106;

                if ($this->VisaAutoDebitAccounts->save($card)) {

                    $detail = "";
                    $detail .= 'Nro: ' . $card_number;
                    $action = 'Creación de Cuenta Débito Automático - Visa';
                    $this->registerActivity($action, $detail, $data['customer_code'], TRUE);

                    $this->Flash->success(__('Cuenta creada correctamente.'));

                    $this->loadModel('CustomersAccounts');
                    $cutomer_account = $this->CustomersAccounts->newEntity();
                    $cutomer_account->customer_code = $customer_code;
                    $cutomer_account->account_id = $card->id;
                    $cutomer_account->payment_getway_id = 106;
                    $this->CustomersAccounts->save($cutomer_account);

                    if ($paraments->gral_config->billing_for_service) {

                        $this->loadModel('Customers');
                        $customer = $this->Customers->get($customer_code, [
                            'contain' => ['Connections']
                        ]);

                        if (sizeof($customer->connections) > 0) {

                            $this->loadModel('Connections');

                            foreach ($customer->connections as $connection) {
                                $connection->visa_auto_debit = TRUE;
                                $this->Connections->save($connection);
                            }
                        }
                    }
                }

            } else {
                $this->Flash->warning(__('Debe habilitar Visa Débito Automático.'));
            }
        }

        return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
    }

    public function showAccounts()
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');

        if (!$payment_getway->config->visa_auto_debit->enabled) {

            $this->Flash->warning('Debe habilitar Visa Débito Automático');
            return $this->redirect(['controller' => 'PaymentMethods', 'action' => 'index']);
        }
    }

    public function retrieves()
    {
        if ($this->request->is('ajax')) { //Ajax Detection 

            $payment_getway = $this->request->getSession()->read('payment_getway');
            $this->loadModel('VisaAutoDebitAccounts');

            $payment_getway_id = $this->request->getQuery()['payment_getway_id'];
            $deleted = $this->request->getQuery()['deleted'];

            $where = [
                'VisaAutoDebitAccounts.deleted'           => $deleted,
                'VisaAutoDebitAccounts.payment_getway_id' => $payment_getway_id
            ];

            $response = new \stdClass();

            $response->draw = intval($this->request->getQuery('draw'));

            $response->recordsTotal = $this->VisaAutoDebitAccounts->find()->where($where)->count();

            $response->data = $this->VisaAutoDebitAccounts->find('ServerSideData', [
                'params' => $this->request->getQuery(),
                'where'  => $where
            ]);

            $response->recordsFiltered = $this->VisaAutoDebitAccounts->find('RecordsFiltered', [
                'params' => $this->request->getQuery(),
                'where'  => $where
            ]);

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function disableAccount()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {

            $data = $this->request->getData();
            $payment_getway = $this->request->getSession()->read('payment_getway');
            $paraments = $this->request->getSession()->read('paraments');
            $account = NULL;

            foreach ($payment_getway->methods as $key => $pg) {

                if ($pg->id == $data['payment_getway_id']) {

                    $config = $pg->config;
                    $model = $payment_getway->config->$config->model;
                    $this->loadModel($model);
                    $account = $this->$model
                        ->find()
                        ->where([
                            'customer_code'     => $data['customer_code'],
                            'id'                => $data['account_id'],
                            'payment_getway_id' => $data['payment_getway_id']
                        ])->first();
                    $account->deleted = TRUE;
                    if ($this->$model->save($account)) {
                        $card_number = $account->card_number;
                        $detail = "";
                        $detail .= 'Nro: ' . $card_number;
                        $action = 'Deshabilitación de Cuenta Débito Automático - Visa';
                        $this->registerActivity($action, $detail, $data['customer_code'], TRUE);
                    }
                    break;
                }
            }

            $this->loadModel('CustomersAccounts');

            $customer_account = $this->CustomersAccounts
                ->find()
                ->where([
                    'customer_code'     => $data['customer_code'],
                    'account_id'        => $data['account_id'],
                    'payment_getway_id' => $data['payment_getway_id']
                ])
                ->first();

            $customer_account->deleted = TRUE;

            if ($this->CustomersAccounts->save($customer_account)) {

                if ($paraments->gral_config->billing_for_service) {

                    $this->loadModel('Customers');
                    $customer = $this->Customers->get($data['customer_code'], [
                        'contain' => ['Connections']
                    ]);

                    if (sizeof($customer->connections) > 0) {

                        $this->loadModel('Connections');
                        foreach ($payment_getway->methods as $key => $pg) {
                            if ($pg->id == $data['payment_getway_id']) {
                                $attribute = $key;
                            }
                        }

                        foreach ($customer->connections as $connection) {
                            $connection->$attribute = FALSE;
                            $this->Connections->save($connection);
                        }
                    }
                }
                $this->Flash->success(__('Se ha deshabilitar correctamente'));
            } else {
                $this->Flash->warning(__('Error, al deshabilitar. Por favor intente nuevamente.'));
            }

            return $this->redirect(['action' => 'showAccounts']);
        }
    }

    public function showAccountsDisabled()
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');

        if (!$payment_getway->config->visa_auto_debit->enabled) {

            $this->Flash->warning('Debe habilitar Visa Débito Automático');
            return $this->redirect(['controller' => 'PaymentMethods', 'action' => 'index']);
        }
    }

    public function enableAccount()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {

            $data = $this->request->getData();
            $payment_getway = $this->request->getSession()->read('payment_getway');
            $paraments = $this->request->getSession()->read('paraments');
            $account = NULL;

            foreach ($payment_getway->methods as $key => $pg) {

                if ($pg->id == $data['payment_getway_id']) {

                    $config = $pg->config;
                    $model = $payment_getway->config->$config->model;
                    $this->loadModel($model);
                    $account = $this->$model
                        ->find()
                        ->where([
                            'customer_code'     => $data['customer_code'],
                            'id'                => $data['account_id'],
                            'payment_getway_id' => $data['payment_getway_id']
                        ])->first();
                    $account->deleted = FALSE;
                    if ($this->$model->save($account)) {
                        $card_number = $account->card_number;
                        $detail = "";
                        $detail .= 'Nro: ' . $card_number;
                        $action = 'Habilitación de Cuenta Débito Automático - Visa';
                        $this->registerActivity($action, $detail, $data['customer_code'], TRUE);
                    }
                    break;
                }
            }

            $this->loadModel('CustomersAccounts');

            $customer_account = $this->CustomersAccounts
                ->find()
                ->where([
                    'customer_code'     => $data['customer_code'],
                    'account_id'        => $data['account_id'],
                    'payment_getway_id' => $data['payment_getway_id']
                ])
                ->first();

            $customer_account->deleted = FALSE;

            if ($this->CustomersAccounts->save($customer_account)) {

                if ($paraments->gral_config->billing_for_service) {

                    $this->loadModel('Customers');
                    $customer = $this->Customers->get($data['customer_code'], [
                        'contain' => ['Connections']
                    ]);

                    if (sizeof($customer->connections) > 0) {

                        $this->loadModel('Connections');
                        foreach ($payment_getway->methods as $key => $pg) {
                            if ($pg->id == $data['payment_getway_id']) {
                                $attribute = $key;
                            }
                        }

                        foreach ($customer->connections as $connection) {
                            $connection->$attribute = TRUE;
                            $this->Connections->save($connection);
                        }
                    }
                }
                $this->Flash->success(__('Se ha habilitado correctamente'));
            } else {
                $this->Flash->warning(__('Error, al habilitar. Por favor intente nuevamente.'));
            }

            return $this->redirect(['action' => 'showAccountsDisabled']);
        }
    }

    public function exportDebts()
    {
        if ($this->request->is('post')) {

            $error = $this->isReloadPage();

            if (!$error) {

                $paraments = $this->request->getSession()->read('paraments');
                $afip_codes =  $this->request->getSession()->read('afip_codes');

                $date_start = explode('/', $this->request->getData('date_start'));
                $date_start_query = $date_start[2] . '-' . $date_start[1] . '-' . $date_start[0] . ' 00:00:00';
    
                $date_end = explode('/', $this->request->getData('date_end'));
                $date_end_query = $date_end[2] . '-' . $date_end[1] . '-' . $date_end[0] . ' 23:59:59';

                $this->loadModel('Invoices');
                $this->loadModel('VisaAutoDebitRecords');

                // Traigo todos los registros del periodo para ver si alguno ya se proceso (aceptado o rechazado)
                $visa_auto_debit_records = $this->VisaAutoDebitRecords
                    ->find()
                    ->select([
                        'id',
                        'invoice_id',
                        'status'
                    ])
                    ->where([
                        'date_start >=' => $date_start_query,
                        'date_end <='   => $date_end_query
                    ]);

                $invoices_ids = [];
                $visa_auto_debit_records_ids = [];

                $where = [
                    'total >'       => 0,
                    'date_start >=' => $date_start_query,
                    'date_end <='   => $date_end_query
                ];

                if ($visa_auto_debit_records->count() > 0) {
                    //recorro los registros para ver si estan procesados
                    foreach ($visa_auto_debit_records as $visa_auto_debit_record) {
                        //status = -1 no se proceso, los guardo para borrarlos y volver a generar
                        if ($visa_auto_debit_record->status == -1) {
                            array_push($visa_auto_debit_records_ids, $visa_auto_debit_record->id);
                        } else {
                            array_push($invoices_ids, $visa_auto_debit_record->invoice_id);
                        }
                    }

                    if (sizeof($invoices_ids) > 0) {
                        //guardo los ids de facturas que no se procesaron para re generar solamente estas, 
                        //las que fueron procesadas ya no se regenarian
                        $where['Invoices.id NOT IN'] = $invoices_ids;
                    }

                    if (sizeof($visa_auto_debit_records_ids) > 0) {
                        foreach ($visa_auto_debit_records_ids as $visa_auto_debit_record_id) {
                            //borro el registro por que se va a crear nuevos
                            $visa_auto_debit = $this->VisaAutoDebitRecords->get($visa_auto_debit_record_id);
                            $this->VisaAutoDebitRecords->delete($visa_auto_debit);
                        }
                    } else {
                        $this->Flash->warning('Ya fue generada la Deuda del Período. Desde: ' . $this->request->getData('date_start') . ' - Hasta: ' . $this->request->getData('date_end'));
                        return $this->redirect(['action' => 'exportDebts']);
                    }
                }

                $invoices = $this->Invoices
                    ->find()
                    ->matching('Customers.VisaAutoDebitAccounts', function ($q) {
                        return $q->where([
                            'VisaAutoDebitAccounts.payment_getway_id' => 106, 
                            'VisaAutoDebitAccounts.deleted'           => 0
                        ]);
                    })
                    ->contain([
                        'Concepts'
                    ])
                    ->where($where);

                $debts_lala[] = [
                    'Fecha',        //0
                    'Desde',        //1
                    'Hasta',        //2
                    'Nro Tarjeta',  //3
                    'Nombre',       //4
                    'Documento',    //5
                    'Cód.',         //6
                    'Ciudad',       //7
                    'Nro Factura',  //8
                    'Vencimiento',  //9
                    'Total',        //10
                    'Empresa'       //11
                ];
                $date = Time::now()->format('d/m/Y');

                foreach ($invoices as $invoice) {

                    $visa_auto_debit_account = $invoice->_matchingData['VisaAutoDebitAccounts'];

                    $visa_auto_debit_record = $this->VisaAutoDebitRecords->newEntity();
                    $visa_auto_debit_record->date_start = $invoice->date_start;
                    $visa_auto_debit_record->date_end = $invoice->date_end;
                    $visa_auto_debit_record->card_number = $visa_auto_debit_account->card_number;
                    $visa_auto_debit_record->customer_name = $invoice->customer_name;
                    $visa_auto_debit_record->customer_code = $invoice->customer_code;
                    $visa_auto_debit_record->customer_doc_type = $invoice->customer_doc_type;
                    $visa_auto_debit_record->customer_ident = $invoice->customer_ident;
                    $visa_auto_debit_record->invoice_num = $invoice->num;
                    $visa_auto_debit_record->invoice_id = $invoice->id;
                    $visa_auto_debit_record->comment = $invoice->comments;
                    $visa_auto_debit_record->duedate = $invoice->duedate;
                    $visa_auto_debit_record->total = $invoice->total;
                    $visa_auto_debit_record->customer_city = $invoice->customer_city;
                    $visa_auto_debit_record->business_id = $invoice->business_id;
                    $visa_auto_debit_record->payment_getway_id = 106;
                    $visa_auto_debit_record->status = -1;
                    $visa_auto_debit_record->account_id = $visa_auto_debit_account->id;

                    $this->VisaAutoDebitRecords->save($visa_auto_debit_record);

                    /*
                        fecha
                        desde
                        hasta
                        nro_tarjeta
                        nombre_cliente
                        doc_cliente
                        cod_cliente
                        ciudad_cliente
                        num_factura
                        vencimiento
                        total
                        empresa
                    */

                    $doc_customer = $afip_codes['doc_types'][$invoice->customer_doc_type] . ' ' . $invoice->customer_ident;

                    $debts_lala[] = [
                        $date,                                  //0
                        $invoice->date_start->format('d/m/Y'),  //1
                        $invoice->date_end->format('d/m/Y'),    //2
                        $visa_auto_debit_record->card_number,   //3
                        $visa_auto_debit_record->customer_name, //4
                        $doc_customer,                          //5
                        $visa_auto_debit_record->customer_code, //6
                        $visa_auto_debit_record->customer_city, //7
                        $invoice->num,                          //8
                        $invoice->duedate->format('d/m/Y'),     //9
                        $invoice->total,                        //10
                        $invoice->company_name                  //11
                    ];
                }

                $this->set(compact('debts_lala'));
                $this->set('_serialize', ['debts_lala']);
            } else {

                $this->Flash->warning(__('La misma acción se intento realizar varias veces.'));
                return $this->redirect(['action' => 'exportDebts']);
            }
        }
    }

    public function showDebts()
    {}

    public function retrievesDebts()
    {
        if ($this->request->is('ajax')) { //Ajax Detection 

            $payment_getway = $this->request->getSession()->read('payment_getway');
            $this->loadModel('VisaAutoDebitRecords');

            $payment_getway_id = $this->request->getQuery()['payment_getway_id'];

            $where = [
                'VisaAutoDebitRecords.payment_getway_id' => $payment_getway_id
            ];

            $response = new \stdClass();

            $response->draw = intval($this->request->getQuery('draw'));

            $response->recordsTotal = $this->VisaAutoDebitRecords->find()->where($where)->count();

            $response->data = $this->VisaAutoDebitRecords->find('ServerSideData', [
                'params' => $this->request->getQuery(),
                'where'  => $where
            ]);

            $response->recordsFiltered = $this->VisaAutoDebitRecords->find('RecordsFiltered', [
                'params' => $this->request->getQuery(),
                'where'  => $where
            ]);

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function acceptPayment()
    {
        if ($this->request->is('ajax')) {

            $response = new \stdClass;
            $response->error = false;
            $response->msg = 'Aceptado Correctamente';
            $response->typeMsg = 'success';

            $data = $this->request->input('json_decode');
            $date = explode('/', $data->date);
            $date = $date[2] . '-' . $date[1] . '-' . $date[0] . ' 00:00:00';
            $user_sistema = 100;

            $this->loadModel('VisaAutoDebitRecords');
            $visa_auto_debit_record = $this->VisaAutoDebitRecords->get($data->id, [
                'contain' => ['Customers.Connections']
            ]);

            if ($visa_auto_debit_record) {

                $visa_auto_debit_record->status = 1;

                if ($this->VisaAutoDebitRecords->save($visa_auto_debit_record)) {

                    $this->loadModel('VisaAutoDebitTransactions');
                    $visa_auto_debit_transaction = $this->VisaAutoDebitTransactions->newEntity();
                    $visa_auto_debit_transaction->id_transaction = $visa_auto_debit_record->id;
                    $visa_auto_debit_transaction->card_number = $visa_auto_debit_record->card_number;
                    $visa_auto_debit_transaction->date = $date;
                    $visa_auto_debit_transaction->total = $visa_auto_debit_record->total;
                    $visa_auto_debit_transaction->comment = "Procesando...";
                    $visa_auto_debit_transaction->bloque_id = NULL;
                    $visa_auto_debit_transaction->customer_code = $visa_auto_debit_record->customer_code;
                    $visa_auto_debit_transaction->payment_getway_id = 106;
                    $visa_auto_debit_transaction->receipt_number = NULL;
                    $visa_auto_debit_transaction->receipt_id = NULL;

                    if ($this->VisaAutoDebitTransactions->save($visa_auto_debit_transaction)) {

                        $visa_auto_debit_account = new \stdClass;
                        $visa_auto_debit_account->customer = $visa_auto_debit_record->customer;
                        $visa_auto_debit_account->payment_getway_id = 106;
                        $visa_auto_debit_account->id = $visa_auto_debit_record->account_id;

                        if (!$this->processTransactions($visa_auto_debit_account, $visa_auto_debit_transaction, 0, $user_sistema)) {
                            $response->msg = 'Error, al aceptar.';
                            $response->typeMsg = 'error';
                        }

                        $customer_code = $visa_auto_debit_record->customer_code;
                        $command = ROOT . "/bin/cake debtMonthControlAfterAction $customer_code 1 > /dev/null &";
                        $result = exec($command);
                    }

                } else {

                    $response->msg = 'Error, al aceptar. No se pudo guardar cambios en el Registro.';
                    $response->typeMsg = 'error';
                }
            } else {

                $response->msg = 'Error, al aceptar. No se encuentra el Registro.';
                $response->typeMsg = 'error';
            }

            $this->set('response', $response);
        }
    }

    public function editAcceptPayment()
    {
        if ($this->request->is('ajax')) {

            $response = new \stdClass;
            $response->error = false;
            $response->msg = 'Aceptado Correctamente';
            $response->typeMsg = 'success';

            $data = $this->request->input('json_decode');
            $date = explode('/', $date = $data->date);
            $date = $date[2] . '-' . $date[1] . '-' . $date[0] . ' 00:00:00';
            $user_sistema = 100;

            $this->loadModel('VisaAutoDebitRecords');
            $visa_auto_debit_record = $this->VisaAutoDebitRecords->get($data->id, [
                'contain' => ['Customers.Connections']
            ]);

            if ($visa_auto_debit_record) {

                $visa_auto_debit_record->status = 1;

                if ($this->VisaAutoDebitRecords->save($visa_auto_debit_record)) {

                    $this->loadModel('VisaAutoDebitTransactions');
                    $visa_auto_debit_transaction = $this->VisaAutoDebitTransactions
                        ->find()
                        ->where([
                            'id_transaction' => $visa_auto_debit_record->id
                        ])
                        ->first();

                    if ($visa_auto_debit_transaction) {

                        $visa_auto_debit_transaction->id_transaction = $visa_auto_debit_record->id;
                        $visa_auto_debit_transaction->date = $date;
                        $visa_auto_debit_transaction->total = $visa_auto_debit_record->total;
                        $visa_auto_debit_transaction->card_number = $visa_auto_debit_record->card_number;
                        $visa_auto_debit_transaction->comment = "Procesando...";
                        $visa_auto_debit_transaction->bloque_id = NULL;
                        $visa_auto_debit_transaction->customer_code = $visa_auto_debit_record->customer_code;
                        $visa_auto_debit_transaction->payment_getway_id = 106;
                        $visa_auto_debit_transaction->receipt_number = NULL;
                        $visa_auto_debit_transaction->receipt_id = NULL;

                        if ($this->VisaAutoDebitTransactions->save($visa_auto_debit_transaction)) {

                            $visa_auto_debit_account = new \stdClass;
                            $visa_auto_debit_account->customer = $visa_auto_debit_record->customer;
                            $visa_auto_debit_account->payment_getway_id = 106;
                            $visa_auto_debit_account->id = $visa_auto_debit_record->account_id;

                            if (!$this->processTransactions($visa_auto_debit_account, $visa_auto_debit_transaction, 0, $user_sistema)) {
                                $response->msg = 'Error, al aceptar.';
                                $response->typeMsg = 'error';
                            }

                            $customer_code = $visa_auto_debit_record->customer_code;
                            $command = ROOT . "/bin/cake debtMonthControlAfterAction $customer_code 1 > /dev/null &";
                            $result = exec($command);
                        } else {

                            $response->msg = 'Error, al aceptar. No se pudo guardar cambios en el Registro.';
                            $response->typeMsg = 'error';
                        }
                    } else {

                        $response->msg = 'Error, al aceptar. No se encontro la transacción.';
                        $response->typeMsg = 'error';
                    }

                } else {

                    $response->msg = 'Error, al aceptar. No se pudo guardar cambios en el Registro.';
                    $response->typeMsg = 'error';
                }
            } else {

                $response->msg = 'Error, al aceptar. No se encuentra el Registro.';
                $response->typeMsg = 'error';
            }

            $this->set('response', $response);
        }
    }

    public function rejectPayment()
    {
        if ($this->request->is('ajax')) {

            $response = new \stdClass;
            $response->error = false;
            $response->msg = 'Rechazado correctamente.';
            $response->typeMsg = 'success';

            $data = $this->request->input('json_decode');

            $this->loadModel('VisaAutoDebitRecords');
            $visa_auto_debit_record = $this->VisaAutoDebitRecords->get($data->id);

            if ($visa_auto_debit_record) {

                $visa_auto_debit_record->status = 0;

                if ($this->VisaAutoDebitRecords->save($visa_auto_debit_record)) {

                    $this->loadModel('VisaAutoDebitTransactions');
                    $visa_auto_debit_transaction = $this->VisaAutoDebitTransactions->newEntity();
                    $visa_auto_debit_transaction->id_transaction = $data->id;
                    $visa_auto_debit_transaction->card_number = $visa_auto_debit_record->card_number;
                    $visa_auto_debit_transaction->date = Time::now();
                    $visa_auto_debit_transaction->total = $visa_auto_debit_record->total;
                    $visa_auto_debit_transaction->comment = "Transacción rechazada manualmente";
                    $visa_auto_debit_transaction->bloque_id = NULL;
                    $visa_auto_debit_transaction->customer_code = $visa_auto_debit_record->customer_code;
                    $visa_auto_debit_transaction->payment_getway_id = 106;
                    $visa_auto_debit_transaction->status = 0;
                    $visa_auto_debit_transaction->receipt_number = NULL;
                    $visa_auto_debit_transaction->receipt_id = NULL;
                    $this->VisaAutoDebitTransactions->save($visa_auto_debit_transaction);
                } else {
                    $response->msg = 'Error, al rechazar. No se pudo guardar cambios en el Registro.';
                    $response->typeMsg = 'error';
                }
            } else {
                $response->msg = 'Error, al rechazar. No se encuentra el Registro.';
                $response->typeMsg = 'error';
            }

            $this->set('response', $response);
        }
    }

    public function editRejectPayment()
    {
        //aca tengo que anular el pago realizado
        if ($this->request->is('ajax')) {

            $response = new \stdClass;
            $response->error = false;
            $response->msg = 'Rechazado correctamente.';
            $response->typeMsg = 'success';

            $data = $this->request->input('json_decode');

            $this->loadModel('VisaAutoDebitRecords');
            $visa_auto_debit_record = $this->VisaAutoDebitRecords->get($data->id);

            if ($visa_auto_debit_record) {

                $visa_auto_debit_record->status = 0;

                if ($this->VisaAutoDebitRecords->save($visa_auto_debit_record)) {

                    $this->loadModel('VisaAutoDebitTransactions');
                    $visa_auto_debit_transaction = $this->VisaAutoDebitTransactions
                        ->find()
                        ->where([
                            'id_transaction' => $data->id
                        ])->first();

                    if ($visa_auto_debit_transaction) {

                        $this->loadModel('Payments');
                        $payment = $this->Payments
                            ->fins()
                            ->contain([
                                'Receipts'
                            ])
                            ->where([
                                'receipt_id' => $visa_auto_debit_transaction->receipt_id
                            ])->first();

                        $this->loadModel('Customers');
                        $customer = $this->Customers
                            ->fins()
                            ->contain([
                                'Areas',
                                'Connections'
                            ])
                            ->where([
                                'code' => $payment->customer_code
                            ])->first();

                        if ($payment) {

                            $user_sistema = 100;

                            if ($this->Accountant->isEnabled()) {

                                $seating = $this->Accountant->addSeating([
                                    'concept'       => 'ANULACIÓN DE PAGO',
                                    'comments'      => '',
                                    'seating'       => NULL,
                                    'account_debe'  => $customer->account_code,
                                    'account_haber' => $payment_getway->config->visa_auto_debit->account,
                                    'value'         => $payment->import,
                                    'user_id'       => $user_sistema,
                                    'created'       => Time::now()
                                ]);

                                if ($seating) {
                                     $data->seating_number = $seating->number;
                                }
                            }

                            $receipt = $payment->receipt;

                            $payment->anulated = Time::now();
                            $this->Payments->save($payment);

                            //log de accion

                            $afip_codes = $this->request->getSession()->read('afip_codes');

                            $tipo_comp = $this->getLetter($afip_codes['comprobantesLetter'], $receipt->tipo_comp);

                            $action = 'Anulación de Pago';
                            $detail = 'Tipo de Comp.: ' . $tipo_comp . PHP_EOL;
                            $detail .= 'Fecha: ' . $receipt->date->format('d/m/Y') . PHP_EOL;
                            $detail .= 'Número: ' . sprintf("%'.04d", $receipt->pto_vta) . '-' . sprintf("%'.08d", $receipt->num) . PHP_EOL;
                            $detail .= 'Total: ' . number_format($payment->total, 2, ',', '.') . PHP_EOL;

                            $detail .= '---------------------------' . PHP_EOL;

                            $this->registerActivity($action, $detail, $payment->customer_code, TRUE);

                            //actualizo el saldo del cliente
                            if (isset($customer->connections)) {

                                foreach ($customer->connections as $connection) {

                                    $this->CurrentAccount->updateDebtMonth($customer->code, $connection->id);
                                }
                            }

                            $this->CurrentAccount->updateDebtMonth($customer->code, $payment->receipt->connection_id);

                            $visa_auto_debit_transaction->date = Time::now();
                            $visa_auto_debit_transaction->total = $visa_auto_debit_record->total;
                            $visa_auto_debit_transaction->card_number = $visa_auto_debit_record->card_number;
                            $visa_auto_debit_transaction->comment = "Transacción rechazada manualmente";
                            $visa_auto_debit_transaction->bloque_id = NULL;
                            $visa_auto_debit_transaction->customer_code = $visa_auto_debit_record->customer_code;
                            $visa_auto_debit_transaction->payment_getway_id = 106;
                            $visa_auto_debit_transaction->status = 0;
                            $visa_auto_debit_transaction->receipt_number = NULL;
                            $visa_auto_debit_transaction->receipt_id = NULL;
                            $this->VisaAutoDebitTransactions->save($visa_auto_debit_transaction);

                        } else {
                            $response->msg = 'Error, al rechazar. No se encuentra el Pago para anularlo.';
                            $response->typeMsg = 'error';
                        }

                    } else {
                        $response->msg = 'Error, al rechazar. No se encuentra el Registro.';
                        $response->typeMsg = 'error';
                    }

                } else {
                    $response->msg = 'Error, al rechazar. No se pudo guardar cambios en el Registro.';
                    $response->typeMsg = 'error';
                }
            } else {
                $response->msg = 'Error, al rechazar. No se encuentra el Registro.';
                $response->typeMsg = 'error';
            }

            $this->set('response', $response);
        }
    }

    private function processTransactions($visa_auto_debit_account, $visa_auto_debit_transaction, $register = 0, $user_sistema = 100)
    {
        $this->loadModel('VisaAutoDebitTransactions');
        $this->loadModel('Connections');

        $customer = $visa_auto_debit_account->customer;

        //cliente con facturacion separa por servicio
        if ($customer->billing_for_service) {

            $where = [
                'customer_code'       => $visa_auto_debit_transaction->customer_code,
                'Connections.deleted' => FALSE
            ];

            if ($visa_auto_debit_transaction->payment_getway_id == 106) {
                $where['visa_auto_debit'] = TRUE;
            }

            $connections = $this->Connections
                ->find()
                ->Contain([
                    'Customers'
                ])
                ->where($where)
                ->order([
                    'Connections.debt_month' => 'DESC'
                ]);

            $bruto = $visa_auto_debit_transaction->total;

            if ($bruto == 0) {
                return 0;
            }

            $connections_size = $connections->count();
            if ($connections_size == 0) {
                $visa_auto_debit_transaction->customer_code = $customer->code;
                $register += $this->generatePayment($visa_auto_debit_transaction, $bruto, $register, $visa_auto_debit_account, NULL);

                return $register;
            }

            if ($connections_size == 1) {

                $connection = $connections->first();

                $visa_auto_debit_transaction->customer_code = $customer->code;
                $register += $this->generatePayment($visa_auto_debit_transaction, $bruto, $register, $visa_auto_debit_account, $connection->id);

                return $register;
            }

            $connections_debts = [];
            $connections_without_debts = [];

            // primer recorrido busco los que tienen deudas y los que no tienen deudas
            foreach ($connections as $key => $connection) {

                $visa_auto_debit_transaction->customer_code = $connection->customer_code;
                $this->VisaAutoDebitTransactions->save($visa_auto_debit_transaction);

                if ($connection->debt_month > 0) {
                    array_push($connections_debts, $connection);
                } else {
                    array_push($connections_without_debts, $connection);
                }
            }

            //array donde cargo los pagos
            $generate_payments = [];
            //bruto que voy descontando las deudas de las conexiones
            $bruto_aux = $bruto;

            // cargo en un array de pagos las conexiones que pueden pagarse
            // a partir de la conexiones con deudas
            if (sizeof($connections_debts) > 0) {

                foreach ($connections_debts as $connections_debt) {

                    // la deuda de la conexiones es menor al bruto
                    if ($connections_debt->debt_month <= $bruto_aux) {
                        $bruto_aux -= $connections_debt->debt_month;

                        $connections_debt->imports = [];
                        array_push($connections_debt->imports, $connections_debt->debt_month);
                        array_push($generate_payments, $connections_debt);
                    } else {
                        // se genera un pago a la conexion con el valor de bruto 
                        $connections_debt->imports = [];
                        array_push($connections_debt->imports, $bruto_aux);
                        $bruto_aux = 0;
                        array_push($generate_payments, $connections_debt);
                        break;
                    }
                }
            }

            // si quedo valor en bruto_aux, puede ser que no exista conexiones con deudas
            if ($bruto_aux > 0) {
                // debe existir conexiones sin deudas
                if (sizeof($connections_without_debts) > 0) {
                    // se asigna el pago a la primer conexion
                    // $connection_without_debt = $connections_without_debts[0];

                    $connection_without_debt_clone = new \sdtClass;
                    $connection_without_debt_clone->id = $connections_without_debts[0]->id;  

                    $connection_without_debt_clone->imports = [];
                    array_push($connection_without_debt_clone->imports, $bruto_aux);
                    $bruto_aux = 0;

                    array_push($generate_payments, $connection_without_debt_clone);

                } else {
                    // se asigna el pago a la primer conexion (con deuda)
                    // $connections_debt = $connections_debts[0];

                    $connections_debt_clone = new \sdtClass;
                    $connections_debt_clone->id = $connections_debts[0]->id;

                    $connections_debt_clone->imports = [];
                    array_push($connections_debt_clone->imports, $bruto_aux);
                    $bruto_aux = 0;

                    array_push($generate_payments, $connections_debt_clone);
                }
            }

            // genero los pagos de las conexiones que tienen deudas
            foreach ($generate_payments as $key => $generate_payment) {

                foreach ($generate_payment->imports as $key => $import) {

                    $register += $this->generatePayment($visa_auto_debit_transaction, $import, $register, $visa_auto_debit_account, $generate_payment->id);
                }
            }

        } else {

            $bruto = $visa_auto_debit_transaction->total;

            if ($bruto == 0) {
                return $register;
            }

            $register = $this->generatePayment($visa_auto_debit_transaction, $bruto, $register, $visa_auto_debit_account);
        }

        return $register;
    }

    private function generatePayment($visa_auto_debit_transaction, $import, $register, $visa_auto_debit_account, $connection_id = NULL, $user_sistema = 100)
    {
        $this->loadModel('Payments');
        $this->loadModel('VisaAutoDebitTransactions');
        $payment_getway = $this->request->getSession()->read('payment_getway');
        $paraments = $this->request->getSession()->read('paraments');
        $seating = NULL;

        $customer = $visa_auto_debit_account->customer;

        //si la contabilidada esta activada registro el asiento
        if ($this->Accountant->isEnabled()) {

            $seating = $this->Accountant->addSeating([
                'concept'       => 'Por Cobranza (Visa Débito Automático)',
                'comments'      => '',
                'seating'       => NULL, 
                'account_debe'  => $payment_getway->config->visa_auto_debit->account,
                'account_haber' => $customer->account_code,
                'value'         => $import,
                'user_id'       => $user_sistema,
                'created'       => Time::now()
            ]);
        }

        $data_payment = [
            'import'             => $import,
            'created'            => $visa_auto_debit_transaction->date,
            'cash_entity_id'     => NULL,
            'user_id'            => $user_sistema,
            'seating_number'     => $seating ? $seating->number : NULL,
            'customer_code'      => $customer->code,
            'payment_method_id'  => 106,
            'concept'            => 'Pago por Visa Débito Automático',
            'connection_id'      => $connection_id,
            'number_transaction' => $visa_auto_debit_transaction->id,
            'account_id'         => $visa_auto_debit_account->id
        ];

        $payment = $this->Payments->newEntity();

        $payment = $this->Payments->patchEntity($payment, $data_payment);

        if ($payment) {

            if ($this->Payments->save($payment)) {

                //genero el recibo del pago
                $receipt = $this->FiscoAfipComp->receipt($payment, FALSE);

                if ($receipt) {

                    $register++;

                    $visa_auto_debit_transaction->status = 1;
                    $visa_auto_debit_transaction->comment = 'procesado correctamente ';

                    if ($visa_auto_debit_transaction->receipt_number != NULL) {
                        $visa_auto_debit_transaction->receipt_number .= ',' . str_pad($receipt->pto_vta, 4, "0", STR_PAD_LEFT) . '-' . str_pad($receipt->num, 8, "0", STR_PAD_LEFT);
                        $visa_auto_debit_transaction->receipt_id .= ',' . $receipt->id;
                    } else {
                        $visa_auto_debit_transaction->receipt_number = str_pad($receipt->pto_vta, 4, "0", STR_PAD_LEFT) . '-' . str_pad($receipt->num, 8, "0", STR_PAD_LEFT);
                        $visa_auto_debit_transaction->receipt_id = $receipt->id;
                    }

                    $this->VisaAutoDebitTransactions->save($visa_auto_debit_transaction);

                    //actualizo el saldo del cliente
                    if (isset($customer->connections)) {

                        foreach ($customer->connections as $connection) {

                            $this->CurrentAccount->updateDebtMonth($customer->code, $connection->id);
                        }
                    }

                    $this->CurrentAccount->updateDebtMonth($customer->code, $connection_id);

                    //habilito la conexion relacionada al serivicio que esta pagando      
                    $this->enableConnection($payment, $customer);

                    $user_id = $this->request->getSession()->read('Auth.User')['id'];
                    $afip_codes = $this->request->getSession()->read('afip_codes');

                    $this->loadModel('Users');
                    $user = $this->Users->get($user_id);
                    $user_name = $user->name . ' - username: ' . $user->username;

                    $customer_name = $customer->name . ' - Código: ' . $customer->code . ' - ' . $afip_codes['doc_types'][$customer->doc_type] . ': ' . $customer->ident;
                    $customer_code = $customer->code;

                    if ($paraments->invoicing->email_emplate_receipt != "") {

                        $controllerClass = 'App\Controller\MassEmailsController';

                        $mass_email = new $controllerClass;
                        if ($mass_email->sendEmailReceipt($receipt->id, $paraments->invoicing->email_emplate_receipt)) {

                            $action = 'Envío recibo al cliente';
                            $detail = 'Cliente: ' . $customer_name . PHP_EOL;
                            $detail .= 'Usuario: ' . $user_name . PHP_EOL;

                            $detail .= '---------------------------' . PHP_EOL;

                            $this->registerActivity($action, $detail, $customer_code, TRUE);
                        } else {

                            $action = 'No envío recibo al cliente';
                            $detail = 'Cliente: ' . $customer_name . PHP_EOL;
                            $detail .= 'Usuario: ' . $user_name . PHP_EOL;

                            $detail .= '---------------------------' . PHP_EOL;

                            $this->registerActivity($action, $detail, $customer_code, TRUE);
                        }
                    }

                } else {

                    $data_error = new \stdClass; 
                    $data_error->request_data = $data_payment;

                    $event = new Event('VisaAutoDebitController.Error', $this, [
                        'msg' => __('Hubo un error al intentar generar el recibo. Verifique los valores cargados.'),
                        'data' => $data_error,
                        'flash' => true
                    ]);

                    $this->getEventManager()->dispatch($event);

                    Log::error($receipt->getErrors(), ['scope' => ['VisaAutoDebitController']]);

                    //rollback payment
                    $this->Payments->delete($payment);
                    //recibo
                    $this->FiscoAfipComp->rollbackReceipt($receipt, TRUE);
                    //rollback seating
                    //si la contabilidada esta activada registro el asientop
                    if ($this->Accountant->isEnabled()) {
                        if ($seating) {
                            $this->Accountant->deleteSeating();
                        }
                    }
                }
            } else {
                Log::error($payment->getErrors(), ['scope' => ['VisaAutoDebitController']]);
            }

        } else {
            Log::error($payment->getErrors(), ['scope' => ['VisaAutoDebitController']]);
        }
        return $register;
    }

    private function strToDeciaml($value)
    {
        $value = str_replace(" ARS", "", $value);
        $value = str_replace(".00", "", $value);
        $value = str_replace('.', '', $value);
        $value = str_replace(',', '.', $value);
        return floatval($value);
    }

    public function assignTransaction()
    {
        //ajax Detection
        if ($this->request->is('ajax')) {

            $transaction_id = $this->request->input('json_decode')->transaction_id;
            $customer_code = $this->request->input('json_decode')->customer_code;

            $response = [
                'type'    => "error",
                'message' => "No se ha asignado el pago al cliente.",
                'error'   => true
            ];

            $this->loadModel('VisaAutoDebitTransactions');
            $visa_auto_debit_transaction = $this->VisaAutoDebitTransactions
                ->find()
                ->where([
                    'id' => $transaction_id
                ])->first();

            if ($visa_auto_debit_transaction) {

                $this->loadModel('VisaAutoDebitRecords');
                $visa_auto_debit_record = $this->VisaAutoDebitRecords
                    ->find()
                    ->where([
                        'id' => $visa_auto_debit_transaction->id_transaction
                    ])->first();

                if ($visa_auto_debit_record) {

                    $visa_auto_debit_record->status = 1;

                    if ($this->VisaAutoDebitRecords->save($visa_auto_debit_record)) {

                        $this->loadModel('VisaAutoDebitAccounts');
                        $visa_auto_debit_account = $this->VisaAutoDebitAccounts
                            ->find()
                            ->contain([
                                'Customers.Connections'
                            ])
                            ->where([
                                'customer_code' => $customer_code
                            ])->first();

                        if ($this->processTransactions($visa_auto_debit_account, $visa_auto_debit_transaction)) {

                            $payment_getway_name = "Visa";
                            $data = "";
                            switch ($visa_auto_debit_account->payment_getway_id) {
                                case 106:
                                    $payment_getway_name = "Débito Automático";
                                    $data = "Nro: " . $visa_auto_debit_account->card_number;
                                    break;
                            }

                            $detail = "";
                            $detail .= 'Medio: '. $payment_getway_name . PHP_EOL;
                            $detail .= $data . PHP_EOL; 
                            $action = 'Asignación de Pago Manual - Visa';
                            $this->registerActivity($action, $detail, $customer_code, TRUE);
                            
                            $response = [
                                'type'    => "success",
                                'message' => "Pago asignado correctamente.",
                                'error'   => FALSE
                            ];

                            $command = ROOT . "/bin/cake debtMonthControlAfterAction $customer_code 1 > /dev/null &";
                            $result = exec($command);
                        }
                    }
                }
            }

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    private function enableConnection($payment, $customer)
    {
        $paraments = $this->request->getSession()->read('paraments');

        $this->loadModel('Connections');

        $connections = [];

        if ($customer->billing_for_service) {

             if ($payment->connection_id) {

                 $connections = $this->Connections->find()->contain(['Customers', 'Controllers', 'Services'])
                    ->where([
                        'Connections.id' => $payment->connection_id,
                        'Connections.deleted' => false,
                        'Connections.enabled' => false
                    ]);
             }

        } else {

            $connections = $this->Connections->find()->contain(['Customers', 'Controllers', 'Services'])
                ->where([
                    'Connections.customer_code' => $customer->code,
                    'Connections.deleted' => false,
                    'Connections.enabled' => false
                ]);
        }

        foreach ($connections as $connection) {

            if ($this->IntegrationRouter->enableConnection($connection, true)) {

                $connection->enabled = true;
                $connection->enabling_date = Time::now();

                if ($this->Connections->save($connection)) {

                    $this->loadModel('ConnectionsHasMessageTemplates');
                    $chmt =  $this->ConnectionsHasMessageTemplates->find()
                        ->where([
                            'connections_id' => $connection->id, 
                            'type' => 'Bloqueo'])
                        ->first();

                    if (!$this->ConnectionsHasMessageTemplates->delete($chmt)) {
                        //$this->Flash->error(__('No se pudo eliminar el aviso de bloqueo.'));
                    }

                    $customer_code = $connection->customer_code;

                    $detail = "";
                    $detail .= 'Conexión: '. $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL; 
                    $action = 'Habilitación de Conexión - Imputación de pago automático Visa Débito Automático';
                    $this->registerActivity($action, $detail, $customer_code, TRUE);
                } else {
                    //$this->Flash->error(__('No se pudo Habilitar la conexión.'));
                }
            }
        }
    }
}
