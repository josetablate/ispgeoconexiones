<?php
namespace App\Controller\Component\Integrations;

use App\Controller\Component\Integrations\Mikrotik;

/**
 * Integration component
 */
abstract class MikrotikPppoe extends Mikrotik
{
     protected $modelControllers;
     protected $modelIpExcluded;
     protected $modelPlans;
     protected $modelPools;
     protected $modelPppoeServers;
     protected $modelProfiles;
     protected $modelQueues;
     protected $modelSecrets;
     
    
     public function initialize(array $config) {
          
          parent::initialize($config);     

     }
     
     protected function initEventManager() {

         parent::initEventManager();
     }    
    
     
     
    //ppp servers
    
    abstract protected function getPppoeServers($controller_id);
    abstract protected function getPppoeServersArray($controller_id);
    abstract protected function getPppoeServersInController($controller);    
    abstract protected function addPppoeServer($data);    
    abstract protected function deletePppoeServerInController($controller, $ppp_server_api_id);    
    abstract protected function deletePppoeServer($id);    
    abstract protected function syncPppoeServer($id);
     
     
     //profiles
    
    abstract protected function getProfiles($controller_id);    
    abstract protected function getProfilesArray($controller_id);    
    abstract protected function getProfilesInController($controller);    
    abstract protected function addProfile($data);    
    abstract protected function deleteProfile($id);
    abstract protected function editProfile($data);    
    abstract protected function deleteProfileInController($controller, $tprofile_api_id);    
    abstract protected function syncProfile($profile_id);
     
     
    //pools
    
    abstract protected function getPools($controller_id);    
    abstract protected function addPool($data);
    abstract protected function editPool($data);    
    abstract protected function deletePool($id);    
    abstract protected function validatePoolUsed($tpool);     
    abstract protected function movePool($data); 
     
     
    //queue
    
    abstract protected function addQueue($connection);    
    abstract protected function deleteQueue($connection);    
    abstract protected function getQueue($connection);    
    abstract protected function editQueue($connection);    
    abstract protected function deleteQueueInController($controller, $tqueue_api_id);
    abstract protected function getQueuesAndConnectionsArray($controller);   
    abstract protected function getQueuesInController($controller);    
    abstract protected function getQueues($controller);    
    abstract protected function syncQueue($connection);
     
     
    //secrets
    
    abstract protected function addSecret($connection);
    abstract protected function editSecret($connection);    
    abstract protected function deleteSecret($connection);    
    abstract protected function getSecret($connection);    
    abstract protected function deleteSecretInController($controller, $secret_api_id);    
    abstract protected function getSecretsAndConnectionsArray($controller);    
    abstract protected function getSecretsInController($controller);    
    abstract protected function getSecrets($controller);    
    abstract protected function syncSecret($connection);  
    abstract protected function generateSecretCredentials($connection);
    abstract protected function randomPassword($len);
     
     
     
    //actives
    
    abstract protected function getActivesInController($controller);    
    abstract protected function deleteActiveInController($controller, $active_api_id);
     
     
    //address list
    
    abstract protected function editAddressList($connection);   
    abstract protected function syncAddressList($addressListId);
    abstract protected function syncAddressListsCorteByConnection($connection);   
    abstract protected function syncAddressListsAvisoByConnection($connection);  
    abstract protected function getAddressListsArray($controller_id);
    abstract protected function deleteAddressListInController($controller, $address_list_api_id);  
    abstract protected function getAddressListsInController($controller);
     
     
    
    
    
}
