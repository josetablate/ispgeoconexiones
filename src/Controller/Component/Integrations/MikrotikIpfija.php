<?php
namespace App\Controller\Component\Integrations;

use App\Controller\Component\Integrations\Mikrotik;


/**
 * Integration component
 */
abstract class MikrotikIpfija extends Mikrotik
{
     protected $modelControllers;
     protected $modelIpExcluded;
     protected $modelPlans;
     protected $modelPools;
     protected $modelGateways;
     protected $modelProfiles;
     protected $modelQueues;
     protected $modelAddressLists;
     protected $modelArps;
     
    
     public function initialize(array $config) {
          
          parent::initialize($config);     

     }
     
     protected function initEventManager() {

        $this->_ctrl->log('initEventManager...', 'debug');

         parent::initEventManager();
     }    
     
     
     //profiles
    
    abstract protected function getProfiles($controller_id);    
    abstract protected function getProfilesArray($controller_id);    
    abstract protected function getProfilesInController($controller);    
    abstract protected function addProfile($data);    
    abstract protected function deleteProfile($id);
    abstract protected function editProfile($data);    
    abstract protected function deleteProfileInController($controller, $tprofile_api_id);    
    abstract protected function syncProfile($profile_id);
     
     
    //pools
    
    abstract protected function getPools($controller_id);    
    abstract protected function addPool($data);
    abstract protected function editPool($data);    
    abstract protected function deletePool($id);    
    abstract protected function validatePoolUsed($tpool);     
    abstract protected function movePool($data); 
    
    //gateways

     abstract protected function getGateways($controller_id);
     abstract protected function addGateway($data);
     abstract protected function editGateway($controller, $data);
     abstract protected function deleteGateway($id);
     abstract protected function deleteGatewayInController($controller, $api_id);
     abstract protected function syncGateway($id);
     abstract protected function getGatewaysArray($controller_id);
     abstract protected function getGatewaysInController($controller);
     
     
    //queue
    
    abstract protected function addQueue($connection);    
    abstract protected function deleteQueue($connection);    
    abstract protected function getQueue($connection);    
    abstract protected function editQueue($connection);    
    abstract protected function deleteQueueInController($controller, $tqueue_api_id);
    abstract protected function getQueuesAndConnectionsArray($controller);   
    abstract protected function getQueuesInController($controller);    
    abstract protected function getQueues($controller);    
    abstract protected function syncQueue($connection);

    
    
    //address list client
     
    abstract protected function addAddressListClient($connection);    
    abstract protected function deleteAddressListClient($connection);    
    abstract protected function getAddressListClient($connection);    
    abstract protected function editAddressListClient($connection);    
    abstract protected function deleteAddressListClientInController($controller, $taddress_list_client_api_id);
    abstract protected function getAddressListsClientAndConnectionsArray($controller);   
    abstract protected function getAddressListsClientInController($controller);    
    abstract protected function getAddressListsClient($controller);    
    abstract protected function syncAddressListClient($addressListid);
    
    
    //arps
    
    abstract protected function addArp($connection);    
    abstract protected function deleteArp($connection);    
    abstract protected function getArp($connection);    
    abstract protected function editArp($connection);    
    abstract protected function deleteArpInController($controller, $tarp_api_id);
    abstract protected function getArpsAndConnectionsArray($controller);   
    abstract protected function getArpsInController($controller);    
    abstract protected function getArps($controller); 
    abstract protected function syncArp($connection);
    
     
     
    //address list
    
    abstract protected function editAddressList($connection);   
    abstract protected function syncAddressList($addressListId);
    abstract protected function syncAddressListsCorteByConnection($connection);   
    abstract protected function syncAddressListsAvisoByConnection($connection);  
    abstract protected function getAddressListsArray($controller_id);
    abstract protected function deleteAddressListInController($controller, $address_list_api_id);  
    abstract protected function getAddressListsInController($controller);
    
    
    
}
