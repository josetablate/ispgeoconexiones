<?php
namespace App\Controller\Component\Integrations;

use App\Controller\Component\Common\MyComponent;

use Cake\Controller\ComponentRegistry;
use Cake\Filesystem\File;
use Cake\I18n\Time;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Event\EventManager;
use Cake\Event\Event;

use Cidr;

/**
 * Integration component
 */
abstract class Integration extends MyComponent 
{ 

    public function initialize(array $config)
    {
        parent::initialize($config);
    }

    protected function integrationEnabled($controller)
    {
        // $paraments = $this->_ctrl->request->getSession()->read('paraments');          
        // return $paraments->system->integration;

        return $controller->integration;
    }

    abstract protected function initEventManager();
    abstract protected function templateTableRegistry();     

    //controller

    abstract protected function addController($controller);
    abstract protected function editController($controller, $data);
    abstract protected function deleteController($controller);
    abstract protected function getControllerTemplate($controller);     
    abstract protected function getStatus($controller);
    abstract protected function cloneController($id);

    //planes

    abstract protected function getPlans($controller_id);    
    abstract protected function getPlan($controller, $id);
    abstract protected function addPlan($data);    
    abstract protected function editPlan($data);    
    abstract protected function deletePlan($id);

    public function existService($service_id)
    {
        $plan = $this->modelPlans->find()
            ->where([
                'service_id' => $service_id
            ])->first();

        return $plan;
    }

    public function removeService($service_id)
    {
        if($this->modelPlans->find()->where(['service_id' => $service_id])->count() > 0){

            return $this->modelPlans->deleteAll(['service_id' => $service_id]);
        }

        return true;
    }

    //ip excluded

    abstract protected function getIPExcluded($controller_id);
    abstract protected function addIPExcludedDirect($data);
    abstract protected function deleteIPExcludedById($id);   
    abstract protected function validateIPExcluded($controller_id, $ip);

    //connections

    abstract protected function addConnection($controller, $data);      
    abstract protected function getConnection($connection);    
    abstract protected function editConnection($connection, $data);    
    abstract protected function deleteConnection($connection);  

    abstract protected function rollbackConnection($connection_id);   

    abstract protected function sync($connection);
    abstract protected function syncConnection($connection);    
    abstract protected function syncDeleteConnection($connection);   

    abstract protected function addAvisoConnection($connection);    
    abstract protected function removeAvisoConnection($connection, $sync);    
    abstract protected function disableConnection($connection);    
    abstract protected function enableConnection($connection, $sync);  

    //ip free

    abstract protected function getIpPool($tpool_id);    
    abstract protected function getIpBusy($tpool_id);    
    abstract protected function getFreeIPByPoolId($controller, $tpool_id);   
    abstract protected function validateIPByController($controller, $ip);
}
