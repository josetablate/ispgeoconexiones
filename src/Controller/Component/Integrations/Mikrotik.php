<?php
namespace App\Controller\Component\Integrations;



use Cake\Log\Log;

use \RouterOS\Query;
use \RouterOS\Client;
use \RouterOS\Config;


// use PEAR2\Net\RouterOS;
// use PEAR2\Net\RouterOS\Response;
// use PEAR2\Net\RouterOS\SocketException;
// use PEAR2\Net\Transmitter\NetworkStream;

/*EvilFreelancer/routeros-api-php*/
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Filesystem\File;
use Cake\Event\EventManager;
use RouterOS\Exceptions\QueryException;
use RouterOS\Exceptions\ClientException;


// use PEAR2\Net\RouterOS\DataFlowException;
// use PEAR2\Net\RouterOS\RouterErrorException;
// use PEAR2\Net\Transmitter\NetworkTransmitter;

use App\Controller\Component\Integrations\Integration;
use App\Controller\Component\Integrations\QueryHelper;



abstract class Mikrotik extends Integration
{    
    
    private $_client;    
    private $_string_conn;    
    private $_string_conn_request; 
    private $_util;    
    // private $_request;
    
    
    public function initialize(array $config) {
         
         parent::initialize($config);
       
        $this->_client = null;
    }
    
    protected function initEventManager(){

        EventManager::instance()->off('Mikrotik.Error');
        EventManager::instance()->off('Mikrotik.Warning');
         
        EventManager::instance()->on(
            'Mikrotik.Error',
            function($event, $msg, $data){
                
                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $log->user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Error';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);
            }
        );
        
        EventManager::instance()->on(
            'Mikrotik.Warning',
            function($event, $msg, $data){
                
                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $log->user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Warning';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);
            }
        );
    }
    
    private function isThisController($controller, $property){
        
        $system_code = [];
        $system_code['name'] = '3SPBr13n';
        $system_code['internal_system_id'] = Configure::read('project.internal_system_id');
        $system_code['controller_id'] = $controller->id;
        
        $return = [
            'value' => $property,
            'other' => true
            ];
   
            
        if(strpos($property, ':') !== false){
            
             $value_array =  explode(':', $property );
             
            //  $length = count($value_array);
            
            if(count($value_array) > 3){
                
                $internal_system_id = $value_array[count($value_array)-2];
                $controller_id = $value_array[count($value_array)-1];
                
                if($internal_system_id == $system_code['internal_system_id'] ){
                    
                    if($controller_id == $system_code['controller_id'] ){
                    
                        $return['value'] = $value_array[0];
                        $return['other'] = false;
                    }
                }
            }
        }
        
        return $return;
    }
    
    private function connectApi($controller, $timeout = 5, $event = true){ 
       
        $this->_ctrl->loadModel('Controllers');

        $config = new Config([
            'host' => $controller->connect_to,
            'user' => $controller->username,
            'pass' => $controller->password,
            'port' => $controller->port,
            'ssl' => false,
            'timeout' => $timeout,
            'attempts' => 2          
        ]);

        $config_ssl = new Config([
            'host' => $controller->connect_to,
            'user' => $controller->username,
            'pass' => $controller->password,
            'port' => $controller->port_ssl,
            'ssl' => true,
            'timeout' => $timeout,
            'attempts' => 2
        ]);       
       
        if($controller->use_tls){
            $string_conn = $controller->connect_to.$controller->port_ssl;  
        }else{
            $string_conn = $controller->connect_to.$controller->port;
        }        
        
        /*controlador deshabilitado*/
        if(!$controller->enabled){
            
            if(!$event){
                return false;
            }
                
            $data_error = new \stdClass; 
            $data_error->controlador = $controller->name;
            
            $event = new Event('Mikrotik.Error', $this, [
                'msg' => 'Controlador Desabilitado',
                'data' => $data_error
                ]);
            $this->_ctrl->getEventManager()->dispatch($event);

            return false;
        }
        /*controaldor eliminado*/
        if($controller->deleted){
            
            if(!$event){
                return false;
            }
            
            $data_error = new \stdClass; 
            $data_error->controlador = $controller->name;
            
            $event = new Event('Mikrotik.Error', $this, [
                'msg' => 'Controlador Eliminado',
                'data' => $data_error
                ]);
            $this->_ctrl->getEventManager()->dispatch($event);
   
            return false;
        }
        
        /**si el string de conexion es diferen, fuerzo a la reconexion */
        if($this->_string_conn != $string_conn){
            
            $this->_client = null;
        }
        
        /** sin no hay conexion activa */
        if(!$this->_client){  
            
            try{

                /**intenta conectar por ssl */
                $this->_client = new Client($config_ssl);

                $use_tls = true;
           
                
            }catch(ClientException $e){

                /**si no intentar conectar por api comun  */
                
                try{
                    
                    $this->_client = new Client($config);
                    
                    $use_tls = false;
                        
                }catch(ClientException $e){                     
                    
                    if(!$event){
                        return false;
                    }
                    
                    $data_error = new \stdClass; 
                    $data_error->controlador = $controller->name;
                    $data_error->code = $e->getCode();                        
                    $data_error->message = $e->getMessage();
                    
                    $event = new Event('Mikrotik.Error', $this, [
                        'msg' => 'Connection Fail',
                        'data' => $data_error
                    ]);
                    $this->_ctrl->getEventManager()->dispatch($event);
                    
                    return false;                
                    
                }              
            }
            
            if(!$this->_client) return false;
            
            $controller->use_tls = $use_tls;
            
            $this->_ctrl->Controllers->save($controller);
          
            if($controller->use_tls){
                $this->_string_conn = $controller->connect_to.$controller->port_ssl;  
            }else{
                $this->_string_conn = $controller->connect_to.$controller->port;
            }             
        }

        return true;
        
    }
     
    protected function getStatusApi($controller) { 
         return $this->connectApi($controller);
    }
  
    public function getInterfacesApi($controller){
      
        if(!$this->integrationEnabled($controller) || !$this->connectApi($controller, 5)){
           return ['ether1' => 'ether1'];
        }

        $array = [];
        $response = $this->_client->wr('/interface/print');
        foreach($response as $interface){
            $array[$interface['name']] = $interface['name'];
        }
    
        return $array;
    }
    
    public function getQueueTypesApi($controller){
        
        if(!$this->integrationEnabled($controller) || !$this->connectApi($controller, 5, false)){
            return [
               'default' => 'default',
               'default-small' => 'default-small',
               'ethernet-default' => 'ethernet-default',
               'hotspot-default' => 'hotspot-default',
               'multi-queue-ethernet-default' => 'multi-queue-ethernet-default',
               'only-hardware-queue' => 'only-hardware-queue',
               'pcq-download-default' => 'pcq-download-default',
               'pcq-upload-default' => 'pcq-upload-default',
               'synchronous-default' => 'synchronous-default',
               'wireless-default' => 'wireless-default',
               ];
        }
        
        $array = [];
        $response = $this->_client->wr('/queue/type/print');
        foreach($response as $type){
            $array[$type['name']] = $type['name'];
        }        
        return $array;
    }
    
    
    
    
    //queue
    
    private function addQueueApi($controller, $queue){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        $system_code = ':3SPBr13n:' . Configure::read('project.internal_system_id') .':'. $controller->id;
    
        $data = [
            'name' => $queue->name,
            'target' => long2ip($queue->target),
            'comment' => $queue->comment . $system_code,
            'max-limit' => $queue->profile->up ? $queue->profile->up .'k/'. $queue->profile->down .'k' : null,
            'limit-at' => $queue->profile->up_at_limit ? $queue->profile->up_at_limit .'k/'. $queue->profile->down_at_limit .'k' : null,
            'burst-limit' => $queue->profile->up_burst ? $queue->profile->up_burst .'k/'. $queue->profile->down_burst .'k' : null,
            'burst-threshold' => $queue->profile->up_threshold ? $queue->profile->up_threshold .'k/'. $queue->profile->down_threshold .'k' : null,
            'burst-time' => $queue->profile->up_time ? $queue->profile->up_time .'/'. $queue->profile->down_time : null,
            'priority' => $queue->profile->priority ? $queue->profile->priority .'/'. $queue->profile->priority : null,
            'queue' => $queue->profile->up_queue_type ? $queue->profile->up_queue_type .'/'. $queue->profile->down_queue_type : null,
        ];

        $data = array_filter($data, function($v){
            return !is_null($v);
        });

        $query = new Query('/queue/simple/add', QueryHelper::parseAttributes($data));
        $response =  $this->_client->wr($query);   

        if(array_key_exists('after', $response)){
            if(array_key_exists('ret', $response['after'])){
                return $response['after']['ret'];
            }else{
                if(array_key_exists('message', $response['after'])){
                    $data_error = new \stdClass;                    
                    $data_error->message = $response['after']['message'];
                    $data_error->queue = $queue;
                    
                    $event = new Event('Mikrotik.Error', $this, [
                            'msg' => 'Error al intentar agregar la cola al controlador',
                            'data' => $data_error
                        ]);
                    $this->_ctrl->getEventManager()->dispatch($event);
                    return false;
                }
            }
        }
    }
    
    private function editQueueApi($controller, $queue){
        
        if(!$this->connectApi($controller, 5)){
            
           return false;           
        }

        $system_code = ':3SPBr13n:' . Configure::read('project.internal_system_id') .':'. $controller->id;
        
    	$data = [
                '.id' => $queue->api_id,            
                'name' => $queue->name,
                'target' => long2ip($queue->target),
                'comment' => $queue->comment . $system_code,
                'max-limit' => $queue->profile->down ? $queue->profile->up .'k/'. $queue->profile->down .'k' : '0/0',
                'limit-at' => $queue->profile->down_at_limit ? $queue->profile->up_at_limit .'k/'. $queue->profile->down_at_limit .'k'  : '0/0',
                'burst-limit' => $queue->profile->down_burst ? $queue->profile->up_burst .'k/'. $queue->profile->down_burst .'k'  : '0/0',
                'burst-threshold' => $queue->profile->down_threshold ? $queue->profile->up_threshold .'k/'. $queue->profile->down_threshold .'k'  : '0/0',
                'burst-time' => $queue->profile->down_threshold ? $queue->profile->up_time .'/'. $queue->profile->down_time : '0s/0s',
                'priority' => $queue->profile->priority ? $queue->profile->priority .'/'. $queue->profile->priority : '8/8',
                'queue' => $queue->profile->down_queue_type ? $queue->profile->up_queue_type .'/'. $queue->profile->down_queue_type : '',
            ];           
           
         
        $query = new Query('/queue/simple/set', QueryHelper::parseAttributes($data));
        $response =  $this->_client->wr($query);

        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass;             
                $data_error->message = $response['after']['message'];
                $data_error->queue = $queue;
                
                $event = new Event('Mikrotik.Error', $this, [
                        'msg' => 'Error al intentar editar la cola en el controlador',
                        'data' => $data_error
                    ]);
    
                $this->_ctrl->getEventManager()->dispatch($event);
    
                return false;
            }
        }
        
        return $queue;
    }
    
    protected function deletedQueueApi($controller, $queue){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        if(!$queue->api_id){
            $queue->api_id = $this->existQueueApi($controller, $queue);
        }
        
        if(!$queue->api_id){
        
            $data_error = new \stdClass; 
            $data_error->queue = $queue;
            
            $event = new Event('Mikrotik.Warning', $this, [
                'msg' => 'La (queue) ya estaba eliminada del controlador',
                'data' => $data_error
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return true;
        }

        $response = $this->_client->wr([
            '/queue/simple/remove',
            '=.id='.$queue->api_id
            ]);

        
        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass;               
                $data_error->message = $response['after']['message'];
                $data_error->queue = $queue;
                
                $event = new Event('Mikrotik.Warning', $this, [
            		'msg' => 'La (queue) ya estaba eliminada en el controlador',
            		'data' => $data_error
            	]);
            	
            	$this->_ctrl->getEventManager()->dispatch($event);
            	
                return $queue;

            }
        }
        
        return $queue;
    }
    
    protected function deletedQueueByApiIdApi($controller, $queue_api_id){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }

        $response = $this->_client->wr([
            '/queue/simple/remove',
            '=.id='.$queue_api_id
            ]);

        
        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass; 
                $data_error->message = $response['after']['message'];
                $data_error->queue_api_id = $queue_api_id;
                
                $event = new Event('Mikrotik.Warning', $this, [
            		'msg' => 'La (queue) ya estaba eliminada en el controlador',
            		'data' => $data_error
            	]);
            	
            	$this->_ctrl->getEventManager()->dispatch($event);
            	
                return true;
            }
        }
        
        return true;
    }
    
    protected function syncQueueApi($controller, $queue){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
      
        if(!$this->existApiIdQueueApi($controller, $queue)){
            $queue->api_id = $this->existQueueApi($controller, $queue);
        }
        
        if($queue->api_id){
            
            if(!$this->editQueueApi($controller, $queue)){
                return false; 
            }
            return $queue;
        }   
       
        $queue->api_id = $this->addQueueApi($controller, $queue);
        
        if(!$queue->api_id){
            return false;
        }
            
      
        return $queue;
    }
    
    private function existApiIdQueueApi($controller, $queue){
        
        if(!$queue->api_id){
           return false;
        }

        if(!$this->connectApi($controller, 5)){
            return false;
         }
      
        $response = $this->_client->wr([
            '/queue/simple/print',
             '?.id='.$queue->api_id
             ]);      
        
        return count($response) > 0;
    }
    
    private function existQueueApi($controller, $queue){
        
        if(!$this->connectApi($controller, 5)){
            return false;
        }

        $response = $this->_client->wr([
            '/queue/simple/print', 
            '?name='.$queue->name
            ]);
     
        return count($response) > 0 ? $response[0]['.id'] : false;
    }  
    
    protected function getQueuesApi($controller){
        
        if(!$this->integrationEnabled($controller) || !$this->connectApi($controller, 5)){
           return [];
        }

        $queues = $this->_client->wr('/queue/simple/print'); 
        
        $array = [];
        
        foreach($queues as $queue){

            if(!array_key_exists('comment', $queue)){
                $queue['comment'] = '';
            }   
            
            $comment = $this->isThisController($controller, $queue['comment']);                 
            
            $array[$queue['name']] = [
        	    'api_id' => $queue['.id'],
                'name' =>  $this->val('name', $queue),
                'target' => $this->val('target', $queue),
                'comment' =>  $comment['value'],
                'max_limit' =>  $this->val('max-limit', $queue),
                'limit_at' =>   $this->val('limit-at', $queue),
                'burst_limit' =>   $this->val('burst-limit', $queue),
                'burst_threshold' =>   $this->val('burst-threshold', $queue),
                'burst_time' =>   $this->val('burst-time', $queue),
                'priority' =>   $this->val('priority', $queue),
                'queue' =>   $this->val('queue', $queue),
                'other' => $comment['other']
            ];
        
        }
         
         ksort($array);
         
         foreach($array as $key => $a){
             
             
             $a['max_limit'] = explode('/',$a['max_limit']);
             $a['max_limit'][0] = $a['max_limit'][0] / 1000;
             $a['max_limit'][1] = $a['max_limit'][1] / 1000;
             $a['max_limit'] = $a['max_limit'][0] . '/' . $a['max_limit'][1];
             
             $a['limit_at'] = explode('/',$a['limit_at']);
             $a['limit_at'][0] = $a['limit_at'][0] / 1000;
             $a['limit_at'][1] = $a['limit_at'][1] / 1000;
             $a['limit_at'] = $a['limit_at'][0] . '/' . $a['limit_at'][1];
             
             $a['burst_limit'] = explode('/',$a['burst_limit']);
             $a['burst_limit'][0] = $a['burst_limit'][0] / 1000;
             $a['burst_limit'][1] = $a['burst_limit'][1] / 1000;
             $a['burst_limit'] = $a['burst_limit'][0] . '/' . $a['burst_limit'][1];
             
             $a['burst_threshold'] = explode('/',$a['burst_threshold']);
             $a['burst_threshold'][0] = $a['burst_threshold'][0] / 1000;
             $a['burst_threshold'][1] = $a['burst_threshold'][1] / 1000;
             $a['burst_threshold'] = $a['burst_threshold'][0] . '/' . $a['burst_threshold'][1];
             
             $array[$key] = $a;
             
         }
       
         return $array;
       
    }
    

    
    
    
    //gateway
    
    private function addGatewayApi($controller, $gateway_data){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        $system_code = ':3SPBr13n:' . Configure::read('project.internal_system_id') .':'. $controller->id;
        
        $data = [
            'address' => $gateway_data->address ? $gateway_data->address : null,
            'interface' => $gateway_data->interface ? $gateway_data->interface : null,
            'comment' => $gateway_data->comment . $system_code
        ];

        $data = array_filter($data, function($v){
            return !is_null($v);
        });
        
        $query = new Query('/ip/address/add', QueryHelper::parseAttributes($data));
        $response =  $this->_client->wr($query);   

        if(array_key_exists('after', $response)){
            if(array_key_exists('ret', $response['after'])){
                return $response['after']['ret'];
            }else{
                if(array_key_exists('message', $response['after'])){

                    $data_error = new \stdClass;           
                    $data_error->message = $response['after']['message'];
                    $data_error->gateway = $gateway;
                    
                    $event = new Event('Mikrotik.Error', $this, [
                            'msg' => 'Error al intentar agregar el address al controlador',
                            'data' => $data_error
                        ]);
                    $this->_ctrl->getEventManager()->dispatch($event);
                    return false;
                }
            }
        }    	
    }
    
    private function editGatewayApi($controller, $gateway_data){
        
        if(!$this->connectApi($controller, 5)){            
           return false;           
        }
        
        $system_code = ':3SPBr13n:' . Configure::read('project.internal_system_id') .':'. $controller->id;
        
    	$data = [
            '.id' => $gateway_data->api_id,
            'address' => $gateway_data->address,
            'interface' => $gateway_data->interface,
            'comment' => $gateway_data->comment . $system_code
            ];
        
        $query = new Query('/ip/address/set', QueryHelper::parseAttributes($data));
        $response =  $this->_client->wr($query);

        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass;            
                $data_error->message = $response['after']['message'];
                $data_error->gateway = $gateway;
                
                $event = new Event('Mikrotik.Error', $this, [
                        'msg' => 'Error al intentar editar el address en el controlador',
                        'data' => $data_error
                    ]);

                $this->_ctrl->getEventManager()->dispatch($event);

                return false;
            }
        }   
     
        return $gateway_data;      
        
    }
    
    protected function deleteGatewayByApiIdApi($controller, $gateway_api_id){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }      


        $response = $this->_client->wr([
            '/ip/address/remove',
            '=.id='.$queue_api_id
            ]);

        
        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass;
                $data_error->message = $response['after']['message'];
                $data_error->gateway_api_id = $gateway_api_id;
                
                $event = new Event('Mikrotik.Warning', $this, [
            		'msg' => 'El gateway ya estaba eliminada en el controlador',
            		'data' => $data_error
            	]);
            	
            	$this->_ctrl->getEventManager()->dispatch($event);
            	
                return true;
            }
        }  
        
        return true;
    }
    
    protected function syncGatewayApi($controller, $gateway){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        if(!$this->existApiIdGatewayApi($controller, $gateway)){
            $gateway->api_id = $this->existGatewayApi($controller, $gateway);
        }
        
        if($gateway->api_id){
            
            if(!$this->editGatewayApi($controller, $gateway)){
                return false; 
            }
            return $gateway;
        }   
       
        $gateway->api_id = $this->addGatewayApi($controller, $gateway);
        
        if(!$gateway->api_id){
            return false;
        }
        
        return $gateway;
    }
    
    private function existApiIdGatewayApi($controller, $gateway){
        
        if(!$gateway->api_id){
           return false;
        }
         
        if(!$this->connectApi($controller, 5)){
           return false;
        }

        $response = $this->_client->wr([
            '/ip/address/print',
             '?.id='.$gateway->api_id
             ]);      
        
        return count($response) > 0;
    }
    
    private function existGatewayApi($controller, $gateway){
        
        $system_code = ':3SPBr13n:' . Configure::read('project.internal_system_id') .':'. $controller->id;
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }

        $response = $this->_client->wr([
            '/ip/address/print', 
            '?address='.$gateway->address
            ]);
     
        if(count($response) > 0){

            if(array_key_exists('comment', $response[0])){

                $comment = $response[0]['comment'];
    
                if(strpos($comment, $system_code) !== false){  
                    return $response[0]['.id'];                    
                }else{                    
                    return false;
                }
            }
        }
    }
    
    protected function getGatewaysApi($controller){
        
        if(!$this->integrationEnabled($controller) || !$this->connectApi($controller, 5)){
           return [];
        }

        $gateways = $this->_client->wr('/ip/address/print'); 
                
        $array = [];
        
        foreach($gateways as $gateway){

            if(!array_key_exists('comment', $gateway)){
                $gateway['comment'] = '';
            } 
            
            $comment = $this->isThisController($controller, $gateway['comment']);            
            $address = $gateway['address'];
        
            $array[$address] = [
        	    'api_id' => $gateway['.id'],
                'address' => $this->val('address', $gateway),
                'interface' => $this->val('interface', $gateway),
                'comment' =>  $comment['value'],
                'other' => $comment['other']
            ];
            
        }
         
         ksort($array);
        
         return $array;
       
    }
    
    
    
    
    
    //arp
    
    private function addArpApi($controller, $arp_data){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        $system_code = ':3SPBr13n:' . Configure::read('project.internal_system_id') .':'. $controller->id;
            	
    	$data = [
            'address' => long2ip($arp_data->address),
            'interface' => $arp_data->interface ? $arp_data->interface : null,
            'comment' => $arp_data->comment . $system_code,
            'mac-address' => $arp_data->mac_address ? $arp_data->mac_address : null,
            ];

        $data = array_filter($data, function($v){
            return !is_null($v);
        });

        $query = new Query('/ip/arp/add', QueryHelper::parseAttributes($data));
        $response =  $this->_client->wr($query);   

        if(array_key_exists('after', $response)){
            if(array_key_exists('ret', $response['after'])){
                return $response['after']['ret'];
            }else{
                if(array_key_exists('message', $response['after'])){

                    $data_error = new \stdClass;                   
                    $data_error->message = $response['after']['message'];
                    $data_error->arp = $arp;
                    
                    $event = new Event('Mikrotik.Error', $this, [
                            'msg' => 'Error al intentar agregar la (arp) al controlador',
                            'data' => $data_error
                        ]);
                    $this->_ctrl->getEventManager()->dispatch($event);
                    return false;
                }
            }
        }  
    }
    
    private function editArpApi($controller, $arp_data){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        $system_code = ':3SPBr13n:' . Configure::read('project.internal_system_id') .':'. $controller->id;

    	$data = [
            '.id' => $arp_data->api_id,
            'address' => long2ip($arp_data->address),
            'interface' => $arp_data->interface,
            'comment' => $arp_data->comment . $system_code,
            'mac-address' => $arp_data->mac_address,
            ];

           
        $query = new Query('/ip/arp/set', QueryHelper::parseAttributes($data));
        $response =  $this->_client->wr($query);

        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass;          
                $data_error->message = $response['after']['message'];
                $data_error->arp = $arp;
                
                $event = new Event('Mikrotik.Error', $this, [
                        'msg' => 'Error al intentar editar el arp en el controlador',
                        'data' => $data_error
                    ]);

                $this->_ctrl->getEventManager()->dispatch($event);
            }
        }

        return $arp_data;      
        
    }
    
    protected function deleteArpApi($controller, $arp){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }

        if(!$arp->api_id){
            $arp->api_id = $this->existArpApi($controller, $arp);             
	    }
	    
	    if(!$arp->api_id){
	        
	        $data_error = new \stdClass; 
            $data_error->arp = $arp;
            
            $event = new Event('Mikrotik.Warning', $this, [
                'msg' => 'El (arp) ya estaba eliminado del controlador',
                'data' => $data_error
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return $arp;
        }

        $response = $this->_client->wr([
            '/ip/arp/remove',
             '=.id='.$arp->api_id
             ]);

        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass; 
                $data_error->message = $response['after']['message'];
                $data_error->arp_data = $arp_data;
                
                $event = new Event('Mikrotik.Warning', $this, [
            		'msg' => 'El (arp) ya estaba eliminada en el controlador',
            		'data' => $data_error
            	]);
            	
            	$this->_ctrl->getEventManager()->dispatch($event);
            	
                return true;
            }            
        }       
        
        return $arp;
    }
    
    protected function deletedArpByApiIdApi($controller, $arp_api_id){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }

        $response = $this->_client->wr([
            '/ip/arp/remove',
            '=.id='.$arp_api_id
            ]);

        
        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass; 
                $data_error->message = $response['after']['message'];
                $data_error->arp_api_id = $arp_api_id;
                
                $event = new Event('Mikrotik.Warning', $this, [
            		'msg' => 'El (arp) ya estaba eliminada en el controlador',
            		'data' => $data_error
            	]);
            	
            	$this->_ctrl->getEventManager()->dispatch($event);
            	
                return true;

            }
        }
   
        return true;
    }
    
    protected function syncArpApi($controller, $arp){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        if(!$this->existApiIdArpApi($controller, $arp)){
           $arp->api_id = $this->existArpApi($controller, $arp); 
        }
        
        if($arp->api_id){
            
            if(!$this->editArpApi($controller, $arp)){
                return false; 
            }
            return $arp;
        }   
       
        $arp->api_id = $this->addArpApi($controller, $arp);
        
        if(!$arp->api_id){
            return false;
        }
        
        return $arp;
    }
    
    private function existApiIdArpApi($controller, $arp){
        
        if(!$arp->api_id){
           return false;
        }
         
        if(!$this->connectApi($controller, 5)){
           return false;
        }

        $response = $this->_client->wr([
            '/ip/arp/print',
             '?.id='.$arp->api_id
             ]);      
        
        return count($response) > 0;
    }
    
    private function existArpApi($controller, $arp){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }

        $response = $this->_client->wr([
            '/ip/arp/print', 
            '?address='.$arp->address,
            '?dynamic=no'
            ]);
     
        return count($response) > 0 ? $response[0]['.id'] : false;
        
    }
    
    protected function getArpsApi($controller){
        
        if(!$this->integrationEnabled($controller) || !$this->connectApi($controller, 5)){
           return [];
        }

        $arps = $this->_client->wr('/ip/arp/print'); 

        $array = [];
        
        foreach($arps as $arp){

            if(!array_key_exists('comment', $arp)){
                $arp['comment'] = '';
            }   
            
            $comment = $this->isThisController($controller, $arp['comment']);            
            $address = $arp['address'];
            
            $array[$address] = [
        	    'api_id' => $arp['.id'],
                'address' =>  $this->val('address', $arp),
                'interface' => $this->val('interface', $arp),
                'comment' =>  $comment['value'],
                'mac_address' =>   $this->val('mac-address', $arp),
                'other' => $comment['other']
            ];
         }
         
         ksort($array);
       
         return $array;
       
    }
    
    protected function getArpsForMigrationApi($controller){
        
        if(!$this->integrationEnabled($controller) || !$this->connectApi($controller, 5)){
           return [];
        }

        $arps = $this->_client->wr('/ip/arp/print'); 

        $array = [];
        
        foreach($arps as $arp){      
            
            $array[$arp['address']] = [
                'dynamic' => $this->val('dynamic', $arp),
        	    'api_id' => $arp['.id'],
                'address' => $this->val('address', $arp),
                'interface' =>  $this->val('interface', $arp),
                'comment' =>   $this->val('comment', $arp),
                'mac_address' =>   $this->val('mac-address', $arp),
                'other' => false
            ];
         }
         
         ksort($array);
       
         return $array;
       
    }
    
    
    
    
     //lease
    
    private function addLeaseApi($controller, $lease){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
       
    	$system_code = ':3SPBr13n:' . Configure::read('project.internal_system_id') .':'. $controller->id;
    	
    	$data = [
                'address' => long2ip($lease->address),
                'mac-address' => $lease->mac_address ? $lease->mac_address : null,
                'server' => $lease->server . $system_code,
                'address-lists' => $lease->address_lists . $system_code,
                'lease-time' => $lease->lease_time ? $lease->lease_time : null,
                'comment' => $lease->comment . $system_code,
            ];

        $data = array_filter($data, function($v){
            return !is_null($v);
        });

        $query = new Query('/ip/dhcp-server/lease/add', QueryHelper::parseAttributes($data));
        $response =  $this->_client->wr($query);   

        if(array_key_exists('after', $response)){
            if(array_key_exists('ret', $response['after'])){
                return $response['after']['ret'];
            }else{
                if(array_key_exists('message', $response['after'])){
                
                    $data_error = new \stdClass;       
                    $data_error->message = $response['after']['message'];
                    $data_error->lease = $lease;
                    
                    $event = new Event('Mikrotik.Error', $this, [
                            'msg' => 'Error al intentar agregar el (lease) al controlador',
                            'data' => $data_error
                        ]);
                    $this->_ctrl->getEventManager()->dispatch($event);
                    return false;
                }
            }
        }
    }
    
    private function editLeaseApi($controller, $lease){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        $system_code = ':3SPBr13n:' . Configure::read('project.internal_system_id') .':'. $controller->id;
    
    	$data = [
            '.id' => $lease->api_id,
            'address' => long2ip($lease->address),
            'mac-address' => $lease->mac_address,
            'server' => $lease->server . $system_code,
            'address-lists' => $lease->address_lists . $system_code,
            'lease-time' => $lease->lease_time,
            'comment' => $lease->comment . $system_code
        ];

        $query = new Query('/ip/dhcp-server/lease/set', QueryHelper::parseAttributes($data));
        $response =  $this->_client->wr($query);

        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass; 
                $data_error->message = $response['after']['message'];
                $data_error->data = $data;
                
                $event = new Event('Mikrotik.Error', $this, [
                        'msg' => 'Error al intentar editar el (lease) en el controlador',
                        'data' => $data_error
                    ]);
    
                $this->_ctrl->getEventManager()->dispatch($event);
    
                return false;
            }
        }
     
        return $lease;      
        
    }
    
    protected function deletedLeaseApi($controller, $lease){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        if(!$lease->api_id){
	        $lease->api_id = $this->existLeaseApi($controller, $lease);
	    }
	    
	    if(!$lease->api_id){
	        
	        $data_error = new \stdClass; 
            $data_error->lease = $lease;
            
            $event = new Event('Mikrotik.Warning', $this, [
                'msg' => 'El (lease) ya estaba eliminado del controlador',
                'data' => $data_error
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return true;
        }
        
        $response = $this->_client->wr([
            '/ip/dhcp-server/lease/remove',
            '=.id='.$lease->api_id
            ]);

        
        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){
                
                $data_error = new \stdClass; 
                $data_error->code = $e->getCode();
                $data_error->message = $e->getResponses()->getProperty('message');
                $data_error->lease = $lease;
                
                $event = new Event('Mikrotik.Warning', $this, [
            		'msg' => 'El (lease) ya estaba eliminada en el controlador',
            		'data' => $data_error
            	]);
            	
            	$this->_ctrl->getEventManager()->dispatch($event);
            	
                return $lease;

            }
        }   	
        
        return $lease;
    }
    
    protected function deletedLeaseByApiIdApi($controller, $lease_api_id){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }

        $response = $this->_client->wr([
            '/ip/dhcp-server/lease/remove',
            '=.id='.$lease_api_id
            ]);

        
        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass;   
                $data_error->message = $response['after']['message'];
                $data_error->lease_api_id = $lease_api_id;
                
                $event = new Event('Mikrotik.Warning', $this, [
            		'msg' => 'El (lease) ya estaba eliminado en el controlador',
            		'data' => $data_error
            	]);
            	
            	$this->_ctrl->getEventManager()->dispatch($event);
            	
                return true;

            }
        }  
        
        return true;
    }
    
    protected function syncLeaseApi($controller, $lease){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
            
        if(!$this->existApiIdLeaseApi($controller, $lease)){
            $lease->api_id = $this->existLeaseApi($controller, $lease);
        }
        
        if($lease->api_id){
            
            if(!$this->editLeaseApi($controller, $lease)){
                 return false;
            }
            return $lease;
        }   
       
        $lease->api_id = $this->addLeaseApi($controller, $lease);
        
        if(!$lease->api_id){
            return false;
        }
        
        return $lease;
        
    }
    
    private function existApiIdLeaseApi($controller, $lease){
        
        if(!$lease->api_id){
           return false;
        }
         
        if(!$this->connectApi($controller, 5)){
           return false;
        }

        $response = $this->_client->wr([
            '/ip/dhcp-server/lease/print',
             '?.id='.$lease->api_id
             ]);      
        
        return count($response) > 0;    
    }
    
    private function existLeaseApi($controller, $lease){
     
        if(!$this->connectApi($controller, 5)){
           return false;
        }

        if(!$this->connectApi($controller, 5)){
            return false;
        }

        $response = $this->_client->wr([
            '/ip/dhcp-server/lease/print', 
            '?address='.long2ip($lease->address)
            ]);
     
        return count($response) > 0 ? $response[0]['.id'] : false;       
    	
    }
    
    protected function getLeasesApi($controller){
        
        if(!$this->integrationEnabled($controller) || !$this->connectApi($controller, 5)){
           return [];
        }

        $leases = $this->_client->wr('/ip/dhcp-server/lease/print'); 
           
        $array = [];        
        
        foreach($leases as $lease){
            
            $address = $lease['address'];

            if(!array_key_exists('comment', $lease)){
                $lease['comment'] = '';
            }

            if(!array_key_exists('server', $lease)){
                $lease['server'] = '';
            }

            if(!array_key_exists('address-lists', $lease)){
                $lease['address-lists'] = '';
            }
            
            $comment = $this->isThisController($controller, $lease['comment']);
            
            $server = $this->isThisController($controller, $lease['server']);
            
            $address_lists = $this->isThisController($controller, $lease['address-lists']);
            
            $array[$address] = [
        	    'api_id' => $lease['.id'],
                'address' =>  $this->val('address', $lease),
                'mac_address' => $this->val('mac-address', $lease),
                'server' =>  $server['value'],
                'comment' =>  $comment['value'],
                'address_lists' =>  $address_lists['value'],
                'lease_time' =>   $this->val('lease-time', $lease),
                'other' => $comment['other']
                
            ];
            
        }
        
      
         ksort($array);
       
         return $array;
    }
    
    
    
    
    //secrets
    
    private function addSecretApi($controller, $secret){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        $system_code = ':3SPBr13n:' . Configure::read('project.internal_system_id') .':'. $controller->id;
        
        $caller_id = '';
        if($secret->caller_id){
            $caller_id  = implode(':', str_split( $secret->caller_id, 2 ));
        }

    	$data = [
            'name' => $secret->name ? $secret->name : null,
            'password' => $secret->password ? $secret->password : null,
            'service' => $secret->service ? $secret->service : null,
            'profile' => $secret->profile ? $secret->profile : null,
            'remote-address' => long2ip($secret->remote_address),
            'comment' => $secret->comment . $system_code,
            'caller-id' => $caller_id  ? $caller_id : null
            ];

        $data = array_filter($data, function($v){
            return !is_null($v);
        });

        $query = new Query('/ppp/secret/add', QueryHelper::parseAttributes($data));
        $response =  $this->_client->wr($query);   

        if(array_key_exists('after', $response)){
            if(array_key_exists('ret', $response['after'])){
                return $response['after']['ret'];
            }else{
                if(array_key_exists('message', $response['after'])){

                    $data_error = new \stdClass;    
                    $data_error->message = $response['after']['message'];
                    $data_error->secret = $data;
                    
                    $event = new Event('Mikrotik.Error', $this, [
                        'msg' => 'Error al intentar agregar el (secret) al controlador',
                        'data' => $data_error
                    ]);
                    
                    $this->_ctrl->getEventManager()->dispatch($event);
                    
                    return false;
                }
            }
        }
    }
    
    private function editSecretApi($controller, $secret){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        $system_code = ':3SPBr13n:' . Configure::read('project.internal_system_id') .':'. $controller->id;
               
        $caller_id = '';
        if($secret->caller_id){
            $caller_id  = implode(':', str_split( $secret->caller_id, 2 ));
        }
    
    	$data = [
            '.id' => $secret->api_id,
            'name' => $secret->name,
            'password' => $secret->password,
            'service' => $secret->service,
            'profile' => $secret->profile,
            'remote-address' => long2ip($secret->remote_address),
            'comment' => $secret->comment . $system_code,
            'caller-id' => $caller_id,
            ];

        $query = new Query('/ppp/secret/set', QueryHelper::parseAttributes($data));
        $response =  $this->_client->wr($query);

        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass;         
                $data_error->message = $response['after']['message'];
                $data_error->secret = $secret;
                
                $event = new Event('Mikrotik.Error', $this, [
                    'msg' => 'Error al intentar editar el secret en el controlador',
                    'data' => $data_error
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
                
                return false;
            }
        }       
       
          
        $this->deleteActiveApi($controller, $secret);
     
        return $secret;      
        
    }
    
    protected function deleteSecretApi($controller, $secret){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        if(!$secret->api_id){
            $secret->api_id = $this->existSecretApi($controller, $secret);
        }
        
        if(!$secret->api_id){
        
            $data_error = new \stdClass; 
            $data_error->secret = $secret;
            
            $event = new Event('Mikrotik.Warning', $this, [
                'msg' => 'La (secret) ya estaba eliminada del controlador',
                'data' => $data_error
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return true;
        }

        $response = $this->_client->wr([
            '/ppp/secret/remove',
            '=.id='.$secret->api_id
            ]);

        
        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass;      
                $data_error->message = $response['after']['message'];
                $data_error->secret = $secret;
                
                $event = new Event('Mikrotik.Warning', $this, [
                    'msg' => 'El secret ya estaba eliminado en el controlador',
                    'data' => $data_error
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
                
                $this->deleteActiveApi($controller, $secret);
          
                return true;
            }
        }   
        
        $this->deleteActiveApi($controller, $secret);
        
        return $secret;
    }
    
    protected function deleteSecretByApiIdApi($controller, $secret_api_id){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }

        $response = $this->_client->wr([
            '/ppp/secret/remove',
            '=.id='.$secret_api_id
            ]);

        
        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass;            
                $data_error->message = $response['after']['message'];
                $data_error->secret_api_id = $secret_api_id;
                
                $event = new Event('Mikrotik.Warning', $this, [
                    'msg' => 'El (secret) ya estaba eliminado en el controlador',
                    'data' => $data_error
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
          
                return true;
            }
        }    	
   
        
        $this->deleteActiveByApiIdSecretApi($controller, $secret_api_id);
        
        return true;
    }

    protected function syncSecretApi($controller, $secret){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        if(!$this->existApiIdSecretApi($controller, $secret)){
            $secret->api_id = $this->existSecretApi($controller, $secret);
        }
        
        if( $secret->api_id){ //existe
            
            if(!$this->editSecretApi($controller, $secret)){
                return false;
            }
            
            return $secret;
        }   
       
        $secret->api_id = $this->addSecretApi($controller, $secret);
        
        if(!$secret->api_id){
            return false;
        }
        
        return $secret;
    }
    
    private function existApiIdSecretApi($controller, $secret){
        
        if(!$secret->api_id){
           return false;
        }
         
        if(!$this->connectApi($controller, 5)){
           return false;
        }

        $response = $this->_client->wr([
            '/ppp/secret/print',
             '?.id='.$secret->api_id
             ]);      
        
        return count($response) > 0;
    }
    
    private function existSecretApi($controller, $secret){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }

        $response = $this->_client->wr([
            '/ppp/secret/print', 
            '?name='.$secret->name
            ]);
     
        return count($response) > 0 ? $response[0]['.id'] : false;     
    }

    protected function getSecretsApi($controller){
        
        if(!$this->integrationEnabled($controller) || !$this->connectApi($controller, 5)){
           return [];
        }

        $secrets = $this->_client->wr('/ppp/secret/print');   
        
        $array = [];
        
        foreach($secrets as $secret){

            if(!array_key_exists('comment', $secret)){
                $secret['comment'] = '';
            }   
            
            $comment = $this->isThisController($controller, $secret['comment']);       
            
            $array[$secret['name']] = [
        	    'api_id' => $secret['.id'],
                'name' => $this->val('name', $secret),
                'password' =>  $this->val('password', $secret),
                'service' =>  $this->val('service', $secret),
                'profile' =>  $this->val('profile', $secret),
                'remote_address' => $this->val('remote-address', $secret),
                'comment' =>  $comment['value'],
                'caller_id' =>  $this->val('caller-id', $secret),
                'other' => $comment['other']
            ];
        }        
         
        ksort($array);
       
        return $array;
       
    }
 


    
    
    //profiles
    
    private function addProfileApi($controller, $profile){
  
        if(!$this->connectApi($controller, 5)){
            return false;
        }
        
        $system_code = ':3SPBr13n:' . Configure::read('project.internal_system_id') .':'. $controller->id;

        $data = [
            'name' => $profile->name,
            'local-address' => $profile->local_address ? $profile->local_address :  null,
            'dns-server' => $profile->dns_server ? $profile->dns_server :  null,
            'rate-limit' => $profile->rate_limit_string ? $profile->rate_limit_string :  null,
            'queue-type' => $profile->queue_type ? $profile->queue_type : null,
            'comment' => $profile->name . $system_code
            ];

        $data = array_filter($data, function($v){
            return !is_null($v);
        });

        $query = new Query('/ppp/profile/add', QueryHelper::parseAttributes($data));
        $response =  $this->_client->wr($query);   

        if(array_key_exists('after', $response)){
            if(array_key_exists('ret', $response['after'])){
                $profile->api_id = $response['after']['ret'];
            }else{
                if(array_key_exists('message', $response['after'])){

                    $data_error = new \stdClass;              
                    $data_error->message = $response['after']['message'];
                    $data_error->profile = $profile;
                    
                    $event = new Event('Mikrotik.Error', $this, [
                        'msg' => 'Error al intentar agregar el perfil al controlador',
                        'data' => $data_error
                    ]);
                    
                    $this->_ctrl->getEventManager()->dispatch($event);
                    
                    return false;

                }
            }
        }         
    
        return $profile;            
    }
    
    private function editProfileApi($controller, $profile){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        $system_code = ':3SPBr13n:' . Configure::read('project.internal_system_id') .':'. $controller->id;
      
        $data = [
            '.id' => $profile->api_id,
            'name' => $profile->name,
            'local-address' => $profile->local_address,
            'dns-server' => $profile->dns_server,
            'rate-limit' => $profile->rate_limit_string,
            'queue-type' => $profile->queue_type,
            'comment' => $profile->name . $system_code
            ];

        $query = new Query('/ppp/profile/set', QueryHelper::parseAttributes($data));
        $response =  $this->_client->wr($query);

        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass;        
                $data_error->message = $response['after']['message'];
                $data_error->profile = $profile;
                
                $event = new Event('Mikrotik.Error', $this, [
                    'msg' => 'Error al intentar editar el perfil en el controlador',
                    'data' => $data_error
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
                
                return false;
            }
        }
 
        return $profile;
        
    }
    
    protected function deleteProfilebyApiIdApi($controller, $tprofile_api_id){
        
        if(!$this->connectApi($controller, 5)){
          return false;
        }
           
        $response = $this->_client->wr([
            '/ppp/profile/remove',
            '=.id='.$tprofile_api_id
            ]);
        
        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass;        
                $data_error->message = $response['after']['message'];
                $data_error->profile_api_id = $tprofile_api_id;
                
                $event = new Event('Mikrotik.Warning', $this, [
                    'msg' => 'El perfil ya estaba eliminado del controlador',
                    'data' => $data_error
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
                
                return true;

            }
        }    
        
        return true;
    }    

    protected function syncProfileApi($controller, $profile){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        if(!$this->existApiIdProfileApi($controller, $profile)){
            $profile->api_id = $this->existProfileApi($controller, $profile);
        }
        
        if($profile->api_id){
            
             if(!$this->editProfileApi($controller, $profile)){
                return false;   
             }
             
             return $profile;
        }   
        
        $profile = $this->addProfileApi($controller, $profile);
        
        if(!$profile){
            return false;
        }
        
        return $profile;
    } 
    
    private function existApiIdProfileApi($controller, $profile){
        
        if(!$profile->api_id){
           return false;
        }
         
        if(!$this->connectApi($controller, 5)){
           return false;
        }

        $response = $this->_client->wr([
            '/ppp/profile/print',
             '?.id='.$profile->api_id
             ]);      
        
        return count($response) > 0;  
    }
    
    private function existProfileApi($controller, $profile){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        $response = $this->_client->wr([
            '/ppp/profile/print', 
            '?name='.$profile->name
        ]);
     
        return count($response) > 0 ? $response[0]['.id'] : false;      
    }
    
    protected function getProfilesApi($controller){
        
        if(!$this->integrationEnabled($controller) || !$this->connectApi($controller, 5)){
           return [];
        }

        $profiles = $this->_client->wr('/ppp/profile/print');
        
        $array = [];
        
        $default_profiles = ['default', 'default-encryption'];
        
        foreach($profiles as $profile){

            if(!array_key_exists('comment', $profile)){
                $profile['comment'] = '';
            }   
            
            $comment = $this->isThisController($controller, $profile['comment']); 
                        
            $name = $profile['name'];
            
            if(in_array($name, $default_profiles)){
                continue;
            }

           
            
            $array[$name] = [
        	    'api_id' => $profile['.id'],
                'name' => $this->val('name', $profile),
                'local_address' => $this->val('local-address', $profile),
                'dns_server' => $this->val('dns-server', $profile),
                'queue_type' => $this->val('queue-type', $profile),
                'rate_limit_string' =>  $this->val('rate-limit', $profile),
                'comment' =>  $this->val('value', $profile),
                'other' => $comment['other']
            ];
        }
        
         
         ksort($array);
       
         return $array;
    }

    private function val($key, $array){
        
        return  (isset($array[$key]) || key_exists($key, $array)) ? $array[$key] : null;
    }
    
    
    
    
    
    
    //ppp servers
    
    private function addPppoeServerApi($controller, $pppoeServer){
       
        if(!$this->connectApi($controller, 5)){
            return false;
        }
        
        $system_code = ':3SPBr13n:' . Configure::read('project.internal_system_id') .':'. $controller->id;
        
        $data = [
            'service-name' => $pppoeServer->service_name . $system_code,
            'interface' => $pppoeServer->interface ? $pppoeServer->interface : null,
            'one-session-per-host' => $pppoeServer->one_session_per_host ? $pppoeServer->one_session_per_host : null,
            'authentication' => $pppoeServer->authentication ? $pppoeServer->authentication : null,
            'disabled' => $pppoeServer->disabled ? $pppoeServer->disabled : null,
            ];

        $data = array_filter($data, function($v){
            return !is_null($v);
        });

        $query = new Query('/interface/pppoe-server/server/add', QueryHelper::parseAttributes($data));
        $response =  $this->_client->wr($query);   

        if(array_key_exists('after', $response)){
            if(array_key_exists('ret', $response['after'])){
                $pppoeServer->api_id =  $response['after']['ret'];
            }else{
                if(array_key_exists('message', $response['after'])){

                    $data_error = new \stdClass;            
                    $data_error->message = $response['after']['message'];
                    $data_error->pppoeServer = $pppoeServer;
                    
                    $event = new Event('Mikrotik.Error', $this, [
                        'msg' => 'Error al intentar agregar el (PPPoE Server) al controlador',
                        'data' => $data_error
                    ]);
                    
                    $this->_ctrl->getEventManager()->dispatch($event);

                    return false;
                }
            }
        }     
                
        return $pppoeServer;
       
    }
    
    private function editPppoeServerApi($controller, $pppoeServer){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        $system_code = ':3SPBr13n:' . Configure::read('project.internal_system_id') .':'. $controller->id;
        
        $data = [
            '.id' => $pppoeServer->api_id,
            'service-name' => $pppoeServer->service_name . $system_code,
            'interface' => $pppoeServer->interface,
            'one-session-per-host' => $pppoeServer->one_session_per_host,
            'authentication' => $pppoeServer->authentication,
            'disabled' => $pppoeServer->disabled,
            ];

        $query = new Query('/interface/pppoe-server/server/set', QueryHelper::parseAttributes($data));
        $response =  $this->_client->wr($query);

        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass;          
                $data_error->message = $response['after']['message'];
                $data_error->pppoeServer = $data;
                
                $event = new Event('Mikrotik.Error', $this, [
                        'msg' => 'Error al intentar editar la (PPPoE Server) en el controlador',
                        'data' => $data_error
                    ]);

                $this->_ctrl->getEventManager()->dispatch($event);

                return false;

            }
        }
            
       
        return $pppoeServer;      
    }
    
    protected function deletePppoeServerByApiIdApi($controller, $pppoeServer_api_id){
        
        if(!$this->connectApi($controller, 5)){
          return false;
        }   
        
        $response = $this->_client->wr([
            '/interface/pppoe-server/server/remove',
            '=.id='.$pppoeServer_api_id
            ]);
        
        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass;     
                $data_error->message = $response['after']['message'];
                $data_error->pppoeServer_api_id =  $pppoeServer_api_id;
                
                $event = new Event('Mikrotik.Warning', $this, [
                    'msg' => 'El (pppoe server) ya estaba eliminado del controlador',
                    'data' => $data_error
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
                
                return true;
            }
        } 	
        
        return true;
    }
    
    protected function syncPppoeServerApi($controller, $pppoeServer){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        if(!$this->existApiIdPppoeServerApi($controller, $pppoeServer)){
            $pppoeServer->api_id = $this->existPppoeServerApi($controller, $pppoeServer);
        }
        
        if($pppoeServer->api_id){
            
            if(!$this->editPppoeServerApi($controller, $pppoeServer)){
                return false;
            }
            
            return $pppoeServer;
        }   
       
        $pppoeServer = $this->addPppoeServerApi($controller, $pppoeServer);
        
        if(!$pppoeServer->api_id){
            return false;
        }
        
        return $pppoeServer;
    }
    
    private function existApiIdPppoeServerApi($controller, $pppoeServer){
        
        if(!$pppoeServer->api_id){
           return false;
        }
         
        if(!$this->connectApi($controller, 5)){
           return false;
        }

        $response = $this->_client->wr([
            '/interface/pppoe-server/server/print',
             '?.id='.$pppoeServer->api_id
             ]);      
        
        return count($response) > 0;       
    }
  
    private function existPppoeServerApi($controller, $pppoeServer){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }

        $response = $this->_client->wr([
            '/interface/pppoe-server/server/print', 
            '?service-name='.$pppoeServer->service_name
            ]);
     
        return count($response) > 0 ? $response[0]['.id'] : false;
    	
    }
    
    protected function getPppoeServersApi($controller){
        
        if(!$this->integrationEnabled($controller) || !$this->connectApi($controller, 5)){
           return [];
        }

        $pppoeServers = $this->_client->wr('/interface/pppoe-server/server/print');

        $array = [];
   
        foreach($pppoeServers as $pppoeServer){

            if(!array_key_exists('service-name', $pppoeServer)){
                $pppoeServer['service-name'] = '';
            }   
            
            $service_name = $this->isThisController($controller, $pppoeServer['service-name']);            
      
            $array[$service_name['value']] = [
                'api_id' => $pppoeServer['.id'],
                'service_name' => ['value'] ,
                'interface' => $this->val('interface', $pppoeServer),
                'one_session_per_host' =>  $this->val('one-session-per-host', $pppoeServer),
                'authentication' =>  $this->val('authentication', $pppoeServer),
                'disabled' => $pppoeServer['disabled']  == 'false' ? 'no' : 'yes',
                'other' => $service_name['other']
                ];
             
         }
         
         return $array;
       
    }
    
    
    
    
    //dhcp servers
    
    private function addDhcpServerApi($controller, $dhcpServer){
       
        if(!$this->connectApi($controller, 5)){
            return false;
        }
        
        $system_code = ':3SPBr13n:' . Configure::read('project.internal_system_id') .':'. $controller->id;
                
        $data = [
                'name' => $dhcpServer->name . $system_code,
                'interface' => $dhcpServer->interface ? $dhcpServer->interface : null,
                'address-pool' => ($dhcpServer->address_pool == 'static-only') ? $dhcpServer->address_pool : $dhcpServer->address_pool . $system_code,
                'disabled' => $dhcpServer->disabled ? $dhcpServer->disabled : null,
                'lease-time' => $dhcpServer->leases_time ? $dhcpServer->leases_time : null,
                'bootp-support' => 'dynamic',
                'bootp-lease-time' => 'lease-time'
            ];

        $data = array_filter($data, function($v){
            return !is_null($v);
        });

        $query = new Query('/ip/dhcp-server/add', QueryHelper::parseAttributes($data));
        $response =  $this->_client->wr($query);   

        if(array_key_exists('after', $response)){
            if(array_key_exists('ret', $response['after'])){
                $dhcpServer->api_id = $response['after']['ret'];
            }else{
                if(array_key_exists('message', $response['after'])){

                    $data_error = new \stdClass;               
                    $data_error->message = $response['after']['message'];
                    $data_error->dhcpServer = $data;
                    
                    $event = new Event('Mikrotik.Error', $this, [
                        'msg' => 'Error al intentar agregar el DHCP Server al controlador',
                        'data' => $data_error
                    ]);
                    
                    $this->_ctrl->getEventManager()->dispatch($event);
    
                    return false;


                }
            }
        } 

        return $dhcpServer;
       
    }
    
    private function editDhcpServerApi($controller, $dhcpServer){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        $system_code = ':3SPBr13n:' . Configure::read('project.internal_system_id') .':'. $controller->id;
                
        $data = [
            '.id' => $dhcpServer->api_id,
            'name' => $dhcpServer->name . $system_code,
            'interface' => $dhcpServer->interface,
            'address-pool' => $dhcpServer->address_pool . $system_code,
            'disabled' => $dhcpServer->disabled,
            'lease-time' => $dhcpServer->leases_time,
            'bootp-support' => 'dynamic',
            'bootp-lease-time' => 'lease-time'
            ];

        $query = new Query('/ip/dhcp-server/set', QueryHelper::parseAttributes($data));
        $response =  $this->_client->wr($query);

        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass;  
                $data_error->message = $response['after']['message'];
                $data_error->dhcpServer = $data;
                
                $event = new Event('Mikrotik.Error', $this, [
                        'msg' => 'Error al intentar editar la (DHCP Server) en el controlador',
                        'data' => $data_error
                    ]);

                $this->_ctrl->getEventManager()->dispatch($event);

                return false;

            }
        }       
     
        return $dhcpServer;      
        
    }  
     
    protected function deleteDhcpServerInControllerApi($controller, $api_id){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        $response = $this->_client->wr([
            '/ip/dhcp-server/remove',
            '=.id='.$api_id
            ]);

        
        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass;  
                $data_error->message = $response['after']['message'];
                $data_error->api_id = $api_id;
                
                $event = new Event('Mikrotik.Warning', $this, [
            		'msg' => 'El (dhcp-server) ya estaba eliminada en el controlador',
            		'data' => $data_error
            	]);
            	
            	$this->_ctrl->getEventManager()->dispatch($event);
            	
                return true;
            }
        }  	
        
        return true;
    }
     
    protected function deleteDhcpServerByApiIdApi($controller, $dhcp_server_api_id){
        
        if(!$this->connectApi($controller, 5)){
          return false;
        }
   
    	$response = $this->_client->wr([
            '/ip/dhcp-server/remove',
            '=.id='.$dhcp_server_api_id
            ]);

        
        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass;  
                $data_error->message = $response['after']['message'];
                $data_error->api_id = $api_id;
                
                $event = new Event('Mikrotik.Warning', $this, [
            		'msg' => 'El (dhcp-server) ya estaba eliminada en el controlador',
            		'data' => $data_error
            	]);
            	
            	$this->_ctrl->getEventManager()->dispatch($event);
            	
                return true;
            }
        }  	
        
        return true;
    }
    
    protected function syncDhcpServerApi($controller, $dhcpServer){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        if(!$this->existApiIdDhcpServerApi($controller, $dhcpServer)){
          $dhcpServer->api_id = $this->existDhcpServerApi($controller, $dhcpServer);  
        }
        
        if($dhcpServer->api_id){
            
            if(!$this->editDhcpServerApi($controller, $dhcpServer)){
                return false;
            }
            
            return $dhcpServer;
        }   
       
        $dhcpServer = $this->addDhcpServerApi($controller, $dhcpServer);
        
        if(!$dhcpServer->api_id){
            return false;
        }
        
        return $dhcpServer;
    }
    
    private function existApiIdDhcpServerApi($controller, $dhcpServer){
        
        if(!$dhcpServer->api_id){
           return false;
        }
         
        if(!$this->connectApi($controller, 5)){
           return false;
        }

        $response = $this->_client->wr([
            '/ip/dhcp-server/print',
             '?.id='.$queue->api_id
             ]);      
        
        return count($response) > 0;        
    }
    
    private function existDhcpServerApi($controller, $dhcpServer){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        $system_code = ':3SPBr13n:' . Configure::read('project.internal_system_id') .':'. $controller->id;

        $response = $this->_client->wr([
            '/ip/dhcp-server/print', 
            '?name='.$dhcpServer->name.$system_code
            ]);
     
        return count($response) > 0 ? $response[0]['.id'] : false;       
    
    }

    protected function getDhcpServersApi($controller){
        
        if(!$this->integrationEnabled($controller) || !$this->connectApi($controller, 5)){
           return [];
        }

        $dhcpServers = $this->_client->wr('/ip/dhcp-server/print');            
        
        $array = [];
   
        foreach($dhcpServers as $dhcpServer){             
            
            $name = $this->isThisController($controller, $dhcpServer['name']);
            
            $address_pool = $this->isThisController($controller, $dhcpServer['address-pool']);
         
            $array[$name['value']] = [
                'api_id' => $dhcpServer['.id'],
                'name' => $name['value'],
                'interface' => $this->val('interface', $dhcpServer),
                'address_pool' => $address_pool['value'],
                'leases_time' => $this->val('lease-time', $dhcpServer),
                'disabled' => $dhcpServer['disabled'] == 'false' ? 'no' : 'yes',
                'other' => $name['other']
                ];
            
        }  
       
        return $array;
       
    }   
    
    
    //dhcp networks
    
    private function addNetworkApi($controller, $tnetwork){
       
        if(!$this->connectApi($controller, 5)){
            return false;
        }
        
        $system_code = ':3SPBr13n:' . Configure::read('project.internal_system_id') .':'. $controller->id;
        
        $data = [
            'address' => $tnetwork->address ? $tnetwork->address : null,
            'gateway' => $tnetwork->gateway ? $tnetwork->gateway : null,
            'dns-server' => $tnetwork->dns_server ? $tnetwork->dns_server : null,
            'comment' => $tnetwork->comment . $system_code,
            ];

        $data = array_filter($data, function($v){
            return !is_null($v);
        });

        $query = new Query('/ip/dhcp-server/network/add', QueryHelper::parseAttributes($data));
        $response =  $this->_client->wr($query);   

        if(array_key_exists('after', $response)){
            if(array_key_exists('ret', $response['after'])){
                $tnetwork->api_id = $response['after']['ret'];
            }else{
                if(array_key_exists('message', $response['after'])){

                    $data_error = new \stdClass; 
                    $data_error->message = $response['after']['message'];
                    $data_error->tnetwork = $tnetwork;
                    
                    $event = new Event('Mikrotik.Error', $this, [
                        'msg' => 'Error al intentar agregar el network al controlador',
                        'data' => $data_error
                    ]);
                    
                    $this->_ctrl->getEventManager()->dispatch($event);

                    return false;
                }
            }
        }
      
        return $tnetwork;
       
    }
    
    private function editNetworkApi($controller, $tnetwork){
    
        if(!$this->connectApi($controller, 5)){
           return false;
        }
                
        $system_code = ':3SPBr13n:' . Configure::read('project.internal_system_id') .':'. $controller->id;
   
        $data = [
            '.id' => $tnetwork->api_id,
            'address' => $tnetwork->address,
            'gateway' => $tnetwork->gateway,
            'dns-server' => $tnetwork->dns_server,
            'comment' => $tnetwork->comment . $system_code
            ];

        $query = new Query('/ip/dhcp-server/network/set', QueryHelper::parseAttributes($data));
        $response =  $this->_client->wr($query);

        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass;   
                $data_error->message = $response['after']['message'];
                $data_error->tnetwork = $tnetwork;
                
                $event = new Event('Mikrotik.Error', $this, [
                        'msg' => 'Error al intentar editar el network en el controlador',
                        'data' => $data_error
                    ]);
    
                $this->_ctrl->getEventManager()->dispatch($event);
    
                return false;

            }
        }

        return true;      
        
    }   
     
    protected function deleteNetworkByApiIdApi($controller, $network_api_id){
        
        if(!$this->connectApi($controller, 5)){
          return false;
        }

        $response = $this->_client->wr([
            '/ip/dhcp-server/network/remove',
            '=.id='.$network_api_id
            ]);

        
        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass;       
                $data_error->message = $e->getResponses()->getProperty('message');
                $data_error->network_api_id =  $network_api_id;
                
                $event = new Event('Mikrotik.Warning', $this, [
                    'msg' => 'El (network) ya estaba eliminado del controlador',
                    'data' => $data_error
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
                
                return true;

            }
        }  
        
        return true;
    }

    protected function deleteNetworksInControllerApi($controller, $api_id){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
   
        $response = $this->_client->wr([
            '/ip/dhcp-server/network/remove',
            '=.id='.$api_id
            ]);

        
        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass;             
                $data_error->message = $response['after']['message'];
                $data_error->api_id = $api_id;
                
                $event = new Event('Mikrotik.Warning', $this, [
            		'msg' => 'El (network) ya estaba eliminada en el controlador',
            		'data' => $data_error
            	]);
            	
            	$this->_ctrl->getEventManager()->dispatch($event);
            	
                return true;

            }
        }        
        
        return true;
    }
    
    protected function syncNetworkApi($controller, $tnetwork){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        if(!$this->existApiIdNetworkApi($controller, $tnetwork)){
           $tnetwork->api_id = $this->existNetworkApi($controller, $tnetwork); 
        }
        
        if($tnetwork->api_id){
            
            if(!$this->editNetworkApi($controller, $tnetwork)){
                return false;
            }
            
            return $tnetwork;
        }   
       
        $tnetwork = $this->addNetworkApi($controller, $tnetwork);
        
        if(!$tnetwork->api_id){
            return false;
        }
        
        return $tnetwork;
    }
    
    private function existApiIdNetworkApi($controller, $tnetwork){
        
        if(!$tnetwork->api_id){
           return false;
        }
         
        if(!$this->connectApi($controller, 5)){
           return false;
        }

        $response = $this->_client->wr([
            '/ip/dhcp-server/network/print',
             '?.id='.$tnetwork->api_id
             ]);      
        
        return count($response) > 0;        
    }
    
    private function existNetworkApi($controller, $tnetwork){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }

        $response = $this->_client->wr([
            '/ip/dhcp-server/network/print', 
            '?name='.$tnetwork->name
            ]);
     
        return count($response) > 0 ? $response[0]['.id'] : false;   
    }
    
    protected function getNetworksApi($controller){
        
        if(!$this->integrationEnabled($controller) || !$this->connectApi($controller, 5)){
           return [];
        }   

        $networks = $this->_client->wr('/ip/dhcp-server/network/print'); 
        
        $array = [];
   
        foreach($networks as $network){

            if(!array_key_exists('comment', $network)){
                $network['comment'] = '';
            }   
            
            $comment = $this->isThisController($controller, $network['comment']);
                         
            $array[$comment['value']] = [
                'api_id' => $network['.id'],
                'comment' => $comment['value'],
                'address' => $this->val('address', $network),
                'gateway' => $this->val('gateway', $network),
                'dns_server' => $this->val('dns-server', $network),
                'other' => $comment['other']
            ];
        
         }
         
         return $array;
       
    }
    

    
    //pools
    
    private function addPoolApi($controller, $tpool){
       
        if(!$this->connectApi($controller, 5)){
            return false;
        }
        
        $system_code = ':3SPBr13n:' . Configure::read('project.internal_system_id') .':'. $controller->id;
        
        $data = [
            'name' => $tpool->name . $system_code,
            'ranges' => $tpool->ranges ? $tpool->ranges : null,
            ];

        $data = array_filter($data, function($v){
            return !is_null($v);
        });

        $query = new Query('/ip/pool/add', QueryHelper::parseAttributes($data));
        $response =  $this->_client->wr($query);   

        if(array_key_exists('after', $response)){
            if(array_key_exists('ret', $response['after'])){
                $tpool->api_id = $response['after']['ret'];
            }else{
                if(array_key_exists('message', $response['after'])){

                    $data_error = new \stdClass;       
                    $data_error->message = $response['after']['message'];
                    $data_error->tpool = $tpool;
                    
                    $event = new Event('Mikrotik.Error', $this, [
                        'msg' => 'Error al intentar agregar el (pool) al controlador',
                        'data' => $data_error
                    ]);
                    
                    $this->_ctrl->getEventManager()->dispatch($event);
    
                    return false;
                }
            }
        }

        return $tpool;
       
    }
    
    private function editPoolApi($controller, $tpool){
    
        if(!$this->connectApi($controller, 5)){
           return false;
        }
            
        $system_code = ':3SPBr13n:' . Configure::read('project.internal_system_id') .':'. $controller->id;
        
        $data = [
            '.id' => $tpool->api_id,
            'name' => $tpool->name . $system_code,
            'ranges' => $tpool->ranges,
            ];

        $query = new Query('/ip/pool/set', QueryHelper::parseAttributes($data));
        $response =  $this->_client->wr($query);

        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){
         
                $data_error = new \stdClass;            
                $data_error->message = $response['after']['message'];
                $data_error->tpool = $tpool;
                
                $event = new Event('Mikrotik.Error', $this, [
                        'msg' => 'Error al intentar editar el (pool) en el controlador',
                        'data' => $data_error
                    ]);

                $this->_ctrl->getEventManager()->dispatch($event);

                return false;

            }
        }
       
        return true;      
        
    }    

    protected function deletePoolInControllerApi($controller, $api_id){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        $response = $this->_client->wr([
            '/ip/pool/remove',
            '=.id='.$api_id
            ]);

        
        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass; 
                $data_error->message = $response['after'];
                $data_error->api_id = $api_id;
                
                $event = new Event('Mikrotik.Warning', $this, [
            		'msg' => 'El (pool) ya estaba eliminada en el controlador',
            		'data' => $data_error
            	]);
            	
            	$this->_ctrl->getEventManager()->dispatch($event);
            	
                return true;
            }
        }
    	        
        return true;
    }
    
    protected function syncPoolApi($controller, $tpool){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        if(!$this->existApiIdPoolApi($controller, $tpool)){
            $tpool->api_id = $this->existPoolApi($controller, $tpool);
        }
        
        if($tpool->api_id){
            
            if(!$this->editPoolApi($controller, $tpool)){
                return false;
            }
            
            return $tpool;
        }   
       
        $tpool = $this->addPoolApi($controller, $tpool);
        
        if(!$tpool->api_id){
            return false;
        }
        
        return $tpool;
    }
    
    private function existApiIdPoolApi($controller, $tpool){
        
        if(!$tpool->api_id){
           return false;
        }
         
        if(!$this->connectApi($controller, 5)){
           return false;
        }

        $response = $this->_client->wr([
            '/ip/pool/print',
             '?.id='.$tpool->api_id
             ]);      
        
        return count($response) > 0;
    }
    
    private function existPoolApi($controller, $tpool){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
    	
        $system_code = ':3SPBr13n:' . Configure::read('project.internal_system_id') .':'. $controller->id;
        
        $response = $this->_client->wr([
            '/ip/pool/print', 
            '?name='.$tpool->name . $system_code
            ]);
     
        return count($response) > 0 ? $response[0]['.id'] : false;
    }
    
    protected function getPoolsApi($controller){
        
        if(!$this->integrationEnabled($controller) || !$this->connectApi($controller, 5)){
           return [];
        }
 
        $pools = $this->_client->wr('/ip/pool/print'); 
        
        $array = [];
   
        foreach($pools as $pool){
            
            $name = $this->isThisController($controller, $pool['name']);
            
            $array[$name['value']] = [
                'api_id' => $pool['.id'],
                'name' => $name['value'],
                'ranges' => $this->val('ranges', $pool),
                'other' => $name['other']
            ];
            
         }
         
         return $array;
       
    }
 
    
    //ip firewall 
    
    private function addAddressListApi($controller, $addressList){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        $system_code = ':3SPBr13n:' . Configure::read('project.internal_system_id') .':'. $controller->id;
        
        $data = [
            'list' => $addressList->list . $system_code,
            'address' => $addressList->address ? $addressList->address : null,
            'comment' => $addressList->comment . $system_code,
        ];

        $data = array_filter($data, function($v){
            return !is_null($v);
        });


        $query = new Query('/ip/firewall/address-list/add', QueryHelper::parseAttributes($data));
        $response =  $this->_client->wr($query);   

        $api_id = null;

        if(array_key_exists('after', $response)){
            if(array_key_exists('ret', $response['after'])){

                $api_id = $response['after']['ret'];

            }else{
                if(array_key_exists('message', $response['after'])){

                    $data_error = new \stdClass;         
                    $data_error->message = $response['after']['message'];
                    $data_error->addressList = $addressList;
                    
                    $event = new Event('Mikrotik.Error', $this, [
                        'msg' => 'Error al intentar agregar el (address-list) al controlador',
                        'data' => $data_error
                    ]);
                    
                    $this->_ctrl->getEventManager()->dispatch($event);
                    
                    return false;
                }
            }
        }
        
        //add web proxy access
        
        if(in_array($addressList->list, ['Avisos', 'Cortes'])){
            $this->syncWebProxyAccessApi($controller, $addressList);
        }
        
        return $api_id;
        
    }
    
    private function editAddressListApi($controller, $addressList){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }      

    	$system_code = ':3SPBr13n:' . Configure::read('project.internal_system_id') .':'. $controller->id;
      
        $data = [
            '.id' => $addressList->api_id,
            'list' => $addressList->list . $system_code,
            'address' => $addressList->address,
            'comment' => $addressList->comment . $system_code,
        ];

           
        $query = new Query('/ip/firewall/address-list/set', QueryHelper::parseAttributes($data));
        $response =  $this->_client->wr($query);

        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass;          
                $data_error->message = $response['after']['message'];
                $data_error->addressList = $addressList;
                
                $event = new Event('Mikrotik.Error', $this, [
                    'msg' => 'Error al intentar editar el (address-list) del controlador',
                    'data' => $data_error
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
                
                return false;
            }
        }    

        //edit web proxy access
        
        if(in_array($addressList->list, ['Avisos', 'Cortes'])){
            $this->syncWebProxyAccessApi($controller, $addressList);
        }
                
        return $addressList;    
     
    }
    
    protected function deleteAddressListByApiIdApi($controller, $address_list_api_id){
        
        if(!$this->connectApi($controller, 5)){
          return false;
        }

        $response = $this->_client->wr([
            '/ip/firewall/address-list/remove',
            '=.id='.$address_list_api_id
            ]);

        
        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass;    
                $data_error->message = $response['after']['message'];
                $data_error->address_list_api_id =  $address_list_api_id;
                
                $event = new Event('Mikrotik.Warning', $this, [
                    'msg' => 'El (address list) ya estaba eliminado del controlador',
                    'data' => $data_error
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
                
                return true;
            }
        }
   
         //remove web proxy access ???
        
        // if(in_array($addressList->list, ['Avisos', 'Cortes'])){
        //     $this->deleteWebProxyAccessApi($controller, $addressList);
        // }
        
        
        return true;
    }
    
    protected function deleteAddressListApi($controller, $addressList){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        // if(!$addressList->api_id){
	        $addressList->api_id = $this->existAddressListApi($controller, $addressList);
	    // }
	    
	    if(!$addressList->api_id){
	        
	        $data_error = new \stdClass; 
            $data_error->addressList = $addressList;
            
            $event = new Event('Mikrotik.Warning', $this, [
                'msg' => 'El (address-list) ya estaba eliminado del controlador',
                'data' => $data_error
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return $addressList;
        }
        
        $response = $this->_client->wr([
            '/ip/firewall/address-list/remove',
             '=.id='.$addressList->api_id
             ]);

        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass;               
                $data_error->message = $response['after']['message'];
                $data_error->addressList = $addressList;
                
                $event = new Event('Mikrotik.Warning', $this, [
                    'msg' => 'El (address-list) ya estaba eliminado del controlador',
                    'data' => $data_error
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
                
                return $addressList;
            }
        }        

         //remove web proxy access
        
        if(in_array($addressList->list, ['Avisos', 'Cortes'])){
            $this->deleteWebProxyAccessApi($controller, $addressList);
        }
	
        return $addressList;
    }
    
    protected function syncAddressListApi($controller, $addressList){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
           
        if(!$this->existApiIdAddressListApi($controller, $addressList)){
            $addressList->api_id = $this->existAddressListApi($controller, $addressList);
        }
        
        if($addressList->api_id){
            
            if(!$this->editAddressListApi($controller, $addressList)){
                return false;
            }
            
            return $addressList;
        }   
       
        $addressList->api_id = $this->addAddressListApi($controller, $addressList);
        
        if(!$addressList->api_id){
            return false;
        }
        
      
        return $addressList;
    }
    
    private function existApiIdAddressListApi($controller, $addressList){
        
        if(!$addressList->api_id){
           return false;
        }
         
        if(!$this->connectApi($controller, 5)){
           return false;
        }

        $response = $this->_client->wr([
            '/ip/firewall/address-list/print',
             '?.id='.$addressList->api_id
             ]);      
        
        return count($response) > 0;        
    }
    
    private function existAddressListApi($controller, $addressList){
        
        if(!$this->connectApi($controller, 5)){
          return false;
        }
        
        $system_code = ':3SPBr13n:' . Configure::read('project.internal_system_id') .':'. $controller->id;
      
        $response = $this->_client->wr([
            '/ip/firewall/address-list/print', 
            '?list='.$addressList->list.$system_code,
            '?address='.$addressList->address
            ]);  
        
        return count($response) > 0 ? $response[0]['.id'] : false;
    }
    
    protected function getAddressListsApi($controller, $mylists ){
        
        if(!$this->integrationEnabled($controller) || !$this->connectApi($controller, 5)){
           return [];
        }
        
        $addressLists = $this->_client->wr('/ip/firewall/address-list/print'); 
        
        $array = [];
        
        foreach($addressLists as $addressList){
                      
            if($addressList['dynamic'] == 'true'){
                continue;
            }

            if(!array_key_exists('comment', $addressList)){
                $addressList['comment'] = '';
            }
            
            $comment = $this->isThisController($controller, $addressList['comment']);
            
            $list = $this->isThisController($controller, $addressList['list']);
            
            $other = $comment['other'];
            
            if(!in_array($list['value'], $mylists)){
                $other = true;
            }
             
           if($list['value'] != 'Privadas'){

               $array[$list['value'].':'.$addressList['address']] = [
                   'api_id' => $addressList['.id'],
                  'list' => $list['value'],
                  'address' => $this->val('address', $addressList),
                  'comment' => $comment['value'] ,
                  'other' => $other
               ];

           }            
        }
         
        ksort($array);
       
        return $array;
    }
    
    
    
    //proxy access
    
    protected function addWebProxyAccessApi($controller, $addressList){
  
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        $system_code = ':3SPBr13n:' . Configure::read('project.internal_system_id') .':'. $controller->id;
        
        $redirect_to = $this->_ctrl->_general_paraments->system->uri_http;
        $redirect_to .= ':81';
        $redirect_to .= ($addressList->list == 'Cortes') ? '/corte.html' : '/aviso.html';
        $redirect_to .= '?ip='.$addressList->address; 
        
        $data = [
            'src-address' => $addressList->address ? $addressList->address : null,
            'dst-port' => 80,
            'action' => 'deny',
            'redirect-to' => $redirect_to ? $redirect_to : null,
            'comment' => $addressList->comment . $system_code,
        ];

        $data = array_filter($data, function($v){
            return !is_null($v);
        });

        $query = new Query('/ip/proxy/access/add', QueryHelper::parseAttributes($data));
        $response =  $this->_client->wr($query);  
        
        $api_id = null;

        if(array_key_exists('after', $response)){
            if(array_key_exists('ret', $response['after'])){
                $api_id =  $response['after']['ret'];
            }else{
                if(array_key_exists('message', $response['after'])){

                    $data_error = new \stdClass;      
                    $data_error->message =  $response['after']['message'];
                    $data_error->addressList = $addressList;
                    
                    $event = new Event('Mikrotik.Error', $this, [
                        'msg' => 'Error al intentar agregar el (proxy-access) al controlador',
                        'data' => $data_error
                    ]);
                    
                    $this->_ctrl->getEventManager()->dispatch($event);
                    
                    return false;
                }
            }
        }

        return $api_id;
        
    }

    private function editWebProxyAccessApi($controller, $addressList, $web_proxy_access_api_id){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }    
    	
    	$system_code = ':3SPBr13n:' . Configure::read('project.internal_system_id') .':'. $controller->id;
      
        $redirect_to = $this->_ctrl->_general_paraments->system->uri_http;
        $redirect_to .= ':81';
        $redirect_to .= ($addressList->list == 'Cortes') ? '/corte.html' : '/aviso.html';
        $redirect_to .= '?ip='.$addressList->address; 
        
        $data = [
            '.id' => $web_proxy_access_api_id,
            'src-address' => $addressList->address,
            'dst-port' => 80,
            'action' => 'deny',
            'redirect-to' => $redirect_to,
            'comment' => $addressList->comment . $system_code,
        ];

             
        $query = new Query('/ip/proxy/access/set', QueryHelper::parseAttributes($data));
        $response =  $this->_client->wr($query);

        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass;          
                $data_error->message = $response['after']['message'];
                $data_error->addressList = $addressList;
                
                $event = new Event('Mikrotik.Error', $this, [
                    'msg' => 'Error al intentar editar el (proxy-access) del controlador',
                    'data' => $data_error
                ]);
                
                return false;
            }
        }
                
        return true;    
     
    }
    
    private function existWebProxyAccessApi($controller, $addressList){
        
        if(!$this->connectApi($controller, 5)){
          return false;
        }

        $response = $this->_client->wr([
            '/ip/proxy/access/print', 
            '?src-address='.$addressList->before_address
            ]);
     
        return count($response) > 0 ? $response[0]['.id'] : false;
    }
    
    private function deleteWebProxyAccessApi($controller, $addressList){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        $addressList->before_address = $addressList->address;
        
        $web_proxy_access_api_id = $this->existWebProxyAccessApi($controller, $addressList);
	  
	    if(!$web_proxy_access_api_id){
	        
	        $data_error = new \stdClass; 
            $data_error->addressList = $addressList;
            
            $event = new Event('Mikrotik.Warning', $this, [
                'msg' => 'El (proxy-access) ya estaba eliminado del controlador',
                'data' => $data_error
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return $addressList;
        }
        
        $response = $this->_client->wr([
            '/ip/proxy/access/remove',
             '=.id='.$web_proxy_access_api_id
             ]);

        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass;                
                $data_error->message = $response['after']['message'];
                $data_error->addressList = $addressList;
                
                $event = new Event('Mikrotik.Warning', $this, [
                    'msg' => 'El (proxy-access) ya estaba eliminado del controlador',
                    'data' => $data_error
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
                
                return $addressList;

            }
        }       
	
        return $addressList;
    }
    
    private function deleteWebProxyAccessByApiIdApi($controller, $web_proxy_access_api_id){
        
        if(!$this->connectApi($controller, 5)){
          return false;
        }

        $response = $this->_client->wr([
            '/ip/proxy/access/remove',
            '=.id='.$web_proxy_access_api_id
            ]);

        
        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass; 
                $data_error->message = $response['after']['message'];
                $data_error->web_proxy_access_api_id =  $web_proxy_access_api_id;
                
                $event = new Event('Mikrotik.Warning', $this, [
                    'msg' => 'El (proxy-access) list ya estaba eliminado del controlador',
                    'data' => $data_error
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
                
                return true;
            }
        }        
   
        return true;
    }
    
    private function syncWebProxyAccessApi($controller, $addressList){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }
        
        $web_proxy_access_api_id = $this->existWebProxyAccessApi($controller, $addressList);
      
        if($web_proxy_access_api_id){
            
            if(!$this->editWebProxyAccessApi($controller, $addressList, $web_proxy_access_api_id)){
                return false;
            }
            
            return true;
        }   
        
        if(!$this->addWebProxyAccessApi($controller, $addressList)){
            return false;
        }
      
        return true;
    }



    // ppp active    
    
    protected function getActivesApi($controller){
        
        if(!$this->integrationEnabled($controller) || !$this->connectApi($controller, 5)){
           return [];
        }
         
        $actives = $this->_client->wr('/ppp/active/print'); 

        $array = [];
        
        foreach($actives as $active){

            if(!array_key_exists('comment', $active)){
                $active['comment'] = '';
            }   
            
            $comment = $this->isThisController($controller, $active['comment']);
   
            $array[$active['name']] = [
        	    'api_id' => $active['.id'],
                'name' => $this->val('name', $active),
                'address' => $this->val('address', $active),
                'comment' =>  $comment['value'],
                'other' => $comment['other']
            ];
        }
        
         
         ksort($array);
       
         return $array;
       
    }

    private function existActiveApi($controller, $secret){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }

        $response = $this->_client->wr([
            '/ppp/active/print', 
            '?address='.$secret->remote_address
            ]);
     
        return count($response) > 0 ? $response[0]['.id'] : false;        
    }
    
    protected function deleteActiveApi($controller, $secret){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }

        // $this->_ctrl->log('deleteActiveApi', 'debug');
        // $this->_ctrl->log($secret, 'debug');

        $response = $this->_client->wr([
            '/ppp/active/print', 
            '?name='.$secret->name
            ]);

        $active_api_id = count($response) > 0 ? $response[0]['.id'] : false;   
        
        if($active_api_id){            
        
            $response = $this->_client->wr([
                '/ppp/active/remove',
                '=.id='.$active_api_id
                ]);  

            // $this->_ctrl->log('response', 'debug');  
            // $this->_ctrl->log($response, 'debug');
        }

        return true;
    }
     
    protected function deleteActiveByApiIdSecretApi($controller, $secret_api_id){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }

        $response = $this->_client->wr([
            '/ppp/secret/print',
             '?.id='.$secret_api_id
             ]);
             

        if(count($response) > 0){


            $response = $this->_client->wr([
                '/ppp/active/print', 
                '?name='.$response[0]['name']
                ]);
    
            $active_api_id = count($response) > 0 ? $response[0]['.id'] : false;   
            
            if($active_api_id){            
            
                $response = $this->_client->wr([
                    '/ppp/active/remove',
                    '=.id='.$active_api_id
                    ]);      
         
            }           
            
        }else{

            return false;
        }         
         
        return true;
    }
    
    protected function deleteActiveByApiIdApi($controller, $active_api_id){
        
        if(!$this->connectApi($controller, 5)){
           return false;
        }

        $response = $this->_client->wr([
            '/ppp/active/remove',
            '=.id='.$active_api_id
            ]);

        
        if(array_key_exists('after', $response)){
            if(array_key_exists('message', $response['after'])){

                $data_error = new \stdClass;                
                $data_error->message = $response['after']['message'];
                $data_error->active_api_id = $active_api_id;
                
                $event = new Event('Mikrotik.Warning', $this, [
                    'msg' => 'El active ya estaba eliminado en el controlador',
                    'data' => $data_error
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
          
                return true;
            }
        }
            
        return true;
    }
    
    
    
    // accounting traffic
    
    public function settingsAccountingTrafficApi($controller, $data){    

        $file = new File(WWW_ROOT . 'mt_scripts/accounting_traffic.sh', false, 0644);
        $aviso_script = $file->read(true, 'r');
        
        $aviso_script = str_replace("[enabled]",    $data->enabled,     $aviso_script);
        $aviso_script = str_replace("[threshold]",  $data->threshold,   $aviso_script);
        
        return $aviso_script;
     }
     
     
     
     //avisos
     
    public function applyConfigAvisosApi($controller, $ip_server){    
 
        $name_file = 'cortes.sh';

        $paraments = $this->_ctrl->request->getSession()->read('paraments');

        Log::debug('$paraments->gral_config');
        Log::debug($paraments->gral_config);

        if($paraments->gral_config->using_avisos){

            $name_file = 'avisos.sh';

        }
        
        $file = new File(WWW_ROOT . 'mt_scripts/'.$name_file, false, 0644);

        $aviso_script = $file->read(true, 'r');
        
        $aviso_script = str_replace("[LIST_PRIVADAS_NAME]", "Privadas:3SPBr13n:"   . Configure::read('project.internal_system_id') .':'. $controller->id, $aviso_script);
        $aviso_script = str_replace("[LIST_CORTES_NAME]",   "Cortes:3SPBr13n:"     . Configure::read('project.internal_system_id') .':'. $controller->id, $aviso_script);
        $aviso_script = str_replace("[LIST_AVISOS_NAME]",   "Avisos:3SPBr13n:"     . Configure::read('project.internal_system_id') .':'. $controller->id, $aviso_script);
        $aviso_script = str_replace("[FORWARD]",            "forward:3SPBr13n:"    . Configure::read('project.internal_system_id') .':'. $controller->id, $aviso_script);
        $aviso_script = str_replace("[REDIRECT]",           "Redirect:3SPBr13n:"   . Configure::read('project.internal_system_id') .':'. $controller->id, $aviso_script);
        
        $aviso_script = str_replace("[IP_SERVER]",  $ip_server, $aviso_script);   
        
        return $aviso_script;      
    }
    
    
    
    //QueuesGraph
     
    public function applyConfigQueuesGraphApi($controller){
             
        $file = new File(WWW_ROOT . 'mt_scripts/queues_graph.sh', false, 0644);
        $aviso_script = $file->read(true, 'r');
       
        return $aviso_script;
    }
    
    
    
    //generar certificado
    
    public function generateCertificateAndApplyApi($controller){ 

        $file = new File(WWW_ROOT . 'mt_scripts/certificate.sh', false, 0644);
        $aviso_script = $file->read(true, 'r');        
        $aviso_script = str_replace("[CERTIFICATE_NAME]",   "ca-3SPBr13n",      $aviso_script);
        $aviso_script = str_replace("[COMMON_NAME]",        "3SPBr13n-ca",      $aviso_script);
        
        return $aviso_script;    
    }
    
    
    //Config temporal accesos al sistema
     
    public function applyConfigAccessNewCustomerApi($controller, $ip_server, $network){
       
        
        $file = new File(WWW_ROOT . 'mt_scripts/access_customer.sh', false, 0644);
        $aviso_script = $file->read(true, 'r');
        
        $name_jumper = "acceso-nuevo-cliente:3SPBr13n:" . Configure::read('project.internal_system_id') .':'. $controller->id;
      
        $aviso_script = str_replace("[NAME_JUMPER]",  $name_jumper, $aviso_script);
        $aviso_script = str_replace("[NETWORK]",  $network, $aviso_script);
        $aviso_script = str_replace("[IP_SERVER]",  $ip_server, $aviso_script);

        return $aviso_script;       
        
    }
    
    public function applyConfigAccessNewCustomerIpFijaApi($controller, $ip_server, $pools){
       
        $address_list_name = $controller->tcontroller->address_list_name . ':3SPBr13n:' .  Configure::read('project.internal_system_id') .':'. $controller->id;
        $name_jumper = "control-no-cliente:3SPBr13n:" . Configure::read('project.internal_system_id') .':'. $controller->id;
        
        $aviso_script = "/ip firewall filter\n";
        $first = true;
        foreach($pools as $pool){
            if($first){
                $aviso_script .= 'add action=jump comment="Control no clientes - ISPBrain" chain=forward jump-target="'.$name_jumper.'" src-address='.$pool->addresses."\n"; 
                $first = false;
            }else{
                $aviso_script .= 'add action=jump chain=forward jump-target="'.$name_jumper.'" src-address='.$pool->addresses."\n"; 
            }            
        }
        
        $aviso_script .= 'add action=accept chain="'.$name_jumper.'" src-address-list="'.$address_list_name.'"'."\n";
        $aviso_script .= 'add action=accept chain="'.$name_jumper.'" dst-address='.$ip_server."\n";
        $aviso_script .= 'add action=drop chain="'.$name_jumper.'"'."\n";

        return $aviso_script;        
        
    }
    
}
   
