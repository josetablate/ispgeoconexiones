<?php
namespace App\Controller\Component\Integrations;

use App\Controller\Component\Integrations\Mikrotik;


/**
 * Integration component
 */
abstract class MikrotikDhcp extends Mikrotik
{
     
     protected $modelDhcpServers;
     protected $modelProfiles;
     protected $modelNetworks;
     protected $modelGateways;
     protected $modelIpExcluded;
     protected $modelPools;
     protected $modelPlans;
     protected $modelQueues;
     protected $modelLeases;     
     protected $modelControllers;  
     
    
     public function initialize(array $config) {
          
          parent::initialize($config);     

     }
     
     protected function initEventManager() {

         parent::initEventManager();
     }    
    

     //dhcp servers

     abstract protected function getDhcpServers($controller_id);
     abstract protected function getDhcpServersArray($controller_id);
     abstract protected function getDhcpServersInController($controller);
     abstract protected function addDhcpServer($data);
     abstract protected function editDhcpServer($data);
     abstract protected function deleteDhcpServerInController($controller, $api_id);
     abstract protected function deleteDhcpServer($id);
     abstract protected function syncDhcpServer($id);
     

     //profiles

     abstract protected function getProfiles($controller_id);
     abstract protected function getProfilesArray($controller_id);
     abstract protected function getProfilesInController($controller);
     abstract protected function addProfile($data);
     abstract protected function deleteProfile($id);
     abstract protected function editProfile($controller, $data);

     //networks

     abstract protected function getNetworks($controller_id);
     abstract protected function addNetwork($data);
     abstract protected function editNetwork($controller, $data);
     abstract protected function deleteNetwork($id);
     abstract protected function validateNetwokUsed($tnetwork);
     abstract protected function deleteNetworksInController($controller, $api_id);
     abstract protected function syncNetwork($id);
     abstract protected function getNetworksArray($controller_id);
     abstract protected function getNetworksInController($controller);
     abstract protected function moveNetwork($data);
     

     //gateways

     abstract protected function getGateways($controller_id);
     abstract protected function addGateway($data);
     abstract protected function editGateway($controller, $data);
     abstract protected function deleteGateway($id);
     abstract protected function deleteGatewayInController($controller, $api_id);
     abstract protected function syncGateway($id);
     abstract protected function getGatewaysArray($controller_id);
     abstract protected function getGatewaysInController($controller);
     
     //pools

     abstract protected function getPools($controller_id);
     abstract protected function addPool($data);
     abstract protected function editPool($controller, $data);
     abstract protected function deletePool($id);
     abstract protected function deletePoolInController($controller, $api_id);
     abstract protected function syncPool($id);
     abstract protected function getPoolsArray($controller_id);
     abstract protected function getPoolsInController($controller);
     
     
     
     //queues

     abstract protected function addQueue($connection);
     abstract protected function deleteQueue($connection);
     abstract protected function getQueue($connection);
     abstract protected function editQueue($connection);
     abstract protected function deleteQueueInController($controller, $tqueue_api_id);
     abstract protected function getQueuesAndConnectionsArray($controller);
     abstract protected function getQueuesInController($controller);
     abstract protected function getQueues($controller);   
     abstract protected function generateQueueName($connection);
     abstract protected function syncQueue($connection);
     
     

     //dhcp leases

     abstract protected function addLease($connection);
     abstract protected function editLease($connection);
     abstract protected function deleteLease($connection);
     abstract protected function getLease($connection);
     abstract protected function deleteLeaseInController($controller, $tqueue_api_id);
     abstract protected function getLeasesAndConnectionsArray($controller);
     abstract protected function getLeasesInController($controller);
     abstract protected function getLeases($controller);
     abstract protected function syncLease($connection);     

     
     // firewall address lists 

     abstract protected function editAddressList($connection);
     abstract protected function syncAddressList($addressListId);
     abstract protected function syncAddressListsCorteByConnection($connection);
     abstract protected function syncAddressListsAvisoByConnection($connection);
     abstract protected function getAddressListsArray($controller_id);
     abstract protected function deleteAddressListInController($controller, $address_list_api_id);
     abstract protected function getAddressListsInController($controller);    

 
    
}
