<?php
namespace App\Controller\Component\Integrations;

use App\Controller\Component\Common\MyComponent;

/**
 * Integration component
 */
class IntegrationRouter extends MyComponent
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public $components = [
        'MikrotikDhcpTa' , 
        'MikrotikIpfijaTa', 
        'MikrotikPppoeTa', 
    ];

    public $templatesComponents = [];  

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->templatesComponents['MikrotikDhcpTa'] = $this->MikrotikDhcpTa;
        $this->templatesComponents['MikrotikIpfijaTa'] = $this->MikrotikIpfijaTa;
        $this->templatesComponents['MikrotikPppoeTa'] = $this->MikrotikPppoeTa;
    }   

    public function getStatus($controller)
    {
        return $this->templatesComponents[$controller->template]->getStatus($controller);
    }

    public function addController($iController)
    {
       return $this->templatesComponents[$iController->template]->addController($iController);
    }

    public function deleteController($iController)
    {
       return $this->templatesComponents[$iController->template]->deleteController($iController);
    }

    public function cloneController($iController)
    {
       return $this->templatesComponents[$iController->template]->cloneController($iController->id);
    }

    public function getAllControllersComplete()
    {
        $this->_ctrl->loadModel('Controllers');
        $controllers = $this->_ctrl->Controllers->find()->where(['enabled' => true, 'deleted' => false]);

        $this->_ctrl->loadModel('Services');

        foreach ($controllers as $controller) {

            $controller->tcontroller = $this->templatesComponents[$controller->template]->getControllerTemplate($controller);

            $controller->tplans = $this->templatesComponents[$controller->template]->getPlans($controller->id);

            //parse pool_id
            foreach ($controller->tplans as $tplan) {

                switch ($controller->template) {                 

                    case 'MikrotikDhcpTa':
                        $tplan->pool_id = $tplan->network_id;
                        break;                   
                }

                $tplan->service = $this->_ctrl->Services->get($tplan->service_id);
            }

            //parse tpools
            switch ($controller->template) {

                case 'MikrotikIpfijaTa':

                    $controller->tpools = $this->templatesComponents[$controller->template]->getPools($controller->id);
                    foreach ($controller->tpools as $tpool) {
                        $tpool->name = $tpool->name . ' ('. $tpool->addresses . ')';
                    }
                    $controller->tgateways = $this->templatesComponents[$controller->template]->getGateways($controller->id);
                    break;

                case 'MikrotikPppoeTa':
                    $controller->tpools = $this->templatesComponents[$controller->template]->getPools($controller->id);
                    foreach ($controller->tpools as $tpool) {
                        $tpool->name = $tpool->name . ' ('. $tpool->addresses . ')';
                    }
                    break;

                case 'MikrotikDhcpTa':
                    $controller->tpools = $this->templatesComponents[$controller->template]->getNetworks($controller->id);
                    foreach ($controller->tpools as $tpool) {
                        $tpool->name = $tpool->comment . ' ('. $tpool->address . ')';
                    }
                    break;
            }
        }
        return $controllers;
    }

    public function existService($controller, $service_id)
    {        
        return $this->templatesComponents[$controller->template]->existService($service_id);
    }     

    public function getFreeIPByPoolId($controller, $pool_id)
    {        
        return $this->templatesComponents[$controller->template]->getFreeIPByPoolId($controller, $pool_id);
    }    

    public function addConnection($controller, $data)
    {       
        return $this->templatesComponents[$controller->template]->addConnection($controller, $data);
    }

    public function deleteConnection($connection)
    {       
        return $this->templatesComponents[$connection->controller->template]->deleteConnection($connection);
    }

    public function getConnection($connection)
    {       
        return $this->templatesComponents[$connection->controller->template]->getConnection($connection);
    }

    public function rollbackConnection($controller, $connection_id)
    {       
        return $this->templatesComponents[$controller->template]->rollbackConnection($connection_id);
    }

    public function editConnection($connection, $data)
    {       
        return $this->templatesComponents[$connection->controller->template]->editConnection($connection, $data);
    }

    public function syncConnection($connection)
    {       
        return $this->templatesComponents[$connection->controller->template]->syncConnection($connection);
    }

    public function addAvisoConnection($connection)
    {
        return $this->templatesComponents[$connection->controller->template]->addAvisoConnection($connection);
    }

    public function removeAvisoConnection($connection, $sync = false)
    {
        return $this->templatesComponents[$connection->controller->template]->removeAvisoConnection($connection, $sync);
    } 

    public function disableConnection($connection)
    {
        return $this->templatesComponents[$connection->controller->template]->disableConnection($connection);
    }

    public function enableConnection($connection, $sync = false)
    {
         return $this->templatesComponents[$connection->controller->template]->enableConnection($connection, $sync);
    }

    public function removeService($controller, $service_id)
    {        
        return $this->templatesComponents[$controller->template]->removeService($service_id);
    }  

    //scripts

    public function settingsAccountingTraffic($controller, $data)
    {
        return $this->templatesComponents[$controller->template]->settingsAccountingTraffic($controller, $data);
    }

    public function applyConfigAvisos($controller, $ip_server)
    {
        return $this->templatesComponents[$controller->template]->applyConfigAvisosApi($controller, $ip_server);
    }

    public function applyConfigQueuesGraph($controller)
    {
        return $this->templatesComponents[$controller->template]->applyConfigQueuesGraphApi($controller);
    }

    public function generateCertificateAndApply($controller)
    {
        return $this->templatesComponents[$controller->template]->generateCertificateAndApplyApi($controller);
    }

    public function applyConfigAccessNewCustomer($controller, $ip_server)
    {
        return $this->templatesComponents[$controller->template]->applyConfigAccessNewCustomer($controller, $ip_server);
    }  

    public function getRateDataQueue($connection)
    {
        return $this->templatesComponents[$connection->controller->template]->getRateDataQueue($connection);
    }

    public function parseTAtoTC()
    {
        $this->_ctrl->loadModel('Controllers');

        /**
            agregar rate_limit_string  y queue_type en profile temnporalmente
            exportar e importa tota la base de datos template a a la base de template c
            editar el tip¿o de template de los controladore
            ejecutar el scrip de creacion de los queues
            luego syncrnizar
            listo!
        */

        $this->MikrotikPppoeTa->createQueuesFromSecrets(29);
        $this->MikrotikPppoeTa->createQueuesFromSecrets(30);
        $this->MikrotikPppoeTa->createQueuesFromSecrets(31);
        $this->MikrotikPppoeTa->createQueuesFromSecrets(32);
        $this->MikrotikPppoeTa->createQueuesFromSecrets(33);
        $this->MikrotikPppoeTa->createQueuesFromSecrets(34);
        $this->MikrotikPppoeTa->createQueuesFromSecrets(35);
        $this->MikrotikPppoeTa->createQueuesFromSecrets(36);
    } 
}
