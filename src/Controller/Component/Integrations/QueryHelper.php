<?php
namespace App\Controller\Component\Integrations;

// use App\Controller\Component\Common\MyComponent;

// use Cake\Controller\ComponentRegistry;
// use Cake\Filesystem\File;
// use Cake\I18n\Time;
// use Cake\Core\Configure;
// use Cake\Datasource\ConnectionManager;
// use Cake\Event\EventManager;
// use Cake\Event\Event;

// use Cidr;

/*EvilFreelancer/routeros-api-php*/
use \RouterOS\Config;
use \RouterOS\Client;
use \RouterOS\Query;
use RouterOS\Exceptions\ClientException;
use RouterOS\Exceptions\QueryException;



class QueryHelper
{
    public static function parseAttributes(array $data): array
    {
        $attributes = [];

        foreach($data as $key => $value){
            
            array_push($attributes, '='.$key.'='.$value); 
        }     

        return $attributes;
    }
   
}

