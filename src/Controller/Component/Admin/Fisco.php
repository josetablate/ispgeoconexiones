<?php
namespace App\Controller\Component\Admin;

use App\Controller\Component\Common\MyComponent;

/**
 * Documents component
 */
abstract class Fisco extends MyComponent
{

    public function initialize(array $config)
    {
        parent::initialize($config);
    }

    abstract public function informar(&$comp, $type);
}
