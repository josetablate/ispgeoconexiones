<?php
namespace App\Controller\Component\Admin\Pdf;

use FPDF;

class PresupuestoPdf extends FPDF {
    
    public $data;
    public $crtl;
    public $tipo;
    public $font;
    public $fontB;
    
    /****scalar imagen**/
    
    const DPI = 96;
    const MM_IN_INCH = 25.4;
    
    const A4_HEIGHT = 297;
    const A4_WIDTH = 210;

    function pixelsToMM($val)
    {
        return $val * self::MM_IN_INCH / self::DPI;
    }
    
    function resizeToFit($imgFilename, $width_max, $height_max)
    {
        list($width, $height) = getimagesize($imgFilename);
        $widthScale = $width_max / $width;
        $heightScale = $height_max / $height;
        $scale = min($widthScale, $heightScale);
        
        return array(
            round($this->pixelsToMM($scale * $width)),
            round($this->pixelsToMM($scale * $height))
        );
    }

    function customImage($img, $x, $y, $width_max, $height_max)
    {
        list($width, $height) = $this->resizeToFit($img, $width_max, $height_max);
        // you will probably want to swap the width/height
        // around depending on the page's orientation
        
        $this->Image(
            $img, $x, $y, $width, $height
        );
    }
    /******/

	function Header()
	{
		$color = explode(',', $this->data->presupuesto->business->color->rgb);
		
		$this->SetFillColor($color[0], $color[1], $color[2]);
		$this->SetDrawColor($color[0], $color[1], $color[2]);
	
		$number = sprintf("%'.04d", $this->data->presupuesto->pto_vta) . '-' . sprintf("%'.08d", $this->data->presupuesto->num);
		$this->SetTitle($number);
       
        $middle_page = $this->GetPageWidth() / 2;

        $this->Rect(10, 10, $this->GetPageWidth() - 20, 10);
        
        $this->SetFont($this->fontB, '', 20);
        $this->SetTextColor($color[0], $color[1], $color[2]);
        $this->SetXY(0,10);
        $this->Cell(210, 10, utf8_decode($this->tipo), 0, 0,'C', false);

        $this->SetFont($this->fontB, '', 30);
 
        $this->SetXY(100,20);
		$this->SetTextColor(255, 255, 255);
		$this->Cell(13, 15, 'X', 1, 1, 'C', true);

        $this->SetTextColor(0, 0, 0);

        $this->Line(10, 20, 10, 75);
        $this->Line($this->GetPageWidth() - 10, 20, $this->GetPageWidth() - 10, 75);
        $this->Line(10, 75, $this->GetPageWidth() - 10, 75);

       	$this->customImage(WWW_ROOT . 'img/'. $this->data->presupuesto->business->logo, 13, 21, 300, 70);
    
       	$this->SetFont($this->fontB, '', 11);
       	$this->SetTextColor($color[0], $color[1], $color[2]);
       	
       	$x = 13;
       	$y = 50;

        $this->SetXY($x, $y);
        $this->MultiCell(80, 4, utf8_decode($this->data->presupuesto->company_name), 0, 'L');
      
        $this->SetXY($x, $y + 6);
        $this->MultiCell(80, 4, utf8_decode($this->data->presupuesto->company_address . ' - ' . $this->data->presupuesto->company_city), 0, 'L');
      
        $this->SetXY($x, $y + 12);
        $this->MultiCell(80, 4, utf8_decode($this->data->responsibles[$this->data->presupuesto->company_responsible]), 0, 'L');
        
        $this->SetXY($x, $y + 18);
        $this->MultiCell(100, 4, utf8_decode($this->data->presupuesto->company_phone), 0, 'L');

        $x =  $this->GetPageWidth() - 90;

        $this->SetFont($this->fontB, '', 24);
        $this->SetTextColor($color[0], $color[1], $color[2]);
        
        $this->Text($x + 18, 35, utf8_decode('PRESUPUESTO'));
        
        $this->SetTextColor(0,0,0);
     
        $this->SetFont($this->font, '', 11);
        $this->SetTextColor(0, 0, 0);
        
        $this->Text($x, 50, utf8_decode('Comp. Nro:'));
        $this->Text($x + 45, 50, $number);

        $this->Text($x, 55, utf8_decode('Fecha de Emisión:'));
        $this->Text($x + 45, 55, $this->data->presupuesto->date->format('d/m/Y'));
   
        $this->Text($x, 60, utf8_decode('CUIT:'));
        $this->Text($x + 45, 60, $this->data->presupuesto->company_ident);
        
        $this->Text($x, 65, utf8_decode('Ingresos Brutos:'));
        $this->Text($x + 45, 65, $this->data->presupuesto->company_ing_bruto);

        $this->Text($x, 70, utf8_decode('Inicio de Actividades:'));
        $this->Text($x + 45, 70, $this->data->presupuesto->company_init_act);
	}
	
	function Period()
	{
	    $this->SetFont($this->font, '', 11);
	    
	    $color = explode(',', $this->data->presupuesto->business->color->rgb);
		
		$this->SetFillColor($color[0], $color[1], $color[2]);
		$this->SetDrawColor($color[0], $color[1], $color[2]);
	    
       //datos del cliente
        $this->Rect(10, 76, $this->GetPageWidth() - 20, 10);
   
        $this->Text(13, 82, utf8_decode('Período Facturado   Desde: '));
        $this->Text(61, 82, $this->data->presupuesto->date_start->format('d/m/Y'));
      
        $this->Text(87, 82, utf8_decode('Hasta:'));
        $this->Text(99, 82, $this->data->presupuesto->date_end->format('d/m/Y'));

        $this->Text(125, 82, utf8_decode('Fecha de Vto. para el Pago:'));
        $this->Text(175, 82, $this->data->presupuesto->duedate->format('d/m/Y'));
	}

	function Customer()
	{
	    $this->SetFont($this->font, '', 11);
	    
	    $color = explode(',', $this->data->presupuesto->business->color->rgb);
		
		$this->SetFillColor($color[0], $color[1], $color[2]);
		$this->SetDrawColor($color[0], $color[1], $color[2]);
	    
        //datos del cliente
        $this->Rect(10, 87, $this->GetPageWidth() - 20, 23);
     
        
        $this->SetFontSize(10);
        $this->Text(15, 93, utf8_decode('Documento:'));
        
        if ($this->data->presupuesto->customer_doc_type == 99){
            $this->Text(30, 93,' -');
        } else {
            $this->Text(30, 93, $this->data->doc_types[$this->data->presupuesto->customer_doc_type] . ' ' . $this->data->presupuesto->customer_ident);
        }

        $this->Text(55, 93, utf8_decode('Nombre:'));
        $this->Text(67, 93, utf8_decode(strtoupper($this->data->presupuesto->customer_name)));

        $this->Text(15, 100, utf8_decode('Condición frente al IVA:'));
        $this->Text(50, 100, utf8_decode($this->data->responsibles[$this->data->presupuesto->customer_responsible]));
        
        $this->Text(124, 100, utf8_decode('Condición de venta:'));
        $this->Text(150, 100, utf8_decode($this->data->cond_venta[$this->data->presupuesto->cond_vta]));
        
        $this->Text(15, 107, utf8_decode('Domicilio:'));
        $this->Text(30, 107, utf8_decode(strtoupper($this->data->presupuesto->customer_address . ' - ' . $this->data->presupuesto->customer_city)));
	}
	
	function Totales()
	{
	    $this->SetFont($this->font, '', 11);
	    
	    $color = explode(',', $this->data->presupuesto->business->color->rgb);

		$cellW = 42;
        $ypos = 238;
        $cellX = 10;

		$this->SetX($cellX);
		$this->SetY($ypos, false);
		$this->SetFontSize(9);
		$this->SetTextColor(255, 255, 255);
		$this->SetDrawColor($color[0], $color[1], $color[2]);
		$this->Cell($cellW, 6, "SUBTOTAL", 1, 1, 'C', true);
		$this->SetX($cellX);
		$this->SetTextColor(0, 0, 0);

	    $this->Cell($cellW, 8, '$ ' . number_format($this->data->presupuesto->total, 2, ',', '.'), 1, 1, 'C');

		$cellX += $cellW+ 2;

		$cellX += $cellW+ 2;

		$cellX += $cellW+ 2;

		$this->SetX($cellX);
		$this->SetY($ypos, false);
		$this->SetFontSize(9);
		$this->SetTextColor(255, 255, 255);
		$this->SetDrawColor($color[0], $color[1], $color[2]);
		$this->Cell($cellW + 16, 6, "TOTAL", 1, 1, 'C', true);
		$this->SetX($cellX);
		$this->SetTextColor(0, 0, 0);
		$this->Cell($cellW + 16, 8, '$ ' . number_format($this->data->presupuesto->total, 2, ',', '.'), 1, 1, 'C');
	}

	function Footer()
	{
	    $this->SetFont($this->font, '', 11);
	    
		global $AliasNbPages;
		
		$x = 10;
		$y = 297 - 44;
		$w = 210 - 20;
		$h = 10;
		
		$this->SetFontSize(10);
		$this->Rect($x, $y, $w, $h);

        $this->SetXY(0, $y);
	}
	
	function THead($x, $y)
	{
	    $this->SetFont($this->font, '', 9);
	    
	    $color = explode(',', $this->data->presupuesto->business->color->rgb);
	    
		$this->SetFillColor($color[0], $color[1], $color[2]);
		$this->SetTextColor(255);
		$this->SetDrawColor(123, 122, 122);
		
		switch ($this->data->presupuesto->tipo_comp) {
		    case '001':
		        
                $x = [$x, 25, 80, 95, 110, 125, 140, 155, 170, 185 ];
                $w = [15, 55, 15, 15, 15, 15, 15, 15, 15, 15];
                
                $i = 0;
	
                $this->SetXY($x[0], $y);
                $this->Cell($w[0], 6, utf8_decode('Cód.'), 1, 0, 'C', true);
                
                $i++;
          
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6, utf8_decode('Descripción'), 1, 0, 'L', true);
                
                $i++;
             
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6, utf8_decode('Cant.'), 1, 0, 'C', true);
                
                $i++;
                
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6, utf8_decode('Unid.'), 1, 0, 'C', true);
                
                $i++;
                
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6,  utf8_decode('Precio'), 1, 0, 'C', true);
                
                $i++;
                
                $this->SetXY($x[$i],$y);
                $this->Cell($w[$i], 6, utf8_decode('% Bonif.'), 1, 0, 'C', true);
                
                $i++;
                
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6, utf8_decode('Subtotal'), 1, 0, 'C', true);
        
                $i++;
                
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6, utf8_decode('Alícuota IVA.'), 1, 0, 'C', true);
                
                $i++;
                
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6, utf8_decode('Importe IVA.'), 1, 0, 'C', true);
                
                $i++;
                
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6, utf8_decode('Subtotal c/IVA'), 1, 0, 'C', true);
		        break;
		        
		    default:
		        
	            $x = [$x, 25, 125, 140, 155, 170, 185 ];
                $w = [15, 100, 15, 15, 15, 15, 15];
                
                $i = 0;
	
                $this->SetXY($x[0], $y);
                $this->Cell($w[0], 6, utf8_decode('Cód.'), 1, 0, 'C', true);
                
                $i++;
          
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6, utf8_decode('Descripción'), 1, 0, 'L', true);
                
                $i++;
             
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6, utf8_decode('Cant.'), 1, 0, 'C', true);
                
                $i++;
                
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6, utf8_decode('Unid.'), 1, 0, 'C', true);
                
                $i++;
                
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6,  utf8_decode('Precio'), 1, 0, 'C', true);
                
                $i++;
                
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6, utf8_decode('% Bonif.'), 1, 0, 'C', true);

                $i++;
                
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6, utf8_decode('Subtotal'), 1, 0, 'C', true);
		        break;
		        
		}
	}
	
	function AddConceptos($concepts, $x, $y, $e)
	{
	    $this->SetFont($this->font, '', 11);
	    
    	$this->SetFontSize(9);
    	$this->SetTextColor(0);
    	
    	$h = 3;
        
        $x = [$x, 25, 125, 140, 155, 170, 185 ];
        $w = [15, 100, 15, 15, 15, 15, 15];
        
         foreach ($concepts as $concept) {
    
	        $i = 0;
	        
	        $this->SetXY($x[$i], $y);
            $this->MultiCell($w[$i], $h, $concept['code'], 0, 'L');
            
            $i++;
            
            $this->SetXY($x[$i],$y);
            $this->MultiCell($w[$i], $h, utf8_decode($concept['description']), 0, 'L');
            
            $i++;
            
            $this->SetXY($x[$i], $y);
            $this->MultiCell($w[$i], $h, $concept['quantity'], 0, 'C');
            
            $i++;
            
            $this->SetXY($x[$i], $y);
            $this->MultiCell($w[$i], $h, $this->data->units_types[$concept['unit']], 0, 'C');
            
            $i++;
            
            $this->SetXY($x[$i], $y);
            $this->MultiCell($w[$i], $h, number_format($concept['sum_price'] + $concept['sum_tax'], 2, ',', '.') , 0, 'R');
            
            $i++;
            
            $this->SetXY($x[$i], $y);
            $this->MultiCell($w[$i], $h, number_format($concept['discount'], 2, ',', '.'), 0, 'R');
            
            $i++;
            
            $this->SetXY($x[$i], $y);
            $this->MultiCell($w[$i], $h, number_format($concept['total'], 2, ',', '.'), 0, 'R');

            $y += $e;
        }
	}

	function Generate($data, $ctrl, $format = '')
	{
	    $this->data = $data;
	    $this->ctrl = $ctrl;

        $this->SetCompression(false);
        $this->AddFont('OpenSans-CondBold', '', 'OpenSans-CondBold.php');
        $this->AddFont('OpenSans-CondensedLight', '', 'OpenSans-CondLight.php');
        
        $this->font = 'OpenSans-CondensedLight';
	    $this->fontB = 'OpenSans-CondBold';

        $limit_per_page = 10;
        
        $packs = array_chunk($this->data->presupuesto->concepts, $limit_per_page);
        
        if (count($packs) > 10) {
            return false;
        }
        
	    foreach ($packs as $concepts) {
	        
	        $this->AddPage();
            $this->Period();
            $this->Customer();
            $this->THead(10, 111);
	        $this->AddConceptos($concepts, 10, 120, 11);
            $this->Totales();
  
	    }
	}
}
	 