<?php
namespace App\Controller\Component\Admin\Pdf;


use FPDF;

class DebitNotePdf extends FPDF {
    
    public $data;
    public $crtl;
    public $tipo;
    public $font;
    public $fontB;
    
    /****scalar imagen**/
    
    const DPI = 96;
    const MM_IN_INCH = 25.4;
    
    const A4_HEIGHT = 297;
    const A4_WIDTH = 210;

    function pixelsToMM($val)
    {
        return $val * self::MM_IN_INCH / self::DPI;
    }
    
    function resizeToFit($imgFilename, $width_max, $height_max)
    {
        list($width, $height) = getimagesize($imgFilename);
        $widthScale = $width_max / $width;
        $heightScale = $height_max / $height;
        $scale = min($widthScale, $heightScale);
        
        return array(
            round($this->pixelsToMM($scale * $width)),
            round($this->pixelsToMM($scale * $height))
        );
    }
    
    function customImage($img, $x, $y, $width_max, $height_max)
    {
        list($width, $height) = $this->resizeToFit($img, $width_max, $height_max);
        // you will probably want to swap the width/height
        // around depending on the page's orientation
        
        $this->Image(
            $img, $x, $y, $width, $height
        );
    }
    /******/

    function getLetter($tipo_comp)
    {
         switch ($tipo_comp) {
            case 'NDX': 
                return 'X';
                break;
            case '002': 
                return 'A';
                break;
            case '007': 
                return 'B';
                break;
            case '012': 
                return 'C';
                break;
        }
        
        return '';
    }

	function Header()
    {
		$color = explode(',', $this->data->debitNote->business->color->rgb);
		
		$this->SetFillColor($color[0], $color[1], $color[2]);
		$this->SetDrawColor($color[0], $color[1], $color[2]);
	
		$number = sprintf("%'.04d", $this->data->debitNote->pto_vta) . '-' . sprintf("%'.08d", $this->data->debitNote->num);
		$this->SetTitle($number);
       
        $middle_page = $this->GetPageWidth() / 2;

        $this->Rect(10, 10, $this->GetPageWidth() - 20, 10);
        
        $this->SetFont($this->fontB, '', 20);
        $this->SetTextColor($color[0], $color[1], $color[2]);
        $this->SetXY(0,10);
        $this->Cell(210, 10, utf8_decode($this->tipo), 0, 0,'C', false);

        $this->SetFont($this->fontB, '', 30);
 
        $this->SetXY(100,20);
		$this->SetTextColor(255, 255, 255);
		$this->Cell(13, 15, $this->getLetter($this->data->debitNote->tipo_comp), 1, 1, 'C', true);

	    if ($this->data->debitNote->tipo_comp != 'NDX') {
	        $this->SetFont($this->fontB, '', 8.5);
	        $this->Text(101.5, 34.5, utf8_decode('CÓD. ') . sprintf("%'.02d", $this->data->debitNote->tipo_comp)); 
	    }

        $this->SetTextColor(0, 0, 0);

        $this->Line(10, 20, 10, 75);
        $this->Line($this->GetPageWidth() - 10, 20, $this->GetPageWidth() - 10, 75);
        $this->Line(10, 75, $this->GetPageWidth() - 10, 75);

       	$this->customImage(WWW_ROOT . 'img/'. $this->data->debitNote->business->logo, 12, 21, 300, 70);
       	
    
       	$this->SetFont($this->fontB, '', 11);
       	$this->SetTextColor($color[0], $color[1], $color[2]);
       	
       	$x = 13;
       	$y = 50;

        $this->SetXY($x, $y);
        $this->MultiCell(80, 4, utf8_decode($this->data->debitNote->company_name), 0, 'L');
      
        $this->SetXY($x, $y + 6);
        $this->MultiCell(80, 4, utf8_decode($this->data->debitNote->company_address . ' - ' . $this->data->debitNote->company_city), 0, 'L');
      
        $this->SetXY($x, $y + 12);
        $this->MultiCell(80, 4, utf8_decode($this->data->responsibles[$this->data->debitNote->company_responsible]), 0, 'L');
        
        $this->SetXY($x, $y + 18);
        $this->MultiCell(80, 4, utf8_decode($this->data->debitNote->company_phone), 0, 'L');

        $x =  $this->GetPageWidth() - 90;

        $this->SetFont($this->fontB, '', 24);
        $this->SetTextColor($color[0], $color[1], $color[2]);

        // if($this->data->debitNote->tipo_comp != 'NDX'){
            $this->Text($x + 18, 35, utf8_decode('NOTA DE DÉBITO'));
        // }else{
        //     $this->Text($x + 18, 35, utf8_decode(''));
        // }
        
        $this->SetTextColor(0, 0, 0);
     
        $this->SetFont($this->font, '', 11);
        $this->SetTextColor(0,0,0);
        
        $this->Text($x, 50, utf8_decode('Comp. Nro:'));
        $this->Text($x + 45, 50, $number);

        $this->Text($x, 55, utf8_decode('Fecha de Emisión:'));
        $this->Text($x + 45, 55, $this->data->debitNote->date->format('d/m/Y'));
   
        $this->Text($x, 60, utf8_decode('CUIT:'));
        $this->Text($x + 45, 60, $this->data->debitNote->company_ident);
        
        $this->Text($x, 65, utf8_decode('Ingresos Brutos:'));
        $this->Text($x + 45, 65, $this->data->debitNote->company_ing_brutot);

        $this->Text($x, 70, utf8_decode('Inicio de Actividades:'));
        $this->Text($x + 45, 70, $this->data->debitNote->company_init_act);
	}

	function Period()
	{
	    $this->SetFont($this->font, '', 11);
	    
	    $color = explode(',', $this->data->debitNote->business->color->rgb);
		
		$this->SetFillColor($color[0], $color[1], $color[2]);
		$this->SetDrawColor($color[0], $color[1], $color[2]);
	    
        //datos del cliente
        $this->Rect(10, 76, $this->GetPageWidth() - 20, 10);
   
        $this->Text(13, 82, utf8_decode('Período Facturado   Desde: '));
        $this->Text(61, 82, $this->data->debitNote->date_start->format('d/m/Y'));
      
        $this->Text(87, 82, utf8_decode('Hasta:'));
        $this->Text(99, 82, $this->data->debitNote->date_end->format('d/m/Y'));

        $this->Text(125, 82, utf8_decode('Fecha de Vto. para el Pago:'));
        $this->Text(175, 82, $this->data->debitNote->duedate->format('d/m/Y'));
	}

	function Customer()
	{
	    $this->SetFont($this->font, '', 11);
	    
	    $color = explode(',', $this->data->debitNote->business->color->rgb);
		
		$this->SetFillColor($color[0], $color[1], $color[2]);
		$this->SetDrawColor($color[0], $color[1], $color[2]);
	    
        //datos del cliente
        $this->Rect(10, 87, $this->GetPageWidth() - 20, 23);
        
        $this->SetFontSize(10);
        $this->Text(15, 93, utf8_decode('Documento:'));
        
        if ($this->data->debitNote->customer_doc_type == 99) {
            $this->Text(30, 93, '-');
        } else {
            $this->Text(30, 93, $this->data->doc_types[$this->data->debitNote->customer_doc_type] . ' ' . $this->data->debitNote->customer_ident);
        }
      
        $this->Text(55, 93, utf8_decode('Nombre:'));
        $this->Text(67, 93, utf8_decode(strtoupper($this->data->debitNote->customer_name)));

        $this->Text(15, 100, utf8_decode('Condición frente al IVA:'));
        $this->Text(50, 100, utf8_decode($this->data->responsibles[$this->data->debitNote->customer_responsible]));
        
        $this->Text(124, 100, utf8_decode('Condición de venta:'));
        $this->Text(150, 100, utf8_decode($this->data->cond_venta[$this->data->debitNote->cond_vta]));
        
        $this->Text(15, 107, utf8_decode('Domicilio:'));
        $this->Text(30, 107, utf8_decode(strtoupper($this->data->debitNote->customer_address . ' - ' . $this->data->debitNote->customer_city)));

	}

	function Totales()
	{
	    $this->SetFont($this->font, '', 11);
	    
	    $color = explode(',', $this->data->debitNote->business->color->rgb);

		$cellW = 42;
        $ypos = 238;
        $cellX = 10;

		$this->SetX($cellX);
		$this->SetY($ypos, false);
		$this->SetFontSize(9);
		$this->SetTextColor(255, 255, 255);
		$this->SetDrawColor($color[0], $color[1], $color[2]);
		$this->Cell($cellW, 6, "SUBTOTAL", 1, 1, 'C', true);
		$this->SetX($cellX);
		$this->SetTextColor(0, 0, 0);
		
		switch ($this->data->debitNote->tipo_comp) {
		    case '002':
        		$this->Cell($cellW, 8, '$ ' . number_format($this->data->debitNote->subtotal, 2, ',', '.'), 1, 1, 'C');
		        break;
		        
		    default:
		        
		        $this->Cell($cellW, 8, '$ ' . number_format($this->data->debitNote->total, 2, ',', '.'), 1, 1, 'C');
		        break;
		}

		$cellX += $cellW + 2;

		switch ($this->data->debitNote->tipo_comp) {
		    case '002':
		        
	        	$this->SetX($cellX);
        		$this->SetY($ypos, false);
        		$this->SetFontSize(9);
        		$this->SetTextColor(255, 255, 255);
        		$this->SetDrawColor($color[0], $color[1], $color[2]);
        		$this->Cell($cellW, 6, "IMPUESTO", 1, 1, 'C', true);
        		$this->SetX($cellX);
        		$this->SetTextColor(0, 0, 0);
        		$this->Cell($cellW, 8, '$ ' . number_format($this->data->debitNote->sum_tax, 2, ',', '.'), 1, 1, 'C');
		        break;
		        
		    default:
		        break;
		}

		$cellX += $cellW + 2;

		$cellX += $cellW + 2;

		$this->SetX($cellX);
		$this->SetY($ypos, false);
		$this->SetFontSize(9);
		$this->SetTextColor(255, 255, 255);
		$this->SetDrawColor($color[0], $color[1], $color[2]);
		$this->Cell($cellW + 16, 6, "TOTAL", 1, 1, 'C', true);
		$this->SetX($cellX);
		$this->SetTextColor(0, 0, 0);
		$this->Cell($cellW + 16, 8, '$ ' . number_format($this->data->debitNote->total, 2, ',', '.'), 1, 1, 'C');
	}

	function BarCode()
    {
	    if ($this->data->debitNote->barcode) {
	        
	        $this->SetTextColor(0, 0, 0);
    		$barcode = $this->data->debitNote->barcode.$this->digitoVerificador($this->data->debitNote->barcode);
    		$this->i25(10, 265, $barcode);
    		
    	    $this->SetFont($this->font, '', 11);
    		$this->Text(164, 270, 'CAE: ' . $this->data->debitNote->cae);
			$this->Text(164, 274, 'VTO: ' . $this->data->debitNote->vto->format('d/m/Y'));
	    }
	}
	
	function i25($xpos, $ypos, $code, $basewidth = 1, $height = 10)
	{
        $wide = $basewidth;
        $narrow = $basewidth / 3;

        // wide/narrow codes for the digits
        $barChar['0'] = 'nnwwn';
        $barChar['1'] = 'wnnnw';
        $barChar['2'] = 'nwnnw';
        $barChar['3'] = 'wwnnn';
        $barChar['4'] = 'nnwnw';
        $barChar['5'] = 'wnwnn';
        $barChar['6'] = 'nwwnn';
        $barChar['7'] = 'nnnww';
        $barChar['8'] = 'wnnwn';
        $barChar['9'] = 'nwnwn';
        $barChar['A'] = 'nn';
        $barChar['Z'] = 'wn';

        // add leading zero if code-length is odd
        if (strlen($code) % 2 != 0) {
            $code = '0' . $code;
        }

        // $this->SetFont('Arial','',10);
        $this->SetFont($this->font, '', 11);
        $this->Text($xpos, $ypos + $height + 4, $code);
        $this->SetFont('Arial', '', 10);
        $this->SetFillColor(0);

        // add start and stop codes
        $code = 'AA'.strtolower($code).'ZA';

        for ($i = 0; $i < strlen($code); $i= $i + 2) {
            // choose next pair of digits
            $charBar = $code[$i];
            $charSpace = $code[$i+1];
            // check whether it is a valid digit
            if (!isset($barChar[$charBar])) {
                $this->Error('Invalid character in barcode: ' . $charBar);
            }
            if (!isset($barChar[$charSpace])) {
                $this->Error('Invalid character in barcode: ' . $charSpace);
            }
            // create a wide/narrow-sequence (first digit=bars, second digit=spaces)
            $seq = '';
            for ($s = 0; $s<strlen($barChar[$charBar]); $s++) {
                $seq .= $barChar[$charBar][$s] . $barChar[$charSpace][$s];
            }
            for ($bar = 0; $bar<strlen($seq); $bar++) {
                // set lineWidth depending on value
                if ($seq[$bar] == 'n') {
                    $lineWidth = $narrow;
                } else {
                    $lineWidth = $wide;
                }
                // draw every second value, because the second digit of the pair is represented by the spaces
                if ($bar % 2 == 0) {
                    $this->Rect($xpos, $ypos, $lineWidth, $height, 'F');
                }
                $xpos += $lineWidth;
            }
        }
    }
	
	private function digitoVerificador( $codigo )
	{
		$digitos = str_split( $codigo );

		$impares = 0;
		for ( $i = 0; $i < count( $digitos ); $i +=2 ) {
			$impares += $digitos[$i];
		}

		$impares = $impares * 3;

		$pares = 0;
		for ( $i = 1; $i < count( $digitos ); $i+=2 ) {
			$pares += $digitos[$i];
		}
		$acum = $impares + $pares;

		$digito = ceil($acum / 10.0) * 10 - $acum;
		return $digito;
	}

	function Footer()
	{
	    $this->SetFont($this->font, '', 11);
	    
		global $AliasNbPages;
		
		$x = 10;
		$y = 297 - 44;
		$w = 210 - 20;
		$h = 10;
		
		$this->SetFontSize(10);
		$this->Rect($x, $y, $w, $h);

        $this->SetXY(0, $y);
        $direccion_comercio_interior = '"DIRECCIÓN DE COMERCIO INTERIOR 0800-444-3346"';
        if (isset($this->data->debitNote->business->direccion_comercio_interior)
            && $this->data->debitNote->business->direccion_comercio_interior != "") {
            $direccion_comercio_interior = $this->data->debitNote->business->direccion_comercio_interior;
        }
        $this->Cell(210, 10, utf8_decode($direccion_comercio_interior), 0, 0, 'C', false);
	}
	
	function THead($x, $y)
	{
	    $this->SetFont($this->font, '', 9);
	    
	    $color = explode(',', $this->data->debitNote->business->color->rgb);
	    
		$this->SetFillColor($color[0], $color[1], $color[2]);
		$this->SetTextColor(255);
		$this->SetDrawColor(123, 122, 122);
		
		switch ($this->data->debitNote->tipo_comp) {
		    case '002':
		        
                $x = [$x, 25, 80, 95, 110, 125, 140, 155, 170, 185 ];
                $w = [15, 55, 15, 15, 15, 15, 15, 15, 15, 15];
                
                $i = 0;
	
                $this->SetXY($x[0], $y);
                $this->Cell($w[0], 6, utf8_decode('Cód.'), 1, 0, 'C', true);
                
                $i++;
          
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6, utf8_decode('Descripción'), 1, 0, 'L', true);
                
                $i++;
             
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6, utf8_decode('Cant.'), 1, 0, 'C', true);
                
                $i++;
                
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6, utf8_decode('Unid.'), 1, 0, 'C', true);
                
                $i++;
                
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6,  utf8_decode('Precio'), 1, 0, 'C', true);
                
                $i++;
                
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6, utf8_decode('% Bonif.'), 1, 0, 'C', true);
                
                $i++;
                
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6, utf8_decode('Subtotal'), 1, 0, 'C', true);
        
                $i++;
                
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6, utf8_decode('Alícuota IVA.'), 1, 0, 'C', true);
                
                $i++;
                
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6, utf8_decode('Importe IVA.'), 1, 0, 'C', true);
                
                $i++;
                
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6, utf8_decode('Subtotal c/IVA'), 1, 0, 'C', true);
		        break;
		        
		    default:
		        
	            $x = [$x, 25, 125, 140, 155, 170, 185 ];
                $w = [15, 100, 15, 15, 15, 15, 15];
                
                $i = 0;
	
                $this->SetXY($x[0], $y);
                $this->Cell($w[0], 6, utf8_decode('Cód.'), 1, 0, 'C', true);
                
                $i++;
          
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6, utf8_decode('Descripción'), 1, 0, 'L', true);
                
                $i++;
             
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6, utf8_decode('Cant.'), 1, 0, 'C', true);
                
                $i++;
                
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6, utf8_decode('Unid.'), 1, 0, 'C', true);
                
                $i++;
                
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6,  utf8_decode('Precio'), 1, 0, 'C', true);
                
                $i++;
                
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6, utf8_decode('% Bonif.'), 1, 0, 'C', true);

                $i++;
                
                $this->SetXY($x[$i], $y);
                $this->Cell($w[$i], 6, utf8_decode('Subtotal'), 1, 0, 'C', true);
		        break;
		}
	}
	
	function AddConceptos($concepts, $x, $y, $e)
	{
	    $this->SetFont($this->font, '', 11);
	    
    	$this->SetFontSize(9);
    	$this->SetTextColor(0);
    	
    	$h = 3;
    	
    	switch ($this->data->debitNote->tipo_comp) {
		    case '002':
		        
                $x = [$x, 25, 80, 95, 110, 125, 140, 155, 170, 185 ];
		        $w = [15, 55, 15, 15, 15, 15, 15, 15, 15, 15];
		        
		        foreach ($concepts as $concept) {
	        
        	        $i = 0;
        	        
        	        $this->SetXY($x[$i], $y);
                    $this->MultiCell($w[$i], $h, $concept['code'], 0, 'L');
                    
                    $i++;
                    
                    $this->SetXY($x[$i], $y);
                    $this->MultiCell($w[$i], $h, utf8_decode($concept['description']), 0, 'L');
                    
                    $i++;
                    
                    $this->SetXY($x[$i], $y);
                    $this->MultiCell($w[$i], $h, $concept['quantity'], 0, 'C');
                    
                    $i++;
                    
                    $this->SetXY($x[$i], $y);
                    $this->MultiCell($w[$i], $h, $this->data->units_types[$concept['unit']], 0, 'C');
                    
                    $i++;
                    
                    $this->SetXY($x[$i], $y);
                    $this->MultiCell($w[$i], $h, number_format($concept['price'], 2, ',', '.') , 0, 'R');
                    
                    $i++;
                    
                    $this->SetXY($x[$i], $y);
                    $this->MultiCell($w[$i], $h, number_format($concept['discount'], 2, ',', '.'), 0, 'R');
                    
                    $i++;
                    
                    $this->SetXY($x[$i], $y);
                    $this->MultiCell($w[$i], $h, number_format($concept['sum_price'], 2, ',', '.'), 0, 'R');
                    
                    $i++;
                    
                    $this->SetXY($x[$i], $y);
                    $this->MultiCell($w[$i], $h, $this->data->alicuotas_types[$concept['tax']] , 0, 'C');
                    
                    $i++;
                    
                    $this->SetXY($x[$i], $y);
                    $this->MultiCell($w[$i], $h, number_format($concept['sum_tax'], 2, ',', '.'), 0, 'R');
                    
                    $i++;
                    
                    $this->SetXY($x[$i], $y);
                    $this->MultiCell($w[$i], $h, number_format($concept['total'], 2, ',', '.'), 0, 'R');

                    $y += $e;
              
                }
                break;
                
            case '007': //NOTA DE DEBITO B
            case '012': //NOTA DE DEBITO C
            case 'NDX': 
                
                $x = [$x, 25, 125, 140, 155, 170, 185 ];
                $w = [15, 100, 15, 15, 15, 15, 15];
		        
		        foreach ($concepts as $concept) {
	        
        	        $i = 0;
        	        
        	        $this->SetXY($x[$i], $y);
                    $this->MultiCell($w[$i], $h, $concept['code'], 0, 'L');
                    
                    $i++;
                    
                    $this->SetXY($x[$i], $y);
                    $this->MultiCell($w[$i], $h, utf8_decode($concept['description']), 0, 'L');
                    
                    $i++;
                    
                    $this->SetXY($x[$i], $y);
                    $this->MultiCell($w[$i], $h, $concept['quantity'], 0, 'C');
                    
                    $i++;
                    
                    $this->SetXY($x[$i], $y);
                    $this->MultiCell($w[$i], $h, $this->data->units_types[$concept['unit']], 0, 'C');
                    
                    $i++;
                    
                    $this->SetXY($x[$i], $y);
                    $this->MultiCell($w[$i], $h, number_format($concept['sum_price'] + $concept['sum_tax'], 2, ',', '.') , 0, 'R');
                    
                    $i++;
                    
                    $this->SetXY($x[$i], $y);
                    $this->MultiCell($w[$i], $h, number_format($concept['discount'], 2, ',', '.'), 0, 'R');
                    
                    $i++;
                    
                    $this->SetXY($x[$i], $y);
                    $this->MultiCell($w[$i], $h, number_format($concept['total'], 2, ',', '.'), 0, 'R');

                    $y += $e;
              
                }
                break;
    	}
	}

	function Generate($data, $ctrl, $format = '')
	{
	    $this->data = $data;
	    $this->ctrl = $ctrl;

        $this->SetCompression(false);
        $this->AddFont('OpenSans-CondBold', '', 'OpenSans-CondBold.php');
        $this->AddFont('OpenSans-CondensedLight', '', 'OpenSans-CondLight.php');
        
        $this->font = 'OpenSans-CondensedLight';
	    $this->fontB = 'OpenSans-CondBold';

        $limit_per_page = 10;
        
        $packs = array_chunk($this->data->debitNote->concepts_debit, $limit_per_page);
        
        if (count($packs) > 10) {
            return false;
        }
        
	    foreach ($packs as $concepts) {
	        
	        $this->AddPage();
            $this->Period();
            $this->Customer();
            $this->THead(10, 111);
	        $this->AddConceptos($concepts, 10, 120, 11);
            $this->Totales();
            $this->BarCode();
	    }
	}
}
