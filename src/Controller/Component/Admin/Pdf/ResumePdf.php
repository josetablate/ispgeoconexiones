<?php
namespace App\Controller\Component\Admin\Pdf;

use FPDF;
use Cake\I18n\Time;

class ResumePDF extends FPDF {

    public $data;
    public $crtl;
    public $pack_concepts;
    public $saldo;

	function Header()
	{
		$this->Image(WWW_ROOT . 'img/' . $this->data->paraments->invoicing->logo, 10, 10, 45);

		$color = explode(',', $this->data->paraments->invoicing->color->rgb);

		$this->SetFillColor($color[0], $color[1], $color[2]);
		$this->SetDrawColor($color[0], $color[1], $color[2]);

		$this->SetFont('UniFont', '', 9);

		$this->SetFontSize(14);
        $this->SetTitle('RESUMEN DE CUENTA');

        $middle_page = $this->GetPageWidth() / 2;

        $this->SetFontSize(8);
        $this->SetXY(60, 12);
        $this->MultiCell(70, 4, utf8_decode($this->data->paraments->invoicing->business[0]->name), null, 'L');
 
        $this->SetXY(60, 17);
        $this->MultiCell(58, 4, utf8_decode($this->data->paraments->invoicing->business[0]->address . ' - ' . $this->data->paraments->invoicing->business[0]->city), null, 'L');
        $this->Text(61, 28, utf8_decode($this->data->responsibles[$this->data->paraments->invoicing->business[0]->responsible]));
        $this->Text(61, 33, 'CUIT: ' . $this->data->paraments->invoicing->business[0]->cuit);
        $this->Text(61, 38, 'TEL.: '.  $this->data->paraments->invoicing->phone);

        $this->SetFontSize(10);
        $this->Text(12, 45, 'RESUMEN DE CUENTA ');

        $this->SetFillColor($color[0], $color[1], $color[2]);

        $this->Text($middle_page + 10, 15, 'FECHA:');

        $this->SetXY($this->GetPageWidth() - 35, 10);

        $this->SetTextColor(255, 255, 255);

        $this->MultiCell(25,5, Time::now()->format('d/m/Y'), null, 'R', true);

        $this->SetTextColor(0, 0, 0);

        $this->Line($middle_page + 10, 16, $this->GetPageWidth() - 10, 16);

        $this->Text($middle_page + 10, 23, 'TOTAL A PAGAR: $');

        $this->SetXY($this->GetPageWidth() - 35, 18);

        $this->SetTextColor(255, 255, 255);

        if ($this->data->saldo > 0) {
             $this->MultiCell(25, 5, number_format($this->data->saldo, 2, ',', '.'), null, 'R', true);
        } else {
             $this->MultiCell(25, 5, number_format(0, 2, ',', '.'), null, 'R',true);
        }

        $this->SetTextColor(0, 0, 0);

        $this->Line($middle_page + 10, 24, $this->GetPageWidth() - 10, 24);

        $this->Text($middle_page + 10, 31, 'VENCIMIENTO: ');

        $this->SetXY($this->GetPageWidth() - 35, 26);

        $duedate = new Time();
        $duedate->year($this->data->to->year);
        $duedate->month($this->data->to->month);
        $duedate->day($this->data->customer->daydue);

        $this->SetTextColor(255, 255, 255);

        $this->MultiCell(25, 5, $duedate->format('d/m/Y'), null, 'R', true);

        $this->SetTextColor(0, 0, 0);

        $this->Line($middle_page + 10, 32, $this->GetPageWidth() - 10, 32);

        //datos del cliente

        $this->SetFontSize(10);
        $this->Text($middle_page + 10, 40, 'CLIENTE:');

        $this->SetXY($this->GetPageWidth() - 79, 37);
        $this->SetFontSize(9);
        $this->MultiCell(70, 4, utf8_decode(strtoupper($this->data->customer->name)), 0, 'R');

        $this->SetFontSize(10);
        $this->Text($middle_page + 10, 45, 'DOC:');

        $this->SetXY($this->GetPageWidth() - 49, 42);
        $this->SetFontSize(9);
        $this->MultiCell(40, 4, $this->data->doc_types[$this->data->customer->doc_type] . ' ' . $this->data->customer->ident, 0, 'R');

        $this->SetFontSize(10);
        $this->Text($middle_page + 10, 50, utf8_decode('CÓDIGO DE CLIENTE:'));

        $this->SetXY($this->GetPageWidth() - 29, 47);
        $this->SetFontSize(9);
        $this->MultiCell(20, 4, sprintf("%'.05d", $this->data->customer->code) , 0, 'R');

        $this->SetFontSize(10);
        $this->Text($middle_page + 10, 55, utf8_decode('PERÍODO:'));

        $this->SetXY($this->GetPageWidth() - 59, 52);
        $this->SetFontSize(9);

        $this->MultiCell(50, 4, $this->data->from->format('d/m/Y') . ' - ' . $this->data->to->format('d/m/Y') , 0, 'R');

        $this->SetFontSize(12);
        $this->Text(10, 58, 'Detalle:');
        $this->Line(10, 60, $this->GetPageWidth() - 10, 60);
	}

	function THead($x, $y)
	{
	    $color = explode(',', $this->data->paraments->invoicing->color->rgb);

		$this->SetFillColor($color[0], $color[1], $color[2]);
		$this->SetTextColor(255);
		$this->SetDrawColor(123, 122, 122);

		$this->SetFontSize(8);

        $x = array($x, 30, 115, 130, 150, 175);
		$w = array(20, 120, 15, 20, 25, 25);

        $this->SetXY($x[0], $y);
        $this->Cell($w[0], 6, utf8_decode('Fecha'), 1, 0, 'C', true);

        $this->SetXY($x[1], $y);
        $this->Cell($w[1], 6, utf8_decode('Descripción'), 1, 0, 'L', true);

        $this->SetXY($x[4], $y);
        $this->Cell($w[4], 6, utf8_decode('Subtotal'), 1, 0, 'C', true);

        $this->SetXY($x[5], $y);
        $this->Cell($w[5], 6, utf8_decode('Saldo'), 1, 0, 'C', true);
	}

	function Footer()
	{
		global $AliasNbPages;
		$this->SetY(-20);
		$this->SetFont('UniFont', '', 9);
		$this->Cell(0, 10, utf8_decode('Página ') . $this->PageNo() . ' de {nb}', 0, 0, 'C');
	}

	function AddConceptos($concepts, $x, $y, $e)
	{
    	$this->SetFontSize(8);
    	$this->SetTextColor(0);

        $x = array($x, 30, 115, 130, 150, 175);
		$w = array(20, 120, 15, 20, 25, 25);

	    foreach ($concepts as $concept) {

	        $this->saldo += $concept['subtotal'];

	        $this->SetXY($x[0], $y);
            $this->MultiCell($w[0], 6, $concept['fecha']->format('d/m/Y'), null, 'L');

            $this->SetXY($x[1], $y);
            $this->MultiCell($w[1], 6, utf8_decode($concept['description']), null, 'L');

            $this->SetXY($x[4], $y);
            $this->MultiCell($w[4], 6, $concept['subtotal'], null, 'R');

            $this->SetXY($x[5], $y);
            $this->MultiCell($w[5], 6, round($this->saldo,2), null, 'R');

            $y += $e;
        }
	}

	function Generate($data, $ctrl)
	{

	    $limit_per_page = 30;

	    $this->data = $data;
	    $this->ctrl = $ctrl;

        $this->SetCompression(false);
		$this->AddFont('UniFont', '', 'DejaVuSans.php');
		$this->AliasNbPages();

		$this->saldo = 0;

	    $packs = array_chunk($this->data->list, $limit_per_page);

	    foreach ($packs as $concepts) {

	        $this->AddPage();
	        $this->THead(10, 61);
	        $this->AddConceptos($concepts, 10, 68, 6);
	    }

		return $this->Output();
	}
}
