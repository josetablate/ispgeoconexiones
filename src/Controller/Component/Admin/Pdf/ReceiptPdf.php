<?php
namespace App\Controller\Component\Admin\Pdf;

use FPDF;
use App\Controller\Component\Admin\Pdf\NumberToLetterConverter;
use Cake\I18n\Time;

class ReceiptPdf extends FPDF {

    /*para auto imprimir*/
    protected $javascript;
    protected $n_js;

    function IncludeJS($script, $isUTF8=false) {
        if (!$isUTF8)
            $script=utf8_encode($script);
        $this->javascript=$script;
    }

    function _putjavascript() {
        $this->_newobj();
        $this->n_js=$this->n;
        $this->_put('<<');
        $this->_put('/Names [(EmbeddedJS) '.($this->n+1).' 0 R]');
        $this->_put('>>');
        $this->_put('endobj');
        $this->_newobj();
        $this->_put('<<');
        $this->_put('/S /JavaScript');
        $this->_put('/JS '.$this->_textstring($this->javascript));
        $this->_put('>>');
        $this->_put('endobj');
    }

    function _putresources() {
        parent::_putresources();
        if (!empty($this->javascript)) {
            $this->_putjavascript();
        }
    }

    function _putcatalog() {
        parent::_putcatalog();
        if (!empty($this->javascript)) {
            $this->_put('/Names <</JavaScript '.($this->n_js).' 0 R>>');
        }
    }

    function AutoPrint($printer='')
    {
        // Open the print dialog
        if ($printer)
        {
            $printer = str_replace('\\', '\\\\', $printer);
            $script = "var pp = getPrintParams();";
            $script .= "pp.interactive = pp.constants.interactionLevel.full;";
            $script .= "pp.printerName = '$printer'";
            $script .= "print(pp);";
        }
        else
            $script = 'print(true);';
        $this->IncludeJS($script);
    }
    /*para auto imprimir*/

    public $data;
    public $crtl;
    public $font;
    public $fontB;
    public $BORDER = 0;

    public $X = 10;
    public $Y = 10;

    /****scalar imagen**/

    const DPI = 96;
    const MM_IN_INCH = 25.4;

    const A4_HEIGHT = 297;
    const A4_WIDTH = 210;

    function pixelsToMM($val)
    {
        return $val * self::MM_IN_INCH / self::DPI;
    }

    function resizeToFit($imgFilename, $width_max, $height_max)
    {
        list($width, $height) = getimagesize($imgFilename);
        $widthScale = $width_max / $width;
        $heightScale = $height_max / $height;
        $scale = min($widthScale, $heightScale);

        return array(
            round($this->pixelsToMM($scale * $width)),
            round($this->pixelsToMM($scale * $height))
        );
    }

    function customImage($img, $x, $y, $width_max, $height_max)
    {
        list($width, $height) = $this->resizeToFit($img, $width_max, $height_max);
        // you will probably want to swap the width/height
        // around depending on the page's orientation

        $this->Image(
            $img, $x, $y, $width, $height
        );
    }

    /******/

    function getLetter($tipo_comp)
    {
         switch ($tipo_comp) {
            case 'XRX': 
                return 'x';
            case 'FRX': 
                return 'X';
                break;
            case '004': 
                return 'A';
                break;
            case '009': 
                return 'B';
                break;
            case '015': 
                return 'C';
                break;
        }

        return '';
    }

	function HeaderCustom()
	{
	    $this->resetPos();

		$color = explode(',', $this->data->receipt->business->color->rgb);

	    $this->SetFillColor($color[0], $color[1], $color[2]);
		$this->SetDrawColor($color[0], $color[1], $color[2]);

		$number = sprintf("%'.04d", $this->data->receipt->pto_vta) . '-' . sprintf("%'.08d", $this->data->receipt->num);
		$this->SetTitle($number);

        $middle_page = $this->GetPageWidth() / 2;

        $this->SetXY($this->X, $this->Y);

        // recuadro encabezado    
        $this->Rect($this->GetX(), $this->GetY(), $this->GetPageWidth() - 20, 45);

        $this->resetPos();

        //tipo de comprobante
        $this->SetFont($this->fontB, '', 40);
        $this->SetXY($this->GetX() + 90, $this->GetY());
		$this->SetTextColor(255, 255, 255);
		$this->Cell(13, 15, $this->getLetter($this->data->receipt->tipo_comp), 1, 1, 'C', true);

        $this->resetPos();

        //logo + infoamcion de empresa

       	$this->resetPos();

       	$this->SetFont($this->font, '', 11);
       	$this->SetTextColor($color[0], $color[1], $color[2]);

        if(trim($this->data->receipt->company_name) != ''){
            $this->SetXY($this->GetX() + 5, $this->GetY() + 22);
            $this->MultiCell(85, 5, utf8_decode($this->data->receipt->company_name), $this->BORDER, 'L');
        }       

        if(trim($this->data->receipt->company_responsible) != ''){
            $this->SetX($this->GetX() + 5);
            $this->MultiCell(85, 5, utf8_decode($this->data->responsibles[$this->data->receipt->company_responsible]), $this->BORDER, 'L');    
        }

        if(trim($this->data->receipt->company_address) != ''){
            $this->SetX($this->GetX() + 5);
            $this->MultiCell(85, 5, utf8_decode($this->data->receipt->company_address . ' - ' . $this->data->receipt->company_city), $this->BORDER, 'L');    
        }

        if (trim($this->data->receipt->company_phone) != ''){
            $this->SetX($this->GetX() + 5);
            $this->MultiCell(85, 5, utf8_decode($this->data->receipt->company_phone), $this->BORDER, 'L'); 
        }        

        $this->resetPos();

        //RECIBO
        $this->SetFont($this->fontB, '', 24);
        $this->SetTextColor($color[0], $color[1], $color[2]);
        $this->Text($this->GetX() + 130, $this->GetY() + 10, utf8_decode('RECIBO'));
        $this->SetTextColor(0, 0, 0);

        $this->resetPos();

        //datos de la factura
        $this->SetFont($this->font, '', 11);
        $this->SetTextColor(0, 0, 0);

        $this->SetXY($this->GetX() + 105 , $this->GetY() + 14);
        
        $this->Cell(40, 5, utf8_decode('Comp. Nro:'), $this->BORDER, 2,'L');
        $this->Cell(40, 5, utf8_decode('Fecha de Emisión:'), $this->BORDER, 2, 'L');

        if (trim($this->data->receipt->company_ident) != ''){
            $this->Cell(40, 5, utf8_decode('CUIT:'), $this->BORDER, 2, 'L');
        }

        if (trim($this->data->receipt->company_ing_bruto) != '') {
            $this->Cell(40, 5, utf8_decode('Ingresos Brutos:'), $this->BORDER, 2, 'L');
        }

        if (trim($this->data->receipt->company_init_act) != '') {
            $this->Cell(40, 5, utf8_decode('Inicio de Actividades:'), $this->BORDER, 2, 'L');
        } 
        
        $this->Cell(40, 5, utf8_decode('Cobrador:'), $this->BORDER, 0, 'L');

        $this->resetPos();

        $this->SetXY($this->GetX() + 145 , $this->GetY() + 14);

        $this->Cell(40, 5, $number, $this->BORDER, 2,'R');
        $this->Cell(40, 5, $this->data->receipt->date->format('d/m/Y'), $this->BORDER, 2, 'R');

        if (trim($this->data->receipt->company_ident) != '') {
            $this->Cell(40, 5, $this->data->receipt->company_ident, $this->BORDER, 2, 'R');
        }
        
        if (trim($this->data->receipt->company_ing_bruto) != '') {
            $this->Cell(40, 5, $this->data->receipt->company_ing_bruto, $this->BORDER, 2, 'R');
        }

        if (trim($this->data->receipt->company_init_act) != '') {
            $this->Cell(40, 5, $this->data->receipt->company_init_act, $this->BORDER, 2, 'R');
        }       
        
        $this->Cell(40, 5, $this->data->receipt->user->name, $this->BORDER, 2, 'R');
	}

	function resetPos()
	{
	     $this->SetXY($this->X, $this->Y);
	}

	function Customer()
	{
	    $this->resetPos();
	    $this->SetXY($this->GetX() , $this->GetY() + 45);

	    $color = explode(',', $this->data->receipt->business->color->rgb);

		$this->SetFillColor($color[0], $color[1], $color[2]);
		$this->SetDrawColor($color[0], $color[1], $color[2]);

        //recuadro
        $this->Rect($this->GetX() , $this->GetY() , $this->GetPageWidth() - 20, 20);

        $this->SetFont($this->font, '', 10);

        $this->resetPos();
	    $this->SetXY($this->GetX() + 5 , $this->GetY() + 48);

        $this->Cell(20, 5, utf8_decode('Documento:'), $this->BORDER, 0, 'L');
        
        if ($this->data->receipt->customer_doc_type == 99) {
            $this->Cell(20, 5, '--' , $this->BORDER, 0, 'L');
        } else {
            $this->Cell(20, 5, $this->data->doc_types[$this->data->receipt->customer_doc_type] . ' ' . $this->data->receipt->customer_ident , $this->BORDER, 0, 'L');
        }

        $this->SetX(64);

        $this->Cell(20, 5, utf8_decode('Nombre:'), $this->BORDER, 0, 'L');

        $this->SetX(76);
        $this->Cell(120, 5, utf8_decode(strtoupper($this->data->receipt->customer_name)), $this->BORDER, 1, 'L');

        $this->SetX(15);

        $this->Cell(20, 5, utf8_decode('Domicilio:'), $this->BORDER, 0, 'L');
        $this->Cell(160, 5, utf8_decode(strtoupper($this->data->receipt->customer_address . ' - ' . $this->data->receipt->customer_city)), $this->BORDER, 1, 'L');

        $this->SetX(15);

        $this->Cell(28, 5, utf8_decode('Condición frente al IVA:'), $this->BORDER, 0, 'L');
        $this->Cell(60, 5, utf8_decode($this->data->responsibles[$this->data->receipt->customer_responsible]), $this->BORDER, 0, 'L');

        $this->Cell(20, 5, utf8_decode('Método de Pago:'), $this->BORDER, 0, 'L');
        $this->Cell(72, 5, utf8_decode($this->data->receipt->payment_method), $this->BORDER, 1, 'L');
	}

	function Concept()
	{
	    $this->resetPos();
	    $this->SetXY($this->GetX() , $this->GetY()+ 65);

	    $color = explode(',', $this->data->receipt->business->color->rgb);

		$this->SetFillColor($color[0], $color[1], $color[2]);
		$this->SetDrawColor($color[0], $color[1], $color[2]);

        $this->Rect($this->GetX(), $this->GetY(), $this->GetPageWidth() - 20, 70);

        $this->SetFont($this->font, '', 11);

        $this->resetPos();
	    $this->SetXY($this->GetX() + 5 , $this->GetY() +  67);

	    //moves

	    $moves = [];

	    if ($this->data->receipt->moves && $this->data->paraments->invoicing->resume_receipt) {

	        $moves_temp = explode('|', $this->data->receipt->moves);

	        foreach ($moves_temp as $mov) {

	            $m = explode('&', $mov);

	            $m_o = new \stdClass();
	            $m_o->date = explode('T', $m[0]); 
	            $m_o->date = new Time($m_o->date[0] . ' 00:00:00');   

	            $m_o->description = $m[1];
	            $m_o->total = $m[2];
	            $m_o->saldo = $m[3];

	            $moves[] = $m_o;
	        }

	        if ($this->data->receipt->connection_id) {

                $this->SetFont($this->fontB, '', 11);
                $this->Cell(20, 5, utf8_decode('Servicio:'), $this->BORDER,  0, 'L');
                $this->Cell(140, 5, utf8_decode($this->data->receipt->connection->service->name . ' - '. $this->data->receipt->connection->address), $this->BORDER,  0, 'L');
                $this->Ln();
                $this->SetX($this->GetX() + 5);
                $this->SetFont($this->font, '', 11);
            }

            $this->SetFont($this->font, '', 11);
            $this->Cell(35, 5, utf8_decode('Últimos movimientos:'), $this->BORDER,  0, 'L');
            $this->Cell(120, 5, utf8_decode(''), $this->BORDER,  0, 'L');
            $this->Cell(25, 5, utf8_decode('Totales ($)'), $this->BORDER,  0, 'R');
            $this->Ln();
            $this->SetX($this->GetX() + 5);
            $this->SetFont($this->font, '', 11);

            $last_saldo = 0;

            $moves_final = [];

            $max = 4;
            $counter = 0;

            $move_anterior = null;

            foreach ($moves as $move) {
                if ($counter < $max ) {
                   $moves_final[] =  $move;
                } else {
                    $move_anterior = $move;
                    break;
                }
                $counter++;
            }

            $moves = array_reverse ($moves_final);

            if ($move_anterior) {

                $this->Cell(20, 5, $move_anterior->date->format('d/m/Y'), $this->BORDER, 0,'C');
    	        $this->Cell(135, 5, utf8_decode('Saldo anterior'), $this->BORDER,0, 'L');
    	        $this->Cell(25, 5, number_format($move_anterior->saldo, 2, ',', '.'), $this->BORDER, 1, 'R');
    	        $this->SetX($this->GetX() + 5);
            }

    	    $max_char = 70;

    	    foreach ($moves as $move) {

    	        $this->Cell(20, 5, $move->date->format('d/m/Y'), $this->BORDER, 0,'C');

    	        $len = strlen($move->description);

    	        if ($len > $max_char) {
    	            $txt = str_split($move->description, $max_char);
    	            $this->Cell(135, 5, utf8_decode($txt[0]) . '...', $this->BORDER, 0, 'L');
    	        } else {
    	            $this->Cell(135, 5, utf8_decode($move->description), $this->BORDER, 0, 'L');
    	        }

    	        $this->Cell(25, 5, number_format($move->total, 2, ',', '.'), $this->BORDER, 1, 'R');
    	        $this->SetX($this->GetX() + 5);

    	        $last_saldo = $move->saldo;
    	    }

    	    $this->SetFont($this->fontB, '', 11);
	        $this->Cell(155, 5, utf8_decode('  Deuda Total'), $this->BORDER, 0, 'L');
	        $this->Cell(25, 5, number_format($last_saldo, 2, ',', '.'), $this->BORDER, 1, 'R');
	        $this->SetX($this->GetX() + 5);

    	    $this->SetFont($this->font, '', 11);
    	    $this->Cell(30, 5, utf8_decode('Pago Realizado:'), $this->BORDER,  0, 'L');
            $this->Ln();
            $this->SetX($this->GetX() + 5);
            $this->SetFont($this->font, '', 11);

            $this->SetFont($this->fontB, '', 11);
            $this->Cell(20, 5, '  ' . $this->data->receipt->date->format('d/m/Y'), $this->BORDER, 0,'C');
            $this->Cell(135, 5, utf8_decode($this->data->receipt->concept),  $this->BORDER, 0, 'L');
            $this->Cell(25, 5, '-' . number_format($this->data->receipt->total, 2, ',', '.'), $this->BORDER,1, 'R');
            $this->SetX($this->GetX() + 5);
            $this->SetFont($this->font, '', 11);

            $this->SetFont($this->font, '', 11);
            $this->Cell(30, 5, utf8_decode('Saldo restante:'), $this->BORDER,  0, 'L');
            $this->Ln();
            $this->SetX($this->GetX() + 5);
            $this->SetFont($this->fontB, '', 11);

            $saldo_restante = $last_saldo - $this->data->receipt->total;

            $this->Cell(20, 5, '  '. $this->data->receipt->date->format('d/m/Y'), $this->BORDER, 0,'C');
            $this->Cell(135, 5, utf8_decode(''),  $this->BORDER,0, 'L');
            $this->Cell(25, 5, number_format($saldo_restante, 2, ',', '.'), $this->BORDER, 1, 'R');
            $this->SetX($this->GetX() + 5);
            $this->SetFont($this->font, '', 11);

	    } else {

            $this->Cell(30, 5, utf8_decode('Recibí la suma de:'), $this->BORDER, 0, 'L');

            $n2LC = new NumberToLetterConverter();
            $importe_letter =  $n2LC->to_word($this->data->receipt->total, 'ARS');
            $this->MultiCell(150, 5, utf8_decode($importe_letter), $this->BORDER, 'L');

            $this->SetXY($this->GetX() + 5, $this->GetY() + 2); 

            $this->Cell(30, 5, utf8_decode('Concepto:'), $this->BORDER, 0, 'L');
            $this->MultiCell(150, 5, utf8_decode(strtoupper($this->data->receipt->concept)), $this->BORDER, 'L');

            $this->SetXY($this->GetX() + 5, $this->GetY() + 2); 

            $this->Cell(30, 5, utf8_decode('TOTAL: $ '), $this->BORDER, 0, 'L');
            $this->MultiCell(150, 5, number_format($this->data->receipt->total, 2, ',', '.'), $this->BORDER, 'L');
	    }

        //observaciones
        $this->SetXY($this->GetX(), $this->GetY()); 
        $this->Cell(25, 5, utf8_decode('Observaciones:'), $this->BORDER, 0, 'L');

        $this->MultiCell(155, 5, utf8_decode($this->data->receipt->observations), $this->BORDER, 'L');
	}

	function Footer() {}

	function Generate($data, $ctrl, $format = '')
	{
	    $this->data = $data;
	    $this->ctrl = $ctrl;

        $this->SetCompression(false);
        $this->SetAutoPageBreak(true, 1);

        $this->AddFont('OpenSans-CondBold', '', 'OpenSans-CondBold.php');
        $this->AddFont('OpenSans-CondensedLight', '', 'OpenSans-CondLight.php');

        $this->font = 'OpenSans-CondensedLight';
	    $this->fontB = 'OpenSans-CondBold';

        $this->AddPage();

	    $this->X = 10;
		$this->Y = 10;
		
		$this->customImage(WWW_ROOT . 'img/' . $this->data->receipt->business->logo, 12, 12, 300, 70);

		$count = $this->data->paraments->invoicing->print_duplicate_receipt ? 2 : 1;
		
		if ($count > 1) {
		    $this->customImage(WWW_ROOT . 'img/' . $this->data->receipt->business->logo, 12, 152, 300, 70);
		}

		for ($i = 0; $i < $count; $i++) {

		    $this->HeaderCustom();
		    $this->Customer();
		    $this->Concept();

		    $this->X = 10;
		    $this->Y = 150;
		}
	}
}
