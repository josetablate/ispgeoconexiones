<?php
namespace App\Controller\Component\Admin\Pdf;

use FPDF;
use Cake\I18n\Time;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\Filesystem\Folder;

class PresaleResumePdf extends FPDF {

    public $data;
    public $crtl;
    public $_page_width;
    public $_page_height;
    public $_page_middle;
    public $_page_marge_left = 10;
    public $_page_marge_right = 10;
    public $_page_marge_top = 10;
    public $_page_marge_down = 10;

    public $_page_padding_left = 2;
    public $_page_padding_right = 2;
    public $_page_padding_top = 3;
    public $_page_padding_down = 3;

    public $_border_cell = 0;

    public $_total_debt_month = 0;

	function Header() {

	    $color = explode(',', $this->data->customer->business->color->rgb);
	    $this->SetFillColor($color[0], $color[1], $color[2]);
		$this->SetDrawColor($color[0], $color[1], $color[2]);
		$logo = WWW_ROOT . 'img/' . $this->data->customer->business->logo;

		$this->SetFont('UniFont', '', 9);

		$this->SetFontSize(14);
        $this->SetTitle('RESUMEN DE VENTA');

        $this->_page_width = $this->GetPageWidth() - $this->_page_marge_left - $this->_page_marge_right;
        $this->_page_height = $this->GetPageHeight() - $this->_page_marge_top - $this->_page_marge_down;
        $this->_page_middle = $this->GetPageWidth() / 2;
        $this->SetXY($this->_page_marge_left, $this->_page_marge_top);

        //rect head
        $rect_head_hight = 20; 

        //logo
        $logo_head_width = 35; 
        $this->imageCenterCell($logo, $this->GetX() + 1, $this->GetY() + 1, $logo_head_width, $rect_head_hight - 2);

        //titulo
        $this->SetFontSize(14);
        $this->Text($this->_page_middle - 30, $this->GetY() + 13, 'RESUMEN DE VENTA ');

        //info empresa
        $info_empresa_head_width = 50; 
        $this->SetFontSize(10);
        $this->SetTextColor(100);

        $this->SetXY($this->GetPageWidth() - $this->_page_marge_right - $info_empresa_head_width - 2, $this->GetY());
        $this->Cell($info_empresa_head_width, $rect_head_hight / 3, utf8_decode($this->data->customer->business->name), $this->_border_cell, 2, 'R');
        $this->Cell($info_empresa_head_width, $rect_head_hight / 3, 'TEL.: '. $this->data->customer->business->phone , $this->_border_cell, 2, 'R');
        $this->Cell($info_empresa_head_width, $rect_head_hight / 3, utf8_decode($this->data->customer->business->address), $this->_border_cell, 1, 'R');
	}

	function Footer() {

		global $AliasNbPages;
		$this->SetY(-20);
		$this->SetFont('UniFont', '', 9);
		$this->Cell(0, 10, utf8_decode('Página ') . $this->PageNo() . ' de {nb}', 0, 0, 'C');
	}

	function AddCustomerInfo() {

	    $color = explode(',', $this->data->customer->business->color->rgb);
	    $this->SetFillColor($color[0], $color[1], $color[2]);
		$this->SetDrawColor($color[0], $color[1], $color[2]);

        //info cliente
        $row_hight = 6; 

        $this->SetFontSize(12);
        $this->SetTextColor(0);

        $this->SetXY($this->_page_marge_left, $this->GetY() + $this->_page_marge_top);
        $this->SetTextColor(255);
        $this->Cell($this->_page_width, $row_hight, 'CLIENTE', $this->_border_cell, 2, 'L', 1);
        $this->SetTextColor(0);

        $this->SetFontSize(10);

        $this->SetXY($this->GetX()+ $this->_page_padding_left, $this->GetY() + 2);
        $this->SetTextColor(0);
        $this->Cell(25, $row_hight, 'Nombre: ', $this->_border_cell, 0, 'L'); 
        $this->SetTextColor(80);
        $this->Cell($this->_page_middle - $this->GetX(), $row_hight, utf8_decode($this->data->customer->name), $this->_border_cell, 0, 'L'); 

        $this->SetX($this->GetX() + 2);
        $this->SetTextColor(0);
        $this->Cell(30, $row_hight, 'Documento: ', $this->_border_cell, 0, 'L'); 
        $this->SetTextColor(80);
        $this->Cell(61, $row_hight, $this->data->doc_types[$this->data->customer->doc_type] . ' ' . $this->data->customer->ident, $this->_border_cell, 1, 'L'); 

        $this->SetX($this->GetX() + 2);
        $this->SetTextColor(0);
        $this->Cell(25, $row_hight, 'Domicilio: ', $this->_border_cell, 0, 'L'); 
        $this->SetTextColor(80);

        $x = $this->GetX();// + $this->_page_middle - $this->GetX() + 2;
        $y = $this->GetY();
        $w = $this->GetPageWidth() - 10  - $this->_page_padding_right - $this->GetX();
        $address = utf8_decode($this->data->customer->address);  
        $this->Cell($w, $row_hight, $address, $this->_border_cell, 1, 'L'); 

        $this->SetXY($this->GetX() + 2, $this->GetY());
        $this->SetTextColor(0);
        $this->Cell(25, $row_hight, 'Tel.: ', $this->_border_cell, 0, 'L'); 
        $this->SetTextColor(80);
        $this->Cell($this->_page_middle - $this->GetX(), $row_hight, $this->data->customer->phone, $this->_border_cell, 0, 'L');

        $this->SetX($this->GetX() + 2);
        $this->SetTextColor(0);
        $this->Cell(30, $row_hight, 'Correo: ', $this->_border_cell, 0, 'L'); 
        $this->SetTextColor(80);
        $this->Cell(61, $row_hight, utf8_decode($this->data->customer->email), $this->_border_cell, 1, 'L');

        $this->SetX($this->GetX() + 2);
        $this->SetTextColor(0);
        $this->Cell(25, $row_hight, 'Tipo: ', $this->_border_cell, 0, 'L'); 
        $this->SetTextColor(80);
        $this->Cell($this->_page_middle - $this->GetX(), $row_hight, utf8_decode($this->data->responsibles[$this->data->customer->responsible]), $this->_border_cell, 0, 'L'); 

        $this->SetX($this->GetX() + 2);
        $this->SetTextColor(0);
        $this->Cell(40, $row_hight, utf8_decode('Día de vencimiento: '), $this->_border_cell, 0, 'L'); 
        $this->SetTextColor(80);
        $this->Cell(51, $row_hight, $this->data->customer->daydue, $this->_border_cell, 1, 'L'); 

        $this->SetX($this->GetX() + 2);
        $this->SetTextColor(0);
        $this->Cell(25, $row_hight, 'Ciudad: ', $this->_border_cell, 0, 'L'); 
        $this->SetTextColor(80);
        $this->Cell($this->_page_middle - $this->GetX(), $row_hight, utf8_decode($this->data->customer->city->name), $this->_border_cell, 1, 'L'); 
	}

	function addComment() {

    	$color = explode(',', $this->data->customer->business->color->rgb);
	    $this->SetFillColor($color[0], $color[1], $color[2]);
		$this->SetDrawColor($color[0], $color[1], $color[2]);

		$this->SetFontSize(12);

	    $row_hight = 6; 

	    $this->SetX($this->GetX());
        $this->SetTextColor(0);
        $this->Cell(25, $row_hight, 'Comentario: ', $this->_border_cell, 0, 'L'); 
        $this->SetTextColor(80);

        $this->ln();

		$this->SetFont('UniFont', '', 14);

        $comments = utf8_decode($this->data->comments);  
        $w = $this->_page_width - 29;
        if ($this->GetStringWidth($comments) > $w ) {
             $this->MultiCell($this->_page_width - 29, $row_hight , $comments , $this->_border_cell, 'L', 0); 
             $this->ln();
        }
        else {
            $this->Cell($this->_page_width - 29, $row_hight, $comments, $this->_border_cell, 0, 'L'); 
            $this->ln();
            $this->ln();
        }
	}

	function AddSellerInfo() {

	    $color = explode(',', $this->data->customer->business->color->rgb);
	    $this->SetFillColor($color[0], $color[1], $color[2]);
		$this->SetDrawColor($color[0], $color[1], $color[2]);

    	//info vendedor

    	$row_hight = 6; 

        $this->SetFontSize(12);
        $this->SetTextColor(0);

        $this->SetXY($this->_page_marge_left, $this->GetY());

        $this->SetTextColor(255);
        $this->Cell($this->_page_width, $row_hight, 'VENDEDOR', $this->_border_cell, 2, 'L', 1);
        $this->SetTextColor(0);

        $this->SetFontSize(10);

        $this->SetXY($this->GetX(), $this->GetY() + 2);
        $this->SetTextColor(0);
        $this->Cell(25, $row_hight, 'Nombre: ', $this->_border_cell, 0, 'L'); 
        $this->SetTextColor(80);
        $this->Cell($this->_page_middle - $this->GetX(), $row_hight, utf8_decode($this->data->user->name), $this->_border_cell, 0, 'L'); 

        $this->SetX($this->GetX() + 2);
        $this->SetTextColor(0);
        $this->Cell(30, $row_hight, 'Fecha: ', $this->_border_cell, 0, 'L'); 
        $this->SetTextColor(80);
        $this->Cell(63, $row_hight, $this->data->created->format('d/m/Y H:i'), $this->_border_cell, 1, 'L'); 
	}

    function AddInstallationInfoTitle() {

        $color = explode(',', $this->data->customer->business->color->rgb);
	    $this->SetFillColor($color[0], $color[1], $color[2]);
		$this->SetDrawColor($color[0], $color[1], $color[2]);

    	//info instalacion

    	$row_hight = 6;

        $this->SetFontSize(12);
        $this->SetTextColor(0);

        $this->SetXY($this->GetX() , $this->GetY() + $this->_page_padding_top);

        $this->SetTextColor(255);
        $this->Cell($this->_page_width, $row_hight, utf8_decode('SERVICIOS'), $this->_border_cell, 2, 'L', 1);

        $this->SetTextColor(0);
        $this->SetFontSize(10);

		$_w = array(
		    $this->_page_width * 0.85, //Concepto
		    $this->_page_width * 0.15, //Importe
	    );

		$this->SetXY($this->GetX(), $this->GetY() + 2);
        $this->SetFont('Arial', 'b', 13);
  
        $this->Cell($_w[0], 6, utf8_decode('Conceptos'), $this->_border_cell, 0, 'C', 0);
        $this->Cell($_w[1], 6, utf8_decode('Importe'), $this->_border_cell, 1, 'R', 0);
    }

	function AddInstallationInfo($service) {

        $color = explode(',', $this->data->customer->business->color->rgb);
	    $this->SetFillColor($color[0], $color[1], $color[2]);
		$this->SetDrawColor($color[0], $color[1], $color[2]);

		$_w = array(
		    $this->_page_width * 0.85, //Concepto
		    $this->_page_width * 0.15, //Importe
	    );

        $this->SetTextColor(80);

        $this->SetXY($this->GetX(), $this->GetY() + 1);
        $this->SetFont('Arial', 'b', 10);
        $this->Cell($_w[0], 6, utf8_decode($service->service_name), $this->_border_cell, 0, 'L', 0);
        $this->SetFont('Arial', '', 12);
        $this->Cell($_w[1], 6, '$  ' . number_format($service->service_total, 2, ',', '.'), $this->_border_cell, 1, 'R', 0);

        $this->SetXY($this->GetX(), $this->GetY() + 1);
        $this->SetFont('Arial', '', 10);
        $this->Cell($_w[0], 6, utf8_decode($service->address), $this->_border_cell, 0, 'L', 0);
        $this->Cell($_w[1], 6, utf8_decode(""), $this->_border_cell, 1, 'C', 0);

        $this->SetXY($this->GetX(), $this->GetY() + 1);
        $this->SetFont('Arial', '', 10);
        $this->Cell($_w[0], 6, utf8_decode("Fecha estimada: " . $service->installation_date->format('d/m/Y')), $this->_border_cell, 0, 'L', 0);
        $this->Cell($_w[1], 6, utf8_decode(""), $this->_border_cell, 1, 'C', 0);

        $subtotal = 0;

        if ($service->discount_name != '') {

            $this->SetXY($this->GetX(), $this->GetY() + 1);
            $this->SetFont('Arial', '', 10);
            $this->Cell($_w[0], 6, utf8_decode($service->discount_name), $this->_border_cell, 0, 'L', 0);
            $discount_value = $service->service_total * $service->discount_total;
            $this->SetFont('Arial', '', 12);
            $this->Cell($_w[1], 6, '$ -' . number_format($discount_value, 2, ',', '.'), $this->_border_cell, 1, 'R', 0);

            $subtotal = $service->service_total * (1 - $service->discount_total);
        } else {
            $subtotal = $service->service_total;
        }

        $this->SetXY($this->GetX(), $this->GetY() + 1);
        $this->SetFont('Arial', 'b', 10);
        $this->Cell($_w[0], 6, utf8_decode("Subtotal " . $service->service_name), $this->_border_cell, 0, 'L', 0);
        $this->SetFont('Arial', 'b', 12);
        $this->Cell($_w[1], 6, '$  ' . number_format($subtotal, 2, ',', '.'), $this->_border_cell, 1, 'R', 0);
	}

	function AddDebtTitle() {

	    $color = explode(',', $this->data->customer->business->color->rgb);
	    $this->SetFillColor($color[0], $color[1], $color[2]);
		$this->SetDrawColor($color[0], $color[1], $color[2]);

        //info deudas

        $row_hight = 6; 

        $this->SetFontSize(12);
        $this->SetTextColor(0);

        $this->SetXY($this->_page_marge_left, $this->GetY() + 4);
        $this->SetTextColor(255);
        $this->Cell($this->_page_width, $row_hight, utf8_decode('VENTA'), $this->_border_cell, 2, 'L', 1);
        $this->SetTextColor(0);

		$this->SetFontSize(10);

		$_w = array(
            $this->_page_width * 0.85, //Conceptos.
            $this->_page_width * 0.15, //Importe
	    );

		$this->SetXY($this->GetX(), $this->GetY() + 2);

        $this->SetFont('Arial', 'b', 13);
        $this->Cell($_w[0], 6, utf8_decode('Conceptos'), $this->_border_cell, 0, 'C', 0);
        $this->Cell($_w[1], 6, utf8_decode('Importe'), $this->_border_cell, 1, 'R', 0);

        $this->SetTextColor(80);
	}

	function AddDebt($debt) {

	    $color = explode(',', $this->data->customer->business->color->rgb);
	    $this->SetFillColor($color[0], $color[1], $color[2]);
		$this->SetDrawColor($color[0], $color[1], $color[2]);

        //info deudas

        $row_hight = 6; 

		$_w = array(
            $this->_page_width * 0.85, //Conceptos.
            $this->_page_width * 0.15, //Importe
	    );

        $this->SetXY($this->GetX(), $this->GetY() + 1);
        $this->SetFont('Arial', 'b', 10);
        $this->Cell($_w[0], 6, utf8_decode($debt->description), $this->_border_cell, 0, 'L', 0);
        $this->SetFont('Arial', '', 12);
        $this->Cell($_w[1], 6, '$  ' . number_format($debt->total, 2, ',', '.'), $this->_border_cell, 1, 'R', 0);

        $subtotal = 0;

        if ($debt->customers_has_discount) {

            $this->SetXY($this->GetX(), $this->GetY() + 1);
            $this->SetFont('Arial', '', 10);
            $this->Cell($_w[0], 6, utf8_decode($debt->customers_has_discount->description), $this->_border_cell, 0, 'L', 0);
            $this->SetFont('Arial', '', 12);
            $this->Cell($_w[1], 6, '$ '. number_format(-$debt->customers_has_discount->total, 2, ',', '.'), $this->_border_cell, 1, 'R', 0);

            $subtotal = $debt->total - $debt->customers_has_discount->total;
        } else {
            $subtotal = $debt->total;
        }

        // if ($debt->duedate < Time::now()->modify('+1 month')->day(1)) {
            $this->_total_debt_month += $subtotal;
        // }

        $this->SetXY($this->GetX(), $this->GetY() + 1);
        $this->SetFont('Arial', 'b', 10);
        $this->Cell($_w[0], 6, utf8_decode("Subtotal " . $debt->description), $this->_border_cell, 0, 'L', 0);
        $this->SetFont('Arial', 'b', 12);
        $this->Cell($_w[1], 6, '$  ' . number_format($subtotal, 2, ',', '.'), $this->_border_cell, 1, 'R', 0);
	}

	function TotalDebt() {

	    $_w = array(
            $this->_page_width * 0.70, //Conceptos.
            $this->_page_width * 0.30, //Importe
	    );

	    $this->SetXY($this->GetX(), $this->GetY() + 1);
        $this->SetFont('Arial', 'b', 14);
        $this->Cell($_w[0], 6, utf8_decode("TOTAL: "), $this->_border_cell, 0, 'R', 0);
        $this->SetFont('Arial', 'b', 14);
        $this->Cell($_w[1], 6, '$  ' . number_format($this->_total_debt_month, 2, ',', '.'), $this->_border_cell, 1, 'R', 0);
	}

	function Generate($data, $ctrl, $dest = 'I', $send_email = FALSE) {

        $limit_per_page = 2;

	    $this->data = $data;
	    $this->ctrl = $ctrl;

        $this->SetCompression(false);
		$this->AddFont('UniFont', '', 'DejaVuSans.php');
		$this->AliasNbPages();

        $current_page = 0;

        $this->AddPage();

        $limit_page = 280;

        $this->AddCustomerInfo();
        $this->AddSellerInfo();

        if (sizeof($this->data->service_pending) > 0) {

            if ($this->GetY() + 30 < $limit_page) {
                $this->AddInstallationInfoTitle();
            } else {
                $current_page++;
                $this->AddPage();
                $this->AddInstallationInfoTitle();
            }

            foreach ($this->data->service_pending as $service_pending) {

                $this->AddInstallationInfo($service_pending);
                $this->Line(10, $this->GetY(), $this->GetPageWidth() - 10, $this->GetY());

                if ($this->GetY() + 30 > $limit_page) {
                    $current_page++;
                    $this->AddPage();
                    $this->AddInstallationInfoTitle();
                }
            }
        } //end servicios pendientes

        if (sizeof($this->data->debts) > 0) {

            if ($this->GetY() + 30 < $limit_page) {
                $this->AddDebtTitle();
            } else {
                $current_page++;
                $this->AddPage();
                $this->AddDebtTitle();
            }

            foreach ($this->data->debts as $debt) {

                $this->AddDebt($debt);
                $this->Line(10, $this->GetY(), $this->GetPageWidth() - 10, $this->GetY());

                if ($this->GetY() + 30 > $limit_page) {
                    $current_page++;
                    $this->AddPage();
                    $this->AddDebtTitle();
                }
            }

            $this->TotalDebt();

        } //end servicios pendientes

        $this->addComment();
	}
}
