<?php
namespace App\Controller\Component\Admin;

use App\Controller\Component\Common\MyComponent;

use Cake\Controller\ComponentRegistry;
use Cake\I18n\Time;
use FPDF;

/**
 * Documents component
 */
class CashRegister extends MyComponent
{
    const TYPE_IN = 0;
    const TYPE_OUT = 1;
    const TYPE_OPEN = 2;
    const TYPE_CLOSE = 3;
    
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->_ctrl->loadModel('CashEntities');
        $this->_ctrl->loadModel('IntOutCashsEntities');
    }

    public function register($data, $type)
    {
        switch ($type) {

            case CashRegister::TYPE_IN:

                $iocash = $this->_ctrl->IntOutCashsEntities->newEntity();
                $iocash->created = $data->created;
                $iocash->type =  $data->type;
                $iocash->payment_method_id =  $data->payment_method_id;
                $iocash->is_table = $data->is_table;
                $iocash->concept = $data->concept;
                $iocash->in_value = $data->in_value;
                $iocash->out_value = 0;
                $iocash->cash_entity_id = $data->cash_entity->id;
                $iocash->user_id = $data->cash_entity->user_id;
                $iocash->data = $data->data ? $data->data : NULL;
                $iocash->seating_number = $data->seating_number;

                if ($iocash->is_table == 1) {
                    $iocash->saldo_contado = $data->cash_entity->contado;
                    $iocash->saldo_other = 0;
                } else {
                    $iocash->saldo_contado = 0;
                    $iocash->saldo_other = $data->cash_entity->cash_other;
                }

                if (!$this->_ctrl->IntOutCashsEntities->save($iocash)) {
                    $this->_ctrl->Flash->error(__('Error, no se pudo registrar el movimiento de caja.'));
                    return false;
                }

                //log de accion

                $action =  'Ingreso de Caja';
                $detail =  'Caja: ' . $data->cash_entity->name . PHP_EOL;
                $detail .= 'Valor: ' . number_format($data->in_value, 2, ',', '.') . PHP_EOL;
                $detail .= 'Concepto: ' . $data->concept . PHP_EOL;
                $detail .= 'Tipo: ' . $data->type . PHP_EOL;
                $detail .= 'Medio: ' . (($data->is_table == 1) ? 'Contado' : 'Otros' ) . PHP_EOL;

                $this->_ctrl->registerActivity($action, $detail);
                break;

            case CashRegister::TYPE_OUT:

                $iocash = $this->_ctrl->IntOutCashsEntities->newEntity();
                $iocash->type = $data->type;
                $iocash->concept = $data->concept;
                $iocash->in_value = 0;
                $iocash->is_table = $data->is_table;
                $iocash->out_value = $data->out_value;
                $iocash->cash_entity_id = $data->cash_entity->id;
                $iocash->user_id = $data->cash_entity->user_id;
                $iocash->data = $data->data ? $data->data : NULL;
                $iocash->seating_number = $data->seating_number;

                if ($iocash->is_table == 1) {
                    $iocash->saldo_contado = $data->cash_entity->contado;
                    $iocash->saldo_other = 0;
                } else {
                    $iocash->saldo_contado = 0;
                    $iocash->saldo_other = $data->cash_entity->cash_other;
                }

                if (!$this->_ctrl->IntOutCashsEntities->save($iocash)) {
                    $this->_ctrl->Flash->error(__('Error, no se pudo registrar el movimiento de caja.'));
                    return false;
                }

                //log de accion

                $action =  'Egreso de Caja';
                $detail =  'Caja: ' . $data->cash_entity->name . PHP_EOL;
                $detail .= 'Valor: ' . number_format($data->out_value, 2, ',', '.') . PHP_EOL;
                $detail .= 'Concepto: ' . $data->concept . PHP_EOL;
                $detail .= 'Tipo: ' . $data->type . PHP_EOL;
                $detail .= 'Medio: ' . (($data->is_table == 1) ? 'Contado' : 'Otros' ) . PHP_EOL;

                $this->_ctrl->registerActivity($action, $detail);
                break;

            case CashRegister::TYPE_OPEN:

                $iocash = $this->_ctrl->IntOutCashsEntities->newEntity();
                $iocash->type = 'APERTURA';
                $iocash->concept = 'Saldo Inicial';
                $iocash->in_value = 0;
                $iocash->out_value = 0;
                $iocash->cash_entity_id = $data->cash_entity->id;
                $iocash->user_id = $data->cash_entity->user_id;

                $iocash->saldo_contado = $data->cash_entity->contado;
                $iocash->saldo_other = $data->cash_entity->cash_other;

                $apertures_amount = $this->_ctrl->IntOutCashsEntities->find()
                    ->where(['cash_entity_id' => $iocash->cash_entity_id, 'deleted' => false, 'type' => 'APERTURA'])->count();

                $iocash->number_part = sprintf("%'.03d", $data->cash_entity->id) . '-' . sprintf("%'.05d", $apertures_amount + 1); 

                if (!$this->_ctrl->IntOutCashsEntities->save($iocash)) {
                    $this->_ctrl->Flash->error(__('Error, no se pudo registrar la apertura de caja.'));
                    return false;
                }

                //log de accion

                $action =  'Apertura de Caja';
                $detail =  'Caja: ' . $data->cash_entity->name . PHP_EOL;
                $detail .= 'Saldo: ' . number_format($data->cash_entity->cash, 2, ',', '.') . PHP_EOL;
                $detail .= 'Parte #: ' . $iocash->number_part. PHP_EOL;
                $detail .= 'Tipo: APERTURA' . PHP_EOL;

                $this->_ctrl->registerActivity($action, $detail);
                break;

            case CashRegister::TYPE_CLOSE:

                $iocash = $this->_ctrl->IntOutCashsEntities->newEntity();
                $iocash->type = 'CIERRE';
                $iocash->concept = 'Saldo';
                $iocash->in_value = 0;
                $iocash->out_value = 0;
                $iocash->cash_entity_id = $data->cash_entity->id;
                $iocash->user_id = $data->cash_entity->user_id;

                $iocash->saldo_contado = $data->cash_entity->contado;
                $iocash->saldo_other = $data->cash_entity->cash_other;

                $apertures_amount = $this->_ctrl->IntOutCashsEntities->find()
                    ->where(['cash_entity_id' => $iocash->cash_entity_id, 'deleted' => false, 'type' => 'APERTURA'])->count();

                $iocash->number_part = sprintf("%'.03d", $data->cash_entity->id) . '-' . sprintf("%'.05d", $apertures_amount); 

                if (!$this->_ctrl->IntOutCashsEntities->save($iocash)) {
                    $this->_ctrl->Flash->error(__('Error, no se pudo registrar el cierre de caja.'));
                    return false;
                }

                //log de accion

                $action =  'Cierre de Caja';
                $detail =  'Caja: ' . $data->cash_entity->name . PHP_EOL;
                $detail .= 'Saldo: ' . number_format($data->cash_entity->cash, 2, ',', '.') . PHP_EOL;
                $detail .= 'Parte #: ' . $iocash->number_part . PHP_EOL;
                $detail .= 'Tipo: CIERRE' . PHP_EOL;

                $this->_ctrl->registerActivity($action, $detail);
                break;
        }

        return $iocash;
    }

    public function remove($iocash)
    {
        if (!$this->_ctrl->IntOutCashsEntities->delete($iocash)) {
            $this->_ctrl->Flash->error(__('Error, no se pudo eliminar el movimiento de caja.'));
            return false;
        }

        return $iocash;
    }
}
