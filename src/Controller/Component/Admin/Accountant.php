<?php
namespace App\Controller\Component\Admin;

use App\Controller\Component\Common\MyComponent;

use Cake\Controller\ComponentRegistry;
use Cake\I18n\Time;
use Cake\Datasource\ConnectionManager;

class Accountant extends MyComponent
{
    protected $_defaultConfig = [];

    public $components = [];

    public function initialize(array $config)
    {
        parent::initialize($config);
    }

    public function isEnabled()
    {
        $paraments = $this->_ctrl->request->getSession()->read('paraments');
        return $paraments->accountant->account_enabled;
    } 

    /**
    *  [account_parent, name]
    */
    public function createAccount(array $data)
    {
        $this->_ctrl->loadModel('Accounts');

        $hrchy = substr(strval($data['account_parent']), 0, 4);

        $database = ConnectionManager::get('default');
        $results = $database->execute("SELECT accounts.code FROM `accounts` WHERE `code` like '$hrchy%' order by code DESC limit 1");

        $lastAccountCode = null;
    	foreach ($results as $r) {
    		$lastAccountCode = $r['code'];						
    	}

        $account = $this->_ctrl->Accounts->newEntity();
        $account->name = $data['name'];

        if (intval($lastAccountCode) == $data['account_parent']) { // no hay hijos todavia
            $account->code = intval($hrchy . '00001');
        } else {
            $lastAccountCode = intval($lastAccountCode);
            $account->code = ++$lastAccountCode;
        }

        if (!$this->_ctrl->Accounts->save($account)) {
            $this->_ctrl->Flash->error(__('No se pudo registrar la cuenta.'));
            return false;
        }

        return $account;
    }

    public function deleteAccount($account)
    {
        $this->_ctrl->loadModel('Accounts');
        $this->_ctrl->Accounts->delete($account);
    }

    /**
    *  [concept, comments, seating, account_debe, account_haber, value, seating_type => a, created = null, user_id = null ]
    */
    public function addSeating(array $data)
    {
        if (!array_key_exists('seating_type', $data) || !$data['seating_type']) {
            $data['seating_type'] = 'a';
        }

        if (!array_key_exists('created', $data) || !$data['created']) {
            $data['created'] = Time::now();
        }

        if (!array_key_exists('user_id', $data) || !$data['user_id']) {
            
            $data['user_id'] = 100;
            
            if (isset($this->_ctrl->request->getSession()->read('Auth.User')['id'])) {
                $data['user_id'] = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
            }
        }

        $this->_ctrl->loadModel('Seating');
        $this->_ctrl->loadModel('Accounts');

        if (!$data['seating']) {

            //creo asiento          
            $data['seating'] = $this->_ctrl->Seating->newEntity();
            $data['seating']->concept = $data['concept'];
            $data['seating']->comments = $data['comments'];
            $data['seating']->user_id = $data['user_id'];
            $data['seating']->type = $data['seating_type'];
            $data['seating']->created = $data['created'];

            //guardo asiento
            if (!$this->_ctrl->Seating->save($data['seating'])) {
                $this->_ctrl->Flash->error(__('No se pudo crear el asiento contable'));
                return false;
            }
        }

        if ($data['account_debe']) {

            //asiento detalle de la cuenta del debe
            $seatingdetail1 = $this->_ctrl->Seating->SeatingDetail->newEntity();
            $seatingdetail1->account_code = $data['account_debe'];
            $seatingdetail1->seating_number = $data['seating']->number;
            $seatingdetail1->debe =  $data['value'];

            //modifico el saldo de la cuenta del debe
            $account = $this->_ctrl->Accounts->find()->where(['code' => $data['account_debe']])->first();
            
            if(!$account){
                $this->deleteSeating($data['seating']->number);
                $this->_ctrl->Flash->error(__('No se pudo crear el detalle asiento contable'));
                return false;
            }
            
            $account->saldo += $seatingdetail1->debe;

            //actualizo saldo del la cuenta del debe
            if (!$this->_ctrl->Accounts->save($account)) {
                $this->_ctrl->Flash->error(__('No se pudo actualizar el saldo de la cuenta {0} ', $account->code));
                $this->deleteSeating($data['seating']->number);
                return false;
            }

            $this->updateSaldoAccountParent($account, $seatingdetail1->debe, 0);

            //actualizo el saldo de la cuenta del debe
            if (!$this->_ctrl->Seating->SeatingDetail->save($seatingdetail1)) {
                $this->deleteSeating($data['seating']->number);
                $this->_ctrl->Flash->error(__('No se pudo crear el detalle asiento contable'));
                return false;
            }
        }

        if ($data['account_haber']) {

            //asiento de la cuenta del haber
            $seatingdetail2 = $this->_ctrl->Seating->SeatingDetail->newEntity();
            $seatingdetail2->account_code = $data['account_haber'];
            $seatingdetail2->seating_number = $data['seating']->number;
            $seatingdetail2->haber =  $data['value'];

            //modifico el saldo de la cuenta del haber
            $account = $this->_ctrl->Accounts->find()->where(['code' => $data['account_haber']])->first();
            
            if(!$account){
                $this->deleteSeating($data['seating']->number);
                $this->_ctrl->Flash->error(__('No se pudo crear el detalle asiento contable'));
                return false;
            }
            
            $account->saldo -= $seatingdetail2->haber;

            //actualizo el saldo de la cuenta del haber
            if (!$this->_ctrl->Accounts->save($account)) {
                $this->_ctrl->Flash->error(__('No se pudo actualizar el saldo de la cuenta {0} ', $account->code));
                $this->deleteSeating($data['seating']->number);
                return false;
            }

            $this->updateSaldoAccountParent($account, 0, $seatingdetail2->haber);

            //guardo detalle del asiento de la cuenta del ahaber
            if (!$this->_ctrl->Seating->SeatingDetail->save($seatingdetail2)) {
                $this->deleteSeating($data['seating']->number);
                $this->_ctrl->Flash->error(__('No se pudo crear el detalle asiento contable'));
                return false;
            }
        }

        return $data['seating'];
    }

    public function deleteSeating($number)
    {
        $this->_ctrl->loadModel('Seating');
        $seating = $this->_ctrl->Seating->get($number, ['contain' => ['SeatingDetail.Accounts']]);

        foreach ($seating->seating_detail as $seating_detail) {

            $seating_detail->account->saldo -= $seating_detail->debe;
            $seating_detail->account->saldo += $seating_detail->haber;

            if ($this->updateSaldoAccountParent($seating_detail->account, -$seating_detail->debe,  -$seating_detail->haber)) {
               
                $this->_ctrl->Seating->SeatingDetail->deleteAll(['seating_number' => $seating->number]);
            } else {
                return false;
            }
        }

        $this->_ctrl->Seating->delete($seating);

        return true;
    }

    public function generateContrasiento($number){

        return true;

        $this->_ctrl->loadModel('Seating');
        $seating = $this->_ctrl->Seating->get($number, ['contain' => ['SeatingDetail.Accounts']]);

        $contraseating = $this->addSeating([
            'concept' => __('Contraaseinto de #{0}', str_pad($number, 5, "0", STR_PAD_LEFT)), 
            'comments' => '',
            'seating' => NULL,
            'account_debe' => NULL,
            'account_haber' => NULL,     
        ]);

        foreach($seating->seating_detail as $seating_detail){

            if($seating_detail->debe > 0){

                $this->addSeating([
                    'concept' => '', 
                    'comments' => '',
                    'seating' => $contraseating,
                    'account_debe' => NULL,
                    'account_haber' => $seating_detail->account_code,
                    'value' => $seating_detail->debe                  
                ]);


            }else if($seating_detail->haber > 0){

                $this->addSeating([
                    'concept' => '', 
                    'comments' => '',
                    'seating' => $contraseating,
                    'account_debe' => $seating_detail->account_code,
                    'account_haber' => NULL,
                    'value' => $seating_detail->haber                  
                ]);
            }
        }
    }

    private function updateSaldoAccountParent($account, $debe = 0, $haber = 0)  
    {

        $hrchy_temp = $account->code;
        $hrchy = '';
        $hrchy_temp = str_split(substr($hrchy_temp, 0, 4));
        foreach ($hrchy_temp as $h) {
            $hrchy .= ($h != 0) ? $h : '';
        }

        $i = strlen($hrchy);

        while ( $i > 0 ) {

            $this->_ctrl->loadModel('Accounts');
            $account_parent = $this->_ctrl->Accounts->find()->where(['code' => str_pad($hrchy, 9, "0", STR_PAD_RIGHT)])->first();

            if ($account_parent) {

                $account_parent->saldo += $debe;
                $account_parent->saldo -= $haber;

                if (!$this->_ctrl->Accounts->save($account_parent)) {
                    return false;
                }
            }

            if ($i > 0)  $i--;

            $hrchy_temp = str_split(substr($hrchy, 0, $i));
            $hrchy = '';
            foreach ($hrchy_temp as $h) {
                $hrchy .= ($h != 0) ? $h : '';
            }

            if ($i > 0) {
                $i = strlen($hrchy);
            }
        }

        return true;
    }
}
