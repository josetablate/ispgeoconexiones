<?php
namespace App\Controller\Component\Pdf;

use Cake\Controller\Component\Common\MyComponent;

use Cake\Controller\ComponentRegistry;
use Cake\I18n\Time;
use FPDF;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Network\Exception\NotFoundException;
use Cake\Event\EventManager;
use Cake\Event\Event;
use App\Controller\Component\Admin\Pdf\ResumePdf;

/**
 * Documents component
 */
class CreateResumePdf extends MyComponent
{
    const TYPE_ACCOUNT_RESUME = 0;

    protected $_defaultConfig = [];

    private $_data;

    // The other component your component uses
    public $components = [];

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->_ctrl->loadModel('Customers');

        $this->initEventManager();
    }

    public function initEventManager() 
    {
        EventManager::instance()->on(
            'CreateResumePdf.Error',
            function($event, $msg, $data, $flash = false) {

                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $log->user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Error';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);

                if ($flash) {
                    $this->_ctrl->Flash->error(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'CreateResumePdf.Warning',
            function($event, $msg, $data, $flash = false) {

                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $log->user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Warning';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);

                if ($flash) {
                    $this->_ctrl->Flash->warning(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'CreateResumePdf.Log',
            function($event, $msg, $data) {

            }
        );

        EventManager::instance()->on(
            'CreateResumePdf.Notice',
            function($event, $msg, $data) {

            }
        );
    }

    public function generate($data, $type, $return = false)
    {
        switch ($type) {

            case PDFGeneratorComponent::TYPE_ACCOUNT_RESUME:
                return $this->resume($data, $return);
                break;
        }

        return true;
    }

    private function resume($data, $return)
    {
        $data->customer = $this->_ctrl->Customers->get($data->code, [
            'contain' => [
                'Users',
                'Areas',
                'Services',
            ]
        ]);

        $data->paraments = $this->_ctrl->request->getSession()->read('paraments');
        $data->responsibles = $this->_ctrl->request->getSession()->read('afip_codes')['responsibles'];
        $data->doc_types = $this->_ctrl->request->getSession()->read('afip_codes')['doc_types'];

        $this->_ctrl->loadModel('Debts');
        $data->customer->debts = $this->_ctrl->Debts->find()->contain(['Users'])->where(['invoice_id IS' => NULL, 'customer_code' => $data->code]);

        $this->_ctrl->loadModel('Invoices');
        $data->customer->invoices = $this->_ctrl->Invoices->find()->contain(['Users', 'Concepts'])->where(['customer_code' => $data->code]);

        $this->_ctrl->loadModel('DebitNotes');
        $data->customer->debitNotes = $this->_ctrl->DebitNotes->find()->contain(['Users', 'ConceptsDebit'])->where(['customer_code' => $data->code]);

        $this->_ctrl->loadModel('Payments');
        $data->customer->payments = $this->_ctrl->Payments->find()->contain(['Users', 'CashEntities', 'Receipts'])->where(['Payments.customer_code' => $data->code]);

        foreach ($data->customer->payments as $payment) {

            foreach ($data->paraments->payment_getway as $pg) {

                if ($pg->id == $payment->payment_method_id) {
                    $payment_method = new \stdClass;
                    $payment_method = $pg;
                    $payment->payment_method = $payment_method;
                }
            }
        }

        $this->_ctrl->loadModel('CreditNotes');
        $data->customer->creditNotes = $this->_ctrl->CreditNotes->find()->contain(['Users', 'ConceptsCredit'])->where(['customer_code' => $data->code]);

        $data->debt_before_period = 0;
        $data->debt_total_period = 0;
        $data->payment_total_period = 0;
        $data->payment_before_period = 0;

        $data->list = [];

        foreach ($data->customer->debts as $debt) {

            if ($debt->duedate < $data->from) {
                $data->debt_before_period += $debt->total;
            } else if ($debt->duedate < $data->to) {
                $d = [
                    'fecha' => $debt->created, 
                    'description' => $debt->description,
                    'cant' => $debt->quantity,
                    'precio' => $debt->price,
                    'subtotal' => $debt->total,
                ];

                $data->list[$debt->created->format('U') + $debt->id] = $d;
                $data->debt_total_period += $debt->total;
            }
        }

        foreach ($data->customer->invoices as $invoice) {

            if ($invoice->duedate < $data->from) {
                $data->debt_before_period += $invoice->total;
            } else if ($invoice->duedate < $data->to) {

                foreach ($invoice->concepts as $concept) {
                     $d =  [
                        'fecha' => $invoice->date, 
                        'description' => $concept->description,
                        'cant' => $concept->quantity,
                        'precio' => $concept->price,
                        'subtotal' => $concept->total,
                     ];
                     $data->list[$invoice->date->format('U') + $invoice->id] = $d;
                }

                $data->debt_total_period += $invoice->total;
            }

        }

        foreach ($data->customer->debitNotes as $debitNote) {

            if ($debitNote->date < $data->from) {
                $data->debt_before_period += $debitNote->total;
            } else if ($debitNote->date < $data->to) {
                $data->debt_total_period += $debitNote->total;

                foreach($debitNote->concepts_debit as $concept){
                     $d =  [
                        'fecha' => $debitNote->date, 
                        'description' => $concept->description,
                        'cant' => $concept->quantity,
                        'precio' => $concept->price,
                        'subtotal' => $concept->total,
                    ];
                    $data->list[$debitNote->date->format('U') + $debitNote->id] = $d;
                }
            }
        }

        foreach ($data->customer->payments as $pay) {

            if ($pay->created < $data->from) {
                $data->payment_before_period += $pay->import;
            } else if ($pay->created < $data->to) {

                 $d =  [
                    'fecha' => $pay->created,
                    'description' => $pay->concept,
                    'cant' => '',
                    'precio' => '',
                    'subtotal' => -$pay->import,
                ];
                $data->list[$pay->created->format('U') + $pay->id] = $d;
                $data->payment_total_period += $pay->import;
            }
        }

        foreach ($data->customer->creditNotes as $creditNote) {

            if ($creditNote->date < $data->from) {
                $data->payment_before_period += $creditNote->total;
            } else if ($creditNote->date < $data->to) {

                foreach ($creditNote->concepts_credit as $concept) {
                     $d =  [
                        'fecha' => $creditNote->date, 
                        'description' => $concept->description,
                        'cant' => $concept->quantity,
                        'precio' => $concept->price,
                        'subtotal' => -$concept->total,
                     ];
                     $data->list[$creditNote->date->format('U') + $creditNote->id] = $d;
                }

                $data->payment_total_period += $creditNote->total;
            }
        }

        $data->saldo = $data->debt_before_period +  $data->debt_total_period - $data->payment_before_period - $data->payment_total_period;

        $d = [
            'fecha' => $data->from,
            'description' => 'Saldo Anterior',
            'cant' => '',
            'precio' => '',
            'subtotal' => $data->debt_before_period - $data->payment_before_period,
        ];
        $data->list[0] = $d;

        ksort($data->list);

        if ($return) {
            return $data;
        }

        $this->response = $this->response->withCharset('UTF-8');
        $this->response = $this->response->withType('application/pdf');

        $resume = new ResumePdf();
        return $resume->generate($data, $this->_ctrl);
    }
}
