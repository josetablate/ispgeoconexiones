<?php
namespace App\Controller\Component\Admin;

use App\Controller\Component\Admin\FiscoAfip;

use Cake\Controller\ComponentRegistry;
use Cake\I18n\Time;
use WsFE;
use FPDF;
use Cake\Log\Log;
use Cake\Error\Debugger;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Event\EventManager;
use Cake\Event\Event;

/**
 * Documents component
 */
class FiscoAfipComp extends FiscoAfip
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    // The other component your component uses
    public $components = [
        'CurrentAccount' => [
            'className' => '\App\Controller\Component\Admin\CurrentAccount'
        ], 
        'Accountant' => [
            'className' => '\App\Controller\Component\Admin\Accountant'
        ]
    ];

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->_ctrl->loadModel('Invoices');
        $this->_ctrl->loadModel('Concepts');

        $this->_ctrl->loadModel('Presupuestos');
        $this->_ctrl->loadModel('PresupuestoConcepts');

        $this->_ctrl->loadModel('Receipts');

        $this->_ctrl->loadModel('CreditNotes');
        $this->_ctrl->loadModel('ConceptsCredit');

        $this->_ctrl->loadModel('DebitNotes');
        $this->_ctrl->loadModel('ConceptsDebit');

        $this->initEventManager();
    }

    public function initEventManager()
    {
        EventManager::instance()->on(
            'FiscoAfipComp.Error',
            function($event, $msg, $data, $flash = false) {

                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $user_id = null;

                if (!isset($this->_ctrl->request->getSession()->read('Auth.User')['id'])) {
                    $user_id = 100;
                } else {
                    $user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                }

                $log->user_id = $user_id;

                $log->type = 'Error';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);

                if ($flash) {
                    $this->_ctrl->Flash->error(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'FiscoAfipComp.Warning',
            function($event, $msg, $data, $flash = false) {

                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();

                $user_id = null;

                if (!isset($this->_ctrl->request->getSession()->read('Auth.User')['id'])) {
                    $user_id = 100;
                } else {
                    $user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                }

                $log->user_id = $user_id;
                $log->type = 'Warning';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);

                if ($flash) {
                    $this->_ctrl->Flash->warning(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'FiscoAfipComp.Log',
            function ($event, $msg, $data) {

            }
        );

        EventManager::instance()->on(
            'FiscoAfipComp.Notice',
            function ($event, $msg, $data) {

            }
        );
    }

    private function getLastNumCompByType($comprobante)
    {
        switch ($comprobante->tipo_comp) {

             case '000':

                $comp = $this->_ctrl->Presupuestos->find()->where(['tipo_comp' => $comprobante->tipo_comp, 'business_id' => $comprobante->business_id])->order(['num' => 'DESC'])->first();
                if ($comp) {
                    return $comp->num + 1;
                } else {
                    return 1;
                }
                break;

            case 'XXX':

                $comp = $this->_ctrl->Invoices->find()->where(['tipo_comp' => $comprobante->tipo_comp, 'business_id' => $comprobante->business_id])->order(['num' => 'DESC'])->first();
                if ($comp) {
                    return $comp->num + 1;
                } else {
                    return 1;
                }
                break;

            case 'NCX':

                $comp = $this->_ctrl->CreditNotes->find()->where(['tipo_comp' => $comprobante->tipo_comp, 'business_id' => $comprobante->business_id])->order(['num' => 'DESC'])->first();
                if ($comp) {
                    return $comp->num + 1;
                } else {
                    return 1;
                }
                break;

            case 'NDX':

                $comp = $this->_ctrl->DebitNotes->find()->where(['tipo_comp' => $comprobante->tipo_comp, 'business_id' => $comprobante->business_id])->order(['num' => 'DESC'])->first();
                if ($comp) {
                    return $comp->num + 1;
                } else {
                    return 1;
                }
                break;

            case 'XRX':
            case 'FRX':

                $comp = $this->_ctrl->Receipts->find()->where(['tipo_comp' => $comprobante->tipo_comp, 'business_id' => $comprobante->business_id])->order(['num' => 'DESC'])->first();
                if ($comp) {
                    return $comp->num + 1;
                } else {
                    return 1;
                }
                break;
        }
    }

    public function receipt(&$payment, $calulate_saldo = TRUE)
    {
        $this->_ctrl->loadModel('Customers');
        $this->_ctrl->loadModel('Payments');

        $paraments = $this->_ctrl->request->getSession()->read('paraments');
        $payment_getway = $this->_ctrl->request->getSession()->read('payment_getway');

        $customer = $this->_ctrl->Customers->get($payment->customer_code, ['contain' => ['Cities']]);

        $receipt = $this->_ctrl->Receipts->newEntity();
        $receipt->date = $payment->created;

        $receipt->business_id = null;
        foreach ($paraments->invoicing->business as $b) {
            if ($b->id == $customer->business_billing) {
                 $receipt->business = $b;
                 $receipt->business_id = $b->id;
            }
        }

        if ($customer->is_presupuesto) {

            //presupuesto
            $receipt->tipo_comp = 'XRX';
            $receipt->num = $this->getLastNumCompByType($receipt);

        } else {

            //factura
            $receipt->tipo_comp = 'FRX';
            $receipt->num = $this->getLastNumCompByType($receipt);
        }

        $receipt->pto_vta = intval($receipt->business->pto_vta);

        $receipt->date = $payment->created;
        $receipt->date_start = $payment->created;
        $receipt->date_end =$payment->created;
        $receipt->duedate = $payment->created;

        $receipt->concept_type = 3;

        $receipt->concept = $payment->concept;
        $receipt->moves = $payment->moves;
        $receipt->observations = $payment->observations;

        $receipt->total = $payment->import;

        $receipt->company_name = $receipt->business->name;
        $receipt->company_address = $receipt->business->address;
        $receipt->company_cp = $receipt->business->cp;
        $receipt->company_city = $receipt->business->city;
        $receipt->company_phone = $receipt->business->phone;
        $receipt->company_ident = $receipt->business->cuit;
        $receipt->company_ing_bruto = $receipt->business->ing_bruto;
        $receipt->company_init_act = $receipt->business->init_act;
        $receipt->company_responsible = $receipt->business->responsible;

        $receipt->customer_code = $customer->code;

        $receipt->customer_name = $customer->name;
        $receipt->customer_address = $customer->address;
        $receipt->customer_cp = $customer->city->cp;
        $receipt->customer_city = $customer->city->name;
        $receipt->customer_country = $this->_ctrl->cod_pais[200];
        $receipt->customer_doc_type = $customer->doc_type;
        $receipt->customer_ident = $customer->ident;

        $receipt->customer_responsible = $customer->responsible;

        if ($payment->account_id != '') {

            switch ($payment->payment_method_id) {

                case 7:
                    // tarjeta de payu
                    $account = NULL;
                    foreach ($payment_getway->methods as $key => $pg) {

                        if ($pg->id == $payment->payment_method_id) {

                            $config = $pg->config;
                            $model = $payment_getway->config->$config->model;
                            $this->_ctrl->loadModel($model);
                            $account = $this->_ctrl->$model
                                ->find()
                                ->where([
                                    'id'                => $payment->account_id,
                                    'payment_getway_id' => $payment->payment_method_id
                                ])->first();
                            break;
                        }
                    }

                    if ($account) {
                        $receipt->barcode_alt = $account->barcode;
                        $receipt->id_comercio = $account->id_comercio;
                    }
                    break;

                case 99:
                    // tarjeta de cobranza cobrodigital
                    $account = NULL;
                    foreach ($payment_getway->methods as $key => $pg) {

                        if ($pg->id == $payment->payment_method_id) {

                            $config = $pg->config;
                            $model = $payment_getway->config->$config->model;
                            $this->_ctrl->loadModel($model);
                            $account = $this->_ctrl->$model
                                ->find()
                                ->where([
                                    'id'                => $payment->account_id,
                                    'payment_getway_id' => $payment->payment_method_id
                                ])->first();
                            break;
                        }
                    }

                    if ($account) {
                        $receipt->electronic_code = $account->electronic_code;
                        $receipt->barcode_alt = $account->barcode;
                        $receipt->id_comercio = $account->id_comercio;
                    }
                    break;

                case 102:
                    // debito automatico chubut
                    $account = NULL;
                    foreach ($payment_getway->methods as $key => $pg) {

                        if ($pg->id == $payment->payment_method_id) {

                            $config = $pg->config;
                            $model = $payment_getway->config->$config->model;
                            $this->_ctrl->loadModel($model);
                            $account = $this->_ctrl->$model
                                ->find()
                                ->where([
                                    'id'                => $payment->account_id,
                                    'payment_getway_id' => $payment->payment_method_id
                                ])->first();
                            break;
                        }
                    }

                    if ($account) {
                        $receipt->firstname = $account->firstname;
                        $receipt->lastname = $account->lastname;
                        $receipt->cuit = $account->cuit;
                        $receipt->cbu = $account->cbu;
                    }
                    break;

                case 104:
                    // debito automatico cobrodigital
                    $account = NULL;
                    foreach ($payment_getway->methods as $key => $pg) {

                        if ($pg->id == $payment->payment_method_id) {

                            $config = $pg->config;
                            $model = $payment_getway->config->$config->model;
                            $this->_ctrl->loadModel($model);
                            $account = $this->_ctrl->$model
                                ->find()
                                ->where([
                                    'id'                => $payment->account_id,
                                    'payment_getway_id' => $payment->payment_method_id
                                ])->first();
                            break;
                        }
                    }

                    if ($account) {
                        $receipt->firstname = $account->firstname;
                        $receipt->lastname = $account->lastname;
                        $receipt->cuit = $account->cuit;
                        $receipt->cbu = $account->cbu;
                        $receipt->email = $account->email;
                        $receipt->id_comercio = $account->id_comercio;
                    }
                    break;

                case 105:
                    // cuenta digital
                    $account = NULL;
                    foreach ($payment_getway->methods as $key => $pg) {

                        if ($pg->id == $payment->payment_method_id) {

                            $config = $pg->config;
                            $model = $payment_getway->config->$config->model;
                            $this->_ctrl->loadModel($model);
                            $account = $this->_ctrl->$model
                                ->find()
                                ->where([
                                    'id'                => $payment->account_id,
                                    'payment_getway_id' => $payment->payment_method_id
                                ])->first();
                            break;
                        }
                    }

                    if ($account) {
                        $receipt->barcode_alt = $account->barcode;
                    }
                    break;

                case 106:
                    // visa debito automativo
                    $account = NULL;
                    foreach ($payment_getway->methods as $key => $pg) {

                        if ($pg->id == $payment->payment_method_id) {

                            $config = $pg->config;
                            $model = $payment_getway->config->$config->model;
                            $this->_ctrl->loadModel($model);
                            $account = $this->_ctrl->$model
                                ->find()
                                ->where([
                                    'id'                => $payment->account_id,
                                    'payment_getway_id' => $payment->payment_method_id
                                ])->first();
                            break;
                        }
                    }

                    if ($account) {
                        $receipt->barcode_alt = $account->card_number;
                    }
                    break;

                case 107:
                    // mastercard debito automatico
                    $account = NULL;
                    foreach ($payment_getway->methods as $key => $pg) {

                        if ($pg->id == $payment->payment_method_id) {

                            $config = $pg->config;
                            $model = $payment_getway->config->$config->model;
                            $this->_ctrl->loadModel($model);
                            $account = $this->_ctrl->$model
                                ->find()
                                ->where([
                                    'id'                => $payment->account_id,
                                    'payment_getway_id' => $payment->payment_method_id
                                ])->first();
                            break;
                        }
                    }

                    if ($account) {
                        $receipt->barcode_alt = $account->card_number;
                    }
                    break;
            }
        }

        foreach ($payment_getway->methods as $key => $pg) {
            if ($pg->id == $payment->payment_method_id) {
                $receipt->payment_method = $pg->name;
            }
        }

        $receipt->comments = $payment->comments;
        $receipt->user_id = $payment->user_id;

        $receipt->connection_id = $payment->connection_id;

        if (!$this->_ctrl->Receipts->save($receipt)) {

            $data_error = new \stdClass;
            $data_error->errors = $receipt->getErrors();

            $event = new Event('FiscoAfipComp.Error', $this, [
            	'msg' => __('Hubo un error al intentar crear el recibo.'),
            	'data' => $data_error,
            	'flash' => true
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);

            return false;
        }

        $payment->receipt_id = $receipt->id;

        if (!$this->_ctrl->Payments->save($payment)) {

            $data_error = new \stdClass;
            $data_error->errors = $payment->getErrors();

            $event = new Event('FiscoAfipComp.Error', $this, [
            	'msg' => __('Hubo un error al intentar actualizar el pago.'),
            	'data' => $data_error,
            	'flash' => true
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);

            return false;
        }

        if (isset($payment->from_cobranza)) {

            if ($payment->number_transaction
                && in_array($payment->payment_method_id, [7, 99, 101, 104, 105])) {

                switch ($payment->payment_method_id) {

                    case 7:
                        // tarjeta de payu
                        $model_transaction = $payment_getway->config->payu->transaction;
                        $this->_ctrl->loadModel($model_transaction);
                        $transaction = $this->_ctrl->$model_transaction->newEntity();
                        $transaction->fecha_pago         = $payment->created;
                        $transaction->estado             = 1;
                        $transaction->customer_code      = $payment->customer_code;
                        $transaction->id_orden           = $payment->number_transaction;
                        $transaction->receipt_id         = $payment->receipt_id;
                        $transaction->payment_getway_id  = $payment->payment_method_id;
                        $transaction->receipt_number     = $receipt->num;
                        $transaction->valor              = $receipt->total;
                        $transaction->concepto           = "Transacción cargada desde Cobranza";
                        $transaction->comments           = "Transacción cargada desde Cobranza";
                        $this->_ctrl->$model_transaction->save($transaction);
                        break;

                    case 99:
                    case 104:
                        // tarjeta de cobranza cobrodigital
                        $model_transaction = $payment_getway->config->cobrodigital->transaction;
                        $this->_ctrl->loadModel($model_transaction);
                        $transaction = $this->_ctrl->$model_transaction->newEntity();
                        $transaction->fecha              = $payment->created;
                        $transaction->estado             = 1;
                        $transaction->customer_code      = $payment->customer_code;
                        $transaction->id_transaccion     = $payment->number_transaction;
                        $transaction->receipt_id         = $payment->receipt_id;
                        $transaction->payment_getway_id  = $payment->payment_method_id;
                        $transaction->receipt_number     = $receipt->num;
                        $transaction->bruto              = $receipt->total;
                        $transaction->concepto           = "Transacción cargada desde Cobranza";
                        $transaction->comments           = "Transacción cargada desde Cobranza";
                        $this->_ctrl->$model_transaction->save($transaction);
                        break;

                    case 101:
                        // mercadopago
                        $model_transaction = $payment_getway->config->mercadopago->transaction;
                        $this->_ctrl->loadModel($model_transaction);
                        $transaction = $this->_ctrl->$model_transaction->newEntity();
                        $transaction->date_created       = $payment->created;
                        $transaction->status             = 'approved';
                        $transaction->local_status       = 1;
                        $transaction->customer_code      = $payment->customer_code;
                        $transaction->payment_id         = $payment->number_transaction;
                        $transaction->receipt_id         = $payment->receipt_id;
                        $transaction->payment_getway_id  = $payment->payment_method_id;
                        $transaction->receipt_number     = $receipt->num;
                        $transaction->total_paid_amount  = $receipt->total;
                        $transaction->info               = "Transacción cargada desde Cobranza";
                        $transaction->comment            = "Transacción cargada desde Cobranza";
                        $this->_ctrl->$model_transaction->save($transaction);
                        break;

                    case 105:
                        // cuenta digital
                        $model_transaction = $payment_getway->config->cuentadigital->transaction;
                        $this->_ctrl->loadModel($model_transaction);
                        $transaction = $this->_ctrl->$model_transaction->newEntity();
                        $transaction->fecha              = $payment->created;
                        $transaction->estado             = 1;
                        $transaction->customer_code      = $payment->customer_code;
                        $transaction->id_transaccion     = $payment->number_transaction;
                        $transaction->receipt_id         = $payment->receipt_id;
                        $transaction->payment_getway_id  = $payment->payment_method_id;
                        $transaction->receipt_number     = $receipt->num;
                        $transaction->neto_original      = $receipt->total;
                        $transaction->info               = "Transacción cargada desde Cobranza";
                        $transaction->comentario         = "Transacción cargada desde Cobranza";
                        $this->_ctrl->$model_transaction->save($transaction);
                        break;
                }
            }
        }

        //log de accion

        $afip_codes = $this->_ctrl->request->getSession()->read('afip_codes');

        $action = 'Cobranza';

        $detail =  'Tipo de Comp.: ' . $afip_codes['comprobantesLetter'][$receipt->tipo_comp] . PHP_EOL;
        $detail .= 'Fecha: ' . $receipt->date->format('d/m/Y') . PHP_EOL;
        $detail .= 'Número: ' . sprintf("%'.04d", $receipt->pto_vta) . '-' . sprintf("%'.08d", $receipt->num) . PHP_EOL;
        $detail .= 'Total: ' . number_format($receipt->total, 2, ',', '.') . PHP_EOL;

        $this->_ctrl->registerActivity($action, $detail, $receipt->customer_code, TRUE);

        if ($calulate_saldo) {

            $this->CurrentAccount->updateDebtMonth($payment->customer_code, $payment->connection_id);
        }

        return $receipt;
    }

    /**
     * [ customer, concept_type, date, date_start, date_end, duedate, debts, discounts,  tipo_comp, comments, 'manual' => false ]
    */
    public function invoice($data, $calulate_saldo = TRUE)
    {
        $this->_ctrl->loadModel('Debts');
        $this->_ctrl->loadModel('CustomersHasDiscounts');

        $paraments = $this->request->getSession()->read('paraments');
        $afip_codes = $this->request->getSession()->read('afip_codes');

        $customer = $data['customer'];

        $data['auto_debit'] = array_key_exists('auto_debit', $data) ? $data['auto_debit'] : false;

        $invoice = $this->_ctrl->Invoices->newEntity();
        $invoice = $this->_ctrl->Invoices->patchEntity($invoice, $data);

        if (!array_key_exists('business_id', $data)) {

            foreach ($paraments->invoicing->business as $b) {
                if ($b->id == $customer->business_billing) {
                     $invoice->business = $b;
                     $invoice->business_id = $b->id;
                }
            }
        } else {
            foreach ($paraments->invoicing->business as $b) {
                if ($b->id == $data['business_id']) {
                     $invoice->business = $b;
                     $invoice->business_id = $b->id;
                }
            }
        }

        $invoice->pto_vta = intval($invoice->business->pto_vta);

        if ($invoice->tipo_comp ==  'XXX') {

            $invoice->num = $this->getLastNumCompByType($invoice);
        }

        $invoice->comments = $data['comments'];

        $invoice->company_name = $invoice->business->name;
        $invoice->company_address = $invoice->business->address;
        $invoice->company_cp = $invoice->business->cp;
        $invoice->company_city = $invoice->business->city;
        $invoice->company_phone = $invoice->business->phone;
        $invoice->company_ident = $invoice->business->cuit;
        $invoice->company_ing_bruto = $invoice->business->ing_bruto;
        $invoice->company_init_act = $invoice->business->init_act;
        $invoice->company_responsible = $invoice->business->responsible;

        $invoice->customer_code = $customer->code;

        if (array_key_exists('connection_id', $data) && $data['connection_id']) {

            $this->_ctrl->loadModel('Connections');
            $connection = $this->_ctrl->Connections->get($data['connection_id']);
            $invoice->connection_id = $data['connection_id'];
            $invoice->customer_address = $connection->address;

        } else {
             $invoice->customer_address = $customer->address;
        }

        $invoice->customer_name = $customer->name;
        $invoice->customer_cp = $customer->city->cp;
        $invoice->customer_city = $customer->city->name;
        $invoice->customer_country = $this->_ctrl->cod_pais[200];

        if ($customer->responsible != 5 || $customer->denomination) {

            $invoice->customer_doc_type = $customer->doc_type;
            $invoice->customer_ident = $customer->ident;

        } else {

            $invoice->customer_doc_type = 99;
            $invoice->customer_ident = 0;
        }

        $invoice->customer_responsible = $customer->responsible;

        if (array_key_exists('cond_vta', $data) && $data['cond_vta']) {

            $invoice->cond_vta = $data['cond_vta'];

        } else {
             $invoice->cond_vta = $customer->cond_venta;
        }

        $invoice->user_id = $this->_ctrl->Auth->user()['id'];

        $invoice->subtotal  = 0;
        $invoice->sum_tax  = 0;
        $invoice->discount  = 0;
        $invoice->total  = 0;

        if (!$this->_ctrl->Invoices->save($invoice)) {

            $data_error = new \stdClass; 
            $data_error->errors = $invoice->getErrors();

            $event = new Event('FiscoAfipComp.Error', $this, [
            	'msg' => __('Hubo un error al intentar crear la factura.'),
            	'data' => $data_error,
            	'flash' => true
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);

            return false;
        }

        $invoice->alicIva = [];

        $invoice_comments = '';

        foreach ($data['debts'] as $debt) {

            //debts

            $conceptdb = $this->_ctrl->Concepts->newEntity();
            $conceptdb->type = $debt->type;
            $conceptdb->code = $debt->code;
            $conceptdb->description = $debt->description;

            $invoice_comments .= $debt->description . ';';
            $conceptdb->quantity = $debt->quantity;
            $conceptdb->unit = $debt->unit;
            $conceptdb->price = $debt->price;

            $conceptdb->discount = $debt->discount;
            $invoice->discount += $conceptdb->discount; 

            $conceptdb->tax = $debt->tax;

            $conceptdb->sum_price = $debt->sum_price;
            $invoice->subtotal += $conceptdb->sum_price;

            $conceptdb->sum_tax = $debt->sum_tax;
            $invoice->sum_tax += $conceptdb->sum_tax;

            if (!array_key_exists($conceptdb->tax,$invoice->alicIva)) {
                $invoice->alicIva[$conceptdb->tax] = ['Id' => $conceptdb->tax, 'BaseImp' => 0, 'Importe' => 0 ];
            }

            $invoice->alicIva[$conceptdb->tax]['BaseImp'] += $conceptdb->sum_price;
            $invoice->alicIva[$conceptdb->tax]['Importe'] += $conceptdb->sum_tax;

            $conceptdb->total = $debt->total;

            $invoice->total += $conceptdb->total;

            $conceptdb->comprobante_id = $invoice->id;

            if (!$data['manual']) {

                if (!$invoice->seating_number) {
                    $invoice->seating_number = $debt->seating_number;
                } else {
                    $invoice->seating_number .= ',' . $debt->seating_number;
                }
            }

            if (!$this->_ctrl->Concepts->save($conceptdb)) {

                $data_error = new \stdClass; 
                $data_error->conceptdb = $conceptdb;

                $event = new Event('FiscoAfipComp.Error', $this, [
                	'msg' => __('Hubo un error al intentar crear el concepto de la factura.'),
                	'data' => $data_error,
                	'flash' => false
                ]);

                $this->_ctrl->getEventManager()->dispatch($event);

                return false;

            } //en debts

            //discounts

            if (!$data['manual'] && $debt->customers_has_discount) {

                $conceptdb = $this->_ctrl->Concepts->newEntity();
                $conceptdb->type = 'S';
                $conceptdb->code = $debt->customers_has_discount->code;
                $conceptdb->description = $debt->customers_has_discount->description;
                $conceptdb->quantity = 1;
                $conceptdb->unit = 7;
                $conceptdb->price = -$debt->customers_has_discount->price;

                $conceptdb->discount = 0;
                $invoice->discount += 0; 

                $conceptdb->tax = $debt->customers_has_discount->tax;

                $conceptdb->sum_price = -$debt->customers_has_discount->sum_price;
                $invoice->subtotal += $conceptdb->sum_price;

                $conceptdb->sum_tax = -$debt->customers_has_discount->sum_tax;
                $invoice->sum_tax += $conceptdb->sum_tax;

                if (!array_key_exists($conceptdb->tax,$invoice->alicIva)) {
                    $invoice->alicIva[$conceptdb->tax] = ['Id' => $conceptdb->tax, 'BaseImp' => 0, 'Importe' => 0 ];
                }

                $invoice->alicIva[$conceptdb->tax]['BaseImp'] += $conceptdb->sum_price;
                $invoice->alicIva[$conceptdb->tax]['Importe'] += $conceptdb->sum_tax;
                
                $conceptdb->total = -$debt->customers_has_discount->total;

                $invoice->total += $conceptdb->total;

                $conceptdb->comprobante_id = $invoice->id;

                if (!$data['manual']) {

                    if (!$invoice->seating_number) {
                        $invoice->seating_number = $debt->customers_has_discount->seating_number;
                    } else {
                        $invoice->seating_number .= ',' . $debt->customers_has_discount->seating_number;        
                    }
                }

                if (!$this->_ctrl->Concepts->save($conceptdb)) {

                    $data_error = new \stdClass; 
                    $data_error->conceptdb = $conceptdb;

                    $event = new Event('FiscoAfipComp.Error', $this, [
                    	'msg' => __('Hubo un error al intentar crear el concepto de la factura (Descuento).'),
                    	'data' => $data_error,
                    	'flash' => false
                    ]);

                    $this->_ctrl->getEventManager()->dispatch($event);

                    return false;
                }

            } //end discount

            if (!$data['manual']) { // si la factura no es de tipo manual, se deben relacionar las deudas a la factura

                $debt->invoice_id = $invoice->id;

                if (!$this->_ctrl->Debts->save($debt)) {

                    $data_error = new \stdClass; 
                    $data_error->errors = $debt->getErrors();

                    $event = new Event('FiscoAfipComp.Error', $this, [
                    	'msg' => __('Hubo un error al intentar actualizar las deudas.'),
                    	'data' => $data_error,
                    	'flash' => false
                    ]);

                    $this->_ctrl->getEventManager()->dispatch($event);

                    return false;
                }

                if ($debt->customers_has_discount) {

                    $debt->customers_has_discount->invoice_id = $invoice->id;

                    if (!$this->_ctrl->CustomersHasDiscounts->save($debt->customers_has_discount)) {

                        $data_error = new \stdClass; 
                        $data_error->errors = $debt->customers_has_discount->getErrors();

                        $event = new Event('FiscoAfipComp.Error', $this, [
                        	'msg' => __('Hubo un error al intentar actualizar los descuentos.'),
                        	'data' => $data_error,
                        	'flash' => false
                        ]);

                        $this->_ctrl->getEventManager()->dispatch($event);

                        return false;
                    }
                }
            }
            
        }

        foreach ($data['discounts'] as $discount) {
            //debts

            $conceptdb = $this->_ctrl->Concepts->newEntity();
            $conceptdb->type = '';
            $conceptdb->code = $discount->code;
            $conceptdb->description = $discount->description;

            $invoice_comments .= $discount->description . ';';

            $conceptdb->quantity = 1;
            $conceptdb->unit = 7;
            $conceptdb->price = -$discount->price;

            $conceptdb->discount = 0;

            $conceptdb->tax = $discount->tax;

            $conceptdb->sum_price = -$discount->sum_price;
            $invoice->subtotal += $conceptdb->sum_price;

            $conceptdb->sum_tax = -$discount->sum_tax;
            $invoice->sum_tax += $conceptdb->sum_tax;

            if (!array_key_exists($conceptdb->tax,$invoice->alicIva)) {
                $invoice->alicIva[$conceptdb->tax] = ['Id' => $conceptdb->tax, 'BaseImp' => 0, 'Importe' => 0 ];
            }

            $invoice->alicIva[$conceptdb->tax]['BaseImp'] += $conceptdb->sum_price;
            $invoice->alicIva[$conceptdb->tax]['Importe'] += $conceptdb->sum_tax;

            $conceptdb->total = -$discount->total;

            $invoice->total += $conceptdb->total;

            $conceptdb->comprobante_id = $invoice->id;

            if (!$data['manual']) {

                if (!$invoice->seating_number) {
                    $invoice->seating_number = $discount->seating_number;
                } else {
                    $invoice->seating_number .= ',' . $discount->seating_number;
                }
            }

            if (!$this->_ctrl->Concepts->save($conceptdb)) {

                $data_error = new \stdClass; 
                $data_error->conceptdb = $conceptdb;

                $event = new Event('FiscoAfipComp.Error', $this, [
                	'msg' => __('Hubo un error al intentar crear el concepto de la factura.'),
                	'data' => $data_error,
                	'flash' => false
                ]);

                $this->_ctrl->getEventManager()->dispatch($event);

                return false;

            } //en debts

            if (!$data['manual']) { // si la factura no es de tipo manual, se deben relacionar las deudas a la factura

                $discount->invoice_id = $invoice->id;

                if (!$this->_ctrl->CustomersHasDiscounts->save($discount)) {

                    $data_error = new \stdClass; 
                    $data_error->errors = $discount->getErrors();

                    $event = new Event('FiscoAfipComp.Error', $this, [
                    	'msg' => __('Hubo un error al intentar actualizar el registro del descuento.'),
                    	'data' => $data_error,
                    	'flash' => false
                    ]);

                    $this->_ctrl->getEventManager()->dispatch($event);

                    return false;
                }
            }
            
        }

        if ($invoice->total < 0 ) {

            $data_error = new \stdClass; 
            $data_error->invoice = $invoice;

            $event = new Event('FiscoAfipComp.Error', $this, [
            	'msg' => __('La suma de los conceptos da como resultado un valor negativo.'),
            	'data' => $data_error,
            	'flash' => true
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);

            return false;
        }

        if ($invoice->total == 0) {

            $invoice->alicIva = [];
        }

        if ($invoice->tipo_comp == '011') {

            /*ajuste por si la deuda se gemero con alicuota, pero leugo se facturo como C*/
            $invoice->subtotal = $invoice->total;
            $invoice->sum_tax = 0;
            $invoice->alicIva = [];
        }

        $invoice->resto = $invoice->total;

        if ($invoice->comments == '') {
            $invoice->comments = $invoice_comments;
        }

        if (!$this->_ctrl->Invoices->save($invoice)) {

            $data_error = new \stdClass; 
            $data_error->invoice = $invoice;

            $event = new Event('FiscoAfipComp.Error', $this, [
            	'msg' => __('Hubo un error al intentar actualizar la factura.'),
            	'data' => $data_error,
            	'flash' => false
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);

            return false;
        }

        //log de accion

        if ($data['manual']) {
            $action =  'Factura Manual';
        } else {
            $action =  'Factura de Deudas';
        }

        $afip_codes = $this->_ctrl->request->getSession()->read('afip_codes');

        $detail =  'Tipo de Comp.: ' . $afip_codes['comprobantesLetter'][$invoice->tipo_comp] . PHP_EOL;
        $detail .= 'Fecha: ' . $invoice->date->format('d/m/Y') . PHP_EOL;
        $detail .= 'Vencimiento: '. $invoice->duedate->format('d/m/Y') . PHP_EOL;
        $detail .= 'Número: ' . sprintf("%'.04d", $invoice->pto_vta) . '-' . sprintf("%'.08d", $invoice->num) . PHP_EOL;
        $detail .= 'Total: ' . number_format($invoice->total, 2, ',', '.') . PHP_EOL;
        $this->_ctrl->registerActivity($action, $detail, $invoice->customer_code, TRUE);

        if ($calulate_saldo) {
            $this->CurrentAccount->updateDebtMonth($invoice->customer_code, $invoice->connection_id);
        }

        return $invoice;
    }

    /**
     * [ customer, concept_type, date, date_start, date_end, duedate, concepts, tipo_comp, comments, 'manual' => false ]
    */
    public function debitNote($data, $calulate_saldo = TRUE)
    {
        $paraments = $this->request->getSession()->read('paraments');
        $afip_codes = $this->request->getSession()->read('afip_codes');

        $customer = $data['customer'];

        $debitNote = $this->_ctrl->DebitNotes->newEntity();
        $debitNote = $this->_ctrl->DebitNotes->patchEntity($debitNote, $data);

        if (!array_key_exists('business_id', $data)) {
            
            foreach ($paraments->invoicing->business as $b) {
                if ($b->id == $customer->business_billing) {
                     $debitNote->business = $b;
                     $debitNote->business_id = $b->id;
                }
            }
        } else {
            foreach ($paraments->invoicing->business as $b) {
                if ($b->id == $data['business_id']) {
                     $debitNote->business = $b;
                     $debitNote->business_id = $b->id;
                }
            }
        }

        $debitNote->pto_vta = intval($debitNote->business->pto_vta);

        if ($debitNote->tipo_comp ==  'NDX') {
            $debitNote->num = $this->getLastNumCompByType($debitNote);
        }
        
        $debitNote->comments = $data['comments'];

        $debitNote->company_name = $debitNote->business->name;
        $debitNote->company_address = $debitNote->business->address;
        $debitNote->company_cp = $debitNote->business->cp;
        $debitNote->company_city = $debitNote->business->city;
        $debitNote->company_phone = $debitNote->business->phone;
        $debitNote->company_ident = $debitNote->business->cuit;
        $debitNote->company_ing_bruto = $debitNote->business->ing_bruto;
        $debitNote->company_init_act = $debitNote->business->init_act;
        $debitNote->company_responsible = $debitNote->business->responsible;

        if (array_key_exists('connection_id', $data) && $data['connection_id']) {

            $this->_ctrl->loadModel('Connections');
            $connection = $this->_ctrl->Connections->get($data['connection_id']);
            $debitNote->customer_address = $connection->address;

        } else {
             $debitNote->customer_address = $customer->address;
        }

        $debitNote->customer_code = $customer->code;
        $debitNote->customer_name = $customer->name;
        $debitNote->customer_cp = $customer->city->cp;
        $debitNote->customer_city = $customer->city->name;
        $debitNote->customer_country = $this->_ctrl->cod_pais[200];

        if ($customer->responsible != 5 || $customer->denomination) {

            $debitNote->customer_doc_type = $customer->doc_type;
            $debitNote->customer_ident = $customer->ident;

        } else {

            $debitNote->customer_doc_type = 99;
            $debitNote->customer_ident = 0;
        }

        $debitNote->customer_responsible = $customer->responsible;

        if (array_key_exists('cond_vta', $data) && $data['cond_vta']) {

            $debitNote->cond_vta = $data['cond_vta'];

        } else {
             $debitNote->cond_vta = $customer->cond_venta;
        }

        if (!isset($this->_ctrl->request->getSession()->read('Auth.User')['id'])) {
            $debitNote->user_id = 100;
        } else {
            $debitNote->user_id= $this->_ctrl->request->getSession()->read('Auth.User')['id'];
        }

        $debitNote->subtotal  = 0;
        $debitNote->sum_tax  = 0;
        $debitNote->discount = 0;
        $debitNote->total  = 0;

        $debitNote->seating_number = NULL;

        if (!$this->_ctrl->DebitNotes->save($debitNote)) {

            $data_error = new \stdClass; 
            $data_error->errors = $debitNote->getErrors();

            $event = new Event('FiscoAfipComp.Error', $this, [
            	'msg' => __('Hubo un error al intentar crear la nota de débito.'),
            	'data' => $data_error,
            	'flash' => false
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);
            return false;
        }

        $debitNote->alicIva = [];

        $debitNote_comments = '';

        foreach ($data['concepts'] as $concept) {

            $conceptdb = $this->_ctrl->ConceptsDebit->newEntity();
            $conceptdb->type = $concept->type;
            $conceptdb->code = $concept->code;
            $conceptdb->description = $concept->description;

            $debitNote_comments .= $conceptdb->description . ';';

            $conceptdb->quantity = $concept->quantity;
            $conceptdb->unit = $concept->unit;
            $conceptdb->price = $concept->price;

            $conceptdb->discount = $concept->discount;
            $debitNote->discount += $conceptdb->discount;

            $conceptdb->tax = $concept->tax;

            $conceptdb->sum_price = $concept->sum_price;
            $debitNote->subtotal += $conceptdb->sum_price;

            $conceptdb->sum_tax = $concept->sum_tax;
            $debitNote->sum_tax += $conceptdb->sum_tax;

            if (!array_key_exists($conceptdb->tax, $debitNote->alicIva)) {
                $debitNote->alicIva[$conceptdb->tax] = ['Id' => $conceptdb->tax, 'BaseImp' => 0, 'Importe' => 0 ];
            }

            $debitNote->alicIva[$conceptdb->tax]['BaseImp'] += $conceptdb->sum_price;
            $debitNote->alicIva[$conceptdb->tax]['Importe'] += $conceptdb->sum_tax;

            $conceptdb->total = $concept->total;
            $debitNote->total += $conceptdb->total;

            $conceptdb->debit_note_id = $debitNote->id;

            if (!$this->_ctrl->ConceptsDebit->save($conceptdb)) {

                $data_error = new \stdClass; 
                $data_error->errors = $conceptdb->getErrors();

                $event = new Event('FiscoAfipComp.Error', $this, [
                	'msg' => __('Hubo un error al intentar crear los conceptos de la nota de débito.'),
                	'data' => $data_error,
                	'flash' => false
                ]);

                $this->_ctrl->getEventManager()->dispatch($event);
                return false;
            }
        }

        $debitNote->resto = $debitNote->total;

        if ($debitNote->comments == '') {
            $debitNote->comments = $debitNote_comments;
        }

        if (!$this->_ctrl->DebitNotes->save($debitNote)) {

            $data_error = new \stdClass; 
            $data_error->errors = $debitNote->getErrors();

            $event = new Event('FiscoAfipComp.Error', $this, [
            	'msg' => __('Hubo un error al intentar actualizar la nota de débito.'),
            	'data' => $data_error,
            	'flash' => false
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);

            return false;
        }

        //log de accion

        if ($data['manual']) {
            $action = 'Nota de Débito Manual';
        } else {
            $action = 'Nota de Débito Autom.';
        }

        $afip_codes = $this->_ctrl->request->getSession()->read('afip_codes');

        $detail =  'Tipo de Comp.: '. $afip_codes['comprobantesLetter'][$debitNote->tipo_comp] . PHP_EOL;
        $detail .= 'Fecha: '. $debitNote->date->format('d/m/Y') . PHP_EOL;

        $detail .= 'Número: ' . sprintf("%'.04d", $debitNote->pto_vta) . '-' . sprintf("%'.08d", $debitNote->num) . PHP_EOL;
        $detail .= 'Total: ' . number_format($debitNote->total, 2, ',', '.') . PHP_EOL;
        if ($debitNote->invoice) {
            $detail .= 'Factura Asociada: '. $afip_codes['comprobantesLetter'][$debitNote->invoice->tipo_comp] . ' ' . sprintf("%'.04d", $debitNote->invoice->pto_vta) . '-' . sprintf("%'.08d", $debitNote->invoice->num) . PHP_EOL;
        }
        $this->_ctrl->registerActivity($action, $detail, $debitNote->customer_code, TRUE);

        if ($debitNote->invoice) {

            $debitNote->invoice = $this->_ctrl->Invoices->get($debitNote->invoice->id);
        }

        if ($calulate_saldo) {
            $this->CurrentAccount->updateDebtMonth($debitNote->customer_code, $debitNote->connection_id);
        }

        return $debitNote;
    }

    /**
     * [ customer, concept_type, date, date_start, date_end, duedate, concepts, tipo_comp, comments, 'manual' => false, invoice, credito_pago_cuenta ]
    */
    public function creditNote($data, $calulate_saldo = TRUE)
    {
        $paraments = $this->request->getSession()->read('paraments');
        $afip_codes = $this->request->getSession()->read('afip_codes');

        $customer = $data['customer'];

        $creditNote = $this->_ctrl->CreditNotes->newEntity();
        $creditNote = $this->_ctrl->CreditNotes->patchEntity($creditNote, $data);

        if (!array_key_exists('business_id', $data)){

            foreach ($paraments->invoicing->business as $b) {
                if ($b->id == $customer->business_billing) {
                     $creditNote->business = $b;
                     $creditNote->business_id = $b->id;
                }
            }
        } else {
            foreach ($paraments->invoicing->business as $b) {
                if ($b->id == $data['business_id']) {
                     $creditNote->business = $b;
                     $creditNote->business_id = $b->id;
                }
            }
        }

        $creditNote->pto_vta = intval($creditNote->business->pto_vta);

        if ($creditNote->tipo_comp ==  'NCX') {
            $creditNote->num = $this->getLastNumCompByType($creditNote);
        }

        $creditNote->comments = $data['comments'];

        $creditNote->company_name = $creditNote->business->name;
        $creditNote->company_address = $creditNote->business->address;
        $creditNote->company_cp = $creditNote->business->cp;
        $creditNote->company_city = $creditNote->business->city;
        $creditNote->company_phone = $creditNote->business->phone;
        $creditNote->company_ident = $creditNote->business->cuit;
        $creditNote->company_ing_bruto = $creditNote->business->ing_bruto;
        $creditNote->company_init_act = $creditNote->business->init_act;
        $creditNote->company_responsible = $creditNote->business->responsible;

        if (array_key_exists('connection_id', $data) && $data['connection_id']) {

            $this->_ctrl->loadModel('Connections');
            $connection = $this->_ctrl->Connections->get($data['connection_id']);
            $creditNote->customer_address = $connection->address;

        } else {
             $creditNote->customer_address = $customer->address;
        }

        $creditNote->customer_code = $customer->code;
        $creditNote->customer_name = $customer->name;
        $creditNote->customer_cp = $customer->city->cp;
        $creditNote->customer_city = $customer->city->name;
        $creditNote->customer_country = $this->_ctrl->cod_pais[200];

        if ($customer->responsible != 5 || $customer->denomination) {

            $creditNote->customer_doc_type = $customer->doc_type;
            $creditNote->customer_ident = $customer->ident;

        } else {

            $creditNote->customer_doc_type = 99;
            $creditNote->customer_ident = 0;
        }

        $creditNote->customer_responsible = $customer->responsible;

        if (array_key_exists('cond_vta', $data) && $data['cond_vta']) {

            $creditNote->cond_vta = $data['cond_vta'];

        } else {
             $creditNote->cond_vta = $customer->cond_venta;
        }

        $creditNote->user_id = $this->_ctrl->Auth->user()['id'];

        $creditNote->subtotal  = 0;
        $creditNote->sum_tax  = 0;
        $creditNote->discount  = 0;
        $creditNote->total  = 0;

        $creditNote->seating_number = NULL;

        if (!$this->_ctrl->CreditNotes->save($creditNote)) {

            $data_error = new \stdClass; 
            $data_error->errors = $creditNote->getErrors();

            $event = new Event('FiscoAfipComp.Error', $this, [
            	'msg' => __('Hubo un error al intentar crear la nota de crédito.'),
            	'data' => $data_error,
            	'flash' => false
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);

            return false;
        }

        $creditNote->alicIva = [];

        $creditNote_comments = '';

        foreach ($data['concepts'] as $concept) {

            $conceptdb = $this->_ctrl->ConceptsCredit->newEntity();
            $conceptdb->type = $concept->type;
            $conceptdb->code = $concept->code;
            $conceptdb->description = $concept->description;

            $creditNote_comments .= $conceptdb->description . ';';

            $conceptdb->quantity = $concept->quantity;
            $conceptdb->unit = $concept->unit;
            $conceptdb->price = $concept->price;

            $conceptdb->discount = $concept->discount;
            $creditNote->discount += $conceptdb->discount; 

            $conceptdb->tax = $concept->tax;

            $conceptdb->sum_price = $concept->sum_price;
            $creditNote->subtotal += $conceptdb->sum_price;

            $conceptdb->sum_tax = $concept->sum_tax;
            $creditNote->sum_tax += $conceptdb->sum_tax;

            if (!array_key_exists($conceptdb->tax, $creditNote->alicIva)) {
                $creditNote->alicIva[$conceptdb->tax] = ['Id' => $conceptdb->tax, 'BaseImp' => 0, 'Importe' => 0 ];
            }

            $creditNote->alicIva[$conceptdb->tax]['BaseImp'] += $conceptdb->sum_price;
            $creditNote->alicIva[$conceptdb->tax]['Importe'] += $conceptdb->sum_tax;

            $conceptdb->total = $concept->total;
            $creditNote->total += $conceptdb->total;

            $conceptdb->credit_note_id = $creditNote->id;

            if (!$this->_ctrl->ConceptsCredit->save($conceptdb)) {

                $data_error = new \stdClass; 
                $data_error->errors = $conceptdb->getErrors();

                $event = new Event('FiscoAfipComp.Error', $this, [
                	'msg' => __('Hubo un error al intentar crear el concepto de la nota de crédito.'),
                	'data' => $data_error,
                	'flash' => false
                ]);

                $this->_ctrl->getEventManager()->dispatch($event);

                return false;
            }
        }

        $creditNote->resto = $creditNote->total;

        if ($creditNote->comments == '') {
            $creditNote->comments = $creditNote_comments;
        }

        if (!$this->_ctrl->CreditNotes->save($creditNote)) {

            $data_error = new \stdClass; 
            $data_error->errors = $debitNote->getErrors();

            $event = new Event('FiscoAfipComp.Error', $this, [
            	'msg' => __('Hubo un error al intentar actualizar la nota de crédito.'),
            	'data' => $data_error,
            	'flash' => false
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);

            return false;
        }

        if ($data['manual']) {
            $action = 'Nota de Crédito Manual';
        } else {
            $action = 'Nota de Crédito Autom.';
        }

        $afip_codes = $this->_ctrl->request->getSession()->read('afip_codes');

        $detail =  'Tipo de Comp.: ' . $afip_codes['comprobantesLetter'][$creditNote->tipo_comp] . PHP_EOL;
        $detail .= 'Fecha: ' . $creditNote->date->format('d/m/Y') . PHP_EOL;
        $detail .= 'Número: ' . sprintf("%'.04d", $creditNote->pto_vta) . '-' . sprintf("%'.08d", $creditNote->num) . PHP_EOL;
        $detail .= 'Total: ' . number_format($creditNote->total, 2, ',', '.') . PHP_EOL;
        if ($creditNote->invoice) {
            $detail .= 'Factura Asociada: '. $afip_codes['comprobantesLetter'][$creditNote->invoice->tipo_comp] . ' ' . sprintf("%'.04d", $creditNote->invoice->pto_vta) . '-' . sprintf("%'.08d", $creditNote->invoice->num) . PHP_EOL;
        }
        $this->_ctrl->registerActivity($action, $detail, $creditNote->customer_code, TRUE);

        //si se asocio a una factura directamente

        if ($creditNote->invoice) {

            $creditNote->invoice = $this->_ctrl->Invoices->get($creditNote->invoice->id);
        } 

        //luego si al credito se quedo saldo sigo asociando miestras existan factuiras con saldo

        if ($calulate_saldo) {
            $this->CurrentAccount->updateDebtMonth($creditNote->customer_code, $creditNote->connection_id);
        }

        return $creditNote;
    }

   /**
    * [ customer, concept_type, date, date_start, date_end, duedate, debts, discounts,  tipo_comp, comments, 'manual' => false ]
    */
    public function presupuesto($data)
    {
        $paraments = $this->request->getSession()->read('paraments');
        $afip_codes = $this->request->getSession()->read('afip_codes');

        $customer = $data['customer'];

        $presupuesto = $this->_ctrl->Presupuestos->newEntity();
        $presupuesto = $this->_ctrl->Presupuestos->patchEntity($presupuesto, $data);

        if (!array_key_exists('business_id', $data)) {

            foreach ($paraments->invoicing->business as $b) {
                if ($b->id == $customer->business_billing) {
                     $presupuesto->business = $b;
                     $presupuesto->business_id = $b->id;
                }
            }
        } else {
            foreach ($paraments->invoicing->business as $b) {
                if ($b->id == $data['business_id']) {
                     $presupuesto->business = $b;
                     $presupuesto->business_id = $b->id;
                }
            }
        }

        $presupuesto->pto_vta = intval($presupuesto->business->pto_vta);
        $presupuesto->num = $this->getLastNumCompByType($presupuesto);

        $presupuesto->comments = $data['comments'];

        $presupuesto->company_name = $presupuesto->business->name;
        $presupuesto->company_address = $presupuesto->business->address;
        $presupuesto->company_cp = $presupuesto->business->cp;
        $presupuesto->company_city = $presupuesto->business->city;
        $presupuesto->company_phone = $presupuesto->business->phone;
        $presupuesto->company_ident = $presupuesto->business->cuit;
        $presupuesto->company_ing_bruto = $presupuesto->business->ing_bruto;
        $presupuesto->company_init_act = $presupuesto->business->init_act;
        $presupuesto->company_responsible = $presupuesto->business->responsible;

        $presupuesto->customer_code = $customer->code;

        if (array_key_exists('connection_id', $data) && $data['connection_id']) {

            $this->_ctrl->loadModel('Connections');
            $connection = $this->_ctrl->Connections->get($data['connection_id']);
            $presupuesto->connection_id = $data['connection_id'];
            $presupuesto->customer_address = $connection->address;

        } else {
             $presupuesto->customer_address = $customer->address;
        }

        $presupuesto->customer_name = $customer->name;
        $presupuesto->customer_cp = $customer->city->cp;
        $presupuesto->customer_city = $customer->city->name;
        $presupuesto->customer_country = $this->_ctrl->cod_pais[200];

        if ($customer->responsible != 5 || $customer->denomination) {

            $presupuesto->customer_doc_type = $customer->doc_type;
            $presupuesto->customer_ident = $customer->ident;

        } else {

            $presupuesto->customer_doc_type = 99;
            $presupuesto->customer_ident = 0;
        }

        $presupuesto->customer_responsible = $customer->responsible;

        if (array_key_exists('cond_vta', $data) && $data['cond_vta']) {

            $presupuesto->cond_vta = $data['cond_vta'];

        } else {
             $presupuesto->cond_vta = $customer->cond_venta;
        }

        $presupuesto->user_id = $this->_ctrl->Auth->user()['id'];

        $presupuesto->subtotal  = 0;
        $presupuesto->sum_tax  = 0;
        $presupuesto->discount  = 0;
        $presupuesto->total  = 0;

        if (!$this->_ctrl->Presupuestos->save($presupuesto)) {

            $data_error = new \stdClass; 
            $data_error->errors = $presupuesto->getErrors();

            $event = new Event('FiscoAfipComp.Error', $this, [
            	'msg' => __('Hubo un error al intentar crear el presupuesto.'),
            	'data' => $data_error,
            	'flash' => true
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);

            return false;
        }

        $presupuesto->alicIva = [];

        $presupuesto_comments = '';

        foreach ($data['debts'] as $debt) {

            //debts

            $conceptdb = $this->_ctrl->Concepts->newEntity();
            $conceptdb->type = $debt->type;
            $conceptdb->code = $debt->code;
            $conceptdb->description = $debt->description;

            $presupuesto_comments .= $debt->description . ';';

            $conceptdb->quantity = $debt->quantity;
            $conceptdb->unit = $debt->unit;
            $conceptdb->price = $debt->price;

            $conceptdb->discount = $debt->discount;
            $presupuesto->discount += $conceptdb->discount; 

            $conceptdb->tax = $debt->tax;

            $conceptdb->sum_price = $debt->sum_price;
            $presupuesto->subtotal += $conceptdb->sum_price;

            $conceptdb->sum_tax = $debt->sum_tax;
            $presupuesto->sum_tax += $conceptdb->sum_tax;

            if (!array_key_exists($conceptdb->tax,$presupuesto->alicIva)) {
                $presupuesto->alicIva[$conceptdb->tax] = ['Id' => $conceptdb->tax, 'BaseImp' => 0, 'Importe' => 0 ];
            }

            $presupuesto->alicIva[$conceptdb->tax]['BaseImp'] += $conceptdb->sum_price;
            $presupuesto->alicIva[$conceptdb->tax]['Importe'] += $conceptdb->sum_tax;

            $conceptdb->total = $debt->total;

            $presupuesto->total += $conceptdb->total;

            $conceptdb->presupuesto_id = $presupuesto->id;

            if (!$this->_ctrl->PresupuestoConcepts->save($conceptdb)) {

                $data_error = new \stdClass; 
                $data_error->conceptdb = $conceptdb;

                $event = new Event('FiscoAfipComp.Error', $this, [
                	'msg' => __('Hubo un error al intentar crear el concepto del presupuesto.'),
                	'data' => $data_error,
                	'flash' => false
                ]);

                $this->_ctrl->getEventManager()->dispatch($event);

                return false;

            } //en debts

        }

        if ($presupuesto->total < 0 ) {

            $data_error = new \stdClass; 
            $data_error->invoice = $invoice;

            $event = new Event('FiscoAfipComp.Error', $this, [
            	'msg' => __('La suma de los conceptos da como resultado un valor negativo.'),
            	'data' => $data_error,
            	'flash' => true
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);

            return false;
        }

        if ($presupuesto->comments == '') {
            $presupuesto->comments = $presupuesto_comments;
        }

        if (!$this->_ctrl->Presupuestos->save($presupuesto)) {

            $data_error = new \stdClass; 
            $data_error->presupuesto = $presupuesto;
            
            $event = new Event('FiscoAfipComp.Error', $this, [
            	'msg' => __('Hubo un error al intentar actualizar el presupuesto.'),
            	'data' => $data_error,
            	'flash' => false
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);

            return false;
        }

        $action =  'Prespuesto Manual';

        $afip_codes = $this->_ctrl->request->getSession()->read('afip_codes');

        $detail =  'Tipo de Comp.: ' . $afip_codes['comprobantesLetter'][$presupuesto->tipo_comp] . PHP_EOL;
        $detail .= 'Fecha: ' . $presupuesto->date->format('d/m/Y') . PHP_EOL;
        $detail .= 'Vencimiento: '. $presupuesto->duedate->format('d/m/Y') . PHP_EOL;
        $detail .= 'Número: ' . sprintf("%'.04d", $presupuesto->pto_vta) . '-' . sprintf("%'.08d", $presupuesto->num) . PHP_EOL;
        $detail .= 'Total: ' . number_format($presupuesto->total, 2, ',', '.') . PHP_EOL;
        $this->_ctrl->registerActivity($action, $detail, $presupuesto->customer_code, TRUE);

        return $presupuesto;
    }

    public function rollbackReceipt($receipt, $force = FALSE)
    {
        if ($receipt->seating_number && $force) {
           $this->Accountant->deleteSeating($receipt->seating_number); 
        }

        if ($this->_ctrl->Receipts->delete($receipt)) {
            $this->CurrentAccount->updateDebtMonth($receipt->customer_code, $receipt->connection_id);
            return true;
        }

        return false;
    }

    public function rollbackInvoice($invoice, $force = false)
    {
        if ($invoice->tipo_comp != 'XXX' && !$force) {
            return false;
        }

        $this->_ctrl->Invoices->Debts->updateAll(['invoice_id' => null], ['invoice_id' => $invoice->id]);

        $this->_ctrl->Invoices->Concepts->deleteAll(['comprobante_id' => $invoice->id]);

        /**
         * ! el seating_number puede ser de las deudas que se facturaron, por,lo atnto no se tiene que eliminar, 
         * ! solo se tien que eliminar si es una factura manual
         */

        if ($invoice->seating_number && $invoice->manual) {
            $this->Accountant->deleteSeating($invoice->seating_number);
        }

        if ( $this->_ctrl->Invoices->delete($invoice)) {

            $this->CurrentAccount->updateDebtMonth($invoice->customer_code, $invoice->connection_id);
            return true;
        }

        return false;
    }

    public function rollbackPresupuesto($presupuesto)
    {
        $this->_ctrl->Presupuestos->PresupuestoConcepts->deleteAll(['presupuesto_id' => $presupuesto->id]);

        if ($this->_ctrl->Presupuestos->delete($presupuesto)) {
            return true;
        }

        return false;
    }

    public function rollbackCreditNote($credit_note, $force = false)
    {
        if ($credit_note->tipo_comp != 'NCX' && !$force) {
            return false;
        }
        
        $this->_ctrl->CreditNotes->ConceptsCredit->deleteAll(['credit_note_id' => $credit_note->id]);

        if ($credit_note->seating_number && $force) {
            $this->Accountant->deleteSeating($credit_note->seating_number);
        }

        if ($this->_ctrl->CreditNotes->delete($credit_note)) {

            $this->CurrentAccount->updateDebtMonth($credit_note->customer_code, $credit_note->connection_id);
            return true;
        }

        return false;
    }

    public function rollbackDebitNote($debit_note, $force = false)
    {
        if ($debit_note->tipo_comp != 'NDX' && !$force) {
            return false;
        }

        $this->_ctrl->DebitNotes->ConceptsDebit->deleteAll(['debit_note_id' => $debit_note->id]);

        if ($debit_note->seating_number && $force) {
            $this->Accountant->deleteSeating($debit_note->seating_number);
        }

        if ($this->_ctrl->DebitNotes->delete($debit_note)) {

            $this->CurrentAccount->updateDebtMonth($debit_note->customer_code, $debit_note->connection_id);

            return true;
        }

        return false;
    }
}
