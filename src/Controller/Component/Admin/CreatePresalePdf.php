<?php
namespace App\Controller\Component\Admin;

use App\Controller\Component\Common\MyComponent;

use Cake\Controller\ComponentRegistry;
use Cake\I18n\Time;
use FPDF;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Network\Exception\NotFoundException;
use Cake\Event\EventManager;
use Cake\Event\Event;
use App\Controller\Component\Admin\Pdf\PresaleResumePdf;

/**
 * Documents component
 */
class CreatePresalePdf extends MyComponent
{
    const TYPE_PRESALE_RESUME = 0;

    protected $_defaultConfig = [];

    private $_data;

    // The other component your component uses
    public $components = [];

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->_ctrl->loadModel('Presales');
    }

    public function initEventManager() 
    {
        EventManager::instance()->on(
            'CreatePresalePdf.Error',
            function($event, $msg, $data, $flash = false) {

                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $log->user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Error';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);

                if ($flash) {
                    $this->_ctrl->Flash->error(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'CreatePresalePdf.Warning',
            function($event, $msg, $data, $flash = false) {

                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $log->user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Warning';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);

                if ($flash) {
                    $this->_ctrl->Flash->warning(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'CreatePresalePdf.Log',
            function($event, $msg, $data) {

            }
        );

        EventManager::instance()->on(
            'CreatePresalePdf.Notice',
            function($event, $msg, $data) {

            }
        );
    }

    public function generate($presale, $send_email = false)
    {
        $presale->paraments = $this->_ctrl->request->getSession()->read('paraments');

        $presale->responsibles = $this->_ctrl->request->getSession()->read('afip_codes')['responsibles'];
        $presale->doc_types = $this->_ctrl->request->getSession()->read('afip_codes')['doc_types'];

        foreach ($presale->paraments->invoicing->business as $b) {
            if ($b->id == $presale->customer->business_billing) {
                 $presale->customer->business = $b;
            }
        }

        $debts = [];

        foreach ($presale->debts as $debt) {
            // if ($debt->duedate < Time::now()->modify('+1 month')->day(1)) {
                $debts[] = $debt;
            // }
        }

        $presale->debts = $debts;

        $resume = new PresaleResumePdf();
        $resume->generate($presale, $this->_ctrl);
        
        if ($send_email) {

            $folder = new Folder(WWW_ROOT . 'temp_files', TRUE, 0755);

            $filename  = WWW_ROOT . 'temp_files/';
            $filename .= Time::now()->toUnixString();
            $filename .= '-venta-'.str_pad($presale->id, 5, "0", STR_PAD_LEFT);
            $filename .= '.pdf';

            $resume->Output($filename, 'F');
            return $filename;
            
        }
            
        $this->response = $this->response->withCharset('UTF-8');
        $this->response = $this->response->withType('application/pdf');
        
        $resume->Output();
        
        return '';
	
    }
}
