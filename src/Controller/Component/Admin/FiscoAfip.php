<?php
namespace App\Controller\Component\Admin;

use App\Controller\Component\Admin\Fisco;
use Cake\I18n\Time;
use WsFE;
use Cake\Log\Log;
use Cake\Error\Debugger;
use Cake\Event\EventManager;
use Cake\Event\Event;

/**
 * Documents component
 */
class FiscoAfip extends Fisco
{
    public const TYPE_RECEIPT = 0;
    public const TYPE_INVOICE = 1;
    public const TYPE_CREDIT_NOTE = 2;
    public const TYPE_DEBIT_NOTE = 3;

    const URLWSAA_HOMO = "https://wsaahomo.afip.gov.ar/ws/services/LoginCms";
    const URLWSW_HOMO = "https://wswhomo.afip.gov.ar/wsfev1/service.asmx";
    const URLWSCDC_HOMO = "https://wswhomo.afip.gov.ar/WSCDC/service.asmx";

    const URLWSAA = "https://wsaa.afip.gov.ar/ws/services/LoginCms";
    const URLWSW = "https://servicios1.afip.gov.ar/wsfev1/service.asmx";
    const URLWSCDC = "https://servicios1.afip.gov.ar/WSCDC/service.asmx";

    // The other component your component uses
    public $components = [];
    
    public function initialize(array $config)
    {
        parent::initialize($config);
        
        $this->initEventManager();
    }
    
    public function initEventManager()
    {
        EventManager::instance()->on(
            'AFIP.Error',
            function ($event, $msg, $data, $flash = false) {
 
                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $user_id = null;
                
                if (!isset($this->_ctrl->request->getSession()->read('Auth.User')['id'])) {
                    $user_id = 100;
                } else {
                    $user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                }
                
                $log->user_id = $user_id;
                
                $log->type = 'Error';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);
                
                if ($flash) {
                    $this->_ctrl->Flash->error(__($msg));
                }
            }
        );
        
        EventManager::instance()->on(
            'AFIP.Warning',
            function ($event, $msg, $data, $flash = false) {
                      
                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                
                $user_id = null;
                
                if (!isset($this->_ctrl->request->getSession()->read('Auth.User')['id'])) {
                    $user_id = 100;
                } else {
                    $user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                }
                
                $log->user_id = $user_id;
                $log->type = 'Warning';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);
                
                if ($flash) {
                    $this->_ctrl->Flash->warning(__($msg));
                }
            }
        );
        
        EventManager::instance()->on(
            'AFIP.Log',
            function ($event, $msg, $data, $flash = false) {
              
            }
        );
        
        EventManager::instance()->on(
            'AFIP.Notice',
            function ($event, $msg, $data, $flash = false) {
               
            }
        );
    }
    
    public function testLoginAFIP($business){

        $certificado = "wsfe/".$business->webservice_afip->crt;
        $clave = "wsfe/".$business->webservice_afip->key;
        
        $urlwsaa = self::URLWSAA_HOMO;
        $urlwsw = self::URLWSW_HOMO;
        $urlwscdc = self::URLWSCDC_HOMO;
        
        if (!$business->webservice_afip->testing) {
            $urlwsaa = self::URLWSAA;
            $urlwsw = self::URLWSW;
            $urlwscdc = self::URLWSCDC;
        }
       
        $wsfe = new WsFE();
        $wsfe->CUIT = $business->cuit;
        $wsfe->setURL($urlwsw);
        
        if ($wsfe->Login($certificado, $clave, $urlwsaa)) {
   
            return true;
            
            
        }else{
            
            $data_error = new \stdClass; 
            $data_error->ErrorCode = $wsfe->ErrorCode;
            $data_error->ErrorDesc = $wsfe->ErrorDesc;
            
            $event = new Event('AFIP.Error', $this, [
            	'msg' => __('Login error.'),
            	'data' => $data_error,
            	'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
         
            return false;
        }
        
    }
    
    public function informar(&$comp, $type)
    {
        if (in_array($comp->tipo_comp, ['XXX', 'NDX', 'NCX'])) {
            return $comp;
        }
        
        $paraments = $this->request->getSession()->read('paraments');

        $certificado = "wsfe/".$comp->business->webservice_afip->crt;
        $clave = "wsfe/".$comp->business->webservice_afip->key;
        
        $urlwsaa = self::URLWSAA_HOMO;
        $urlwsw = self::URLWSW_HOMO;
        $urlwscdc = self::URLWSCDC_HOMO;
        
        if (!$comp->business->webservice_afip->testing) {
            $urlwsaa = self::URLWSAA;
            $urlwsw = self::URLWSW;
            $urlwscdc = self::URLWSCDC;
        }
       
        $wsfe = new WsFE();
        $wsfe->CUIT = $comp->company_ident;
        $wsfe->setURL($urlwsw);
        
        if ($wsfe->Login($certificado, $clave, $urlwsaa)) {
        
            if (!$wsfe->RecuperaLastCMP($comp->pto_vta, $comp->tipo_comp)) {
                
                $data_error = new \stdClass; 
                $data_error->ErrorCode = $wsfe->ErrorCode;
                $data_error->ErrorDesc = $wsfe->ErrorDesc;
                $data_error->comp = $comp;
                
                $event = new Event('AFIP.Error', $this, [
                	'msg' => __('RecuperaLastCMP error.'),
                	'data' => $data_error,
                	'flash' => false
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);

                return false;
                
            } else {
                
                $wsfe->Reset();
                $comp->num = $wsfe->RespUltNro + 1;
    
                if ($type == self::TYPE_INVOICE) {

                    if ($comp->concept_type == 1) {
                        $wsfe->AgregaFactura($comp->concept_type, $comp->customer_doc_type, $comp->customer_ident, $comp->num, $comp->num, $comp->date->format('Ymd'), $comp->total, 0.0, $comp->subtotal, 0.0, "", "",  "", "PES", 1);
                        
                    } else {
                        $wsfe->AgregaFactura($comp->concept_type, $comp->customer_doc_type, $comp->customer_ident, $comp->num, $comp->num, $comp->date->format('Ymd'), $comp->total, 0.0, $comp->subtotal, 0.0, $comp->date_start->format('Ymd'),  $comp->date_end->format('Ymd'),  $comp->duedate->format('Ymd'), "PES", 1);
                    }
                    
                    if ($comp->alicIva && $comp->tipo_comp != '011') {

                        foreach ($comp->alicIva as $alicIva) {
                            $wsfe->AgregaIVA($alicIva['Id'], $alicIva['BaseImp'], $alicIva['Importe']);
                        }
                    }
                    
                    if ($comp->comp_asociados) {
                         foreach ($comp->comp_asociados as $comp_asociado) {
                            $wsfe->AgregaCompAsoc($comp_asociado->tipo_comp, $comp_asociado->pto_vta, $comp_asociado->num);    
                        }
                    }
                    
                    
                } else if ($type == self::TYPE_RECEIPT) {
                    
                    $wsfe->AgregaFactura($comp->concept_type, $comp->customer_doc_type, $comp->customer_ident, $comp->num, $comp->num, $comp->date->format('Ymd'), $comp->total, 0.0, $comp->total, 0.0, $comp->date->format('Ymd'),  $comp->date->format('Ymd'),  $comp->date->format('Ymd'), "PES", 1);

                } else if ($type == self::TYPE_CREDIT_NOTE  ) {
                    
                     if ($comp->concept_type == 1) {
                        $wsfe->AgregaFactura($comp->concept_type, $comp->customer_doc_type, $comp->customer_ident, $comp->num, $comp->num, $comp->date->format('Ymd'), $comp->total, 0.0, $comp->subtotal, 0.0, "", "",  "", "PES", 1);
                        
                    } else {
                        $wsfe->AgregaFactura($comp->concept_type, $comp->customer_doc_type, $comp->customer_ident, $comp->num, $comp->num, $comp->date->format('Ymd'), $comp->total, 0.0, $comp->subtotal, 0.0, $comp->date_start->format('Ymd'),  $comp->date_end->format('Ymd'),  $comp->duedate->format('Ymd'), "PES", 1);
                    }

                    if ($comp->alicIva && $comp->tipo_comp != '013') {
                        
                        foreach($comp->alicIva as $alicIva) {
                            $wsfe->AgregaIVA($alicIva['Id'], $alicIva['BaseImp'], $alicIva['Importe']);
                        }
                    }
                    
                    if ($comp->comp_asociados) {
                         foreach ($comp->comp_asociados as $comp_asociado) {
                            $wsfe->AgregaCompAsoc($comp_asociado->tipo_comp, $comp_asociado->pto_vta, $comp_asociado->num);    
                        }
                    }

                } else if ($type == self::TYPE_DEBIT_NOTE) {
                    
                     if ($comp->concept_type == 1) {
                        $wsfe->AgregaFactura($comp->concept_type, $comp->customer_doc_type, $comp->customer_ident, $comp->num, $comp->num, $comp->date->format('Ymd'), $comp->total, 0.0, $comp->subtotal, 0.0, "", "",  "", "PES", 1);
                        
                    } else {
                        $wsfe->AgregaFactura($comp->concept_type, $comp->customer_doc_type, $comp->customer_ident, $comp->num, $comp->num, $comp->date->format('Ymd'), $comp->total, 0.0, $comp->subtotal, 0.0, $comp->date_start->format('Ymd'),  $comp->date_end->format('Ymd'),  $comp->duedate->format('Ymd'), "PES", 1);
                    }
                    
                    if ($comp->alicIva && $comp->tipo_comp != '012') {
                        foreach($comp->alicIva as $alicIva){
                            $wsfe->AgregaIVA($alicIva['Id'], $alicIva['BaseImp'], $alicIva['Importe']);
                        }
                    }
                    
                    if ($comp->comp_asociados) {
                         foreach ($comp->comp_asociados as $comp_asociado) {
                            $wsfe->AgregaCompAsoc($comp_asociado->tipo_comp, $comp_asociado->pto_vta, $comp_asociado->num);    
                        }
                    }
                }
           
                $auth = false;
                try {
      
                    if ($wsfe->Autorizar($comp->pto_vta, $comp->tipo_comp, $comp->business->webservice_afip->log)) {
                        
                        $auth = true;

                    } else {
                        
                        $data_error = new \stdClass; 
                        $data_error->ErrorCode = $wsfe->ErrorCode;
                        $data_error->ErrorDesc = $wsfe->ErrorDesc;
                        $data_error->comp = $comp;
                        
                        $event = new Event('AFIP.Error', $this, [
                        	'msg' => __('Autorizar error.'),
                        	'data' => $data_error,
                        	'flash' => false
                        ]);
                        
                        $this->_ctrl->getEventManager()->dispatch($event);
                    
                        return false;
                    }
                } catch (Exception $e) {
                    
                    if ($wsfe->CmpConsultar($comp->tipo_comp, $comp->pto_vta, $comp->num, $cbte)) {
                        
                        $auth = true;

                    } else {
                        
                        $data_error = new \stdClass;
                        $data_error->ErrorCode = $wsfe->ErrorCode;
                        $data_error->ErrorDesc = $wsfe->ErrorDesc;
                        $data_error->comp = $comp;
                        
                        $event = new Event('AFIP.Error', $this, [
                        	'msg' => __('CmpConsultar error.'),
                        	'data' => $data_error,
                        	'flash' => false
                        ]);
                        
                        $this->_ctrl->getEventManager()->dispatch($event);
                    }
                }
                if ($auth) {

                    $comp->cae = $wsfe->RespCAE;
                    $comp->vto = $wsfe->RespVencimiento;
                    $comp->barcode = $comp->company_ident . sprintf('%02d', $comp->tipo_comp) . sprintf('%04d', $comp->pto_vta) . $wsfe->RespCAE . $wsfe->RespVencimiento;
                }
            }
        } else {

            $data_error = new \stdClass; 
            $data_error->ErrorCode = $wsfe->ErrorCode;
            $data_error->ErrorDesc = $wsfe->ErrorDesc;
            $data_error->comp = $comp;
            
            $event = new Event('AFIP.Error', $this, [
            	'msg' => __('Login error.'),
            	'data' => $data_error,
            	'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
         
            return false;
        }
      
        return true;
    }
}
