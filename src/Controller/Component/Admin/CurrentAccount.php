<?php
namespace App\Controller\Component\Admin;

use App\Controller\Component\Common\MyComponent;
use Cake\Controller\ComponentRegistry;
use Cake\I18n\Time;
use Cake\Datasource\ConnectionManager;

/**
 * Accountant component
 */
class CurrentAccount extends MyComponent
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    // The other component your component uses
    public $components = [
        'Accountant' => [
            'className' => '\App\Controller\Component\Admin\Accountant'
        ]
    ];

    public function initialize(array $config)
    {
        parent::initialize($config);
    }

    /**
    *  crea deudas de conexiones ...
    *  [concept, seating, 'customer, service, calculate_proporcional,  period, connection,  duedate = null, tipo_comp  = null]
    */
    public function addDebtService(array $data, $calulate_saldo = TRUE)
    {
        $this->_ctrl->loadModel('Debts');

        $paraments = $this->_ctrl->request->getSession()->read('paraments');
        $afip_codes = $this->_ctrl->request->getSession()->read('afip_codes');

        $debt = $this->_ctrl->Debts->newEntity();

        $duedate = $data['duedate'];

        if (!$duedate) {

            $duedate = new Time($data['connection']->created->format('Y-m-d H:i:s'));
            $duedate = $duedate->day($data['customer']->daydue);

            if ($paraments->accountant->type == '01') { //mes vencido
                $duedate->modify('+1 month');
            }

            if ($data['connection']->created > $duedate) {
                $duedate = $data['connection']->created;
            }
        }

        $debt->duedate = $duedate;
        $debt->type = 'S';
        $debt->code = 'S' . sprintf("%'.03d", $data['service']->id);
        $debt->description = $data['concept'];

        $debt->quantity = 1; 
        $debt->unit = 07; 
        $debt->discount = 0;

        $debt->price = $data['service']->price; //precio final (con iva)

        if ($data['calculate_proporcional']) {

            $value = $debt->price;
            $now = $data['connection']->created;

            $number_day = $this->get_number_of_days_in_month($now->month, $now->year);
            $value = ($data['service']->price / $number_day) * ($number_day - $now->day + 1);
            $debt->price = ceil($value);

            $debt->created = $data['connection']->created;
            $debt->modified = $data['connection']->created;
        }

        if (array_key_exists('tipo_comp', $data) && $data['tipo_comp'] == 'XXX') {

            $debt->tipo_comp ='XXX';
        } else {

            if (array_key_exists('tipo_comp', $data) && $data['tipo_comp'] != '011' && $data['tipo_comp'] != 'XXX') { //RI

                $debt->tipo_comp = $data['tipo_comp'];
            } else { //MO

                if ($data['customer']->is_presupuesto) {

                    $debt->tipo_comp ='XXX';

                } else {

                    foreach ($paraments->invoicing->business as $business) {

                        if ($business->id == $data['customer']->business_billing) {
                            $data['customer']->business = $business;
                        }
                        
                    }

                    $debt->tipo_comp = $afip_codes['combinationsInvoices'][$data['customer']->business->responsible][$data['customer']->responsible];
                }
            }
        }

        $debt->tax = $data['service']->aliquot;

        $tax =  $afip_codes['alicuotas_percectage'][$debt->tax];

        $total = $debt->price;

        $row = $this->calulateIvaAndNeto($total, $tax, $debt->quantity);

        $debt->price = $row->price;
        $debt->sum_price = $row->sum_price;
        $debt->sum_tax = $row->sum_tax;
        $debt->total = $row->total;

        $debt->dues = 1;

        $debt->user_id = $this->_ctrl->Auth->user()['id'];
        $debt->customer_code = $data['customer']->code;

        //creo el asiento

        if (!$data['seating'] && $this->Accountant->isEnabled()) {

            $data['seating'] = $this->Accountant->addSeating([
                'concept' => 'Venta de Servicio', 
                'comments' => '',
                'seating' => null,
                'account_debe' => $data['customer']->account_code,
                'account_haber' =>  $data['service']->account_code,
                'value' => $debt->total,
                'created' => $data['connection']->created
            ]);
            
            if($data['seating']){
                $debt->seating_number = $data['seating']->number;
            }
            
        } else if ($this->Accountant->isEnabled()) {
            
            $debt->seating_number = $data['seating']->number;
        }
        
        $connection_id = NULL;
        if ($data['customer']->billing_for_service) {
            $connection_id = $data['connection']->id;
        }

        $debt->connection_id = $connection_id;
        $debt->period = $data['period'];

        if (!$this->_ctrl->Debts->save($debt)) {
            $this->_ctrl->Flash->error(__('Error al agregar la deuda al cliente.'));
            return false;
        }

        //si existe agrega descuento

        if ($data['connection']->discount_month > 0 || $data['connection']->discount_always) {

           if (!$this->addDiscountByConnection($data['customer'], $data['connection'], $debt)) {
               $this->_ctrl->Flash->error(__('Error al intentar aplicar el descuento a la deuda.'));
               return false;
           }
        }

        if ($calulate_saldo) {
            $this->updateDebtMonth($data['customer']->code, $connection_id);
        }

        return $debt;
    }

    /**
    *  [amount,  'customer, product, 'duedate', 'dues'  ]
    */
    public function addDebtProduct(array $data)
    {
        $this->_ctrl->loadModel('Debts');

        $paraments = $this->_ctrl->request->getSession()->read('paraments');
        $afip_codes = $this->_ctrl->request->getSession()->read('afip_codes');

        $time_now = Time::now();
        $hms = $time_now->i18nFormat('HH:mm:ss');

        $period = explode('/', $data['period']);
        $period = new Time($period[1].'-'.$period[0].'-1 ' . $hms);

        //mas de una cuota
        if ($data['dues'] > 1) {

            $inetreses = $paraments->accountant->dues;

            $duedate = $data['duedate'];

            for ($i = 0; $i < $data['dues']; $i++) {

                $debt = $this->_ctrl->Debts->newEntity();

                $debt->created = Time::now();
                $debt->modified = Time::now();
                $debt->duedate = $duedate;
                $debt->period = $period->format('m/Y');
                $debt->type = 'P';
                $debt->code = 'P' . sprintf("%'.03d", $data['product']->id);
                $debt->quantity =  1;
                $debt->discount = 0;
                $debt->description = $data['amount'] . ' ' . $data['product']->name . ' Cuota ' . ($i + 1) . '-' . $data['dues'];
                $debt->unit = 07; 

                $debt->tax = $data['product']->aliquot;
                $tax =  $afip_codes['alicuotas_percectage'][$debt->tax];

                $interes = $inetreses[$data['dues'] - 1];

                $total = $data['product']->unit_price * ($interes + 1);

                $total = $total / $data['dues']; // precio + iva + interes (por cuota)

                $total =  round($total, 2);

                $row = $this->calulateIvaAndNeto($total, $tax, $debt->quantity);

                $debt->price = $row->price;
                $debt->sum_price = $row->sum_price;
                $debt->sum_tax = $row->sum_tax;
                $debt->total = $row->total;

                //crear el asiento 
                
                if ($this->Accountant->isEnabled()) {
                    
                    $seating = $this->Accountant->addSeating([
                        'concept' => 'Por Venta de Producto',
                        'comments' => '',
                        'seating' => null, 
                        'account_debe' => $data['customer']->account_code,
                        'account_haber' => $data['product']->account_code,
                        'value' => $debt->total
                    ]);
                    
                    $debt->seating_number = $seating->number;
                }

                $debt->dues = $data['dues'];

                if ($data['customer']->is_presupuesto) {
                    $debt->tipo_comp ='XXX';
                } else {

                    foreach ($paraments->invoicing->business as $business) {
                        if ($business->id == $data['customer']->business_billing) {
                            $data['customer']->business = $business;
                        }
                    }

                    $debt->tipo_comp = $afip_codes['combinationsInvoices'][$data['customer']->business->responsible][$data['customer']->responsible];
                }

                $debt->user_id = $this->_ctrl->Auth->user()['id'];
                $debt->customer_code = $data['customer']->code;
                
                $debt->presale_id =  array_key_exists('presale_id', $data) ? $data['presale_id'] : null;
                
                $debt->connection_id = array_key_exists('connection_id', $data) ? $data['connection_id'] : null;

                if (!$this->_ctrl->Debts->save($debt)) {
                    $this->_ctrl->Flash->error(__('Error al agregar la deuda al cliente.'));
                    return false;
                }

                if (array_key_exists('discount_id', $data) && $data['discount_id']) {
                    
                    if (!$this->addDiscount($data['customer'], $debt, $data['discount_id'])) {
                        $this->_ctrl->Flash->error(__('Error al agregar el decuento a la deuda.'));
                        return false;
                    }
                }
                
                $this->updateDebtMonth($data['customer']->code, $debt->connection_id);

                $duedate->day($data['customer']->daydue);
                $duedate->modify('+1 month');
                
                $period->modify('+1 month');
            }
        } else {
            
            $duedate = $data['duedate'];

            $debt = $this->_ctrl->Debts->newEntity();

            $debt->created = Time::now();
            $debt->modified = Time::now();
            $debt->duedate = $duedate;
            $debt->period = $period->format('m/Y');
            $debt->type = 'P';
            $debt->code = 'P' . sprintf("%'.03d", $data['product']->id);
            $debt->quantity = $data['amount']; 
            $debt->description = $data['product']->name;
            $debt->unit = 07; 
            $debt->discount = 0;

            $debt->tax = $data['product']->aliquot;
            $tax =  $afip_codes['alicuotas_percectage'][$debt->tax];

            $total = $data['product']->unit_price * $debt->quantity;

            $row = $this->calulateIvaAndNeto($total, $tax, $debt->quantity);

            $debt->price = $row->price;
            $debt->sum_price = $row->sum_price;
            $debt->sum_tax = $row->sum_tax;
            $debt->total = $row->total;

            //crear el asiento 
            
            if ($this->Accountant->isEnabled()) {
                
                $seating = $this->Accountant->addSeating([
                    'concept' => 'Por Venta de Producto',
                    'comments' => '',
                    'seating' => null, 
                    'account_debe' => $data['customer']->account_code,
                    'account_haber' => $data['product']->account_code,
                    'value' => $debt->total
                ]);
                    
                $debt->seating_number = $seating->number;
            }

            $debt->dues = 1;

            if ($data['customer']->is_presupuesto) {
                $debt->tipo_comp ='XXX';
            } else {

                foreach ($paraments->invoicing->business as $business) {
                    if ($business->id == $data['customer']->business_billing) {
                        $data['customer']->business = $business;
                    }
                }

                $debt->tipo_comp = $afip_codes['combinationsInvoices'][$data['customer']->business->responsible][$data['customer']->responsible];
            }

            $debt->user_id = $this->_ctrl->Auth->user()['id'];
            $debt->customer_code = $data['customer']->code;
            
            $debt->presale_id =  array_key_exists('presale_id', $data) ? $data['presale_id'] : null;
            
            $debt->connection_id = array_key_exists('connection_id', $data) ? $data['connection_id'] : null;

            if (!$this->_ctrl->Debts->save($debt)) {
                $this->_ctrl->Flash->error(__('Error al agregar la deuda al cliente.'));
                 return false;
            }

            if (array_key_exists('discount_id', $data) && $data['discount_id']) {

                if (!$this->addDiscount($data['customer'], $debt, $data['discount_id'])) {
                    $this->_ctrl->Flash->error(__('Error al agregar el decuento a la deuda.'));
                    return false;
                }
            }
            
            $this->updateDebtMonth($data['customer']->code, $debt->connection_id);
            
        }
        
        return true;
    }

   /**
    *  [amount,  'customer, package, 'duedate', 'dues', discount_id ]
    */
    public function addDebtPackage(array $data)
    {
        $this->_ctrl->loadModel('Debts');

        $paraments = $this->_ctrl->request->getSession()->read('paraments');
        $afip_codes = $this->_ctrl->request->getSession()->read('afip_codes');

        $time_now = Time::now();
        $hms = $time_now->i18nFormat('HH:mm:ss');

        $period = explode('/', $data['period']);
        $period = new Time($period[1].'-'.$period[0].'-1 ' . $hms);

        //mas de una cuota
        if ($data['dues'] > 1) {

            $inetreses = $paraments->accountant->dues;

            $duedate = $data['duedate'];

            for ($i = 0; $i < $data['dues']; $i++) {

                $debt = $this->_ctrl->Debts->newEntity();

                $debt->created = Time::now();
                $debt->modified = Time::now();
                $debt->duedate = $duedate;
                $debt->period = $period->format('m/Y');
                $debt->type = 'I';
                $debt->code = 'I'.sprintf("%'.03d", $data['package']->id);
                $debt->quantity = 1; 
                $debt->description = $data['package']->name . ' Cuota ' . ($i + 1) . '-' . $data['dues'];
                $debt->unit = 07; 
                $debt->discount = 0;

                $debt->tax = $data['package']->aliquot;
                $tax =  $afip_codes['alicuotas_percectage'][$debt->tax];

                $interes = $inetreses[$data['dues'] - 1];

                $total = $data['package']->price * ($interes + 1);

                $total = $total / $data['dues']; // precio + iva + interes (por cuota)

                $total =  round($total, 2);

                $row = $this->calulateIvaAndNeto($total, $tax, $debt->quantity);

                $debt->price = $row->price;
                $debt->sum_price = $row->sum_price;
                $debt->sum_tax = $row->sum_tax;
                $debt->total = $row->total;

                //crear el asiento 
                
                if ($this->Accountant->isEnabled()) {
                    
                    $seating = $this->Accountant->addSeating([
                        'concept' => 'Por Venta de Paquete',
                        'comments' => '',
                        'seating' => null, 
                        'account_debe' => $data['customer']->account_code,
                        'account_haber' => $data['package']->account_code,
                        'value' => $debt->total
                    ]);
                        
                    $debt->seating_number = $seating->number;
                }

                $debt->dues = $data['dues'];

                if ($data['customer']->is_presupuesto) {
                    $debt->tipo_comp ='XXX';
                } else {

                    foreach ($paraments->invoicing->business as $business) {
                        if ($business->id == $data['customer']->business_billing) {
                            $data['customer']->business = $business;
                        }
                    }

                    $debt->tipo_comp = $afip_codes['combinationsInvoices'][$data['customer']->business->responsible][$data['customer']->responsible];
                }

                $debt->user_id = $this->_ctrl->Auth->user()['id'];
                $debt->customer_code = $data['customer']->code;
                
                $debt->presale_id = array_key_exists('presale_id', $data) ?  $data['presale_id'] : null;

                $debt->connection_id = array_key_exists('connection_id', $data) ? $data['connection_id'] : null;

                if (!$this->_ctrl->Debts->save($debt)) {
                    $this->_ctrl->Flash->error(__('Error al agregar la deuda al cliente.'));
                    return false;
                }

                if (array_key_exists('discount_id', $data) && $data['discount_id']) {

                    if (!$this->addDiscount($data['customer'], $debt, $data['discount_id'])) {
                        $this->_ctrl->Flash->error(__('Error al agregar el decuento a la deuda.'));
                        return false;
                    }
                }

                $this->updateDebtMonth($data['customer']->code, $debt->connection_id);

                $duedate->day($data['customer']->daydue);
                $duedate->modify('+1 month');

                $period->modify('+1 month');
            }
        } else {

            $duedate = $data['duedate'];

            $debt = $this->_ctrl->Debts->newEntity();

            $debt->created = Time::now();
            $debt->modified = Time::now();
            $debt->duedate = $duedate;
            $debt->period = $period->format('m/Y');
            $debt->type = 'I';
            $debt->code = 'I' . sprintf("%'.03d", $data['package']->id);
            $debt->quantity = $data['amount']; 
            $debt->description = $data['package']->name;
            $debt->unit = 07; 
            $debt->discount = 0;

            $debt->tax = $data['package']->aliquot;
            $tax =  $afip_codes['alicuotas_percectage'][$debt->tax];

            $total = $data['package']->price * $debt->quantity;

            $row = $this->calulateIvaAndNeto($total, $tax, $debt->quantity);

            $debt->price = $row->price;
            $debt->sum_price = $row->sum_price;
            $debt->sum_tax = $row->sum_tax;
            $debt->total = $row->total;

            if ($this->Accountant->isEnabled()) {

                $seating = $this->Accountant->addSeating([
                    'concept' => 'Por Venta de Paquete',
                    'comments' => '',
                    'seating' => null, 
                    'account_debe' => $data['customer']->account_code,
                    'account_haber' => $data['package']->account_code,
                    'value' => $debt->total
                ]);

                $debt->seating_number = $seating->number;
            }

            $debt->dues = 1;

            if ($data['customer']->is_presupuesto) {
                $debt->tipo_comp ='XXX';
            } else {

                foreach ($paraments->invoicing->business as $business) {
                    if ($business->id == $data['customer']->business_billing) {
                        $data['customer']->business = $business;
                    }
                }

                $debt->tipo_comp = $afip_codes['combinationsInvoices'][$data['customer']->business->responsible][$data['customer']->responsible];
            }

            $debt->user_id = $this->_ctrl->Auth->user()['id'];
            $debt->customer_code = $data['customer']->code;

            $debt->presale_id = array_key_exists('presale_id', $data) ?  $data['presale_id'] : null;

            $debt->connection_id = array_key_exists('connection_id', $data) ? $data['connection_id'] : null;

            if (!$this->_ctrl->Debts->save($debt)) {
                $this->_ctrl->Flash->error(__('Error al agregar la deuda al cliente.'));
                 return false;
            }

            if (array_key_exists('discount_id', $data) && $data['discount_id']) {

                if (!$this->addDiscount($data['customer'], $debt, $data['discount_id'])) {
                    $this->_ctrl->Flash->error(__('Error al agregar el decuento a la deuda.'));
                    return false;
                }
            }

            $this->updateDebtMonth($data['customer']->code, $debt->connection_id);
        }

        return true;
    }

    /**
    *  [concept, seating, 'customer, value , duedate, connection_id, is_presupuesto_force]
    */
    public function addDebt(array $data, $calulate_saldo = TRUE)
    {
        $this->_ctrl->loadModel('Debts');

        $paraments = $this->_ctrl->request->getSession()->read('paraments');
        $afip_codes = $this->_ctrl->request->getSession()->read('afip_codes');

        $debt = $this->_ctrl->Debts->newEntity();

        $debt->created = Time::now();
        $debt->modified = Time::now();
        $debt->duedate = $data['duedate'];
        $debt->period = $data['period'];
        $debt->type = 'Fix';
        $debt->code = 99;
        $debt->description = $data['concept'];

        $debt->quantity = 1;
        $debt->unit = 07;

        $debt->tax = $data['alicuot'];
        $tax =  $afip_codes['alicuotas_percectage'][$debt->tax];

        $row = $this->calulateIvaAndNeto($data['value'], $tax, 1);
        $debt->price = $row->price;
        $debt->sum_price = $row->sum_price;
        $debt->sum_tax = $row->sum_tax;
        $debt->total = $row->total;

        $debt->dues = 1;

        if (array_key_exists('is_presupuesto_force', $data) && $data['is_presupuesto_force']) {

            $debt->tipo_comp ='XXX';

        } else {

            if ($data['customer']->is_presupuesto) { 
                $debt->tipo_comp ='XXX';
            } else {

                foreach ($paraments->invoicing->business as $business) {
                    if ($business->id == $data['customer']->business_billing) {
                        $data['customer']->business = $business;
                    }
                }

                $debt->tipo_comp = $afip_codes['combinationsInvoices'][$data['customer']->business->responsible][$data['customer']->responsible];
            }
        }

        if (!isset($this->_ctrl->request->getSession()->read('Auth.User')['id'])) {
            $debt->user_id = 100;
        } else {
            $debt->user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
        }

        $debt->customer_code = $data['customer']->code;
        $debt->connection_id = $data['connection_id'];

        //creo el asiento

        if (!$data['seating'] && $this->Accountant->isEnabled()) {

            $data['seating'] = $this->Accountant->addSeating([
                'concept' => $debt->description, 
                'comments' => '',
                'seating' => null,
                'account_debe' => $data['customer']->account_code,
                'account_haber' => $paraments->accountant->acounts_parent->surcharge,
                'value' => $debt->total
            ]);

            $debt->seating_number = $data['seating']->number;
            
        } else if ($this->Accountant->isEnabled()) {

            $debt->seating_number = $data['seating']->number;
        }

        if (!$this->_ctrl->Debts->save($debt)) {

            $this->_ctrl->Flash->error(__('Error al agregar la deuda al cliente.'));
            return false;
        }

        if ($calulate_saldo) {
            $this->updateDebtMonth($debt->customer_code, $debt->connection_id);
        }

        return $debt;
    }

    public function deleteDebt($debt)
    {
        

        $this->_ctrl->Debts->delete($debt);

        $this->updateDebtMonth($debt->customer_code, $debt->connection_id);

        return true;
    }

    //discounts

    private function addDiscountByConnection($customer, $connection, $debt)
    {
        $paraments = $this->_ctrl->request->getSession()->read('paraments');
        $afip_codes = $this->_ctrl->request->getSession()->read('afip_codes');

        $this->_ctrl->loadModel('CustomersHasDiscounts');
        $customersHasDiscount = $this->_ctrl->CustomersHasDiscounts->newEntity();

        $customersHasDiscount->create =  Time::now();
        $customersHasDiscount->modified = Time::now();
        $customersHasDiscount->code =  $connection->discount_code;

        $customersHasDiscount->description = $connection->discount_description;

        if (!$connection->discount_always) {
            $customersHasDiscount->description .= ' (Mes ' . ($connection->discount_total_month - $connection->discount_month + 1);;
            $customersHasDiscount->description .= ' de '. $connection->discount_total_month . ')';
        }

        $customersHasDiscount->customer_code =  $connection->customer_code;

        $total = $debt->total * $connection->discount_value;

        //como es un descuento realcionado a uan deuda las alicutas deben ser las mismas
        $customersHasDiscount->tipo_comp = $debt->tipo_comp;
        $customersHasDiscount->tax = $debt->tax;
        $tax =  $afip_codes['alicuotas_percectage'][$customersHasDiscount->tax];

        $row = $this->calulateIvaAndNeto($total, $tax, 1);
        $customersHasDiscount->price = $row->price;
        $customersHasDiscount->sum_price = $row->sum_price;
        $customersHasDiscount->sum_tax = $row->sum_tax;
        $customersHasDiscount->total = $row->total;
        $customersHasDiscount->seating_number = NULL;

        //creo el asiento

        if ($this->Accountant->isEnabled()) {
            
            $seating = $this->Accountant->addSeating([
                'concept' => 'Bonificación de servicio', 
                'comments' => $customersHasDiscount->description,
                'seating' => null,
                'account_debe' => $paraments->accountant->acounts_parent->bonus,
                'account_haber' => $customer->account_code,
                'value' => $customersHasDiscount->total,
                'created' => $customersHasDiscount->created
            ]);
            
            $customersHasDiscount->seating_number = $seating->number;
        }
        
        $customersHasDiscount->user_id = $this->_ctrl->Auth->user()['id'];
        $customersHasDiscount->connection_id = $connection->id;

        $customersHasDiscount->debt_id = $debt->id;
        $customersHasDiscount->period = $debt->period;

        if (!$this->_ctrl->CustomersHasDiscounts->save($customersHasDiscount)) {
            $this->_ctrl->Flash->error(__('No se pudo registrar el descuento de la conexión.'));
            return false;
        }

        if (!$connection->discount_always) {
            $connection->discount_month--;
        } else {
            $connection->discount_month = 0;
        }

        if ($connection->discount_month == 0 && !$connection->discount_always) {

            $connection->discount_total_month = null;
            $connection->discount_month = null;
            $connection->discount_type_value = null;
            $connection->discount_value = null;
            $connection->discount_description = null;
            $connection->discount_code = null;
            $connection->discount_alicuot = null;
            $connection->discount_always = null;
        }

        $this->_ctrl->loadModel('Connections');
        $this->_ctrl->Connections->save($connection);

        return true;
    }

    private function addDiscount($customer, $debt, $discount_id)
    {
        $paraments = $this->_ctrl->request->getSession()->read('paraments');
        $afip_codes = $this->_ctrl->request->getSession()->read('afip_codes');

        $this->_ctrl->loadModel('Discounts');
        $discount = $this->_ctrl->Discounts->get($discount_id);

        $this->_ctrl->loadModel('CustomersHasDiscounts');
        $customersHasDiscount = $this->_ctrl->CustomersHasDiscounts->newEntity();

        $customersHasDiscount->create =  Time::now();
        $customersHasDiscount->modified = Time::now();
        $customersHasDiscount->code =  $discount->code;
        $customersHasDiscount->description =  $discount->concept;
        $customersHasDiscount->customer_code =  $customer->code;

        $total = $debt->total * $discount->value;

        //como es un descuento realcionado a uan deuda las alicutas deben ser las mismas
        $customersHasDiscount->tax = $debt->tax;
        $customersHasDiscount->tipo_comp = $debt->tipo_comp;
        $tax = $afip_codes['alicuotas_percectage'][$customersHasDiscount->tax];

        $row = $this->calulateIvaAndNeto($total, $tax, 1);
        $customersHasDiscount->price = $row->price;
        $customersHasDiscount->sum_price = $row->sum_price;
        $customersHasDiscount->sum_tax = $row->sum_tax;
         $customersHasDiscount->total = $row->total;
        $customersHasDiscount->seating_number = NULL;

        //creo el asiento
        
        if ($this->Accountant->isEnabled()) {

            $seating = $this->Accountant->addSeating([
                'concept' => 'Bonificación', 
                'comments' => $customersHasDiscount->description,
                'seating' => null,
                'account_debe' => $customer->account_code,
                'account_haber' =>  $paraments->accountant->acounts_parent->bonus, //??
                'value' => $customersHasDiscount->total,
                'created' => $customersHasDiscount->created
            ]);

            $customersHasDiscount->seating_number = $seating->number;
        }

        $customersHasDiscount->user_id = $this->_ctrl->Auth->user()['id'];
        $customersHasDiscount->connection_id = 0;

        $customersHasDiscount->debt_id = $debt->id;
        $customersHasDiscount->period = $debt->period;

        if (!$this->_ctrl->CustomersHasDiscounts->save($customersHasDiscount)) {
            $this->_ctrl->Flash->error(__('No se pudo registrar el descuento.'));
            return false;
        }
   
        return true;
    }

    /**
    * [concept, seating, 'customer, 'value, 'alicuot , connection_id, period]
    */
    public function addDiscountWithoutDebt($data, $calulate_saldo = TRUE)
    {
        $paraments = $this->_ctrl->request->getSession()->read('paraments');
        $afip_codes = $this->_ctrl->request->getSession()->read('afip_codes');

        $this->_ctrl->loadModel('CustomersHasDiscounts');
        $customersHasDiscount = $this->_ctrl->CustomersHasDiscounts->newEntity();

        $customersHasDiscount->create = Time::now();
        $customersHasDiscount->modified = Time::now();
        $customersHasDiscount->code =  99;
        $customersHasDiscount->description =  $data['concept'];
        $customersHasDiscount->customer_code =  $data['customer']->code;

        if (array_key_exists('is_presupuesto_force', $data) && $data['is_presupuesto_force']) {

            $customersHasDiscount->tipo_comp ='XXX';

        } else {

            if ($data['customer']->is_presupuesto) {
                $customersHasDiscount->tipo_comp ='XXX';
            } else {

                foreach ($paraments->invoicing->business as $business) {
                    if ($business->id == $data['customer']->business_billing) {
                        $data['customer']->business = $business;
                    }
                }

                $customersHasDiscount->tipo_comp = $afip_codes['combinationsInvoices'][$data['customer']->business->responsible][$data['customer']->responsible];
            }
        }

        $customersHasDiscount->tax = $data['alicuot'];
        $tax =  $afip_codes['alicuotas_percectage'][$customersHasDiscount->tax];

        $row = $this->calulateIvaAndNeto($data['value'], $tax, 1);
        $customersHasDiscount->price = $row->price;
        $customersHasDiscount->sum_price = $row->sum_price;
        $customersHasDiscount->sum_tax = $row->sum_tax;
        $customersHasDiscount->total = $row->total;
        $customersHasDiscount->seating_number = NULL;

        //creo el asiento

        if ($this->Accountant->isEnabled()) {

            $seating = $this->Accountant->addSeating([
                'concept' => 'Bonificación', 
                'comments' => $customersHasDiscount->description,
                'seating' => null,
                'account_debe' => $paraments->accountant->acounts_parent->bonus,
                'account_haber' => $data['customer']->account_code, 
                'value' => $customersHasDiscount->total,
                'created' => $customersHasDiscount->created
            ]);

            $customersHasDiscount->seating_number = $seating->number;
        }

        $customersHasDiscount->user_id = $this->_ctrl->Auth->user()['id'];
        $customersHasDiscount->connection_id = $data['connection_id'];

        $customersHasDiscount->debt_id = null;
        $customersHasDiscount->period = $data['period'];

        if (!$this->_ctrl->CustomersHasDiscounts->save($customersHasDiscount)) {
            $this->_ctrl->Flash->error(__('No se pudo registrar el descuento.'));
            return false;
        }

        if ($calulate_saldo) {
            $this->updateDebtMonth($customersHasDiscount->customer_code, $customersHasDiscount->connection_id);
        }

        return $customersHasDiscount;
    }

    public function deleteDiscount($discount)
    {
        $this->_ctrl->CustomersHasDiscounts->delete($discount);

        $this->updateDebtMonth($discount->customer_code, $discount->connection_id);

        return true;
    }

    //calularte debts and update conencion y customers

    public function updateDebtMonth($customer_code, $connection_id = null, $save = true)
    {
        if (!$connection_id) {

            $this->updateDebtCustomer($customer_code);
            $this->controlInvoicesPaid($customer_code);
            $this->updateAmountInvoincesNoPaid($customer_code);

        } else {

            $this->_ctrl->loadModel('Connections');
            $this->_ctrl->loadModel('Customers');
            $this->_ctrl->loadModel('Debts');
            $this->_ctrl->loadModel('CustomersHasDiscounts');
            $this->_ctrl->loadModel('DebitNotes');
            $this->_ctrl->loadModel('Invoices');
            $this->_ctrl->loadModel('Payments');
            $this->_ctrl->loadModel('CreditNotes');
            $this->_ctrl->loadModel('DebitNotes');

            $connection = $this->_ctrl->Connections->get($connection_id);

            if ($connection) {

                //deuda del mes  **** 

                $firstDayNexMonth = Time::now();
                $firstDayNexMonth->modify('+1 month')->day(1)->hour(0)->minute(0)->second(0);

                $connection->debt_month = 0;

                $debts_total = 0;
                $discounts_total = 0;

                $debts = $this->_ctrl->Debts->find()
                    ->contain([
                        'CustomersHasDiscounts'
                    ])
                    ->where([
                        'Debts.connection_id' => $connection->id,
                        'Debts.invoice_id IS' => NULL,
                        'Debts.duedate <' => $firstDayNexMonth->format('Y-m-d H:i:s')
                    ]);

                foreach ($debts as $debt) {

                    if ($debt->customers_has_discount) {
                        $discounts_total += $debt->customers_has_discount->total;
                    }
                    $debts_total += $debt->total;
                }

                $customersHasDiscounts = $this->_ctrl->CustomersHasDiscounts->find()
                    ->where([
                        'connection_id' => $connection->id,
                        'debt_id IS' => NULL,
                        'invoice_id IS' => NULL,
                    ]);

                $customersHasDiscounts = $customersHasDiscounts->select([
                    'total' => $customersHasDiscounts->func()->sum('total')
                ])->first();

                $invoices = $this->_ctrl->Invoices->find()
                    ->where([
                        'connection_id' => $connection->id,
                        'duedate <' => $firstDayNexMonth->format('Y-m-d H:i:s')
                    ]);

                $invoices = $invoices->select([
                        'total' => $invoices->func()->sum('total')
                    ])->first();

                $debitNotes = $this->_ctrl->DebitNotes->find()
                    ->where([
                        'connection_id' => $connection->id,
                        'duedate <' => $firstDayNexMonth->format('Y-m-d H:i:s')
                    ]);

                $debitNotes = $debitNotes->select(['total' => $debitNotes->func()->sum('total')])->first();

                $creditNotes = $this->_ctrl->CreditNotes->find()
                    ->where([
                        'connection_id' => $connection->id
                    ]);

                $creditNotes = $creditNotes->select(['total' => $creditNotes->func()->sum('total')])->first();

                $payments = $this->_ctrl->Payments->find()
                    ->where([
                        'connection_id' => $connection->id,
                        'anulated IS' => null
                    ]);

                $payments = $payments->select(['total' => $payments->func()->sum('import')])->first();

                $connection->debt_month += $debts_total;
                $connection->debt_month -= $discounts_total;
                $connection->debt_month += $invoices->total;
                $connection->debt_month += $debitNotes->total;
                $connection->debt_month -= $payments->total;
                $connection->debt_month -= $creditNotes->total;
                $connection->debt_month -= $customersHasDiscounts->total;

                $connection->debt_month = round($connection->debt_month, 2);

                //deuda total  ****

                $connection->debt_total = 0;

                $debts_total = 0;
                $discounts_total = 0;

                $debts = $this->_ctrl->Debts->find()
                    ->contain([
                        'CustomersHasDiscounts'
                    ])
                    ->where([
                        'Debts.connection_id' => $connection->id,
                        'Debts.invoice_id IS' => NULL,
                    ]);

                foreach ($debts as $debt) {

                    if ($debt->customers_has_discount) {
                        $discounts_total += $debt->customers_has_discount->total;
                    }
                    $debts_total += $debt->total;
                }

                $customersHasDiscounts = $this->_ctrl->CustomersHasDiscounts->find()
                    ->where([
                        'connection_id' => $connection->id,
                        'debt_id IS' => NULL,
                        'invoice_id IS' => NULL,
                    ]);

                $customersHasDiscounts = $customersHasDiscounts->select([
                    'total' => $customersHasDiscounts->func()->sum('total')
                ])->first();

                $invoices = $this->_ctrl->Invoices->find()
                    ->where([
                        'connection_id' => $connection->id,
                    ]);

                $invoices = $invoices->select([
                        'total' => $invoices->func()->sum('total')
                    ])->first();

                $debitNotes = $this->_ctrl->DebitNotes->find()
                    ->where([
                        'connection_id' => $connection->id,
                    ]);

                $debitNotes = $debitNotes->select(['total' => $debitNotes->func()->sum('total')])->first();

                $creditNotes = $this->_ctrl->CreditNotes->find()
                    ->where([
                        'connection_id' => $connection->id
                    ]);

                $creditNotes = $creditNotes->select(['total' => $creditNotes->func()->sum('total')])->first();

                $payments = $this->_ctrl->Payments->find()
                    ->where([
                        'connection_id' => $connection->id,
                        'anulated IS' => null
                    ]);

                $payments = $payments->select(['total' => $payments->func()->sum('import')])->first();

                $connection->debt_total += $debts_total;
                $connection->debt_total -= $discounts_total;
                $connection->debt_total += $invoices->total;
                $connection->debt_total += $debitNotes->total;
                $connection->debt_total -= $payments->total;
                $connection->debt_total -= $creditNotes->total;
                $connection->debt_total -= $customersHasDiscounts->total;

                $connection->debt_total = round($connection->debt_total, 2);

                if ($save) {

                    if (!$this->_ctrl->Connections->save($connection)) {
                        $this->_ctrl->Flash->error(__('Error al intentar actualizar la deuda mensual de la conexión.'));
                        return false;
                    }

                    $this->updateDebtCustomer($connection->customer_code);

                    $this->controlInvoicesPaid($connection->customer_code, $connection->id);

                    $this->updateAmountInvoincesNoPaid($connection->customer_code);
                }
            }
        }
    }

    private function controlInvoicesPaid($customer_code, $connection_id = null)
    {
        $this->_ctrl->loadModel('Invoices');
        $this->_ctrl->loadModel('Receipts');
        $this->_ctrl->loadModel('CreditNotes');
        $this->_ctrl->loadModel('DebitNotes');
        $this->_ctrl->loadModel('Connections');

        if ($connection_id) {

            $this->_ctrl->Invoices->updateAll(
                [  
                    'paid' => NULL,
                ],
                [  
                    'paid IS NOT' => NULL,
                    'connection_id' => $connection_id,
                    'customer_code' => $customer_code
                ]
            );

            $invoices = $this->_ctrl->Invoices->find()
                ->where([
                    'connection_id' => $connection_id,
                    'customer_code' => $customer_code
                    ])->map(function ($row) { 
                            $row->type = 'invoice';
                        return $row;
                    })->toArray();
  
            $receipts = $this->_ctrl->Receipts->find()->contain(['Payments'])
                ->where([
                        'Receipts.connection_id' => $connection_id,
                        'Receipts.customer_code' => $customer_code,
                        'Payments.anulated IS' => NULL
                        ])->map(function ($row) { 
                                $row->type = 'receipt';
                            return $row;
                        })->toArray();

            $creditNotes = $this->_ctrl->CreditNotes->find()
                ->where([
                    'connection_id' => $connection_id,
                    'customer_code' => $customer_code
                    ])->map(function ($row) { 
                            $row->type = 'credit';
                        return $row;
                    })->toArray();

            $debitNotes = $this->_ctrl->DebitNotes->find()
                ->where([
                    'connection_id' => $connection_id,
                    'customer_code' => $customer_code
                    ])->map(function ($row) { 
                            $row->type = 'debit';
                        return $row;
                    })->toArray();
        } else {

             $this->_ctrl->Invoices->updateAll(
                [  
                    'paid' => NULL,
                ],
                [  
                    'paid IS NOT' => NULL,
                    'connection_id IS' => NULL,
                    'customer_code' => $customer_code
                ]
            );

            $invoices = $this->_ctrl->Invoices->find()
                ->where([
                    'connection_id IS' => NULL,
                    'customer_code' => $customer_code
                    ])->map(function ($row) { 
                            $row->type = 'invoice';
                        return $row;
                    })->toArray();

            $receipts = $this->_ctrl->Receipts->find()->contain(['Payments'])
                ->where([
                        'Receipts.connection_id IS' => NULL,
                        'Receipts.customer_code' => $customer_code,
                        'Payments.anulated IS' => NULL
                        ])->map(function ($row) { 
                                $row->type = 'receipt';
                            return $row;
                        })->toArray();

            $creditNotes = $this->_ctrl->CreditNotes->find()
                ->where([
                    'connection_id IS' => NULL,
                    'customer_code' => $customer_code
                    ])->map(function ($row) { 
                            $row->type = 'credit';
                        return $row;
                    })->toArray();

            $debitNotes = $this->_ctrl->DebitNotes->find()
                ->where([
                    'connection_id IS' => NULL,
                    'customer_code' => $customer_code
                    ])->map(function ($row) { 
                            $row->type = 'debit';
                        return $row;
                    })->toArray();
        }

        $comprobantes = array_merge($invoices, $receipts, $creditNotes, $debitNotes); 

        $order_asc = function($a, $b)  {

              if ($a['date'] < $b['date']) {
                  return -1;
              } else if ($a['date'] > $b['date']) {
                  return 1;
              } else {
                  return 0;
              }
        };

        usort($comprobantes, $order_asc);

        $saldo_paid = 0;
        $invoice_array = [];

        //recorre todos los comprabantes del cliente
        foreach ($comprobantes as $comprobante) {

            switch ($comprobante['type']) {          

                case 'invoice':

                    $comprobante['saldo'] = $comprobante['total'];
                    $invoice_array[$comprobante['id']] = $comprobante;

                    //recorro las facturas gurdadas para ver si me alxzan para pagar alguna
                    foreach ($invoice_array as $key => $invoice) {

                        //verifico si la factura ya no se marco como pagada en un ciclo anterior
                         if (!$invoice['paid']) {

                             //si me alcanza para pagar una factura
                            if ($invoice['total'] <= $saldo_paid) {

                                //descuento el valor de la factura de mi saldo disponible
                                $saldo_paid -= $invoice['total'];
                                //marco a la factura 
                                $invoice['paid'] = $comprobante->date;
                            }
                         }
                    }

                    break;

                case 'receipt':
                case 'credit':

                    //si es recibo o nota de credito

                    //acumulo el saldo diponible para pagar facturas
                    $saldo_paid += $comprobante['total'];

                    //recorre las facturas para ver si me alcanza para pagar alguna con el saldo diponible nuevo
                    foreach ($invoice_array as $key => $invoice) {

                        //verifico si la factura ya no se marco como pagada en un ciclo anterior
                        if (!$invoice['paid']) {

                            //si me alcanza para pagar una factura
                            if ($invoice['total'] <= $saldo_paid) {

                                //descuento el valor de la factura de mi saldo disponible
                                $saldo_paid -= $invoice['total'];
                                //marco a la factura 
                                $invoice['paid'] = $comprobante->date;
                            }
                        }
                    }

                    break;

                case 'debit':

                    //si es una nota de debito tengo que descontar del saldo diponioble para pagos
                    $saldo_paid -= $comprobante['total'];

                    break;
            }
        }

        //recorro las facturas guardadas 
        foreach ($invoice_array as $key => $invoice) {

            //si estan marcadas como pagadas
            if ($invoice['paid']) {

                //actulizo la factura en la base de datos
                $invoiceObj = $this->_ctrl->Invoices->get($invoice['id']);
                $invoiceObj->paid = $invoice['paid'];
                $this->_ctrl->Invoices->save($invoiceObj);
            }
        }
    }

    private function updateAmountInvoincesNoPaid($customer_code)
    {
        $this->_ctrl->loadModel('Invoices');
        $this->_ctrl->loadModel('Customers');
        $this->_ctrl->loadModel('Connections');

        $customer = $this->_ctrl->Customers->get($customer_code, [
            'contain' => [
                'Connections'
                ]
            ]);

        $amount_invoicve_no_paid = 0;
        $amount_invoicve_no_paid_acumulated_connection = 0;

        foreach ($customer->connections as $connection) {

            $amount_invoicve_no_paid = $this->_ctrl->Invoices->find()
                ->where([
                    'customer_code' => $connection->customer_code,
                    'paid IS' => NULL,
                    'connection_id' => $connection->id
                ])
                ->count();

            $connection->invoices_no_paid = $amount_invoicve_no_paid;

            $amount_invoicve_no_paid_acumulated_connection += $connection->invoices_no_paid;

            $this->_ctrl->Connections->save($connection);
        }

        $amount_invoicve_no_paid_no_connection = $this->_ctrl->Invoices->find()
            ->where([
                'customer_code' => $customer->code,
                'paid IS' => NULL,
                'connection_id IS' => NULL
            ])->count();

        $customer->invoices_no_paid = $amount_invoicve_no_paid_no_connection + $amount_invoicve_no_paid_acumulated_connection;

        $this->_ctrl->Customers->save($customer);
    }

    public function getDebtMonthNoConnection($customer_code)
    {
        $this->_ctrl->loadModel('Customers');
        $this->_ctrl->loadModel('Debts');
        $this->_ctrl->loadModel('CustomersHasDiscounts');
        $this->_ctrl->loadModel('DebitNotes');
        $this->_ctrl->loadModel('Invoices');
        $this->_ctrl->loadModel('Payments');
        $this->_ctrl->loadModel('CreditNotes');
        $this->_ctrl->loadModel('DebitNotes');

        $debt_month = 0;

        $firstDayNexMonth = Time::now();
        $firstDayNexMonth->modify('+1 month')->day(1)->hour(0)->minute(0)->second(0);

        $debts_total = 0;
        $discounts_total = 0;

        $debts = $this->_ctrl->Debts->find()
            ->contain([
                'CustomersHasDiscounts'
            ])
            ->where([
                'Debts.connection_id IS' => NULL,
                'Debts.customer_code' => $customer_code,
                'Debts.invoice_id IS' => NULL,
                'Debts.duedate <' => $firstDayNexMonth->format('Y-m-d H:i:s')
            ]);

        foreach ($debts as $debt) {
            
            if ($debt->customers_has_discount) { 
                $discounts_total += $debt->customers_has_discount->total;
            }
            $debts_total += $debt->total;
        }

        $customersHasDiscounts = $this->_ctrl->CustomersHasDiscounts->find()
            ->where([
                'connection_id IS' => NULL,
                'customer_code' => $customer_code,
                'invoice_id IS' => NULL,
                'debt_id IS' => NULL
                ]);

        $customersHasDiscounts = $customersHasDiscounts->select(['total' => $customersHasDiscounts->func()->sum('total')])->first();

        $invoices = $this->_ctrl->Invoices->find()
            ->where([
                'connection_id IS' => NULL,
                'customer_code' => $customer_code,
                'duedate <' => $firstDayNexMonth->format('Y-m-d H:i:s')
            ]);

        $invoices = $invoices->select(['total' => $invoices->func()->sum('total')])->first();

        $debitNotes = $this->_ctrl->DebitNotes->find()
            ->where([
                'connection_id IS' => NULL,
                'customer_code' => $customer_code,
                'duedate <' => $firstDayNexMonth->format('Y-m-d H:i:s')
            ]);

        $debitNotes = $debitNotes->select(['total' => $debitNotes->func()->sum('total')])->first();

        $creditNotes = $this->_ctrl->CreditNotes->find()
            ->where([
                'connection_id IS' => NULL,
                'customer_code' => $customer_code
            ]);

        $creditNotes = $creditNotes->select(['total' => $creditNotes->func()->sum('total')])->first();

        $payments = $this->_ctrl->Payments->find()
            ->where([
                'connection_id IS' => NULL,
                'customer_code' => $customer_code,
                'anulated IS' => NULL
            ]);

        $payments = $payments->select(['total' => $payments->func()->sum('import')])->first();

        $debt_month += $debts_total;
        $debt_month -= $discounts_total;
        $debt_month += $invoices->total;
        $debt_month += $debitNotes->total;
        $debt_month -= $payments->total;
        $debt_month -= $creditNotes->total;
        $debt_month -= $customersHasDiscounts->total;

        $debt_month = round($debt_month, 2);

        return $debt_month;
    }

    public function getDebtMonthNoConnectionFacturado($customer_code)
    {
        $this->_ctrl->loadModel('Customers');
        $this->_ctrl->loadModel('DebitNotes');
        $this->_ctrl->loadModel('Invoices');
        $this->_ctrl->loadModel('Payments');
        $this->_ctrl->loadModel('CreditNotes');
        $this->_ctrl->loadModel('DebitNotes');

        $debt_month = 0;

        $firstDayNexMonth = Time::now();
        $firstDayNexMonth->modify('+1 month')->day(1)->hour(0)->minute(0)->second(0);

        $invoices = $this->_ctrl->Invoices->find()
            ->where([
                'connection_id IS' => NULL,
                'customer_code'    => $customer_code,
                'duedate <'        => $firstDayNexMonth->format('Y-m-d H:i:s')
            ]);

        $invoices = $invoices->select(['total' => $invoices->func()->sum('total')])->first();

        $debitNotes = $this->_ctrl->DebitNotes->find()
            ->where([
                'connection_id IS' => NULL,
                'customer_code'    => $customer_code,
                'duedate <'        => $firstDayNexMonth->format('Y-m-d H:i:s')
            ]);

        $debitNotes = $debitNotes->select(['total' => $debitNotes->func()->sum('total')])->first();

        $creditNotes = $this->_ctrl->CreditNotes->find()
            ->where([
                'connection_id IS' => NULL,
                'customer_code'    => $customer_code
            ]);

        $creditNotes = $creditNotes->select(['total' => $creditNotes->func()->sum('total')])->first();

        $payments = $this->_ctrl->Payments->find()
            ->where([
                'connection_id IS' => NULL,
                'customer_code'    => $customer_code,
                'anulated IS'      => NULL
            ]);

        $payments = $payments->select(['total' => $payments->func()->sum('import')])->first();

        $debt_month += $invoices->total;
        $debt_month += $debitNotes->total;
        $debt_month -= $payments->total;
        $debt_month -= $creditNotes->total;

        $debt_month = round($debt_month, 2);

        return $debt_month;
    }

    public function getDebtMonthNoConnectionSinFacturado($customer_code)
    {
        $this->_ctrl->loadModel('Customers');
        $this->_ctrl->loadModel('Debts');
        $this->_ctrl->loadModel('CustomersHasDiscounts');

        $debt_month = 0;

        $firstDayNexMonth = Time::now();
        $firstDayNexMonth->modify('+1 month')->day(1)->hour(0)->minute(0)->second(0);

        $debts_total = 0;
        $discounts_total = 0;

        $debts = $this->_ctrl->Debts->find()
            ->contain([
                'CustomersHasDiscounts'
            ])
            ->where([
                'Debts.connection_id IS' => NULL,
                'Debts.customer_code'    => $customer_code,
                'Debts.invoice_id IS'    => NULL,
                'Debts.duedate <'        => $firstDayNexMonth->format('Y-m-d H:i:s')
            ]);

        foreach ($debts as $debt) {

            if ($debt->customers_has_discount) { 
                $discounts_total += $debt->customers_has_discount->total;
            }
            $debts_total += $debt->total;
        }

        $customersHasDiscounts = $this->_ctrl->CustomersHasDiscounts->find()
            ->where([
                'connection_id IS' => NULL,
                'customer_code'    => $customer_code,
                'invoice_id IS'    => NULL,
                'debt_id IS'       => NULL
            ]);

        $customersHasDiscounts = $customersHasDiscounts->select(['total' => $customersHasDiscounts->func()->sum('total')])->first();

        $debt_month += $debts_total;
        $debt_month -= $discounts_total;
        $debt_month -= $customersHasDiscounts->total;

        $debt_month = round($debt_month, 2);

        return $debt_month;
    }

    public function getDebtMonthFacturado($connection_id)
    {
        $this->_ctrl->loadModel('DebitNotes');
        $this->_ctrl->loadModel('Invoices');
        $this->_ctrl->loadModel('Payments');
        $this->_ctrl->loadModel('CreditNotes');
        $this->_ctrl->loadModel('DebitNotes');

        $debt_month = 0;

        $firstDayNexMonth = Time::now();
        $firstDayNexMonth->modify('+1 month')->day(1)->hour(0)->minute(0)->second(0);

        $invoices = $this->_ctrl->Invoices->find()
            ->where([
                'connection_id' => $connection_id,
                'duedate <'     => $firstDayNexMonth->format('Y-m-d H:i:s')
            ]);

        $invoices = $invoices->select(['total' => $invoices->func()->sum('total')])->first();

        $debitNotes = $this->_ctrl->DebitNotes->find()
            ->where([
                'connection_id' => $connection_id,
                'duedate <'     => $firstDayNexMonth->format('Y-m-d H:i:s')
            ]);

        $debitNotes = $debitNotes->select(['total' => $debitNotes->func()->sum('total')])->first();

        $creditNotes = $this->_ctrl->CreditNotes->find()
            ->where([
                'connection_id' => $connection_id
            ]);

        $creditNotes = $creditNotes->select(['total' => $creditNotes->func()->sum('total')])->first();

        $payments = $this->_ctrl->Payments->find()
            ->where([
                'connection_id' => $connection_id,
                'anulated IS'   => NULL
            ]);

        $payments = $payments->select(['total' => $payments->func()->sum('import')])->first();

        $debt_month += $invoices->total;
        $debt_month += $debitNotes->total;
        $debt_month -= $payments->total;
        $debt_month -= $creditNotes->total;

        $debt_month = round($debt_month, 2);

        return $debt_month;
    }

    public function getDebtMonthSinFacturado($connection_id)
    {
        $this->_ctrl->loadModel('Debts');
        $this->_ctrl->loadModel('CustomersHasDiscounts');

        $debt_month = 0;

        $firstDayNexMonth = Time::now();
        $firstDayNexMonth->modify('+1 month')->day(1)->hour(0)->minute(0)->second(0);

        $debts_total = 0;
        $discounts_total = 0;

        $debts = $this->_ctrl->Debts->find()
            ->contain([
                'CustomersHasDiscounts'
            ])
            ->where([
                'Debts.connection_id' => $connection_id,
                'Debts.invoice_id IS' => NULL,
                'Debts.duedate <'     => $firstDayNexMonth->format('Y-m-d H:i:s')
            ]);

        foreach ($debts as $debt) {

            if ($debt->customers_has_discount) { 
                $discounts_total += $debt->customers_has_discount->total;
            }
            $debts_total += $debt->total;
        }

        $customersHasDiscounts = $this->_ctrl->CustomersHasDiscounts->find()
            ->where([
                'connection_id' => $connection_id,
                'invoice_id IS' => NULL,
                'debt_id IS'    => NULL
            ]);

        $customersHasDiscounts = $customersHasDiscounts->select(['total' => $customersHasDiscounts->func()->sum('total')])->first();

        $debt_month += $debts_total;
        $debt_month -= $discounts_total;
        $debt_month -= $customersHasDiscounts->total;

        $debt_month = round($debt_month, 2);

        return $debt_month;
    }

    private function getDebtTotalNoConnection($customer_code)
    {
        $this->_ctrl->loadModel('Customers');
        $this->_ctrl->loadModel('Debts');
        $this->_ctrl->loadModel('CustomersHasDiscounts');
        $this->_ctrl->loadModel('DebitNotes');
        $this->_ctrl->loadModel('Invoices');
        $this->_ctrl->loadModel('Payments');
        $this->_ctrl->loadModel('CreditNotes');
        $this->_ctrl->loadModel('DebitNotes');

        $total = 0;
       
        $debts_total = 0;
        $discounts_total = 0;

        $debts = $this->_ctrl->Debts->find()
            ->contain([
                'CustomersHasDiscounts'
            ])
            ->where([
                'Debts.connection_id IS' => NULL,
                'Debts.customer_code' => $customer_code,
                'Debts.invoice_id IS' => NULL,
            ]);

        foreach ($debts as $debt) {
            
            if ($debt->customers_has_discount) { 
                $discounts_total += $debt->customers_has_discount->total;
            }
            $debts_total += $debt->total;
        }

        $customersHasDiscounts = $this->_ctrl->CustomersHasDiscounts->find()
            ->where([
                'connection_id IS' => NULL,
                'customer_code' => $customer_code,
                'invoice_id IS' => NULL,
                'debt_id IS' => NULL
                ]);

        $customersHasDiscounts = $customersHasDiscounts->select(['total' => $customersHasDiscounts->func()->sum('total')])->first();

        $invoices = $this->_ctrl->Invoices->find()
            ->where([
                'connection_id IS' => NULL,
                'customer_code' => $customer_code,

            ]);

        $invoices = $invoices->select(['total' => $invoices->func()->sum('total')])->first();

        $debitNotes = $this->_ctrl->DebitNotes->find()
            ->where([
                'connection_id IS' => NULL,
                'customer_code' => $customer_code,
        
            ]);

        $debitNotes = $debitNotes->select(['total' => $debitNotes->func()->sum('total')])->first();

        $creditNotes = $this->_ctrl->CreditNotes->find()
            ->where([
                'connection_id IS' => NULL,
                'customer_code' => $customer_code
            ]);

        $creditNotes = $creditNotes->select(['total' => $creditNotes->func()->sum('total')])->first();

        $payments = $this->_ctrl->Payments->find()
            ->where([
                'connection_id IS' => NULL,
                'customer_code' => $customer_code,
                'anulated IS' => NULL
            ]);

        $payments = $payments->select(['total' => $payments->func()->sum('import')])->first();

        $total += $debts_total;
        $total -= $discounts_total;
        $total += $invoices->total;
        $total += $debitNotes->total;
        $total -= $payments->total;
        $total -= $creditNotes->total;
        $total -= $customersHasDiscounts->total;

        $total = round($total, 2);

        return $total;
    }

    private function updateDebtCustomer($customer_code)
    {
        $this->_ctrl->loadModel('Connections');
        $this->_ctrl->loadModel('Customers');

        $connections = $this->_ctrl->Connections->find()->where(['customer_code' => $customer_code]);
        $connections = $connections->select([
            'total_debt_month' => $connections->func()->sum('debt_month'),
            'total_debt' => $connections->func()->sum('debt_total')
        ])->first();

        $customer = $this->_ctrl->Customers->get($customer_code);
        $customer->debt_month = $connections->total_debt_month + $this->getDebtMonthNoConnection($customer_code);
        $customer->total_debt = $connections->total_debt + $this->getDebtTotalNoConnection($customer_code);
        $this->_ctrl->Customers->save($customer);

        // $this->_ctrl->Customers->updateAll(
        //     [
        //         'debt_month' => $connections->total_debt_month + $this->getDebtMonthNoConnection($customer_code),
        //         'debt_total' => $connections->total_debt + $this->getDebtTotalNoConnection($customer_code),
        //     ],
        //     [  
        //         'code' => $customer_code
        //     ]
        // );
    }

    public function getDebtMonthCustomerFromConnections($customer)
    {
        $this->_ctrl->loadModel('Customers');
        $this->_ctrl->loadModel('Invoices');
        $this->_ctrl->loadModel('Payments');
        $this->_ctrl->loadModel('CreditNotes');
        $this->_ctrl->loadModel('DebitNotes');

        if ($customer) {

            $now = Time::now();

            $now->modify('+1 month')->day(1)->hour(0)->minute(0)->second(0);

            $debt_month = 0;

            $invoices = $this->_ctrl->Invoices->find()->where([
                'customer_code' => $customer->code ,
                'connection_id IS NOT' => NULL,
                'duedate <' => $now->format('Y-m-d H:i:s')
            ]);
            $invoices = $invoices->select(['total' => $invoices->func()->sum('total')])->first();

            $debitNotes = $this->_ctrl->DebitNotes->find()->where([
                'customer_code' => $customer->code,
                'connection_id IS NOT' => NULL,
            ]);

            $debitNotes_total = 0;

            foreach ($debitNotes as $debitNote) {
                $debitNotes_total += $debitNote->total;
            }

            $payments = $this->_ctrl->Payments->find()->where([
                'customer_code' => $customer->code,
                'connection_id IS NOT' => NULL,
                'anulated IS' => null 
            ]);
            $payments = $payments->select(['total' => $payments->func()->sum('import')])->first();

            $creditNotes = $this->_ctrl->CreditNotes->find()->where([
                'customer_code' => $customer->code,
                'connection_id IS NOT' => NULL,
            ]);

            $creditNotes_total = 0;

            foreach ($creditNotes as $creditNote) {
                $creditNotes_total += $creditNote->total;
            }

            $debt_month += $invoices->total;
            $debt_month += $debitNotes_total;
            $debt_month -= $payments->total;
            $debt_month -= $creditNotes_total;

            $debt_month = round($debt_month, 2);
        }

        return $debt_month;
    }

    public function get_number_of_days_in_month($month, $year)
    {
        $date = $year . "-" . $month . "-1";
        return date("t", strtotime($date));
    }

    public function calulateIvaAndNeto($total, $aliquot_per, $quantity)
    {
        $neto = $total / ($aliquot_per + 1);

        $neto = round($neto, 2);

        $iva = $total - $neto;

        $iva2 = $neto * $aliquot_per;

        $iva = round($iva, 2);
        $iva2 = round($iva2, 2);

        if ($iva < $iva2) {
            $iva = $iva2;
        }

        $price = $neto / $quantity;

        $price = round($price, 2);

        $response = new \stdClass;

        $response->quantity = $quantity;
        $response->price = $price;
        $response->sum_price = $neto;
        $response->sum_tax = $iva;
        $response->total = $total;

        return $response;
    }

    public function calulateCustomerDebtTotal($customer_code)
    {
        $debts_total = 0;
        $discounts_total = 0;

        $this->_ctrl->loadModel('Customers');

        $debts = $this->_ctrl->Customers->Debts->find()->contain(['CustomersHasDiscounts'])->where(['Debts.customer_code' => $customer_code, 'Debts.invoice_id IS' => NULL]);

        foreach ($debts as $debt) {

            if ($debt->customers_has_discount) {
                $discounts_total += $debt->customers_has_discount->total;
            }
            $debts_total += $debt->total;
        }

        $customersHasDiscounts = $this->_ctrl->Customers->CustomersHasDiscounts->find()->where(['customer_code' => $customer_code, 'debt_id IS' => NULL, 'invoice_id IS' => NULL]);
        $customersHasDiscounts = $customersHasDiscounts->select(['total' => $customersHasDiscounts->func()->sum('total')])->first();

        $invoices = $this->_ctrl->Customers->Invoices->find()->where(['customer_code' => $customer_code]);
        $invoices = $invoices->select(['total' => $invoices->func()->sum('total')])->first();

        $debitNotes = $this->_ctrl->Customers->DebitNotes->find()->where(['customer_code' => $customer_code]);
        $debitNotes = $debitNotes->select(['total' => $debitNotes->func()->sum('total')])->first();

        $creditNotes = $this->_ctrl->Customers->CreditNotes->find()->where(['customer_code' => $customer_code]);
        $creditNotes = $creditNotes->select(['total' => $creditNotes->func()->sum('total')])->first();

        $payments = $this->_ctrl->Customers->Payments->find()->where(['customer_code' => $customer_code, 'anulated IS' => null ]);
        $payments = $payments->select(['total' => $payments->func()->sum('import')])->first();

        $debts_total -= $discounts_total;
        $debts_total += $invoices->total;
        $debts_total += $debitNotes->total;
        $debts_total -= $payments->total;
        $debts_total -= $creditNotes->total;
        $debts_total -= $customersHasDiscounts->total;

        return round($debts_total, 2);
    }
}
