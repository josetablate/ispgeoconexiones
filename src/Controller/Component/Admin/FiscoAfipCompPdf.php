<?php
namespace App\Controller\Component\Admin;

use App\Controller\Component\Common\MyComponent;

use Cake\Network\Exception\NotFoundException;
use Cake\Event\EventManager;
use Cake\Event\Event;
use Cake\Filesystem\Folder;
use Cake\I18n\Time;
use App\Controller\Component\Admin\Pdf\InvoicePdf;
use App\Controller\Component\Admin\Pdf\CreditNotePdf;
use App\Controller\Component\Admin\Pdf\DebitNotePdf;
use App\Controller\Component\Admin\Pdf\ReceiptPdf;
use App\Controller\Component\Admin\Pdf\PresupuestoPdf;

/**
 * Documents component
 */
class FiscoAfipCompPdf extends MyComponent
{
    const TYPE_RECEIPT = 0;
    const TYPE_INVOICE = 1;
    const TYPE_CREDIT_NOTE = 2;
    const TYPE_DEBIT_NOTE = 3;
    const TYPE_PRESUPUESTO = 4;
    const TYPE_ACCOUNT_SUMMARY = 5;

    public $components = [
        'CobroDigital',
    ];

    public function initialize(array $config)
    {
        parent::initialize($config);
        
        $this->_ctrl->loadModel('Invoices');
        $this->_ctrl->loadModel('Presupuestos');
        
        $this->_ctrl->loadModel('Receipts');
        $this->_ctrl->loadModel('CreditNotes');
        $this->_ctrl->loadModel('DebitNotes');
        $this->_ctrl->loadModel('Customers');

        $this->initEventManager();
    }

    public function initEventManager()
    {
        EventManager::instance()->on(
            'FiscoAfipCompPdf.Error',
            function($event, $msg, $data, $flash = false) {

                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $log->user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Error';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);

                if ($flash) {
                    $this->_ctrl->Flash->error(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'FiscoAfipCompPdf.Warning',
            function($event, $msg, $data, $flash = false) {

                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $log->user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Warning';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);

                if ($flash) {
                    $this->_ctrl->Flash->warning(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'FiscoAfipCompPdf.Log',
            function($event, $msg, $data) {

            }
        );

        EventManager::instance()->on(
            'FiscoAfipCompPdf.Notice',
            function($event, $msg, $data) {

            }
        );
    }

    public function view($data, $type)
    {
        $paraments = $this->_ctrl->request->getSession()->read('paraments');

        switch ($type) {

            case FiscoAfipCompPdf::TYPE_RECEIPT: {

                $receipt = $this->_ctrl->Receipts->get($data->id);

                if ($receipt->printed) {

                    try {

                        if ($paraments->invoicing->auto_print) {
                            return $receipt->printed;
                        }

                        return $this->response->withFile($receipt->printed);

                    } catch (NotFoundException $e) {

                        $data_error = new \stdClass; 
                        $data_error->receipt = $receipt;

                        $event = new Event('FiscoAfipCompPdf.Error', $this, [
                        	'msg' => __('No se encuentra el archivo relacionado al recibo.'),
                        	'data' => $data_error,
                        	'flash' => true
                        ]);

                        $this->_ctrl->getEventManager()->dispatch($event);

                        $this->_ctrl->redirect($this->_ctrl->referer());
                    }
                }

                break;
            }

            case FiscoAfipCompPdf::TYPE_INVOICE: {

                $invoice = $this->_ctrl->Invoices->get($data->id);

                if ($invoice->printed) {

                    try {
                        return $this->response->withFile($invoice->printed);
                    }
                    catch (NotFoundException $e) {

                        $data_error = new \stdClass; 
                        $data_error->invoice = $invoice;

                        $event = new Event('FiscoAfipCompPdf.Error', $this, [
                        	'msg' => __('No se encuentra el archivo relacionado a la factura.'),
                        	'data' => $data_error,
                        	'flash' => true
                        ]);

                        $this->_ctrl->getEventManager()->dispatch($event);

                        $this->_ctrl->redirect($this->_ctrl->referer());
                    }
                }
                break;
            }

            case FiscoAfipCompPdf::TYPE_CREDIT_NOTE: {

                $creditNote = $this->_ctrl->CreditNotes->get($data->id);

                if ($creditNote->printed) {

                    try {

                        return $this->response->withFile($creditNote->printed);

                    } catch (NotFoundException $e) {
                        
                        $data_error = new \stdClass; 
                        $data_error->creditNote = $creditNote;
                        
                        $event = new Event('FiscoAfipCompPdf.Error', $this, [
                        	'msg' => __('No se encuentra el archivo relacionado a la nota de crédito.'),
                        	'data' => $data_error,
                        	'flash' => true
                        ]);

                        $this->_ctrl->getEventManager()->dispatch($event);

                        $this->_ctrl->redirect($this->_ctrl->referer());

                    }
                }
                break;
            }
                
            case FiscoAfipCompPdf::TYPE_DEBIT_NOTE: {

                $debitNote = $this->_ctrl->DebitNotes->get($data->id);

                if ($debitNote->printed) {

                    try {

                        return $this->response->withFile($debitNote->printed);
                    } catch (NotFoundException $e) {

                        $data_error = new \stdClass; 
                        $data_error->debitNote = $debitNote;

                        $event = new Event('FiscoAfipCompPdf.Error', $this, [
                        	'msg' => __('No se encuentra el archivo relacionado a la nota de débito.'),
                        	'data' => $data_error,
                        	'flash' => true
                        ]);

                        $this->_ctrl->getEventManager()->dispatch($event);

                        $this->_ctrl->redirect($this->_ctrl->referer());
                    }
                }
                break;
            }

            case FiscoAfipCompPdf::TYPE_PRESUPUESTO: {

                $presupuesto = $this->_ctrl->Presupuestos->get($data->id);

                if ($presupuesto->printed) {

                    try {
                        return $this->response->withFile($presupuesto->printed);
                    } catch (NotFoundException $e) {

                        $data_error = new \stdClass; 
                        $data_error->presupuesto = $presupuesto;

                        $event = new Event('FiscoAfipCompPdf.Error', $this, [
                        	'msg' => __('No se encuentra el archivo relacionado al prespuesto.'),
                        	'data' => $data_error,
                        	'flash' => true
                        ]);

                        $this->_ctrl->getEventManager()->dispatch($event);

                        $this->_ctrl->redirect($this->_ctrl->referer());
                    }
                }
                break;
            }
        }

        return true;
    }

    public function invoice($data, $send_email = false, $download = false, $unix_download = null)
    {
        $data->invoice = $this->_ctrl->Invoices->get($data->id, ['contain' => []]);

        $data->invoice->concepts = $this->_ctrl->Invoices->Concepts->find()->where(['comprobante_id' => $data->invoice->id])->toArray();

        $data->paraments = $this->_ctrl->request->getSession()->read('paraments');

        foreach ($data->paraments->invoicing->business as $b) {
            if ($b->id == $data->invoice->business_id) {
                 $data->invoice->business = $b;
            }
        }

        $data->responsibles = $this->_ctrl->request->getSession()->read('afip_codes')['responsibles'];
        $data->doc_types = $this->_ctrl->request->getSession()->read('afip_codes')['doc_types'];
        $data->cond_venta = $this->_ctrl->request->getSession()->read('afip_codes')['cond_venta'];
        $data->units_types = $this->_ctrl->request->getSession()->read('afip_codes')['units_types'];
        $data->alicuotas_types = $this->_ctrl->request->getSession()->read('afip_codes')['alicuotas_types'];

        $invoice = new InvoicePdf();

        $invoice->tipo = 'ORIGINAL';
        $invoice->generate($data, $this->_ctrl);

        if ($data->invoice->business->copias > 1) {

            $invoice->tipo = 'DUPLICADO';
            $invoice->generate($data, $this->_ctrl);
        }

        if ($data->invoice->business->copias > 2) {

            $invoice->tipo = 'TRIPLICADO';
            $invoice->generate($data, $this->_ctrl);
        }

        if ($send_email) {

            $folder = new Folder(WWW_ROOT . 'temp_files_mail', TRUE, 0755);

            $filename = WWW_ROOT . 'temp_files_mail/';
            $filename .= Time::now()->toUnixString();
            $filename .= '-invoice-'.str_pad($data->invoice->id, 5, "0", STR_PAD_LEFT);
            $filename .= '.pdf';

            $invoice->Output($filename, 'F');
            return $filename;
        }

        if ($download) {

            $folder = new Folder(WWW_ROOT . "temp_files_downloand/$unix_download", TRUE, 0755);

            $filename = WWW_ROOT . "temp_files_downloand/$unix_download/";
            $filename .= ' ' . $data->invoice->date->format('Ymdhis');
            $filename .= ' ' . sprintf("%'.04d", $data->invoice->pto_vta) . '-' . sprintf("%'.08d", $data->invoice->num);
            $filename .= ' ' . str_pad($data->invoice->customer_code, 5, "0", STR_PAD_LEFT);
            $filename .= ' ' . $data->invoice->tipo_comp;
            $filename .= '.pdf';

            $invoice->Output($filename, 'F');
            return $filename;
        }

        $this->response = $this->response->withCharset('UTF-8');
        $this->response = $this->response->withType('application/pdf');

        $invoice->Output();

        return '';
    }

    public function receipt($data, $send_email = false, $download = false, $unix_download = null)
    {
        $data->receipt = $this->_ctrl->Receipts->get($data->id, ['contain' => ['Connections.Services', 'Users']]);

        $data->paraments = $this->_ctrl->request->getSession()->read('paraments');

        foreach ($data->paraments->invoicing->business as $b) {
            if ($b->id == $data->receipt->business_id) {
                 $data->receipt->business = $b;
            }
        }

        $data->responsibles = $this->_ctrl->request->getSession()->read('afip_codes')['responsibles'];
        $data->doc_types = $this->_ctrl->request->getSession()->read('afip_codes')['doc_types'];

        $receipt = new ReceiptPdf();
        $receipt->generate($data, $this->_ctrl);

        if ($send_email) {

            $folder = new Folder(WWW_ROOT . 'temp_files_mail', TRUE, 0755);

            $filename  = WWW_ROOT . 'temp_files_mail/';
            $filename .= Time::now()->toUnixString();
            $filename .= '-recibo-'.str_pad($data->receipt->id, 5, "0", STR_PAD_LEFT);
            $filename .= '.pdf';

            $receipt->Output($filename, 'F');
            return $filename;
        }

        if ($download) {

            $folder = new Folder(WWW_ROOT . "temp_files_downloand/$unix_download", TRUE, 0755);

            $filename = WWW_ROOT . "temp_files_downloand/$unix_download/";
            $filename .= ' ' . $data->receipt->date->format('Ymdhis');
            $filename .= ' ' . sprintf("%'.04d", $data->receipt->pto_vta) . '-' . sprintf("%'.08d", $data->receipt->num);
            $filename .= ' ' . str_pad($data->receipt->customer_code, 5, "0", STR_PAD_LEFT);
            $filename .= ' ' . $data->receipt->tipo_comp;
            $filename .= '.pdf';

            $receipt->Output($filename, 'F');
            return $filename;
        }

        $this->response = $this->response->withCharset('UTF-8');
        $this->response = $this->response->withType('application/pdf');

        if ($data->paraments->invoicing->auto_print) {
            $receipt->AutoPrint();
        }

        $receipt->Output();

        return '';
    }

    public function creditNote($data, $send_email = false, $download = false, $unix_download = null)
    {
        $data->creditNote = $this->_ctrl->CreditNotes->get($data->id, ['contain' => ['ConceptsCredit']]);

        $data->paraments = $this->_ctrl->request->getSession()->read('paraments');
        
        foreach ($data->paraments->invoicing->business as $b) {
            if ($b->id == $data->creditNote->business_id){
                 $data->creditNote->business = $b;
            }
        }

        $data->responsibles = $this->_ctrl->request->getSession()->read('afip_codes')['responsibles'];
        $data->doc_types = $this->_ctrl->request->getSession()->read('afip_codes')['doc_types'];
        $data->cond_venta = $this->_ctrl->request->getSession()->read('afip_codes')['cond_venta'];
        $data->alicuotas_types = $this->_ctrl->request->getSession()->read('afip_codes')['alicuotas_types'];
        $data->units_types = $this->_ctrl->request->getSession()->read('afip_codes')['units_types'];

        $creditNote = new CreditNotePdf();

        $creditNote->tipo = 'ORIGINAL';
        $creditNote->generate($data, $this->_ctrl);

        if ($data->creditNote->business->copias > 1) {
            
            $creditNote->tipo = 'DUPLICADO';
            $creditNote->generate($data, $this->_ctrl);
        }

        if ($data->creditNote->business->copias > 2) {

            $creditNote->tipo = 'TRIPLICADO';
            $creditNote->generate($data, $this->_ctrl);
        }

        if ($send_email) {

            $folder = new Folder(WWW_ROOT . 'temp_files_mail', TRUE, 0755);

            $filename  = WWW_ROOT . 'temp_files_mail/';
            $filename .= Time::now()->toUnixString();
            $filename .= '-nota-credito-'.str_pad($data->creditNote->id, 5, "0", STR_PAD_LEFT);
            $filename .= '.pdf';

            $creditNote->Output($filename, 'F');
            return $filename;
        }

        if ($download) {

            $folder = new Folder(WWW_ROOT . "temp_files_downloand/$unix_download", TRUE, 0755);

            $filename = WWW_ROOT . "temp_files_downloand/$unix_download/";
            $filename .= ' ' . $data->creditNote->date->format('Ymdhis');
            $filename .= ' ' . sprintf("%'.04d", $data->creditNote->pto_vta) . '-' . sprintf("%'.08d", $data->creditNote->num);
            $filename .= ' ' . str_pad($data->creditNote->customer_code, 5, "0", STR_PAD_LEFT);
            $filename .= ' ' . $data->creditNote->tipo_comp;
            $filename .= '.pdf';

            $creditNote->Output($filename, 'F');
            return $filename;
        }

        $this->response = $this->response->withCharset('UTF-8');
        $this->response = $this->response->withType('application/pdf');

        $creditNote->Output();

        return '';
    }

    public function debitNote($data, $send_email = false, $download = false, $unix_download = null)
    {
        $data->debitNote = $this->_ctrl->DebitNotes->get($data->id, ['contain' => ['ConceptsDebit']]);

        $data->paraments = $this->_ctrl->request->getSession()->read('paraments');

        foreach ($data->paraments->invoicing->business as $b) {
            if ($b->id == $data->debitNote->business_id) {
                 $data->debitNote->business = $b;
            }
        }

        $data->responsibles = $this->_ctrl->request->getSession()->read('afip_codes')['responsibles'];
        $data->doc_types = $this->_ctrl->request->getSession()->read('afip_codes')['doc_types'];
        $data->cond_venta = $this->_ctrl->request->getSession()->read('afip_codes')['cond_venta'];
        $data->alicuotas_types = $this->_ctrl->request->getSession()->read('afip_codes')['alicuotas_types'];
        $data->units_types = $this->_ctrl->request->getSession()->read('afip_codes')['units_types'];

        $debitNote = new DebitNotePdf();

        $debitNote->tipo = 'ORIGINAL';
        $debitNote->generate($data, $this->_ctrl);

        if ($data->debitNote->business->copias > 1) {

            $debitNote->tipo = 'DUPLICADO';
            $debitNote->generate($data, $this->_ctrl);
        }

        if ($data->debitNote->business->copias > 2) {

            $debitNote->tipo = 'TRIPLICADO';
            $debitNote->generate($data, $this->_ctrl);
        }

        if ($send_email) {

            $folder = new Folder(WWW_ROOT . 'temp_files_mail', TRUE, 0755);

            $filename  = WWW_ROOT . 'temp_files_mail/';
            $filename .= Time::now()->toUnixString();
            $filename .= '-nota-debito-'.str_pad($data->debitNote->id, 5, "0", STR_PAD_LEFT);
            $filename .= '.pdf';

            $debitNote->Output($filename, 'F');
            return $filename;
        }

        if ($download) {

            $folder = new Folder(WWW_ROOT . "temp_files_downloand/$unix_download", TRUE, 0755);

            $filename = WWW_ROOT . "temp_files_downloand/$unix_download/";
            $filename .= ' ' . $data->debitNote->date->format('Ymdhis');
            $filename .= ' ' . sprintf("%'.04d", $data->debitNote->pto_vta) . '-' . sprintf("%'.08d", $data->debitNote->num);
            $filename .= ' ' . str_pad($data->debitNote->customer_code, 5, "0", STR_PAD_LEFT);
            $filename .= ' ' . $data->debitNote->tipo_comp;
            $filename .= '.pdf';

            $debitNote->Output($filename, 'F');
            return $filename;
        }

        $this->response = $this->response->withCharset('UTF-8');
        $this->response = $this->response->withType('application/pdf');

        $debitNote->Output();

        return '';
    }

    public function presupuesto($data, $send_email = false)
    {
        $data->presupuesto = $this->_ctrl->Presupuestos->get($data->id, ['contain' => []]);

        $data->presupuesto->concepts = $this->_ctrl->Presupuestos->PresupuestoConcepts->find()->where(['presupuesto_id' => $data->presupuesto->id])->toArray();

        $data->paraments = $this->_ctrl->request->getSession()->read('paraments');

        foreach ($data->paraments->invoicing->business as $b) {
            if ($b->id == $data->presupuesto->business_id) {
                 $data->presupuesto->business = $b;
            }
        }

        $data->responsibles = $this->_ctrl->request->getSession()->read('afip_codes')['responsibles'];
        $data->doc_types = $this->_ctrl->request->getSession()->read('afip_codes')['doc_types'];
        $data->cond_venta = $this->_ctrl->request->getSession()->read('afip_codes')['cond_venta'];
        $data->alicuotas_types = $this->_ctrl->request->getSession()->read('afip_codes')['alicuotas_types'];
        $data->units_types = $this->_ctrl->request->getSession()->read('afip_codes')['units_types'];

        $presupuesto = new PresupuestoPdf();

        $presupuesto->tipo = 'ORIGINAL';
        $presupuesto->generate($data, $this->_ctrl);

        if ($send_email) {

            $folder = new Folder(WWW_ROOT . 'temp_files_mail', TRUE, 0755);

            $filename = WWW_ROOT . 'temp_files_mail/';
            $filename .= Time::now()->toUnixString();
            $filename .= '-presupuesto-'.str_pad($data->presupuesto->id, 5, "0", STR_PAD_LEFT);
            $filename .= '.pdf';

            $presupuesto->Output($filename, 'F');
            return $filename;
        }

        $this->response = $this->response->withCharset('UTF-8');
        $this->response = $this->response->withType('application/pdf');

        $presupuesto->Output();

        return '';
    }

    public function account_summary($customer_code, $send_email = FALSE, $download = FALSE, $unix_download = NULL, $cobrodigital = FALSE, $payu = FALSE, $cuentadigital = FALSE)
    {
        $customer = $this->_ctrl->Customers->get($customer_code, [
            'contain' => [
                'Connections.Services'
            ]
        ]);

        $parament = $this->_ctrl->request->getSession()->read('paraments');
        $afip_codes =  $this->_ctrl->request->getSession()->read('afip_codes');

        //DATOS CLIENTE
        $customer_name = $customer->name;
        $customer_address = $customer->address;
        $customer_phone = $customer->phone;
        $customer_doc_type = $afip_codes['doc_types'][$customer->doc_type];
        $customer_ident = $customer->ident;
        $customer_type = $afip_codes['responsibles'][$customer->responsible];

        //DATOS EMPRESA
        $enterprise_name = "Empresa";
        $enterprise_address = "";
        $enterprise_phone = "";
        $enterprise_logo = WWW_ROOT . "img/logo.png";

        foreach ($parament->invoicing->business as $b) {

            if ($customer != NULL && $customer->business_billing == $b->id) {
                $enterprise_name = $b->name;
                $enterprise_address = $b->address;
                $enterprise_phone = $b->phone;
                $enterprise_logo = WWW_ROOT . 'img/' . $b->logo;
                break;
            } else {

                $enterprise_name = $b->name;
                $enterprise_address = $b->address;
                $enterprise_phone = $b->phone;
                $enterprise_logo = WWW_ROOT . 'img/' . $b->logo;
            }
        }
        $now = Time::now();
        $month = str_pad($now->month, 2, "0", STR_PAD_LEFT);
        $day = str_pad($now->day, 2, "0", STR_PAD_LEFT);
        $now = $day . '/' . $month . '/' . $now->year;

        $this->_ctrl->loadModel('Payments');

        if ($parament->gral_config->billing_for_service) {

            $comprobantes = [];

            $duedate = Time::now();
            $duedate->day(1);
            $duedate->modify('+1 month');

            $invoices = $this->_ctrl->Invoices
                ->find()
                ->where([
                    'customer_code'       => $customer_code,
                    'Invoices.duedate <=' => $duedate
                ])->toArray();

            $credit_notes = $this->_ctrl->CreditNotes
                ->find()
                ->where([
                    'customer_code' => $customer_code
                ])->toArray();

            $debit_notes = $this->_ctrl->DebitNotes
                ->find()
                ->where([
                    'customer_code'         => $customer_code,
                    'DebitNotes.duedate <=' => $duedate
                ])->toArray();

            $payments = $this->_ctrl->Payments
                ->find()
                ->where([
                    'customer_code' => $customer_code,
                    'anulated IS'   => NULL
                ])->toArray();

            $comprobantes = array_merge($invoices, $credit_notes, $debit_notes, $payments);

            $order_asc = function($a, $b)
            {
                $date_a = isset($a->date) ? $a->date : $a->created;
                $date_b = isset($b->date) ? $b->date : $b->created;

                if ($date_a < $date_b) {
                    return -1;
                } else if ($date_a > $date_b) {
                    return 1;
                } else {
                    return 0;
                }
            };

            usort($comprobantes, $order_asc);

            $newData = [];

            $saldo = 0;

            $customer->comprobantes = [];

            foreach ($comprobantes as $comprobante) {

                if (isset($comprobante->total)) {
                    if (in_array($comprobante->tipo_comp, ['NCX', '008', '003', '013'])) {
                        $total = $comprobante->total * -1;
                    } else {
                        $total = $comprobante->total;
                    }
                } else {
                    $total = $comprobante->import * -1;
                }

                $saldo += $total;

                if ($comprobante->connection_id == NULL) {

                    if (isset($comprobante->total)) {
                        if (in_array($comprobante->tipo_comp, ['NCX', '008', '003', '013'])) {
                            $total = $comprobante->total * -1;
                        } else {
                            $total = $comprobante->total;
                        }
                    } else {
                        $total = $comprobante->import * -1;
                    }

                    $customer->other_saldo += $total;
                    $comprobante->saldo = $customer->other_saldo;
                    $customer->comprobantes[] = $comprobante;

                    if ($comprobante->saldo == 0 && !$parament->invoicing->account_summary_complete) {
                        $customer->comprobantes = [];
                    }

                } else {

                    foreach ($customer->connections as $connection) {

                        if (!isset($connection->comprobantes)) {
                            $connection->comprobantes = [];
                        }

                        if ($connection->id == $comprobante->connection_id) {

                            if (isset($comprobante->total)) {
                                if (in_array($comprobante->tipo_comp, ['NCX', '008', '003', '013'])) {
                                    $total = $comprobante->total * -1;
                                } else {
                                    $total = $comprobante->total;
                                }
                            } else {
                                $total = $comprobante->import * -1;
                            }

                            $connection->saldo += $total;
                            $comprobante->saldo = $connection->saldo;
                            $connection->comprobantes[] = $comprobante;

                            if ($connection->saldo == 0 && !$parament->invoicing->account_summary_complete) {
                                $connection->comprobantes = [];
                            }

                            break;
                        }
                    }
                }
            }

            if (sizeof($customer->comprobantes) > 0) {
                $customer->comprobantes = array_reverse($customer->comprobantes);
            }

            foreach ($customer->connections as $connection) {
                if (sizeof($connection->comprobantes) > 0) {
                    $connection->comprobantes = array_reverse($connection->comprobantes);
                } else {
                    $connection->comprobantes = [];
                }
            }

            $class_debt = "color: #f05f40";
            if ($saldo == 0) {
                $class_debt = "color: black";
            } else if ($saldo > 0) {
                $class_debt = "color: #f05f40";
            } else {
                $class_debt = "color: #28a745";
            }

            $saldo = number_format((float)$saldo, 2, ',', '');

            $html = '
            <html>
                <head>
                    <style>
                        body {font-family: sans-serif;
                        	font-size: 10pt;
                        }
                        p {	margin: 0pt; }
                        table.items {
                        	border: 0.1mm solid #000000;
                        }
                        td { vertical-align: top; }
                        .items td {
                        	border-left: 0.1mm solid #000000;
                        	border-right: 0.1mm solid #000000;
                        }
                        table thead td { background-color: #EEEEEE;
                        	text-align: center;
                        	border: 0.1mm solid #000000;
                        	font-variant: small-caps;
                        }
                        .items td.blanktotal {
                        	background-color: #EEEEEE;
                        	border: 0.1mm solid #000000;
                        	background-color: #FFFFFF;
                        	border: 0mm none #000000;
                        	border-top: 0.1mm solid #000000;
                        	border-right: 0.1mm solid #000000;
                        }
                        .items td.totals {
                        	text-align: right;
                        	border: 0.1mm solid #000000;
                        }
                        .items td.cost {
                        	text-align: "." center;
                        }
                        .items-striped {
                            background-color: #dee2e6;
                        }
                        .barcode {
                        	padding: 1.5mm;
                        	margin: 0;
                        	vertical-align: top;
                        	color: #000000;
                        }
                        .barcodecell {
                        	text-align: center;
                        	vertical-align: middle;
                        	padding: 0;
                        }
                    </style>
                </head>
                <body>
                    <!--mpdf
                    <htmlpageheader name="myheader">
                        <table width="100%">
                            <tr>
                                <td width="50%" ><img src="' . $enterprise_logo . '" height="100px"/></td>
                                <td width="50%" style="color:#615d5d; text-align: right;">
                                    <span style="font-weight: bold; font-size: 14pt;">' . $enterprise_name . '</span>
                                    <br />' . $enterprise_address . '
                                    <br />
                                    <span style="font-family:dejavusanscondensed;">&#9742;</span> ' . $enterprise_phone . '
                                </td>
                            </tr>
                        </table>
                    </htmlpageheader>
                    <htmlpagefooter name="myfooter">
                        <div style="border-top: 1px solid #000000; font-size: 9pt; text-align: center; padding-top: 3mm; ">
                            Página {PAGENO} de {nb}
                        </div>
                    </htmlpagefooter>
                    <sethtmlpageheader name="myheader" value="on" show-this-page="1" />
                    <sethtmlpagefooter name="myfooter" value="on" />
                    mpdf-->
                    <div style="text-align: right">Fecha emisión: ' . $now . '</div>
                	<table width="100%" style="font-family: serif;" cellpadding="10">
                		<tr>
                			<td width="100%" style="border: 0.1mm solid #888888; ">
                				<span style="font-size: 18pt; color: #555555; font-family: sans;">Resumen de Cuenta</span>
                				<br />
                				<br /><b>Nombre</b>: ' . $customer_name . '
                				<br /><b>Código</b>: ' . $customer_code . '
                				<br /><b>' . $customer_doc_type . '</b>: ' . $customer_ident . '
                				<br /><b>Domicilio</b>: ' . $customer_address . '
                				<br /><b>Tel.</b>: ' . $customer_phone . '
                				<br /><b>Tipo</b>: ' . $customer_type . '
                				<br />
                				<br /><b style="font-size: 14pt;' . $class_debt . '">Saldo</b>: <span style="font-size: 14pt;">$' . $saldo . '</span>
                			</td>
                		</tr>
                	</table>
                	<br />';

                    foreach ($customer->connections as $connection) {

                        if (sizeof($connection->comprobantes) < 1) {
                            continue;
                        }

                        $plan = $connection->service->name;
                        $address = $connection->address;
                        $saldo = $connection->saldo;

                        $class_debt = "color: #f05f40";
                        if ($saldo == 0) {
                            $class_debt = "color: black";
                        } else if ($saldo > 0) {
                            $class_debt = "color: #f05f40";
                        } else {
                            $class_debt = "color: #28a745";
                        }

                        $saldo = number_format((float)$saldo, 2, ',', '');

                        $html .=
                    	'<table width="100%" style="font-family: serif;" cellpadding="10">
                    		<tr>
                    			<td width="100%" style="border: 0.1mm solid #888888; ">
                    				<span style="font-size: 16pt; color: #555555; font-family: sans;">Servicio</span>
                    				<br />
                    				<br /><b>Plan</b>: ' . $plan . '
                    				<br /><b>Domicilio</b>: ' . $address . '
                    				<br />
                    				<br /><b style="font-size: 14pt;' . $class_debt . '">Saldo</b>: <span style="font-size: 14pt;">$' . $saldo . '</span>
                    			</td>
                    		</tr>
                    	</table>
                    	<br />
                    	<table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse; " cellpadding="8">
                            <thead>
                                <tr>
                                    <td width="12%">Fecha</td>
                                    <td width="38%">Descrip.</td>
                                    <td width="19%">Total</td>
                                    <td width="12%">Vto.</td>
                                    <td width="19%">Saldo</td>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- ITEMS HERE -->';

                                foreach ($connection->comprobantes as $key => $comprobante) {

                                    $date_object = isset($comprobante->date) ? $comprobante->date : $comprobante->created;

                                    $date = str_pad($date_object->day, 2, "0", STR_PAD_LEFT) . '/' . str_pad($date_object->month, 2, "0", STR_PAD_LEFT) . '/' . $date_object->year;

                                    $comments = "";

                                    if (isset($comprobante->comments)) {
                                        $comments = $comprobante->comments;
                                    } else {
                                        $comments = $comprobante->concept;
                                    }

                                    $total = isset($comprobante->total) ? $comprobante->total : ($comprobante->import * -1);
                                    $total = number_format((float)$total, 2, ',', '');

                                    $duedate = "";

                                    if (isset($comprobante->duedate)) {
                                        $duedate = str_pad($comprobante->duedate->day, 2, "0", STR_PAD_LEFT) . '/' . str_pad($comprobante->duedate->month, 2, "0", STR_PAD_LEFT) . '/' . $comprobante->duedate->year;
                                    }

                                    $saldo = $comprobante->saldo;
                                    $saldo = number_format((float)$saldo, 2, ',', '');

                                    $class_striped = "items-striped";

                                    if ($key % 2) {
                                        $class_striped = "";
                                    }

                                    $html .=
                                    '<tr class="' . $class_striped . '">
                                        <td align="center">' . $date . '</td>
                                        <td>' . $comments . '</td>
                                        <td class="cost">$' . $total . '</td>
                                        <td align="center">' . $duedate . '</td>
                                        <td class="cost">$' . $saldo . '</td>
                                    </tr>';
                                }

                        $html .=
                        '</tbody>
                        </table>
                        <br />';
                    }

                    if (sizeof($customer->comprobantes) > 0) {

                        $saldo = $customer->other_saldo;

                        $class_debt = "color: #f05f40";
                        if ($saldo == 0) {
                            $class_debt = "color: black";
                        } else if ($saldo > 0) {
                            $class_debt = "color: #f05f40";
                        } else {
                            $class_debt = "color: #28a745";
                        }

                        $saldo = number_format((float)$saldo, 2, ',', '');

                        $html .=
                    	'<table width="100%" style="font-family: serif;" cellpadding="10">
                    		<tr>
                    			<td width="100%" style="border: 0.1mm solid #888888; ">
                    				<span style="font-size: 16pt; color: #555555; font-family: sans;">Otras Ventas</span>
                    				<br />
                    				<br /><b style="font-size: 14pt;' . $class_debt . '">Saldo</b>: <span style="font-size: 14pt;">$' . $saldo . '</span>
                    			</td>
                    		</tr>
                    	</table>
                    	<br />
                    	<table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse; " cellpadding="8">
                            <thead>
                                <tr>
                                    <td width="12%">Fecha</td>
                                    <td width="38%">Descrip.</td>
                                    <td width="19%">Total</td>
                                    <td width="12%">Vto.</td>
                                    <td width="19%">Saldo</td>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- ITEMS HERE -->';

                                foreach ($customer->comprobantes as $key => $comprobante) {

                                    $date_object = isset($comprobante->date) ? $comprobante->date : $comprobante->created;

                                    $date = str_pad($date_object->day, 2, "0", STR_PAD_LEFT) . '/' . str_pad($date_object->month, 2, "0", STR_PAD_LEFT) . '/' . $date_object->year;

                                    $comments = "";

                                    if (isset($comprobante->comments)) {
                                        $comments = $comprobante->comments;
                                    } else {
                                        $comments = $comprobante->concept;
                                    }

                                    $total = isset($comprobante->total) ? $comprobante->total : ($comprobante->import * -1);
                                    $total = number_format((float)$total, 2, ',', '');

                                    $duedate = "";

                                    if (isset($comprobante->duedate)) {
                                        $duedate = str_pad($comprobante->duedate->day, 2, "0", STR_PAD_LEFT) . '/' . str_pad($comprobante->duedate->month, 2, "0", STR_PAD_LEFT) . '/' . $comprobante->duedate->year;
                                    }

                                    $saldo = $comprobante->saldo;
                                    $saldo = number_format((float)$saldo, 2, ',', '');

                                    $class_striped = "items-striped";

                                    if ($key % 2) {
                                        $class_striped = "";
                                    }

                                    $html .=
                                    '<tr class="' . $class_striped . '">
                                        <td align="center">' . $date . '</td>
                                        <td>' . $comments . '</td>
                                        <td class="cost">$' . $total . '</td>
                                        <td align="center">' . $duedate . '</td>
                                        <td class="cost">$' . $saldo . '</td>
                                    </tr>';
                                }

                        $html .=
                        '</tbody>
                        </table>';
                    }

                    if ($cobrodigital) {

                        $this->_ctrl->loadModel('CobrodigitalAccounts');
                        $cobrodigital_account = $this->_ctrl->CobrodigitalAccounts
                            ->find()
                            ->where([
                                'customer_code'     => $customer_code,
                                'payment_getway_id' => 99,
                                'deleted'           => FALSE
                            ])->first();

                        if ($cobrodigital_account) {

                            $barcode = $cobrodigital_account->barcode;
                            $electronic_code = $cobrodigital_account->electronic_code;
                            $html .= '
                    <br />
                    <div class="barcodecell"><barcode code="' . $barcode . '" type="I25" class="barcode" /></div>
                    <div style="text-align: center;">
                        <label>' . $barcode . '</label>
                    <div>';
                            if (!empty($cobrodigital_account->electronic_code)) {
                                $html .= '
                    <br>
                    <div style="text-align: center;">
                        <label>Código electrónico</label>
                        <br>
                        <label>' . $cobrodigital_account->electronic_code . '</label>
                    <div>';
                            }
                        } else {
                            $cobrodigital = FALSE;
                        }
                    }

                    if ($payu) {

                        $this->_ctrl->loadModel('PayuAccounts');
                        $payu_account = $this->_ctrl->PayuAccounts
                            ->find()
                            ->where([
                                'customer_code'     => $customer_code,
                                'payment_getway_id' => 7,
                                'deleted'           => FALSE
                            ])->first();

                        if ($payu_account) {

                            $barcode = $payu_account->barcode;

                            $html .= '
                    <br />
                    <div class="barcodecell"><barcode code="' . $barcode . '" type="I25" class="barcode" /></div>
                    <div style="text-align: center;">
                        <label>' . $barcode . '</label>
                    <div>
                    ';
                        } else {
                            $payu = FALSE;
                        }
                    }

                    if ($cuentadigital) {

                        $this->_ctrl->loadModel('CuentadigitalAccounts');
                        $cuentadigital_account = $this->_ctrl->CuentadigitalAccounts
                            ->find()
                            ->where([
                                'customer_code'     => $customer_code,
                                'payment_getway_id' => 105,
                                'deleted'           => FALSE
                            ])->first();

                        if ($cuentadigital_account && !empty($cuentadigital_account->barcode)) {

                            $image_src = $cuentadigital_account->image_src;

                            $html .= '
                    <br />
                    <div style="text-align: center;">
                        <img src="' . $image_src . '">
                    <div>
                    ';
                        } else {
                            $cuentadigital = FALSE;
                        }
                    }

                    if ($cobrodigital || $payu || $cuentadigital) {
                        $html .= '
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="responsive-table">
						<tr>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<!-- COPY -->
										<td align="center" style="font-size: 32px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding-copy">
											¿Cómo Pago?
										</td>
									</tr>
									<tr>
										<td align="center" style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">
											El pago podés realizarlo por medio del código de barra que se encuentra en este documento. Por favor consultar los medios de pagos disponibles.
										</td>
									</tr>
								 
								</table>
							</td>
						</tr>
					</table>
					<!--[if (gte mso 9)|(IE)]>';
                    }

                    $html .=
                    '
                </body>
            </html>';

        } else {

            $comprobantes = [];

            $duedate = Time::now();
            $duedate->day(1);
            $duedate->modify('+1 month');

            $invoices = $this->_ctrl->Invoices
                ->find()
                ->where([
                    'customer_code'       => $customer_code,
                    'Invoices.duedate <=' => $duedate
                ])->toArray();

            $credit_notes = $this->_ctrl->CreditNotes
                ->find()
                ->where([
                    'customer_code' => $customer_code
                ])->toArray();

            $debit_notes = $this->_ctrl->DebitNotes
                ->find()
                ->where([
                    'customer_code'         => $customer_code,
                    'DebitNotes.duedate <=' => $duedate
                ])->toArray();

            $payments = $this->_ctrl->Payments
                ->find()
                ->where([
                    'customer_code' => $customer_code,
                    'anulated IS'   => NULL
                ])->toArray();

            $comprobantes = array_merge($invoices, $credit_notes, $debit_notes, $payments);

            $order_asc = function($a, $b)
            {
                $date_a = isset($a->date) ? $a->date : $a->created;
                $date_b = isset($b->date) ? $b->date : $b->created;

                if ($date_a < $date_b) {
                    return -1;
                } else if ($date_a > $date_b) {
                    return 1;
                } else {
                    return 0;
                }
            };

            usort($comprobantes, $order_asc);
            
            $newData = [];

            $saldo = 0;

            foreach ($comprobantes as $comprobante) {

                if (isset($comprobante->total)) {
                    if (in_array($comprobante->tipo_comp, ['NCX', '008', '003', '013'])) {
                        $total = $comprobante->total * -1;
                    } else {
                        $total = $comprobante->total;
                    }
                } else {
                    $total = $comprobante->import * -1;
                }

                $saldo += $total;

                $comprobante->saldo = $saldo;

                $newData[] = $comprobante;

                if ($comprobante->saldo == 0 && !$parament->invoicing->account_summary_complete) {
                    $newData = [];
                }
            }

            $comprobantes = array_reverse($newData);

            $class_debt = "color: #f05f40";
            if ($saldo == 0) {
                $class_debt = "color: black";
            } else if ($saldo > 0) {
                $class_debt = "color: #f05f40";
            } else {
                $class_debt = "color: #28a745";
            }

            $saldo = number_format((float)$saldo, 2, ',', '');

            $html = '
            <html>
                <head>
                    <style>
                        body {font-family: sans-serif;
                        	font-size: 10pt;
                        }
                        p {	margin: 0pt; }
                        table.items {
                        	border: 0.1mm solid #000000;
                        }
                        td { vertical-align: top; }
                        .items td {
                        	border-left: 0.1mm solid #000000;
                        	border-right: 0.1mm solid #000000;
                        }
                        table thead td { background-color: #EEEEEE;
                        	text-align: center;
                        	border: 0.1mm solid #000000;
                        	font-variant: small-caps;
                        }
                        .items td.blanktotal {
                        	background-color: #EEEEEE;
                        	border: 0.1mm solid #000000;
                        	background-color: #FFFFFF;
                        	border: 0mm none #000000;
                        	border-top: 0.1mm solid #000000;
                        	border-right: 0.1mm solid #000000;
                        }
                        .items td.totals {
                        	text-align: right;
                        	border: 0.1mm solid #000000;
                        }
                        .items td.cost {
                        	text-align: "." center;
                        }
                        .items-striped {
                            background-color: #f5f5f7;
                        }
                        .barcode {
                            padding: 1.5mm;
                            margin: 0;
                            vertical-align: top;
                            color: #000044;
                        }
                        .barcodecell {
                            text-align: center;
                            vertical-align: middle;
                        }
                    </style>
                </head>
                <body>
                    <!--mpdf
                    <htmlpageheader name="myheader">
                        <table width="100%">
                            <tr>
                                <td width="50%" ><img src="' . $enterprise_logo . '" height="100px"/></td>
                                <td width="50%" style="color:#615d5d; text-align: right;">
                                    <span style="font-weight: bold; font-size: 14pt;">' . $enterprise_name . '</span>
                                    <br />' . $enterprise_address . '
                                    <br />
                                    <span style="font-family:dejavusanscondensed;">&#9742;</span> ' . $enterprise_phone . '
                                </td>
                            </tr>
                        </table>
                    </htmlpageheader>
                    <htmlpagefooter name="myfooter">
                        <div style="border-top: 1px solid #000000; font-size: 9pt; text-align: center; padding-top: 3mm; ">
                            Página {PAGENO} de {nb}
                        </div>
                    </htmlpagefooter>
                    <sethtmlpageheader name="myheader" value="on" show-this-page="1" />
                    <sethtmlpagefooter name="myfooter" value="on" />
                    mpdf-->
                    <div style="text-align: right">Fecha emisión: ' . $now . '</div>
                	<table width="100%" style="font-family: serif;" cellpadding="10">
                		<tr>
                			<td width="100%" style="border: 0.1mm solid #888888; ">
                				<span style="font-size: 18pt; color: #555555; font-family: sans;">Resumen de Cuenta</span>
                				<br />
                				<br /><b>Nombre</b>: ' . $customer_name . '
                				<br /><b>Código</b>: ' . $customer_code . '
                				<br /><b>' . $customer_doc_type . '</b>: ' . $customer_ident . '
                				<br /><b>Domicilio</b>: ' . $customer_address . '
                				<br /><b>Tel.</b>: ' . $customer_phone . '
                				<br /><b>Tipo</b>: ' . $customer_type . '
                				<br />
                				<br /><b style="font-size: 14pt;' . $class_debt . '">Saldo</b>: <span style="font-size: 14pt;">$' . $saldo . '</span>
                			</td>
                		</tr>
                	</table>';

                    if (sizeof($comprobantes) > 0) {
                        $html .=
                    	'<br />
                    	<table class="items" width="100%" style="font-size: 8pt; border-collapse: collapse; " cellpadding="8">
                            <thead style="font-size: 10pt;">
                                <tr>
                                    <td width="12%">Fecha</td>
                                    <td width="38%">Descrip.</td>
                                    <td width="19%">Total</td>
                                    <td width="12%">Vto.</td>
                                    <td width="19%">Saldo</td>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- ITEMS HERE -->';

                                foreach ($comprobantes as $key => $comprobante) {

                                    $date_object = isset($comprobante->date) ? $comprobante->date : $comprobante->created;

                                    $date = str_pad($date_object->day, 2, "0", STR_PAD_LEFT) . '/' . str_pad($date_object->month, 2, "0", STR_PAD_LEFT) . '/' . $date_object->year;

                                    $comments = "";

                                    if (isset($comprobante->comments)) {
                                        $comments = $comprobante->comments;
                                    } else {
                                        $comments = $comprobante->concept;
                                    }

                                    $total = isset($comprobante->total) ? $comprobante->total : ($comprobante->import * -1);
                                    $total = number_format((float)$total, 2, ',', '');

                                    $duedate = "";

                                    if (isset($comprobante->duedate)) {
                                        $duedate = str_pad($comprobante->duedate->day, 2, "0", STR_PAD_LEFT) . '/' . str_pad($comprobante->duedate->month, 2, "0", STR_PAD_LEFT) . '/' . $comprobante->duedate->year;
                                    }

                                    $saldo = $comprobante->saldo;
                                    $saldo = number_format((float)$saldo, 2, ',', '');

                                    $class_striped = "items-striped";

                                    if ($key % 2) {
                                        $class_striped = "";
                                    }

                                    $html .=
                                    '<tr class="' . $class_striped . '">
                                        <td align="center">' . $date . '</td>
                                        <td>' . $comments . '</td>
                                        <td class="cost">$' . $total . '</td>
                                        <td align="center">' . $duedate . '</td>
                                        <td class="cost">$' . $saldo . '</td>
                                    </tr>';
                                }

                        $html .= 
                        '</tbody>
                        </table>';
                    }

                    if ($cobrodigital) {

                        $this->_ctrl->loadModel('CobrodigitalAccounts');
                        $cobrodigital_account = $this->_ctrl->CobrodigitalAccounts
                            ->find()
                            ->where([
                                'customer_code'     => $customer_code,
                                'payment_getway_id' => 99,
                                'deleted'           => FALSE
                            ])->first();

                        if ($cobrodigital_account) {

                            $result = $this->CobroDigital->getBarcodeFromCobroDigital($cobrodigital_account->barcode);
                            $electronic_code = $cobrodigital_account->electronic_code;

                            if ($result) {

                                $html .= '
                                    <br />
                                    <div style="text-align: center;">
                                        <img src="' . $result . '">
                                    <div>';

                            } else {

                                $html .= '
                                    <br />
                                    <div class="barcodecell"><barcode code="' . $cobrodigital_account->barcode . '" type="ITF-14" class="barcode" /></div>
                                    <div style="text-align: center;">
                                        <label>' . $cobrodigital_account->barcode . '</label>
                                    <div>';
                            }

                            if (!empty($cobrodigital_account->electronic_code)) {
                                $html .= '
                    <br>
                    <div style="text-align: center;">
                        <label>Código electrónico</label>
                        <br>
                        <label>' . $cobrodigital_account->electronic_code . '</label>
                    <div>';
                            }
                        } else {
                            $cobrodigital = FALSE;
                        }
                    }

                    if ($payu) {

                        $this->_ctrl->loadModel('PayuAccounts');
                        $payu_account = $this->_ctrl->PayuAccounts
                            ->find()
                            ->where([
                                'customer_code'     => $customer_code,
                                'payment_getway_id' => 7,
                                'deleted'           => FALSE
                            ])->first();

                        if ($payu_account) {

                            $barcode = $payu_account->barcode;
                            $html .= '
                    <br />
                    <div class="barcodecell"><barcode code="' . $barcode . '" type="I25" class="barcode" /></div>
                    <div style="text-align: center;">
                        <label>' . $barcode . '</label>
                    <div>
                    ';
                        } else {
                            $payu = FALSE;
                        }
                    }

                    if ($cuentadigital) {

                        $this->_ctrl->loadModel('CuentadigitalAccounts');
                        $cuentadigital_account = $this->_ctrl->CuentadigitalAccounts
                            ->find()
                            ->where([
                                'customer_code'     => $customer_code,
                                'payment_getway_id' => 105,
                                'deleted'           => FALSE
                            ])->first();

                        if ($cuentadigital_account && !empty($cuentadigital_account->barcode)) {
                            $image_src = $cuentadigital_account->image_src;

                            $html .= '
                    <br />
                    <div style="text-align: center;">
                        <img src="' . $image_src . '">
                    <div>
                    ';
                        } else {
                            $cuentadigital = FALSE;
                        }
                    }

                    if ($cobrodigital || $payu || $cuentadigital) {
                        $html .= '
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="responsive-table">
						<tr>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<!-- COPY -->
										<td align="center" style="font-size: 32px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding-copy">
											¿Cómo Pago?
										</td>
									</tr>
									<tr>
										<td align="center" style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">
											El pago podés realizarlo por medio del código de barra que se encuentra en este documento. Por favor consultar los medios de pagos disponibles.
										</td>
									</tr>
								 
								</table>
							</td>
						</tr>
					</table>
					<!--[if (gte mso 9)|(IE)]>';
                    }

                $html .= '</body>
            </html>';
        }

        if ($send_email) {

            $mpdf = new \Mpdf\Mpdf([
            	'margin_left'   => 20,
            	'margin_right'  => 15,
            	'margin_top'    => 48,
            	'margin_bottom' => 25,
            	'margin_header' => 10,
            	'margin_footer' => 10
            ]);

            $folder = new Folder(WWW_ROOT . 'temp_files_mail', TRUE, 0755);

            $filename = WWW_ROOT . 'temp_files_mail/';
            $pdf_title = Time::now()->toUnixString();
            $pdf_title .= '-resumen-' . str_pad($customer_code, 5, "0", STR_PAD_LEFT);
            $pdf_title .= '.pdf';

            $filename .= $pdf_title;

            $mpdf->SetTitle($pdf_title);
            $mpdf->SetDisplayMode('fullpage');
            $mpdf->allow_charset_conversion = TRUE;
            $mpdf->charset_in = 'UTF-8';
            $html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');
            $mpdf->WriteHTML($html);

            $mpdf->Output($filename, \Mpdf\Output\Destination::FILE);
            return $filename;
        }

        return $html;
    }
}
