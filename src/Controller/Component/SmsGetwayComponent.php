<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Http\Client;
use Cake\Core\Configure;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\Event\EventManager;
use Cake\Event\Event;

/**
 * SmsGetway component
 */
class SmsGetwayComponent extends Component
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    private $_ctrl;
    private $_conf;

    public function initialize(array $config)
    {
        $this->_ctrl = $this->_registry->getController();
        $paraments = $this->_ctrl->request->getSession()->read('paraments');
        $this->_conf = $paraments->sms->platform->sms_getway;

        $this->initEventManager();
    }

    public function initEventManager() 
    {
        EventManager::instance()->on(
            'SmsGetwayComponent.Error',
            function($event, $msg, $data, $flash = false) {

                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $log->user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Error';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);

                if ($flash) {
                    $this->_ctrl->Flash->error(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'SmsGetwayComponent.Warning',
            function($event, $msg, $data, $flash = false) {

                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $log->user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Warning';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);

                if ($flash) {
                    $this->_ctrl->Flash->warning(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'SmsGetwayComponent.Log',
            function($event, $msg, $data) {

            }
        );

        EventManager::instance()->on(
            'SmsGetwayComponent.Notice',
            function($event, $msg, $data) {

            }
        );
    }

    public function validateAccount()
    {
        $token = $this->_conf->token;

        $opts = array('http' => 
        	array (
        		'method'  => 'GET',
        		'header'  => 
        		    "Authorization: " . $token . "\r\n"
        	)
        );

        // Create the POST context
        $context  = stream_context_create($opts);

        $url = $this->_conf->url . $this->_conf->api . $this->_conf->getting_device_information . $this->_conf->device_id;

        $smsrespuesta = @file_get_contents($url, FALSE, $context);
        if (strpos($http_response_header[0], "200")) { 
            if (!$smsrespuesta) {
                $data_error = new \stdClass; 
                $data_error->errors = "";

                $event = new Event('SmsGetwayComponent.Error', $this, [
                	'msg'   => __('Hubo un error al validar la cuenta de SMS Getway.'),
                	'data'  => $data_error,
                	'flash' => true
                ]);

                $this->_ctrl->getEventManager()->dispatch($event);
                Log::write('error', 'SmsGetwayComponent - validateAccount sms respuesta false.');
                return FALSE;
            }
        } else { 
            $data_error = new \stdClass; 
            $data_error->errors = $http_response_header;

            $event = new Event('SmsGetwayComponent.Error', $this, [
            	'msg'   => __('Hubo un error al validar la cuenta de SMS Getway.'),
            	'data'  => $data_error,
            	'flash' => true
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);
            Log::write('error', 'Error al realizar la consulta al servidor SMS Getway');
            Log::write('error', $http_response_header);
            return FALSE;
        } 
        return TRUE;
    }

    public function enviarBloque($messages)
    {
        $json_data = json_encode($messages);

        $token = $this->_conf->token;

        $opts = array('http' => 
        	array (
        		'method'  => 'POST',
        		'header'  => 
        		    "Content-type: application/json\r\n" .
        		    "Authorization: " . $token . "\r\n",
                    "Connection: close\r\n" .
                    "Content-length: " . strlen($json_data) . "\r\n",
                'content' => $json_data,
        	)
        );

        // Create the POST context
        $context  = stream_context_create($opts);

        $url = $this->_conf->url . $this->_conf->api . $this->_conf->sending_messages;
        $smsrespuesta = @file_get_contents($url, false, $context);
        if (strpos($http_response_header[0], "200")) { 
            if (!$smsrespuesta) {
                $data_error = new \stdClass; 
                $data_error->errors = "";

                $event = new Event('SmsGetwayComponent.Error', $this, [
                	'msg'   => __('Hubo un error al enviar el bloque de SMS Getway.'),
                	'data'  => $data_error,
                	'flash' => true
                ]);

                $this->_ctrl->getEventManager()->dispatch($event);
                Log::write('error', 'SmsGetwayComponent - sending_messages sms respuesta false.');
                return false;
            }
        } else {
            $data_error = new \stdClass; 
            $data_error->errors = $http_response_header;

            $event = new Event('SmsGetwayComponent.Error', $this, [
            	'msg'   => __('Error al realizar la consulta al servidor SMS Getway.'),
            	'data'  => $data_error,
            	'flash' => true
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);
            Log::write('error', 'Error al realizar la consulta al servidor SMS Getway');
            Log::write('error', $http_response_header);
            return false;
        } 
        return $smsrespuesta;
    }

    public function cancellingMessages($ids)
    {
        $json_data = json_encode($ids);

        $token = $this->_conf->token;

        $opts = array('http' => 
        	array (
        		'method'  => 'POST',
        		'header'  => 
        		    "Content-type: application/json\r\n" .
        		    "Authorization: " . $token . "\r\n",
                    "Connection: close\r\n" .
                    "Content-length: " . strlen($json_data) . "\r\n",
                'content' => $json_data,
        	)
        );

        // Create the POST context
        $context  = stream_context_create($opts);

        $url = $this->_conf->url . $this->_conf->api . $this->_conf->cancelling_messages;
        $smsrespuesta = @file_get_contents($url, false, $context);
        if (strpos($http_response_header[0], "200")) { 
            if (!$smsrespuesta) {
                $data_error = new \stdClass; 
                $data_error->errors = "";

                $event = new Event('SmsGetwayComponent.Error', $this, [
                	'msg'   => __('Error al cancelar el mensaje SMS Getway.'),
                	'data'  => $data_error,
                	'flash' => true
                ]);

                $this->_ctrl->getEventManager()->dispatch($event);
                Log::write('error', 'SmsGetwayComponent - cancellingMessages sms respuesta false.');
                return false;
            }
        } else { 
            $data_error = new \stdClass; 
            $data_error->errors = $http_response_header;

            $event = new Event('SmsGetwayComponent.Error', $this, [
            	'msg'   => __('Error al realizar la consulta al servidor SMS Getway.'),
            	'data'  => $data_error,
            	'flash' => true
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);
            Log::write('error', 'Error al realizar la consulta al servidor SMS Getway');
            Log::write('error', $http_response_header);
            return false;
        } 
        return $smsrespuesta;
    }

    public function gettingMessagesInformation($id)
    {
        $token = $this->_conf->token;

        $opts = array('http' => 
        	array (
        		'method'  => 'GET',
        		'header'  => 
        		    "Authorization: " . $token . "\r\n"
        	)
        );

        // Create the POST context
        $context  = stream_context_create($opts);

        $url = $this->_conf->url . $this->_conf->api . $this->_conf->getting_messages_information . $id;
        $smsrespuesta = @file_get_contents($url, false, $context);
        if (strpos($http_response_header[0], "200")) { 
            if (!$smsrespuesta) {
                $data_error = new \stdClass; 
                $data_error->errors = "";
    
                $event = new Event('SmsGetwayComponent.Error', $this, [
                	'msg'   => __('Error al obtener información de los mensajes al servidor SMS Getway.'),
                	'data'  => $data_error,
                	'flash' => true
                ]);
    
                $this->_ctrl->getEventManager()->dispatch($event);
                Log::write('error', 'SmsGetwayComponent - gettingMessagesInformation sms respuesta false.');
                return false;
            }
        } else {
            $data_error = new \stdClass; 
            $data_error->errors = $http_response_header;

            $event = new Event('SmsGetwayComponent.Error', $this, [
            	'msg'   => __('Error al realizar la consulta al servidor SMS Getway.'),
            	'data'  => $data_error,
            	'flash' => true
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);
            Log::write('error', 'Error al realizar la consulta al servidor SMS Getway');
            Log::write('error', $http_response_header);
            return false;
        } 
        return $smsrespuesta;
    }

    public function consultarBloque($filters)
    {
        $json_data = json_encode($filters);

        $token = $this->_conf->token;

        $opts = array('http' => 
        	array (
        		'method'  => 'POST',
        		'header'  => 
        		    "Content-type: application/json\r\n" .
        		    "Authorization: " . $token . "\r\n",
                    "Connection: close\r\n" .
                    "Content-length: " . strlen($json_data) . "\r\n",
                'content' => $json_data,
        	)
        );

        // Create the POST context
        $context  = stream_context_create($opts);

        $url = $this->_conf->url . $this->_conf->api . $this->_conf->searching_messages;
        $smsrespuesta = @file_get_contents($url, false, $context);
        if (strpos($http_response_header[0], "200")) { 
            if (!$smsrespuesta) {
                $data_error = new \stdClass; 
                $data_error->errors = "";
    
                $event = new Event('SmsGetwayComponent.Error', $this, [
                	'msg'   => __('Error al consultar bloque al servidor SMS Getway.'),
                	'data'  => $data_error,
                	'flash' => true
                ]);
    
                $this->_ctrl->getEventManager()->dispatch($event);
                Log::write('error', 'SmsGetwayComponent - searching_messages sms respuesta false.');
                return false;
            }
        } else {
            $data_error = new \stdClass; 
            $data_error->errors = $http_response_header;

            $event = new Event('SmsGetwayComponent.Error', $this, [
            	'msg'   => __('Error al realizar la consulta al servidor SMS Getway.'),
            	'data'  => $data_error,
            	'flash' => true
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);
            Log::write('error', 'Error al realizar la consulta al servidor SMS Getway');
            Log::write('error', $http_response_header);
            return false;
        } 
        return $smsrespuesta;
    }

    public function getDevices($token)
    {
        $filter_search = new \stdClass;

        $filters = [
            [
                'field' => 'name',
                'operator' => '=',
                'value' => 'New Device'
            ],
        ];

        $order_by = [
            [
                'field' => 'type',
                'direction' => 'DESC'
            ],
            [
                'field' => 'make',
                'direction' => 'ASC'
            ]
        ];

        $filter_search->filters = $filters;
        $filter_search->order_by = $order_by;
        $filter_search->limit = 5;
        $filter_search->offset = 0;

        $json_data = json_encode($filters);

        $opts = array('http' => 
        	array (
        		'method'  => 'POST',
        		'header'  => 
        		    "Content-type: application/json\r\n" .
        		    "Authorization: " . $token . "\r\n",
                    "Connection: close\r\n" .
                    "Content-length: " . strlen($json_data) . "\r\n",
                'content' => $json_data,
        	)
        );

        // Create the POST context
        $context  = stream_context_create($opts);

        $url = $this->_conf->url . $this->_conf->api . $this->_conf->getting_devices;
        $smsrespuesta = @file_get_contents($url, false, $context);
        if (strpos($http_response_header[0], "200")) { 
            if (!$smsrespuesta) {
                $data_error = new \stdClass; 
                $data_error->errors = "";

                $event = new Event('SmsGetwayComponent.Error', $this, [
                	'msg'   => __('Error al obtener los dispositivos del servidor SMS Getway.'),
                	'data'  => $data_error,
                	'flash' => true
                ]);

                $this->_ctrl->getEventManager()->dispatch($event);
                Log::write('error', 'SmsGetwayComponent - searching_messages sms respuesta false.');
                return false;
            }
        } else {
            $data_error = new \stdClass; 
            $data_error->errors = $http_response_header;

            $event = new Event('SmsGetwayComponent.Error', $this, [
            	'msg'   => __('Error al realizar la consulta al servidor SMS Getway.'),
            	'data'  => $data_error,
            	'flash' => true
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);
            Log::write('error', 'Error al realizar la consulta al servidor SMS Getway.');
            Log::write('error', $http_response_header);
            return false;
        } 
        return $smsrespuesta;
    }
}
