<?php
namespace App\Controller\Component;

use App\Controller\Component\Common\MyComponent;
use Cake\Controller\ComponentRegistry;
use Cake\Http\Client;
use Cake\Core\Configure;
use Cake\Filesystem\File;
use Cake\Event\EventManager;
use Cake\Event\Event;
use Cake\I18n\Time;

/**
 * BrainminatorWS component
 */
class CobroDigitalComponent extends MyComponent
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    private $_cobrodigital;

    public function initialize(array $config)
    {
        parent::initialize($config);

        $payment_getway = $this->_ctrl->request->getSession()->read('payment_getway');
        $this->_cobrodigital = $payment_getway->config->cobrodigital;

        $this->initEventManager();
    }

    public function initEventManager()
    {
        EventManager::instance()->on(
            'CobroDigitalComponent.Error',
            function ($event, $msg, $data, $flash = false) {

                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $log->user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Error';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);

                if ($flash) {
                    $this->_ctrl->Flash->error(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'CobroDigitalComponent.Warning',
            function($event, $msg, $data, $flash = false) {

                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $log->user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Warning';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);

                if ($flash) {
                    $this->_ctrl->Flash->warning(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'CobroDigitalComponent.Log',
            function ($event, $msg, $data) {

            }
        );

        EventManager::instance()->on(
            'CobroDigitalComponent.Notice',
            function ($event, $msg, $data) {

            }
        );
    }

    /**
     * Estructura del pagador
     * $pagador = [
        	'nombre'    => 'Lucas Rodriguez',
        	'Id'        => '123',
        	'direccion' => 'Corrientes 12',
        	'mail'      => 'lucas@yopmail.com'
        ]
     */ 
    public function crearPagador($pagador)
    {
        // Config
        $client = new \nusoap_client($this->_cd_webservices->url);

        $modo = $this->_cd_configs->modo;

        $envios = [
            'idComercio'        => $this->_cd_params->$modo->idComercio,
            'sid'               => $this->_cd_params->$modo->sid,
            'metodo_webservice' => $this->_cd_webservices->endpoint_crear_pagador,
            'pagador'           => $pagador
        ];

        // Calls
        $result = $client->call($this->_cd_webservices->call_soap, array(json_encode($envios)));
        return json_decode($result);
    }

    /**
     * $key: Id, nombre, direccion, mail
     *
     * $value: depende de la $key
     * 
     * Estructura del pagador
     * $pagador = [
        	'nombre'    => 'Lucas Rodriguez',
        	'Id'        => '123',
        	'direccion' => 'Corrientes 12',
        	'mail'      => 'lucas@yopmail.com'
        ]
     */ 
    public function editarPagador($key = 'id', $value, $pagador)
    {
        // Config
        $client = new \nusoap_client($this->_cd_webservices->url);

        $modo = $this->_cd_configs->modo;

        $envios = [
            'idComercio'        => $this->_cd_params->$modo->idComercio,
            'sid'               => $this->_cd_params->$modo->sid,
            'metodo_webservice' => $this->_cd_webservices->endpoint_editar_pagador,
            'identificador'     => $key,
            'buscar'            => $value,
            'pagador'           => $pagador
        ];

        // Calls
        $result = $client->call($this->_cd_webservices->call_soap, array(json_encode($envios)));
        return json_decode($result);
    }

    public function generarBoleta($key = 'Id', $value, $boleta = null)
    {
        // Config
        $client = new \nusoap_client($this->_cd_webservices->url);

        $modo = $this->_cd_configs->modo;

        /***********************************************************************
            $envios[] = array(
                'idComercio'=>'TU_IDENTIFICADOR_COMERCIAL',
    			'sid'=>'TU_CLAVE_DE_AUTENTICACION',
    			'metodo_webservice'=>'generar_boleta',
    			'identificador'=>'Id',
    			'buscar'=>'12345678',
    			'fechas_vencimiento'=>array('20180101','20180105'),
    			'importes'=>array(100.98,102),
    			'concepto'=>'Boleta de Prueba',
    			'plantilla'=>'init'
			);
         */

        $envios = [
            'idComercio'         => $this->_co_params->$modo->idComercio,
            'sid'                => $this->_co_params->$modo->sid,
            'metodo_webservice'  => $this->_cd_webservices->endpoint_generar_boleta,
            'identificador'      => $key,
            'buscar'             => $value,
            'fechas_vencimiento' => $boleta->fechas_vencimiento,
            'importes'           => $boleta->importes,
            'concepto'           => $boleta->concepto,
			'plantilla'          => $this->_cd_configs->plantilla
        ];

        // Calls
        $result = $client->call($this->_cd_webservices->call_soap, array(json_encode($envios)));
        return json_decode($result);
    }

    public function inhabilitarBoleta($nro_boleta)
    {
        // Config
        $client = new \nusoap_client($this->_cd_webservices->url);

        $modo = $this->_cd_configs->modo;

        /***********************************************************************
            $envios[] = array(
                'idComercio'        => 'TU_IDENTIFICADOR_COMERCIAL',
				'sid'               => 'TU_CLAVE_DE_AUTENTICACION',
				'metodo_webservice' => 'inhabilitar_boleta',
				'nro_boleta'        => '1'
			);
         */

        $envios = [
            'idComercio'         => $this->_cd_params->$modo->idComercio,
            'sid'                => $this->_cd_params->$modo->sid,
            'metodo_webservice'  => $this->_cd_webservices->endpoint_inhabilitar_boleta,
            'nro_boleta'         => $nro_boleta
        ];

        // Calls
        $result = $client->call($this->_cd_webservices->call_soap, array(json_encode($envios)));
        return json_decode($result);
    }

    public function consultarTransacciones($desde, $hasta, $offset = null, $limit = null, $filtros = null, $id_comercio, $sid)
    {
        // Config
        //$client = new \nusoap_client($this->_cobrodigital->url);

        /***********************************************************************
            $envios[] = array(
                'idComercio'        => 'TU_IDENTIFICADOR_COMERCIAL',
				'sid'               => 'TU_CLAVE_DE_AUTENTICACION',
				'metodo_webservice' => 'consultar_transacciones',
				'desde'             => '20160932',
				'hasta'             => '20161001'
			);
         */

        /*$envios = [
            'idComercio'         => $this->_cd_params->$modo->idComercio,
            'sid'                => $this->_cd_params->$modo->sid,
            'metodo_webservice'  => $this->_cd_webservices->endpoint_consultar_transacciones,
            'desde'              => '',
			'hasta'              => '',
			'filtros' => [
			    'nro_boleta'    => '',
			    'concepto'      => '',
			    'identificador' => '',
			    'nombre'        => '',
			],
			'offset'             => '',
			'limit'              => '',
        ];*/

        $envios = [
            'idComercio'         => $id_comercio,
            'sid'                => $sid,
            'metodo_webservice'  => $this->_cobrodigital->endpoint_consultar_transacciones,
            'desde'              => $desde,
			'hasta'              => $hasta
        ];

        if (!empty($filtros)) {
            $envios['filtros'] = $filtros;
        }

        if (!empty($offset)) {
            $envios['offset'] = $offset;
        }

        if (!empty($limit)) {
            $envios['limit'] = $limit;
        }

        // Calls
        /*$result = $client->call($this->_cobrodigital->call_soap, array(json_encode($envios)));
        Debug($result); die();
        return json_decode($result);*/

        $postdata = json_encode($envios);
        $opts = array('http' =>
            array(
                'method'  => 'post',
                'header'  => 'Content-type: application/json',
                'content' => $postdata
            )
        );
        $context = stream_context_create($opts);

        $result = file_get_contents($this->_cobrodigital->url, false, $context);
        $raw_result = $result;
        $result = json_decode($result);

        $detail =  'Resultado de consultar transacciones - Cobro Digital';
        $detail .=  'Datos: ' . PHP_EOL;
        $detail .=  'SID: ' . $sid . PHP_EOL;
        $detail .=  'ID Comercio: ' . $id_comercio . PHP_EOL;
        $detail .=  'Fecha: ' . Time::now()->format('d/m/Y H:m') . PHP_EOL;
        $detail .=  'URL: ' . $this->_cobrodigital->url . PHP_EOL;
        $detail .=  'Desde: ' . $desde . PHP_EOL;
        $detail .=  'Hasta: ' . $hasta . PHP_EOL;
        $detail .=  'metodo_webservice: ' . $this->_cobrodigital->endpoint_consultar_transacciones . PHP_EOL;
        $detail .=  '--------------------------------' . PHP_EOL;
        $detail .= 'Resultado: ' . PHP_EOL;
        $detail .= trim($raw_result);

        $path = $this->generateLog($detail);

        $action = 'Generación LOG - Cobro Digital';
        $detail = "";
        $detail .= 'Datos: ' . PHP_EOL;
        $detail .= 'SID: ' . $sid . PHP_EOL;
        $detail .= 'ID Comercio: ' . $id_comercio . PHP_EOL;
        $detail .= 'Fecha: ' . Time::now()->format('d/m/Y H:m') . PHP_EOL;
        $detail .= 'URL: ' . $this->_cobrodigital->url . PHP_EOL;
        $detail .= 'Desde: ' . $desde . PHP_EOL;
        $detail .= 'Hasta: ' . $hasta . PHP_EOL;
        $detail .= 'Path: ' . $path;
        $this->_ctrl->registerActivity($action, $detail, NULL, TRUE);

        if (isset($result->datos)) {
            $result->datos = json_decode(json_encode($result->datos[0]), true);
        }
        return $result;
    }

    private function generateLog($data)
    {
        $time = Time::now()->format('Ymd');

        $path = "log_payment_getway/$time-cobrodigital.txt";

        $file = new File(WWW_ROOT . $path, true, 0775);
        $file->write($data, 'w+', TRUE);
        $file->close();

        return $path;
    }

    public function verificarExistenciaPagador($key = 'Id', $value)
    {
        // Config
        $client = new \nusoap_client($this->_cobrodigital->url);

        /***********************************************************************
            $envios[] = array(
                'idComercio'        => 'TU_IDENTIFICADOR_COMERCIAL',
				'sid'               => 'TU_CLAVE_DE_AUTENTICACION',
				'metodo_webservice' => 'verificar_existencia_pagador',
				'identificador'     => 'Su_identificador',
				'buscar'            => '1AF8'
			);
         */

        $envios = [
            'idComercio'         => $this->_cobrodigital->idComercio,
            'sid'                => $this->_cobrodigital->sid,
            'metodo_webservice'  => $this->_cobrodigital->endpoint_verificar_existencia_pagador,
            'identificador'      => $key,
            'buscar'             => $value
        ];

        // Calls
        $result = $client->call($this->_cobrodigital->call_soap, array(json_encode($envios)));
        return json_decode($result);
    }

    public function obtenerCodigoBarra($boleta = null)
    {
        // Config
        $client = new \nusoap_client($this->_cd_webservices->url);

        $modo = $this->_cd_configs->modo;

        /***********************************************************************
            $envios[] = array(
                'idComercio'=>'TU_IDENTIFICADOR_COMERCIAL',
				'sid'=>'TU_CLAVE_DE_AUTENTICACION',
				'metodo_webservice'=>'obtener_codigo_de_barras',
				'nro_boleta'=>'1'
			);
         */

        // $envios = [
        //     'idComercio'         => $this->_cd_params->$modo->idComercio,
        //     'sid'                => $this->_cd_params->$modo->sid,
        //     'metodo_webservice'  => $this->_cd_webservices->endpoint_obtener_codigo_de_barra,
        //     'nro_boleta'         => '421',
        // ];

        $envios = [
            'idComercio'         => $this->_cd_params->$modo->idComercio,
            'sid'                => $this->_cd_params->$modo->sid,
            'metodo_webservice'  => $this->_cd_webservices->endpoint_obtener_codigo_de_barra,
            'nro_boleta'         => $boleta->nro
        ];

        // Calls
        $result = $client->call($this->_cd_webservices->call_soap, array(json_encode($envios)));
        return json_decode($result);
    }

    public function consultarActividadMicrositio($key = 'Id', $value, $desde, $hasta)
    {
        // Config
        $client = new \nusoap_client($this->_cd_webservices->url);

        $modo = $this->_cd_configs->modo;

        /***********************************************************************
            $envios[] = array(
                'idComercio'=>'TU_IDENTIFICADOR_COMERCIAL',
				'sid'=>'TU_CLAVE_DE_AUTENTICACION',
				'metodo_webservice'=>'consultar_actividad_micrositio',
				'identificador'=>'Id',
				'buscar'=>'12345678',
				'desde'=>'20160720',
				'hasta'=>'20160801'
			);
         */

        $envios = [
            'idComercio'         => $this->_cd_params->$modo->idComercio,
            'sid'                => $this->_cd_params->$modo->sid,
            'metodo_webservice'  => $this->_cd_webservices->endpoint_consultar_actividad_micrositio,
            'identificador'      => $key,
			'buscar'             => $value,
			'desde'              => $desde,
			'hasta'              => $hasta
        ];

        // Calls
        $result = $client->call($this->_cd_webservices->call_soap, array(json_encode($envios)));
        return json_decode($result);
    }

    public function obtenerCodigoElectronico($key = 'Id', $value)
    {
        // Config
        $client = new \nusoap_client($this->_cd_webservices->url);

        $modo = $this->_cd_configs->modo;

        /***********************************************************************
            $envios[] = array(
                'idComercio'=>'TU_IDENTIFICADOR_COMERCIAL',
				'sid'=>'TU_CLAVE_DE_AUTENTICACION',
				'metodo_webservice'=>'consultar_actividad_micrositio',
				'identificador'=>'Id',
				'buscar'=>'12345678',
				'desde'=>'20160720',
				'hasta'=>'20160801'
			);
         */

        $envios = [
            'idComercio'         => $this->_cd_params->$modo->idComercio,
            'sid'                => $this->_cd_params->$modo->sid,
            'metodo_webservice'  => $this->_cd_webservices->endpoint_obtener_codigo_electronico,
            'identificador'      => $key,
			'buscar'             => $value
        ];

        // Calls
        $result = $client->call($this->_cd_webservices->call_soap, array(json_encode($envios)));
        return json_decode($result);
    }

    public function consultarEstructuraPagadores()
    {
        $client = new \nusoap_client($this->_cobrodigital->url);

        $envios = [
            'idComercio'         => $this->_cobrodigital->idComercio,
            'sid'                => $this->_cobrodigital->sid,
            'metodo_webservice'  => $this->_cobrodigital->endpoint_consultar_estucutra_pagadores,
        ];

        // Calls
        $result = $client->call($this->_cobrodigital->call_soap, array(json_encode($envios)));
        return json_decode($result);
    }

    public function consultarBoletas($desde, $hasta, $filtros = null)
    {
        // Config
        $client = new \nusoap_client($this->_cd_webservices->url);

        $modo = $this->_cd_configs->modo;

//         $envios = [
//             'idComercio'        => $this->_cd_params->$modo->idComercio,
//             'sid'               => $this->_cd_params->$modo->sid,
//             'metodo_webservice' => $this->_cd_webservices->endpoint_consultar_boletas,
//             'desde'             => $boleta->desde,
// 			'hasta'             => $boleta->hasta,
// 			'filtros'           => $boleta->filtros
// 			'filtros' => [
// 			    'identificacion' => '',
// 			    'nombre'         => '',
// 			    'nro_boleta'     => '421',
// 			    'concepto'       => ''
// 		    ]
//         ];

        $envios = [
            'idComercio'        => $this->_cd_params->$modo->idComercio,
            'sid'               => $this->_cd_params->$modo->sid,
            'metodo_webservice' => $this->_cd_webservices->endpoint_consultar_boletas,
            'desde'             => $desde,
			'hasta'             => $hasta,
			'filtros'           => $filtros
        ];

        // Calls
        $result = $client->call($this->_cd_webservices->call_soap, array(json_encode($envios)));
        return json_decode($result);
    }

    public function obtenerBoletaHtml($boleta = null)
    {
        // Config
        $client = new \nusoap_client($this->_cd_webservices->url);
        // Set timeouts, nusoap default is 30
        $client->timeout = 0;
        $client->response_timeout = 300;
        $modo = $this->_cd_configs->modo;

        // $envios = [
        //     'idComercio'        => $this->_cd_params->$modo->idComercio,
        //     'sid'               => $this->_cd_params->$modo->sid,
        //     'metodo_webservice' => $this->_cd_webservices->endpoint_obtener_boleta_html,
        //     'nro_boleta'        => '421'
        // ];

        $envios = [
            'idComercio'        => $this->_cd_params->$modo->idComercio,
            'sid'               => $this->_cd_params->$modo->sid,
            'metodo_webservice' => $this->_cd_webservices->endpoint_obtener_boleta_html,
            'nro_boleta'        => $boleta->nro
        ];

        // Calls
        $result = $client->call($this->_cd_webservices->call_soap, array(json_encode($envios)));
       
        return json_decode($result);
    }

    public function meta($metodos = null)
    {
        // Config
        $client = new \nusoap_client($this->_cd_webservices->url);

        $modo = $this->_cd_configs->modo;

    //     $envios = [
    //         'idComercio'        => $this->_cd_params->$modo->idComercio,
    //         'sid'               => $this->_cd_params->$modo->sid,
    //         'metodo_webservice' => $this->_cd_webservices->endpoint_meta,
    //         0 => [
    //             'metodo_webservice'  => $this->_cd_webservices->endpoint_generar_boleta,
    //             'identificador'      => 'Id',
    //             'buscar'             => '12345678',
    //             'fechas_vencimiento' => array('20180101','20180105'),
    //             'importes'           => array(100.98,102),
    //             'concepto'           => 'Boleta de Prueba1',
    // 			'plantilla'          => 'init'
    //         ],
    //         1 => [
    //             'metodo_webservice'  => $this->_cd_webservices->endpoint_generar_boleta,
    //             'identificador'      => 'Id',
    //             'buscar'             => '12345679',
    //             'fechas_vencimiento' => array('20180101','20180105'),
    //             'importes'           => array(100.98,102),
    //             'concepto'           => 'Boleta de Prueba2',
    // 			'plantilla'          => 'init'
    //         ],
    //         2 => [
    //             'metodo_webservice'  => $this->_cd_webservices->endpoint_generar_boleta,
    //             'identificador'      => 'Id',
    //             'buscar'             => '12345676',
    //             'fechas_vencimiento' => array('20180101','20180105'),
    //             'importes'           => array(100.98,102),
    //             'concepto'           => 'Boleta de Prueba3',
    // 			'plantilla'          => 'init'
    //         ],
    //     ];

        $envios = [
            'idComercio'        => $this->_cd_params->$modo->idComercio,
            'sid'               => $this->_cd_params->$modo->sid,
            'metodo_webservice' => $this->_cd_webservices->endpoint_meta,
        ];

        foreach ($metodos as $key => $metodo) {
            $envios[$key] = $metodo;
        }

        // Calls
        $result = $client->call($this->_cd_webservices->call_soap, array(json_encode($envios)));
        return json_decode($result);
    }

    public function generarDebitoAutomatico($debito = null)
    {
        // Config
        $client = new \nusoap_client($this->_cd_webservices->url);

        $modo = $this->_cd_configs->modo;

        // $envios = [
        //     'idComercio'        => $this->_cd_params->$modo->idComercio,
        //     'sid'               => $this->_cd_params->$modo->sid,
        //     'metodo_webservice' => $this->_cd_webservices->endpoint_generar_debito_automatico,
        //     'nombre'            => 'Jorge',
        //     'apellido'          => 'Perez',
        //     'cuit'              => '20123456781',
        //     'email'             => 'juan@yopmail.com',
        //     'cbu'               => '2850590940090418135201',
        //     'importe'           => '123',
        //     'fecha'             => '20180201',
        //     'concepto'          => 'Abono mensual mes febrero',
        //     'cuotas'            => 1,
        //     'modalidad_cuotas'  => 'mes' //semana, semanal, semanales, quincena, quincenal, quincenales, mes, mensual o mensuales.
        // ];

        $envios = [
            'idComercio'        => $this->_cd_params->$modo->idComercio,
            'sid'               => $this->_cd_params->$modo->sid,
            'metodo_webservice' => $this->_cd_webservices->endpoint_generar_debito_automatico,
            'nombre'            => $debito->nombre,
            'apellido'          => $debito->apellido,
            'cuit'              => $debito->cuit,
            'email'             => $debito->email,
            'cbu'               => $debito->cbu,
            'importe'           => $debito->importe,
            'fecha'             => $debito->fecha,
            'concepto'          => $debito->concepto,
            'cuotas'            => $debito->cuotas,
            'modalidad_cuotas'  => $debito->modalidad_cuotas //semana, semanal, semanales, quincena, quincenal, quincenales, mes, mensual o mensuales.
        ];

        // Calls
        $result = $client->call($this->_cd_webservices->call_soap, array(json_encode($envios)));
        return json_decode($result);
    }

    public function checkPagos($desde, $hasta, $id_comercio, $sid)
    {
        // Config
        $client = new \nusoap_client("https://cobro.digital/ws/");

        $params = array();

        //$params['IdComercio'] = $id_comercio; 
        //$params['SID'] = $sid;

        //cem
        // $params['IdComercio'] = 'KH929692'; 
        // $params['SID']        = 'YEWDDSRRNJOACPZWOJTGULVGXPPKOWBNAYRERIVFRKFUAEQOOKVIWRPTHEE';

        //uvmundo2
        // $params['IdComercio'] = 'RR449949'; 
        // $params['SID']        = 'JAl12z61F6N36dCZC2DPKR6lc2JQnlHBL9SZ0EkPRc4alPxD98ovMkp38H1';

        //mediosueldo
        // $params['IdComercio'] = 'AU369755'; 
        // $params['SID']        = '68qNXmqb9a5993oe9cNBLE3093J7418Gi4yHALSRq23hbWv840TSv1Rf9I3';

        //cconectados wisp
        //$params['IdComercio'] = 'BJ537537'; 
        //$params['SID']        = 'LVgJI7w835nBM846qSb3G241jpeLw8OG0a0L4LtR20XDvcMNIOZ7R3T53M0';

        $params['fecha'] = 'movimiento'; // para buscar por fecha de moviemiento 
        $params['desde'] = $desde;   // desde fecha 
        $params['hasta'] = $hasta;   // hasta fecha

        $result = $client->call('CheckPagos', $params);
    }

    public function getBarcodeFromCobroDigital($barcode)
    {
        $result = file_get_contents('https://www.cobrodigital.com/wse/bccd/' . $barcode . 'h.png');
        return $result ? 'https://www.cobrodigital.com/wse/bccd/' . $barcode . 'h.png' : FALSE;
    }
}
