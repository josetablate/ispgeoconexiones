<?php

namespace App\Controller\Component;

use App\Controller\Component\Common\MyComponent;
use Cake\Controller\ComponentRegistry;
use Cake\Filesystem\File;
use Cake\Event\EventManager;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Log\Log;

/**
 * WebServices component
 */
class MercadoPagoComponent extends MyComponent
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    private $_mp = null;

    private $_payment_getway;

    public function initialize(array $config)
    {
        parent::initialize($config);
        $payment_getway = $this->_ctrl->request->getSession()->read('payment_getway');
        $this->_payment_getway = $payment_getway;
    }

    public function initEventManager()
    {
        EventManager::instance()->on(
            'MercadoPagoComponent.Error',
            function ($event, $msg, $data, $flash = false) {

                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $log->user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Error';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);

                if ($flash) {
                    $this->_ctrl->Flash->error(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'MercadoPagoComponent.Warning',
            function ($event, $msg, $data, $flash = false) {

                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $log->user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Warning';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);

                if ($flash) {
                    $this->_ctrl->Flash->warning(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'MercadoPagoComponent.Log',
            function ($event, $msg, $data) {

            }
        );

        EventManager::instance()->on(
            'MercadoPagoComponent.Notice',
            function ($event, $msg, $data) {

            }
        );
    }

    public function instance() 
    {
        $CLIENT_ID = $this->_payment_getway->config->mercadopago->CLIENT_ID;
        $CLIENT_SECRET = $this->_payment_getway->config->mercadopago->CLIENT_SECRET;
        $this->_mp = new \MP($CLIENT_ID, $CLIENT_SECRET);
    }

    public function getToken()
    {
        if (empty($this->_mp)) {
            $this->instance();
        }
        $mode = $this->_payment_getway->config->mercadopago->mode;
        $this->_mp->sandbox_mode($mode == 'sandbox');
        $access_token = $this->_payment_getway->config->mercadopago->$mode->access_token;
        return $access_token;
    }

    public function post_payments($payment_data)
    {
        $access_token = $this->getToken();
        $this->_mp = new \MP($access_token);
        $payment = $this->_mp->post("/v1/payments", $payment_data);
        return $payment;
    }

    public function search_payment()
    {
        $access_token = $this->getToken();
        if ($access_token == "") {
            return [];
        }
        $this->_mp = new \MP($access_token);
        //$CLIENT_ID = $this->_payment_getway->config->mercadopago->CLIENT_ID;
        //$payment = $this->_mp->get("/v1/payments/search?range=date_created&begin_date=NOW-1MONTH&end_date=NOW&status=approved&operation_type=regular_payment&external_reference=$CLIENT_ID&installments=1");
        //$payment = $this->_mp->get("/v1/payments/search?range=date_created&begin_date=NOW-20DAYS&end_date=NOW&limit=100");
        $payment = $this->_mp->get("/v1/payments/search?range=date_created&begin_date=NOW-20DAYS&end_date=NOW");

        /*$filters = array (
            "site_id"    => "MLA",
            "begin_date" => "NOW-3DAYS",
            "end_date"   => "NOW"
        );

        $payment = $this->_mp->search_payment($filters, 0, 100);*/

        $detail = 'Resultado de consultar transacciones - Mercado Pago';
        $detail .= 'Datos: ' . PHP_EOL;
        $detail .= 'CLIENT ID: ' . $this->_payment_getway->config->mercadopago->CLIENT_ID . PHP_EOL;
        $detail .= 'CLIENT SECRET: ' . $this->_payment_getway->config->mercadopago->CLIENT_SECRET . PHP_EOL;
        $detail .= 'Token: ' . $access_token . PHP_EOL;
        $detail .= 'Método: get' . PHP_EOL;
        $detail .= 'Parámetros: /v1/payments/search?range=date_created&begin_date=NOW-20DAYS&end_date=NOW' . PHP_EOL;
        $detail .=  '--------------------------------' . PHP_EOL;
        $detail .= 'Resultado: ' . PHP_EOL;
        $detail .= json_encode($payment);

        $path = $this->generateLog($detail);

        $action = 'Generación LOG - Mercado Pago';
        $detail = "";
        $detail .= 'Datos: ' . PHP_EOL;
        $detail .= 'CLIENT ID: ' . $this->_payment_getway->config->mercadopago->CLIENT_ID . PHP_EOL;
        $detail .= 'CLIENT SECRET: ' . $this->_payment_getway->config->mercadopago->CLIENT_SECRET . PHP_EOL;
        $detail .= 'Token: ' . $access_token . PHP_EOL;
        $detail .= 'Método: get' . PHP_EOL;
        $detail .= 'Parámetros: /v1/payments/search?range=date_created&begin_date=NOW-20DAYS&end_date=NOW' . PHP_EOL;
        $detail .= 'Path: ' . $path;

        $this->_ctrl->registerActivity($action, $detail, NULL, TRUE);

        return $payment;
    }

    private function generateLog($data)
    {
        $time = Time::now()->format('Ymd');

        $path = "log_payment_getway/$time-mercadopago.txt";

        $file = new File(WWW_ROOT . $path, true, 0775);
        $file->write($data, 'w+', TRUE);
        $file->close();

        return $path;
    }

    public function get_payments($id)
    {
        $access_token = $this->getToken();
        $this->_mp = new \MP($access_token);
        $payment = $this->_mp->get("/v1/payments/" . $id);
        return $payment;
    }

    public function createPaymentButton($item, $payer, $external_reference)
    {
        if (empty($this->_mp)) {
            $this->instance();
        }

        $excluded_payment_methods = [];

        foreach ($this->_payment_getway->config->mercadopago->excluded_payment_methods as $excluded_payment_method) {
            $excluded_pm = new \stdClass;
            $excluded_pm->id = $excluded_payment_method;
            // array_push($excluded_payment_methods, [
            //     "id" => $excluded_payment_method
            // ]);
            array_push($excluded_payment_methods, $excluded_pm);
        }

        $excluded_payment_types = [];

        foreach ($this->_payment_getway->config->mercadopago->excluded_payment_types as $excluded_payment_type) {
            $excluded_pt = new \stdClass;
            $excluded_pt->id = $excluded_payment_type;
            // array_push($excluded_payment_types, [
            //     "id" => $excluded_payment_type
            // ]);
            array_push($excluded_payment_types, $excluded_pt);
        }

        $unit_price = str_replace('.', '', $item->unit_price);
        $unit_price = floatval(str_replace(',', '.', $unit_price));

        $preference_data = array(
            "items" => array(
                array(
                    "title"       => $item->title,
                    "currency_id" => 'ARS',
                    "category_id" => "-",
                    "quantity"    => 1,
                    "unit_price"  => $unit_price
                )
            ),
        	"payer" => array(
        		"name"    => $payer->name,
        		"surname" => "",
        		"email"   => $payer->email,
        		"identification" => array(
        			"type"   => $payer->type,
        			"number" => $payer->number
        		)
        	),
        	"payment_methods" => array(
        		"excluded_payment_methods" => $excluded_payment_methods,
        		"excluded_payment_types"   => $excluded_payment_types,
        		"installments"             => intval($this->_payment_getway->config->mercadopago->installments),
        	),
        	"external_reference" => $external_reference
        );

        // $preference_data = array(
        //     "items" => array(
        //         array(
        //             "title"       => "01234-Algo",
        //             "currency_id" => "ARS",
        //             "category_id" => "-",
        //             "quantity"    => 1,
        //             "unit_price"  => 10.2
        //         )
        //     ),
        // 	"payer" => array(
        // 		"name"    => "user-name",
        // 		"surname" => "dasda",
        // 		"email"   => "user@email.com",
        // 		"identification" => array(
        // 			"type"   => "DNI",
        // 			"number" => "12345678"
        // 		)
        // 	),
        //  "payment_methods" => array(
        // 		"excluded_payment_methods" => array(
        // 			array(
        // 				"id" => "amex",
        // 			)
        // 		),
        // 		"excluded_payment_types" => array(
        // 			array(
        // 				"id" => "ticket"
        // 			)
        // 		),
        // 		"installments" => 24,
        // 		"default_payment_method_id" => null,
        // 		"default_installments" => null,
        // 	),
        // );

        try {

            return $this->_mp->create_preference($preference_data);
        } catch (\Exception $e) {

            Log::debug('$e');
            Log::debug($e);

            Log::debug('$preference_data');
            Log::debug($preference_data);

            $data_error = new \stdClass;
            $data_error->request_data = $e->getMessage();
            $data_error->payer = $payer;

            $event = new Event('MercadoPagoComponent.Error', $this, [
                'msg' => __('Error al generar el botón de pago de MercadoPago.'),
                'data' => $data_error,
                'flash' => true
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);
            return FALSE;
        }
    }
}
