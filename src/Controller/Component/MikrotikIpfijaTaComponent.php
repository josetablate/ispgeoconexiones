<?php
namespace App\Controller\Component;


use App\Controller\Component\Integrations\MikrotikIpfija;
// use Cake\Controller\ComponentRegistry;

use Cidr;
// use Cake\I18n\Time;
use Cake\Event\EventManager;
use Cake\Event\Event;

use Cake\Core\Configure;

use Cake\ORM\TableRegistry;
use Cake\ORM\Locator\TableLocator;

use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Query;



/**
 * Integration component
 */
class MikrotikIpfijaTaComponent extends MikrotikIpfija
{
    
    public function initialize(array $config) {  
         
        parent::initialize($config);
         
        $this->initEventManager();         
        
        $this->templateTableRegistry();
        
    }
    
    protected function initEventManager(){
         
         parent::initEventManager();
        
        EventManager::instance()->on(
            'MikrotikIpfijaTaComponent.Error',
            function($event, $msg, $data, $flash = false){
                
                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $log->user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Error';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);
                
                if($flash){
                    $this->_ctrl->Flash->error(__($msg));
                }
            }
        );
        
        EventManager::instance()->on(
            'MikrotikIpfijaTaComponent.Warning',
            function($event, $msg, $data, $flash = false){
                
                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $log->user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Warning';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);
                
                if($flash){
                    $this->_ctrl->Flash->warning(__($msg));
                }
            }
        );
        
        EventManager::instance()->on(
            'MikrotikIpfijaTaComponent.Log',
            function($event, $msg, $data){
                
            }
        );
        
        EventManager::instance()->on(
            'MikrotikIpfijaTaComponent.Notice',
            function($event, $msg, $data){
             
            }
        );
    }
     
    protected function templateTableRegistry(){
         
         if(!TableRegistry::getTableLocator()->exists('MikrotikIpfijaTa_Controllers')){
              
               TableRegistry::getTableLocator()->setConfig('MikrotikIpfijaTa_Controllers', [
                 'className' => 'App\Model\Table\MikrotikIpfijaTa\ControllersTable'
               ]);               
         }
         
         if(!TableRegistry::getTableLocator()->exists('MikrotikIpfijaTa_IpExcluded')){
              
               TableRegistry::getTableLocator()->setConfig('MikrotikIpfijaTa_IpExcluded', [
                 'className' => 'App\Model\Table\MikrotikIpfijaTa\IpExcludedTable'
               ]);               
         }
         
         if(!TableRegistry::getTableLocator()->exists('MikrotikIpfijaTa_Plans')){
              
               TableRegistry::getTableLocator()->setConfig('MikrotikIpfijaTa_Plans', [
                 'className' => 'App\Model\Table\MikrotikIpfijaTa\PlansTable'
               ]);               
         }
         
         if(!TableRegistry::getTableLocator()->exists('MikrotikIpfijaTa_Pools')){
              
               TableRegistry::getTableLocator()->setConfig('MikrotikIpfijaTa_Pools', [
                 'className' => 'App\Model\Table\MikrotikIpfijaTa\PoolsTable'
               ]);               
         }
         
         if(!TableRegistry::getTableLocator()->exists('MikrotikIpfijaTa_Gateways')){
              
               TableRegistry::getTableLocator()->setConfig('MikrotikIpfijaTa_Gateways', [
                 'className' => 'App\Model\Table\MikrotikIpfijaTa\GatewaysTable'
               ]);               
         }
         
         if(!TableRegistry::getTableLocator()->exists('MikrotikIpfijaTa_Profiles')){
              
               TableRegistry::getTableLocator()->setConfig('MikrotikIpfijaTa_Profiles', [
                 'className' => 'App\Model\Table\MikrotikIpfijaTa\ProfilesTable'
               ]);               
         }
         
         if(!TableRegistry::getTableLocator()->exists('MikrotikIpfijaTa_Queues')){
              
               TableRegistry::getTableLocator()->setConfig('MikrotikIpfijaTa_Queues', [
                 'className' => 'App\Model\Table\MikrotikIpfijaTa\QueuesTable'
               ]);               
         }
         
         if(!TableRegistry::getTableLocator()->exists('MikrotikIpfijaTa_AddressLists')){
              
               TableRegistry::getTableLocator()->setConfig('MikrotikIpfijaTa_AddressLists', [
                 'className' => 'App\Model\Table\MikrotikIpfijaTa\AddressListsTable'
               ]);               
         }
         
         if(!TableRegistry::getTableLocator()->exists('MikrotikIpfijaTa_Arps')){
              
               TableRegistry::getTableLocator()->setConfig('MikrotikIpfijaTa_Arps', [
                 'className' => 'App\Model\Table\MikrotikIpfijaTa\ArpsTable'
               ]);               
         }
         

          $this->modelControllers = $this->_ctrl->loadModel('MikrotikIpfijaTa_Controllers');
          $this->modelIpExcluded = $this->_ctrl->loadModel('MikrotikIpfijaTa_IpExcluded');
          $this->modelPlans = $this->_ctrl->loadModel('MikrotikIpfijaTa_Plans');
          $this->modelPools = $this->_ctrl->loadModel('MikrotikIpfijaTa_Pools');
          $this->modelGateways = $this->_ctrl->loadModel('MikrotikIpfijaTa_Gateways');
          $this->modelProfiles = $this->_ctrl->loadModel('MikrotikIpfijaTa_Profiles');
          $this->modelQueues = $this->_ctrl->loadModel('MikrotikIpfijaTa_Queues');
          $this->modelAddressLists = $this->_ctrl->loadModel('MikrotikIpfijaTa_AddressLists'); 
          $this->modelArps = $this->_ctrl->loadModel('MikrotikIpfijaTa_Arps'); 
      
    }   
    
     
     
    

    
    //controller
    
    public function addController($controller){
         
        $tController =  $this->modelControllers->newEntity();
        $tController->id = $controller->id;       
        $tController->address_list_name =  'IPFIJA-Clients';
        
        if(!$this->modelControllers->save($tController)){
            
            $data_error = new \stdClass; 
            $data_error->tController = $tController;
            $data_error->errors = $tController->getErrors();
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                'msg' => __('No se pudo registrar el Controlador.'),
                'data' => $data_error,
                'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
        return $tController;
    }
    
    public function editController($controller, $data){
         
        $tController = $this->modelControllers->get($controller->id);
        
        $tController =  $this->modelControllers->patchEntity($tController, $data);
        
        if(!$this->modelControllers->save($tController)){
            
            $data_error = new \stdClass; 
            $data_error->tController = $tController;
            $data_error->errors = $tController->getErrors();
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                'msg' => __('No se pudo editar el Controlador en la base de datos.'),
                'data' => $data_error,
                'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        return true;
    }
    
    public function getControllerTemplate($controller){
      
        return $this->modelControllers->get($controller->id);
    }
    
    public function deleteController($controller){  
        
        $tController = $this->modelControllers->get($controller->id);
        $tController->deleted = true;
        
        if(!$this->modelControllers->save($tController)){
            
            $data_error = new \stdClass; 
            $data_error->tController = $tController;
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
            'msg' => __('No se pudo eliminar el Controlador de la base de datos.'),
            'data' => $data_error,
            'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
          
        }
        return $tController;
    }
     
    public function getStatus($controller){          
         return $this->getStatusApi($controller);          
    }
     
    public function cloneController($id){
         
         $this->_ctrl->loadModel('Controllers');
         
         $error = false;
         
         $controller = $this->_ctrl->Controllers->get($id);
         $tcontroller = $this->getControllerTemplate($controller);
 
         $tcontroller->ip_excluded = $this->getIPExcluded($id);
         $tcontroller->profiles = $this->getProfiles($id);
         $tcontroller->pools = $this->getPools($id);
         $tcontroller->plans = $this->getPlans($id);
         $tcontroller->gateways = $this->getGateways($id);
         
         $newController = $this->_ctrl->Controllers->newEntity();    
         $newController = $this->_ctrl->Controllers->patchEntity($newController, $controller->toArray());
         
         $newController->name .= ' (Copia)';
         $newController->apply_config_avisos = false;
         $newController->apply_config_queue_graph = false;
         $newController->apply_config_https = false;
         $newController->apply_certificate_apissl = false;
         $newController->apply_certificate_wwwssl = false;
         
         if(!$this->_ctrl->Controllers->save($newController)){
              return false;
         }

         $newTController = $this->AddController($newController);  

         if(!$newTController){
              $this->Flash->error(__('No se pudo crear el (controller).')); 
              $error = true; 
         }    

        if(!$error){
              
            $parse_profiles = [];
            $parse_pool = [];
          
            $newTController->ip_excluded = [];
            $newTController->profiles = [];
            $newTController->pools = [];
            $newTController->plans = [];   
            $newTController->gateways = [];


            foreach($tcontroller->ip_excluded as $ie){

                 $data =  $ie->toArray();
                 $data['controller_id'] = $newTController->id;         
                 $new_ip_exc = $this->addIPExcludedDirect($data);     
                
                 if(!$new_ip_exc){                              
                    
                      $error = true;
                 
                      $data_error = new \stdClass; 
                      $data_error->data = $data;               

                      $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                           'msg' => __('No se pudo crear la ip excluida'),
                           'data' => $data_error,
                           'flash' => false
                      ]);

                      $this->_ctrl->getEventManager()->dispatch($event);

                      break;
                 }
                 
                 $newTController->ip_excluded[] = $new_ip_exc;                                
           }

           if(!$error){

                foreach($tcontroller->profiles as $pr){
                     
                      $parse_profiles[$pr->id] = null;

                      $data =  $pr->toArray();
                      $data['controller_id'] = $newTController->id;         
                      $new_profile = $this->addProfile($data);     
                     
                      if(!$new_profile){
                           
                           $error = true;
                 
                           $data_error = new \stdClass; 
                           $data_error->data = $data;               

                           $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                                'msg' => __('No se pudo crear el (profile)'),
                                'data' => $data_error,
                                'flash' => false
                           ]);

                           $this->_ctrl->getEventManager()->dispatch($event);

                           break;                                   
                      }
                     
                      $parse_profiles[$pr->id] = $new_profile->id; 
                     
                      $newTController->profiles[] = $new_profile;                                
                }

                if(!$error){

                     foreach($tcontroller->pools as $po){
                          
                           $parse_pool[$po->id] = null;

                           $data =  $po->toArray();
                           $data['controller_id'] = $newTController->id;                                   
                           $data['next_pool_id'] = null;
                          
                           $new_pool =  $this->addPool($data); 
                          
                           if(!$new_pool){
                                
                                $error = true;
                 
                                $data_error = new \stdClass; 
                                $data_error->data = $data;               

                                $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                                     'msg' => __('No se pudo crear el (pool)'),
                                     'data' => $data_error,
                                     'flash' => false
                                ]);

                                $this->_ctrl->getEventManager()->dispatch($event);

                                break;
                           }
                          
                           $parse_pool[$po->id] = $new_pool->id;
                          
                           $newTController->pools[] = $new_pool;   
                     }

                    if(!$error){

                        foreach($tcontroller->plans as $pl){

                            $data =  $pl->toArray();
                           
                            $data['controller_id'] = $newTController->id;                                          
                            $data['profile_id'] = $parse_profiles[$data['profile_id']];
                           
                            if($data['pool_id']){
                                 $data['pool_id'] = $parse_pool[$data['pool_id']];
                            }        
                           
                            $data['pool'] = null;
                            $data['profile'] = null;                                       
                           
                            $new_plan = $this->addPlan($data);    
                           
                            if(!$new_plan){
                                 
                                 $error = true;
             
                                 $data_error = new \stdClass; 
                                 $data_error->data = $data;               

                                 $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                                      'msg' => __('No se pudo crear el plan'),
                                      'data' => $data_error,
                                      'flash' => false
                                 ]);

                                 $this->_ctrl->getEventManager()->dispatch($event);

                                 break;
                            }
                            $newTController->plans[] = $new_plan;
                        }
                        
                        if(!$error){
    
                            foreach($tcontroller->gateways as $ga){
    
                                $data =  $ga->toArray();
                                
                                $data['controller_id'] = $newTController->id; 
                           
                                if($data['pool_id']){
                                     $data['pool_id'] = $parse_pool[$data['pool_id']];
                                }        
                               
                                $data['pool'] = null;
                               
                                $new_gateway = $this->addGateway($data);    
                               
                                if(!$new_gateway){
                                     
                                     $error = true;
                 
                                     $data_error = new \stdClass; 
                                     $data_error->data = $data;               
    
                                     $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                                          'msg' => __('No se pudo crear el (gateway)'),
                                          'data' => $data_error,
                                          'flash' => false
                                     ]);
    
                                     $this->_ctrl->getEventManager()->dispatch($event);
    
                                     break;
                                }
                                $newTController->gateways[] = $new_gateway;
                            }                                       
                        } 
                    }                                  
                }
            }     
             
         }

         if($error){
              
              $this->modelPlans->deleteAll(['controller_id' => $newController->id]);
              $this->modelPools->deleteAll(['controller_id' => $newController->id]);
              $this->modelProfiles->deleteAll(['controller_id' => $newController->id]);
              $this->modelIpExcluded->deleteAll(['controller_id' => $newController->id]);
              $this->modelPppoeServers->deleteAll(['controller_id' => $newController->id]);
              $this->modelControllers->deleteAll(['controller_id' => $newController->id]);
                                                  
              $this->_ctrl->Controllers->delete($newController);     
                                                  
             return false;
              
         }
     
         return true;
     
    }
    
    public function generateArpsFromMikrotik($controller){     
        
        $controller->tcontroller =  $this->getControllerTemplate($controller);
        
        if(!$controller->tcontroller->arp){
            
            $data_error = new \stdClass; 

            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                'msg' => __('No se pude regerar los (arp) si el controlador tiene la opción de ARP deshabilitada.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
        $connections_amount = 0;
        $arps_not_changes_amount = 0;
        $arps_not_create_amount = 0;
        
        $this->_ctrl->loadModel('Connections');
        
        $connections = $this->_ctrl->Connections->find()
                    ->contain(['Customers'])
                    ->where([
                        'Connections.deleted' => false,
                        'Connections.controller_id' => $controller->id
                        ]);
                
        
        $arpsArray = $this->getArpsForMigrationApi($controller);
        
        foreach($connections as $connection){
            
            $connections_amount++;
            
            $connection->diff = true;
            
            $arpdb = $this->modelArps->find()
                ->where([
                    'connection_id' => $connection->id,
                    'controller_id' => $controller->id
                    ])
                ->first();
                
            if($arpdb){
                
                $arpmt = array_key_exists($connection->ip, $arpsArray) ? $arpsArray[$connection->ip] : null ;
                
                $this->_ctrl->log($arpmt, 'debug');
                
                if($arpmt && $arpmt['dynamic'] == 'true'){
                    
                    $arpmt['mac_address'] = str_replace(':', '', $arpmt['mac_address']);
                    $arpmt['mac_address'] = str_replace(' ', '', $arpmt['mac_address']);
                    $arpmt['mac_address'] = str_replace('-', '', $arpmt['mac_address']);
                    
                    $arpdb->mac_address = $arpmt['mac_address'];
                    $arpdb->interface = $arpmt['interface'];
                    
                    $connection->mac = $arpmt['mac_address'];
                    
                    if(!$this->modelArps->save($arpdb)){
                        
                        $arps_not_changes_amount++;
                        
                        $data_error = new \stdClass; 
                        $data_error->arp = $arpdb;
                        $data_error->errors = $arpdb->errors();
            
                        $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                            'msg' => __('Error al intentar editar un (arp)'),
                            'data' => $data_error,
                            'flash' => false
                        ]);
                        
                        $this->_ctrl->getEventManager()->dispatch($event);
                        
                        return false;
                    }
                    
                    
                    
                    $connection->diff = false;
                    
                }
                else if($arpmt){
                    
                    $connection->diff = false;
                }
                else{ // no econtro el arp en el mt
                
                    $arps_not_changes_amount++;
                    
                }
                
            }else{ //no existe el arp en la base de datos
                
                $arpmt = array_key_exists($connection->ip, $arpsArray) ? $arpsArray[$connection->ip] : null ;
                
                if($arpmt && $arpmt['dynamic'] == 'true'){
                    
                    $arpmt['mac_address'] = str_replace(':', '', $arpmt['mac_address']);
                    $arpmt['mac_address'] = str_replace(' ', '', $arpmt['mac_address']);
                    $arpmt['mac_address'] = str_replace('-', '', $arpmt['mac_address']);
                    
                    $connection->mac = $arpmt['mac_address'];
                    $connection->interface = $arpmt['interface'];
                    
                    $connection->comment = sprintf("%'.05d",  $connection->customer->code) .' - '. $connection->customer->name;
                    
                    if(!$this->addArp($connection)){
                            
                        $arps_not_create_amount++;
                        
                        $data_error = new \stdClass; 
                        $data_error->connection = $connection;
                        $data_error->errors = $connection->errors();
            
                        $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                            'msg' => __('Error al intentar crear un (arp)'),
                            'data' => $data_error,
                            'flash' => false
                        ]);
                        
                        $this->_ctrl->getEventManager()->dispatch($event);
                        
                        return false;
                    }
                    
                    $connection->diff = false;
                }
                else if($arpmt){
                    $arps_not_create_amount++;
                }
                
                
            }
            
            $this->_ctrl->Connections->save($connection);
            
        }
     
        return [
            'connections_amount' => $connections_amount,
            'arps_not_changes_amount' => $arps_not_changes_amount,
            'arps_not_create_amount' => $arps_not_create_amount,
            ];
             
    }
    
    
    
    //profiles
    
    public function getProfiles($controller_id){                            
       
        $profiles = $this->modelProfiles->find()
            ->where([
                'controller_id' => $controller_id
                ]);
        
        foreach($profiles as $profile){
            
            $profile->connections_amount = $this->modelQueues->find()
                ->where([
                    'profile_id' => $profile->id,
                    'controller_id' => $profile->controller_id
                    ])
                ->count();
        }
    
        return $profiles;
    
    }
    
    public function getProfilesArray($controller_id){
        
        //busca el objeto en la base del template
        $profiles =  $this->modelProfiles->find()
            ->where([
                'controller_id' => $controller_id
                ]);
        
        $profilesArray = [];
        foreach ($profiles as $profile) {
            
            $profilesArray[$profile->name] = $profile;
        }
        
        return $profilesArray;
    
    }
    
    public function getProfilesInController($controller){
        return $this->getProfilesApi($controller);
    }
    
    public function addProfile($data){        
        
        $profile = $this->modelProfiles->find()
            ->where([
                'name' => $data['name'],
                'controller_id' => $data['controller_id']
                ])
            ->first();
        
        if($profile){
            
            $data_error = new \stdClass; 
            $data_error->other_profile = $profile;
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
            'msg' => __('Ya existe un (profile) con el mismo nombre en este controlador.'),
            'data' => $data_error,
            'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
          
            return false;
        }
   
        $profile = $this->modelProfiles->newEntity();
        $profile = $this->modelProfiles->patchEntity($profile, $data );
        
        if(!$this->modelProfiles->save($profile)){
            
            $data_error = new \stdClass; 
            $data_error->profile = $profile;
            $data_error->errors = $profile->getErrors();
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                'msg' => __('Error al intentar agregra el (profile) a la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
           
            return false;
        }
   
        return $profile;
        
    }
    
    public function deleteProfile($id){        
        
        $count = $this->modelPlans->find()
            ->where([
                'profile_id' => $id
                ])
            ->count();
        
        if($count > 0){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                'msg' => __('No se puede eliminar. Existe {0} Plan(es) relacionado(s) a este  (profile).', $count),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
        $count = $this->modelQueues->find()
            ->where([
                'profile_id' => $id
            ])
            ->count();
        
        if($count > 0){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                'msg' => __('No se puede eliminar. Existe {0} Queue(s) relacionado(s) a este  (profile).', $count),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
        $profile = $this->modelProfiles->get($id);
        
        if(!$this->modelProfiles->delete($profile)){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                'msg' => __('Error al intentar eliminar el (profile) de la base de datos'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
      
        return $profile;
    }
    
    public function editProfile($data){
    
        $profile = $this->modelProfiles->find()
            ->where([
                'name' => $data['name'],
                'id !=' => $data['id'],
                'controller_id' => $data['controller_id']
                ])
            ->first();
        
        if($profile){
            
            $data_error = new \stdClass;
            $data_error->other_profile = $profile;
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                'msg' => __('Ya existe un (profile) con el mismo nombre'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
   
        $profile = $this->modelProfiles->get($data['id']);
        $profile = $this->modelProfiles->patchEntity($profile, $data );
        
        if(!$this->modelProfiles->save($profile)){
            
            $data_error = new \stdClass;
            $data_error->profile = $profile;
            $data_error->errors =  $profile->getErrors();
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                'msg' => __('Ya existe un (profile) con el mismo nombre'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
 
        return $profile;
        
    }
    
    public function deleteProfileInController($controller, $tprofile_api_id){
      return $this->deleteProfilebyApiIdApi($controller, $tprofile_api_id);
    }
    
    public function syncProfile($profile_id){
        
        $profile =  $this->modelProfiles->get($profile_id);
        
        $this->_ctrl->loadModel('Controllers');
        $controller = $this->_ctrl->Controllers->get($profile->controller_id);

        if(!$this->integrationEnabled($controller)){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
            	'msg' => __('Intento de sinconizacion de (profile). La integración esta desactivada.'),
            	'data' => $data_error,
            	'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            return false;  
        }
      
        $profile = $this->syncProfileApi($controller, $profile);
        
        if(!$profile){
            return false;
        }
        
        //limpia los api_id que existan duplicados
        $duplicates = $this->modelProfiles->find()
        	->where([
        	'api_id' => $profile->api_id,
        	'id !=' => $profile->id,
        	'controller_id' => $profile->controller_id
        	]);
        
        foreach($duplicates as $duplicated){
        
        	$duplicated->api_id = NULL;
        	$this->modelProfiles->save($duplicated);
        }
        
      
        if(!$this->modelProfiles->save($profile)){
            
            $data_error = new \stdClass; 
            $data_error->profile = $profile;
            $data_error->errors = $profile->getErrors();
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
            	'msg' => __('Error al intentar actualizar el (profile).'),
            	'data' => $data_error,
            	'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;  
        }
    
        return $profile;  
        
    }
    
    
    
    //pools
    
    public function getPools($controller_id){
         
        $pools = $this->modelPools->find()
            ->where([
                'controller_id' => $controller_id,
                ]);
           
        foreach($pools as $pool){
            
            $pool->connections_amount = $this->modelQueues->find()
                ->where([
                    'pool_id' => $pool->id,
                    'controller_id' => $pool->controller_id
                    ])
                ->count();
            
            if($pool->next_pool_id){
                $pool->next_pool =  $this->modelPools->get($pool->next_pool_id);
            }
        }
       
        return $pools;
        
    }
    
    public function addPool($data){
        
        $tpool = $this->modelPools->find()
            ->where([
                'name' => $data['name'],
                'controller_id' => $data['controller_id']
                ])
            ->first();
        
        if($tpool){
            
            $data_error = new \stdClass; 
            $data_error->other_tpool = $tpool;
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                'msg' => __('Ya existe un (pool) con el mismo nombre en este controlador.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
        
         $tpool = $this->modelPools->find()
            ->where([
                'addresses' => $data['addresses'],
                'controller_id' => $data['controller_id']
                ])
            ->first();
        
        if($tpool){
            
            $data_error = new \stdClass; 
            $data_error->other_tpool = $tpool;
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                'msg' => __('Ya existe un (pool) con la misma red en este controlador.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
   
        $tpool = $this->modelPools->newEntity();
        $tpool = $this->modelPools->patchEntity($tpool, $data );
        
        $tpoolnext_pool = $this->modelPools->find()
            ->where([
                'next_pool_id' => $tpool->next_pool_id
                ])
            ->first();
         
        if($tpoolnext_pool){
          
            $data_error = new \stdClass; 
            $data_error->other_tpool = $tpoolnext_pool;
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                'msg' => __('El (pool) siguiente ya esta usando. '),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
        
        
        if(!$this->modelPools->save($tpool)){
            
            $data_error = new \stdClass; 
            $data_error->tpool = $tpool;
            $data_error->errors = $tpool->getErrors();
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                'msg' => __('Error al intentar agregar el (pool) en al base de datos'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
   
        return $tpool;
        
    }
    
    public function editPool($data){
        
        $pool = $this->modelPools->find()
            ->where([
                'name' => $data['name'],
                'id !=' => $data['id'],
                'controller_id' => $data['controller_id']
                ])
            ->first();
        
        if($pool){
            
            $data_error = new \stdClass; 
            $data_error->other_pool = $pool;
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                'msg' => __('Ya existe un (pool) con el mismo nombre en este controlador'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
        
        $pool = $this->modelPools->find()
            ->where([
                'addresses' => $data['addresses'],
                'id !=' => $data['id'],
                'controller_id' => $data['controller_id']
                ])
            ->first();
        
        if($pool){
            
            $data_error = new \stdClass; 
            $data_error->other_pool = $pool;
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                'msg' => __('Ya existe un (pool) con la misma red en este controlador'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
        
        $pool = $this->modelPools->get($data['id']);
         
        if($pool->id != $data['addresses']){
             
             if($this->validatePoolUsed($pool)){
             
                 $data_error = new \stdClass; 

                 $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                     'msg' => __('La red de este (pool) no puede ser editado por que existen elementos relacionados'),
                     'data' => $data_error,
                     'flash' => false
                 ]);

                 $this->_ctrl->getEventManager()->dispatch($event);

                 return false;
             }             
        }       
                
        
        
        $pool_old = $pool;
      
        $pool = $this->modelPools->patchEntity($pool, $data );
        
        $pool_next_pool = $this->modelPools->find()
            ->where([
                'next_pool_id' => $pool->next_pool_id,
                'id != ' => $pool->id,
                'controller_id' => $data['controller_id'],
                ])
            ->first();
         
        if($pool_next_pool){
          
            $data_error = new \stdClass; 
            $data_error->other_tpool = $pool_next_pool;
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                'msg' => __('El (pool) siguiente ya esta usando.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
        
       
        if(!$this->modelPools->save($pool)){
            
            $data_error = new \stdClass; 
            $data_error->pool = $pool;
            $data_error->errors = $pool->getErrors();
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                'msg' => __('Error al intentar editar el (pool) en al base de datos'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
       

        return $pool;
        
    }     
   
    public function deletePool($id){
        
        $count = $this->modelPlans->find()->where(['pool_id' => $id])->count();
        
        if($count > 0){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                'msg' => __('No se puede eliminar. Existe {0} Plan(es) relacionado(s) a este (pool).', $count),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
            
        }

        $count = $this->modelQueues->find()->where(['pool_id' => $id])->count();
        
        if($count > 0){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                'msg' => __('No se puede eliminar. Existe {0} queues(es) relacionado(s) a este (pool).', $count),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
            
        }

        $pool = $this->modelPools->get($id);       
    
        $pool_next = $this->modelPools->find()->where(['next_pool_id' => $pool->id])->first();
        
        if($pool_next){
            
            $data_error = new \stdClass; 
            $data_error->other_pool = $pool_next;
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                'msg' => __('No se puede eliminar por que es (pool) siguiente del (pool): {0}', $pool_next->name),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
        
        if(!$this->modelPools->delete($pool)){
            
            $data_error = new \stdClass; 
            $data_error->pool = $pool;
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                'msg' => __('Error al intentar eliminar el (pool) de la base de datos'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
      
        return $pool;
    }
  
    protected function validatePoolUsed($pool){
        
        $min_host = $pool->min_host;
        $max_host = $pool->max_host;
      
        $queue = $this->modelQueues->find()->where([
                'target >= ' => ip2long($min_host),
                'target < ' => ip2long(Cidr::nextIp($max_host)),
                'controller_id' => $pool->controller_id
            ])->first();
            
        if($queue){
            return true;
        }
        return false;
    } 
   
    public function movePool($data) {
        
        $this->_ctrl->loadModel('Controllers');
        $this->_ctrl->loadModel('Connections');
        $this->_ctrl->loadModel('FirewallAddressLists');
        $this->_ctrl->loadModel('ConnectionsHasMessageTemplates');        
        
        $pool_from = $this->modelPools->get($data['pool_from_id']);
        $pool_to = $this->modelPools->get($data['pool_to_id']);
        
        $controller = $this->_ctrl->Controllers->get($pool_from->controller_id);
        
        $queuesFix = $this->modelQueues->find()->where(['pool_id' => $pool_from->id]);
         
         $this->_ctrl->log($queuesFix, 'debug');
        
        foreach($queuesFix as $queue){
            
            //busco ip libre
            $ip_data = $this->getFreeIPByPoolId($controller, $pool_to->id);
            
            if(!$ip_data){
                return false;
            }
                
                
            //connection
            $connection = $this->_ctrl->Connections->get($queue->connection_id);
            $connection->ip = $ip_data['ip'];
            $connection->diff = true;
            $this->_ctrl->Connections->save($connection);
                
                
            //queue
            $queue->target = ip2long($connection->ip);
            $queue->pool_id = $ip_data['pool_id'];
            $this->modelQueues->save($queue);
            
            //address list cliente
            $addressListClient = $this->modelAddressLists->find()->where(['connection_id' => $queue->connection_id])->first();
            $addressListClient->address = ip2long($connection->ip);
            $this->modelAddressLists->save($addressListClient);   
            
            
            //FirewallAddressLists
            $this->_ctrl->FirewallAddressLists->updateAll(
                [  
                    'address' => $connection->ip 
                ],
                [  
                    'connection_id' => $connection->id
                ]
            );
            
            //ConnectionsHasMessageTemplates
            $this->_ctrl->ConnectionsHasMessageTemplates->updateAll(
                [  
                    'ip' => $connection->ip 
                ],
                [  
                    'connections_id' => $connection->id
                ]
            );
        }
        
        return true;
        
    }
    
    
    
    
    //ip free
    
    protected function getIpPool($tpool_id){
         
        //ips de pools
        /**
         * $ips_pool = [
         *      ip long => pool_id
         * 
         *      168430081 => 1,
         *      168430075 => 1,
         * 
         *      168440020 => 2,
         * ]
        */
        
        $ips_pool = [];
        
        do{
            $pool = $this->modelPools->get($tpool_id);
            
            $ips = range(ip2long($pool->min_host), ip2long($pool->max_host));
            $ips = array_fill_keys($ips, $pool->id);
            $ips_pool += $ips;
           
            $tpool_id = $pool->next_pool_id;            
      
            
        }while($tpool_id);

        return $ips_pool;
        
    }
    
    protected function getIpBusy($tpool_id){
        
        //ips usadas
        /**
         * $ips_busy = [
         *      ip long => pool_id
         * 
         *      168430081 => 1,
         *      168430075 => 1,
         * 
         *      168440020 => 2,
         * ]
        */
        
        $ips_busy = [];
        
        do{
            
            $pool = $this->modelPools->get($tpool_id);
            
            $ips = $this->modelQueues->find('list', [
                    'keyField' => 'target',
                    'valueField' => 'pool_id'
                ])
                ->where([
                    'pool_id' => $pool->id,
                    'controller_id' => $pool->controller_id
                    ])
                ->toArray();
       
            $ips_busy += $ips;
       
            $tpool_id = $pool->next_pool_id;
            
        }while($tpool_id);
      
        return $ips_busy;
        
    }
    
    public function getFreeIPByPoolId($controller, $tpool_id){
        
        $ips_pool = $this->getIpPool($tpool_id);
        
        $ips_busy = $this->getIpBusy($tpool_id);
        
        foreach($ips_pool as  $ip => $pool_id){
            if((!$this->validateIPExcluded($controller->id, $ip) && !array_key_exists($ip, $ips_busy))){
                return ['ip' => long2ip($ip), 'pool_id' => $pool_id];
            }
        }

        return false;
    
    }
   
    protected function validateIPByController($controller, $ip){
         
        $pool_selected = null;
        
        
        //valido que la ip no este usada en alguna queue
        
        $tqueue = $this->modelQueues->find()->where([
            'target' => ip2long($ip),
            'controller_id' => $controller->id
        ])->first();
        
        if($tqueue){
        
            $data_error = new \stdClass;
            $data_error->tqueue = $tqueue;
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
            'msg' => __('la IP: {0} esta ocupada por otra conexión en este contrador.', $ip),
            'data' => $data_error,
            'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
         
            return false;
        }
        
        
        //valido si la ip no esta en alista de ip excluidas
        
        $exist = $this->validateIPExcluded($controller->id, $ip);
        
        if($exist){
            
            $data_error = new \stdClass; 
            $data_error->ip = $ip;
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                'msg' => __('la IP: {0} se encuentra en la lista de IP Excluidas.', $ip),
                'data' => $data_error,
                'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
        
        //valido que la ip perteneczca alguan pool del controlador
        
        $pools = $this->modelPools->find()->where(['controller_id' => $controller->id]);
        
        $pool_valid = false;
        
        foreach($pools as $pool){
       
            if(Cidr::cidr_match($ip, $pool->addresses)){
                $pool_valid = true;
                $pool_selected = $pool;
                break;
            }
        }
        
        
        if(!$pool_valid){
            
            $data_error = new \stdClass; 
            $data_error->ip = $ip;
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                'msg' => __('la IP: {0} no pertece a ninguna red del controlador.', $ip),
                'data' => $data_error,
                'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
            
        }

        return ($pool_valid) ? ['ip' => $ip, 'pool_id' => $pool_selected->id] : false;
    }
    
    
    
    
     //ip excluded
    
    public function getIPExcluded($controller_id){
        
        return $this->modelIpExcluded->find()
            ->where(['controller_id' => $controller_id]);
           
    }

    public function addIPExcludedDirect($data){
        
        $rango = false;
        
        if(strpos($data['ip'], '-') !== false){  //rango
        
            $rango = true;
            
            $temp = explode('-', $data['ip']);
            $ip_from = $temp[0];
            $ip_to = $temp[1];
            
            if(!Cidr::validIP($ip_from) || !Cidr::validIP($ip_to)){
                
                $data_error = new \stdClass; 
                $data_error->ip = $data['ip'];
                
                $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                    'msg' => __('Algunas de las IP ingresada no es valida.'),
                    'data' => $data_error,
                    'flash' => false
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
         
                return false;
                
            } 
            
            $ip_from    = ip2long($temp[0]);
            $ip_to      = ip2long($temp[1]);
            
            if($ip_from >= $ip_to){
                
                $data_error = new \stdClass; 
                $data_error->ip = $data['ip'];
                
                $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                    'msg' => __('{0} >= {1} Rango invalido.', $temp[0], $temp[1] ),
                    'data' => $data_error,
                    'flash' => false
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
         
                return false;
            } 
            
        } else { //no es un rango
            
            if(!Cidr::validIP($data['ip'])){ 
                
                $data_error = new \stdClass; 
                $data_error->ip = $data['ip'];
                
                $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                    'msg' => __('La IP ingresa no es valida.'),
                    'data' => $data_error,
                    'flash' => false
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
         
                return false;
            }
        }
        
        //falta validada si la ip o rango de ip esta dentro de alguin rango ya excluido 
       
        $ipExcuded = $this->modelIpExcluded->find()
            ->where([
                'ip' => $data['ip'],
                'controller_id' => $data['controller_id']
                ])
            ->first();
            
        if($ipExcuded){
            
            $data_error = new \stdClass; 
            $data_error->ip = $data['ip'];
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                'msg' => __('La {0} ya esta excluida es este controlador.', $data['ip']),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
     
            return false;
        }
        
        
        if($rango){
            
            $temp = explode('-', $data['ip']);
            $ip_from    = ip2long($temp[0]);
            $ip_to      = ip2long($temp[1]);
            
            $tqueue = $this->modelQueues->find()
                ->where(function (QueryExpression $exp, Query $q) use ($ip_from, $ip_to) {
                return $exp->between('target', $ip_from, $ip_to);
            })->first();
        
        }else{
            
            $tqueue = $this->modelQueues->find()
                ->where(['target' => ip2long($data['ip']),
                    'controller_id' => $data['controller_id']
                    ])
                ->first();
        }
        
        
        if($tqueue){
            
            $data_error = new \stdClass; 
            $data_error->ip = $data['ip'];
            $data_error->tqueue = $tqueue;
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                'msg' => __('{0} ingresada no esta libre.', $data['ip']),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
     
            return false;
        }
       
       
        $ipExcluded = $this->modelIpExcluded->newEntity();
        
        $ipExcluded = $this->modelIpExcluded->patchEntity($ipExcluded, $data );

        if(!$this->modelIpExcluded->save($ipExcluded)){
            
            $data_error = new \stdClass; 
            $data_error->ipExcluded = $ipExcluded->errors();
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                'msg' => __('Error al intentar agregar la IP Excluida a la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
     
            return false;
        }

        return $ipExcluded;
    }

    public function deleteIPExcludedById($id){
        
        $ipExcluded = $this->modelIpExcluded->get($id);
        
        if(!$this->modelIpExcluded->delete($ipExcluded)){
            
            $data_error = new \stdClass; 
            $data_error->ipExcluded = $ipExcluded;
            $data_error->errors = $ipExcluded->getErrors();
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                'msg' => __('Error al Intentar agreghar la Ip Excluded a la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
      
            return false;
        }
      

        return $ipExcluded;
   
    }
   
    protected function validateIPExcluded($controller_id, $ip){
         
        $ipExcluidasRango = $this->modelIpExcluded->find()
            ->where([
                'ip like' => '%-%',
                'controller_id' => $controller_id
                ]);
        
        foreach($ipExcluidasRango as $ip_e_r){
            
            $temp = explode('-', $ip_e_r->ip);
            $ip_from = ip2long($temp[0]);
            $ip_to = ip2long($temp[1]);
            
            if($ip_from <= $ip){
                if($ip <= $ip_to){
                    return true;
                }
            }
        }
        
        return ($this->modelIpExcluded->find()
            ->where([
                'controller_id' => $controller_id, 
                'ip not like' => '%-%',
                'ip' => long2ip($ip)
                ])->first()) ? true : false;
           
    }
    
    
    
    
    //planes
    
    public function getPlans($controller_id){
        
        $tplans =  $this->modelPlans->find()
            ->contain(['MikrotikIpfijaTa_Profiles', 'MikrotikIpfijaTa_Pools'])
            ->where([
                'MikrotikIpfijaTa_Plans.controller_id' => $controller_id,
                ]);
                
        $this->_ctrl->loadModel('Services');
        
        foreach ($tplans as $plan) {
            
            $plan->connections_amount = $this->modelQueues->find()
                ->where([
                    'plan_id' => $plan->id,
                    'controller_id' => $controller_id
                    ])
                ->count();
                
            $plan->service = $this->_ctrl->Services->get($plan->service_id);
        }
        
        return $tplans;
    }
    
    public function getPlan($controller, $id){
        
        return $this->modelPlans->find()
            ->contain(['MikrotikIpfijaTa_Profiles', 'MikrotikIpfijaTa_Pools'])
            ->where([
                'MikrotikIpfijaTa_Plans.id' => $id,
                ])->first();
    }

    public function addPlan($data){
    
        $tplan = $this->modelPlans->find()
            ->where([
                'service_id' => $data['service_id'],
                'controller_id' => $data['controller_id']
                ])
            ->first();
       
        if($tplan){
            
            $data_error = new \stdClass; 
            $data_error->other_plan = $tplan;
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                'msg' => __('Este servicio ya existe en el controlador'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
   
        $tplan = $this->modelPlans->newEntity();
        $tplan = $this->modelPlans->patchEntity($tplan, $data );
        
        if(!$this->modelPlans->save($tplan)){
            
            $data_error = new \stdClass; 
            $data_error->tplan = $tplan;
            $data_error->errors = $tplan->getErrors();
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                'msg' => __('Error al intentar agregar el Servicio '),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
      
            return false;
        }

        return $tplan;
    }
    
    public function editPlan($data){
        
        $plan = $this->modelPlans->get($data['id']);
        
        $plan->pool_id = $data['pool_id'] ? $data['pool_id'] : null;
       
        if($plan->profile_id != $data['profile_id'] ){ //cambio de profile
        
            $profile = $this->modelProfiles->get($data['profile_id']);
        
            $queuesFixProfile = $this->modelQueues->find()->where([
                'plan_id' => $plan->id,
                'controller_id' => $plan->controller_id
            ]);
            
            $this->_ctrl->loadModel('Connections');
             
            foreach($queuesFixProfile as $queue){
                
                //marco diferencia en conexiones
                $connection = $this->_ctrl->Connections->get($queue->connection_id);
                $connection->diff = true;
                $this->_ctrl->Connections->save($connection);
                
                //edito queue
                $queue->profile_id = $profile->id;
                $this->modelQueues->save($queue);                  
                
            }
            
            //edito plan
            $plan->profile_id = $data['profile_id'];
            
            if(!$this->modelPlans->save($plan)){
            
                $data_error = new \stdClass; 
                $data_error->plan = $plan;
                
                $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                    'msg' => __('Error al intentar editar el Plan.'),
                    'data' => $data_error,
                    'flash' => false
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
       
                return false;
            }
            
        }
        else{
            
             //edito plan
            if(!$this->modelPlans->save($plan)){
            
                $data_error = new \stdClass; 
                $data_error->plan = $plan;
                
                $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                    'msg' => __('Error al intentar editar el Plan.'),
                    'data' => $data_error,
                    'flash' => false
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
       
                return false;
            }
        }
        
        
        return $plan;
        
    }
    
    public function deletePlan($id){
        
        $count = $this->modelQueues->find()->where(['plan_id' => $id])->count();
        
        $plan = $this->modelPlans->get($id);
        
        if($count > 0){
            
            $data_error = new \stdClass; 
            $data_error->plan = $plan;
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                'msg' => __('No se puede eliminar. Existe {0} Queue(s) relacionado(s) a este plan.', $count),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
      
            return false;    
        }
        
       
        
        if(!$this->modelPlans->delete($plan)){
            
            $data_error = new \stdClass; 
            $data_error->tplan = $plan;
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                'msg' => __('Error al intentar eliminar el Plan'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
      
            return false;  
           
        }

        return $plan;
    }
     
    
    
    
    
    
    //queue
    
    protected function addQueue($connection){
         
        $data = [
               'name' => $connection->queue_name,
               'target' => ip2long($connection->ip),
               'comment' => $connection->comment,
               'connection_id' => $connection->id,
               'controller_id' => $connection->controller_id,
               'pool_id' => $connection->pool_id,
               'profile_id' => $connection->profile_id,
               'plan_id' => $connection->plan_id,
               ];
    
    
        $queue = $this->modelQueues->newEntity();
        $queue = $this->modelQueues->patchEntity($queue, $data);
        
        if(!$this->modelQueues->save($queue)){
            
            $data_error = new \stdClass; 
            $data_error->queue = $queue;
            $data_error->errors = $queue->getErrors();
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                'msg' => __('Error al Intentar agregar el Queue en la base de datos.'),
                'data' => $data_error,
                'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
    
        return $queue;
        
    }
    
    protected function deleteQueue($connection){
    
        $connection->queue = $this->modelQueues->find()
            ->where([
                'connection_id' => $connection->id
                ])
            ->first();
        
        if($connection->queue){
            
            if(!$this->modelQueues->delete($connection->queue)){
                
                $data_error = new \stdClass; 
                $data_error->queue = $connection->queue;
                
                $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                	'msg' => __('Error al intentar eliminar el (queue) a la base de datos.'),
                	'data' => $data_error
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
                
                return false;
            }
            
        }else{
            
            $data_error = new \stdClass; 
            $data_error->connection = $connection;
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                'msg' => __('No se encontro el (queue).'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
        }
        
        return $connection;
    }
    
    protected function getQueue($connection){
         
        //busca el objeto en la base del template
        return $this->modelQueues->find()
            ->contain(['MikrotikIpfijaTa_Plans', 'MikrotikIpfijaTa_Pools', 'MikrotikIpfijaTa_Profiles'])
            ->where([
                'MikrotikIpfijaTa_Queues.connection_id' => $connection->id,
                'MikrotikIpfijaTa_Queues.controller_id' => $connection->controller_id
            ])->first();
    }
    
    protected function editQueue($connection){
         
        
        $queue = $this->modelQueues->find()
            ->where([
                'name' => $connection->queue_name,
                'id !=' => $connection->queue->id, 
                'controller_id' => $connection->controller->id
                ])
            ->first();
        
        if($queue){
            
            $data_error = new \stdClass; 
            $data_error->other_queue = $queue;
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
            	'msg' => __('Ya existe un (queue) con el mismo nombre.'),
            	'data' => $data_error,
            	'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
   
            return false;
        }
       
        $queue = $this->modelQueues->get($connection->queue->id);
        
        
        $data = [
            'name' => $connection->queue_name,
            'target' => ip2long($connection->ip),
            'comment' => $connection->comment,
            'connection_id' => $connection->id,
            'controller_id' => $connection->controller_id,
            'pool_id' => $connection->pool_id,
            'profile_id' => $connection->profile_id,
            'plan_id' => $connection->plan_id,
         ];

        $queue = $this->modelQueues->patchEntity($queue, $data );
        
        $queue->tprofile = $this->modelProfiles->get($queue->profile_id);
        
        if(!$this->modelQueues->save($queue)){
            
            $data_error = new \stdClass; 
            $data_error->queue = $queue;
            $data_error->errors = $queue->getErrors();
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
            	'msg' => __('Error al intentar editar el (queue) de la base de datos.'),
            	'data' => $data_error,
            	'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
    
        return $queue;
        
    }
    
    public function deleteQueueInController($controller, $tqueue_api_id){
       return $this->deletedQueueByApiIdApi($controller, $tqueue_api_id);
    }

    public function getQueuesAndConnectionsArray($controller){
        
        $this->_ctrl->loadModel('Connections');
       
        $queues =  $this->getQueues($controller);
        
        $connections =  $this->_ctrl->Connections->find()
            ->where([
                'controller_id' => $controller->id
                ])
            ->order([
                'customer_code' => 'ASC'
                ]);
        
        $queueArray = [];
        
        foreach($queues as $queue){
            foreach($connections as $connection){
                if($queue->connection_id == $connection->id){
                    $queue->connection = $connection;
                    break;
                }
            }
            
            $queue['target'] = $queue->target . '/32';
            $queue['max_limit'] = $queue->profile->down ? $queue->profile->up .'/'. $queue->profile->down : '0/0';
            $queue['limit_at'] = $queue->profile->down_at_limit ? $queue->profile->up_at_limit .'/'. $queue->profile->down_at_limit : '0/0';
            $queue['burst_limit'] = $queue->profile->down_burst ? $queue->profile->up_burst .'/'. $queue->profile->down_burst : '0/0';
            $queue['burst_threshold'] = $queue->profile->down_threshold ? $queue->profile->up_threshold .'/'. $queue->profile->down_threshold : '0/0';
            $queue['burst_time'] = $queue->profile->down_time ? $queue->profile->up_time .'s/'. $queue->profile->down_time .'s': '0s/0s';
            $queue['priority'] = $queue->profile->priority ? $queue->profile->priority .'/'. $queue->profile->priority : '8/8';
            $queue['queue'] = $queue->profile->down_queue_type ? $queue->profile->up_queue_type .'/'. $queue->profile->down_queue_type : '';
         
            $queueArray[$queue->name] = $queue;
        }
        
        return $queueArray;

    }
    
    public function getQueuesInController($controller){
        return $this->getQueuesApi($controller);
    }
    
    protected function getQueues($controller){
         
        //busca el objeto en la base del template
        return $this->modelQueues->find()->contain(['MikrotikIpfijaTa_Profiles'])
            ->where([
                'MikrotikIpfijaTa_Queues.controller_id' => $controller->id
                ])
            ->order([
                'MikrotikIpfijaTa_Queues.name' => 'ASC'
                ])
            ->map(function ($row) { 
                $row->target = long2ip($row->target);
                return $row;
            });
    
    }
    
    public function generateQueueName($connection){
          
        $internal_system_id =  Configure::read('project.internal_system_id');
        
        return 'h' . $internal_system_id . str_pad(dechex($connection->id), 4, '0', STR_PAD_LEFT); 
     }
    
    public function syncQueue($connection){
        
        if(!$this->integrationEnabled($connection->controller)){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
            'msg' => __('Intento de sincronizacion con integracion desabilitada.'),
            'data' => $data_error,
            'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
  
            return false;
        }        
 
            
        $queue =  $this->modelQueues->find()->contain(['MikrotikIpfijaTa_Profiles'])
            ->where([
                'MikrotikIpfijaTa_Queues.connection_id' => $connection->id,
                'MikrotikIpfijaTa_Queues.controller_id' => $connection->controller_id,
                ])
            ->first();
        
        $queue->comment = sprintf("%'.05d",  $connection->customer->code) . ' - ' . $connection->customer->name;
        
        $queue = $this->syncQueueApi($connection->controller, $queue);
        
        if(!$queue){
            return false;
        }
        
        //limpia los api_id que existan duplicados
        $duplicates = $this->modelQueues->find()
        	->where([
        	'api_id' => $queue->api_id,
        	'id !=' => $queue->id,
        	'controller_id' => $connection->controller_id
        	]);
        
        foreach($duplicates as $duplicated){
        
        	$duplicated->api_id = NULL;
        	$this->modelQueues->save($duplicated);
        }
      
        if(!$this->modelQueues->save($queue)){
            
            $data_error = new \stdClass; 
            $data_error->queue = $queue;
            $data_error->errors = $queue->getErrors();
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                'msg' => __('Error al intentar actualizar el (queue) en la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            return false;
        }
        
        return $queue;
        
    }
    
   
    
    
    
    //arp
    
    public function addArp($connection){
        
        $data = [
            'address' => ip2long($connection->ip),
            'mac_address' => $connection->mac,
            'interface' => $connection->interface,
            'enabled' => true,
            'comment' => $connection->comment,
            'connection_id' => $connection->id,
            'controller_id' => $connection->controller_id,
            ];
        
    
        $arp = $this->modelArps->newEntity();
        $arp = $this->modelArps->patchEntity($arp, $data);
        
        if(!$this->modelArps->save($arp)){
            
            $data_error = new \stdClass; 
            $data_error->arp = $arp;
            $data_error->errors = $arp->getErrors();
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                'msg' => __('Error al Intentar guardar el (arp) de la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
        
        return $arp;
    }
    
    public function editArp($connection){
          
        $arp = $this->modelArps->find()->where(['connection_id' => $connection->id])->first();;
        
        if(!$arp){
            
            $data_error = new \stdClass; 
            $data_error->connection = $connection;
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                'msg' => __('No se econtro el (arp) correspondiente a la conexión.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
      
        $data = [
            'address' => ip2long($connection->ip),
            'mac_address' => $connection->mac,
            'interface' => $connection->interface,
            'enabled' => true,
            'comment' => $connection->queue->comment,
            'connection_id' => $connection->id,
            'controller_id' => $connection->controller_id,
            ];
            
        $arp = $this->modelArps->patchEntity($arp, $data);
        
        if(!$this->modelArps->save($arp)){
            
            $data_error = new \stdClass; 
            $data_error->arp = $arp;
            $data_error->errors = $arp->getErrors();
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                'msg' => __('Error al Intentar editar el (arp) en la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            return false;
        }
   
       
        return $arp;
        
    }
    
    public function deleteArp($connection){
        
        $connection->arp = $this->modelArps->find()
            ->where([
                'connection_id' => $connection->id
                ])
            ->first();
            
            
        if($connection->arp){
            
            if(!$this->modelArps->delete($connection->arp)){
            
                $data_error = new \stdClass; 
                $data_error->arp = $connection->arp;
                
                $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                    'msg' => __('Error al Intentar eliminar el (arp) de la base de datos.'),
                    'data' => $data_error,
                    'flash' => false
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
        
                return false;
            }
            
        }else{
            
            $data_error = new \stdClass; 
            $data_error->connection = $connection;
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                'msg' => __('El (arp) ya estaba eliminado de la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return true;
            
        }
        
        return $connection;
        
    }
    
    public function getArp($connection){
        
        //busca el objeto en la base del template
        return $this->modelArps->find()
            ->where([
                'connection_id' => $connection->id,
                'controller_id' => $connection->controller_id
            ])->first();
    }
    
    public function deleteArpInController($controller, $arp_api_id){
       return $this->deletedArpByApiIdApi($controller, $arp_api_id);
    }
    
    public function getArpsAndConnectionsArray($controller){
        
        $this->_ctrl->loadModel('Connections');
       
        $arps =  $this->getArps($controller);
        
        $connections =  $this->_ctrl->Connections->find()
            ->where([
                'controller_id' => $controller->id
                ])
            ->order([
                'customer_code' => 'ASC'
                ]);
        
        $arpArray = [];
        
        foreach($arps as $arp){
            foreach($connections as $connection){
                if($arp->connection_id == $connection->id){
                    $arp->connection = $connection;
                    break;
                }
            }
            
            $arpArray[$arp->address] = $arp;
        }
    
        return $arpArray;

    }
    
    public function getArpsInController($controller){
     
        return $this->getArpsApi($controller);
    }
 
    
    public function getArps($controller){
        
        //busca el objeto en la base del template
        return $this->modelArps->find()
            ->where([
                'controller_id' => $controller->id
                ])
            ->order([
                'address' => 'ASC'
                ])
            ->map(function ($row) { 
                $row->address = long2ip($row->address);
                $row->mac_address = implode(':',str_split(strtoupper($row->mac_address),2));
                return $row;
            });
    
        
        return $this->modelArps->find()->where(['controller_id' => $controller->id]);
    }
    
    public function syncArp($connection){
        
        if(!$this->integrationEnabled($connection->controller)){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
            'msg' => __('Intento de sincronizacion con integracion desactivada.'),
            'data' => $data_error,
            'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
        $arp =  $this->modelArps->find()
            ->where([
                'connection_id' => $connection->id,
                'controller_id' => $connection->controller_id
                ])
            ->first();
         
        $arp = $this->syncArpApi($connection->controller, $arp);
        
        if(!$arp){
            return false;
        }
        
        //limpia los api_id que existan duplicados
        $duplicates = $this->modelArps->find()
        	->where([
        	'api_id' => $arp->api_id,
        	'id !=' => $arp->id,
        	'controller_id' => $arp->controller_id,
        	]);
        
        foreach($duplicates as $duplicated){
        
        	$duplicated->api_id = NULL;
        	$this->modelArps->save($duplicated);
        }
    
        if(!$this->modelArps->save($arp)){
            
            $data_error = new \stdClass; 
            $data_error->arp = $arp;
            $data_error->errors = $arp->getErrors();
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
            'msg' => __('Error al Intentar actualizar el (arp) de la base de datos.'),
            'data' => $data_error,
            'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
          
            return false;
        }
        
        return $arp;
        
    }
    
    
    
    //ip firewall address-list CLiente
    
    public function addAddressListClient($connection){
        
        $tcontroller = $this->modelControllers->get($connection->controller_id);
     
        $data = [
            'list' => $tcontroller->address_list_name,
            'address' => ip2long($connection->ip),
            'comment' => $connection->comment,
            'connection_id' => $connection->id,
            'controller_id' => $connection->controller_id
            ];
        
    
        $addressList = $this->modelAddressLists->newEntity();
        $addressList = $this->modelAddressLists->patchEntity($addressList, $data);
        
        if(!$this->modelAddressLists->save($addressList)){
            
            $data_error = new \stdClass; 
            $data_error->tAddressList = $addressList;
            $data_error->errors = $addressList->getErrors();
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                'msg' => __('Error al Intentar guardar el (address list de cliente) de la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
        
        return $addressList;
    }
    
    public function editAddressListClient($connection){
          
        $addressList = $this->modelAddressLists->find()
            ->where([
                'connection_id' => $connection->id,
                'controller_id' => $connection->controller_id
                ])
            ->first();;
        
        if(!$addressList){
            
            $data_error = new \stdClass; 
            $data_error->connection = $connection;
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                'msg' => __('No se encontro el (address list) correspondiente a la conexión.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
             
        $tcontroller = $this->modelControllers->get($connection->controller_id);
             
        $data = [
            'list' => $tcontroller->address_list_name,
            'address' => ip2long($connection->ip),
            'comment' => $connection->comment,
            'connection_id' => $connection->id,
            'controller_id' => $connection->controller_id
            ];
        
        $addressList = $this->modelAddressLists->patchEntity($addressList, $data);
        
        if(!$this->modelAddressLists->save($addressList)){
            
            $data_error = new \stdClass; 
            $data_error->addressList = $addressList;
            $data_error->errors = $addressList->getErrors();
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                'msg' => __('Error al Intentar editar el (address list cliente) en la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            return false;
        }
   
       
        return $addressList;
        
    }
    
    public function deleteAddressListClient($connection){
        
        $connection->addressList = $this->modelAddressLists->find()
            ->where([
                'connection_id' => $connection->id
                ])
            ->first();
        
        if($connection->addressList){
            
            if(!$this->modelAddressLists->delete($connection->addressList)){
            
                $data_error = new \stdClass; 
                $data_error->addressList = $connection->addressList;
                
                $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                    'msg' => __('Error al Intentar eliminar el (address list cliente) de la base de datos.'),
                    'data' => $data_error,
                    'flash' => false
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
        
                return false;
            }
            
        }else{
            
            $data_error = new \stdClass; 
            $data_error->connection = $connection;
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                'msg' => __('El (address list cliente) ya estaba eliminado de la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return true;
        }
        
        return $connection;
        
    }
    
    public function getAddressListClient($connection){
        
        //busca el objeto en la base del template
        return $this->modelAddressLists->find()
            ->where([
                'connection_id' => $connection->id,
                'controller_id' => $connection->controller_id
            ])->first();
    }
    
    public function deleteAddressListClientInController($controller, $address_list_cliente_api_id){
       return $this->deleteAddressListByApiIdApi($controller, $address_list_cliente_api_id);
    }
    
    public function getAddressListsClientAndConnectionsArray($controller){
          
        $this->_ctrl->loadModel('Connections');
       
        $addressListsClient =  $this->getAddressListsClient($controller);
        
        $connections =  $this->_ctrl->Connections->find()
            ->where([
                'controller_id' => $controller->id
                ])
            ->order([
                'customer_code' => 'ASC'
                ]);
        
        $addressListsClientArray = [];
        
        foreach($addressListsClient as $alc){
            foreach($connections as $connection){
                if($alc->connection_id == $connection->id){
                    $alc->connection = $connection;
                    break;
                }
            }
            
            $addressListsClientArray[$alc->list.':'.$alc->address] = $alc;
        }
        
        return $addressListsClientArray;
     }
    
    public function getAddressListsClientInController($controller){
        
        $controller->tcontroller = $this->getControllerTemplate($controller);
        return $this->getAddressListsApi($controller, [$controller->tcontroller->address_list_name]);
    }
    
    public function getAddressListsClient($controller){
        
        //busca el objeto en la base del template
        return $this->modelAddressLists->find()
            ->where([
                'controller_id' => $controller->id
                ])
            ->order([
                'address' => 'ASC'
                ])
            ->map(function ($row) { 
                $row->address = long2ip($row->address);
                return $row;
            });
    }
    
    public function syncAddressListClient($connection){
        
        if(!$this->integrationEnabled($connection->controller)){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
            'msg' => __('Intento de sincronizacion con integracion desactivada.'),
            'data' => $data_error,
            'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
        $addressList =  $this->modelAddressLists->find()
            ->where([
                'connection_id' => $connection->id,
                'controller_id' => $connection->controller_id
                ])
            ->first();
            
      
        $this->_ctrl->loadModel('Controllers');
        $controller = $this->_ctrl->Controllers->get($addressList->controller_id);
        
        $addressList->address = long2ip($addressList->address);
        
        $addressList = $this->syncAddressListApi($controller, $addressList);
        
        if(!$addressList){
            return false;
        }
        
        $addressList->address = ip2long($addressList->address);
        
        //limpia los api_id que existan duplicados
        $duplicates = $this->modelAddressLists->find()
        	->where([
        	'api_id' => $addressList->api_id,
        	'id !=' => $addressList->id,
        	'controller_id' => $addressList->controller_id,
        	]);
        
        foreach($duplicates as $duplicated){
        
        	$duplicated->api_id = NULL;
        	$this->modelAddressLists->save($duplicated);
        }
    
        if(!$this->modelAddressLists->save($addressList)){
            
            $data_error = new \stdClass; 
            $data_error->addressList = $addressList;
            $data_error->errors = $addressList->getErrors();
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
            'msg' => __('Error al Intentar actualizar el (address list cliente) de la base de datos.'),
            'data' => $data_error,
            'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
          
            return false;
        }
        
        return $addressList;
        
    }
    
    
    
    
    
    
    //address list
    
    protected function editAddressList($connection){
            
        $this->_ctrl->loadModel('FirewallAddressLists');
        
        $firewallAddressList = $this->_ctrl->FirewallAddressLists->find()
            ->where([
                'connection_id' => $connection->id,
                'controller_id' => $connection->controller_id
                ])->first(); 
                
        if($firewallAddressList){
                
            $firewallAddressList->before_address = $connection->before_ip;
            $firewallAddressList->address = $connection->ip;
            $firewallAddressList->comment = sprintf("%'.05d", $connection->customer->code)  . ' - ' . $connection->customer->name;
            
            if(!$this->_ctrl->FirewallAddressLists->save($firewallAddressList)){
                
                $data_error = new \stdClass; 
                $data_error->firewallAddressList = $firewallAddressList;
                $data_error->errors = $firewallAddressList->getErrors();
                
                $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                	'msg' => __('Error al Intentar editar el (address-list) en la base de datos.'),
                	'data' => $data_error,
                	'flash' => true
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);

                return false;
            }
            
        }
       
        return true;
        
    }
    
    public function syncAddressList($addressListId){            
    
        $this->_ctrl->loadModel('FirewallAddressLists');
            
        $addressList =  $this->_ctrl->FirewallAddressLists->find()
        ->where([
            'id' => $addressListId
            ])
        ->first();
      
        $this->_ctrl->loadModel('Controllers');
        $controller = $this->_ctrl->Controllers->get($addressList->controller_id);

        if(!$this->integrationEnabled($controller)){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
            'msg' => __('Intento de sincronizacion con integracion desabilitada.'),
            'data' => $data_error,
            'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
  
            return false;
        }
        
        $addressList = $this->syncAddressListApi($controller, $addressList);
            
        if(!$addressList){
            return false;
        }
        
        //limpia los api_id que existan duplicados
        $duplicates = $this->_ctrl->FirewallAddressLists->find()
        	->where([
        	'api_id' => $addressList->api_id,
        	'id !=' => $addressList->id,
        	'controller_id' => $addressList->controller_id
        	]);
        
        foreach($duplicates as $duplicated){
        
        	$duplicated->api_id = NULL;
        	$this->_ctrl->FirewallAddressLists->save($duplicated);
        }
      
        if(!$this->_ctrl->FirewallAddressLists->save($addressList)){
            
            $data_error = new \stdClass; 
            $data_error->addressList = $addressList;
            $data_error->errors = $addressList->getErrors();
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                'msg' => __('Error al intentar actualizar el (address-list) en la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
        return $addressList;
        
    }
    
    protected function syncAddressListsCorteByConnection($connection){
        
        if(!$this->integrationEnabled($connection->controller)){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
            'msg' => __('integracion deshabilitada.'),
            'data' => $data_error,
            'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
  
            return false;
        }
        
        $this->_ctrl->loadModel('FirewallAddressLists');
      
        $addressListCorte = $this->_ctrl->FirewallAddressLists->find()
            ->where([
                'connection_id' => $connection->id,
                'list' => 'Cortes', 
                'controller_id' => $connection->controller_id
                ])
            ->first();
        
        if(!$addressListCorte){ //no hay nada para sync
            return true;
        }
        
        $addressListCorte->comment = sprintf("%'.05d", $connection->customer->code)  . ' - ' . $connection->customer->name;
   
        $addressListCorte = $this->syncAddressListApi($connection->controller, $addressListCorte);
        
        if(!$addressListCorte){ // no salio bien la sync en el router
            return false;
        }
        
        
        //
        
        //limpia los api_id que existan duplicados
        
        $duplicates = $this->_ctrl->FirewallAddressLists->find()
        	->where([
        	'api_id' => $addressListCorte->api_id,
        	'id !=' => $addressListCorte->id,
        	'controller_id' => $connection->controller_id
        	]);
        
        foreach($duplicates as $duplicated){
        
        	$duplicated->api_id = NULL;
        	$this->_ctrl->FirewallAddressLists->save($duplicated);
        }
        
        
        if(!$this->_ctrl->FirewallAddressLists->save($addressListCorte)){
            
            $data_error = new \stdClass; 
            $data_error->addressListCorte = $addressListCorte;
            $data_error->errors = $addressListCorte->getErrors();
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                'msg' => __('Error al Intentar actualizar el (address-list) de Corte en la base de datos .'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
        return $addressListCorte;
    }
    
    protected function syncAddressListsAvisoByConnection($connection){
        
        if(!$this->integrationEnabled($connection->controller)){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
            'msg' => __('integracion deshabilitada.'),
            'data' => $data_error,
            'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
  
            return false;
        }
 
        $this->_ctrl->loadModel('FirewallAddressLists');
        
        $addressListAviso = $this->_ctrl->FirewallAddressLists->find()
            ->where([
                'connection_id' => $connection->id,
                'list' => 'Avisos', 
                'controller_id' => $connection->controller_id
                ])
            ->first();
        
        if(!$addressListAviso){ // no hay nada para sync
             return true;
        }
            
        $addressListAviso->comment = sprintf("%'.05d", $connection->customer->code)  . ' - ' . $connection->customer->name;
       
        $addressListAviso = $this->syncAddressListApi($connection->controller, $addressListAviso);
        
        if(!$addressListAviso){ // no salio bien la sync en el router
            return false;
        }
        
        //limpia los api_id que existan duplicados
        $duplicates = $this->_ctrl->FirewallAddressLists->find()
        	->where([
        	'api_id' => $addressListAviso->api_id,
        	'id !=' => $addressListAviso->id,
        	'controller_id' => $connection->controller_id
        	]);
        
        foreach($duplicates as $duplicated){
        
        	$duplicated->api_id = NULL;
        	$this->_ctrl->FirewallAddressLists->save($duplicated);
        }
        
        if(!$this->_ctrl->FirewallAddressLists->save($addressListAviso)){
            
            $data_error = new \stdClass; 
            $data_error->addressListAviso = $addressListAviso;
            $data_error->errors = $addressListAviso->getErrors();
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                'msg' => __('Error al Intentar actualizar el (address-list) de Aviso en la base de datos .'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
            
        
        return $addressListAviso;
    }
    
    public function getAddressListsArray($controller_id){
                            
         $this->_ctrl->loadModel('FirewallAddressLists');
        
        //busca el objeto en la base del template
        $addressLists =  $this->_ctrl->FirewallAddressLists->find()->where(['controller_id' => $controller_id]);
        
        $addressListsArray = [];
        foreach ($addressLists as $addressList) {
            
            $addressListsArray[$addressList->list.':'.$addressList->address] = $addressList;
        }
        
        return $addressListsArray;
    
    }
    
    public function deleteAddressListInController($controller, $address_list_api_id){
       return $this->deleteAddressListByApiIdApi($controller, $address_list_api_id);
    }
    
    public function getAddressListsInController($controller){
        return $this->getAddressListsApi($controller, ['Cortes', 'Avisos']);
    }


    
    
    
    //gateway
    
    public function getGateways($controller_id){
          
         return $this->modelGateways->find()
            ->where([
                'controller_id' => $controller_id,
                ]);
          
     }
     
    public function addGateway($data){
          
        $tpool = $this->modelPools->get($data['pool_id']);
        
        $tgateway = $this->modelGateways->find()
             ->where([
                  'address' => $tpool->addresses,
                  'controller_id' => $tpool->controller_id
             ])
             ->first();
        
        if($tgateway){ //se repite el addresss
            
            $data_error = new \stdClass; 
            $data_error->other_gateway = $tgateway;
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('Ya existe un (gateway) con el mismo addresss.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
     
            return false;
        }
        
        $tgateway = $this->modelGateways->newEntity();
        
        $length_net = explode('/', $tpool->addresses)[1];
        $data['address'] = $tpool->gateway . '/' . $length_net;
        $data['enabled'] = true;
        
        
        $tgateway = $this->modelGateways->patchEntity($tgateway, $data);
     
        if(!$this->modelGateways->save($tgateway)){ 
            
            $data_error = new \stdClass; 
            $data_error->tgateway = $tgateway;
            $data_error->errors = $tgateway->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al intentar agregar el (gateway) a la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
     
            return false;
        }
        
   
        return $tgateway;          
     }
    
    public function editGateway($controller, $data){
        
        $tpool = $this->modelPools->get($data['pool_id']);
    
        $gateway = $this->modelGateways->find()->where(['id' => $data['id']])->first();
        
        $length_net = explode('/', $tpool->addresses)[1];
        $data['address'] = $tpool->gateway . '/' . $length_net;
        
        $gateway = $this->modelGateways->patchEntity($gateway, $data );
        
        if(!$this->modelGateways->save($gateway)){
            
            $data_error = new \stdClass; 
            $data_error->gateway = $gateway;
            $data_error->errors = $gateway->getErrors();
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                'msg' => __('Error al intentar editar el (gateway) en la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
    
        return $gateway;
        
    }
    
    public function deleteGateway($id){
          
        $tgateway = $this->modelGateways->get($id);
      
        if(!$this->modelGateways->delete($tgateway)){
            
            $data_error = new \stdClass;
            $data_error->tgateway = $tgateway;
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al intentar eliminar el (gateway)'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
     
    
        return $tgateway;
     }
     
    public function deleteGatewayInController($controller, $gateway_api_id){
      return $this->deleteGatewayByApiIdApi($controller, $gateway_api_id);
    }
    
    public function syncGateway($gateway_id){
 
        $gateway =  $this->modelGateways->get($gateway_id);

        $this->_ctrl->loadModel('Controllers');
        $controller = $this->_ctrl->Controllers->get($gateway->controller_id);
            
        if($this->integrationEnabled($controller)){           
            
            $gateway = $this->syncGatewayApi($controller, $gateway);
            
            if(!$gateway){
                return false;
            }
            
            //limpia los api_id que existan duplicados
            $duplicates = $this->modelGateways->find()
            	->where([
            	'api_id' => $gateway->api_id,
            	'id !=' => $gateway->id,
            	'controller_id' => $gateway->controller_id,
            	]);
            
            foreach($duplicates as $duplicated){
            
            	$duplicated->api_id = NULL;
            	$this->modelGateways->save($duplicated);
            }
                
        }
        
        if(!$this->modelGateways->save($gateway)){
            
            $data_error = new \stdClass; 
            $data_error->gateway = $gateway;
            $data_error->errors = $gateway->getErrors();
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                'msg' => __('Error al Intentar actualizar el (gateway) en la base de datos .'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            return false;
        }
        
        return true;
    }
    
    public function getGatewaysArray($controller_id){
        
        //busca el objeto en la base del template
        $gateways =  $this->modelGateways->find()->where(['controller_id' => $controller_id]);
        
        $gatewaysArray = [];
        foreach ($gateways as $gateway) {
            
            $gatewaysArray[$gateway->address] = $gateway;
        }
        
        return $gatewaysArray;
    
    }
    
    public function getGatewaysInController($controller){
        return $this->getGatewaysApi($controller);
    }
    
    
    
    
    //connections
   
    public function addConnection($controller, $data) {
        
        $tcontroller = $this->modelControllers->get($controller->id);
        
        if($tcontroller->arp && $data['mac'] == ''){
            
            $data_error = new \stdClass; 
            $data_error->data = $data;
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                'msg' => __('Creación de ARP habilitado. MAC vacio.'),
                'data' => $data_error,
                'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);

            return false;
        }
        
        
        $this->_ctrl->loadModel('Connections');
        
        $tplan = $this->getPlan($controller, $data['plan_id']);
        
        $data['user_id'] = $this->_ctrl->Auth->user()['id'];
        $data['controller_id'] = $controller->id;
        $data['service_id'] = $tplan->service_id;
        $data['profile_id'] = $tplan->profile->id;
        $data['mac'] = strtoupper($data['mac']);
        $data['comment'] = sprintf("%'.05d",  $data['customer_code']) . ' - ' . $data['customer_name'];
     
        if(!$data['validate_ip']){
            
            $data['pool_id'] = $this->getPoolIdByIP($controller, $data['ip']);
        }    
     
        $connection = $this->_ctrl->Connections->newEntity();
         
        $connection = $this->_ctrl->Connections->patchEntity($connection, $data);
         
          //para provocar error de base de datos
          //$connection->address = null;
     
        if(!$this->_ctrl->Connections->save($connection)){
            
            $data_error = new \stdClass; 
            $data_error->connection = $connection;
            $data_error->errors = $connection->getErrors();
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                'msg' => __('Error al intentar agregar la conexion a la base de datos.'),
                'data' => $data_error,
                'flash' => true
               ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
        
            return false;
        }
        
        $connection->tcontroller = $tcontroller;
        
        //name de queue
        if(!array_key_exists('queue_name', $data) || !$data['queue_name']){
           $connection->queue_name = $this->generateQueueName($connection);
        }else{
           $connection->queue_name = $data['queue_name'];
        } 
        
        $this->_ctrl->Connections->save($connection);
        
        $queue = $this->addQueue($connection);
        
        if($queue){
            
            if($this->addAddressListClient($connection)){
        
                if($connection->tcontroller->arp){
                    
                    if(!$this->addArp($connection)){
                        
                        $this->deleteAddressListClient($connection);
                        $this->deleteQueue($connection);
                        $this->_ctrl->Connections->delete($connection);
                        
                        return false;
                    }
                }
                
            }else{
                
                $this->_ctrl->Connections->delete($connection);
                
                $this->deleteQueue($connection);
                
                return false;
            }
        }
        else{
            
            $this->_ctrl->Connections->delete($connection);
            
            return false;
        }
    
        return $this->sync($connection);
    } 
    
    public function getConnection($connection){
        
        $connection->queue = $this->getQueue($connection);
        $connection->addressList = $this->getAddressListClient($connection);
        $connection->arp = $this->getArp($connection);
        return $connection;
    }
     
    public function deleteConnection($connection){
        
        $connection->diff = false;
        
        if(!$connection->enabled){
            $connection = $this->enableConnection($connection);
            if(!$connection){
                return false;
            }
        }
        
        $connection = $this->deleteQueue($connection);
        
        if(!$connection){
            return false;
        }
        
        $connection = $this->deleteAddressListClient($connection);
        
        if(!$connection){
            return false;
        }
        
        $connection->tcontroller = $this->getControllerTemplate($connection->controller);
        
        if($connection->tcontroller->arp){
            
            $connection = $this->deleteArp($connection);
            
            if(!$connection){
                return false;
            }
        }
        
        $connection = $this->removeAvisoConnection($connection);
        
        if(!$connection){
            return false;
        }
        
        return $this->syncDeleteConnection($connection);
    }

    public function editConnection($connection, $data)
    {
        $connection->tcontroller = $this->getControllerTemplate($connection->controller);

        if ($connection->tcontroller->arp && $data['mac'] == '') {

            $data_error = new \stdClass;
            $data_error->connection = $connection;
            $data_error->errors = $connection->errors();

            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                'msg' => __('Creación de ARP habilitado. MAC vacio.'),
                'data' => $data_error,
                'flash' => true
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);

            return false;
        }

        $this->_ctrl->loadModel('Connections');        

        $tplan = $this->getPlan($connection->controller, $data['plan_id']);

        $data['service_id'] = $tplan->service_id;
        $data['profile_id'] = $tplan->profile->id;
        $data['mac'] = strtoupper($data['mac']);
        $data['comment'] = sprintf("%'.05d",  $connection->customer->code) . ' - ' . $connection->customer->name;

        if (!$data['validate_ip']) {

            $data['pool_id'] = $this->getPoolIdByIP($connection->controller, $data['ip']);
        }

        // $data['name'] = $data['queue_name'];

        $connection = $this->_ctrl->Connections->patchEntity($connection, $data);

        //eedit queue
        $connection->queue = $this->editQueue($connection);
        if (!$connection->queue) {
            return false;
        }

        //edit address list client
        $connection->addressList = $this->editAddressListClient($connection);
        if (!$connection->addressList) {
            return false;
        }

        if ($connection->tcontroller->arp) {

            $connection->arp = $this->editArp($connection);

            if (!$connection->arp){

                if (!$this->addArp($connection)) {
                    return false;
                }
            }
        }

        //edito address list
        if (!$this->editAddressList($connection)) {
            return false;
        }

        //guardo la conexion
        if (!$this->_ctrl->Connections->save($connection)) {

            $data_error = new \stdClass; 
            $data_error->connection = $connection;
            $data_error->errors = $connection->errors();

            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                'msg' => __('Error al intentar editar la conexión en la base de datos.'),
                'data' => $data_error,
                'flash' => true
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);

            return false;
        }

        return $this->sync($connection);
    }

    public function getPoolIdByIP($controller, $ip){
        
        $pools = $this->modelPools->find()
            ->where([
                'controller_id' => $controller->id
                ]);
                
        $iplong = ip2long($ip);   
                
        foreach($pools as $pool){
            if(ip2long($pool->min_host) <= $iplong){
                if(ip2long($pool->max_host) >= $iplong){
                    return $pool->id;
                }
                    
            }
        }
        
        return null;
    }
    
    public function rollbackConnection($connection_id){
        
        $this->_ctrl->loadModel('ConnectionsTransactions');
        $connectionTransaction = $this->_ctrl->ConnectionsTransactions->find()
            ->where([
                'connection_id' => $connection_id
                ])->first();
                
        $connectionRestore = unserialize($connectionTransaction->data);
  
        $connectionArray = json_decode(json_encode($connectionRestore), true);   
        
        $this->_ctrl->loadModel('Connections');
        $connection = $this->_ctrl->Connections->find()->where(['id' => $connection_id])->first();
        
        $tqueue_data = $connectionArray['queue'];
        $taddressList_data = $connectionArray['addressList'];
        $tarp_data = $connectionArray['arp'];
            
        $connectionArray['service'] = null;
        $connectionArray['controller'] = null;
        $connectionArray['customer'] = null;
        $connectionArray['queue'] = null;
        $connectionArray['addressList'] = null;
        $connectionArray['arp'] = null;

        if(!$connection){            
               $connection = $this->_ctrl->Connections->newEntity();
          }

        $connection = $this->_ctrl->Connections->patchEntity($connection, $connectionArray);

        if(!$this->_ctrl->Connections->save($connection)){
               
               $data_error = new \stdClass; 
               $data_error->errors = $connection->getErrors();

               $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                    'msg' => __('Error al intentar agregar la conexion a la base de datos.'),
                    'data' => $data_error,
                    'flash' => true
               ]);

               $this->_ctrl->getEventManager()->dispatch($event);

               return false;
        }
        
         
        $tqueue_data['profile'] = null;
        $tqueue_data['pool'] = null;
        $tqueue_data['plan'] = null;

        $queue = $this->modelQueues->find()->where(['id' =>$tqueue_data['id']])->first();
         
        if(!$queue){
               $queue = $this->modelQueues->newEntity();             
          }
         
        $queue = $this->modelQueues->patchEntity($queue, $tqueue_data);

        if(!$this->modelQueues->save($queue)){

                $data_error = new \stdClass; 
                $data_error->errors = $queue->getErrors();

                $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                    'msg' => __('Error al intentar agregar la Queue a la base de datos.'),
                    'data' => $data_error,
                    'flash' => true
                ]);

                $this->_ctrl->getEventManager()->dispatch($event);

                return false;
        }
    
        $addressList = $this->modelAddressLists->find()->where(['id' => $taddressList_data['id']])->first();
        
        if(!$addressList){
            $addressList = $this->modelAddressLists->newEntity();             
        }
         
        $addressList = $this->modelAddressLists->patchEntity($addressList, $taddressList_data);

        if(!$this->modelAddressLists->save($addressList)){

            $data_error = new \stdClass; 
            $data_error->errors = $addressList->getErrors();

            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
               'msg' => __('Error al intentar agregar la (address list client) a la base de datos.'),
               'data' => $data_error,
               'flash' => true
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);

            return false;
        }
        
        $connection->tcontroller = $this->modelControllers->get($connection->controller_id);
        
        if($connection->tcontroller->arp){
            
            $arp = $this->modelArps->find()->where(['id' => $tarp_data['id']])->first();
            
            if(!$arp){
                $arp = $this->modelArps->newEntity();             
            }
             
            $arp = $this->modelArps->patchEntity($arp, $tarp_data);
    
            if(!$this->modelArps->save($arp)){
    
                $data_error = new \stdClass; 
                $data_error->errors = $arp->getErrors();
    
                $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                   'msg' => __('Error al intentar agregar la (arp) a la base de datos.'),
                   'data' => $data_error,
                   'flash' => true
                ]);
    
                $this->_ctrl->getEventManager()->dispatch($event);
    
                return false;
            }
        }

        
        $this->sync($connectionRestore);
        
        $this->_ctrl->ConnectionsTransactions->delete($connectionTransaction);
        
        return true;

    }
    
    protected function sync($connection){
        
        $connection->controller =  $this->_ctrl->Connections->Controllers->get($connection->controller_id);

        if(!$this->integrationEnabled($connection->controller)){
            return $connection;
        }

        $connection->customer   =  $this->_ctrl->Connections->Customers->get($connection->customer_code);
        
        return $this->syncConnection($connection);
    
    }
    
    public function syncConnection($connection){
            
        $connection->diff =  false;
    
        if(!$this->syncQueue($connection)) {
            $connection->diff =  true;
        }
        
        if(!$this->syncAddressListClient($connection)) {
            $connection->diff =  true;
        }
        
        if(!$connection->tcontroller){
            $connection->tcontroller = $this->modelControllers->get($connection->controller_id);
        }
        
        if($connection->tcontroller->arp){
            
            if(!$this->syncArp($connection)) {
                $connection->diff =  true;
            }
        }
        
        if(!$this->syncAddressListsCorteByConnection($connection)){
            $connection->diff =  true;
        }
        
        if(!$this->syncAddressListsAvisoByConnection($connection)){
            $connection->diff =  true;
        }
   
        $this->_ctrl->loadModel('Connections');
        $this->_ctrl->Connections->save($connection);
        
        return $connection;
        
    }
     
    protected function syncDeleteConnection($connection) {      
      
        
        $this->deletedQueueApi($connection->controller, $connection->queue);
        $this->deleteAddressListApi($connection->controller, $connection->addressList);
        
        if($connection->tcontroller->arp){
            $this->deleteArpApi($connection->controller, $connection->arp);
        }
        
        if($connection->addressListCorte){
            $this->deleteAddressListApi($connection->controller, $connection->addressListCorte);
        }
        
        if($connection->addressListAviso){
            $this->deleteAddressListApi($connection->controller, $connection->addressListAviso);
        }
        
        return $connection;
    }    
    
    public function addAvisoConnection($connection){
         
        if(!$this->getStatus($connection->controller)){
             return false;
        } 
 
        $this->_ctrl->loadModel('FirewallAddressLists');
        
        $fal = $this->_ctrl->FirewallAddressLists->find()
            ->where([
                'address' => $connection->ip,
                'list' => 'Avisos',
                'controller_id' => $connection->controller_id
                ])
            ->first();
        
        if($fal){
            return true;
        }      
        
        $firewallAddressList = $this->_ctrl->FirewallAddressLists->newEntity();
                 
        $data = [
             'list' => 'Avisos',
             'address' => $connection->ip,
             'before_address' => $connection->ip,
             'comment' => sprintf("%'.05d", $connection->customer->code)  . ' - ' . $connection->customer->name,
             'connection_id' => $connection->id,
             'controller_id' => $connection->controller->id
             ];
             
        $firewallAddressList = $this->_ctrl->FirewallAddressLists->patchEntity($firewallAddressList, $data);
        
        if(!$this->_ctrl->FirewallAddressLists->save($firewallAddressList)){
            
            $data_error = new \stdClass; 
            $data_error->firewallAddressList = $firewallAddressList;
            $data_error->errors = $firewallAddressList->getErrors();
            
            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                'msg' => __('Error al Intentar agregar el (address-list) del Aviso en la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
            
        }
  
        $connection->tcontroller = $this->modelControllers->get($connection->controller_id);
        
        return $this->sync($connection);
    }
    
    public function removeAvisoConnection($connection, $sync = false){

        $this->_ctrl->loadModel('FirewallAddressLists');
        
        $connection->addressListAviso = $this->_ctrl->FirewallAddressLists->find()
            ->where([
                'connection_id' => $connection->id,
                'list' => 'Avisos',
                'controller_id' => $connection->controller_id
                ])
            ->first();
        
        if($connection->addressListAviso){
            
            if($this->integrationEnabled($connection->controller) && $sync){  
                
                if(!$this->deleteAddressListApi($connection->controller, $connection->addressListAviso)){
                    $connection->diff = true;
                    $this->_ctrl->loadModel('Connections');
                    $this->_ctrl->Connections->save($connection);
                }
            }
            
            if(!$this->_ctrl->FirewallAddressLists->delete($connection->addressListAviso)){
                
                $data_error = new \stdClass; 
                $data_error->addressListAviso = $connection->addressListAviso;
                
                $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                'msg' => __('Error al Intentar eliminar el (address-list) del Aviso de la base de datos.'),
                'data' => $data_error,
                'flash' => false
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
                
                return false;
            }
            
        }
        
   
        
        return $connection;
    } 

    public function disableConnection($connection)
    {
        $this->_ctrl->loadModel('FirewallAddressLists');
        $firewallAddressList = $this->_ctrl->FirewallAddressLists->newEntity();

        $data = [
             'list' => 'Cortes',
             'address' => $connection->ip,
             'before_address' => $connection->ip,
             'comment' => sprintf("%'.05d", $connection->customer->code)  . ' - ' . $connection->customer->name,
             'connection_id' => $connection->id,
             'controller_id' => $connection->controller->id
         ];

        $firewallAddressList = $this->_ctrl->FirewallAddressLists->patchEntity($firewallAddressList, $data);

        if (!$this->_ctrl->FirewallAddressLists->save($firewallAddressList)) {

            $data_error = new \stdClass; 
            $data_error->firewallAddressList = $firewallAddressList;
            $data_error->errors = $firewallAddressList->getErrors();

            $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
            	'msg' => __('Error al Intentar agregar el (address-list) de Corte en la base de datos.'),
            	'data' => $data_error,
            	'flash' => true
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);

            return false;
        }

        $connection->tcontroller = $this->modelControllers->get($connection->controller_id);

        return $this->sync($connection);
    }

    public function enableConnection($connection, $sync = false){
    
        $this->_ctrl->loadModel('FirewallAddressLists');
        
        $connection->addressListCorte = $this->_ctrl->FirewallAddressLists->find()
            ->where([
                'connection_id' => $connection->id,
                'list' => 'Cortes', 
                'controller_id' => $connection->controller_id
                ])
            ->first();
        
        if($connection->addressListCorte){
            
            if($this->integrationEnabled($connection->controller) && $sync){
                                
                if(!$this->deleteAddressListApi($connection->controller, $connection->addressListCorte)){
                    $connection->diff = true;
                    $this->_ctrl->loadModel('Connections');
                    $this->_ctrl->Connections->save($connection);
                }
            }
            
            if(!$this->_ctrl->FirewallAddressLists->delete($connection->addressListCorte)){
                
                $data_error = new \stdClass; 
                $data_error->addressList = $connection->addressListCorte;
                $data_error->errors = $connection->addressListCorte->getErrors();
                
                $event = new Event('MikrotikIpfijaTaComponent.Error', $this, [
                	'msg' => __('Error al Intentar eliminar el (address-list) de Corte de la base de datos.'),
                	'data' => $data_error,
                	'flash' => true
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
             
                return false;
            }
        
        }else{
            
            $data_error = new \stdClass; 
            $data_error->connection = $connection;
            
            $event = new Event('MikrotikIpfijaTaComponent.Warning', $this, [
                'msg' => __('No se encuentra el (address-list).'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
        }
   
     
        
        return $connection;
    }
    
    public function applyConfigAccessNewCustomer($controller, $ip_server){  
        
        $controller->tcontroller = $this->getControllerTemplate($controller);
        $pools = $this->getPools($controller->id);
        
        if($pools->count() == 0){
            
            $data_error = new \stdClass; 
            $data_error->conntroller = $controller;
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('No existen pools creados para aplicar esta configuración.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
        return $this->applyConfigAccessNewCustomerIpFijaApi($controller, $ip_server, $pools);
    }
     
     
    //temporal ...

    public function addWebProxyAccessApiTemp($controller, $addressList){
        return $this->addWebProxyAccessApi($controller, $addressList);
    }
    

}






