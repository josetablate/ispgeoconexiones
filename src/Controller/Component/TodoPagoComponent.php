<?php

namespace App\Controller\Component;

use App\Controller\Component\MyComponent;
use Cake\Controller\ComponentRegistry;
use Cake\Filesystem\File;

/**
 * WebServices component
 */
class TodoPagoComponent extends MyComponent
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    private $_connector = null;

    private $_paraments;

    private $_tp_codes;

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->_paraments = $this->_ctrl->request->getSession()->read('paraments');
        $this->_tp_codes = $this->_ctrl->request->getSession()->read('tp_codes');

    }

    public function instance()
    {
        $mode = $this->_paraments->payment_getway->todopago->MODO; //identificador de entorno obligatorio, la otra opción es "prod"
        $http_header = array(
            'Authorization' => $this->_paraments->payment_getway->todopago->$mode->api_keys
        );//authorization key del ambiente requerido

        $this->_connector = new \TodoPago\Sdk($http_header, $mode);
    }

    public function discoverPaymentMethods()
    {
        if (empty($this->_connector)) {
            $this->instance();
        }

        return $this->_connector->discoverPaymentMethods();
    }

    public function sendAuthorizeRequest($operation)
    {
        if (empty($this->_connector)) {
            $this->instance();
        }
        $mode = $this->_paraments->payment_getway->todopago->MODO;

        $paymentsMethods = $this->discoverPaymentMethods();

        $limit = sizeof($paymentsMethods['PaymentMethod']) - 1;
        $AVAILABLEPAYMENTMETHODSIDS = "";

        foreach ($paymentsMethods['PaymentMethod'] as $key => $payment_method) {
            $AVAILABLEPAYMENTMETHODSIDS .= $payment_method['ID'];
            if ($key != $limit) {
                $AVAILABLEPAYMENTMETHODSIDS .= '#';
            }
        }

        $optionsSAR_comercio = array (
        	'Security'       => str_replace("TODOPAGO ", "", $this->_paraments->payment_getway->todopago->$mode->api_keys),
        	'EncodingMethod' => 'XML',
        	'Merchant'       => $this->_paraments->payment_getway->todopago->$mode->merchant_id,
        	'URL_OK'         => $this->_paraments->portal->uri . $this->_paraments->payment_getway->todopago->URL_OK . $operation->OPERATIONID,
        	'URL_ERROR'      => $this->_paraments->portal->uri . $this->_paraments->payment_getway->todopago->URL_ERROR . $operation->OPERATIONID,
        );

        $optionsSAR_operacion = array (
        	'MERCHANT'                   => $this->_paraments->payment_getway->todopago->$mode->merchant_id, //dato fijo (número identificador del comercio)
        	'OPERATIONID'                => $operation->OPERATIONID, // ej.:27398173292187 - número único que identifica la operación, generado por el comercio.
        	'CURRENCYCODE'               => $this->_paraments->payment_getway->todopago->CURRENCYCODE, //por el momento es el único tipo de moneda aceptada
        	'AMOUNT'                     => $operation->AMOUNT, //'54.00'
        	'EMAILCLIENTE'               => $operation->EMAILCLIENTE, //'ispbrain@gmail.com'
        	'CSBTCITY'                   => $operation->CSBTCITY, //Ciudad de facturación, REQUERIDO.
        	'CSBTCOUNTRY'                => $operation->CSBTCOUNTRY, //País de facturación. REQUERIDO. Código ISO.
        	'CSBTCUSTOMERID'             => $operation->CSBTCUSTOMERID, //Identificador del usuario al que se le emite la factura. REQUERIDO. No puede contener un correo electrónico.
        	'CSBTIPADDRESS'              => $operation->CSBTIPADDRESS, //IP de la PC del comprador. REQUERIDO.
        	'CSBTEMAIL'                  => $operation->CSBTEMAIL, //Mail del usuario al que se le emite la factura. REQUERIDO.
        	'CSBTFIRSTNAME'              => $operation->CSBTFIRSTNAME,//Nombre del usuario al que se le emite la factura. REQUERIDO.
        	'CSBTLASTNAME'               => $operation->CSBTLASTNAME, //Apellido del usuario al que se le emite la factura. REQUERIDO.
        	'CSBTPHONENUMBER'            => $operation->CSBTPHONENUMBER, //Teléfono del usuario al que se le emite la factura. No utilizar guiones, puntos o espacios. Incluir código de país. REQUERIDO.
        	'CSBTPOSTALCODE'             => $operation->CSBTPOSTALCODE, //Código Postal de la dirección de facturación. REQUERIDO.
        	'CSBTSTATE'                  => $operation->CSBTSTATE, //Provincia de la dirección de facturación. REQUERIDO. Ver tabla anexa de provincias.
        	'CSBTSTREET1'                => $operation->CSBTSTREET1, //Domicilio de facturación (calle y nro). REQUERIDO.
        	'CSPTCURRENCY'               => $this->_paraments->payment_getway->todopago->CSPTCURRENCY, //Moneda. REQUERIDO.
        	'CSPTGRANDTOTALAMOUNT'       => $operation->CSPTGRANDTOTALAMOUNT, //Con decimales opcional usando el punto como separador de decimales. No se permiten comas, ni como separador de miles ni como separador de decimales. REQUERIDO. (Ejemplos:$125,38-> 125.38 $12-> 12 o 12.00)
        	'CSSTCITY'                   => $operation->CSSTCITY, //Ciudad de envío de la orden. REQUERIDO.
        	'CSSTCOUNTRY'                => $operation->CSSTCOUNTRY, //País de envío de la orden. REQUERIDO.
        	'CSSTEMAIL'                  => $operation->CSSTEMAIL, //Mail del destinatario, REQUERIDO.
        	'CSSTFIRSTNAME'              => $operation->CSSTFIRSTNAME, //Nombre del destinatario. REQUERIDO.
        	'CSSTLASTNAME'               => $operation->CSSTLASTNAME, //Apellido del destinatario. REQUERIDO.
        	'CSSTPHONENUMBER'            => $operation->CSSTPHONENUMBER, //Número de teléfono del destinatario. REQUERIDO.
        	'CSSTPOSTALCODE'             => $operation->CSSTPOSTALCODE, //Código postal del domicilio de envío. REQUERIDO.
        	'CSSTSTATE'                  => $operation->CSSTSTATE, //Provincia de envío. REQUERIDO. Son de 1 caracter
        	'CSSTSTREET1'                => $operation->CSSTSTREET1, //Domicilio de envío. REQUERIDO.
        	//Retail: datos a enviar por cada producto, los valores deben estar separados con #:
        	'CSITPRODUCTCODE'            => $operation->CSITPRODUCTCODE, //Código de producto. REQUERIDO. Valores posibles(adult_content;coupon;default;electronic_good;electronic_software;gift_certificate;handling_only;service;shipping_and_handling;shipping_only;subscription)
        	'CSITPRODUCTDESCRIPTION'     => $operation->CSITPRODUCTDESCRIPTION, //Descripción del producto. REQUERIDO.
        	'CSITPRODUCTNAME'            => $operation->CSITPRODUCTNAME, //Nombre del producto. REQUERIDO.
        	'CSITPRODUCTSKU'             => $operation->CSITPRODUCTSKU, //Código identificador del producto. REQUERIDO.
        	'CSITTOTALAMOUNT'            => $operation->CSITTOTALAMOUNT, //CSITTOTALAMOUNT=CSITUNITPRICE*CSITQUANTITY "999999[.CC]" Con decimales opcional usando el punto como separador de decimales. No se permiten comas, ni como separador de miles ni como separador de decimales. REQUERIDO.
        	'CSITQUANTITY'               => $operation->CSITQUANTITY, //Cantidad del producto. REQUERIDO.
        	'CSITUNITPRICE'              => $operation->CSITUNITPRICE, //Formato Idem CSITTOTALAMOUNT. REQUERIDO.
            'MININSTALLMENTS'            => $this->_paraments->payment_getway->todopago->MININSTALLMENTS, //Mínimo de cuotas a mostrar en el formulario
	        'MAXINSTALLMENTS'            => $this->_paraments->payment_getway->todopago->MAXINSTALLMENTS, //Máximo de cuotas a mostrar en el formulario
        	'AVAILABLEPAYMENTMETHODSIDS' => $AVAILABLEPAYMENTMETHODSIDS,
    	);

        return $this->_connector->sendAuthorizeRequest($optionsSAR_comercio, $optionsSAR_operacion);
    }

    public function returnRequest($values)
    {
        if (empty($this->_connector)) {
            $this->instance();
        }
        $mode = $this->_paraments->payment_getway->todopago->MODO;
        $options = array(
        	"Security"   => str_replace("TODOPAGO ", "", $this->_paraments->payment_getway->todopago->$mode->api_keys), // API Key del comercio asignada por TodoPago
        	"Merchant"   => $this->_paraments->payment_getway->todopago->$mode->merchant_id, // Merchant o Nro de comercio asignado por TodoPago
        	"RequestKey" => $values['request_key'], // RequestKey devuelto como respuesta del servicio SendAutorizeRequest
        	"AMOUNT"     => $values['amount'], // Opcional. Monto a devolver, si no se envía, se trata de una devolución total
        );

        return $this->_connector->returnRequest($options);
    }

    public function getStatus($operation_id)
    {
        if (empty($this->_connector)) {
            $this->instance();
        }
        $mode = $this->_paraments->payment_getway->todopago->MODO;
        $options = array(
        	"MERCHANT"    => $this->_paraments->payment_getway->todopago->$mode->merchant_id, // Merchant o Nro de comercio asignado por TodoPago
        	"OPERATIONID" => $operation_id // RequestKey devuelto como respuesta del servicio SendAutorizeRequest
        );
        return $this->_connector->getStatus($options);
    }

    /**
     * En este caso hay que llamar a getByRangeDateTime() y devolverá todas las operaciones realizadas en el rango de fechas dado
     * 
     * * Este método devuelve páginas de 5 transacciones, por medio del campo PAGENUMBER se puede indicar a que página se desea acceder
     * 
     * Respuesta:
     * 
     * array (size=1)
          'Operations' =>
            array (size=19)
              'RESULTCODE' => string '999' (length=3)
              'RESULTMESSAGE' => string 'RECHAZADA' (length=9)
              'DATETIME' => string '2015-05-13T14:11:38.287+00:00' (length=29)
              'OPERATIONID' => string '01' (length=2)
              'CURRENCYCODE' => string '32' (length=2)
              'AMOUNT' => int 54
              'AMOUNTBUYER' => string '67.30' (length=5)
              'TYPE' => string 'compra_online' (length=13)
              'INSTALLMENTPAYMENTS' => string '4' (length=1)
              'CUSTOMEREMAIL' => string 'cosme@fulanito.com' (length=18)
              'IDENTIFICATIONTYPE' => string 'DNI' (length=3)
              'IDENTIFICATION' => string '1212121212' (length=10)
              'CARDNUMBER' => string '12121212XXXXXX1212' (length=18)
              'CARDHOLDERNAME' => string 'Cosme Fulanito' (length=14)
              'TICKETNUMBER' => int 0
              'AUTHORIZATIONCODE' => null
              'BARCODE' => null
              'COUPONEXPDATE' => null
              'COUPONSECEXPDATE' => null
              'COUPONSUBSCRIBER' => null
     */ 
    public function getByRangeDateTime($start_date, $end_date, $page_number)
    {
        if (empty($this->_connector)) {
            $this->instance();
        }
        $mode = $this->_paraments->payment_getway->todopago->MODO;
        //Fecha en formato "Y-m-d"
        return $this->_connector->getByRangeDateTime(array(
        	"MERCHANT"   => $this->_paraments->payment_getway->todopago->$mode->merchant_id, // Merchant o Nro de comercio asignado por TodoPago
            "STARTDATE"  => $start_date, 
            "ENDDATE"    => $end_date, 
            "PAGENUMBER" => $page_number)
        );
    }
}