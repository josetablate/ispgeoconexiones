<?php
namespace App\Controller\Component;

use App\Controller\Component\Common\MyComponent;
use Cake\Controller\ComponentRegistry;
use Cake\Http\Client;
use Cake\Core\Configure;
use Cake\Filesystem\File;
use Cake\I18n\Time;

/**
 * BrainminatorWS component
 */
class CuentaDigitalComponent extends MyComponent
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    private $_cuentadigital;

    public function initialize(array $config)
    {
        parent::initialize($config);

        $payment_getway = $this->_ctrl->request->getSession()->read('payment_getway');
        $this->_cuentadigital = $payment_getway->config->cuentadigital;
    }

    public function generateBarcode($customer_code, $account_number)
    {
        //$account_number = $this->_cuentadigital->account_number;
        $url = $this->_cuentadigital->url_generate_barcode;
        $url = str_replace("customer_code", $customer_code, $url);
        $url = str_replace("account_number", $account_number, $url);
        $result = file_get_contents($url);
        return $result;
    }

    public function consultarTransacciones($control_number, $account_number, $date = NULL)
    {
        //resultado
        // 20190314|140720|100,00|92,44|01908949628987|PagoFacil|1 20190314|140712|100,00|92,44|01905196669497|PagoFacil|2

        //https:\/\/www.cuentadigital.com\/exportacion.php?control=control_number&fecha=date&hour1=00&min1=00&hour2=23&min2=59

        if ($date == NULL) {
            $date = $this->_cuentadigital->period != "" ? $this->_cuentadigital->period : Time::now()->format('Ymd');
        }

        //$date = '20190314';

        //$control_number = $this->_cuentadigital->control_number;
        //$url = $this->_cuentadigital->url_export;
        //$url = "https://www.cuentadigital.com/exportacionsandbox.php?control=control_number&fecha=date&hour1=00&min1=00&hour2=23&min2=59";

        // URL TESTING
        //$url = "https://www.cuentadigital.com/exportacionsandbox.php?control=control_number&fecha=date";

        // URL PROD
        $url = "https://www.cuentadigital.com/exportacion.php?control=control_number&fecha=date";

        $url = str_replace("control_number", $control_number, $url);
        $url = str_replace("date", $date, $url);

        $result = file_get_contents($url);

        $detail = 'Resultado de consultar transacciones - Cuenta Digital';
        $detail .= 'Datos: ' . PHP_EOL;
        $detail .= 'Control: ' . $control_number . PHP_EOL;
        //$detail .= 'Nro de Cuenta: ' . $this->_cuentadigital->account_number . PHP_EOL;
        $detail .= 'Nro de Cuenta: ' . $account_number . PHP_EOL;
        $detail .= 'Fecha: ' . Time::now()->format('d/m/Y HH:mm') . PHP_EOL;
        $detail .= 'URL: ' . $url . PHP_EOL;
        $detail .=  '--------------------------------' . PHP_EOL;
        $detail .= 'Resultado: ' . PHP_EOL;
        $detail .= json_encode($result);

        $path = $this->generateLog($detail);

        $action = 'Generación LOG - Cuenta Digital';
        $detail = "";
        $detail .= 'Datos: ' . PHP_EOL;
        $detail .= 'Control: ' . $control_number . PHP_EOL;
        //$detail .= 'Nro de Cuenta: ' . $this->_cuentadigital->account_number . PHP_EOL;
        $detail .= 'Nro de Cuenta: ' . $account_number . PHP_EOL;
        $detail .= 'Fecha: ' . Time::now()->format('d/m/Y HH:mm') . PHP_EOL;
        $detail .= 'URL: ' . $url . PHP_EOL;
        $detail .= 'Path: ' . $path;

        $this->_ctrl->registerActivity($action, $detail, NULL, TRUE);

        return $result;
    }

    public function createLinkPago($customer_code, $data)
    {
        //https://www.cuentadigital.com/api.php?id=637797&precio=15,30&venc=7&codigo=15&hacia=website2@website2.com&concepto=hosting plan 4
        $url = "https://www.cuentadigital.com/api.php?id=";
        $this->_ctrl->loadModel('CuentadigitalAccounts');
        $cuentadigital_account = $this->_ctrl->CuentadigitalAccounts
            ->find()
            ->contain([
                'Customers'
            ])
            ->where([
                'customer_code' => $customer_code
            ])
            ->first();

        if ($cuentadigital_account && $cuentadigital_account->account_number) {

            $url .= $cuentadigital_account->account_number;

            //monto a cobrar, si es abierto no se agrega nada
            if ($data->cd_saldo == 1) {

                //cd_saldo: 0 -> abierto
                //cd_saldo: 1 -> saldo al mes
                $url .= '&precio=';

                if ($data->saldo_customer == 0
                    || $data->saldo_customer < 5) {
                    return FALSE;
                }

                $saldo = $data->saldo_customer;

                $url .= $saldo;
            }

            if ($data->cd_venc != "") {

                $url .= '&venc=' . $data->cd_venc;
            }

            $url .= '&codigo=ispbrain-' . $customer_code;

            $array_email = explode(',', $cuentadigital_account->customer->email);

            $email = $array_email[0];

            $url .= '&hacia=' . $email;
            $url .= '&concepto=Servicio Internet';

            if ($data->cd_credit_card) {
                $url .= '&m2=1';
            } else {
                $url .= '&m2=0';
            }

            if ($data->cd_efectivo) {
                $url .= '&m4=1';
            } else {
                $url .= '&m4=0';
            }

        } else {

            return FALSE;
        }

        return $url;
    }

    private function generateLog($data)
    {
        $time = Time::now()->format('Ymd');

        $path = "log_payment_getway/$time-cuentadigital.txt";

        $file = new File(WWW_ROOT . $path, true, 0775);
        $file->write($data, 'w+', TRUE);
        $file->close();

        return $path;
    }
}
