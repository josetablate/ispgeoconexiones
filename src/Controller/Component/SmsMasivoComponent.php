<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Http\Client;
use Cake\Core\Configure;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\Event\EventManager;
use Cake\Event\Event;

/**
 * SmsMasivo component
 */
class SmsMasivoComponent extends Component
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    private $_ctrl;
    private $_conf;

    public function initialize(array $config)
    {
        $this->_ctrl = $this->_registry->getController();
        $paraments = $this->_ctrl->request->getSession()->read('paraments');
        $this->_conf = $paraments->sms->platform->smsmasivo;

        $this->initEventManager();
    }

    public function initEventManager() 
    {
        EventManager::instance()->on(
            'SmsMasivoComponent.Error',
            function($event, $msg, $data, $flash = false) {

                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $log->user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Error';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);

                if ($flash) {
                    $this->_ctrl->Flash->error(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'SmsMasivoComponent.Warning',
            function($event, $msg, $data, $flash = false) {

                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $log->user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Warning';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);

                if ($flash) {
                    $this->_ctrl->Flash->warning(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'SmsMasivoComponent.Log',
            function($event, $msg, $data) {

            }
        );

        EventManager::instance()->on(
            'SmsMasivoComponent.Notice',
            function($event, $msg, $data) {

            }
        );
    }

    public function validateAccount()
    {
        $method_url = $this->_conf->consulta_saldo_endpoint;
        $usuario = $this->_conf->smsusuario;
        $method_url = str_replace("<usuario>", urlencode($usuario), $method_url);
        $clave = $this->_conf->smsclave;
        $method_url = str_replace("<clave>", urlencode($clave), $method_url);

        $url = $this->_conf->url . $method_url;
        $smsrespuesta = null;

        $smsrespuesta = @file_get_contents($url);
        if (strpos($http_response_header[0], "200")) {
            if (!$smsrespuesta) {
                $data_error = new \stdClass; 
                $data_error->errors = "";

                $event = new Event('SmsMasivoComponent.Error', $this, [
                	'msg'   => __('Hubo un error al validar la cuenta en SMS Masivo.'),
                	'data'  => $data_error,
                	'flash' => true
                ]);

                $this->_ctrl->getEventManager()->dispatch($event);
                return false;
            }
        } else {
            $data_error = new \stdClass; 
            $data_error->errors = $http_response_header;

            $event = new Event('SmsMasivoComponent.Error', $this, [
            	'msg'   => __('Hubo un error al comunicarse con el servidor SMS Masivo y la validar la cuenta.'),
            	'data'  => $data_error,
            	'flash' => true
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);
            return false;
        }
        return true;
    }

    public function enviarMensaje()
    {
        $response = null;
        $method_url = $this->_conf->enviar_mensaje_endpoint;
        $usuario = $this->_conf->smsusuario;
        $method_url = str_replace("<usuario>", $user, $method_url);
        $clave = $this->_conf->smsclave;
        $method_url = str_replace("<clave>", $clave, $method_url);
        $url = $this->_conf->url . $this->_conf->enviar_mensaje_endpoint;
        try {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
            $response = curl_exec($curl);
            curl_close($curl);
        } catch (\Exception $e) {
            $data_error = new \stdClass; 
            $data_error->errors = $e->getMessage();

            $event = new Event('SmsMasivoComponent.Error', $this, [
            	'msg'   => __('Hubo un error al enviar mensajes al servidor de SMS Masivo.'),
            	'data'  => $data_error,
            	'flash' => true
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);
            Log::write('error', $e->getMessage());
        }

        if (!empty($response)) {
            if (isset($response->code) && $response->code != 200) {

            } else {
                if (!empty(json_decode($response))) {
                    foreach (json_decode($response) as $res) {
                        $stores[$res->id] = $res->nombre;
                    }
                }
            }
        }

        return $stores;
    }

    public function enviarBloque($bloque)
    {
        $method_url = $this->_conf->enviar_bloque_endpoint;
        $usuario = $this->_conf->smsusuario;
        $method_url = str_replace("<usuario>", urlencode($usuario), $method_url);
        $clave = $this->_conf->smsclave;
        $method_url = str_replace("<clave>", urlencode($clave), $method_url);
        $method_url = str_replace("<bloque>", urlencode($bloque), $method_url);

        if ($this->_conf->test) {
            $method_url .= "&test=1";
        }

        $url = $this->_conf->url . $method_url;
        $smsrespuesta = null;

        $smsrespuesta = @file_get_contents($url);

        if (strpos($http_response_header[0], "200")) { 
            if (!$smsrespuesta) {
                $data_error = new \stdClass; 
                $data_error->errors = "";

                $event = new Event('SmsMasivoComponent.Error', $this, [
                	'msg'   => __('Error al enviar bloque al servidor de SMS Masivo.'),
                	'data'  => $data_error,
                	'flash' => true
                ]);

                $this->_ctrl->getEventManager()->dispatch($event);
                Log::write('error', 'SmsMasivoComponent - enviarBloque sms respuesta false.');
                return false;
            }
        } else {
            $data_error = new \stdClass; 
            $data_error->errors = $http_response_header;

            $event = new Event('SmsMasivoComponent.Error', $this, [
            	'msg'   => __('Error al realizar la consulta al servidor SMS Masivo.'),
            	'data'  => $data_error,
            	'flash' => true
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);
            Log::write('error', 'Error al realizar la consulta al servidor SMS Masivo');
            Log::write('error', $http_response_header);
            return false;
        }
        return $smsrespuesta;
    }

    public function mensajesEnviados()
    {
        //obtener_envios.asp?usuario=<usuario>&clave=<clave>
        $method_url = $this->_conf->consulta_mensajes_enviados_endpoint;
        $usuario = $this->_conf->smsusuario;
        $method_url = str_replace("<usuario>", urlencode($usuario), $method_url);
        $clave = $this->_conf->smsclave;
        $method_url = str_replace("<clave>", urlencode($clave), $method_url);

        $url = $this->_conf->url . $method_url;
        $smsrespuesta = null;

        $smsrespuesta = @file_get_contents($url);
        if (strpos($http_response_header[0], "200")) { 
            if (!$smsrespuesta) {
                $data_error = new \stdClass; 
                $data_error->errors = "";

                $event = new Event('SmsMasivoComponent.Error', $this, [
                	'msg'   => __('Error al enviar Mensajes al servidor de SMS Masivo.'),
                	'data'  => $data_error,
                	'flash' => true
                ]);

                $this->_ctrl->getEventManager()->dispatch($event);
                Log::write('error', 'SmsMasivoComponent - mensajesEnviados sms respuesta false.');
                return false;
            }
        } else {
            $data_error = new \stdClass; 
            $data_error->errors = $http_response_header;

            $event = new Event('SmsMasivoComponent.Error', $this, [
            	'msg'   => __('Error al enviar mensajes al servidor de SMS Masivo.'),
            	'data'  => $data_error,
            	'flash' => true
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);
            Log::write('error', 'Error al realizar la consulta');
            Log::write('error', $http_response_header);
            return false;
        } 
        return $smsrespuesta;
    }

    public function consultarVencimientoPaquete()
    {
        //obtener_vencimiento_paquete.asp?usuario=<usuario>&clave=<clave>
        $method_url = $this->_conf->consulta_vencimiento_paquete_endpoint;
        $usuario = $this->_conf->smsusuario;
        $method_url = str_replace("<usuario>", urlencode($usuario), $method_url);
        $clave = $this->_conf->smsclave;
        $method_url = str_replace("<clave>", urlencode($clave), $method_url);

        $url = $this->_conf->url . $method_url;
        $smsrespuesta = null;

        $smsrespuesta = @file_get_contents($url);
        if (strpos($http_response_header[0], "200")) { 
            if (!$smsrespuesta) {
                $data_error = new \stdClass; 
                $data_error->errors = "";

                $event = new Event('SmsMasivoComponent.Error', $this, [
                	'msg'   => __('Error al consultar vencimiento de paquete al servidor de SMS Masivo.'),
                	'data'  => $data_error,
                	'flash' => true
                ]);

                $this->_ctrl->getEventManager()->dispatch($event);
                Log::write('error', 'SmsMasivoComponent - consultarVencimientoPaquete sms respuesta false.');
                return false;
            }
        } else {
            $data_error = new \stdClass; 
            $data_error->errors = $http_response_header;

            $event = new Event('SmsMasivoComponent.Error', $this, [
            	'msg'   => __('Error al consultar vencimiento de paquete al servidor SMS Masivo.'),
            	'data'  => $data_error,
            	'flash' => true
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);
            Log::write('error', 'Error al realizar la consulta');
            Log::write('error', $http_response_header);
            return false;
        } 
        return $smsrespuesta;
    }

    public function consultarSaldo()
    {
        //obtener_saldo.asp?usuario=<usuario>&clave=<clave>
        $method_url = $this->_conf->consulta_saldo_endpoint;
        $usuario = $this->_conf->smsusuario;
        $method_url = str_replace("<usuario>", urlencode($usuario), $method_url);
        $clave = $this->_conf->smsclave;
        $method_url = str_replace("<clave>", urlencode($clave), $method_url);

        $url = $this->_conf->url . $method_url;
        $smsrespuesta = null;

        $smsrespuesta = @file_get_contents($url);
        if (strpos($http_response_header[0], "200")) {
            if (!$smsrespuesta) {
                $data_error = new \stdClass; 
                $data_error->errors = "";

                $event = new Event('SmsMasivoComponent.Error', $this, [
                    'msg'   => __('Error al consultar saldo al servidor de SMS Masivo.'),
                    'data'  => $data_error,
                    'flash' => true
                ]);

                $this->_ctrl->getEventManager()->dispatch($event);
                Log::write('error', 'SmsMasivoComponent - consultarSaldo sms respuesta false.');
                return false;
            }
        } else {
            $data_error = new \stdClass; 
            $data_error->errors = $http_response_header;

            $event = new Event('SmsMasivoComponent.Error', $this, [
                'msg'   => __('Error al consultar saldo al servidor de SMS Masivo.'),
                'data'  => $data_error,
                'flash' => true
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);
            Log::write('error', 'Error al realizar la consulta');
            Log::write('error', $http_response_header);
            return false;
        }
        return $smsrespuesta;
    }

    public function consultarBloqueIdInterno($id_interno)
    {
        //obtener_respuestaapi_bloque.asp?usuario=<usuario>&clave=<clave>&idinterno=<idinterno>
        $method_url = $this->_conf->consulta_bloque_idinterno_endpoint;
        $usuario = $this->_conf->smsusuario;
        $method_url = str_replace("<usuario>", urlencode($usuario), $method_url);
        $clave = $this->_conf->smsclave;
        $method_url = str_replace("<clave>", urlencode($clave), $method_url);
        $method_url = str_replace("<idinterno>", urlencode($id_interno), $method_url);

        $url = $this->_conf->url . $method_url;
        $smsrespuesta = null;

        $smsrespuesta = @file_get_contents($url);
        if (strpos($http_response_header[0], "200")) { 
            if (!$smsrespuesta) {
                $data_error = new \stdClass; 
                $data_error->errors = "";

                $event = new Event('SmsMasivoComponent.Error', $this, [
                    'msg'   => __('Error al consultar bloque interno al servidor SMS Masivo.'),
                    'data'  => $data_error,
                    'flash' => true
                ]);

                $this->_ctrl->getEventManager()->dispatch($event);
                Log::write('error', 'SmsMasivoComponent - consultarBloqueIdInterno sms respuesta false.');
                return false;
            }
        } else {
            $data_error = new \stdClass; 
            $data_error->errors = $http_response_header;

            $event = new Event('SmsMasivoComponent.Error', $this, [
                'msg'   => __('Error al consultar bloque interno al servidor SMS Masivo.'),
                'data'  => $data_error,
                'flash' => true
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);
            Log::write('error', 'Error al realizar la consulta');
            Log::write('error', $http_response_header);
            return false;
        } 
        return $smsrespuesta;
    }

    public function consultarBloqueFecha($fecha)
    {
        //obtener_respuestaapi_bloque.asp?usuario=<usuario>&clave=<clave>&fecha=<fecha>
        $method_url = $this->_conf->consulta_bloque_fecha_endpoint;
        $usuario = $this->_conf->smsusuario;
        $method_url = str_replace("<usuario>", urlencode($usuario), $method_url);
        $clave = $this->_conf->smsclave;
        $method_url = str_replace("<clave>", urlencode($clave), $method_url);
        $method_url = str_replace("<fecha>", urlencode($fecha), $method_url);

        $url = $this->_conf->url . $method_url;
        $smsrespuesta = null;

        $smsrespuesta = @file_get_contents($url);
        if (strpos($http_response_header[0], "200")) { 
            if (!$smsrespuesta) {
                $data_error = new \stdClass; 
                $data_error->errors = "";

                $event = new Event('SmsMasivoComponent.Error', $this, [
                    'msg'   => __('Error al consultar bloque por fecha al servidor SMS Masivo.'),
                    'data'  => $data_error,
                    'flash' => true
                ]);

                $this->_ctrl->getEventManager()->dispatch($event);
                Log::write('error', 'SmsMasivoComponent - consultarBloqueFecha sms respuesta false.');
                return false;
            }
        } else {
            $data_error = new \stdClass; 
            $data_error->errors = $http_response_header;

            $event = new Event('SmsMasivoComponent.Error', $this, [
                'msg'   => __('Error al consultar bloque por fecha al servidor SMS Masivo.'),
                'data'  => $data_error,
                'flash' => true
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);
            Log::write('error', 'Error al realizar la consulta');
            Log::write('error', $http_response_header);
            return false;
        } 
        return $smsrespuesta;
    }

    public function consultarFechaServidor()
    {
        //get_fecha.asp?iso=1
        $method_url = $this->_conf->consulta_fecha_servidor_endpoint;
        $url = $this->_conf->url . $method_url;
        $smsrespuesta = null;

        $smsrespuesta = @file_get_contents($url);
        if (strpos($http_response_header[0], "200")) { 
            if (!$smsrespuesta) {
                $data_error = new \stdClass; 
                $data_error->errors = "";

                $event = new Event('SmsMasivoComponent.Error', $this, [
                    'msg'   => __('Error al consultar la fecha al servidor SMS Masivo.'),
                    'data'  => $data_error,
                    'flash' => true
                ]);

                $this->_ctrl->getEventManager()->dispatch($event);
                Log::write('error', 'SmsMasivoComponent - consultarFechaServidor sms respuesta false.');
                return false;
            }
        } else {
            $data_error = new \stdClass; 
            $data_error->errors = $http_response_header;

            $event = new Event('SmsMasivoComponent.Error', $this, [
                'msg'   => __('Error al consultar la fecha al servidor SMS Masivo.'),
                'data'  => $data_error,
                'flash' => true
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);
            Log::write('error', 'Error al realizar la consulta');
            Log::write('error', $http_response_header);
            return false;
        } 

        return $smsrespuesta;
    }
}
