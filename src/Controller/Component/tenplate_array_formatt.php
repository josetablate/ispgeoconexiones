<?php

     $templates = [
        
        'mikrotik' => [
            
            'template_a' => [
                'trademark' => 'mikrotik',
                'name' => 'PPPOE SECRETS + ASIG. ESTATICA + DINAMICO SIMPLE',
                'description' => '',
                'connection_type' => [
                    'access' => 'pppoe_secrets',
                    'address' => 'static',
                    'bandwidth' => 'simple_dynamic'
                    ]
                ],
                
            'template_b' => [
                'trademark' => 'mikrotik',
                'name' => 'PPPOE SECRETS + ASIG. DINAMICO + DINAMICO SIMPLE',
                'description' => '',
                'connection_type' => [
                    'access' => 'pppoe-secrets',
                    'address' => 'dynamic',
                    'bandwidth' => 'simple-dynamic'
                    ]
                ],
            ],
        'otra_marca' => [
            
            'template_c' => [
                'trademark' => 'otra_marca',
                'name' => 'PPPOE SECRETS + ASIG. ESTATICA + DINAMICO SIMPLE',
                'description' => '',
                'connection_type' => [
                    'access' => 'pppoe-secrets',
                    'address' => 'static',
                    'bandwidth' => 'simple-dynamic'
                    ]
                ],
            'template_d' => [
                'trademark' => 'otra_marca',
                'name' => 'PPPOE SECRETS + ASIG. ESTATICA + DINAMICO SIMPLE',
                'description' => '',
                'connection_type' => [
                    'access' => 'pppoe-secrets',
                    'address' => 'static',
                    'bandwidth' => 'simple-dynamic'
                    ]
                ],
            ]
        ];
    
    