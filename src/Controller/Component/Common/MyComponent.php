<?php
namespace App\Controller\Component\Common;

use Cake\Controller\Component;

/**
 * Integration component
 */
class MyComponent extends Component 
{   
     protected $_ctrl;

     public function initialize(array $config) {

          $this->_ctrl = $this->_registry->getController();    

     }   
}
