<?php
namespace App\Controller\Component;


use App\Controller\Component\Integrations\MikrotikPppoe;
// use Cake\Controller\ComponentRegistry;

use Cidr;
// use Cake\I18n\Time;
use Cake\Event\EventManager;
use Cake\Event\Event;

use Cake\Core\Configure;

use Cake\ORM\TableRegistry;
use Cake\ORM\Locator\TableLocator;

use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Query;



/**
 * Integration component
 */
class MikrotikPppoeTaComponent extends MikrotikPppoe
{
    
    public function initialize(array $config)
    {  
        parent::initialize($config);

        $this->initEventManager();         
        
        $this->templateTableRegistry();
    }

    protected function initEventManager()
    {
        parent::initEventManager();
        
        EventManager::instance()->on(
            'MikrotikPppoeTaComponent.Error',
            function($event, $msg, $data, $flash = false){
                
                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $log->user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Error';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);
                
                if($flash){
                    $this->_ctrl->Flash->error(__($msg));
                }
            }
        );
        
        EventManager::instance()->on(
            'MikrotikPppoeTaComponent.Warning',
            function($event, $msg, $data, $flash = false){
                
                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $log->user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Warning';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);
                
                if($flash){
                    $this->_ctrl->Flash->warning(__($msg));
                }
            }
        );
        
        EventManager::instance()->on(
            'MikrotikPppoeTaComponent.Log',
            function($event, $msg, $data){
                
            }
        );
        
        EventManager::instance()->on(
            'MikrotikPppoeTaComponent.Notice',
            function($event, $msg, $data){
             
            }
        );
    }
     
    protected function templateTableRegistry(){
         
         if(!TableRegistry::getTableLocator()->exists('MikrotikPppoeTa_Controllers')){
              
               TableRegistry::getTableLocator()->setConfig('MikrotikPppoeTa_Controllers', [
                 'className' => 'App\Model\Table\MikrotikPppoeTa\ControllersTable'
               ]);               
         }
         
         if(!TableRegistry::getTableLocator()->exists('MikrotikPppoeTa_IpExcluded')){
              
               TableRegistry::getTableLocator()->setConfig('MikrotikPppoeTa_IpExcluded', [
                 'className' => 'App\Model\Table\MikrotikPppoeTa\IpExcludedTable'
               ]);               
         }
         
         if(!TableRegistry::getTableLocator()->exists('MikrotikPppoeTa_Plans')){
              
               TableRegistry::getTableLocator()->setConfig('MikrotikPppoeTa_Plans', [
                 'className' => 'App\Model\Table\MikrotikPppoeTa\PlansTable'
               ]);               
         }
         
         if(!TableRegistry::getTableLocator()->exists('MikrotikPppoeTa_Pools')){
              
               TableRegistry::getTableLocator()->setConfig('MikrotikPppoeTa_Pools', [
                 'className' => 'App\Model\Table\MikrotikPppoeTa\PoolsTable'
               ]);               
         }
         
         if(!TableRegistry::getTableLocator()->exists('MikrotikPppoeTa_PppoeServers')){
              
               TableRegistry::getTableLocator()->setConfig('MikrotikPppoeTa_PppoeServers', [
                 'className' => 'App\Model\Table\MikrotikPppoeTa\PppoeServersTable'
               ]);               
         }
         
         if(!TableRegistry::getTableLocator()->exists('MikrotikPppoeTa_Profiles')){
              
               TableRegistry::getTableLocator()->setConfig('MikrotikPppoeTa_Profiles', [
                 'className' => 'App\Model\Table\MikrotikPppoeTa\ProfilesTable'
               ]);               
         }
         
         if(!TableRegistry::getTableLocator()->exists('MikrotikPppoeTa_Queues')){
              
               TableRegistry::getTableLocator()->setConfig('MikrotikPppoeTa_Queues', [
                 'className' => 'App\Model\Table\MikrotikPppoeTa\QueuesTable'
               ]);               
         }
         
         if(!TableRegistry::getTableLocator()->exists('MikrotikPppoeTa_Secrets')){
              
               TableRegistry::getTableLocator()->setConfig('MikrotikPppoeTa_Secrets', [
                 'className' => 'App\Model\Table\MikrotikPppoeTa\SecretsTable'
               ]);               
         }
                    


          $this->modelControllers = $this->_ctrl->loadModel('MikrotikPppoeTa_Controllers');
          $this->modelIpExcluded = $this->_ctrl->loadModel('MikrotikPppoeTa_IpExcluded');
          $this->modelPlans = $this->_ctrl->loadModel('MikrotikPppoeTa_Plans');
          $this->modelPools = $this->_ctrl->loadModel('MikrotikPppoeTa_Pools');
          $this->modelPppoeServers = $this->_ctrl->loadModel('MikrotikPppoeTa_PppoeServers');
          $this->modelProfiles = $this->_ctrl->loadModel('MikrotikPppoeTa_Profiles');
          $this->modelQueues = $this->_ctrl->loadModel('MikrotikPppoeTa_Queues');
          $this->modelSecrets = $this->_ctrl->loadModel('MikrotikPppoeTa_Secrets');             
      
    }   
    
  

    
    //controller
    
    public function addController($controller){
         
        $tController =  $this->modelControllers->newEntity();
        $tController->id = $controller->id;        
        
        if(!$this->modelControllers->save($tController)){
            
            $data_error = new \stdClass; 
            $data_error->tController = $tController;
            $data_error->errors = $tController->getErrors();
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                'msg' => __('No se pudo registrar el Controlador.'),
                'data' => $data_error,
                'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
        return $tController;
    }
    
    public function editController($controller, $data){
         
        $tController = $this->modelControllers->get($controller->id);
        
        $tController =  $this->modelControllers->patchEntity($tController, $data);
        
        if(!$this->modelControllers->save($tController)){
            
            $data_error = new \stdClass; 
            $data_error->tController = $tController;
            $data_error->errors = $tController->getErrors();
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                'msg' => __('No se pudo editar el Controlador en la base de datos.'),
                'data' => $data_error,
                'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        return true;
    }
    
    public function getControllerTemplate($controller){
      
        return $this->modelControllers->get($controller->id);
    }
    
    public function deleteController($controller){  
        
        $tController = $this->modelControllers->get($controller->id);
        $tController->deleted = true;
        
        if(!$this->modelControllers->save($tController)){
            
            $data_error = new \stdClass; 
            $data_error->tController = $tController;
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
            'msg' => __('No se pudo eliminar el Controlador de la base de datos.'),
            'data' => $data_error,
            'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
          
        }
        return $tController;
    }
     
    public function getStatus($controller){          
         return $this->getStatusApi($controller);          
    }
     
    public function cloneController($id){
         
         $this->_ctrl->loadModel('Controllers');
         
         $error = false;
         
         $controller = $this->_ctrl->Controllers->get($id);
         $tcontroller = $this->getControllerTemplate($controller);
         
         $tcontroller->pppoe_servers = $this->getPppoeServers($id);
         $tcontroller->ip_excluded = $this->getIPExcluded($id);
         $tcontroller->profiles = $this->getProfiles($id);
         $tcontroller->pools = $this->getPools($id);
         $tcontroller->plans = $this->getPlans($id);
         
         $newController = $this->_ctrl->Controllers->newEntity();    
         $newController = $this->_ctrl->Controllers->patchEntity($newController, $controller->toArray());
         
         $newController->name .= ' (Copia)';
         $newController->apply_config_avisos = false;
         $newController->apply_config_queue_graph = false;
         $newController->apply_config_https = false;
         $newController->apply_certificate_apissl = false;
         $newController->apply_certificate_wwwssl = false;
         
         if(!$this->_ctrl->Controllers->save($newController)){
              return false;
         }

         $newTController = $this->AddController($newController);  

         if(!$newTController){
              $this->Flash->error(__('No se pudo crear el (controller).')); 
              $error = true; 
         }    

         if(!$error){
              
              $parse_profiles = [];
              $parse_pool = [];

              $newTController->pppoe_servers = [];
              $newTController->ip_excluded = [];
              $newTController->profiles = [];
              $newTController->pools = [];
              $newTController->plans = [];   

              foreach($tcontroller->pppoe_servers as $ps){

                    $data =  $ps->toArray();
                    $data['controller_id'] = $newTController->id;         
                    $new_pppoe_server = $this->addPppoeServer($data);  
                   
                    if(!$new_pppoe_server){
                            
                         $error = true; 
                         
                         $data_error = new \stdClass; 
                         $data_error->data = $data;               

                         $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                              'msg' => __('No se pudo crear el (pppoe server)'),
                              'data' => $data_error,
                              'flash' => false
                         ]);

                         $this->_ctrl->getEventManager()->dispatch($event);

                         break;
                    }
                   
                    $newTController->pppoe_servers[] = $new_pppoe_server;                    
              }

              if(!$error){

                   foreach($tcontroller->ip_excluded as $ie){

                         $data =  $ie->toArray();
                         $data['controller_id'] = $newTController->id;         
                         $new_ip_exc = $this->addIPExcludedDirect($data);     
                        
                         if(!$new_ip_exc){                              
                            
                              $error = true;
                         
                              $data_error = new \stdClass; 
                              $data_error->data = $data;               

                              $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                                   'msg' => __('No se pudo crear la ip excluida'),
                                   'data' => $data_error,
                                   'flash' => false
                              ]);

                              $this->_ctrl->getEventManager()->dispatch($event);

                              break;
                         }
                         $newTController->ip_excluded[] = $new_ip_exc;                                
                   }

                   if(!$error){

                        foreach($tcontroller->profiles as $pr){
                             
                              $parse_profiles[$pr->id] = null;

                              $data =  $pr->toArray();
                              $data['controller_id'] = $newTController->id;         
                              $new_profile = $this->addProfile($data);     
                             
                              if(!$new_profile){
                                   
                                   $error = true;
                         
                                   $data_error = new \stdClass; 
                                   $data_error->data = $data;               

                                   $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                                        'msg' => __('No se pudo crear el (profile)'),
                                        'data' => $data_error,
                                        'flash' => false
                                   ]);

                                   $this->_ctrl->getEventManager()->dispatch($event);

                                   break;                                   
                              }
                             
                              $parse_profiles[$pr->id] = $new_profile->id; 
                             
                              $newTController->profiles[] = $new_profile;                                
                        }

                        if(!$error){

                             foreach($tcontroller->pools as $po){
                                  
                                   $parse_pool[$po->id] = null;

                                   $data =  $po->toArray();
                                   $data['controller_id'] = $newTController->id;                                   
                                   $data['next_pool_id'] = null;
                                  
                                   $new_pool =  $this->addPool($data); 
                                  
                                   if(!$new_pool){
                                        
                                        $error = true;
                         
                                        $data_error = new \stdClass; 
                                        $data_error->data = $data;               

                                        $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                                             'msg' => __('No se pudo crear el (pool)'),
                                             'data' => $data_error,
                                             'flash' => false
                                        ]);

                                        $this->_ctrl->getEventManager()->dispatch($event);

                                        break;
                                   }
                                  
                                   $parse_pool[$po->id] = $new_pool->id;
                                  
                                   $newTController->pools[] = $new_pool;   
                             }

                             if(!$error){

                                  foreach($tcontroller->plans as $pl){

                                        $data =  $pl->toArray();
                                       
                                        $data['controller_id'] = $newTController->id;                                          
                                        $data['profile_id'] = $parse_profiles[$data['profile_id']];
                                       
                                        if($data['pool_id']){
                                             $data['pool_id'] = $parse_pool[$data['pool_id']];
                                        }        
                                       
                                        $data['pool'] = null;
                                        $data['profile'] = null;                                       
                                       
                                        $new_plan = $this->addPlan($data);    
                                       
                                        if(!$new_plan){
                                             
                                             $error = true;
                         
                                             $data_error = new \stdClass; 
                                             $data_error->data = $data;               

                                             $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                                                  'msg' => __('No se pudo crear el plan'),
                                                  'data' => $data_error,
                                                  'flash' => false
                                             ]);

                                             $this->_ctrl->getEventManager()->dispatch($event);

                                             break;
                                        }
                                        $newTController->plans[] = $new_plan;
                                  }                                       
                             }                                  
                        }
                   }        
              }
         }

         if($error){
              
              $this->modelPlans->deleteAll(['controller_id' => $newController->id]);
              $this->modelPools->deleteAll(['controller_id' => $newController->id]);
              $this->modelProfiles->deleteAll(['controller_id' => $newController->id]);
              $this->modelIpExcluded->deleteAll(['controller_id' => $newController->id]);
              $this->modelPppoeServers->deleteAll(['controller_id' => $newController->id]);
              $this->modelControllers->deleteAll(['controller_id' => $newController->id]);
                                                  
              $this->_ctrl->Controllers->delete($newController);     
                                                  
             return false;
              
         }
     
         return true;
     
    }
     

    

        
    //ppp servers
    
    public function getPppoeServers($controller_id){    
       
        return $this->modelPppoeServers->find()
            ->where([
                'controller_id' => $controller_id
                ]);
    }
    
    public function getPppoeServersArray($controller_id){
         
        //busca el objeto en la base del template
        $pppoeservers =  $this->modelPppoeServers->find()
            ->where([
                'controller_id' => $controller_id
                ])
            ->order([
                'service_name' => 'ASC'
                ])
            ->toArray();
        
        $array = [];
     
        foreach($pppoeservers as $pppoeserver){
            $array[$pppoeserver->service_name] = $pppoeserver;
        }
     
        return $array;
     
    }
    
    public function getPppoeServersInController($controller){
        return $this->getPppoeServersApi($controller);
    }
    
    public function addPppoeServer($data){                       
     
        $tPppServer = $this->modelPppoeServers->find()
            ->where([
                'service_name' => $data['service_name'],
                'controller_id' => $data['controller_id'] 
                ])
            ->first();
        
        if($tPppServer){
            
            $data_error = new \stdClass; 
            $data_error->other_ppp_server = $tPppServer;
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                'msg' => __('Ya existe un (ppp server) con el mismo nombre'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
      
            return false;  
        }
   
        $tPppServer = $this->modelPppoeServers->newEntity();
        $tPppServer = $this->modelPppoeServers->patchEntity($tPppServer,$data );
        
        if(!$this->modelPppoeServers->save($tPppServer)){
            
            $data_error = new \stdClass; 
            $data_error->tPppServer = $tPppServer;
            $data_error->errors = $tPppServer->getErrors();
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                'msg' => __('Error al intentar agregra el (ppp server)'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
      
            return false;  
        }
  
        return $tPppServer;
        
    }
    
    public function deletePppoeServerInController($controller, $ppp_server_api_id){
       return $this->deletePppoeServerByApiIdApi($controller, $ppp_server_api_id);
    }
    
    public function deletePppoeServer($id){  
       
        $tPppServer = $this->modelPppoeServers->get($id);
        
        if(!$this->modelPppoeServers->delete($tPppServer)){
            
            $data_error = new \stdClass; 
            $data_error->tPppServer = $tPppServer;
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                'msg' => __('No se pudo eliminar el (ppp server)'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
      
            return false;
        }
      
        return $tPppServer; 
    }
    
    public function syncPppoeServer($id){
    
        $pppServer =  $this->modelPppoeServers->get($id);
        
        $this->_ctrl->loadModel('Controllers');
        $controller = $this->_ctrl->Controllers->get($pppServer->controller_id);

        if(!$this->integrationEnabled($controller)){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                'msg' => __('Intento de sincronizacion de (ppp server). Integracion desactivada.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
   
            return false;  
        }
      
        $pppServer = $this->syncPppoeServerApi($controller, $pppServer);
        
        if(!$pppServer){
            return false;
        }
        
        
        //limpia los api_id que existan duplicados
        $duplicates = $this->modelPppoeServers->find()
        	->where([
        	'api_id' => $pppServer->api_id,
        	'id !=' => $pppServer->id,
        	'controller_id' => $controller->id
        	]);
        
        foreach($duplicates as $duplicated){
        
        	$duplicated->api_id = NULL;
        	$this->modelPppoeServers->save($duplicated);
        }
      
      
        if(!$this->modelPppoeServers->save($pppServer)){
            
            $data_error = new \stdClass; 
            $data_error->pppServer = $pppServer;
            $data_error->errors = $pppServer->getErrors();
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                'msg' => __('Error al intentar actualizar el (ppp server) en la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
   
            return false;  
        }
       
        return $pppServer;  
        
    }
    
    
    
    
    //profiles
    
    public function getProfiles($controller_id){                            
       
        $profiles = $this->modelProfiles->find()
            ->where([
                'controller_id' => $controller_id
                ]);
        
        foreach($profiles as $profile){
            
            $profile->connections_amount = $this->modelQueues->find()
                ->where([
                    'profile_id' => $profile->id,
                    'controller_id' => $profile->controller_id
                    ])
                ->count();
        }
    
        return $profiles;
    
    }
    
    public function getProfilesArray($controller_id){
        
        //busca el objeto en la base del template
        $profiles =  $this->modelProfiles->find()
            ->where([
                'controller_id' => $controller_id
                ]);
        
        $profilesArray = [];
        foreach ($profiles as $profile) {
            
            $profilesArray[$profile->name] = $profile;
        }
        
        return $profilesArray;
    
    }
    
    public function getProfilesInController($controller){
        return $this->getProfilesApi($controller);
    }
    
    public function addProfile($data){        
        
        $profile = $this->modelProfiles->find()
            ->where([
                'name' => $data['name'],
                'controller_id' => $data['controller_id']
                ])
            ->first();
        
        if($profile){
            
            $data_error = new \stdClass; 
            $data_error->other_profile = $profile;
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
            'msg' => __('Ya existe un (profile) con el mismo nombre en este controlador.'),
            'data' => $data_error,
            'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
          
            return false;
        }
   
        $profile = $this->modelProfiles->newEntity();
        $profile = $this->modelProfiles->patchEntity($profile, $data );
        
        if(!$this->modelProfiles->save($profile)){
            
            $data_error = new \stdClass; 
            $data_error->profile = $profile;
            $data_error->errors = $profile->getErrors();
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                'msg' => __('Error al intentar agregra el (profile) a la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
           
            return false;
        }
   
        return $profile;
        
    }
    
    public function deleteProfile($id){        
        
        $count = $this->modelPlans->find()
            ->where([
                'profile_id' => $id
                ])
            ->count();
        
        if($count > 0){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                'msg' => __('No se puede eliminar. Existe {0} Plan(es) relacionado(s) a este  (profile).', $count),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
        $count = $this->modelQueues->find()
            ->where([
                'profile_id' => $id
            ])
            ->count();
        
        if($count > 0){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                'msg' => __('No se puede eliminar. Existe {0} Queue(s) relacionado(s) a este  (profile).', $count),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
        $profile = $this->modelProfiles->get($id);
        
        if(!$this->modelProfiles->delete($profile)){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                'msg' => __('Error al intentar eliminar el (profile) de la base de datos'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
      
        return $profile;
    }
    
    public function editProfile($data){
    
        $profile = $this->modelProfiles->find()
            ->where([
                'name' => $data['name'],
                'id !=' => $data['id'],
                'controller_id' => $data['controller_id']
                ])
            ->first();
        
        if($profile){
            
            $data_error = new \stdClass;
            $data_error->other_profile = $profile;
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                'msg' => __('Ya existe un (profile) con el mismo nombre'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
   
        $profile = $this->modelProfiles->get($data['id']);
        $profile = $this->modelProfiles->patchEntity($profile, $data );
        
        if(!$this->modelProfiles->save($profile)){
            
            $data_error = new \stdClass;
            $data_error->profile = $profile;
            $data_error->errors =  $profile->getErrors();
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                'msg' => __('Ya existe un (profile) con el mismo nombre'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
 
        return $profile;
        
    }
    
    public function deleteProfileInController($controller, $tprofile_api_id){
      return $this->deleteProfilebyApiIdApi($controller, $tprofile_api_id);
    }
    
    public function syncProfile($profile_id){
        
        $profile =  $this->modelProfiles->get($profile_id);
        
        $this->_ctrl->loadModel('Controllers');
        $controller = $this->_ctrl->Controllers->get($profile->controller_id);

        if(!$this->integrationEnabled($controller)){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
            	'msg' => __('Intento de sinconizacion de (profile). La integración esta desactivada.'),
            	'data' => $data_error,
            	'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            return false;  
        }
      
        $profile = $this->syncProfileApi($controller, $profile);
        
        if(!$profile){
            return false;
        }
        
        //limpia los api_id que existan duplicados
        $duplicates = $this->modelProfiles->find()
        	->where([
        	'api_id' => $profile->api_id,
        	'id !=' => $profile->id,
        	'controller_id' => $profile->controller_id
        	]);
        
        foreach($duplicates as $duplicated){
        
        	$duplicated->api_id = NULL;
        	$this->modelProfiles->save($duplicated);
        }
        
      
        if(!$this->modelProfiles->save($profile)){
            
            $data_error = new \stdClass; 
            $data_error->profile = $profile;
            $data_error->errors = $profile->getErrors();
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
            	'msg' => __('Error al intentar actualizar el (profile).'),
            	'data' => $data_error,
            	'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;  
        }
    
        return $profile;  
        
    }
    
    
    
    //pools
    
    public function getPools($controller_id){
         
        $pools = $this->modelPools->find()
            ->where([
                'controller_id' => $controller_id,
                ]);
           
        foreach($pools as $pool){
            
            $pool->connections_amount = $this->modelQueues->find()
                ->where([
                    'pool_id' => $pool->id,
                    'controller_id' => $pool->controller_id
                    ])
                ->count();
            
            if($pool->next_pool_id){
                $pool->next_pool =  $this->modelPools->get($pool->next_pool_id);
            }
        }
       
        return $pools;
        
    }
    
    public function addPool($data){
        
        $tpool = $this->modelPools->find()
            ->where([
                'name' => $data['name'],
                'controller_id' => $data['controller_id']
                ])
            ->first();
        
        if($tpool){
            
            $data_error = new \stdClass; 
            $data_error->other_tpool = $tpool;
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                'msg' => __('Ya existe un (pool) con el mismo nombre en este controlador.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
        
         $tpool = $this->modelPools->find()
            ->where([
                'addresses' => $data['addresses'],
                'controller_id' => $data['controller_id']
                ])
            ->first();
        
        if($tpool){
            
            $data_error = new \stdClass; 
            $data_error->other_tpool = $tpool;
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                'msg' => __('Ya existe un (pool) con la misma red en este controlador.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
   
        $tpool = $this->modelPools->newEntity();
        $tpool = $this->modelPools->patchEntity($tpool, $data );
        
        $tpoolnext_pool = $this->modelPools->find()
            ->where([
                'next_pool_id' => $tpool->next_pool_id
                ])
            ->first();
         
        if($tpoolnext_pool){
          
            $data_error = new \stdClass; 
            $data_error->other_tpool = $tpoolnext_pool;
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                'msg' => __('El (pool) siguiente ya esta usando. '),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
        
        
        if(!$this->modelPools->save($tpool)){
            
            $data_error = new \stdClass; 
            $data_error->tpool = $tpool;
            $data_error->errors = $tpool->getErrors();
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                'msg' => __('Error al intentar agregar el (pool) en al base de datos'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
   
        return $tpool;
        
    }
    
    public function editPool($data){
        
        $pool = $this->modelPools->find()
            ->where([
                'name' => $data['name'],
                'id !=' => $data['id'],
                'controller_id' => $data['controller_id']
                ])
            ->first();
        
        if($pool){
            
            $data_error = new \stdClass; 
            $data_error->other_pool = $pool;
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                'msg' => __('Ya existe un (pool) con el mismo nombre en este controlador'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
        
        $pool = $this->modelPools->find()
            ->where([
                'addresses' => $data['addresses'],
                'id !=' => $data['id'],
                'controller_id' => $data['controller_id']
                ])
            ->first();
        
        if($pool){
            
            $data_error = new \stdClass; 
            $data_error->other_pool = $pool;
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                'msg' => __('Ya existe un (pool) con la misma red en este controlador'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
        
        $pool = $this->modelPools->get($data['id']);
         
        if($pool->id != $data['addresses']){
             
             if($this->validatePoolUsed($pool)){
             
                 $data_error = new \stdClass; 

                 $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                     'msg' => __('La red de este (pool) no puede ser editado por que existen elementos relacionados'),
                     'data' => $data_error,
                     'flash' => false
                 ]);

                 $this->_ctrl->getEventManager()->dispatch($event);

                 return false;
             }             
        }       
                
        
        
        $pool_old = $pool;
      
        $pool = $this->modelPools->patchEntity($pool, $data );
        
        $pool_next_pool = $this->modelPools->find()
            ->where([
                'next_pool_id' => $pool->next_pool_id,
                'id != ' => $pool->id,
                'controller_id' => $data['controller_id'],
                ])
            ->first();
         
        if($pool_next_pool){
          
            $data_error = new \stdClass; 
            $data_error->other_tpool = $pool_next_pool;
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                'msg' => __('El (pool) siguiente ya esta usando.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
        
       
        if(!$this->modelPools->save($pool)){
            
            $data_error = new \stdClass; 
            $data_error->pool = $pool;
            $data_error->errors = $pool->getErrors();
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                'msg' => __('Error al intentar editar el (pool) en al base de datos'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
       

        return $pool;
        
    }     
   
    public function deletePool($id){
        
        $count = $this->modelPlans->find()->where(['pool_id' => $id])->count();
        
        if($count > 0){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                'msg' => __('No se puede eliminar. Existe {0} Plan(es) relacionado(s) a este (pool).', $count),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
            
        }
         
        $pool = $this->modelPools->get($id);
        
        if( $this->validatePoolUsed($pool)){
             
            $data_error = new \stdClass; 
        
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                'msg' => __('La red de este (pool) no puede ser eliminada por que existen elementos relacionados'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
             
        }     
        
    
        $pool_next = $this->modelPools->find()->where(['next_pool_id' => $pool->id])->first();
        
        if($pool_next){
            
            $data_error = new \stdClass; 
            $data_error->other_pool = $pool_next;
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                'msg' => __('No se puede eliminar por que es (pool) siguiente del (pool): {0}', $pool_next->name),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
        
        if(!$this->modelPools->delete($pool)){
            
            $data_error = new \stdClass; 
            $data_error->pool = $pool;
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                'msg' => __('Error al intentar eliminar el (pool) de la base de datos'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
      
        return $pool;
    }
  
    protected function validatePoolUsed($pool){
        
        $min_host = $pool->min_host;
        $max_host = $pool->max_host;
      
        $queue = $this->modelQueues->find()->where([
                'target >= ' => ip2long($min_host),
                'target < ' => ip2long(Cidr::nextIp($max_host)),
                'controller_id' => $pool->controller_id
            ])->first();
            
        if($queue){
            return true;
        }
        return false;
    } 
   
    public function movePool($data) {
        
        $this->_ctrl->loadModel('Controllers');
        $this->_ctrl->loadModel('Connections');
        $this->_ctrl->loadModel('FirewallAddressLists');
        $this->_ctrl->loadModel('ConnectionsHasMessageTemplates');        
        
        $pool_from = $this->modelPools->get($data['pool_from_id']);
        $pool_to = $this->modelPools->get($data['pool_to_id']);
        
        $controller = $this->_ctrl->Controllers->get($pool_from->controller_id);
        
        $queuesFix = $this->modelQueues->find()->where(['pool_id' => $pool_from->id]);
         
         $this->_ctrl->log($queuesFix, 'debug');
        
        foreach($queuesFix as $queue){
            
            //busco ip libre
            $ip_data = $this->getFreeIPByPoolId($controller, $pool_to->id);
            
            if(!$ip_data){
                return false;
            }
                
                
            //connection
            $connection = $this->_ctrl->Connections->get($queue->connection_id);
            $connection->ip = $ip_data['ip'];
            $connection->diff = true;
            $this->_ctrl->Connections->save($connection);
                
                
            //queue
            $queue->target = ip2long($connection->ip);
            $queue->pool_id = $ip_data['pool_id'];
            $this->modelQueues->save($queue);
            
            //secrets
            $secret = $this->modelSecrets->find()->where(['connection_id' => $queue->connection_id])->first();
            $secret->remote_address = ip2long($connection->ip);
            $this->modelSecrets->save($secret);   
            
            
            //FirewallAddressLists
            $this->_ctrl->FirewallAddressLists->updateAll(
                [  
                    'address' => $connection->ip 
                ],
                [  
                    'connection_id' => $connection->id
                ]
            );
            
            //ConnectionsHasMessageTemplates
            $this->_ctrl->ConnectionsHasMessageTemplates->updateAll(
                [  
                    'ip' => $connection->ip 
                ],
                [  
                    'connections_id' => $connection->id
                ]
            );
        }
        
        return true;
        
    }
    
    
    
    
    //ip free
    
    protected function getIpPool($tpool_id){
         
        //ips de pools
        /**
         * $ips_pool = [
         *      ip long => pool_id
         * 
         *      168430081 => 1,
         *      168430075 => 1,
         * 
         *      168440020 => 2,
         * ]
        */
        
        $ips_pool = [];
        
        do{
            $pool = $this->modelPools->get($tpool_id);
            
            $ips = range(ip2long($pool->min_host), ip2long($pool->max_host));
            $ips = array_fill_keys($ips, $pool->id);
            $ips_pool += $ips;
           
            $tpool_id = $pool->next_pool_id;            
      
            
        }while($tpool_id);

        return $ips_pool;
        
    }
    
    protected function getIpBusy($tpool_id){
        
        //ips usadas
        /**
         * $ips_busy = [
         *      ip long => pool_id
         * 
         *      168430081 => 1,
         *      168430075 => 1,
         * 
         *      168440020 => 2,
         * ]
        */
        
        $ips_busy = [];
        
        do{
            
            $pool = $this->modelPools->get($tpool_id);
            
            $ips = $this->modelQueues->find('list', [
                    'keyField' => 'target',
                    'valueField' => 'pool_id'
                ])
                ->where([
                    'pool_id' => $pool->id,
                    'controller_id' => $pool->controller_id
                    ])
                ->toArray();
       
            $ips_busy += $ips;
       
            $tpool_id = $pool->next_pool_id;
            
        }while($tpool_id);
      
        return $ips_busy;
        
    }
    
    public function getFreeIPByPoolId($controller, $tpool_id){
        
        $ips_pool = $this->getIpPool($tpool_id);
        
        $ips_busy = $this->getIpBusy($tpool_id);
        
        foreach($ips_pool as  $ip => $pool_id){
            if((!$this->validateIPExcluded($controller->id, $ip) && !array_key_exists($ip, $ips_busy))){
                return ['ip' => long2ip($ip), 'pool_id' => $pool_id];
            }
        }

        return false;
    
    }
   
    protected function validateIPByController($controller, $ip){
         
        $pool_selected = null;
        
        
        //valido que la ip no este usada en alguna queue
        
        $tqueue = $this->modelQueues->find()->where([
            'target' => ip2long($ip),
            'controller_id' => $controller->id
        ])->first();
        
        if($tqueue){
        
            $data_error = new \stdClass;
            $data_error->tqueue = $tqueue;
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
            'msg' => __('la IP: {0} esta ocupada por otra conexión en este contrador.', $ip),
            'data' => $data_error,
            'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
         
            return false;
        }
        
        
        //valido si la ip no esta en alista de ip excluidas
        
        $exist = $this->validateIPExcluded($controller->id, $ip);
        
        if($exist){
            
            $data_error = new \stdClass; 
            $data_error->ip = $ip;
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                'msg' => __('la IP: {0} se encuentra en la lista de IP Excluidas.', $ip),
                'data' => $data_error,
                'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
        
        //valido que la ip perteneczca alguan pool del controlador
        
        $pools = $this->modelPools->find()->where(['controller_id' => $controller->id]);
        
        $pool_valid = false;
        
        foreach($pools as $pool){
       
            if(Cidr::cidr_match($ip, $pool->addresses)){
                $pool_valid = true;
                $pool_selected = $pool;
                break;
            }
        }
        
        
        if(!$pool_valid){
            
            $data_error = new \stdClass; 
            $data_error->ip = $ip;
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                'msg' => __('la IP: {0} no pertece a ninguna red del controlador.', $ip),
                'data' => $data_error,
                'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
            
        }

        return ($pool_valid) ? ['ip' => $ip, 'pool_id' => $pool_selected->id] : false;
    }
    
    
    
    
    
     //ip excluded
    
    public function getIPExcluded($controller_id){
        
        return $this->modelIpExcluded->find()
            ->where(['controller_id' => $controller_id]);
           
    }

    public function addIPExcludedDirect($data){
        
        $rango = false;
        
        if(strpos($data['ip'], '-') !== false){  //rango
        
            $rango = true;
            
            $temp = explode('-', $data['ip']);
            $ip_from = $temp[0];
            $ip_to = $temp[1];
            
            if(!Cidr::validIP($ip_from) || !Cidr::validIP($ip_to)){
                
                $data_error = new \stdClass; 
                $data_error->ip = $data['ip'];
                
                $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                    'msg' => __('Algunas de las IP ingresada no es valida.'),
                    'data' => $data_error,
                    'flash' => false
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
         
                return false;
                
            } 
            
            $ip_from    = ip2long($temp[0]);
            $ip_to      = ip2long($temp[1]);
            
            if($ip_from >= $ip_to){
                
                $data_error = new \stdClass; 
                $data_error->ip = $data['ip'];
                
                $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                    'msg' => __('{0} >= {1} Rango invalido.', $temp[0], $temp[1] ),
                    'data' => $data_error,
                    'flash' => false
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
         
                return false;
            } 
            
        } else { //no es un rango
            
            if(!Cidr::validIP($data['ip'])){ 
                
                $data_error = new \stdClass; 
                $data_error->ip = $data['ip'];
                
                $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                    'msg' => __('La IP ingresa no es valida.'),
                    'data' => $data_error,
                    'flash' => false
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
         
                return false;
            }
        }
        
        //falta validada si la ip o rango de ip esta dentro de alguin rango ya excluido 
       
        $ipExcuded = $this->modelIpExcluded->find()
            ->where([
                'ip' => $data['ip'],
                'controller_id' => $data['controller_id']
                ])
            ->first();
            
        if($ipExcuded){
            
            $data_error = new \stdClass; 
            $data_error->ip = $data['ip'];
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                'msg' => __('La {0} ya esta excluida es este controlador.', $data['ip']),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
     
            return false;
        }
        
        
        if($rango){
            
            $temp = explode('-', $data['ip']);
            $ip_from    = ip2long($temp[0]);
            $ip_to      = ip2long($temp[1]);
            
            $tqueue = $this->modelQueues->find()
                ->where(function (QueryExpression $exp, Query $q) use ($ip_from, $ip_to) {
                return $exp->between('target', $ip_from, $ip_to);
            })->first();
        
        }else{
            
            $tqueue = $this->modelQueues->find()
                ->where(['target' => ip2long($data['ip']),
                    'controller_id' => $data['controller_id']
                    ])
                ->first();
        }
        
        
        if($tqueue){
            
            $data_error = new \stdClass; 
            $data_error->ip = $data['ip'];
            $data_error->tqueue = $tqueue;
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                'msg' => __('{0} ingresada no esta libre.', $data['ip']),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
     
            return false;
        }
       
       
        $ipExcluded = $this->modelIpExcluded->newEntity();
        
        $ipExcluded = $this->modelIpExcluded->patchEntity($ipExcluded, $data );

        if(!$this->modelIpExcluded->save($ipExcluded)){
            
            $data_error = new \stdClass; 
            $data_error->ipExcluded = $ipExcluded->errors();
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                'msg' => __('Error al intentar agregar la IP Excluida a la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
     
            return false;
        }

        return $ipExcluded;
    }

    public function deleteIPExcludedById($id){
        
        $ipExcluded = $this->modelIpExcluded->get($id);
        
        if(!$this->modelIpExcluded->delete($ipExcluded)){
            
            $data_error = new \stdClass; 
            $data_error->ipExcluded = $ipExcluded;
            $data_error->errors = $ipExcluded->getErrors();
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                'msg' => __('Error al Intentar agreghar la Ip Excluded a la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
      
            return false;
        }
      

        return $ipExcluded;
   
    }
   
    protected function validateIPExcluded($controller_id, $ip){
         
        $ipExcluidasRango = $this->modelIpExcluded->find()
            ->where([
                'ip like' => '%-%',
                'controller_id' => $controller_id
                ]);
        
        foreach($ipExcluidasRango as $ip_e_r){
            
            $temp = explode('-', $ip_e_r->ip);
            $ip_from = ip2long($temp[0]);
            $ip_to = ip2long($temp[1]);
            
            if($ip_from <= $ip){
                if($ip <= $ip_to){
                    return true;
                }
            }
        }
        
        return ($this->modelIpExcluded->find()
            ->where([
                'controller_id' => $controller_id, 
                'ip not like' => '%-%',
                'ip' => long2ip($ip)
                ])->first()) ? true : false;
           
    }
    
    
    
    
    //planes
    
    public function getPlans($controller_id){
        
        $tplans =  $this->modelPlans->find()
            ->contain(['MikrotikPppoeTa_Profiles', 'MikrotikPppoeTa_Pools'])
            ->where([
                'MikrotikPppoeTa_Plans.controller_id' => $controller_id,
                ]);
                
        $this->_ctrl->loadModel('Services');
        
        foreach ($tplans as $plan) {
            
            $plan->connections_amount = $this->modelQueues->find()
                ->where([
                    'plan_id' => $plan->id,
                    'controller_id' => $controller_id
                    ])
                ->count();
                
            $plan->service = $this->_ctrl->Services->get($plan->service_id);
        }
        
        return $tplans;
    }
    
    public function getPlan($controller, $id){
        
        return $this->modelPlans->find()
            ->contain(['MikrotikPppoeTa_Profiles', 'MikrotikPppoeTa_Pools'])
            ->where([
                'MikrotikPppoeTa_Plans.id' => $id,
                ])->first();
    }

    public function addPlan($data){
    
        $tplan = $this->modelPlans->find()
            ->where([
                'service_id' => $data['service_id'],
                'controller_id' => $data['controller_id']
                ])
            ->first();
       
        if($tplan){
            
            $data_error = new \stdClass; 
            $data_error->other_plan = $tplan;
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                'msg' => __('Este servicio ya existe en el controlador'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
   
        $tplan = $this->modelPlans->newEntity();
        $tplan = $this->modelPlans->patchEntity($tplan, $data );
        
        if(!$this->modelPlans->save($tplan)){
            
            $data_error = new \stdClass; 
            $data_error->tplan = $tplan;
            $data_error->errors = $tplan->getErrors();
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                'msg' => __('Error al intentar agregar el Servicio '),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
      
            return false;
        }

        return $tplan;
    }
    
    public function editPlan($data){
        
        $plan = $this->modelPlans->get($data['id']);
        
        $plan->pool_id = $data['pool_id'] ? $data['pool_id'] : null;
       
        if($plan->profile_id != $data['profile_id'] ){ //cambio de profile
        
            $profile = $this->modelProfiles->get($data['profile_id']);
        
            $queuesFixProfile = $this->modelQueues->find()->where([
                'plan_id' => $plan->id,
                'controller_id' => $plan->controller_id
            ]);
            
            $this->_ctrl->loadModel('Connections');
             
            foreach($queuesFixProfile as $queue){
                
                //marco diferencia en conexiones
                $connection = $this->_ctrl->Connections->get($queue->connection_id);
                $connection->diff = true;
                $this->_ctrl->Connections->save($connection);
                
                //edito queue
                $queue->profile_id = $profile->id;
                $this->modelQueues->save($queue);
                
                
                //edito secret
                $secretFix = $this->modelSecrets->find()->where([
                    'connection_id' => $connection->id,
                    'controller_id' => $plan->controller_id
                ])->first();
             
                $secretFix->profile = $profile->name;
                $this->modelSecrets->save($secretFix);
                
            }
            
            //edito plan
            $plan->profile_id = $data['profile_id'];
            
            if(!$this->modelPlans->save($plan)){
            
                $data_error = new \stdClass; 
                $data_error->plan = $plan;
                
                $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                    'msg' => __('Error al intentar editar el Plan.'),
                    'data' => $data_error,
                    'flash' => false
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
       
                return false;
            }
            
        }
        else{
            
             //edito plan
            if(!$this->modelPlans->save($plan)){
            
                $data_error = new \stdClass; 
                $data_error->plan = $plan;
                
                $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                    'msg' => __('Error al intentar editar el Plan.'),
                    'data' => $data_error,
                    'flash' => false
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
       
                return false;
            }
        }
        
        
        return $plan;
        
    }
    
    public function movePlan($data){
        
        /*
        [service_id_destino] => 2
        [controller_id] => 1
        [id] => 1
        */
        
        //plan origen
        $plan_origen = $this->modelPlans->get($data['id']);
        
        //busco el plan que tiene el servicio destino
        $plan_destino = $this->modelPlans->find()->where([
            'service_id' => $data['service_id_destino'],
            'controller_id' => $data['controller_id']]
            )->first();
            
        $this->_ctrl->loadModel('Connections');

        //busco las conexion que tiene el servicio origen
        $connections =  $this->_ctrl->Connections->find()
            ->contain(['Controllers', 'Customers'])
            ->where([
                'Connections.controller_id' => $data['controller_id'],
                'Connections.service_id' => $plan_origen->service_id,
                'Connections.deleted' => false
                ]);
              
    
        //inico el data con valores generales  
        $data__connection = [
            'plan_id' => $plan_destino->id,
            'validate_ip' => '0',
            'pool_id' => ''
        ];
       
                
        //recorro las conexiones que tiene el servicio origen
        foreach($connections as $connection){
            
            //obtengo info completa de la conexion, quuees + secrets
            $connection = $this->getConnection($connection);
            
            if(!$connection){
                $this->_ctrl->log('ERROR: no encontro info completa de la connection_id: ' . $connection->id, 'debug');
                continue;
            }
            
            if(!$connection->queue){
                $this->_ctrl->log('ERROR: no encontro queue de la connection_id: ' . $connection->id, 'debug');
               continue;
            }
            
            if($connection->queue->pool_id){
                
                //la conexion estaba con validar ip, busco ip con la red nueva
                
                $data__connection['validate_ip'] = '1';
                
                $data_find_ip = null;
                
                if($plan_destino->pool_id){
                    
                    //si el plan destino tiene red por defecto, bucco con esa red
                     $data_find_ip = $this->getFreeIPByPoolId($connection->controller, $plan_destino->pool_id);
                     
                     if($data_find_ip){
                         
                         //encontro ip libre
                         $data__connection['ip'] = $data_find_ip['ip'];
                         $data__connection['pool_id'] = $data_find_ip['pool_id'];
                         $data__connection['before_ip'] = $connection->ip;
                         
                     }else{
                         //no encontro ip libre
                        $this->_ctrl->log('no encontro ip libre para connection_id: ' . $connection->id, 'debug');
                        continue;
                     }
                }
                else{
                    //el plan destino no tiene red por defecto,  dejo la mismo ip y red que tenia
                     $data__connection['ip'] = $connection->ip;
                }
              
            }
            else{
                //la conexion no estaba con validar ip, dejo la misma ip que tenia
                $data__connection['ip'] = $connection->ip;
                
            }
            
            //paso valores que no deberian cambiar
            $data__connection['mac'] = $connection->mac;
            $data__connection['name'] = $connection->pppoe_name;
            $data__connection['password'] = $connection->pppoe_pass;
            $data__connection['pppoe_name'] = $connection->queue_name;
            
            /*
            'lat' => '-37.0227',
        	'lng' => '-65.332',
        	'customer_code' => '2',
        	'controller_id' => '1',
        	'plan_id' => '1',
        	'pool_id' => '1',
        	'interface' => '',
        	'ip' => '10.20.30.2',
        	'validate_ip' => '1',
        	'mac' => '',
        	'name' => 'h00002',
        	'password' => 'trzw2',
        	'address' => '1805 Brisa ForkNorth Oren, PA 97818-3413',
        	'area_id' => '1',
        	'clave_wifi' => '',
        	'ports' => '',
        	'ip_alt' => '',
        	'ing_traffic' => '',
        	'queue_name' => 'h00002',
        	'comments' => ''
        	*/
         
            
            if($this->editConnection($connection, $data__connection)){
                $this->_ctrl->log('conexion id: ' . $connection->id . ' EDITADO!', 'debug');
            }else{
                $this->_ctrl->log('conexion id: ' . $connection->id . ' ERROR!', 'debug');
            }
        }
        
        return true;
        
    }
    
    
    
    public function deletePlan($id){
        
        $count = $this->modelQueues->find()->where(['plan_id' => $id])->count();
        
        $plan = $this->modelPlans->get($id);
        
        if($count > 0){
            
            $data_error = new \stdClass; 
            $data_error->plan = $plan;
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                'msg' => __('No se puede eliminar. Existe {0} Queue(s) relacionado(s) a este plan.', $count),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
      
            return false;    
        }
        
       
        
        if(!$this->modelPlans->delete($plan)){
            
            $data_error = new \stdClass; 
            $data_error->tplan = $plan;
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                'msg' => __('Error al intentar eliminar el Plan'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
      
            return false;  
           
        }

        return $plan;
    }
     
    
    
    
    
    
    //queue
    
    protected function addQueue($connection){
         
        $data = [
               'name' => $connection->name,
               'target' => ip2long($connection->ip),
               'comment' => $connection->comment,
               'connection_id' => $connection->id,
               'controller_id' => $connection->controller_id,
               'pool_id' => $connection->pool_id,
               'profile_id' => $connection->profile_id,
               'plan_id' => $connection->plan_id,
               ];
    
    
        $queue = $this->modelQueues->newEntity();
        $queue = $this->modelQueues->patchEntity($queue, $data);
        
        if(!$this->modelQueues->save($queue)){
            
            $data_error = new \stdClass; 
            $data_error->queue = $queue;
            $data_error->errors = $queue->getErrors();
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                'msg' => __('Error al Intentar agregar el Queue en la base de datos.'),
                'data' => $data_error,
                'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
    
        return $queue;
        
    }
    
    protected function deleteQueue($connection){
    
        $connection->queue = $this->modelQueues->find()
            ->where([
                'connection_id' => $connection->id
                ])
            ->first();
        
        if($connection->queue){
            
            if(!$this->modelQueues->delete($connection->queue)){
                
                $data_error = new \stdClass; 
                $data_error->queue = $connection->queue;
                
                $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                	'msg' => __('Error al intentar eliminar el (queue) a la base de datos.'),
                	'data' => $data_error
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
                
                return false;
            }
            
        }else{
            
            $data_error = new \stdClass; 
            $data_error->connection = $connection;
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                'msg' => __('No se encontro el (queue).'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
        }
        
        return $connection;
    }
    
    protected function getQueue($connection){
         
        //busca el objeto en la base del template
        return $this->modelQueues->find()
            ->contain(['MikrotikPppoeTa_Plans', 'MikrotikPppoeTa_Pools', 'MikrotikPppoeTa_Profiles'])
            ->where([
                'MikrotikPppoeTa_Queues.connection_id' => $connection->id,
                'MikrotikPppoeTa_Queues.controller_id' => $connection->controller_id
            ])->first();
    }
    
    protected function editQueue($connection){
         
        
        $queue = $this->modelQueues->find()
            ->where([
                'name' => $connection->queue_name,
                'id !=' => $connection->queue->id, 
                'controller_id' => $connection->controller->id
                ])
            ->first();
        
        if($queue){
            
            $data_error = new \stdClass; 
            $data_error->other_queue = $queue;
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
            	'msg' => __('Ya existe un (queue) con el mismo nombre.'),
            	'data' => $data_error,
            	'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
   
            return false;
        }
       
        $queue = $this->modelQueues->get($connection->queue->id);
        
        
        $data = [
            'name' => $connection->name,
            'target' => ip2long($connection->ip),
            'comment' => $connection->comment,
            'connection_id' => $connection->id,
            'controller_id' => $connection->controller_id,
            'pool_id' => $connection->pool_id,
            'profile_id' => $connection->profile_id,
            'plan_id' => $connection->plan_id,
         ];

        $queue = $this->modelQueues->patchEntity($queue, $data );
        
        $queue->tprofile = $this->modelProfiles->get($queue->profile_id);
        
        if(!$this->modelQueues->save($queue)){
            
            $data_error = new \stdClass; 
            $data_error->queue = $queue;
            $data_error->errors = $queue->getErrors();
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
            	'msg' => __('Error al intentar editar el (queue) de la base de datos.'),
            	'data' => $data_error,
            	'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
    
        return $queue;
        
    }
    
    public function deleteQueueInController($controller, $tqueue_api_id){
       return $this->deletedQueueByApiIdApi($controller, $tqueue_api_id);
    }

    public function getQueuesAndConnectionsArray($controller){
        
        $this->_ctrl->loadModel('Connections');
       
        $queues =  $this->getQueues($controller);
        
        $connections =  $this->_ctrl->Connections->find()
            ->where([
                'controller_id' => $controller->id
                ])
            ->order([
                'customer_code' => 'ASC'
                ]);
        
        $queueArray = [];
        
        foreach($queues as $queue){
            foreach($connections as $connection){
                if($queue->connection_id == $connection->id){
                    $queue->connection = $connection;
                    break;
                }
            }
            
            $queue['target'] = $queue->target . '/32';
            $queue['max_limit'] = $queue->profile->up ? $queue->profile->up .'/'. $queue->profile->down : '0/0';
            $queue['limit_at'] = $queue->profile->up_at_limit ? $queue->profile->up_at_limit .'/'. $queue->profile->down_at_limit : '0/0';
            $queue['burst_limit'] = $queue->profile->up_burst ? $queue->profile->up_burst .'/'. $queue->profile->down_burst : '0/0';
            $queue['burst_threshold'] = $queue->profile->up_threshold ? $queue->profile->up_threshold .'/'. $queue->profile->down_threshold : '0/0';
            $queue['burst_time'] = $queue->profile->up_time ? $queue->profile->up_time .'s/'. $queue->profile->down_time . 's' : '0s/0s';
            $queue['priority'] = $queue->profile->priority ? $queue->profile->priority .'/'. $queue->profile->priority : '8/8';
            $queue['queue'] = $queue->profile->up_queue_type ? $queue->profile->up_queue_type .'/'. $queue->profile->down_queue_type : '';
         
            $queueArray[$queue->name] = $queue;
        }
        
        return $queueArray;

    }
    
    public function getQueuesInController($controller){
        return $this->getQueuesApi($controller);
    }
    
    protected function getQueues($controller){
         
        //busca el objeto en la base del template
        return $this->modelQueues->find()->contain(['MikrotikPppoeTa_Profiles'])
            ->where([
                'MikrotikPppoeTa_Queues.controller_id' => $controller->id
                ])
            ->order([
                'MikrotikPppoeTa_Queues.name' => 'ASC'
                ])
            ->map(function ($row) { 
                $row->target = long2ip($row->target);
                return $row;
            });
    
    }
    
    public function syncQueue($connection){
        
        if(!$this->integrationEnabled($connection->controller)){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
            'msg' => __('Intento de sincronizacion con integracion desabilitada.'),
            'data' => $data_error,
            'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
  
            return false;
        }        
 
            
        $queue =  $this->modelQueues->find()->contain(['MikrotikPppoeTa_Profiles'])
            ->where([
                'MikrotikPppoeTa_Queues.connection_id' => $connection->id,
                'MikrotikPppoeTa_Queues.controller_id' => $connection->controller_id,
                ])
            ->first();
        
        $queue->comment = sprintf("%'.05d",  $connection->customer->code) . ' - ' . $connection->customer->name;
        
        $queue = $this->syncQueueApi($connection->controller, $queue);
        
        if(!$queue){
            return false;
        }
        
        //limpia los api_id que existan duplicados
        $duplicates = $this->modelQueues->find()
        	->where([
        	'api_id' => $queue->api_id,
        	'id !=' => $queue->id,
        	'controller_id' => $connection->controller_id
        	]);
        
        foreach($duplicates as $duplicated){
        
        	$duplicated->api_id = NULL;
        	$this->modelQueues->save($duplicated);
        }
      
        if(!$this->modelQueues->save($queue)){
            
            $data_error = new \stdClass; 
            $data_error->queue = $queue;
            $data_error->errors = $queue->getErrors();
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                'msg' => __('Error al intentar actualizar el (queue) en la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            return false;
        }
        
        return $queue;
        
    }
    
    
    
    
    
    //secrets
    
    protected function addSecret($connection){
        
        $profile = $this->modelProfiles->get($connection->profile_id);
    
        $data = [
            'name' => $connection->name,
            'password' => $connection->password,
            'service' => 'any', // estaba pppoe
            'remote_address' => ip2long($connection->ip),
            'comment' => $connection->comment,
            'connection_id' => $connection->id,
            'controller_id' => $connection->controller_id,
            'profile' => $profile->name,
            'caller_id' => $connection->mac
            ];
    
    
        $secret = $this->modelSecrets->newEntity();
        $secret = $this->modelSecrets->patchEntity($secret, $data);
        
        if(!$this->modelSecrets->save($secret)){
            
            $data_error = new \stdClass; 
            $data_error->secret = $secret;
            $data_error->errors = $secret->getErrors();
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
            	'msg' => __('Error al intentar agregar el secret a la base de datos.'),
            	'data' => $data_error,
            	'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
           
            return false;
        }
    
        return $secret;
        
    }
    
    protected function editSecret($connection){
         
        $secret = $this->modelSecrets->find()
            ->where([
                'name' => $connection->name,
                'id !=' => $connection->secret->id, 
                'controller_id' => $connection->controller->id
                ])
            ->first();
        
        if($secret){
            
            $data_error = new \stdClass; 
            $data_error->other_secret = $secret;
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
            	'msg' => __('Ya existe un (secret) con el mismo User pppoe.'),
            	'data' => $data_error,
            	'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            return false;
        }
        
        $profile = $this->modelProfiles->get($connection->profile_id);
    
        $data = [
            'name' => $connection->name,
            'password' => $connection->password,
            'service' => 'any', // estaba pppoe
            'remote_address' => ip2long($connection->ip),
            'comment' => $connection->comment,
            'connection_id' => $connection->id,
            'controller_id' => $connection->controller_id,
            'profile' => $profile->name,
            'caller_id' => $connection->mac
            ];
        
       
        $secret = $this->modelSecrets->get($connection->secret->id);
        $secret = $this->modelSecrets->patchEntity($secret, $data );
        
        if(!$this->modelSecrets->save($secret)){
            
            $data_error = new \stdClass; 
            $data_error->secret = $secret;
            $data_error->errors = $secret->getErrors();
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
            	'msg' => __('Error al intentar editar el (secret) de la base de datos.'),
            	'data' => $data_error,
            	'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
    
        return $secret;
        
    }
    
    protected function deleteSecret($connection){
         
        $connection->secret = $this->modelSecrets->find()
            ->where([
                'connection_id' => $connection->id
                ])
            ->first();
        
        if($connection->secret){
            
            if(!$this->modelSecrets->delete($connection->secret)){
                
                $data_error = new \stdClass; 
                $data_error->secret = $connection->secret;
                
                $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                	'msg' => __('Error al intentar eliminar el (secret) a la base de datos.'),
                	'data' => $data_error
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
                
                return false;
            }
            
        }else{
            
            $data_error = new \stdClass; 
            $data_error->connection = $connection;
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                'msg' => __('No se encontro el (secret).'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
        }
        
        return $connection;
    }
    
    protected function getSecret($connection){
                 
        //busca el objeto en la base del template
        return $this->modelSecrets->find()
            ->where([
                'connection_id' => $connection->id,
                'controller_id' => $connection->controller_id
            ])->first();
    }
    
    public function deleteSecretInController($controller, $secret_api_id){
       return $this->deleteSecretByApiIdApi($controller, $secret_api_id);
    }
    
    public function getSecretsAndConnectionsArray($controller){
        
        $this->_ctrl->loadModel('Connections');
       
        $secrets =  $this->getSecrets($controller);
        
        $connections =  $this->_ctrl->Connections->find()
            ->where([
                'controller_id' => $controller->id
                ])
            ->order([
                'customer_code' => 'ASC'
                ]);
        
        $secretsArray = [];
        
        foreach($secrets as $secret){
            foreach($connections as $connection){
                if($secret->connection_id == $connection->id){
                    $secret->connection = $connection;
                    break;
                }
            }
         
            $secretsArray[$secret->name] = $secret;
        }
        
        return $secretsArray;

    }
    
    public function getSecretsInController($controller){
        return $this->getSecretsApi($controller);
    }
    
    protected function getSecrets($controller){
        
        //busca el objeto en la base del template
        $secrets =  $this->modelSecrets->find()
            ->where([
                'controller_id' => $controller->id
                ])
            ->order([
                'name' => 'ASC'
                ]);
                
        foreach($secrets as $secret){
            
            $queue = $this->modelQueues->find()->contain(['MikrotikPppoeTa_Profiles'])
                ->where([
                    'connection_id' => $secret->connection_id,
                    ])
                ->first();
                
            $secret->caller_id = implode(':',str_split(strtoupper($secret->caller_id),2));
            $secret->profile = $queue->profile;
            $secret->remote_address = long2ip($secret->remote_address);
            
        }
        
        return  $secrets;
    
    }
    
    public function syncSecret($connection){
        
        if(!$this->integrationEnabled($connection->controller)){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
            'msg' => __('Intento de sincronizacion con integracion desabilitada.'),
            'data' => $data_error,
            'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
  
            return false;
        }
                    
        $secret =  $this->modelSecrets->find()
            ->where([
                'connection_id' => $connection->id,
                'controller_id' => $connection->controller_id,
                ])
            ->first();
        
        $secret->comment = sprintf("%'.05d",  $connection->customer->code) . ' - ' . $connection->customer->name;
        
        $queue = $this->modelQueues->find()->contain(['MikrotikPppoeTa_Profiles'])
                ->where([
                    'connection_id' => $secret->connection_id,
                    ])
                ->first();
                
        $secret->profile = $queue->profile->name;
      
        $secret = $this->syncSecretApi($connection->controller, $secret);
        
        if(!$secret){
            return false;
        }
        
        //limpia los api_id que existan duplicados
        $duplicates = $this->modelSecrets->find()
        	->where([
        	'api_id' => $secret->api_id,
        	'id !=' => $secret->id,
        	'controller_id' => $connection->controller_id
        	]);
        
        foreach($duplicates as $duplicated){
        
        	$duplicated->api_id = NULL;
        	$this->modelSecrets->save($duplicated);
        }
      
        if(!$this->modelSecrets->save($secret)){
            
            $data_error = new \stdClass; 
            $data_error->secret = $secret;
            $data_error->errors = $secret->getErrors();
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                'msg' => __('Error al intentar actualizar el (secret) en la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            return false;
        }
        
        return $connection;
        
    }
    
    protected function generateSecretCredentials($connection){
         
        if($connection->name != '' && $connection->password != ''){ //viene desde una eciiona de controlador
             
            $connection->pppoe_name = $connection->name;
            $connection->pppoe_pass = $connection->password;
            $connection->queue_name = $connection->pppoe_name;
        }
        else{
             
             $paraments = $this->_ctrl->request->getSession()->read('paraments');

             if($paraments->connection->pppoe_credential){ // si esta habilitado la generacion automatica

                 $internal_system_id =  Configure::read('project.internal_system_id');
                 $connection->name = 'h' . $internal_system_id . str_pad(dechex($connection->id), 4, '0', STR_PAD_LEFT); 
                 $connection->password = $this->randomPassword(5);
                 $connection->pppoe_name = $connection->name;
                 $connection->pppoe_pass = $connection->password;
                 $connection->queue_name = $connection->pppoe_name;
             }
        }
         
        return $connection;
        
    }
     
    protected function randomPassword($len = 6)
    {
        $alphabet = "abcdefghijklmnopqrstuwxyz0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $len; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
    
    
    
    //actives
    
    public function getActivesInController($controller){
        return $this->getActivesApi($controller);
    }
    
    public function deleteActiveInController($controller, $active_api_id){
       return $this->deleteActiveByApiIdApi($controller, $active_api_id);
    }
    
    
    
    
    //address list
    
    protected function editAddressList($connection){
            
        $this->_ctrl->loadModel('FirewallAddressLists');
        
        $firewallAddressList = $this->_ctrl->FirewallAddressLists->find()
            ->where([
                'connection_id' => $connection->id,
                'controller_id' => $connection->controller_id
                ])->first();  
                
        if($firewallAddressList){
            
            $firewallAddressList->before_address = $connection->before_ip;
            $firewallAddressList->address = $connection->ip;
            $firewallAddressList->comment = sprintf("%'.05d", $connection->customer->code)  . ' - ' . $connection->customer->name;
            
            if(!$this->_ctrl->FirewallAddressLists->save($firewallAddressList)){
                
                $data_error = new \stdClass; 
                $data_error->firewallAddressList = $firewallAddressList;
                $data_error->errors = $firewallAddressList->getErrors();
                
                $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                	'msg' => __('Error al Intentar editar el (address-list) en la base de datos.'),
                	'data' => $data_error,
                	'flash' => true
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);

                return false;
            }
            
        }
       
        return true;
        
    }
    
    public function syncAddressList($addressListId){
                
    
        $this->_ctrl->loadModel('FirewallAddressLists');
            
        $addressList =  $this->_ctrl->FirewallAddressLists->find()
        ->where([
            'id' => $addressListId
            ])
        ->first();
      
        $this->_ctrl->loadModel('Controllers');
        $controller = $this->_ctrl->Controllers->get($addressList->controller_id);

        if(!$this->integrationEnabled($controller)){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
            'msg' => __('Intento de sincronizacion con integracion desabilitada.'),
            'data' => $data_error,
            'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
  
            return false;
        }
        
        $addressList = $this->syncAddressListApi($controller, $addressList);
            
        if(!$addressList){
            return false;
        }
        
        //limpia los api_id que existan duplicados
        $duplicates = $this->_ctrl->FirewallAddressLists->find()
        	->where([
        	'api_id' => $addressList->api_id,
        	'id !=' => $addressList->id,
        	'controller_id' => $addressList->controller_id
        	]);
        
        foreach($duplicates as $duplicated){
        
        	$duplicated->api_id = NULL;
        	$this->_ctrl->FirewallAddressLists->save($duplicated);
        }
      
        if(!$this->_ctrl->FirewallAddressLists->save($addressList)){
            
            $data_error = new \stdClass; 
            $data_error->addressList = $addressList;
            $data_error->errors = $addressList->getErrors();
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                'msg' => __('Error al intentar actualizar el (address-list) en la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
        return $addressList;
        
    }
    
    protected function syncAddressListsCorteByConnection($connection){
        
        if(!$this->integrationEnabled($connection->controller)){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
            'msg' => __('integracion deshabilitada.'),
            'data' => $data_error,
            'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
  
            return false;
        }
        
        $this->_ctrl->loadModel('FirewallAddressLists');
      
        $addressListCorte = $this->_ctrl->FirewallAddressLists->find()
            ->where([
                'connection_id' => $connection->id,
                'list' => 'Cortes', 
                'controller_id' => $connection->controller_id
                ])
            ->first();
        
        if(!$addressListCorte){ //no hay nada para sync
            return true;
        }
        
        $addressListCorte->comment = sprintf("%'.05d", $connection->customer->code)  . ' - ' . $connection->customer->name;
   
        $addressListCorte = $this->syncAddressListApi($connection->controller, $addressListCorte);
        
        if(!$addressListCorte){ // no salio bien la sync en el router
            return false;
        }
        
        //limpia los api_id que existan duplicados
        
        $duplicates = $this->_ctrl->FirewallAddressLists->find()
        	->where([
        	'api_id' => $addressListCorte->api_id,
        	'id !=' => $addressListCorte->id,
        	'controller_id' => $connection->controller_id
        	]);
        
        foreach($duplicates as $duplicated){
        
        	$duplicated->api_id = NULL;
        	$this->_ctrl->FirewallAddressLists->save($duplicated);
        }
        
        
        if(!$this->_ctrl->FirewallAddressLists->save($addressListCorte)){
            
            $data_error = new \stdClass; 
            $data_error->addressListCorte = $addressListCorte;
            $data_error->errors = $addressListCorte->getErrors();
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                'msg' => __('Error al Intentar actualizar el (address-list) de Corte en la base de datos .'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
        return $addressListCorte;
    }
    
    protected function syncAddressListsAvisoByConnection($connection){
        
        if(!$this->integrationEnabled($connection->controller)){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
            'msg' => __('integracion deshabilitada.'),
            'data' => $data_error,
            'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
  
            return false;
        }
 
        $this->_ctrl->loadModel('FirewallAddressLists');
        
        $addressListAviso = $this->_ctrl->FirewallAddressLists->find()
            ->where([
                'connection_id' => $connection->id,
                'list' => 'Avisos', 
                'controller_id' => $connection->controller_id
                ])
            ->first();
        
        if(!$addressListAviso){ // no hay nada para sync
             return true;
        }
            
        $addressListAviso->comment = sprintf("%'.05d", $connection->customer->code)  . ' - ' . $connection->customer->name;
       
        $addressListAviso = $this->syncAddressListApi($connection->controller, $addressListAviso);
        
        if(!$addressListAviso){ // no salio bien la sync en el router
            return false;
        }
        
        //limpia los api_id que existan duplicados
        $duplicates = $this->_ctrl->FirewallAddressLists->find()
        	->where([
        	'api_id' => $addressListAviso->api_id,
        	'id !=' => $addressListAviso->id,
        	'controller_id' => $connection->controller_id
        	]);
        
        foreach($duplicates as $duplicated){
        
        	$duplicated->api_id = NULL;
        	$this->_ctrl->FirewallAddressLists->save($duplicated);
        }
        
        if(!$this->_ctrl->FirewallAddressLists->save($addressListAviso)){
            
            $data_error = new \stdClass; 
            $data_error->addressListAviso = $addressListAviso;
            $data_error->errors = $addressListAviso->getErrors();
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                'msg' => __('Error al Intentar actualizar el (address-list) de Aviso en la base de datos .'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
            
        
        return $addressListAviso;
    }
    
    public function getAddressListsArray($controller_id){
                            
         $this->_ctrl->loadModel('FirewallAddressLists');
        
        //busca el objeto en la base del template
        $addressLists =  $this->_ctrl->FirewallAddressLists->find()->where(['controller_id' => $controller_id]);
        
        $addressListsArray = [];
        foreach ($addressLists as $addressList) {
            
            $addressListsArray[$addressList->list.':'.$addressList->address] = $addressList;
        }
        
        return $addressListsArray;
    
    }
    
    public function deleteAddressListInController($controller, $address_list_api_id){
       return $this->deleteAddressListByApiIdApi($controller, $address_list_api_id);
    }
    
    public function getAddressListsInController($controller){
        return $this->getAddressListsApi($controller, ['Cortes', 'Avisos']);
    }
    
    
    
    
    
    //connections
   
    public function addConnection($controller, $data) {
        
        $this->_ctrl->loadModel('Connections');
        
        $tplan = $this->getPlan($controller, $data['plan_id']);
        
        $data['user_id'] = $this->_ctrl->Auth->user()['id'];
        $data['controller_id'] = $controller->id;
        $data['service_id'] = $tplan->service_id;
        $data['profile_id'] = $tplan->profile->id;
        $data['mac'] = strtoupper($data['mac']);
        $data['comment'] = sprintf("%'.05d",  $data['customer_code']) . ' - ' . $data['customer_name'];
     
        if(!$data['validate_ip']){
            
            $data['pool_id'] = null;
        }    
     
        $connection = $this->_ctrl->Connections->newEntity();
         
        $connection = $this->_ctrl->Connections->patchEntity($connection, $data);
         
          //para provocar error de base de datos
          //$connection->address = null;
     
        if(!$this->_ctrl->Connections->save($connection)){
            
            $data_error = new \stdClass; 
            $data_error->connection = $connection;
            $data_error->errors = $connection->getErrors();
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                'msg' => __('Error al intentar agregar la conexion a la base de datos.'),
                'data' => $data_error,
                'flash' => true
               ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
        
            return false;
        }
        
        $connection = $this->generateSecretCredentials($connection);
        
        $this->_ctrl->Connections->save($connection);
        
        $queue = $this->addQueue($connection);
        
        if($queue){
            
            if(!$this->addSecret($connection)){
                
                $this->_ctrl->Connections->delete($connection);
                
                $this->deleteQueue($connection);
                
                return false;
            }
        }
        else{
            
            $this->_ctrl->Connections->delete($connection);
            
            return false;
        }
    
        return $this->sync($connection);
    } 
    
    public function getConnection($connection){
        
        $connection->secret = $this->getSecret($connection);
        $connection->queue = $this->getQueue($connection);
      
        return $connection;
    }
     
    public function deleteConnection($connection){
        
        $connection->diff = false;
        
        if(!$connection->enabled){
            $connection = $this->enableConnection($connection);
            if(!$connection){
                return false;
            }
        }
        
        $connection = $this->deleteQueue($connection);
        
        if(!$connection){
            return false;
        }
        
        $connection = $this->deleteSecret($connection);
        
        if(!$connection){
            return false;
        }
        
        $connection = $this->removeAvisoConnection($connection);
        
        if(!$connection){
            return false;
        }
        
        return $this->syncDeleteConnection($connection);
    }

    public function editConnection($connection, $data)
    {
        $this->_ctrl->loadModel('Connections');        

        $tplan = $this->getPlan($connection->controller, $data['plan_id']);

        $data['service_id'] = $tplan->service_id;
        $data['profile_id'] = $tplan->profile->id;
        $data['mac'] = strtoupper($data['mac']);
        $data['comment'] = sprintf("%'.05d",  $connection->customer->code) . ' - ' . $connection->customer->name;

        if (!$data['validate_ip']) {

            $data['pool_id'] = null;
        }        

        $data['pppoe_name'] = $data['name'];
        $data['pppoe_pass'] = $data['password'];
        $data['queue_name'] = $data['pppoe_name'];

        $connection = $this->_ctrl->Connections->patchEntity($connection, $data);

        //eedit queue
        $connection->queue = $this->editQueue($connection);
        if (!$connection->queue) {
            return false;
        }

        //edit secret
        $connection->secret = $this->editSecret($connection);
        if (!$connection->secret) {
            return false;
        }

        //edito address list
        if (!$this->editAddressList($connection)) {
            return false;
        }

        //guardo la conexion
        if (!$this->_ctrl->Connections->save($connection)) {

            $data_error = new \stdClass; 
            $data_error->connection = $connection;
            $data_error->errors = $connection->errors();

            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                'msg' => __('Error al intentar editar la conexión en la base de datos.'),
                'data' => $data_error,
                'flash' => true
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);

            return false;
        }

        return $this->sync($connection);
    }

    public function rollbackConnection($connection_id){
        
        $this->_ctrl->loadModel('ConnectionsTransactions');
        $connectionTransaction = $this->_ctrl->ConnectionsTransactions->find()
            ->where([
                'connection_id' => $connection_id
                ])->first();
                
        $connectionRestore = unserialize($connectionTransaction->data);
  
        $connectionArray = json_decode(json_encode($connectionRestore), true);   
        
        $this->_ctrl->loadModel('Connections');
        $connection = $this->_ctrl->Connections->find()->where(['id' => $connection_id])->first();
        
        $tqueue_data = $connectionArray['queue'];
        $tsecret_data = $connectionArray['secret'];
            
          $connectionArray['service'] = null;
          $connectionArray['controller'] = null;
          $connectionArray['customer'] = null;
          $connectionArray['queue'] = null;
          $connectionArray['secret'] = null;

          if(!$connection){            
               $connection = $this->_ctrl->Connections->newEntity();
          }

          $connection = $this->_ctrl->Connections->patchEntity($connection, $connectionArray);

          if(!$this->_ctrl->Connections->save($connection)){
               
               $data_error = new \stdClass; 
               $data_error->errors = $connection->getErrors();

               $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                    'msg' => __('Error al intentar agregar la conexion a la base de datos.'),
                    'data' => $data_error,
                    'flash' => true
               ]);

               $this->_ctrl->getEventManager()->dispatch($event);

               return false;
          }
        
         
          $tqueue_data['profile'] = null;
          $tqueue_data['pool'] = null;
          $tqueue_data['plan'] = null;

          $tQueue = $this->modelQueues->find()->where(['id' =>$tqueue_data['id']])->first();
         
          if(!$tQueue){
               $tQueue = $this->modelQueues->newEntity();             
          }
         
          $tQueue = $this->modelQueues->patchEntity($tQueue, $tqueue_data);

          if(!$this->modelQueues->save($tQueue)){

                $data_error = new \stdClass; 
                $data_error->errors = $tQueue->getErrors();

                $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                    'msg' => __('Error al intentar agregar la Queue a la base de datos.'),
                    'data' => $data_error,
                    'flash' => true
                ]);

                $this->_ctrl->getEventManager()->dispatch($event);

                return false;

          }

    
        $tSecret = $this->modelSecrets->find()->where(['id' => $tsecret_data['id']])->first();
        
        if(!$tSecret){
            $tSecret = $this->modelSecrets->newEntity();             
        }
         
       $tSecret = $this->modelSecrets->patchEntity($tSecret, $tsecret_data);

       if(!$this->modelSecrets->save($tSecret)){

           $data_error = new \stdClass; 
           $data_error->errors = $tQueue->getErrors();

           $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
               'msg' => __('Error al intentar agregar la Secret a la base de datos.'),
               'data' => $data_error,
               'flash' => true
           ]);

           $this->_ctrl->getEventManager()->dispatch($event);

           return false;
       }

        
        $this->sync($connectionRestore);
        
        $this->_ctrl->ConnectionsTransactions->delete($connectionTransaction);
        
        return true;

    }
    
    protected function sync($connection){      
        
        $connection->controller =  $this->_ctrl->Connections->Controllers->get($connection->controller_id);

        if(!$this->integrationEnabled($connection->controller)){
            
            $this->_ctrl->loadModel('Connections');
            $this->_ctrl->Connections->save($connection);
            
            return $connection;
        }

        $connection->customer   =  $this->_ctrl->Connections->Customers->get($connection->customer_code);
        
        return $this->syncConnection($connection);
    
    }
    
    public function syncConnection($connection){
            
        $connection->diff =  false;
    
        if(!$this->syncQueue($connection)) {
            $connection->diff =  true;
        }
        
        if(!$this->syncSecret($connection)) {
            $connection->diff =  true;
        }
        
        if(!$this->syncAddressListsCorteByConnection($connection)){
            $connection->diff =  true;
        }
        
        if(!$this->syncAddressListsAvisoByConnection($connection)){
            $connection->diff =  true;
        }
   
        $this->_ctrl->loadModel('Connections');
        $this->_ctrl->Connections->save($connection);
        
        return $connection;
        
    }
     
    protected function syncDeleteConnection($connection) {      
      
        
        $this->deletedQueueApi($connection->controller, $connection->queue);
        $this->deleteSecretApi($connection->controller, $connection->secret);
        
        if($connection->addressListCorte){
            $this->deleteAddressListApi($connection->controller, $connection->addressListCorte);
        }
        
        if($connection->addressListAviso){
            $this->deleteAddressListApi($connection->controller, $connection->addressListAviso);
        }
        
        return $connection;
    }    
    
    public function addAvisoConnection($connection){
         
        if(!$this->getStatus($connection->controller)){
             return false;
        } 
 
        $this->_ctrl->loadModel('FirewallAddressLists');
        
        $fal = $this->_ctrl->FirewallAddressLists->find()
            ->where([
                'address' => $connection->ip,
                'list' => 'Avisos',
                'controller_id' => $connection->controller_id
                ])
            ->first();
        
        if($fal){
            return true;
        }      
               
        
        $firewallAddressList = $this->_ctrl->FirewallAddressLists->newEntity();
                 
        $data = [
             'list' => 'Avisos',
             'address' => $connection->ip,
             'before_address' => $connection->ip, 
             'comment' => sprintf("%'.05d", $connection->customer->code)  . ' - ' . $connection->customer->name,
             'connection_id' => $connection->id,
             'controller_id' => $connection->controller->id
             ];
             
        $firewallAddressList = $this->_ctrl->FirewallAddressLists->patchEntity($firewallAddressList, $data);
        
        if(!$this->_ctrl->FirewallAddressLists->save($firewallAddressList)){
            
            $data_error = new \stdClass; 
            $data_error->firewallAddressList = $firewallAddressList;
            $data_error->errors = $firewallAddressList->getErrors();
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                'msg' => __('Error al Intentar agregar el (address-list) del Aviso en la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
            
        }
  
        
        return $this->sync($connection);
    }
    
    public function removeAvisoConnection($connection, $sync = false){

        $this->_ctrl->loadModel('FirewallAddressLists');
        
        $connection->addressListAviso = $this->_ctrl->FirewallAddressLists->find()
            ->where([
                'connection_id' => $connection->id,
                'list' => 'Avisos',
                'controller_id' => $connection->controller_id
                ])
            ->first();
        
        if($connection->addressListAviso){
            
            if($this->integrationEnabled($connection->controller) && $sync){  
                
                if(!$this->deleteAddressListApi($connection->controller, $connection->addressListAviso)){
                    $connection->diff = true;
                    $this->_ctrl->loadModel('Connections');
                    $this->_ctrl->Connections->save($connection);
                }
            }
            
            if(!$this->_ctrl->FirewallAddressLists->delete($connection->addressListAviso)){
                
                $data_error = new \stdClass; 
                $data_error->addressListAviso = $connection->addressListAviso;
                
                $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                'msg' => __('Error al Intentar eliminar el (address-list) del Aviso de la base de datos.'),
                'data' => $data_error,
                'flash' => false
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
                
                return false;
            }
            
        }
        
   
        
        return $connection;
    } 
    
    public function disableConnection($connection)
    {
        $this->_ctrl->loadModel('FirewallAddressLists');
        $firewallAddressList = $this->_ctrl->FirewallAddressLists->newEntity();
                 
        $data = [
             'list' => 'Cortes',
             'address' => $connection->ip,
             'before_address' => $connection->ip,
             'comment' => sprintf("%'.05d", $connection->customer->code)  . ' - ' . $connection->customer->name,
             'connection_id' => $connection->id,
             'controller_id' => $connection->controller->id
             ];
             
        $firewallAddressList = $this->_ctrl->FirewallAddressLists->patchEntity($firewallAddressList, $data);
        
        if (!$this->_ctrl->FirewallAddressLists->save($firewallAddressList)) {
            
            $data_error = new \stdClass; 
            $data_error->firewallAddressList = $firewallAddressList;
            $data_error->errors = $firewallAddressList->getErrors();
            
            $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
            	'msg' => __('Error al Intentar agregar el (address-list) de Corte en la base de datos.'),
            	'data' => $data_error,
            	'flash' => true
            ]);
            
            return false;
        }
   
        return $this->sync($connection);
    }

    public function enableConnection($connection, $sync = false)
    {
        $this->_ctrl->loadModel('FirewallAddressLists');
        
        $connection->addressListCorte = $this->_ctrl->FirewallAddressLists->find()
            ->where([
                'connection_id' => $connection->id,
                'list' => 'Cortes', 
                'controller_id' => $connection->controller_id
                ])
            ->first();
        
        if ($connection->addressListCorte) {
            
            if ($this->integrationEnabled($connection->controller) && $sync) {
                                
                if (!$this->deleteAddressListApi($connection->controller, $connection->addressListCorte)) {
                    $connection->diff = true;
                    $this->_ctrl->loadModel('Connections');
                    $this->_ctrl->Connections->save($connection);
                }
            }
            
            if (!$this->_ctrl->FirewallAddressLists->delete($connection->addressListCorte)) {
                
                $data_error = new \stdClass; 
                $data_error->addressList = $connection->addressListCorte;
                $data_error->errors = $connection->addressListCorte->getErrors();
                
                $event = new Event('MikrotikPppoeTaComponent.Error', $this, [
                	'msg' => __('Error al Intentar eliminar el (address-list) de Corte de la base de datos.'),
                	'data' => $data_error,
                	'flash' => true
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
             
                return false;
            }
        
        } else {
            
            $data_error = new \stdClass; 
            $data_error->connection = $connection;
            
            $event = new Event('MikrotikPppoeTaComponent.Warning', $this, [
                'msg' => __('No se encuentra el (address-list).'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
        }

        return $connection;
    }

    //temporal ...
    public function createQueuesFromSecrets($controller_id)
    {
        //busca el objeto en la base del template
        $secrets = $this->modelSecrets->find()
            ->where([
                'controller_id' => $controller_id
            ]);

        foreach ($secrets as $secret) {

            $connection = new \stdClass;
            $connection->name          = $secret->name;
            $connection->ip            = long2ip($secret->remote_address);
            $connection->comment       = $secret->comment;
            $connection->id            = $secret->connection_id;
            $connection->controller_id = $secret->controller_id;
            $connection->pool_id       = $secret->pool_id;
            $connection->profile_id    = $secret->profile_id;
            $connection->plan_id       = $secret->plan_id;

            $this->addQueue($connection);
        }    
    }
    
    //temporal ...
    public function addWebProxyAccessApiTemp($controller, $addressList){
        return $this->addWebProxyAccessApi($controller, $addressList);
    }
}






