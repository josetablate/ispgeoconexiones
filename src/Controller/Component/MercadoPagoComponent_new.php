<?php

namespace App\Controller\Component;

use App\Controller\Component\Common\MyComponent;
use Cake\Controller\ComponentRegistry;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\Event\EventManager;
use Cake\Event\Event;

/**
 * WebServices component
 */
class MercadoPagoComponent extends MyComponent
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    protected $_config = NULL;

    private $_payment_getway;

    public function initialize(array $config)
    {
        parent::initialize($config);
        $payment_getway = $this->_ctrl->request->getSession()->read('payment_getway');
        $this->_payment_getway = $payment_getway;

        $this->initEventManager();
    }

    public function initEventManager()
    {
        EventManager::instance()->on(
            'MercadoPagoComponent.Error',
            function ($event, $msg, $data, $flash = false) {

                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $log->user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Error';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);

                if ($flash) {
                    $this->_ctrl->Flash->error(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'MercadoPagoComponent.Warning',
            function ($event, $msg, $data, $flash = false) {

                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $log->user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Warning';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);

                if ($flash) {
                    $this->_ctrl->Flash->warning(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'MercadoPagoComponent.Log',
            function ($event, $msg, $data) {

            }
        );

        EventManager::instance()->on(
            'MercadoPagoComponent.Notice',
            function ($event, $msg, $data) {

            }
        );
    }

    public function instance() 
    {
        \MercadoPago\MercadoPagoSdk::initialize(); 
        $this->_config = \MercadoPago\MercadoPagoSdk::config(); 
    }

    public function getToken()
    {
        if (empty($this->_config)) {
            $this->instance();
        }
        $mode = $this->_payment_getway->config->mercadopago->mode;
        $access_token = $this->_payment_getway->config->mercadopago->$mode->access_token;
        return $access_token;
    }

    public function post_payments($payment_data)
    {
        $access_token = $this->getToken();
        $this->_mp = new \MP($access_token);
        $payment = $this->_mp->post("/v1/payments", $payment_data);
        return $payment;
    }

    public function search_payment()
    {
        if (empty($this->_config)) {
            $this->instance();
        }

        $access_token = $this->getToken();

        if ($access_token == "") {
            return [];
        }

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api.mercadopago.com/v1/payments/search');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "access_token=$access_token&range=date_created&begin_date=NOW-3DAYS&end_date=NOW&limit=100");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            Log::write('error', 'Error:' . curl_error($ch));
        }
        curl_close ($ch);
        return $result;
    }

    public function get_payments($id)
    {
        $access_token = $this->getToken();
        $this->_mp = new \MP($access_token);
        $payment = $this->_mp->get("/v1/payments/" . $id);
        return $payment;
    }

    public function createPaymentButton($item, $payer, $external_reference)
    {
        if (empty($this->_config)) {
            $this->instance();
        }

        $access_token = $this->getToken();
        $this->_config->set('ACCESS_TOKEN', $access_token);

        $preference = new \MercadoPago\Preference();

        $excluded_payment_types = [];

        foreach ($this->_payment_getway->config->mercadopago->excluded_payment_types as $excluded_payment_type) {
            array_push($excluded_payment_types, [
                "id" => $excluded_payment_type
            ]);
        }

        # Building an item

        $item1 = new \MercadoPago\Item();
        $item1->id = $item->id;
        $item1->title = $item->title; 
        $item1->quantity = 1;
        $item1->unit_price = $item->unit_price;

        $preference->items = array($item1);

        $preference->payment_methods = array(
            "excluded_payment_types" => $excluded_payment_types,
            "installments"           => intval($this->_payment_getway->config->mercadopago->installments)
        );

        $preference->external_reference = $external_reference;

        $preference->save(); # Save the preference and send the HTTP Request to create

        # Return the link to HTML code for button

        return $preference->sandbox_init_point;
    }
}
