<?php
namespace App\Controller\Component\Ticket;

use App\Controller\Component\Common\MyComponent;

use Cake\Controller\ComponentRegistry;
use Cake\I18n\Time;
use FPDF;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Network\Exception\NotFoundException;
use Cake\Event\EventManager;
use Cake\Event\Event;
use App\Controller\Component\Ticket\Pdf\TicketTablePdf;

/**
 * Documents component
 */
class CreateTicketPdf extends MyComponent
{
    const TYPE_TICKET_TABLE = 0;

    protected $_defaultConfig = [];

    private $_data;

    // The other component your component uses
    public $components = [];

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->initEventManager();
    }

    public function initEventManager() 
    {
        EventManager::instance()->on(
            'CreateTicketPdf.Error',
            function($event, $msg, $data, $flash = false) {

                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $log->user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Error';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);

                if ($flash) {
                    $this->_ctrl->Flash->error(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'CreateTicketPdf.Warning',
            function($event, $msg, $data, $flash = false) {

                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $log->user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Warning';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);

                if ($flash) {
                    $this->_ctrl->Flash->warning(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'CreateTicketPdf.Log',
            function($event, $msg, $data) {

            }
        );

        EventManager::instance()->on(
            'CreateTicketPdf.Notice',
            function($event, $msg, $data) {

            }
        );
    }

    public function generate($data, $type, $return = false)
    {
        switch ($type) {

            case CreateTicketPdf::TYPE_TICKET_TABLE:
                return $this->ticket($data, $return);
                break;
        }

        return true;
    }

    private function ticket($data, $return)
    {
        $data->paraments = $this->_ctrl->request->getSession()->read('paraments');
        $data->responsibles = $this->_ctrl->request->getSession()->read('afip_codes')['responsibles'];
        $data->doc_types = $this->_ctrl->request->getSession()->read('afip_codes')['doc_types'];

        $this->_ctrl->loadModel('Areas');
        $areas = $this->_ctrl->Areas
            ->find('list')->toArray();

        $data->list = [];

        foreach ($data->tickets as $ticket) {

            $address = "";
            if (!empty($ticket->connection)) {
                $address = $ticket->connection->address;
            } else {
                if (!empty($ticket->service_pending)) {
                    $address = $ticket->service_pending->address;
                } else if (!empty($ticket->customer)) {
                    $address = $ticket->customer->address;
                }
            }

            $area = "";
            if ($ticket->area_id != NULL) {
                $area = $areas[$ticket->area_id];
            }

            $description = html_entity_decode($ticket->description);
            $description = strip_tags($description);

            $asigne_user = "";
            if (!empty($ticket->asigned_user_id)) {
                if (isset($ticket->asigned_user)) {
                    $asigne_user = $ticket->asigned_user->name;
                } else {
                    $asigne_user = $ticket->asigned_user_id->name;
                }
            }

            $d = [
                '#'           => $ticket->id,
                'inicio'      => $ticket->start_task,
                'estado'      => $ticket->status->name,
                'categoria'   => $ticket->category->name,
                'cliente'     => empty ($ticket->customer) ? '' : str_pad($ticket->customer->code, 5, "0", STR_PAD_LEFT) . ' ' . $ticket->customer->name,
                'domicilio'   => $address . ' - ' . $area,
                'tel'         => empty ($ticket->customer) ? '' : $ticket->customer->phone,
                'asignado'    => $asigne_user,
                'descripcion' => $description,
                'firma'       => ''
            ];

            $data->list[$ticket->id] = $d;
        }

        ksort($data->list);

        if ($return) {
            return $data;
        }

        $this->response = $this->response->withCharset('UTF-8');
        $this->response = $this->response->withType('application/pdf');

        $ticket = new TicketTablePdf();
        return $ticket->generate($data, $this->_ctrl);
    }
}
