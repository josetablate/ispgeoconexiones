<?php
namespace App\Controller\Component\Ticket\Pdf;

use FPDF;
use Cake\I18n\Time;

class TicketTablePdf extends FPDF {

    public $data;
    public $crtl;
    public $pack_concepts;
    public $saldo;
    public $_border = 0;
    public $font;

	function Header() {

        $this->SetTitle('TICKETS');

		$color = explode(',', $this->data->paraments->invoicing->business[0]->color->rgb);

		$this->SetFillColor($color[0], $color[1], $color[2]);
		$this->SetDrawColor($color[0], $color[1], $color[2]);

		$this->SetFont($this->fontB, '', 14);

		$this->SetXY(10, 15);
		$this->Cell(30, 5, 'TICKETS', $this->_border, 0, 'L');

        $this->SetXY($this->GetPageWidth() - 60, 15);

        $this->Cell(20, 5, 'FECHA:', $this->_border, 0, 'L');
        $this->Cell(30, 5, Time::now()->format('d/m/Y'), $this->_border , 1, 'R');
	}

	function THead($x, $y) {

        $tam = 7;
	    $color = explode(',', $this->data->paraments->invoicing->business[0]->color->rgb);

		$this->SetFillColor($color[0], $color[1], $color[2]);
		$this->SetTextColor(255);
		$this->SetDrawColor(123, 122, 122);

        $this->SetFont($this->fontB, '', 11);

		$w = array(10, 25, 18, 25, 39, 40, 18, 50, 25, 27);

		$this->SetXY($x, $y);

        $this->Cell($w[0], $tam, utf8_decode('#'), 1, 0, 'C', true);
        $this->Cell($w[1], $tam, utf8_decode('Inicio'), 1, 0, 'C', true);
        $this->Cell($w[2], $tam, utf8_decode('Estado'), 1, 0, 'C', true);
        $this->Cell($w[3], $tam, utf8_decode('Categoría'), 1, 0, 'C', true);
        $this->Cell($w[4], $tam, utf8_decode('Cliente'), 1, 0, 'C', true);
        $this->Cell($w[5], $tam, utf8_decode('Dom. - Área'), 1, 0, 'C', true);
        $this->Cell($w[6], $tam, utf8_decode('Tel.'), 1, 0, 'C', true);
        $this->Cell($w[7], $tam, utf8_decode('Descrip.'), 1, 0, 'C', true);
        $this->Cell($w[8], $tam, utf8_decode('Asignado'), 1, 0, 'C', true);
        $this->Cell($w[9], $tam, utf8_decode('Firma'), 1, 0, 'C', true);
	}

	function Footer() {

		global $AliasNbPages;
		$this->SetY(-20);
     	$this->SetFont($this->font, '', 11);
		$this->Cell(0, 10, utf8_decode('Página ') . $this->PageNo() . ' de {nb}', 0, 0, 'C');
	}

    function parseFormarDatetimeJSToPHP($string) {

        $change_start_task = "";
        if ($string != '') {
            $change_start_task_array = explode("T", $string);
            $change_start_task = explode("-", $change_start_task_array[0]);

            $colita = explode("-", $change_start_task_array[1]);
            $colita = explode(":", $colita[0]);
            $colita = $colita[0] . ':' . $colita[1];

            $change_start_task = $change_start_task[2] . '/' . $change_start_task[1] . '/' . $change_start_task[0] . ' ' . $colita; 
        }

        return $change_start_task;
    } 

    function myCell($w, $h, $x, $t, $max_char = 15, $aling = 'L') {

        $height = $h / 4;
        $first = $height + 2;
        $second = $height + $height + $height + 3;
        $third = $height + $height + $height + $height + $height + 4;
        $len = strlen($t);

        if ($len > $max_char) {

            $txt = str_split($t, $max_char);
            $this->SetX($x);
            $this->Cell($w, $first, $txt[0], '', '', $aling);
            $this->SetX($x);
            $this->Cell($w, $second, $txt[1], '', '', $aling);

            if (count($txt) > 2) {
                $this->SetX($x);

                if (count($txt) > 3) {
                    $this->Cell($w, $third, $txt[2] . '...', '', '', $aling);
                } else {
                    $this->Cell($w, $third, $txt[2], '', '', $aling);
                }
            }

            $this->SetX($x);
            $this->Cell($w, $h, '', 'LTRB', 0 , $aling,0);

        } else {

            $this->SetX($x);
            $this->Cell($w, $h, $t, 'LTRB', 0 ,$aling, 0);
        }
    }

	function AddTickets($tickets, $x, $y) {

	    $this->SetFont($this->font, '', 11);

    	$this->SetTextColor(0);

		$w = array(10, 25, 18, 25, 39, 40, 18, 50, 25, 27);

		$this->SetXY($x, $y);
		$heightCell = 13;

        foreach ($tickets as $key => $ticket) {

            $this->myCell($w[0], $heightCell, $this->GetX(), $ticket['#'], 5, 'C');
            $this->myCell($w[1], $heightCell, $this->GetX(), $this->parseFormarDatetimeJSToPHP($ticket['inicio']), 16, 'C');
            $this->myCell($w[2], $heightCell, $this->GetX(), utf8_decode($ticket['estado']), 10, 'C');
            $this->myCell($w[3], $heightCell, $this->GetX(), utf8_decode($ticket['categoria']), 15, 'C');
            $this->myCell($w[4], $heightCell, $this->GetX(), utf8_decode($ticket['cliente']), 23, 'C');
            $this->myCell($w[5], $heightCell, $this->GetX(), utf8_decode($ticket['domicilio']), 23, 'C' );
            $this->myCell($w[6], $heightCell, $this->GetX(), utf8_decode($ticket['tel']), 11, 'C');
            $this->myCell($w[7], $heightCell, $this->GetX(), utf8_decode($ticket['descripcion']), 27, 'C' );
            $this->myCell($w[8], $heightCell, $this->GetX(), utf8_decode($ticket['asignado']), 15, 'C');
            $this->myCell($w[9], $heightCell, $this->GetX(), '', 14, 'C');

            $this->Ln();
        }
	}

	function Generate($data, $ctrl, $format = 'tickets.pdf') {

	    $limit_per_page = 11;

	    $this->data = $data;
	    $this->ctrl = $ctrl;

        $this->SetCompression(false);

        $this->AddFont('OpenSans-CondBold', '', 'OpenSans-CondBold.php');
        $this->AddFont('OpenSans-CondensedLight', '', 'OpenSans-CondLight.php');

        $this->font = 'OpenSans-CondensedLight';
	    $this->fontB = 'OpenSans-CondBold';

		$this->AliasNbPages();

	    $packs = array_chunk($this->data->list, $limit_per_page);

	    foreach ($packs as $tickets) {
	        $this->AddPage('L');
	        $this->THead(10, 24);
	        $this->AddTickets($tickets, 10, 31);
	    }

		return $this->Output('I', $format);
	}
}
