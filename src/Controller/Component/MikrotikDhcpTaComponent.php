<?php

namespace App\Controller\Component;


use App\Controller\Component\Integrations\MikrotikDhcp;
// use Cake\Controller\ComponentRegistry;

use Cidr;
// use Cake\I18n\Time;
use Cake\Event\EventManager;
use Cake\Event\Event;

use Cake\Core\Configure;

use Cake\ORM\TableRegistry;
use Cake\ORM\Locator\TableLocator;

use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Query;



/**
 * Integration component
 */
class MikrotikDhcpTaComponent extends MikrotikDhcp
{

    public function initialize(array $config) {  
         
        parent::initialize($config);
         
        $this->initEventManager();         
        
        $this->templateTableRegistry();
        
    }
    
    protected function initEventManager(){
         
         parent::initEventManager();
        
        EventManager::instance()->on(
            'MikrotikDhcpTaComponent.Error',
            function($event, $msg, $data, $flash = false){
                
                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $log->user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Error';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);
                
                if($flash){
                    $this->_ctrl->Flash->error(__($msg));
                }
            }
        );
        
        EventManager::instance()->on(
            'MikrotikDhcpTaComponent.Warning',
            function($event, $msg, $data, $flash = false){
                
                $this->_ctrl->loadModel('ErrorLog');
                $log = $this->_ctrl->ErrorLog->newEntity();
                $log->user_id = $this->_ctrl->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Warning';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->_ctrl->ErrorLog->save($log);
                
                if($flash){
                    $this->_ctrl->Flash->warning(__($msg));
                }
            }
        );
        
        EventManager::instance()->on(
            'MikrotikDhcpTaComponent.Log',
            function($event, $msg, $data){
                
            }
        );
        
        EventManager::instance()->on(
            'MikrotikDhcpTaComponent.Notice',
            function($event, $msg, $data){
             
            }
        );
    }
     
    protected function templateTableRegistry(){
         
          if(!TableRegistry::getTableLocator()->exists('MikrotikDhcpTa_Controllers')){
               
               TableRegistry::getTableLocator()->setConfig('MikrotikDhcpTa_Controllers', [
                 'className' => 'App\Model\Table\MikrotikDhcpTa\ControllersTable'
               ]);
          }
         
         if(!TableRegistry::getTableLocator()->exists('MikrotikDhcpTa_IpExcluded')){
              
               TableRegistry::getTableLocator()->setConfig('MikrotikDhcpTa_IpExcluded', [
                 'className' => 'App\Model\Table\MikrotikDhcpTa\IpExcludedTable'
               ]);               
         }
         
         if(!TableRegistry::getTableLocator()->exists('MikrotikDhcpTa_Plans')){
              
               TableRegistry::getTableLocator()->setConfig('MikrotikDhcpTa_Plans', [
                 'className' => 'App\Model\Table\MikrotikDhcpTa\PlansTable'
               ]);               
         }
         
         if(!TableRegistry::getTableLocator()->exists('MikrotikDhcpTa_Pools')){
              
               TableRegistry::getTableLocator()->setConfig('MikrotikDhcpTa_Pools', [
                 'className' => 'App\Model\Table\MikrotikDhcpTa\PoolsTable'
               ]);               
         }
         
         
         if(!TableRegistry::getTableLocator()->exists('MikrotikDhcpTa_DhcpServers')){
              
               TableRegistry::getTableLocator()->setConfig('MikrotikDhcpTa_DhcpServers', [
                 'className' => 'App\Model\Table\MikrotikDhcpTa\DhcpServersTable'
               ]);               
         }
         
         if(!TableRegistry::getTableLocator()->exists('MikrotikDhcpTa_Profiles')){
              
               TableRegistry::getTableLocator()->setConfig('MikrotikDhcpTa_Profiles', [
                 'className' => 'App\Model\Table\MikrotikDhcpTa\ProfilesTable'
               ]);               
         }
         
         if(!TableRegistry::getTableLocator()->exists('MikrotikDhcpTa_Queues')){
              
               TableRegistry::getTableLocator()->setConfig('MikrotikDhcpTa_Queues', [
                 'className' => 'App\Model\Table\MikrotikDhcpTa\QueuesTable'
               ]);               
         }
         
          if(!TableRegistry::getTableLocator()->exists('MikrotikDhcpTa_Leases')){
              
               TableRegistry::getTableLocator()->setConfig('MikrotikDhcpTa_Leases', [
                 'className' => 'App\Model\Table\MikrotikDhcpTa\LeasesTable'
               ]);               
         }
         
          if(!TableRegistry::getTableLocator()->exists('MikrotikDhcpTa_Networks')){
              
               TableRegistry::getTableLocator()->setConfig('MikrotikDhcpTa_Networks', [
                 'className' => 'App\Model\Table\MikrotikDhcpTa\NetworksTable'
               ]);               
         }
         
         if(!TableRegistry::getTableLocator()->exists('MikrotikDhcpTa_Gateways')){
              
               TableRegistry::getTableLocator()->setConfig('MikrotikDhcpTa_Gateways', [
                 'className' => 'App\Model\Table\MikrotikDhcpTa\GatewaysTable'
               ]);               
         }

          $this->modelControllers = $this->_ctrl->loadModel('MikrotikDhcpTa_Controllers');
          $this->modelIpExcluded = $this->_ctrl->loadModel('MikrotikDhcpTa_IpExcluded');
          $this->modelPlans = $this->_ctrl->loadModel('MikrotikDhcpTa_Plans');
          $this->modelPools = $this->_ctrl->loadModel('MikrotikDhcpTa_Pools');
          $this->modelDhcpServers = $this->_ctrl->loadModel('MikrotikDhcpTa_DhcpServers');
          $this->modelProfiles = $this->_ctrl->loadModel('MikrotikDhcpTa_Profiles');
          $this->modelQueues = $this->_ctrl->loadModel('MikrotikDhcpTa_Queues');
          $this->modelLeases = $this->_ctrl->loadModel('MikrotikDhcpTa_Leases'); 
          $this->modelNetworks = $this->_ctrl->loadModel('MikrotikDhcpTa_Networks');
          $this->modelGateways = $this->_ctrl->loadModel('MikrotikDhcpTa_Gateways');    
      
    }   
     
    
    //controller
    
    public function addController($controller){
         
        $tController =  $this->modelControllers->newEntity();
        $tController->id = $controller->id;        
        
        if(!$this->modelControllers->save($tController)){
            
            $data_error = new \stdClass; 
            $data_error->tController = $tController;
            $data_error->errors = $tController->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('No se pudo registrar el Controlador.'),
                'data' => $data_error,
                'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
        return $tController;
    }
    
    public function editController($controller, $data){
         
        $tController = $this->modelControllers->get($controller->id);
        
        $tController =  $this->modelControllers->patchEntity($tController, $data);
        
        if(!$this->modelControllers->save($tController)){
            
            $data_error = new \stdClass; 
            $data_error->tController = $tController;
            $data_error->errors = $tController->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('No se pudo editar el Controlador en la base de datos.'),
                'data' => $data_error,
                'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        return true;
    }
    
    public function getControllerTemplate($controller){
      
        return $this->modelControllers->get($controller->id);
    }
    
    public function deleteController($controller){  
        
        $tController = $this->modelControllers->get($controller->id);
        $tController->deleted = true;
        
        if(!$this->modelControllers->save($tController)){
            
            $data_error = new \stdClass; 
            $data_error->tController = $tController;
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
            'msg' => __('No se pudo eliminar el Controlador de la base de datos.'),
            'data' => $data_error,
            'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
          
        }
        return $tController;
    }
     
    public function getStatus($controller){          
         return $this->getStatusApi($controller);          
    }
     
    public function cloneController($id){
         
         $this->_ctrl->loadModel('Controllers');
         
         $error = false;
         
         $controller = $this->_ctrl->Controllers->get($id);
         $tcontroller = $this->getControllerTemplate($controller);
         
         
         $tcontroller->pools = $this->getPools($id);
         $tcontroller->dhcp_servers = $this->getDhcpServers($id);
         $tcontroller->ip_excluded = $this->getIPExcluded($id);
         $tcontroller->profiles = $this->getProfiles($id);
         $tcontroller->networks = $this->getNetworks($id);
         $tcontroller->plans = $this->getPlans($id);         
         $tcontroller->gateways = $this->getGateways($id);
         
         $newController = $this->_ctrl->Controllers->newEntity();    
         $newController = $this->_ctrl->Controllers->patchEntity($newController, $controller->toArray());
         
         $newController->name .= ' (Copia)';
         $newController->apply_config_avisos = false;
         $newController->apply_config_queue_graph = false;
         $newController->apply_config_https = false;
         $newController->apply_certificate_apissl = false;
         $newController->apply_certificate_wwwssl = false;
         $newController->apply_config_temp_access = false;
         
         if(!$this->_ctrl->Controllers->save($newController)){
              return false;
         }

         $newTController = $this->AddController($newController);  

         if(!$newTController){
              $this->Flash->error(__('No se pudo crear el (controller).')); 
              $error = true; 
         }   
         
         $parse_profiles = [];
         $parse_networks = [];
         $parse_pools = [];

         $newTController->pools = [];   
         $newTController->dhcp_servers = [];
         $newTController->ip_excluded = [];
         $newTController->profiles = [];
         $newTController->networks = [];
         $newTController->plans = []; 
         $newTController->gateways = []; 
         
         if(!$error){

              foreach($tcontroller->pools as $po){
                   
                    $parse_pools[$po->id] = null;

                    $data =  $po->toArray();
                    $data['controller_id'] = $newTController->id;         
                    $new_pool = $this->addPool($data);  
                   
                    if(!$new_pool){
                            
                         $error = true; 
                         
                         $data_error = new \stdClass; 
                         $data_error->data = $data;               

                         $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                              'msg' => __('No se pudo crear el (pool)'),
                              'data' => $data_error,
                              'flash' => false
                         ]);

                         $this->_ctrl->getEventManager()->dispatch($event);

                         break;
                    }
                   
                    $parse_pools[$po->id] = $new_pool->id; 
                   
                    $newTController->pools[] = $new_pool;                    
              }
              
              if(!$error){

                   foreach($tcontroller->dhcp_servers as $ps){

                         $data =  $ps->toArray();
                         $data['controller_id'] = $newTController->id;                          
                              
                         if($data['address_pool']){
                              $data['address_pool'] = $parse_pools[$data['address_pool']];
                         }                        
                        
                         $new_dhcp_server = $this->addDhcpServer($data);  

                         if(!$new_dhcp_server){

                              $error = true; 

                              $data_error = new \stdClass; 
                              $data_error->data = $data;               

                              $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                                   'msg' => __('No se pudo crear el (dhcp server)'),
                                   'data' => $data_error,
                                   'flash' => false
                              ]);

                              $this->_ctrl->getEventManager()->dispatch($event);

                              break;
                         }

                         $newTController->dhcp_servers[] = $new_dhcp_server;                    
                   }

                   if(!$error){

                        foreach($tcontroller->ip_excluded as $ie){

                              $data =  $ie->toArray();
                              $data['controller_id'] = $newTController->id;         
                              $new_ip_exc = $this->addIPExcludedDirect($data);     

                              if(!$new_ip_exc){                              

                                   $error = true;

                                   $data_error = new \stdClass; 
                                   $data_error->data = $data;               

                                   $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                                        'msg' => __('No se pudo crear la ip excluida'),
                                        'data' => $data_error,
                                        'flash' => false
                                   ]);

                                   $this->_ctrl->getEventManager()->dispatch($event);

                                   break;
                              }
                              $newTController->ip_excluded[] = $new_ip_exc;                                
                        }

                        if(!$error){

                             foreach($tcontroller->profiles as $pr){

                                   $parse_profiles[$pr->id] = null;

                                   $data =  $pr->toArray();
                                   $data['controller_id'] = $newTController->id;         
                                   $new_profile = $this->addProfile($data);     

                                   if(!$new_profile){

                                        $error = true;

                                        $data_error = new \stdClass; 
                                        $data_error->data = $data;               

                                        $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                                             'msg' => __('No se pudo crear el (profile)'),
                                             'data' => $data_error,
                                             'flash' => false
                                        ]);

                                        $this->_ctrl->getEventManager()->dispatch($event);

                                        break;                                   
                                   }

                                   $parse_profiles[$pr->id] = $new_profile->id; 

                                   $newTController->profiles[] = $new_profile;                                
                             }

                             if(!$error){

                                  foreach($tcontroller->networks as $ne){

                                        $parse_networks[$ne->id] = null;

                                        $data =  $ne->toArray();
                                        $data['controller_id'] = $newTController->id;                                   
                                        $data['next_network_id'] = null;

                                        $new_network =  $this->addNetwork($data); 

                                        if(!$new_network){

                                             $error = true;

                                             $data_error = new \stdClass; 
                                             $data_error->data = $data;               

                                             $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                                                  'msg' => __('No se pudo crear el (network)'),
                                                  'data' => $data_error,
                                                  'flash' => false
                                             ]);

                                             $this->_ctrl->getEventManager()->dispatch($event);

                                             break;
                                        }

                                        $parse_networks[$ne->id] = $new_network->id;

                                        $newTController->networks[] = $new_network;   
                                  }

                                  if(!$error){

                                       foreach($tcontroller->plans as $pl){

                                             $data =  $pl->toArray();

                                             $data['controller_id'] = $newTController->id;                                          
                                             $data['profile_id'] = $parse_profiles[$data['profile_id']];

                                             if($data['network_id']){
                                                  $data['network_id'] = $parse_networks[$data['network_id']];
                                             }        

                                             $data['network'] = null;
                                             $data['profile'] = null;                                       

                                             $new_plan = $this->addPlan($data);    

                                             if(!$new_plan){

                                                  $error = true;

                                                  $data_error = new \stdClass; 
                                                  $data_error->data = $data;               

                                                  $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                                                       'msg' => __('No se pudo crear el plan'),
                                                       'data' => $data_error,
                                                       'flash' => false
                                                  ]);

                                                  $this->_ctrl->getEventManager()->dispatch($event);

                                                  break;
                                             }
                                             $newTController->plans[] = $new_plan;
                                       }
                                       
                                       if(!$error){

                                            foreach($tcontroller->gateways as $ga){

                                                  $data =  $ga->toArray();

                                                  $data['controller_id'] = $newTController->id; 

                                                  $new_gateway = $this->addGateway($data);    

                                                  if(!$new_gateway){

                                                       $error = true;

                                                       $data_error = new \stdClass; 
                                                       $data_error->data = $data;               

                                                       $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                                                            'msg' => __('No se pudo crear el (gateway)'),
                                                            'data' => $data_error,
                                                            'flash' => false
                                                       ]);

                                                       $this->_ctrl->getEventManager()->dispatch($event);

                                                       break;
                                                  }
                                                 
                                                  $newTController->gateways[] = $new_gateway;
                                            }
                                       }                                        
                                  }                                  
                             }
                        }        
                   }
              }                                       
         }

         if($error){
              
              $this->modelPlans->deleteAll(['controller_id' => $newController->id]);
              $this->modelPools->deleteAll(['controller_id' => $newController->id]);
              $this->modelProfiles->deleteAll(['controller_id' => $newController->id]);
              $this->modelIpExcluded->deleteAll(['controller_id' => $newController->id]);
              $this->modelDhcpServers->deleteAll(['controller_id' => $newController->id]);
              $this->modelControllers->deleteAll(['controller_id' => $newController->id]);
              
              $this->modelPools->deleteAll(['controller_id' => $newController->id]);
              $this->modelGateways->deleteAll(['controller_id' => $newController->id]);             
                                                  
              $this->_ctrl->Controllers->delete($newController);     
                                                  
             return false;
              
         }
     
         return true;
     
    }
     
     
     //ip free
    
    protected function getIpPool($tnetwork_id){
         
        //ips de pools
        /**
         * $ips_pool = [
         *      ip long => pool_id
         * 
         *      168430081 => 1,
         *      168430075 => 1,
         * 
         *      168440020 => 2,
         * ]
        */
        
        $ips_network = [];
        
        do{
            $network = $this->modelNetworks->get($tnetwork_id);
            
            $ips = range(ip2long($network->min_host), ip2long($network->max_host));
            $ips = array_fill_keys($ips, $network->id);
            $ips_network += $ips;
           
            $tnetwork_id = $network->next_network_id;  
            
        }while($tnetwork_id);

        return $ips_network;
        
    }
    
    protected function getIpBusy($tnetwork_id){
        
        //ips usadas
        /**
         * $ips_busy = [
         *      ip long => pool_id
         * 
         *      168430081 => 1,
         *      168430075 => 1,
         * 
         *      168440020 => 2,
         * ]
        */
        
        $ips_busy = [];
        
        do{
            
            $network = $this->modelNetworks->get($tnetwork_id);
             
            $ips = $this->modelQueues->find('list', [
                    'keyField' => 'target',
                    'valueField' => 'network_id'
                ])
                ->where([
                    'network_id' => $network->id,
                    'controller_id' => $network->controller_id
                    ])
                ->toArray();
       
            $ips_busy += $ips;
       
            $tnetwork_id = $network->next_network_id;
            
        }while($tnetwork_id);
      
        return $ips_busy;
        
    }
    
    public function getFreeIPByPoolId($controller, $id){
        
        $ips_network = $this->getIpPool($id);         
      
        $ips_busy = $this->getIpBusy($id);     
        
        foreach($ips_network as  $ip => $network_id){
            if((!$this->validateIPExcluded($controller->id, $ip) && !array_key_exists($ip, $ips_busy))){
                return ['ip' => long2ip($ip), 'network_id' => $network_id, 'pool_id' => $network_id];
            }
        }

        return false;
    
    }
   
    protected function validateIPByController($controller, $ip){
         
        $network_selected = null;
        
        
        //valido que la ip no este usada en alguna queue
        
        $tqueue = $this->modelQueues->find()->where([
            'target' => ip2long($ip),
            'controller_id' => $controller->id
        ])->first();
        
        if($tqueue){
        
            $data_error = new \stdClass;
            $data_error->queue = $tqueue;
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
            'msg' => __('la IP: {0} esta ocupada por otra conexión en este contrador.', $ip),
            'data' => $data_error,
            'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
         
            return false;
        }
        
        
        //valido si la ip no esta en alista de ip excluidas
        
        $exist = $this->validateIPExcluded($controller->id, $ip);
        
        if($exist){
            
            $data_error = new \stdClass; 
            $data_error->ip = $ip;
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('la IP: {0} se encuentra en la lista de IP Excluidas.', $ip),
                'data' => $data_error,
                'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
        
        //valido que la ip perteneczca alguan network del controlador
        
        $networks = $this->modelNetworks->find()->where(['controller_id' => $controller->id]);
        
        $network_valid = false;
        
        foreach($networks as $network){
       
            if(Cidr::cidr_match($ip, $network->addresses)){
                $network_valid = true;
                $network_selected = $network;
                break;
            }
        }
        
        
        if(!$network_valid){
            
            $data_error = new \stdClass; 
            $data_error->ip = $ip;
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('la IP: {0} no pertece a ninguna red del controlador.', $ip),
                'data' => $data_error,
                'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
            
        }

        return ($network_valid) ? ['ip' => $ip, 'pool_id' => $network_selected->id] : false;
    }
     
     
     
     //ip excluded
    
    public function getIPExcluded($controller_id){
        
        return $this->modelIpExcluded->find()
            ->where(['controller_id' => $controller_id]);
           
    }

    public function addIPExcludedDirect($data){
        
        $rango = false;
        
        if(strpos($data['ip'], '-') !== false){  //rango
        
            $rango = true;
            
            $temp = explode('-', $data['ip']);
            $ip_from = $temp[0];
            $ip_to = $temp[1];
            
            if(!Cidr::validIP($ip_from) || !Cidr::validIP($ip_to)){
                
                $data_error = new \stdClass; 
                $data_error->ip = $data['ip'];
                
                $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                    'msg' => __('Algunas de las IP ingresada no es valida.'),
                    'data' => $data_error,
                    'flash' => false
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
         
                return false;
                
            } 
            
            $ip_from    = ip2long($temp[0]);
            $ip_to      = ip2long($temp[1]);
            
            if($ip_from >= $ip_to){
                
                $data_error = new \stdClass; 
                $data_error->ip = $data['ip'];
                
                $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                    'msg' => __('{0} >= {1} Rango invalido.', $temp[0], $temp[1] ),
                    'data' => $data_error,
                    'flash' => false
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
         
                return false;
            } 
            
        } else { //no es un rango
            
            if(!Cidr::validIP($data['ip'])){ 
                
                $data_error = new \stdClass; 
                $data_error->ip = $data['ip'];
                
                $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                    'msg' => __('La IP ingresa no es valida.'),
                    'data' => $data_error,
                    'flash' => false
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
         
                return false;
            }
        }
        
        //falta validada si la ip o rango de ip esta dentro de alguin rango ya excluido 
       
        $ipExcuded = $this->modelIpExcluded->find()
            ->where([
                'ip' => $data['ip'],
                'controller_id' => $data['controller_id']
                ])
            ->first();
            
        if($ipExcuded){
            
            $data_error = new \stdClass; 
            $data_error->ip = $data['ip'];
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('La {0} ya esta excluida es este controlador.', $data['ip']),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
     
            return false;
        }
        
        
        if($rango){
            
            $temp = explode('-', $data['ip']);
            $ip_from    = ip2long($temp[0]);
            $ip_to      = ip2long($temp[1]);
            
            $tqueue = $this->modelQueues->find()
                ->where(function (QueryExpression $exp, Query $q) use ($ip_from, $ip_to) {
                return $exp->between('target', $ip_from, $ip_to);
            })->first();
        
        }else{
            
            $tqueue = $this->modelQueues->find()
                ->where(['target' => ip2long($data['ip']),
                    'controller_id' => $data['controller_id']
                    ])
                ->first();
        }
        
        
        if($tqueue){
            
            $data_error = new \stdClass; 
            $data_error->ip = $data['ip'];
            $data_error->queue = $tqueue;
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('{0} ingresada no esta libre.', $data['ip']),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
     
            return false;
        }
       
       
        $ipExcluded = $this->modelIpExcluded->newEntity();
        
        $ipExcluded = $this->modelIpExcluded->patchEntity($ipExcluded, $data );

        if(!$this->modelIpExcluded->save($ipExcluded)){
            
            $data_error = new \stdClass; 
            $data_error->ipExcluded = $ipExcluded->errors();
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al intentar agregar la IP Excluida a la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
     
            return false;
        }

        return $ipExcluded;
    }

    public function deleteIPExcludedById($id){
        
        $ipExcluded = $this->modelIpExcluded->get($id);
        
        if(!$this->modelIpExcluded->delete($ipExcluded)){
            
            $data_error = new \stdClass; 
            $data_error->ipExcluded = $ipExcluded;
            $data_error->errors = $ipExcluded->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al Intentar agreghar la Ip Excluded a la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
      
            return false;
        }
      

        return $ipExcluded;
   
    }
   
    protected function validateIPExcluded($controller_id, $ip){
         
        $ipExcluidasRango = $this->modelIpExcluded->find()
            ->where([
                'ip like' => '%-%',
                'controller_id' => $controller_id
                ]);
        
        foreach($ipExcluidasRango as $ip_e_r){
            
            $temp = explode('-', $ip_e_r->ip);
            $ip_from = ip2long($temp[0]);
            $ip_to = ip2long($temp[1]);
            
            if($ip_from <= $ip){
                if($ip <= $ip_to){
                    return true;
                }
            }
        }
        
        return ($this->modelIpExcluded->find()
            ->where([
                'controller_id' => $controller_id, 
                'ip not like' => '%-%',
                'ip' => long2ip($ip)
                ])->first()) ? true : false;
           
    }
    
    
    
    
    //planes
    
    public function getPlans($controller_id){
        
        $tplans =  $this->modelPlans->find()
            ->contain(['MikrotikDhcpTa_Profiles', 'MikrotikDhcpTa_Networks'])
            ->where([
                'MikrotikDhcpTa_Plans.controller_id' => $controller_id,
                ]);
                
        $this->_ctrl->loadModel('Services');
        
        foreach ($tplans as $plan) {
            
            $plan->connections_amount = $this->modelQueues->find()
                ->where([
                    'plan_id' => $plan->id,
                    'controller_id' => $controller_id
                    ])
                ->count();
                
            $plan->service = $this->_ctrl->Services->get($plan->service_id);
        }
        
        return $tplans;
    }
    
    public function getPlan($controller, $id){
        
        return $this->modelPlans->find()
            ->contain(['MikrotikDhcpTa_Profiles', 'MikrotikDhcpTa_Networks'])
            ->where([
                'MikrotikDhcpTa_Plans.id' => $id,
                ])->first();
    }

    public function addPlan($data){
    
        $tplan = $this->modelPlans->find()
            ->where([
                'service_id' => $data['service_id'],
                'controller_id' => $data['controller_id']
                ])
            ->first();
       
        if($tplan){
            
            $data_error = new \stdClass; 
            $data_error->other_plan = $tplan;
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('Este servicio ya existe en el controlador'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
   
        $tplan = $this->modelPlans->newEntity();
        $tplan = $this->modelPlans->patchEntity($tplan, $data );
        
        if(!$this->modelPlans->save($tplan)){
            
            $data_error = new \stdClass; 
            $data_error->tplan = $tplan;
            $data_error->errors = $tplan->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al intentar agregar el Servicio '),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
      
            return false;
        }

        return $tplan;
    }
    
    public function editPlan($data){
        
        $plan = $this->modelPlans->get($data['id']);
        
        $plan->network_id = $data['network_id'] ? $data['network_id'] : null;
       
        if($plan->profile_id != $data['profile_id'] ){ //cambio de profile
        
            $profile = $this->modelProfiles->get($data['profile_id']);
        
            $queuesFixProfile = $this->modelQueues->find()->where([
                'plan_id' => $plan->id,
                'controller_id' => $plan->controller_id
            ]);
            
            $this->_ctrl->loadModel('Connections');
             
            foreach($queuesFixProfile as $queue){
                
                //marco diferencia en conexiones
                $connection = $this->_ctrl->Connections->get($queue->connection_id);
                $connection->diff = true;
                $this->_ctrl->Connections->save($connection);
                
                //edito queue
                $queue->profile_id = $profile->id;
                $this->modelQueues->save($queue);                
            }
            
            //edito plan
            $plan->profile_id = $data['profile_id'];
            
            if(!$this->modelPlans->save($plan)){
            
                $data_error = new \stdClass; 
                $data_error->plan = $plan;
                
                $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                    'msg' => __('Error al intentar editar el Plan.'),
                    'data' => $data_error,
                    'flash' => false
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
       
                return false;
            }
            
        }
        else{
            
             //edito plan
            if(!$this->modelPlans->save($plan)){
            
                $data_error = new \stdClass; 
                $data_error->plan = $plan;
                
                $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                    'msg' => __('Error al intentar editar el Plan.'),
                    'data' => $data_error,
                    'flash' => false
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
       
                return false;
            }
        }
        
        
        return $plan;
        
    }
    
    public function movePlan($data){
        
        /*
        [service_id_destino] => 2
        [controller_id] => 1
        [id] => 1
        */
        
        //plan origen
        $plan_origen = $this->modelPlans->get($data['id']);
        
        //busco el plan que tiene el servicio destino
        $plan_destino = $this->modelPlans->find()->where([
            'service_id' => $data['service_id_destino'],
            'controller_id' => $data['controller_id']]
            )->first();
            
        $this->_ctrl->loadModel('Connections');

        //busco las conexion que tiene el servicio origen
        $connections =  $this->_ctrl->Connections->find()
            ->contain(['Controllers', 'Customers'])
            ->where([
                'Connections.controller_id' => $data['controller_id'],
                'Connections.service_id' => $plan_origen->service_id,
                'Connections.deleted' => false
                ]);
              
    
        //inico el data con valores generales  
        $data__connection = [
            'plan_id' => $plan_destino->id,
            'validate_ip' => '0',
            'pool_id' => ''
        ];
       
                
        //recorro las conexiones que tiene el servicio origen
        foreach($connections as $connection){
            
            //obtengo info completa de la conexion, quuees + secrets
            $connection = $this->getConnection($connection);
            
            if(!$connection){
                $this->_ctrl->log('ERROR: no encontro info completa de la connection_id: ' . $connection->id, 'debug');
                continue;
            }
            
            if(!$connection->queue){
                $this->_ctrl->log('ERROR: no encontro queue de la connection_id: ' . $connection->id, 'debug');
               continue;
            }
            
            if($connection->queue->network_id){
                
                //la conexion estaba con validar ip, busco ip con la red nueva
                
                $data__connection['validate_ip'] = '1';
                
                $data_find_ip = null;
                
                if($plan_destino->network_id){
                    
                    //si el plan destino tiene red por defecto, bucco con esa red
                     $data_find_ip = $this->getFreeIPByPoolId($connection->controller, $plan_destino->network_id);
                     
                     if($data_find_ip){
                         
                         //encontro ip libre
                         $data__connection['ip'] = $data_find_ip['ip'];
                         $data__connection['pool_id'] = $data_find_ip['network_id'];
                         $data__connection['before_ip'] = $connection->ip;
                         
                     }else{
                         //no encontro ip libre
                        $this->_ctrl->log('no encontro ip libre para connection_id: ' . $connection->id, 'debug');
                        continue;
                     }
                }
                else{
                    //el plan destino no tiene red por defecto,  dejo la mismo ip y red que tenia
                     $data__connection['ip'] = $connection->ip;
                }
              
            }
            else{
                //la conexion no estaba con validar ip, dejo la misma ip que tenia
                $data__connection['ip'] = $connection->ip;
                
            }
            
            //paso valores que no deberian cambiar
            $data__connection['mac'] = $connection->mac;
            
            /*
            'lat' => '',
        	'lng' => '',
        	'customer_code' => '4',
        	'controller_id' => '2',
        	'plan_id' => '2',
        	'pool_id' => '2',
        	'interface' => '',
        	'ip' => '10.20.45.3',
        	'validate_ip' => '1',
        	'mac' => '02AEFC02F3E4',
        	'name' => '',
        	'password' => '',
        	'address' => '8765 Kathryn Club Suite 549Estelbury, ME 95731',
        	'area_id' => '1',
        	'clave_wifi' => '',
        	'ports' => '',
        	'ip_alt' => '',
        	'ing_traffic' => '',
        	'queue_name' => 'h00004',
        	'comments' => ''
        	*/
         
            
            if($this->editConnection($connection, $data__connection)){
                $this->_ctrl->log('conexion id: ' . $connection->id . ' EDITADO!', 'debug');
            }else{
                $this->_ctrl->log('conexion id: ' . $connection->id . ' ERROR!', 'debug');
            }
        }
        
        return true;
        
    }
    
    public function deletePlan($id){
        
        $count = $this->modelQueues->find()->where(['plan_id' => $id])->count();
        
        $plan = $this->modelPlans->get($id);
        
        if($count > 0){
            
            $data_error = new \stdClass; 
            $data_error->plan = $plan;
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('No se puede eliminar. Existe {0} Queue(s) relacionado(s) a este plan.', $count),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
      
            return false;    
        }
        
       
        
        if(!$this->modelPlans->delete($plan)){
            
            $data_error = new \stdClass; 
            $data_error->tplan = $plan;
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al intentar eliminar el Plan'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
      
            return false;  
           
        }

        return $plan;
    }
     
     
     
    //ppp servers
    
    public function getDhcpServers($controller_id){    
       
        $dhcpServers =  $this->modelDhcpServers->find()
            ->where([
                'controller_id' => $controller_id
                ]);
         
        foreach($dhcpServers as $dhcpServer){
            
            $address_pool = $this->modelPools->find()
                ->where([
                    'name' => $dhcpServer->address_pool,
                    'controller_id' => $dhcpServer->controller_id 
                    ])
                ->first();
            
            if($address_pool){
                $dhcpServer->address_pool = $address_pool->id;
                $dhcpServer->address_pool_name = $address_pool->name;
            }else{
                $dhcpServer->address_pool = null;
                $dhcpServer->address_pool_name = null;
            }
        }
         
         return $dhcpServers; 
         
    }
    
    public function getDhcpServersArray($controller_id){
         
         //busca el objeto en la base del template
        $dhcpservers =  $this->modelDhcpServers->find()
            ->where([
                'controller_id' => $controller_id
                ])
            ->order([
                'name' => 'ASC'
                ])
            ->toArray();
        
        $array = [];
     
        foreach($dhcpservers as $dhcpserver){
            $array[$dhcpserver->name] = $dhcpserver;
        }
     
        return $array;
     
    }
    
    public function getDhcpServersInController($controller){
        return $this->getDhcpServersApi($controller);
    }
    
    public function addDhcpServer($data){                       
     
        $dhcpServer = $this->modelDhcpServers->find()
            ->where([
                'name' => $data['name'],
                'controller_id' => $data['controller_id'] 
                ])
            ->first();
        
        if($dhcpServer){
            
            $data_error = new \stdClass; 
            $data_error->other_dhcp_server = $dhcpServer;
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('Ya existe un (dhcp server) con el mismo nombre'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
      
            return false;  
        }
        
        if($data['address_pool'] != ''){
            
            $tpool = $this->modelPools->get($data['address_pool']);
            $data['address_pool'] =  $tpool->name;
        }
        
   
        $dhcpServer = $this->modelDhcpServers->newEntity();
        
        if(!$data['address_pool']){
           $data['address_pool'] = 'static-only'; 
        } 
        
        $dhcpServer = $this->modelDhcpServers->patchEntity($dhcpServer, $data );
        
        if(!$this->modelDhcpServers->save($dhcpServer)){
            
            $data_error = new \stdClass; 
            $data_error->dhcpServer = $dhcpServer;
            $data_error->errors = $dhcpServer->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al intentar agregra el (dhcp server)'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
      
            return false;  
        }
  
        return $dhcpServer;
        
    }
    
    public function deleteDhcpServerInController($controller, $dhcp_server_api_id){
       return $this->deleteDhcpServerByApiIdApi($controller, $dhcp_server_api_id);
    }
    
    public function deleteDhcpServer($id){
       
        $dhcpServer = $this->modelDhcpServers->get($id);
        
        if(!$this->modelDhcpServers->delete($dhcpServer)){
            
            $data_error = new \stdClass; 
            $data_error->dhcpServer = $dhcpServer;
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('No se pudo eliminar el (dhcp server)'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
      
            return false;
        }
      
        return $dhcpServer; 
    }
     
    public function editDHCPServer($data){     
        
       $dhcpServer = $this->modelDhcpServers->find()
            ->where([
                'name' => $data['name'],
                'controller_id' => $data['controller_id'],
                'id !=' => $data['id']
                ])
            ->first();
        
        if($dhcpServer){
            
            $data_error = new \stdClass; 
            $data_error->other_dhcpServer = $dhcpServer;
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('Ya existe un (dhcp server) con el mismo nombre.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
   
            return false;
        }
       
        if($data['address_pool'] != ''){
            
            $tpool = $this->modelPools->get($data['address_pool']);
            $data['address_pool'] =  $tpool->name;
        }
        
        if(!$data['address_pool']){
           $data['address_pool'] = 'static-only'; 
        } 
       
        
        $dhcpServer = $this->modelDhcpServers->get($data['id']);
        $dhcpServer = $this->modelDhcpServers->patchEntity($dhcpServer, $data );
        
        
        if(!$this->modelDhcpServers->save($dhcpServer)){
            
            $data_error = new \stdClass; 
            $data_error->dHCPServer = $dhcpServer;
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al intentar editar el (dhcp server).'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
   
            return false;
        }
        
        //si cambio el nombre  del DHCP server cambiaer en todos los 
    
        $this->modelLeases->updateAll(
            [  
                'server' => $dhcpServer->name,
                'lease_time' => $dhcpServer->leases_time_gral,
            ],
            [  
                'controller_id' => $dhcpServer->controller_id
            ]
        );

        return $dhcpServer;
        
    }
    
    public function syncDhcpServer($id){    
               
    
        $dhcpServer =  $this->modelDhcpServers->get($id);
        
        $this->_ctrl->loadModel('Controllers');
        $controller = $this->_ctrl->Controllers->get($dhcpServer->controller_id);

        if(!$this->integrationEnabled($controller)){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('Intento de sincronizacion de (dhcp server). Integracion desactivada.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
   
            return false;  
        }
      
        $dhcpServer = $this->syncDhcpServerApi($controller, $dhcpServer);
        
        if(!$dhcpServer){
            return false;
        }
        
        
        //limpia los api_id que existan duplicados
        $duplicates = $this->modelDhcpServers->find()
        	->where([
        	'api_id' => $dhcpServer->api_id,
        	'id !=' => $dhcpServer->id,
        	'controller_id' => $controller->id
        	]);
        
        foreach($duplicates as $duplicated){
        
        	$duplicated->api_id = NULL;
        	$this->modelDhcpServers->save($duplicated);
        }
      
      
        if(!$this->modelDhcpServers->save($dhcpServer)){
            
            $data_error = new \stdClass; 
            $data_error->dhcpServer = $dhcpServer;
            $data_error->errors = $dhcpServer->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al intentar actualizar el (dhcp server) en la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
   
            return false;  
        }
       
        return $dhcpServer;  
        
    }
     
     
    //profiles

     public function getProfiles($controller_id){
          
         $profiles = $this->modelProfiles->find()
            ->where([
                'controller_id' => $controller_id
                ]);
        
        foreach($profiles as $profile){
            
            $profile->connections_amount = $this->modelQueues->find()
                ->where([
                    'profile_id' => $profile->id,
                    'controller_id' => $profile->controller_id
                    ])
                ->count();
        }
     
        return $profiles;
          
     }
     
     public function getProfilesArray($controller_id){
          
        //busca el objeto en la base del template
        $profiles =  $this->modelProfiles->find()
            ->where([
                'controller_id' => $controller_id
                ]);
        
        $profilesArray = [];
        foreach ($profiles as $profile) {
            
            $profilesArray[$profile->name] = $profile;
        }
        
        return $profilesArray;
          
     }
     
     public function getProfilesInController($controller){
          return $this->getProfilesApi($controller);
     }
     
     public function addProfile($data){
          
          $profile = $this->modelProfiles->find()
            ->where([
                'name' => $data['name'],
                'controller_id' => $data['controller_id']
                ])
            ->first();
        
        if($profile){
            
            $data_error = new \stdClass; 
            $data_error->other_profile = $profile;
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('Ya existe un (profile) con el mismo nombre para este controlador.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
   
        $profile = $this->modelProfiles->newEntity();
        $profile = $this->modelProfiles->patchEntity($profile, $data);
        
        if(!$this->modelProfiles->save($profile)){
            
            $data_error = new \stdClass; 
            $data_error->profile = $profile;
            $data_error->errors = $profile->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al intentar agregar el (profile) en la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
      
        return $profile;
     }
     
     public function deleteProfile($id){
          
          $count = $this->modelPlans->find()
            ->where([
                'profile_id' => $id
                ])
            ->count();
        
        if($count > 0){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('No se puede eliminar. Existe {0} Plan(es) relacionado(s) a este  (profile).', $count),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
        $count = $this->modelQueues->find()
            ->where([
                'profile_id' => $id
            ])
            ->count();
        
        if($count > 0){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('No se puede eliminar. Existe {0} Queue(s) relacionado(s) a este  (profile).', $count),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
        $profile = $this->modelProfiles->get($id);
        
        if(!$this->modelProfiles->delete($profile)){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al intentar eliminar el  (profile) de la base de datos'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
      
        return $profile;
          
     }
     
     public function editProfile($controller, $data){         
   
        $profile = $this->modelProfiles->find()
            ->where([
                'name' => $data['name'],
                'id !=' => $data['id'],
                'controller_id' => $data['controller_id']
                ])
            ->first();
        
        if($profile){
            
            $data_error = new \stdClass;
            $data_error->other_profile = $profile;
         
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('Ya existe un (profile) con el mismo nombre.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);

            return false;
        }
   
        $profile = $this->modelProfiles->get($data['id']);
        $profile = $this->modelProfiles->patchEntity($profile, $data );
        
        if(!$this->modelProfiles->save($profile)){
            
            
            $data_error = new \stdClass; 
            $data_error->profile = $profile;
            $data_error->errors = $profile->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al intentar editar el  (profile) en la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
    
        return $profile;
          
          
     }
     
     
     //networks

     public function getNetworks($controller_id){
          
         $networks = $this->modelNetworks->find()
            ->where([
                'controller_id' => $controller_id,
                ]);
           
        foreach($networks as $network){
            
            $network->connections_amount = $this->modelQueues->find()
                ->where([
                    'network_id' => $network->id,
                    'controller_id' => $controller_id
                    ])
                ->count();
            
            if($network->next_network_id){
                $network->next_network =  $this->modelNetworks->get($network->next_network_id);
            }
        }

        return $networks;
     }
     
     public function addNetwork($data){
       
        $tnetwork = $this->modelNetworks->find()
            ->where([
                'comment' => $data['comment'],
                'controller_id' => $data['controller_id']
                ])
            ->first();
        
        if($tnetwork){ //se repite el nombre
            
            $data_error = new \stdClass; 
            $data_error->other_tnetwork = $tnetwork;
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('Ya existe un (network) con el mismo nombre en este controlador.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
     
            return false;
        }
        
        $tnetwork = $this->modelNetworks->find()
            ->where([
                'address' => $data['address'],
                'controller_id' => $data['controller_id']
                ])
            ->first();
            
        if($tnetwork){ //ya existe la red en el controlador
            
            $data_error = new \stdClass; 
            $data_error->other_tnetwork = $tnetwork;
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('Ya existe un (network)  con la misma red es este controlador.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
     
            return false;
        }
        
        
        if(!Cidr::cidr_match($data['gateway'], $data['address'])){ //el gateway no pertenece a la red
            
            $data_error = new \stdClass; 
            $data_error->gateway = $data['gateway'];
            $data_error->address = $data['address'];
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('El (gateway) debe pertenecer al address.'),
                'data' => $data_error,
                'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
     
            return false;
            
        }
        
        if($data['access_new_customer']){
            
              $tnetwork = $this->modelNetworks->find()
                ->where([
                    'controller_id' => $data['controller_id'],
                    'access_new_customer' => true
                    ])
                ->first();
              
              if($tnetwork){
                    $data_error = new \stdClass; 
                    $data_error->tnetwork = $tnetwork;
                    
                    $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                        'msg' => __('Solo puede existir un (network) para acceso de nuevos clientes.'),
                        'data' => $data_error,
                        'flash' => true
                    ]);
                    
                    $this->_ctrl->getEventManager()->dispatch($event);
             
                    return false;
              }
        }
        
        $ipEcluded = [
            'ip' => $data['gateway'],
            'controller_id' => $data['controller_id'],
            'comments' => 'Gateway nertwork ' . $data['comment'],
        ];
        
        $this->addIPExcludedDirect($ipEcluded);
        
        $tnetwork = $this->modelNetworks->newEntity();
        $tnetwork = $this->modelNetworks->patchEntity($tnetwork, $data);
     
        $tnetwork_next_network = $this->modelNetworks->find()
            ->where([
                'next_network_id' => $tnetwork->next_network_id
                ])
            ->first();
         
        if($tnetwork_next_network){
          
            $data_error = new \stdClass; 
            $data_error->other_tnetwork = $tnetwork_next_network;
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('El (nertwork) siguiente ya esta usando.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
       
            return false;
        }
     
     
        if(!$this->modelNetworks->save($tnetwork)){ 
            
            $data_error = new \stdClass; 
            $data_error->tnetwork = $tnetwork;
            $data_error->errors = $tnetwork->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al intentar agregar el (network) a la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
     
            return false;
        }
        
   
        return $tnetwork;
     }
     
     public function editNetwork($controller, $data){          
      
        $tnetwork = $this->modelNetworks->find()
            ->where([
                'comment' => $data['comment'],
                'id !=' => $data['id'],
                'controller_id' => $data['controller_id']
                ])
            ->first();
        
        if($tnetwork){
            
            $data_error = new \stdClass; 
            $data_error->other_network = $tnetwork;
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('Ya existe un (network) con el mismo nombre en este controlador.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
   
            return false;
        }
        
        $tnetwork = $this->modelNetworks->find()
            ->where([
                'address' => $data['address'],
                'id !=' => $data['id'],
                'controller_id' => $data['controller_id']
                ])
            ->first();
        
        if($tnetwork){
            
            $data_error = new \stdClass; 
            $data_error->other_network = $tnetwork;
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('Ya existe un (network) con la misma red en este controlador.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
   
            return false;
        }
        
        
        $tnetwork = $this->modelNetworks->get($data['id']);
        
        if(!Cidr::cidr_match($data['gateway'], $data['address'])){
            
            $data_error = new \stdClass; 
            $data_error->gateway = $data['gateway'];
            $data_error->address = $data['address'];
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('El gateway debe pertenecer al address.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
     
            return false;
            
        }
          
        if($tnetwork->address != $data['address']){
             
             if( $this->validateNetwokUsed($tnetwork)  ){
             
                 $data_error = new \stdClass; 
                 $data_error->tppNetwork = $tnetwork;

                 $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                     'msg' => __('La red de este (network) no puede ser editado por que existen elementos relacionados.'),
                     'data' => $data_error,
                     'flash' => false
                 ]);

                 $this->_ctrl->getEventManager()->dispatch($event);

                 return false;
             }             
        }            
        
        
         if($data['access_new_customer']){
            
              $other_network = $this->modelNetworks->find()
                ->where([
                    'controller_id' => $data['controller_id'],
                    'access_new_customer' => true,
                    'id !=' => $data['id']
                    ])
                ->first();
              
              if($other_network){
                   
                    $data_error = new \stdClass; 
                    $data_error->other_network = $other_network;
                    
                    $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                        'msg' => __('Solo puede existir un (network) para acceso de nuevos clientes en el controlador.'),
                        'data' => $data_error,
                        'flash' => true
                    ]);
                    
                    $this->_ctrl->getEventManager()->dispatch($event);
             
                    return false;
              }
        }
        
        
        $gateway_old = $tnetwork->gateway;
     
        $tnetwork = $this->modelNetworks->patchEntity($tnetwork, $data );
       
        if(!$this->modelNetworks->save($tnetwork)){
            
            $data_error = new \stdClass; 
            $data_error->tnetwork = $tnetwork;
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al intentar editar el Network.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
   
            return false;
        }
        
        
        if($gateway_old != $data['gateway']){
            
            $ipEcluded = [
                'ip' => $data['gateway'],
                'controller_id' => $data['controller_id'],
                'comments' => 'Gateway nertwork ' . $data['comment'],
            ];
            
            $this->addIPExcludedDirect($ipEcluded);
        }

        return $tnetwork;
          
     }          
          
     public function deleteNetwork($id){
   
        $count = $this->modelPlans->find()->where(['network_id' => $id ])->count();
        
        if($count > 0){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('No se puede eliminar. Existe {0} Plan(es) relacionado(s) a este (network).', $count),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
            
        }
          
         $tnetwork = $this->modelNetworks->get($id);
        
        if( $this->validateNetwokUsed($tnetwork)  ){
             
            $data_error = new \stdClass; 
            $data_error->tppNetwork = $tnetwork;
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('La red de este (network) no puede ser eliminada por que existen elementos relacionados.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
   
            return false;
        }
        
        //verficar referencias
        $network_next = $this->modelNetworks->find()->where(['next_network_id' => $tnetwork->id, 'controller_id' => $tnetwork->controller_id])->first();
        
        if($network_next){
            
            $data_error = new \stdClass;
            $data_error->other_network = $network_next;
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('No se puede eliminar por que es (network) siguiente del (network): {0}', $network_ref->name),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
        $this->_ctrl->loadModel('Controllers');
        $controller = $this->_ctrl->Controllers->get($tnetwork->controller_id);
      
        $gateway_ip = $tnetwork->gateway;
        
        if(!$this->modelNetworks->delete($tnetwork)){
            
            $data_error = new \stdClass;
            $data_error->tnetwork = $tnetwork;
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al intentar eliminar el (network) de la base de datos'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
        return $tnetwork;
     }
     
     public function validateNetwokUsed($tnetwork){
          
        $min_host = $tnetwork->min_host;
        $max_host = $tnetwork->max_host ;
          
        $queue = $this->modelQueues->find()
             ->where([
                  'target >= ' => ip2long($min_host),
                  'target < ' => ip2long(Cidr::nextIp($max_host)),
                  'controller_id' => $tnetwork->controller_id
             ])
             ->first();
            
        if($queue){
            return true;
        }
        
        return false;
     }
     
     public function deleteNetworksInController($controller, $api_id){
          return $this->deleteNetworksInControllerApi($controller, $api_id);
     }
     
     public function syncNetwork($id){ 
      
        $network =  $this->modelNetworks->get($id);
        
        $this->_ctrl->loadModel('Controllers');
        $controller = $this->_ctrl->Controllers->get($network->controller_id);

        if(!$this->integrationEnabled($controller)){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('Intento de sincronizacion de (network). Integracion desactivada.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
   
            return false;  
        }  
      
        $network = $this->syncNetworkApi($controller, $network);
        
        if(!$network){
            return false;
        }
        
        
        //limpia los api_id que existan duplicados
        $duplicates = $this->modelNetworks->find()
        	->where([
        	'api_id' => $network->api_id,
        	'id !=' => $network->id,
        	'controller_id' => $controller->id
        	]);
        
        foreach($duplicates as $duplicated){
        
        	$duplicated->api_id = NULL;
        	$this->modelNetworks->save($duplicated);
        }
      
      
        if(!$this->modelNetworks->save($network)){
            
            $data_error = new \stdClass; 
            $data_error->network = $network;
            $data_error->errors = $network->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al intentar actualizar el (netwrok) en la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
   
            return false;  
        }
       
        return $network; 
          
     }
     
     public function getNetworksArray($controller_id){
          
        //busca el objeto en la base del template
        $networks =  $this->modelNetworks->find()
             ->where([
                  'controller_id' => $controller_id
             ])
             ->order([
                  'comment' => 'ASC'
             ])
             ->toArray();
        
        $array = [];
     
        foreach($networks as $network){
            $array[$network->comment] = $network;
        }
     
        return $array;
     }
     
     public function getNetworksInController($controller){
          return $this->getNetworksApi($controller);         
     }
     
     public function moveNetwork($data){
          
        $this->_ctrl->loadModel('Controllers');
        $this->_ctrl->loadModel('Connections');
        $this->_ctrl->loadModel('FirewallAddressLists');
        $this->_ctrl->loadModel('ConnectionsHasMessageTemplates');
        
        
        $network_from = $this->modelNetworks->get($data['network_from_id']);
        $network_to = $this->modelNetworks->get($data['network_to_id']);
        
        $controller = $this->_ctrl->Controllers->get($network_from->controller_id);
        
        $queuesFix = $this->modelQueues->find()->where(['network_id' => $network_from->id]);
        
        foreach($queuesFix as $queue){
            
            //busco ip libre
            $ip_data = $this->getFreeIPByPoolId($controller, $network_to->id);
            
            if(!$ip_data){
                return false;
            }
            
            //connection
            $connection = $this->_ctrl->Connections->get($queue->connection_id);
            $connection->ip = $ip_data['ip'];
            $connection->diff = true;
            $this->_ctrl->Connections->save($connection);
                
            //queue
            $queue->target = ip2long($connection->ip);
            $queue->network_id = $ip_data['network_id'];
            $this->modelQueues->save($queue);
            
            //lease
            $lease = $this->modelLeases->find()->where(['connection_id' => $queue->connection_id])->first();
            $lease->address = ip2long($connection->ip);
            $this->modelLeases->save($lease);
            
            
            //FirewallAddressLists
            $this->_ctrl->FirewallAddressLists->updateAll(
                [  
                    'address' => $connection->ip 
                ],
                [  
                    'connection_id' => $connection->id
                ]
            );
            
            //ConnectionsHasMessageTemplates
            $this->_ctrl->ConnectionsHasMessageTemplates->updateAll(
                [  
                    'ip' => $connection->ip 
                ],
                [  
                    'connections_id' => $connection->id
                ]
            );
        }
        
        return true;
          
     }
     
     
     
     //gateways

     public function getGateways($controller_id){
          
         $gateways = $this->modelGateways->find()
            ->where([
                'controller_id' => $controller_id,
                ]);
                
                
        foreach($gateways as $gateway){
            
             $network = $this->modelNetworks->find()
                ->where([
                    'address' => $gateway->address,
                    'controller_id' => $controller_id,
                    ])->first();
                    
            if($network){
                $gateway->address_id = $network->id;
            }
            
        }
       
        return $gateways;         
          
     }
     
     public function addGateway($data){
          
        $tnetwork = $this->modelNetworks->find()
             ->where([
                  'id' => $data['network_id'],
                  'controller_id' => $data['controller_id']
             ])
             ->first();
        
        $tgateway = $this->modelGateways->find()
             ->where([
                  'address' => $tnetwork->address,
                  'controller_id' => $tnetwork->controller_id
             ])
             ->first();
        
        if($tgateway){ //se repite el addresss
            
            $data_error = new \stdClass; 
            $data_error->other_gateway = $tgateway;
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('Ya existe un (gateway) con el mismo addresss.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
     
            return false;
        }
        
      
        
        $tgateway = $this->modelGateways->newEntity();
        
        $length_net = explode('/', $tnetwork->address)[1];
        $data['address'] = $tnetwork->gateway . '/' . $length_net;
        
        $tgateway = $this->modelGateways->patchEntity($tgateway, $data);
     
        if(!$this->modelGateways->save($tgateway)){ 
            
            $data_error = new \stdClass; 
            $data_error->tgateway = $tgateway;
            $data_error->errors = $tgateway->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al intentar agregar el (gateway) a la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
     
            return false;
        }
        
   
        return $tgateway;          
     }
     
     public function editGateway($controller, $data){
          
        $tnetwork = $this->modelNetworks->find()
             ->where([
                  'id' => $data['network_id'],
                  'controller_id' => $data['controller_id']
             ])
             ->first();
        
        $tgateway = $this->modelGateways->find()
             ->where([
                  'address' => $tnetwork->address, 
                  'id !=' => $data['id'],
                  'controller_id' => $data['controller_id']
             ])
             ->first();
        
        if($tgateway){
            
            $data_error = new \stdClass; 
            $data_error->other_tgateway = $tgateway;
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('Ya existe un (gateway) con el mismo address.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
   
            return false;
        }
        
       
        $tgateway = $this->modelGateways->get($data['id']);
        
        $length_net = explode('/', $tnetwork->address)[1];
        $data['address'] = $tnetwork->gateway . '/' . $length_net;
        
        $tgateway = $this->modelGateways->patchEntity($tgateway, $data);
       
        if(!$this->modelGateways->save($tgateway)){
            
            $data_error = new \stdClass; 
            $data_error->tgateway = $tgateway;
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al intentar editar el (gateway).'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
   
            return false;
        }
        
        return $tgateway;
          
     }
     
     public function deleteGateway($id){
          
        $tgateway = $this->modelGateways->get($id);
      
        if(!$this->modelGateways->delete($tgateway)){
            
            $data_error = new \stdClass;
            $data_error->tgateway = $tgateway;
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al intentar eliminar el Gateway'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
     
    
        return $tgateway;
     }
     
     public function deleteGatewayInController($controller, $api_id){
          return $this->deleteGatewayByApiIdApi($controller, $api_id);          
     }
     
     public function syncGateway($id){
          
        $tgateway =  $this->modelGateways->get($id);
        
        $this->_ctrl->loadModel('Controllers');
        $controller = $this->_ctrl->Controllers->get($tgateway->controller_id);

        if(!$this->integrationEnabled($controller)){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('Intento de sincronizacion de (gateway). Integracion desactivada.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
   
            return false;  
        }
    
        $tgateway = $this->syncGatewayApi($controller, $tgateway);
        
        if(!$tgateway){
            return false;
        }
        
        //limpia los api_id que existan duplicados
        $duplicates = $this->modelGateways->find()
        	->where([
        	'api_id' => $tgateway->api_id,
        	'id !=' => $tgateway->id,
        	'controller_id' => $controller->id
        	]);
        
        foreach($duplicates as $duplicated){
        
        	$duplicated->api_id = NULL;
        	$this->modelGateways->save($duplicated);
        }
      
      
        if(!$this->modelGateways->save($tgateway)){
            
            $data_error = new \stdClass; 
            $data_error->tgateway = $tgateway;
            $data_error->errors = $tgateway->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al intentar actualizar el (gateway) en la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
   
            return false;  
        }
       
        return $tgateway;
     }
     
     public function getGatewaysArray($controller_id){
          
          //busca el objeto en la base del template
        $gateways =  $this->modelGateways->find()->where(['controller_id' => $controller_id])->order(['address' => 'ASC'])->toArray();
        
        $array = [];
     
        foreach($gateways as $gateway){
            $array[$gateway->address] = $gateway;
        }
     
        return $array;
          
     }
     
     public function getGatewaysInController($controller){
          return $this->getGatewaysApi($controller);
     }
     
     
     //pools

     public function getPools($controller_id){
          
        $pools = $this->modelPools->find()
            ->where([
                'controller_id' => $controller_id,
                ]);
       
        return $pools;
          
     }
     
     public function addPool($data){
          
        $tpool = $this->modelPools->find()
             ->where([
                  'name' => $data['name'],
                  'controller_id' => $data['controller_id']
             ])
             ->first();
        
        if($tpool){ //se repite el nombre
            
            $data_error = new \stdClass; 
            $data_error->other_pool = $tpool;
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('Ya existe un (pool) con el mismo nombre.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
     
            return false;
        }
        
        
        $rango = false;
        
        if(strpos($data['ranges'], '-') !== false){  //rango
        
            $rango = true;
            
            $temp = explode('-', $data['ranges']);
            $ip_from = $temp[0];
            $ip_to = $temp[1];
            
            if(!Cidr::validIP($ip_from) || !Cidr::validIP($ip_to)){
                
                $data_error = new \stdClass; 
                $data_error->ip = $data['ranges'];
                
                $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                    'msg' => __('Pool: Algunas de las IP ingresada no el valida.'),
                    'data' => $data_error,
                    'flash' => false
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
         
                return false;
                
            } 
            
            $ip_from    = ip2long($temp[0]);
            $ip_to      = ip2long($temp[1]);
            
            if($ip_from >= $ip_to){
                
                $data_error = new \stdClass; 
                $data_error->ip = $data['ranges'];
                
                $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                    'msg' => __('Pool: {0} >= {1} Rango invalido.', $temp[0], $temp[1] ),
                    'data' => $data_error,
                    'flash' => false
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
         
                return false;
            } 
            
        } else { //no es un rango
        
            if(strpos($data['ranges'], '/') !== false){  //red
        
                $ip_tem = explode('/', $data['ranges']);
            
                if(!Cidr::validIP($ip_tem[0])){
                    
                    $data_error = new \stdClass; 
                    $data_error->ip = $data['ranges'];
                    
                    $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                        'msg' => __('Pool: La rango ingresado no es valida.{0}', $data['ranges']),
                        'data' => $data_error,
                        'flash' => false
                    ]);
                    
                    $this->_ctrl->getEventManager()->dispatch($event);
             
                    return false;
                    
                }
                
                $mask = intval($ip_tem[1]);
                 
                if($mask < 0 || $mask > 32){
                    
                    $data_error = new \stdClass; 
                    $data_error->ip = $data['ranges'];
                    
                    $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                        'msg' => __('Pool: La rango ingresado no es valida.{0}', $data['ranges']),
                        'data' => $data_error,
                        'flash' => false
                    ]);
                    
                    $this->_ctrl->getEventManager()->dispatch($event);
             
                    return false;
                }  
                
            }else{                
              

               $data_error = new \stdClass; 
               $data_error->ranges = $data['ranges'];

               $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                   'msg' => __('Pool: El rango ingresado no es valido.{0}', $data['ranges']),
                   'data' => $data_error,
                   'flash' => false
               ]);

               $this->_ctrl->getEventManager()->dispatch($event);

               return false;
                    
            }
        }
        
        $tpool = $this->modelPools->find()
             ->where([
                  'ranges' => $data['ranges'],
                  'controller_id' => $data['controller_id']
             ])
             ->first();
        
        if($tpool){ //ya existe la red en el controlador
            
            $data_error = new \stdClass; 
            $data_error->other_tpool = $tpool;
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('Ya existe un (pool) con la misma red.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
     
            return false;
        }
       
        
        $tpool = $this->modelPools->newEntity();
        $tpool = $this->modelPools->patchEntity($tpool, $data);
     
        if(!$this->modelPools->save($tpool)){ 
            
            $data_error = new \stdClass; 
            $data_error->tpool = $tpool;
            $data_error->errors = $tpool->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al intentar agregar el (pool) a la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
     
            return false;
        }
        
   
        return $tpool;
          
     }
     
     public function editPool($controller, $data){
          
          $tpool = $this->modelPools->find()
               ->where([
                    'name' => $data['name'],
                    'id !=' => $data['id'],
                    'controller_id' => $data['controller_id']
               ])
               ->first();
        
        if($tpool){
            
            $data_error = new \stdClass; 
            $data_error->other_pool = $tpool;
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('Ya existe un (pool) con el mismo nombre.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
   
            return false;
        }
        
        $rango = false;
        
        if(strpos($data['ranges'], '-') !== false){  //rango
        
            $rango = true;
            
            $temp = explode('-', $data['ranges']);
            $ip_from = $temp[0];
            $ip_to = $temp[1];
            
            if(!Cidr::validIP($ip_from) || !Cidr::validIP($ip_to)){
                
                $data_error = new \stdClass; 
                $data_error->ip = $data['ranges'];
                
                $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                    'msg' => __('Pool: Algunas de las IP ingresada no el valida.'),
                    'data' => $data_error,
                    'flash' => false
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
         
                return false;
                
            } 
            
            $ip_from    = ip2long($temp[0]);
            $ip_to      = ip2long($temp[1]);
            
            if($ip_from >= $ip_to){
                
                $data_error = new \stdClass; 
                $data_error->ip = $data['ranges'];
                
                $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                    'msg' => __('Pool: {0} >= {1} Rango invalido.', $temp[0], $temp[1] ),
                    'data' => $data_error,
                    'flash' => false
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
         
                return false;
            } 
            
        } else { //no es un rango
            
            if(strpos($data['ranges'], '/') !== false){  //red
        
                $ip_tem = explode('/', $data['ranges']);
            
                if(!Cidr::validIP($ip_tem[0])){
                    
                    $data_error = new \stdClass; 
                    $data_error->ip = $data['ranges'];
                    
                    $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                        'msg' => __('Pool: La rango ingresado no es valida.{0}', $data['ranges']),
                        'data' => $data_error,
                        'flash' => false
                    ]);
                    
                    $this->_ctrl->getEventManager()->dispatch($event);
             
                    return false;
                    
                }
                
                $mask = intval($ip_tem[1]);
                 
                if($mask < 0 || $mask > 32){
                    
                    $data_error = new \stdClass; 
                    $data_error->ip = $data['ranges'];
                    
                    $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                        'msg' => __('Pool: La rango ingresado no es valida.{0}', $data['ranges']),
                        'data' => $data_error,
                        'flash' => false
                    ]);
                    
                    $this->_ctrl->getEventManager()->dispatch($event);
             
                    return false;
                }  
                
            }else{
                
              

               $data_error = new \stdClass; 
               $data_error->ranges = $data['ranges'];

               $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                   'msg' => __('Pool: El rango ingresado no es valido.{0}', $data['ranges']),
                   'data' => $data_error,
                   'flash' => false
               ]);

               $this->_ctrl->getEventManager()->dispatch($event);

               return false;
               
                    
            }
        }
        
        $tpool = $this->modelPools->find()
             ->where([
                  'ranges' => $data['ranges'],
                  'id !=' => $data['id'],
                  'controller_id' => $data['controller_id']
             ])
             ->first();
        
        if($tpool){
            
            $data_error = new \stdClass; 
            $data_error->other_pool = $tpool;
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('Ya existe un (pool) con la misma red.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
   
            return false;
        }
       
        $tpool = $this->modelPools->get($data['id']);
        
        $last_name = $tpool->name; 
     
        $tpool = $this->modelPools->patchEntity($tpool, $data);
       
        if(!$this->modelPools->save($tpool)){
            
            $data_error = new \stdClass; 
            $data_error->tpool = $tpool;
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al intentar editar el (pool).'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
   
            return false;
        }
        
        
        //buscao si el dhcp servre esta usando
        
        $tdhcpServer =  $this->modelDhcpServers->find()
             ->where([
                  'address_pool' => $last_name, 
                  'controller_id' => $data['controller_id']
             ])
             ->first();
        
        if($tdhcpServer){
            
            $tdhcpServer->address_pool = $tpool->name;
            $this->modelDhcpServers->save($tdhcpServer);
        }

        return $tpool;
          
     }
     
     public function deletePool($id){
          
        $tpool = $this->modelPools->get($id);        
        
        $tdhcpServer =  $this->modelDhcpServers->find()
             ->where([
                  'address_pool' => $tpool->name,
                  'controller_id' => $tpool->controller_id
             ])
             ->first();
        
        if($tdhcpServer){
            
            $data_error = new \stdClass;
            $data_error->tpool = $tpool;
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('El pool {0} es address_pool de DHCP Server {1}',$tpool->name, $tdhcpServer->name),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }

      
        if(!$this->modelPools->delete($tpool)){
            
            $data_error = new \stdClass;
            $data_error->tpool = $tpool;
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al intentar eliminar el Pool'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
     
    
        return $tpool;
     }
     
     public function deletePoolInController($controller, $api_id){
          return $this->deletePoolInControllerApi($controller, $api_id);          
     }
     
     public function syncPool($id){
          
                
    
        $pool =  $this->modelPools->get($id);
        
        $this->_ctrl->loadModel('Controllers');
        $controller = $this->_ctrl->Controllers->get($pool->controller_id);

        if(!$this->integrationEnabled($controller)){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('Intento de sincronizacion de (pool). Integracion desactivada.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
   
            return false;  
        }
    
        $pool = $this->syncPoolApi($controller, $pool);
        
        if(!$pool){
            return false;
        }
        
        //limpia los api_id que existan duplicados
        $duplicates = $this->modelPools->find()
        	->where([
        	'api_id' => $pool->api_id,
        	'id !=' => $pool->id,
        	'controller_id' => $controller->id
        	]);
        
        foreach($duplicates as $duplicated){
        
        	$duplicated->api_id = NULL;
        	$this->modelPools->save($duplicated);
        }
      
      
        if(!$this->modelPools->save($pool)){
            
            $data_error = new \stdClass; 
            $data_error->pool = $pool;
            $data_error->errors = $pool->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al intentar actualizar el (pool) en la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
   
            return false;  
        }
       
        return $pool;
     }
     
     public function getPoolsArray($controller_id){
          
          //busca el objeto en la base del template
        $pools =  $this->modelPools->find()
             ->where([
                  'controller_id' => $controller_id
             ])
             ->order([
                  'name' => 'ASC'
             ])
             ->toArray();
        
        $array = [];
     
        foreach($pools as $pool){
            $array[$pool->name] = $pool;
        }
     
        return $array;
          
     }
     
     public function getPoolsInController($controller){
          return $this->getPoolsApi($controller);
     }
     
     
     
     //queues

     protected function addQueue($connection){          
   
        $data = [
            'name' => $connection->queue_name,
            'target' => ip2long($connection->ip),
            'comment' => $connection->comment,
            'connection_id' => $connection->id,
            'controller_id' => $connection->controller_id,
            'network_id' => $connection->network_id,
            'profile_id' => $connection->profile_id,
            'plan_id' => $connection->plan_id,
         ];
        
        $queue = $this->modelQueues->newEntity();
        $queue = $this->modelQueues->patchEntity($queue, $data);
         
        if(!$this->modelQueues->save($queue)){
            
            $data_error = new \stdClass; 
            $data_error->queue = $queue;
            $data_error->errors = $queue->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al Intentar agregar el Queue en la base de datos.'),
                'data' => $data_error,
                'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
          
        }
    
        return $queue;
     }
     
     protected function deleteQueue($connection){    
    
        $connection->queue = $this->modelQueues->find()
            ->where([
                'connection_id' => $connection->id
                ])
            ->first();
        
        if($connection->queue){
            
            if(!$this->modelQueues->delete($connection->queue)){
                
                $data_error = new \stdClass; 
                $data_error->queue = $connection->queue;
                
                $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                	'msg' => __('Error al intentar eliminar el (queue) a la base de datos.'),
                	'data' => $data_error
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
                
                return false;
            }
            
        }else{
            
            $data_error = new \stdClass; 
            $data_error->connection = $connection;
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('No se encontro el (queue).'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
        }
        
        return $connection;
     }
     
     protected function getQueue($connection){
       
        //busca el objeto en la base del template
        return $this->modelQueues->find()
            ->contain(['MikrotikDhcpTa_Plans', 'MikrotikDhcpTa_Networks', 'MikrotikDhcpTa_Profiles'])
            ->where([
                'MikrotikDhcpTa_Queues.connection_id' => $connection->id,
                'MikrotikDhcpTa_Queues.controller_id' => $connection->controller_id
            ])->first();
     }
     
     protected function editQueue($connection){

        $queue = $this->modelQueues->find()
            ->where([
                'name' => $connection->queue_name,
                'id !=' => $connection->queue->id, 
                'controller_id' => $connection->controller->id
                ])
            ->first();
        
        if($queue){
            
            $data_error = new \stdClass; 
            $data_error->other_queue = $queue;
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
            	'msg' => __('Ya existe un (queue) con el mismo nombre.'),
            	'data' => $data_error,
            	'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
   
            return false;
        }
  
   
        $queue = $this->modelQueues->get($connection->queue->id);
        
        $data = [
            'name' => $connection->queue_name,
            'target' => ip2long($connection->ip),
            'comment' => $connection->comment,
            'connection_id' => $connection->id,
            'controller_id' => $connection->controller_id,
            'network_id' => $connection->network_id,
            'profile_id' => $connection->profile_id,
            'plan_id' => $connection->plan_id,
         ];

        $queue = $this->modelQueues->patchEntity($queue, $data );
        
        $queue->profile = $this->modelProfiles->get($queue->profile_id);
        
        if(!$this->modelQueues->save($queue)){
            
            $data_error = new \stdClass; 
            $data_error->queue = $queue;
            $data_error->errors = $queue->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
            	'msg' => __('Error al intentar editar el (queue) de la base de datos.'),
            	'data' => $data_error,
            	'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
    
        return $queue;
     }
     
     public function deleteQueueInController($controller, $tqueue_api_id){
          return $this->deletedQueueByApiIdApi($controller, $tqueue_api_id);
     }
     
     public function getQueuesAndConnectionsArray($controller){
          
        $this->_ctrl->loadModel('Connections');
       
        $queues =  $this->getQueues($controller);
        
        $connections =  $this->_ctrl->Connections->find()
            ->where([
                'controller_id' => $controller->id
                ])
            ->order([
                'customer_code' => 'ASC'
                ]);
        
        $queueArray = [];
        
        foreach($queues as $queue){
            foreach($connections as $connection){
                if($queue->connection_id == $connection->id){
                    $queue->connection = $connection;
                    break;
                }
            }
            
            $queue['target'] = $queue->target . '/32';
            $queue['max_limit'] = $queue->profile->up ? $queue->profile->up .'/'. $queue->profile->down : '0/0';
            $queue['limit_at'] = $queue->profile->up_at_limit ? $queue->profile->up_at_limit .'/'. $queue->profile->down_at_limit : '0/0';
            $queue['burst_limit'] = $queue->profile->up_burst ? $queue->profile->up_burst .'/'. $queue->profile->down_burst : '0/0';
            $queue['burst_threshold'] = $queue->profile->up_threshold ? $queue->profile->up_threshold .'/'. $queue->profile->down_threshold : '0/0';
            $queue['burst_time'] = $queue->profile->up_time ? $queue->profile->up_time .'s/'. $queue->profile->down_time .'s': '0s/0s';
            $queue['priority'] = $queue->profile->priority ? $queue->profile->priority .'/'. $queue->profile->priority : '8/8';
            $queue['queue'] = $queue->profile->up_queue_type ? $queue->profile->up_queue_type .'/'. $queue->profile->down_queue_type : '';
         
            $queueArray[$queue->name] = $queue;
        }
        
        return $queueArray;
     }
     
     public function getQueuesInController($controller){
          return $this->getQueuesApi($controller);
     }
     
     protected function getQueues($controller){
        
        //busca el objeto en la base del template
        return $this->modelQueues->find()->contain(['MikrotikDhcpTa_Profiles'])
            ->where([
                'MikrotikDhcpTa_Queues.controller_id' => $controller->id
                ])
            ->order([
                'MikrotikDhcpTa_Queues.name' => 'ASC'
                ])
            ->map(function ($row) { 
                $row->target = long2ip($row->target);
                return $row;
            });
     }
     
     public function generateQueueName($connection){
          
        $internal_system_id =  Configure::read('project.internal_system_id');
        
        return 'h' . $internal_system_id . str_pad(dechex($connection->id), 4, '0', STR_PAD_LEFT); 
     }
     
     public function syncQueue($connection){
          
        if(!$this->integrationEnabled($connection->controller)){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
            'msg' => __('Intento de sincronizacion con integracion desabilitada.'),
            'data' => $data_error,
            'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
  
            return false;
        }   

            
        $queue =  $this->modelQueues->find()->contain(['MikrotikDhcpTa_Profiles'])
             ->where([
                 'MikrotikDhcpTa_Queues.connection_id' => $connection->id,
                 'MikrotikDhcpTa_Queues.controller_id' => $connection->controller_id,
                 ])
             ->first();
        
        $queue->comment = sprintf("%'.05d",  $connection->customer->code) . ' - ' . $connection->customer->name;
      
        $queue = $this->syncQueueApi($connection->controller, $queue);
        
        if(!$queue){
            return false;
        }
        
        //limpia los api_id que existan duplicados
        $duplicates = $this->modelQueues->find()
        	->where([
        	'api_id' => $queue->api_id,
        	'id !=' => $queue->id,
        	'controller_id' => $connection->controller_id
        	]);
        
        foreach($duplicates as $duplicated){
        
        	$duplicated->api_id = NULL;
        	$this->modelQueues->save($duplicated);
        }
      
        if(!$this->modelQueues->save($queue)){
            
            $data_error = new \stdClass; 
            $data_error->queue = $queue;
            $data_error->errors = $queue->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al intentar actualizar el Queue en la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            return false;
        }
        
        return $queue;
     }
     
     
     //dhcp leases

     protected function addLease($connection){
  
        $t_controller = $this->modelControllers->get($connection->controller_id);
        $t_dhcp_server = $this->modelDhcpServers->find()
            ->where([
                'controller_id' => $connection->controller_id
                ])
            ->first();
        
        $data = [
            'controller_id' => $connection->controller_id,
            'connection_id' => $connection->id,
            'address' => ip2long($connection->ip),
            'mac_address' => $connection->mac,
            'server' => $t_dhcp_server->name,
            'lease_time' => $t_dhcp_server->leases_time_gral,
            'address_lists' => $t_controller->address_list_name,
            'comment' => $connection->comment,
            ];
        
    
        $lease = $this->modelLeases->newEntity();
        $lease = $this->modelLeases->patchEntity($lease, $data);
        
        if(!$this->modelLeases->save($lease)){
            
            $data_error = new \stdClass; 
            $data_error->lease = $lease;
            $data_error->errors = $lease->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al Intentar agregar el (lease) en la base de datos.'),
                'data' => $data_error,
                'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
          
        }
    
        return $lease;
     }
     
     protected function editLease($connection){          
       
        $lease = $this->modelLeases->find()
            ->where([
                'connection_id' => $connection->id,
                'controller_id' => $connection->controller_id
                ])
            ->first();;
        
        if(!$lease){
            
            $data_error = new \stdClass; 
            $data_error->connection = $connection;
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('No se encontro el (lease) correspondiente a la conexión.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
         
        $t_controller = $this->modelControllers->get($connection->controller_id);
        $t_dhcp_server = $this->modelDhcpServers->find()
             ->where([
                  'controller_id' => $connection->controller_id
             ])
             ->first();
        
        $data = [
            'controller_id' => $connection->controller_id,
            'connection_id' => $connection->id,
            'address' => ip2long($connection->ip),
            'mac_address' => $connection->mac,
            'server' => $t_dhcp_server->name,
            'lease_time' => $t_dhcp_server->leases_time_gral,
            'address_lists' => $t_controller->address_list_name,
            'comment' => $connection->comment,
            ];
    
        
        $lease = $this->modelLeases->patchEntity($lease, $data);
        
        if(!$this->modelLeases->save($lease)){
            
            $data_error = new \stdClass; 
            $data_error->lease = $lease;
            $data_error->errors = $lease->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al Intentar editar el (lease) en la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            return false;
        }
   
       
        return $lease;
     }
     
     protected function deleteLease($connection){          
      
        $connection->lease = $this->modelLeases->find()
            ->where([
                'connection_id' => $connection->id
                ])
            ->first();
        
        if($connection->lease){
            
            if(!$this->modelLeases->delete($connection->lease)){
                
                $data_error = new \stdClass; 
                $data_error->leases = $connection->lease;
                
                $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                    'msg' => __('Error al Intentar eliminar el (lease) de la base de datos.'),
                    'data' => $data_error,
                    'flash' => false
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
        
                return false;
            }
               
        }else{
            
            $data_error = new \stdClass; 
            $data_error->connection = $connection;
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('No se encontro el (lease).'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
        }
        
        return $connection;
          
     }
     
     protected function getLease($connection){          
     
        //busca el objeto en la base del template
        return $this->modelLeases->find()
            ->where([
                'connection_id' => $connection->id,
                'controller_id' => $connection->controller_id
            ])->first();
     }
     
     public function deleteLeaseInController($controller, $tqueue_api_id){
          return $this->deletedLeaseByApiIdApi($controller, $tqueue_api_id);
     }
     
     public function getLeasesAndConnectionsArray($controller){
          
        $this->_ctrl->loadModel('Connections');
       
        $leases =  $this->getLeases($controller);
        
        $connections =  $this->_ctrl->Connections->find()
            ->where([
                'controller_id' => $controller->id
                ])
            ->order([
                'customer_code' => 'ASC'
                ]);
        
        $leasesArray = [];
        
        foreach($leases as $lease){
            foreach($connections as $connection){
                if($lease->connection_id == $connection->id){
                    $lease->connection = $connection;
                    break;
                }
            }
            
            $leasesArray[$lease->address] = $lease;
        }
        
        return $leasesArray;
     }
     
     public function getLeasesInController($controller){
          return $this->getLeasesApi($controller);
     }
     
     protected function getLeases($controller){     
        
        //busca el objeto en la base del template
        return $this->modelLeases->find()
            ->where([
                'controller_id' => $controller->id
                ])
            ->order([
                'address' => 'ASC'
                ])
            ->map(function ($row) { 
                $row->address = long2ip($row->address);
                $row->mac_address = implode(':',str_split(strtoupper($row->mac_address),2));
                return $row;
            });
     }
     
     public function syncLease($connection){
          
          if(!$this->integrationEnabled($connection->controller)){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                 'msg' => __('Intento de sincronizacion con integracion desactivada.'),
                 'data' => $data_error,
                 'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }        
     
        $lease =  $this->modelLeases->find()
        ->where([
            'connection_id' => $connection->id,
            'controller_id' => $connection->controller_id
            ])
        ->first();
        
        $lease->comment =  sprintf("%'.05d",  $connection->customer->code) . ' - ' . $connection->customer->name;
            
        $lease = $this->syncLeaseApi($connection->controller, $lease);
        
        if(!$lease){
            return false;
        }
        
        //limpia los api_id que existan duplicados
        $duplicates = $this->modelLeases->find()
        	->where([
        	'api_id' => $lease->api_id,
        	'id !=' => $lease->id,
        	'controller_id' => $connection->controller_id
        	]);
        
        foreach($duplicates as $duplicated){
        
        	$duplicated->api_id = NULL;
        	$this->modelLeases->save($duplicated);
        }
    
        if(!$this->modelLeases->save($lease)){
            
            $data_error = new \stdClass; 
            $data_error->lease = $lease;
            $data_error->errors = $lease->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                 'msg' => __('Error al Intentar actualizar el (lease) de la base de datos.'),
                 'data' => $data_error,
                 'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
          
            return false;
        }
        
        return $connection;
     }
     
     
     
     //address lists 

     public function editAddressList($connection){
          
        $this->_ctrl->loadModel('FirewallAddressLists');
        
        $firewallAddressList = $this->_ctrl->FirewallAddressLists->find()
            ->where([
                'connection_id' => $connection->id,
                'controller_id' => $connection->controller_id
                ])->first(); 
                
        if($firewallAddressList){
                    
            $firewallAddressList->before_address = $connection->before_ip;
            $firewallAddressList->address = $connection->ip;
            $firewallAddressList->comment = sprintf("%'.05d", $connection->customer->code)  . ' - ' . $connection->customer->name;
            
            if(!$this->_ctrl->FirewallAddressLists->save($firewallAddressList)){
                
                $data_error = new \stdClass; 
                $data_error->firewallAddressList = $firewallAddressList;
                $data_error->errors = $firewallAddressList->getErrors();
                
                $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                	'msg' => __('Error al Intentar editar el (address-list) en la base de datos.'),
                	'data' => $data_error,
                	'flash' => true
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);

                return false;
            }
            
        }
    
       
        return true;
     }
     
     public function syncAddressList($addressListId){       
    
        $this->_ctrl->loadModel('FirewallAddressLists');
            
        $addressList =  $this->_ctrl->FirewallAddressLists->find()
        ->where([
            'id' => $addressListId
            ])
        ->first();
      
        $this->_ctrl->loadModel('Controllers');
        $controller = $this->_ctrl->Controllers->get($addressList->controller_id);

        if(!$this->integrationEnabled($controller)){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                 'msg' => __('Intento de sincronizacion con integracion desabilitada.'),
                 'data' => $data_error,
                 'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
  
            return false;
        }
        
        $addressList = $this->syncAddressListApi($controller, $addressList);
            
        if(!$addressList){
            return false;
        }
        
        //limpia los api_id que existan duplicados
        $duplicates = $this->_ctrl->FirewallAddressLists->find()
        	->where([
        	'api_id' => $addressList->api_id,
        	'id !=' => $addressList->id,
        	'controller_id' => $addressList->controller_id
        	]);
        
        foreach($duplicates as $duplicated){
        
        	$duplicated->api_id = NULL;
        	$this->_ctrl->FirewallAddressLists->save($duplicated);
        }
      
        if(!$this->_ctrl->FirewallAddressLists->save($addressList)){
            
            $data_error = new \stdClass; 
            $data_error->addressList = $addressList;
            $data_error->errors = $addressList->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al intentar actualizar el (address-list) en la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
        return $addressList;
     }
     
     protected function syncAddressListsCorteByConnection($connection){
          
          if(!$this->integrationEnabled($connection->controller)){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
            'msg' => __('integracion deshabilitada.'),
            'data' => $data_error,
            'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
  
            return false;
        }
        
        $this->_ctrl->loadModel('FirewallAddressLists');
      
        $addressListCorte = $this->_ctrl->FirewallAddressLists->find()
            ->where([
                'connection_id' => $connection->id,
                'list' => 'Cortes', 
                'controller_id' => $connection->controller_id
                ])
            ->first();
        
        if(!$addressListCorte){ //no hay nada para sync
            return true;
        }
        
        $addressListCorte->comment = sprintf("%'.05d", $connection->customer->code)  . ' - ' . $connection->customer->name;
   
        $addressListCorte = $this->syncAddressListApi($connection->controller, $addressListCorte);
        
        if(!$addressListCorte){ // no salio bien la sync en el router
            return false;
        }
        
        //limpia los api_id que existan duplicados
        
        $duplicates = $this->_ctrl->FirewallAddressLists->find()
        	->where([
        	'api_id' => $addressListCorte->api_id,
        	'id !=' => $addressListCorte->id,
        	'controller_id' => $connection->controller_id
        	]);
        
        foreach($duplicates as $duplicated){
        
        	$duplicated->api_id = NULL;
        	$this->_ctrl->FirewallAddressLists->save($duplicated);
        }
        
        
        if(!$this->_ctrl->FirewallAddressLists->save($addressListCorte)){
            
            $data_error = new \stdClass; 
            $data_error->addressListCorte = $addressListCorte;
            $data_error->errors = $addressListCorte->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al Intentar actualizar el (address-list) de Corte en la base de datos .'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
        return $addressListCorte;
          
     }
     
     protected function syncAddressListsAvisoByConnection($connection){
          
          if(!$this->integrationEnabled($connection->controller)){
            
            $data_error = new \stdClass; 
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
            'msg' => __('integracion deshabilitada.'),
            'data' => $data_error,
            'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
  
            return false;
        }
 
        $this->_ctrl->loadModel('FirewallAddressLists');
        
        $addressListAviso = $this->_ctrl->FirewallAddressLists->find()
            ->where([
                'connection_id' => $connection->id,
                'list' => 'Avisos', 
                'controller_id' => $connection->controller_id
                ])
            ->first();
        
        if(!$addressListAviso){ // no hay nada para sync
             return true;
        }
            
        $addressListAviso->comment = sprintf("%'.05d", $connection->customer->code)  . ' - ' . $connection->customer->name;
       
        $addressListAviso = $this->syncAddressListApi($connection->controller, $addressListAviso);
        
        if(!$addressListAviso){ // no salio bien la sync en el router
            return false;
        }
        
        //limpia los api_id que existan duplicados
        $duplicates = $this->_ctrl->FirewallAddressLists->find()
        	->where([
        	'api_id' => $addressListAviso->api_id,
        	'id !=' => $addressListAviso->id,
        	'controller_id' => $connection->controller_id
        	]);
        
        foreach($duplicates as $duplicated){
        
        	$duplicated->api_id = NULL;
        	$this->_ctrl->FirewallAddressLists->save($duplicated);
        }
        
        if(!$this->_ctrl->FirewallAddressLists->save($addressListAviso)){
            
            $data_error = new \stdClass; 
            $data_error->addressListAviso = $addressListAviso;
            $data_error->errors = $addressListAviso->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al Intentar actualizar el (address-list) de Aviso en la base de datos .'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
            
        
        return $addressListAviso;
     }
     
     public function getAddressListsArray($controller_id){
          
        $this->_ctrl->loadModel('FirewallAddressLists');
        
        //busca el objeto en la base del template
        $addressLists =  $this->_ctrl->FirewallAddressLists->find()->where(['controller_id' => $controller_id]);
        
        $addressListsArray = [];
        foreach ($addressLists as $addressList) {
            
            $addressListsArray[$addressList->list.':'.$addressList->address] = $addressList;
        }
        
        return $addressListsArray;
     }
     
     public function deleteAddressListInController($controller, $address_list_api_id){
          return $this->deleteAddressListByApiIdApi($controller, $address_list_api_id);
     }
     
     public function getAddressListsInController($controller){
          return $this->getAddressListsApi($controller, ['Cortes', 'Avisos']);
     } 
     
     
     
     //connections
   
    public function addConnection($controller, $data){
 
        $this->_ctrl->loadModel('Connections'); 
       
        $tplan = $this->getPlan($controller, $data['plan_id']);      

        $data['user_id'] = $this->_ctrl->Auth->user()['id'];
        $data['controller_id'] = $controller->id;
        $data['service_id'] = $tplan->service_id;
        $data['profile_id'] = $tplan->profile->id;
        $data['mac'] = strtoupper($data['mac']);
        $data['comment'] = sprintf("%'.05d",  $data['customer_code']) . ' - ' . $data['customer_name'];
             
        $data['network_id'] = $data['pool_id'];
        
        if(!$data['validate_ip']){
            
            $data['network_id'] = null;
        }        

        $connection = $this->_ctrl->Connections->newEntity();
         
        //para provocar error de base de datos
//         $data['address'] = null;
         
        $connection = $this->_ctrl->Connections->patchEntity($connection, $data); 
     
        if(!$this->_ctrl->Connections->save($connection)){
            
            $data_error = new \stdClass; 
            $data_error->errors = $connection->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('Error al intentar agregar la conexión a la base de datos.'),
                'data' => $data_error,
                'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
        
        //name de queue
        if(!array_key_exists('queue_name', $data) || !$data['queue_name']){
           $connection->queue_name = $this->generateQueueName($connection);
        }else{
           $connection->queue_name = $data['queue_name'];
        }      
        
        $this->_ctrl->Connections->save($connection);
     
        $queue = $this->addQueue($connection);
        
        if($queue){
            
            if(!$this->addLease($connection)){
                
                $this->deleteQueue($connection);
                
                $this->_ctrl->Connections->delete($connection);
                
                return false;
            }
            
        }else{
    
            $this->_ctrl->Connections->delete($connection);
            
            return false;
        }
        
        return $this->sync($connection);
    }
    
    public function deleteConnection($connection){
        
        $connection->diff = false;
        
        if(!$connection->enabled){
            $connection = $this->enableConnection($connection);
            if(!$connection){
                return false;
            }
        }
        
        $connection = $this->deleteQueue($connection);
        
        if(!$connection){
            return false;
        }
        
        $connection = $this->deleteLease($connection);
        
        if(!$connection){
            return false;
        }
        
        $connection = $this->removeAvisoConnection($connection);
        
        if(!$connection){
            return false;
        }
        
        return $this->syncDeleteConnection($connection);
    }
    
    public function getConnection($connection){
        
        $connection->queue = $this->getQueue($connection);
        $connection->lease = $this->getLease($connection);
        return $connection;
    }

    public function editConnection($connection, $data)
    {
        $this->_ctrl->loadModel('Connections');       

        $tplan = $this->getPlan($connection->controller, $data['plan_id']);
        $data['service_id'] = $tplan->service_id;
        $data['profile_id'] = $tplan->profile->id;
        $data['mac'] = strtoupper($data['mac']);
        $data['comment'] = sprintf("%'.05d",  $connection->customer->code) . ' - ' . $connection->customer->name;

        $data['network_id'] = $data['pool_id'];

        if (!$data['validate_ip']) {

            $data['network_id'] = null;
        }

        $connection = $this->_ctrl->Connections->patchEntity($connection, $data);

        // $data['name'] = $data['queue_name'];

        //edito queue
        $connection->queue = $this->editQueue($connection, $data);
        if (!$connection->queue) {
            return false;
        }

        //edit lease
        $connection->lease = $this->editLease($connection);
        if (!$connection->lease) {
            return false;
        }

        //edito address list
        if (!$this->editAddressList($connection)) {
            return false;
        }

        //edito connection
        if (!$this->_ctrl->Connections->save($connection)) {

            $data_error = new \stdClass; 
            $data_error->connection_id = $connection->id;
            $data_error->errors = $connection->errors;

            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al intentar editar la conexión en la base de datos.'),
                'data' => $data_error,
                'flash' => true
            ]);

            $this->_ctrl->getEventManager()->dispatch($event);

            return false;
        }

        return $this->sync($connection);
    }

    public function sync($connection){
        
        $connection->controller =  $this->_ctrl->Connections->Controllers->get($connection->controller_id);

        if(!$this->integrationEnabled($connection->controller)){
            return $connection;
        }

        $connection->customer   =  $this->_ctrl->Connections->Customers->get($connection->customer_code);
        
        return $this->syncConnection($connection);
    }
    
    public function syncDeleteConnection($connection){
         
         
        //$this->log($connection, 'debug');
        
        $this->deletedQueueApi($connection->controller, $connection->queue);
        $this->deletedLeaseApi($connection->controller, $connection->lease);
        
        if($connection->addressListCorte){
            $this->deleteAddressListApi($connection->controller, $connection->addressListCorte);
        }
        
        if($connection->addressListAviso){
            $this->deleteAddressListApi($connection->controller, $connection->addressListAviso);
        }
        
        return $connection;
    }

    public function syncConnection($connection){
            
        $connection->diff =  false;
    
        if(!$this->syncQueue($connection)) {
            $connection->diff =  true;
        }
        
        if(!$this->syncLease($connection)) {
            $connection->diff =  true;
        }
        
        if(!$this->syncAddressListsCorteByConnection($connection)){
            $connection->diff =  true;
        }
        
        if(!$this->syncAddressListsAvisoByConnection($connection)){
            $connection->diff =  true;
        }
   
        $this->_ctrl->loadModel('Connections');
        $this->_ctrl->Connections->save($connection);
   
        
        return $connection;
        
    }
    
    public function addAvisoConnection($connection){
         
        if(!$this->getStatus($connection->controller)){
             return false;
        } 
 
        $this->_ctrl->loadModel('FirewallAddressLists');
        
        $fal = $this->_ctrl->FirewallAddressLists->find()
            ->where([
                'address' => $connection->ip,
                'list' => 'Avisos',
                'controller_id' => $connection->controller_id
                ])
            ->first();
        
        if($fal){
            return true;
        }
        
        $firewallAddressList = $this->_ctrl->FirewallAddressLists->newEntity();
                 
        $data = [
             'list' => 'Avisos',
             'address' => $connection->ip,
             'before_address' => $connection->ip,
             'comment' => sprintf("%'.05d", $connection->customer->code)  . ' - ' . $connection->customer->name,
             'connection_id' => $connection->id,
             'controller_id' => $connection->controller->id
             ];
             
        $firewallAddressList = $this->_ctrl->FirewallAddressLists->patchEntity($firewallAddressList, $data);
        
        if(!$this->_ctrl->FirewallAddressLists->save($firewallAddressList)){
            
            $data_error = new \stdClass; 
            $data_error->firewallAddressList = $firewallAddressList;
            $data_error->errors = $firewallAddressList->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al Intentar agregar el (address-list) del Aviso en la base de datos.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
            
        }
  
        
        return $this->sync($connection);
    }
    
    public function removeAvisoConnection($connection, $sync = false){

        $this->_ctrl->loadModel('FirewallAddressLists');
        
        $connection->addressListAviso = $this->_ctrl->FirewallAddressLists->find()
            ->where([
                'connection_id' => $connection->id,
                'list' => 'Avisos',
                ])
            ->first();
        
        if($connection->addressListAviso){
            
            if($this->integrationEnabled($connection->controller) && $sync){  
                
                if(!$this->deleteAddressListApi($connection->controller, $connection->addressListAviso)){
                    $connection->diff = true;
                    $this->_ctrl->loadModel('Connections');
                    $this->_ctrl->Connections->save($connection);
                }
            }
            
            if(!$this->_ctrl->FirewallAddressLists->delete($connection->addressListAviso)){
                
                $data_error = new \stdClass; 
                $data_error->addressListAviso = $connection->addressListAviso;
                
                $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Error al Intentar eliminar el (address-list) de Aviso de la base de datos.'),
                'data' => $data_error,
                'flash' => false
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
                
                return false;
            }
            
        }
        
   
        
        return $connection;
    } 
    
    public function disableConnection($connection){
            
        $this->_ctrl->loadModel('FirewallAddressLists');
        $firewallAddressList = $this->_ctrl->FirewallAddressLists->newEntity();
                 
        $data = [
             'list' => 'Cortes',
             'address' => $connection->ip,
             'before_address' => $connection->ip,
             'comment' => sprintf("%'.05d", $connection->customer->code)  . ' - ' . $connection->customer->name,
             'connection_id' => $connection->id,
             'controller_id' => $connection->controller->id
             ];
             
        $firewallAddressList = $this->_ctrl->FirewallAddressLists->patchEntity($firewallAddressList, $data);
        
        if(!$this->_ctrl->FirewallAddressLists->save($firewallAddressList)){
            
            $data_error = new \stdClass; 
            $data_error->firewallAddressList = $firewallAddressList;
            $data_error->errors = $firewallAddressList->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
            	'msg' => __('Error al Intentar agregar el (address-list) de Corte en la base de datos.'),
            	'data' => $data_error,
            	'flash' => true
            ]);
            
            return false;
        }
   
        return $this->sync($connection);
        
    }
    
    public function enableConnection($connection, $sync = false){
    
        $this->_ctrl->loadModel('FirewallAddressLists');
        
        $connection->addressListCorte = $this->_ctrl->FirewallAddressLists->find()
            ->where([
                'connection_id' => $connection->id,
                'list' => 'Cortes', 
                'controller_id' => $connection->controller_id
                ])
            ->first();
        
        if($connection->addressListCorte){
            
            if($this->integrationEnabled($connection->controller) && $sync){
                                
                if(!$this->deleteAddressListApi($connection->controller, $connection->addressListCorte)){
                    $connection->diff = true;
                    $this->_ctrl->loadModel('Connections');
                    $this->_ctrl->Connections->save($connection);
                }
            }
            
            if(!$this->_ctrl->FirewallAddressLists->delete($connection->addressListCorte)){
                
                $data_error = new \stdClass; 
                $data_error->addressList = $connection->addressListCorte;
                $data_error->errors = $connection->addressListCorte->getErrors();
                
                $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                	'msg' => __('Error al Intentar eliminar el (address-list) de Corte de la base de datos.'),
                	'data' => $data_error,
                	'flash' => true
                ]);
                
                $this->_ctrl->getEventManager()->dispatch($event);
             
                return false;
            }
        
        }else{
            
            $data_error = new \stdClass; 
            $data_error->connection = $connection;
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('No se encuentra el (address-list).'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
        }
   
     
        
        return $connection;
    }
    
    public function rollbackConnection($connection_id){
        
        $this->_ctrl->loadModel('ConnectionsTransactions');
        $connectionTransaction = $this->_ctrl->ConnectionsTransactions->find()
            ->where([
                'connection_id' => $connection_id
                ])->first();
                
        $connectionRestore = unserialize($connectionTransaction->data);
  
        $connectionArray = json_decode(json_encode($connectionRestore), true);   
        
        $this->_ctrl->loadModel('Connections');
        $connection = $this->_ctrl->Connections->find()->where(['id' => $connection_id])->first();
        
        $tqueue_data = $connectionArray['queue'];
        $tlease_data = $connectionArray['lease'];
        
        $connectionArray['service'] = null;
        $connectionArray['controller'] = null;
        $connectionArray['customer'] = null;
        $connectionArray['queue'] = null;
        $connectionArray['lease'] = null;
        
        if(!$connection){
            $connection = $this->_ctrl->Connections->newEntity();
        }
        
        $connection = $this->_ctrl->Connections->patchEntity($connection, $connectionArray);
           
        if(!$this->_ctrl->Connections->save($connection)){
           
            $data_error = new \stdClass; 
            $data_error->errors = $connection->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('Error al intentar agregar la conexion a la base de datos.'),
                'data' => $data_error,
                'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
        $tQueue = $this->modelQueues->find()->where(['id' => $tqueue_data['id']])->first();
        
        $tqueue_data['profile'] = null;
        $tqueue_data['network'] = null;
        $tqueue_data['plan'] = null;
        
        if(!$tQueue){
            $tQueue = $this->modelQueues->newEntity();
        }
        
        $tQueue = $this->modelQueues->patchEntity($tQueue, $tqueue_data);
        
        if(!$this->modelQueues->save($tQueue)){
            
            $data_error = new \stdClass; 
            $data_error->errors = $tQueue->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('Error al intentar agregar la Queue a la base de datos.'),
                'data' => $data_error,
                'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
    
        $tlease = $this->modelLeases->find()->where(['id' => $tlease_data['id']])->first();
        
        if(!$tlease){
            $tlease = $this->modelLeases->newEntity();
        }
        
        $tlease = $this->modelLeases->patchEntity($tlease, $tlease_data);
           
        if(!$this->modelLeases->save($tlease)){          
            
            $data_error = new \stdClass; 
            $data_error->errors = $tlease->getErrors();
            
            $event = new Event('MikrotikDhcpTaComponent.Warning', $this, [
                'msg' => __('Error al intentar agregar el (lease) a la base de datos.'),
                'data' => $data_error,
                'flash' => true
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
       
        
        $this->sync($connectionRestore);
        
        $this->_ctrl->ConnectionsTransactions->delete($connectionTransaction);
        
        return true;

    }
     
     
     
    public function applyConfigAccessNewCustomer($controller, $ip_server){       
        
        $network = $this->modelNetworks->find()
            ->where([
                'access_new_customer' => true,
                'controller_id' => $controller->id,
                ])->first();

                $this->_ctrl->log($network, 'debug');
        
        if(!$network){
            
            $data_error = new \stdClass; 
            $data_error->conntroller = $controller;
            
            $event = new Event('MikrotikDhcpTaComponent.Error', $this, [
                'msg' => __('Debe seleccionar un network para aplicar esta configuración.'),
                'data' => $data_error,
                'flash' => false
            ]);
            
            $this->_ctrl->getEventManager()->dispatch($event);
            
            return false;
        }
        
        return $this->applyConfigAccessNewCustomerApi($controller, $ip_server, $network->address);
    }
    
    
    public function getRateDataQueue($connection){
        
        $response = $this->getAllDataQueueApi($connection->controller, $this->getQueue($connection));
        
        $scale = 1024;
        
        if($response){
            
            $rate = $response->getProperty('rate');
            
            //$this->log($rate, 'debug');
            
            $rate = explode('/', $rate);
            $rate = ['up' => intval($rate[0]), 'down' => intval($rate[1])];
            
            $rate['up']     /= $scale;
            $rate['down']   /= $scale;
            
            $rate['up']     /= $scale;
            $rate['down']   /= $scale;
            
            $rate['up']     = round($rate['up'], 2);
            $rate['down']   = round($rate['down'], 2);
            
            return $rate;
        }
        
        return false;
    }
    
    //temporal ...
    public function addWebProxyAccessApiTemp($controller, $addressList){
        return $this->addWebProxyAccessApi($controller, $addressList);
    }

}