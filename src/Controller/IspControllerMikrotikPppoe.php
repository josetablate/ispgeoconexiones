<?php
namespace App\Controller;

use App\Controller\IspControllerMikrotik;


abstract class IspControllerMikrotikPppoe extends IspControllerMikrotik
{
    public function initialize()
    {
        parent::initialize();        
    
    }   
          
     
    //CONFIGUARATIONS                                                   

    //pools
    
    abstract protected function getPoolsByController();    
    abstract protected function addPool();    
    abstract protected function deletePool();
    abstract protected function editPool();    
    abstract protected function getRangeNetwork();    
    abstract protected function movePool();       
    
    //profiles
    
    abstract protected function getProfilesByController();
    abstract protected function addProfile();
    abstract protected function deleteProfile();
    abstract protected function editProfile();  
    
    //pppoe servers
    
    abstract protected function getPppoeServersByController();
    abstract protected function addPppoeServer();
    abstract protected function deletePppoeServer();   
     
   
     
    //SYNCRONIZATIONS    
    
    //sync secrets

    abstract protected function syncSecrets();
    abstract protected function syncSecretIndi();
    abstract protected function refreshSecrets();  
    abstract protected function deleteSecretInController();
    
    abstract protected function diffSecrets($controller, $file);    
    abstract protected function clearSecrets();   
    
    
    //sync actives

    abstract protected function syncActives();  
    abstract protected function refreshActives();
    abstract protected function deleteActiveInController();
    
    abstract protected function diffActives($controller, $file);    
    abstract protected function clearActives();    
    
    
     //sync queues

    abstract protected function syncQueues();    
    abstract protected function syncQueueIndi();  
    abstract protected function refreshQueues();  
    abstract protected function deleteQueueInController();
    
    abstract protected function diffQueues($controller, $file);    
    abstract protected function clearQueues(); 
    
    
    // sync profiles

    abstract protected function syncProfiles();
    abstract protected function syncProfileIndi();
    abstract protected function refreshProfiles();                                                        
    abstract protected function deleteProfileInController();
                                                        
    abstract protected function diffProfiles($controller);    
    abstract protected function clearProfiles();
    
    
     // sync address list

    abstract protected function syncAddressLists();    
    abstract protected function syncAddressListIndi();  
    abstract protected function refreshAddressLists();
    abstract protected function deleteAddressListInController();    
                                                        
    abstract protected function diffAddressLists($controller, $file);    
    abstract protected function clearAddressLists();
    
    
    //sync pppoe-servers
    
    abstract protected function syncPppoeServers();
    abstract protected function syncPppoeServerIndi();
    abstract protected function refreshPppoeServers();    
    abstract protected function deletePppoeServerInController();
                                                        
    abstract protected function diffPppoeServers($controller);
    abstract protected function clearPppoeServers();   
    
  
}

















