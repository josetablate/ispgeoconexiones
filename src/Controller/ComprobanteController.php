<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\Component\Admin\FiscoAfipComp;
use App\Controller\Component\Admin\FiscoAfipCompPdf;
use Cake\I18n\Time;

/**
 * Comprobante Controller
 *
 * @property \App\Model\Table\ComprobanteTable $Comprobante
 */
class ComprobanteController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Accountant', [
             'className' => '\App\Controller\Component\Admin\Accountant'
        ]);

        $this->loadComponent('FiscoAfipComp', [
             'className' => '\App\Controller\Component\Admin\FiscoAfipComp'
        ]);

        $this->loadComponent('FiscoAfip', [
             'className' => '\App\Controller\Component\Admin\FiscoAfip'
        ]);

        $this->loadComponent('FiscoAfipCompPdf', [
             'className' => '\App\Controller\Component\Admin\FiscoAfipCompPdf'
        ]);
    }

    public function isAuthorized($user = null)
    {
        if ($this->request->getParam('action') == 'getAllByCustomer') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function add()
    {
        $parament = $this->request->getSession()->read('paraments');

        if ($this->request->is('ajax')) {

            $this->loadModel('Customers');

            $paraments = $this->request->getSession()->read('paraments');

            $data = $this->request->input('json_decode');

            $response = false;

            $customer = $this->Customers->get($data->customer_code, ['contain' => ['Cities']]);

            switch ($data->type) {

                case 'XXX': //PRESUX
                case '001': //FACTURA A
                case '006': //FACTURA B
                case '011': //FACTURA C

                    $invoice  = [
                        'customer' => $customer,
                        'concept_type' => $data->concept_type,
                        'date' => new Time($data->date),
                        'date_start' => new Time($data->date_start),
                        'date_end' => new Time($data->date_end),
                        'duedate' => new Time($data->duedate),
                        'debts' => $data->concepts,
                        'discounts' => [],
                        'tipo_comp' => $data->type,
                        'comments' => $data->comments,
                        'manual' => true,
                        'business_id' => $data->business_id,
                        'connection_id' => $data->connection_id,
                        'seating_number' => NULL,
                        'cond_vta' => $data->cond_venta,
                    ];

                    // crear la factura

                    $invoice = $this->FiscoAfipComp->invoice($invoice);

                    if ($invoice) {

                      // creo asiento

                      if ($this->Accountant->isEnabled()) {

                            $seating = $this->Accountant->addSeating([
                                'concept' => 'Factura Manual',
                                'comments' => '',
                                'seating' => null,
                                'account_debe' => $customer->account_code,
                                'account_haber' => $paraments->accountant->acounts_parent->surcharge,
                                'value' => $invoice->total,
                                'redirect' => null
                            ]);

                            if ($seating) {
                                $invoice->seating_number = $seating->number;
                                $this->Invoices->save($invoice);
                            }
                        }

                       // informo afip si no es de tipo X

                       if ($this->FiscoAfip->informar($invoice, FiscoAfipComp::TYPE_INVOICE)) {

                           //generar PDF

                           $this->Invoices->save($invoice);

                           // $this->FiscoAfipCompPdf->invoice($invoice);

                           $response['tipo_comp'] =  $invoice->tipo_comp;
                           $response['comp_id'] =  $invoice->id;

                       } else {
                           $this->FiscoAfipComp->rollbackInvoice($invoice, true);
                       }

                    } else {
                        $this->FiscoAfipComp->rollbackInvoice($invoice, true);
                    }
                    break;

                case 'NDX': //NOTA DE DEBITO X
                case '002': //NOTA DE DEBITO A
                case '007': //NOTA DE DEBITO B
                case '012': //NOTA DE DEBITO C

                    $debitNote  = [
                        'customer' => $customer,
                        'concept_type' => $data->concept_type,
                        'date' => new Time($data->date),
                        'date_start' => new Time($data->date_start),
                        'date_end' => new Time($data->date_end),
                        'duedate' => new Time($data->duedate),
                        'concepts' => $data->concepts,
                        'tipo_comp' => $data->type,
                        'comments' => $data->comments,
                        'manual' => true,
                        'business_id' => $data->business_id,
                        'connection_id' => $data->connection_id,
                        'seating_number' => NULL,
                        'cond_vta' => $data->cond_venta,
                    ];

                    $debitNote = $this->FiscoAfipComp->debitNote($debitNote);

                    if ($debitNote) {

                        // creo asiento

                        if ($this->Accountant->isEnabled()) {

                            $seating = $this->Accountant->addSeating([
                                'concept' => 'Nota Débito Manual',
                                'comments' => '',
                                'seating' => null,
                                'account_debe' => $customer->account_code,
                                'account_haber' => $paraments->accountant->acounts_parent->surcharge,
                                'value' => $debitNote->total,
                                'redirect' => null
                            ]);

                            $debitNote->seating_number = $seating->number; 

                            if ($seating) {
                                $debitNote->seating_number = $seating->number;
                                $this->DebitNotes->Save($debitNote);
                            }
                        }
 
                        // informo afip si no es de tipo X

                        if ($this->FiscoAfip->informar($debitNote, FiscoAfipComp::TYPE_DEBIT_NOTE)) {

                            $this->DebitNotes->save($debitNote);

                            // geenrar PDF

                            // $this->FiscoAfipCompPdf->debitNote($debitNote);

                            $response['tipo_comp'] =  $debitNote->tipo_comp;
                            $response['comp_id'] =  $debitNote->id;
                        } else {
                            $this->FiscoAfipComp->rollbackDebitNote($debitNote, true);
                        }
                    } else {
                        $this->FiscoAfipComp->rollbackDebitNote($debitNote, true);
                    }
                    break;

                case 'NCX': //NOTA DE CREDITO X
                case '003': //NOTA DE CREDITO A
                case '008': //NOTA DE CREDITO B
                case '013': //NOTA DE CREDITO C

                    $creditNote  = [
                        'customer' => $customer,
                        'concept_type' => $data->concept_type,
                        'date' => new Time($data->date),
                        'date_start' => new Time($data->date_start),
                        'date_end' => new Time($data->date_end),
                        'duedate' => new Time($data->duedate),
                        'concepts' => $data->concepts,
                        'tipo_comp' => $data->type,
                        'comments' => $data->comments,
                        'manual' => true,
                        'business_id' => $data->business_id,
                        'connection_id' => $data->connection_id,
                        'seating_number' => NULL,
                        'cond_vta' => $data->cond_venta,
                    ];

                    $creditNote = $this->FiscoAfipComp->creditNote($creditNote);

                    if ($creditNote) {

                       // creo asiento

                       if ($this->Accountant->isEnabled()) {

                            $seating = $this->Accountant->addSeating([
                                'concept' => 'Nota Crédito Manual',
                                'comments' => '',
                                'seating' => null,
                                'account_debe' => $paraments->accountant->acounts_parent->bonus,
                                'account_haber' => $customer->account_code,
                                'value' => $creditNote->total,
                                'redirect' => null
                            ]);
          
                            $creditNote->seating_number = $seating->number; 

                            if ($seating) {
                                $creditNote->seating_number = $seating->number;
                                $this->CreditNotes->Save($creditNote);
                            }
                        }

                        // informo afip si no es de tipo X

                        if ($this->FiscoAfip->informar($creditNote, FiscoAfipComp::TYPE_CREDIT_NOTE)) {

                           $this->CreditNotes->save($creditNote);

                           // generar PDF

                           // $this->FiscoAfipCompPdf->creditNote($creditNote);

                           $response['tipo_comp'] =  $creditNote->tipo_comp;
                           $response['comp_id'] =  $creditNote->id;
                        } else {
                           $this->FiscoAfipComp->rollbackCreditNote($creditNote, true);
                        }
                    } else {
                        $this->FiscoAfipComp->rollbackCreditNote($creditNote, true);
                    }
                    break;

                case '000': //PRESUPUESTO

                    $presupuesto  = [
                        'customer' => $customer,
                        'concept_type' => $data->concept_type,
                        'date' => new Time($data->date),
                        'date_start' => new Time($data->date_start),
                        'date_end' => new Time($data->date_end),
                        'duedate' => new Time($data->duedate),
                        'debts' => $data->concepts,
                        'discounts' => [],
                        'tipo_comp' => $data->type,
                        'comments' => $data->comments,
                        'manual' => true,
                        'business_id' => $data->business_id,
                        'connection_id' => $data->connection_id,
                        'seating_number' => NULL,
                        'cond_vta' => $data->cond_venta,
                    ];

                    // crear la presupuesto

                    $presupuesto = $this->FiscoAfipComp->presupuesto($presupuesto);

                    if ($presupuesto) {

                        // generar PDF

                        // $this->FiscoAfipCompPdf->presupuesto($presupuesto);

                        $response['tipo_comp'] =  $presupuesto->tipo_comp;
                        $response['comp_id'] =  $presupuesto->id;

                    } else {

                        $this->FiscoAfipComp->rollbackPresupuesto($presupuesto);
                    }

                    break;
            }

            $this->set('response', $response);
        }

        $business = [];
        $business[''] = 'Seleccione Empresa';
        foreach ($parament->invoicing->business as $b) {
            if($b->enable){
                $business[$b->id] = $b->name . ' (' . $b->address. ')';
            }            
        }

        $this->set(compact('business'));
    }

    public function getAllByCustomer()
    {
        $code = $this->request->getQuery('customer_code');

        $comprobantes_list = [];

        $this->loadModel('Invoices');
        $this->loadModel('Receipts');
        $this->loadModel('CreditNotes');
        $this->loadModel('DebitNotes');

        $invoices = $this->Invoices->find()
            ->where([
                'customer_code' => $code
            ])
            ->order(['date' => 'ASC']);

        $receipts = $this->Receipts->find()
            ->where([
                'customer_code' => $code
            ])
            ->order(['date' => 'ASC']);
  
        $creditNotes = $this->CreditNotes->find()
            ->where([
                'customer_code' => $code
            ])
            ->order(['date' => 'ASC']);

        $debitNotes = $this->DebitNotes->find()
            ->where([
                'customer_code' => $code
            ])
            ->order(['date' => 'ASC']);

        foreach ($invoices as $invoice) {

            $comprobantes_list[] = [
                'date' => $invoice->date,
                'date_start' => $invoice->date_start,
                'date_end' => $invoice->date_end,
                'duedate' => $invoice->duedate,
                'tipo_comp' => $invoice->tipo_comp,
                'pto_vta' => $invoice->pto_vta,
                'num' => $invoice->num,
                'total' => $invoice->total,
                'cae' => $invoice->cae,
            ];
        }

        foreach ($receipts as $receipt) {

            $comprobantes_list[] = [
                'date' => $receipt->date,
                'date_start' => $receipt->date_start,
                'date_end' => $receipt->date_end,
                'duedate' => $receipt->duedate,
                'tipo_comp' => $receipt->tipo_comp,
                'pto_vta' => $receipt->pto_vta,
                'num' => $receipt->num,
                'total' => $receipt->total,
                'cae' => $receipt->cae,
            ];
        }

        foreach ($creditNotes as $creditNote) {

            $comprobantes_list[] = [

                'date' => $creditNote->date,
                'date_start' => $creditNote->date_start,
                'date_end' => $creditNote->date_end,
                'duedate' => $creditNote->duedate,
                'tipo_comp' => $creditNote->tipo_comp,
                'pto_vta' => $creditNote->pto_vta,
                'num' => $creditNote->num,
                'total' => $creditNote->total,
                'cae' => $creditNote->cae,
            ];
        }

        foreach ($debitNotes as $debitNote) {
            $comprobantes_list[] = [
                'date' => $debitNote->date,
                'date_start' => $debitNote->date_start,
                'date_end' => $debitNote->date_end,
                'duedate' => $debitNote->duedate,
                'tipo_comp' => $debitNote->tipo_comp,
                'pto_vta' => $debitNote->pto_vta,
                'num' => $debitNote->num,
                'total' => $debitNote->total,
                'cae' => $debitNote->cae,
            ];
        }

        $this->set(compact('comprobantes_list'));
        $this->set('_serialize', ['comprobantes_list']);
    }
}
