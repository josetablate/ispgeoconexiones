<?php
namespace App\Controller;

use Cake\Core\Configure;
use App\Controller\AppController;
use PEAR2\Net\ControllerOS;
use PEAR2\Net\ControllerOS\SocketException;
use PEAR2\Net\ControllerOS\DataFlowException;
use Cidr;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;
use App\Controller\Component\Admin\FiscoAfipComp;
use Cake\Event\Event;

/**
 * Connections Controller
 * ABM de conexion en la base y en el controller.  
 *
 * @property \App\Model\Table\ConnectionsTable $Connections
 */
class ConnectionsController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('IntegrationRouter', [
            'className' => '\App\Controller\Component\Integrations\IntegrationRouter'
        ]); 

        $this->loadComponent('CurrentAccount', [
            'className' => '\App\Controller\Component\Admin\CurrentAccount'
        ]);

        $this->loadComponent('FiscoAfipComp', [
            'className' => '\App\Controller\Component\Admin\FiscoAfipComp'
        ]);

        $this->loadComponent('FiscoAfipCompPdf', [
            'className' => '\App\Controller\Component\Admin\FiscoAfipCompPdf'
        ]);

        $this->loadComponent('FiscoAfip', [
            'className' => '\App\Controller\Component\Admin\FiscoAfip'
        ]);
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow('lockedMasiveFromCron');
    }

    public function isAuthorized($user = null)
    {
        if ($this->request->getParam('action') == 'findIPAjax') {
            return true;
        }

        if ($this->request->getParam('action') == 'getPreviewSecretCredentials') {
            return true;
        }

        if ($this->request->getParam('action') == 'clearErrors') {
            return true;
        }

        if ($this->request->getParam('action') == 'getConnections') {
            return true;
        }

        if ($this->request->getParam('action') == 'getConnectionsByController') {
            return true;
        }

        if ($this->request->getParam('action') == 'getConnectionsByFilter') {
            return true;
        }

        if ($this->request->getParam('action') == 'getConnectionCustomer') {
            return true;
        }

        if ($this->request->getParam('action') == 'getConnectionNoInstalationsCustomer') {
            return true;
        }

        if ($this->request->getParam('action') == 'editBySecretId') {
            return true;
        }

        if ($this->request->getParam('action') == 'lockedMasive') {
            return true;
        }

        if ($this->request->getParam('action') == 'unlockedMasive') {
            return true;
        }

        if ($this->request->getParam('action') == 'editConnectionFromDebt') {
            return true;
        }

        if ($this->request->getParam('action') == 'changeLabels') {
            return true;
        }

        if ($this->request->getParam('action') == 'getConnectionsSelectByCustomer') {
            return true;
        }

        if ($this->request->getParam('action') == 'administrative') {
            return true;
        }

        if ($this->request->getParam('action') == 'getCounterUnsync') {
            return true;
        }

        if ($this->request->getParam('action') == 'importAndros') {
            return true;
        }

        if ($this->request->getParam('action') == 'lockedMasiveFromCron') {
            return true;
        }

        if ($this->request->getParam('action') == 'getConnectionsAdministrative') {
            return true;
        }
        
        if ($this->request->getParam('action') == 'getAccountsStatus') {
            return true;
        }

        if ($this->request->getParam('action') == 'applyMassiveDebt') {
            return true;
        }

        if ($this->request->getParam('action') == 'applyMassiveDiscount') {
            return true;
        }

        if ($this->request->getParam('action') == 'lockedMasiveFromCustomers') {
            return true;
        }

        if ($this->request->getParam('action') == 'unlockedMasiveFromCustomers') {
            return true;
        }

        if ($this->request->getParam('action') == 'getConnectionCustomerFromTicket') {
            return true;
        }

        if ($this->request->getParam('action') == 'getNameMonth') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function index($last_conn_pppoe_id = null)
    {
        $this->loadModel('Controllers');
        $controllers = $this->Controllers
            ->find()
            ->where([
                'enabled' => TRUE, 
                'deleted' => FALSE
            ]);

        $controllersArray = [];
        $controllersArray[''] = 'Seleccione';
        $controllersArray['-2'] = 'Recientes';
        $controllersArray['-1'] = 'Todos';

        foreach ($controllers as $controller) {
            $controllersArray[$controller->id] = $controller->name;
        }

        $this->loadModel('Labels');
        $labels_array = $this->Labels->find()->where(['entity' => 1])->toArray();

        $labels = [];
        $labels[] = [
            'id' => 0,
            'text' => 'Sin etiquetas',
            'entity' => 0,
            'color_back' => '#000000',
            'color_text' => '#ffffff',
            'enabled' => 1,
        ];

        foreach ($labels_array as $label) {
            $labels[] = $label;
        }

        $areas = [];
        $this->loadModel('Areas');
        $areas_model = $this->Areas->find();
        foreach ($areas_model as $a) {
            $areas[$a->id] = $a->name;
        }

        $controllers = [];
        $this->loadModel('Controllers');
        $controllers_model = $this->Controllers
            ->find()
            ->where([
        		'enabled' => TRUE,
        		'deleted' => FALSE
        	]);
        foreach ($controllers_model as $controller) {
            $controllers[$controller->id] = $controller->name;
        }

        $services = [];
        $this->loadModel('Services');
        $services_model = $this->Services
            ->find()
            ->where([
                'enabled' => TRUE,
                'deleted' => FALSE
            ]);
        foreach ($services_model as $service) {
            $services[$service->id] = $service->name;
        }

        $users = [];
        $this->loadModel('Users');
        $users_model = $this->Users->find();
        foreach ($users_model as $user) {
            $users[$user->id] = $user->name;
        }

        $last_pppoe = [];

        if ($last_conn_pppoe_id) {

            $lconn = $this->Connections->find()
                ->contain([
                    'Customers', 
                    'Controllers', 
                    'Services'
                ])
                ->where([
                    'Connections.id' => $last_conn_pppoe_id,
                    'Connections.pppoe_name IS NOT' => NULL
                ])
                ->first();

            if ($lconn) {

                $last_pppoe['last-pppoe-customer-code'] = $lconn->customer->code;
                $last_pppoe['last-pppoe-customer'] = $lconn->customer->name;
                $last_pppoe['last-pppoe-address'] = $lconn->address;
                $last_pppoe['last-pppoe-controller'] = $lconn->controller->name;
                $last_pppoe['last-pppoe-service'] = $lconn->service->name;
                $last_pppoe['last-pppoe-ip'] = $lconn->ip;
                $last_pppoe['last-pppoe-name'] = $lconn->pppoe_name;
                $last_pppoe['last-pppoe-pass'] = $lconn->pppoe_pass; 
            }
        }

        $this->set(compact('controllersArray', 'labels', 'areas', 'controllers', 'services', 'users', 'last_pppoe'));
    }

    public function indexAdministrative()
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');

        $this->loadModel('Labels');
        $labels_array = $this->Labels->find()->where(['entity' => 1])->toArray();

        $labels = [];
        $labels[] = [
            'id' => 0,
            'text' => 'Sin etiquetas',
            'entity' => 0,
            'color_back' => '#000000',
            'color_text' => '#ffffff',
            'enabled' => 1,
        ];

        foreach ($labels_array as $label) {
            $labels[] = $label;
        }

        $areas = [];
        $this->loadModel('Areas');
        $areas_model = $this->Areas->find();
        foreach ($areas_model as $a) {
            $areas[$a->id] = $a->name;
        }

        $controllers = [];
        $this->loadModel('Controllers');
        $controllers_model = $this->Controllers
            ->find()
            ->where([
        		'enabled' => TRUE,
        		'deleted' => FALSE
        	]);
        foreach ($controllers_model as $controller) {
            $controllers[$controller->id] = $controller->name;
        }

        $services = [];
        $this->loadModel('Services');
        $services_model = $this->Services->find();
        foreach ($services_model as $service) {
            $services[$service->id] = $service->name;
        }

        $users = [];
        $this->loadModel('Users');
        $users_model = $this->Users->find();
        foreach ($users_model as $user) {
            $users[$user->id] = $user->name;
        }

        $this->set(compact('controllersArray', 'labels', 'payment_getway', 'areas', 'controllers', 'services', 'users'));
    }

    public function lockeds()
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');

        $this->loadModel('Labels');
        $labels_array = $this->Labels->find()->where(['entity' => 1])->toArray();

        $labels = [];
        $labels[] = [
            'id' => 0,
            'text' => 'Sin etiquetas',
            'entity' => 0,
            'color_back' => '#000000',
            'color_text' => '#ffffff',
            'enabled' => 1,
        ];

        foreach ($labels_array as $label) {
            $labels[] = $label;
        }

        $areas = [];
        $this->loadModel('Areas');
        $areas_model = $this->Areas->find();
        foreach ($areas_model as $a) {
            $areas[$a->id] = $a->name;
        }

        $controllers = [];
        $this->loadModel('Controllers');
        $controllers_model = $this->Controllers
            ->find()
            ->where([
        		'enabled' => TRUE,
        		'deleted' => FALSE
        	]);
        foreach ($controllers_model as $controller) {
            $controllers[$controller->id] = $controller->name;
        }

        $services = [];
        $this->loadModel('Services');
        $services_model = $this->Services->find();
        foreach ($services_model as $service) {
            $services[$service->id] = $service->name . ' ($' . $service->price . ')';
        }

        $users = [];
        $this->loadModel('Users');
        $users_model = $this->Users->find();
        foreach ($users_model as $user) {
            $users[$user->id] = $user->name;
        }

        $this->set(compact('controllersArray', 'labels', 'payment_getway', 'areas', 'controllers', 'services', 'users'));
    }

    /**
     * called from 
     *  customer/view
     *  ticlets/add
     *  tickets/index
    */
    public function getConnectionCustomer() 
    {
        if ($this->request->is('Ajax')) {//Ajax Detection

            $code = $this->request->getQuery('customer_code');

            $deleted = $this->request->getQuery('deleted');

            $connections = [];

            if (boolval($deleted)) {

                $connections = $this->Connections->find()->contain([
                    'Controllers',
                    'Customers',
                    'Services', 
                    'Areas',
                ])->where([
                    'customer_code' => $code,
                ]);
    
            } else {

                $connections = $this->Connections->find()->contain([
                    'Controllers',
                    'Customers',
                    'Services',
                    'Areas',
                ])->where([
                    'customer_code'       => $code,
                    'Connections.deleted' => false
                ]);
            }

            $this->set(compact('connections'));
            $this->set('_serialize', ['connections']);
        }
    }

    /**
     * called from 
     *  customer/view
     *  ticlets/add
     *  tickets/index
    */
    public function getConnectionCustomerFromTicket() 
    {
        if ($this->request->is('Ajax')) {//Ajax Detection

            $code = $this->request->getQuery('customer_code');

            $connections = [];

            $connections = $this->Connections
                ->find()
                ->contain([
                    'Controllers',
                    'Customers',
                    'Services', 
                    'Areas',
                ])->where([
                    'customer_code' => $code,
                ]);

            $this->set(compact('connections'));
            $this->set('_serialize', ['connections']);
        }
    }

    /**
     * called from 
     *  customer/view
     *  ticlets/add
     *  tickets/index
    */
    public function getAccountsStatus() 
    {
        if ($this->request->is('Ajax')) {//Ajax Detection

            $paraments = $this->request->getSession()->read('paraments');

            $code = $this->request->getQuery('customer_code');

            $deleted = $this->request->getQuery('deleted');

            $connections = [];

            if (boolval($deleted)) {

                $connections = $this->Connections->find()->contain([
                    'Services', 
                ])->where([
                    'customer_code' => $code,
                ])->toArray();

            } else {

                $connections = $this->Connections->find()->contain([
                    'Services',
                ])->where([
                    'customer_code'       => $code,
                    'Connections.deleted' => false
                ])->toArray();
            }

            if ($paraments->gral_config->billing_for_service) {
                $this->loadComponent('CurrentAccount', [
                     'className' => '\App\Controller\Component\Admin\CurrentAccount'
                ]);
                $connection = new \stdClass;
                $connection->created = "";
                $connection->debt_month = $this->CurrentAccount->getDebtMonthNoConnection($code);
                $connection->enabled = TRUE;
                array_push($connections, $connection);
            }

            $this->set(compact('connections'));
            $this->set('_serialize', ['connections']);
        }
    }

    public function view($id = null)
    {
        $connection = $this->Connections->get($id, [
            'contain' => ['Customers', 'Controllers', 'Services', 'Users', 'Areas']
        ]);

        $connection = $this->IntegrationRouter->getConnection($connection);

        $this->set(compact('connection'));
        $this->set('_serialize', ['connection']);
    }

    // public function viewQueueGraph($id = null) 
    // {
    //     $connection = $this->Connections->get($id, [
    //         'contain' => ['Customers', 'Controllers', 'Services', 'Users', 'Areas']
    //     ]);

    //     $connection = $this->IntegrationRouter->getConnection($connection);       

    //     $urlController = "https://" . $connection->controller->connect_to;

    //     if ($connection->controller->port_grap) {
    //       $urlController .= ':' . $connection->controller->port_grap; 
    //     }  

    //     $urldaily = $urlController . "/graphs/queue/" . $connection->queue->name . "/daily.gif";
    //     $this->grab_image($urldaily, WWW_ROOT . 'queues_graphs/daily-' . $connection->id . '.gif');

    //     $urlweekly = $urlController . "/graphs/queue/".$connection->queue->name . "/weekly.gif";   
    //     $this->grab_image($urlweekly, WWW_ROOT . 'queues_graphs/weekly-' . $connection->id . '.gif');

    //     $urlmonthly = $urlController . "/graphs/queue/".$connection->queue->name . "/monthly.gif"; 
    //     $this->grab_image($urlmonthly, WWW_ROOT . 'queues_graphs/monthly-' . $connection->id . '.gif');

    //     $urlyearly = $urlController . "/graphs/queue/".$connection->queue->name . "/yearly.gif";      
    //     $this->grab_image($urlyearly, WWW_ROOT . 'queues_graphs/yearly-' . $connection->id . '.gif');

    //     $html = $this->getHmtl($urlController . "/graphs/queue/".$connection->queue->name . "/");

    //     if (!$html) {
    //          $this->Flash->warning(__('No se pudo obtener las gráficas. Verifique la configración del controlador de esta conexión..'));
    //          return $this->redirect(['action' => 'index']);
    //     }

    //     $html = str_replace("Average", "Promedio", $html);
    //     $html = str_replace("Current", "Actual", $html);

    //     $box = explode('<div class="box">', $html);

    //     $caption = [];
    //     $caption['daily'] = trim($this->get_string_between($box[1], '<p>', '</p>'));
    //     $caption['daily'] = str_replace("<em>", "", $caption['daily']);
    //     $caption['daily'] = str_replace("</em>", "", $caption['daily']);

    //     $caption['weekly'] = trim($this->get_string_between($box[2], '<p>', '</p>'));
    //     $caption['weekly'] = str_replace("<em>", "", $caption['weekly']);
    //     $caption['weekly'] = str_replace("</em>", "", $caption['weekly']);

    //     $caption['monthly'] = trim($this->get_string_between($box[3], '<p>', '</p>'));
    //     $caption['monthly'] = str_replace("<em>", "", $caption['monthly']);
    //     $caption['monthly'] = str_replace("</em>", "", $caption['monthly']);

    //     $caption['yearly'] = trim($this->get_string_between($box[4], '<p>', '</p>'));
    //     $caption['yearly'] = str_replace("<em>", "", $caption['yearly']);
    //     $caption['yearly'] = str_replace("</em>", "", $caption['yearly']);

    //     $this->set(compact('connection', 'caption'));
    //     $this->set('_serialize', ['connection', 'caption']);
    // }

    // private function get_string_between($string, $start, $end)
    // {
    //     $string = ' ' . $string;
    //     $ini = strpos($string, $start);
    //     if ($ini == 0) return '';
    //     $ini += strlen($start);
    //     $len = strpos($string, $end, $ini) - $ini;
    //     return substr($string, $ini, $len);
    // }

    // private function getHmtl($url)
    // {
    //     $ch = curl_init ($url);

    //     curl_setopt($ch, CURLOPT_HEADER, 0);
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //     curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);

    //     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    //     curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

    //     $raw = curl_exec($ch);

    //     $code = curl_getinfo ($ch, CURLINFO_RESPONSE_CODE);  

    //     curl_close ($ch);

    //     if ($code != 200) {              
    //          return false;  
    //     }

    //     return $raw;
    // }

    // private function grab_image($url,$saveto)
    // {
    //     $ch = curl_init ($url);

    //     curl_setopt($ch, CURLOPT_HEADER, 0);
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //     curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);

    //     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    //     curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

    //     $raw = curl_exec($ch);
    //     curl_close ($ch);
    //     if (file_exists($saveto)) {
    //         unlink($saveto);
    //     }
    //     $fp = fopen($saveto, 'x+');
    //     fwrite($fp, $raw);
    //     fclose($fp);
    // }

    private function getNameMonth($period)
    {
        $name_month['01'] = 'Enero ';
        $name_month['02'] = 'Febrero ';
        $name_month['03'] = 'Marzo ';
        $name_month['04'] = 'Abril ';
        $name_month['05'] = 'Mayo ';
        $name_month['06'] = 'Junio ';
        $name_month['07'] = 'Julio ';
        $name_month['08'] = 'Agosto ';
        $name_month['09'] = 'Septiembre ';
        $name_month['10'] = 'Octubre ';
        $name_month['11'] = 'Noviembre ';
        $name_month['12'] = 'Diciembre ';

        return $name_month[explode('/', $period)[0]] . explode('/', $period)[1];
    }

    public function getConnectionsSelectByCustomer()
    {
        if ($this->request->is('Ajax')) {//Ajax Detection

            $customer_code = $this->request->input('json_decode')->customer_code;

            $this->loadModel('Connections');

            $connections = $this->Connections
                ->find()
                ->contain([
                    'Users', 
                    'Controllers', 
                    'Services'
                ])->where([
                    'customer_code' => $customer_code
                ]);

            $connectionsArray = [];

            $connectionsArray[0] = 'Otras ventas';

            foreach ($connections as $connection ) {

                if ($connection->deleted && $connection->debt_month == 0) {
                    continue;
                }

                $connectionsArray[$connection->id] = (($connection->deleted) ? '(Eliminado) ' : '') .  $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address;
            }

            krsort($connectionsArray);

            $this->set('connectionsArray', $connectionsArray);
        }
    }

    public function add($last_conn_pppoe_id = null)
    {
        $connection = $this->Connections->newEntity();

        if ($this->request->is('post')) {  

            $action = 'Agregado Conexión';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $paraments = $this->request->getSession()->read('paraments');
            $payment_getway = $this->request->getSession()->read('payment_getway');
            $afip_codes =  $this->request->getSession()->read('afip_codes');

            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($this->request->getData('controller_id'));

            $data = $this->request->getData();

            //validacion area_id
            if ($data['area_id'] == '') {
                $this->Flash->warning(__('Error al crear la conexión. Debe asignar un área al cliente, para ello debe ir a la vista de edición del cliente.'));
                return $this->redirect(['action' => 'add']);
            }

            if ($paraments->system->mode_migration) {

                $created = $data['created'];
                $created = explode('/', $created);
                $created = $created[2] . '-' . $created[1] . '-' . $created[0] . Time::now()->format(' H:i:s');

                $data['created'] = $created;
                $data['modified'] = $created;
            } else {
                $data['created'] = Time::now();
                $data['modified'] = Time::now();
            }

            if ($data['mac'] != '') {

                $data['mac'] = str_replace(':', '', $data['mac']);
                $data['mac'] = str_replace(' ', '', $data['mac']);
                $data['mac'] = str_replace('-', '', $data['mac']);
                $data['mac'] = str_replace('.', '', $data['mac']);

                $conn = $this->Connections->find()
                     ->where([
                         'mac' =>  $data['mac'], 
                         'deleted' => false
                     ])
                     ->first();

                 if ($conn) {

                    $this->Flash->warning(__('La MAC {0} ya está usada por otra conexión.', $data['mac'] ));                    
                    return $this->redirect(['action' => 'add']);
                 }
            }

            //valido queue_name o name 
            if ($data['queue_name'] != '' || $data['name'] != '') {         

                 if ($data['queue_name'] != '') { //dhcp - ipfija

                      $conn = $this->Connections->find()
                          ->where([
                              'queue_name' => $data['queue_name'],
                              'deleted' => false
                          ])
                          ->first();

                      if ($conn) {
                        $this->Flash->warning(__('Ya existe una conexión con este nombre de (queue): {0}', $data['queue_name']));
                        return $this->redirect(['action' => 'add']);
                      }
                 } else {//pppoe

                      $conn = $this->Connections->find()
                          ->where([
                              'pppoe_name' =>  $data['name'],
                              'deleted' => false
                          ])
                          ->first();

                      if ($conn) {
                         $this->Flash->warning(__('Ya existe una conexión con este nombre de usuario (pppoe): {0}', $data['name']));
                        return $this->redirect(['action' => 'add']);
                      } 
                 }
            }

            $data['before_ip'] = $data['ip'];

            $connection = $this->IntegrationRouter->addConnection($controller, $data);

            //una vez que la conexion se creo correctamente relizo las operaciones administrativas
            if ($connection) {

                $this->loadModel('Customers');
                $customer = $this->Customers->get($connection->customer_code, [
                    'contain' => [
                        'Areas',
                        'CustomersAccounts'
                    ]
                ]);
                $customer->status = 'CC';
                $this->Customers->save($customer);

                $this->loadModel('Services');
                $service = $this->Services->get($connection->service_id);

                $servicePending = null;

                if ($this->request->getData('service_pending_id') != '') {

                    $this->loadModel('ServicePending');
                    $servicePending = $this->ServicePending->get($this->request->getData('service_pending_id'), ['contain' => ['Discounts']]);

                    if ($servicePending->discount) {

                        $connection->discount_total_month = $servicePending->discount->duration;
                        $connection->discount_month = $servicePending->discount->duration;
                        $connection->discount_value = $servicePending->discount->value;
                        $connection->discount_description = $servicePending->discount->concept;
                        $connection->discount_code = $servicePending->discount->code;
                        $connection->discount_alicuot = $servicePending->discount->alicuot;
                        $connection->discount_always = $servicePending->discount->always;

                        if (!$this->Connections->save($connection)) {
                            $this->Flash->error(__('Error relacionar el descuento con la conexión.'));
                            return $this->redirect(['action' => 'add']);
                        }
                    }

                    $servicePending->used = Time::now();
                    $this->ServicePending->save($servicePending);
                }

                if ($this->request->getData('proportional')) {

                    $now = $connection->created;

                    $debt = $this->CurrentAccount->addDebtService([
                        'concept' => 'Abono proporcional ' . $service->name . ' ' . $this->getNameMonth($now->format('m/Y')),
                        'seating' => false,
                        'customer' => $customer,
                        'service' => $service,
                        'calculate_proporcional' => true,
                        'period' => $now->format('m/Y'),
                        'connection' => $connection,
                        'duedate' => null,
                        'redirect' => ['action' => 'add'],
                    ]);

                    $this->loadModel("ConnectionsDebtsMonth");
                    $cdm = $this->ConnectionsDebtsMonth->newEntity();

                    $cdm->created = $now;
                    $cdm->business_billing = $customer->business_billing;
                    $cdm->period = $now->format('m/Y');
                    $cdm->debt_id = $debt->id;

                    if ($debt->tipo_comp == 'XXX') {
                         $cdm->totalx = $debt->total;
                    } else {
                         $cdm->total = $debt->total;
                    }
                    $cdm->connection_id = $connection->id;
                    $this->ConnectionsDebtsMonth->save($cdm);
                }

                if ($connection->diff) {
                    $this->Flash->warning(__('El proceso no se completo correctamente.'));  
                } else {
                    $this->Flash->success(__('Conexión agregada.'));  
                }

                //aplicar marcado para generacion de deudas en las pasarelas de pagos.
                if ($paraments->gral_config->billing_for_service) {
                    if (sizeof($customer->customers_accounts) > 0) {
                        foreach ($customer->customers_accounts as $customer_account) {
                            if (!$customer_account->deleted) {
                                foreach ($payment_getway->methods as $key => $pg) {
                                    if ($pg->id == $customer_account->payment_getway_id) {
                                        $connection->$key = TRUE;
                                        $this->Connections->save($connection);
                                    }
                                }
                            }
                        }
                    }
                }

                if ($this->request->getData('btn-add') == 'exit') {
                    return $this->redirect(['action' => 'index', $connection->id]);
                }
                return $this->redirect(['action' => 'add', $connection->id]);

            } else {
                return $this->redirect(['action' => 'add']);
            }
        }

        $paraments = $this->request->getSession()->read('paraments');

        $this->loadModel('Areas');
        $areas_temp = $this->Areas->find('list')->where(['enabled' => true])->toArray();

        $areas = [];
        $areas[''] = 'Seleccione';
        foreach ($areas_temp as $key => $area_t) {
            $areas[$key] = $area_t;
        }

        $services = [];
        $this->loadModel('Services');
        $services_model = $this->Services->find();
        foreach ($services_model as $a) {
            $services[$a->id] = $a->name;
        }

        $users = [];
        $this->loadModel('Users');
        $users_model = $this->Users->find();
        foreach ($users_model as $user) {
            $users[$user->id] = $user->name;
        }

        $controllers = $this->IntegrationRouter->getAllControllersComplete();

        $this->loadModel('Labels');
        $labels_array = $this->Labels->find()->where(['entity' => 0])->toArray();

        $labels = [];
        $labels[] = [
            'id' => 0,
            'text' => 'Sin etiquetas',
            'entity' => 0,
            'color_back' => '#000000',
            'color_text' => '#000000',
            'enabled' => 1,
        ];

        foreach ($labels_array as $label) {
            $labels[] = $label;
        }

        $last_pppoe = [];

        if ($last_conn_pppoe_id) {

            $lconn = $this->Connections->find()
                ->contain([
                    'Customers', 
                    'Controllers', 
                    'Services'
                ])
                ->where([
                    'Connections.id' => $last_conn_pppoe_id,
                    'Connections.pppoe_name IS NOT' => NULL
                ])
                ->first();

            if ($lconn) {

                $last_pppoe['last-pppoe-customer-code'] = $lconn->customer->code;
                $last_pppoe['last-pppoe-customer'] = $lconn->customer->name;
                $last_pppoe['last-pppoe-address'] = $lconn->address;
                $last_pppoe['last-pppoe-controller'] = $lconn->controller->name;
                $last_pppoe['last-pppoe-service'] = $lconn->service->name;
                $last_pppoe['last-pppoe-ip'] = $lconn->ip;
                $last_pppoe['last-pppoe-name'] = $lconn->pppoe_name;
                $last_pppoe['last-pppoe-pass'] = $lconn->pppoe_pass;
            }
        }

        //envio al formulario de creacion de conexion de la lista de cliente y la informacion necesaria
        $this->set(compact('connection', 'customers', 'controllers', 'paraments', 'areas', 'services', 'users', 'labels', 'last_pppoe'));
        $this->set('_serialize', ['connection']);
    }

    public function syncConnection($connection_id)
    {
        $this->loadModel('Connections');
        $connection = $this->Connections->get($connection_id, ['contain' => ['Controllers', 'Customers', 'Services']]);

        $connection = $this->IntegrationRouter->syncConnection($connection);

        if ($connection && $connection->diff == false) {

            $action = 'Sincronización de Conexión';
            $detail = 'Controlador: ' . $connection->controller->name . PHP_EOL; 
            $detail .= 'Servicio: ' . $connection->service->name . PHP_EOL; 
            $detail .= 'IP: ' . $connection->ip . PHP_EOL; 
            $this->registerActivity($action, $detail, $connection->customer->code, TRUE);

            $this->Flash->success(__('Sincronización completa.'));  

        } else {
            $this->Flash->warning(__('Error al intentar sincronizar la conexión.'));  
        }
        return $this->redirect(['action' => 'index']);
    }

    public function edit($id = null)
    {
        $paraments = $this->request->getSession()->read('paraments');
        $payment_getway = $this->request->getSession()->read('payment_getway');

        $connection = $this->Connections->get($id, [
            'contain' => [
                'Customers.CustomersAccounts', 
                'Controllers', 
                'Services'
            ]
        ]);

        $connection = $this->IntegrationRouter->getConnection($connection);

        $connection_id = $connection->id;

        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Edición Conexión';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $data = $this->request->getData();

            if ($data['mac'] != '') {

                $data['mac'] = str_replace(':', '', $data['mac']);
                $data['mac'] = str_replace(' ', '', $data['mac']);
                $data['mac'] = str_replace('-', '', $data['mac']);
                $data['mac'] = str_replace('.', '', $data['mac']);
            }

            //valido mac
            if ($data['mac'] && strtoupper($connection->mac) != strtoupper($data['mac']) ) {

                 $conn = $this->Connections->find()
                     ->where([
                         'mac' =>  $data['mac'], 
                         'id !=' => $connection->id,
                         'deleted' => false
                     ])
                     ->first();

                 if ($conn) {

                    $this->Flash->warning(__('La MAC {0} ya está usada por otra conexión.', $data['mac'] ));                    
                    return $this->redirect(['action' => 'edit', $id]);
                 }
            } 

            //valido queue_name o name 
            if ($data['queue_name'] != '' || $data['name'] != '') {

                 if ($data['queue_name'] != '') { //dhcp - ipfija

                      $conn = $this->Connections->find()
                          ->where([
                              'queue_name' => $data['queue_name'],
                              'id !=' => $connection->id,
                              'deleted' => false
                          ])
                          ->first();

                      if ($conn) {
                         $this->Flash->warning(__('Ya existe una conexión con este nombre de (queue): {0}', $data['queue_name']));
                        return $this->redirect(['action' => 'edit', $id]);
                      }                   
                 } else {//pppoe

                      $conn = $this->Connections->find()
                          ->where([
                              'pppoe_name' =>  $data['name'],
                               'id !=' => $connection->id,
                              'deleted' => false
                          ])
                          ->first();

                      if ($conn) {
                         $this->Flash->warning(__('Ya existe una conexión con este nombre de usuario (pppoe): {0}', $data['name']));
                        return $this->redirect(['action' => 'edit', $id]);
                      } 
                 }
            }

            //resguardo la connection
            $this->loadModel('ConnectionsTransactions');
            $connectionTransaction =  $this->ConnectionsTransactions->newEntity();
            $connectionTransaction->created = Time::now();
            $connectionTransaction->customer_code = $connection->customer_code;
            $connectionTransaction->connection_id = $connection->id;
            $connectionTransaction->data = serialize($connection);
            $this->ConnectionsTransactions->save($connectionTransaction);

            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($this->request->getData('controller_id'));        

            if ($connection->controller->id == $this->request->getData('controller_id')) {

                $data['before_ip'] = $connection->ip;

                //si solo se edita el plan y no el nodo es una edicion de conexion
                if (!$this->IntegrationRouter->editConnection($connection, $data)) {

                    //restaurar conexion a como estaba.
                    $this->IntegrationRouter->rollbackConnection($controller, $connection_id);

                    $this->Flash->error(__('Error al intentar Editar la conexión.'));

                    return $this->redirect(['action' => 'edit', $id]);
                }

            } else {

                $before_blocked = !$connection->enabled;

                //si se cambia el nodo la conexion debe eliminarse del nodo origen y crearse en el nodo destino
                if ($this->internalDelete($connection)) {

                    $data['name'] = $this->request->getData('name');
                    $data['password'] = $this->request->getData('password');

                    //mentener alguunos de los datos qye no peude cambiar
                    $data['id'] = $connection->id;
                    $data['created'] = $connection->created->format('Y-m-d H:i:s');
                    $data['customer_code'] = $connection->customer_code;

                    //esto pudo haber cambio por otro lado
                    $data['customer_name'] = $connection->customer->name;

                    //para mantener la informacion de descuentos
                    $data['discount_total_month'] = $connection->discount_total_month;
                    $data['discount_month'] = $connection->discount_month;
                    $data['discount_value'] = $connection->discount_value;
                    $data['discount_description'] = $connection->discount_description;
                    $data['discount_code'] = $connection->discount_code;
                    $data['discount_alicuot'] = $connection->discount_alicuot;
                    $data['deleted'] = $connection->deleted;
                    $data['before_blocked'] = $connection->before_blocked; 

                    if ($paraments->gral_config->billing_for_service) {
                        if (sizeof($connection->customer->customers_accounts) > 0) {
                            foreach ($connection->customer->customers_accounts as $customer_account) {
                                if (!$customer_account->deleted) {
                                    foreach ($payment_getway->methods as $key => $pg) {
                                        if ($pg->id == $customer_account->payment_getway_id) {
                                            $data[$key] = $connection->$key;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $connection = $this->IntegrationRouter->addConnection($controller, $data);

                    if (!$connection) {

                        //restaurar conexion a como estaba.
                        $this->IntegrationRouter->rollbackConnection($controller, $connection_id);
                        $this->Flash->error(__('Error al intentar Editar la conexión.'));

                        return $this->redirect(['action' => 'edit', $id]);
                    }

                } else {

                    //restaurar conexion a como estaba.
                    $this->IntegrationRouter->rollbackConnection($controller, $connection_id);

                    $this->Flash->error(__('Error al intentar Editar la conexión.'));

                    return $this->redirect(['action' => 'edit', $id]);
                }

                if ($before_blocked) {
                     $this->disableInternal($connection_id);
                }
            }

            $this->ConnectionsTransactions->delete($connectionTransaction);

            if ($connection->diff) {
                 $this->Flash->warning(__('El proceso no se completo.'));
            } else {
                 $this->Flash->success(__('Edición exitosa.')); 
            }

            return $this->redirect(['action' => 'index']);
        }

        $this->loadModel('Areas');
        $zoneArray = $this->Areas->find('list');

        $controllers = $this->IntegrationRouter->getAllControllersComplete();

        //envio al formulario de creacion de conexion de la lista de cliente y la informacion necesaria
        $this->set(compact('connection', 'customers', 'controllers', 'zoneArray', 'paraments'));
        $this->set('_serialize', ['connection']);
    }

    public function disable()
    {
        $this->request->allowMethod(['post']);
        
        $this->loadModel('ConnectionsHasMessageTemplates');

        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Bloqueo de Conexión';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $connection = $this->Connections->get($_POST['id'], [
                'contain' => ['Customers', 'Controllers']
            ]);

            if (!$connection->enabled) {

                $this->Flash->default(__('La conexión ya está bloqueada.'));
                return $this->redirect($this->referer());
            }

            $this->loadModel('MessageTemplates');
            $messageTemplate = $this->MessageTemplates->find()->where(['deleted' => 0, 'type' => 'Bloqueo', 'enabled' => 1])
                ->order(['priority' => 'DESC'])->first();

            if (!$messageTemplate) {
                $this->Flash->warning(__('No existe un aviso armado para el bloqueo de conexiones.'));
                return $this->redirect($this->referer());
            }

            $paraments = $this->request->getSession()->read('paraments');

            //verifico si tiene msj enviados y si los tiene los elimino
            $this->loadModel('ConnectionsHasMessageTemplates');

            if ($this->ConnectionsHasMessageTemplates->find()->where(['connections_id' => $connection->id, 'type' => 'Aviso'])->count() > 0) {

                $connection =  $this->IntegrationRouter->removeAvisoConnection($connection, TRUE);

                if ($connection) {
                    $this->ConnectionsHasMessageTemplates->deleteAll(['connections_id' => $connection->id]);
                } else {
                    $this->Flash->warning(__('No se pudo eliminar los avisos de la conexión.'));
                    return $this->redirect($this->referer());
                }
            }

            $connection = $this->IntegrationRouter->disableConnection($connection);

            $unsync = false;

            if ($connection && !$connection->diff) { 
               //...
            } else if ($connection && $connection->diff) {  
               $unsync = true;
            }

            if ($connection) {

               $connection->enabled = FALSE;
               $connection->blocking_date = Time::now();
               $this->Connections->save($connection);

               $chmt =  $this->ConnectionsHasMessageTemplates->newEntity();
               $chmt->created = Time::now();
               $chmt->connections_id = $connection->id;
               $chmt->message_template_id = $messageTemplate->id;
               $chmt->ip = $connection->ip;
               $chmt->type = $messageTemplate->type;

               $this->ConnectionsHasMessageTemplates->save($chmt);  

               if ($unsync) {
                   $this->Flash->warning(__('La conexión se bloqueo solo en el sistema.'));
                   $this->Flash->warning(__('Revise el estado del controlador y luego sincronice la conexión.'));
               } else {
                    $this->Flash->success(__('La conexión se bloqueo correctamente.'));
               }
          } else {
               $this->Flash->error(__('Error al intentar bloquear la conexión.'));
          }
        }

        return $this->redirect($this->referer());
    }

    public function disableCobranza()
    {
        $this->request->allowMethod(['post']);

        $this->loadModel('ConnectionsHasMessageTemplates');

        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Bloqueo de Conexión (Cobranza)';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $connection = $this->Connections->get($_POST['id'], [
                'contain' => ['Customers', 'Controllers']
            ]);

            if (!$connection->enabled) {

                $this->Flash->default(__('La conexión ya está bloqueada.'));
                $referer = $this->referer();
                $url = explode('/', $referer);
                $controller = $url[4];
                $url = $url[0] . '//' . $url[2] . '/' . $url[3] . '/' . $url[4] . '/' . $url[5];
                if (strpos($controller, 'payments') !== FALSE || strpos($controller, 'Payments') !== FALSE) {
                    $url .= '/0/' . $connection->customer_code;
                } else {
                    $url = $referer;
                }
                return $this->redirect($url);
            }

            $this->loadModel('MessageTemplates');
            $messageTemplate = $this->MessageTemplates->find()->where(['deleted' => 0, 'type' => 'Bloqueo', 'enabled' => 1])
                ->order(['priority' => 'DESC'])->first();

            if (!$messageTemplate) {
                $this->Flash->warning(__('No existe un aviso armado para el bloqueo de conexiones.'));
                $referer = $this->referer();
                $url = explode('/', $referer);
                $controller = $url[4];
                $url = $url[0] . '//' . $url[2] . '/' . $url[3] . '/' . $url[4] . '/' . $url[5];
                if (strpos($controller, 'payments') !== FALSE || strpos($controller, 'Payments') !== FALSE) {
                    $url .= '/0/' . $connection->customer_code;
                } else {
                    $url = $referer;
                }
                return $this->redirect($url);
            }

            $paraments = $this->request->getSession()->read('paraments');

            //verifico si tiene msj enviados y si los tiene los elimino
            $this->loadModel('ConnectionsHasMessageTemplates');

            if ($this->ConnectionsHasMessageTemplates->find()->where(['connections_id' => $connection->id, 'type' => 'Aviso'])->count() > 0) {

                $connection =  $this->IntegrationRouter->removeAvisoConnection($connection, TRUE);

                if ($connection) {
                    $this->ConnectionsHasMessageTemplates->deleteAll(['connections_id' => $connection->id]);
                } else {
                    $this->Flash->warning(__('no se pudo eliminar los avisos de la conexion.'));
                    $referer = $this->referer();
                    $url = explode('/', $referer);
                    $controller = $url[4];
                    $url = $url[0] . '//' . $url[2] . '/' . $url[3] . '/' . $url[4] . '/' . $url[5];
                    if (strpos($controller, 'payments') !== FALSE || strpos($controller, 'Payments') !== FALSE) {
                        $url .= '/0/' . $connection->customer_code;
                    } else {
                        $url = $referer;
                    }
                    return $this->redirect($url);
                }
            }

            $connection = $this->IntegrationRouter->disableConnection($connection);

            $unsync = false;

            if ($connection && !$connection->diff) { 
               //...
            } else if ($connection && $connection->diff) {  
               $unsync = true;
            }           

            if ($connection) {

               $connection->enabled = FALSE;
               $connection->blocking_date = Time::now();
               $this->Connections->save($connection);

               $chmt =  $this->ConnectionsHasMessageTemplates->newEntity();
               $chmt->created = Time::now();
               $chmt->connections_id = $connection->id;
               $chmt->message_template_id = $messageTemplate->id;
               $chmt->ip = $connection->ip;
               $chmt->type = $messageTemplate->type;

               $this->ConnectionsHasMessageTemplates->save($chmt);  

               if ($unsync) {
                   $this->Flash->warning(__('La conexión se bloqueo solo en el sistema.'));
                   $this->Flash->warning(__('Revise el estado del controlador y luego sincronice la conexión.'));
               } else {
                    $this->Flash->success(__('La conexión se bloqueo correctamente.'));
               }
          } else {
               $this->Flash->error(__('Error al intentar bloquear la conexión.'));
          }
        }

        $referer = $this->referer();
        $url = explode('/', $referer);
        $controller = $url[4];
        $url = $url[0] . '//' . $url[2] . '/' . $url[3] . '/' . $url[4] . '/' . $url[5];
        if (strpos($controller, 'payments') !== FALSE || strpos($controller, 'Payments') !== FALSE) {
            $url .= '/0/' . $connection->customer_code;
        } else {
            $url = $referer;
        }
        return $this->redirect($url);
    }

    public function disableInternal($id)
    {
        $connection = $this->Connections->get($id, [
            'contain' => ['Customers', 'Controllers']
        ]);

        $this->loadModel('MessageTemplates');
        $messageTemplate = $this->MessageTemplates->find()->where(['deleted' => 0, 'type' => 'Bloqueo', 'enabled' => 1])
            ->order(['priority' => 'DESC'])->first();

        if (!$messageTemplate) {
            return false;
        }

        //verifico si tiene msj enviados y si los tiene los elimino
        $this->loadModel('ConnectionsHasMessageTemplates');

        if ($this->ConnectionsHasMessageTemplates->find()->where(['connections_id' => $connection->id, 'type' => 'Aviso'])->count() > 0){

            $connection =  $this->IntegrationRouter->removeAvisoConnection($connection, true);

            if ($connection) {
                $this->ConnectionsHasMessageTemplates->deleteAll(['connections_id' => $connection->id]);
            } else {
                return false;
            }
        }

        $connection = $this->IntegrationRouter->disableConnection($connection);

        if ($connection) { 

            $connection->enabled = false;

            if ($this->Connections->save($connection)) {

                $this->loadModel('ConnectionsHasMessageTemplates');

                $chmt =  $this->ConnectionsHasMessageTemplates->newEntity();
                $chmt->created = Time::now();
                $chmt->connections_id = $connection->id;
                $chmt->message_template_id = $messageTemplate->id;
                $chmt->ip = $connection->ip;
                $chmt->type = $messageTemplate->type;

                if (!$this->ConnectionsHasMessageTemplates->save($chmt)) {
                   return false;
                }

            } else {                
                return false;
            }             

        } else {
             return false;
        }       

        return true;
    }

    public function enable()
    {
        $this->request->allowMethod(['post']);

        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Habilitación de Conexión';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $connection = $this->Connections->get($_POST['id'], [
                'contain' => ['Customers', 'Controllers']
            ]);

            if ($connection->enabled) {
                $this->Flash->default(__('La conexión no está bloqueada.'));
                return $this->redirect($this->referer());
            }

            // $paraments = $this->request->getSession()->read('paraments');
            //verifico conecxion con el controlador 
            if ($connection->controller->integration && !$this->IntegrationRouter->getStatus($connection->controller)) {
                $this->Flash->error(__('No se pude establecer una conexión con el controlador {0}.', $connection->controller->name));
                return $this->redirect($this->referer());
            }

            if ($this->IntegrationRouter->enableConnection($connection, TRUE)) {

                $connection->enabled = TRUE;
                $connection->enabling_date = Time::now();

                if ($this->Connections->save($connection)) {

                    $this->loadModel('ConnectionsHasMessageTemplates');
                    $chmt =  $this->ConnectionsHasMessageTemplates->find()->where(['connections_id' => $connection->id, 'type' => 'Bloqueo'])->first();

                    if (!$this->ConnectionsHasMessageTemplates->delete($chmt)) {
                        $this->Flash->error(__('No se pudo eliminar el aviso de bloqueo.'));
                    } else {
                        $this->Flash->success(__('Conexion Habilitada.'));
                    }
                } else {
                    $this->Flash->error(__('No se pudo Habilitar la conexión.'));
                }
            }
        }

        return $this->redirect($this->referer());
    }

    public function enableCobranza()
    {
        $this->request->allowMethod(['post']);

        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Habilitación de Conexión (Cobranza)';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $connection = $this->Connections->get($_POST['id'], [
                'contain' => ['Customers', 'Controllers']
            ]);

            if ($connection->enabled) {
                $this->Flash->default(__('La conexión no está bloqueada.'));
                $referer = $this->referer();
                $url = explode('/', $referer);
                $controller = $url[4];
                $url = $url[0] . '//' . $url[2] . '/' . $url[3] . '/' . $url[4] . '/' . $url[5];
                if (strpos($controller, 'payments') !== FALSE || strpos($controller, 'Payments') !== FALSE) {
                    $url .= '/0/' . $connection->customer_code;
                } else {
                    $url = $referer;
                }
                return $this->redirect($url);
            }

            // $paraments = $this->request->getSession()->read('paraments');
            //verifico conecxion con el controlador 
            if ($connection->controller->integration && !$this->IntegrationRouter->getStatus($connection->controller)) {
                $this->Flash->error(__('No se pude establecer una conexión con el controlador {0}.', $connection->controller->name));
                $referer = $this->referer();
                $url = explode('/', $referer);
                $controller = $url[4];
                $url = $url[0] . '//' . $url[2] . '/' . $url[3] . '/' . $url[4] . '/' . $url[5];
                if (strpos($controller, 'payments') !== FALSE || strpos($controller, 'Payments') !== FALSE) {
                    $url .= '/0/' . $connection->customer_code;
                } else {
                    $url = $referer;
                }
                return $this->redirect($url);
            }

            if ($this->IntegrationRouter->enableConnection($connection, TRUE)) {

                $connection->enabled = TRUE;
                $connection->enabling_date = Time::now();

                if ($this->Connections->save($connection)) {

                    $this->loadModel('ConnectionsHasMessageTemplates');
                    $chmt =  $this->ConnectionsHasMessageTemplates->find()->where(['connections_id' => $connection->id, 'type' => 'Bloqueo'])->first();

                    if (!$this->ConnectionsHasMessageTemplates->delete($chmt)) {
                        $this->Flash->error(__('No se pudo eliminar el aviso de bloqueo.'));
                    } else {
                        $this->Flash->success(__('Conexion Habilitada.'));
                    }
                } else {
                    $this->Flash->error(__('No se pudo Habilitar la conexión.'));
                }
            }
        }

        $referer = $this->referer();
        $url = explode('/', $referer);
        $controller = $url[4];
        $url = $url[0] . '//' . $url[2] . '/' . $url[3] . '/' . $url[4] . '/' . $url[5];
        if (strpos($controller, 'payments') !== FALSE || strpos($controller, 'Payments') !== FALSE) {
            $url .= '/0/' . $connection->customer_code;
        } else {
            $url = $referer;
        }
        return $this->redirect($url);
    }

    public function findIPAjax()
    {
        if ($this->request->is('Ajax')) { //Ajax Detection

            $this->loadModel('Controllers');

            $controller_id = $this->request->input('json_decode')->controller_id;
            $pool_id = $this->request->input('json_decode')->pool_id;

            $controller = $this->Controllers->get($controller_id);

            $data = $this->IntegrationRouter->getFreeIPByPoolId($controller, $pool_id);

            if (!$data['ip']) {
                $data['error'] = 'No existe IP libre para está conexión. ';
            }
            $this->set('data', $data);
        }
    }

    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);

        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Eliminación de conexión';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $connection = $this->Connections->get($_POST['id'], [
                'contain' => ['Customers', 'Controllers', 'Services']
            ]);

            // $paraments = $this->request->getSession()->read('paraments');

            //verifico conecxion con el controlador 
            // if ($paraments->system->integration && !$this->IntegrationRouter->getStatus($connection->controller)) {
            //     $this->Flash->error(__('No se pude establecer una conexion con el controlador {0}.', $connection->controller->name));
            //     return $this->redirect(['action' => 'index']);
            // }

            $connection->deleted = true;

            $connection = $this->IntegrationRouter->deleteConnection($connection); //si devuelve false, es por que algo salio mal con la base de datos

            if ($connection) {

                //verifico si tiene msj enviados y si los tiene los elimino
                $this->loadModel('ConnectionsHasMessageTemplates');
                $chmts = $this->ConnectionsHasMessageTemplates->deleteAll(['connections_id' => $connection->id]);

                $ip = $connection->ip ? $connection->ip : "";
                $mac = $connection->mac ? $connection->mac : "";
                $queue_name = $connection->queue_name ? $connection->queue_name : "";
                $pppoe_name = $connection->pppoe_name ? $connection->pppoe_name : "";
                $pppoe_pass = $connection->pppoe_pass ? $connection->pppoe_pass : "";

                $connection->deleted = true;
                $connection->ip = null;
                $connection->mac = null;
                $connection->queue_name = null;
                $connection->pppoe_name = null;
                $connection->pppoe_pass = null;
  
                if ($this->Connections->save($connection)) {

                    $customer = $this->Connections->Customers->get($connection->customer_code);

                    $this->Flash->success(__('Conexión eliminada.'));

                    $customer_code = $connection->customer_code;

                    $detail = "";
                    $detail .= 'Conexión: '. $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL; 
                    $detail .= 'IP: ' . $ip . PHP_EOL;
                    $detail .= 'MAC: ' . $mac . PHP_EOL;
                    $detail .= 'Queue name: ' . $queue_name . PHP_EOL;
                    $detail .= 'PPPoE name: ' . $pppoe_name . PHP_EOL;
                    $detail .= 'PPPoE pass: ' . $pppoe_pass . PHP_EOL;
                    $action = 'Eliminación de Conexión';
                    $this->registerActivity($action, $detail, $customer_code);

                } else {

                    $this->IntegrationRouter->rollbackConnection($connection->controller, $connection->id);
                    $this->Flash->error(__('No se pudo eliminar la conexión de la base de datos.'));
                }
            } else {

                $this->IntegrationRouter->rollbackConnection($connection->controller, $connection->id);
                $this->Flash->error(__('No se pudo eliminar la conexión de la base de datos.'));
            }
        }

        return $this->redirect($this->referer());
    }

    private function internalDelete($connection)
    {
       $connection = $this->IntegrationRouter->deleteConnection($connection); //si devuelve falso es por que algo slaio mal con la base de datos
        
        if ($connection) {

            //verifico si tiene msj enviados y si los tiene los elimino
            $this->loadModel('ConnectionsHasMessageTemplates');
            $chmts = $this->ConnectionsHasMessageTemplates->deleteAll(['connections_id' => $connection->id]);

            if (!$this->Connections->delete($connection)) {
                return false;
            }

            return true;
        }

        return false;
    }

    public function importAndros()
    {
        $connection = $this->Connections->newEntity();
        
        if ($this->request->is('post')) {

            if ($_FILES['csv']) {

                $this->loadModel('Customers');
                $this->loadModel('ConnectionsLabels');
                $controllers = $this->IntegrationRouter->getAllControllersComplete();

                $handle = fopen($_FILES['csv']['tmp_name'], "r");

                $error = false;
 
                $first = true;
                $counter = 0;

                while ($data = fgetcsv($handle, 999999, ";")) {

                    if ($first) {
                        $first = false;
                        continue;
                    }
               
                    $customer_code = trim($data[0]);
                    $ip = trim($data[2]);
                    $plan_name = trim($data[4]);
                    $controller_name = trim($data[5]);
                    $address = trim($data[6]);
                    $is_disabled = trim($data[7]);
                    $etiqueta_name = trim($data[8]);
                    $column1 = trim($data[9]);
                    $column2 = trim($data[10]);
                    
                    $customer = $this->Customers->find()->where(['code' => $customer_code])->first();
                    
                    if (!$customer) {
                        Debug($data); die();
                    }
                    
                    $connection_data = [
                        'controller_id' => '',
                    	'plan_id' => '',
                    	'pool_id' => '',
                    	'name' => '',
                    	'password' => '',
                    	'ip' => '',
                    	'validate_ip' => '',
                    	'interface' => '',
                    	'mac' => '',
                    	'proportional' => '0',
                    	'address' => $address == '' ? utf8_decode($customer->address) : utf8_decode($address),
                    	'area_id' => $customer->area_id,
                    	'queue_name' => '',
                    	'clave_wifi' => '',
                    	'ports' => '',
                    	'ip_alt' => '',
                    	'ing_traffic' => '',
                    	'comments' => "columna1: $column1; columna2: $column2",
                    	'customer_name' => utf8_decode($customer->name),
                    	'customer_code' =>  $customer->code,
                    	'service_pending_id' => '',
                    	'lat' => '',
                    	'lng' => '',
                    	'btn-add' => 'exit'
                    ];
                    
                    foreach ($controllers as $controller) {
                      
                        if (strtoupper($controller->name) == strtoupper($controller_name)) {
                            
                            $connection_data['controller_id'] = $controller->id;
                            $controller_selected = $controller;
                            
                            foreach ($controller->tplans as $plan) {
                        
                                if (strtoupper($plan->service->name) == strtoupper($plan_name)) {
                                    
                                    $connection_data['plan_id'] = $plan->id;
                                    
                                    foreach ($controller->tpools as $pool) {
                                
                                        if (Cidr::cidr_match($ip, $pool->addresses)) {
                                            
                                            $connection_data['validate_ip'] = '1';
                                            $connection_data['ip'] = $ip;
                                            $connection_data['pool_id'] = $pool->id;
                                            break;
                                                
                                        } else {
                                            $connection_data['validate_ip'] = '0';
                                            $connection_data['ip'] = $ip;
                                            $connection_data['pool_id'] = '';
                                            
                                        }
                                    }
                                    
                                    if (!$connection_data['validate_ip']) {
                                        
                                        $connection_label = $this->ConnectionsLabels->newEntity();
                                        $connection_label->connection_id = $connection->id;
                                        $connection_label->label_id = 13; //revisar  ip
                                        $this->ConnectionsLabels->save($connection_label);
                                    }
                                    
                                    break;
                                    
                                }
                            }
                            
                            break;
                        }
                    }
                    
                    if ($connection_data['ip'] == '') {
                         Debug($data); die();
                    }

                    $connection = $this->IntegrationRouter->addConnection($controller_selected, $connection_data);
                    
                    if ($connection) {
                        
                         $counter++;
                         
                        if (strtoupper($is_disabled) == 'SI' ) {
                            $this->disableInternal($connection->id);
                        }
                        
                        $label_id = "";
                        
                        switch ($etiqueta_name) {
                            
                            case 'bonificado':
                                $label_id = 11;
                                break;
                            case 'Cambiar IP':
                                $label_id = 12;
                                break;
                            case 'Solicito Baja Desinstalar':
                                $label_id = 9;
                                break;
                            case 'Suspendido':
                                $label_id = 10;
                                break;
                            case 'Suspendido/desinstalar':
                                $label_id = 2;
                                break;
                        }
                        
                        if ($label_id != "") {
                            $connection_label = $this->ConnectionsLabels->newEntity();
                            $connection_label->connection_id = $connection->id;
                            $connection_label->label_id = $label_id;
                            $this->ConnectionsLabels->save($connection_label);
                        }
                    }
                }

               $this->Flash->success(__('Se cargaron ' .$counter. ' Conexiones'));
               
               fclose($handle);
            }
        }

        $this->set(compact('connection'));
        $this->set('_serialize', ['connection']);
    }

    public function importGuilleDBCubeConnetions()
    {
        $customer = $this->Customers->newEntity();

        // if ($this->request->is('post')) {

            $this->loadModel('Accounts');
            $this->loadModel('ConnectionsLabels');
            $paraments = $this->request->session()->read('paraments');
            
            $controllers = $this->IntegrationRouter->getAllControllersComplete();

            $controllersParse = [
                'r2.n2.gmn' => 1, //N2-GMN [Tano]
                'r1.n8' => 7, //N8-GMN [BrynGwyn]
                'r1.n7.gmn' => 6 , //N7-GMN [LaAngostura]
                'r1.n6.gmn' => 5, //N6-GMN [Loma] 
                'r1.n5.gmn' => 4, //N5-GMN [Chacra]
                'r1.n4.gmn' => 3, //N4-GMN [Gmn.Nuevo]
                'r1.n4.dol' => 13, //N4-DOL [CoopDol]
                'r1.n3.gmn' => 2, //N3-GMN [Planta]
                'r1.n3.dol' => 12, //N3-DOL [Bethesda]
                'r1.n2.ju' => 14,// N2-JU [28deJulio]
                'r1.n2.dol' => 11, //N2-DOL [Loma]
                'r1.n1.ju' => 10, //N1-JU [28deJulio]
                'r1.n1.dol' => 9, //N1-DOL [RadioAlan]
            ];

            $serviciosParse = [
                'PM1' => 1,
                'P1' => 1,
                'P7' => 2,
                'P3' => 2,
                'P4' => 2,
                'P5' => 2,
                'P17' => 3,
                'P10' => 3,
                'P13' => 4,
                'P24' => 5,
                'P28' => 9,
                'P29' => 10,
                'P26' => 12,
                'P21' => 12,
                'P6' => 14,
                'P27' => 17,
                'P25' => 18,
                'P15' => 20,
                'P19' => 21,
                'P2' => 22,
                'P22' => 25,
                'P12' => 6,
                'P16' => 28,
                'P8' => 29,
                'P9' => 29,
                'P23' => 31,
                'P18' => 30,
                'Guille' => 32
            ];

            $error =  false;

            $first = true;
            $counter = 0;
            $error_counter = 0;
            
            $connection_intranet_cube = ConnectionManager::get('intranet_cube');
            
            $ConnectionsCube = $connection_intranet_cube->execute('SELECT * FROM connections')->fetchAll('assoc');

            $data = null;
     
            $this->loadModel('Users');
            $this->loadModel('Observations');

            foreach ($ConnectionsCube as $ConnectionCube) {

                $customer_code = $ConnectionCube['idcustomer'];
                $plan_id_name = $ConnectionCube['idplan'];
                $controller_id_name = $ConnectionCube['nas'];

                if (!$plan_id_name || !$controller_id_name) {
                    
                      Debug($ConnectionCube);
                      continue;
                }
 
                $customer = $this->Customers->find()->where(['comments' => $customer_code])->first();
                
                if (!$customer) {
                    Debug($ConnectionCube);
                    continue;
                }
                
                $controller_id = $controllersParse[$controller_id_name];
                $service_id = $serviciosParse[$plan_id_name];
            
                
                
                $connection_data = [
                    'controller_id' => $controllersParse[$controller_id_name],
                	'plan_id' => '',
                	'pool_id' => '',
                	'name' => $ConnectionCube['radiususer'],
                	'password' => $ConnectionCube['radiuspass'],
                	'ip' => '',
                	'validate_ip' => '',
                	'interface' => '',
                	'mac' => '',
                	'proportional' => '0',
                	'address' => $customer->address,
                	'area_id' => $customer->area_id,
                	'queue_name' => '',
                	'clave_wifi' => '',
                	'ports' => '',
                	'ip_alt' => '',
                	'ing_traffic' => '',
                	'comments' => "",
                	'customer_name' => $customer->name,
                	'customer_code' =>  $customer->code,
                	'service_pending_id' => '',
                	'lat' => $customer->lat,
                	'lng' => $customer->lng,
                	'btn-add' => 'exit'
                ];
                
                foreach ($controllers as $controller) {
                  
                    if ($controller->id == $controller_id) {
                        
                        $connection_data['controller_id'] = $controller->id;
                        
                        $controller_selected = $controller;
                        
                        foreach ($controller->tplans as $plan) {
                    
                            if ($plan->service->id == $service_id) {
                                
                                $connection_data['plan_id'] = $plan->id;
                                
                                $data = $this->IntegrationRouter->getFreeIPByPoolId($controller_selected, $plan->pool_id);

                                $connection_data['validate_ip'] = '1';
                                $connection_data['ip'] = $data['ip'];
                                $connection_data['pool_id'] = $plan->pool_id;
                              
                                break;
                                
                            }
                        }
                        
                        break;
                }
            }
             
            if ($connection_data['ip'] == '') {
                Debug($ConnectionCube); die();
            }

            $connection = $this->IntegrationRouter->addConnection($controller_selected, $connection_data);
                
            if (!$connection) {
                Debug($data);
                Debug($ConnectionCube); die();
            }

            $counter++;

            if ($error) {
                $this->Flash->error(__('Error al cargar'));
            } else {
                $this->Flash->success(__('Se cargaron ' . $counter . ' Clientes'));
                $this->Flash->error(__('Clientes faltan datos: ' . $error_counter));
            }
           
        }

        die();
    }
    
    public function importPatagoniaDBCubeConnetions($block = 0)
    {
        $salto = 300 * $block;
        
        $this->loadModel('Accounts');
        $this->loadModel('ConnectionsLabels');
        $paraments = $this->request->session()->read('paraments');
        
        $controllers = $this->IntegrationRouter->getAllControllersComplete();

        $controllersParse =[
            104 => 5, //r1.n7.tw -> N8-TW [Aserradero]	
            106 => 1, //r2.n1.tw -> N1-TW [Central]
            107 => 4, //r1.n12.tw -> N14-TW [Edificio]
            108 => 3, //r1.n1.pu -> N1-RW [CasaNico]
            109 => 2 //r1.n5.tw -> N18-TW [Cachito]
        ];
 
        $serviciosParse = [
            '4MB' => 45,
            'P10MB' => 43,
            'PB' => 47,
            'PE' => 42,
            'PE+IVA' => 41,
            'PE2' => 40,
            'PF' => 39,
            'PF+IVA' => 38,
            'Plan 10M' => 46,
            'PM' => 37,
            'PM+IVA' => 36,
            'PV' => 35,
        ];

        $error =  false;

        $first = true;
        $counter = 0;
        $error_counter = 0;
        
        $data = null;

        $this->loadModel('Users');
        $this->loadModel('Observations');

        $connection_patagonia_cube = ConnectionManager::get('244');
            
        $ConnectionsCube = $connection_patagonia_cube->execute('SELECT * FROM connections')->fetchAll('assoc');
        
        $ConnectionsCube = array_slice($ConnectionsCube, $salto);

        $connection_patagonia_815 = ConnectionManager::get('db815_gateway');
        
        $this->loadModel('Customers');
        
        $i = 0;
        
        foreach ($ConnectionsCube as $connCube) {
            
            if ($i > 300) {
                break;
            }
            $i++;
            
            $customer = $this->Customers->find()->where(['deleted' => false, 'comments' => $connCube['idcustomer'] ])->first();
            
            if ($customer) {
                
                $Connection815 = $connection_patagonia_815->execute("SELECT * FROM clientes_cuentasimple where nombre like '%".$customer->name."%'")->fetchAll('assoc');
                
                if (count($Connection815) > 0) {

                    if (!$Connection815[0]['acceso_dhcp_id']) {
                        continue;
                    }
                    
                    // Debug($customer->name .' = '. $Connection815[0]['nombre']);
                    
                    //aca tengo que crear la conexion
                    
                    $customer_code = $customer->code;
                    $plan_id_name = $connCube['idplan'];
                    $controller_id_name = $Connection815[0]['acceso_dhcp_id'];
                    $mac = $Connection815[0]['direccion_mac'];
                    $domicilio = $Connection815[0]['domicilio'];
                    
                    $mac = str_replace(':', '', $mac);
                    $mac = str_replace(' ', '', $mac);
                    $mac = str_replace('-', '', $mac);
                    
                    
                    if (!$plan_id_name || !$controller_id_name) {
                        
                          Debug("error: " . $i);
                          Debug($connCube);
                          continue;
                    }
                    
                    $controller_id = $controllersParse[$controller_id_name];
                    $service_id = $serviciosParse[$plan_id_name];
                    
                    $connection_data = [
                        'controller_id' => $controllersParse[$controller_id_name],
                    	'plan_id' => '',
                    	'pool_id' => '',
                    	'name' => null,
                    	'password' => null,
                    	'ip' => '',
                    	'validate_ip' => '',
                    	'interface' => '',
                    	'mac' => $mac,
                    	'proportional' => '0',
                    	'address' => $domicilio ? $domicilio : $customer->address,
                    	'area_id' => $customer->area_id,
                    	'queue_name' => '',
                    	'clave_wifi' => '',
                    	'ports' => '',
                    	'ip_alt' => '',
                    	'ing_traffic' => '',
                    	'comments' => "",
                    	'customer_name' => $customer->name,
                    	'customer_code' =>  $customer->code,
                    	'service_pending_id' => '',
                    	'lat' => $customer->lat,
                    	'lng' => $customer->lng,
                    	'btn-add' => 'exit'
                    ];
                    
                    foreach ($controllers as $controller) {
                          
                            if ($controller->id == $controller_id) {
                                
                                $connection_data['controller_id'] = $controller->id;
                                
                                $controller_selected = $controller;
                                
                                foreach ($controller->tplans as $plan) {
                            
                                    if ($plan->service->id == $service_id) {
                                        
                                        $connection_data['plan_id'] = $plan->id;
                                        
                                        $data = $this->IntegrationRouter->getFreeIPByPoolId($controller_selected, $plan->pool_id);
                                     
                                        $connection_data['validate_ip'] = '1';
                                        $connection_data['ip'] = $data['ip'];
                                        $connection_data['pool_id'] = $plan->pool_id;
                                      
                                        break;
                                        
                                    }
                                }
                                
                                break;
                            }
                    }

                    if ($connection_data['ip'] == '') {
                        
                         //$this->log("------error: ", 'debug');

                         //$this->log($connCube, 'debug'); 
                         
                         //$this->log('--------------------------------------', 'debug');
                         
                         continue;
                    }

                    $connection = $this->IntegrationRouter->addConnection($controller_selected, $connection_data);
                    
                    if (!$connection) {
                        
                         Debug("error: " . $i);
                         Debug($customer[0]);
                         Debug($Connection815[0]);
                         Debug($connCube); die();
                    }
                }
            
            }
           
        }

        die("cantidad: " . $i);
     
    }

    public function importGuilleDBCubeConnetionsDisable()
    {
        $customer = $this->Customers->newEntity();

        // if ($this->request->is('post')) {

            $this->loadModel('Accounts');
            $this->loadModel('ConnectionsLabels');
            $paraments = $this->request->session()->read('paraments');
            
              $controllers = $this->IntegrationRouter->getAllControllersComplete();
            
         
            $error =  false;

            $first = true;
            $counter = 0;
            $error_counter = 0;
            
            $connection_intranet_cube = ConnectionManager::get('intranet_cube');
            
            $ConnectionsCube = $connection_intranet_cube->execute('SELECT * FROM customer')->fetchAll('assoc');
            
            
            $data = null;
     
            $this->loadModel('Users');
            $this->loadModel('Observations');

            foreach ($ConnectionsCube as $ConnectionCube) {

                $customer_code = $ConnectionCube['idcustomer'];

                if ($ConnectionCube['status'] == 0) {
                    
                    continue;
                }

                $customer = $this->Customers->find()->where(['comments' => $customer_code])->first();
                
                if (!$customer) {
                    Debug($ConnectionCube);
                    continue;
                }

                $connections = $this->Connections->find()->where(['customer_code' => $customer->code]);

                foreach ($connections as $connection) {

                     if ($this->disableInternal($connection->id)) {
                        $counter++;
                     }
                    
                }

            if ($error) {
                $this->Flash->error(__('Error al cargar'));
            } else {
                $this->Flash->success(__('Se cargaron ' . $counter . ' Clientes'));
                $this->Flash->error(__('Clientes faltan datos: ' . $error_counter));
            }
           
        }

        die();
    }
    
    public function importPatagoniaDBCubeConnetionsDisable()
    {
        $customer = $this->Customers->newEntity();

        // if ($this->request->is('post')) {

            $this->loadModel('Accounts');
            $this->loadModel('ConnectionsLabels');
            $paraments = $this->request->session()->read('paraments');
            
            $controllers = $this->IntegrationRouter->getAllControllersComplete();

            $error =  false;

            $first = true;
            $counter = 0;
            $error_counter = 0;
            
            $connection_intranet_cube = ConnectionManager::get('244');
            
            $ConnectionsCube = $connection_intranet_cube->execute('SELECT * FROM customer')->fetchAll('assoc');

            $data = null;
     
            $this->loadModel('Users');
            $this->loadModel('Observations');

            foreach ($ConnectionsCube as $ConnectionCube) {

                $customer_code = $ConnectionCube['idcustomer'];

                if ($ConnectionCube['status'] == 0) {
                    
                    continue;
                }

                $customer = $this->Customers->find()->where(['comments' => $customer_code])->first();
                
                 if (!$customer) {
                    Debug($ConnectionCube);
                    continue;
                }

                $connections = $this->Connections->find()->where(['customer_code' => $customer->code]);

                foreach ($connections as $connection) {

                     if ($this->disableInternal($connection->id)) {
                        $counter++;
                     }

                }

            if ($error) {
                $this->Flash->error(__('Error al cargar'));
            } else {
                $this->Flash->success(__('Se cargaron ' . $counter . ' Clientes'));
                $this->Flash->error(__('Clientes faltan datos: ' . $error_counter));
            }
           
        }

        die();
    }

    /**
     * Called from:   (OLD)
     *  Connections/index
    */
    public function getConnections()
    {
       if ($this->request->is('ajax')) {

            $response = new \stdClass();

            $response->draw = intval($this->request->getQuery('draw'));

            $customer_ids_disabled = $this->getCustomersIdsBusinessDisabled();

            $where = [];
            $where += ['Connections.deleted' => false];
            if(count($customer_ids_disabled) > 0){
                $where += ['Connections.customer_code NOT IN' => $customer_ids_disabled];
            }

            $response->recordsTotal = $this->Connections->find()->where($where)->count();

            $response->data = $this->Connections->find('ServerSideData', [
                'params' => $this->request->getQuery(),
                'where' => $where
            ]);

            $response->recordsFiltered = $this->Connections->find('RecordsFiltered', [
                'params' => $this->request->getQuery(),
                'where' => $where
            ]);

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    /**
     * Called from: 
     *  Connections/index
     *  Connections/lockeds
    */
    public function getConnectionsByController()
    {
        if ($this->request->is('ajax')) {

            $controller_id = $this->request->getQuery('controller_id');
            $filter = $this->request->getQuery('filter');

            $connections = [];

            if ($controller_id != '') {

                $connections = $this->Connections->find()->contain([
                    'Users',
                    'Areas',
                    'Customers.Accounts',
                    'Controllers',
                    'Services'
                ])->select([
                    'Connections.id',
                    'Connections.address',
                    'Connections.created',
                    'Connections.modified',
                    'Connections.enabled',
                    'Customers.code',
                    'Customers.name',
                    'Customers.doc_type',
                    'Customers.ident',
                    'Customers.phone',
                    'Customers.billing_for_service',
                    'Customers.debt_month',
                    'Users.username',
                    'Areas.name',
                    'Controllers.name',
                    'Controllers.template',
                    'Services.name',
                    'Connections.ip',
                    'Connections.comments',
                    'Connections.mac',
                    'Connections.diff',
                    'Connections.debt_month',
                    'Accounts.saldo',
                    'Connections.queue_name',
                    'Connections.pppoe_name',
                    'Connections.pppoe_pass'
                ]);

                $where = ['Connections.deleted' => FALSE];

                if ($controller_id > 0) {
                    $where['Connections.controller_id'] = $controller_id;
                    $connections = $connections->where($where);
                }

                if ($filter != -1) {

                    switch ($filter) {
                        case 0: //lockeds
                            $where['Connections.enabled'] = 0;
                            $connections = $connections->where($where);
                            break;

                        case 1: //unlockeds
                            $where['Connections.enabled'] = 1;
                            $connections = $connections->where($where);
                            break;

                        case 2: //unlockeds
                            $where['Connections.diff IS NOT'] = NULL;
                            $connections = $connections->where($where);
                            break;
                    }
                } else {
                    $connections = $connections->where($where);
                }

                if ($controller_id == -2 ) {
                    $connections = $connections->order(['Connections.modified' => 'DESC'])->limit(100);
                }
            }

            $this->set(compact('connections'));
            $this->set('_serialize', ['connections']);
        }
    }

    public function clearErrors()
    {
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $connection_id = $this->request->input('json_decode')->connection_id;
         
            $connection = $this->Connections->get($connection_id, [
                'contain' => ['Customers', 'Controllers', 'Services']
            ]);
         
            $connection->diff = false;
            if (!$this->Connections->save($connection)) {
                 $this->set('data', false);
            } else {
                
                $action = 'Limpieza de Error de Conexion';
                $detail = 'Controlador: ' . $connection->controller->name . PHP_EOL; 
                $detail .= 'Servicio: ' . $connection->service->name . PHP_EOL; 
                $detail .= 'IP: '. $connection->ip . PHP_EOL; 
                $this->registerActivity($action, $detail, $connection->customer->code);
                
                $this->set('data', true);
            }
        }
    }

    public function lockedMasive()
    {
        if ($this->request->is('ajax')) {//Ajax Detection

            $response = new \stdClass;
            $response->error = false;
            $response->msg = '';
            $response->typeMsg = 'success';

            $this->loadModel('MessageTemplates');
            $this->loadModel('ConnectionsHasMessageTemplates');
            $messageTemplate = $this->MessageTemplates->find()->where(['deleted' => 0, 'type' => 'Bloqueo', 'enabled' => 1])
                ->order(['priority' => 'DESC'])->first();

            if (!$messageTemplate) {
                $response->error = true;
                $response->msg = __('No existe un aviso armado para el bloqueo de conexiones.');
                $response->typeMsg = 'warning';
            }

            if (!$response->error) {

                $ids = $this->request->input('json_decode')->ids;

                $amount = count($ids);

                $success = 0;
                $unsync = 0;
                $current_disabled = 0;

                $action = 'Bloqueo Masivo de Conexiones';
                $detail = '';
                $this->registerActivity($action, $detail, NULL, TRUE);

                foreach ($ids as $id) {

                    $connection = $this->Connections->get($id, [
                        'contain' => ['Customers', 'Controllers']
                    ]);

                    if (!$connection->enabled) {
                        $current_disabled++;
                        continue;
                    }

                    $connection = $this->IntegrationRouter->disableConnection($connection);
                     
                    if ($connection && !$connection->diff) {
                          $success++;
                    } else if ($connection && $connection->diff) {                         
                         $unsync++;
                    }

                    $connection->enabled = false;
                    $connection->blocking_date = Time::now();
                    $this->Connections->save($connection);

                    $chmt =  $this->ConnectionsHasMessageTemplates->newEntity();
                    $chmt->created = Time::now();
                    $chmt->connections_id = $connection->id;
                    $chmt->message_template_id = $messageTemplate->id;
                    $chmt->ip = $connection->ip;
                    $chmt->type = $messageTemplate->type;

                    $this->ConnectionsHasMessageTemplates->save($chmt);
                }

                $response->msg = __('{0} Conexiones seleccionadas. <br> {1} Estaban bloqueadas. <br> {2} Se Bloquearon correntamente. <br> {3} Falta sincronización.', $amount, $current_disabled, $success, $unsync);
            }

            $this->set('response', $response);
        }
    }

    public function unlockedMasive()
    {
       if ($this->request->is('ajax')) {//Ajax Detection

            $response = new \stdClass;
            $response->error = false;
            $response->msg = '';
            $response->typeMsg = 'success';

            $ids = $this->request->input('json_decode')->ids;

            $amount = count($ids);

            $i = 0;
            $a = 0;

            $this->loadModel('ConnectionsHasMessageTemplates');

            $action = 'Desbloqueo Masivo de Conexiones';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            foreach ($ids as $id) {

                $connection = $this->Connections->get($id, [
                    'contain' => ['Customers', 'Controllers']
                ]);

                if ($connection->enabled) {
                    $a++;
                    continue;
                }

                if ($this->IntegrationRouter->enableConnection($connection, true)) {

                    $connection->enabled = true;
                    $connection->enabling_date = Time::now();

                    if ($this->Connections->save($connection)) {

                        $chmt =  $this->ConnectionsHasMessageTemplates->find()->where(['connections_id' => $connection->id, 'type' => 'Bloqueo'])->first();

                        if ($this->ConnectionsHasMessageTemplates->delete($chmt)) {
                            $i++;
                        } 
                    } 
                }
            }

            $response->msg = __('{0} Conexiones seleccionadas. <br> {1} Estaban Habilitadas. <br> {2} Se Habilitaron.', $amount, $a, $i);

            $this->set('response', $response);
        }
    }

    public function changeLabels()
    {
        if ($this->request->is('ajax')) {

            $action = 'Cambio de Etiqueta Conexión';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $data_input = $this->request->input('json_decode');
            
            $connection = $this->Connections->get($data_input->connection_id, ['contain' => ['Labels']]);
            
            $data = $this->request->getData();
            $data['labels'] = [];
            $data['labels']['_ids'] = $data_input->labels;
            
            $connection = $this->Connections->patchEntity($connection, $data);
            
            if ($this->Connections->save($connection)) {
               $connection = $this->Connections->get($data_input->connection_id, ['contain' => ['Labels']]);
            } else {
                $connection = false;
            }

            $this->set('connection', $connection);
        }
    }

    public function lockedMasiveFromCron()
    {
        $paraments = $this->request->getSession()->read('paraments');
        $afip_codes = $this->request->getSession()->read('afip_codes');

        $data = new \stdClass;
        $data->error_msg = '';
        $data->ok = TRUE;
        $data->counter = 0;

        $this->loadModel('MessageTemplates');
        $messageTemplate = $this->MessageTemplates->find()
            ->where([
                'deleted' => 0,
                'type'    => 'Bloqueo',
                'enabled' => 1
            ])
            ->order(['priority' => 'DESC'])->first();

        $this->loadModel('DebitNotes');

        $action = 'Bloqueo Masivo de Conexiones desde Cron';
        $detail = '';
        $this->registerActivity($action, $detail, NULL, TRUE);

        if ($paraments->automatic_connection_blocking->force) { //configuracion general forzada

            if ($paraments->automatic_connection_blocking->type == 'connection' && $paraments->gral_config->billing_for_service) {

                $this->loadModel('Connections');

                $connections = $this->Connections->find()->contain(['Customers.Cities', 'Controllers', 'Services'])
                    ->where([
                        'Connections.debt_month >=' => $paraments->automatic_connection_blocking->due_debt,
                        'Connections.invoices_no_paid >=' => $paraments->automatic_connection_blocking->invoice_no_paid,
                        'Connections.deleted' => FALSE,
                        'Connections.enabled' => TRUE
                    ]);

                foreach ($connections as $connection) {

                    //VALIDACION DE COMPROMISO DE PAGO
                    //tiene compromiso de pago, tiene que saltar el corte por el compromiso de pago
                    if ($connection->customer->payment_commitment != NULL) {
                        continue;
                    }

                    // Salta porque no debe generar deuda la conexion
                    if (!$connection->force_debt) {
                        continue;
                    }

                    if ($this->IntegrationRouter->disableConnection($connection)) {

                        $connection->enabled = FALSE;
                        $connection->blocking_date = Time::now();

                        if ($this->Connections->save($connection)) {

                            $this->loadModel('ConnectionsHasMessageTemplates');

                            $chmt =  $this->ConnectionsHasMessageTemplates->newEntity();
                            $chmt->created = Time::now();
                            $chmt->connections_id = $connection->id;
                            $chmt->message_template_id = $messageTemplate->id;
                            $chmt->ip = $connection->ip;
                            $chmt->type = $messageTemplate->type;

                            if (!$this->ConnectionsHasMessageTemplates->save($chmt)) {
                                $data->ok = FALSE;
                                $data->error_msg .= 'no pudo crear ConnectionsHasMessageTemplates';
                                break;
                            }

                            //aplico recargo
                            if ($paraments->automatic_connection_blocking->surcharge_enabled 
                                && $paraments->automatic_connection_blocking->surcharge_value > 0) {

                                switch ($paraments->automatic_connection_blocking->surcharge_type) {

                                    case 'next_month':

                                        $debt_data = [
                                            'concept'              => $paraments->automatic_connection_blocking->surcharge_concept,
                                            'seating'              => NULL,
                                            'customer'             => $connection->customer,
                                            'value'                => $paraments->automatic_connection_blocking->surcharge_value,
                                            'alicuot'              => $connection->service->aliquot,
                                            'duedate'              => Time::now()->day($paraments->accountant->daydue)->modify('+1 month'),
                                            'connection_id'        => $connection->customer->billing_for_service ? $connection->id : NULL,
                                            'is_presupuesto_force' => $paraments->automatic_connection_blocking->surcharge_presupuesto,
                                            'period'               => Time::now()->format('m/Y'),
                                        ];

                                        $this->CurrentAccount->addDebt($debt_data);
                                        break;

                                    case 'this_month':

                                        $tipo_comp = 'NDX';

                                        if (!$paraments->automatic_connection_blocking->surcharge_presupuesto) {

                                            if ($connection->customer->is_presupuesto) {

                                                $tipo_comp ='NDX';

                                            } else {

                                                foreach ($paraments->invoicing->business as $business) {

                                                    if ($business->id == $connection->customer->business_billing) {
                                                        $connection->customer->business = $business;
                                                    }
                                                }

                                                $tipo_comp = $afip_codes['combinationsDebitNotes'][$connection->customer->business->responsible][$connection->customer->responsible];
                                            }
                                            
                                        }

                                        //parametros $total, $tax, $debt->quantity
                                        $row = $this->CurrentAccount->calulateIvaAndNeto($paraments->automatic_connection_blocking->surcharge_value, $afip_codes['alicuotas_percectage'][$connection->service->aliquot], 1);

                                        $concepts = [];

                                        $concept = new \stdClass;
                                        $concept->type        = 'S';
                                        $concept->code        = 99;
                                        $concept->description = $paraments->automatic_connection_blocking->surcharge_concept;
                                        $concept->quantity    = 1;
                                        $concept->unit        = 7;
                                        $concept->price       = $row->price;
                                        $concept->discount    = 0;
                                        $concept->sum_price   = $row->sum_price;;
                                        $concept->tax         = $connection->service->aliquot;
                                        $concept->sum_tax     = $row->sum_tax;
                                        $concept->total       = $row->total;
                                        $concepts[0] = $concept;

                                        $debitNote  = [
                                            'customer'      => $connection->customer,
                                            'concept_type'  => 2,
                                            'date'          => Time::now(),
                                            'date_start'    => Time::now(),
                                            'date_end'      => Time::now(),
                                            'duedate'       => Time::now(),
                                            'concepts'      => $concepts,
                                            'tipo_comp'     => $tipo_comp,
                                            'comments'      => $paraments->automatic_connection_blocking->surcharge_concept,
                                            'manual'        => true,
                                            'business_id'   => $connection->customer->business_billing,
                                            'connection_id' => $connection->customer->billing_for_service ? $connection->id : NULL
                                        ];

                                        $debitNote = $this->FiscoAfipComp->debitNote($debitNote);
                                        
                                        if ($debitNote) {
                                        
                                           //informo afip si no es de tipo X
    
                                           if ($this->FiscoAfip->informar($debitNote, FiscoAfipComp::TYPE_DEBIT_NOTE)) {
                    
                                               $this->DebitNotes->save($debitNote);
                                           } 
                                           else {
                                               $this->FiscoAfipComp->rollbackInvoice($debitNote, TRUE);
                                           }
                                            
                                        }else{
                                            $this->FiscoAfipComp->rollbackInvoice($debitNote, TRUE);
                                        }

                                        break;
                                }
                            } //end surcharge_enabled = true

                            $data->counter++;

                        } else {
                            $data->ok = FALSE;
                            $data->error_msg .= 'no pudo guardar la conexión';
                            break;
                        }
                    } else {
                        $data->ok = FALSE;
                        $data->error_msg .= 'no pudo deshabilitar la conexión';
                    }
                }
  
            } else { //tipo de control por cliente

                $this->loadModel('Customers');

                $customers = $this->Customers->find()->contain(['Connections', 'Cities'])
                    ->where([
                        'Customers.debt_month >=' => $paraments->automatic_connection_blocking->due_debt,
                        'Customers.invoices_no_paid >=' => $paraments->automatic_connection_blocking->invoice_no_paid,
                        'Customers.deleted' => FALSE,
                    ]);

                foreach ($customers as $customer) {

                    //VALIDACION DE COMPROMISO DE PAGO
                    //tiene compromiso de pago, tiene que saltar el corte por el compromiso de pago
                    if ($customer->payment_commitment != NULL) {
                        continue;
                    }

                    foreach ($customer->connections as $conn) {

                        if (!$conn->enabled) {
                            continue;
                        }

                        // Salta porque no debe generar deuda la conexion
                        if (!$conn->force_debt) {
                            continue;
                        }

                        $connection = $this->Connections->get($conn->id, [
                            'contain' => ['Customers.Cities', 'Controllers', 'Services']
                        ]);

                        if ($this->IntegrationRouter->disableConnection($connection)) {

                            $connection->enabled = FALSE;

                            if ($this->Connections->save($connection)) {

                                $this->loadModel('ConnectionsHasMessageTemplates');

                                $chmt =  $this->ConnectionsHasMessageTemplates->newEntity();
                                $chmt->created = Time::now();
                                $chmt->connections_id = $connection->id;
                                $chmt->message_template_id = $messageTemplate->id;
                                $chmt->ip = $connection->ip;
                                $chmt->type = $messageTemplate->type;

                                if (!$this->ConnectionsHasMessageTemplates->save($chmt)) {
                                    $data->ok = FALSE;
                                    $data->error_msg .= 'no pudo crear ConnectionsHasMessageTemplates';
                                    break;
                                }

                                //aplico recargo
                                if ($paraments->automatic_connection_blocking->surcharge_enabled 
                                    && $paraments->automatic_connection_blocking->surcharge_value > 0) {

                                    switch ($paraments->automatic_connection_blocking->surcharge_type) {

                                        case 'next_month':

                                            $debt_data = [
                                                'concept'              => $paraments->automatic_connection_blocking->surcharge_concept,
                                                'seating'              => NULL,
                                                'customer'             => $customer,
                                                'value'                => $paraments->automatic_connection_blocking->surcharge_value,
                                                'alicuot'              => $connection->service->aliquot,
                                                'duedate'              => Time::now()->day($paraments->accountant->daydue)->modify('+1 month'),
                                                'connection_id'        => $connection->customer->billing_for_service ? $connection->id : NULL,
                                                'is_presupuesto_force' => $paraments->automatic_connection_blocking->surcharge_presupuesto,
                                                'period'               => Time::now()->format('m/Y'),
                                            ];

                                            $this->CurrentAccount->addDebt($debt_data);
                                            break;

                                        case 'this_month':

                                            $tipo_comp = 'NDX';

                                            if (!$paraments->automatic_connection_blocking->surcharge_presupuesto) {

                                                if ($customer->is_presupuesto) { 

                                                    $tipo_comp ='NDX';

                                                } else {

                                                    foreach ($paraments->invoicing->business as $business) {

                                                        if ($business->id == $customer->business_billing) {
                                                            $customer->business = $business;
                                                        }
                                                    }

                                                    $tipo_comp = $afip_codes['combinationsDebitNotes'][$customer->business->responsible][$customer->responsible];
                                                }
                                                
                                            }

                                            //parametros $total, $tax, $debt->quantity
                                            $row = $this->CurrentAccount->calulateIvaAndNeto($paraments->automatic_connection_blocking->surcharge_value, $afip_codes['alicuotas_percectage'][$connection->service->aliquot], 1);

                                            $concepts = [];

                                            $concept = new \stdClass;
                                            $concept->type        = 'S';
                                            $concept->code        = 99;
                                            $concept->description = $paraments->automatic_connection_blocking->surcharge_concept;
                                            $concept->quantity    = 1;
                                            $concept->unit        = 7;
                                            $concept->price       = $row->price;
                                            $concept->discount    = 0;
                                            $concept->sum_price   = $row->sum_price;;
                                            $concept->tax         = $connection->service->aliquot;
                                            $concept->sum_tax     = $row->sum_tax;
                                            $concept->total       = $row->total;
                                            $concepts[0] = $concept;

                                            $debitNote  = [
                                                'customer'      => $customer,
                                                'concept_type'  => 2,
                                                'date'          => Time::now(),
                                                'date_start'    => Time::now(),
                                                'date_end'      => Time::now(),
                                                'duedate'       => Time::now(),
                                                'concepts'      => $concepts,
                                                'tipo_comp'     => $tipo_comp,
                                                'comments'      => $paraments->automatic_connection_blocking->surcharge_concept,
                                                'manual'        => TRUE,
                                                'business_id'   => $customer->business_billing,
                                                'connection_id' => $connection->customer->billing_for_service ? $connection->id : NULL
                                            ];

                                            $debitNote = $this->FiscoAfipComp->debitNote($debitNote);
                                        
                                            if ($debitNote) {
                                            
                                               //informo afip si no es de tipo X
        
                                               if ($this->FiscoAfip->informar($debitNote, FiscoAfipComp::TYPE_DEBIT_NOTE)) {
                        
                                                   $this->DebitNotes->save($debitNote);
                                               } 
                                               else {
                                                   $this->FiscoAfipComp->rollbackInvoice($debitNote, TRUE);
                                               }
                                                
                                            }else{
                                                $this->FiscoAfipComp->rollbackInvoice($debitNote, TRUE);
                                            }
                                            
                                            break;
                                    }
                                } //end surcharge_enabled = true

                                $data->counter++;

                            } else {
                                $data->ok = FALSE;
                                $data->error_msg .= 'no pudo guardar la conexion';
                                break;
                            }
                        } else {
                            $data->ok = FALSE;
                            $data->error_msg .= 'no pudo deshabilitar la conexion';
                        }
                    }
                }
            }

        } else { //si la configuracion general NO esta forzada

            $this->loadModel('Customers');

            $customers = $this->Customers
                ->find()
                ->contain([
                    'Connections', 
                    'Cities'
                ])
                ->where([
                    'Customers.deleted' => FALSE,
                ]);

            foreach ($customers as $customer) { //recorro los clientes

                //VALIDACION DE COMPROMISO DE PAGO
                //tiene compromiso de pago, tiene que saltar el corte por el compromiso de pago
                if ($customer->payment_commitment != NULL) {
                    continue;
                }

                if (!$customer->acb_enabled) {
                    continue;
                }

                if ($customer->acb_type == 'connection') { //tipo de control por conexion

                    foreach ($customer->connections as $conn) {

                        if (!$conn->enabled) {
                            continue;
                        }

                        // Salta porque no debe generar deuda la conexion
                        if (!$conn->force_debt) {
                            continue;
                        }

                        if ($conn->debt_month >= $customer->acb_due_debt 
                            && $conn->invoices_no_paid >= $customer->acb_invoice_no_paid) {

                            $connection = $this->Connections->get($conn->id, [
                                'contain' => ['Customers.Cities', 'Controllers', 'Services']
                            ]);

                            if ($this->IntegrationRouter->disableConnection($connection)) {

                                $connection->enabled = FALSE;

                                if ($this->Connections->save($connection)) {

                                    $this->loadModel('ConnectionsHasMessageTemplates');

                                    $chmt =  $this->ConnectionsHasMessageTemplates->newEntity();
                                    $chmt->created = Time::now();
                                    $chmt->connections_id = $connection->id;
                                    $chmt->message_template_id = $messageTemplate->id;
                                    $chmt->ip = $connection->ip;
                                    $chmt->type = $messageTemplate->type;

                                    if (!$this->ConnectionsHasMessageTemplates->save($chmt)) {
                                        $data->ok = FALSE;
                                        break;
                                    }

                                    //aplico recargo
                                    if ($customer->acb_surcharge_enabled 
                                        && $paraments->automatic_connection_blocking->surcharge_value > 0) {

                                        switch ($paraments->automatic_connection_blocking->surcharge_type) {

                                            case 'next_month':

                                                $debt_data = [
                                                    'concept'              => $paraments->automatic_connection_blocking->surcharge_concept,
                                                    'seating'              => NULL,
                                                    'customer'             => $customer,
                                                    'value'                => $paraments->automatic_connection_blocking->surcharge_value,
                                                    'alicuot'              => $connection->service->aliquot,
                                                    'duedate'              => Time::now()->day($paraments->accountant->daydue)->modify('+1 month'),
                                                    'connection_id'        => $connection->customer->billing_for_service ? $connection->id : NULL,
                                                    'is_presupuesto_force' => $paraments->automatic_connection_blocking->surcharge_presupuesto,
                                                    'period'               => Time::now()->format('m/Y'),
                                                ];

                                                $this->CurrentAccount->addDebt($debt_data);

                                                break;

                                            case 'this_month':

                                                $tipo_comp = 'NDX';

                                                if (!$paraments->automatic_connection_blocking->surcharge_presupuesto) {

                                                    if ($customer->is_presupuesto) { 

                                                        $tipo_comp ='NDX';

                                                    } else {

                                                        foreach ($paraments->invoicing->business as $business) {

                                                            if ($business->id == $customer->business_billing) {
                                                                $customer->business = $business;
                                                            }
                                                        }

                                                        $tipo_comp = $afip_codes['combinationsDebitNotes'][$customer->business->responsible][$customer->responsible];
                                                    }
                                                    
                                                }

                                                //parametros $total, $tax, $debt->quantity
                                                $row = $this->CurrentAccount->calulateIvaAndNeto($paraments->automatic_connection_blocking->surcharge_value, $afip_codes['alicuotas_percectage'][$connection->service->aliquot], 1);

                                                $concepts = [];

                                                $concept = new \stdClass;
                                                $concept->type        = 'S';
                                                $concept->code        = 99;
                                                $concept->description = $paraments->automatic_connection_blocking->surcharge_concept;
                                                $concept->quantity    = 1;
                                                $concept->unit        = 7;
                                                $concept->price       = $row->price;
                                                $concept->discount    = 0;
                                                $concept->sum_price   = $row->sum_price;;
                                                $concept->tax         = $connection->service->aliquot;
                                                $concept->sum_tax     = $row->sum_tax;
                                                $concept->total       = $row->total;
                                                $concepts[0] = $concept;

                                                $debitNote  = [
                                                    'customer'      => $customer,
                                                    'concept_type'  => 2,
                                                    'date'          => Time::now(),
                                                    'date_start'    => Time::now(),
                                                    'date_end'      => Time::now(),
                                                    'duedate'       => Time::now(),
                                                    'concepts'      => $concepts,
                                                    'tipo_comp'     => $tipo_comp,
                                                    'comments'      => $paraments->automatic_connection_blocking->surcharge_concept,
                                                    'manual'        => TRUE,
                                                    'business_id'   => $customer->business_billing,
                                                    'connection_id' => $connection->customer->billing_for_service ? $connection->id : NULL
                                                ];

                                                $debitNote = $this->FiscoAfipComp->debitNote($debitNote);
                                            
                                                if ($debitNote) {
                                                
                                                   //informo afip si no es de tipo X
            
                                                   if ($this->FiscoAfip->informar($debitNote, FiscoAfipComp::TYPE_DEBIT_NOTE)) {
                            
                                                       $this->DebitNotes->save($debitNote);
                                                   } 
                                                   else {
                                                       $this->FiscoAfipComp->rollbackInvoice($debitNote, TRUE);
                                                   }
                                                    
                                                }else{
                                                    $this->FiscoAfipComp->rollbackInvoice($debitNote, TRUE);
                                                }

                                                break;
                                        }
                                    } //end surcharge_enabled = true

                                    $data->counter++;

                                } else {
                                    $data->ok = FALSE;
                                    break;
                                }
                            } else {
                                $data->ok = FALSE;
                            }
                        }
                    }

                } else { //tipo de control por cliente

                    if ($customer->debt_month >= $customer->acb_due_debt 
                        && $customer->invoices_no_paid >= $customer->acb_invoice_no_paid) {

                        foreach ($customer->connections as $conn) {

                            if (!$conn->enabled) {
                                continue;
                            }

                            // Salta porque no debe generar deuda la conexion
                            if (!$conn->force_debt) {
                                continue;
                            }

                            $connection = $this->Connections->get($conn->id, [
                                'contain' => ['Customers.Cities', 'Controllers', 'Services']
                            ]);

                            if ($this->IntegrationRouter->disableConnection($connection)) {

                                $connection->enabled = FALSE;

                                if ($this->Connections->save($connection)) {

                                    $this->loadModel('ConnectionsHasMessageTemplates');

                                    $chmt = $this->ConnectionsHasMessageTemplates->newEntity();
                                    $chmt->created = Time::now();
                                    $chmt->connections_id = $connection->id;
                                    $chmt->message_template_id = $messageTemplate->id;
                                    $chmt->ip = $connection->ip;
                                    $chmt->type = $messageTemplate->type;

                                    if (!$this->ConnectionsHasMessageTemplates->save($chmt)) {
                                        $data->ok = FALSE;
                                        $data->error_msg .= 'no pudo crear ConnectionsHasMessageTemplates';
                                        break;
                                    }

                                    //aplico recargo
                                    if ($customer->acb_surcharge_enabled 
                                        && $paraments->automatic_connection_blocking->surcharge_value > 0) {

                                        switch ($paraments->automatic_connection_blocking->surcharge_type) {

                                            case 'next_month':

                                                $debt_data = [
                                                    'concept'              => $paraments->automatic_connection_blocking->surcharge_concept,
                                                    'seating'              => NULL,
                                                    'customer'             => $customer,
                                                    'value'                => $paraments->automatic_connection_blocking->surcharge_value,
                                                    'alicuot'              => $connection->service->aliquot,
                                                    'duedate'              => Time::now()->day($paraments->accountant->daydue)->modify('+1 month'),
                                                    'connection_id'        => $connection->customer->billing_for_service ? $connection->id : NULL,
                                                    'is_presupuesto_force' => $paraments->automatic_connection_blocking->surcharge_presupuesto,
                                                    'period'               => Time::now()->format('m/Y'),
                                                ];

                                                $debt = $this->CurrentAccount->addDebt($debt_data);

                                                break;

                                            case 'this_month':

                                                $tipo_comp = 'NDX';

                                                if (!$paraments->automatic_connection_blocking->surcharge_presupuesto) {

                                                    if ($customer->is_presupuesto) { 

                                                        $tipo_comp ='NDX';

                                                    } else {

                                                        foreach ($paraments->invoicing->business as $business) {

                                                            if ($business->id == $customer->business_billing) {
                                                                $customer->business = $business;
                                                            }
                                                        }

                                                        $tipo_comp = $afip_codes['combinationsDebitNotes'][$customer->business->responsible][$customer->responsible];
                                                    }
                                                    
                                                }

                                                //parametros $total, $tax, $debt->quantity
                                                $row = $this->CurrentAccount->calulateIvaAndNeto($paraments->automatic_connection_blocking->surcharge_value, $afip_codes['alicuotas_percectage'][$connection->service->aliquot], 1);

                                                $concepts = [];

                                                $concept = new \stdClass;
                                                $concept->type        = 'S';
                                                $concept->code        = 99;
                                                $concept->description = $paraments->automatic_connection_blocking->surcharge_concept;
                                                $concept->quantity    = 1;
                                                $concept->unit        = 7;
                                                $concept->price       = $row->price;
                                                $concept->discount    = 0;
                                                $concept->sum_price   = $row->sum_price;;
                                                $concept->tax         = $connection->service->aliquot;
                                                $concept->sum_tax     = $row->sum_tax;
                                                $concept->total       = $row->total;
                                                $concepts[0] = $concept;

                                                $debitNote  = [
                                                    'customer'      => $customer,
                                                    'concept_type'  => 2,
                                                    'date'          => Time::now(),
                                                    'date_start'    => Time::now(),
                                                    'date_end'      => Time::now(),
                                                    'duedate'       => Time::now(),
                                                    'concepts'      => $concepts,
                                                    'tipo_comp'     => $tipo_comp,
                                                    'comments'      => $paraments->automatic_connection_blocking->surcharge_concept,
                                                    'manual'        => TRUE,
                                                    'business_id'   => $customer->business_billing,
                                                    'connection_id' => $connection->customer->billing_for_service ? $connection->id : NULL
                                                ];

                                                
                                                $debitNote = $this->FiscoAfipComp->debitNote($debitNote);
                                        
                                                if ($debitNote) {
                                                
                                                   //informo afip si no es de tipo X
            
                                                   if ($this->FiscoAfip->informar($debitNote, FiscoAfipComp::TYPE_DEBIT_NOTE)) {
                            
                                                       $this->DebitNotes->save($debitNote);
                                                   } 
                                                   else {
                                                       $this->FiscoAfipComp->rollbackInvoice($debitNote, TRUE);
                                                   }
                                                    
                                                }else{
                                                    $this->FiscoAfipComp->rollbackInvoice($debitNote, TRUE);
                                                }
                                                
                                                break;
                                        }
                                    } //end surcharge_enabled = true

                                    $data->counter++;

                                } else {
                                    $data->ok = FALSE;
                                    $data->error_msg .= 'no pudo guardar la conexión';
                                    break;
                                }
                            } else {
                                $data->ok = FALSE;
                                $data->error_msg .= 'no pudo deshabilitar la conexión';
                            }
                        }
                    }
                }
            }//end foreach customers

        }

        return $data;
    }

    public function editConnectionFromDebt()
    {
        if ($this->request->is(['ajax'])) {

            $action = 'Edición Administrativa de Conexión';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $data = $this->request->input('json_decode');

            $connection = $this->Connections->get($data->id);
            $connection->force_debt = $data->force_debt;

            if ($this->Connections->save($connection)) {
                $connection = true;
            } else {
                $connection = false;
            }

            $this->set('connection', $connection);
        }
    }

    /* administracion */

    public function administrative($id)
    {
        $connection = $this->Connections->get($id, [
            'contain' => [
                'Customers.CustomersAccounts', 
                'Controllers', 
                'Services'
            ]
        ]);

        $payment_getway = $this->request->getSession()->read('payment_getway');
        $paraments = $this->request->getSession()->read('paraments');

        $auto_debits = [];
        foreach ($payment_getway->methods as $pg) {
            $config = $pg->config;
            if ($payment_getway->config->$config->enabled) {
                if ($pg->auto_debit) {
                    array_push($auto_debits, $pg->id);
                }
            }
        }

        $this->loadModel('Discounts');

        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Edición Administrativa de Conexión';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            if ($this->request->getData('new_discount_id') != '') {

                $discount = $this->Discounts->get($this->request->getData('new_discount_id'));

                $connection->discount_total_month = $discount->duration;
                $connection->discount_month       = $discount->duration;
                $connection->discount_value       = $discount->value;
                $connection->discount_description = $discount->concept;
                $connection->discount_code        = $discount->code;
                $connection->discount_alicuot     = $discount->alicuot;
                $connection->discount_always      = $discount->always;
            } else {

                $data = $this->request->getData();
                $connection = $this->Connections->patchEntity($connection, $data);
            }

            $connection->force_debt = $this->request->getData('force_debt') == '1' ? TRUE : FALSE;  

            if ($paraments->gral_config->billing_for_service) {
                foreach ($connection->customer->customers_accounts as $customer_account) {
                    if (!$customer_account->deleted) {
                        foreach ($payment_getway->methods as $key => $pg) {
                            if ($pg->id == $customer_account->payment_getway_id) {
                                $connection->$key = $this->request->getData($key) == '1' ? TRUE : FALSE;
                            }
                        }
                    }
                }
            }

            if ($this->Connections->save($connection)) {

                $this->Flash->success(__('Cambios guardados correctamente.'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Error al intentar guardar los cambios.'));
            }
        }

        $discounts = $this->Discounts->find()->where([
            'deleted' => false, 
            'enabled' => true, 
            'type'    => 'service'
        ]);

        $discounts_list = [];
        foreach ($discounts as $discount) {
            $discounts_list[$discount->id] = $discount->concept;
        }

        $this->set(compact('connection', 'discounts_list', 'payment_getway'));
        $this->set('_serialize', ['connection']);
    } 

    public function getCounterUnsync()
    {
        //Ajax Detection
        if ($this->request->is('Ajax')) {

            $paraments = $this->request->getSession()->read('paraments');

            $controllers_unsync = -1;

            //verifico conecxion con el controlador 
            if ($paraments->system->integration) {

                $this->loadModel('Controllers');

                $controllers_unsync = $this->Controllers->find()->where([
                   'deleted' => false,
                   'enabled' => true,
                   ])
                   ->toArray();

            }
            $this->set('controllers_unsync', $controllers_unsync);
        }
    }

    /**
    * Called from:   (OLD)
    *  Connections/index_adminitrative
    */
    public function getConnectionsAdministrative()
    {
       if ($this->request->is('ajax')) {

            $response = new \stdClass();

            $response->draw = intval($this->request->query['draw']);

            $response->recordsTotal = $this->Connections->find()->where(['Connections.deleted' => FALSE])->count();

            $response->data = $this->Connections->find('ServerSideDataAdministrative', [
                'params' => $this->request->query,
                'where' => ['Connections.deleted' => FALSE]
            ])->contain([]);

            $response->recordsFiltered = $this->Connections->find('RecordsFilteredAdministrative', [
                'params' => $this->request->query,
                'where' => ['Connections.deleted' => FALSE]
            ]);

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function applyMassiveDebt()
    {
        if ($this->request->is('ajax')) {

            $paraments = $this->request->getSession()->read('paraments');
            $afip_codes = $this->request->getSession()->read('afip_codes');

            $response = new \stdClass;
            $response->error = false;
            $response->msg = '';
            $response->typeMsg = 'success';

            $data = $this->request->input('json_decode');

            $ids = $data->ids;

            $amount = count($ids);

            $success = 0;
            $error = 0;

            $this->loadModel('DebitNotes');

            $action = 'Aplicación Masiva de Deudas';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            foreach ($ids as $id) {

                $connection = $this->Connections->get($id, [
                    'contain' => ['Customers.Cities', 'Services']
                ]);

                $customer = $connection->customer;

                switch ($data->surcharge_type) {

                    case 'next_month':

                        $next_month_now = Time::now();
                        $hms = $next_month_now->i18nFormat('HH:mm:ss');

                        $duedate = explode("/", $data->duedate);
                        $duedate = $duedate[2] . '-' . $duedate[1] . '-' . $duedate[0] . ' ' . $hms;
                        $duedate = new Time($duedate);

                        $debt_data = [
                            'concept'              => $data->concept,
                            'seating'              => NULL,
                            'customer'             => $customer,
                            'value'                => $data->importe,
                            'alicuot'              => $connection->service->aliquot,
                            'duedate'              => $duedate,
                            'connection_id'        => $id,
                            'is_presupuesto_force' => $data->presupuesto,
                            'period'               => $data->periode,
                        ];

                        if ($this->CurrentAccount->addDebt($debt_data)) {
                            $success++;
                        } else {
                            $error++;
                        }
                        break;

                    case 'this_month':

                        $tipo_comp = 'NDX';

                        if (!$data->presupuesto) {

                            if ($customer->is_presupuesto) { 

                                $tipo_comp ='NDX';

                            } else {

                                foreach ($paraments->invoicing->business as $business) {

                                    if ($business->id == $customer->business_billing) {
                                        $customer->business = $business;
                                    }
                                }

                                $tipo_comp = $afip_codes['combinationsDebitNotes'][$customer->business->responsible][$customer->responsible];
                            }
                            
                        }

                        //parametros $total, $tax, $debt->quantity
                        $row = $this->CurrentAccount->calulateIvaAndNeto($data->importe, $afip_codes['alicuotas_percectage'][$connection->service->aliquot], 1);

                        $concepts = [];

                        $concept = new \stdClass;
                        $concept->type        = 'S';
                        $concept->code        = 99;
                        $concept->description = $data->concept;
                        $concept->quantity    = 1;
                        $concept->unit        = 7;
                        $concept->price       = $row->price;
                        $concept->discount    = 0;
                        $concept->sum_price   = $row->sum_price;;
                        $concept->tax         = $connection->service->aliquot;
                        $concept->sum_tax     = $row->sum_tax;
                        $concept->total       = $row->total;
                        $concepts[0] = $concept;

                        $debitNote  = [
                            'customer'      => $customer,
                            'concept_type'  => 2,
                            'date'          => Time::now(),
                            'date_start'    => Time::now(),
                            'date_end'      => Time::now(),
                            'duedate'       => Time::now(),
                            'concepts'      => $concepts,
                            'tipo_comp'     => $tipo_comp,
                            'comments'      => $data->concept,
                            'manual'        => TRUE,
                            'business_id'   => $customer->business_billing,
                            'connection_id' => $id
                        ];

                        $debitNote = $this->FiscoAfipComp->debitNote($debitNote);

                        if ($debitNote) {

                           //informo afip si no es de tipo X

                           if ($this->FiscoAfip->informar($debitNote, FiscoAfipComp::TYPE_DEBIT_NOTE)) {

                               if ($this->DebitNotes->save($debitNote)) {
                                   $success++;
                               } else {
                                   $error++;
                               }
                           } else {
                               $this->FiscoAfipComp->rollbackInvoice($debitNote, TRUE);
                               $error++;
                           }

                        } else {
                            $this->FiscoAfipComp->rollbackInvoice($debitNote, TRUE);
                            $error++;
                        }
                        break;
                }
            }

            $response->msg = __('{0} Conexiones seleccionadas. <br> {1} Conexiones generaron recargos. <br> {2} Conexiones no generaron recargos.', $amount, $success, $error);

            $this->set('response', $response);
        }
    }

    public function applyMassiveDiscount()
    {
        if ($this->request->is('ajax')) {

            $paraments = $this->request->getSession()->read('paraments');
            $afip_codes = $this->request->getSession()->read('afip_codes');

            $response = new \stdClass;
            $response->error = false;
            $response->msg = '';
            $response->typeMsg = 'success';

            $data = $this->request->input('json_decode');
            $ids = $data->ids;

            $amount = count($ids);

            $success = 0;
            $error = 0;

            $this->loadModel('CreditNotes');

            $action = 'Aplicación Masiva de Descuentos';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            foreach ($ids as $id) {

                $connection = $this->Connections->get($id, [
                    'contain' => ['Customers.Cities', 'Services']
                ]);

                $customer = $connection->customer;

                switch ($data->surcharge_type) {

                    case 'next_month':

                        $discount_data = [
                            'concept'              => $data->concept,
                            'seating'              => NULL,
                            'customer'             => $customer,
                            'value'                => $data->importe,
                            'alicuot'              => $connection->service->aliquot,
                            'connection_id'        => $id,
                            'is_presupuesto_force' => $data->presupuesto,
                            'period'               => $data->periode,
                        ];

                        if ($this->CurrentAccount->addDiscountWithoutDebt($discount_data)) {
                            $success++;
                        } else {
                            $error++;
                        }
                        break;

                    case 'this_month':

                        $tipo_comp = 'NCX';

                        if (!$data->presupuesto) {

                            if ($customer->is_presupuesto) { 

                                $tipo_comp ='NCX';

                            } else {

                                foreach ($paraments->invoicing->business as $business) {

                                    if ($business->id == $customer->business_billing) {
                                        $customer->business = $business;
                                    }
                                }

                                $tipo_comp = $afip_codes['combinationsCreditNotes'][$customer->business->responsible][$customer->responsible];
                            }

                        }

                        //parametros $total, $tax, $debt->quantity
                        $row = $this->CurrentAccount->calulateIvaAndNeto($data->importe, $afip_codes['alicuotas_percectage'][$connection->service->aliquot], 1);

                        $concepts = [];

                        $concept = new \stdClass;
                        $concept->type        = 'S';
                        $concept->code        = 99;
                        $concept->description = $data->concept;
                        $concept->quantity    = 1;
                        $concept->unit        = 7;
                        $concept->price       = $row->price;
                        $concept->discount    = 0;
                        $concept->sum_price   = $row->sum_price;;
                        $concept->tax         = $connection->service->aliquot;
                        $concept->sum_tax     = $row->sum_tax;
                        $concept->total       = $row->total;
                        $concepts[0] = $concept;

                        $creditNote  = [
                            'customer'      => $customer,
                            'concept_type'  => 2,
                            'date'          => Time::now(),
                            'date_start'    => Time::now(),
                            'date_end'      => Time::now(),
                            'duedate'       => Time::now(),
                            'concepts'      => $concepts,
                            'tipo_comp'     => $tipo_comp,
                            'comments'      => $data->concept,
                            'manual'        => TRUE,
                            'business_id'   => $customer->business_billing,
                            'connection_id' => $id
                        ];

                        $creditNote = $this->FiscoAfipComp->creditNote($creditNote);

                        if ($creditNote) {

                           //informo afip si no es de tipo X

                           if ($this->FiscoAfip->informar($creditNote, FiscoAfipComp::TYPE_CREDIT_NOTE)) {

                               if ($this->CreditNotes->save($creditNote)) {
                                   $success++;
                               } else {
                                   $error++;
                               }
                           } else {
                               $this->FiscoAfipComp->rollbackInvoice($creditNote, TRUE);
                               $error++;
                           }

                        } else {
                            $this->FiscoAfipComp->rollbackInvoice($creditNote, TRUE);
                            $error++;
                        }
                        break;
                }
            }

            $response->msg = __('{0} Conexiones seleccionadas. <br> {1} Conexiones generaron descuentos. <br> {2} Conexiones no generaron descuentos.', $amount, $success, $error);

            $this->set('response', $response);
        }
    }

    public function lockedMasiveFromCustomers()
    {
        if ($this->request->is('ajax')) {//Ajax Detection

            $response = new \stdClass;
            $response->error = false;
            $response->msg = '';
            $response->typeMsg = 'success';

            $this->loadModel('MessageTemplates');
            $this->loadModel('ConnectionsHasMessageTemplates');
            $this->loadModel('Customers');

            $messageTemplate = $this->MessageTemplates->find()->where(['deleted' => 0, 'type' => 'Bloqueo', 'enabled' => 1])
                ->order(['priority' => 'DESC'])->first();

            if (!$messageTemplate) {
                $response->error = true;
                $response->msg = __('No existe un aviso armado para el bloqueo de conexiones.');
                $response->typeMsg = 'warning';
            }

            if (!$response->error) {

                $ids = $this->request->input('json_decode')->ids;
                $blocking_payment_commitment = $this->request->input('json_decode')->blocking_payment_commitment;

                $amount = count($ids);

                $success = 0;
                $unsync = 0;
                $current_disabled = 0;

                $action = 'Bloqueo Masivo de Conexiones';
                $detail = '';
                $this->registerActivity($action, $detail, NULL, TRUE);

                foreach ($ids as $id) {

                    $customer = $this->Customers->get($id, [
                        'contain' => ['Connections.Controllers']
                    ]);

                    //si no esta marcado para que bloquee igual
                    if ($customer->payment_commitment != NULL) {
                        if (!$blocking_payment_commitment) {
                            continue;
                        }
                    }

                    foreach ($customer->connections as $connection) {

                        if ($connection->deleted) {
                            continue;
                        }

                        $customerx = new \stdClass;
                        $customerx->code = $customer->code;
                        $customerx->name = $customer->name;
                        $connection->customer = $customerx;

                         if (!$connection->enabled) {
                            $current_disabled++;
                            continue;
                        }

                        $connection = $this->IntegrationRouter->disableConnection($connection);

                        if ($connection && !$connection->diff) {
                              $success++;
                        } else if ($connection && $connection->diff) {                         
                             $unsync++;
                        }

                        if ($connection) {

                            $connection->enabled = false;
                            $connection->blocking_date = Time::now();
                            $this->Customers->Connections->save($connection);

                            $chmt =  $this->ConnectionsHasMessageTemplates->newEntity();
                            $chmt->created = Time::now();
                            $chmt->connections_id = $connection->id;
                            $chmt->message_template_id = $messageTemplate->id;
                            $chmt->ip = $connection->ip;
                            $chmt->type = $messageTemplate->type;

                            $this->ConnectionsHasMessageTemplates->save($chmt);
                        }
                    }
                }

                $response->msg = __('{0} Clientes seleccionados. <br> {1} Estaban bloqueados. <br> {2} Se Bloquearon correntamente. <br> {3} Falta sincronización.', $amount, $current_disabled, $success, $unsync);
            }

            $this->set('response', $response);
        }
    }

    public function unlockedMasiveFromCustomers()
    {
       if ($this->request->is('ajax')) {//Ajax Detection

            $response = new \stdClass;
            $response->error = false;
            $response->msg = '';
            $response->typeMsg = 'success';

            $ids = $this->request->input('json_decode')->ids;

            $amount = count($ids);

            $i = 0;
            $a = 0;
            $this->loadModel('ConnectionsHasMessageTemplates');

            $action = 'Desbloqueo Masivo de Conexiones';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            foreach ($ids as $id) {

                $customer = $this->Customers->get($id, [
                    'contain' => ['Connections.Controllers']
                ]);

                foreach ($customer->connections as $connection) {

                    if ($connection->enabled) {
                        $a++;
                        continue;
                    }

                    if ($this->IntegrationRouter->enableConnection($connection, true)) {
    
                        $connection->enabled = true;
                        $connection->enabling_date = Time::now();

                        if ($this->Customers->Connections->save($connection)) {

                            $chmt =  $this->ConnectionsHasMessageTemplates->find()->where(['connections_id' => $connection->id, 'type' => 'Bloqueo'])->first();

                            if ($this->ConnectionsHasMessageTemplates->delete($chmt)) {
                                $i++;
                            } 
                        } 
                    }
                }
            }

            $response->msg = __('{0} Clientes seleccionadas. <br> {1} Estaban Habilitadas. <br> {2} Se Habilitaron.', $amount, $a, $i);

            $this->set('response', $response);
        }
    }

    public function forceExecution()
    {
        if ($this->request->is('post')) {

            $action = 'Ejecución de control de bloqueo automático forzada (Cron)';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $command = ROOT . "/bin/cake AutomaticConnectionBlocking -f > /dev/null &";
            $result = exec($command, $output);
            $this->Flash->success(__('Consulta ejecutada. ' . $result));
            return  $this->redirect($this->referer());
        }
    }
}
