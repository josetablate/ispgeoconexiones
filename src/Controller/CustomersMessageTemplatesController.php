<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Routing\Router;

/**
 * CustomersMessageTemplates Controller
 *
 * @property \App\Model\Table\CustomersMessageTemplatesTable $CustomersMessageTemplates
 *
 * @method \App\Model\Entity\CustomersMessageTemplate[] paginate($object = null, array $settings = [])
 */
class CustomersMessageTemplatesController extends AppController
{

    public function isAuthorized($user = null) 
    {
        return parent::allowRol($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $customersMessageTemplates = $this->CustomersMessageTemplates->find();

        $this->set(compact('customersMessageTemplates'));
        $this->set('_serialize', ['customersMessageTemplates']);
    }

    public function add()
    {
        if ($this->request->is('post')) {

            $customerMessageTemplate = $this->CustomersMessageTemplates->find()->where(['name' => $this->request->getData('name')])->first();

            if ($customerMessageTemplate) {
                $this->Flash->warning(__('Ya existe una Plantilla con este Nombre'));
                return $this->redirect(['action' => 'add']);
            }

            $customerMessageTemplate = $this->CustomersMessageTemplates->newEntity();

            $customerMessageTemplate = $this->CustomersMessageTemplates->patchEntity($customerMessageTemplate, $this->request->getData());

            if ($this->CustomersMessageTemplates->save($customerMessageTemplate)) {
                $this->Flash->success(__('Se ha guardado correctamente la Plantilla.'));

                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(__('No se ha podido guardar la Plantilla. Por favor, intente nuevamente.'));
        }
        $customerMessageTemplate = $this->CustomersMessageTemplates->newEntity();
        $this->loadModel('TemplateResources');
        $templateResources = $this->TemplateResources->find();

        $this->set(compact('customerMessageTemplate', 'templateResources'));
        $this->set('_serialize', ['customerMessageTemplate']);
    }

    public function edit($id = null)
    {
        $customerMessageTemplate = $this->CustomersMessageTemplates->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            
            if ($this->CustomersMessageTemplates->find()->where(['name' => $this->request->getData('name'), 'id !=' => $id])->first()) {
                $this->Flash->warning(__('Ya existe una Plantilla con este Nombre'));
                return $this->redirect(['action' => 'edit', $id]);
            }
            $customerMessageTemplate = $this->CustomersMessageTemplates->patchEntity($customerMessageTemplate, $this->request->getData());
            if ($this->CustomersMessageTemplates->save($customerMessageTemplate)) {
                $this->Flash->success(__('Se ha modificado correctamente la Plantilla.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se ha modificado la Plantilla. Por favor, intente nuevamente.'));
        }
        $this->loadModel('TemplateResources');
        $templateResources = $this->TemplateResources->find();

        $this->set(compact('customerMessageTemplate', 'templateResources'));
        $this->set('_serialize', ['customerMessageTemplate']);
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $customerMessageTemplate = $this->CustomersMessageTemplates->get($_POST['id']);
        if ($this->CustomersMessageTemplates->delete($customerMessageTemplate)) {
            $this->Flash->success(__('La Plantilla de mensaje ha sido eliminada.'));
        } else {
            $this->Flash->error(__('La Plantilla no pudo ser eliminada. Por favor, inténtalo de nuevo.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
