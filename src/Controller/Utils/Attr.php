<?php
namespace App\Controller\Utils;

class Attr {

    public $value;
    public $state;

    public function __construct($_value, $_state)
    {
        $this->value = $_value;
        $this->state = $_state;
    }
}
