<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Event\Event;

/**
 * CustomersHasDiscounts Controller
 *
 * @property \App\Model\Table\CustomersHasDiscountsTable $CustomersHasDiscounts
 *
 * @method \App\Model\Entity\CustomersHasDiscount[] paginate($object = null, array $settings = [])
 */
class CustomersHasDiscountsController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('CurrentAccount', [
             'className' => '\App\Controller\Component\Admin\CurrentAccount'
        ]);
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function isAuthorized($user = null)
    {
        if ($this->request->getParam('action') == 'editAjax') {
            return true;
        }

        if ($this->request->getParam('action') == 'delete') {
            return true;
        }

        if ($this->request->getParam('action') == 'deleteMasive') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {}

    /**
     * View method
     *
     * @param string|null $id Customers Has Discount id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {}

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $paraments = $this->request->getSession()->read('paraments');

        $customersHasDiscount_formm = $this->CustomersHasDiscounts->newEntity();

        $session = $this->request->getSession();
        $flag = false;
        if (array_key_exists("data", $this->request->getData())) {
            $data = json_decode($this->request->getData('data'), true);
            if ($data['from'] == 'presale') {
                $flag = true;
            }
        }
        $presale = ($session->check('presale') && ($flag
                || $from_presale));
        if ($presale) {
            $presale = $session->read('presale');
        }

        if ($this->request->is('post')) {

            $request_data = json_decode($this->request->getData('data'), true);

            if (!$presale) {
               if ($request_data['customer_code']  == '' ) {

                    $this->Flash->error(__('Debe selecionar un cliente.'));
                    return $this->redirect(['action' => 'add']);
                }
                $customer = $this->Debts->Customers->get($request_data['customer_code']); 
            }

            if ($request_data['discount_id'] == '') {

                $this->Flash->error(__('Debe selecionar un descunto.'));
                if ($presale) {
                    return $this->redirect(['controller' => 'Presales', 'action' => 'add']);
                }
                return $this->redirect(['action' => 'add']);
            }

            $this->loadModel('Discounts');
            $discount = $this->Discounts->get($request_data['discount_id']);

            if (!$discount) {
                $this->Flash->error(__('No se encontro el descuento.'));
                if ($discount) {
                    return $this->redirect(['controller' => 'Presales', 'action' => 'add']);
                }
                return $this->redirect(['action' => 'add']);
            }

            if ($presale) {
                array_push($presale->discounts, $discount);
                $session->write('presale', $presale);
                $this->Flash->success( __('Se ha cargado con éxito el descuento'));
                return $this->redirect(['controller' => 'Presales', 'action' => 'add']);
            }

            //log de accion

            $action = 'Aplicación de Descuento';
            $detail = 'Descuento: ' . $discount->code . PHP_EOL;
            $this->registerActivity($action, $detail, $customer->code);

            $ok = $this->Accountant->addDiscount($customer);

            if (!$ok) {
                return $this->redirect(['action' => 'sellingPackage']);
            }

            $this->Flash->success(__('El Paquete se agregó a la cuenta corriente del cliente'));

            return $this->redirect(['action' => 'sellingPackage']);
        }

        $dues_interest = $paraments->accountant->dues;

        $this->set(compact('customersHasDiscount_form', 'presale'));
        $this->set('_serialize', ['customersHasDiscount_form']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Customers Has Discount id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $customersHasDiscount = $this->CustomersHasDiscounts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $customersHasDiscount = $this->CustomersHasDiscounts->patchEntity($customersHasDiscount, $this->request->getData());
            if ($this->CustomersHasDiscounts->save($customersHasDiscount)) {
                $this->Flash->success(__('The customers has discount has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The customers has discount could not be saved. Please, try again.'));
        }
        $debts = $this->CustomersHasDiscounts->Debts->find('list', ['limit' => 200]);
        $users = $this->CustomersHasDiscounts->Users->find('list', ['limit' => 200]);
        $invoices = $this->CustomersHasDiscounts->Invoices->find('list', ['limit' => 200]);
        $connections = $this->CustomersHasDiscounts->Connections->find('list', ['limit' => 200]);
        $this->set(compact('customersHasDiscount', 'debts', 'users', 'invoices', 'connections'));
        $this->set('_serialize', ['customersHasDiscount']);
    }

    public function editAjax()
    {
        if ($this->request->is('ajax')) {

            $data = $this->request->input('json_decode');

            $discount = $this->CustomersHasDiscounts->get($data->discount_id, ['contain' => ['Customers', 'Users']]);

            $discount->tipo_comp = $data->tipo_comp;

            if (!$this->CustomersHasDiscounts->save($discount)) {

                $discount = false;
            }

            $this->set('discount', $discount);
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Customers Has Discount id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete()
    {
        if ($this->request->is('ajax')) {

            $data = $this->request->input('json_decode');

            $customerHasDicount = $this->CustomersHasDiscounts->get($data->id);

            $ok = false;

            $afip_codes = $this->request->getSession()->read('afip_codes');

            $customer_code = $customerHasDicount->customer_code;
            $created = $customerHasDicount->created->format('d/m/Y');
            $description = $customerHasDicount->description;
            $tipo_comp = $afip_codes['comprobantes'][$customerHasDicount->tipo_comp];
            $total = number_format($customerHasDicount->total, 2, ',', '.');
            $periode = $customerHasDicount->period;
            $type = $entity->debt_id ? 'Descuento relacionado a una deuda' : 'Descuento individual';

            if ($this->CurrentAccount->deleteDiscount($customerHasDicount)) {
                $action = 'Eliminación de Descuento';
                $detail .= 'Fecha: ' . $created . PHP_EOL;
                $detail .= 'Descripción: ' . $description . PHP_EOL;
                $detail .= 'Destino: ' . $tipo_comp . PHP_EOL;
                $detail .= 'Total: ' .  $total . PHP_EOL;
                $detail .= 'Período: ' . $periode . PHP_EOL;
                $detail .= 'Tipo: ' . $type;
                $this->registerActivity($action, $detail, $customer_code);
                $ok = true;

                if ($this->Accountant->isEnabled()) {
                    $this->Accountant->generateContrasiento($customerHasDicount->seating_number);   
                }
            }

            $this->set("ok", $ok);
        }
    }

    public function deleteMasive()
    {
        if ($this->request->is('ajax')) {

            $data = $this->request->input('json_decode');

            $afip_codes = $this->request->getSession()->read('afip_codes');

            $action = 'Eliminación Masiva de Descuentos';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            foreach ($data as $id) {

                $customerHasDicount = $this->CustomersHasDiscounts->get($id);

                $ok = false;

                $customer_code = $customerHasDicount->customer_code;
                $created = $customerHasDicount->created->format('d/m/Y');
                $description = $customerHasDicount->description;
                $tipo_comp = $afip_codes['comprobantes'][$customerHasDicount->tipo_comp];
                $total = number_format($customerHasDicount->total, 2, ',', '.');
                $periode = $customerHasDicount->period;
                $type = $entity->debt_id ? 'Descuento relacionado a una deuda' : 'Descuento individual';

                if ($this->CurrentAccount->deleteDiscount($customerHasDicount)) {
                    $action = 'Eliminación de Descuento';
                    $detail .= 'Fecha: ' . $created . PHP_EOL;
                    $detail .= 'Descripción: ' . $description . PHP_EOL;
                    $detail .= 'Destino: ' . $tipo_comp . PHP_EOL;
                    $detail .= 'Total: ' .  $total . PHP_EOL;
                    $detail .= 'Período: ' . $periode . PHP_EOL;
                    $detail .= 'Tipo: ' . $type;
                    $this->registerActivity($action, $detail, $customer_code);
                    $ok = true;

                    if ($this->Accountant->isEnabled()) {
                        $this->Accountant->generateContrasiento($customerHasDicount->seating_number);   
                    }
                }

                $this->set("ok", $ok);
            }
        }
    }
}
