<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Areas Controller
 *
 * @property \App\Model\Table\ObservationsTable $observations
 */
class ObservationsController extends AppController
{
    public function isAuthorized($user = null) 
    {
        if ($this->request->getParam('action') == 'index') {
            return true;
        }

        if ($this->request->getParam('action') == 'add') {
            return true;
        }

        if ($this->request->getParam('action') == 'edit') {
            return true;
        }

        if ($this->request->getParam('action') == 'delete') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function index()
    {
        if ($this->request->is('ajax')) {

            $response = new \stdClass();
            $response->draw = intval($this->request->getQuery('draw'));

            $response->recordsTotal = $this->Observations->find()->count();

            $where = [];

            if (array_key_exists('customer_code', $this->request->getQuery())) {
                $where['customer_code'] = $this->request->getQuery()['customer_code'];
            }

            $response->data = $this->Observations->find('ServerSideData', [
                'params' => $this->request->getQuery(),
                'where'  => $where
            ]);

            $response->recordsFiltered = $this->Observations->find('RecordsFiltered', [
                'params' => $this->request->getQuery(),
                'where'  => $where
            ]);

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function add()
    {
        if ($this->request->is('ajax')) {

            $data = [
                'type' => 'error',
                'msg'  => 'Error, no se ha agregado la observación.'
            ];

            $customer_code = $this->request->input('json_decode')->customer_code;
            $comment = $this->request->input('json_decode')->comment;
            $user_id = $this->request->getSession()->read('Auth.User')['id'];

            $observation = $this->Observations->newEntity();
            $observation->customer_code = $customer_code;
            $observation->comment = $comment;
            $observation->user_id = $user_id;

            if ($this->Observations->save($observation)) {
                $data['type'] = 'success';
                $data['msg'] = 'Se ha agregado correctamente.';
            }

            $this->set('data', $data);
        }
    }

    public function edit($id = null)
    {
        if ($this->request->is('ajax')) {

            $data = [
                'type' => 'error',
                'msg'  => 'Error, no se ha editado la observación.'
            ];

            $id = $this->request->input('json_decode')->id;
            $comment = $this->request->input('json_decode')->comment;

            $observation = $this->Observations
                ->find()
                ->where([
                    'id' => $id
                ])->first();

            $observation->comment = $comment;

            if ($observation) {
                if ($this->Observations->save($observation)) {
                    $data['type'] = 'success';
                    $data['msg'] = 'Se ha editado correctamente.';
                }
            }

            $this->set('data', $data);
        }
    }

    public function delete()
    {
        if ($this->request->is('ajax')) {

            $data = [
                'type' => 'error',
                'msg'  => 'Error, no se ha eliminado la observación.'
            ];

            $id = $this->request->input('json_decode')->id;

            $observation = $this->Observations
                ->find()
                ->where([
                    'id' => $id
                ])->first();

            if ($observation) {
                if ($this->Observations->delete($observation)) {
                    $data['type'] = 'success';
                    $data['msg'] = 'Se ha eliminado correctamente.';
                }
            }

            $this->set('data', $data);
        }
    }
}
