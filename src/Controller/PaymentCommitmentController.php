<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use App\Controller\Component\Admin\FiscoAfipComp;
use Cake\Event\Event;
use Cake\Event\EventManager;

/**
 * PaymentCommitment Controller
 *
 *
 * @method \App\Model\Entity\PaymentCommitment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PaymentCommitmentController extends AppController
{

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('IntegrationRouter', [
            'className' => '\App\Controller\Component\Integrations\IntegrationRouter'
        ]);

        $this->loadComponent('CurrentAccount', [
            'className' => '\App\Controller\Component\Admin\CurrentAccount'
        ]);

        $this->loadComponent('FiscoAfipComp', [
            'className' => '\App\Controller\Component\Admin\FiscoAfipComp'
        ]);

        $this->loadComponent('FiscoAfipCompPdf', [
            'className' => '\App\Controller\Component\Admin\FiscoAfipCompPdf'
        ]);

        $this->loadComponent('FiscoAfip', [
            'className' => '\App\Controller\Component\Admin\FiscoAfip'
        ]);

        $this->initEventManager();
    }

    public function initEventManager()
    {
        EventManager::instance()->on(
            'PaymentCommitmentController.Error',
            function ($event, $msg, $data, $flash = false) {

                $this->loadModel('ErrorLog');
                $log = $this->ErrorLog->newEntity();
                $log->user_id = $this->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Error';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->ErrorLog->save($log);

                if ($flash) {
                    $this->Flash->error(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'PaymentCommitmentController.Warning',
            function ($event, $msg, $data, $flash = false) {

                $this->loadModel('ErrorLog');
                $log = $this->ErrorLog->newEntity();
                $log->user_id = $this->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Warning';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->ErrorLog->save($log);

                if ($flash) {
                    $this->Flash->warning(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'PaymentCommitmentController.Log',
            function ($event, $msg, $data) {
                
            }
        );

        EventManager::instance()->on(
            'PaymentCommitmentController.Notice',
            function ($event, $msg, $data) {

            }
        );
    }

    public function isAuthorized($user = null) 
    {
        if ($this->request->getParam('action') == 'index') {
            return true;
        }

        if ($this->request->getParam('action') == 'add') {
            return true;
        }

        if ($this->request->getParam('action') == 'edit') {
            return true;
        }

        if ($this->request->getParam('action') == 'delete') {
            return true;
        }

        if ($this->request->getParam('action') == 'paymentCommitmentControl') {
            return true;
        }

        if ($this->request->getParam('action') == 'getMovements') {
            return true;
        }

        if ($this->request->getParam('action') == 'verifyCompliance') {
            return true;
        }

        if ($this->request->getParam('action') == 'view') {
            return true;
        }

        if ($this->request->getParam('action') == 'ServerSidePaymentCommitmentMovements') {
            return true;
        }

        if ($this->request->getParam('action') == 'serverSideMovements') {
            return true;
        }

        if ($this->request->getParam('action') == 'ServerSidePaymentsMovements') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if ($this->request->is('ajax')) {

            $response = new \stdClass();
            $response->draw = intval($this->request->getQuery('draw'));

            $response->recordsTotal = $this->PaymentCommitment->find()->count();

            $where = [];

            if (array_key_exists('customer_code', $this->request->getQuery())) {
                $where['customer_code'] = $this->request->getQuery()['customer_code'];
            }

            if (array_key_exists('connection_id', $this->request->getQuery())) {
                $where['connection_id'] = $this->request->getQuery()['connection_id'];
            }

            $response->data = $this->PaymentCommitment->find('ServerSideData', [
                'params' => $this->request->getQuery(),
                'where'  => $where
            ]);

            $response->recordsFiltered = $this->PaymentCommitment->find('RecordsFiltered', [
                'params' => $this->request->getQuery(),
                'where'  => $where
            ]);

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    /**
     * View method
     *
     * @param string|null $id Payment Commitment id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $paymentCommitment = $this->PaymentCommitment->get($id, [
            'contain' => []
        ]);

        $this->set('paymentCommitment', $paymentCommitment);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->request->is('ajax')) {
            $customer_code = $this->request->input('json_decode')->customer_code;
            $comment = $this->request->input('json_decode')->comment;
            $duedate = $this->request->input('json_decode')->duedate;
            $import = $this->request->input('json_decode')->import;

            $duedate_observation = $this->request->input('json_decode')->duedate;
            $duedate = explode('/', $this->request->input('json_decode')->duedate);
            $duedate = new Time($duedate[2] . '-' . $duedate[1] . '-' . $duedate[0]);

            $connection_enabled = 0;
            $connection_no_enabled = 0;

            $this->loadModel('Connections');
            $this->loadModel('ConnectionsHasMessageTemplates');
            $connecitons = $this->Connections
                ->find()
                ->contain([
                    'Customers', 
                    'Controllers'
                ])
                ->where([
                    'customer_code'       => $customer_code, 
                    'Connections.enabled' => FALSE
                ]);

            if ($connecitons) {

                foreach ($connecitons as $connection) {

                    if ($this->IntegrationRouter->enableConnection($connection, TRUE)) {

                        $connection->enabled = TRUE;

                        if ($this->Connections->save($connection)) {

                            $chmt =  $this->ConnectionsHasMessageTemplates
                                ->find()
                                ->where([
                                    'connections_id' => $connection->id, 
                                    'type'           => 'Bloqueo'
                                ])
                                ->first();

                            if (!$this->ConnectionsHasMessageTemplates->delete($chmt)) {
                                $connection_no_enabled++;
                            } else {
                                $connection_enabled++;
                            }
                        } else {
                            $connection_no_enabled++;
                        }
                    } else {
                        $connection_no_enabled++;
                    }
                }
            }

            $data = [
                'type' => 'error',
                'msg'  => 'Error, no se ha generado el Compromiso de Pago.'
            ];

            $payment_commitment = $this->PaymentCommitment->newEntity();
            $payment_commitment->customer_code = $customer_code;
            $payment_commitment->comment = $comment;
            $payment_commitment->duedate = $duedate;
            $payment_commitment->import  = $import;

            if ($this->PaymentCommitment->save($payment_commitment)) {

                $data['type'] = 'success';
                $info = '';

                $afip_codes =  $this->request->getSession()->read('afip_codes');
                $user_id = $this->request->getSession()->read('Auth.User')['id'];

                $this->loadModel('Users');
                $user = $this->Users->get($user_id);
                $user_name = $user->name . ' - username: ' . $user->username;

                $data['type'] = 'success';
                $info = '';

                $this->loadModel('Customers');
                $customer = $this->Customers
                    ->find()
                    ->where([
                        'code' => $customer_code
                    ])
                    ->first();

                $customer_name = $customer->name . ' - Código: ' . $customer->code . ' - ' . $afip_codes['doc_types'][$customer->doc_type] . ': ' . $customer->ident;

                $action = 'Agregado de Compromiso de Pago';
                $detail = 'Cliente: ' . $customer_name . PHP_EOL;
                $detail .= 'Fecha vencimiento: ' . $duedate_observation . PHP_EOL;
                $detail .= 'Importe: ' . $import . PHP_EOL;
                $detail .= 'Comentario: ' . $comment . PHP_EOL;
                $detail .= 'Usuario: ' . $user_name . PHP_EOL;

                $detail .= '---------------------------' . PHP_EOL;

                $this->registerActivity($action, $detail, $customer_code, TRUE);

                $customer->payment_commitment = $duedate;
                $this->Customers->save($customer);

                // Agregado del Compromiso de Pago en Observaciones
                $this->loadModel('Observations');

                $observation = $this->Observations->newEntity();
                $observation->customer_code = $customer->code;
                $observation->comment = 'Agregado Compromiso de Pago' . ' - Fecha de Vencimiento: ' . $duedate_observation . ' - Importe: ' . $import . ' - Mensaje: ' . $comment;
                $observation->user_id = $user_id;

                $this->Observations->save($observation);

                if ($connecitons->count() > 0) {

                    $info .= 'Conexiones habilitadas: ' . $connection_enabled;

                    if ($connection_no_enabled > 0) {
                        $info .= ' - Conexiones sin habilitar: ' . $connection_no_enabled;
                    }
                }
                $data['msg'] = 'Se ha generado correctamente el Compromiso de Pago. ' . $info;
            }

            $this->set('data', $data);
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Payment Commitment id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if ($this->request->is('ajax')) {
            $customer_code = $this->request->input('json_decode')->customer_code;
            $comment = $this->request->input('json_decode')->comment;
            $duedate_observation = $this->request->input('json_decode')->duedate;
            $duedate = $this->request->input('json_decode')->duedate;
            $import = $this->request->input('json_decode')->import;
            $duedate = explode('/', $this->request->input('json_decode')->duedate);
            $duedate = new Time($duedate[2] . '-' . $duedate[1] . '-' . $duedate[0]);
            $id = $this->request->input('json_decode')->id;

            $connection_enabled = 0;
            $connection_no_enabled = 0;

            $this->loadModel('Connections');
            $this->loadModel('ConnectionsHasMessageTemplates');

            $connecitons = $this->Connections
                ->find()
                ->contain([
                    'Customers', 
                    'Controllers'
                ])
                ->where([
                    'customer_code'       => $customer_code, 
                    'Connections.enabled' => FALSE
                ]);

            if ($connecitons) {

                foreach ($connecitons as $connection) {

                    if ($this->IntegrationRouter->enableConnection($connection, TRUE)) {

                        $connection->enabled = TRUE;

                        if ($this->Connections->save($connection)) {

                            $chmt =  $this->ConnectionsHasMessageTemplates
                                ->find()
                                ->where([
                                    'connections_id' => $connection->id, 
                                    'type'           => 'Bloqueo'
                                ])
                                ->first();

                            if (!$this->ConnectionsHasMessageTemplates->delete($chmt)) {
                                $connection_no_enabled++;
                            } else {
                                $connection_enabled++;
                            }
                        } else {
                            $connection_no_enabled++;
                        }
                    } else {
                        $connection_no_enabled++;
                    }
                }
            }

            $data = [
                'type' => 'error',
                'msg'  => 'Error, no se ha editado el Compromiso de Pago.'
            ];

            $payment_commitment = $this->PaymentCommitment
                ->find()
                ->where([
                    'id' => $id
                ])
                ->first();

            $duedate_observation_before = $payment_commitment->duedate->format('d/m/Y');
            $comment_before = $payment_commitment->comment;
            $import_before = $payment_commitment->import;

            $payment_commitment->customer_code = $customer_code;
            $payment_commitment->comment = $comment;
            $payment_commitment->duedate = $duedate;
            $payment_commitment->import = $import;

            if ($this->PaymentCommitment->save($payment_commitment)) {

                $data['type'] = 'success';
                $info = '';
                
                $afip_codes =  $this->request->getSession()->read('afip_codes');
                $user_id = $this->request->getSession()->read('Auth.User')['id'];

                $this->loadModel('Users');
                $user = $this->Users->get($user_id);
                $user_name = $user->name . ' - username: ' . $user->username;

                $data['type'] = 'success';
                $info = '';

                $this->loadModel('Customers');
                $customer = $this->Customers
                    ->find()
                    ->where([
                        'code' => $customer_code
                    ])
                    ->first();

                $customer_name = $customer->name . ' - Código: ' . $customer->code . ' - ' . $afip_codes['doc_types'][$customer->doc_type] . ': ' . $customer->ident;

                $import_register = $payment_commitment->import;
                $comment_register = $payment_commitment->comment;
                $duedate_register = $payment_commitment->duedate->format('d/m/Y');

                $action = 'Edición de Compromiso de Pago';
                $detail = 'Cliente: ' . $customer_name . PHP_EOL;
                $detail .= 'Datos anteriores:' . PHP_EOL;
                $detail .= 'Fecha: ' . $duedate_observation_before . PHP_EOL;
                $detail .= 'Importe: ' . $import_before . PHP_EOL;
                $detail .= 'Comentario: ' . $comment_before . PHP_EOL;
                $detail .= '---------------------------' . PHP_EOL;
                $detail .= 'Datos editados:' . PHP_EOL;
                $detail .= 'Fecha vencimiento: ' . $duedate_register . PHP_EOL;
                $detail .= 'Importe: ' . $import_register . PHP_EOL;
                $detail .= 'Comentario: ' . $comment_register . PHP_EOL;
                $detail .= 'Usuario: ' . $user_name . PHP_EOL;

                $detail .= '---------------------------' . PHP_EOL;

                $this->registerActivity($action, $detail, $customer_code, TRUE);

                $customer->payment_commitment = $duedate;
                $this->Customers->save($customer);

                // Agregado del Compromiso de Pago en Observaciones
                $this->loadModel('Observations');

                $observation = $this->Observations->newEntity();
                $observation->customer_code = $customer->code;
                $observation->comment = 'Edición de Compromiso de Pago - Fecha de Vencimiento: ' . $duedate_observation . ' - Importe: ' . $import . ' - Mensaje: ' . $comment;
                $observation->user_id = $user_id;

                $this->Observations->save($observation);

                if ($connecitons->count() > 0) {
                    $info .= 'Conexiones habilitadas: ' . $connection_enabled;

                    if ($connection_no_enabled > 0) {
                        $info .= ' - Conexiones sin habilitar: ' . $connection_no_enabled;
                    }
                }
                $data['msg'] = 'Se ha editado correctamente el Compromiso de Pago. ' . $info;
            }

            $this->set('data', $data);
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Payment Commitment id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        if ($this->request->is('ajax')) {

            $customer_code = $this->request->input('json_decode')->customer_code;
            $id = $this->request->input('json_decode')->id;

            $payment_commitment = $this->PaymentCommitment->get($id);

            $duedate_observation = $payment_commitment->duedate->format('d/m/Y');
            $import = $payment_commitment->import;
            $comment = $payment_commitment->comment;

            if ($this->PaymentCommitment->delete($payment_commitment)) {

                $afip_codes =  $this->request->getSession()->read('afip_codes');
                $user_id = $this->request->getSession()->read('Auth.User')['id'];

                $this->loadModel('Users');
                $user = $this->Users->get($user_id);
                $user_name = $user->name . ' - username: ' . $user->username;

                $data['type'] = 'success';
                $info = '';

                $this->loadModel('Customers');
                $customer = $this->Customers
                    ->find()
                    ->where([
                        'code' => $customer_code
                    ])
                    ->first();

                $customer_name = $customer->name . ' - Código: ' . $customer->code . ' - ' . $afip_codes['doc_types'][$customer->doc_type] . ': ' . $customer->ident;

                $action = 'Eliminación de Compromiso de Pago';
                $detail = 'Cliente: ' . $customer_name . PHP_EOL;
                $detail .= 'Fecha vencimiento: ' . $duedate_observation . PHP_EOL;
                $detail .= 'Importe: ' . $import . PHP_EOL;
                $detail .= 'Comentario: ' . $comment . PHP_EOL;
                $detail .= 'Usuario: ' . $user_name . PHP_EOL;

                $detail .= '---------------------------' . PHP_EOL;

                $this->registerActivity($action, $detail, $customer_code, TRUE);

                $customer->payment_commitment = NULL;
                $this->Customers->save($customer);

                // Agregado del Compromiso de Pago en Observaciones
                $this->loadModel('Observations');

                $observation = $this->Observations->newEntity();
                $observation->customer_code = $customer->code;
                $observation->comment = 'Eliminación de Compromiso de Pago' . ' - Fecha de Vencimiento: ' . $duedate_observation . ' - Importe: ' . $import . ' - Mensaje: ' . $comment;
                $observation->user_id = $user_id;

                $this->Observations->save($observation);

                $data['msg'] = 'Se ha eliminado correctamente el Compromiso de Pago.';
            }

            $this->set('data', $data);
        }
    }

    public function paymentCommitmentControl()
    {
        $action = 'Ejecución Compromiso de Pago (Cron)';
        $detail = '';
        $this->registerActivity($action, $detail, NULL, TRUE);

        $paraments = $this->request->getSession()->read('paraments');
        $afip_codes = $this->request->getSession()->read('afip_codes');

        $data = [
            'code'    => 200,
            'message' => 'datos general',
            'counter'   => 0
        ];

        $now = Time::now();
        $month = str_pad($now->month, 2, "0", STR_PAD_LEFT);
        $day = str_pad($now->day, 2, "0", STR_PAD_LEFT);
        $now = $now->year . '-' . $month . '-' . $day;

        $this->loadModel('PaymentCommitment');
        $this->loadModel('Connections');
        $this->loadModel('ConnectionsHasMessageTemplates');
        $this->loadModel('MessageTemplates');
        $this->loadModel('Debts');
        $this->loadModel('DebitNotes');

        $payment_commitment = $this->PaymentCommitment
            ->find()
            ->contain([
                'Customers.Connections.Services',
            ])->where([
                'PaymentCommitment.duedate LIKE' => $now . '%',
                'PaymentCommitment.ok IS'        => NULL
            ]);

        $message = "";

        $messageTemplate = $this->MessageTemplates->find()
            ->where([
                'deleted' => 0,
                'type'    => 'Bloqueo',
                'enabled' => 1
            ])
            ->order([
                'priority' => 'DESC'
            ])->first();

        if ($paraments->automatic_connection_blocking->force) { //configuracion general forzada

            if ($paraments->automatic_connection_blocking->type == 'connection') {

                foreach ($payment_commitment as $pc) {

                    $customer = $pc->customer;
                    $customer->payment_commitment = NULL;
                    $this->PaymentCommitment->Customers->save($customer);

                    $pc->ok = Time::now();
                    $this->PaymentCommitment->save($pc);

                    foreach ($pc->customer->connections as $connection) {

                        // Salta configuracion conexion no genere deuda
                        if (!$connection->force_debt) {
                            continue;
                        }

                        if ($connection->debt_month >= $paraments->automatic_connection_blocking->due_debt
                            && $connection->invoices_no_paid >= $paraments->automatic_connection_blocking->invoice_no_paid
                            && $connection->deleted == FALSE
                            && $connection->enabled == TRUE) {

                            $connection = $this->Connections->get($connection->id, [
                                'contain' => [
                                    'Customers.Cities', 
                                    'Controllers',
                                    'Services'
                                ]
                            ]);

                            if ($this->IntegrationRouter->disableConnection($connection)) {

                                $connection->enabled = FALSE;

                                if ($this->Connections->save($connection)) {

                                    $customer_code = $connection->customer_code;
                                    $ip = $connection->ip;
                                    $mac = $connection->mac;

                                    $detail = "";
                                    $detail .= 'Conexión: ' . $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL;
                                    $detail .= 'IP: ' . $ip . PHP_EOL;
                                    $detail .= 'MAC: ' . $mac . PHP_EOL;
                                    $action = 'Bloqueo de Conexión - vencimiento de Compromiso de Pago';
                                    $this->registerActivity($action, $detail, $customer_code);

                                    $chmt =  $this->ConnectionsHasMessageTemplates->newEntity();
                                    $chmt->created = Time::now();
                                    $chmt->connections_id = $connection->id;
                                    $chmt->message_template_id = $messageTemplate->id;
                                    $chmt->ip = $connection->ip;
                                    $chmt->type = $messageTemplate->type;

                                    if (!$this->ConnectionsHasMessageTemplates->save($chmt)) {
                                        $message .= 'connnection_id: ' . $connection->id . '. No pudo crear ConnectionsHasMessageTemplates';
                                        break;
                                    }

                                    // Se controla, si ya se aplico el recargo.
                                    $now = Time::now();
                                    $date_now_blocking = $now->year . '-' . $now->month . '-' . $paraments->automatic_connection_blocking->day . ' ' . $paraments->automatic_connection_blocking->hours_execution . ':00';

                                    $date_now_blocking = new Time($date_now_blocking);
                                    $date_payment_commitment = new Time($pc->created);

                                    // Condición si se debe generar el recargo o no.
                                    if ($date_now_blocking > $date_payment_commitment) {

                                        //aplico recargo
                                        if ($paraments->automatic_connection_blocking->surcharge_enabled 
                                            && $paraments->automatic_connection_blocking->surcharge_value > 0) {

                                            switch ($paraments->automatic_connection_blocking->surcharge_type) {

                                                case 'next_month':

                                                    $duedate = Time::now()->day($paraments->accountant->daydue)->modify('+1 month');

                                                    $debt_data = [
                                                        'concept'              => $paraments->automatic_connection_blocking->surcharge_concept,
                                                        'seating'              => NULL,
                                                        'customer'             => $connection->customer,
                                                        'value'                => $paraments->automatic_connection_blocking->surcharge_value,
                                                        'alicuot'              => $connection->service->aliquot,
                                                        'duedate'              => $duedate,
                                                        'connection_id'        => $connection->customer->billing_for_service ? $connection->id : NULL,
                                                        'is_presupuesto_force' => $paraments->automatic_connection_blocking->surcharge_presupuesto,
                                                        'period'               => Time::now()->format('m/Y'),
                                                    ];

                                                    $this->CurrentAccount->addDebt($debt_data);

                                                    $customer_code = $connection->customer_code;

                                                    $duedate = $duedate->format('d/m/Y');

                                                    $detail = "";
                                                    $detail .= "Información: se genero una deuda para el próximo mes según configuración Bloqueo Automático.";
                                                    $detail .= 'Conexión: ' . $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL; 
                                                    $detail .= "Vencimiento: " . $duedate . PHP_EOL;
                                                    $detail .= "Concepto: " . $paraments->automatic_connection_blocking->surcharge_concept . PHP_EOL;
                                                    $detail .= "Total: " . $paraments->automatic_connection_blocking->surcharge_value . PHP_EOL;
                                                    $action = 'Recargo por reconexión - vencimiento de Compromiso de Pago';
                                                    $this->registerActivity($action, $detail, $customer_code);
                                                    break;

                                                case 'this_month':

                                                    $tipo_comp = 'NDX';

                                                    if (!$paraments->automatic_connection_blocking->surcharge_presupuesto) {

                                                        if ($connection->customer->is_presupuesto) { 

                                                            $tipo_comp ='NDX';

                                                        } else {

                                                            foreach ($paraments->invoicing->business as $business) {

                                                                if ($business->id == $connection->customer->business_billing) {
                                                                    $connection->customer->business = $business;
                                                                }
                                                            }

                                                            $tipo_comp = $afip_codes['combinationsDebitNotes'][$connection->customer->business->responsible][$connection->customer->responsible];
                                                        }
                                                    }

                                                    //parametros $total, $tax, $debt->quantity
                                                    $row = $this->CurrentAccount->calulateIvaAndNeto($paraments->automatic_connection_blocking->surcharge_value, $afip_codes['alicuotas_percectage'][$connection->service->aliquot], 1);

                                                    $concepts = [];

                                                    $concept = new \stdClass;
                                                    $concept->type        = 'S';
                                                    $concept->code        = 99;
                                                    $concept->description = $paraments->automatic_connection_blocking->surcharge_concept;
                                                    $concept->quantity    = 1;
                                                    $concept->unit        = 7;
                                                    $concept->price       = $row->price;
                                                    $concept->discount    = 0;
                                                    $concept->sum_price   = $row->sum_price;;
                                                    $concept->tax         = $connection->service->aliquot;
                                                    $concept->sum_tax     = $row->sum_tax;
                                                    $concept->total       = $row->total;
                                                    $concepts[0] = $concept;

                                                    $debitNote  = [
                                                        'customer'      => $connection->customer,
                                                        'concept_type'  => 2,
                                                        'date'          => Time::now(),
                                                        'date_start'    => Time::now(),
                                                        'date_end'      => Time::now(),
                                                        'duedate'       => Time::now(),
                                                        'concepts'      => $concepts,
                                                        'tipo_comp'     => $tipo_comp,
                                                        'comments'      => $paraments->automatic_connection_blocking->surcharge_concept,
                                                        'manual'        => TRUE,
                                                        'business_id'   => $connection->customer->business_billing,
                                                        'connection_id' => $connection->customer->billing_for_service ? $connection->id : NULL,
                                                    ];

                                                    $debitNote = $this->FiscoAfipComp->debitNote($debitNote);

                                                    if ($debitNote) {

                                                        //informo afip si no es de tipo X

                                                        if ($this->FiscoAfip->informar($debitNote, FiscoAfipComp::TYPE_DEBIT_NOTE)) {

                                                            if ($this->DebitNotes->save($debitNote)) {

                                                                $customer_code = $connection->customer_code;

                                                                $duedate = Time::now()->format('d/m/Y');

                                                                $detail = "";
                                                                $detail .= 'Conexión: ' . $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL; 
                                                                $detail .= "Tipo de Comp: " . $tipo_comp . PHP_EOL;
                                                                $detail .= "Vencimiento: " . $duedate . PHP_EOL;
                                                                $detail .= "Concepto: " . $paraments->automatic_connection_blocking->surcharge_concept . PHP_EOL;
                                                                $detail .= "Total: " . $concept->total . PHP_EOL;
                                                                $action = 'Recargo por reconexión - vencimiento de Compromiso de Pago';
                                                                $this->registerActivity($action, $detail, $customer_code);
                                                           }

                                                       } else {
                                                           $this->FiscoAfipComp->rollbackInvoice($debitNote, TRUE);
                                                       }
                                                    } else {
                                                        $this->FiscoAfipComp->rollbackInvoice($debitNote, TRUE);
                                                    }
                                                    break;
                                            }
                                        } //end surcharge_enabled = true
                                    }

                                    $data['counter']++;

                                } else {
                                    $message .= 'connnection_id: ' . $connection->id . '. No pudo guardar la conexión';
                                    break;
                                }
                            } else {
                                $message .= 'connnection_id: ' . $connection->id . '. No pudo deshabilitar la conexión';
                            }
                        }
                    }
                }

            } else { //tipo de control por cliente

                foreach ($payment_commitment as $pc) {

                    $customer = $pc->customer;
                    $customer->payment_commitment = NULL;
                    $this->PaymentCommitment->Customers->save($customer);

                    $pc->ok = Time::now();
                    $this->PaymentCommitment->save($pc);

                    if ($customer->debt_month >= $paraments->automatic_connection_blocking->due_debt
                        && $customer->invoices_no_paid >= $paraments->automatic_connection_blocking->invoice_no_paid
                        && $customer->deleted == FALSE) {

                        foreach ($customer->connections as $conn) {

                            if (!$conn->enabled) {
                                continue;
                            }

                            // Salta configuracion conexion no genere deuda
                            if (!$conn->force_debt) {
                                continue;
                            }

                            $connection = $this->Connections->get($conn->id, [
                                'contain' => [
                                    'Customers.Cities', 
                                    'Controllers',
                                    'Services'
                                ]
                            ]);

                            if ($this->IntegrationRouter->disableConnection($connection)) {

                                $connection->enabled = FALSE;

                                if ($this->Connections->save($connection)) {

                                    $customer_code = $connection->customer_code;
                                    $ip = $connection->ip;
                                    $mac = $connection->mac;

                                    $detail = "";
                                    $detail .= 'Conexión: '. $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL; 
                                    $detail .= 'IP: ' . $ip . PHP_EOL;
                                    $detail .= 'MAC: ' . $mac . PHP_EOL;
                                    $action = 'Bloqueo de Conexión - vencimiento de Compromiso de Pago';
                                    $this->registerActivity($action, $detail, $customer_code);


                                    $chmt =  $this->ConnectionsHasMessageTemplates->newEntity();
                                    $chmt->created = Time::now();
                                    $chmt->connections_id = $connection->id;
                                    $chmt->message_template_id = $messageTemplate->id;
                                    $chmt->ip = $connection->ip;
                                    $chmt->type = $messageTemplate->type;

                                    if (!$this->ConnectionsHasMessageTemplates->save($chmt)) {
                                        $message .= 'connnection_id: ' . $connection->id . '. No pudo crear ConnectionsHasMessageTemplates';
                                        break;
                                    }
                                    
                                    // Se controla, si ya se aplico el recargo.
                                    $now = Time::now();
                                    $date_now_blocking = $now->year . '-' . $now->month . '-' . $paraments->automatic_connection_blocking->day . ' ' . $paraments->automatic_connection_blocking->hours_execution . ':00';

                                    $date_now_blocking = new Time($date_now_blocking);
                                    $date_payment_commitment = new Time($pc->created);

                                    // Condición si se debe generar el recargo o no.
                                    if ($date_now_blocking > $date_payment_commitment) {

                                        //aplico recargo
                                        if ($paraments->automatic_connection_blocking->surcharge_enabled 
                                            && $paraments->automatic_connection_blocking->surcharge_value > 0) {

                                            switch ($paraments->automatic_connection_blocking->surcharge_type) {

                                                case 'next_month':

                                                    $duedate = Time::now()->day($paraments->accountant->daydue)->modify('+1 month');

                                                    $debt_data = [
                                                        'concept'              => $paraments->automatic_connection_blocking->surcharge_concept,
                                                        'seating'              => NULL,
                                                        'customer'             => $connection->customer,
                                                        'value'                => $paraments->automatic_connection_blocking->surcharge_value,
                                                        'alicuot'              => $connection->service->aliquot,
                                                        'duedate'              => $duedate,
                                                        'connection_id'        => $connection->customer->billing_for_service ? $connection->id : NULL,
                                                        'is_presupuesto_force' => $paraments->automatic_connection_blocking->surcharge_presupuesto,
                                                        'period'               => Time::now()->format('m/Y'),
                                                    ];

                                                    $this->CurrentAccount->addDebt($debt_data);
                                                    $customer_code = $connection->customer_code;

                                                    $duedate = $duedate->format('d/m/Y');

                                                    $detail = "";
                                                    $detail .= "Información: se genero una deuda para el próximo mes según configuración Bloqueo Automático.";
                                                    $detail .= 'Conexión: ' . $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL; 
                                                    $detail .= "Vencimiento: " . $duedate . PHP_EOL;
                                                    $detail .= "Concepto: " . $paraments->automatic_connection_blocking->surcharge_concept . PHP_EOL;
                                                    $detail .= "Total: " . $paraments->automatic_connection_blocking->surcharge_value . PHP_EOL;
                                                    $action = 'Recargo por reconexión - vencimiento de Compromiso de Pago';
                                                    $this->registerActivity($action, $detail, $customer_code);
                                                    break;

                                                case 'this_month':

                                                    $tipo_comp = 'NDX';

                                                    if (!$paraments->automatic_connection_blocking->surcharge_presupuesto) {

                                                        if ($customer->is_presupuesto) { 

                                                            $tipo_comp ='NDX';

                                                        } else {

                                                            foreach ($paraments->invoicing->business as $business) {

                                                                if ($business->id == $customer->business_billing) {
                                                                    $customer->business = $business;
                                                                }
                                                            }

                                                            $tipo_comp = $afip_codes['combinationsDebitNotes'][$customer->business->responsible][$customer->responsible];
                                                        }
                                                    }

                                                    //parametros $total, $tax, $debt->quantity
                                                    $row = $this->CurrentAccount->calulateIvaAndNeto($paraments->automatic_connection_blocking->surcharge_value, $afip_codes['alicuotas_percectage'][$connection->service->aliquot], 1);

                                                    $concepts = [];

                                                    $concept = new \stdClass;
                                                    $concept->type        = 'S';
                                                    $concept->code        = 99;
                                                    $concept->description = $paraments->automatic_connection_blocking->surcharge_concept;
                                                    $concept->quantity    = 1;
                                                    $concept->unit        = 7;
                                                    $concept->price       = $row->price;
                                                    $concept->discount    = 0;
                                                    $concept->sum_price   = $row->sum_price;;
                                                    $concept->tax         = $connection->service->aliquot;
                                                    $concept->sum_tax     = $row->sum_tax;
                                                    $concept->total       = $row->total;
                                                    $concepts[0] = $concept;

                                                    $debitNote  = [
                                                        'customer'      => $connection->customer,
                                                        'concept_type'  => 2,
                                                        'date'          => Time::now(),
                                                        'date_start'    => Time::now(),
                                                        'date_end'      => Time::now(),
                                                        'duedate'       => Time::now(),
                                                        'concepts'      => $concepts,
                                                        'tipo_comp'     => $tipo_comp,
                                                        'comments'      => $paraments->automatic_connection_blocking->surcharge_concept,
                                                        'manual'        => TRUE,
                                                        'business_id'   => $customer->business_billing,
                                                        'connection_id' => $connection->customer->billing_for_service ? $connection->id : NULL,
                                                    ];

                                                    $debitNote = $this->FiscoAfipComp->debitNote($debitNote);

                                                    if ($debitNote) {

                                                        //informo afip si no es de tipo X

                                                        if ($this->FiscoAfip->informar($debitNote, FiscoAfipComp::TYPE_DEBIT_NOTE)) {

                                                            if ($this->DebitNotes->save($debitNote)) {
                                                                $customer_code = $connection->customer_code;

                                                                $duedate = Time::now()->format('d/m/Y');

                                                                $detail = "";
                                                                $detail .= 'Conexión: ' . $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL; 
                                                                $detail .= "Tipo de Comp: " . $tipo_comp . PHP_EOL;
                                                                $detail .= "Vencimiento: " . $duedate . PHP_EOL;
                                                                $detail .= "Concepto: " . $paraments->automatic_connection_blocking->surcharge_concept . PHP_EOL;
                                                                $detail .= "Total: " . $concept->total . PHP_EOL;
                                                                $action = 'Recargo por reconexión - vencimiento de Compromiso de Pago';
                                                                $this->registerActivity($action, $detail, $customer_code);
                                                           }
                                                       } else {
                                                           $this->FiscoAfipComp->rollbackInvoice($debitNote, TRUE);
                                                       }
                                                    } else {
                                                        $this->FiscoAfipComp->rollbackInvoice($debitNote, TRUE);
                                                    }
                                                    break;
                                            }
                                        } //end surcharge_enabled = true
                                    }

                                    $data['counter']++;

                                } else {
                                    $message .= 'connnection_id: ' . $connection->id . '. No pudo guardar la conexión';
                                    break;
                                }
                            } else {
                                $message .= 'connnection_id: ' . $connection->id . '. No pudo deshabilitar la conexión';
                            }
                        }
                    }
                }
            }

        } else { //si la configuracion general NO esta forzada

            foreach ($payment_commitment as $pc) {//recorro los clientes

                $customer = $pc->customer;
                $customer->payment_commitment = NULL;
                $this->PaymentCommitment->Customers->save($customer);

                $pc->ok = Time::now();
                $this->PaymentCommitment->save($pc);

                if ($customer->deleted == TRUE) {
                    continue;
                }

                if ($customer->acb_type == 'connection') { //tipo de control por conexion

                    foreach ($customer->connections as $conn) {

                        if (!$conn->enabled) {
                            continue;
                        }

                        // Salta configuracion conexion no genere deuda
                        if (!$conn->force_debt) {
                            continue;
                        }

                        if ($conn->debt_month >= $customer->acb_due_debt 
                            && $conn->invoices_no_paid >= $customer->acb_invoice_no_paid) {

                            $connection = $this->Connections->get($conn->id, [
                                'contain' => [
                                    'Customers.Cities', 
                                    'Controllers',
                                    'Services'
                                ]
                            ]);

                            if ($this->IntegrationRouter->disableConnection($connection)) {

                                $connection->enabled = FALSE;

                                if ($this->Connections->save($connection)) {

                                    $customer_code = $connection->customer_code;
                                    $ip = $connection->ip;
                                    $mac = $connection->mac;

                                    $detail = "";
                                    $detail .= 'Conexión: '. $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL; 
                                    $detail .= 'IP: ' . $ip . PHP_EOL;
                                    $detail .= 'MAC: ' . $mac . PHP_EOL;
                                    $action = 'Bloqueo de Conexión - vencimiento de Compromiso de Pago';
                                    $this->registerActivity($action, $detail, $customer_code);

                                    $chmt =  $this->ConnectionsHasMessageTemplates->newEntity();
                                    $chmt->created = Time::now();
                                    $chmt->connections_id = $connection->id;
                                    $chmt->message_template_id = $messageTemplate->id;
                                    $chmt->ip = $connection->ip;
                                    $chmt->type = $messageTemplate->type;

                                    if (!$this->ConnectionsHasMessageTemplates->save($chmt)) {
                                        $message .= 'connnection_id: ' . $connection->id . '. No pudo crear ConnectionsHasMessageTemplates';
                                        break;
                                    }

                                    // Se controla, si ya se aplico el recargo.
                                    $now = Time::now();
                                    $date_now_blocking = $now->year . '-' . $now->month . '-' . $paraments->automatic_connection_blocking->day . ' ' . $paraments->automatic_connection_blocking->hours_execution . ':00';

                                    $date_now_blocking = new Time($date_now_blocking);
                                    $date_payment_commitment = new Time($pc->created);

                                    // Condición si se debe generar el recargo o no.
                                    if ($date_now_blocking > $date_payment_commitment) {

                                        //aplico recargo
                                        if ($customer->acb_surcharge_enabled 
                                            && $paraments->automatic_connection_blocking->surcharge_value > 0) {

                                            switch ($paraments->automatic_connection_blocking->surcharge_type) {

                                                case 'next_month':

                                                    $duedate = Time::now()->day($paraments->accountant->daydue)->modify('+1 month');

                                                    $debt_data = [
                                                        'concept'              => $paraments->automatic_connection_blocking->surcharge_concept,
                                                        'seating'              => NULL,
                                                        'customer'             => $connection->customer,
                                                        'value'                => $paraments->automatic_connection_blocking->surcharge_value,
                                                        'alicuot'              => $connection->service->aliquot,
                                                        'duedate'              => $duedate,
                                                        'connection_id'        => $connection->customer->billing_for_service ? $connection->id : NULL,
                                                        'is_presupuesto_force' => $paraments->automatic_connection_blocking->surcharge_presupuesto,
                                                        'period'               => Time::now()->format('m/Y'),
                                                    ];

                                                    $this->CurrentAccount->addDebt($debt_data);
                                                    $customer_code = $connection->customer_code;

                                                    $duedate = $duedate->format('d/m/Y');

                                                    $detail = "";
                                                    $detail .= "Información: se genero una deuda para el próximo mes según configuración Bloqueo Automático.";
                                                    $detail .= 'Conexión: ' . $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL; 
                                                    $detail .= "Vencimiento: " . $duedate . PHP_EOL;
                                                    $detail .= "Concepto: " . $paraments->automatic_connection_blocking->surcharge_concept . PHP_EOL;
                                                    $detail .= "Total: " . $paraments->automatic_connection_blocking->surcharge_value . PHP_EOL;
                                                    $action = 'Recargo por reconexión - vencimiento de Compromiso de Pago';
                                                    $this->registerActivity($action, $detail, $customer_code);
                                                    break;

                                                case 'this_month':

                                                    $tipo_comp = 'NDX';

                                                    if (!$paraments->automatic_connection_blocking->surcharge_presupuesto) {

                                                        if ($customer->is_presupuesto) { 

                                                            $tipo_comp ='NDX';

                                                        } else {

                                                            foreach ($paraments->invoicing->business as $business) {

                                                                if ($business->id == $customer->business_billing) {
                                                                    $customer->business = $business;
                                                                }
                                                            }

                                                            $tipo_comp = $afip_codes['combinationsDebitNotes'][$customer->business->responsible][$customer->responsible];
                                                        }
                                                    }

                                                    //parametros $total, $tax, $debt->quantity
                                                    $row = $this->CurrentAccount->calulateIvaAndNeto($paraments->automatic_connection_blocking->surcharge_value, $afip_codes['alicuotas_percectage'][$connection->service->aliquot], 1);

                                                    $concepts = [];

                                                    $concept = new \stdClass;
                                                    $concept->type        = 'S';
                                                    $concept->code        = 99;
                                                    $concept->description = $paraments->automatic_connection_blocking->surcharge_concept;
                                                    $concept->quantity    = 1;
                                                    $concept->unit        = 7;
                                                    $concept->price       = $row->price;
                                                    $concept->discount    = 0;
                                                    $concept->sum_price   = $row->sum_price;;
                                                    $concept->tax         = $connection->service->aliquot;
                                                    $concept->sum_tax     = $row->sum_tax;
                                                    $concept->total       = $row->total;
                                                    $concepts[0] = $concept;

                                                    $debitNote  = [
                                                        'customer'      => $connection->customer,
                                                        'concept_type'  => 2,
                                                        'date'          => Time::now(),
                                                        'date_start'    => Time::now(),
                                                        'date_end'      => Time::now(),
                                                        'duedate'       => Time::now(),
                                                        'concepts'      => $concepts,
                                                        'tipo_comp'     => $tipo_comp,
                                                        'comments'      => $paraments->automatic_connection_blocking->surcharge_concept,
                                                        'manual'        => true,
                                                        'business_id'   => $customer->business_billing,
                                                        'connection_id' => $connection->customer->billing_for_service ? $connection->id : NULL,
                                                    ];

                                                    $debitNote = $this->FiscoAfipComp->debitNote($debitNote);
                                        
                                                    if ($debitNote) {

                                                        //informo afip si no es de tipo X

                                                        if ($this->FiscoAfip->informar($debitNote, FiscoAfipComp::TYPE_DEBIT_NOTE)) {

                                                            if ($this->DebitNotes->save($debitNote)) {
                                                                $customer_code = $connection->customer_code;

                                                                $duedate = Time::now()->format('d/m/Y');

                                                                $detail = "";
                                                                $detail .= 'Conexión: ' . $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL; 
                                                                $detail .= "Tipo de Comp: " . $tipo_comp . PHP_EOL;
                                                                $detail .= "Vencimiento: " . $duedate . PHP_EOL;
                                                                $detail .= "Concepto: " . $paraments->automatic_connection_blocking->surcharge_concept . PHP_EOL;
                                                                $detail .= "Total: " . $concept->total . PHP_EOL;
                                                                $action = 'Recargo por reconexión - vencimiento de Compromiso de Pago';
                                                                $this->registerActivity($action, $detail, $customer_code);
                                                           }
                                                       } else {
                                                           $this->FiscoAfipComp->rollbackInvoice($debitNote, TRUE);
                                                       }
                                                    } else{
                                                        $this->FiscoAfipComp->rollbackInvoice($debitNote, TRUE);
                                                    }
                                                    break;
                                            }
                                        } //end surcharge_enabled = true
                                    }

                                    $data['counter']++;

                                } else {
                                    $message .= 'connnection_id: ' . $connection->id . '. No pudo guardar la conexión';
                                    break;
                                }
                            } else {
                                $message .= 'connnection_id: ' . $connection->id . '. No pudo deshabilitar la conexión';
                            }
                        }
                    }

                } else { //tipo de control por cliente

                    if ($customer->debt_month >= $customer->acb_due_debt 
                        && $customer->invoices_no_paid >= $customer->acb_invoice_no_paid) {

                        foreach ($customer->connections as $conn) {

                            if (!$conn->enabled) {
                                continue;
                            }

                            $connection = $this->Connections->get($conn->id, [
                                'contain' => ['Customers.Cities', 'Controllers', 'Services']
                            ]);

                            if ($this->IntegrationRouter->disableConnection($connection)) {

                                $connection->enabled = false;

                                if ($this->Connections->save($connection)) {

                                    $customer_code = $connection->customer_code;
                                    $ip = $connection->ip;
                                    $mac = $connection->mac;

                                    $detail = "";
                                    $detail .= 'Conexión: '. $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL; 
                                    $detail .= 'IP: ' . $ip . PHP_EOL;
                                    $detail .= 'MAC: ' . $mac . PHP_EOL;
                                    $action = 'Bloqueo de Conexión - vencimiento de Compromiso de Pago';
                                    $this->registerActivity($action, $detail, $customer_code);

                                    $chmt =  $this->ConnectionsHasMessageTemplates->newEntity();
                                    $chmt->created = Time::now();
                                    $chmt->connections_id = $connection->id;
                                    $chmt->message_template_id = $messageTemplate->id;
                                    $chmt->ip = $connection->ip;
                                    $chmt->type = $messageTemplate->type;

                                    if (!$this->ConnectionsHasMessageTemplates->save($chmt)) {
                                        $message .= 'connnection_id: ' . $connection->id . '. No pudo crear ConnectionsHasMessageTemplates';
                                        break;
                                    }

                                    // Se controla, si ya se aplico el recargo.
                                    $now = Time::now();
                                    $date_now_blocking = $now->year . '-' . $now->month . '-' . $paraments->automatic_connection_blocking->day . ' ' . $paraments->automatic_connection_blocking->hours_execution . ':00';

                                    $date_now_blocking = new Time($date_now_blocking);
                                    $date_payment_commitment = new Time($pc->created);

                                    // Condición si se debe generar el recargo o no.
                                    if ($date_now_blocking > $date_payment_commitment) {

                                        //aplico recargo
                                        if ($customer->acb_surcharge_enabled 
                                            && $paraments->automatic_connection_blocking->surcharge_value > 0) {

                                            switch ($paraments->automatic_connection_blocking->surcharge_type) {

                                                case 'next_month':

                                                    $duedate = Time::now()->day($paraments->accountant->daydue)->modify('+1 month');

                                                    $debt_data = [
                                                        'concept'              => $paraments->automatic_connection_blocking->surcharge_concept,
                                                        'seating'              => NULL,
                                                        'customer'             => $connection->customer,
                                                        'value'                => $paraments->automatic_connection_blocking->surcharge_value,
                                                        'alicuot'              => $connection->service->aliquot,
                                                        'duedate'              => $duedate,
                                                        'connection_id'        => $connection->customer->billing_for_service ? $connection->id : NULL,
                                                        'is_presupuesto_force' => $paraments->automatic_connection_blocking->surcharge_presupuesto,
                                                        'period'               => Time::now()->format('m/Y'),
                                                    ];

                                                    $debt = $this->CurrentAccount->addDebt($debt_data);
                                                    $customer_code = $connection->customer_code;

                                                    $duedate = $duedate->format('d/m/Y');

                                                    $detail = "";
                                                    $detail .= "Información: se genero una deuda para el próximo mes según configuración Bloqueo Automático.";
                                                    $detail .= 'Conexión: ' . $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL; 
                                                    $detail .= "Vencimiento: " . $duedate . PHP_EOL;
                                                    $detail .= "Concepto: " . $paraments->automatic_connection_blocking->surcharge_concept . PHP_EOL;
                                                    $detail .= "Total: " . $paraments->automatic_connection_blocking->surcharge_value . PHP_EOL;
                                                    $action = 'Recargo por reconexión - vencimiento de Compromiso de Pago';
                                                    $this->registerActivity($action, $detail, $customer_code);
                                                    break;

                                                case 'this_month':

                                                    $tipo_comp = 'NDX';

                                                    if (!$paraments->automatic_connection_blocking->surcharge_presupuesto) {

                                                        if ($customer->is_presupuesto) { 

                                                            $tipo_comp ='NDX';

                                                        } else {

                                                            foreach ($paraments->invoicing->business as $business) {

                                                                if ($business->id == $customer->business_billing) {
                                                                    $customer->business = $business;
                                                                }
                                                            }

                                                            $tipo_comp = $afip_codes['combinationsDebitNotes'][$customer->business->responsible][$customer->responsible];
                                                        }
                                                    }

                                                    //parametros $total, $tax, $debt->quantity
                                                    $row = $this->CurrentAccount->calulateIvaAndNeto($paraments->automatic_connection_blocking->surcharge_value, $afip_codes['alicuotas_percectage'][$connection->service->aliquot], 1);

                                                    $concepts = [];

                                                    $concept = new \stdClass;
                                                    $concept->type        = 'S';
                                                    $concept->code        = 99;
                                                    $concept->description = $paraments->automatic_connection_blocking->surcharge_concept;
                                                    $concept->quantity    = 1;
                                                    $concept->unit        = 7;
                                                    $concept->price       = $row->price;
                                                    $concept->discount    = 0;
                                                    $concept->sum_price   = $row->sum_price;;
                                                    $concept->tax         = $connection->service->aliquot;
                                                    $concept->sum_tax     = $row->sum_tax;
                                                    $concept->total       = $row->total;
                                                    $concepts[0] = $concept;

                                                    $debitNote  = [
                                                        'customer'      => $connection->customer,
                                                        'concept_type'  => 2,
                                                        'date'          => Time::now(),
                                                        'date_start'    => Time::now(),
                                                        'date_end'      => Time::now(),
                                                        'duedate'       => Time::now(),
                                                        'concepts'      => $concepts,
                                                        'tipo_comp'     => $tipo_comp,
                                                        'comments'      => $paraments->automatic_connection_blocking->surcharge_concept,
                                                        'manual'        => TRUE,
                                                        'business_id'   => $customer->business_billing,
                                                        'connection_id' => $connection->customer->billing_for_service ? $connection->id : NULL,
                                                    ];

                                                    $debitNote = $this->FiscoAfipComp->debitNote($debitNote);
                                        
                                                    if ($debitNote) {

                                                        //informo afip si no es de tipo X

                                                        if ($this->FiscoAfip->informar($debitNote, FiscoAfipComp::TYPE_DEBIT_NOTE)) {

                                                            if ($this->DebitNotes->save($debitNote)) {
                                                                $customer_code = $connection->customer_code;

                                                                $duedate = Time::now()->format('d/m/Y');

                                                                $detail = "";
                                                                $detail .= 'Conexión: ' . $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL; 
                                                                $detail .= "Tipo de Comp: " . $tipo_comp . PHP_EOL;
                                                                $detail .= "Vencimiento: " . $duedate . PHP_EOL;
                                                                $detail .= "Concepto: " . $paraments->automatic_connection_blocking->surcharge_concept . PHP_EOL;
                                                                $detail .= "Total: " . $concept->total . PHP_EOL;
                                                                $action = 'Recargo por reconexión - vencimiento de Compromiso de Pago';
                                                                $this->registerActivity($action, $detail, $customer_code);
                                                           }
                                                       } else {
                                                           $this->FiscoAfipComp->rollbackInvoice($debitNote, TRUE);
                                                       }
                                                    } else {
                                                        $this->FiscoAfipComp->rollbackInvoice($debitNote, TRUE);
                                                    }
                                                    break;
                                            }
                                        } //end surcharge_enabled = true
                                    }

                                    $data['counter']++;

                                } else {
                                    $message .= 'connnection_id: ' . $connection->id . '. No pudo guardar la conexión';
                                    break;
                                }
                            } else {
                                $message .= 'connnection_id: ' . $connection->id . '. No pudo deshabilitar la conexión';
                            }
                        }
                    }
                }
            }//end foreach customers
        }
        return json_encode($data);
    }

    public function getMovements()
    {
        if ($this->request->is('ajax')) {

            $response = new \stdClass();
            $response->draw = intval($this->request->getQuery('draw'));
            $response->recordsTotal = 0;
            $response->data = [];
            $response->recordsFiltered = 0;

            $response = $this->serverSideMovements($response, $this->request->getQuery());

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function serverSideMovements($response, $params)
    {
        $response = $this->ServerSidePaymentCommitmentMovements($response, $params);

        $response = $this->ServerSidePaymentsMovements($response, $params);

        $order_asc = function($a, $b)
        {
              if ($a->created < $b->created) {
                  return -1;
              } else if ($a->created > $b->created) {
                  return 1;
              } else {
                  return 0;
              }
        };

        usort($response->data, $order_asc);

        $newData = [];

        foreach ($response->data as $data) {
            $newData[] = $data;
        }

        $response->data = array_reverse($newData); 

        return $response;
    }

    private function ServerSidePaymentCommitmentMovements($response, $params)
    {
        $where = [];
        if ($params['where']['customer_code'] != 0) {
            $where = [
                'customer_code' => $params['where']['customer_code']
            ];
        }

        $response->recordsTotal += $this->PaymentCommitment
            ->find()
            ->where($where)
            ->count();

        $payment_commitment_moves = $this->PaymentCommitment->find('ServerSideDataMovements', [
            'params' => $params,
            'where'  => $where,
        ]);

        $response->recordsFiltered += $this->PaymentCommitment->find('RecordsFilteredMovements', [
            'params' => $params,
            'where'  => $where,
        ]);

        foreach ($payment_commitment_moves as $payment_commitment_move) {
            $response->data[] = new Movement($payment_commitment_move, 'payment_commitment'); 
        }

        return $response;
    }

    private function ServerSidePaymentsMovements($response, $params)
    {
        $this->loadModel('Payments');

        $where = [];
        if ($params['where']['customer_code'] != 0) {
            $where = [
                'customer_code' => $params['where']['customer_code']
            ];
        }

        $response->recordsTotal += $this->Payments
            ->find()
            ->where($where)
            ->count();

        $payments_moves = $this->Payments->find('ServerSideDataMovements', [
            'params' => $params,
            'where'  => $where,
        ]);

        $response->recordsFiltered += $this->Payments->find('RecordsFilteredMovements', [
            'params' => $params,
            'where'  => $where,
        ]);

        foreach ($payments_moves as $payment_move) {
            $response->data[] = new Movement($payment_move, 'payments'); 
        }

        return $response;
    }

    public function verifyCompliance($customer_code)
    {
        $action = 'Verificación de Compromiso de Pago';
        $detail = '';
        $this->registerActivity($action, $detail, NULL, TRUE);

        $paraments = $this->request->getSession()->read('paraments');

        $this->loadModel('PaymentCommitment');
        $this->loadModel('Connections');

        $payment_commitment = $this->PaymentCommitment
            ->find()
            ->contain([
                'Customers.Connections',
            ])->where([
                'PaymentCommitment.customer_code' => $customer_code,
                'PaymentCommitment.ok IS'         => NULL,
            ])->first();

        if ($payment_commitment == NULL) {
            return FALSE;
        }

        $customer = $payment_commitment->customer;

        if ($paraments->automatic_connection_blocking->force) { //configuracion general forzada

            if ($paraments->automatic_connection_blocking->type == 'connection') {

                $flag = TRUE;

                foreach ($customer->connections as $connection) {

                    // Salta configuracion conexion no genere deuda
                    if (!$connection->force_debt) {
                        continue;
                    }

                    if ($connection->debt_month >= $paraments->automatic_connection_blocking->due_debt
                        && $connection->invoices_no_paid >= $paraments->automatic_connection_blocking->invoice_no_paid
                        && $connection->deleted == FALSE
                        && $connection->enabled == TRUE) {

                        //se debe cortar
                        $flag = FALSE;
                    }
                }

                if ($flag) {
                    // se quita el compromiso porque cumplio
                    $customer->payment_commitment = NULL;
                    $this->PaymentCommitment->Customers->save($customer);

                    $payment_commitment->ok = Time::now();
                    $duedate_observation = $payment_commitment->duedate;
                    if ($this->PaymentCommitment->save($payment_commitment)) {

                        $data['type'] = 'success';
                        $info = '';

                        $action = 'Cumplío Compromiso de Pago';
                        $detail .= 'Fecha vencimiento: ' . $duedate_observation . PHP_EOL;
                        $detail .= '---------------------------' . PHP_EOL;

                        $this->registerActivity($action, $detail, $customer->code);
                    }
                } else {
                    return FALSE;
                }

            } else { //tipo de control por cliente

                if ($customer->debt_month >= $paraments->automatic_connection_blocking->due_debt
                    && $customer->invoices_no_paid >= $paraments->automatic_connection_blocking->invoice_no_paid
                    && $customer->deleted == FALSE) {

                    //se debe cortar las conexiones
                    return FALSE;
                } else {
                    // se quita el compromiso porque cumplio
                    $customer->payment_commitment = NULL;
                    $this->PaymentCommitment->Customers->save($customer);

                    $payment_commitment->ok = Time::now();
                    $duedate_observation = $payment_commitment->duedate;
                    if ($this->PaymentCommitment->save($payment_commitment)) {

                        $data['type'] = 'success';
                        $info = '';

                        $action = 'Cumplío Compromiso de Pago';
                        $detail .= 'Fecha vencimiento: ' . $duedate_observation . PHP_EOL;

                        $detail .= '---------------------------' . PHP_EOL;

                        $this->registerActivity($action, $detail, $customer->code);
                    }
                }
            }

        } else { //si la configuracion general NO esta forzada

            if ($customer->deleted == TRUE) {
                return FALSE;
            }

            if ($customer->acb_type == 'connection') { //tipo de control por conexion

                $flag = TRUE;

                foreach ($customer->connections as $conn) {

                    if (!$conn->enabled) {
                        continue;
                    }

                    // Salta configuracion conexion no genere deuda
                    if (!$conn->force_debt) {
                        continue;
                    }

                    if ($conn->debt_month >= $customer->acb_due_debt 
                        && $conn->invoices_no_paid >= $customer->acb_invoice_no_paid) {

                        // debe cortar
                        $flag = FALSE;
                    }
                }

                if ($flag) {
                    // se quita el compromiso porque cumplio
                    $customer->payment_commitment = NULL;
                    $this->PaymentCommitment->Customers->save($customer);

                    $payment_commitment->ok = Time::now();
                    $duedate_observation = $payment_commitment->duedate;
                    if ($this->PaymentCommitment->save($payment_commitment)) {

                        $data['type'] = 'success';
                        $info = '';

                        $action = 'Cumplío Compromiso de Pago';
                        $detail .= 'Fecha vencimiento: ' . $duedate_observation . PHP_EOL;

                        $detail .= '---------------------------' . PHP_EOL;

                        $this->registerActivity($action, $detail, $customer->code);
                    }
                } else {
                    return FALSE;
                }

            } else { //tipo de control por cliente

                if ($customer->debt_month >= $customer->acb_due_debt 
                    && $customer->invoices_no_paid >= $customer->acb_invoice_no_paid) {

                    //se debe cortar
                    return FALSE;
                } else {

                    // se quita el compromiso porque cumplio
                    $customer = $payment_commitment->customer;
                    $customer->payment_commitment = NULL;
                    $this->PaymentCommitment->Customers->save($customer);

                    $payment_commitment->ok = Time::now();
                    $duedate_observation = $payment_commitment->duedate;
                    if ($this->PaymentCommitment->save($payment_commitment)) {

                        $data['type'] = 'success';
                        $info = '';

                        $customer_name = $customer->name . ' - Código: ' . $customer->code . ' - ' . $afip_codes['doc_types'][$customer->doc_type] . ': ' . $customer->ident;

                        $action = 'Cumplío Compromiso de Pago';
                        $detail .= 'Fecha vencimiento: ' . $duedate_observation . PHP_EOL;

                        $detail .= '---------------------------' . PHP_EOL;

                        $this->registerActivity($action, $detail, $customer->code);
                    }
                }
            }
        }
        return TRUE;
    }

    public function forcePaymentCommitmentControl()
    {
        if ($this->request->is('post')) {

            $now = $_POST['date'];
            $now = explode('/', $now);
            $now = $now[2] . '-' . $now[1] . '-' . $now[0];

            $action = 'Ejecución Compromiso de Pago (forzada)';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $paraments = $this->request->getSession()->read('paraments');
            $afip_codes = $this->request->getSession()->read('afip_codes');

            $data = [
                'code'    => 200,
                'message' => 'datos general',
                'counter'   => 0
            ];

            $this->loadModel('PaymentCommitment');
            $this->loadModel('Connections');
            $this->loadModel('ConnectionsHasMessageTemplates');
            $this->loadModel('MessageTemplates');
            $this->loadModel('Debts');
            $this->loadModel('DebitNotes');

            $payment_commitment = $this->PaymentCommitment
                ->find()
                ->contain([
                    'Customers.Connections.Services',
                ])->where([
                    'PaymentCommitment.duedate LIKE' => $now . '%',
                    'PaymentCommitment.ok IS'        => NULL
                ]);

            $message = "";

            $messageTemplate = $this->MessageTemplates->find()
                ->where([
                    'deleted' => 0,
                    'type'    => 'Bloqueo',
                    'enabled' => 1
                ])
                ->order([
                    'priority' => 'DESC'
                ])->first();

            if ($paraments->automatic_connection_blocking->force) { //configuracion general forzada

                if ($paraments->automatic_connection_blocking->type == 'connection') {

                    foreach ($payment_commitment as $pc) {

                        $customer = $pc->customer;
                        $customer->payment_commitment = NULL;
                        $this->PaymentCommitment->Customers->save($customer);

                        $pc->ok = Time::now();
                        $this->PaymentCommitment->save($pc);

                        foreach ($pc->customer->connections as $connection) {

                            // Salta configuracion conexion no genere deuda
                            if (!$connection->force_debt) {
                                continue;
                            }

                            if ($connection->debt_month >= $paraments->automatic_connection_blocking->due_debt
                                && $connection->invoices_no_paid >= $paraments->automatic_connection_blocking->invoice_no_paid
                                && $connection->deleted == FALSE
                                && $connection->enabled == TRUE) {

                                $connection = $this->Connections->get($connection->id, [
                                    'contain' => [
                                        'Customers.Cities', 
                                        'Controllers',
                                        'Services'
                                    ]
                                ]);

                                if ($this->IntegrationRouter->disableConnection($connection)) {

                                    $connection->enabled = FALSE;

                                    if ($this->Connections->save($connection)) {

                                        $customer_code = $connection->customer_code;
                                        $ip = $connection->ip;
                                        $mac = $connection->mac;

                                        $detail = "";
                                        $detail .= 'Conexión: ' . $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL;
                                        $detail .= 'IP: ' . $ip . PHP_EOL;
                                        $detail .= 'MAC: ' . $mac . PHP_EOL;
                                        $action = 'Bloqueo de Conexión - vencimiento de Compromiso de Pago';
                                        $this->registerActivity($action, $detail, $customer_code);

                                        $chmt =  $this->ConnectionsHasMessageTemplates->newEntity();
                                        $chmt->created = Time::now();
                                        $chmt->connections_id = $connection->id;
                                        $chmt->message_template_id = $messageTemplate->id;
                                        $chmt->ip = $connection->ip;
                                        $chmt->type = $messageTemplate->type;

                                        if (!$this->ConnectionsHasMessageTemplates->save($chmt)) {
                                            $message .= 'connnection_id: ' . $connection->id . '. No pudo crear ConnectionsHasMessageTemplates';
                                            break;
                                        }

                                        // Se controla, si ya se aplico el recargo.
                                        $now = Time::now();
                                        $date_now_blocking = $now->year . '-' . $now->month . '-' . $paraments->automatic_connection_blocking->day . ' ' . $paraments->automatic_connection_blocking->hours_execution . ':00';

                                        $date_now_blocking = new Time($date_now_blocking);
                                        $date_payment_commitment = new Time($pc->created);

                                        // Condición si se debe generar el recargo o no.
                                        if ($date_now_blocking > $date_payment_commitment) {

                                            //aplico recargo
                                            if ($paraments->automatic_connection_blocking->surcharge_enabled 
                                                && $paraments->automatic_connection_blocking->surcharge_value > 0) {

                                                switch ($paraments->automatic_connection_blocking->surcharge_type) {

                                                    case 'next_month':

                                                        $duedate = Time::now()->day($paraments->accountant->daydue)->modify('+1 month');

                                                        $debt_data = [
                                                            'concept'              => $paraments->automatic_connection_blocking->surcharge_concept,
                                                            'seating'              => NULL,
                                                            'customer'             => $connection->customer,
                                                            'value'                => $paraments->automatic_connection_blocking->surcharge_value,
                                                            'alicuot'              => $connection->service->aliquot,
                                                            'duedate'              => $duedate,
                                                            'connection_id'        => $connection->customer->billing_for_service ? $connection->id : NULL,
                                                            'is_presupuesto_force' => $paraments->automatic_connection_blocking->surcharge_presupuesto,
                                                            'period'               => Time::now()->format('m/Y'),
                                                        ];

                                                        $this->CurrentAccount->addDebt($debt_data);

                                                        $customer_code = $connection->customer_code;

                                                        $duedate = $duedate->format('d/m/Y');

                                                        $detail = "";
                                                        $detail .= "Información: se genero una deuda para el próximo mes según configuración Bloqueo Automático.";
                                                        $detail .= 'Conexión: ' . $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL; 
                                                        $detail .= "Vencimiento: " . $duedate . PHP_EOL;
                                                        $detail .= "Concepto: " . $paraments->automatic_connection_blocking->surcharge_concept . PHP_EOL;
                                                        $detail .= "Total: " . $paraments->automatic_connection_blocking->surcharge_value . PHP_EOL;
                                                        $action = 'Recargo por reconexión - vencimiento de Compromiso de Pago';
                                                        $this->registerActivity($action, $detail, $customer_code);
                                                        break;

                                                    case 'this_month':

                                                        $tipo_comp = 'NDX';

                                                        if (!$paraments->automatic_connection_blocking->surcharge_presupuesto) {

                                                            if ($connection->customer->is_presupuesto) { 

                                                                $tipo_comp ='NDX';

                                                            } else {

                                                                foreach ($paraments->invoicing->business as $business) {

                                                                    if ($business->id == $connection->customer->business_billing) {
                                                                        $connection->customer->business = $business;
                                                                    }
                                                                }

                                                                $tipo_comp = $afip_codes['combinationsDebitNotes'][$connection->customer->business->responsible][$connection->customer->responsible];
                                                            }
                                                        }

                                                        //parametros $total, $tax, $debt->quantity
                                                        $row = $this->CurrentAccount->calulateIvaAndNeto($paraments->automatic_connection_blocking->surcharge_value, $afip_codes['alicuotas_percectage'][$connection->service->aliquot], 1);

                                                        $concepts = [];

                                                        $concept = new \stdClass;
                                                        $concept->type        = 'S';
                                                        $concept->code        = 99;
                                                        $concept->description = $paraments->automatic_connection_blocking->surcharge_concept;
                                                        $concept->quantity    = 1;
                                                        $concept->unit        = 7;
                                                        $concept->price       = $row->price;
                                                        $concept->discount    = 0;
                                                        $concept->sum_price   = $row->sum_price;;
                                                        $concept->tax         = $connection->service->aliquot;
                                                        $concept->sum_tax     = $row->sum_tax;
                                                        $concept->total       = $row->total;
                                                        $concepts[0] = $concept;

                                                        $debitNote  = [
                                                            'customer'      => $connection->customer,
                                                            'concept_type'  => 2,
                                                            'date'          => Time::now(),
                                                            'date_start'    => Time::now(),
                                                            'date_end'      => Time::now(),
                                                            'duedate'       => Time::now(),
                                                            'concepts'      => $concepts,
                                                            'tipo_comp'     => $tipo_comp,
                                                            'comments'      => $paraments->automatic_connection_blocking->surcharge_concept,
                                                            'manual'        => TRUE,
                                                            'business_id'   => $connection->customer->business_billing,
                                                            'connection_id' => $connection->customer->billing_for_service ? $connection->id : NULL,
                                                        ];

                                                        $debitNote = $this->FiscoAfipComp->debitNote($debitNote);

                                                        if ($debitNote) {

                                                            //informo afip si no es de tipo X

                                                            if ($this->FiscoAfip->informar($debitNote, FiscoAfipComp::TYPE_DEBIT_NOTE)) {

                                                                if ($this->DebitNotes->save($debitNote)) {

                                                                    $customer_code = $connection->customer_code;

                                                                    $duedate = Time::now()->format('d/m/Y');

                                                                    $detail = "";
                                                                    $detail .= 'Conexión: ' . $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL; 
                                                                    $detail .= "Tipo de Comp: " . $tipo_comp . PHP_EOL;
                                                                    $detail .= "Vencimiento: " . $duedate . PHP_EOL;
                                                                    $detail .= "Concepto: " . $paraments->automatic_connection_blocking->surcharge_concept . PHP_EOL;
                                                                    $detail .= "Total: " . $concept->total . PHP_EOL;
                                                                    $action = 'Recargo por reconexión - vencimiento de Compromiso de Pago';
                                                                    $this->registerActivity($action, $detail, $customer_code);
                                                            }

                                                        } else {
                                                            $this->FiscoAfipComp->rollbackInvoice($debitNote, TRUE);
                                                        }
                                                        } else {
                                                            $this->FiscoAfipComp->rollbackInvoice($debitNote, TRUE);
                                                        }
                                                        break;
                                                }
                                            } //end surcharge_enabled = true
                                        }

                                        $data['counter']++;

                                    } else {
                                        $message .= 'connnection_id: ' . $connection->id . '. No pudo guardar la conexión';
                                        break;
                                    }
                                } else {
                                    $message .= 'connnection_id: ' . $connection->id . '. No pudo deshabilitar la conexión';
                                }
                            }
                        }
                    }

                } else { //tipo de control por cliente

                    foreach ($payment_commitment as $pc) {

                        $customer = $pc->customer;
                        $customer->payment_commitment = NULL;
                        $this->PaymentCommitment->Customers->save($customer);

                        $pc->ok = Time::now();
                        $this->PaymentCommitment->save($pc);

                        if ($customer->debt_month >= $paraments->automatic_connection_blocking->due_debt
                            && $customer->invoices_no_paid >= $paraments->automatic_connection_blocking->invoice_no_paid
                            && $customer->deleted == FALSE) {

                            foreach ($customer->connections as $conn) {

                                if (!$conn->enabled) {
                                    continue;
                                }

                                // Salta configuracion conexion no genere deuda
                                if (!$conn->force_debt) {
                                    continue;
                                }

                                $connection = $this->Connections->get($conn->id, [
                                    'contain' => [
                                        'Customers.Cities', 
                                        'Controllers',
                                        'Services'
                                    ]
                                ]);

                                if ($this->IntegrationRouter->disableConnection($connection)) {

                                    $connection->enabled = FALSE;

                                    if ($this->Connections->save($connection)) {

                                        $customer_code = $connection->customer_code;
                                        $ip = $connection->ip;
                                        $mac = $connection->mac;

                                        $detail = "";
                                        $detail .= 'Conexión: '. $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL; 
                                        $detail .= 'IP: ' . $ip . PHP_EOL;
                                        $detail .= 'MAC: ' . $mac . PHP_EOL;
                                        $action = 'Bloqueo de Conexión - vencimiento de Compromiso de Pago';
                                        $this->registerActivity($action, $detail, $customer_code);


                                        $chmt =  $this->ConnectionsHasMessageTemplates->newEntity();
                                        $chmt->created = Time::now();
                                        $chmt->connections_id = $connection->id;
                                        $chmt->message_template_id = $messageTemplate->id;
                                        $chmt->ip = $connection->ip;
                                        $chmt->type = $messageTemplate->type;

                                        if (!$this->ConnectionsHasMessageTemplates->save($chmt)) {
                                            $message .= 'connnection_id: ' . $connection->id . '. No pudo crear ConnectionsHasMessageTemplates';
                                            break;
                                        }
                                        
                                        // Se controla, si ya se aplico el recargo.
                                        $now = Time::now();
                                        $date_now_blocking = $now->year . '-' . $now->month . '-' . $paraments->automatic_connection_blocking->day . ' ' . $paraments->automatic_connection_blocking->hours_execution . ':00';

                                        $date_now_blocking = new Time($date_now_blocking);
                                        $date_payment_commitment = new Time($pc->created);

                                        // Condición si se debe generar el recargo o no.
                                        if ($date_now_blocking > $date_payment_commitment) {

                                            //aplico recargo
                                            if ($paraments->automatic_connection_blocking->surcharge_enabled 
                                                && $paraments->automatic_connection_blocking->surcharge_value > 0) {

                                                switch ($paraments->automatic_connection_blocking->surcharge_type) {

                                                    case 'next_month':

                                                        $duedate = Time::now()->day($paraments->accountant->daydue)->modify('+1 month');

                                                        $debt_data = [
                                                            'concept'              => $paraments->automatic_connection_blocking->surcharge_concept,
                                                            'seating'              => NULL,
                                                            'customer'             => $connection->customer,
                                                            'value'                => $paraments->automatic_connection_blocking->surcharge_value,
                                                            'alicuot'              => $connection->service->aliquot,
                                                            'duedate'              => $duedate,
                                                            'connection_id'        => $connection->customer->billing_for_service ? $connection->id : NULL,
                                                            'is_presupuesto_force' => $paraments->automatic_connection_blocking->surcharge_presupuesto,
                                                            'period'               => Time::now()->format('m/Y'),
                                                        ];

                                                        $this->CurrentAccount->addDebt($debt_data);
                                                        $customer_code = $connection->customer_code;

                                                        $duedate = $duedate->format('d/m/Y');

                                                        $detail = "";
                                                        $detail .= "Información: se genero una deuda para el próximo mes según configuración Bloqueo Automático.";
                                                        $detail .= 'Conexión: ' . $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL; 
                                                        $detail .= "Vencimiento: " . $duedate . PHP_EOL;
                                                        $detail .= "Concepto: " . $paraments->automatic_connection_blocking->surcharge_concept . PHP_EOL;
                                                        $detail .= "Total: " . $paraments->automatic_connection_blocking->surcharge_value . PHP_EOL;
                                                        $action = 'Recargo por reconexión - vencimiento de Compromiso de Pago';
                                                        $this->registerActivity($action, $detail, $customer_code);
                                                        break;

                                                    case 'this_month':

                                                        $tipo_comp = 'NDX';

                                                        if (!$paraments->automatic_connection_blocking->surcharge_presupuesto) {

                                                            if ($customer->is_presupuesto) { 

                                                                $tipo_comp ='NDX';

                                                            } else {

                                                                foreach ($paraments->invoicing->business as $business) {

                                                                    if ($business->id == $customer->business_billing) {
                                                                        $customer->business = $business;
                                                                    }
                                                                }

                                                                $tipo_comp = $afip_codes['combinationsDebitNotes'][$customer->business->responsible][$customer->responsible];
                                                            }
                                                        }

                                                        //parametros $total, $tax, $debt->quantity
                                                        $row = $this->CurrentAccount->calulateIvaAndNeto($paraments->automatic_connection_blocking->surcharge_value, $afip_codes['alicuotas_percectage'][$connection->service->aliquot], 1);

                                                        $concepts = [];

                                                        $concept = new \stdClass;
                                                        $concept->type        = 'S';
                                                        $concept->code        = 99;
                                                        $concept->description = $paraments->automatic_connection_blocking->surcharge_concept;
                                                        $concept->quantity    = 1;
                                                        $concept->unit        = 7;
                                                        $concept->price       = $row->price;
                                                        $concept->discount    = 0;
                                                        $concept->sum_price   = $row->sum_price;;
                                                        $concept->tax         = $connection->service->aliquot;
                                                        $concept->sum_tax     = $row->sum_tax;
                                                        $concept->total       = $row->total;
                                                        $concepts[0] = $concept;

                                                        $debitNote  = [
                                                            'customer'      => $connection->customer,
                                                            'concept_type'  => 2,
                                                            'date'          => Time::now(),
                                                            'date_start'    => Time::now(),
                                                            'date_end'      => Time::now(),
                                                            'duedate'       => Time::now(),
                                                            'concepts'      => $concepts,
                                                            'tipo_comp'     => $tipo_comp,
                                                            'comments'      => $paraments->automatic_connection_blocking->surcharge_concept,
                                                            'manual'        => TRUE,
                                                            'business_id'   => $customer->business_billing,
                                                            'connection_id' => $connection->customer->billing_for_service ? $connection->id : NULL,
                                                        ];

                                                        $debitNote = $this->FiscoAfipComp->debitNote($debitNote);

                                                        if ($debitNote) {

                                                            //informo afip si no es de tipo X

                                                            if ($this->FiscoAfip->informar($debitNote, FiscoAfipComp::TYPE_DEBIT_NOTE)) {

                                                                if ($this->DebitNotes->save($debitNote)) {
                                                                    $customer_code = $connection->customer_code;

                                                                    $duedate = Time::now()->format('d/m/Y');

                                                                    $detail = "";
                                                                    $detail .= 'Conexión: ' . $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL; 
                                                                    $detail .= "Tipo de Comp: " . $tipo_comp . PHP_EOL;
                                                                    $detail .= "Vencimiento: " . $duedate . PHP_EOL;
                                                                    $detail .= "Concepto: " . $paraments->automatic_connection_blocking->surcharge_concept . PHP_EOL;
                                                                    $detail .= "Total: " . $concept->total . PHP_EOL;
                                                                    $action = 'Recargo por reconexión - vencimiento de Compromiso de Pago';
                                                                    $this->registerActivity($action, $detail, $customer_code);
                                                            }
                                                        } else {
                                                            $this->FiscoAfipComp->rollbackInvoice($debitNote, TRUE);
                                                        }
                                                        } else {
                                                            $this->FiscoAfipComp->rollbackInvoice($debitNote, TRUE);
                                                        }
                                                        break;
                                                }
                                            } //end surcharge_enabled = true
                                        }

                                        $data['counter']++;

                                    } else {
                                        $message .= 'connnection_id: ' . $connection->id . '. No pudo guardar la conexión';
                                        break;
                                    }
                                } else {
                                    $message .= 'connnection_id: ' . $connection->id . '. No pudo deshabilitar la conexión';
                                }
                            }
                        }
                    }
                }

            } else { //si la configuracion general NO esta forzada

                foreach ($payment_commitment as $pc) {//recorro los clientes

                    $customer = $pc->customer;
                    $customer->payment_commitment = NULL;
                    $this->PaymentCommitment->Customers->save($customer);

                    $pc->ok = Time::now();
                    $this->PaymentCommitment->save($pc);

                    if ($customer->deleted == TRUE) {
                        continue;
                    }

                    if ($customer->acb_type == 'connection') { //tipo de control por conexion

                        foreach ($customer->connections as $conn) {

                            if (!$conn->enabled) {
                                continue;
                            }

                            // Salta configuracion conexion no genere deuda
                            if (!$conn->force_debt) {
                                continue;
                            }

                            if ($conn->debt_month >= $customer->acb_due_debt 
                                && $conn->invoices_no_paid >= $customer->acb_invoice_no_paid) {

                                $connection = $this->Connections->get($conn->id, [
                                    'contain' => [
                                        'Customers.Cities', 
                                        'Controllers',
                                        'Services'
                                    ]
                                ]);

                                if ($this->IntegrationRouter->disableConnection($connection)) {

                                    $connection->enabled = FALSE;

                                    if ($this->Connections->save($connection)) {

                                        $customer_code = $connection->customer_code;
                                        $ip = $connection->ip;
                                        $mac = $connection->mac;

                                        $detail = "";
                                        $detail .= 'Conexión: '. $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL; 
                                        $detail .= 'IP: ' . $ip . PHP_EOL;
                                        $detail .= 'MAC: ' . $mac . PHP_EOL;
                                        $action = 'Bloqueo de Conexión - vencimiento de Compromiso de Pago';
                                        $this->registerActivity($action, $detail, $customer_code);

                                        $chmt =  $this->ConnectionsHasMessageTemplates->newEntity();
                                        $chmt->created = Time::now();
                                        $chmt->connections_id = $connection->id;
                                        $chmt->message_template_id = $messageTemplate->id;
                                        $chmt->ip = $connection->ip;
                                        $chmt->type = $messageTemplate->type;

                                        if (!$this->ConnectionsHasMessageTemplates->save($chmt)) {
                                            $message .= 'connnection_id: ' . $connection->id . '. No pudo crear ConnectionsHasMessageTemplates';
                                            break;
                                        }

                                        // Se controla, si ya se aplico el recargo.
                                        $now = Time::now();
                                        $date_now_blocking = $now->year . '-' . $now->month . '-' . $paraments->automatic_connection_blocking->day . ' ' . $paraments->automatic_connection_blocking->hours_execution . ':00';

                                        $date_now_blocking = new Time($date_now_blocking);
                                        $date_payment_commitment = new Time($pc->created);

                                        // Condición si se debe generar el recargo o no.
                                        if ($date_now_blocking > $date_payment_commitment) {

                                            //aplico recargo
                                            if ($customer->acb_surcharge_enabled 
                                                && $paraments->automatic_connection_blocking->surcharge_value > 0) {

                                                switch ($paraments->automatic_connection_blocking->surcharge_type) {

                                                    case 'next_month':

                                                        $duedate = Time::now()->day($paraments->accountant->daydue)->modify('+1 month');

                                                        $debt_data = [
                                                            'concept'              => $paraments->automatic_connection_blocking->surcharge_concept,
                                                            'seating'              => NULL,
                                                            'customer'             => $connection->customer,
                                                            'value'                => $paraments->automatic_connection_blocking->surcharge_value,
                                                            'alicuot'              => $connection->service->aliquot,
                                                            'duedate'              => $duedate,
                                                            'connection_id'        => $connection->customer->billing_for_service ? $connection->id : NULL,
                                                            'is_presupuesto_force' => $paraments->automatic_connection_blocking->surcharge_presupuesto,
                                                            'period'               => Time::now()->format('m/Y'),
                                                        ];

                                                        $this->CurrentAccount->addDebt($debt_data);
                                                        $customer_code = $connection->customer_code;

                                                        $duedate = $duedate->format('d/m/Y');

                                                        $detail = "";
                                                        $detail .= "Información: se genero una deuda para el próximo mes según configuración Bloqueo Automático.";
                                                        $detail .= 'Conexión: ' . $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL; 
                                                        $detail .= "Vencimiento: " . $duedate . PHP_EOL;
                                                        $detail .= "Concepto: " . $paraments->automatic_connection_blocking->surcharge_concept . PHP_EOL;
                                                        $detail .= "Total: " . $paraments->automatic_connection_blocking->surcharge_value . PHP_EOL;
                                                        $action = 'Recargo por reconexión - vencimiento de Compromiso de Pago';
                                                        $this->registerActivity($action, $detail, $customer_code);
                                                        break;

                                                    case 'this_month':

                                                        $tipo_comp = 'NDX';

                                                        if (!$paraments->automatic_connection_blocking->surcharge_presupuesto) {

                                                            if ($customer->is_presupuesto) { 

                                                                $tipo_comp ='NDX';

                                                            } else {

                                                                foreach ($paraments->invoicing->business as $business) {

                                                                    if ($business->id == $customer->business_billing) {
                                                                        $customer->business = $business;
                                                                    }
                                                                }

                                                                $tipo_comp = $afip_codes['combinationsDebitNotes'][$customer->business->responsible][$customer->responsible];
                                                            }
                                                        }

                                                        //parametros $total, $tax, $debt->quantity
                                                        $row = $this->CurrentAccount->calulateIvaAndNeto($paraments->automatic_connection_blocking->surcharge_value, $afip_codes['alicuotas_percectage'][$connection->service->aliquot], 1);

                                                        $concepts = [];

                                                        $concept = new \stdClass;
                                                        $concept->type        = 'S';
                                                        $concept->code        = 99;
                                                        $concept->description = $paraments->automatic_connection_blocking->surcharge_concept;
                                                        $concept->quantity    = 1;
                                                        $concept->unit        = 7;
                                                        $concept->price       = $row->price;
                                                        $concept->discount    = 0;
                                                        $concept->sum_price   = $row->sum_price;;
                                                        $concept->tax         = $connection->service->aliquot;
                                                        $concept->sum_tax     = $row->sum_tax;
                                                        $concept->total       = $row->total;
                                                        $concepts[0] = $concept;

                                                        $debitNote  = [
                                                            'customer'      => $connection->customer,
                                                            'concept_type'  => 2,
                                                            'date'          => Time::now(),
                                                            'date_start'    => Time::now(),
                                                            'date_end'      => Time::now(),
                                                            'duedate'       => Time::now(),
                                                            'concepts'      => $concepts,
                                                            'tipo_comp'     => $tipo_comp,
                                                            'comments'      => $paraments->automatic_connection_blocking->surcharge_concept,
                                                            'manual'        => true,
                                                            'business_id'   => $customer->business_billing,
                                                            'connection_id' => $connection->customer->billing_for_service ? $connection->id : NULL,
                                                        ];

                                                        $debitNote = $this->FiscoAfipComp->debitNote($debitNote);
                                            
                                                        if ($debitNote) {

                                                            //informo afip si no es de tipo X

                                                            if ($this->FiscoAfip->informar($debitNote, FiscoAfipComp::TYPE_DEBIT_NOTE)) {

                                                                if ($this->DebitNotes->save($debitNote)) {
                                                                    $customer_code = $connection->customer_code;

                                                                    $duedate = Time::now()->format('d/m/Y');

                                                                    $detail = "";
                                                                    $detail .= 'Conexión: ' . $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL; 
                                                                    $detail .= "Tipo de Comp: " . $tipo_comp . PHP_EOL;
                                                                    $detail .= "Vencimiento: " . $duedate . PHP_EOL;
                                                                    $detail .= "Concepto: " . $paraments->automatic_connection_blocking->surcharge_concept . PHP_EOL;
                                                                    $detail .= "Total: " . $concept->total . PHP_EOL;
                                                                    $action = 'Recargo por reconexión - vencimiento de Compromiso de Pago';
                                                                    $this->registerActivity($action, $detail, $customer_code);
                                                            }
                                                        } else {
                                                            $this->FiscoAfipComp->rollbackInvoice($debitNote, TRUE);
                                                        }
                                                        } else{
                                                            $this->FiscoAfipComp->rollbackInvoice($debitNote, TRUE);
                                                        }
                                                        break;
                                                }
                                            } //end surcharge_enabled = true
                                        }

                                        $data['counter']++;

                                    } else {
                                        $message .= 'connnection_id: ' . $connection->id . '. No pudo guardar la conexión';
                                        break;
                                    }
                                } else {
                                    $message .= 'connnection_id: ' . $connection->id . '. No pudo deshabilitar la conexión';
                                }
                            }
                        }

                    } else { //tipo de control por cliente

                        if ($customer->debt_month >= $customer->acb_due_debt 
                            && $customer->invoices_no_paid >= $customer->acb_invoice_no_paid) {

                            foreach ($customer->connections as $conn) {

                                if (!$conn->enabled) {
                                    continue;
                                }

                                $connection = $this->Connections->get($conn->id, [
                                    'contain' => ['Customers.Cities', 'Controllers', 'Services']
                                ]);

                                if ($this->IntegrationRouter->disableConnection($connection)) {

                                    $connection->enabled = false;

                                    if ($this->Connections->save($connection)) {

                                        $customer_code = $connection->customer_code;
                                        $ip = $connection->ip;
                                        $mac = $connection->mac;

                                        $detail = "";
                                        $detail .= 'Conexión: '. $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL; 
                                        $detail .= 'IP: ' . $ip . PHP_EOL;
                                        $detail .= 'MAC: ' . $mac . PHP_EOL;
                                        $action = 'Bloqueo de Conexión - vencimiento de Compromiso de Pago';
                                        $this->registerActivity($action, $detail, $customer_code);

                                        $chmt =  $this->ConnectionsHasMessageTemplates->newEntity();
                                        $chmt->created = Time::now();
                                        $chmt->connections_id = $connection->id;
                                        $chmt->message_template_id = $messageTemplate->id;
                                        $chmt->ip = $connection->ip;
                                        $chmt->type = $messageTemplate->type;

                                        if (!$this->ConnectionsHasMessageTemplates->save($chmt)) {
                                            $message .= 'connnection_id: ' . $connection->id . '. No pudo crear ConnectionsHasMessageTemplates';
                                            break;
                                        }

                                        // Se controla, si ya se aplico el recargo.
                                        $now = Time::now();
                                        $date_now_blocking = $now->year . '-' . $now->month . '-' . $paraments->automatic_connection_blocking->day . ' ' . $paraments->automatic_connection_blocking->hours_execution . ':00';

                                        $date_now_blocking = new Time($date_now_blocking);
                                        $date_payment_commitment = new Time($pc->created);

                                        // Condición si se debe generar el recargo o no.
                                        if ($date_now_blocking > $date_payment_commitment) {

                                            //aplico recargo
                                            if ($customer->acb_surcharge_enabled 
                                                && $paraments->automatic_connection_blocking->surcharge_value > 0) {

                                                switch ($paraments->automatic_connection_blocking->surcharge_type) {

                                                    case 'next_month':

                                                        $duedate = Time::now()->day($paraments->accountant->daydue)->modify('+1 month');

                                                        $debt_data = [
                                                            'concept'              => $paraments->automatic_connection_blocking->surcharge_concept,
                                                            'seating'              => NULL,
                                                            'customer'             => $connection->customer,
                                                            'value'                => $paraments->automatic_connection_blocking->surcharge_value,
                                                            'alicuot'              => $connection->service->aliquot,
                                                            'duedate'              => $duedate,
                                                            'connection_id'        => $connection->customer->billing_for_service ? $connection->id : NULL,
                                                            'is_presupuesto_force' => $paraments->automatic_connection_blocking->surcharge_presupuesto,
                                                            'period'               => Time::now()->format('m/Y'),
                                                        ];

                                                        $debt = $this->CurrentAccount->addDebt($debt_data);
                                                        $customer_code = $connection->customer_code;

                                                        $duedate = $duedate->format('d/m/Y');

                                                        $detail = "";
                                                        $detail .= "Información: se genero una deuda para el próximo mes según configuración Bloqueo Automático.";
                                                        $detail .= 'Conexión: ' . $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL; 
                                                        $detail .= "Vencimiento: " . $duedate . PHP_EOL;
                                                        $detail .= "Concepto: " . $paraments->automatic_connection_blocking->surcharge_concept . PHP_EOL;
                                                        $detail .= "Total: " . $paraments->automatic_connection_blocking->surcharge_value . PHP_EOL;
                                                        $action = 'Recargo por reconexión - vencimiento de Compromiso de Pago';
                                                        $this->registerActivity($action, $detail, $customer_code);
                                                        break;

                                                    case 'this_month':

                                                        $tipo_comp = 'NDX';

                                                        if (!$paraments->automatic_connection_blocking->surcharge_presupuesto) {

                                                            if ($customer->is_presupuesto) { 

                                                                $tipo_comp ='NDX';

                                                            } else {

                                                                foreach ($paraments->invoicing->business as $business) {

                                                                    if ($business->id == $customer->business_billing) {
                                                                        $customer->business = $business;
                                                                    }
                                                                }

                                                                $tipo_comp = $afip_codes['combinationsDebitNotes'][$customer->business->responsible][$customer->responsible];
                                                            }
                                                        }

                                                        //parametros $total, $tax, $debt->quantity
                                                        $row = $this->CurrentAccount->calulateIvaAndNeto($paraments->automatic_connection_blocking->surcharge_value, $afip_codes['alicuotas_percectage'][$connection->service->aliquot], 1);

                                                        $concepts = [];

                                                        $concept = new \stdClass;
                                                        $concept->type        = 'S';
                                                        $concept->code        = 99;
                                                        $concept->description = $paraments->automatic_connection_blocking->surcharge_concept;
                                                        $concept->quantity    = 1;
                                                        $concept->unit        = 7;
                                                        $concept->price       = $row->price;
                                                        $concept->discount    = 0;
                                                        $concept->sum_price   = $row->sum_price;;
                                                        $concept->tax         = $connection->service->aliquot;
                                                        $concept->sum_tax     = $row->sum_tax;
                                                        $concept->total       = $row->total;
                                                        $concepts[0] = $concept;

                                                        $debitNote  = [
                                                            'customer'      => $connection->customer,
                                                            'concept_type'  => 2,
                                                            'date'          => Time::now(),
                                                            'date_start'    => Time::now(),
                                                            'date_end'      => Time::now(),
                                                            'duedate'       => Time::now(),
                                                            'concepts'      => $concepts,
                                                            'tipo_comp'     => $tipo_comp,
                                                            'comments'      => $paraments->automatic_connection_blocking->surcharge_concept,
                                                            'manual'        => TRUE,
                                                            'business_id'   => $customer->business_billing,
                                                            'connection_id' => $connection->customer->billing_for_service ? $connection->id : NULL,
                                                        ];

                                                        $debitNote = $this->FiscoAfipComp->debitNote($debitNote);
                                            
                                                        if ($debitNote) {

                                                            //informo afip si no es de tipo X

                                                            if ($this->FiscoAfip->informar($debitNote, FiscoAfipComp::TYPE_DEBIT_NOTE)) {

                                                                if ($this->DebitNotes->save($debitNote)) {
                                                                    $customer_code = $connection->customer_code;

                                                                    $duedate = Time::now()->format('d/m/Y');

                                                                    $detail = "";
                                                                    $detail .= 'Conexión: ' . $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL; 
                                                                    $detail .= "Tipo de Comp: " . $tipo_comp . PHP_EOL;
                                                                    $detail .= "Vencimiento: " . $duedate . PHP_EOL;
                                                                    $detail .= "Concepto: " . $paraments->automatic_connection_blocking->surcharge_concept . PHP_EOL;
                                                                    $detail .= "Total: " . $concept->total . PHP_EOL;
                                                                    $action = 'Recargo por reconexión - vencimiento de Compromiso de Pago';
                                                                    $this->registerActivity($action, $detail, $customer_code);
                                                            }
                                                        } else {
                                                            $this->FiscoAfipComp->rollbackInvoice($debitNote, TRUE);
                                                        }
                                                        } else {
                                                            $this->FiscoAfipComp->rollbackInvoice($debitNote, TRUE);
                                                        }
                                                        break;
                                                }
                                            } //end surcharge_enabled = true
                                        }

                                        $data['counter']++;

                                    } else {
                                        $message .= 'connnection_id: ' . $connection->id . '. No pudo guardar la conexión';
                                        break;
                                    }
                                } else {
                                    $message .= 'connnection_id: ' . $connection->id . '. No pudo deshabilitar la conexión';
                                }
                            }
                        }
                    }
                }//end foreach customers
            }
            $this->Flash->success('Mensaje: ' . $data['message'] . ' - Compromiso de pago completados: ' . $data['counter']);
            return  $this->redirect($this->referer());
        }
    }
}

class Movement {

    public $id;
    public $created;
    public $detail;
    public $duedate;
    public $customer;
    public $import;

    public $type_movement;

    public function __construct($data, $type) {

        switch ($type) {

            case 'payment_commitment':

                $this->id       = $data->id;
                $this->created  = $data->created;
                $this->detail   = $data->comment;
                $this->duedate  = $data->duedate;
                $this->customer = $data->customer;
                $this->import   = $data->import;

                $this->type_move  = $type;
                $this->controller = 'PaymentCommitment';
                break;

            case 'payments':

                $this->id       = $data->id;
                $this->created  = $data->created;
                $this->detail   = $data->concept;
                $this->duedate  = NULL;
                $this->customer = $data->customer;
                $this->import   = $data->import;

                $this->type_move  = $type;
                $this->controller = 'Payments';
                break;
        }
    }
}
