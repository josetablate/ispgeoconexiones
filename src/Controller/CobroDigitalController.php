<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Event\Event;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\Filesystem\Folder;
use Cake\Event\EventManager;

use App\Controller\Component\ComprobantesComponent;
use App\Controller\Component\AccountantComponent;
use App\Controller\Component\PDFGeneratorComponent;

class CobroDigitalController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Accountant', [
             'className' => '\App\Controller\Component\Admin\Accountant'
        ]);

        $this->loadComponent('CurrentAccount', [
             'className' => '\App\Controller\Component\Admin\CurrentAccount'
        ]);

        $this->loadComponent('FiscoAfipComp', [
             'className' => '\App\Controller\Component\Admin\FiscoAfipComp'
        ]);

        $this->loadComponent('FiscoAfipCompPdf', [
             'className' => '\App\Controller\Component\Admin\FiscoAfipCompPdf'
        ]);

        $this->loadComponent('IntegrationRouter', [
             'className' => '\App\Controller\Component\Integrations\IntegrationRouter'
        ]);

        $this->initEventManager();
    }

    public function initEventManager()
    {
        EventManager::instance()->on(
            'CobroDigitalController.Error',
            function ($event, $msg, $data, $flash = false) {

                $this->loadModel('ErrorLog');
                $log = $this->ErrorLog->newEntity();
                $log->user_id = $this->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Error';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->ErrorLog->save($log);

                if ($flash) {
                    $this->Flash->error(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'CobroDigitalController.Warning',
            function ($event, $msg, $data, $flash = false) {

                $this->loadModel('ErrorLog');
                $log = $this->ErrorLog->newEntity();
                $log->user_id = $this->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Warning';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->ErrorLog->save($log);

                if ($flash) {
                    $this->Flash->warning(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'CobroDigitalController.Log',
            function ($event, $msg, $data) {
                
            }
        );

        EventManager::instance()->on(
            'CobroDigitalController.Notice',
            function ($event, $msg, $data) {

            }
        );
    }

    public function isAuthorized($user = null) 
    {
        if ($this->request->getParam('action') == 'getCredentials') {
            return true;
        }

        if ($this->request->getParam('action') == 'retrieves') {
            return true;
        }

        if ($this->request->getParam('action') == 'getExportDebtsCobrodigitalCard') {
            return true;
        }

        if ($this->request->getParam('action') == 'getExportDebtsAutoDebit') {
            return true;
        }

        if ($this->request->getParam('action') == 'getCards') {
            return true;
        }

        if ($this->request->getParam('action') == 'assignTransaction') {
            return true;
        }

        if ($this->request->getParam('action') == 'ticketsControl') {
            return true;
        }

        if ($this->request->getParam('action') == 'serverSideDebts') {
            return true;
        }

        if ($this->request->getParam('action') == 'serverSideCobordigitalCard') {
            return true;
        }
        
        if ($this->request->getParam('action') == 'serverSideCobrodigitalCardOther') {
            return true;
        }

        if ($this->request->getParam('action') == 'serverSideDebtsAutoDebit') {
            return true;
        }
        
        if ($this->request->getParam('action') == 'serverSideCobrodigitalAutoDebit') {
            return true;
        }

        if ($this->request->getParam('action') == 'serverSideCobrodigitalAutoDebitOther') {
            return true;
        }

        if ($this->request->getParam('action') == 'processTransactions') {
            return true;
        }
        
        if ($this->request->getParam('action') == 'generatePayment') {
            return true;
        }

        if ($this->request->getParam('action') == 'strToDeciaml') {
            return true;
        }

        if ($this->request->getParam('action') == 'enableConnection') {
            return true;
        }

        if ($this->request->getParam('action') == 'test') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow('ticketsControl');
    }

    public function index()
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');
        $paraments = $this->request->getSession()->read('paraments');
        $account_enabled = $this->Accountant->isEnabled();

        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Configuración Cobro Digital';
            $detail = "";
            $this->registerActivity($action, $detail, NULL, TRUE);

            // COBRODIGITAL BEGIN
            $payment_getway->config->cobrodigital->enabled = $this->request->getData('enabled');
            $payment_getway->config->cobrodigital->hours_execution = $this->request->getData('hours_execution');
            $payment_getway->config->cobrodigital->period = $this->request->getData('period');

            $payment_getway->config->cobrodigital->automatic = $this->request->getData('automatic');
            $payment_getway->config->cobrodigital->cash = $this->request->getData('cash');
            $payment_getway->config->cobrodigital->portal = $this->request->getData('portal');
            $payment_getway->config->cobrodigital->url = $this->request->getData('url');

            $action = 'Configuración Guardada' . PHP_EOL;
            $detail = 'Habilitar: ' . ($payment_getway->config->cobrodigital->enabled ? 'Si' : 'No') . PHP_EOL;
            $detail .= 'Horario de ejecución: ' . $payment_getway->config->cobrodigital->hours_execution . PHP_EOL;
            $detail .= 'Período: ' . $payment_getway->config->cobrodigital->period . PHP_EOL;

            if ($account_enabled) {
                $payment_getway->config->cobrodigital->account = $this->request->getData('account');
                $detail .= 'Cuenta contable: ' . $payment_getway->config->cobrodigital->account . PHP_EOL;
            }

            $this->registerActivity($action, $detail, NULL);

            $this->savePaymentGetwayParaments($payment_getway);
            
            // COBRODIGITAL END

            // PORTAL BEGIN
            // $portal = $this->request->getSession()->read('portal');
            // $portal->portal_sync = false;
            // $portal->payment_getway->cobrodigital->enabled = $this->request->getData('portal');

            // $this->savePortalParaments($portal);
            // PORTAL END
        }

        $this->loadModel('Accounts');
        $accountsTitles = $this->Accounts->find()->where(['title' => true])->order(['code' => 'ASC']);
        $accounts = $this->Accounts->find()
            ->order([
                'code' => 'ASC'
            ]);

        $this->set(compact('paraments', 'accountsTitles', 'accounts', 'payment_getway', 'account_enabled'));
        $this->set('_serialize', ['paraments', 'payment_getway']);
    }

    public function getCredentials()
    {
        if ($this->request->is(['ajax'])) {
            $payment_getway = $this->request->getSession()->read('payment_getway');
            $credentials = $payment_getway->config->cobrodigital->credentials;
            $this->set(compact('credentials'));
            $this->set('_serialize', ['credentials']); 
        }
    }

    public function addCredential()
    {
        if ($this->request->is(['post'])) {

            $action = 'Agregado de Cuenta - Cobro Digital';
            $detail = "";
            $this->registerActivity($action, $detail, NULL, TRUE);
            
            $payment_getway = $this->request->getSession()->read('payment_getway');

            $credential = new \stdClass;
            $credential->id         = time();
            $credential->name       = $this->request->getData('name');
            $credential->sid        = $this->request->getData('sid');
            $credential->idComercio = $this->request->getData('idComercio');
            $credential->enabled    = $this->request->getData('enabled') == '1' ? TRUE : FALSE;
            $credential->card       = $this->request->getData('card') == '1' ? TRUE : FALSE;
            $credential->auto_debit = $this->request->getData('auto_debit') == '1' ? TRUE : FALSE;

            $payment_getway->config->cobrodigital->credentials[] = $credential;

            //se guarda los cambios
            $this->savePaymentGetwayParaments($payment_getway);

            $action = 'Cuenta Guardada' . PHP_EOL;
            $detail = 'Habilitar: ' . ($credential->enabled ? 'Si' : 'No') . PHP_EOL;
            $detail .= 'Nombre: ' . $credential->name . PHP_EOL;
            $detail .= 'SID: ' . $credential->sid . PHP_EOL;
            $detail .= 'ID Comercio: ' . $credential->idComercio . PHP_EOL;
            $detail .= 'Tarjeta de Cobranza: ' . ($credential->card ? 'Si' : 'No') . PHP_EOL;
            $detail .= 'Débito Automático Banco: ' . ($credential->auto_debit ? 'Si' : 'No') . PHP_EOL;
            $this->registerActivity($action, $detail, NULL);

            // PORTAL BEGIN

            // $portal = $this->request->getSession()->read('portal');
            // $portal->payment_getway->cobrodigital->credentials[] = $credential;
            // $portal->portal_sync = false;

            // $this->savePortalParaments($payment_getway);

            // PORTAL END
        }
        return $this->redirect(['action' => 'index']);
    }

    public function editCredential()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Edición de Cuenta - Cobro Digital';
            $detail = "";
            $this->registerActivity($action, $detail, NULL, TRUE);

            $payment_getway = $this->request->getSession()->read('payment_getway');

            foreach ($payment_getway->config->cobrodigital->credentials as $credential) {
                if ($credential->id == $this->request->getData('id')) {
                    $credential->name               = $this->request->getData('name');
                    $credential->sid                = $this->request->getData('sid');
                    $credential->idComercio         = $this->request->getData('idComercio');
                    $credential->enabled            = $this->request->getData('enabled') == '1' ? TRUE : FALSE;
                    $credential->card               = $this->request->getData('card') == '1' ? TRUE : FALSE;
                    $credential->auto_debit         = $this->request->getData('auto_debit') == '1' ? TRUE : FALSE;
                    break;
                }
            }
            //se guarda los cambios
            $this->savePaymentGetwayParaments($payment_getway);

            $action = 'Cuenta Guardada' . PHP_EOL;
            $detail = 'Habilitar: ' . ($credential->enabled ? 'Si' : 'No') . PHP_EOL;
            $detail .= 'Nombre: ' . $credential->name . PHP_EOL;
            $detail .= 'SID: ' . $credential->sid . PHP_EOL;
            $detail .= 'ID Comercio: ' . $credential->idComercio . PHP_EOL;
            $detail .= 'Tarjeta de Cobranza: ' . ($credential->card ? 'Si' : 'No') . PHP_EOL;
            $detail .= 'Débito Automático Banco: ' . ($credential->auto_debit ? 'Si' : 'No') . PHP_EOL;
            $this->registerActivity($action, $detail, NULL);

            // PORTAL BEGIN

            // $portal = $this->request->getSession()->read('portal');

            // foreach ($portal->payment_getway->cobrodigital->credentials as $credential) {
            //     if ($credential->id == $this->request->getData('id')) {
            //         $credential->name       = $this->request->getData('name');
            //         $credential->sid        = $this->request->getData('sid');
            //         $credential->idComercio = $this->request->getData('idComercio');
            //         $credential->enabled    = $this->request->getData('enabled') == '1' ? true : false;
            //         $portal->portal_sync = false;
            //         break;
            //     }
            // }

            // $this->savePortalParaments($portal);
            //PORTAL END

            return $this->redirect(['action' => 'index']);
        }
    }

    public function uploadCards()
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');

        if ($payment_getway->config->cobrodigital->enabled) {

            if (sizeof($payment_getway->config->cobrodigital->credentials) > 0) {

                if ($this->request->is('post')) {

                    $error = $this->isReloadPage();

                    if (!$error) {

                        if ($_FILES['csv']) {

                            $handle = fopen($_FILES['csv']['tmp_name'], "r");
                            $this->loadModel('CobrodigitalAccounts');
                            $gut_count = 0;
                            $no_gut = 0;
                            $count = 0;
                            $repeat = 0;

                            while ($data = fgetcsv($handle, 999999, ";")) {

                                if ($count != 0) {

                                    $card = $this->CobrodigitalAccounts->find()->where(['barcode' => $data[1]])->first();

                                    if (empty($card)) {

                                        $card = $this->CobrodigitalAccounts->newEntity();
                                        $id_comercio = "";

                                        foreach ($payment_getway->config->cobrodigital->credentials as $credential) {
                                            if ($credential->id == $this->request->getData('credential')) {
                                                $id_comercio = $credential->idComercio;
                                            }
                                        }

                                        $card_data = [
                                            'electronic_code'   => trim($data[0]),
                                            'barcode'           => trim($data[1]),
                                            'used'              => false,
                                            'deleted'           => false,
                                            'id_comercio'       => $id_comercio,
                                            'payment_getway_id' => 99
                                        ];

                                        $cardEntity = $this->CobrodigitalAccounts->patchEntity($card, $card_data);

                                        if ($this->CobrodigitalAccounts->save($cardEntity)) {
                                            $gut_count++;
                                        } else {
                                            $no_gut++;
                                            $file_path = LOGS . 'cd_error_import_card.log';
                                            $main_error = "Error al guardar.";
                                            $message = "Mensaje: " . $main_error . "\n";
                                            $message .= "columna 1: " . $data[0] . "\n";
                                            $message .= "columna 2: " . $data[1] . "\n";
                                            $message .= "objeto: " . json_encode($card->getErrors()) . "\n\n";

                                            file_put_contents($file_path, $message, FILE_APPEND | LOCK_EX);
                                        }
                                    } else {
                                        $repeat++;
                                    }
                                }
                                $count++;
                            }
                            $this->Flash->success(__('Cantidad de tarjetas cargadas correctamente: ' . $gut_count . '\n' . 'Cantidad de tarjetas no cargadas: ' . $no_gut . '\n' . 'Cantidad de tarjetas previamente cargadas: ' . $repeat));
                        }
                    } else {

                        $this->Flash->warning(__('La misma acción se intento realizar varias veces.'));
                        return $this->redirect(['action' => 'uploadCards']);
                    }
                }
            } else {
                $this->Flash->warning('Debe cargar al menos una cuenta de Cobro digital');
                return $this->redirect(['controller' => 'PaymentMethods', 'action' => 'index']);
            }
        } else {
            $this->Flash->warning('Debe habilitar Cobro digital');
            return $this->redirect(['controller' => 'PaymentMethods', 'action' => 'index']);
        }
        $this->set(compact('payment_getway'));
        $this->set('_serialize', ['payment_getway']);
    }

    public function cards()
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');

        if ($payment_getway->config->cobrodigital->enabled) {

            if (sizeof($payment_getway->config->cobrodigital->credentials) < 1) {

                $this->Flash->warning('Debe cargar al menos una cuenta de Cobro digital');
                return $this->redirect(['controller' => 'PaymentMethods', 'action' => 'index']);
            }
        } else {
            $this->Flash->warning('Debe habilitar Cobro digital');
            return $this->redirect(['controller' => 'PaymentMethods', 'action' => 'index']);
        }

        $id_comercios = [];
        foreach ($payment_getway->config->cobrodigital->credentials as $credential) {
            $id_comercios[$credential->idComercio] = '(' . $credential->idComercio . ') ' . $credential->name;
        }

        $this->set(compact('id_comercios'));
        $this->set('_serialize', ['id_comercios']);
    }

    public function retrieves()
    {
        if ($this->request->is('ajax')) { //Ajax Detection 

            $payment_getway = $this->request->getSession()->read('payment_getway');
            $this->loadModel('CobrodigitalAccounts');

            $payment_getway_id = $this->request->getQuery()['payment_getway_id'];
            $used = $this->request->getQuery()['used'];
            $deleted = $this->request->getQuery()['deleted'];
            $date_exported = $this->request->getQuery()['date_exported'] ? $this->request->getQuery()['date_exported'] : NULL;

            // $ids = [];
            // foreach ($payment_getway->config->cobrodigital->credentials as $credential) {
            //     if ($credential->enabled) {
            //         array_push($ids, $credential->idComercio);
            //     }
            // }

            $where = [
                'CobrodigitalAccounts.deleted'           => $deleted,
                //'CobrodigitalAccounts.id_comercio IN'    => $ids,
                'CobrodigitalAccounts.date_exported IS'  => $date_exported,
                'CobrodigitalAccounts.payment_getway_id' => $payment_getway_id,
                'CobrodigitalAccounts.used'              => $used
            ];

            $response = new \stdClass();

            $response->draw = intval($this->request->getQuery('draw'));

            $response->recordsTotal = $this->CobrodigitalAccounts->find()->where($where)->count();

            $response->data = $this->CobrodigitalAccounts->find('ServerSideData', [
                'params' => $this->request->getQuery(),
                'where'  => $where
            ]);

            $response->recordsFiltered = $this->CobrodigitalAccounts->find('RecordsFiltered', [
                'params' => $this->request->getQuery(),
                'where'  => $where
            ]);

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function usedCards()
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');

        if ($payment_getway->config->cobrodigital->enabled) {

            if (sizeof($payment_getway->config->cobrodigital->credentials) < 1) {

                $this->Flash->warning('Debe cargar al menos una cuenta de Cobro digital');
                return $this->redirect(['controller' => 'PaymentMethods', 'action' => 'index']);
            }
        } else {
            $this->Flash->warning('Debe habilitar Cobro digital');
            return $this->redirect(['controller' => 'PaymentMethods', 'action' => 'index']);
        }

        $id_comercios = [];
        foreach ($payment_getway->config->cobrodigital->credentials as $credential) {
            $id_comercios[$credential->idComercio] = '(' . $credential->idComercio . ') ' . $credential->name;
        }

        $this->set(compact('id_comercios'));
        $this->set('_serialize', ['id_comercios']);
    }

    public function deletedCards()
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');

        if ($payment_getway->config->cobrodigital->enabled) {

            if (sizeof($payment_getway->config->cobrodigital->credentials) < 1) {

                $this->Flash->warning('Debe cargar al menos una cuenta de Cobro digital');
                return $this->redirect(['controller' => 'PaymentMethods', 'action' => 'index']);
            }
        } else {
            $this->Flash->warning('Debe habilitar Cobro digital');
            return $this->redirect(['controller' => 'PaymentMethods', 'action' => 'index']);
        }

        $id_comercios = [];
        foreach ($payment_getway->config->cobrodigital->credentials as $credential) {
            $id_comercios[$credential->idComercio] = '(' . $credential->idComercio . ') ' . $credential->name;
        }

        $this->set(compact('id_comercios'));
        $this->set('_serialize', ['id_comercios']);
    }

    public function deleteCard($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $this->loadModel('CobrodigitalAccounts');
        $this->loadModel('CustomersAccounts');

        $customer_account = $this->CustomersAccounts
            ->find()
            ->where([
                'account_id' => $id
            ])->first();

        if ($customer_account) {
            $this->CustomersAccounts->delete($customer_account);
        }

        $card = $this->CobrodigitalAccounts->get($id);

        $customer_code = $card->customer_code;
        $barcode = $card->barcode;
        $electronic_code = $card->electronic_code;
        $id_comercio = $card->id_comercio;

        $card->used = FALSE;
        $card->customer_code = NULL;
        $card->date_exported = NULL;
        $card->deleted = TRUE;

        if ($this->CobrodigitalAccounts->save($card)) {

            $detail = "";
            $detail .= 'Cód. Barra: ' . $barcode . PHP_EOL;
            $detail .= 'Cód. Electrónico: ' . $electronic_code . PHP_EOL;
            $detail .= 'ID Comercio: ' . $id_comercio . PHP_EOL;
            $action = 'Eliminación de Tarjeta de Cobranza - Cobro Digital';
            $this->registerActivity($action, $detail, $customer_code, TRUE);
            
            $this->Flash->success(__('Se ha eliminado la Tarjeta correctamente.'));
        } else {
            $this->Flash->error(__('No se ha eliminado la Tarjeta. por favor, intente nuevamente.'));
        }

        return $this->redirect(['action' => 'cards']);
    }

    public function restoreCard($id = null)
    {
        $this->loadModel('CobrodigitalAccounts');

        $card = $this->CobrodigitalAccounts->get($id, [
            'contain' => []
        ]);

        $data = $this->request->getData();
        $data['deleted'] = FALSE;

        $card = $this->CobrodigitalAccounts->patchEntity($card, $data);
        $card->asigned = Time::now();

        if ($this->CobrodigitalAccounts->save($card)) {

            $customer_code = $card->customer_code;
            $barcode = $card->barcode;
            $electronic_code = $card->electronic_code;
            $id_comercio = $card->id_comercio;

            $detail = "";
            $detail .= 'Cód. Barra: ' . $barcode . PHP_EOL;
            $detail .= 'Cód. Electrónico: ' . $electronic_code . PHP_EOL;
            $detail .= 'ID Comercio: ' . $id_comercio . PHP_EOL;
            $action = 'Restauración de Tarjeta de Cobranza - Cobro Digital';
            $this->registerActivity($action, $detail, $customer_code, TRUE);

            $this->Flash->success(__('Tarjeta Restaurada.'));
        } else {
            $this->Flash->error(__('Error al restaurar la Tarjeta.'));
        }

        return $this->redirect(['action' => 'deletedCards']);
    }

    public function freedCard($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $this->loadModel('CustomersAccounts');
        $paraments = $this->request->getSession()->read('paraments');
        $data = $this->request->getData();

        $customer_account = $this->CustomersAccounts
            ->find()
            ->where([
                'customer_code'     => $data['customer_code'],
                'account_id'        => $id,
                'payemnt_getway_id' => 99
            ])->first();

        if ($this->CustomersAccounts->delete($customer_account)) {

            $this->loadModel('CobrodigitalAccounts');

            $card = $this->CobrodigitalAccounts->get($id);

            $customer_code = $card->customer_code;
            $barcode = $card->barcode;
            $electronic_code = $card->electronic_code;
            $id_comercio = $card->id_comercio;

            $card->used = FALSE;
            $card->deleted = FALSE;
            $card->customer_code = NULL;
            $card->date_exported = NULL;

            if ($this->CobrodigitalAccounts->save($card)) {

                $detail = "";
                $detail .= 'Cód. Barra: ' . $barcode . PHP_EOL;
                $action = 'Liberación de Tarjeta de Cobranza - Cobro Digital';
                $this->registerActivity($action, $detail, $customer_code, TRUE);

                if ($paraments->gral_config->billing_for_service) {

                    $this->loadModel('Customers');
                    $customer = $this->Customers->get($customer_code, [
                        'contain' => ['Connections']
                    ]);

                    if (sizeof($customer->connections) > 0) {

                        $this->loadModel('Connections');

                        foreach ($customer->connections as $connection) {
                            $connection->cobrodigital_card = FALSE;
                            $this->Connections->save($customer);
                        }
                    }
                }
                $this->Flash->success(__('Se ha liberado la Tarjeta correctamente.'));
            } else {
                $this->Flash->error(__('No se ha liberado la Tarjeta. por favor, intente nuevamente.'));
            }
        } else {
            $this->Flash->error(__('No se ha liberado la Tarjeta. por favor, intente nuevamente.'));
        }

        return $this->redirect(['action' => 'cards']);
    }

    public function getExportDebtsCobrodigitalCard()
    {
        if ($this->request->is('ajax')) { //Ajax Detection

            $response = new \stdClass();
            $response->draw = intval($this->request->getQuery('draw'));
            $response->recordsTotal = 0;
            $response->data = [];
            $response->recordsFiltered = 0;

            $response = $this->serverSideDebts($response, $this->request->getQuery());

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function serverSideDebts($response, $params)
    {
        $paraments = $this->request->getSession()->read('paraments');
        $payment_getway_id = $params['payment_getway_id'];

        $this->loadModel('CobrodigitalAccounts');
        $customer_codes = $this->CobrodigitalAccounts
            ->find('list')
            ->select([
                'customer_code'
            ])
            ->contain([
                'Customers'
            ])
            ->where([
                'customer_code IS NOT'         => NULL,
                'CobrodigitalAccounts.deleted' => FALSE,
                'payment_getway_id'            => $payment_getway_id
            ])->toArray();

        if (sizeof($customer_codes) > 0) {

            if ($paraments->gral_config->billing_for_service) {
                $response = $this->serverSideCobordigitalCard($response, $params, $customer_codes);
            }

            $response = $this->serverSideCobrodigitalCardOther($response, $params, $customer_codes);

            switch ($params['order'][0]['column']) {
                case 1:
                    //date
                    if ($params['order'][0]['dir'] == 'asc') {
                        //asc
                        $order = function($a, $b)
                        {
                              if ($a->date < $b->date) {
                                  return -1;
                              } else if ($a->date > $b->date) {
                                  return 1;
                              } else {
                                  return 0;
                              }
                        };
                    } else {
                        //desc
                        $order = function($a, $b)
                        {
                            if ($a->date > $b->date) {
                                return -1;
                            } else if ($a->date < $b->date) {
                                return 1;
                            } else {
                                return 0;
                            }
                        };
                    }
                    break;
                case 3:
                    //num
                    if ($params['order'][0]['dir'] == 'asc') {
                        //asc
                        $order = function($a, $b)
                        {
                              if ($a->num < $b->num) {
                                  return -1;
                              } else if ($a->num > $b->num) {
                                  return 1;
                              } else {
                                  return 0;
                              }
                        };
                    } else {
                        //desc
                        $order = function($a, $b)
                        {
                            if ($a->num > $b->num) {
                                return -1;
                            } else if ($a->num < $b->num) {
                                return 1;
                            } else {
                                return 0;
                            }
                        };
                    }
                    break;
                case 4:
                    //date_start
                    if ($params['order'][0]['dir'] == 'asc') {
                        //asc
                        $order = function($a, $b)
                        {
                              if ($a->date_start < $b->date_start) {
                                  return -1;
                              } else if ($a->date_start > $b->date_start) {
                                  return 1;
                              } else {
                                  return 0;
                              }
                        };
                    } else {
                        //desc
                        $order = function($a, $b)
                        {
                            if ($a->date_start > $b->date_start) {
                                return -1;
                            } else if ($a->date_start < $b->date_start) {
                                return 1;
                            } else {
                                return 0;
                            }
                        };
                    }
                    break;
                case 5:
                    //date_end
                    if ($params['order'][0]['dir'] == 'asc') {
                        //asc
                        $order = function($a, $b)
                        {
                              if ($a->date_end < $b->date_end) {
                                  return -1;
                              } else if ($a->date_end > $b->date_end) {
                                  return 1;
                              } else {
                                  return 0;
                              }
                        };
                    } else {
                        //desc
                        $order = function($a, $b)
                        {
                            if ($a->date_end > $b->date_end) {
                                return -1;
                            } else if ($a->date_end < $b->date_end) {
                                return 1;
                            } else {
                                return 0;
                            }
                        };
                    }
                    break;
                case 6:
                    //duedate
                    if ($params['order'][0]['dir'] == 'asc') {
                        //asc
                        $order = function($a, $b)
                        {
                              if ($a->duedate < $b->duedate) {
                                  return -1;
                              } else if ($a->duedate > $b->duedate) {
                                  return 1;
                              } else {
                                  return 0;
                              }
                        };
                    } else {
                        //desc
                        $order = function($a, $b)
                        {
                            if ($a->duedate > $b->duedate) {
                                return -1;
                            } else if ($a->duedate < $b->duedate) {
                                return 1;
                            } else {
                                return 0;
                            }
                        };
                    }
                    break;
                case 7:
                    //total
                    if ($params['order'][0]['dir'] == 'asc') {
                        //asc
                        $order = function($a, $b)
                        {
                              if ($a->total < $b->total) {
                                  return -1;
                              } else if ($a->total > $b->total) {
                                  return 1;
                              } else {
                                  return 0;
                              }
                        };
                    } else {
                        //desc
                        $order = function($a, $b)
                        {
                            if ($a->total > $b->total) {
                                return -1;
                            } else if ($a->total < $b->total) {
                                return 1;
                            } else {
                                return 0;
                            }
                        };
                    }
                    break;
                case 8:
                    //customer_name
                    if ($params['order'][0]['dir'] == 'asc') {
                        //asc
                        $order = function($a, $b)
                        {
                              if ($a->customer_name < $b->customer_name) {
                                  return -1;
                              } else if ($a->customer_name > $b->customer_name) {
                                  return 1;
                              } else {
                                  return 0;
                              }
                        };
                    } else {
                        //desc
                        $order = function($a, $b)
                        {
                            if ($a->customer_name > $b->customer_name) {
                                return -1;
                            } else if ($a->customer_name < $b->customer_name) {
                                return 1;
                            } else {
                                return 0;
                            }
                        };
                    }
                    break;
                case 10:
                    //customer_code
                    if ($params['order'][0]['dir'] == 'asc') {
                        //asc
                        $order = function($a, $b)
                        {
                              if ($a->customer_code < $b->customer_code) {
                                  return -1;
                              } else if ($a->customer_code > $b->customer_code) {
                                  return 1;
                              } else {
                                  return 0;
                              }
                        };
                    } else {
                        //desc
                        $order = function($a, $b)
                        {
                            if ($a->customer_code > $b->customer_code) {
                                return -1;
                            } else if ($a->customer_code < $b->customer_code) {
                                return 1;
                            } else {
                                return 0;
                            }
                        };
                    }
                    break;
            }

            usort($response->data, $order);

            $newData = [];

            foreach ($response->data as $data) {
                $newData[] = $data;
            }

            $response->data = array_reverse($newData); 
        }

        return $response;
    }

    public function serverSideCobordigitalCard($response, $params, $customer_codes)
    {
        $date_exported = $params['date_exported'];

        $where = [
            'Invoices.paid IS'              => NULL,
            'Invoices.customer_code IN'     => $customer_codes,
            'Invoices.connection_id IS NOT' => NULL,
            'Connections.cobrodigital_card' => TRUE,
        ];

        if ($date_exported) {
            $where['Invoices.cd_date_exported IS NOT'] = NULL;
        } else {
            $where['Invoices.cd_date_exported IS'] = NULL;
        }

        $this->loadModel('Invoices');

        $response->recordsTotal += $this->Invoices->find()->contain(['Connections'])->where($where)->count();

        $invoices = $this->Invoices->find('ServerSideDataCobrodigital', [
            'params' => $params,
            'where'  => $where
        ]);

        $response->recordsFiltered += $this->Invoices->find('RecordsFilteredCobrodigital', [
            'params' => $params,
            'where'  => $where
        ]);

        foreach ($invoices as $invoice) {
            $response->data[] = $invoice;
        }

        return $response;
    }

    public function serverSideCobrodigitalCardOther($response, $params, $customer_codes)
    {
        $date_exported = $params['date_exported'];

        $where = [
            'Invoices.paid IS'          => NULL,
            'Invoices.customer_code IN' => $customer_codes,
            'Invoices.connection_id IS' => NULL,
        ];

        if ($date_exported) {
            $where['Invoices.cd_date_exported IS NOT'] = NULL;
        } else {
            $where['Invoices.cd_date_exported IS'] = NULL;
        }

        $this->loadModel('Invoices');

        $response->recordsTotal += $this->Invoices->find()->where($where)->count();

        $invoices = $this->Invoices->find('ServerSideDataCobrodigital', [
            'params' => $params,
            'where'  => $where
        ]);

        $response->recordsFiltered += $this->Invoices->find('RecordsFilteredCobrodigital', [
            'params' => $params,
            'where'  => $where
        ]);

        foreach ($invoices as $invoice) {
            $response->data[] = $invoice;
        }

        return $response;
    }

    public function getExportDebtsAutoDebit()
    {
        if ($this->request->is('ajax')) { //Ajax Detection

            $response = new \stdClass();
            $response->draw = intval($this->request->getQuery('draw'));
            $response->recordsTotal = 0;
            $response->data = [];
            $response->recordsFiltered = 0;

            $response = $this->serverSideDebtsAutoDebit($response, $this->request->getQuery());

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function serverSideDebtsAutoDebit($response, $params)
    {
        $paraments = $this->request->getSession()->read('paraments');
        $payment_getway_id = $params['payment_getway_id'];
        
        $this->loadModel('CobrodigitalAccounts');
            $customer_codes = $this->CobrodigitalAccounts
                ->find('list')
                ->select([
                    'customer_code'
                ])
                ->contain([
                    'Customers'
                ])
                ->where([
                    'customer_code IS NOT'         => NULL,     
                    'CobrodigitalAccounts.deleted' => FALSE, 
                    'payment_getway_id'            => $payment_getway_id
                ])->toArray();

        if (sizeof($customer_codes) > 0) {

            if ($paraments->gral_config->billing_for_service) {
                $response = $this->serverSideCobrodigitalAutoDebit($response, $params, $customer_codes);
            }

            $response = $this->serverSideCobrodigitalAutoDebitOther($response, $params, $customer_codes);

            switch ($params['order'][0]['column']) {
                case 1:
                    //date
                    if ($params['order'][0]['dir'] == 'asc') {
                        //asc
                        $order = function($a, $b)
                        {
                              if ($a->date < $b->date) {
                                  return -1;
                              } else if ($a->date > $b->date) {
                                  return 1;
                              } else {
                                  return 0;
                              }
                        };
                    } else {
                        //desc
                        $order = function($a, $b)
                        {
                            if ($a->date > $b->date) {
                                return -1;
                            } else if ($a->date < $b->date) {
                                return 1;
                            } else {
                                return 0;
                            }
                        };
                    }
                    break;
                case 3:
                    //num
                    if ($params['order'][0]['dir'] == 'asc') {
                        //asc
                        $order = function($a, $b)
                        {
                              if ($a->num < $b->num) {
                                  return -1;
                              } else if ($a->num > $b->num) {
                                  return 1;
                              } else {
                                  return 0;
                              }
                        };
                    } else {
                        //desc
                        $order = function($a, $b)
                        {
                            if ($a->num > $b->num) {
                                return -1;
                            } else if ($a->num < $b->num) {
                                return 1;
                            } else {
                                return 0;
                            }
                        };
                    }
                    break;
                case 4:
                    //date_start
                    if ($params['order'][0]['dir'] == 'asc') {
                        //asc
                        $order = function($a, $b)
                        {
                              if ($a->date_start < $b->date_start) {
                                  return -1;
                              } else if ($a->date_start > $b->date_start) {
                                  return 1;
                              } else {
                                  return 0;
                              }
                        };
                    } else {
                        //desc
                        $order = function($a, $b)
                        {
                            if ($a->date_start > $b->date_start) {
                                return -1;
                            } else if ($a->date_start < $b->date_start) {
                                return 1;
                            } else {
                                return 0;
                            }
                        };
                    }
                    break;
                case 5:
                    //date_end
                    if ($params['order'][0]['dir'] == 'asc') {
                        //asc
                        $order = function($a, $b)
                        {
                              if ($a->date_end < $b->date_end) {
                                  return -1;
                              } else if ($a->date_end > $b->date_end) {
                                  return 1;
                              } else {
                                  return 0;
                              }
                        };
                    } else {
                        //desc
                        $order = function($a, $b)
                        {
                            if ($a->date_end > $b->date_end) {
                                return -1;
                            } else if ($a->date_end < $b->date_end) {
                                return 1;
                            } else {
                                return 0;
                            }
                        };
                    }
                    break;
                case 6:
                    //duedate
                    if ($params['order'][0]['dir'] == 'asc') {
                        //asc
                        $order = function($a, $b)
                        {
                              if ($a->duedate < $b->duedate) {
                                  return -1;
                              } else if ($a->duedate > $b->duedate) {
                                  return 1;
                              } else {
                                  return 0;
                              }
                        };
                    } else {
                        //desc
                        $order = function($a, $b)
                        {
                            if ($a->duedate > $b->duedate) {
                                return -1;
                            } else if ($a->duedate < $b->duedate) {
                                return 1;
                            } else {
                                return 0;
                            }
                        };
                    }
                    break;
                case 7:
                    //total
                    if ($params['order'][0]['dir'] == 'asc') {
                        //asc
                        $order = function($a, $b)
                        {
                              if ($a->total < $b->total) {
                                  return -1;
                              } else if ($a->total > $b->total) {
                                  return 1;
                              } else {
                                  return 0;
                              }
                        };
                    } else {
                        //desc
                        $order = function($a, $b)
                        {
                            if ($a->total > $b->total) {
                                return -1;
                            } else if ($a->total < $b->total) {
                                return 1;
                            } else {
                                return 0;
                            }
                        };
                    }
                    break;
                case 8:
                    //customer_name
                    if ($params['order'][0]['dir'] == 'asc') {
                        //asc
                        $order = function($a, $b)
                        {
                              if ($a->customer_name < $b->customer_name) {
                                  return -1;
                              } else if ($a->customer_name > $b->customer_name) {
                                  return 1;
                              } else {
                                  return 0;
                              }
                        };
                    } else {
                        //desc
                        $order = function($a, $b)
                        {
                            if ($a->customer_name > $b->customer_name) {
                                return -1;
                            } else if ($a->customer_name < $b->customer_name) {
                                return 1;
                            } else {
                                return 0;
                            }
                        };
                    }
                    break;
                case 10:
                    //customer_code
                    if ($params['order'][0]['dir'] == 'asc') {
                        //asc
                        $order = function($a, $b)
                        {
                              if ($a->customer_code < $b->customer_code) {
                                  return -1;
                              } else if ($a->customer_code > $b->customer_code) {
                                  return 1;
                              } else {
                                  return 0;
                              }
                        };
                    } else {
                        //desc
                        $order = function($a, $b)
                        {
                            if ($a->customer_code > $b->customer_code) {
                                return -1;
                            } else if ($a->customer_code < $b->customer_code) {
                                return 1;
                            } else {
                                return 0;
                            }
                        };
                    }
                    break;
            }

            usort($response->data, $order);

            $newData = [];

            foreach ($response->data as $data) {
                $newData[] = $data;
            }

            $response->data = array_reverse($newData); 
        }

        return $response;
    }

    public function serverSideCobrodigitalAutoDebit($response, $params, $customer_codes)
    {
        if ($this->request->is('ajax')) { //Ajax Detection

            $date_exported = $params['date_exported'];

            $where = [
                'Invoices.paid IS'                    => NULL,
                'Invoices.customer_code IN'           => $customer_codes,
                'Invoices.connection_id IS NOT'       => NULL,
                'Connections.cobrodigital_auto_debit' => TRUE
            ];

            if ($date_exported) {
                $where['Invoices.cd_auto_debit_date_exported IS NOT'] = NULL;
            } else {
                $where['Invoices.cd_auto_debit_date_exported IS'] = NULL;
            }

            $this->loadModel('Invoices');

            $response->recordsTotal += $this->Invoices->find()->contain(['Connections'])->where($where)->count();

            $invoices = $this->Invoices->find('ServerSideDataCobrodigital', [
                'params' => $params,
                'where'  => $where
            ]);

            $response->recordsFiltered += $this->Invoices->find('RecordsFilteredCobrodigital', [
                'params' => $params,
                'where'  => $where
            ]);

            foreach ($invoices as $invoice) {
                $response->data[] = $invoice;
            }

            return $response;
        }
    }

    public function serverSideCobrodigitalAutoDebitOther($response, $params, $customer_codes)
    {
        if ($this->request->is('ajax')) { //Ajax Detection

            $date_exported = $params['date_exported'];

            $where = [
                'Invoices.paid IS'          => NULL,
                'Invoices.customer_code IN' => $customer_codes,
                'Invoices.connection_id IS' => NULL,
            ];

            if ($date_exported) {
                $where['Invoices.cd_auto_debit_date_exported IS NOT'] = NULL;
            } else {
                $where['Invoices.cd_auto_debit_date_exported IS'] = NULL;
            }

            $this->loadModel('Invoices');

            $response->recordsTotal += $this->Invoices->find()->where($where)->count();

            $invoices = $this->Invoices->find('ServerSideDataCobrodigital', [
                'params' => $params,
                'where'  => $where
            ]);

            $response->recordsFiltered += $this->Invoices->find('RecordsFilteredCobrodigital', [
                'params' => $params,
                'where'  => $where
            ]);

            foreach ($invoices as $invoice) {
                $response->data[] = $invoice;
            }

            return $response;
        }
    }

    public function exportDebts()
    {
        if (!$this->request->is('xlsx')) {

            $payment_getway = $this->request->getSession()->read('payment_getway');

            if ($payment_getway->config->cobrodigital->enabled) {

                if (sizeof($payment_getway->config->cobrodigital->credentials) < 1) {

                    $this->Flash->warning('Debe cargar al menos una cuenta de Cobro digital');
                    return $this->redirect(['controller' => 'PaymentMethods', 'action' => 'index']);
                }
            } else {
                $this->Flash->warning('Debe habilitar Cobro digital');
                return $this->redirect(['controller' => 'PaymentMethods', 'action' => 'index']);
            }

            $id_comercios = [];
            foreach ($payment_getway->config->cobrodigital->credentials as $credential) {
                $id_comercios[$credential->idComercio] = '(' . $credential->idComercio . ') ' . $credential->name;
            }

            $duedate = Time::now();
            $this->set(compact('id_comercios', 'duedate'));
        } else {

            if ($this->request->is('post')) {

                $error = $this->isReloadPage();

                if (!$error) {

                    $this->loadModel('Invoices');
                    $this->loadModel('CobrodigitalAccounts');

                    $data = json_decode($this->request->getData('data'));
                    $duedate = new Time($data->duedate);
                    $duedate = $duedate->year . str_pad($duedate->month, 2, "0", STR_PAD_LEFT) . str_pad($duedate->day, 2, "0", STR_PAD_LEFT);
                    $invoices = [];
                    $invoices[] = [
                        'electronic_code',
                        'barcode', 
                        'name',
                        'id', // customer_code
                        'importe',
                        'vencimiento'
                    ];

                    foreach ($data->ids as $id) {

                        $invoice = $this->Invoices->get($id);

                        $cobrodigital_account = $this->CobrodigitalAccounts
                            ->find()
                            ->where([
                                'CobrodigitalAccounts.customer_code' => $invoice->customer_code,
                                'CobrodigitalAccounts.deleted'       => FALSE,
                                'payment_getway_id'                  => 99
                            ])->first();

                        $total = explode('.', strval($invoice->total));
                        if (count($total) == 2) {
                            $total = $invoice->total;
                        } else {
                            $total = $total[0];
                        }

                        if (array_key_exists($invoice->customer_code, $invoices)) {
                            $invoices[$invoice->customer_code][4] += $total;
                        } else {
                            $invoices[$invoice->customer_code] = [
                                $cobrodigital_account->electronic_code . '',
                                $cobrodigital_account->barcode,
                                $invoice->customer_name,
                                $invoice->customer_code,
                                $total,
                                $duedate
                            ];
                        }

                        $invoice->cd_date_exported = Time::now();
                        $invoice->cd_duedate = $duedate;
                        $this->Invoices->save($invoice);
                    }
                    $this->set(compact('invoices'));
                    $this->set('_serialize', ['Invoices']);
                } else {

                    $this->Flash->warning(__('La misma acción se intento realizar varias veces.'));
                    return $this->redirect(['action' => 'exportDebts']);
                }
            }
        }
    }

    public function exportAutoDebits()
    {
        if (!$this->request->is('xlsx')) {

            $payment_getway = $this->request->getSession()->read('payment_getway');

            if ($payment_getway->config->cobrodigital->enabled) {

                if (sizeof($payment_getway->config->cobrodigital->credentials) < 1) {

                    $this->Flash->warning('Debe cargar al menos una cuenta de Cobro digital');
                    return $this->redirect(['controller' => 'PaymentMethods', 'action' => 'index']);
                }
            } else {
                $this->Flash->warning('Debe habilitar Cobro digital');
                return $this->redirect(['controller' => 'PaymentMethods', 'action' => 'index']);
            }

            $id_comercios = [];
            foreach ($payment_getway->config->cobrodigital->credentials as $credential) {
                $id_comercios[$credential->idComercio] = '(' . $credential->idComercio . ') ' . $credential->name;
            }

            $duedate = Time::now();
            $this->set(compact('id_comercios', 'duedate'));
        } else {

            if ($this->request->is('post')) {

                $error = $this->isReloadPage();

                if (!$error) {

                    $this->loadModel('Invoices');
                    $this->loadModel('CobrodigitalAccounts');

                    $data = json_decode($this->request->getData('data'));
                    $duedate = new Time($data->duedate);
                    $duedate = $duedate->year . str_pad($duedate->month, 2, "0", STR_PAD_LEFT) . str_pad($duedate->day, 2, "0", STR_PAD_LEFT);
                    $invoices = [];
                    $invoices[] = [
                        'Nombre',
                        'Apellido', 
                        'Cuit',
                        //'Email',
                        'Cbu',
                        'Importe',
                        'Fecha',
                        'Concepto',
                        //'Cuotas',
                        //'Modalidad_cuotas'
                    ];

                    foreach ($data->ids as $id) {

                        $invoice = $this->Invoices->get($id);

                        $cobrodigital_account = $this->CobrodigitalAccounts
                            ->find()
                            ->where([
                                'CobrodigitalAccounts.customer_code' => $invoice->customer_code,
                                'CobrodigitalAccounts.deleted'       => FALSE,
                                'payment_getway_id'                  => 104
                            ])->first();

                        $total = explode('.', strval($invoice->total));
                        if (count($total) == 2) {
                            $total = $invoice->total;
                        } else {
                            $total = $total[0];
                        }

                        if (array_key_exists($invoice->customer_code, $invoices)) {
                            $invoices[$invoice->customer_code][5] += $total;
                        } else {
                            $invoices[$invoice->customer_code] = [
                                $cobrodigital_account->firstname,
                                $cobrodigital_account->lastname,
                                $cobrodigital_account->cuit,
                                //$cobrodigital_account->email,
                                $cobrodigital_account->cbu,
                                $total,
                                $duedate,
                                $id . '-Abono mensual Débito Automático', // concatenado id factura
                                //1,
                                //'mes',
                            ];
                        }

                        $invoice->cd_auto_debit_date_exported = Time::now();
                        $invoice->cd_duedate = $duedate;
                        $this->Invoices->save($invoice);
                    }
                    $this->set(compact('invoices'));
                    $this->set('_serialize', ['Invoices']);
                } else {

                    $this->Flash->warning(__('La misma acción se intento realizar varias veces.'));
                    return $this->redirect(['action' => 'exportAutoDebits']);
                }
            }
        }
    }

    public function exportExportedDebts()
    {
        if (!$this->request->is('xlsx')) {

            $payment_getway = $this->request->getSession()->read('payment_getway');

            if ($payment_getway->config->cobrodigital->enabled) {

                if (sizeof($payment_getway->config->cobrodigital->credentials) < 1) {

                    $this->Flash->warning('Debe cargar al menos una cuenta de Cobro digital');
                    return $this->redirect(['controller' => 'PaymentMethods', 'action' => 'index']);
                }
            } else {
                $this->Flash->warning('Debe habilitar Cobro digital');
                return $this->redirect(['controller' => 'PaymentMethods', 'action' => 'index']);
            }

            $id_comercios = [];
            foreach ($payment_getway->config->cobrodigital->credentials as $credential) {
                $id_comercios[$credential->idComercio] = '(' . $credential->idComercio . ') ' . $credential->name;
            }

            $duedate = Time::now();
            $this->set(compact('id_comercios', 'duedate'));
        } else {

            if ($this->request->is('post')) {

                $error = $this->isReloadPage();

                if (!$error) {

                    $this->loadModel('Invoices');
                    $this->loadModel('CobrodigitalAccounts');

                    $data = json_decode($this->request->getData('data'));
                    $duedate = new Time($data->duedate);
                    $duedate = $duedate->year . str_pad($duedate->month, 2, "0", STR_PAD_LEFT) . str_pad($duedate->day, 2, "0", STR_PAD_LEFT);
                    $invoices = [];
                    $invoices[] = [
                        'electronic_code',
                        'barcode', 
                        'name',
                        'id', // customer_code
                        'importe',
                        'vencimiento'
                    ];

                    foreach ($data->ids as $id) {

                        $invoice = $this->Invoices->get($id);

                        $cobrodigital_account = $this->CobrodigitalAccounts
                            ->find()
                            ->where([
                                'CobrodigitalAccounts.customer_code' => $invoice->customer_code,
                                'CobrodigitalAccounts.deleted'       => FALSE,
                                'payment_getway_id'                  => 99
                            ])->first();

                        $total = explode('.', strval($invoice->total));
                        if (count($total) == 2) {
                            $total = $invoice->total;
                        } else {
                            $total = $total[0];
                        }

                        if (array_key_exists($invoice->customer_code, $invoices)) {
                            $invoices[$invoice->customer_code][4] += $total;
                        } else {
                            $invoices[$invoice->customer_code] = [
                                $cobrodigital_account->electronic_code . '',
                                $cobrodigital_account->barcode,
                                $invoice->customer_name,
                                $invoice->customer_code,
                                $total,
                                $duedate
                            ];
                        }

                        $invoice->cd_date_exported = Time::now();
                        $invoice->cd_duedate = $duedate;
                        $this->Invoices->save($invoice);
                    }
                    $this->set(compact('invoices'));
                    $this->set('_serialize', ['Invoices']);
                } else {

                    $this->Flash->warning(__('La misma acción se intento realizar varias veces.'));
                    return $this->redirect(['action' => 'loadpayment']);
                }
            }
        }
    }

    public function exportExportedAutoDebits()
    {
        if (!$this->request->is('xlsx')) {

            $payment_getway = $this->request->getSession()->read('payment_getway');

            if ($payment_getway->config->cobrodigital->enabled) {

                if (sizeof($payment_getway->config->cobrodigital->credentials) < 1) {

                    $this->Flash->warning('Debe cargar al menos una cuenta de Cobro digital');
                    return $this->redirect(['controller' => 'PaymentMethods', 'action' => 'index']);
                }
            } else {
                $this->Flash->warning('Debe habilitar Cobro digital');
                return $this->redirect(['controller' => 'PaymentMethods', 'action' => 'index']);
            }

            $id_comercios = [];
            foreach ($payment_getway->config->cobrodigital->credentials as $credential) {
                $id_comercios[$credential->idComercio] = '(' . $credential->idComercio . ') ' . $credential->name;
            }

            $duedate = Time::now();
            $this->set(compact('id_comercios', 'duedate'));
        } else {

            if ($this->request->is('post')) {

                $error = $this->isReloadPage();

                if (!$error) {

                    $this->loadModel('Invoices');
                    $this->loadModel('CobrodigitalAccounts');

                    $data = json_decode($this->request->getData('data'));
                    $duedate = new Time($data->duedate);
                    $duedate = $duedate->year . str_pad($duedate->month, 2, "0", STR_PAD_LEFT) . str_pad($duedate->day, 2, "0", STR_PAD_LEFT);
                    $invoices = [];
                    $invoices[] = [
                        'Nombre',
                        'Apellido', 
                        'Cuit',
                        //'Email',
                        'Cbu',
                        'Importe',
                        'Fecha',
                        'Concepto',
                        //'Cuotas',
                        //'Modalidad_cuotas'
                    ];

                    foreach ($data->ids as $id) {

                        $invoice = $this->Invoices->get($id);

                        $cobrodigital_account = $this->CobrodigitalAccounts
                            ->find()
                            ->where([
                                'CobrodigitalAccounts.customer_code' => $invoice->customer_code,
                                'CobrodigitalAccounts.deleted'       => FALSE,
                                'payment_getway_id'                  => 104
                            ])->first();

                        $total = explode('.', strval($invoice->total));
                        if (count($total) == 2) {
                            $total = $invoice->total;
                        } else {
                            $total = $total[0];
                        }

                        if (array_key_exists($invoice->customer_code, $invoices)) {
                            $invoices[$invoice->customer_code][5] += $total;
                        } else {
                            $invoices[$invoice->customer_code] = [
                                $cobrodigital_account->firstname,
                                $cobrodigital_account->lastname,
                                $cobrodigital_account->cuit,
                                //$cobrodigital_account->email,
                                $cobrodigital_account->cbu,
                                $total,
                                $duedate,
                                $id . '-Abono mensual Débito Automático', // concatenado id factura
                                //1,
                                //'mes',
                            ];
                        }

                        $invoice->cd_auto_debit_date_exported = Time::now();
                        $invoice->cd_duedate = $duedate;
                        $this->Invoices->save($invoice);
                    }
                    $this->set(compact('invoices'));
                    $this->set('_serialize', ['Invoices']);
                } else {

                    $this->Flash->warning(__('La misma acción se intento realizar varias veces.'));
                    return $this->redirect(['action' => 'exportExportedAutoDebits']);
                }
            }
        }
    }

    public function getCards()
    {
        $data = [
            'status' => 400
        ];

        $msg = '';

        if ($this->request->is('ajax')) {

            $payment_getway = $this->request->getSession()->read('payment_getway');

            if ($payment_getway->config->cobrodigital->enabled) {

                if (sizeof($payment_getway->config->cobrodigital->credentials) > 0) {
                    // $ids = [];
                    // foreach ($payment_getway->config->cobrodigital->credentials as $credential) {
                    //     if ($credential->enabled && $credential->card) {
                    //         array_push($ids, $credential->idComercio);
                    //     }
                    // }

                    $cards = [];

                    //if (sizeof($ids) > 0) {
                        $this->loadModel('CobrodigitalAccounts');
                        $cards = $this->CobrodigitalAccounts->find()->where([
                            'used'              => FALSE, 
                            'deleted'           => FALSE,
                            //'id_comercio IN'    => $ids,
                            'payment_getway_id' => 99
                        ])->toArray();
                    //}

                    $data['cards'] = $cards;
                    $data['status'] = 200;

                } else {
                    $msg = 'Debe cargar al menos una cuenta de Cobro digital';
                }
            } else {
                $msg = 'Debe habilitar Cobro digital';
            }
        }
        $data['message'] = $msg;

        $this->set(compact('data'));
        $this->set('_serialize', ['data']);
    }

    public function markUsedCard()
    {
        if ($this->request->is('post')) {

            $payment_getway = $this->request->getSession()->read('payment_getway');
            $paraments = $this->request->getSession()->read('paraments');

            if ($payment_getway->config->cobrodigital->enabled) {

                if (sizeof($payment_getway->config->cobrodigital->credentials) > 0) {

                    $data = $this->request->getData();
                    $card_id = $data['barcode'];
                    $customer_code = $data['customer_code'];

                    $this->loadModel('CustomersAccounts');
                    $customers_accounts_cobrodigital_card = $this->CustomersAccounts
                        ->find()
                        ->where([
                            'customer_code'     => $customer_code,
                            'deleted'           => FALSE,
                            'payment_getway_id' => 99,
                        ]);

                    $where = [
                        'customer_code' => $customer_code,
                        'deleted'       => FALSE,
                    ];

                    $auto_debit = FALSE;
                    $customers_accounts_cobrodigital_auto_debit = [];

                    foreach ($payment_getway->methods as $pg) {
                        $config = $pg->config;
                        if ($payment_getway->config->$config->enabled) {
                            if ($pg->auto_debit) {
                                $auto_debit = TRUE;

                                if (!array_key_exists('OR', $where)) {
                                    $where['OR'] = [];
                                }

                                $where['OR'][] = [
                                    'payment_getway_id' => $pg->id
                                ];
                            }
                        }
                    }

                    if ($auto_debit) {
                        $customers_accounts_cobrodigital_auto_debit = $this->CustomersAccounts
                            ->find()
                            ->where($where)
                            ->toArray();
                    }

                    if ($customers_accounts_cobrodigital_card->count() > 0) {
                        $this->Flash->warning(__('Únicamente puede tener una Cuenta por Cliente. Este Cliente posee una Cuenta del mismo tipo activa, en caso de querer crear una nueva Cuenta deberá "deshabilitar" la actual Cuenta activa del Cliente'));
                        return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
                    }

                    if (sizeof($customers_accounts_cobrodigital_auto_debit) > 0) {
                        $this->Flash->warning(__('El Cliente tiene una Cuenta de Débito Automático activa, esto inhabilita la creación de otra Cuenta, para poder crear otras Cuentas deberá "deshabilitar" dicha Cuenta de Débito Automático.'));
                        return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
                    }

                    $this->loadModel('CobrodigitalAccounts');

                    $cobrodigital_account = $this->CobrodigitalAccounts
                        ->find()
                        ->where([
                            'customer_code'     => $customer_code,
                            'payment_getway_id' => 99,
                            'deleted'           => FALSE
                        ]);

                    if ($cobrodigital_account->count() > 0) {
                        $this->Flash->warning(__('Únicamente puede tener una Tarjeta de Cobranza de Cobro Digital por Cliente.'));
                        return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
                    }

                    $card = $this->CobrodigitalAccounts->get($card_id);
                    $card->used = TRUE;
                    $card->customer_code = $customer_code;
                    $card->asigned = Time::now();

                    if ($this->CobrodigitalAccounts->save($card)) {

                        $barcode = $card->barcode;
                        $electronic_code = $card->electronic_code;
                        $id_comercio = $card->id_comercio;

                        $detail = "";
                        $detail .= 'Cód. Barra: ' . $barcode . PHP_EOL;
                        $detail .= 'Cód. Electrónico: ' . $electronic_code . PHP_EOL;
                        $detail .= 'ID Comercio: ' . $id_comercio . PHP_EOL;
                        $action = 'Asignación de Tarjeta de Cobranza - Cobro Digital';
                        $this->registerActivity($action, $detail, $customer_code, TRUE);

                        $this->Flash->success(__('Tarjeta asiganada correctamente.'));

                        $this->loadModel('CustomersAccounts');
                        $cutomer_account = $this->CustomersAccounts->newEntity();
                        $cutomer_account->customer_code = $customer_code;
                        $cutomer_account->account_id = $card_id;
                        $cutomer_account->payment_getway_id = 99;
                        $this->CustomersAccounts->save($cutomer_account);

                        if ($paraments->gral_config->billing_for_service) {

                            $this->loadModel('Customers');
                            $customer = $this->Customers->get($customer_code, [
                                'contain' => ['Connections']
                            ]);

                            if (sizeof($customer->connections) > 0) {

                                $this->loadModel('Connections');

                                foreach ($customer->connections as $connection) {
                                    $connection->cobrodigital_card = TRUE;
                                    $this->Connections->save($connection);
                                }
                            }
                        }
                    }

                } else {
                    $this->Flash->warning(__('Debe cargar al menos una cuenta de Cobro digital.'));
                }
            } else {
                $this->Flash->warning(__('Debe habilitar Cobro digital.'));
            }
        }

        return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
    }

    public function createAutoDebitAccount()
    {
        if ($this->request->is('post')) {

            $payment_getway = $this->request->getSession()->read('payment_getway');
            $paraments = $this->request->getSession()->read('paraments');

            if ($payment_getway->config->cobrodigital->enabled) {

                if (sizeof($payment_getway->config->cobrodigital->credentials) > 0) {

                    $data = $this->request->getData();

                    $customer_code = $data['customer_code'];
                    $id_comercio = $data['id_comerciox'];
                    $firstname = $data['cd-firstname'];
                    $lastname = $data['cd-lastname'];
                    $email = $data['cd-email'];
                    $cuit = $data['cd-cuit'];
                    $cbu = $data['cd-cbu'];

                    $this->loadModel('CustomersAccounts');
                    $customers_accounts = $this->CustomersAccounts
                        ->find()
                        ->where([
                            'customer_code' => $customer_code,
                            'deleted'       => FALSE,
                        ]);

                    if ($customers_accounts->count() > 0) {
                        $this->Flash->warning(__('El Cliente tiene una Cuenta, esto inhabilita la creación de la Cuenta, para poder una Cuenta de Débito Automático deberá "deshabilitar" dicha Cuenta.'));
                        return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
                    }

                    $this->loadModel('CobrodigitalAccounts');

                    $cobrodigial_account = $this->CobrodigitalAccounts->newEntity();
                    $cobrodigial_account->customer_code = $customer_code;
                    $cobrodigial_account->id_comercio = $id_comercio;
                    $cobrodigial_account->firstname = $firstname;
                    $cobrodigial_account->lastname = $lastname;
                    $cobrodigial_account->email = $email;
                    $cobrodigial_account->cuit = $cuit;
                    $cobrodigial_account->cbu = $cbu;
                    $cobrodigial_account->payment_getway_id = 104;

                    if ($this->CobrodigitalAccounts->save($cobrodigial_account)) {

                        $detail = "";
                        $detail .= 'Nombre: ' . $firstname . PHP_EOL;
                        $detail .= 'Apellido: ' . $lastname . PHP_EOL;
                        $detail .= 'CUIT: ' . $cuit . PHP_EOL;
                        $detail .= 'CBU: ' . $cbu . PHP_EOL;
                        $detail .= 'ID Comercio: ' . $id_comercio . PHP_EOL;
                        $action = 'Creación de Cuenta Débito Automático Banco - Cobro Digital';
                        $this->registerActivity($action, $detail, $customer_code, TRUE);

                        $this->Flash->success(__('Se ha creado la Cuenta correctamente.'));

                        $cutomer_account = $this->CustomersAccounts->newEntity();
                        $cutomer_account->customer_code = $customer_code;
                        $cutomer_account->account_id = $cobrodigial_account->id;
                        $cutomer_account->payment_getway_id = 104;
                        $this->CustomersAccounts->save($cutomer_account);

                        if ($paraments->gral_config->billing_for_service) {

                            $this->loadModel('Customers');
                            $customer = $this->Customers->get($customer_code, [
                                'contain' => ['Connections']
                            ]);

                            if (sizeof($customer->connections) > 0) {

                                $this->loadModel('Connections');

                                foreach ($customer->connections as $connection) {
                                    $connection->cobrodigital_auto_debit = TRUE;
                                    $this->Connections->save($connection);
                                }
                            }
                        }
                    }

                } else {
                    $this->Flash->warning(__('Debe cargar al menos una Cuenta de Cobro digital.'));
                }
            } else {
                $this->Flash->warning(__('Debe habilitar Cobro digital.'));
            }
        }

        return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
    }

    public function assignTransaction()
    {
        //ajax Detection
        if ($this->request->is('ajax')) {

            $transaction_id = $this->request->input('json_decode')->transaction_id;
            $customer_code = $this->request->input('json_decode')->customer_code;

            $response = [
                'type'    => "error",
                'message' => "No se ha asignado el pago al cliente.",
                'error'   => true
            ];

            $this->loadModel('Customers');
            $customer = $this->Customers
                ->find()
                ->where([
                    'code' => $customer_code
                ])->first();

            if ($customer) {

                $this->loadModel('CobrodigitalTransactions');
                $cobrodigital_transaction = $this->CobrodigitalTransactions
                    ->find()
                    ->where([
                        'id' => $transaction_id
                    ])->first();

                $cobrodigital_account = NULL;

                $this->loadModel('CobrodigitalAccounts');
                $cobrodigital_account = $this->CobrodigitalAccounts
                    ->find()
                    ->contain([
                        'Customers'
                    ])
                    ->where([
                        'customer_code' => $customer_code
                    ])->first();

                if ($cobrodigital_transaction) {

                    if ($this->processTransactions($customer, $cobrodigital_account, $cobrodigital_transaction)) {

                        $payment_getway_name = "Cobro Digital";
                        $data = "";
                        switch ($cobrodigital_account->payment_getway_id) {
                            case 99:
                                $payment_getway_name = "Tarjeta de Cobranza";
                                $data = "Cód. Barra: " . $cobrodigital_account->barcode . PHP_EOL;
                                $data .= "Cód. Electrónico: " . $cobrodigital_account->electronic_code . PHP_EOL;
                                $data .= "ID Comercio: " . $cobrodigital_account->id_comercio;
                                break;
                            case 104:
                                $payment_getway_name = "Débito Automático Banco";
                                $data = "Nombre: " . $cobrodigital_account->firstname . PHP_EOL;
                                $data .= "Apellido: " . $cobrodigital_account->lastname . PHP_EOL;
                                $data .= "CUIT: " . $cobrodigital_account->cuit . PHP_EOL;
                                $data .= "CBU: " . $cobrodigital_account->cbu . PHP_EOL;
                                $data .= "ID Comercio: " . $cobrodigital_account->id_comercio;
                                break;
                        }

                        $detail = "";
                        $detail .= 'Medio: '. $payment_getway_name . PHP_EOL;
                        $detail .= $data . PHP_EOL; 
                        $action = 'Asignación de Pago Manual - Cobro Digital';
                        $this->registerActivity($action, $detail, $customer_code, TRUE);

                        $response = [
                            'type'    => "success",
                            'message' => "Pago asignado correctamente.",
                            'error'   => false
                        ];

                        $command = ROOT . "/bin/cake debtMonthControlAfterAction $customer_code 1 > /dev/null &";
                        $result = exec($command);
                    }
                }
            }

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function ticketsControl($fromView = FALSE, $from = NULL, $to = NULL)
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');

        //CONSULTAR A COBRO DIGITAL LAS TRANSACCIONES
        $this->loadComponent('CobroDigital');

        if ($from == NULL) {
            //CONSULTA RANGO DE 4 DIAS
            $from = Time::now()->modify($payment_getway->config->cobrodigital->period)->format('Ymd'); //'d/m/Y'
        }

        if ($to == NULL) {
            $to = Time::now()->format('Ymd');
        }

        $data = [
            'code'    => 200,
            'message' => 'datos general'
        ];

        $this->loadModel('Users');
        $this->loadModel('Customers');
        $this->loadModel('CobrodigitalTransactions');
        $this->loadModel('CobrodigitalAccounts');

        $credentials_aux = $payment_getway->config->cobrodigital->credentials;
        $credentials = [];
        foreach ($credentials_aux as $credential_aux) {
            if ($credential_aux->enabled) {
                array_push($credentials, $credential_aux);
            }
        }

        $action = 'Control de Pagos Cobro Digital Cron';
        $detail = '';
        $this->registerActivity($action, $detail, NULL, TRUE);

        foreach ($credentials as $credential) {

            $result = $this->CobroDigital->consultarTransacciones($from, $to, null, null, null, $credential->idComercio, $credential->sid);

            if ($result == NULL) {

                $message = "Cobro Digital devolvio una lista vacia";

                Log::error($message, ['scope' => ['CobroDigitalController']]);

                $data[$credential->idComercio] = [
                    'code'    => 400, 
                    'message' => $message
                ];

                $data_error = new \stdClass;
                $data_error->request_data = $message;

                $event = new Event('CobroDigitalController.Error', $this, [
                    'msg'   => __('Error al consultar los pagos a Cobro Digital.'),
                    'data'  => $data_error,
                    'flash' => true
                ]);

                $this->getEventManager()->dispatch($event);

            } else {

                if ($result->ejecucion_correcta) {

                    $message = "";

                    foreach ($result->log as $l) {
                        $message .= $l . '\n';
                    }

                    Log::info($message, ['scope' => ['CobroDigitalController']]);

                    $action = 'Iniciando proceso de imputación de pagos - Cobro Digital';
                    $detail = 'ID Comercio: ' . $credential->idComercio . PHP_EOL;
                    $detail .= 'SID: ' . $credential->sid . PHP_EOL;
                    $detail .= $message . PHP_EOL;
                    $this->registerActivity($action, $detail, NULL);

                    if (isset($result->datos)) {

                        $user_sistema = 100;

                        $datos = $result->datos;

                        //ESTRUCTURA DE DATO
                        // $dato = [
                        //     'id_transaccion'   => 'id_transaccion',
                        //     'Fecha'            => 'fecha',
                        //     'Código de barras' => 'codigo_barra',
                        //     'Nro Boleta'       => 'nro_boleta', 
                        //     'Identificación'   => 'identificacion',
                        //     'Nombre'           => 'nombre',
                        //     'Info'             => 'info',
                        //     'Concepto'         => 'concepto',
                        //     'Bruto'            => 'bruto',
                        //     'Comisión'         => 'comision',
                        //     'Neto'             => 'importe_neto',
                        //     'Saldo acumulado'  => 'saldo_acumulado'
                        // ];

                        /*
                        "id_transaccion": "COOGVJV9luAgFn+SIYlP4A==",
                        "Fecha": "06/02/2020",
                        "sumaresta": "1",
                        "Codigo de barras": "73859300231028516022800000005",
                        "Nro Boleta": "285",
                        "Identificacion": "ID: 375",
                        "Nombre": "Aldo Américo Ramírez",
                        "Info": "Rapipago",
                        "Concepto": "Tarjeta de Cobranza",
                        "Bruto": "1.400,00",
                        "Comision": "-61,78",
                        "Neto": "1.338,22",
                        "Saldo acumulado": "22.484,48",
                        "Fecha_pago": "05/02/2020"
                        */

                        $register = 0;
                        $bloque_id = time();

                        $customers_codes = "";
                        $first = TRUE;

                        foreach ($datos as $dato) {

                            $trasaccion = $this->CobrodigitalTransactions
                                ->find()
                                ->select(['id_transaccion', 'estado'])
                                ->where(['id_transaccion' => $dato['id_transaccion']])->first();

                            //si la transaccion ya esta registra pasamo a la siguiente

                            if ($trasaccion) {
                                continue;
                            }

                            //sino existe la registro

                            $dato['Fecha'] = explode('/', $dato['Fecha']);
                            $dato['Fecha'] = new Time($dato['Fecha'][2] . '-' . $dato['Fecha'][1] . '-' . $dato['Fecha'][0] . Time::now()->format(' H:i:s'));

                            $Fecha_pago = NULL;

                            if (array_key_exists('Fecha_pago', $dato)) {
                                $Fecha_pago = explode('/', $dato['Fecha_pago']);
                                $Fecha_pago = new Time($Fecha_pago[2] . '-' . $Fecha_pago[1] . '-' . $Fecha_pago[0] . Time::now()->format(' H:i:s'));
                            }

                            $cobrodigital_transaction = $this->CobrodigitalTransactions->newEntity();
                            $cobrodigital_transaction->id_transaccion  = $dato['id_transaccion'];
                            $cobrodigital_transaction->fecha           = $dato['Fecha'];
                            $cobrodigital_transaction->fecha_pago      = $Fecha_pago;
                            $cobrodigital_transaction->codigo_barras   = $dato['Código de barras'];
                            $cobrodigital_transaction->identificador   = $dato['Identificación'];
                            $cobrodigital_transaction->nro_boleta      = $dato['Nro Boleta'];
                            $cobrodigital_transaction->nombre          = $dato['Nombre'];
                            $cobrodigital_transaction->info            = $dato['Info'];
                            $cobrodigital_transaction->concepto        = $dato['Concepto'] == NULL ? 'Pago por Cobro Digital' : $dato['Concepto'];
                            $cobrodigital_transaction->bruto           = $this->strToDeciaml($dato['Bruto']);
                            $cobrodigital_transaction->comision        = $this->strToDeciaml($dato['Comisión']);
                            $cobrodigital_transaction->neto            = $this->strToDeciaml($dato['Neto']);
                            $cobrodigital_transaction->saldo_acumulado = $this->strToDeciaml($dato['Saldo acumulado']);
                            $cobrodigital_transaction->estado          = 0;
                            $cobrodigital_transaction->bloque_id       = $bloque_id;

                            $cobrodigital_account = $this->CobrodigitalAccounts
                                ->find()
                                ->contain([
                                    'Customers'
                                ])
                                ->where([
                                    'barcode' => $cobrodigital_transaction->codigo_barras
                                ])->first();

                            $customerx = NULL;

                            // no coincide el codigo de barra de la transaccion con el codigo de barra de las tarjetas.
                            if (empty($cobrodigital_account)) {

                                // si no existe un cliente al cual asociar el pago de esta transaccion no se pude procesar
                                // no coincide con codigo de barra de tarjeta y no vino identificador que hace referencia al customer_code nuestro

                                // identicacion puede venir
                                // 1: ID: (VACIO)
                                // 2: ID: NUMERO
                                // 3: CUIT/CUIL NUMERO

                                $is_auto_debit = FALSE;
                                $pmc = FALSE;

                                $identificador = str_replace("ID: ", "", $cobrodigital_transaction->identificador);
                                //tener en cuenta la transaccion de debito automatica rechazada tambien
                                if (strpos($cobrodigital_transaction->info, 'Debito automático cbu') !== FALSE
                                    || strpos($cobrodigital_transaction->info, 'Rechazo de débito automático') !== FALSE) {

                                    if (strpos($identificador, 'CUIT/CUIL: ') !== FALSE) {
                                        $is_auto_debit = TRUE;
                                        $identificador = str_replace("CUIT/CUIL: ", "", $identificador);
                                    } else {
                                        $cobrodigital_transaction->comments = 'Transacción por débito automático, pero sin CUIT para identificar la cuenta del cliente.';
                                        $this->CobrodigitalTransactions->save($cobrodigital_transaction);
                                        continue;
                                    }
                                }

                                if (strpos($cobrodigital_transaction->info, 'Pagomiscuentas') !== FALSE) {

                                    $pmc = TRUE;
                                }

                                if ($identificador == "") {
                                    $cobrodigital_transaction->comments = 'No existe cliente con este código de barras y no cuenta con identificador de cliente (customer_code)';
                                    $this->CobrodigitalTransactions->save($cobrodigital_transaction);
                                    continue;
                                } else {

                                    $customerx = NULL;

                                    if ($is_auto_debit) {

                                        $cobrodigital_transaction->payment_getway_id = 104;

                                        $cobrodigital_account = $this->CobrodigitalAccounts
                                            ->find()
                                            ->contain([
                                                'Customers.Connections'
                                            ])
                                            ->where([
                                                'cuit' => $identificador
                                            ])->first();

                                        if ($cobrodigital_account) {
                                            $customerx = $cobrodigital_account->customer;
                                        }

                                        //asginar la transaccion de debito automatico rechazada al cliente correspondiente y saltar a la sgte transaccion
                                        if (strpos($cobrodigital_transaction->info, 'Rechazo de débito automático') !== FALSE) {
                                            $cobrodigital_transaction->customer_code = $customerx->code;
                                            $cobrodigital_transaction->comments = $cobrodigital_transaction->info;
                                            $this->CobrodigitalTransactions->save($cobrodigital_transaction);
                                            continue;
                                        }

                                    } else if ($pmc) {

                                        // Tratar PMC

                                        $cobrodigital_transaction->payment_getway_id = 99;

                                        $cobrodigital_account = $this->CobrodigitalAccounts
                                            ->find()
                                            ->contain([
                                                'Customers.Connections'
                                            ])
                                            ->where([
                                                'customer_code' => $identificador
                                            ])->first();

                                        if ($cobrodigital_account) {
                                            $customerx = $cobrodigital_account->customer;
                                        }
                                    } else {

                                        if ($cobrodigital_transaction->codigo_barras == NULL) {
                                            $cobrodigital_transaction->comments = 'Sin código de barras y no descartado que es Débito Automático.';
                                            $this->CobrodigitalTransactions->save($cobrodigital_transaction);
                                            continue;
                                        }

                                        if (is_numeric($identificador)) {

                                            $cobrodigital_transaction->payment_getway_id = 99;
                                            $cobrodigital_account = $this->CobrodigitalAccounts
                                                ->find()
                                                ->contain([
                                                    'Customers.Connections'
                                                ])
                                                ->where([
                                                    'customer_code' => $identificador
                                                ])->first();

                                            if ($cobrodigital_account) {
                                                $customerx = $cobrodigital_account->customer;
                                            }
                                        } else {
                                            $cobrodigital_transaction->comments = 'No existe cliente con este código de barras y no cuenta con identificador de cliente (customer_code)';
                                            $this->CobrodigitalTransactions->save($cobrodigital_transaction);
                                            continue;
                                        }
                                    }

                                    if ($customerx == NULL) {

                                        $cobrodigital_transaction->comments = 'No existe cliente con este código de barras y el identificador (customer_code) no pertenece a ningún cliente';
                                        $this->CobrodigitalTransactions->save($cobrodigital_transaction);
                                        continue;
                                    }
                                }

                            } else {
                                $cobrodigital_transaction->payment_getway_id = 99;
                                $customerx = $cobrodigital_account->customer;
                            }

                            //proceso la transaccion ....

                            if ($customerx != NULL && $cobrodigital_account != NULL) {

                                if ($first) {
                                    $customers_codes .= $customerx->code;
                                    $first = FALSE;
                                } else {
                                    $customers_codes .= '.' . $customerx->code;
                                }

                                $register += $this->processTransactions($customerx, $cobrodigital_account, $cobrodigital_transaction, $register, $user_sistema);
                            }
                        }

                        $data[$credential->idComercio] = [
                            'code'    => 200,
                            'message' => 'Pagos registrados: ' . $register
                        ];

                        $action = 'Resultado de proceso de imputación de pagos - Cobro Digital';
                        $detail = 'ID Comercio: ' . $credential->idComercio . PHP_EOL;
                        $detail .= 'SID: ' . $credential->sid . PHP_EOL;
                        $detail .= 'Pagos registrados: ' . $register . PHP_EOL;
                        $detail .= $message . PHP_EOL;
                        $this->registerActivity($action, $detail, NULL);

                    } else {
                        $message = "no hay datos";
                        Log::info($message, ['scope' => ['CobroDigitalController']]);
                        $data[$credential->idComercio] = [
                            'code'    => 200, 
                            'message' => $message
                        ];
                        $action = 'Resultado de proceso de imputación de pagos - Cobro Digital';
                        $detail = 'ID Comercio: ' . $credential->idComercio . PHP_EOL;
                        $detail .= 'SID: ' . $credential->sid . PHP_EOL;
                        $detail .= 'Resultado: no hay datos'. PHP_EOL;
                        $detail .= $message . PHP_EOL;
                        $this->registerActivity($action, $detail, NULL);
                    }
                } else {
                    $message = "";
                    foreach ($result->log as $l) {
                        $message .= $l . '\n';
                    }

                    Log::error($message, ['scope' => ['CobroDigitalController']]);
                    $data[$credential->idComercio] = [
                        'code'    => 400, 
                        'message' => $message
                    ];

                    $data_error = new \stdClass; 
                    $data_error->request_data = $message;

                    $event = new Event('CobroDigitalController.Error', $this, [
                        'msg'   => __('Error al consulta de transacciones a Cobro Digital'),
                        'data'  => $data_error,
                        'flash' => true
                    ]);

                    $this->getEventManager()->dispatch($event);
                }
            }
        }

        if ($customers_codes != "") {
            $command = ROOT . "/bin/cake debtMonthControlAfterAction $customers_codes 1 > /dev/null &";
            $result = exec($command);
        }

        if ($fromView) {
            $this->set([
                'data'       => $data,
                '_serialize' => ['data']
            ]);
        } else {
            return json_encode($data);
        }
    }

    private function processTransactions($customer, $cobrodigital_account, $cobrodigital_transaction, $register = 0, $user_sistema = 100)
    {
        $this->loadModel('CobrodigitalTransactions');
        $this->loadModel('Connections');

        //cliente con facturacion separa por servicio
        if ($customer->billing_for_service) {

            $where = [
                'customer_code'       => $customer->code,
                'Connections.deleted' => FALSE
            ];

            if ($cobrodigital_transaction->payment_getway_id == 99) {
                $where['cobrodigital_card'] = TRUE;
            } else if ($cobrodigital_transaction->payment_getway_id == 104) {
                $where['cobrodigital_auto_debit'] = TRUE;
            }

            $connections = $this->Connections
                ->find()
                ->Contain([
                    'Customers'
                ])
                ->where($where)
                ->order([
                    'Connections.debt_month' => 'DESC'
                ]);

            $bruto = $cobrodigital_transaction->bruto;

            if ($bruto == 0) {
                return 0;
            }

            $connections_size = $connections->count();
            if ($connections_size == 0) {
                $cobrodigital_transaction->customer_code = $customer->code;
                $register += $this->generatePayment($customer, $cobrodigital_transaction, $bruto, $register, $cobrodigital_account, NULL);

                return $register;
            }

            if ($connections_size == 1) {

                $connection = $connections->first();

                $cobrodigital_transaction->customer_code = $customer->code;
                $register += $this->generatePayment($customer, $cobrodigital_transaction, $bruto, $register, $cobrodigital_account, $connection->id);

                return $register;
            }

            $connections_debts = [];
            $connections_without_debts = [];

            // primer recorrido busco los que tienen deudas y los que no tienen deudas
            foreach ($connections as $key => $connection) {

                $cobrodigital_transaction->customer_code = $connection->customer_code;
                $this->CobrodigitalTransactions->save($cobrodigital_transaction);

                if ($connection->debt_month > 0) {
                    array_push($connections_debts, $connection);
                } else {
                    array_push($connections_without_debts, $connection);
                }
            }

            //array donde cargo los pagos
            $generate_payments = [];
            //bruto que voy descontando las deudas de las conexiones
            $bruto_aux = $bruto;

            // cargo en un array de pagos las conexiones que pueden pagarse
            // a partir de la conexiones con deudas
            if (sizeof($connections_debts) > 0) {

                foreach ($connections_debts as $connections_debt) {

                    // la deuda de la conexiones es menor al bruto
                    if ($connections_debt->debt_month <= $bruto_aux) {  //1er conexion 1200 <= 2300 si | 2da conn 1050 <= 1100 si

                        $bruto_aux -= $connections_debt->debt_month; //2300 - 1200 = 1100 |  1100 - 1050 = 50

                        $connections_debt->imports = [];
                        array_push($connections_debt->imports, $connections_debt->debt_month); //conn1.imports = [1200] | conn2.imports = [1050]
                        array_push($generate_payments, $connections_debt); // [conn1, conn2]

                    } else {
                        // se genera un pago a la conexion con el valor de bruto 
                        $connections_debt->imports = [];
                        array_push($connections_debt->imports, $bruto_aux);
                        $bruto_aux = 0;
                        array_push($generate_payments, $connections_debt);
                        break;
                    }
                }
            }

            // si quedo valor en bruto_aux, puede ser que no exista conexiones con deudas
            if ($bruto_aux > 0) { //50

                // debe existir conexiones sin deudas
                if (sizeof($connections_without_debts) > 0) {
                    // se asigna el pago a la primer conexion
                    // $connection_without_debt = $connections_without_debts[0];

                    $connection_without_debt_clone = new \sdtClass;
                    $connection_without_debt_clone->id = $connections_without_debts[0]->id;  

                    $connection_without_debt_clone->imports = [];
                    array_push($connection_without_debt_clone->imports, $bruto_aux);
                    $bruto_aux = 0;                 

                    array_push($generate_payments, $connection_without_debt_clone);

                } else {
                    // se asigna el pago a la primer conexion (con deuda)
                    // $connections_debt = $connections_debts[0]; //conn1 

                    $connections_debt_clone = new \sdtClass;
                    $connections_debt_clone->id = $connections_debts[0]->id;                

                    $connections_debt_clone->imports = [];
                    array_push($connections_debt_clone->imports, $bruto_aux); //conn1.imports = [50]
                    $bruto_aux = 0;

                    array_push($generate_payments, $connections_debt_clone);
                }
            }

            // genero los pagos de las conexiones que tienen deudas
            foreach ($generate_payments as $key => $generate_payment) {

                foreach ($generate_payment->imports as $key => $import) {

                    $register += $this->generatePayment($customer, $cobrodigital_transaction, $import, $register, $cobrodigital_account, $generate_payment->id);
                }
            }

        } else {

            $bruto = $cobrodigital_transaction->bruto;

            if ($bruto == 0) {
                return 0;
            }

            $cobrodigital_transaction->customer_code = $customer->code;
            $register = $this->generatePayment($customer, $cobrodigital_transaction, $bruto, $register, $cobrodigital_account);
        }

        return $register;
    }

    private function generatePayment($customer, $cobrodigital_transaction, $import, $register, $cobrodigital_account, $connection_id = NULL, $user_sistema = 100)
    {
        $this->loadModel('Payments');
        $this->loadModel('CobrodigitalTransactions');
        $payment_getway = $this->request->getSession()->read('payment_getway');
        $paraments = $this->request->getSession()->read('paraments');
        $seating = NULL;

        //si la contabilidada esta activada registro el asiento
        if ($this->Accountant->isEnabled()) {

            $seating = $this->Accountant->addSeating([
                'concept'       => 'Por Cobranza (cobrodigital)',
                'comments'      => '',
                'seating'       => NULL,
                'account_debe'  => $payment_getway->config->cobrodigital->account,
                'account_haber' => $customer->account_code,
                'value'         => $import,
                'user_id'       => $user_sistema,
                'created'       => Time::now()
            ]);
        }

        $data_payment = [
            'import'                => $import,
            'created'               => $cobrodigital_transaction->fecha,
            'cash_entity_id'        => NULL,
            'user_id'               => $user_sistema,
            'seating_number'        => $seating ? $seating->number : NULL,
            'customer_code'         => $customer->code,
            'payment_method_id'     => $cobrodigital_account->payment_getway_id, //id = 99 metodo de pago -> cobrodigital
            'concept'               => $cobrodigital_transaction->concepto,
            'connection_id'         => $connection_id,
            'number_transaction'    => $cobrodigital_transaction->id_transaccion,
            'account_id'            => $cobrodigital_account->id
        ];

        $payment = $this->Payments->newEntity();

        $payment = $this->Payments->patchEntity($payment, $data_payment);

        if ($payment) {

            if ($this->Payments->save($payment)) {

                //genero el recibo del pago
                $receipt = $this->FiscoAfipComp->receipt($payment, FALSE);

                if ($receipt) {

                    $register++;

                    $cobrodigital_transaction->comments .= ' - procesado correctamente ';
                    $cobrodigital_transaction->estado = 1;

                    if ($cobrodigital_transaction->receipt_number != NULL) {
                        $cobrodigital_transaction->receipt_number .= ',' . str_pad($receipt->pto_vta, 4, "0", STR_PAD_LEFT) . '-' . str_pad($receipt->num, 8, "0", STR_PAD_LEFT);
                        $cobrodigital_transaction->receipt_id .= ',' . $receipt->id;
                    } else {
                        $cobrodigital_transaction->receipt_number = str_pad($receipt->pto_vta, 4, "0", STR_PAD_LEFT) . '-' . str_pad($receipt->num, 8, "0", STR_PAD_LEFT);
                        $cobrodigital_transaction->receipt_id = $receipt->id;
                    }

                    $this->CobrodigitalTransactions->save($cobrodigital_transaction);

                    //habilito la conexion relacionada al serivicio que esta pagando
                    $this->enableConnection($payment, $customer);

                    $afip_codes = $this->request->getSession()->read('afip_codes');

                    $this->loadModel('Users');
                    $user = $this->Users->get($user_sistema);
                    $user_name = $user->name . ' - username: ' . $user->username;

                    $customer_name = $customer->name . ' - Código: ' . $customer->code . ' - ' . $afip_codes['doc_types'][$customer->doc_type] . ': ' . $customer->ident;
                    $customer_code = $customer->code;

                    if ($paraments->invoicing->email_emplate_receipt != "") {

                        $controllerClass = 'App\Controller\MassEmailsController';

                        $mass_email = new $controllerClass;
                        if ($mass_email->sendEmailReceipt($receipt->id, $paraments->invoicing->email_emplate_receipt)) {

                            $action = 'Envío recibo al cliente';
                            $detail = 'Cliente: ' . $customer_name . PHP_EOL;
                            $detail .= 'Usuario: ' . $user_name . PHP_EOL;

                            $detail .= '---------------------------' . PHP_EOL;

                            $this->registerActivity($action, $detail, $customer_code, TRUE);
                        } else {

                            $action = 'No envío recibo al cliente';
                            $detail = 'Cliente: ' . $customer_name . PHP_EOL;
                            $detail .= 'Usuario: ' . $user_name . PHP_EOL;

                            $detail .= '---------------------------' . PHP_EOL;

                            $this->registerActivity($action, $detail, $customer_code, TRUE);
                        }
                    }

                } else {

                    $data_error = new \stdClass; 
                    $data_error->request_data = $data_payment;

                    $event = new Event('CobroDigitalController.Error', $this, [
                        'msg' => __('Hubo un error al intentar generar el recibo. Verifique los valores cargados.'),
                        'data' => $data_error,
                        'flash' => true
                    ]);

                    $this->getEventManager()->dispatch($event);

                    Log::error($receipt->getErrors(), ['scope' => ['CobroDigitalController']]);

                    //rollback payment
                    $this->Payments->delete($payment);
                    //recibo
                    $this->FiscoAfipComp->rollbackReceipt($receipt, TRUE);
                    //rollback seating
                    //si la contabilidada esta activada registro el asientop
                    if ($this->Accountant->isEnabled()) {
                        if ($seating) {
                            $this->Accountant->deleteSeating();
                        }
                    }
                }
            } else {
                Log::error($payment->getErrors(), ['scope' => ['CobroDigitalController']]);
            }

        } else {
            Log::error($payment->getErrors(), ['scope' => ['CobroDigitalController']]);
        }
        return $register;
    }

    public function createCard()
    {
        if ($this->request->is('post')) {

            $payment_getway = $this->request->getSession()->read('payment_getway');
            $paraments = $this->request->getSession()->read('paraments');

            if ($payment_getway->config->cobrodigital->enabled) {

                if (sizeof($payment_getway->config->cobrodigital->credentials) > 0) {

                    $data = $this->request->getData();
                    $barcode = $data['barcode'];
                    $electronic_code = $data['electronic_code'];
                    $customer_code = $data['customer_code'];

                    $this->loadModel('CustomersAccounts');
                    $customers_accounts_cobrodigital_card = $this->CustomersAccounts
                        ->find()
                        ->where([
                            'customer_code'     => $customer_code,
                            'deleted'           => FALSE,
                            'payment_getway_id' => 99,
                        ]);

                    $where = [
                        'customer_code' => $customer_code,
                        'deleted'       => FALSE,
                    ];

                    $auto_debit = FALSE;
                    $customers_accounts_cobrodigital_auto_debit = [];

                    foreach ($payment_getway->methods as $pg) {
                        $config = $pg->config;
                        if ($payment_getway->config->$config->enabled) {
                            if ($pg->auto_debit) {
                                $auto_debit = TRUE;

                                if (!array_key_exists('OR', $where)) {
                                    $where['OR'] = [];
                                }

                                $where['OR'][] = [
                                    'payment_getway_id' => $pg->id
                                ];
                            }
                        }
                    }

                    if ($auto_debit) {
                        $customers_accounts_cobrodigital_auto_debit = $this->CustomersAccounts
                            ->find()
                            ->where($where)
                            ->toArray();
                    }

                    if ($customers_accounts_cobrodigital_card->count() > 0) {
                        $this->Flash->warning(__('Únicamente puede tener una Cuenta por Cliente. Este Cliente posee una Cuenta del mismo tipo activa, en caso de querer crear una nueva Cuenta deberá "deshabilitar" la actual Cuenta activa del Cliente'));
                        return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
                    }

                    if (sizeof($customers_accounts_cobrodigital_auto_debit) > 0) {
                        $this->Flash->warning(__('El Cliente tiene una Cuenta de Débito Automático activa, esto inhabilita la creación de otra Cuenta, para poder crear otras Cuentas deberá "deshabilitar" dicha Cuenta de Débito Automático.'));
                        return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
                    }

                    $this->loadModel('CobrodigitalAccounts');

                    $cobrodigital_account = $this->CobrodigitalAccounts
                        ->find()
                        ->where([
                            'customer_code'     => $customer_code,
                            'payment_getway_id' => 99,
                            'deleted'           => FALSE
                        ]);

                    if ($cobrodigital_account->count() > 0) {
                        $this->Flash->warning(__('Únicamente puede tener una Tarjeta de Cobranza de Cobro Digital por Cliente.'));
                        return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
                    }

                    $card = $this->CobrodigitalAccounts
                        ->find()
                        ->where([
                            'barcode' => $barcode
                        ])->first();

                    if ($card == NULL) {
                        $card = $this->CobrodigitalAccounts->newEntity();
                        $card->barcode = $barcode;
                        $card->electronic_code = $electronic_code;
                        $card->id_comercio = "";
                    } else if ($card->used) {
                        $this->Flash->warning(__('El código barra: ' . $barcode . '. Está siendo utilizada por una cuenta actualmente.'));
                        return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
                    }

                    $card->used = TRUE;
                    $card->customer_code = $customer_code;
                    $card->asigned = Time::now();
                    $card->payment_getway_id = 99;

                    if ($this->CobrodigitalAccounts->save($card)) {

                        $barcode = $card->barcode;
                        $electronic_code = $card->electronic_code;
                        $id_comercio = $card->id_comercio;

                        $detail = "";
                        $detail .= 'Cód. Barra: ' . $barcode . PHP_EOL;
                        $detail .= 'Cód. Electrónico: ' . $electronic_code . PHP_EOL;
                        $detail .= 'ID Comercio: ' . $id_comercio . PHP_EOL;
                        $action = 'Creación Manual de Tarjeta de Cobranza - Cobro Digital';
                        $this->registerActivity($action, $detail, $customer_code, TRUE);

                        $this->Flash->success(__('Tarjeta asiganada correctamente.'));

                        $this->loadModel('CustomersAccounts');
                        $cutomer_account = $this->CustomersAccounts->newEntity();
                        $cutomer_account->customer_code = $customer_code;
                        $cutomer_account->account_id = $card->id;
                        $cutomer_account->payment_getway_id = 99;
                        $this->CustomersAccounts->save($cutomer_account);

                        if ($paraments->gral_config->billing_for_service) {

                            $this->loadModel('Customers');
                            $customer = $this->Customers->get($customer_code, [
                                'contain' => ['Connections']
                            ]);

                            if (sizeof($customer->connections) > 0) {

                                $this->loadModel('Connections');

                                foreach ($customer->connections as $connection) {
                                    $connection->cobrodigital_card = TRUE;
                                    $this->Connections->save($connection);
                                }
                            }
                        }
                    }

                } else {
                    $this->Flash->warning(__('Debe cargar al menos una cuenta de Cobro digital.'));
                }
            } else {
                $this->Flash->warning(__('Debe habilitar Cobro digital.'));
            }
        }

        return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
    }

    private function strToDeciaml($value)
    {
       $value = str_replace('.', '', $value);
       $value = str_replace(',', '.', $value);
       return floatval($value);
    }

    private function enableConnection($payment, $customer)
    {
        $paraments = $this->request->getSession()->read('paraments');

        $this->loadModel('Connections');

        $connections = [];

        if ($customer->billing_for_service) {

             if ($payment->connection_id) {

                 $connections = $this->Connections->find()->contain(['Customers', 'Controllers', 'Services'])
                    ->where([
                        'Connections.id' => $payment->connection_id,
                        'Connections.deleted' => false,
                        'Connections.enabled' => false
                    ]);
             }

        } else {

            $connections = $this->Connections->find()->contain(['Customers', 'Controllers', 'Services'])
                ->where([
                    'Connections.customer_code' => $customer->code,
                    'Connections.deleted' => false,
                    'Connections.enabled' => false
                ]);
        }

        foreach ($connections as $connection) {

            if ($this->IntegrationRouter->enableConnection($connection, true)) {

                $connection->enabled = true;
                $connection->enabling_date = Time::now();

                if ($this->Connections->save($connection)) {

                    $this->loadModel('ConnectionsHasMessageTemplates');
                    $chmt =  $this->ConnectionsHasMessageTemplates->find()
                        ->where([
                            'connections_id' => $connection->id, 
                            'type' => 'Bloqueo'])
                        ->first();

                    if (!$this->ConnectionsHasMessageTemplates->delete($chmt)) {
                        //$this->Flash->error(__('No se pudo eliminar el aviso de bloqueo.'));
                    }

                    $customer_code = $connection->customer_code;

                    $detail = "";
                    $detail .= 'Conexión: '. $connection->created->format('d/m/Y') . ' - ' . $connection->service->name . ' - ' . $connection->address . PHP_EOL; 
                    $action = 'Habilitación de Conexión - Imputación de pago automático Cobro Digital';
                    $this->registerActivity($action, $detail, $customer_code, TRUE);
                } else {
                    //$this->Flash->error(__('No se pudo Habilitar la conexión.'));
                }
            }
        }
    }

    public function test()
    {
        if ($this->request->is('ajax')) {

            $payment_getway = $this->request->getSession()->read('payment_getway');
            $id = $this->request->input('json_decode')->id;

            $from = Time::now()->modify($payment_getway->config->cobrodigital->period)->format('Ymd'); //'d/m/Y'

            $to = Time::now()->format('Ymd');

            //CONSULTAR A COBRO DIGITAL LAS TRANSACCIONES
            $this->loadComponent('CobroDigital');

            $credentials = $payment_getway->config->cobrodigital->credentials;
            $credential_selected = null;

            foreach ($credentials as $credential) {
                if ($id == $credential->id) {
                    $credential_selected = $credential;
                }
            }

            $result = $this->CobroDigital->consultarTransacciones($from, $to, null, null, null, $credential_selected->idComercio, $credential_selected->sid);

            $messagelala = "";

            if ($result == NULL) {
                $messagelala = "Sin comunicación";
            } else {
                if ($result->ejecucion_correcta) {
                    $messagelala = "OK";
                } else {
                    $messagelala = "Sin comunicación";
                }
            }
            $this->set('messagelala', $messagelala);
        }
    }

    public function forceExecution()
    {
        if ($this->request->is('post')) {

            $action = 'Ejecución de control de pagos forzada - Cobro Digital';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $command = ROOT . "/bin/cake CobroDigitalControl -f > /dev/null &";
            $result = exec($command, $output);
            $this->Flash->success(__('Consulta ejecutada. ' . $result));
            return  $this->redirect($this->referer());
        }
    }

    public function cleanTransactions()
    {
        if ($this->request->is('post')) {

            $action = 'Borrado de transacciones con estado sin asignar - Cobro Digital';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $this->loadModel('CobrodigitalTransactions');
            if ($this->CobrodigitalTransactions->deleteAll(['estado' => 0])) {
                $this->Flash->success(__('Se han borrado correctamente las transacciones con estado: sin asignar.'));
            } else {
                $this->Flash->error(__('No se han podido borrar las transacciones con estaod: sin asignar.'));
            }
            
            return  $this->redirect($this->referer());
        }
    }
}
