<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ConnectionsTraffic Controller
 *
 * @property \App\Model\Table\ConnectionsTrafficTable $ConnectionsTraffic
 */
class ConnectionsTrafficController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }

    public function isAuthorized($user = null)
    {
        if ($this->request->getParam('action') == 'getAccountingTrafficHistorial') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function view($id)
    {
        $this->loadModel('Connections');

        $connection = $this->Connections->get($id, [
            'contain' => ['Customers', 'Controllers', 'Services', 'Users', 'Areas']
        ]);

        $this->set(compact('connection'));
        $this->set('_serialize', ['connection']);
    }

    public function getAccountingTrafficHistorial()
    {
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $ip = $this->request->input('json_decode')->ip;

            $connectionsTraffic = $this->ConnectionsTraffic->find()->where(['ip_address' => ip2long($ip)]);
            $this->set('connectionsTraffic', $connectionsTraffic);
        }
    }
}
