<?php
namespace App\Controller;

use App\Controller\AppController;

use App\Controller\Component\Admin\FiscoAfipComp;
use App\Controller\Component\Admin\FiscoAfipCompPdf;

use Cake\I18n\Time;
use Cake\Datasource\ConnectionManager;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

/**
 * Invoices Controller
 *
 * @property \App\Model\Table\InvoicesTable $Invoices
 */
class InvoicesController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('FiscoAfipComp', [
             'className' => '\App\Controller\Component\Admin\FiscoAfipComp'
        ]);

        $this->loadComponent('FiscoAfip', [
             'className' => '\App\Controller\Component\Admin\FiscoAfip'
        ]);

        $this->loadComponent('FiscoAfipCompPdf', [
             'className' => '\App\Controller\Component\Admin\FiscoAfipCompPdf'
        ]);

        $this->loadComponent('Accountant', [
             'className' => '\App\Controller\Component\Admin\Accountant'
        ]);

        $this->loadComponent('CurrentAccount', [
             'className' => '\App\Controller\Component\Admin\CurrentAccount'
        ]);
    }

    public function isAuthorized($user = null)
    {
        if ($this->request->getParam('action') == 'getAll') {
            return true;
        }

        if ($this->request->getParam('action') == 'getPresuX') {
            return true;
        }

        if ($this->request->getParam('action') == 'getInvoices') {
            return true;
        }

        if ($this->request->getParam('action') == 'getInvoicesCustomer') {
            return true;
        }

        if ($this->request->getParam('action') == 'getInvoicesNoPaidCustomer') {
            return true;
        }

        if ($this->request->getParam('action') == 'removeAutoDebit') {
            return true;
        }

        if ($this->request->getParam('action') == 'addAutoDebit') {
            return true;
        }

        if ($this->request->getParam('action') == 'delete') {
            return true;
        }

        if ($this->request->getParam('action') == 'generateIndiFromPresu') {
            return true;
        }

        if ($this->request->getParam('action') == 'generateIndiFromPresuX') {
            return true;
        }

        if ($this->request->getParam('action') == 'generateIndiFromDebt') {
            return true;
        }

        if ($this->request->getParam('action') == 'anulate') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->loadModel('MassEmailsTemplates');
        $mass_emails_templates_model = $this->MassEmailsTemplates
            ->find()
            ->where([
                'enabled' => TRUE
            ]);

        $mass_emails_templates = [
            '' => 'Seleccione'
        ];

        foreach ($mass_emails_templates_model as $mass_email_template_model) {
            $mass_emails_templates[$mass_email_template_model->id] = $mass_email_template_model->name;
        }

        $paraments = $this->request->getSession()->read('paraments');

        $business = [];
        foreach ($paraments->invoicing->business as $b) {
            if($b->enable){
                $business[$b->id] = $b->name . ' (' . $b->address. ')';
            }            
        }

        $this->set(compact('mass_emails_templates', 'business'));
        $this->set('_serialize', ['mass_emails_templates']);
    }

    public function getInvoices()
    {
        $response = new \stdClass();

        $response->draw = intval($this->request->getQuery('draw'));


        $customer_ids_disabled = $this->getCustomersIdsBusinessDisabled();

        $where = [];
        if(count($customer_ids_disabled) > 0){
            $where += ['Invoices.customer_code NOT IN' => $customer_ids_disabled];
        }


        if (null !== $this->request->getQuery('where')) {
            $where += $this->request->getQuery('where');
            $response->recordsTotal = $this->Invoices->find()->where($where)->count();
        } else {
            $response->recordsTotal = $this->Invoices->find()->where($where)->count();
        }

        $response->data = $this->Invoices->find('ServerSideData', [
            'params' => $this->request->getQuery(),
            'where' => count($where) > 0 ?  $where : false
        ]);

        $response->recordsFiltered = $this->Invoices->find('RecordsFiltered', [
            'params' => $this->request->getQuery(),
            'where' =>  count($where) > 0 ?  $where : false
        ]);

        $this->set(compact('response'));
        $this->set('_serialize', ['response']);
    }
    
    public function getPresuX()
    {
        $response = new \stdClass();

        $response->draw = intval($this->request->getQuery('draw'));
        
        $where = $this->request->getQuery('where');
        $where += ['Invoices.tipo_comp' => 'XXX'];
        
        $response->recordsTotal = $this->Invoices->find()->where($where)->count();

        $response->data = $this->Invoices->find('ServerSideDataPresuX', [
            'params' => $this->request->getQuery(),
            'where' =>  $where 
        ]);

        $response->recordsFiltered = $this->Invoices->find('RecordsFilteredPresuX', [
            'params' => $this->request->getQuery(),
            'where' =>   $where
        ]);

        $this->set(compact('response'));
        $this->set('_serialize', ['response']);
    }

    /**
     * called from: 
     *      payment/add
     *      comprobante/add
    */
    public function getInvoicesNoPaidCustomer()
    {
        $code = $this->request->getQuery('customer_code');
        $connection_id = $this->request->getQuery('connection_id');

        $invoices = [];

        if ($code) {

            if ($connection_id != 0) {

                //$this->log('connection_id != 0', 'debug');

                $invoices = $this->Invoices->find()
                    ->contain(['Users', 'Customers', 'Connections'])
                    ->where([
                        'Invoices.paid IS' => NULL, 
                        'Invoices.customer_code' => $code,
                        'Invoices.connection_id' => $connection_id
                    ])
                    ->order(['date' => 'ASC']);
            } else {

                //$this->log('connection_id == 0', 'debug');

                $invoices = $this->Invoices->find()
                    ->contain(['Users', 'Customers', 'Connections'])
                    ->where([
                        'Invoices.paid IS' => NULL, 
                        'Invoices.customer_code' => $code,
                        'Invoices.connection_id IS' => NULL
                    ])
                    ->order(['date' => 'ASC']);
            }
        }

        $this->set(compact('invoices'));
        $this->set('_serialize', ['invoices']);
    }

    public function generateMasiveFromDebts()
    {
        $this->loadModel('Users');
        $users_model = $this->Users->find();
        $users = [];

        foreach ($users_model as $user_model) {
            $users[$user_model->name] = $user_model->name;
        }

        if ($this->request->is('post')) {

            $error = $this->isReloadPage();

            if (!$error) {

                $action = 'Generación de Facturación Masiva';
                $detail = '';
                $this->registerActivity($action, $detail, NULL, TRUE);

                $paraments = $this->request->getSession()->read('paraments');

                $this->loadModel('CustomersHasDiscounts');

                $data = json_decode($this->request->getData('data'));
                $customer_invoices = [];

                $data->ids = array_reverse($data->ids);
                $invoices_ids = "";
                $customers_codes = "";

                foreach ($data->ids as $id) {

                    if (strstr($id, '_', true) == 'debt') { //si es id de debt

                        $debt = $this->Invoices->Debts->find()
                            ->contain(['Customers.Cities', 'Connections', 'CustomersHasDiscounts', 'Users'])
                            ->where([
                                'Debts.invoice_id IS' => NULL,
                                'Debts.id' => str_replace('debt_', '', $id)
                            ])->first();

                        if ($debt) {

                            if (!array_key_exists($debt->customer->code, $customer_invoices)) {
                                $customer_invoices[$debt->customer->code] = [];
                            }

                            if ($debt->connection) {

                                if (!array_key_exists($debt->connection->id, $customer_invoices[$debt->customer->code])) {
                                    $customer_invoices[$debt->customer->code][$debt->connection->id] = [];
                                }

                                if (!array_key_exists($debt->tipo_comp, $customer_invoices[$debt->customer->code][$debt->connection->id])) {
                                    $customer_invoices[$debt->customer->code][$debt->connection->id][$debt->tipo_comp] = [
                                        'debts' => [],
                                        'discounts' => [],
                                        'customer' => $debt->customer,
                                        'address' => $debt->connection->address,
                                        'comments' => ''
                                    ];
                                }

                                $customer_invoices[$debt->customer->code][$debt->connection->id][$debt->tipo_comp]['debts'][] = $debt;
                                $customer_invoices[$debt->customer->code][$debt->connection->id][$debt->tipo_comp]['comments'] = $debt->description;

                            } else { //no es deuda de conexion

                                if (!array_key_exists(0, $customer_invoices[$debt->customer->code])) {
                                    $customer_invoices[$debt->customer->code][0] = [];
                                }

                                if (!array_key_exists($debt->tipo_comp, $customer_invoices[$debt->customer->code][0])) {
                                    $customer_invoices[$debt->customer->code][0][$debt->tipo_comp] = [
                                        'debts' => [],
                                        'discounts' => [],
                                        'customer' => $debt->customer,
                                        'address' => $debt->customer->address,
                                        'comments' => ''
                                    ];
                                }

                                $customer_invoices[$debt->customer->code][0][$debt->tipo_comp]['debts'][] = $debt;
                                $customer_invoices[$debt->customer->code][0][$debt->tipo_comp]['comments'] .= $debt->description . '; ';
                            }
                        }
                    } else { //si es id de descuento

                        $customerDiscount = $this->CustomersHasDiscounts->find()
                            ->contain(['Customers.Cities', 'Connections', 'Users'])
                            ->where([
                                'CustomersHasDiscounts.invoice_id IS' => NULL,
                                'CustomersHasDiscounts.id' => str_replace('discount_', '', $id)
                            ])->first();

                        if ($customerDiscount) {

                            if (!array_key_exists($customerDiscount->customer->code, $customer_invoices)) {
                                $customer_invoices[$customerDiscount->customer->code] = [];
                            }

                            if ($customerDiscount->connection) {

                                if (!array_key_exists($customerDiscount->connection->id, $customer_invoices[$customerDiscount->customer->code])) {
                                    $customer_invoices[$customerDiscount->customer->code][$customerDiscount->connection->id] = [];
                                }

                                if (!array_key_exists($customerDiscount->tipo_comp, $customer_invoices[$customerDiscount->customer->code][$customerDiscount->connection->id])) {
                                    $customer_invoices[$customerDiscount->customer->code][$customerDiscount->connection->id][$customerDiscount->tipo_comp] = [
                                        'debts' => [],
                                        'discounts' => [],
                                        'customer' => $customerDiscount->customer,
                                        'address' => $customerDiscount->connection->address,
                                        'comments' => ''
                                    ];
                                }

                                $customer_invoices[$customerDiscount->customer->code][$customerDiscount->connection->id][$customerDiscount->tipo_comp]['discounts'][] = $customerDiscount;
                                $customer_invoices[$customerDiscount->customer->code][$customerDiscount->connection->id][$customerDiscount->tipo_comp]['comments'] = $customerDiscount->description;

                            } else { //no es discounts de conexion

                                if (!array_key_exists(0, $customer_invoices[$customerDiscount->customer->code])) {
                                    $customer_invoices[$customerDiscount->customer->code][0] = [];
                                }

                                if (!array_key_exists($customerDiscount->tipo_comp, $customer_invoices[$customerDiscount->customer->code][0])) {
                                    $customer_invoices[$customerDiscount->customer->code][0][$customerDiscount->tipo_comp] = [
                                        'debts' => [],
                                        'discounts' => [],
                                        'customer' => $customerDiscount->customer,
                                        'address' => $customerDiscount->customer->address,
                                        'comments' => ''
                                    ];
                                }

                                $customer_invoices[$customerDiscount->customer->code][0][$customerDiscount->tipo_comp]['discounts'][] = $customerDiscount;
                                $customer_invoices[$customerDiscount->customer->code][0][$customerDiscount->tipo_comp]['comments'] .= $customerDiscount->description . '; ';
                            }
                        }
                    }
                }

                $success = true;

                $first = TRUE;

                foreach ($customer_invoices as $customer_code => $customer_invoice) {

                    foreach ($customer_invoice as $connection_id => $connection_invoice) {

                        foreach ($connection_invoice as $tipo_comp => $invoice) {

                            $objet = [];

                            $objet['customer'] = $invoice['customer'];
                            $objet['concept_type'] = $data->concept_type;
                            $objet['date']  = $data->date;
                            $objet['date_start'] = $data->date_start;
                            $objet['date_end'] = $data->date_end;
                            $objet['duedate'] = $data->duedate;
                            $objet['debts'] = $invoice['debts'];
                            $objet['discounts'] = $invoice['discounts'];
                            $objet['tipo_comp'] = $tipo_comp;
                            $objet['comments'] = $invoice['comments'] != '' ? $invoice['comments'] : 'Facturador';
                            $objet['user_id'] = $this->Auth->user()['id'];
                            $objet['manual'] = false;

                            $objet['connection_id'] = $connection_id != 0 ? $connection_id : null;
                            $objet['address']  = $invoice['address'];

                            $invoice = $this->FiscoAfipComp->invoice($objet, FALSE);

                            if ($invoice) {

                                //informo afip si no es de tipo X

                                if ($this->FiscoAfip->informar($invoice, FiscoAfipComp::TYPE_INVOICE)) {

                                    //generar PDF

                                    $this->Invoices->save($invoice);

                                    if ($first) {
                                        $invoices_ids .= $invoice->id;
                                        $customers_codes .= $invoice->customer_code;
                                        $first = FALSE;
                                    } else {
                                        $invoices_ids .= '.' . $invoice->id;
                                        $customers_codes .= '.' . $invoice->customer_code;
                                    }

                                    // $this->FiscoAfipCompPdf->invoice($invoice, FiscoAfipCompPdf::TYPE_INVOICE);

                                } else {
                                    $success = false;
                                    $this->FiscoAfipComp->rollbackInvoice($invoice, true);
                                }
                            } else {
                                $success = false;
                                //porque -> $invoice = $this->FiscoAfipComp->invoice($objet, FALSE); retorna false y no el objeto de invoice porque no se crea entrecomillas
                                //$this->FiscoAfipComp->rollbackInvoice($invoice, true);
                            }
                        }
                    }
                }

                if ($success) {

                    $command = ROOT . "/bin/cake debtMonthControlAfterAction $customers_codes 0 > /dev/null &";
                    $result = exec($command);

                    $this->Flash->success(__('Facturas generada correctamentes'));

                    if (!empty($paraments->invoicing->email_emplate_invoice)) {
                        $command = ROOT . "/bin/cake SendEmailInvoices $invoices_ids > /dev/null &";
                        exec($command, $output);
                    }
                }

                return $this->redirect(['action' => 'generateMasiveFromDebts']);
            } else {

                $this->Flash->warning(__('La misma acción se intento realizar varias veces.'));
                return $this->redirect(['action' => 'generateMasiveFromDebts']);
            }
        }

        $this->set(compact('users'));
    }

    public function generateMasiveFromPresuX()
    {
        if ($this->request->is('post')) {

            $this->loadModel('Invoices');

            $data = json_decode($this->request->getData('data'));
            $customer_invoices = [];

            foreach ($data->ids as $id) {

                $presu_x = $this->Invoices->find()->contain(['Concepts', 'Customers.Cities', 'Connections', 'Users'])->where([ 'Invoices.id' => $id])->first();

                if ($presu_x) {

                    $objet = [];

                    $objet['customer'] = $presu_x->customer;
                    $objet['concept_type'] = $data->concept_type;
                    $objet['date']  = $data->date;
                    $objet['date_start'] = $data->date_start;
                    $objet['date_end'] = $data->date_end;
                    $objet['duedate'] = $data->duedate;

                    $objet['debts'] = $presu_x->concepts;

                    $objet['discounts'] = [];
                    $objet['tipo_comp'] = $data->type;
                    $objet['comments'] = $presu_x->comments;
                    $objet['user_id'] = $this->Auth->user()['id'];
                    $objet['manual'] = true;

                    $objet['business_id'] = $data->business_id;

                    $objet['connection_id'] = $presu_x->connection_id;
                    $objet['address']  = $presu_x->customer_addres;

                    $invoice = $this->FiscoAfipComp->invoice($objet);

                    if ($invoice) {

                        //informo afip si no es de tipo X

                        if ($this->FiscoAfip->informar($invoice, FiscoAfipComp::TYPE_INVOICE)) {

                            //generar PDF

                            $this->Invoices->save($invoice);

                            $this->FiscoAfipCompPdf->invoice($invoice, FiscoAfipCompPdf::TYPE_INVOICE);

                            $this->Invoices->Debts->updateAll(['invoice_id' => $invoice->id], ['invoice_id' => $presu_x->id]);

                            $this->FiscoAfipComp->rollbackInvoice($presu_x);

                        } else {

                           $this->FiscoAfipComp->rollbackInvoice($invoice, true);

                           $this->Flash->error(__('Error al intentar generar la factura.'));

                           return $this->redirect(['action' => 'generateMasiveFromPresuX']);
                        }

                    } else {

                        $this->FiscoAfipComp->rollbackInvoice($invoice, true);

                        $this->Flash->error(__('Error al intentar generar la factura.'));

                        return $this->redirect(['action' => 'generateMasiveFromPresuX']);
                    }
                }
            }

            $this->Flash->success(__('Factura generada correctamentes'));
        }

        $paraments = $this->request->getSession()->read('paraments');

        $business = [];
        foreach ($paraments->invoicing->business as $b) {
            $business[$b->id] = $b->name;
        }

        $this->set(compact('business'));
    }

    public function generateIndiFromPresu()
    {
        if ($this->request->is('post')) {

            $this->loadModel('Presupuestos');

            $data = json_decode($this->request->getData('data'));
            $customer_invoices = [];

            $paraments = $this->request->getSession()->read('paraments');

            foreach ($data->ids as $id) {

                $presupuesto = $this->Presupuestos->find()->contain(['PresupuestoConcepts', 'Customers.Cities', 'Connections', 'Users'])->where([ 'Presupuestos.id' => $id])->first();

                if ($presupuesto) {

                    $objet = [];

                    $objet['customer'] = $presupuesto->customer;
                    $objet['concept_type'] = $data->concept_type;
                    $objet['date']  = $data->date;
                    $objet['date_start'] = $data->date_start;
                    $objet['date_end'] = $data->date_end;
                    $objet['duedate'] = $data->duedate;

                    $objet['debts'] = $presupuesto->presupuesto_concepts;

                    $objet['discounts'] = [];
                    $objet['tipo_comp'] = $data->type;
                    $objet['comments'] = $presupuesto->comments;
                    $objet['user_id'] = $this->Auth->user()['id'];
                    $objet['manual'] = true;

                    $objet['business_id'] = $data->business_id;

                    $objet['connection_id'] = $presupuesto->connection_id;
                    $objet['address']  = $presupuesto->customer_addres;

                     if ($this->Accountant->isEnabled()) {

                        $seating = $this->Accountant->addSeating([
                            'concept' => 'Factura Manual',
                            'comments' => '',
                            'seating' => null,
                            'account_debe' => $presupuesto->customer->account_code,
                            'account_haber' => $paraments->accountant->acounts_parent->surcharge,
                            'value' => $presupuesto->total,
                            'redirect' => null
                        ]);

                        if ($seating) {
                            $objet['seating_number'] = $seating->number;
                        }
                    }

                    $invoice = $this->FiscoAfipComp->invoice($objet);

                    if ($invoice) {

                        //informo afip si no es de tipo X

                        if ($this->FiscoAfip->informar($invoice, FiscoAfipComp::TYPE_INVOICE)) {

                            //generar PDF

                            $this->Invoices->save($invoice);

                            // $this->FiscoAfipCompPdf->invoice($invoice, FiscoAfipCompPdf::TYPE_INVOICE);

                            $this->FiscoAfipComp->rollbackPresupuesto($presupuesto);

                            $this->Flash->success(__('Factura generada correctamentes'));

                        } else {

                           $this->FiscoAfipComp->rollbackInvoice($invoice, true);

                           $this->Flash->error(__('Error al intentar generar la factura.'));
                        }
                    } else {

                        $this->FiscoAfipComp->rollbackInvoice($invoice, true);

                        $this->Flash->error(__('Error al intentar generar la factura.'));
                    }
                }
            }
        }

        return $this->redirect($this->referer());
    }

    public function generateIndiFromPresuX()
    {
        if ($this->request->is('post')) {

            $this->loadModel('Invoices');

            $data = json_decode($this->request->getData('data'));
            $customer_invoices = [];

            foreach ($data->ids as $id) {

                $presu_x = $this->Invoices->find()->contain(['Concepts', 'Customers.Cities', 'Connections', 'Users'])->where([ 'Invoices.id' => $id])->first();

                if ($presu_x) {

                    $objet = [];

                    $objet['customer'] = $presu_x->customer;
                    $objet['concept_type'] = $data->concept_type;
                    $objet['date']  = $data->date;
                    $objet['date_start'] = $data->date_start;
                    $objet['date_end'] = $data->date_end;
                    $objet['duedate'] = $data->duedate;

                    $objet['debts'] = $presu_x->concepts;

                    $objet['discounts'] = [];
                    $objet['tipo_comp'] = $data->type;
                    $objet['comments'] = $presu_x->comments;
                    $objet['user_id'] = $this->Auth->user()['id'];
                    $objet['manual'] = true;

                    $objet['business_id'] = $data->business_id;

                    $objet['connection_id'] = $presu_x->connection_id;
                    $objet['address']  = $presu_x->customer_addres;

                    $invoice = $this->FiscoAfipComp->invoice($objet);

                    if ($invoice) {

                        //informo afip si no es de tipo X

                        if ($this->FiscoAfip->informar($invoice, FiscoAfipComp::TYPE_INVOICE)) {

                            //generar PDF

                            $this->Invoices->save($invoice);

                            // $this->FiscoAfipCompPdf->invoice($invoice, FiscoAfipCompPdf::TYPE_INVOICE);

                            $this->Invoices->Debts->updateAll(['invoice_id' => $invoice->id], ['invoice_id' => $presu_x->id]);

                            $this->FiscoAfipComp->rollbackInvoice($presu_x);

                            $this->Flash->success(__('Factura generada correctamentes'));

                        } else {

                           $this->FiscoAfipComp->rollbackInvoice($invoice, true);

                           $this->Flash->error(__('Error al intentar generar la factura.'));
                        }
                    } else {

                        $this->FiscoAfipComp->rollbackInvoice($invoice, true);
                        $this->Flash->error(__('Error al intentar generar la factura.'));
                    }
                }
            }
        }

        return $this->redirect($this->referer());
    }

    public function generateIndiFromDebt($debt_id = null)
    {
        if ($this->request->is('post')) {

            $this->loadModel('CustomersHasDiscounts');

            $data = json_decode($this->request->getData('data'));
            $customer_invoices = [];

            $data->ids = array_reverse($data->ids);

            foreach ($data->ids as $id) {

                if (strstr($id, '_', true) == 'debt') { //si es id de debt

                    $debt = $this->Invoices->Debts->find()
                        ->contain(['Customers.Cities', 'Connections', 'CustomersHasDiscounts', 'Users'])
                        ->where([
                            'Debts.id'            => str_replace('debt_', '', $id),
                            'Debts.invoice_id IS' => NULL
                        ])->first();

                    if ($debt) {

                        $debt->tipo_comp = $data->type;

                        if (!array_key_exists($debt->customer->code, $customer_invoices)) {
                            $customer_invoices[$debt->customer->code] = [];
                        }

                        if ($debt->connection) {

                            if (!array_key_exists($debt->connection->id, $customer_invoices[$debt->customer->code])) {
                                $customer_invoices[$debt->customer->code][$debt->connection->id] = [];
                            }

                            if (!array_key_exists($debt->tipo_comp, $customer_invoices[$debt->customer->code][$debt->connection->id])) {
                                $customer_invoices[$debt->customer->code][$debt->connection->id][$debt->tipo_comp] = [
                                    'debts' => [],
                                    'discounts' => [],
                                    'customer' => $debt->customer,
                                    'address' => $debt->connection->address,
                                    'comments' => ''
                                ];
                            }

                            $customer_invoices[$debt->customer->code][$debt->connection->id][$debt->tipo_comp]['debts'][] = $debt;
                            $customer_invoices[$debt->customer->code][$debt->connection->id][$debt->tipo_comp]['comments'] = $debt->description;

                        } else { //no es deuda de conexion

                            if (!array_key_exists(0, $customer_invoices[$debt->customer->code])) {
                                $customer_invoices[$debt->customer->code][0] = [];
                            }

                            if (!array_key_exists($debt->tipo_comp, $customer_invoices[$debt->customer->code][0])) {
                                $customer_invoices[$debt->customer->code][0][$debt->tipo_comp] = [
                                    'debts' => [],
                                    'discounts' => [],
                                    'customer' => $debt->customer,
                                    'address' => $debt->customer->address,
                                    'comments' => ''
                                ];
                            }

                            $customer_invoices[$debt->customer->code][0][$debt->tipo_comp]['debts'][] = $debt;
                            $customer_invoices[$debt->customer->code][0][$debt->tipo_comp]['comments'] .= $debt->description . '; ';
                        }
                    }
                } else { //si es id de descuento

                    $customerDiscount = $this->CustomersHasDiscounts->find()
                        ->contain(['Customers.Cities', 'Connections', 'Users'])
                        ->where([
                            // 'CustomersHasDiscounts.invoice_id IS' => NULL,
                            'CustomersHasDiscounts.id'            => str_replace('discount_', '', $id),
                            'CustomersHasDiscounts.invoice_id IS' => NULL
                        ])->first();

                    if ($customerDiscount) {

                        $customerDiscount->tipo_comp = $data->type;

                        if (!array_key_exists($customerDiscount->customer->code, $customer_invoices)) {
                            $customer_invoices[$customerDiscount->customer->code] = [];
                        }

                        if ($customerDiscount->connection) {

                            if (!array_key_exists($customerDiscount->connection->id, $customer_invoices[$customerDiscount->customer->code])) {
                                $customer_invoices[$customerDiscount->customer->code][$customerDiscount->connection->id] = [];
                            }

                            if (!array_key_exists($customerDiscount->tipo_comp, $customer_invoices[$customerDiscount->customer->code][$customerDiscount->connection->id])) {
                                $customer_invoices[$customerDiscount->customer->code][$customerDiscount->connection->id][$customerDiscount->tipo_comp] = [
                                    'debts' => [],
                                    'discounts' => [],
                                    'customer' => $customerDiscount->customer,
                                    'address' => $customerDiscount->connection->address,
                                    'comments' => ''
                                ];
                            }

                            $customer_invoices[$customerDiscount->customer->code][$customerDiscount->connection->id][$customerDiscount->tipo_comp]['discounts'][] = $customerDiscount;
                            $customer_invoices[$customerDiscount->customer->code][$customerDiscount->connection->id][$customerDiscount->tipo_comp]['comments'] = $customerDiscount->description;

                        } else { //no es discounts de conexion

                            if (!array_key_exists(0, $customer_invoices[$customerDiscount->customer->code])) {
                                $customer_invoices[$customerDiscount->customer->code][0] = [];
                            }

                            if (!array_key_exists($customerDiscount->tipo_comp, $customer_invoices[$customerDiscount->customer->code][0])) {
                                $customer_invoices[$customerDiscount->customer->code][0][$customerDiscount->tipo_comp] = [
                                    'debts' => [],
                                    'discounts' => [],
                                    'customer' => $customerDiscount->customer,
                                    'address' => $customerDiscount->customer->address,
                                    'comments' => ''
                                ];
                            }

                            $customer_invoices[$customerDiscount->customer->code][0][$customerDiscount->tipo_comp]['discounts'][] = $customerDiscount;
                            $customer_invoices[$customerDiscount->customer->code][0][$customerDiscount->tipo_comp]['comments'] .= $customerDiscount->description . '; ';
                        }
                    }
                }
            }

            $success = true; 

            foreach ($customer_invoices as $customer_code => $customer_invoice) {

                foreach ($customer_invoice as $connection_id => $connection_invoice) {

                    foreach ($connection_invoice as $tipo_comp => $invoice) {

                        $objet = [];

                        $objet['customer'] = $invoice['customer'];
                        $objet['concept_type'] = $data->concept_type;
                        $objet['date']  = $data->date;
                        $objet['date_start'] = $data->date_start;
                        $objet['date_end'] = $data->date_end;
                        $objet['duedate'] = $data->duedate;
                        $objet['debts'] = $invoice['debts'];
                        $objet['discounts'] = $invoice['discounts'];
                        $objet['tipo_comp'] = $tipo_comp;
                        $objet['comments'] = $invoice['comments'] != '' ? $invoice['comments'] : 'Facturador';
                        $objet['user_id'] = $this->Auth->user()['id'];
                        $objet['manual'] = false;

                        $objet['connection_id'] = $connection_id != 0 ? $connection_id : null;
                        $objet['address']  = $invoice['address'];

                        $objet['business_id'] = $data->business_id;

                        $invoice = $this->FiscoAfipComp->invoice($objet);

                        if ($invoice) {

                            //informo afip si no es de tipo X

                            if ($this->FiscoAfip->informar($invoice, FiscoAfipComp::TYPE_INVOICE)) {

                                //generar PDF

                                $this->Invoices->save($invoice);

                                // $this->FiscoAfipCompPdf->invoice($invoice, FiscoAfipCompPdf::TYPE_INVOICE);

                                $this->Flash->success(__('Factura generada correctamentes'));

                            } else {
                               $this->FiscoAfipComp->rollbackInvoice($invoice, true);
                               $this->Flash->error(__('Error al intentar generar la factura.'));
                            }
                        } else {
                            $this->FiscoAfipComp->rollbackInvoice($invoice, true);
                            $this->Flash->error(__('Error al intentar generar la factura.'));
                        }
                    }
                }
            }

            $url = explode('/', $this->referer());
            $controller = $url[4];
            $url = $url[0] . '//' . $url[2] . '/' . $url[3] . '/' . $url[4] . '/' . $url[5];
            if (strpos($controller, 'customers') !== FALSE 
                || strpos($controller, 'Customers') !== FALSE) {

                $url .= '/' . $data->customer_code . '/' . $data->tab;
            }
            return $this->redirect($url);
        }
    }

    public function showprint($id)
    {
        $data = new \stdClass;
        $data->id = $id;
        $this->FiscoAfipCompPdf->invoice($data);
    }

    public function delete()
    {
        if ($this->request->is('ajax')) {
    
            $data = $this->request->input('json_decode');

            $nvoice = $this->Invoices->get($data->id);

            $ok = false;

            if ($this->FiscoAfipComp->rollbackInvoice($nvoice)) {
                $ok = true;
            }

            $this->set("ok", $ok);
        }
    }

    public function anulate()
    {
        if ($this->request->is('ajax')) {

            $action = 'Anulación Rápida de Factura';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $afip_codes = $this->request->getSession()->read('afip_codes');

            $data = $this->request->input('json_decode');

            $this->loadModel('Invoices');
            $this->loadModel('CreditNotes');

            $invoice = $this->Invoices->find()->contain(['Concepts', 'Customers.Cities', 'Connections', 'Users'])->where([ 'Invoices.id' => $data->id])->first();

            if ($invoice) {

                $concept = new \stdClass;
                $concept->type        = 'S';
                $concept->code        = 99;
                $number = sprintf("%'.04d", $invoice->pto_vta) . '-' . sprintf("%'.08d", $invoice->num);
                $concept->description = "Anulación de Factura #" . $number;
                $concept->quantity    = 1;
                $concept->unit        = 7;
                $concept->discount    = 0;
                $concept->tax         = $invoice->tipo_comp == "011" ? 1 : 5;
                $tax =  $afip_codes['alicuotas_percectage'][$concept->tax];
                $row = $this->CurrentAccount->calulateIvaAndNeto($invoice->total, $tax, $concept->quantity);

                $concept->price = $row->price;
                $concept->sum_price = $row->sum_price;
                $concept->sum_tax = $row->sum_tax;
                $concept->total = $row->total;

                $concepts = [];
                $concepts[0] = $concept;

                $objet = [];

                $objet['customer'] = $invoice->customer;
                $objet['concept_type'] = $invoice->concept_type;
                $objet['date']  = Time::now();
                $objet['date_start'] = Time::now();
                $objet['date_end'] = Time::now();
                $objet['duedate'] = Time::now();

                $objet['concepts'] = $concepts;

                $objet['discounts'] = [];

                $objet['tipo_comp'] = 'NCX';
                if ($invoice->tipo_comp == '001') {       //a
                    $objet['tipo_comp'] = '003';
                } else if ($invoice->tipo_comp == '006') { //b
                    $objet['tipo_comp'] = '008';
                } else if ($invoice->tipo_comp == '011') { //c
                    $objet['tipo_comp'] = '013';
                }

                $objet['comments'] = $concept->description;
                $objet['user_id'] = $this->Auth->user()['id'];
                $objet['manual'] = true;

                $objet['business_id'] = $invoice->business_id;

                $objet['connection_id'] = $invoice->connection_id;
                $objet['address'] = $invoice->customer_addres;

                $credit_note = $this->FiscoAfipComp->creditNote($objet);

                if ($credit_note) {

                    //informo afip si no es de tipo X

                    if ($this->FiscoAfip->informar($credit_note, FiscoAfipComp::TYPE_CREDIT_NOTE)) {

                        $this->CreditNotes->save($credit_note);
                        $ok = true;
                        $invoice->anulated = Time::now();
                        $this->Invoices->save($invoice);

                    } else {
                       $this->FiscoAfipComp->rollbackCreditNote($credit_note, true);
                       $ok = false;
                    }

                } else {
                    $this->FiscoAfipComp->rollbackCreditNote($credit_note, true);
                    $ok = false;
                }
            }

            $this->set("ok", $ok);
        }
    }

    public function remove()
    {
        if ($this->request->is('post')) {

            $error = $this->isReloadPage();

            if (!$error) {

                $action = 'Anulación Factura Masiva';
                $detail = '';
                $this->registerActivity($action, $detail, NULL, TRUE);

                $afip_codes = $this->request->getSession()->read('afip_codes');

                $data = json_decode($_POST['data']);
                $invoices_removes = $data->invoices_removes;
                $ids = $data->ids;

                $action = 'Información de Anulación de Factura Masiva';
                $detail = 'Selección: ' . PHP_EOL;

                if ($invoices_removes->fa->count > 0) {
                    $detail .= '>>FA: ' . $invoices_removes->fa->count . PHP_EOL;
                }

                if ($invoices_removes->fb->count > 0) {
                    $detail .= '>>FB: ' . $invoices_removes->fb->count . PHP_EOL;
                }

                if ($invoices_removes->fc->count > 0) {
                    $detail .= '>>FC: ' . $invoices_removes->fc->count . PHP_EOL;
                }

                if ($invoices_removes->fx->count > 0) {
                    $detail .= '>>FX: ' . $invoices_removes->fx->count . PHP_EOL;
                }

                $detail .= 'Total comprobanes seleccionados: ' . $invoices_removes->total . PHP_EOL;
                $detail .= '------------------------------------------------' . PHP_EOL;
                $detail .= 'Resultado:' . PHP_EOL;

                if ($invoices_removes->fa->count > 0) {
                    $detail .= '>>NCA a crear: ' . $invoices_removes->fa->count . PHP_EOL;
                }

                if ($invoices_removes->fb->count > 0) {
                    $detail .= '>>NCB a crear: ' . $invoices_removes->fb->count . PHP_EOL;
                }

                if ($invoices_removes->fc->count > 0) {
                    $detail .= '>>NCC a crear: ' . $invoices_removes->fc->count . PHP_EOL;
                }

                if ($invoices_removes->fx->count > 0) {
                    $detail .= '>>FX a eliminar: ' . $invoices_removes->fx->count . PHP_EOL;
                }

                $this->registerActivity($action, $detail, NULL);

                $this->loadModel('Invoices');
                $this->loadModel('CreditNotes');

                $fx = 0;
                $fx_error = 0;
                $fa = 0;
                $fa_error = 0;
                $fb = 0;
                $fb_error = 0;
                $fc = 0;
                $fc_error = 0;

                $customers_codes = "";
                $first = TRUE;

                foreach ($ids as $id) {

                    $invoice = $this->Invoices
                        ->find()
                        ->contain([
                            'Concepts', 
                            'Customers.Cities', 
                            'Connections', 
                            'Users'
                        ])->where([
                            'Invoices.id' => $id
                        ])->first();

                    if ($invoice) {

                        if ($invoice->tipo_comp == 'XXX') {

                            if ($this->FiscoAfipComp->rollbackInvoice($invoice)) {
                                $fx++;
                            } else {
                                $fx_error++;
                            }
                        } else {

                            $concept = new \stdClass;
                            $concept->type        = 'S';
                            $concept->code        = 99;
                            $number               = sprintf("%'.04d", $invoice->pto_vta) . '-' . sprintf("%'.08d", $invoice->num);
                            $concept->description = "Anulación de Factura #" . $number;
                            $concept->quantity    = 1;
                            $concept->unit        = 7;
                            $concept->discount    = 0;
                            $concept->tax         = $invoice->tipo_comp == "011" ? 1 : 5;
                            $tax                  = $afip_codes['alicuotas_percectage'][$concept->tax];
                            $row                  = $this->CurrentAccount->calulateIvaAndNeto($invoice->total, $tax, $concept->quantity);

                            $concept->price = $row->price;
                            $concept->sum_price = $row->sum_price;
                            $concept->sum_tax = $row->sum_tax;
                            $concept->total = $row->total;

                            $concepts = [];
                            $concepts[0] = $concept;

                            $objet = [];

                            $objet['customer'] = $invoice->customer;
                            $objet['concept_type'] = $invoice->concept_type;
                            $objet['date']  = Time::now();
                            $objet['date_start'] = Time::now();
                            $objet['date_end'] = Time::now();
                            $objet['duedate'] = Time::now();

                            $objet['concepts'] = $concepts;

                            $objet['discounts'] = [];

                            if ($invoice->tipo_comp == '001') {       //a
                                $objet['tipo_comp'] = '003';
                            } else if ($invoice->tipo_comp == '006') { //b
                                $objet['tipo_comp'] = '008';
                            } else if ($invoice->tipo_comp == '011') { //c
                                $objet['tipo_comp'] = '013';
                            }

                            $objet['comments'] = $concept->description;
                            $objet['user_id'] = $this->Auth->user()['id'];
                            $objet['manual'] = true;

                            $objet['business_id'] = $invoice->business_id;

                            $objet['connection_id'] = $invoice->connection_id;
                            $objet['address'] = $invoice->customer_addres;

                            $credit_note = $this->FiscoAfipComp->creditNote($objet, FALSE);

                            if ($credit_note) {

                                //informo afip si no es de tipo X

                                if ($this->FiscoAfip->informar($credit_note, FiscoAfipComp::TYPE_CREDIT_NOTE)) {

                                    $this->CreditNotes->save($credit_note);
                                    if ($invoice->tipo_comp == '001') {       //a
                                        $fa++;
                                    } else if ($invoice->tipo_comp == '006') { //b
                                        $fb++;
                                    } else if ($invoice->tipo_comp == '011') { //c
                                        $fc++;
                                    }
                                    $invoice->anulated = Time::now();
                                    $this->Invoices->save($invoice);
                                    if ($first) {
                                        $customers_codes .= $invoice->customer_code;
                                        $first = FALSE;
                                    } else {
                                        $customers_codes .= '.' . $invoice->customer_code;
                                    }

                                } else {
                                    $this->FiscoAfipComp->rollbackCreditNote($credit_note, true);
                                }

                            } else {
                                $this->FiscoAfipComp->rollbackCreditNote($credit_note, true);
                                if ($invoice->tipo_comp == '001') {       //a
                                    $fa_error++;
                                    break;
                                } else if ($invoice->tipo_comp == '006') { //b
                                    $fb_error++;
                                    break;
                                } else if ($invoice->tipo_comp == '011') { //c
                                    $fc_error++;
                                    break;
                                }
                                break;
                            }
                        }
                    }
                }

                $action = 'Resultados de Anulación Factura Masiva';
                $detail .= 'Resultado esperado:' . PHP_EOL;
                $recalculo = FALSE;

                if ($invoices_removes->fa->count > 0) {
                    $recalculo = TRUE;
                    $detail .= '>>NCA a crear: ' . $invoices_removes->fa->count . PHP_EOL;
                }

                if ($invoices_removes->fb->count > 0) {
                    $recalculo = TRUE;
                    $detail .= '>>NCB a crear: ' . $invoices_removes->fb->count . PHP_EOL;
                }

                if ($invoices_removes->fc->count > 0) {
                    $recalculo = TRUE;
                    $detail .= '>>NCC a crear: ' . $invoices_removes->fc->count . PHP_EOL;
                }

                if ($invoices_removes->fx->count > 0) {
                    $detail .= '>>FX a eliminar: ' . $invoices_removes->fx->count . PHP_EOL;
                }
                $detail .= '----------------------------------------' . PHP_EOL;
                $detail .= 'Resultado final:' . PHP_EOL;
                if ($invoices_removes->fa->count > 0) {
                    $detail .= '>>NCA creado: ' . $fa . PHP_EOL;
                    if ($fa_error) {
                        $detail .= '>>NCA fallido: ' . $fa_error . PHP_EOL;
                    }
                }

                if ($recalculo) {

                    if ($customers_codes != "") {
                        $command = ROOT . "/bin/cake debtMonthControlAfterAction $customers_codes 0 > /dev/null &";
                        $result = exec($command);
                    }
                }

                if ($invoices_removes->fb->count > 0) {
                    $detail .= '>>NCB creado: ' . $fb . PHP_EOL;
                    if ($fb_error) {
                        $detail .= '>>NCB fallido: ' . $fb_error . PHP_EOL;
                    }
                }

                if ($invoices_removes->fc->count > 0) {
                    $detail .= '>>NCC creado: ' . $fc . PHP_EOL;
                    if ($fc_error) {
                        $detail .= '>>NCC fallido: ' . $fc_error . PHP_EOL;
                    }
                }

                if ($invoices_removes->fx->count > 0) {
                    $detail .= '>>FX eliminado: ' . $fx . PHP_EOL;
                    if ($fx_error) {
                        $detail .= '>>FX fallido: ' . $fx_error . PHP_EOL;
                    }
                }
                $this->registerActivity($action, $detail, NULL);

                $message = "Resultado:";

                if ($invoices_removes->fa->count > 0) {
                    $message .= '>>NCA creado: ' . $fa . ' - ';
                    if ($fa_error) {
                        $message .= '>>NCA fallido: ' . $fa_error . ' - ';
                    }
                }

                if ($invoices_removes->fb->count > 0) {
                    $message .= '>>NCB creado: ' . $fb . ' - ';
                    if ($fb_error) {
                        $message .= '>>NCB fallido: ' . $fb_error . ' - ';
                    }
                }

                if ($invoices_removes->fc->count > 0) {
                    $message .= '>>NCC creado: ' . $fc . ' - ';
                    if ($fc_error) {
                        $message .= '>>NCC fallido: ' . $fc_error . ' - ';
                    }
                }

                if ($invoices_removes->fx->count > 0) {
                    $message .= '>>FX eliminado: ' . $fx . ' - ';
                    if ($fx_error) {
                        $message .= '>>FX fallido: ' . $fx_error . ' - ';
                    }
                }

                $this->Flash->success($message);

                return $this->redirect($this->referer());
            } else {

                $this->Flash->warning(__('La misma acción se intento realizar varias veces.'));
                return $this->redirect(['action' => 'index']);
            }
        }
    }
}
