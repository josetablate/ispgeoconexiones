<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\Component\Admin\FiscoAfipComp;
use App\Controller\Component\Admin\FiscoAfipCompPdf;

/**
 * Presupuestos Controller
 *
 * @property \App\Model\Table\PresupuestosTable $Presupuestos
 *
 * @method \App\Model\Entity\Presupuesto[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PresupuestosController extends AppController
{
    
    public function initialize()
    {
        parent::initialize();
        
        $this->loadComponent('FiscoAfipComp', [
             'className' => '\App\Controller\Component\Admin\FiscoAfipComp'
        ]);

        $this->loadComponent('FiscoAfipCompPdf', [
             'className' => '\App\Controller\Component\Admin\FiscoAfipCompPdf'
        ]);
    }

    public function isAuthorized($user = null)
    {
        if ($this->request->getParam('action') == 'getPresupuestos') {
            return true;
        }

        if ($this->request->getParam('action') == 'delete') {
            return true;
        }

        return parent::allowRol($user['id']);
    }
    
      /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->loadModel('MassEmailsTemplates');
        $mass_emails_templates_model = $this->MassEmailsTemplates
            ->find()
            ->where([
                'enabled' => TRUE
            ]);

        $mass_emails_templates = [
            '' => 'Seleccione'
        ];

        foreach ($mass_emails_templates_model as $mass_email_template_model) {
            $mass_emails_templates[$mass_email_template_model->id] = $mass_email_template_model->name;
        }
        
        $paraments = $this->request->getSession()->read('paraments');

        $business = [];
        foreach ($paraments->invoicing->business as $b) {
            $business[$b->id] = $b->name;
        }

        $this->set(compact('mass_emails_templates', 'business'));
        $this->set('_serialize', ['mass_emails_templates']);
    }

    public function getPresupuestos()
    {
        $response = new \stdClass();

        $response->draw = intval($this->request->getQuery('draw'));

        if (null !== $this->request->getQuery('where')) {
            $response->recordsTotal = $this->Presupuestos->find()->where($this->request->getQuery('where'))->count();
        } else {
            $response->recordsTotal = $this->Presupuestos->find()->where()->count();
        }

        $response->data = $this->Presupuestos->find('ServerSideData', [
            'params' => $this->request->getQuery(),
            'where' => null !== $this->request->getQuery('where') ? $this->request->getQuery('where') : false
        ]);

        $response->recordsFiltered = $this->Presupuestos->find('RecordsFiltered', [
            'params' => $this->request->getQuery(),
            'where' =>  null !== $this->request->getQuery('where') ? $this->request->getQuery('where') : false
        ]);

        $this->set(compact('response'));
        $this->set('_serialize', ['response']);
    }

    public function showprint($id)
    {
        $data = new \stdClass;
        $data->id = $id;
        $this->FiscoAfipCompPdf->presupuesto($data);
    }

    public function delete()
    {
        if ($this->request->is('ajax')) {

            $data = $this->request->input('json_decode');

            $presupuesto = $this->Presupuestos->get($data->id);

            $ok = false;

            if ($this->FiscoAfipComp->rollbackPresupuesto($presupuesto)) {
                $ok = true;
            }

            $this->set("ok", $ok);
        }
    }
}
