<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;

abstract class IspController extends AppController
{
    protected $templates = [       
        [
            'value' => 'MikrotikIpfijaTa',
            'trademarks' => ['mikrotik' => 'Mikrotik', 'otra_marca' => 'Otra Marca'],
            'name' => 'IP + Estática + Estático Simple',
            'description' => "Se crea una queue simple estática. La IP que se 
            asigna es fija, la cual fue asignada por ISPBrain. Se crea un address_list llamado
            \"IPFIJAControllerID-Clientes-ISPBrain\", el cual puede ser usado para
            permitir la navegación solo de clientes cargados en ISPBrain.",
            ],
        [
            'value' => 'MikrotikPppoeTa',
            'trademarks' => ['mikrotik' => 'Mikrotik', 'otra_marca' => 'Otra Marca'],
            'name' => 'PPPoE Secret + Estática + Estático Simple',
            'description' => "Se crea un secret para cada conexión en el controlador
            asociado. La IP que se asigna es fija, la cual fue definida por ISPBrain
            y la queue simple se crea de manera estática al crear la conexión en ISPBrain.",
            ],
        [
            'value' => 'MikrotikDhcpTa',
            'trademarks' => ['mikrotik' => 'Mikrotik', 'otra_marca' => 'Otra Marca'],
            'name' => 'DHCP + Estático + Estático',
            'description' => "Se asigna IP vía DHCP pero la misma es amarrada a la 
            MAC del CPE haciendo que la asignación de IP sea fija, es decir misma 
            IP siempre a la misma conexión. La queue simple se crea en el momento 
            de creación de la conexión en ISPBrain",
        ],
    ];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('IntegrationRouter', [
             'className' => '\App\Controller\Component\Integrations\IntegrationRouter'
        ]);         
         
    }

    public function isAuthorized($user = null)
    {
        if ($this->request->getParam('action') == 'getTemplates') {
            return true;
        }

        return parent::allowRol($user['id'], 'IspController', 'admin');
    }

    public function index() {}

    abstract protected function add();

    abstract protected function delete();

    abstract protected function createIPTable($controller);

    abstract protected function getTemplateName($template_id);

    abstract protected function getTrademarksByTemplateAjax();

    abstract protected function getTrademarksByTemplate($template_id);

    abstract protected function getStatus($controller_id);

    abstract protected function getTemplates();

    abstract protected function getControllers();

    abstract protected function cloneController();

    abstract protected function checkSync($id);

    //escrib el archivo de cntrol
    abstract protected function updateFile($data, $name, $t);

    abstract protected function getLastErrorJson($data);

    //controller

    abstract protected function edit(); 
    abstract protected function config($id);
    abstract protected function sync($id);

    //plans  

    abstract protected function getPlansByController();
    abstract protected function addPlan();
    abstract protected function editPlan();
    abstract protected function deletePlan();   

    //ip excluida    

    abstract protected function getIpExcludedByController();
    abstract protected function deleteIPExcluded();
    abstract protected function addIPExcluded();
}
