<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Event\Event;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\Filesystem\Folder;

use App\Controller\Component\ComprobantesComponent;
use App\Controller\Component\AccountantComponent;
use App\Controller\Component\PDFGeneratorComponent;

class DebitAutoChubutController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Accountant', [
             'className' => '\App\Controller\Component\Admin\Accountant'
        ]);
    }

    public function isAuthorized($user = null) 
    {
        return parent::allowRol($user['id']);
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function index()
    {
        $parament = $this->request->getSession()->read('paraments');
        $payment_getway = $this->request->getSession()->read('payment_getway');
        $account_enabled = $this->Accountant->isEnabled();

        if ($this->request->is(['patch', 'post', 'put'])) {

            $payment_getway->config->chubut_auto_debit->enabled = $this->request->getData('enabled');
            $payment_getway->config->chubut_auto_debit->cash = $this->request->getData('cash');
            $payment_getway->config->chubut_auto_debit->portal = $this->request->getData('portal');
            $payment_getway->config->chubut_auto_debit->automatic = $this->request->getData('automatic');
            if ($account_enabled) {
                $payment_getway->config->chubut_auto_debit->account = $this->request->getData('account');
            }

            //se guarda los cambios
            $this->savePaymentGetwayParaments($payment_getway);
        }

        $this->loadModel('Accounts');
        $accountsTitles = $this->Accounts->find()->where(['title' => true])->order(['code' => 'ASC']);
        $accounts = $this->Accounts->find()
            ->order([
                'code' => 'ASC'
            ]);

        $this->set(compact('parament', 'accountsTitles', 'accounts', 'payment_getway', 'account_enabled'));
        $this->set('_serialize', ['parament', 'accounts', 'payment_getway']);
    }

    public function createAutoDebitAccount()
    {
        if ($this->request->is('post')) {

            $payment_getway = $this->request->getSession()->read('payment_getway');
            $paraments = $this->request->getSession()->read('paraments');

            if ($payment_getway->config->chubut_auto_debit->enabled) {

                $data = $this->request->getData();
                $customer_code = $data['customer_code'];
                $firstname = $data['firstname'];
                $lastname = $data['lastname'];
                $cuit = $data['cuit'];
                $cbu = $data['cbu'];

                $this->loadModel('CustomersAccounts');
                $customers_accounts = $this->CustomersAccounts
                    ->find()
                    ->where([
                        'customer_code' => $customer_code,
                        'deleted'       => FALSE,
                    ]);

                if ($customers_accounts->count() > 0) {
                    $this->Flash->warning(__('El Cliente tiene Cuenta/s, esto inhabilita la creación de la Cuenta, para poder crear una Cuenta de Débito Automático deberá "deshabilitar" dicha/s Cuenta/s.'));
                    return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
                }

                $this->loadModel('AutoDebitsAccounts');

                $auto_debit_account = $this->AutoDebitsAccounts->newEntity();
                $auto_debit_account->customer_code = $customer_code;
                $auto_debit_account->firstname = $firstname;
                $auto_debit_account->lastname = $lastname;
                $auto_debit_account->cuit = $cuit;
                $auto_debit_account->cbu = $cbu;
                $auto_debit_account->payment_getway_id = 102;

                if ($this->AutoDebitsAccounts->save($auto_debit_account)) {

                    $this->Flash->success(__('Se ha creado la Cuenta correctamente.'));

                    $cutomer_account = $this->CustomersAccounts->newEntity();
                    $cutomer_account->customer_code = $customer_code;
                    $cutomer_account->account_id = $auto_debit_account->id;
                    $cutomer_account->payment_getway_id = 102;
                    $this->CustomersAccounts->save($cutomer_account);

                    if ($paraments->gral_config->billing_for_service) {

                        $this->loadModel('Customers');
                        $customer = $this->Customers->get($customer_code, [
                            'contain' => ['Connections']
                        ]);

                        if (sizeof($customer->connections) > 0) {

                            $this->loadModel('Connections');

                            foreach ($customer->connections as $connection) {
                                $connection->chubut_auto_debit = TRUE;
                                $this->Connections->save($connection);
                            }
                        }
                    }
                }

            } else {
                $this->Flash->warning(__('Debe habilitar Débito Automático Chubut.'));
            }
        }

        return $this->redirect(['controller' => 'Customers', 'action' => 'view', $data['customer_code'], $data['tab']]);
    }
}
