<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Routing\Router;

/**
 * TemplateResources Controller
 *
 * @property \App\Model\Table\TemplateResourcesTable $TemplateResources
 */
class TemplateResourcesController extends AppController
{
    
    public function isAuthorized($user = null) {
        
        return true;
        // return parent::allowRol($user['id']);
        return parent::allowRol($user['id'], 'AvisoHttp', 'admin');
    }

    public function initialize()
    {
        parent::initialize();
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $templateResources = $this->paginate($this->TemplateResources);

        $this->set(compact('templateResources'));
        $this->set('_serialize', ['templateResources']);
    }

    /**
     * View method
     *
     * @param string|null $id Email Asset id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $templateResource = $this->TemplateResources->get($id, [
            'contain' => []
        ]);

        $this->set('templateResource', $templateResource);
        $this->set('_serialize', ['templateResource']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->request->is('post')) {

            $data = $this->request->getData();

            foreach ($this->request->getData('files') as $file) {

                if ($file['tmp_name']) {

                    $data['type'] = 'Imagen';

                    $templateResource = $this->TemplateResources->newEntity();

                    $ext = explode('.', $file['name']);
                    $now = Time::now();
                    $newName = $now->toUnixString() .'-' . $ext[0] . '.' .  end($ext);
                    $from = $file['tmp_name'];
                    $to =  '../../avisos/resources/'. $newName ;

                    if (move_uploaded_file($from, $to)) {

                        $data['url'] = $newName;
                    }
                }

                $templateResource = $this->TemplateResources->patchEntity($templateResource, $data);
                if (!$this->TemplateResources->save($templateResource)) {
                    $this->Flash->error(__('No se han guardado los recursos. Por favor, intente nuevamente.'));
                }
            }
            $this->Flash->success(__('Se han guardado los recursos correctamente.'));
            $id = $this->request->getData('action') == 'edit' ? $this->request->getData('message_template_id') : '';
            return $this->redirect(['controller' => 'MessageTemplates', 'action' => $this->request->getData('action'), $id]);
        }
        $this->set(compact('templateResource'));
        $this->set('_serialize', ['templateResource']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Email Asset id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    // public function edit($id = null)
    // {
    //     $templateResource = $this->TemplateResources->get($id, [
    //         'contain' => []
    //     ]);
    //     if ($this->request->is(['patch', 'post', 'put'])) {
    //         $templateResource = $this->TemplateResources->patchEntity($templateResource, $this->request->getData());
    //         if ($this->TemplateResources->save($templateResource)) {
    //             $this->Flash->success(__('The email asset has been saved.'));

    //             return $this->redirect(['action' => 'index']);
    //         }
    //         $this->Flash->error(__('The email asset could not be saved. Please, try again.'));
    //     }
    //     $this->set(compact('templateResource'));
    //     $this->set('_serialize', ['templateResource']);
    // }

    /**
     * Delete method
     *
     * @param string|null $id Email Asset id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $templateResource = $this->TemplateResources->get($this->request->getData('id'));

        if (unlink('../../avisos/resources/'. $templateResource->url)) {

            if ($this->TemplateResources->delete($templateResource)) {
                $this->Flash->success(__('El Recurso se ha eliminado correctamente.'));
            } else {
                $this->Flash->error(__('El Recurso no se ha eliminado. Por favor, intente nuevamente.'));
            }
        }
        $id = $this->request->getData()('action') == 'edit' ? $this->request->getData('message_template_id') : '';
        return $this->redirect(['controller' => 'MessageTemplates', 'action' => $this->request->getData('action'), $id]);
    }
}
