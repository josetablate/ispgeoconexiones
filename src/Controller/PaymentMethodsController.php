<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Log\Log;
use App\Controller\Component\Admin\FiscoAfipCompPdf;

/**
 * PaymentMethods Controller
 *
 * @property \App\Model\Table\PaymentMethodsTable $PaymentMethods
 */
class PaymentMethodsController extends AppController
{
    public function isAuthorized($user = null) 
    {
        if ($this->request->getParam('action') == 'getTransactions') {
            return true;
        }

        if ($this->request->getParam('action') == 'serverSideTransactions') {
            return true;
        }

        if ($this->request->getParam('action') == 'serverSideCobrodigitalTransactions') {
            return true;
        }

        if ($this->request->getParam('action') == 'serverSidePayuTransactions') {
            return true;
        }

        if ($this->request->getParam('action') == 'serverSideVisaAutoDebitTransactions') {
            return true;
        }

        if ($this->request->getParam('action') == 'serverSideMastercardAutoDebitTransactions') {
            return true;
        }

        if ($this->request->getParam('action') == 'serverSideMpTransactions') {
            return true;
        }

        if ($this->request->getParam('action') == 'serverSideCuentadigitalTransactions') {
            return true;
        }

        return parent::allowRol($user['id']);
    }
    
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('FiscoAfipCompPdf', [
             'className' => '\App\Controller\Component\Admin\FiscoAfipCompPdf'
        ]);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');
        $actions = [];

        foreach ($payment_getway->config as $key => $pg) {
            foreach ($pg->actions as $k => $atribute) {
                $actions[$key][$k] = $atribute;
            }
        }

        $this->set(compact('payment_getway', 'actions'));
        $this->set('_serialize', ['payment_getway']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $paymentMethod = $this->PaymentMethods->newEntity();
        if ($this->request->is('post')) {
            $paymentMethod = $this->PaymentMethods->patchEntity($paymentMethod, $this->request->getData());
            if ($this->PaymentMethods->save($paymentMethod)) {
                $this->Flash->success(__('Método de pago agregado.'));

                return $this->redirect(['action' => 'add']);
            } else {
                $this->Flash->error(__('Error al agregar el método de pago.'));
            }
        }
        $this->set(compact('paymentMethod'));
        $this->set('_serialize', ['paymentMethod']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Payment Method id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if ($this->request->is(['patch', 'post', 'put'])) {

            $paymentMethod = $this->PaymentMethods->get($_POST['id'], [
                'contain' => []
            ]);

            $paymentMethod = $this->PaymentMethods->patchEntity($paymentMethod, $this->request->getData());
            if ($this->PaymentMethods->save($paymentMethod)) {
                $this->Flash->success(__('Datos actualizados.'));
            } else {
                $this->Flash->error(__('Error al actualizar los datos.'));
            }
            return $this->redirect(['action' => 'index']);
        }
    }

    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);
        $paymentMethod = $this->PaymentMethods->get($_POST['id']);
        if ($this->PaymentMethods->delete($paymentMethod)) {
            $this->Flash->success(__('Método de pago eliminado .'));
        } else {
            $this->Flash->error(__('Error al eliminar el método de pago.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function transaction()
    {
        $payment_getway = $this->request->getSession()->read('payment_getway');
        $payment_methods = [];
        foreach ($payment_getway->methods as $payment_method) {
            $config = $payment_method->config;
            if ($payment_getway->config->$config->enabled) {
                $payment_methods[$payment_method->id] = $payment_method->name;
            }
        }

        $this->set(compact('payment_getway', 'payment_methods'));
        $this->set('_serialize', ['payment_getway']);
    }

    public function getTransactions()
    {
        if ($this->request->is('ajax')) {

            $response = new \stdClass();
            $response->draw = intval($this->request->getQuery('draw'));
            $response->recordsTotal = 0;
            $response->data = [];
            $response->recordsFiltered = 0;

            $response = $this->serverSideTransactions($response, $this->request->getQuery());

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function serverSideTransactions($response, $params)
    {
        if ($params['columns'][1]['search']['value'] == '') {

            $response = $this->serverSideCobrodigitalTransactions($response, $params);
            $response = $this->serverSideCuentadigitalTransactions($response, $params);
            $response = $this->serverSideMpTransactions($response, $params);
            $response = $this->serverSidePayuTransactions($response, $params);
            $response = $this->serverSideVisaAutoDebitTransactions($response, $params);
            $response = $this->serverSideMastercardAutoDebitTransactions($response, $params);
        } else {
            switch ($params['columns'][1]['search']['value']) {
                case 7:
                    $response = $this->serverSidePayuTransactions($response, $params);
                    break;

                case 99:
                    $response = $this->serverSideCobrodigitalTransactions($response, $params);
                    break;

                case 101:
                    $response = $this->serverSideMpTransactions($response, $params);
                    break;

                case 104:
                    $response = $this->serverSideCobrodigitalTransactions($response, $params);
                    break;

                case 105:
                    $response = $this->serverSideCuentadigitalTransactions($response, $params);
                    break;

                case 106:
                    $response = $this->serverSideVisaAutoDebitTransactions($response, $params);
                    break;

                case 107:
                    $response = $this->serverSideMastercardAutoDebitTransactions($response, $params);
                    break;
            }
        }

        $order_asc = function($a, $b)
        {
              if ($a->date < $b->date) {
                  return -1;
              } else if ($a->date > $b->date) {
                  return 1;
              } else {
                  return 0;
              }
        };

        usort($response->data, $order_asc);

        $newData = [];

        foreach ($response->data as $data) {
            $newData[] = $data;
        }

        $response->data = array_reverse($newData); 

        return $response;
    }

    private function serverSideCobrodigitalTransactions($response, $params)
    {
        $this->loadModel('CobrodigitalTransactions');

        $where = [];
        if ($params['where']['customer_code'] != 0) {
            $where = [
                'customer_code' => $params['where']['customer_code']
            ];
        }

        $response->recordsTotal += $this->CobrodigitalTransactions
            ->find()
            ->where($where)
            ->count();

        $cobrodigital_transactions = $this->CobrodigitalTransactions->find('ServerSideDataTransactions', [
            'params' => $params,
            'where'  => $where,
        ]);

        $response->recordsFiltered += $this->CobrodigitalTransactions->find('RecordsFilteredTransactions', [
            'params' => $params,
            'where'  => $where,
        ]);

        foreach ($cobrodigital_transactions as $cobrodigital_transaction) {
            $response->data[] = new Transactions($cobrodigital_transaction, 'cobrodigital'); 
        }

        return $response;
    }

    private function serverSidePayuTransactions($response, $params)
    {
        $this->loadModel('PayuTransactions');

        $where = [];
        if ($params['where']['customer_code'] != 0) {
            $where = [
                'customer_code' => $params['where']['customer_code']
            ];
        }

        $response->recordsTotal += $this->PayuTransactions
            ->find()
            ->where($where)
            ->count();

        $payu_transactions = $this->PayuTransactions->find('ServerSideDataTransactions', [
            'params' => $params,
            'where'  => $where,
        ]);

        $response->recordsFiltered += $this->PayuTransactions->find('RecordsFilteredTransactions', [
            'params' => $params,
            'where'  => $where,
        ]);

        foreach ($payu_transactions as $payu_transaction) {
            $response->data[] = new Transactions($payu_transaction, 'payu'); 
        }

        return $response;
    }

    private function serverSideVisaAutoDebitTransactions($response, $params)
    {
        $this->loadModel('VisaAutoDebitTransactions');

        $where = [];
        if ($params['where']['customer_code'] != 0) {
            $where = [
                'customer_code' => $params['where']['customer_code']
            ];
        }

        $response->recordsTotal += $this->VisaAutoDebitTransactions
            ->find()
            ->where($where)
            ->count();

        $visa_auto_debit_transactions = $this->VisaAutoDebitTransactions->find('ServerSideDataTransactions', [
            'params' => $params,
            'where'  => $where,
        ]);

        $response->recordsFiltered += $this->VisaAutoDebitTransactions->find('RecordsFilteredTransactions', [
            'params' => $params,
            'where'  => $where,
        ]);

        foreach ($visa_auto_debit_transactions as $visa_auto_debit_transaction) {
            $response->data[] = new Transactions($visa_auto_debit_transaction, 'visa_auto_debit'); 
        }

        return $response;
    }

    private function serverSideMastercardAutoDebitTransactions($response, $params)
    {
        $this->loadModel('MastercardAutoDebitTransactions');

        $where = [];
        if ($params['where']['customer_code'] != 0) {
            $where = [
                'customer_code' => $params['where']['customer_code']
            ];
        }

        $response->recordsTotal += $this->MastercardAutoDebitTransactions
            ->find()
            ->where($where)
            ->count();

        $mastercard_auto_debit_transactions = $this->MastercardAutoDebitTransactions->find('ServerSideDataTransactions', [
            'params' => $params,
            'where'  => $where,
        ]);

        $response->recordsFiltered += $this->MastercardAutoDebitTransactions->find('RecordsFilteredTransactions', [
            'params' => $params,
            'where'  => $where,
        ]);

        foreach ($mastercard_auto_debit_transactions as $mastercard_auto_debit_transaction) {
            $response->data[] = new Transactions($mastercard_auto_debit_transaction, 'mastercard_auto_debit'); 
        }

        return $response;
    }

    private function serverSideMpTransactions($response, $params)
    {
        $this->loadModel('MpTransactions');

        $where = [];
        if ($params['where']['customer_code'] != 0) {
            $where = [
                'customer_code' => $params['where']['customer_code']
            ];
        }

        $response->recordsTotal += $this->MpTransactions
            ->find()
            ->where($where)
            ->count();

        $mp_transactions = $this->MpTransactions->find('ServerSideDataTransactions', [
            'params' => $params,
            'where'  => $where,
        ]);

        $response->recordsFiltered += $this->MpTransactions->find('RecordsFilteredTransactions', [
            'params' => $params,
            'where'  => $where,
        ]);

        foreach ($mp_transactions as $mp_transaction) {
            $response->data[] = new Transactions($mp_transaction, 'mercadopago');
        }

        return $response;
    }

    private function serverSideCuentadigitalTransactions($response, $params)
    {
        $this->loadModel('CuentadigitalTransactions');

        $where = [];
        if ($params['where']['customer_code'] != 0) {
            $where = [
                'customer_code' => $params['where']['customer_code']
            ];
        }

        $response->recordsTotal += $this->CuentadigitalTransactions
            ->find()
            ->where($where)
            ->count();

        $cuentadigital_transactions = $this->CuentadigitalTransactions->find('ServerSideDataTransactions', [
            'params' => $params,
            'where'  => $where,
        ]);

        $response->recordsFiltered += $this->CuentadigitalTransactions->find('RecordsFilteredTransactions', [
            'params' => $params,
            'where'  => $where,
        ]);

        foreach ($cuentadigital_transactions as $cuentadigital_transaction) {
            $response->data[] = new Transactions($cuentadigital_transaction, 'cuentadigital'); 
        }

        return $response;
    }

    public function printReceipt($receipt_id)
    {
        return $this->redirect(['controller' => 'Payments', 'action' => 'printReceipt', $receipt_id]);
    }
}

class Transactions {

    public $id;
    public $date;
    public $number_transaction;
    public $customer;
    public $status;
    public $import;
    public $comment;
    public $barcode;
    public $receipt_id;
    public $receipt_number;
    public $controller;
    public $payment_getway_id;

    public $type_transactiom;

    public function __construct($data, $type) {

        switch ($type) {

            case 'cobrodigital':

                $this->id                 = $data->id;
                $this->date               = $data->fecha;
                $this->number_transaction = $data->id_transaccion;
                $this->payment_getway_id  = $data->payment_getway_id ? $data->payment_getway_id : 'Cobro Digital';
                $this->status             = $data->estado;
                $this->comment            = $data->comments;
                $this->customer           = $data->customer;
                $this->import             = $data->bruto;
                $this->barcode            = $data->codigo_barras;
                $this->receipt_number     = $data->receipt_number;
                $this->receipt_id         = $data->receipt_id;
                $this->type_transactiom   = $type;
                $this->controller         = 'CobroDigital';
                break;

            case 'payu':

                $this->id                 = $data->id;
                $this->date               = $data->fecha_pago;
                $this->number_transaction = $data->id_orden;
                $this->payment_getway_id  = $data->payment_getway_id ? $data->payment_getway_id : 'Payu';
                $this->status             = $data->estado;
                $this->comment            = $data->comments;
                $this->customer           = $data->customer;
                $this->import             = $data->valor;
                $this->barcode            = $data->numero_pago;
                $this->receipt_number     = $data->receipt_number;
                $this->receipt_id         = $data->receipt_id;
                $this->type_transactiom   = $type;
                $this->controller         = 'Payu';
                break;

            case 'visa_auto_debit':

                $this->id                 = $data->id;
                $this->date               = $data->date;
                $this->number_transaction = $data->id;
                $this->payment_getway_id  = $data->payment_getway_id ? $data->payment_getway_id : 'Visa Débito Automático';
                $this->status             = $data->status;
                $this->comment            = $data->comment;
                $this->customer           = $data->customer;
                $this->import             = $data->total;
                $this->barcode            = $data->card_number;
                $this->receipt_number     = $data->receipt_number;
                $this->receipt_id         = $data->receipt_id;
                $this->type_transactiom   = $type;
                $this->controller         = 'VisaAutoDebit';
                break;

            case 'mastercard_auto_debit':

                $this->id                 = $data->id;
                $this->date               = $data->date;
                $this->number_transaction = $data->id;
                $this->payment_getway_id  = $data->payment_getway_id ? $data->payment_getway_id : 'MasterCard Débito Automático';
                $this->status             = $data->status;
                $this->comment            = $data->comment;
                $this->customer           = $data->customer;
                $this->import             = $data->total;
                $this->barcode            = $data->card_number;
                $this->receipt_number     = $data->receipt_number;
                $this->receipt_id         = $data->receipt_id;
                $this->type_transactiom   = $type;
                $this->controller         = 'MastercardAutoDebit';
                break;

            case 'mercadopago':

                $this->id                 = $data->id;
                $this->date               = $data->date_created;
                $this->number_transaction = $data->payment_id;
                $this->payment_getway_id  = $data->payment_getway_id ? $data->payment_getway_id : 'Mercado Pago';
                $this->status             = $data->local_status;
                $this->comment            = $data->comment;
                $this->customer           = $data->customer;
                $this->import             = $data->total_paid_amount;
                $this->barcode            = '';
                $this->receipt_number     = $data->receipt_number;
                $this->receipt_id         = $data->receipt_id;
                $this->type_transactiom   = $type;
                $this->controller         = 'MercadoPago';
                break;

            case 'cuentadigital':

                $this->id                 = $data->id;
                $this->date               = $data->fecha;
                $this->number_transaction = $data->id;
                $this->payment_getway_id  = $data->payment_getway_id ? $data->payment_getway_id : 'Cuenta Digital';
                $this->status             = $data->estado;
                $this->comment            = $data->comentario;
                $this->customer           = $data->customer;
                $this->import             = $data->neto_original;
                $this->barcode            = $data->barcode;
                $this->receipt_number     = $data->receipt_number;
                $this->receipt_id         = $data->receipt_id;
                $this->type_transactiom   = $type;
                $this->controller         = 'Cuentadigital';
                break;
        }
    }
}
