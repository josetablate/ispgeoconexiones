<?php
namespace App\Controller;

use Cake\Core\Configure;
use App\Controller\AppController;
use PEAR2\Net\ControllerOS;
use PEAR2\Net\ControllerOS\SocketException;
use PEAR2\Net\ControllerOS\DataFlowException;
use Cidr;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;
use Cake\Event\Event;

/**
 * Connections Controller
 * ABM de conexion en la base y en el controller.  
 *
 * @property \App\Model\Table\ConnectionsTable $Connections
 */
class ConnectionsGraphicController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('IntegrationRouter', [
            'className' => '\App\Controller\Component\Integrations\IntegrationRouter'
        ]);

        $this->loadModel('Connections');
        $this->loadModel('RealTimeSpeed');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function isAuthorized($user = null)
    {
        if ($this->request->getParam('action') == 'restartRealTimeGraph') {
            return true;
        }

        if ($this->request->getParam('action') == 'updateRealTimeGraph') {
            return true;
        }

        if ($this->request->getParam('action') == 'get_string_between') {
            return true;
        }

        if ($this->request->getParam('action') == 'getHmtl') {
            return true;
        }

        if ($this->request->getParam('action') == 'grab_image') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function queueRealTimeGraph($id)
    {
        $realTimeSpeedActive = $this->RealTimeSpeed->find()->where(['connection_id' => $id])->first();

        if (!$realTimeSpeedActive) {

            $command = ROOT . "/bin/cake RealTimeSpeed $id > /dev/null &";
            exec($command, $output); 
        }

        $connection = $this->Connections->get($id, ['contain' => ['Controllers', 'Customers', 'Services']]);

        $connection = $this->IntegrationRouter->getConnection($connection);

        $this->set(compact('connection'));
        $this->set('_serialize', ['connection']);
    }

    public function restartRealTimeGraph()
    {
        if ($this->request->is('Ajax')) {//Ajax Detection

            $id = $this->request->input('json_decode')->connection_id;

            $realTimeSpeedActive = $this->RealTimeSpeed->find()->where(['connection_id' => $id])->first();

            if (!$realTimeSpeedActive) {

                $command = ROOT . "/bin/cake RealTimeSpeed $id > /dev/null &";
                exec($command, $output); 
            }

            $this->set('response', true);
        }
    }

    public function UpdateRealTimeGraph()
    {
         if ($this->request->is('Ajax')) {//Ajax Detection

            $connection_id = $this->request->input('json_decode')->connection_id;
            $rates = $this->RealTimeSpeed->find()->where(['connection_id' => $connection_id])->order(['id' => 'DESC']);
            $this->set('rates', $rates);
         }
    }

    public function queueHistoryGraph($id)
    {
        $connection = $this->Connections->get($id, [
            'contain' => ['Customers', 'Controllers', 'Services', 'Users', 'Areas']
        ]);

        $connection = $this->IntegrationRouter->getConnection($connection);       

        $urlController = "https://" . $connection->controller->connect_to;

        if ($connection->controller->port_grap) {
           $urlController .= ':' . $connection->controller->port_grap; 
        }  

        $urldaily = $urlController . "/graphs/queue/" . $connection->queue->name . "/daily.gif";
        $this->grab_image($urldaily, WWW_ROOT . 'queues_graphs/daily-' . $connection->id . '.gif');

        $urlweekly = $urlController . "/graphs/queue/".$connection->queue->name . "/weekly.gif";   
        $this->grab_image($urlweekly, WWW_ROOT . 'queues_graphs/weekly-' . $connection->id . '.gif');

        $urlmonthly = $urlController . "/graphs/queue/".$connection->queue->name . "/monthly.gif"; 
        $this->grab_image($urlmonthly, WWW_ROOT . 'queues_graphs/monthly-' . $connection->id . '.gif');

        $urlyearly = $urlController . "/graphs/queue/".$connection->queue->name . "/yearly.gif";      
        $this->grab_image($urlyearly, WWW_ROOT . 'queues_graphs/yearly-' . $connection->id . '.gif');

        $html = $this->getHmtl($urlController . "/graphs/queue/".$connection->queue->name . "/");

        if (!$html) {
            $this->Flash->warning(__('No se pudo obtener las gráficas. Verifique la configración del controlador de esta conexión..'));

            $data_error = new \stdClass; 
            $data_error->urlController = $urlController;

            $this->loadModel('ErrorLog');
            $log = $this->ErrorLog->newEntity();
            $log->user_id = $this->request->getSession()->read('Auth.User')['id'];
            $log->type = 'Error';
            $log->msg = __('No se pudo obtener las gráficas');
            $log->data = json_encode($data_error, JSON_PRETTY_PRINT);
            $log->event = 'ConnectionsGraphic.Error';
            $this->ErrorLog->save($log);

            return $this->redirect(['controller' => 'connections',  'action' => 'index']);
        }

        $html = str_replace("Average", "Promedio", $html);
        $html = str_replace("Current", "Actual", $html);

        $box = explode('<div class="box">', $html);

        $caption = [];
        $caption['daily'] = trim($this->get_string_between($box[1], '<p>', '</p>'));
        $caption['daily'] = str_replace("<em>", "", $caption['daily']);
        $caption['daily'] = str_replace("</em>", "", $caption['daily']);

        $caption['weekly'] = trim($this->get_string_between($box[2], '<p>', '</p>'));
        $caption['weekly'] = str_replace("<em>", "", $caption['weekly']);
        $caption['weekly'] = str_replace("</em>", "", $caption['weekly']);

        $caption['monthly'] = trim($this->get_string_between($box[3], '<p>', '</p>'));
        $caption['monthly'] = str_replace("<em>", "", $caption['monthly']);
        $caption['monthly'] = str_replace("</em>", "", $caption['monthly']);

        $caption['yearly'] = trim($this->get_string_between($box[4], '<p>', '</p>'));
        $caption['yearly'] = str_replace("<em>", "", $caption['yearly']);
        $caption['yearly'] = str_replace("</em>", "", $caption['yearly']);

        $this->set(compact('connection', 'caption'));
        $this->set('_serialize', ['connection', 'caption']);
    }

    private function get_string_between($string, $start, $end)
    {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    private function getHmtl($url)
    {
        $ch = curl_init ($url);

        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        $raw = curl_exec($ch);

        $code = curl_getinfo ($ch, CURLINFO_RESPONSE_CODE);  

        curl_close ($ch);

        if ($code != 200) {              
             return false;  
        }   

        return $raw;       
    }

    private function grab_image($url,$saveto)
    {
        $ch = curl_init ($url);

        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        $raw = curl_exec($ch);
        curl_close ($ch);
        if (file_exists($saveto)) {
            unlink($saveto);
        }
        $fp = fopen($saveto, 'x+');
        fwrite($fp, $raw);
        fclose($fp);
    }
}
