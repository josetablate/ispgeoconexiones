<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
/**
 * SmsGetway component
 */
class TelegramController extends AppController
{
    public function isAuthorized($user = null) 
    {
        return TRUE;
    }

    public function sendMessage($message, $chat_id = NULL)
    {
        $paraments = $this->request->getSession()->read('paraments');
        $telegram = $paraments->telegram;
        $token = $telegram->token;

        if ($chat_id == NULL) {
            $chat_id = $telegram->chat_id;
        }

        $data = [
            'chat_id' => $chat_id,
            'text'    => $message
        ];

        $url = $telegram->url;
        $url = str_replace("%token%", $token, $url);

        $response = file_get_contents($url . http_build_query($data));
        return $response;
    }
}
