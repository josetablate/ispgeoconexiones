<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Event\Event;

/**
 * ErrorLog Controller
 *
 * @property \App\Model\Table\ErrorLogTable $ErrorLog
 *
 * @method \App\Model\Entity\ErrorLog[] paginate($object = null, array $settings = [])
 */
class ErrorLogController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }

    public function isAuthorized($user = null) 
    {
        if ($this->request->getParam('action') == 'getCounterNewError') {
            return true;
        }

        if ($this->request->getParam('action') == 'getErrorLogQuickView') {
            return true;
        }

        if ($this->request->getParam('action') == 'getErrorLog') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {}

    public function getErrorLog()
    {
        if ($this->request->is('Ajax')) { //Ajax Detection 
            $response = new \stdClass();
            $response->draw = intval($this->request->getQuery('draw'));

            $user_id = $this->Auth->user()['id'];
            $where = [];

            if ($user_id != 100) {
                $response->recordsTotal = $this->ErrorLog->find()->where(['user_id' => $user_id])->count();
                $where['user_id'] = $user_id;
            } else {
                $response->recordsTotal = $this->ErrorLog->find()->count();
            }

            $response->recordsTotal = $this->ErrorLog->find()->count();

            $response->data = $this->ErrorLog->find('ServerSideData', [
                'params' => $this->request->getQuery(),
                'where'  => $where
            ]);

            $response->recordsFiltered = $this->ErrorLog->find('RecordsFiltered', [
                'params' => $this->request->getQuery(),
                'where'  => $where
            ]);

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function getErrorLogQuickView()
    {
        if ($this->request->is('Ajax')) { //Ajax Detection 
            $response = new \stdClass();
            $response->draw = intval($this->request->getQuery('draw'));

            $user_id = $this->Auth->user()['id'];
            $where = [];

            if ($user_id != 100) {

                $response->recordsTotal = $this->ErrorLog->find()->where(['user_id' => $user_id, 'ErrorLog.created >' => Time::now()->modify('-1 day')])->count();
                $where['user_id'] = $user_id;
                $where['ErrorLog.created >'] = Time::now()->modify('-1 day');

            } else {

                $response->recordsTotal = $this->ErrorLog->find()->where(['ErrorLog.created >' => Time::now()->modify('-1 day')])->count();
                $where['ErrorLog.created >'] = Time::now()->modify('-1 day');
            }

            $response->recordsTotal = $this->ErrorLog->find()->count();

            $response->data = $this->ErrorLog->find('ServerSideDataQuickView', [
                'params' => $this->request->getQuery(),
                'where'  => $where
            ]);

            $response->recordsFiltered = $this->ErrorLog->find('RecordsFilteredQuickView',  [
                'params' => $this->request->getQuery(),
                'where'  => $where
            ]);

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function getCounterNewError()
    {
        $this->getEventManager()->off($this->Csrf);

        if ($this->request->is('Ajax')) { //Ajax Detection 
            $user_id = $this->Auth->user()['id'];

            $error_new_count = $this->ErrorLog->find()->where(['view IS' => null, 'user_id' => $user_id])->count();

            $this->set('error_new_count', $error_new_count);
        }
    }

    public function viewAll()
    {
        $this->ErrorLog->updateAll(['view' => Time::now()], ['view IS' => null]);

        return $this->redirect(['action' => 'index']);
    }

    public function markView($id)
    {
        $error = $this->ErrorLog->get($id);
        $error->view = Time::now();
        $this->ErrorLog->save($error);

        return $this->redirect(['action' => 'index']);
    }

    public function deleteAll()
    {
        $this->ErrorLog->deleteAll(['view IS NOT' => null]);

        return $this->redirect(['action' => 'index']);
    }
}
