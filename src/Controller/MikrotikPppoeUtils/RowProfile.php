<?php
namespace App\Controller\MikrotikPppoeUtils;

use App\Controller\Utils\Attr;

class RowProfile {
   
  
    public $marked_to_delete;
    
    public $id;
    public $api_id;
    public $name;
    public $local_address;
    public $dns_server;
    public $rate_limit_string;
    public $queue_type;

    public $other;
    
    public function __construct($profile, $side) {
        
        $this->other = $profile['other'];
        
        $this->marked_to_delete = false;
        
        if($side == 'A'){
            $this->id = $profile['id'];
        }
        
        $this->api_id = new Attr($profile['api_id'], true);
        $this->name = new Attr($profile['name'], true);
        $this->local_address = new Attr($profile['local_address'], true);
        $this->dns_server = new Attr($profile['dns_server'], true);
        $this->rate_limit_string = new Attr($profile['rate_limit_string'], true);
        $this->queue_type = new Attr($profile['queue_type'], true);
    }
    
    
    public function setNew(){
        
        $this->marked_to_delete = true;
        
        $this->api_id->state = false;
        $this->name->state = false;
        $this->local_address->state = false;
        $this->dns_server->state = false;
        $this->rate_limit_string->state = false;
        $this->queue_type->state = false;
    }
    
}
