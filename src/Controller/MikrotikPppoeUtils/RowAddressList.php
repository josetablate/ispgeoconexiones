<?php
namespace App\Controller\MikrotikPppoeUtils;

use App\Controller\Utils\Attr;

class RowAddressList {
   
  
    public $marked_to_delete;
    
    public $id;
    public $api_id;
    public $list;
    public $address;
    public $comment;
    
    public $other;
    
    public function __construct($addressList, $side) {
        
        $this->other = $addressList['other'];
        
        $this->marked_to_delete = false;
        
        if($side == 'A'){
            $this->id = $addressList['id'];
        }
        
        $this->api_id = new Attr($addressList['api_id'], true);
        $this->list = new Attr($addressList['list'], true);
        $this->address = new Attr($addressList['address'], true);
        $this->comment = new Attr($addressList['comment'], true);
     
    }
    
    
    public function setNew(){
        
        $this->marked_to_delete = true;
        
        $this->api_id->state = false;
        $this->list->state = false;
        $this->address->state = false;
        $this->comment->state = false;
    }
    
}
