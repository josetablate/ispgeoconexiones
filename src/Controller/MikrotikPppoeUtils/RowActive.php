<?php
namespace App\Controller\MikrotikPppoeUtils;

use App\Controller\Utils\Attr;

class RowActive{
   
   
    public $active_id;
    public $connection_id;
    
    public $marked_to_delete;
    
    public $api_id;
    public $name;
    
    public $remote_address;
    public $address;
    
    public $comment;
    
    public $other;
    
    public function __construct($active, $side) {
        
        $this->other = $active['other'];
        
        $this->marked_to_delete = false;
        
        if($side == 'A'){
            $this->active_id = $active['id'];
            $this->connection_id = $active['connection']['id'];
        }
        
        $this->api_id = new Attr($active['api_id'], true);
        $this->name = new Attr($active['name'], true);
        $this->comment = new Attr($active['comment'], true);
        
        if($side == 'A'){
            $this->remote_address = new Attr(long2ip($active['remote_address']), true);
        }else{
            $this->address = new Attr($active['address'], true);
        }
        
    
    }
    
    
    public function setNew(){
        
        $this->marked_to_delete = true;
        
        $this->api_id->state = false;
        $this->name->state = false;
        
        if($this->remote_address){
            $this->remote_address->state = false;
        }
        
        if($this->address){
            $this->address->state = false;
        }
        
        $this->address->state = false;
        $this->comment->state = false;

    }
    
}
