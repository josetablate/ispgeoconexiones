<?php
namespace App\Controller\MikrotikPppoeUtils;

use App\Controller\Utils\Attr;

class RowSecret {   
   
    public $secret_id;
    public $connection_id;
    
    public $marked_to_delete;
    
    public $api_id;
    public $name;
    public $password;
    public $service;
    public $profile;
    public $remote_address;
    public $comment;
    public $caller_id;
    
    public $other;
    
    public function __construct($secret, $side) {
        
        $this->other = $secret['other'];
        
        $this->marked_to_delete = false;
        
        if($side == 'A'){
            $this->secret_id = $secret['id'];
            $this->connection_id = $secret['connection']['id'];
        }
       
        $this->api_id = new Attr($secret['api_id'], true);
        $this->service = new Attr($secret['service'], true);
        $this->remote_address = new Attr($secret['remote_address'], true);
        $this->caller_id = new Attr($secret['caller_id'], true);
        
        if($side == 'A'){
            $this->profile = new Attr($secret['profile']['name'], true);
            $this->name = new Attr($secret['name'], true);
            $this->password = new Attr($secret['password'], true);
            $this->comment = new Attr($secret['comment'], true);
        }else{
            $this->profile = new Attr($secret['profile'], true);
            $this->name = new Attr($this->convert_to($secret['name'], "UTF-8"), true);
            $this->password = new Attr($this->convert_to($secret['password'], "UTF-8"), true);
            $this->comment = new Attr($this->convert_to($secret['comment'], "UTF-8"), true);
        }
    
    }
    
    
    public function setNew(){
        
        $this->marked_to_delete = true;
        
        $this->api_id->state = false;
        $this->name->state = false;
        $this->password->state = false;
        $this->service->state = false;
        $this->profile->state = false;
        $this->remote_address->state = false;
        $this->comment->state = false;
        $this->caller_id->state = false;
    }
    
    private function convert_to( $source, $target_encoding )  {
        
        if($source != ''){
            
            // detect the character encoding of the incoming file
            $encoding = mb_detect_encoding( $source, "auto" );
               
            // escape all of the question marks so we can remove artifacts from
            // the unicode conversion process
            $target = str_replace( "?", "[question_mark]", $source );
               
            // convert the string to the target encoding
            $target = mb_convert_encoding( $target, $target_encoding, $encoding);
               
            // remove any question marks that have been introduced because of illegal characters
            $target = str_replace( "?", "", $target );
               
            // replace the token string "[question_mark]" with the symbol "?"
            $target = str_replace( "[question_mark]", "?", $target );
           
            return $target;
        }
        
         return $source;
    }
    
}
