<?php
namespace App\Controller\MikrotikPppoeUtils;

use App\Controller\Utils\Attr;

class RowPppoeServer {
 
    public $id;
    public $controller_id;
    
    public $marked_to_delete;
    
    public $api_id;
    public $service_name;
    public $interface;
    public $disabled;
    
    public $other;
    
    
    public function __construct($pppoeserve, $side) {
        
        $this->other = $pppoeserve['other'];
        
        $this->marked_to_delete = false;
        
        if($side == 'A'){
            $this->id = $pppoeserve['id'];
            $this->controller_id = $pppoeserve['controller_id'];
        }
  
        
        $this->api_id = new Attr($pppoeserve['api_id'], true);
        $this->service_name = new Attr($pppoeserve['service_name'], true);
        $this->interface = new Attr($pppoeserve['interface'], true);
        $this->disabled = new Attr($pppoeserve['disabled'], true);
    
    }
    
    
    public function setNew(){
        
        $this->marked_to_delete = true;
        
        $this->api_id->state = false;
        $this->service_name->state = false;
        $this->interface->state = false;
        $this->disabled->state = false;
    }
    
}
