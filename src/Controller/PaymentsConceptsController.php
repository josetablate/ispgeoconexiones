<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PaymentsConcepts Controller
 *
 * @property \App\Model\Table\PaymentsConceptsTable $PaymentsConcepts
 *
 * @method \App\Model\Entity\PaymentsConcept[] paginate($object = null, array $settings = [])
 */
class PaymentsConceptsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $paymentsConcepts = $this->paginate($this->PaymentsConcepts);

        $this->set(compact('paymentsConcepts'));
        $this->set('_serialize', ['paymentsConcepts']);
    }

    /**
     * View method
     *
     * @param string|null $id Payments Concept id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $paymentsConcept = $this->PaymentsConcepts->get($id, [
            'contain' => []
        ]);

        $this->set('paymentsConcept', $paymentsConcept);
        $this->set('_serialize', ['paymentsConcept']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $paymentsConcept = $this->PaymentsConcepts->newEntity();
        if ($this->request->is('post')) {
            $paymentsConcept = $this->PaymentsConcepts->patchEntity($paymentsConcept, $this->request->getData());
            if ($this->PaymentsConcepts->save($paymentsConcept)) {
                $this->Flash->success(__('The payments concept has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The payments concept could not be saved. Please, try again.'));
        }
        $this->set(compact('paymentsConcept'));
        $this->set('_serialize', ['paymentsConcept']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Payments Concept id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $paymentsConcept = $this->PaymentsConcepts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $paymentsConcept = $this->PaymentsConcepts->patchEntity($paymentsConcept, $this->request->getData());
            if ($this->PaymentsConcepts->save($paymentsConcept)) {
                $this->Flash->success(__('The payments concept has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The payments concept could not be saved. Please, try again.'));
        }
        $this->set(compact('paymentsConcept'));
        $this->set('_serialize', ['paymentsConcept']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Payments Concept id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $paymentsConcept = $this->PaymentsConcepts->get($id);
        if ($this->PaymentsConcepts->delete($paymentsConcept)) {
            $this->Flash->success(__('The payments concept has been deleted.'));
        } else {
            $this->Flash->error(__('The payments concept could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
