<?php
namespace App\Controller;

use App\Controller\IspController;
use Cake\Datasource\ConnectionManager;
use Cake\Filesystem\File;
use Cake\Core\Configure;
use Cake\I18n\Time;




class IspControllerController extends IspController
{
    public function initialize()
    {
        parent::initialize();   

        $this->loadModel('Controllers');
    }   

    public function index() {}

    public function add()
    {
        $controller = $this->Controllers->newEntity();

        if ($this->request->is('post')) {

            $controllerExist =  $this->Controllers->find()
                 ->where([
                      'name' =>  $this->request->getData('name'),
                      'deleted' => false
                 ])
                 ->first();

            if ($controllerExist) {

                $this->Flash->warning(__('Ya existe otro controlador con este nombre.'));

                return $this->redirect(['action' => 'add']);
            }

            $controller->last_control_diff = Time::now();

            $controller = $this->Controllers->patchEntity($controller, $this->request->getData());

            // $controller->integration = true;

            if ($this->Controllers->save($controller)) {

                if (!$this->IntegrationRouter->addController($controller)) {

                    $this->Controllers->delete($controller);

                    return $this->redirect(['action' => 'add']); 
                }               
                

                $this->Flash->success(__('El controlador se agrego correctamente.'));

                return $this->redirect(['action' => 'index']);

            } else {

                $this->Flash->error(__('No se pudo agregar el controlador'));
            }
        }

        $controller->port = 8728;
        $controller->port_ssl = 8729;

        $this->set(compact('controller'));
        $this->set('_serialize', ['controller']);
    }

    public function cloneController()
    {
        $this->loadModel('Controllers');
        $controller = $this->Controllers->get($_POST['id']);

        if ($this->IntegrationRouter->cloneController($controller)) {
           $this->Flash->success(__('EL controlador de duplicó correctamente.'));
        } else {
           $this->Flash->error(__('Error al intentar duplicar el controlador {0}.', $controller->name));
        }
        
        return $this->redirect(['action' => 'index']);
    }

    public function edit() {}

    public function config($id) {} 

    public function sync($id) {} 

    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);

        $id = $_POST['id'];

        //verificar referencias y si no hay eliminar

        $this->loadModel('Connections');
        $count = $this->Connections->find()->where(['controller_id' => $id, 'deleted' => false])->count();

        if ($count > 0) {
            $this->Flash->warning(__('No se puede Eliminar. Existen {0} Conexiones relacionados a este controlador. ', $count));
            return $this->redirect(['action' => 'index']);
        }

        $this->loadModel('Controllers');
        $inController = $this->Controllers->get($_POST['id']);
        $inController->deleted = true;

        if ($this->Controllers->save($inController)) {
            $this->Flash->success(__('El controlador se elimino correctamente.'));
        } else {
            $this->Flash->error(__('No se pudo eliminar el controlador.'));
        }

        return $this->redirect(['controller' => 'isp-controller', 'action' => 'index']);
    }     

    public function getTrademarksByTemplateAjax()
    {
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $template_id = $this->request->input('json_decode')->template_id;
            $data['trademarks'] = $this->getTrademarksByTemplate($template_id);
            $this->set('data', $data);
        }
    }

    public function getStatus($controller_id)
    {
        $controller = $this->Controllers->get($controller_id);      

        if ($this->IntegrationRouter->getStatus($controller)) {
            $this->Flash->success(__('Conexión Exitosa.'));
        } else {
            $this->Flash->warning(__('No se pudo conectar.'));
        }


        return $this->redirect(['action' => 'index']); 
    }

    public function getTemplates()
    {
        if ($this->request->is('ajax')) {

            $templates = $this->templates;

            $this->set(compact('templates'));
            $this->set('_serialize', ['templates']);
        }
    }    

    public function getControllers()
    {
        $this->loadModel('Connections');       

        $controllers = $this->Controllers->find()->where(['deleted' => false]);

        foreach ($controllers as $controller) {
            $controller->connections_amount = $this->Connections->find()->where(['controller_id' => $controller->id, 'deleted' => false])->count();
            $controller->template_name = $this->getTemplateName($controller->template);
            $controller->trademark_name = $this->getTrademarksByTemplate($controller->template)[$controller->trademark];
        }

        $this->set(compact('controllers'));
        $this->set('_serialize', ['controllers']);
    }      

    public function checkSync($id) {}

    //plans  

    public function getPlansByController() {} 
    public function addPlan() {} 
    public function editPlan() {} 
    public function deletePlan() {}  

    //ip excluida    

    public function getIpExcludedByController() {} 
    public function deleteIPExcluded() {} 
    public function addIPExcluded() {} 

    //protected

    protected function createIPTable($controller)
    {
        //generar el csr con el shell
        $createIPTableShell = new \App\Shell\CreateIPTableShell;

        $controller->port_ip_table = 9000 + $controller->id;

        $this->loadModel('Controllers');
        $this->Controllers->save($controller);        

        $createIPTableShell->add($controller->port_ip_table, $controller->conect_to, 8291);
    }

    protected function getTemplateName($template_id)
    {
        foreach ($this->templates as $template) {
            if ($template['value'] == $template_id) {
                return $template['name']; 
            }
        }
        return '';
    }

    protected function getTrademarksByTemplate($template_id)
    {
        foreach ($this->templates as $template) {
            if ($template['value'] == $template_id) {
                return $template['trademarks']; 
            }
        }
        return '';
    }     

    protected function updateFile($data, $name, $t)
    {
        $json = new \stdClass;
        $json->data = $data;

        if (Configure::read('debug')) {
            $json = json_encode($json, JSON_PRETTY_PRINT);
        } else {
            $json = json_encode($json);
        }   

        $this->getLastErrorJson($json);

        $file = new File(WWW_ROOT . "sync/$t/$name.json", true, 0775);
        $file->write($json, 'w+', true);
        $file->close();
    } 

    protected function getLastErrorJson($data)
    {
        $error = false;

        switch(json_last_error()) {
            case JSON_ERROR_NONE:
                $error = false;
            break;
            case JSON_ERROR_DEPTH:
                $error =  ' - Excedido tamaño máximo de la pila';
            break;
            case JSON_ERROR_STATE_MISMATCH:
                $error =  ' - Desbordamiento de buffer o los modos no coinciden';
            break;
            case JSON_ERROR_CTRL_CHAR:
                $error =  ' - Encontrado carácter de control no esperado';
            break;
            case JSON_ERROR_SYNTAX:
                $error =  ' - Error de sintaxis, JSON mal formado';
            break;
            case JSON_ERROR_UTF8:
                $error =  ' - Caracteres UTF-8 malformados, posiblemente codificados de forma incorrecta';
            break;
            default:
                $error =  ' - Error desconocido';
            break;
        }

        if ($error) {

            $this->loadModel('ErrorLog');
            $log = $this->ErrorLog->newEntity();
            $log->user_id = $this->request->getSession()->read('Auth.User')['id'];
            $log->type = 'Error';
            $log->msg = __($error);
            $log->data = null;
            $log->event = 'IspControllerController.getLastErrorJson';
            $this->ErrorLog->save($log);
        }
            
    }
}
