<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Event\Event;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use FPDF;
use App\Controller\Component\Ticket\CreateTicketPdf;

/**
 * Tickets Controller
 *
 * @property \App\Model\Table\TicketsTable $Tickets
 */
class TicketsController extends AppController
{
    public static $TECHNICAL_SUPPORT_CATEGORY = 0;
    public static $INSTALLATIONS_CATEGORY = 1;
    public static $INVOICING_CATEGORY = 2;
    public static $SALE_CATEGORY = 3;

    public static $TICKETS_CATEGORIES = [
        '' => 'Seleccione Categoría',
        0 => 'Soporte Técnico',
        1 => 'Instalación',
        2 => 'Facturación',
        3 => 'Venta',
    ];

    public static $AVAILABLE_STATUS = 0;
    public static $ASSIGNED_STATUS = 1;
    public static $FINISHED_STATUS = 2;

    public static $TICKETS_STATUS = [
        0 => 'Disponible',
        1 => 'Asignado',
        2 => 'Finalizado',
    ];

    public static $TICKETS_STATUS_COLORS = [
        0 => 'warning',
        1 => 'danger',
        2 => 'success',
    ];

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('StatusTickets');
        $this->loadModel('CategoriesTickets');
        $this->loadModel('Labels');
        $this->loadComponent('CreateTicketPdf', [
            'className' => '\App\Controller\Component\Ticket\CreateTicketPdf'
        ]);
    }

    public function isAuthorized($user = null)
    {
        if ($this->request->getParam('action') == 'indexCard') {
            return true;
        }

        if ($this->request->getParam('action') == 'addComment') {
            return true;
        }

        if ($this->request->getParam('action') == 'userAssignment') {
            return true;
        }

        if ($this->request->getParam('action') == 'getTicketsByCustomer') {
            return true;
        }

        if ($this->request->getParam('action') == 'updateStatusTicket') {
            return true;
        }

        if ($this->request->getParam('action') == 'getTickets') {
            return true;
        }

        if ($this->request->getParam('action') == 'addDigitalSignature') {
            return true;
        }

        if ($this->request->getParam('action') == 'config') {
            return true;
        }

        if ($this->request->getParam('action') == 'getStatusTickets') {
            return true;
        }

        if ($this->request->getParam('action') == 'getCategoriesTickets') {
            return true;
        }

        if ($this->request->getParam('action') == 'changeCategory') {
            return true;
        }

        if ($this->request->getParam('action') == 'changeStatus') {
            return true;
        }

        if ($this->request->getParam('action') == 'changeStartTask') {
            return true;
        }

        if ($this->request->getParam('action') == 'changeStartTaskFromCalendar') {
            return true;
        }

        if ($this->request->getParam('action') == 'changeStartTaskFromConfig') {
            return true;
        }

        if ($this->request->getParam('action') == 'addPhoto') {
            return true;
        }

        if ($this->request->getParam('action') == 'getTicketsFromTable') {
            return true;
        }

        if ($this->request->getParam('action') == 'editTitle') {
            return true;
        }

        if ($this->request->getParam('action') == 'editDescription') {
            return true;
        }

        if ($this->request->getParam('action') == 'addFromCustomer') {
            return true;
        }

        if ($this->request->getParam('action') == 'changeAreas') {
            return true;
        }

        if ($this->request->getParam('action') == 'changeMasiveStatus') {
            return true;
        }

        if ($this->request->getParam('action') == 'getTicketsFromAddTicket') {
            return true;
        }

        if ($this->request->getParam('action') == 'addCustomer') {
            return true;
        }

        if ($this->request->getParam('action') == 'clearCustomer') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function beforeRender(Event $event) 
    {
        parent::beforeRender($event);

        $tickets_status = $this->StatusTickets->find()->where(['deleted' => false])->order(['ordering' => 'ASC'])->toArray();
        $last_status = array_shift($tickets_status);
        array_push($tickets_status, $last_status);
        $tickets_categories = $this->CategoriesTickets->find()->where(['deleted' => false])->order(['ordering' => 'ASC'])->toArray();

        $this->set('tickets_categories', $tickets_categories);
        $this->set('tickets_status', $tickets_status);
    }

    public function indexTable()
    {
        $parament = $this->request->getSession()->read('paraments');

        $this->loadModel('Users');
        $users_list = $this->Users->find('list')->where(['deleted' => 0, 'id NOT IN' => $parament->system->users]);
        $users = [];

        $users[0] = "Sin asignar";
        foreach ($users_list as $key => $user) {
            $users[$key] = $user;
        }

        $this->loadModel('Controllers');
        $controllers = $this->Controllers->find('list')->where(['deleted' => 0]);

        $this->loadModel('Services');
        $services = $this->Services->find('list')->where(['deleted' => 0]);

        $this->loadModel('Areas');
        $areas = $this->Areas->find('list')->where(['deleted' => 0])->toArray();

        $tickets_status = $this->StatusTickets->find()->where(['deleted' => false])->order(['ordering' => 'ASC'])->toArray();
        $last_status = array_shift($tickets_status);
        array_push($tickets_status, $last_status);
        $status = [];
        foreach ($tickets_status as $st) {
            $status_ti[$st->name] = $st->name;
        }

        $tickets_categories = $this->CategoriesTickets->find()->where(['deleted' => false])->order(['ordering' => 'ASC'])->toArray();
        $categories = [];
        foreach ($tickets_categories as $cat) {
            $categories[$cat->name] = $cat->name;
        }

        $this->set(compact('users', 'controllers', 'services', 'status_ti', 'categories', 'areas'));
    }

    public function archived()
    {
        $parament = $this->request->getSession()->read('paraments');

        $this->loadModel('Users');
        $users_list = $this->Users
            ->find('list')
            ->where([
                'deleted'   => 0, 
                'id NOT IN' => $parament->system->users
            ]);
        $users = [];

        $users[0] = "Sin asignar";
        foreach ($users_list as $key => $user) {
            $users[$key] = $user;
        }

        $this->loadModel('Controllers');
        $controllers = $this->Controllers
            ->find('list')
            ->where([
                'enabled' => TRUE,
                'deleted' => 0
            ]);

        $this->loadModel('Services');
        $services = $this->Services->find('list')->where(['deleted' => 0]);

        $this->loadModel('Areas');
        $areas = $this->Areas->find('list')->where(['deleted' => 0])->toArray();

        $tickets_status = $this->StatusTickets->find()->where(['deleted' => false])->order(['ordering' => 'ASC'])->toArray();
        $last_status = array_shift($tickets_status);
        array_push($tickets_status, $last_status);
        $status = [];
        foreach ($tickets_status as $st) {
            $status_ti[$st->name] = $st->name;
        }

        $tickets_categories = $this->CategoriesTickets->find()->where(['deleted' => false])->order(['ordering' => 'ASC'])->toArray();
        $categories = [];
        foreach ($tickets_categories as $cat) {
            $categories[$cat->name] = $cat->name;
        }

        $this->set(compact('users', 'controllers', 'services', 'status_ti', 'categories', 'areas'));
    }

    public function indexCard()
    {
        $parament = $this->request->getSession()->read('paraments');

        $this->loadModel('Users');
        $users_list = $this->Users->find('list')->where(['deleted' => 0, 'id NOT IN' => $parament->system->users]);
        $users = [];

        $users[0] = "Sin asignar";
        foreach ($users_list as $key => $user) {
            $users[$key] = $user;
        }

        $this->loadModel('Controllers');
        $controllers = $this->Controllers->find('list')->where(['deleted' => 0]);

        $this->loadModel('Services');
        $services = $this->Services->find('list')->where(['deleted' => 0]);

        $this->set(compact('users', 'controllers', 'services'));
    }

   /**
     * called from card view
     * tickets/index_card
    */
    public function getTickets() 
    {
        if ($this->request->is('ajax')) { //Ajax Detection 

            $status = $this->request->input('json_decode')->status;
            $category = $this->request->input('json_decode')->category;

            $tickets = [];
            $filters = [];

            if ($status != -1) {
                $filters['Tickets.status'] = $status;
            }

            if ($category != -1) {
                $filters['Tickets.category'] = $category;
            }

            $tickets = $this->Tickets->find()
                ->contain([
                     'Users', 
                     'Connections', 
                     'TicketsRecords.Users', 
                     'Customers.Connections', 
                     'CategoriesTickets', 
                     'StatusTickets', 
                     'ServicePending'
                 ])
                ->where($filters)->toArray();

            $this->loadModel('Users');
            $users = $this->Users->find();

            foreach ($tickets as $ticket) {

                $ticket->tickets_records = array_reverse($ticket->tickets_records);

                $ticket->user_asigned = null;

                if ($ticket->asigned_user_id) {

                    foreach ($users as $user) {
                        if ($user->id == $ticket->asigned_user_id->id) {
                            $ticket->user_asigned = $user;
                            break;
                        }
                    }
                }
            }

            $this->set('tickets', $tickets);
        }
    }

    /**
     * called from
     * tickets/index_table
     */
    public function getTicketsFromTable($code = null)
    {
        if ($this->request->is('ajax')) { //Ajax Detection

            $this->loadModel('RolesUsers');
            $role_user = $this->RolesUsers->find()->where(['user_id' => $this->Auth->user()['id']])->where(['role_id IN' => [100, 102, 108, 113]])->first();
            $role_id = true;
            if ($role_user) {
            	$role_id = false;
            }

            $response = new \stdClass();

            $response->draw = intval($this->request->getQuery('draw'));

            $where = [
                'duedate IS' => NULL
            ];

            if ($this->request->getQuery()['status'] != 0) {
                if ($this->request->getQuery()['status'] == 2) {
                    $where['Tickets.status'] = 2;
                } else {
                    $where['Tickets.status !='] = 2;
                }
            }

            $where['Tickets.archived'] = $this->request->getQuery()['archived'];

            if (!empty($code)) {
                $where['customer_id'] = $code;
            }

            $response->recordsTotal = $this->Tickets->find()->where($where)->count();

            $response->data = $this->Tickets->find('ServerSideData', [
                'params'  => $this->request->getQuery(),
                'where'   => $where,
                'role_id' => $role_id
            ]);

            $response->recordsFiltered = $this->Tickets->find('RecordsFiltered', [
                'params'  => $this->request->getQuery(),
                'where'   => $where,
                'role_id' => $role_id
            ]);

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    /**
     * called from
     * tickets/add
     */
    public function getTicketsFromAddTicket()
    {
        if ($this->request->is('ajax')) { //Ajax Detection

            $code = $this->request->getQuery('customer_code');

            $this->loadModel('RolesUsers');
            $role_user = $this->RolesUsers->find()->where(['user_id' => $this->Auth->user()['id']])->where(['role_id IN' => [100, 102, 108, 113]])->first();
            $role_id = true;
            if ($role_user) {
            	$role_id = false;
            }

            $response = new \stdClass();

            $response->draw = intval($this->request->getQuery('draw'));

            $where = [
                'duedate IS'  => NULL,
                'customer_id' => $code
            ];

            $response->recordsTotal = $this->Tickets->find()->where($where)->count();

            $response->data = $this->Tickets->find('ServerSideData', [
                'params'  => $this->request->getQuery(),
                'where'   => $where,
                'role_id' => $role_id
            ]);

            $response->recordsFiltered = $this->Tickets->find('RecordsFiltered', [
                'params'  => $this->request->getQuery(),
                'where'   => $where,
                'role_id' => $role_id
            ]);

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function view($id)
    {
        $ticket = $this->Tickets->get($id, [
            'contain' => ['Users', 'Connections', 'TicketsRecords', 'CategoriesTickets', 'StatusTickets']
        ]);

        $this->set('ticket', $ticket);
        $this->set('_serialize', ['ticket']);
    }

    public function add($ticket_number = null) 
    {
        $parament = $this->request->getSession()->read('paraments');
        $ticket = $this->Tickets->newEntity();
        $this->loadModel('Areas');
        $areas = $this->Areas
            ->find()
            ->where([
                'deleted' => FALSE
            ]);

        $areas_ticket = $this->Areas
            ->find('list')
            ->where([
                'deleted' => 0
            ])->toArray();

        $this->loadModel('Users');
        $users_list = $this->Users
            ->find('list')
            ->where([
                'deleted'   => 0, 
                'id NOT IN' => $parament->system->users
            ]);
        $users = [];

        $users[0] = "Sin asignar";
        foreach ($users_list as $key => $user) {
            $users[$key] = $user;
        }

        $users_add = $this->Users
            ->find()
            ->where([
                'deleted'   => 0, 
                'id NOT IN' => $parament->system->users
            ]);

        $tickets_status = $this->StatusTickets
            ->find()
            ->where([
                'deleted' => false
            ])
            ->order([
                'ordering' => 'ASC'
            ])->toArray();
        $last_status = array_shift($tickets_status);
        array_push($tickets_status, $last_status);
        $status = [];
        foreach ($tickets_status as $st) {
            $status_ti[$st->name] = $st->name;
        }

        $tickets_categories = $this->CategoriesTickets
            ->find()
            ->where([
                'deleted' => false
            ])
            ->order([
                'ordering' => 'ASC'
            ])->toArray();
        $categories = [];
        foreach ($tickets_categories as $cat) {
            $categories[$cat->name] = $cat->name;
        }

        if ($this->request->is('post')) {

            $request_data = $this->request->getData();

            $request_data['user_id'] = $this->Auth->user()['id'];

            $date_array = explode(' ', $this->request->getData('start_task'));
            //primer parte -> dd/mm/yyyy
            $start_task_1 = explode('/', $date_array[0]);
            //primer parte -> hh:mm
            $start_task_2 = explode(':', $date_array[1]);

            $start_task = $start_task_1[2] . '-' . $start_task_1[1] . '-' . $start_task_1[0] . ' ' . $start_task_2[0] . ':' . $start_task_2[1];

            $start_task = new Time($start_task);

            $request_data['start_task'] = $start_task;

            $ticket = $this->Tickets->patchEntity($ticket, $request_data);

            $ticket->status = 1; //estado abierto -> estado estático predefinido
            $ticket->category = $request_data['category'];
            $ticket->asigned_user_id = $request_data['asigned_user_id'] == '' ? NULL : $request_data['asigned_user_id'];

            if ($ticket->title == '') {
                $this->Flash->warning(__('Es obligatorio ingresar un título al Ticket.'));

                return $this->redirect(['action' => 'add', $ticket->id]);
            }

            if ($ticket->description == '') {
                $ticket->description = '';
            }

            // if ($ticket->description == '') {
            //     $this->Flash->warning(__('Es obligatorio ingresar una descripción al Ticket.'));

            //     return $this->redirect(['action' => 'add', $ticket->id]);
            // }

            if ($this->Tickets->save($ticket)) {

                //log de accion

                $user_id = $this->request->getSession()->read('Auth.User')['id'];

                $this->loadModel('Users');
                $user = $this->Users->get($user_id);
                $user_name = $user->name . ' - username: ' . $user->username;

                $action =  'Creación Ticket';

                $now = Time::now();

                $customer_id = $ticket->customer_id ? $ticket->customer_id : '';
                $connection_id = $ticket->connection_id ? $ticket->connection_id : '';

                $detail = 'Nro: #' . $ticket->id . PHP_EOL;
                $detail .= 'Fecha: ' . $now->format('d/m/Y') . PHP_EOL;
                $detail .=  'Descrip.: ' . $ticket->description . PHP_EOL;
                $detail .=  'Cliente: ' . $customer_id . PHP_EOL;
                $detail .=  'ID Conexión: ' . $connection_id . PHP_EOL;
                $detail .=  'Usuario: ' . $user_name . PHP_EOL;
                $detail .= '---------------------------' . PHP_EOL;

                $this->registerActivity($action, $detail, $ticket->customer_id);

                return $this->redirect(['action' => 'add', $ticket->id]);
            }

            //log de accion

            $user_id = $this->request->getSession()->read('Auth.User')['id'];

            $this->loadModel('Users');
            $user = $this->Users->get($user_id);
            $user_name = $user->name . ' - username: ' . $user->username;

            $action =  'Error en la creación Ticket';

            $now = Time::now();

            $customer_id = $ticket->customer_id ? $ticket->customer_id : '';
            $connection_id = $ticket->connection_id ? $ticket->connection_id : '';

            $detail = 'Nro: #' . $ticket->id . PHP_EOL;
            $detail .= 'Fecha: ' . $now->format('d/m/Y') . PHP_EOL;
            $detail .=  'Descrip.: ' . $ticket->description . PHP_EOL;
            $detail .=  'Cód. Cliente: ' . $customer_id . PHP_EOL;
            $detail .=  'ID Conexión: ' . $connection_id . PHP_EOL;
            $detail .=  'Usuario: ' . $user_name . PHP_EOL;
            $detail .= '---------------------------' . PHP_EOL;

            $this->registerActivity($action, $detail, $ticket->customer_id);

            return $this->redirect(['action' => 'add', $ticket->id]);

            $this->Flash->error(__('El Ticket no se ha agregado. Por favor, intente nuevamente.'));
        }

        $this->set(compact('ticket', 'ticket_number', 'areas', 'users', 'status_ti', 'categories', 'areas_ticket', 'users_add'));
        $this->set('_serialize', ['ticket']);
    }

    public function edit($id = null) 
    {
        $ticket = $this->Tickets->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {

            $ticket = $this->Tickets->patchEntity($ticket, $this->request->getData());

            if ($this->Tickets->save($ticket)) {

                //log de accion

                $user_id = $this->request->getSession()->read('Auth.User')['id'];

                $this->loadModel('Users');
                $user = $this->Users->get($user_id);
                $user_name = $user->name . ' - username: ' . $user->username;

                $action =  'Edición de Ticket';

                $now = Time::now();

                $customer_id = $ticket->customer_id ? $ticket->customer_id : '';
                $connection_id = $ticket->connection_id ? $ticket->connection_id : '';

                $detail = 'Nro: #' . $ticket->id . PHP_EOL;
                $detail .= 'Fecha: ' . $now->format('d/m/Y') . PHP_EOL;
                $detail .=  'Descrip.: ' . $ticket->description . PHP_EOL;
                $detail .=  'Cód. Cliente: ' . $customer_id . PHP_EOL;
                $detail .=  'ID Conexión: ' . $connection_id . PHP_EOL;
                $detail .=  'Usuario: ' . $user_name . PHP_EOL;
                $detail .= '---------------------------' . PHP_EOL;

                $this->registerActivity($action, $detail, $ticket->customer_id);

                $this->Flash->success(__('El ticket se ha editado correctamente.'));

                return $this->redirect(['action' => 'index']);
            }

            //log de accion

            $user_id = $this->request->getSession()->read('Auth.User')['id'];

            $this->loadModel('Users');
            $user = $this->Users->get($user_id);
            $user_name = $user->name . ' - username: ' . $user->username;

            $action =  'Error de edición de Ticket';

            $now = Time::now();

            $customer_id = $ticket->customer_id ? $ticket->customer_id : '';
            $connection_id = $ticket->connection_id ? $ticket->connection_id : '';

            $detail = 'Nro: #' . $ticket->id . PHP_EOL;
            $detail .= 'Fecha: ' . $now->format('d/m/Y') . PHP_EOL;
            $detail .=  'Descrip.: ' . $ticket->description . PHP_EOL;
            $detail .=  'Cód. Cliente: ' . $customer_id . PHP_EOL;
            $detail .=  'ID Conexión: ' . $connection_id . PHP_EOL;
            $detail .=  'Usuario: ' . $user_name . PHP_EOL;
            $detail .= '---------------------------' . PHP_EOL;

            $this->registerActivity($action, $detail, $ticket->customer_id);

            $this->Flash->error(__('El Ticket no se ha editado. Por favor, intente nuevamente.'));
        }
        $users = $this->Tickets->Users->find('list', ['limit' => 200]);
        $connections = $this->Tickets->Connections->find('list', ['limit' => 200]);
        $this->set(compact('ticket', 'users', 'connections'));
        $this->set('_serialize', ['ticket']);
    }

    public function delete($id = null) 
    {
        $this->request->allowMethod(['post', 'delete']);
        $ticket = $this->Tickets->get($id);
        $ticket_aux = $ticket;

        if ($this->Tickets->delete($ticket)) {

            //log de accion

            $user_id = $this->request->getSession()->read('Auth.User')['id'];

            $this->loadModel('Users');
            $user = $this->Users->get($user_id);
            $user_name = $user->name . ' - username: ' . $user->username;

            $action = 'Eliminación de Ticket';

            $now = Time::now();

            $customer_id = $ticket_aux->customer_id ? $ticket_aux->customer_id : '';
            $connection_id = $ticket_aux->connection_id ? $ticket_aux->connection_id : '';

            $detail = 'Nro: #' . $ticket_aux->id . PHP_EOL;
            $detail .= 'Fecha: ' . $now->format('d/m/Y') . PHP_EOL;
            $detail .=  'Descrip.: ' . $ticket_aux->description . PHP_EOL;
            $detail .=  'Cód. Cliente: ' . $customer_id . PHP_EOL;
            $detail .=  'ID Conexión: ' . $connection_id . PHP_EOL;
            $detail .=  'Usuario: ' . $user_name . PHP_EOL;
            $detail .= '---------------------------' . PHP_EOL;

            $this->registerActivity($action, $detail, $ticket_aux->customer_id);

            $this->Flash->success(__('El Ticket se ha eliminado correctamente.'));

            $this->loadModel('TicketsRecords');

            $ticketsRecords = $this->TicketsRecords
                ->find()
                ->where([
                    'ticket_id' => $id
                ]);

            foreach ($ticketsRecords as $ticketRecord) {

                if ($ticketRecord->image) {

                    $file = WWW_ROOT . 'ticket_image/' . $ticketRecord->image;

                    if (is_file($file)) {
                        unlink($file);
                    }
                }

                $this->TicketsRecords->delete($ticketRecord);
            }
        } else {

            //log de accion

            $user_id = $this->request->getSession()->read('Auth.User')['id'];

            $this->loadModel('Users');
            $user = $this->Users->get($user_id);
            $user_name = $user->name . ' - username: ' . $user->username;

            $action =  'Error en la eliminación de Ticket';

            $now = Time::now();

            $customer_id = $ticket_aux->customer_id ? $ticket_aux->customer_id : '';
            $connection_id = $ticket_aux->connection_id ? $ticket_aux->connection_id : '';

            $detail = 'Nro: #' . $ticket_aux->id . PHP_EOL;
            $detail .= 'Fecha: ' . $now->format('d/m/Y') . PHP_EOL;
            $detail .=  'Descrip.: ' . $ticket_aux->description . PHP_EOL;
            $detail .=  'Cód. Cliente: ' . $customer_id . PHP_EOL;
            $detail .=  'ID Conexión: ' . $connection_id . PHP_EOL;
            $detail .=  'Usuario: ' . $user_name . PHP_EOL;
            $detail .= '---------------------------' . PHP_EOL;

            $this->registerActivity($action, $detail, $ticket_aux->customer_id);

            $this->Flash->error(__('El Ticket no se ha eliminado. Por favor, intente nuevamente.'));
        }

        return $this->redirect(['action' => 'index_table']);
    }

    public function restore($id = null) 
    {
        if ($this->request->is(['patch', 'post', 'put'])) {

            $ticket = $this->Tickets->get($id);
            $ticket->archived = FALSE;

            if ($this->Tickets->save($ticket)) {

                //log de accion

                $user_id = $this->request->getSession()->read('Auth.User')['id'];

                $this->loadModel('Users');
                $user = $this->Users->get($user_id);
                $user_name = $user->name . ' - username: ' . $user->username;

                $action =  'Restauración de Ticket';

                $now = Time::now();

                $customer_id = $ticket->customer_id ? $ticket->customer_id : '';
                $connection_id = $ticket->connection_id ? $ticket->connection_id : '';

                $detail = 'Nro: #' . $ticket->id . PHP_EOL;
                $detail .= 'Fecha: ' . $now->format('d/m/Y') . PHP_EOL;
                $detail .=  'Descrip.: ' . $ticket->description . PHP_EOL;
                $detail .=  'Cód. Cliente: ' . $customer_id . PHP_EOL;
                $detail .=  'ID Conexión: ' . $connection_id . PHP_EOL;
                $detail .=  'Usuario: ' . $user_name . PHP_EOL;
                $detail .= '---------------------------' . PHP_EOL;

                $this->registerActivity($action, $detail, $ticket->customer_id);

                $this->Flash->success(__('El ticket se ha restaurado correctamente.'));
            } else {

                //log de accion

                $user_id = $this->request->getSession()->read('Auth.User')['id'];

                $this->loadModel('Users');
                $user = $this->Users->get($user_id);
                $user_name = $user->name . ' - username: ' . $user->username;

                $action =  'Error de restauración de Ticket';

                $now = Time::now();

                $customer_id = $ticket->customer_id ? $ticket->customer_id : '';
                $connection_id = $ticket->connection_id ? $ticket->connection_id : '';

                $detail = 'Nro: #' . $ticket->id . PHP_EOL;
                $detail .= 'Fecha: ' . $now->format('d/m/Y') . PHP_EOL;
                $detail .=  'Descrip.: ' . $ticket->description . PHP_EOL;
                $detail .=  'Cód. Cliente: ' . $customer_id . PHP_EOL;
                $detail .=  'ID Conexión: ' . $connection_id . PHP_EOL;
                $detail .=  'Usuario: ' . $user_name . PHP_EOL;
                $detail .= '---------------------------' . PHP_EOL;

                $this->registerActivity($action, $detail, $ticket->customer_id);

                $this->Flash->error(__('El ticket no se ha restaurado. Por favor, intente nuevamente.'));
            }
        }
        return $this->redirect(['action' => 'index_table']);
    }

    public function archive($id = null) 
    {
        if ($this->request->is(['patch', 'post', 'put'])) {

            $ticket = $this->Tickets->get($id);
            $ticket->archived = TRUE;

            if ($this->Tickets->save($ticket)) {

                //log de accion

                $user_id = $this->request->getSession()->read('Auth.User')['id'];

                $this->loadModel('Users');
                $user = $this->Users->get($user_id);
                $user_name = $user->name . ' - username: ' . $user->username;

                $action =  'Archivado de Ticket';

                $now = Time::now();

                $customer_id = $ticket->customer_id ? $ticket->customer_id : '';
                $connection_id = $ticket->connection_id ? $ticket->connection_id : '';

                $detail = 'Nro: #' . $ticket->id . PHP_EOL;
                $detail .= 'Fecha: ' . $now->format('d/m/Y') . PHP_EOL;
                $detail .=  'Descrip.: ' . $ticket->description . PHP_EOL;
                $detail .=  'Cód. Cliente: ' . $customer_id . PHP_EOL;
                $detail .=  'ID Conexión: ' . $connection_id . PHP_EOL;
                $detail .=  'Usuario: ' . $user_name . PHP_EOL;
                $detail .= '---------------------------' . PHP_EOL;

                $this->registerActivity($action, $detail, $ticket->customer_id);

                $this->Flash->success(__('El ticket se ha archivado correctamente.'));
            } else {

                //log de accion

                $user_id = $this->request->getSession()->read('Auth.User')['id'];

                $this->loadModel('Users');
                $user = $this->Users->get($user_id);
                $user_name = $user->name . ' - username: ' . $user->username;

                $action =  'Error de archivado de Ticket';

                $now = Time::now();

                $customer_id = $ticket->customer_id ? $ticket->customer_id : '';
                $connection_id = $ticket->connection_id ? $ticket->connection_id : '';

                $detail = 'Nro: #' . $ticket->id . PHP_EOL;
                $detail .= 'Fecha: ' . $now->format('d/m/Y') . PHP_EOL;
                $detail .=  'Descrip.: ' . $ticket->description . PHP_EOL;
                $detail .=  'Cód. Cliente: ' . $customer_id . PHP_EOL;
                $detail .=  'ID Conexión: ' . $connection_id . PHP_EOL;
                $detail .=  'Usuario: ' . $user_name . PHP_EOL;
                $detail .= '---------------------------' . PHP_EOL;

                $this->registerActivity($action, $detail, $ticket->customer_id);

                $this->Flash->error(__('El ticket no se ha archivado. Por favor, intente nuevamente.'));
            }
        }
        return $this->redirect(['action' => 'index_table']);
    }

    public function userAssignment()
    {
        if ($this->request->is('Ajax')) { //Ajax Detection

            $response = new \stdClass;
            $response->error = false;
            $response->msg = 'Usuario asignado correctamente.';
            $response->typeMsg = 'success';

            $data = $this->request->input('json_decode')->data;

            $ticket = $this->Tickets->get($data->id, [
                'contain' => []
            ]);

            if ($data->asigned_user_id == 0) {
                $ticket->asigned_user_id = NULL;
            } else {
                $ticket->asigned_user_id = $data->asigned_user_id;
            }

            if (!$this->Tickets->save($ticket)) {

                $response->error = true;
                $response->msg = 'Error al intentar actualizar el Ticket.';
                $response->typeMsg = 'error';
            }

            if (!$response->error) {

                $this->loadModel('Users');
                if ($ticket->asigned_user_id != 0) {
                    $user = $this->Users->get($ticket->asigned_user_id);
                }

                $this->loadModel('TicketsRecords');
                $ticketRecord = $this->TicketsRecords->newEntity();

                $ticketRecord->ticket_id = $ticket->id;
                $ticketRecord->user_id = $this->Auth->user()['id'];
                $ticketRecord->short_description = "Asignación de usuario";

                if ($ticket->asigned_user_id != 0) {
                    $ticketRecord->description = "Se asigno el Usuario: " . $user->username;
                } else {
                    $ticketRecord->description = "Sin asignar";
                }

                if (!$this->TicketsRecords->save($ticketRecord)) {

                    $response->error = true;
                    $response->msg = 'Error al intentar actualizar el Ticket record.';
                    $response->typeMsg = 'error';
                }
            }

            $this->set('response', $response);
        }
    }

    public function addComment() 
    {
        if ($this->request->is('Ajax')) { //Ajax Detection

            $response = new \stdClass;
            $response->error = false;
            $response->msg = 'Comentario agregado correctamente.';
            $response->typeMsg = 'success';

            $data = $this->request->input('json_decode')->data;

            $this->loadModel('TicketsRecords');
            $ticketRecord = $this->TicketsRecords->newEntity();

            $ticketRecord->ticket_id = $data->ticket_id;
            $ticketRecord->short_description = "Agregado de comentario";
            $ticketRecord->description = $data->description;
            $ticketRecord->user_id = $this->Auth->user()['id'];

            if (!$this->TicketsRecords->save($ticketRecord)) {

                $response->error = true;
                $response->msg = 'Error al intentar agregar el cometario al Ticket.';
                $response->typeMsg = 'error';
            }

            $this->set('response', $response);
        }
    }

    public function editTitle()
    {
        if ($this->request->is('ajax')) { //Ajax Detection

            $response = new \stdClass;
            $response->error = false;
            $response->msg = 'Título editado correctamente.';
            $response->typeMsg = 'success';

            $data = $this->request->input('json_decode')->data;

            $ticket = $this->Tickets->get($data->ticket_id, [
                'contain' => []
            ]);

            $old_title = $ticket->title;
            $ticket->title = $data->title;

            if (!$this->Tickets->save($ticket)) {

                $response->error = true;
                $response->msg = 'Error al intentar actualizar el Ticket.';
                $response->typeMsg = 'error';
            }

            if (!$response->error) {
                $this->loadModel('TicketsRecords');
                $ticketRecord = $this->TicketsRecords->newEntity();
                $ticketRecord->ticket_id = $data->ticket_id;
                $ticketRecord->short_description = "Edición título";
                $ticketRecord->description = $old_title . ' -> ' . $data->title;
                $ticketRecord->user_id = $this->Auth->user()['id'];

                if (!$this->TicketsRecords->save($ticketRecord)) {

                    $response->error = true;
                    $response->msg = 'Error al intentar editar el título del Ticket.';
                    $response->typeMsg = 'error';
                }
            }

            $this->set('response', $response);
        }
    }

    public function editDescription()
    {
        if ($this->request->is('ajax')) { //Ajax Detection

            $response = new \stdClass;
            $response->error = false;
            $response->msg = 'Descripción editada correctamente.';
            $response->typeMsg = 'success';

            $data = $this->request->input('json_decode')->data;

            $ticket = $this->Tickets->get($data->ticket_id, [
                'contain' => []
            ]);

            $old_description = $ticket->description;
            $ticket->description = $data->description;

            if (!$this->Tickets->save($ticket)) {

                $response->error = true;
                $response->msg = 'Error al intentar actualizar el Ticket.';
                $response->typeMsg = 'error';
            }

            if (!$response->error) {
                $this->loadModel('TicketsRecords');
                $ticketRecord = $this->TicketsRecords->newEntity();
                $ticketRecord->ticket_id = $data->ticket_id;
                $ticketRecord->short_description = "Edición descripción";
                $ticketRecord->description = $old_description . ' -> ' . $data->description;
                $ticketRecord->user_id = $this->Auth->user()['id'];

                if (!$this->TicketsRecords->save($ticketRecord)) {

                    $response->error = true;
                    $response->msg = 'Error al intentar editar la descripción del Ticket.';
                    $response->typeMsg = 'error';
                }
            }

            $this->set('response', $response);
        }
    }

    public function changeStatus() 
    {
        if ($this->request->is('ajax')) { //Ajax Detection

            $response = new \stdClass;
            $response->error = false;
            $response->msg = 'Cambio de estado del Ticket exitoso.';
            $response->typeMsg = 'success';

            $data = $this->request->input('json_decode')->data;

            $ticket = $this->Tickets->get($data->ticket_id, [
                'contain' => ['StatusTickets']
            ]);

            $before_status = $ticket->status->name;

            $ticket->status = $data->change_status_id;

            if ($this->Tickets->save($ticket)) {
                $this->loadModel('TicketsRecords');
                $ticketRecord = $this->TicketsRecords->newEntity();

                $ticketRecord->ticket_id = $data->ticket_id;
                $ticketRecord->short_description = "Cambio de estado: " . $before_status . ' -> ' . $data->status_name;
                $ticketRecord->description = "Cambio de estado: " . $before_status . ' -> ' . $data->status_name;
                $ticketRecord->user_id = $this->Auth->user()['id'];

                if (!$this->TicketsRecords->save($ticketRecord)) {
                    $response->error = true;
                    $response->msg = 'Error al intentar cambiar el estado del Ticket.';
                    $response->typeMsg = 'error';
                }
            } else {
                $response->error = true;
                $response->msg = 'Error al intentar cambiar el estado del Ticket.';
                $response->typeMsg = 'error';
            }

            $this->set('response', $response);
        }
    }

    public function changeCategory() 
    {
        if ($this->request->is('ajax')) { //Ajax Detection

            $response = new \stdClass;
            $response->error = false;
            $response->msg = 'Cambio de categoría del Ticket exitoso.';
            $response->typeMsg = 'success';

            $data = $this->request->input('json_decode')->data;

            $ticket = $this->Tickets->get($data->ticket_id, [
                'contain' => ['CategoriesTickets']
            ]);

            $before_category = $ticket->category->name;

            $ticket->category = $data->change_category_id;

            if ($this->Tickets->save($ticket)) {
                $this->loadModel('TicketsRecords');
                $ticketRecord = $this->TicketsRecords->newEntity();

                $ticketRecord->ticket_id = $data->ticket_id;
                $ticketRecord->short_description = "Cambio de categroría: " . $before_category . ' -> ' . $data->category_name;
                $ticketRecord->description = "Cambio de categroría: " . $before_category . ' -> ' . $data->category_name;
                $ticketRecord->user_id = $this->Auth->user()['id'];

                if (!$this->TicketsRecords->save($ticketRecord)) {
                    $response->error = true;
                    $response->msg = 'Error al intentar cambiar la categroría del Ticket.';
                    $response->typeMsg = 'error';
                }
            } else {
                $response->error = true;
                $response->msg = 'Error al intentar cambiar la categroría del Ticket.';
                $response->typeMsg = 'error';
            }

            $this->set('response', $response);
        }
    }

    public function changeStartTask() 
    {
        if ($this->request->is('ajax')) { //Ajax Detection

            $response = new \stdClass;
            $response->error = false;
            $response->msg = 'Cambio de fecha de inicio del Ticket exitoso.';
            $response->typeMsg = 'success';

            $data = $this->request->input('json_decode')->data;

            $ticket = $this->Tickets->get($data->ticket_id);

            $time = explode(" ", $data->change_start_task);
            
            //primer parte -> dd/mm/yyyy
            $start_task_1 = explode('/', $time[0]);
            //primer parte -> hh:mm
            $start_task_2 = explode(':', $time[1]);
            $start_task = $start_task_1[2] . '-' . $start_task_1[1] . '-' . $start_task_1[0] . ' ' . $start_task_2[0] . ':' . $start_task_2[1];

            $start_task = new Time($start_task);
            $ticket->start_task = $start_task;

            $time_start_task = $ticket->start_task->day . '/' . $ticket->start_task->month . '/' . $ticket->start_task->year . ' ' . $ticket->start_task->hour . ':' . $ticket->start_task->minute;

            if ($this->Tickets->save($ticket)) {
                $parament = $this->request->getSession()->read('paraments');

                if ($parament->ticket->save_change_start_task) {
                    $this->loadModel('TicketsRecords');
                    $ticketRecord = $this->TicketsRecords->newEntity();

                    $ticketRecord->ticket_id = $data->ticket_id;
                    $ticketRecord->short_description = "Cambio de fecha";
                    $ticketRecord->description = "Cambio de fecha de inicio: " . $time_start_task . ' -> ' . $data->change_start_task;
                    $ticketRecord->user_id = $this->Auth->user()['id'];

                    if (!$this->TicketsRecords->save($ticketRecord)) {
                        $response->error = true;
                        $response->msg = 'Error al intentar cambiar la fecha de inicio del ticket.';
                        $response->typeMsg = 'error';
                    }
                }
            } else {
                $response->error = true;
                $response->msg = 'Error al intentar cambiar la fecha de inicio del ticket.';
                $response->typeMsg = 'error';
            }

            $this->set('response', $response);
        }
    }

    public function changeStartTaskFromCalendar() 
    {
        if ($this->request->is('ajax')) { //Ajax Detection

            $response = new \stdClass;
            $response->error = false;
            $response->msg = 'Cambio de fecha de inicio del ticket exitoso.';
            $response->typeMsg = 'success';

            $datas = $this->request->input('json_decode')->data;

            foreach ($datas as $data) {

                if (empty($data)) {
                    continue;
                }

                $ticket = $this->Tickets->get($data->ticket_id);
                //01/04/2018 18:37
                $ticket->start_task = new Time($data->change_start_task);

                $time_start_task = $ticket->start_task->day . '/' . $ticket->start_task->month . '/' . $ticket->start_task->year . ' ' . $ticket->start_task->hour . ':' . $ticket->start_task->minute;

                if ($this->Tickets->save($ticket)) {
                    $parament = $this->request->session()->read('paraments');

                    if ($parament->ticket->save_change_start_task) {
                        $this->loadModel('TicketsRecords');
                        $ticketRecord = $this->TicketsRecords->newEntity();

                        $ticketRecord->ticket_id = $data->ticket_id;
                        $ticketRecord->short_description = "Cambio de fecha";

                        $change_start_task_array = explode("T", $data->change_start_task);
                        $change_start_task = explode("-", $change_start_task_array[0]);

                        $colita = explode("-", $change_start_task_array[1]);
                        $colita = explode(":", $colita[0]);
                        $colita = $colita[0] . ':' . $colita[1];

                        $change_start_task = $change_start_task[2] . '/' . $change_start_task[1] . '/' . $change_start_task[0] . ' ' . $colita; 

                        $ticketRecord->description = "Cambio de fecha de inicio: " . $time_start_task . ' -> ' . $change_start_task;
                        $ticketRecord->user_id = $this->Auth->user()['id'];

                        if (!$this->TicketsRecords->save($ticketRecord)) {
                            $response->error = true;
                            $response->msg = 'Error al intentar cambiar la fecha de inicio del ticket.';
                            $response->typeMsg = 'error';
                        }
                    }
                } else {
                    $response->error = true;
                    $response->msg = 'Error al intentar cambiar la fecha de inicio del ticket.';
                    $response->typeMsg = 'error';
                }
            }

            $this->set('response', $response);
        }
    }

    public function changeAreas()
    {
        if ($this->request->is('ajax')) { //Ajax Detection

            $response = new \stdClass;
            $response->error = false;
            $response->msg = 'Cambio de área del Ticket exitoso.';
            $response->typeMsg = 'success';

            $data = $this->request->input('json_decode')->data;

            $ticket = $this->Tickets->get($data->ticket_id, [
                'contain' => ['Areas']
            ]);

            $before_status = "";
            if ($ticket->area_id != NULL) {
                $before_status = $ticket->area->name;
            }

            $ticket->area_id = $data->change_area_id;

            if ($this->Tickets->save($ticket)) {

                $this->loadModel('TicketsRecords');
                $ticketRecord = $this->TicketsRecords->newEntity();

                $ticketRecord->ticket_id = $data->ticket_id;
                $ticketRecord->short_description = "Cambio de área: " . $before_status . ' -> ' . $data->area_name;
                $ticketRecord->description = "Cambio de área: " . $before_status . ' -> ' . $data->area_name;
                $ticketRecord->user_id = $this->Auth->user()['id'];

                if (!$this->TicketsRecords->save($ticketRecord)) {
                    $response->error = true;
                    $response->msg = 'Error al intentar cambiar el área del Ticket.';
                    $response->typeMsg = 'error';
                }
            } else {
                $response->error = true;
                $response->msg = 'Error al intentar cambiar el área del Ticket.';
                $response->typeMsg = 'error';
            }

            $this->set('response', $response);
        }
    }

    public function updateStatusTicket() 
    {
        if ($this->request->is('ajax')) { //Ajax Detection

            $response = new \stdClass;
            $response->error = false;
            $response->msg = 'Cambio de estado correcto.';
            $response->typeMsg = 'success';

            $data = $this->request->input('json_decode');

            $ticket = $this->Tickets->get($data->id, [
                'contain' => []
            ]);

            $ticket->status = $data->status;

            if ($ticket->status == self::$AVAILABLE_STATUS) {
                $ticket->asigned_user_id = null;
            }

            if (!$this->Tickets->save($ticket)) {

                $response->error = true;
                $response->msg = 'Error al intentar actulizar el ticket.';
                $response->typeMsg = 'error';
            }

            $this->loadModel('TicketsRecords');
            $ticketRecord = $this->TicketsRecords->newEntity();

            $ticketRecord->ticket_id = $data->id;
            $ticketRecord->user_id = $this->Auth->user()['id'];
            $ticketRecord->short_description = "Apertura";
            $ticketRecord->description = "Apertura del Ticket";

            if ($ticket->status == self::$FINISHED_STATUS) {
                $ticketRecord->description = "Cierre del Ticket";
            }

            if (!$this->TicketsRecords->save($ticketRecord)) {

                $response->error = true;
                $response->msg = 'Error al intentar actulizar el ticket record.';
                $response->typeMsg = 'error';
            }

            $this->set('response', $response);
        }
    }

    public function getTicketsByCustomer()
    {
        if ($this->request->is('ajax')) { //Ajax Detection

            $customer_code = $this->request->input('json_decode')->customer_code;

            $tickets = $this->Tickets->find()
                ->contain(['Users', 'TicketsRecords.Users', 'Customers.Connections'])
                ->where([
                    'Tickets.customer_id' => $customer_code,
                    'Tickets.status !=' => self::$FINISHED_STATUS
                    ]);

            $this->loadModel('Users');
            $users = $this->Users->find();

            foreach ($tickets as $ticket) {
                
                $ticket->user_asigned = null;

                if ($ticket->asigned_user_id) {

                    foreach ($users as $user) {
                        if ($user->id == $ticket->asigned_user_id) {
                            $ticket->user_asigned = $user;
                            break;
                        }
                    }
                }
            }

            $this->set('tickets', $tickets);
        }
    }

    public function individualTicketExportPDF($id)
    {
        if ($this->request->is('post')) {

            $parament = $this->request->session()->read('paraments');
            $afip_codes =  $this->request->getSession()->read('afip_codes');

            $data = $this->request->getData();

            $ticket = $this->Tickets->get($data['id'], [
                'contain' => ['Customers', 'StatusTickets', 'CategoriesTickets', 'Users', 'Connections.Services', 'Areas', 'ServicePending']
            ]);

            //DATOS CLIENTE
            $customer = $ticket->customer;

            $customer_name = "";
            $customer_code = "";
            $customer_address = "";
            $customer_phone = "";
            $customer_doc_type = "Doc.";
            $customer_ident = "";
            $lat = NULL;
            $lng = NULL;

            if ($customer) {
                $customer_name = $customer->name;
                $customer_code = $customer->code;
                $customer_address = $customer->address;
                $customer_phone = $customer->phone;
                $customer_doc_type = $afip_codes['doc_types'][$customer->doc_type];
                $customer_ident = $customer->ident;
                $lat = $customer->lat;
                $lng = $customer->lng;
            }

            //DATOS TICKET
            $ticket_number = $ticket->id;
            $ticket_category = $ticket->category ? $ticket->category->name : '';
            $ticket_status = $ticket->status ? $ticket->category->name : '';
            $ticket_assignmentet = $ticket->asigned_user_id ? $ticket->asigned_user_id->name : '';
            $ticket_description = $ticket->description;

            $ticket_service = "";

            if ($ticket->connection) {
                $ticket_service = $ticket->connection->created->format('d/m/Y') . ' - ' . $ticket->connection->service->name . ' - ' . $ticket->connection->address;
            }

            if ($ticket->service_pending) {
                $ticket_service = $ticket->service_pending->service_name . ' - ' . $ticket->service_pending->address;
            }

            $ticket_service = $ticket_service;

            $ticket_area = $ticket->area ? $ticket->area->name : '';

            if ($ticket->conection) {
                if ($ticket->conection->lat && $ticket->conection->lng) {
                    $lat = $ticket->conection->lat;
                    $lng = $ticket->conection->lng;
                }
            }
            $ticket_start_task = "";
            if ($ticket->start_task) {
                $ticket_start_task = $ticket->start_task->format('d/m/Y H:i');
            }

            //DATOS EMPRESA
            $enterprise_name = "Empresa";
            $enterprise_address = "";
            $enterprise_phone = "";
            $enterprise_logo = WWW_ROOT . "img/logo.png";

            foreach ($parament->invoicing->business as $b) {

                if ($customer != NULL && $customer->business_billing == $b->id) {
                    $enterprise_name = $b->name;
                    $enterprise_address = $b->address;
                    $enterprise_phone = $b->phone;
                    $enterprise_logo = WWW_ROOT . 'img/' . $b->logo;
                    break;
                } else {

                    $enterprise_name = $b->name;
                    $enterprise_address = $b->address;
                    $enterprise_phone = $b->phone;
                    $enterprise_logo = WWW_ROOT . 'img/' . $b->logo;
                    break;
                }
            }
            $now = Time::now();
            $month = str_pad($now->month, 2, "0", STR_PAD_LEFT);
            $day = str_pad($now->day, 2, "0", STR_PAD_LEFT);
            $now = $day . '/' . $month . '/' . $now->year;

            $image_map = "";
            if ($lat != NULL && $lng != NULL) {
                $image_map = "<img src='https://maps.googleapis.com/maps/api/staticmap?center=$lat,$lng&zoom=14&size=700x260&markers=color:red%7Clabel:P%7C$lat,$lng&key=AIzaSyAZG7dni5-GuQaooMmy6fLtYf3B_kHtAOY' />";
            }

            $html = '
                <html>
                    <head>
                        <style>
                            body {font-family: sans-serif;
                            	font-size: 10pt;
                            }
                            p {	margin: 0pt; }
                            table.items {
                            	border: 0.1mm solid #000000;
                            }
                            td { vertical-align: top; }
                            .items td {
                            	border-left: 0.1mm solid #000000;
                            	border-right: 0.1mm solid #000000;
                            }
                            table thead td { background-color: #EEEEEE;
                            	text-align: center;
                            	border: 0.1mm solid #000000;
                            	font-variant: small-caps;
                            }
                            .items td.blanktotal {
                            	background-color: #EEEEEE;
                            	border: 0.1mm solid #000000;
                            	background-color: #FFFFFF;
                            	border: 0mm none #000000;
                            	border-top: 0.1mm solid #000000;
                            	border-right: 0.1mm solid #000000;
                            }
                            .items td.totals {
                            	text-align: right;
                            	border: 0.1mm solid #000000;
                            }
                            .items td.cost {
                            	text-align: "." center;
                            }
                        </style>
                    </head>
                    <body>
                        <!--mpdf
                        <htmlpageheader name="myheader">
                            <table width="100%">
                                <tr>
                                    <td width="50%" ><img src="' . $enterprise_logo . '" height="100px"/></td>
                                    <td width="50%" style="color:#615d5d; text-align: right;">
                                        <span style="font-weight: bold; font-size: 14pt;">' . $enterprise_name . '</span>
                                        <br />' . $enterprise_address . '
                                        <br />
                                        <span style="font-family:dejavusanscondensed;">&#9742;</span> ' . $enterprise_phone . '
                                    </td>
                                </tr>
                            </table>
                        </htmlpageheader>
                        <htmlpagefooter name="myfooter">
                            <div style="border-top: 1px solid #000000; font-size: 9pt; text-align: center; padding-top: 3mm; ">
                                Página {PAGENO} de {nb}
                            </div>
                        </htmlpagefooter>
                        <sethtmlpageheader name="myheader" value="on" show-this-page="1" />
                        <sethtmlpagefooter name="myfooter" value="on" />
                        mpdf-->
                        <div style="text-align: right">Fecha emisión: ' . $now . '</div>
                    	<table width="100%" style="font-family: serif;" cellpadding="10">
                    		<tr>
                    			<td width="45%" style="border: 0.1mm solid #888888; ">
                    				<span style="font-size: 12pt; color: #555555; font-family: sans;">Datos cliente:</span>
                    				<br />
                    				<br /><b>Nombre</b>: ' . $customer_name . '
                    				<br /><b>Código</b>: ' . $customer_code . '
                    				<br /><b>' . $customer_doc_type . '</b>: ' . $customer_ident . '
                    				<br /><b>Domicilio</b>: ' . $customer_address . '
                    				<br /><b>Tel.</b>: ' . $customer_phone . '
                    			</td>
                    			<td width="10%">&nbsp;</td>
                    			<td width="45%" style="border: 0.1mm solid #888888;">
                    				<span style="font-size: 12pt; color: #555555; font-family: sans;">Ticket #' . $ticket_number . '</span>
                    				<br />
                    				<br /><b>Categoría</b>: ' . $ticket_category . '
                    				<br /><b>Estado</b>: ' . $ticket_status . '
                    				<br /><b>Asignado</b>: ' . $ticket_assignmentet . '
                    				<br /><b>Servicio</b>: ' . $ticket_service . '
                    				<br /><b>Área</b>: ' . $ticket_area . '
                    				<br /><b>Visita</b>: ' . $ticket_start_task . '
                    			</td>
                    		</tr>
                    	</table>
                    	<br />
                    	<b style="font-size: 15px;">Detalle</b>
                        <br />
                        <p>
                            ' . $ticket_description . '
                        </p>
                        <br />
                        <b style="font-size: 15px;">Observaciones</b>
                        <div style="border: 1px solid #000000; height: 100px;">
                        </div>
                        <br />
                        ' . $image_map . '
                        <br />
                        <br />
                        <br />
                        <br />
                        Firma:....................................................................
                        <br />
                        <br />
                        Aclaración:.............................................................
                        <br />
                    </body>
                </html>';

            $mpdf = new \Mpdf\Mpdf([
            	'margin_left'   => 20,
            	'margin_right'  => 15,
            	'margin_top'    => 48,
            	'margin_bottom' => 25,
            	'margin_header' => 10,
            	'margin_footer' => 10
            ]);

            $mpdf->SetProtection(array('print'));
            $pdf_title = "ticket-" . $ticket->id;
            $mpdf->SetTitle($pdf_title);
            $mpdf->SetDisplayMode('fullpage');
            $mpdf->SetJS('this.print();');
            $mpdf->WriteHTML($html);
            $mpdf->Output($pdf_title . 'pdf', \Mpdf\Output\Destination::INLINE);

            $this->redirect($this->referer());
        }
    }

    public function ticketExportPDF($id = null)
    {
        if ($this->request->is('post')) {

            if (count($paraments = $this->request->getSession()->read('paraments')->invoicing->business) > 0) {

                $data = new \stdClass;
                $data->tickets = json_decode($this->request->getData('tickets'));

                $this->CreateTicketPdf->generate($data, CreateTicketPdf::TYPE_TICKET_TABLE);

            } else {
                $this->Flash->error(__('Configuración incompleta. Faltan datos de la empresa'));
                return $this->redirect(['action' => 'index']);
            }
        }
    }

    public function addDigitalSignature()
    {
        if ($this->request->is('post') && null !== $this->request->getData('hdnSignature')) {

            $folder = new Folder(WWW_ROOT . 'ticket_image', true, 0755);

            $this->loadModel('TicketsRecords');
            $ticketRecord = $this->TicketsRecords->newEntity();

            $ticketRecord->ticket_id = $this->request->getData('ticket_id');
            $ticketRecord->short_description = "Firma";
            $ticketRecord->description = $this->request->getData('description');

            $sign = $this->request->getData('hdnSignature');
            $digital_signature_name = time() . '-' . $this->Auth->user()['id'] . '.png';
            $path = WWW_ROOT . 'ticket_image/' . $digital_signature_name;
            $ticketRecord->user_id = $this->Auth->user()['id'];

            $sign = base64_decode($sign); //convert base64

            if (file_put_contents($path, $sign)) { //save to file
                $ticketRecord->image = $digital_signature_name;

                if ($this->TicketsRecords->save($ticketRecord)) {
                    $this->Flash->success(__('Se ha guardado correctamente la firma.'));
                } else {
                    $this->Flash->warning(__('No se ha guardado la firma. Por favor, intente nuevamente.'));
                }
            } else {
                $this->Flash->warning(__('No se ha guardado la firma. Por favor, intente nuevamente.'));
            }
        } else {
            $this->Flash->warning(__('No se ha guardado la firma. Por favor, intente nuevamente.'));
        }
        return $this->redirect(['action' => 'index_' . $this->request->getData('view')]);
    }

    public function config()
    {
        $this->set('user_id', $this->request->getSession()->read('Auth.User')['id']);
    }

    public function getStatusTickets()
    {
        $status_tickets = $this->StatusTickets->find()->where(['deleted' => false])->order(['ordering' => 'ASC'])->toArray();
        $last_status = array_shift($status_tickets);
        array_push($status_tickets, $last_status);

        foreach ($status_tickets as $status) {
            if ($status->id == 1) {
                $status->ordering = 'Inicial';
            }

            if ($status->id == 2) {
                $status->ordering = 'Final';
            }
        }

        $this->set(compact('status_tickets'));
        $this->set('_serialize', ['status_tickets']);
    }

    public function getCategoriesTickets()
    {
        $categories_tickets = $this->CategoriesTickets->find()->where(['deleted' => false]);

        $this->set(compact('categories_tickets'));
        $this->set('_serialize', ['categories_tickets']);
    }

    public function addStatus()
    {
        $status_ticket = $this->StatusTickets->newEntity();
        if ($this->request->is('post')) {

            if ($this->StatusTickets->find()->where(['name' => $this->request->getData('name')])->first()) {
                $this->Flash->warning(__('El nombre para el estado ya se encuentra en uso.'));
                return $this->redirect(['action' => 'config']);
            }

            $status_ticket = $this->StatusTickets->patchEntity($status_ticket, $this->request->getData());

            if ($this->StatusTickets->save($status_ticket)) {
                $this->Flash->success(__('El estado se ha agregado correctamente.'));
            } else {
                $this->Flash->error(__('El estado no se ha agregado. Por favor, intente nuevamente.'));
            }
        }

        return $this->redirect(['action' => 'config']);
    }

    public function addCategory()
    {
        $category_ticket = $this->CategoriesTickets->newEntity();
        if ($this->request->is('post')) {

            if ($this->CategoriesTickets->find()->where(['name' => $this->request->getData('name')])->first()) {
                $this->Flash->warning(__('El nombre para la categoría ya se encuentra en uso.'));
                return $this->redirect(['action' => 'config']);
            }

            $category_ticket = $this->CategoriesTickets->patchEntity($category_ticket, $this->request->getData());

            if ($this->CategoriesTickets->save($category_ticket)) {
                $this->Flash->success(__('La categoría se ha agregado correctamente.'));
            } else {
                $this->Flash->error(__('La categoría no se ha agregado. Por favor, intente nuevamente.'));
            }
        }

        return $this->redirect(['action' => 'config']);
    }

    public function editStatus()
    {
        $status_ticket = $this->StatusTickets->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {

            if ($this->StatusTickets->find()->where(['name' => $this->request->getData('name'), 'id !=' => $this->request->getData('id')])->first()) {
                $this->Flash->warning(__('El nombre para el estado ya se encuentra en uso.'));
                return $this->redirect(['action' => 'config']);
            }

            $status_ticket = $this->StatusTickets->patchEntity($status_ticket, $this->request->getData());

            if ($this->StatusTickets->save($status_ticket)) {
                $this->Flash->success(__('El estado se ha editado correctamente.'));
            } else {
                $this->Flash->error(__('La estado no se ha editado. Por favor, intente nuevamente.'));
            }
        }

        return $this->redirect(['action' => 'config']);
    }

    public function editCategory()
    {
        $category_ticket = $this->CategoriesTickets->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {

            if ($this->CategoriesTickets->find()->where(['name' => $this->request->getData('name'), 'id !=' => $this->request->getData('id')])->first()) {
                $this->Flash->warning(__('El nombre para la categoría ya se encuentra en uso.'));
                return $this->redirect(['action' => 'config']);
            }

            $category_ticket = $this->CategoriesTickets->patchEntity($category_ticket, $this->request->getData());

            if ($this->CategoriesTickets->save($category_ticket)) {
                $this->Flash->success(__('La categoría se ha editado correctamente.'));
            } else {
                $this->Flash->error(__('La categoría no se ha editado. Por favor, intente nuevamente.'));
            }
        }

        return $this->redirect(['action' => 'config']);
    }

    public function deleteStatus()
    {
        if ($this->request->is(['post'])) {
            $status_ticket = $this->StatusTickets->get($_POST['id']);
            $status_ticket->deleted = true;

            if ($this->StatusTickets->save($status_ticket)) {
                $this->Flash->success(__('Estado eliminado.'));
            } else {
                $this->Flash->error(__('Error al eliminar el estado.'));
            }
        }
        return $this->redirect(['action' => 'config']);
    }

    public function deleteCategory()
    {
        if ($this->request->is(['post'])) {
            $category_ticket = $this->CategoriesTickets->get($_POST['id']);
            $category_ticket->deleted = true;

            if ($this->CategoriesTickets->save($category_ticket)) {
                $this->Flash->success(__('Categoría eliminada.'));
            } else {
                $this->Flash->error(__('Error al eliminar la categoría.'));
            }
        }
        return $this->redirect(['action' => 'config']);
    }

    public function calendar()
    {
        $events = [];
        $parament = $this->request->getSession()->read('paraments');
        $this->loadModel('Users');
        $users = $this->Users->find('list')->where(['deleted' => 0, 'id NOT IN' => $parament->system->users]);
        if ($this->request->is(['post'])) {
            $tickets = json_decode($this->request->getData('tickets'));
            if (sizeof($tickets) < 1) {
                $this->Flash->warning(__('Debe generar una búsqueda de Tickets para poder visualizar los mismos en el calendario.'));
                return $this->redirect(['action' => 'index']);
            }

            foreach ($tickets as $ticket) {
                $ticket = $this->Tickets->get($ticket->id, [
                    'contain' => ['CategoriesTickets', 'StatusTickets', 'Users', 'Customers']
                ]);

                $event = new \stdClass;
                $event->title         = '#' . $ticket->id . ' ' . $ticket->category->name;
                $event->start         = $ticket->start_task;
                $event->start_task    = $ticket->start_task;
                $event->color         = $ticket->category->color_back;
                $event->textColor     = $ticket->category->color_text;
                $event->ticket_id     = $ticket->id;
                $event->customer      = $ticket->customer ? str_pad($ticket->customer->code, 5, "0", STR_PAD_LEFT) . ' ' .  $ticket->customer->name : '-';
                $event->ticket_title  = $ticket->title;
                $event->description   = $ticket->description;
                $event->status = $ticket->status->name;
                $event->user_asigned  = $ticket->asigned_user_id ? $ticket->asigned_user_id->name : '';
                array_push($events, $event);
            }
        }

        $this->set(compact('events', 'users'));
        $this->set('_serialize', ['events']);
    }

    public function changeStartTaskFromConfig()
    {
        $paraments = $this->request->getSession()->read('paraments');
        $paraments->ticket->save_change_start_task = !$paraments->ticket->save_change_start_task;

        $this->saveGeneralParaments($paraments);

        return $this->redirect(['action' => 'config']);
    }

    public function addPhoto()
    {
        if ($this->request->is('post')) {

            $folder = new Folder(WWW_ROOT . 'ticket_image', true, 0755);
            $this->loadModel('TicketsRecords');
            
            foreach ($this->request->getData('files') as $file) {

                $ext = explode('.', $file['name']);
                $now = new \DateTime();
                $newName = $now->getTimestamp() . '-' . $ext[0] . '.' .  end($ext);
                $from = $file['tmp_name'];
                $newName = str_replace (" ", "_", $newName);
                $to = WWW_ROOT . 'ticket_image/' . $newName;

                if (move_uploaded_file($from, $to)) {

                    $data['image'] = $newName;
                    $data['short_description'] = "Foto";
                    $data['user_id'] = $this->Auth->user()['id'];
                    $data['description'] = $this->request->getData('description');
                    $data['ticket_id'] = $this->request->getData('ticket_id');

                    $ticketRecord = $this->TicketsRecords->newEntity();
                    $ticketRecord = $this->TicketsRecords->patchEntity($ticketRecord, $data);

                    if ($this->TicketsRecords->save($ticketRecord)) {

                        $this->Flash->success(__('La foto se ha guardado correctamente.'));

                    } else {

                        $this->Flash->error(__('La foto no se ha guardado. Por favor, intente nuevamente.'));
                    }

                } else {
                    $this->Flash->error(__('La foto no se ha guardado. Por favor, intente nuevamente.'));
                }
            }

            return $this->redirect(['controller' => 'tickets', 'action' => 'index_' . $this->request->getData('view')]);
        }
    }

    public function addFromCustomer() 
    {
        if ($this->request->is('ajax')) {

            $ticket = $this->Tickets->newEntity();

            $data = $this->request->input('json_decode');
            $response = [
                'error' => TRUE,
                'msg'   => 'Error, no se ha creado el Ticket.',
                'type'  => 'error'
            ];

            $date_array = explode(' ', $data->start_task);
            //primer parte -> dd/mm/yyyy
            $start_task_1 = explode('/', $date_array[0]);
            //primer parte -> hh:mm
            $start_task_2 = explode(':', $date_array[1]);

            $start_task = $start_task_1[2] . '-' . $start_task_1[1] . '-' . $start_task_1[0] . ' ' . $start_task_2[0] . ':' . $start_task_2[1];

            $start_task = new Time($start_task);

            $request_data = [
                'user_id'       => $this->Auth->user()['id'],
                'start_task'    => $start_task,
                'title'         => $data->title,
                'description'   => $data->description,
                'customer_id'   => $data->customer_code,
                'area_id'       => $data->area_id, 
            ];

            $ticket = $this->Tickets->patchEntity($ticket, $request_data);

            if (isset($data->connection_id)) {
                $ticket->connection_id = $data->connection_id ? $data->connection_id : NULL;
            }

            $ticket->category = $data->category;
            $ticket->status = 1;

            if ($this->Tickets->save($ticket)) {
                $response = [
                    'error' => FALSE,
                    'msg'   => 'Ticket nº: ' . $ticket->id . '. Creado correctamente',
                    'type'  => 'success'
                ];
            } else {
                //$this->log($ticket->errors());
            }
        }

        $this->set(compact('response'));
        $this->set('_serialize', ['response']);
    }

    public function changeMasiveStatus()
    {
        if ($this->request->is('ajax')) { //Ajax Detection

            $action = 'Cambio Masivo de Estado de Tickets';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $response = new \stdClass;
            $response->error = false;
            $response->msg = 'Cambio de estado exitoso.';
            $response->typeMsg = 'success';

            $data = $this->request->input('json_decode')->data;

            $ok = 0;
            $error = 0;
            $total = 0;

            foreach ($data->ids as $id) {

                $total++;

                $ticket = $this->Tickets->get($id, [
                    'contain' => ['StatusTickets']
                ]);

                $before_status = $ticket->status->name;

                $ticket->status = $data->status;

                if ($this->Tickets->save($ticket)) {

                    $this->loadModel('TicketsRecords');
                    $ticketRecord = $this->TicketsRecords->newEntity();

                    $ticketRecord->ticket_id = $id;
                    $ticketRecord->short_description = "Cambio de estado: " . $before_status . ' -> ' . $data->status_name;
                    $ticketRecord->description = "Cambio de estado: " . $before_status . ' -> ' . $data->status_name;
                    $ticketRecord->user_id = $this->Auth->user()['id'];

                    if ($this->TicketsRecords->save($ticketRecord)) {
                        $ok++;
                    } else {
                        $error++;
                    }

                } else {
                    $error++;
                }
            }

            $response->msg .= "<br/>" . 'Cant. ticket cambiados estados: ' . $ok . "<br/>" . "Cant. ticket no cambiados estados: " . $error . "<br/>" . "Total ticket enviados: " . $total;

            $this->set('response', $response);
        }
    }

    public function addCustomer()
    {
        if ($this->request->is('ajax')) { //Ajax Detection

            $response = new \stdClass;
            $response->error = false;
            $response->msg = 'Agregado de cliente al Ticket exitoso.';
            $response->typeMsg = 'success';

            $data = $this->request->input('json_decode')->data;

            $ticket = $this->Tickets->get($data->ticket_id, [
                'contain' => ['Customers']
            ]);

            $before_customer = $ticket->customer ? $ticket->customer->name . ' (' . $ticket->customer->code . ')' : '';

            $ticket->customer_id = $data->customer_code;

            $this->loadModel('Customers');
            $customer = $this->Customers->get($data->customer_code);

            if ($this->Tickets->save($ticket)) {
                $this->loadModel('TicketsRecords');
                $ticketRecord = $this->TicketsRecords->newEntity();

                $ticketRecord->ticket_id = $data->ticket_id;
                $ticketRecord->short_description = "Agregado de cliente: anterior: " . $before_customer . ' - Nuevo: ' . $customer->name . ' (' . $customer->code . ')';
                $ticketRecord->description = "Agregado de cliente: anterior: " . $before_customer . ' - Nuevo: ' . $customer->name . ' (' . $customer->code . ')';
                $ticketRecord->user_id = $this->Auth->user()['id'];

                if (!$this->TicketsRecords->save($ticketRecord)) {
                    $response->error = true;
                    $response->msg = 'Error al intentar agregar un clienta al Ticket.';
                    $response->typeMsg = 'error';
                }
            } else {
                $response->error = true;
                $response->msg = 'Error al intentar agregar un clienta al Ticket.';
                $response->typeMsg = 'error';
            }

            $this->set('response', $response);
        }
    }

    public function clearCustomer()
    {
        if ($this->request->is('ajax')) { //Ajax Detection

            $response = new \stdClass;
            $response->error = false;
            $response->msg = 'Se ha quitado el cliente del Ticket exitosamente.';
            $response->typeMsg = 'success';

            $data = $this->request->input('json_decode')->data;

            $ticket = $this->Tickets->get($data->ticket_id, [
                'contain' => ['Customers']
            ]);

            $before_customer = $ticket->customer ? $ticket->customer->name . ' (' . $ticket->customer->code . ')' : '';

            $ticket->customer_id = NULL;

            if ($this->Tickets->save($ticket)) {
                $this->loadModel('TicketsRecords');
                $ticketRecord = $this->TicketsRecords->newEntity();

                $ticketRecord->ticket_id = $data->ticket_id;
                $ticketRecord->short_description = "Se elimino al cliente del Ticket: " . $before_customer;
                $ticketRecord->description = "Se elimino al cliente del Ticket: " . $before_customer;
                $ticketRecord->user_id = $this->Auth->user()['id'];

                if (!$this->TicketsRecords->save($ticketRecord)) {
                    $response->error = true;
                    $response->msg = 'Error al intentar eliminar el clienta del Ticket.';
                    $response->typeMsg = 'error';
                }
            } else {
                $response->error = true;
                $response->msg = 'Error al intentar eliminar el clienta del Ticket.';
                $response->typeMsg = 'error';
            }

            $this->set('response', $response);
        }
    }
}
