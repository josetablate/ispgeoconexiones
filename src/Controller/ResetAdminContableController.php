<?php
namespace App\Controller;

use Cake\I18n\Time;
use App\Controller\AppController;

/**
 * Labels Controller
 *
 * @property \App\Model\Table\LabelsTable $Labels
 *
 * @method \App\Model\Entity\Label[] paginate($object = null, array $settings = [])
 */
class ResetAdminContableController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Accountant', [
            'className' => '\App\Controller\Component\Admin\Accountant'
        ]);

    }

    public function isAuthorized($user = null)
    {
        return true;
    }

    public function index(){

        dd('hola desde MatenimientoController');

    }

    public function resetContable()
    {
        die(__('resetContable disable'));

        $this->loadModel('Customers');
        $customers = $this->Customers->find()->where(['deleted' => 0, 'debt_month !=' => 0]);

        $seating = $this->Accountant->addSeating([
            'concept' => 'Saldo',
            'comments' => '',
            'seating' => NULL,
            'account_debe' => NULL,
            'account_haber' => NULL,
            'redirect' => null
        ]);

        $debe_total = 0;
        $haber_total = 0;

        foreach($customers as $customer){

            if($customer->debt_month > 0){

                $seating = $this->Accountant->addSeating([
                    'concept' => '', 
                    'comments' => '',
                    'seating' => $seating,
                    'account_debe' => $customer->account_code,
                    'account_haber' => NULL,
                    'value' => $customer->debt_month,
                    'redirect' => null
                ]);

                $debe_total += $customer->debt_month;

            }else{

                $seating = $this->Accountant->addSeating([
                    'concept' => '', 
                    'comments' => '',
                    'seating' => $seating,
                    'account_debe' => NULL,
                    'account_haber' => $customer->account_code,
                    'value' => abs($customer->debt_month),
                    'redirect' => null
                ]);

                $haber_total += abs($customer->debt_month);
                
            }
        }

        $diff = 0;

        if($debe_total > $haber_total){

            $diff = $debe_total - $haber_total;

            $seating = $this->Accountant->addSeating([
                'concept' => '', 
                'comments' => '',
                'seating' => $seating,
                'account_debe' => NULL,
                'account_haber' => 400000001,
                'value' => $diff,
                'redirect' => null
            ]);


        }
        else  if($debe_total < $haber_total){

            $diff = $haber_total - $debe_total;

            $seating = $this->Accountant->addSeating([
                'concept' => '', 
                'comments' => '',
                'seating' => $seating,
                'account_debe' => 400000001,
                'account_haber' => NULL,
                'value' => $diff,
                'redirect' => null
            ]);
        }

        die(__('resetContable end'));
    }


    public function deleteAllOldMoves()
    {
        die(__('deleteAllOldMoves disable'));


        /**eliminar facturas y concetos */

        // $this->loadModel('Invoices');
        // $invoices_delete = $this->Invoices->find()->select(['id'])->where(['not_delete' => false])->toArray();

        // $invoices_delete_ids = array_map(function($c){
        //     return $c->id;
        // }, $invoices_delete);

        // // dd($invoices_delete_ids);

        // $this->loadModel('Concepts');
        // $this->Concepts->deleteAll(['comprobante_id IN' => $invoices_delete_ids]);
        // $this->Invoices->deleteAll(['not_delete' => false]);


         /**eliminar paygos y recibos */

        // $this->loadModel('Payments');    
        // $this->Payments->deleteAll(['not_delete' => false]);

        // $this->loadModel('Receipts');   
        // $this->Receipts->deleteAll(['not_delete' => false]);
      

        // /**eliminar notas de ceditos y sus conceptos */

        // $this->loadModel('CreditNotes');
        // $credits_delete = $this->CreditNotes->find()->select(['id'])->where(['not_delete' => false])->toArray();

        // $credits_delete_ids = array_map(function($c){
        //     return $c->id;
        // }, $credits_delete); 

        // $this->loadModel('ConceptsCredit');
        // $this->ConceptsCredit->deleteAll(['credit_note_id IN' => $credits_delete_ids]);

        // $this->CreditNotes->deleteAll(['not_delete' => false]);


        // /**eliminar notas de debitos y sus conceptos */

        // $this->loadModel('DebitNotes');
        // $debits_delete = $this->DebitNotes->find()->select(['id'])->where(['not_delete' => false])->toArray();

        // $debits_delete_ids = array_map(function($c){
        //     return $c->id;
        // }, $debits_delete);        


        // $this->loadModel('ConceptsDebit');
        // $this->ConceptsDebit->deleteAll(['debit_note_id IN' => $debits_delete_ids]);

        // $this->DebitNotes->deleteAll(['not_delete' => false]);

        die(__('deleteAllOldMoves end'));

    }


    public function getMovesOnlyCurrentDebt($blockNumber = 0)
    {
        die(__('getMovesOnlyCurrentDebt disable'));

        $moves = [];

        $blockAmount = 3000;
        
        $salto = $blockAmount * $blockNumber;

        $this->loadModel('Customers');
        $customers = $this->Customers->find()->where(['deleted' => 0])->toArray();

        $customers = array_slice($customers, $salto);

        $i = 0;

        foreach ($customers as $customer) {

            if ($i > $blockAmount) {
                break;
            }

            $i++;        
        
            $moves = array_merge( $moves, $this->getMoves($customer->code) );     
       
        }

        $this->markNoDelete($moves);

        // return $this->toCSV(json_decode(json_encode($moves), true), 'clientes_moves_all');
        
 

        die(__('getMovesOnlyCurrentDebt end moves amount {0} clientes {1}', count($moves), count($customers)));
    }

    private function markNoDelete($moves){

        /**marco las facturas que no debo eliminar  */

        $invoices = array_filter($moves, function($c){
            return $c->type_move == 'invoice';       
        });

        if(count($invoices) > 0){
            
            $invoices_ids = array_map(function($c){          
                return $c->id;
            }, $invoices);

            $this->loadModel('Invoices');
            $this->Invoices->updateAll([
                'not_delete' => true
            ],
            [
                'id IN' => $invoices_ids
            ]);
        }

        /**marco los pagos y los recibos que no debo eliminar  */

        $payments = array_filter($moves, function($c){
            return $c->type_move == 'payment';       
        });

        if(count($payments) > 0){
            
            $payments_ids = array_map(function($c){          
                return $c->id;
            }, $payments);

            $this->loadModel('Payments');
            $this->Payments->updateAll([
                'not_delete' => true
            ],
            [
                'id IN' => $payments_ids
            ]);

            $receipts_ids = array_map(function($c){          
                return $c->receipt_id;
            }, $payments);
            

            $this->loadModel('Receipts');
            $this->Receipts->updateAll([
                'not_delete' => true
            ],
            [
                'id IN' => $receipts_ids
            ]);
        }


        /**marco las notas de credito que no debo eliminar  */

        $credits = array_filter($moves, function($c){
            return $c->type_move == 'credit';       
        });

        if(count($credits) > 0){
            
            $credits_ids = array_map(function($c){          
                return $c->id;
            }, $credits);

            $this->loadModel('CreditNotes');
            $this->CreditNotes->updateAll([
                'not_delete' => true
            ],
            [
                'id IN' => $credits_ids
            ]);
        }


        /**marco las notas de debits que no debo eliminar  */

        $debits = array_filter($moves, function($c){
            return $c->type_move == 'debit';       
        });

        if(count($debits) > 0){
            
            $debits_ids = array_map(function($c){          
                return $c->id;
            }, $debits);

            $this->loadModel('DebitNotes');
            $this->DebitNotes->updateAll([
                'not_delete' => true
            ],
            [
                'id IN' => $debits_ids
            ]);
        }
    }


    private function getMoves($code)
    {
        // Debug("codigo: $code");

        $movesAll = [];      

        $movesAll = array_merge($movesAll, $this->getInvoicesMoves($code));    
        $movesAll = array_merge($movesAll, $this->getPaymentsMoves($code));    
        $movesAll = array_merge($movesAll, $this->getCreditsMoves($code));    
        $movesAll = array_merge($movesAll, $this->getDebitsMoves($code)); 
        

        // Debug('$movesAll listo');

        // dd($movesAll);

        usort($movesAll, function($a, $b)
        {
              if ($a->date < $b->date) {
                  return -1;
              } else if ($a->date > $b->date) {
                  return 1;
              } else {
                  return 0;
              }
        });

        $dataFiltered = [];

        $saldo = 0;

        foreach ($movesAll as $move) {             
         
            $saldo += $move->total;
            $move->saldo = $saldo;
  
            $dataFiltered[] = $move;

            // Debug(__(" fecha: {0} total: {1} saldo {2}", $move->date->format('d/m/Y'), $move->total, $saldo ));

            if ($saldo == 0) {
                $dataFiltered = [];
            }          
        }   

        // Debug('$dataFiltered listo');

        // foreach ($dataFiltered as $d) {  
        //     Debug(__(" fecha: {0} total: {1} saldo {2}", $d->date->format('d/m/Y'), $d->total, $d->saldo));
        // }         

        return $dataFiltered; 

        // return array_reverse($dataFiltered); 

    }

    private function getInvoicesMoves($code)
    {
        $moves = [];

        $this->loadModel('Invoices');
        $invoices = $this->Invoices->find()->where(['customer_code' => $code]);    

        foreach ($invoices as $invoice) {
            $moves[] = new Move($invoice, 'invoice'); 
        }

        return $moves;
    } 

    private function getPaymentsMoves($code)
    {
        $moves = [];

        $this->loadModel('Payments');
        $payments = $this->Payments->find()->where(['customer_code' => $code, 'anulated IS' => null]);    

        foreach ($payments as $payment) {
            $moves[] = new Move($payment, 'payment'); 
        }

        return $moves;
    } 

    private function getCreditsMoves($code)
    {
        $moves = [];

        $this->loadModel('CreditNotes');
        $creditNotes = $this->CreditNotes->find()->where(['customer_code' => $code]);    

        foreach ($creditNotes as $creditNote) {
            $moves[] = new Move($creditNote, 'credit'); 
        }

        return $moves;
    } 

    private function getDebitsMoves($code)
    {
        $moves = [];

        $this->loadModel('DebitNotes');
        $debitNotes = $this->DebitNotes->find()->where(['customer_code' => $code]);    

        foreach ($debitNotes as $debitNote) {
            $moves[] = new Move($debitNote, 'debit'); 
        }

        return $moves;
    } 

    private function toCSV($data, $filename)
    {
        // dd($this->_ctrl);

        $response = $this->response;
        $response = $response->withStringBody($this->dataToStringCSV($data));
        $response = $response->withType('csv');
   
        $filename .= '.csv';
        $response = $response->withDownload($filename);
        // ob_clean(); // clean slate
        return $response;
    }

    private function dataToStringCSV($data)
    {
        $string = '';
        $isPrintHeader = false;
        foreach ($data as $row) {
            if (!$isPrintHeader) {
                $string .= utf8_decode(implode(";", array_keys($row))) . "\n";
                $isPrintHeader = true;
            }
            $r = array_map(function ($v) {
                if (is_numeric($v)) {
                    return number_format($v, 2, ',', '');
                } else {
                    return $v;
                }
            }, array_values($row));
            $string .= utf8_decode(implode(";", array_values($r))) . "\n";
        }
       
        return $string;
    }

    



 
}

class Move {

    public $id;
    public $date; 
    public $total; 
    public $saldo;
    public $type_move;
    public $customer_code;


    public function __construct($data, $type) {

        switch ($type) {

            case 'invoice':

                $this->id = $data->id;
                $this->date = $data->date;            
                $this->total = $data->total;          
                $this->saldo = 0; 
                $this->type_move = $type;  
                $this->customer_code = $data->customer_code;            

                break;

            case 'payment':

                $this->id = $data->id;
                $this->date = $data->created;              
                $this->total = -$data->import;              
                $this->saldo = 0;     
                $this->type_move = $type;     
                $this->customer_code = $data->customer_code;   
                $this->receipt_id = $data->receipt_id;               

                break;

            case 'debit':

                $this->id = $data->id;
                $this->date = $data->date;           
                $this->total = $data->total;              
                $this->saldo = 0;     
                $this->type_move = $type;           
                $this->customer_code = $data->customer_code;                 

                break;

            case 'credit':

                $this->id = $data->id;
                $this->date = $data->date;              
                $this->total = -$data->total;             
                $this->saldo = 0;  
                $this->type_move = $type;  
                $this->customer_code = $data->customer_code;         

                break;
        }
    }
}