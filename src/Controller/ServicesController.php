<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Services Controller
 *
 * @property \App\Model\Table\ServicesTable $Services
 */
class ServicesController extends AppController
{
    
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Accountant', [
             'className' => '\App\Controller\Component\Admin\Accountant'
        ]);

        $this->loadComponent('IntegrationRouter', [
             'className' => '\App\Controller\Component\Integrations\IntegrationRouter'
        ]);
    }

    public function isAuthorized($user = null)
    {
        if ($this->request->getParam('action') == 'getAllAjax') {
            return true;
        }

        if ($this->request->getParam('action') == 'getAll') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function index()
    {}

    public function getAll()
    {
        $this->loadModel('Customers');
        $this->loadModel('Connections');

        $services = $this->Services
            ->find()
            ->contain([
                'Accounts'
            ])->where([
                'Services.deleted' => FALSE
            ]);

        foreach ($services as $service) {

            $service_id = $service->id;

            $customers_count = $this->Customers
                ->find()
                ->matching('Connections', function ($q) use ($service_id) {
                    return $q->where([
                        'Connections.deleted'    => FALSE,
                        'Connections.enabled'    => TRUE,
                        'Connections.service_id' => $service_id
                    ]);
                })->count();

            $service->customers_count = $customers_count;

            $connections_count = $this->Connections
                ->find()
                ->where([
                    'deleted'    => FALSE,
                    'enabled'    => TRUE,
                    'service_id' => $service->id
                ])->count();

            $service->connections_count = $connections_count;
        }

        $this->set(compact('services'));
        $this->set('_serialize', ['services']);
    }

    public function getAllAjax()
    {
        if ($this->request->is('ajax')) {//Ajax Detection

            $services = $this->Services
                ->find()
                ->where([
                    'enabled' => TRUE, 
                    'deleted' => FALSE
                ])->toArray();

            if (count($services) > 0) {
                 $this->set('services', $services);
            } else {
                 $this->set('services', FALSE);
            }
        }
    } 

    public function add()
    {
        $parament = $this->request->getSession()->read('paraments');
        $afip_codes = $this->request->getSession()->read('afip_codes');

        //si la contabilidada esta activada registro el asiento
        if ($this->Accountant->isEnabled()) {
            if (!$parament->accountant->acounts_parent->services) {
                $this->Flash->warning(__('Debe especificar la cuenta padre de los servicios.'));
                return $this->redirect(['action' => 'add']);
            }
        }

        $service = $this->Services->newEntity();

        if ($this->request->is('post')) {

            $s = $this->Services
                ->find()
                ->where([
                    'name'    => $this->request->getData('name'), 
                    'deleted' => FALSE
                ])->first();

            if (!$s) {

                $request_data = $this->request->getData();
                $request_data['account_code'] = NULL;

                //si la contabilidada esta activada registro el asientop
                if ($this->Accountant->isEnabled()) {
                    $account = $this->Accountant->createAccount([
                        'account_parent' => $parament->accountant->acounts_parent->services,
                        'name' => $this->request->getData('name'),
                        'redirect' =>['action' => 'add']
                    ]);
                    $request_data['account_code'] = $account->code;
                }

                $service = $this->Services->patchEntity($service, $request_data);

                if ($this->Services->save($service)) {
                    $this->Flash->success(__('Servicio agregado correctamente.'));
                    return $this->redirect(['action' => 'add']);
                } else {

                    Debug($service->errors());

                    $this->Flash->error(__('Error al agregar el Servicio.'));

                    //si la contabilidada esta activada registro el asientop
                    if ($this->Accountant->isEnabled()) {
                        $this->Accountant->deleteAccount($account);
                        
                    }
                }

            } else {
                $this->Flash->warning(__('El nombre "{0}" de servicio ya existe.', $s->name ));
            }
        }

        $this->set(compact('service'));
        $this->set('_serialize', ['service']);
    }

    public function edit($id = null)
    {
        $service = $this->Services->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {

            $s = $this->Services
                ->find()
                ->where([
                    'name'    => $this->request->getData('name'), 
                    'deleted' => FALSE, 
                    'id != '  => $id
                ])->first();

            if (!$s) {
                $service = $this->Services->patchEntity($service, $this->request->getData());

                if ($this->Services->save($service)) {

                    //si la contabilidada esta activada registro el asientop
                    if ($this->Accountant->isEnabled()) {
                        $this->loadModel('Accounts');
                        $account = $this->Accounts
                            ->find()
                            ->where([
                                'code' => $this->request->getData('account_code')
                            ])->first();

                        if (!$account) {
                            $this->Flash->warning(__('No existe el número de cuenta {0}.', $this->request->getData('account_code')));
                            return $this->redirect(['action' => 'edit', $id]);
                        }

                        //edito cuenta contable del servicio
                        $this->loadModel('Accounts');

                        $account = $this->Accounts
                            ->find()
                            ->where([
                                'code' => $service->account_code,
                        ])->first();

                        $account->name = $service->name;

                        if (!$this->Accounts->save($account)) {
                            $this->Flash->error(__('No se pudo actualizar la cuenta.'));
                        }
                    }

                    $this->Flash->success(__('Datos actualizados.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('Error al actualizar los datos.'));
                }
            } else {
                $this->Flash->warning(__('El nombre "{0}" de servicio ya existe.', $s->name ));
            }
        }

        $this->set(compact('service'));
        $this->set('_serialize', ['service']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Service id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);

        $service = $this->Services->get($_POST['id']);

        //verifico referrencias

        $this->loadModel('Connections');
        $conection = $this->Connections
            ->find()
            ->where([
                'service_id' => $service->id, 
                'deleted'    => FALSE
            ])->first();

        if ($conection) {
            $this->Flash->error(__('No se puede eliminar este servicio por que existen referencias.'));
            return $this->redirect(['action' => 'index']);
        }

        $this->loadModel('Controllers');
        $controllers = $this->Controllers
            ->find()
            ->where([
                'deleted' => FALSE
            ]);

        foreach ($controllers as $controller) {
            if ($this->IntegrationRouter->existService($controller, $service->id)) {
                $this->Flash->error(__('No se puede eliminar este servicio por que existen referencias.'));
                return $this->redirect(['action' => 'index']);
            }
        }

        if (!$this->IntegrationRouter->removeService($controller, $service->id)) {
            $this->Flash->error(__('Error al intentar eliminar el servicio.'));
            return $this->redirect(['action' => 'index']);
        }

        $service->deleted = TRUE;
        $this->Services->save($service);

        $this->Flash->success(__('Servicio eliminado correctamente.'));
        return $this->redirect(['action' => 'index']);
    }
}
