<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;
use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Query;

/**
 * ImpuestosController Controller
 *
 */
class ImpuestosController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Invoices');
        $this->loadModel('CreditNotes');
        $this->loadModel('DebitNotes');
    }

    public function isAuthorized($user = null)
    {
        return parent::allowRol($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $paraments = $this->request->getSession()->read('paraments');

        if ($this->request->is('post')) {

            $date_start = explode('/', $this->request->getData('date_start'));
            $date_start = new Time($date_start[2] . '-' . $date_start[1] . '-' . $date_start[0] . ' 00:00:00');

            $date_end = explode('/', $this->request->getData('date_end'));
            $date_end = new Time($date_end[2] . '-' . $date_end[1] . '-' . $date_end[0] . ' 23:59:59');

            $params = new \stdClass;
            $params->date_start = $date_start;
            $params->date_end = $date_end;
            $params->business_billing = $this->request->getData('business_billing');

            switch ($this->request->getData('files')) {
                case 'siap-vta':
                    return $this->ventaSiap($params);
                    break;
                case 'siap-alicuota-vta':
                    return $this->alicuotaVentaSiap($params);
                    break;
            }
        }

        $business = [];
        $business[''] = 'Seleccione Empresa';

        foreach ($paraments->invoicing->business as $b) {

            if ($b->enable && $b->responsible == 1) {

                $business[$b->id] = $b->name . ' (' . $b->address . ')';
            }
        }

        $files = [
            ''                  => __('Seleccionar archivo'),
            'siap-vta'          => __('SIAP vta'),
            'siap-alicuota-vta' => __('SIAP alícuota vta'),
            'alicuotas-vta' => __('Alícuotas vta (EXCEL)'),
        ];

        $this->set(compact('business', 'files'));
    }

    public function ventaSiap($params)
    {
        $comprobantes = [];
        $paraments = $this->request->getSession()->read('paraments');

        // $invoices = $this->Invoices
        //     ->find()
        //     ->where([
        //         'business_id'   => $params->business_billing,
        //         'tipo_comp !='  => 'XXX',
        //         'total >'       => 0,
        //         'date_start >=' => $params->date_start,
        //         'date_end <='   => $params->date_end
        //     ])->map(function ($row) { 
        //         $row->type = 'invoice';
        //         return $row;
        //     })->toArray();

        $invoices = $this->Invoices
            ->find()
            ->where([
                'business_id'  => $params->business_billing,
                'tipo_comp !=' => 'XXX',
                'total >'      => 0
            ])->andWhere(function (QueryExpression $exp, Query $q) use ($params) {
                return $exp->between('date', $params->date_start, $params->date_end);
            })->map(function ($row) { 
                $row->type = 'invoice';
                return $row;
            })->toArray();

        // $credit_notes = $this->CreditNotes
        //     ->find()
        //     ->where([
        //         'business_id'   => $params->business_billing,
        //         'tipo_comp !='  => 'NCX',
        //         'total >'       => 0,
        //         'date_start >=' => $params->date_start,
        //         'date_end <='   => $params->date_end
        //     ])->map(function ($row) { 
        //         $row->type = 'credit';
        //         return $row;
        //     })->toArray();

        $credit_notes = $this->CreditNotes
            ->find()
            ->where([
                'business_id'  => $params->business_billing,
                'tipo_comp !=' => 'NCX',
                'total >'      => 0
            ])->andWhere(function (QueryExpression $exp, Query $q) use ($params) {
                return $exp->between('date', $params->date_start, $params->date_end);
            })->map(function ($row) { 
                $row->type = 'credit';
                return $row;
            })->toArray();

        // $debit_notes = $this->DebitNotes
        //     ->find()
        //     ->where([
        //         'business_id'   => $params->business_billing,
        //         'tipo_comp !='  => 'NDX',
        //         'total >'       => 0,
        //         'date_start >=' => $params->date_start,
        //         'date_end <='   => $params->date_end
        //     ])->map(function ($row) { 
        //         $row->type = 'debit';
        //         return $row;
        //     })->toArray();

        $debit_notes = $this->DebitNotes
            ->find()
            ->where([
                'business_id'  => $params->business_billing,
                'tipo_comp !=' => 'NDX',
                'total >'      => 0
            ])->andWhere(function (QueryExpression $exp, Query $q) use ($params) {
                return $exp->between('date', $params->date_start, $params->date_end);
            })->map(function ($row) { 
                $row->type = 'debit';
                return $row;
            })->toArray();

        $comprobantes = array_merge($invoices, $credit_notes, $debit_notes);
        $comprobantes_size = sizeof($invoices) + sizeof($credit_notes) + sizeof($debit_notes);

        $rows = "";
        $business = NULL;

        foreach ($paraments->invoicing->business as $b) {
            if ($b->id == $params->business_billing) {
                $business = $b;
            }
        }

        foreach ($comprobantes as $key => $comprobante) {

            // FECHA
            $date = $comprobante->date->year . str_pad($comprobante->date->month, 2, "0", STR_PAD_LEFT) . str_pad($comprobante->date->day, 2, "0", STR_PAD_LEFT);

            // TIPO DE COMPROBANTE
            $tipo_comp = str_pad($comprobante->tipo_comp, 3, "0", STR_PAD_LEFT);

            // PTO. VTA.
            $pto_vta = str_pad($comprobante->pto_vta, 5, "0", STR_PAD_LEFT);

            // NUMERO
            $num = str_pad($comprobante->num, 20, "0", STR_PAD_LEFT);

            // COD. DOC. COMPRADOR
            $customer_doc_type = str_pad($comprobante->customer_doc_type, 2, "0", STR_PAD_RIGHT);

            // NRO ID COMPRADOR
            $customer_ident = str_pad($comprobante->customer_ident, 20, "0", STR_PAD_LEFT);

            // RAZON SOC. COMPRADOR
            $customer_name = substr($comprobante->customer_name, 0, 29);
            $customer_name = str_pad(utf8_decode($customer_name), 30, " ", STR_PAD_RIGHT);

            // IMPORTE TOTAL OPERACION
            $total = number_format((float) $comprobante->total, 2, '.', '');
            $total = str_replace(".", "", $total);
            $total = str_pad($total, 15, "0", STR_PAD_LEFT);

            // PARTE OSCURA
            // Importes q no integran el NG
            $dark_part_1_imp_no_integra_ng = str_pad("", 15, "0", STR_PAD_LEFT);

            // Percepc a No Categorizados
            $dark_part_2_percepc_no_categorizados = str_pad("", 15, "0", STR_PAD_LEFT);

            // Imp. Operaciones Exentas
            $dark_part_3_imp_operaciones_exentas = str_pad("", 15, "0", STR_PAD_LEFT);

            // Percepc.Pgo a Cta de otros imp.Nac.
            $dark_part_4_percepc_pgo_cta_otro_imp_nac = str_pad("", 15, "0", STR_PAD_LEFT);

            // Percepc.Ingresos Brutos
            $dark_part_5_percepc_ingresos_brutos = str_pad("", 15, "0", STR_PAD_LEFT);

            // Percepc.Imp. Municip.
            $dark_part_6_percepc_imp_municip = str_pad("", 15, "0", STR_PAD_LEFT);

            // Imp.Int.
            $dark_part_7_imp_int = str_pad("", 15, "0", STR_PAD_LEFT);

            // Codigo de moneda
            $codigo_moneda = str_pad("PES", 3, "0", STR_PAD_LEFT);

            // Tipo de cambio
            $tipo_moneda_entero = str_pad(1, 4, "0", STR_PAD_LEFT);

            $tipo_moneda_decimal = str_pad(0, 6, "0", STR_PAD_LEFT);

            $id = $comprobante->id;

            switch ($comprobante['type']) {

                case 'invoice':
                    $sql = "SELECT COUNT(DISTINCT(`tax`)) AS cantidad_tipo_alicuota 
                        FROM  `concepts` 
                        WHERE  `comprobante_id` = $id
                        ORDER BY  `tax`"; 
                    break;

                case 'credit':
                    $sql = "SELECT COUNT(DISTINCT(`tax`)) AS cantidad_tipo_alicuota 
                        FROM  `concepts_credit` 
                        WHERE  `credit_note_id` = $id
                        ORDER BY  `tax`"; 
                    break;

                case 'debit':
                    $sql = "SELECT COUNT(DISTINCT(`tax`)) AS cantidad_tipo_alicuota 
                        FROM  `concepts_debit` 
                        WHERE  `debit_note_id` = $id
                        ORDER BY  `tax`"; 
                    break;
            }

            $database = ConnectionManager::get('default');
            $result_query = $database->execute($sql)->fetchAll('assoc');

            $cant_alicuota = $result_query[0]['cantidad_tipo_alicuota'];

            // Responsable Monotributo
            if ($business->responsible == 6) {
                $cant_alicuota = 1;
            }

            // Cantidad alicuotas IVA
            $cant_alicuota_iva = str_pad($cant_alicuota, 1, "0", STR_PAD_LEFT);

            // Codigo de operacion 
            // parte oscura 8
            $dark_part_8_codigo_operacion = str_pad("", 1, "0", STR_PAD_LEFT);

            // Otros Tributos
            // parte oscura 9
            $dark_part_9_otros_atributos = str_pad("", 15, "0", STR_PAD_LEFT);

            //Fecha Vencim.Pago
            $duedate = $comprobante->duedate->year . str_pad($comprobante->duedate->month, 2, "0", STR_PAD_LEFT) . str_pad($comprobante->duedate->day, 2, "0", STR_PAD_LEFT);

            $rows .= $date . $tipo_comp . $pto_vta . $num . $num . $customer_doc_type . $customer_ident . $customer_name . $total . $dark_part_1_imp_no_integra_ng . $dark_part_2_percepc_no_categorizados . $dark_part_3_imp_operaciones_exentas . $dark_part_4_percepc_pgo_cta_otro_imp_nac . $dark_part_5_percepc_ingresos_brutos . $dark_part_6_percepc_imp_municip . $dark_part_7_imp_int . $codigo_moneda . $tipo_moneda_entero . $tipo_moneda_decimal . $cant_alicuota_iva . $dark_part_8_codigo_operacion . $dark_part_9_otros_atributos . $duedate;
            if (($key + 1) != $comprobantes_size) {
                $rows .= "\r\n";
            }
        }

        $filename = 'siap/venta/';
        $created = Time::now()->format('Ymd');
        $filenamex = $created . '-' . 'venta-siap.txt';
        $filename .= $filenamex;

        $file = new File(WWW_ROOT . $filename, false, 0644);

        if ($file->write($rows, 'w', true)) {

            $this->response->file(WWW_ROOT . $filename, array(
                'download' => true,
                'name'     => $filenamex,
            ));
            $file->close();
            return $this->response;

        } else {
            $this->Flash->error(__('Error al generar el archivo VTA SIAP.'));
        }
        $file->close();
    }

    public function alicuotaVentaSiap($params)
    {
        $comprobantes = [];
        $paraments = $this->request->getSession()->read('paraments');

        // $invoices = $this->Invoices
        //     ->find()
        //     ->contain(['Concepts'])
        //     ->where([
        //         'business_id'   => $params->business_billing,
        //         'tipo_comp !='  => 'XXX',
        //         'total >'       => 0,
        //         'date_start >=' => $params->date_start,
        //         'date_end <='   => $params->date_end
        //     ])
        //     ->map(function ($row) { 
        //         $row->type = 'invoice';
        //         return $row;
        //     })->toArray();

        $invoices = $this->Invoices
            ->find()
            ->contain(['Concepts'])
            ->where([
                'business_id'  => $params->business_billing,
                'tipo_comp !=' => 'XXX',
                'total >'      => 0
            ])->andWhere(function (QueryExpression $exp, Query $q) use ($params) {
                return $exp->between('date', $params->date_start, $params->date_end);
            })->map(function ($row) { 
                $row->type = 'invoice';
                return $row;
            })->toArray();

        // $credit_notes = $this->CreditNotes
        //     ->find()
        //     ->contain(['ConceptsCredit'])
        //     ->where([
        //         'business_id'   => $params->business_billing,
        //         'tipo_comp !='  => 'NCX',
        //         'total >'       => 0,
        //         'date_start >=' => $params->date_start,
        //         'date_end <='   => $params->date_end
        //     ])->map(function ($row) { 
        //         $row->type = 'concepts_credit';
        //         return $row;
        //     })->toArray();

        $credit_notes = $this->CreditNotes
            ->find()
            ->contain(['ConceptsCredit'])
            ->where([
                'business_id'  => $params->business_billing,
                'tipo_comp !=' => 'NCX',
                'total >'      => 0
            ])->andWhere(function (QueryExpression $exp, Query $q) use ($params) {
                return $exp->between('date', $params->date_start, $params->date_end);
            })->map(function ($row) { 
                $row->type = 'concepts_credit';
                return $row;
            })->toArray();

        // $debit_notes = $this->DebitNotes
        //     ->find()
        //     ->contain(['ConceptsDebit'])
        //     ->where([
        //         'business_id'   => $params->business_billing,
        //         'tipo_comp !='  => 'NDX',
        //         'total >'       => 0,
        //         'date_start >=' => $params->date_start,
        //         'date_end <='   => $params->date_end
        //     ])->map(function ($row) { 
        //         $row->type = 'concepts_debit';
        //         return $row;
        //     })->toArray();

        $debit_notes = $this->DebitNotes
            ->find()
            ->contain(['ConceptsDebit'])
            ->where([
                'business_id'  => $params->business_billing,
                'tipo_comp !=' => 'NDX',
                'total >'      => 0
            ])->andWhere(function (QueryExpression $exp, Query $q) use ($params) {
                return $exp->between('date', $params->date_start, $params->date_end);
            })->map(function ($row) { 
                $row->type = 'concepts_debit';
                return $row;
            })->toArray();

        $comprobantes = array_merge($invoices, $credit_notes, $debit_notes);

        $rows = "";
        $business = NULL;

        foreach ($paraments->invoicing->business as $b) {
            if ($b->id == $params->business_billing) {
                $business = $b;
            }
        }

        foreach ($comprobantes as $key => $comprobante) {

            $taxs = [];

            switch ($comprobante['type']) {

                case 'invoice':
                    foreach ($comprobante->concepts as $concept) {
                        if (!array_key_exists($concept->tax, $taxs)) {
                            $taxs[$concept->tax] = [
                                'sum_tax'   => 0,
                                'sum_price' => 0
                            ];
                        }
                        $taxs[$concept->tax]['sum_tax']   += $concept->sum_tax;
                        $taxs[$concept->tax]['sum_price'] += $concept->sum_price;
                    }
                    break;

                case 'concepts_credit':
                    foreach ($comprobante->concepts_credit as $concept) {
                        if (!array_key_exists($concept->tax, $taxs)) {
                            $taxs[$concept->tax] = [
                                'sum_tax'   => 0,
                                'sum_price' => 0
                            ];
                        }
                        $taxs[$concept->tax]['sum_tax']   += $concept->sum_tax;
                        $taxs[$concept->tax]['sum_price'] += $concept->sum_price;
                    }
                    break;

                case 'concepts_debit':
                    foreach ($comprobante->concepts_debit as $concept) {
                        if (!array_key_exists($concept->tax, $taxs)) {
                            $taxs[$concept->tax] = [
                                'sum_tax'   => 0,
                                'sum_price' => 0
                            ];
                        }
                        $taxs[$concept->tax]['sum_tax']   += $concept->sum_tax;
                        $taxs[$concept->tax]['sum_price'] += $concept->sum_price;
                    }
                    break;
            }
            
            // tipo comprobante
            $tipo_comprobante = str_pad($comprobante->tipo_comp, 3, "0", STR_PAD_LEFT);

            // PTO. VTA.
            $pto_vta = str_pad($comprobante->pto_vta, 5, "0", STR_PAD_LEFT);

            // NUMERO
            $num = str_pad($comprobante->num, 20, "0", STR_PAD_LEFT);

            foreach ($taxs as $key => $tax) {

                // IMPORTE NETO GRAVADO
                $importe_neto_gravado = number_format((float) $tax['sum_price'], 2, '.', '');
                $importe_neto_gravado = str_replace(".", "", $importe_neto_gravado);
                if ($business->responsible == 6) {
                    $importe_neto_gravado = 0;
                }
                $importe_neto_gravado = str_pad($importe_neto_gravado, 15, "0", STR_PAD_LEFT);

                // ALICUOTA DE IVA
                if ($business->responsible == 6) {
                    $key = 3;
                }
                $alicuota_iva = str_pad($key, 4, "0", STR_PAD_LEFT);

                // IMPUESTO LIQUIDADO
                $impuesto_liquidado = number_format((float) $tax['sum_tax'], 2, '.', '');
                $impuesto_liquidado = str_replace(".", "", $impuesto_liquidado);
                if ($business->responsible == 6) {
                    $impuesto_liquidado = 0;
                }
                $impuesto_liquidado = str_pad($impuesto_liquidado, 15, "0", STR_PAD_LEFT);

                $rows .= $tipo_comprobante . $pto_vta . $num . $importe_neto_gravado . $alicuota_iva . $impuesto_liquidado . "\r\n";
            }
        }

        $rows = trim($rows);

        $filename = 'siap/venta/';
        $created = Time::now()->format('Ymd');
        $filenamex = $created . '-' . 'venta-alicuota-siap.txt';
        $filename .= $filenamex;

        $file = new File(WWW_ROOT . $filename, false, 0644);

        if ($file->write($rows, 'w', true)) {

            $this->response->file(WWW_ROOT . $filename, array(
                'download' => true,
                'name'     => $filenamex,
            ));
            $file->close();
            return $this->response;

        } else {
            $this->Flash->error(__('Error al generar el archivo VTA ALICUOTA SIAP.'));
        }
        $file->close();
    }

    public function alicuotaVenta()
    {
        if ($this->request->is('post')) {

            $afip_codes =  $this->request->getSession()->read('afip_codes');

            $params = json_decode($this->request->getData()['data']);

            $comprobantes = [];

            // $invoices = $this->Invoices
            //     ->find()
            //     ->contain([
            //         'Concepts',
            //         'Customers.Provinces'
            //     ])
            //     ->where([
            //         'business_id'   => $params->business_billing,
            //         'tipo_comp !='  => 'XXX',
            //         'total >'       => 0,
            //         'date_start >=' => $params->date_start,
            //         'date_end <='   => $params->date_end
            //     ])
            //     ->map(function ($row) { 
            //         $row->type = 'invoice';
            //         return $row;
            //     })->toArray();

            $invoices = $this->Invoices
                ->find()
                ->contain([
                    'Concepts',
                    'Customers.Provinces'
                ])->where([
                    'business_id'  => $params->business_billing,
                    'tipo_comp !=' => 'XXX',
                    'total >'      => 0
                ])->andWhere(function (QueryExpression $exp, Query $q) use ($params) {
                    return $exp->between('date', $params->date_start, $params->date_end);
                })->map(function ($row) {
                    $row->type = 'invoice';
                    return $row;
                })->toArray();

            // $credit_notes = $this->CreditNotes
            //     ->find()
            //     ->contain([
            //         'ConceptsCredit',
            //         'Customers.Provinces'
            //     ])
            //     ->where([
            //         'business_id'   => $params->business_billing,
            //         'tipo_comp !='  => 'NCX',
            //         'total >'       => 0,
            //         'date_start >=' => $params->date_start,
            //         'date_end <='   => $params->date_end
            //     ])->map(function ($row) { 
            //         $row->type = 'concepts_credit';
            //         return $row;
            //     })->toArray();

            $credit_notes = $this->CreditNotes
                ->find()
                ->contain([
                    'ConceptsCredit',
                    'Customers.Provinces'
                ])->where([
                    'business_id'  => $params->business_billing,
                    'tipo_comp !=' => 'NCX',
                    'total >'      => 0
                ])->andWhere(function (QueryExpression $exp, Query $q) use ($params) {
                    return $exp->between('date', $params->date_start, $params->date_end);
                })->map(function ($row) { 
                    $row->type = 'concepts_credit';
                    return $row;
                })->toArray();

            // $debit_notes = $this->DebitNotes
            //     ->find()
            //     ->contain([
            //         'ConceptsDebit',
            //         'Customers.Provinces'
            //     ])
            //     ->where([
            //         'business_id'   => $params->business_billing,
            //         'tipo_comp !='  => 'NDX',
            //         'total >'       => 0,
            //         'date_start >=' => $params->date_start,
            //         'date_end <='   => $params->date_end
            //     ])->map(function ($row) { 
            //         $row->type = 'concepts_debit';
            //         return $row;
            //     })->toArray();

            $debit_notes = $this->DebitNotes
                ->find()
                ->contain([
                    'ConceptsDebit',
                    'Customers.Provinces'
                ])->where([
                    'business_id'  => $params->business_billing,
                    'tipo_comp !=' => 'NDX',
                    'total >'      => 0
                ])->andWhere(function (QueryExpression $exp, Query $q) use ($params) {
                    return $exp->between('date', $params->date_start, $params->date_end);
                })->map(function ($row) { 
                    $row->type = 'concepts_debit';
                    return $row;
                })->toArray();

            $comprobantes_aux = array_merge($invoices, $credit_notes, $debit_notes);
            $comprobantes_lala[] = [
            	'Fecha',        //0
                'Tipos',        //1
                'Pto Vta',      //2
                'Nro',          //3
                'Cód.',         //4
                'Nombre',       //5
                'SubTotal',     //6
                'IVA 0',        //7
                'IVA 2',        //8
                'IVA 5',        //9
                'IVA 10',       //10
                'IVA 21',       //11
                'IVA 27',       //12
                'Total',        //13
                'Cond. Fiscal', //14
                'Ciudad',       //15
                'Provincia'     //16
            ];

            $rows = "";

            foreach ($comprobantes_aux as $key => $comprobante_aux) {

                $taxs = [
                    3 => 0, // 0%,
                    9 => 0, // 2.5%
                    8 => 0, // 5%
                    4 => 0, // 10.5%
                    5 => 0, // 21%
                    6 => 0  // 27%
                ];

                switch ($comprobante_aux['type']) {

                    case 'invoice':
                        foreach ($comprobante_aux->concepts as $concept) {
                            if (!array_key_exists($concept->tax, $taxs)) {
                                $taxs[$concept->tax] = 0;
                            }
                            $taxs[$concept->tax] += $concept->sum_tax;
                        }
                        break;

                    case 'concepts_credit':
                        foreach ($comprobante_aux->concepts_credit as $concept) {
                            if (!array_key_exists($concept->tax, $taxs)) {
                                $taxs[$concept->tax] = 0;
                            }
                            $taxs[$concept->tax] += $concept->sum_tax;
                        }
                        break;

                    case 'concepts_debit':
                        foreach ($comprobante_aux->concepts_debit as $concept) {
                            if (!array_key_exists($concept->tax, $taxs)) {
                                $taxs[$concept->tax] = 0;
                            }
                            $taxs[$concept->tax] += $concept->sum_tax;
                        }
                        break;
                }

                $iva_0  = $taxs[3] == 0 ? '0' : $taxs[3];
                $iva_2  = $taxs[9] == 0 ? '0' : $taxs[9];
                $iva_5  = $taxs[8] == 0 ? '0' : $taxs[8];
                $iva_10 = $taxs[4] == 0 ? '0' : $taxs[4];
                $iva_21 = $taxs[5] == 0 ? '0' : $taxs[5];
                $iva_27 = $taxs[6] == 0 ? '0' : $taxs[6];
                $customer_responsible = $comprobante_aux->customer_responsible;
                $customer_responsible = $afip_codes['responsibles'][$customer_responsible];

                $province = trim($comprobante_aux->customer->province->name);

                $index = $comprobante_aux->tipo_comp . '-' . $comprobante_aux->id;

                $date = $comprobante_aux->date->day . '/' . $comprobante_aux->date->month . '/' . $comprobante_aux->date->year;

                $city = trim($comprobante_aux->customer_city);

                $comprobantes_lala[$index] = [
                    $date,                            //0
                    $comprobante_aux->tipo_comp,      //1
                    $comprobante_aux->pto_vta,        //2
                    $comprobante_aux->num,            //3
                    $comprobante_aux->customer_code,  //4
                    $comprobante_aux->customer_name,  //5
                    $comprobante_aux->subtotal,       //6
                    $iva_0,                           //7
                    $iva_2,                           //8
                    $iva_5,                           //9
                    $iva_10,                          //10
                    $iva_21,                          //11
                    $iva_27,                          //12
                    $comprobante_aux->total,          //13
                    $customer_responsible,            //14
                    $city,                            //15
                    $province                         //16
                ];
            }

            $this->set(compact('comprobantes_lala'));
            $this->set('_serialize', ['comprobantes_lala']);
        }
    }
}