<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Discounts Controller
 *
 * @property \App\Model\Table\DiscountsTable $Discounts
 *
 * @method \App\Model\Entity\Discount[] paginate($object = null, array $settings = [])
 */
class DiscountsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }

    public function isAuthorized($user = null) 
    {
        if ($this->request->getParam('action') == 'getGenericAllAjax') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $discounts = $this->Discounts->find()->contain(['Users'])->where(['Discounts.deleted' => false]);

        $this->set(compact('discounts'));
        $this->set('_serialize', ['discounts']);
    }

    public function getGenericAllAjax()
    {
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $discounts = $this->Discounts->find()->where(['enabled' => true, 'deleted' => false, 'type' => 'generic']); 

            if (count($discounts) > 0) {
                $this->set('discounts',$discounts );
            } else {
                $this->set('discounts', false);
            }
        }
    } 

    /**
     * View method
     *
     * @param string|null $id Discount id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $discount = $this->Discounts->get($id, [
            'contain' => []
        ]);

        $this->set('discount', $discount);
        $this->set('_serialize', ['discount']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $discount = $this->Discounts->newEntity();
        if ($this->request->is('post')) {

            $discount = $this->Discounts->find()->where(['code' => $this->request->getData('code') , 'deleted' => false])->first();

            if (!$discount) {

                $discount = $this->Discounts->newEntity();
                $discount = $this->Discounts->patchEntity($discount, $this->request->getData());

                $discount->code = strtoupper($discount->code);
                $discount->concept = strtoupper($discount->concept);
                $discount->user_id =  $this->Auth->user()['id'];

                if ($this->Discounts->save($discount)) {

                    $this->Flash->success(__('Descuento agregado correctamente.'));

                    return $this->redirect(['action' => 'index']);
                }

                $this->Flash->error(__('Error al intentar agergar el descuento.'));
            } else {

                $this->Flash->warning(__('Existe otro descuento con el código {0} . ', $discount->code ));
            }
        }
        $this->set(compact('discount'));
        $this->set('_serialize', ['discount']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Discount id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {

            $discount = $this->Discounts->find()->where(['code' => $this->request->getData('code') , 'deleted' => false, 'id !=' => $this->request->getData('id')])->first();

            if (!$discount) {

                $discount = $this->Discounts->get($this->request->getData('id'), [
                    'contain' => []
                ]);

                $discount = $this->Discounts->patchEntity($discount, $this->request->getData());

                $discount->code = strtoupper($discount->code);
                $discount->concept = strtoupper($discount->concept);

                if ($this->Discounts->save($discount)) {
                    $this->Flash->success(__('El descuento se edito correctamente.'));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('Error al intentar editar el descuento'));

            } else {

                $this->Flash->warning(__('Existe otro descuento con el código {0}.', $discount->code ));
            }
        }
        $this->set(compact('discount'));
        $this->set('_serialize', ['discount']);
    }

    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);
        $discount = $this->Discounts->get($_POST['id']);

        $discount->deleted = true;

        if ($this->Discounts->save($discount)) {
            $this->Flash->success(__('Descuento eliminado.'));
        } else {
            $this->Flash->error(__('Error al eliminar el Descuento.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

