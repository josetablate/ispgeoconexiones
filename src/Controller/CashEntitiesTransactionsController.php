<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CashEntitiesTransactions Controller
 *
 * @property \App\Model\Table\CashEntitiesTransactionsTable $CashEntitiesTransactions
 *
 * @method \App\Model\Entity\CashEntitiesTransaction[] paginate($object = null, array $settings = [])
 */
class CashEntitiesTransactionsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }

    public function isAuthorized($user = null) 
    {
        if ($this->request->getParam('action') == 'verify') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $cashEntitiesTransactions = $this->paginate($this->CashEntitiesTransactions);

        $this->set(compact('cashEntitiesTransactions'));
        $this->set('_serialize', ['cashEntitiesTransactions']);
    }

    public function verify()
    {
        //Ajax Detection
        if ($this->request->is('Ajax')) {

            $this->loadModel('CashEntitiesTransactions');
            $cashEntitiesTransactios = $this->CashEntitiesTransactions->find()->contain(['Users'])
                ->where([
                    'CashEntitiesTransactions.acepted IS' => NULL,
                    'user_destination_id' => $this->Auth->user()['id'],
                    ]);

            $this->set('transactions', $cashEntitiesTransactios);  
        }
    }

    /**
     * View method
     *
     * @param string|null $id Cash Entities Transaction id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $cashEntitiesTransaction = $this->CashEntitiesTransactions->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('cashEntitiesTransaction', $cashEntitiesTransaction);
        $this->set('_serialize', ['cashEntitiesTransaction']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $cashEntitiesTransaction = $this->CashEntitiesTransactions->newEntity();
        if ($this->request->is('post')) {
            $cashEntitiesTransaction = $this->CashEntitiesTransactions->patchEntity($cashEntitiesTransaction, $this->request->getData());
            if ($this->CashEntitiesTransactions->save($cashEntitiesTransaction)) {
                $this->Flash->success(__('The cash entities transaction has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cash entities transaction could not be saved. Please, try again.'));
        }
        $users = $this->CashEntitiesTransactions->Users->find('list', ['limit' => 200]);
        $this->set(compact('cashEntitiesTransaction', 'users'));
        $this->set('_serialize', ['cashEntitiesTransaction']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cash Entities Transaction id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $cashEntitiesTransaction = $this->CashEntitiesTransactions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cashEntitiesTransaction = $this->CashEntitiesTransactions->patchEntity($cashEntitiesTransaction, $this->request->getData());
            if ($this->CashEntitiesTransactions->save($cashEntitiesTransaction)) {
                $this->Flash->success(__('The cash entities transaction has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cash entities transaction could not be saved. Please, try again.'));
        }
        $users = $this->CashEntitiesTransactions->Users->find('list', ['limit' => 200]);
        $this->set(compact('cashEntitiesTransaction', 'users'));
        $this->set('_serialize', ['cashEntitiesTransaction']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cash Entities Transaction id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $cashEntitiesTransaction = $this->CashEntitiesTransactions->get($id);
        if ($this->CashEntitiesTransactions->delete($cashEntitiesTransaction)) {
            $this->Flash->success(__('The cash entities transaction has been deleted.'));
        } else {
            $this->Flash->error(__('The cash entities transaction could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
