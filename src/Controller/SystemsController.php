<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\I18n\Time;

/**
 * SystemsEntities Controller
 *
 */
class SystemsController extends AppController
{
    public function initialize()
    {
        parent::initialize();  
  
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow('check');
    }

    public function isAuthorized($user = null) 
    {
        return true;
    }

    public function check()
    {
        $this->set([
            'data'       => ['status' => true],
            '_serialize' => ['data']
        ]);
    }
}