<?php
namespace App\Controller;

use App\Controller\IspControllerMikrotik;


abstract class IspControllerMikrotikIpfija extends IspControllerMikrotik
{
    public function initialize()
    {
        parent::initialize();        
    
    }   
          
     
    //CONFIGUARATIONS                                                   

    //pools
    
    abstract protected function getPoolsByController();    
    abstract protected function addPool();    
    abstract protected function deletePool();
    abstract protected function editPool();    
    abstract protected function getRangeNetwork();    
    abstract protected function movePool();      
    
    //gateways
    
    abstract protected function getGatewaysByController();    
    abstract protected function addGateway();   
    abstract protected function deleteGateway();     
    abstract protected function editGateway();
    
    //profiles
    
    abstract protected function getProfilesByController();
    abstract protected function addProfile();
    abstract protected function deleteProfile();
    abstract protected function editProfile();  
     
   
     
    //SYNCRONIZATIONS    
    
    //sync address list client

    abstract protected function syncAddressListsClient();
    abstract protected function syncAddressListClientIndi();
    abstract protected function refreshAddressListsClient();  
    abstract protected function deleteAddressListClientInController();
    
    abstract protected function diffAddressListsClient($controller, $file);    
    abstract protected function clearAddressListsClient();   
    
    
     //sync queues

    abstract protected function syncQueues();    
    abstract protected function syncQueueIndi();  
    abstract protected function refreshQueues();  
    abstract protected function deleteQueueInController();
    
    abstract protected function diffQueues($controller, $file);    
    abstract protected function clearQueues(); 
    
    
     // sync address list

    abstract protected function syncAddressLists();    
    abstract protected function syncAddressListIndi();  
    abstract protected function refreshAddressLists();
    abstract protected function deleteAddressListInController();    
                                                        
    abstract protected function diffAddressLists($controller, $file);    
    abstract protected function clearAddressLists();
    
    
    //sync arp
    
    abstract protected function syncArps();
    abstract protected function syncArpIndi();
    abstract protected function refreshArp();
    abstract protected function deleteArpInController();
    
    abstract protected function diffArp($controller, $file);
    abstract protected function clearArp();
    
    
    //gateways
    
    abstract protected function syncGateways();  
    abstract protected function syncGatewayIndi(); 
    abstract protected function refreshGateways(); 
    abstract protected function deleteGatewayInController();   
    
    abstract protected function diffGateways($controller);     
    abstract protected function clearGateways();
    
  
    
  
}

















