<?php
namespace App\Controller\Api\V1;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Core\Configure;
use Cake\Filesystem\File;
use Cake\Log\Log;

/**
 * Systems Controller
 *
 *
 * @method \App\Model\Entity\System[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SystemsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['token', 'login']);
    }

    public function isAuthorized($user = null)
    {
        return true;
    }

    public function token()
    {
        $this->set([
            'token'      => $this->request->getParam('_csrfToken'),
            '_serialize' => ['token']
        ]);
    }

    public function login()
    {
        if ($this->request->is(['post'])) {

            $data = $this->request->input('json_decode', TRUE);

            if (array_key_exists("token", $data) && ($data['token'] == Configure::read('project.token'))) {

                $this->loadModel('Customers');

                $customer = $this->Customers
                    ->find()
                    ->contain([
                        'Connections.Services',
                        'Invoices',
                        'Receipts'
                    ])
                    ->where([
                        'ident'         => $data['ident'],
                        'clave_portal'  => $data['clave_portal'],
                    ])->first();

                if ($customer) {

                    $data['customer'] = $customer;
                    $data['message'] = "Ingreso correcto.";
                    $data['status'] = 200;
                } else {
                    $data['message'] = "Nro Documento o Clave incorrectos.";
                    $data['status'] = 200;
                }

            } else {
                $data['message'] = "Token inválido";
                $data['status'] = 401;
            }

            $this->set([
                'data'       => $data,
                '_serialize' => ['data']
            ]);
        }
    }
}
