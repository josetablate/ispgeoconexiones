<?php
namespace App\Controller\Api\V1;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;

/**
 * Customers Controller
 *
 * @property \App\Model\Table\CustomersTable $Customers
 */
class CustomersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }

    public function isAuthorized($user = null) 
    {
        return parent::allowRol($user['id']);
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['changeClave', 'forgotClave', 'cdRequestCard']);
    }

    private function randomPassword($len = 6)
    {
        $alphabet = "abcdefghijklmnopqrstuwxyz0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $len; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function changeClave($code)
    {
        if ($this->request->is(['put'])) {
            $data = $this->request->input('json_decode', true);

            if (array_key_exists("token", $data) && ($data['token'] == Configure::read('project.token'))) {

                if (array_key_exists("code", $data)) {
                    $customer = $this->Customers->get($data['code']);

                    if (array_key_exists("password", $data)) {

                        $customer->clave_portal = $data['password'];

                        if ($this->Customers->save($customer)) {
                            $data['message'] = "Cambio con éxito";
                            $data['status'] = 200;
                        } else {
                            $data['message'] = "No se ha podido realizar el cambio. Por favor intente nuevamente.";
                            $data['status'] = 400;
                        }

                    } else {
                        $data['message'] = "Falta el campo a cambiar";
                        $data['status'] = 400;
                    }
                } else {
                    $data['message'] = "Falta el campo code para realizar la busqueda";
                    $data['status'] = 400;
                }

            } else {
                $data['message'] = "Token inválido";
                $data['status'] = 401;
            }

            $this->set([
                'data'       => $data,
                '_serialize' => ['data']
            ]);
        }
    }

    public function forgotClave()
    {
        if ($this->request->is(['put'])) {
            $data = $this->request->input('json_decode', true);

            if (array_key_exists("token", $data) && ($data['token'] == Configure::read('project.token'))) {

                if (array_key_exists("email", $data)) {
                    $customer = $this->Customers->find()->where(['email' => $data['email']])->first();

                    if ($customer) {
                        $customer->clave_portal = $this->randomPassword();

                        if ($this->Customers->save($customer)) {
                            $data['password'] = $customer->clave_portal;
                            $data['message'] = "Clave restablecida con éxito";
                            $data['status'] = 200;
                        } else {
                            $data['message'] = "No se ha podido restablecer la Clave. Por favor intente nuevamente.";
                            $data['status'] = 400;
                        }
                    } else {
                        $data['message'] = "El Correo enviado no se encuentra registrado";
                        $data['status'] = 400;
                    }
                } else {
                    $data['message'] = "Falta el campo Correo para realizar la busqueda";
                    $data['status'] = 400;
                }

            } else {
                $data['message'] = "Token inválido";
                $data['status'] = 401;
            }

            $this->set([
                'data'       => $data,
                '_serialize' => ['data']
            ]);
        }
    }

    public function cdRequestCard()
    {
        if ($this->request->is(['post'])) {
            $data = $this->request->input('json_decode', true);
            if (array_key_exists("token", $data) && ($data['token'] == Configure::read('project.token'))) {
                if (array_key_exists("code", $data)) {
                    $customer = $this->Customers->get($data['code']);
                    $this->loadModel('CobrodigitalCards');
                    $card = $this->CobrodigitalCards->find()->where(['used' => false, 'deleted' => false])->limit(1)->first();
                    if ($card) {
                        $customer->cobrodigital = true;
                        $customer->cd_barcode = $card->barcode;
                        $customer->cd_electronic_code = $card->electronic_code;
                        $this->Customers->save($customer);
                        $card->used = true;
                        $this->CobrodigitalCards->save($card);

                        $data['message'] = "Asignación de Tarjeta de Pago exitosa.";
                        $data['cobrodigital'] = true;
                        $data['barcode'] = $card->barcode;
                        $data['electronic_code'] = $card->electronic_code;
                        $data['status'] = 200;
                    } else {
                        $data['message'] = "No hay Tarjetas de Pagos disponibles.";
                        $data['cobrodigital'] = false;
                        $data['status'] = 200;
                    }
                } else {
                    $data['message'] = "Falta el campo code para realizar la busqueda";
                    $data['status'] = 400;
                }
            } else {
                $data['message'] = "Token inválido";
                $data['status'] = 401;
            }

            $this->set([
                'data'       => $data,
                '_serialize' => ['data']
            ]);
        }
    }
}
