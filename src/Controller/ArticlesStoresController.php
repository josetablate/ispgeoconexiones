<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * ArticlesStores Controller
 *
 * @property \App\Model\Table\ArticlesStoresTable $ArticlesStores
 */
class ArticlesStoresController extends AppController
{
    public function isAuthorized($user = null) 
    {
        if ($this->request->getParam('action') == 'getStockByStoreAjax') {
            return true;
        }

        if ($this->request->getParam('action') == 'getArticlesByStoreByArticleAjax') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function index()
    {
        $articlesStores = $this->ArticlesStores->find()->contain(['Stores', 'Articles'])->where(['Stores.deleted' => false, 'current_amount >' => 0]);

        $this->set(compact('articlesStores'));
        $this->set('_serialize', ['articlesStores']);
    }

    public function add()
    {
        $articleStore = $this->ArticlesStores->newEntity();

        if ($this->request->is('post')) {

            $action = 'Actualización de Stock de Artículos en Depósito';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $this->loadModel('Articles');

            $article = null;

            if ($this->request->getData('article_id') != '') {
                $article = $this->Articles->find()->where(['id' => $this->request->getData('article_id')])->first();
            }

            if (!$article) {

                $this->Flash->warning(__('El artículo no existe. Se creará uno nuevo.'));

                $article = $this->Articles->newEntity();

                $article->code = strtoupper($this->request->getData('code'));
                $article->name = $this->request->getData('name');
                $article->total_amount = 0;

                if (!$this->Articles->save($article)) {
                    $this->Flash->error(__('Error al atualizar el contador total del artículo.'));
                    return $this->redirect(['action' => 'add']);
                }
            }

            $articleStore = $this->ArticlesStores->find()->where([
                 'article_id' => $article->id,
                 'store_id' => $this->request->getData('store_id')
            ])->first();

            if (!$articleStore) {
                $articleStore = $this->ArticlesStores->newEntity();
                $articleStore->article_id = $article->id;
                $articleStore->store_id = $this->request->getData('store_id');
            }

            $used = false;
     
            $articleStore->current_amount += $this->request->getData('amount');
            $article->total_amount += $this->request->getData('amount');

            if (!$this->Articles->save($article)) {
                $this->Flash->error(__('Error al atualizar el contador total del artículo.'));
                return $this->redirect(['action' => 'add']);
            }

            if ($this->ArticlesStores->save($articleStore)) {
                $this->loadModel('Stores');
                $store = $this->Stores->get($articleStore->store_id);
                $action = 'Stock de Artículos en Depósito actualizados';
                $detail = '';
                $detail .= 'Artículo: ' . $article->name . PHP_EOL;
                $detail .= 'Depósito: '  . $store->name . PHP_EOL;
                $detail .= 'Cantidad: ' . $articleStore->current_amount . PHP_EOL;
                $this->registerActivity($action, $detail, NULL);
                $this->Flash->success(__('Carga completa.'));
                return $this->redirect(['action' => 'add']);
            } else {
                $this->Flash->error(__('Error al actualizar el stock.'));
            }
        }

        $stores = $this->ArticlesStores->Stores->find('list')->where(['enabled' => true, 'deleted' => false]);

        $this->set(compact('articleStore', 'stores'));
        $this->set('_serialize', ['articleStore']);
    }

    public function edit()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Corrección de Stock de Artículos en Depósito';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $articleStore = $this->ArticlesStores->get($this->request->getData('article-store-id'), [
                'contain' => [
                    'Articles'
                ]
            ]);

            $articleStore->current_amount = $this->request->getData('current_amount');

            if ($this->ArticlesStores->save($articleStore)) {
                $this->loadModel('Stores');
                $store = $this->Stores->get($articleStore->store_id);
                $action = 'Stock de Artículos en Depósito corregidos';
                $detail = '';
                $detail .= 'Artículo: ' . $articleStore->article->name . PHP_EOL;
                $detail .= 'Depósito: '  . $store->name . PHP_EOL;
                $detail .= 'Cantidad: ' . $articleStore->current_amount . PHP_EOL;
                $this->registerActivity($action, $detail, NULL);
            } else {
                $this->Flash->error(__('Error al actualizar la cantidad del artículo.'));
                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->success(__('Cantidad Editada correctamente.'));
            return $this->redirect(['action' => 'index']);
        }
    }

    public function getStockByStoreAjax()
    {
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $store_id_from= $this->request->input('json_decode')->store_id_from;

            $articles = $this->ArticlesStores->find()
            ->contain(['Articles'])
            ->where(['store_id' => $store_id_from,  'current_amount !=' => 0, 'Articles.enabled' => true, 'Articles.deleted' => false]);

            $this->set('articles', $articles);
        }
    } 

    public function getArticlesByStoreByArticleAjax()
    {
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $store_id = $this->request->input('json_decode')->store_id;
            $article_id = $this->request->input('json_decode')->article_id;

            $snids = $this->ArticleStores->find()
            ->select(['ArticleStores.id','Articles.code','Articles.name', 'ArticleStores.snid', 'ArticleStores.current_amount'])
            ->contain('Articles')
            ->where(['store_id' => $store_id, 'current_amount >' => 0, 'article_id' => $article_id, 'Articles.enabled' => true, 'Articles.deleted' => false])
            ->toArray(); 

            $this->set('snids', $snids);
        }
    } 

    public function move()
    {
        $articleStore = $this->ArticlesStores->newEntity();

        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Movimiento de Stock de un Depósito a otro';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            if ($this->request->getData('store_id_from') == $this->request->getData('store_id_to') ) {
                $this->Flash->error(__('El Depósito de origen y destino no pueden ser el mismo.'));
                return $this->redirect(['action' => 'move']);
            }

            if ($this->request->getData('article_store_id') == '') {
                $this->Flash->error(__('El Código de artículo no existe en el stock'));
                return $this->redirect(['action' => 'move']);
            }

            $articleStoreFrom = $this->ArticlesStores->get($this->request->getData('article_store_id'), ['contain' => ['Articles']]);

            $articleStoreTo = $this->ArticlesStores->find()->where([
                'store_id' => $this->request->getData('store_id_to'),
                'article_id' => $articleStoreFrom->article_id, 
            ])->first();

            //el deposito destino nunca tuvo este stock entonces hay que crear el registro
            if ($articleStoreTo == null) {

                $articleStoreTo =  $this->ArticlesStores->newEntity();
                $articleStoreTo->article_id = $articleStoreFrom->article_id;
                $articleStoreTo->store_id = $this->request->getData()['store_id_to'];
                $articleStoreTo->current_amount = 0;

                if (!$this->ArticlesStores->save($articleStoreTo)) {
                    $this->Flash->error(__('Error al crear el registro en el depósito de destino.'));
                    return $this->redirect(['action' => 'move']);
                }
            }

            if ($articleStoreFrom->article->exist_snid) {

                $this->loadModel('SnidStores');

                $snidStores = $this->SnidStores->find()->where(['snid' => $this->request->getData('code'), 'deleted' => false])->first();

                $snidStores->store_id = $articleStoreTo->store_id;

                if (!$this->SnidStores->save($snidStores)) {
                    $this->Flash->error(__('Error al actualizar la lista de SNID.'));
                    return $this->redirect(['action' => 'move']);
                }

                $amount = 1;
            } else {
                $amount = $this->request->getData('amount');
            }

            if ($articleStoreFrom->current_amount >= intval($amount)) {

                $articleStoreFrom->current_amount -= $amount;

                if ($articleStoreFrom->current_amount == 0) {
                    $this->ArticlesStores->delete($articleStoreFrom);
                }

                $articleStoreTo->current_amount += $amount;

                if ($articleStoreFrom->current_amount > 0) {

                    if (!$this->ArticlesStores->save($articleStoreFrom)) {
                         $this->Flash->error(__('Error al guardar el artículo en el depósito de origen.'));
                         return $this->redirect(['action' => 'move']);
                    }
                }

                if ($this->ArticlesStores->save($articleStoreTo)) {
                    $this->loadModel('Stores');
                    $store_to = $this->Stores->get($articleStoreTo->store_id);
                    $store_from = $this->Stores->get($articleStoreFrom->store_id);
                    $this->loadModel('Articles');
                    $store_to = $this->Articles->get($articleStoreTo->article_id);
                    $action = 'Movimiento de Stock de un Depósito a otro concretado';
                    $detail = '';
                    $detail .= 'Artículo: ' . $articleStoreFrom->article->name . PHP_EOL;
                    $detail .= 'Depósito origen: '  . $store_from->name . PHP_EOL;
                    $detail .= 'Depósito destino: '  . $store_to->name . PHP_EOL;
                    $detail .= 'Cantidad: ' . $articleStoreTo->current_amount . PHP_EOL;
                    $this->registerActivity($action, $detail, NULL);
                    $this->Flash->success(__('El artículo se movió correntamente.'));
                    return $this->redirect(['action' => 'move']);
                } else {
                    $this->Flash->error(__('Error al guardar el artículo en el depósito de destino.'));
                    return $this->redirect(['action' => 'move']);
                }

            } else {
                $this->Flash->error(__('No existe la cantidad suficiente'));
                return $this->redirect(['action' => 'move']);
            }
        }

        $stores = $this->ArticlesStores->Stores->find('list')->where(['enabled' => true, 'deleted' => false]);
        $this->set(compact('articleStore', 'stores'));
        $this->set('_serialize', ['articleStore']);
    }
}
