<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Filesystem\File;
use Cake\I18n\Time;

/**
 * ActionLog Controller
 *
 * @property \App\Model\Table\ActionLogTable $ActionLog
 */
class ActionLogController extends AppController
{
    public function isAuthorized($user = null) 
    {
        if ($this->request->getParam('action') == 'getActionsLogs') {
            return true;
        }

        if ($this->request->getParam('action') == 'getActionsLogsFromCustomer') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function index()
    {
        $this->loadModel('Users');
        $paraments = $this->request->getSession()->read('paraments');
        $users_list = $this->Users->find('list')->where(['deleted' => 0, 'id NOT IN' => $paraments->system->users]);
        $users = [];
        $users[0] = "Sin asignar";
        foreach ($users_list as $key => $user) {
            $users[$key] = $user;
        }

        $this->set(compact('users'));
    }

    public function getActionsLogs()
    {
        if ($this->request->is('ajax')) { //Ajax Detection

            //$time = Time::now()->modify('-1 month');

            $response = new \stdClass();

            $response->draw = intval($this->request->getQuery('draw'));

            $where = [
                //'ActionLog.created >' => $time
            ];

            $response->recordsTotal = $this->ActionLog->find()->where($where)->count();

            $response->data = $this->ActionLog->find('ServerSideData', [
                'params' => $this->request->getQuery(),
                'where'  => $where
            ]);

            $response->recordsFiltered = $this->ActionLog->find('RecordsFiltered', [
                'params' => $this->request->getQuery(),
                'where'  => $where
            ]);

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function getActionsLogsFromCustomer()
    {
        if ($this->request->is('ajax')) { //Ajax Detection

            //$time = Time::now()->modify('-1 month');

            $response = new \stdClass();

            $response->draw = intval($this->request->getQuery('draw'));

            $where = [
                //'ActionLog.created >' => $time
                'ActionLog.customer_code' => $this->request->getQuery()['customer_code'],
            ];

            $response->recordsTotal = $this->ActionLog->find()->where($where)->count();

            $response->data = $this->ActionLog->find('ServerSideData', [
                'params' => $this->request->getQuery(),
                'where'  => $where
            ]);

            $response->recordsFiltered = $this->ActionLog->find('RecordsFiltered', [
                'params' => $this->request->getQuery(),
                'where'  => $where
            ]);

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }
}
