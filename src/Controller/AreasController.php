<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Areas Controller
 *
 * @property \App\Model\Table\CitiesTable $Areas
 */
class AreasController extends AppController
{
    public function isAuthorized($user = null) 
    {
        return parent::allowRol($user['id']);
    }

    public function index()
    {
        $areas = $this->Areas->find()->contain(['Cities'])->where(['deleted' => false]);
        $paraments = $this->request->getSession()->read('paraments');

        $cities = $this->Areas->Cities->find('list');
        $business = ["" => "Seleccionar Empresa"];
        foreach ($paraments->invoicing->business as $b) {
            $business[$b->id] = $b->name;
        }

        $this->set(compact('areas', 'cities', 'business'));
        $this->set('_serialize', ['areas']);
    }

    public function add()
    {
        $city = $this->Areas->newEntity();
        $paraments = $this->request->getSession()->read('paraments');

        if ($this->request->is('post')) {

            $action = 'Agregado de Área';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $area = $this->Areas->find()->where(['name' => $this->request->getData('name'), 'deleted' => false])->first();

            if ($area) {

                $this->Flash->warning(__('Existe otra área con el mismo nombre.'));
                return $this->redirect(['action' => 'add']);
            }

            $city = $this->Areas->patchEntity($city, $this->request->getData());

            $city->deleted = false;

            if ($this->Areas->save($city)) {

                $empresa_defecto = "";

                foreach ($paraments->invoicing->business as $b) {
                    if ($b->id == $city->business_billing_default) {
                        $empresa_defecto = $b->name . ' (' . $b->address . ')';
                    }
                }

                $this->loadModel('Cities');
                $city_posta = $this->Cities->get($city->city_id);

                $action = 'Área Agregada';
                $detail = '';
                $detail .= 'Habilitada: ' . ($city->enabled ? 'Si' : 'No') . PHP_EOL;
                $detail .= 'Nombre: ' . $city->name;
                $detail .= 'Ciudad: ' . $city_posta->name;
                $detail .= 'Empresa: ' . $empresa_defecto;
                $detail .= 'Descripción: ' . $city->description;
                $this->registerActivity($action, $detail, NULL);

                $this->Flash->success(__('Área agregada.'));
                return $this->redirect(['action' => 'add']);

            } else {

                $this->Flash->error(__('Error al agregar la área.'));
            }
        }

        $cities = $this->Areas->Cities->find('list')->where(['enabled' => TRUE]);

        $business = ["" => "Seleccionar Empresa"];
        foreach ($paraments->invoicing->business as $b) {
            $business[$b->id] = $b->name;
        }

        $this->set(compact('city', 'cities', 'business'));
        $this->set('_serialize', ['city']);
    }

    public function edit($id = null)
    {
        $paraments = $this->request->getSession()->read('paraments');

        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Edición de Área';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $city = $this->Areas->get($_POST['id'], [
                'contain' => []
            ]);

            $area = $this->Areas->find()->where([
                'name' => $this->request->getData('name'),
                'deleted' => false,
                'id !=' => $city->id ])
                ->first();

            if ($area) {

                $this->Flash->warning(__('Existe otra área con el mismo nombre.'));
                return $this->redirect(['action' => 'index']);
            }

            $city = $this->Areas->patchEntity($city, $this->request->getData());

            if ($this->Areas->save($city)) {

                $empresa_defecto = "";

                foreach ($paraments->invoicing->business as $b) {
                    if ($b->id == $city->business_billing_default) {
                        $empresa_defecto = $b->name . ' (' . $b->address . ')';
                    }
                }

                $this->loadModel('Cities');
                $city_posta = $this->Cities->get($city->city_id);

                $action = 'Área editada';
                $detail = '';
                $detail .= 'Habilitada: ' . ($city->enabled ? 'Si' : 'No') . PHP_EOL;
                $detail .= 'Nombre: ' . $city->name;
                $detail .= 'Ciudad: ' . $city_posta->name;
                $detail .= 'Empresa: ' . $empresa_defecto;
                $detail .= 'Descripción: ' . $city->description;
                $this->registerActivity($action, $detail, NULL);

                $this->Flash->success(__('Datos actualizados.'));

            } else {

                $this->Flash->error(__('Error al actualizar los datos.'));
            }

            return $this->redirect(['action' => 'index']);
        }
    }

    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);
        $paraments = $this->request->getSession()->read('paraments');
        $action = 'Eliminación de Área';
        $detail = '';
        $this->registerActivity($action, $detail, NULL, TRUE);

        $city = $this->Areas->get($_POST['id']);

        $city->name .= ' (Eliminado)';

        $city->deleted = true;

        $empresa_defecto = "";

        foreach ($paraments->invoicing->business as $b) {
            if ($b->id == $city->business_billing_default) {
                $empresa_defecto = $b->name . ' (' . $b->address . ')';
            }
        }

        $this->loadModel('Cities');
        $city_posta = $this->Cities->get($city->city_id);

        if ($this->Areas->save($city)) {

            $action = 'Área eliminada';
            $detail = '';
            $detail .= 'Habilitada: ' . ($city->enabled ? 'Si' : 'No') . PHP_EOL;
            $detail .= 'Nombre: ' . $city->name;
            $detail .= 'Ciudad: ' . $city_posta->name;
            $detail .= 'Empresa: ' . $empresa_defecto;
            $detail .= 'Descripción: ' . $city->description;
            $this->registerActivity($action, $detail, NULL);

            $this->Flash->success(__('Área eliminada.'));
        } else {

            $this->Flash->error(__('Error al eliminar la área.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
