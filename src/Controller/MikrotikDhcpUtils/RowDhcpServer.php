<?php
namespace App\Controller\MikrotikDhcpUtils;

use App\Controller\Utils\Attr;

class RowDhcpServer {
 
    public $id;
    public $controller_id;
    
    public $marked_to_delete;
    
    public $api_id;
    public $name;
    public $interface;
    
    public $address_pool;
    public $leases_time;
    
    public $disabled;
    
    public $other;
    
    
    public function __construct($dhcpserve, $side) {
        
        $this->other = $dhcpserve['other'];
        
        $this->marked_to_delete = false;
        
        if ($side == 'A') {
            $this->id = $dhcpserve['id'];
            $this->controller_id = $dhcpserve['controller_id'];
        }
        
        $this->api_id = new Attr($dhcpserve['api_id'], true);
        
        if ($side == 'A') {
            $this->name = new Attr($dhcpserve['name'], true);
        }else{
            $this->name = new Attr($this->convert_to($dhcpserve['name'], "UTF-8"), true);
        }
        
        
        $this->interface = new Attr($dhcpserve['interface'], true);
        $this->address_pool = new Attr($dhcpserve['address_pool'], true);
        $this->leases_time = new Attr($dhcpserve['leases_time'], true);
        $this->disabled = new Attr($dhcpserve['disabled'], true);
    
    }
    
    
    public function setNew(){
        
        $this->marked_to_delete = true;
        
        $this->api_id->state = false;
        $this->name->state = false;
        $this->interface->state = false;
        $this->address_pool->state = false;
        $this->leases_time->state = false;
        $this->disabled->state = false;
    }
     
     private function convert_to( $source, $target_encoding )  {
        
        if($source != ''){
            
            // detect the character encoding of the incoming file
            $encoding = mb_detect_encoding( $source, "auto" );
               
            // escape all of the question marks so we can remove artifacts from
            // the unicode conversion process
            $target = str_replace( "?", "[question_mark]", $source );
               
            // convert the string to the target encoding
            $target = mb_convert_encoding( $target, $target_encoding, $encoding);
               
            // remove any question marks that have been introduced because of illegal characters
            $target = str_replace( "?", "", $target );
               
            // replace the token string "[question_mark]" with the symbol "?"
            $target = str_replace( "[question_mark]", "?", $target );
           
            return $target;
        }
        
         return $source;
    }
    
}
