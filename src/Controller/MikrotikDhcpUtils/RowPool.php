<?php
namespace App\Controller\MikrotikDhcpUtils;

use App\Controller\Utils\Attr;

class RowPool {
 
    public $id;
    public $controller_id;
    
    public $marked_to_delete;
    
    public $api_id;
    public $name;
    public $ranges;
    
    public $other;

    public function __construct($pool, $side) {
        
        $this->other = $pool['other'];
        
        $this->marked_to_delete = false;
        
        if ($side == 'A') {
            $this->id = $pool['id'];
            $this->controller_id = $pool['controller_id'];
        }
        
        $this->api_id = new Attr($pool['api_id'], true);
        
        if ($side == 'A') {
            $this->name = new Attr($pool['name'], true);
        }else{
            $this->name = new Attr($this->convert_to($pool['name'], "UTF-8"), true);
        }
        
        $this->ranges = new Attr($pool['ranges'], true);

    }

    public function setNew() {
        
        $this->marked_to_delete = true;
        
        $this->api_id->state = false;
        $this->name->state = false;
        $this->ranges->state = false;
    }
    
    private function convert_to( $source, $target_encoding )  {
        
        if($source != ''){
            
            // detect the character encoding of the incoming file
            $encoding = mb_detect_encoding( $source, "auto" );
               
            // escape all of the question marks so we can remove artifacts from
            // the unicode conversion process
            $target = str_replace( "?", "[question_mark]", $source );
               
            // convert the string to the target encoding
            $target = mb_convert_encoding( $target, $target_encoding, $encoding);
               
            // remove any question marks that have been introduced because of illegal characters
            $target = str_replace( "?", "", $target );
               
            // replace the token string "[question_mark]" with the symbol "?"
            $target = str_replace( "[question_mark]", "?", $target );
           
            return $target;
        }
        
         return $source;
    }
    
}
