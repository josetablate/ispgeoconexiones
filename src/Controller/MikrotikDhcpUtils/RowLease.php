<?php
namespace App\Controller\MikrotikDhcpUtils;

use App\Controller\Utils\Attr;

class RowLease {   
   
    public $lease_id;
    public $connection_id;
    
    public $marked_to_delete;
    
    public $address;
    public $mac_address;
    public $server;
    public $comment;
    public $address_lists;
    public $lease_time;
     
    public $api_id;    
    
    public $other;
    
    public function __construct($lease, $side) {
        
        $this->other = $lease['other'];
        
        $this->marked_to_delete = false;
        
        if ($side == 'A') {
            $this->lease_id = $lease['id'];
            $this->connection_id = $lease['connection']['id'];
        }
    
        
        $this->api_id = new Attr($lease['api_id'], true);
        $this->address = new Attr($lease['address'], true);
        $this->mac_address = new Attr($lease['mac_address'], true);
        $this->server = new Attr($lease['server'], true);
        $this->address_lists = new Attr($lease['address_lists'], true);
        $this->lease_time = new Attr($lease['lease_time'], true);
        
        if ($side == 'A') {
            $this->comment = new Attr($lease['comment'], true);
        }else{
            $this->comment = new Attr($this->convert_to($lease['comment'], "UTF-8"), true);
        }
        
        
    }
    
    
    public function setNew(){
        
        $this->marked_to_delete = true;        
        $this->api_id->state = false;        
        $this->address->state = false;
        $this->mac_address->state = false;       
        $this->server->state = false;
        $this->comment->state = false;
        $this->address_lists->state = false;
        $this->lease_time->state = false;
    }
     
    private function convert_to( $source, $target_encoding )  {
        
        if($source != ''){
            
            // detect the character encoding of the incoming file
            $encoding = mb_detect_encoding( $source, "auto" );
               
            // escape all of the question marks so we can remove artifacts from
            // the unicode conversion process
            $target = str_replace( "?", "[question_mark]", $source );
               
            // convert the string to the target encoding
            $target = mb_convert_encoding( $target, $target_encoding, $encoding);
               
            // remove any question marks that have been introduced because of illegal characters
            $target = str_replace( "?", "", $target );
               
            // replace the token string "[question_mark]" with the symbol "?"
            $target = str_replace( "[question_mark]", "?", $target );
           
            return $target;
        }
        
         return $source;
    }
    
}
