<?php
namespace App\Controller\MikrotikDhcpUtils;

use App\Controller\Utils\Attr;

class RowGateway {
 
    public $id;
    public $controller_id;
    
    public $marked_to_delete;
    
    public $api_id;
    public $address;
    public $interface;
    
    public $other;

    public function __construct($gateway, $side) {
        
        $this->other = $gateway['other'];
        
        $this->marked_to_delete = false;
        
        if ($side == 'A') {
            $this->id = $gateway['id'];
            $this->controller_id = $gateway['controller_id'];
        }
        
        $this->api_id = new Attr($gateway['api_id'], true);
        $this->address = new Attr($gateway['address'], true);
        $this->interface = new Attr($gateway['interface'], true);
        
        if ($side == 'A') {
            $this->comment = new Attr($gateway['comment'], true);
        }else{
            $this->comment = new Attr($this->convert_to($gateway['comment'], "UTF-8"), true);
        }
    }

    public function setNew() {
        
        $this->marked_to_delete = true;
        
        $this->api_id->state = false;
        $this->address->state = false;
        $this->interface->state = false;
        $this->comment->state = false;
    }
    
    private function convert_to( $source, $target_encoding )  {
        
        if($source != ''){
            
            // detect the character encoding of the incoming file
            $encoding = mb_detect_encoding( $source, "auto" );
               
            // escape all of the question marks so we can remove artifacts from
            // the unicode conversion process
            $target = str_replace( "?", "[question_mark]", $source );
               
            // convert the string to the target encoding
            $target = mb_convert_encoding( $target, $target_encoding, $encoding);
               
            // remove any question marks that have been introduced because of illegal characters
            $target = str_replace( "?", "", $target );
               
            // replace the token string "[question_mark]" with the symbol "?"
            $target = str_replace( "[question_mark]", "?", $target );
           
            return $target;
        }
        
         return $source;
    }
    
}