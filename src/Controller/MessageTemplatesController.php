<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Routing\Router;

/**
 * MessageTemplates Controller
 *
 * @property \App\Model\Table\MessageTemplatesTable $MessageTemplates
 */
class MessageTemplatesController extends AppController
{
    public function isAuthorized($user = null)
    {
        return parent::allowRol($user['id'], 'AvisoHttp', 'admin');
    }

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('IntegrationRouter', [
            'className' => '\App\Controller\Component\Integrations\IntegrationRouter'
        ]);
    }

    public function index()
    {
        $messageTemplates = $this->MessageTemplates->find()->contain(['Users'])->where(['MessageTemplates.deleted' => false]);

        $this->set(compact('messageTemplates'));
        $this->set('_serialize', ['messageTemplates']);
    }

    public function add()
    {
        if ($this->request->is('post')) {

            $messageTemplate = $this->MessageTemplates->find()->where(['name' => $this->request->getData('name'), 'deleted' => false ])->first();

            if ($messageTemplate) {
                $this->Flash->warning(__('Ya existe una Plantilla con este Nombre'));
                return $this->redirect(['action' => 'add']);
            }

            $data = $this->request->getData();

            $messageTemplate = $this->MessageTemplates->newEntity();
            // Se recupera el id de la sesión del usuario actual.
            $id = $this->Auth->user()['id'];
            // Se le asigna dicha id a la request para que guarde la nueva plantilla con la id del usuario que lo creo.
            $data['user_id'] = $id;
            $data['deleted'] = 0;

            $messageTemplate = $this->MessageTemplates->patchEntity($messageTemplate, $data);

            if ($this->MessageTemplates->save($messageTemplate)) {

                $this->Flash->success(__('Se ha guardado correctamente la Plantilla.'));

                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(__('No se ha podido guardar la Plantilla. Por favor, intente nuevamente.'));
        }
        $messageTemplate = $this->MessageTemplates->newEntity();
        $this->loadModel('TemplateResources');
        $templateResources = $this->TemplateResources->find();

        $this->set(compact('messageTemplate', 'templateResources'));
        $this->set('_serialize', ['messageTemplate']);
    }
  
    public function edit($id = null)
    {
        $messageTemplate = $this->MessageTemplates->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {

            if ($this->MessageTemplates->find()->where(['name' => $this->request->getData('name'), 'deleted' => false, 'id !=' => $id])->first()) {
                $this->Flash->warning(__('Ya existe una Plantilla con este Nombre'));
                return $this->redirect(['action' => 'edit', $id]);
            }
            $messageTemplate = $this->MessageTemplates->patchEntity($messageTemplate, $this->request->getData());
            if ($this->MessageTemplates->save($messageTemplate)) {
                $this->Flash->success(__('Se ha modificado correctamente la Plantilla.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se ha modificado la Plantilla. Por favor, intente nuevamente.'));
        }
        $this->loadModel('TemplateResources');
        $templateResources = $this->TemplateResources->find();

        $this->set(compact('messageTemplate', 'templateResources'));
        $this->set('_serialize', ['messageTemplate']);
    }
 
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $messageTemplate = $this->MessageTemplates->get($_POST['id']);

        $this->loadModel('ConnectionsHasMessageTemplates');
        $count = $this->ConnectionsHasMessageTemplates->find()->where(['message_template_id' => $messageTemplate->id])->count();

        if ($count > 0) {

            $this->Flash->warning(__('Existen {0} avisos activos relacionados a esta plantilla, primero debe eliminar los avisos.', $count));

            return $this->redirect(['action' => 'index']);
        }

        if ($this->MessageTemplates->delete($messageTemplate)) {
            
            $this->Flash->success(__('La plantilla se elimino correctamnete.'));
        } else {
            $this->Flash->error(__('No se pudo eliminar la plantilla'));
        }

        return $this->redirect(['action' => 'index']);
    }

     /**
     * called from 
     * MessagesTemplates/select_customers
    */
    public function getCustomers()
    {
        $this->loadModel('Customers');

        $customers = $this->Customers->find()
            ->contain([
                'Cities',
                'Areas',
                'Connections'
            ])
            ->select([
                'created',
                'code',
                'invoices_no_paid',
                'debt_month',
                'name',
                'doc_type',
                'ident',
                'address',
                'Cities.name',
                'Areas.name',
                'phone',
                'seller',
                'responsible',
                'is_presupuesto',
                'denomination',
                'email',
                'comments',
                'daydue',
                'clave_portal',
            ])
            ->innerJoinWith('Connections', function ($q) {
                    return $q->where(['Connections.deleted' => false, 'Connections.enabled' => true]);
            })
            ->where([
                'Customers.deleted' => false,
            ]);

        $this->set(compact('customers'));
        $this->set('_serialize', ['customers']);
    }

    public function selectCustomers($id_template)
    {
        $messageTemplate = $this->MessageTemplates->get($id_template, [
            'contain' => []
        ]);

        $paraments = $this->request->getSession()->read('paraments');

        //verifico conecxion con el controlador 
        // if (!$paraments->system->integration) {
        //     $this->Flash->warning(__('La conexión con los controladores esta deshabilitada. No se podrá enviar avisos'));
        // }

        if ($this->request->is(['patch', 'post', 'put'])) {     

            // if (!$paraments->system->integration) {
            //     return $this->redirect(['action' => 'index']);    
            // }

            $this->LoadModel('Connections');
            $this->LoadModel('ConnectionsHasMessageTemplates');
             
            $customers_selected = explode(',', $this->request->getData('customers_selected'));      

            $send = 0;
            $re_send = 0;
            $blocked = 0;
            $error_send = 0;
            $send_edit = 0;
        
            foreach ($customers_selected as $code) {
                
                $connections = $this->Connections->find()
                    ->contain([
                        'Controllers',
                        'Customers'
                    ])
                    ->where([
                        'customer_code' => $code,
                        'Connections.deleted' => false
                    ]);

                foreach ($connections as $connection) {

                    if(!$connection->controller->integration){
                        continue;
                    }

                    $chmt_new = null;

                     //veriico primero si ya tiene un aviso activo
                     
                    $chmt_other = $this->ConnectionsHasMessageTemplates->find()
                        ->where([
                            'connections_id' => $connection->id,
                        ])->first();

                    if ($chmt_other) {

                        if ($chmt_other->type != 'Bloqueo') {

                            if ($chmt_other->message_template_id != $messageTemplate->id) {

                                $chmt_other->message_template_id = $messageTemplate->id; //cambio la plantilla nomas

                                if (!$this->ConnectionsHasMessageTemplates->save($chmt_other)) {
                                     $this->Flash->error(__('Error al intentar guardar el aviso en la base de datos. Código de cliente: {0}', $code));
                                     return $this->redirect(['action' => 'selectCustomers', $id_template]); 
                                }

                                $send_edit++;

                            } else {
                                $re_send++;
                                continue;
                            }

                        } else {
                            $blocked++;
                            continue; //por que es un avuiso de bloqueo
                        }

                    } else { //no hay otro aviso

                         $chmt_new =  $this->ConnectionsHasMessageTemplates->newEntity();
                         $chmt_new->created = Time::now();
                         $chmt_new->connections_id = $connection->id;
                         $chmt_new->message_template_id = $messageTemplate->id;
                         $chmt_new->ip = $connection->ip;
                         $chmt_new->type = $messageTemplate->type;

                         if (!$this->ConnectionsHasMessageTemplates->save($chmt_new)) {
                             $this->Flash->error(__('Error al intentar guardar el aviso en la base de datos. Código de cliente: {0}', $code));
                             return $this->redirect(['action' => 'selectCustomers', $id_template]); 
                         }

                         $connection = $this->IntegrationRouter->addAvisoConnection($connection);

                         if (!$connection) {
                            $this->ConnectionsHasMessageTemplates->delete($chmt_new);
                            $error_send++;
                         } else {
                             $send++;
                         }
                    }

                }//end foreach connections

            } //end foreach customers selected

            if ($send > 0) {
                $this->Flash->success(__('{0} Avisos enviados.', $send));
            }
            if ($re_send > 0) {
                $this->Flash->warning(__('{0} Avisos ya enviados.', $re_send));
            }
            if ($blocked > 0) {
                $this->Flash->warning(__('{0} Avisos no enviados, por conexion bloqueada.', $blocked));
            }
            if ($error_send > 0) {
                $this->Flash->warning(__('{0} Errores al intentar enviar.', $error_send));
            }
            if ($send_edit > 0) {
                $this->Flash->warning(__('{0} Ediciones de plantila de avisos ya enviados.', $send_edit));            
            }

            return $this->redirect(['action' => 'selectCustomers', $id_template]);
        }

        $this->loadModel('Labels');
        $labels_array = $this->Labels->find()->where(['entity' => 0])->toArray();
        
        $labels = [];
        $labels[] = [
            'id' => 0,
            'text' => 'Sin etiquetas',
            'entity' => 0,
            'color_back' => '#000000',
            'color_text' => '#000000',
            'enabled' => 1,
        ];

        foreach ($labels_array as $label) {
            $labels[] = $label;
        }

        $this->set(compact('messageTemplate', 'labels'));
        $this->set('_serialize', ['messageTemplate']);
    }

    public function selectConnections($id_template)
    {
        $messageTemplate = $this->MessageTemplates->get($id_template, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {

            $this->LoadModel('Connections');
            $this->LoadModel('ConnectionsHasMessageTemplates');

            $connections_selected = explode(',', $this->request->getData('connections_selected'));

            $counter = 0;
            $counter_unsync = 0;
            $controllers_unsync = '';

            foreach ($connections_selected as $id) {

                $connection = $this->Connections->get($id, ['contain' => ['Controllers', 'Customers']]);

                $chmt_new = null;

                //veriico primero si ya tiene un aviso activo

                $chmt_other = $this->ConnectionsHasMessageTemplates->find()
                    ->where([
                        'connections_id' => $connection->id,
                    ])->first();

                 if ($chmt_other) {

                    if ($chmt_other->type != 'Bloqueo') {

                        if ($chmt_other->message_template_id != $messageTemplate->id) {

                            $chmt_other->message_template_id = $messageTemplate->id; //cambio la plantilla nomas

                            if (!$this->ConnectionsHasMessageTemplates->save($chmt_other)) {
                                 $this->Flash->error(__('Error al intentar guardar el aviso en la base de datos. Código de cliente: {0}', $code));
                                 return $this->redirect(['action' => 'selectConnections', $id_template]); 
                            }

                        } else {
                            continue;
                        }
                    } else {
                        continue; //por que es un avuiso de bloqueo
                    }

                } else { //no hay otro aviso

                    $chmt_new =  $this->ConnectionsHasMessageTemplates->newEntity();
                    $chmt_new->created = Time::now();
                    $chmt_new->connections_id = $connection->id;
                    $chmt_new->message_template_id = $messageTemplate->id;
                    $chmt_new->ip = $connection->ip;
                    $chmt_new->type = $messageTemplate->type;

                    if (!$this->ConnectionsHasMessageTemplates->save($chmt_new)) {
                        $this->Flash->error(__('Erro al intentar guardar el aviso en la base de datos. Código de cliente: {0}', $code));
                        return $this->redirect(['action' => 'selectConnections', $id_template]); 
                    }

                    $connection = $this->IntegrationRouter->addAvisoConnection($connection);

                    if (!$connection) {
                        $this->ConnectionsHasMessageTemplates->delete($chmt_new);
                    } 
                }
            }

            $this->Flash->success(__('Avisos enviados.'));

            return $this->redirect(['action' => 'selectConnections', $id_template]);
        }

        $this->loadModel('Controllers');
        $controllers = $this->Controllers->find()->where(['enabled' => true, 'deleted' => false]);

        $controllersArray = [];
        $controllersArray[''] = 'Seleccione';
        $controllersArray['-2'] = 'Recientes';
        $controllersArray['-1'] = 'Todos';

        foreach ($controllers as $controller) {
            $controllersArray[$controller->id] = $controller->name;
        }

        $this->set(compact('controllersArray'));

        $this->set(compact('messageTemplate'));
        $this->set('_serialize', ['messageTemplate']);
    }
}
