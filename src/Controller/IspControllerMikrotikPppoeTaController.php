<?php
namespace App\Controller;

use App\Controller\IspControllerMikrotikPppoe;

use App\Controller\MikrotikPppoeUtils\RowActive;
use App\Controller\MikrotikPppoeUtils\RowAddressList;
use App\Controller\MikrotikPppoeUtils\RowPppoeServer;
use App\Controller\MikrotikPppoeUtils\RowProfile;
use App\Controller\MikrotikPppoeUtils\RowQueue;
use App\Controller\MikrotikPppoeUtils\RowSecret;

use Cidr;
use Cake\Filesystem\File;
use Cake\I18n\Time;



class IspControllerMikrotikPppoeTaController extends IspControllerMikrotikPppoe
{
    public function initialize() {
         
        parent::initialize();     
    }    
    
    
    //########CONFIG#########
    
    //controller

    public function edit($id = null) {         
        
        $this->loadModel('Controllers');

        $controller = $this->Controllers->get($id, [
            'contain' => []
        ]);

        $tcontroller = $this->IntegrationRouter->MikrotikPppoeTa->getControllerTemplate($controller); 

        $controller->local_address = $tcontroller->local_address;
        $controller->dns_server = $tcontroller->dns_server;  
        $controller->queue_default = $tcontroller->queue_default;  
        

        if ($this->request->is(['patch', 'post', 'put'])) {

            $request_data = $this->request->getData();

            $request_data['queue_default'] = ( array_key_exists('queue_default', $request_data)) ? $request_data['queue_default'] : $tcontroller->queue_default; 

            if ($this->IntegrationRouter->MikrotikPppoeTa->editController($controller, $request_data)) {

                $controller =  $this->Controllers->patchEntity($controller, $request_data);

                if ($this->Controllers->save($controller)) {
                    $this->Flash->success(__('Los cambios se guardaron correctamente.'));
                } else {
                    $this->_ctrl->Flash->error(__('No se pudo editar el Controlador.'));
                }
            }
        }

        $controller->queue_default = ($controller->queue_default) ? $controller->queue_default : 'default-small' ; 
        $controller->trademark_name = $this->getTrademarksByTemplate($controller->template)[$controller->trademark];
        $controller->template_name = $this->getTemplateName($controller->template);
        $queue_types_array = $this->IntegrationRouter->MikrotikPppoeTa->getQueueTypesApi($controller);

        $this->set(compact('controller', 'queue_types_array'));
        $this->set('_serialize', ['controller']);
    }
    
    public function config($id) {
        
        $this->loadModel('Controllers');
        $this->loadModel('Services');

        $controller = $this->Controllers->get($id, [
            'contain' => []
        ]);

        $tcontroller = $this->IntegrationRouter->MikrotikPppoeTa->getControllerTemplate($controller); 

        $controller->local_address = $tcontroller->local_address;
        $controller->dns_server = $tcontroller->dns_server;  
        $controller->queue_default = $tcontroller->queue_default;  

        $controller->queue_default = ($controller->queue_default) ? $controller->queue_default : 'default-small' ; 
         
        $controller->interfaces = $this->IntegrationRouter->MikrotikPppoeTa->getInterfacesApi($controller);
     
        $queue_types_array = $this->IntegrationRouter->MikrotikPppoeTa->getQueueTypesApi($controller);

        $services = $this->Services->find()->where(['deleted' => false, 'enabled' => true]);
        $servicesArray = [];
        $servicesArray[''] = '';         
        foreach ($services as $service){
            $servicesArray[$service->id] = $service->name;
        }    

        $this->set(compact('controller', 'queue_types_array', 'servicesArray'));
        $this->set('_serialize', ['controller']);
    }

    public function sync($id) {
        
        $this->loadModel('Controllers');

        $controller = $this->Controllers->get($id);
        
        $this->clearQueues();
        $this->clearSecrets();
        $this->clearActives();
        $this->clearProfiles();
        $this->clearAddressLists();
        $this->clearPppoeServers();

        $this->set(compact('controller'));
        $this->set('_serialize', ['iController']);
    }
     
    //desde shell (control automatico)
    public function checkSync($id){
        
        $this->loadModel('Controllers');
        
        $controller = $this->Controllers->get($id);
         
        $paraments = $this->request->getSession()->read('paraments');                 
         
        if($controller->integration && $this->IntegrationRouter->MikrotikPppoeTa->getStatus($controller)){
             
             $controller->counter_unsync = 0;        
             $controller->counter_unsync += $this->diffQueues($controller, true);
             $controller->counter_unsync += $this->diffSecrets($controller, true);
             $controller->counter_unsync += $this->diffAddressLists($controller, true);       
             $controller->last_status = true;                  
        }else{
             $controller->last_status = false;              
        }
        
        $controller->last_control_diff = Time::now();      
        $this->Controllers->save($controller);    
        
        return $controller->counter_unsync;
        
    }
     
    
  
    
    //plans
    
    public function getPlansByController(){

        $controller_id = $this->request->getQuery('controller_id');
        
        $plans = $this->IntegrationRouter->MikrotikPppoeTa->getPlans($controller_id);
    
        $this->set(compact('plans'));
        $this->set('_serialize', ['plans']);
    }

    public function addPlan(){
        
      if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);

            $response = $this->IntegrationRouter->MikrotikPppoeTa->addPlan($request_data);
            
            $this->set('response', $response);
        }
    }
    
    public function movePlan(){
        
      if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            /*
            [service_id_destino] => 2
            [controller_id] => 1
            [id] => 1
            */
            
            //$this->log($request_data, 'debug');

            $response = $this->IntegrationRouter->MikrotikPppoeTa->movePlan($request_data);
            
            $this->set('response', $response);
        }
    }
    
    
     
    public function editPlan() {
        
        if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            $response = $this->IntegrationRouter->MikrotikPppoeTa->editPlan($request_data);
            
            $this->set('response', $response);
        }
    }

    public function deletePlan() {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $data = $this->request->input('json_decode');
    
            $response = $this->IntegrationRouter->MikrotikPppoeTa->deletePlan($data->id);

            $this->set('response', $response);
            
        }
    }
    
    
    //pools
    
    public function getPoolsByController(){

        $controller_id = $this->request->getQuery('controller_id');
        
        $pools = $this->IntegrationRouter->MikrotikPppoeTa->getPools($controller_id);
    
        $this->set(compact('pools'));
        $this->set('_serialize', ['pools']);
    }
    
    public function addPool() {
        
        if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            $response = $this->IntegrationRouter->MikrotikPppoeTa->addPool($request_data);
            
            $this->set('response', $response);
        }
    }
    
    public function deletePool() {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $data = $this->request->input('json_decode');
            
            $response = $this->IntegrationRouter->MikrotikPppoeTa->deletePool( $data->id);

            $this->set('response', $response);
        }
    }

    public function editPool(){
        
        if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            $response = $this->IntegrationRouter->MikrotikPppoeTa->editPool($request_data);
            
            $this->set('response', $response);
        }
    }
    
    public function getRangeNetwork()  {
         
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $addresses = $this->request->input('json_decode')->addresses;
            $range = Cidr::cidrToRange($addresses);
            $this->set('range', $range);
        }
    } 
    
    public function movePool(){
        
        if ($this->request->is('ajax')) {

            $data = $this->request->input('json_decode');
             
            $request_data = json_decode(json_encode($data), true);

            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($request_data['controller_id']);

            $response = $this->IntegrationRouter->MikrotikPppoeTa->movePool($request_data);

            $this->set('response', $response);
        }
        
    }
    
    
    //profiles
    
    public function getProfilesByController(){

        $controller_id = $this->request->getQuery('controller_id');
        
        $profiles = $this->IntegrationRouter->MikrotikPppoeTa->getProfiles($controller_id);
    
        $this->set(compact('profiles'));
        $this->set('_serialize', ['profiles']);
    }
    
    public function addProfile(){
        
         if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            $response = $this->IntegrationRouter->MikrotikPppoeTa->addProfile($request_data);
            
            $this->set('response', $response);
        }

    }

    public function deleteProfile()    {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $data = $this->request->input('json_decode');
            
            $response = $this->IntegrationRouter->MikrotikPppoeTa->deleteProfile( $data->id);

            $this->set('response', $response);
        }
    }

    public function editProfile()  {
        
         if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            $response = $this->IntegrationRouter->MikrotikPppoeTa->editProfile($request_data);
            
            $this->set('response', $response);
        }
 
    }
     
    
    //pppoe servers
    
    public function getPppoeServersByController(){

        $controller_id = $this->request->getQuery('controller_id');
 
        $pppServers = $this->IntegrationRouter->MikrotikPppoeTa->getPppoeServers($controller_id);
    
        $this->set(compact('pppServers'));
        $this->set('_serialize', ['pppServers']);
    }
    
    public function addPppoeServer()  {
        
        if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            $response = $this->IntegrationRouter->MikrotikPppoeTa->addPppoeServer($request_data);
            
            $this->set('response', $response);
        }
       
    }

    public function deletePppoeServer()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $data = $this->request->input('json_decode');
            
            $response = $this->IntegrationRouter->MikrotikPppoeTa->deletePppoeServer( $data->id);

            $this->set('response', $response);
        }
    }
    
    
    //ip excluida    
   
    public function getIpExcludedByController() {

        $controller_id = $this->request->getQuery('controller_id');
 
        $ip_excluded = $this->IntegrationRouter->MikrotikPppoeTa->getIPExcluded($controller_id);
    
        $this->set(compact('ip_excluded'));
        $this->set('_serialize', ['ip_excluded']);
    }
    
    public function deleteIPExcluded() {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $data = $this->request->input('json_decode');
            
            $response = $this->IntegrationRouter->MikrotikPppoeTa->deleteIPExcludedById( $data->id);

            $this->set('response', $response);
        }
    }
    
    public function addIPExcluded() {
        
        if ($this->request->is('ajax')) {
            
            $data = $this->request->input('json_decode');
            $request_data = json_decode(json_encode($data), true);
            
            $response = $this->IntegrationRouter->MikrotikPppoeTa->addIPExcludedDirect($request_data);
            
            $this->set('response', $response);
        }
       
    } 
     
     
     
     
     
     
    
    
    //########SYNC#########   

    
    //sync secrets

    public function syncSecrets()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
      
            $connections_ids = $this->request->input('json_decode')->connections_ids;
            $delete_secret_side_b = $this->request->input('json_decode')->delete_secret_side_b;
            
            $this->loadModel('Connections');
            $success = true;
        
                
            $fileB = new File(WWW_ROOT . 'sync/mikrotik_pppoe_ta/secretsB.json', false, 0644);
            $json = $fileB->read($fileB, 'r');
            $fileB->close();
            $json =  json_decode($json);
            
            
            foreach($json->data as $row){
                
                if($row->marked_to_delete){
                    
                    if(!$row->other){ //son registro del controlador, que deben eliminarse,            
                    	
                    	if(!$this->IntegrationRouter->MikrotikPppoeTa->deleteSecretInController($controller, $row->api_id->value)){
                             $success = false;
                             break;
                        }
                    	
                    }else if($row->other && $delete_secret_side_b){ //son registros agenos al controlador, y esta marcado para limpieza
                    	
                    	if(!$this->IntegrationRouter->MikrotikPppoeTa->deleteSecretInController($controller, $row->api_id->value)){
                             $success = false;
                             break;
                        }
                    }
                    
                }
            }
            
            foreach($connections_ids as $connection_id){
                
                $connection = $this->Connections->get($connection_id, ['contain' => ['Controllers', 'Customers']]);
                if(!$this->IntegrationRouter->MikrotikPppoeTa->syncSecret($connection)){
                    $success = false;
                    break;
                }
            }
    
            $this->diffSecrets($controller);

            $this->set('data', $success);
        }
    }
    
    public function syncSecretIndi()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
      
            $connections_ids = $this->request->input('json_decode')->connections_ids;
            
            $this->loadModel('Connections');
            $success = true;
         
            
            foreach($connections_ids as $connection_id){
                
                $connection = $this->Connections->get($connection_id, ['contain' => ['Controllers', 'Customers']]);
                if(!$this->IntegrationRouter->MikrotikPppoeTa->syncSecret($connection)){
                    $success = false;
                    break;
                }
            }        
         
            
            $this->diffSecrets($controller);

            $this->set('data', $success);
        }
    }
  
    public function refreshSecrets(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $ok = false;
            
            if($this->IntegrationRouter->MikrotikPppoeTa->getStatus($controller)){
  
                $ok = true;
            }
            
            if($ok){
                $this->diffSecrets($controller);
            }
            
            $this->set('data', $ok);
        }
        
    }
  
    public function deleteSecretInController(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
            $secret_api_id = $this->request->input('json_decode')->secret_api_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $success = true;
            
            if(!$this->IntegrationRouter->MikrotikPppoeTa->deleteSecretInController($controller, $secret_api_id->value)){
                 $success = false;
            }
                     
            
            $this->diffSecrets($controller);
            
            $this->set('data', $success);
        }
        
    }
    
    protected function diffSecrets($controller, $file = true){
        
        if($file){
             $this->clearSecrets();    
        } 

        $secretsA = $this->IntegrationRouter->MikrotikPppoeTa->getSecretsAndConnectionsArray($controller);
        
        $secretsB = $this->IntegrationRouter->MikrotikPppoeTa->getSecretsInController($controller);
        
        $tableA = [];
        $tableB = [];
        
        $count = 0;  
        
        foreach ($secretsA as $secret_a) {
            
            $row = new RowSecret($secret_a, 'A');
             
            //existe en A pero no en B
            if(!array_key_exists($secret_a['name'], $secretsB)){
                $tableA[] = $row;
                $count++;
            }else{
                
                $diff = false;
                
                $secret_b = $secretsB[$secret_a['name']];
                
                if(strtoupper($secret_a['api_id']) != strtoupper($secret_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
             
                }
                
                if($secret_a['name'] != $secret_b['name']){
                    $row->name->state = false;
                     $diff = true;
                }
                
                if($secret_a['password'] != $secret_b['password']){
                    $row->password->state = false;
                     $diff = true;
                }
                
                if($secret_a['service'] != $secret_b['service']){
                    $row->service->state = false;
                     $diff = true;
                }
                
                if($secret_a['profile']['name'] != $secret_b['profile']){
                    $row->profile->state = false;
                     $diff = true;
                }
                
                if(ip2long($secret_a['remote_address']) != ip2long($secret_b['remote_address'])){
                    $row->remote_address->state = false;
                    $diff = true;
                }
                
                if($secret_a['comment'] != $secret_b['comment']){
                    $row->comment->state = false;
                    $diff = true;
                }
                
                if($secret_a['caller_id'] != $secret_b['caller_id']){
                    $row->caller_id->state = false;
                    $diff = true;
                }
                
              
                if( $diff){
                    $tableA[] = $row;
                    $count++;
                }
                
            }
        }
    
        foreach ($secretsB as $secret_b) {
            
            $row = new RowSecret($secret_b, 'B');
            
            if($secret_b['other'] == true){
            	$row->setNew();
            	$tableB[] = $row;
            	continue;
            }
            
            //existe en B pero no en A
            if(!array_key_exists($secret_b['name'], $secretsA)){
                $row->setNew();
                $tableB[] = $row;
                $count++;
                
            }else{
                
                $diff = false;
                
                $secret_a = $secretsA[$secret_b['name']];
            
                if(strtoupper($secret_a['api_id']) != strtoupper($secret_b['api_id'])){
                    $row->setNew();
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($secret_a['name'] != $secret_b['name']){
                    $row->name->state = false;
                     $diff = true;
                }
                
                if($secret_a['password'] != $secret_b['password']){
                    $row->password->state = false;
                     $diff = true;
                }
                
                if($secret_a['service'] != $secret_b['service']){
                    $row->service->state = false;
                     $diff = true;
                }
                
                if($secret_a['profile']['name'] != $secret_b['profile']){
                    $row->profile->state = false;
                     $diff = true;
                }
                
                if(ip2long($secret_a['remote_address']) != ip2long($secret_b['remote_address'])){
                    $row->remote_address->state = false;
                    $diff = true;
                }
                
                if($secret_a['comment'] != $secret_b['comment']){
                    $row->comment->state = false;
                    $diff = true;
                }
                
                if($secret_a['caller_id'] != $secret_b['caller_id']){
                    $row->caller_id->state = false;
                    $diff = true;
                }
                
              
                if( $diff){
                    $tableB[] = $row;
                    $count++;
                }
            }
            
        }
        
        if($file){
            
            $this->updateFile($tableA,'secretsA', 'mikrotik_pppoe_ta');
            $this->updateFile($tableB,'secretsB', 'mikrotik_pppoe_ta');
        }
        
        return $count; 
        
    } 
    
    protected function clearSecrets(){
        
        $this->updateFile([],'secretsA', 'mikrotik_pppoe_ta');
        $this->updateFile([],'secretsB', 'mikrotik_pppoe_ta');
        
    } 
    
    
    
    
    //sync actives

    public function syncActives()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
      
            $connections_ids = $this->request->input('json_decode')->connections_ids;
            $delete_active_side_b = $this->request->input('json_decode')->delete_active_side_b;
            
            $this->loadModel('Connections');
            $success = true;
        
                
            $fileB = new File(WWW_ROOT . 'sync/mikrotik_pppoe_ta/activesB.json', false, 0644);
            $json = $fileB->read($fileB, 'r');
            $fileB->close();
            $json =  json_decode($json);
            
            
            foreach($json->data as $row){
                
                if($row->marked_to_delete){
                    
                    if(!$row->other){ //son registro del controlador, que deben eliminarse,            
                    	
                    	if(!$this->IntegrationRouter->MikrotikPppoeTa->deleteActiveInController($controller, $row->api_id->value)){
                             $success = false;
                             break;
                        }
                    	
                    }else if($row->other && $delete_active_side_b){ //son registros agenos al controlador, y esta marcado para limpieza
                    	
                    	if(!$this->IntegrationRouter->MikrotikPppoeTa->deleteActiveInController($controller, $row->api_id->value)){
                             $success = false;
                             break;
                        }
                    }
                    
                }
            }           
   
            
            $this->diffActives($controller);

            $this->set('data', $success);
        }
    }
  
    public function refreshActives(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $ok = false;
            
            if($this->IntegrationRouter->MikrotikPppoeTa->getStatus($controller)){
  
                $ok = true;
            }
            
            if($ok){
                $this->diffActives($controller);
            }
            
            $this->set('data', $ok);
        }
        
    }
  
    public function deleteActiveInController(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
            $active_api_id = $this->request->input('json_decode')->active_api_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $success = true;
            
            if(!$this->IntegrationRouter->MikrotikPppoeTa->deleteActiveInController($controller, $active_api_id->value)){
                 $success = false;
            }           
           
            
            $this->diffActives($controller);
            
            $this->set('data', $success);
        }
        
    }
    
    protected function diffActives($controller, $file = true){
         
        if($file){
               $this->clearActives();             
        }

        $activesA = $this->IntegrationRouter->MikrotikPppoeTa->getSecretsAndConnectionsArray($controller);
        
        $activesB = $this->IntegrationRouter->MikrotikPppoeTa->getActivesInController($controller);
        
        $tableA = [];
        $tableB = [];
        
        $count = 0;
        
        foreach ($activesA as $active_a) {
            
            $row = new RowActive($active_a, 'A');
             
            //existe en A pero no en B
            if(!array_key_exists($active_a['name'], $activesB)){
                $tableA[] = $row;
                $count++;
            }else{
                
                $diff = false;
                
                $active_b = $activesB[$active_a['name']];
               
                if($active_a['name'] != $active_b['name']){
                    $row->name->state = false;
                     $diff = true;
                }
               
                if($active_a['remote_address'] != ip2long($active_b['address'])){
                    $row->remote_address->state = false;
                    $diff = true;
                }
              
              
                if( $diff){
                    $tableA[] = $row;
                    $count++;
                }
                
            }
        }
    
        foreach ($activesB as $active_b) {
            
            $row = new RowActive($active_b, 'B');
            
            if($active_b['other'] == true){
            	$row->setNew();
            	$tableB[] = $row;
            	continue;
            }
            
            //existe en B pero no en A
            if(!array_key_exists($active_b['name'], $activesA)){
                $row->setNew();
                $tableB[] = $row;
                $count++;
                
            }else{
                
                $diff = false;
                
                $active_a = $activesA[$active_b['name']];
              
                if($active_a['name'] != $active_b['name']){
                    $row->name->state = false;
                     $diff = true;
                }
             
                if($active_a['remote_address'] != ip2long($active_b['address'])){
                    $row->address->state = false;
                    $diff = true;
                }
              
                if( $diff){
                    $row->setNew();
                    $tableB[] = $row;
                    $count++;
                }
            }
            
        }
        
        if($file){
            
            $this->updateFile($tableA,'activesA', 'mikrotik_pppoe_ta');
            $this->updateFile($tableB,'activesB', 'mikrotik_pppoe_ta');
            
        }
        
        return $count; 
        
    } 
    
    protected function clearActives(){
        
        $this->updateFile([],'activesA', 'mikrotik_pppoe_ta');
        $this->updateFile([],'activesB', 'mikrotik_pppoe_ta');
        
    } 
    
    
    
    
    
     //sync queues

    public function syncQueues()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
      
            $connections_ids = $this->request->input('json_decode')->connections_ids;
            $delete_queue_side_b = $this->request->input('json_decode')->delete_queue_side_b;
            
            $this->loadModel('Connections');
            $success = true;
            
            $fileB = new File(WWW_ROOT . 'sync/mikrotik_pppoe_ta/queuesB.json', false, 0644);
            $json = $fileB->read($fileB, 'r');
            $fileB->close();
            $json =  json_decode($json);
            
            foreach($json->data as $row){
                
                if($row->marked_to_delete){
                    
                    if(!$row->other){ //son registro del controlador, que deben eliminarse,            
                    	
                    	if(!$this->IntegrationRouter->MikrotikPppoeTa->deleteQueueInController($controller, $row->api_id->value)){
                             $success = false;
                             break;
                        }
                    	
                    }else if($row->other && $delete_queue_side_b){ //son registros agenos al controlador, y esta marcado para limpieza
                    	
                    	if(!$this->IntegrationRouter->MikrotikPppoeTa->deleteQueueInController($controller, $row->api_id->value)){
                             $success = false;
                             break;
                        }
                    }
                }
            }
   
            
            
            foreach($connections_ids as $connection_id){
                
                $connection = $this->Connections->get($connection_id, ['contain' => ['Controllers', 'Customers']]);
                if(!$this->IntegrationRouter->MikrotikPppoeTa->syncQueue($connection)){
                    $success = false;
                    break;
                }
            }
            
            $this->diffQueues($controller);

            $this->set('response', $success);
        }
    }
    
    public function syncQueueIndi()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
      
            $connections_ids = $this->request->input('json_decode')->connections_ids;
            
            $this->loadModel('Connections');
            $success = true;
         
            foreach($connections_ids as $connection_id){
                
                $connection = $this->Connections->get($connection_id, ['contain' => ['Controllers', 'Customers']]);
                if(!$this->IntegrationRouter->MikrotikPppoeTa->syncQueue($connection)){
                    $success = false;
                    break;
                }
            }
            
         
            
            $this->diffQueues($controller);

            $this->set('response', $success);
        }
    }
  
    public function refreshQueues(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $ok = false;
            
            if($this->IntegrationRouter->MikrotikPppoeTa->getStatus($controller)){
  
                $ok = true;
            }
            
            if($ok){
                $this->diffQueues($controller);
            }
            
            $this->set('data', $ok);
        }
        
    }
  
    public function deleteQueueInController(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
            $queue_api_id = $this->request->input('json_decode')->queue_api_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $success = true;
        
            if(!$this->IntegrationRouter->MikrotikPppoeTa->deleteQueueInController($controller, $queue_api_id->value)){
                 $success = false;
            }            
            
            $this->diffQueues($controller);
            
            $this->set('data', $success);
        }
        
    }
    
    protected function diffQueues($controller, $file = true){
         
        if($file){
              $this->clearQueues();
        }

        $queuesA = $this->IntegrationRouter->MikrotikPppoeTa->getQueuesAndConnectionsArray($controller);
        
        $queuesB = $this->IntegrationRouter->MikrotikPppoeTa->getQueuesInController($controller);
        
        $tableA = [];
        $tableB = [];
        
        $count = 0;
        
        foreach ($queuesA as $queue_a) {
            
            $row = new RowQueue($queue_a, 'A');
             
            //existe en A pero no en B
            if(!array_key_exists($queue_a['name'], $queuesB)){
                $tableA[] = $row;
                $count++;
            }else{
                
                $diff = false;
                
                $queue_b = $queuesB[$queue_a['name']];
                
                if(strtoupper($queue_a['api_id']) != strtoupper($queue_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($queue_a['name'] != $queue_b['name']){
                    $row->name->state = false;
                     $diff = true;
                }
                
                if($queue_a['comment'] != $queue_b['comment']){
                    $row->comment->state = false;
                    $diff = true;
                }
                
                if($queue_a['target'] != $queue_b['target']){
                    $row->target->state = false;
                    $diff = true;
                }
                
            
                if($queue_a['max_limit']  != $queue_b['max_limit']){
                    $row->max_limit->state = false;
                    $diff = true;
                }
                
                 if($queue_a['limit_at']  != $queue_b['limit_at']){
                    $row->limit_at->state = false;
                    $diff = true;
                }
                
                 if($queue_a['burst_limit']  != $queue_b['burst_limit']){
                    $row->burst_limit->state = false;
                    $diff = true;
                }
                
                 if($queue_a['burst_threshold']  != $queue_b['burst_threshold']){
                    $row->burst_threshold->state = false;
                    $diff = true;
                }
                
                 if($queue_a['burst_time']  != $queue_b['burst_time']){
                    $row->burst_time->state = false;
                    $diff = true;
                }
                
                 if($queue_a['priority']  != $queue_b['priority']){
                    $row->priority->state = false;
                    $diff = true;
                }
                
                 if($queue_a['queue']  != $queue_b['queue']){
                    $row->queue->state = false;
                    $diff = true;
                }
                
              
                if( $diff){
                    $tableA[] = $row;
                    $count++;
                }
                
            }
        }
        
        foreach ($queuesB as $queue_b) {
            
            $row = new RowQueue($queue_b, 'B');
            
            if($queue_b['other'] == true){
            	$row->setNew();
            	$tableB[] = $row;
            	continue;
            }
            
            //existe en B pero no en A
            if(!array_key_exists($queue_b['name'], $queuesA)){
                $row->setNew();
                $tableB[] = $row;
                $count++;
                
            }else{
              
                $diff = false;
                
                $queue_a = $queuesA[$queue_b['name']];
            
                if(strtoupper($queue_a['api_id']) != strtoupper($queue_b['api_id'])){
                    $row->setNew();
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($queue_a['name'] != $queue_b['name']){
                    $row->name->state = false;
                     $diff = true;
                }
                
                if($queue_a['comment'] != $queue_b['comment']){
                    $row->comment->state = false;
                    $diff = true;
                }
               
                if($queue_a['target'] != $queue_b['target']){
                    $row->target->state = false;
                    $diff = true;
                }
                
                if($queue_a['max_limit']  != $queue_b['max_limit']){
                    $row->max_limit->state = false;
                    $diff = true;
                }
                
                 if($queue_a['limit_at']  != $queue_b['limit_at']){
                    $row->limit_at->state = false;
                    $diff = true;
                }
                
                 if($queue_a['burst_limit']  != $queue_b['burst_limit']){
                    $row->burst_limit->state = false;
                    $diff = true;
                }
                
                 if($queue_a['burst_threshold']  != $queue_b['burst_threshold']){
                    $row->burst_threshold->state = false;
                    $diff = true;
                }
                
                 if($queue_a['burst_time']  != $queue_b['burst_time']){
                    $row->burst_time->state = false;
                    $diff = true;
                }
                
                 if($queue_a['priority']  != $queue_b['priority']){
                    $row->priority->state = false;
                    $diff = true;
                }
                
                 if($queue_a['queue']  != $queue_b['queue']){
                    $row->queue->state = false;
                    $diff = true;
                }
                
              
                if( $diff){
                    $tableB[] = $row;
                    $count++;
                }
            }
            
        }
        
        if($file){
             
            $this->updateFile($tableA,'queuesA', 'mikrotik_pppoe_ta');
            $this->updateFile($tableB,'queuesB', 'mikrotik_pppoe_ta');
        }
        
        return $count;
    } 
    
    protected function clearQueues(){
        
        $this->updateFile([],'queuesA', 'mikrotik_pppoe_ta');
        $this->updateFile([],'queuesB', 'mikrotik_pppoe_ta');
    } 
    
    
    
    
    // sync profiles

    public function syncProfiles()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $ok = true;
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            $profiles_ids = $this->request->input('json_decode')->profiles_ids;
            $delete_profiles_side_b = $this->request->input('json_decode')->delete_profiles_side_b;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
                
            $fileB = new File(WWW_ROOT . 'sync/mikrotik_pppoe_ta/profilesB.json', false, 0644);
            $json = $fileB->read($fileB, 'r');
            $fileB->close();
            $json =  json_decode($json);
            
            foreach($json->data as $row){
                
                if($row->marked_to_delete){
                    
                    if(!$row->other){ //son registro del controlador, que deben eliminarse,                        
                    
                        if(!$this->IntegrationRouter->MikrotikPppoeTa->deleteProfileInController($controller, $row->api_id->value)){
                            $ok = false;
                            break;
                        }
                    	
                    }else if($row->other && $delete_profiles_side_b){ //son registros agenos al controlador, y esta marcado para limpieza
                    	
                    	if(!$this->IntegrationRouter->MikrotikPppoeTa->deleteProfileInController($controller, $row->api_id->value)){
                            $ok = false;
                            break;
                        }
                    }
                }
            }
           
   
            if($ok){
                
                foreach($profiles_ids as $profile_id){
                    
                    if(!$this->IntegrationRouter->MikrotikPppoeTa->syncProfile($profile_id)){
                        $ok = false;
                        break;
                    }
                }
                
            }
            
            $this->diffProfiles($controller);

            $this->set('response', $ok);
        }
    }
    
    public function syncProfileIndi()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $ok = true;
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            $profiles_ids = $this->request->input('json_decode')->profiles_ids;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
                
            foreach($profiles_ids as $profile_id){
                
                if(!$this->IntegrationRouter->MikrotikPppoeTa->syncProfile($profile_id)){
                    $ok = false;
                    break;
                }
            }
            
            $this->diffProfiles($controller);

            $this->set('response', $ok);
        }
    }
  
    public function refreshProfiles(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $ok = false;
            
            if($this->IntegrationRouter->MikrotikPppoeTa->getStatus($controller)){
  
                $ok = true;
            }
            
            if($ok){
                $this->diffProfiles($controller);
            }
            
            $this->set('data', $ok);
        }
        
    }
 
    public function deleteProfileInController(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
            $profile_api_id = $this->request->input('json_decode')->profile_api_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $success = true;
            
            if(!$this->IntegrationRouter->MikrotikPppoeTa->deleteProfileInController($controller, $profile_api_id->value)){
                 $success = false;
            }

            $this->diffProfiles($controller);
            
            $this->set('data', $success);
        }
        
    }
    
    protected function diffProfiles($controller){
         
        $this->clearProfiles();

        $profilesA = $this->IntegrationRouter->MikrotikPppoeTa->getProfilesArray($controller->id);
        
        $profilesB = $this->IntegrationRouter->MikrotikPppoeTa->getProfilesInController($controller);
        
        $tableA = [];
        $tableB = [];
        
        foreach ($profilesA as $profile_a) {
            
            $row = new RowProfile($profile_a, 'A');
             
            //existe en A pero no en B
            if(!array_key_exists($profile_a['name'], $profilesB)){
                $tableA[] = $row;
            }else{
                
                $diff = false;
                
                $profile_b = $profilesB[$profile_a['name']];
                
                if(strtoupper($profile_a['api_id']) != strtoupper($profile_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($profile_a['name'] != $profile_b['name']){
                    $row->name->state = false;
                     $diff = true;
                }
                
                if($profile_a['local_address'] != $profile_b['local_address']){
                    $row->local_address->state = false;
                     $diff = true;
                }
                
                if($profile_a['dns_server'] != $profile_b['dns_server']){
                    $row->dns_server->state = false;
                     $diff = true;
                }
                
                if($profile_a['rate_limit_string'] != $profile_b['rate_limit_string']){
                    $row->rate_limit_string->state = false;
                     $diff = true;
                }
                
                if($profile_a['queue_type'] != $profile_b['queue_type']){
                    $row->queue_type->state = false;
                    $diff = true;
                }
         
              
                if( $diff){
                    $tableA[] = $row;
                }
                
            }
        }
        
        foreach ($profilesB as $profile_b) {
            
            $row = new RowProfile($profile_b, 'B');
            
            if($profile_b['other'] == true){
            	$row->setNew();
            	$tableB[] = $row;
            	continue;
            }
            
            //existe en B pero no en A
            if(!array_key_exists($profile_b['name'], $profilesA)){
                $row->setNew();
                $tableB[] = $row;
                
            }else{
                
                $diff = false;
                
                $profile_a = $profilesA[$profile_b['name']];
            
                if(strtoupper($profile_a['api_id']) != strtoupper($profile_b['api_id'])){
                    $row->setNew();
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($profile_a['name'] != $profile_b['name']){
                    $row->name->state = false;
                     $diff = true;
                }
                
                if($profile_a['local_address'] != $profile_b['local_address']){
                    $row->local_address->state = false;
                     $diff = true;
                }
                
                if($profile_a['dns_server'] != $profile_b['dns_server']){
                    $row->dns_server->state = false;
                     $diff = true;
                }
                
                if($profile_a['rate_limit_string'] != $profile_b['rate_limit_string']){
                    $row->rate_limit_string->state = false;
                     $diff = true;
                }
                
                if($profile_a['queue_type'] != $profile_b['queue_type']){
                    $row->queue_type->state = false;
                    $diff = true;
                }
              
                if( $diff){
                    $tableB[] = $row;
                }
            }
            
        }
        
        $this->updateFile($tableA,'profilesA', 'mikrotik_pppoe_ta');
        $this->updateFile($tableB,'profilesB', 'mikrotik_pppoe_ta');
        
    } 
    
    protected function clearProfiles(){

        $this->updateFile([],'profilesA', 'mikrotik_pppoe_ta');
        $this->updateFile([],'profilesB', 'mikrotik_pppoe_ta');
    } 
    
    
    
    
     // sync address list

    public function syncAddressLists()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $ok = true;
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            $address_lists_ids = $this->request->input('json_decode')->address_lists_ids;
            $delete_address_list_side_b = $this->request->input('json_decode')->delete_address_lists_side_b;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
                
            $fileB = new File(WWW_ROOT . 'sync/mikrotik_pppoe_ta/addressListsB.json', false, 0644);
            $json = $fileB->read($fileB, 'r');
            $fileB->close();
            $json =  json_decode($json);
            
            
            foreach($json->data as $row){
                
                if($row->marked_to_delete){
                    
                    if(!$row->other){ //son registro del controlador, que deben eliminarse,            
	                    
	                    if(!$this->IntegrationRouter->MikrotikPppoeTa->deleteAddressListInController($controller, $row->api_id->value)){
                            $ok = false;
                            break;
                        }
                    	
                    }else if($row->other && $delete_address_list_side_b){ //son registros agenos al controlador, y esta marcado para limpieza
                    	
                    	if(!$this->IntegrationRouter->MikrotikPppoeTa->deleteAddressListInController($controller, $row->api_id->value)){
                            $ok = false;
                            break;
                        }
                    }
                }
            }
   
   
            if($ok){
                
                foreach($address_lists_ids as $address_list_id){
                    
                    if(!$this->IntegrationRouter->MikrotikPppoeTa->syncAddressList($address_list_id)){
                        $ok = false;
                        break;
                    }
                }
                
            }
            
            $this->diffAddressLists($controller);

            $this->set('response', $ok);
        }
    }
    
    public function syncAddressListIndi()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $ok = true;
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            $address_lists_ids = $this->request->input('json_decode')->address_lists_ids;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
   
            foreach($address_lists_ids as $address_list_id){
                
                if(!$this->IntegrationRouter->MikrotikPppoeTa->syncAddressList($address_list_id)){
                    $ok = false;
                    break;
                }
            }            
          
            $this->diffAddressLists($controller);

            $this->set('response', $ok);
        }
    }
  
    public function refreshAddressLists(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $ok = false;
            
            if($this->IntegrationRouter->MikrotikPppoeTa->getStatus($controller)){
  
                $ok = true;
            }
            
            if($ok){
                $this->diffAddressLists($controller);
            }
            
            $this->set('data', $ok);
        }
        
    }
 
    public function deleteAddressListInController(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
            $address_list_api_id = $this->request->input('json_decode')->address_list_api_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $success = true;
          
            if(!$this->IntegrationRouter->MikrotikPppoeTa->deleteAddressListInController($controller, $address_list_api_id->value)){
                 $success = false;
            }
            
            $this->diffAddressLists($controller);
            
            $this->set('data', $success);
        }
        
    }
    
    protected function diffAddressLists($controller, $file = true){
         
        if($file){
             $this->clearAddressLists();            
        }

        $addressListsA = $this->IntegrationRouter->MikrotikPppoeTa->getAddressListsArray($controller->id);
        
        $addressListsB = $this->IntegrationRouter->MikrotikPppoeTa->getAddressListsInController($controller);
        
        $tableA = [];
        $tableB = [];
        
        $count = 0;
        
        foreach ($addressListsA as $address_list_a) {
            
            $row = new RowAddressList($address_list_a, 'A');
             
            //existe en A pero no en B
            if(!array_key_exists($address_list_a['list'].':'.$address_list_a['address'], $addressListsB)){
                $tableA[] = $row;
                $count++;
            }else{
                
                $diff = false;
                
                $address_list_b = $addressListsB[$address_list_a['list'].':'.$address_list_a['address']];
                
                if(strtoupper($address_list_a['api_id']) != strtoupper($address_list_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($address_list_a['list'] != $address_list_b['list']){
                    $row->list->state = false;
                     $diff = true;
                }
                
                if($address_list_a['address'] != $address_list_b['address']){
                    $row->address->state = false;
                     $diff = true;
                }
                
                if($address_list_a['comment'] != $address_list_b['comment']){
                    $row->comment->state = false;
                    $diff = true;
                }
              
                if( $diff){
                    $tableA[] = $row;
                    $count++;
                }
                
            }
        }
        
        foreach ($addressListsB as $address_list_b) {
            
            $row = new RowAddressList($address_list_b, 'B');
            
            if($address_list_b['other'] == true){
            	$row->setNew();
            	$tableB[] = $row;
            	continue;
            }
            
            //existe en B pero no en A
            if(!array_key_exists($address_list_b['list'].':'.$address_list_b['address'], $addressListsA)){
                $row->setNew();
                $tableB[] = $row;
                $count++;
                
            }else{
                
                $diff = false;
                
                $address_list_a = $addressListsA[$address_list_b['list'].':'.$address_list_b['address']];
            
                if(strtoupper($address_list_a['api_id']) != strtoupper($address_list_b['api_id'])){
                    $row->setNew();
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($address_list_a['list'] != $address_list_b['list']){
                    $row->list->state = false;
                    $diff = true;
                }
                
                if($address_list_a['address'] != $address_list_b['address']){
                    $row->address->state = false;
                    $diff = true;
                }
                
                if($address_list_a['comment'] != $address_list_b['comment']){
                    $row->comment->state = false;
                     $diff = true;
                }
               
              
                if( $diff){
                    $tableB[] = $row;
                    $count++;
                }
            }
            
        }
        
        if($file){
             
            $this->updateFile($tableA,'addressListsA', 'mikrotik_pppoe_ta');
            $this->updateFile($tableB,'addressListsB', 'mikrotik_pppoe_ta');
        }
        
        return $count;
    } 
    
    protected function clearAddressLists(){
        
        $this->updateFile([],'addressListsA', 'mikrotik_pppoe_ta');
        $this->updateFile([],'addressListsB', 'mikrotik_pppoe_ta');
    } 
    
    
    
    
    //sync pppoe-servers 
    
    public function syncPppoeServers()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {             
            $ok = true;
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            $ids = $this->request->input('json_decode')->ids;
            $delete_pppoeserver_side_b = $this->request->input('json_decode')->delete_pppoeserver_side_b;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
            
            $fileB = new File(WWW_ROOT . 'sync/mikrotik_pppoe_ta/pppoeServersB.json', false, 0644);
            $json = $fileB->read($fileB, 'r');
            $fileB->close();
            $json =  json_decode($json);
            
            
            foreach($json->data as $row){
                
                if($row->marked_to_delete){
                    
                    if(!$row->other){ //son registro del controlador, que deben eliminarse,            
                    	
                    	if(!$this->IntegrationRouter->MikrotikPppoeTa->deletePppoeServerInController($controller, $row->api_id->value)){
                            $ok = false;
                            break;
                        }
                    	
                    }else if($row->other && $delete_pppoeserver_side_b){ //son registros agenos al controlador, y esta marcado para limpieza
                    	
                    	if(!$this->IntegrationRouter->MikrotikPppoeTa->deletePppoeServerInController($controller, $row->api_id->value)){
                            $ok = false;
                            break;
                        }
                    }
                }
            }
        
   
            if($ok){
                
                foreach($ids as $id){
                    
                    if(!$this->IntegrationRouter->MikrotikPppoeTa->syncPppoeServer($id)){
                        $ok = false;
                        break;
                    }
                }
                
            }
            
            $this->diffPppoeServers($controller);

            $this->set('response', $ok);
        }
    }
    
    public function syncPppoeServerIndi()  {
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $ok = true;
            
            $controller_id = $this->request->input('json_decode')->controller_id;
            $ids = $this->request->input('json_decode')->ids;
            
            $this->loadModel('Controllers');
            $controller = $this->Controllers->get($controller_id);
            
            foreach($ids as $id){
                
                if(!$this->IntegrationRouter->MikrotikPppoeTa->syncPppoeServer($id)){
                    $ok = false;
                    break;
                }
            }            
     
            $this->diffPppoeServers($controller);

            $this->set('response', $ok);
        }
    }
    
    public function refreshPppoeServers(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            
            $ok = false;
            
            if($this->IntegrationRouter->MikrotikPppoeTa->getStatus($controller)){
  
                $ok = true;
            }
            
            if($ok){
                $this->diffPppoeServers($controller);
            }
            
            $this->set('data', $ok);
        }
        
    }
    
    protected function diffPppoeServers($controller){
         
        $this->clearPppoeServers();

        $pppoeServersA = $this->IntegrationRouter->MikrotikPppoeTa->getPppoeServersArray($controller->id);
        $pppoeServersB = $this->IntegrationRouter->MikrotikPppoeTa->getPppoeServersInController($controller);

        $tableA = [];
        $tableB = [];
        
        foreach ($pppoeServersA as $pppoeserver_a) {
            
            $row = new RowPppoeServer($pppoeserver_a, 'A');
             
            //existe en A pero no en B
            if(!array_key_exists($pppoeserver_a['service_name'], $pppoeServersB)){
                $tableA[] = $row;
            }else{
                
                $diff = false;
                
                $pppoeserver_b = $pppoeServersB[$pppoeserver_a['service_name']];
                
                if(strtoupper($pppoeserver_a['api_id']) != strtoupper($pppoeserver_b['api_id'])){
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($pppoeserver_a['service_name'] != $pppoeserver_b['service_name']){
                    $row->service_name->state = false;
                     $diff = true;
                }
                
                if($pppoeserver_a['interface'] != $pppoeserver_b['interface']){
                    $row->interface->state = false;
                    $diff = true;
                }
                
                if($pppoeserver_a['disabled'] != $pppoeserver_b['disabled']){
                    $row->disabled->state = false;
                    $diff = true;
                }
                if( $diff){
                    $tableA[] = $row;
                }
                
            }
        }
      
        foreach ($pppoeServersB as $pppoeserver_b) {
            
            $row = new RowPppoeServer($pppoeserver_b, 'B');
            
            if($pppoeserver_b['other'] == true){
            	$row->setNew();
            	$tableB[] = $row;
            	continue;
            }
            
            //existe en B pero no en A
            if(!array_key_exists($pppoeserver_b['service_name'], $pppoeServersA)){
                $row->setNew();
                $tableB[] = $row;
                
            }else{
                
                $diff = false;
                
                $pppoeserver_a = $pppoeServersA[$pppoeserver_b['service_name']];
            
                if(strtoupper($pppoeserver_a['api_id']) != strtoupper($pppoeserver_b['api_id'])){
                    $row->setNew();
                    $row->api_id->state = false;
                    $diff = true;
                }
                
                if($pppoeserver_a['service_name'] != $pppoeserver_b['service_name']){
                    $row->service_name->state = false;
                     $diff = true;
                }
                
                if($pppoeserver_a['interface'] != $pppoeserver_b['interface']){
                    $row->interface->state = false;
                    $diff = true;
                }
               
               
                if($pppoeserver_a['disabled'] != $pppoeserver_b['disabled']){
                    $row->disabled->state = false;
                    $diff = true;
                }
              
                if( $diff){
                    $tableB[] = $row;
                }
            }
            
        }
        
        $this->updateFile($tableA,'pppoeServersA', 'mikrotik_pppoe_ta');
        $this->updateFile($tableB,'pppoeServersB', 'mikrotik_pppoe_ta');
        
    } 
    
    public function deletePppoeServerInController(){
        
        if ($this->request->is('Ajax')) //Ajax Detection
        {
            $controller_id = $this->request->input('json_decode')->controller_id;
            $pppoeserver_api_id = $this->request->input('json_decode')->pppoeserver_api_id;
        
            $this->loadModel('Controllers');
    
            $controller = $this->Controllers->get($controller_id, [
                'contain' => []
            ]);
            
            $success = true;
          
            if(!$this->IntegrationRouter->MikrotikPppoeTa->deletePppoeServerInController($controller, $pppoeserver_api_id->value)){
                 $success = false;
            }
             
            $this->diffPppoeServers($controller);
            
            $this->set('response', $success);
        }
        
    }
    
    protected function clearPppoeServers(){
        
        $this->updateFile([],'pppoeServersA', 'mikrotik_pppoe_ta');
        $this->updateFile([],'pppoeServersB', 'mikrotik_pppoe_ta');
    }       
   
    
   
    
}







