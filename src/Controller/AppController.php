<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

use Cake\I18n\Number;
use Cake\I18n\Time;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    public $_general_paraments = null;

    public function initialize()
    {
        parent::initialize();

        $file = new File(CONFIG . 'conff.json', false, 0644);
        $json = $file->read(true, 'r');
        $conff = json_decode($json);

        // Condicional corte por falta de pagos
        if ($conff->status == 'co') {
            $this->render('/Blocked/block');
        }

        // Condicional bloqueo sin conexion
        if ($conff->status == 'sc') {
            $this->render('/Blocked/index');
        }

        // Condicional bloqueo período de prueba finalizado
        if ($conff->status == 'pf') {
            $this->render('/Blocked/end');
        }

        $session = $this->request->getSession();
        $session->write('conff', $conff);

        $this->loadComponent('RequestHandler', [
            'viewClassMap' => [
                'xlsx' => 'CakeExcel.Excel'
            ]
        ]);

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'username',
                        'password' => 'password'
                    ]
                ]
            ],
            'authError' => 'No posee los privilegios necesarios.',

            'authorize' => 'Controller',
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'storage' => 'Session'
        ]);
        $this->loadComponent('Cookie');

        $this->loadResources();

        if ($this->request->is('mobile')) {
          $this->viewBuilder()->setLayout('layoutMainMobile');
        } else {
            $this->viewBuilder()->setLayout('layoutMain');
        }

        // CONTROL DE SEGURIDAD SI SE QUIERE ACCEDER A UN CONTROLADOR DIFERENTE DE PORTAL Y TYPO DE USUARIO DIFERENTE DE SISTEMA QUE VUELE.
        // if ($this->request->controller != 'Portal' && $this->request->getSession()->read('type') != 'sistema' ) {
        //     if ($this->Auth) {
        //         $this->Auth->logout();
        //     }
        // } else if ($this->request->controller == 'Portal') {
        //     $this->viewBuilder()->setLayout('layoutPortal');
        // }
    }

    protected function getCustomersIdsBusinessDisabled()
    {
         $bussiones_ids_disabled =  $this->getBusinessDisabled();

         if (count($bussiones_ids_disabled) > 0) {

            $this->LoadModel('Customers');
            return  $this->Customers->find('list', [
                'keyField'   => 'code',
                'valueField' => 'code'
            ])->select(['code'])->where(['business_billing IN' => $bussiones_ids_disabled])->toArray();
         }

         return [];
    }

    protected function getBusinessDisabled()
    {
        $paraments = $this->request->getSession()->read('paraments');

        $bussiones_ids_disabled = [];

        foreach ($paraments->invoicing->business as $busines) {
            if (!$busines->enable) {
                $bussiones_ids_disabled[] = $busines->id;
            }
        }

        return $bussiones_ids_disabled;
    }

    private function merge_paraments($paraments_default, $paraments_customer) 
    {
        if ($paraments_customer) {

            if (property_exists($paraments_customer, 'system')) {

                if (property_exists($paraments_customer->system, 'integration')) {
                    $paraments_default->system->integration = $paraments_customer->system->integration;
                }
                if (property_exists($paraments_customer->system, 'map')) {
                    if (property_exists($paraments_customer->system->map, 'lng')) {
                        $paraments_default->system->map->lng = $paraments_customer->system->map->lng;
                    }
                    if (property_exists($paraments_customer->system->map, 'lat')) {
                        $paraments_default->system->map->lat = $paraments_customer->system->map->lat;
                    }
                    if (property_exists($paraments_customer->system->map, 'zoom')) {
                        $paraments_default->system->map->zoom = $paraments_customer->system->map->zoom;
                    }
                }
                if (property_exists($paraments_customer->system, 'server')) {
                    if (property_exists($paraments_customer->system->server, 'ip')) {
                        $paraments_default->system->server->ip = $paraments_customer->system->server->ip;
                    }
                }
                if (property_exists($paraments_customer->system, 'mode_migration')) {
                    $paraments_default->system->mode_migration = $paraments_customer->system->mode_migration;
                }
                if (property_exists($paraments_customer->system, 'show_error_log')) {
                    $paraments_default->system->show_error_log = $paraments_customer->system->show_error_log;
                }
                
                if (property_exists($paraments_customer->system, 'uri_http')) {
                    $paraments_default->system->uri_http = $paraments_customer->system->uri_http;
                }

            } //end system

            if (property_exists($paraments_customer, 'gral_config')) {

                if (property_exists($paraments_customer->gral_config, 'billing_for_service')) {
                    $paraments_default->gral_config->billing_for_service = $paraments_customer->gral_config->billing_for_service;
                }
                
                if (property_exists($paraments_customer->gral_config, 'value_service_customer')) {
                    $paraments_default->gral_config->value_service_customer = $paraments_customer->gral_config->value_service_customer;
                }

                if (property_exists($paraments_customer->gral_config, 'using_privilegios')) {
                    $paraments_default->gral_config->using_privilegios = $paraments_customer->gral_config->using_privilegios;
                }

                if (property_exists($paraments_customer->gral_config, 'using_avisos')) {
                    $paraments_default->gral_config->using_avisos = $paraments_customer->gral_config->using_avisos;
                }

                if (property_exists($paraments_customer->gral_config, 'enterprise_name')) {
                    $paraments_default->gral_config->enterprise_name = $paraments_customer->gral_config->enterprise_name;
                }

                if (property_exists($paraments_customer->gral_config, 'using_chat')) {
                    $paraments_default->gral_config->using_chat = $paraments_customer->gral_config->using_chat;
                }

                if (property_exists($paraments_customer->gral_config, 'telegram_group_link')) {
                    $paraments_default->gral_config->telegram_group_link = $paraments_customer->gral_config->telegram_group_link;
                }

            } //end gral_config

            if (property_exists($paraments_customer, 'customer')) {
                if (property_exists($paraments_customer->customer, 'doc_validate')) {
                    $paraments_default->customer->doc_validate = $paraments_customer->customer->doc_validate;
                }
                if (property_exists($paraments_customer->customer, 'email_validate')) {
                    $paraments_default->customer->email_validate = $paraments_customer->customer->email_validate;
                }
                if (property_exists($paraments_customer->customer, 'search_list_customers')) {
                    $paraments_default->customer->search_list_customers = $paraments_customer->customer->search_list_customers;
                }
                if (property_exists($paraments_customer->customer, 'doc_type')) {
                    $paraments_default->customer->doc_type = $paraments_customer->customer->doc_type;
                }
                if (property_exists($paraments_customer->customer, 'responsible')) {
                    $paraments_default->customer->responsible = $paraments_customer->customer->responsible;
                }
                if (property_exists($paraments_customer->customer, 'cond_venta')) {
                    $paraments_default->customer->cond_venta = $paraments_customer->customer->cond_venta;
                }
                if (property_exists($paraments_customer->customer, 'billing_for_service')) {
                    $paraments_default->customer->billing_for_service = $paraments_customer->customer->billing_for_service;
                }
                if (property_exists($paraments_customer->customer, 'denomination')) {
                    $paraments_default->customer->denomination = $paraments_customer->customer->denomination;
                }
                if (property_exists($paraments_customer->customer, 'invoice_separated_default')) {
                    $paraments_default->customer->invoice_separated_default = $paraments_customer->customer->invoice_separated_default;
                }
                if (property_exists($paraments_customer->customer, 'country_default')) {
                    $paraments_default->customer->country_default = $paraments_customer->customer->country_default;
                }
                if (property_exists($paraments_customer->customer, 'province_default')) {
                    $paraments_default->customer->province_default = $paraments_customer->customer->province_default;
                }
                if (property_exists($paraments_customer->customer, 'city_default')) {
                    $paraments_default->customer->city_default = $paraments_customer->customer->city_default;
                }
                if (property_exists($paraments_customer->customer, 'payment_commitment')) {
                    $paraments_default->customer->payment_commitment = $paraments_customer->customer->payment_commitment;
                }
            } //end customer

            if (property_exists($paraments_customer, 'connection')) {

                if (property_exists($paraments_customer->connection, 'pppoe_credential')) {
                    $paraments_default->connection->pppoe_credential = $paraments_customer->connection->pppoe_credential;
                }

                if (property_exists($paraments_customer->connection, 'pool_plan')) {
                    $paraments_default->connection->pool_plan = $paraments_customer->connection->pool_plan;
                }

                if (property_exists($paraments_customer->connection, 'accounting_traffic')) {
                    if (property_exists($paraments_customer->connection->accounting_traffic, 'enabled')) {
                        $paraments_default->connection->accounting_traffic->enabled = $paraments_customer->connection->accounting_traffic->enabled;
                    }
                    if (property_exists($paraments_customer->connection->accounting_traffic, 'delay')) {
                        $paraments_default->connection->accounting_traffic->delay = $paraments_customer->connection->accounting_traffic->delay;
                    }
                    if (property_exists($paraments_customer->connection->accounting_traffic, 'threshold')) {
                        $paraments_default->connection->accounting_traffic->threshold = $paraments_customer->connection->accounting_traffic->threshold;
                    }
                }
            } //end connection

            if (property_exists($paraments_customer, 'invoicing')) {

                if (property_exists($paraments_customer->invoicing, 'is_presupuesto')) {
                    $paraments_default->invoicing->is_presupuesto = $paraments_customer->invoicing->is_presupuesto;
                }

                if (property_exists($paraments_customer->invoicing, 'business')) {
                    $paraments_default->invoicing->business = $paraments_customer->invoicing->business;
                }

                if (property_exists($paraments_customer->invoicing, 'print_duplicate_receipt')) {
                    $paraments_default->invoicing->print_duplicate_receipt = $paraments_customer->invoicing->print_duplicate_receipt;
                }

                if (property_exists($paraments_customer->invoicing, 'auto_print')) {
                    $paraments_default->invoicing->auto_print = $paraments_customer->invoicing->auto_print;
                }

                if (property_exists($paraments_customer->invoicing, 'resume_receipt')) {
                    $paraments_default->invoicing->resume_receipt = $paraments_customer->invoicing->resume_receipt;
                }

                if (property_exists($paraments_customer->invoicing, 'business_billing_default')) {
                    $paraments_default->invoicing->business_billing_default = $paraments_customer->invoicing->business_billing_default;
                }

                if (property_exists($paraments_customer->invoicing, 'observations_receipt')) {
                    $paraments_default->invoicing->observations_receipt = $paraments_customer->invoicing->observations_receipt;
                }

                if (property_exists($paraments_customer->invoicing, 'account_summary_complete')) {
                    $paraments_default->invoicing->account_summary_complete = $paraments_customer->invoicing->account_summary_complete;
                }

                if (property_exists($paraments_customer->invoicing, 'email_emplate_receipt')) {
                    $paraments_default->invoicing->email_emplate_receipt = $paraments_customer->invoicing->email_emplate_receipt;
                }

                if (property_exists($paraments_customer->invoicing, 'add_cobrodigital_account_summary')) {
                    $paraments_default->invoicing->add_cobrodigital_account_summary = $paraments_customer->invoicing->add_cobrodigital_account_summary;
                }

                if (property_exists($paraments_customer->invoicing, 'add_payu_account_summary')) {
                    $paraments_default->invoicing->add_payu_account_summary = $paraments_customer->invoicing->add_payu_account_summary;
                }

                if (property_exists($paraments_customer->invoicing, 'add_cuentadigital_account_summary')) {
                    $paraments_default->invoicing->add_cuentadigital_account_summary = $paraments_customer->invoicing->add_cuentadigital_account_summary;
                }

                if (property_exists($paraments_customer->invoicing, 'connection_auto_selected_cobranza')) {
                    $paraments_default->invoicing->connection_auto_selected_cobranza = $paraments_customer->invoicing->connection_auto_selected_cobranza;
                }

                if (property_exists($paraments_customer->invoicing, 'email_emplate_invoice')) {
                    $paraments_default->invoicing->email_emplate_invoice = $paraments_customer->invoicing->email_emplate_invoice;
                }

                if (property_exists($paraments_customer->invoicing, 'show_comprobante_from_facturador_manual_after_generation')) {
                    $paraments_default->invoicing->show_comprobante_from_facturador_manual_after_generation = $paraments_customer->invoicing->show_comprobante_from_facturador_manual_after_generation;
                }

                if (property_exists($paraments_customer->invoicing, 'generate_ticket')) {
                    $paraments_default->invoicing->generate_ticket = $paraments_customer->invoicing->generate_ticket;
                }

                if (property_exists($paraments_customer->invoicing, 'use_cash_for_payment_anulate')) {
                    $paraments_default->invoicing->use_cash_for_payment_anulate = $paraments_customer->invoicing->use_cash_for_payment_anulate;
                }

            } //end invoicing

            if (property_exists($paraments_customer, 'presale')) {
                if (property_exists($paraments_customer->presale, 'range_hours_installation')) {
                    $paraments_default->presale->range_hours_installation = $paraments_customer->presale->range_hours_installation;
                }
            } //end presale

            if (property_exists($paraments_customer, 'accountant')) {

                if (property_exists($paraments_customer->accountant, 'account_enabled')) {
                    $paraments_default->accountant->account_enabled = $paraments_customer->accountant->account_enabled;
                }

                if (property_exists($paraments_customer->accountant, 'type')) {
                    $paraments_default->accountant->type = $paraments_customer->accountant->type;
                }
                if (property_exists($paraments_customer->accountant, 'daydue')) {
                    $paraments_default->accountant->daydue = $paraments_customer->accountant->daydue;
                }
                if (property_exists($paraments_customer->accountant, 'service_debt')) {
                    $paraments_default->accountant->service_debt = $paraments_customer->accountant->service_debt;
                }
                if (property_exists($paraments_customer->accountant, 'service_debt_change')) {
                    $paraments_default->accountant->service_debt_change = $paraments_customer->accountant->service_debt_change;
                }

                if (property_exists($paraments_customer->accountant, 'acounts_parent')) {
                    if (property_exists($paraments_customer->accountant->acounts_parent, 'cashs')) {
                        $paraments_default->accountant->acounts_parent->cashs = $paraments_customer->accountant->acounts_parent->cashs;
                    }
                    if (property_exists($paraments_customer->accountant->acounts_parent, 'customers')) {
                        $paraments_default->accountant->acounts_parent->customers = $paraments_customer->accountant->acounts_parent->customers;
                    }
                    if (property_exists($paraments_customer->accountant->acounts_parent, 'services')) {
                        $paraments_default->accountant->acounts_parent->services = $paraments_customer->accountant->acounts_parent->services;
                    }
                    if (property_exists($paraments_customer->accountant->acounts_parent, 'packages')) {
                        $paraments_default->accountant->acounts_parent->packages = $paraments_customer->accountant->acounts_parent->packages;
                    }
                    if (property_exists($paraments_customer->accountant->acounts_parent, 'products')) {
                        $paraments_default->accountant->acounts_parent->products = $paraments_customer->accountant->acounts_parent->products;
                    }
                    if (property_exists($paraments_customer->accountant->acounts_parent, 'surcharge')) {
                        $paraments_default->accountant->acounts_parent->surcharge = $paraments_customer->accountant->acounts_parent->surcharge;
                    }
                    if (property_exists($paraments_customer->accountant->acounts_parent, 'bonus')) {
                        $paraments_default->accountant->acounts_parent->bonus = $paraments_customer->accountant->acounts_parent->bonus;
                    }
                    if (property_exists($paraments_customer->accountant->acounts_parent, 'services_free')) {
                        $paraments_default->accountant->acounts_parent->services_free = $paraments_customer->accountant->acounts_parent->services_free;
                    }
                    if (property_exists($paraments_customer->accountant->acounts_parent, 'paid_to_account')) {
                        $paraments_default->accountant->acounts_parent->paid_to_account = $paraments_customer->accountant->acounts_parent->paid_to_account;
                    }
                    if (property_exists($paraments_customer->accountant->acounts_parent, 'cobrodigital')) {
                        $paraments_default->accountant->acounts_parent->cobrodigital = $paraments_customer->accountant->acounts_parent->cobrodigital;
                    }
                    if (property_exists($paraments_customer->accountant->acounts_parent, 'todopago')) {
                        $paraments_default->accountant->acounts_parent->todopago = $paraments_customer->accountant->acounts_parent->todopago;
                    }
                    if (property_exists($paraments_customer->accountant->acounts_parent, 'mercadopago')) {
                        $paraments_default->accountant->acounts_parent->mercadopago = $paraments_customer->accountant->acounts_parent->mercadopago;
                    }
                }

                if (property_exists($paraments_customer->accountant, 'duedates')) {
                    if (property_exists($paraments_customer->accountant->duedates, 'enabled')) {
                        $paraments_default->accountant->duedates->enabled = $paraments_customer->accountant->duedates->enabled;
                    }
                    if (property_exists($paraments_customer->accountant->duedates, 'aliquot')) {
                        $paraments_default->accountant->duedates->aliquot = $paraments_customer->accountant->duedates->aliquot;
                    }
                    if (property_exists($paraments_customer->accountant->duedates, 'type')) {
                        $paraments_default->accountant->duedates->type = $paraments_customer->accountant->duedates->type;
                    }
                    if (property_exists($paraments_customer->accountant->duedates, 'due')) {
                        if (property_exists($paraments_customer->accountant->duedates->due, 'surcharge')) {
                            $paraments_default->accountant->duedates->due->surcharge = $paraments_customer->accountant->duedates->due->surcharge;
                        }
                    }
                    if (property_exists($paraments_customer->accountant->duedates, 'due_1')) {
                        if (property_exists($paraments_customer->accountant->duedates->due_1, 'day')) {
                            $paraments_default->accountant->duedates->due_1->day = $paraments_customer->accountant->duedates->due_1->day;
                        }
                        if(property_exists($paraments_customer->accountant->duedates->due_1, 'surcharge')) {
                            $paraments_default->accountant->duedates->due_1->surcharge = $paraments_customer->accountant->duedates->due_1->surcharge;
                        }
                    }
                    if (property_exists($paraments_customer->accountant->duedates, 'due_2')) {
                        if (property_exists($paraments_customer->accountant->duedates->due_2, 'day')) {
                            $paraments_default->accountant->duedates->due_2->day = $paraments_customer->accountant->duedates->due_2->day;
                        }
                        if (property_exists($paraments_customer->accountant->duedates->due_2, 'surcharge')) {
                            $paraments_default->accountant->duedates->due_2->surcharge = $paraments_customer->accountant->duedates->due_2->surcharge;
                        }
                    }
                    if (property_exists($paraments_customer->accountant->duedates, 'due_3')) {
                        if (property_exists($paraments_customer->accountant->duedates->due_3, 'day')) {
                            $paraments_default->accountant->duedates->due_3->day = $paraments_customer->accountant->duedates->due_3->day;
                        }
                        if (property_exists($paraments_customer->accountant->duedates->due_3, 'surcharge')) {
                            $paraments_default->accountant->duedates->due_3->surcharge = $paraments_customer->accountant->duedates->due_3->surcharge;
                        }
                    }
                }

                if (property_exists($paraments_customer->accountant, 'dues')) {
                    $paraments_default->accountant->dues = $paraments_customer->accountant->dues;
                }

            } //end accountant

            //automatic_connection_blocking

            if (property_exists($paraments_customer, 'automatic_connection_blocking')) {
                if (property_exists($paraments_customer->automatic_connection_blocking, 'enabled')) {
                    $paraments_default->automatic_connection_blocking->enabled = $paraments_customer->automatic_connection_blocking->enabled;
                }
                if (property_exists($paraments_customer->automatic_connection_blocking, 'force')) {
                    $paraments_default->automatic_connection_blocking->force = $paraments_customer->automatic_connection_blocking->force;
                }
                if (property_exists($paraments_customer->automatic_connection_blocking, 'day')) {
                    $paraments_default->automatic_connection_blocking->day = $paraments_customer->automatic_connection_blocking->day;
                }
                if (property_exists($paraments_customer->automatic_connection_blocking, 'hours_execution')) {
                    $paraments_default->automatic_connection_blocking->hours_execution = $paraments_customer->automatic_connection_blocking->hours_execution;
                }
                if (property_exists($paraments_customer->automatic_connection_blocking, 'due_debt')) {
                    $paraments_default->automatic_connection_blocking->due_debt = $paraments_customer->automatic_connection_blocking->due_debt;
                }
                if (property_exists($paraments_customer->automatic_connection_blocking, 'invoice_no_paid')) {
                    $paraments_default->automatic_connection_blocking->invoice_no_paid = $paraments_customer->automatic_connection_blocking->invoice_no_paid;
                }
                if (property_exists($paraments_customer->automatic_connection_blocking, 'type')) {
                    $paraments_default->automatic_connection_blocking->type = $paraments_customer->automatic_connection_blocking->type;
                }

                if (property_exists($paraments_customer->automatic_connection_blocking, 'surcharge_enabled')) {
                    $paraments_default->automatic_connection_blocking->surcharge_enabled = $paraments_customer->automatic_connection_blocking->surcharge_enabled;
                }
                if (property_exists($paraments_customer->automatic_connection_blocking, 'surcharge_value')) {
                    $paraments_default->automatic_connection_blocking->surcharge_value = $paraments_customer->automatic_connection_blocking->surcharge_value;
                }
                if (property_exists($paraments_customer->automatic_connection_blocking, 'surcharge_concept')) {
                    $paraments_default->automatic_connection_blocking->surcharge_concept = $paraments_customer->automatic_connection_blocking->surcharge_concept;
                }
                if (property_exists($paraments_customer->automatic_connection_blocking, 'surcharge_type')) {
                    $paraments_default->automatic_connection_blocking->surcharge_type = $paraments_customer->automatic_connection_blocking->surcharge_type;
                }
                if (property_exists($paraments_customer->automatic_connection_blocking, 'surcharge_presupuesto')) {
                    $paraments_default->automatic_connection_blocking->surcharge_presupuesto = $paraments_customer->automatic_connection_blocking->surcharge_presupuesto;
                }
            }

            if (property_exists($paraments_customer, 'backup')) {
                if (property_exists($paraments_customer->backup, 'enabled')) {
                    $paraments_default->backup->enabled = $paraments_customer->backup->enabled;
                }
                if (property_exists($paraments_customer->backup, 'hours_execution')) {
                    $paraments_default->backup->hours_execution = $paraments_customer->backup->hours_execution;
                }
                if (property_exists($paraments_customer->backup, 'folder_name')) {
                    $paraments_default->backup->folder_name = $paraments_customer->backup->folder_name;
                }
                if (property_exists($paraments_customer->backup, 'amount_files')) {
                    $paraments_default->backup->amount_files = $paraments_customer->backup->amount_files;
                }
                if (property_exists($paraments_customer->backup, 'app_key')) {
                    $paraments_default->backup->app_key = $paraments_customer->backup->app_key;
                }
                if (property_exists($paraments_customer->backup, 'app_secret')) {
                    $paraments_default->backup->app_secret = $paraments_customer->backup->app_secret;
                }
                if (property_exists($paraments_customer->backup, 'token')) {
                    $paraments_default->backup->token = $paraments_customer->backup->token;
                }
                // if (property_exists($paraments_customer->backup, 'enabled_comprobantes')) {
                //     $paraments_default->backup->enabled_comprobantes = $paraments_customer->backup->enabled_comprobantes;
                // }

            } //end backup

            if (property_exists($paraments_customer, 'notificators')) {
                $paraments_default->notificators = $paraments_customer->notificators;
            } //end notifications

            if (property_exists($paraments_customer, 'sms')) {

                if (property_exists($paraments_customer->sms, 'default_platform')) {
                    $paraments_default->sms->default_platform = $paraments_customer->sms->default_platform;
                }

                if (property_exists($paraments_customer->sms, 'num_characters')) {
                    $paraments_default->sms->num_characters = $paraments_customer->sms->num_characters;
                }

                if (property_exists($paraments_customer->sms, 'platform')) {
                    if (property_exists($paraments_customer->sms->platform, 'smsmasivo')) {

                        if (property_exists($paraments_customer->sms->platform->smsmasivo, 'enabled')) {
                            $paraments_default->sms->platform->smsmasivo->enabled = $paraments_customer->sms->platform->smsmasivo->enabled;
                        }
                        if (property_exists($paraments_customer->sms->platform->smsmasivo, 'smsusuario')) {
                            $paraments_default->sms->platform->smsmasivo->smsusuario = $paraments_customer->sms->platform->smsmasivo->smsusuario;
                        }
                        if (property_exists($paraments_customer->sms->platform->smsmasivo, 'smsclave')) {
                            $paraments_default->sms->platform->smsmasivo->smsclave = $paraments_customer->sms->platform->smsmasivo->smsclave;
                        }
                        if (property_exists($paraments_customer->sms->platform->smsmasivo, 'test')) {
                            $paraments_default->sms->platform->smsmasivo->test = $paraments_customer->sms->platform->smsmasivo->test;
                        }
                        if (property_exists($paraments_customer->sms->platform->smsmasivo, 'fulltime')) {
                            $paraments_default->sms->platform->smsmasivo->fulltime = $paraments_customer->sms->platform->smsmasivo->fulltime;
                        }

                    } //end smsmasivo

                    if (property_exists($paraments_customer->sms->platform, 'sms_getway')) {

                        if (property_exists($paraments_customer->sms->platform->sms_getway, 'enabled')) {
                            $paraments_default->sms->platform->sms_getway->enabled = $paraments_customer->sms->platform->sms_getway->enabled;
                        }
                        if (property_exists($paraments_customer->sms->platform->sms_getway, 'device_id')) {
                            $paraments_default->sms->platform->sms_getway->device_id = $paraments_customer->sms->platform->sms_getway->device_id;
                        }
                        if (property_exists($paraments_customer->sms->platform->sms_getway, 'devices')) {
                            $paraments_default->sms->platform->sms_getway->devices = $paraments_customer->sms->platform->sms_getway->devices;
                        }
                        if (property_exists($paraments_customer->sms->platform->sms_getway, 'token')) {
                            $paraments_default->sms->platform->sms_getway->token = $paraments_customer->sms->platform->sms_getway->token;
                        }

                    } //end sms_getway
                }
            }

            if (property_exists($paraments_customer, 'ticket')) {
                if (property_exists($paraments_customer->ticket, 'save_change_start_task')) {
                    $paraments_default->ticket->save_change_start_task = $paraments_customer->ticket->save_change_start_task;
                }
            } //end ticket

            if (property_exists($paraments_customer, 'mass_emails')) {
                if (property_exists($paraments_customer->mass_emails, 'emails')) {
                    $paraments_default->mass_emails->emails = $paraments_customer->mass_emails->emails;
                }
                if (property_exists($paraments_customer->mass_emails, 'limit')) {
                    $paraments_default->mass_emails->limit = $paraments_customer->mass_emails->limit;
                }
            } //end Mass Emails

            if (property_exists($paraments_customer, 'emails_accounts')) {

                if (property_exists($paraments_customer->emails_accounts, 'provider')) {
                    $paraments_customer->emails_accounts->provider = $paraments_customer->emails_accounts->provider;
                }

                if (property_exists($paraments_customer->emails_accounts, 'providers')) {

                    if (property_exists($paraments_customer->emails_accounts->providers, 'sendgrid')) {

                        if (property_exists($paraments_customer->emails_accounts->providers->sendgrid, 'id')) {
                            $paraments_default->emails_accounts->providers->sendgrid->id = $paraments_customer->emails_accounts->providers->sendgrid->id;
                        }

                        if (property_exists($paraments_customer->emails_accounts->providers->sendgrid, 'name')) {
                            $paraments_default->emails_accounts->providers->sendgrid->name = $paraments_customer->emails_accounts->providers->sendgrid->name;
                        }

                        if (property_exists($paraments_customer->emails_accounts->providers->sendgrid, 'api_key')) {
                            $paraments_default->emails_accounts->providers->sendgrid->api_key = $paraments_customer->emails_accounts->providers->sendgrid->api_key;
                        }

                        if (property_exists($paraments_customer->emails_accounts->providers->sendgrid, 'enabled')) {
                            $paraments_default->emails_accounts->providers->sendgrid->enabled = $paraments_customer->emails_accounts->providers->sendgrid->enabled;
                        }

                        if (property_exists($paraments_customer->emails_accounts->providers->sendgrid, 'endpoint')) {
                            $paraments_default->emails_accounts->providers->sendgrid->endpoint = $paraments_customer->emails_accounts->providers->sendgrid->endpoint;
                        }
                    }
                }
                if (property_exists($paraments_customer->emails_accounts, 'provider')) {
                    $paraments_default->emails_accounts->provider = $paraments_customer->emails_accounts->provider;
                }
            } //end Emails Accounts

            if (property_exists($paraments_customer, 'telegram')) {
                if (property_exists($paraments_customer->telegram, 'token')) {
                    $paraments_default->telegram->token = $paraments_customer->telegram->token;
                }
                if (property_exists($paraments_customer->telegram, 'chat_id')) {
                    $paraments_default->telegram->chat_id = $paraments_customer->telegram->chat_id;
                }
                if (property_exists($paraments_customer->telegram, 'url')) {
                    $paraments_default->telegram->url = $paraments_customer->telegram->url;
                }
            } //end Telegram

            //Billing

            if (property_exists($paraments_customer, 'billing')) {

                if (property_exists($paraments_customer->billing, 'razon_social')) {
                    $paraments_default->billing->razon_social = $paraments_customer->billing->razon_social;
                }

                if (property_exists($paraments_customer->billing, 'province')) {
                    $paraments_default->billing->province = $paraments_customer->billing->province;
                }

                if (property_exists($paraments_customer->billing, 'localidad')) {
                    $paraments_default->billing->localidad = $paraments_customer->billing->localidad;
                }

                if (property_exists($paraments_customer->billing, 'address')) {
                    $paraments_default->billing->address = $paraments_customer->billing->address;
                }

                if (property_exists($paraments_customer->billing, 'email')) {
                    $paraments_default->billing->email = $paraments_customer->billing->email;
                }

                if (property_exists($paraments_customer->billing, 'doc_type')) {
                    $paraments_default->billing->doc_type = $paraments_customer->billing->doc_type;
                }

                if (property_exists($paraments_customer->billing, 'ident')) {
                    $paraments_default->billing->ident = $paraments_customer->billing->ident;
                }

                if (property_exists($paraments_customer->billing, 'reponsible')) {
                    $paraments_default->billing->reponsible = $paraments_customer->billing->reponsible;
                }

                if (property_exists($paraments_customer->billing, 'confirm')) {
                    $paraments_default->billing->confirm = $paraments_customer->billing->confirm;
                }

            } //end Billing

        }
        return $paraments_default;
    }

    private function merge_payment_getway($payment_getway_default, $payment_getway_customer) 
    {
        if ($payment_getway_customer) {

            if (property_exists($payment_getway_customer, 'config')) {

                if (property_exists($payment_getway_customer->config, 'efectivo')) {

                    if (property_exists($payment_getway_customer->config->efectivo, 'enabled')) {
                        $payment_getway_default->config->efectivo->enabled = $payment_getway_customer->config->efectivo->enabled;
                    }

                    if (property_exists($payment_getway_customer->config->efectivo, 'automatic')) {
                        $payment_getway_default->config->efectivo->automatic = $payment_getway_customer->config->efectivo->automatic;
                    }

                    if (property_exists($payment_getway_customer->config->efectivo, 'cash')) {
                        $payment_getway_default->config->efectivo->cash = $payment_getway_customer->config->efectivo->cash;
                    }

                    if (property_exists($payment_getway_customer->config->efectivo, 'portal')) {
                        $payment_getway_default->config->efectivo->portal = $payment_getway_customer->config->efectivo->portal;
                    }
                }

                if (property_exists($payment_getway_customer->config, 'cheque')) {

                    if (property_exists($payment_getway_customer->config->cheque, 'enabled')) {
                        $payment_getway_default->config->cheque->enabled = $payment_getway_customer->config->cheque->enabled;
                    }

                    if (property_exists($payment_getway_customer->config->cheque, 'banks')) {
                        $payment_getway_default->config->cheque->banks = $payment_getway_customer->config->cheque->banks;
                    }

                    if (property_exists($payment_getway_customer->config->cheque, 'automatic')) {
                        $payment_getway_default->config->cheque->automatic = $payment_getway_customer->config->cheque->automatic;
                    }

                    if (property_exists($payment_getway_customer->config->cheque, 'account')) {
                        $payment_getway_default->config->cheque->account = $payment_getway_customer->config->cheque->account;
                    }

                    if (property_exists($payment_getway_customer->config->cheque, 'cash')) {
                        $payment_getway_default->config->cheque->cash = $payment_getway_customer->config->cheque->cash;
                    }

                    if (property_exists($payment_getway_customer->config->cheque, 'portal')) {
                        $payment_getway_default->config->cheque->portal = $payment_getway_customer->config->cheque->portal;
                    }
                }

                if (property_exists($payment_getway_customer->config, 'transferencia')) {

                    if (property_exists($payment_getway_customer->config->transferencia, 'enabled')) {
                        $payment_getway_default->config->transferencia->enabled = $payment_getway_customer->config->transferencia->enabled;
                    }

                    if (property_exists($payment_getway_customer->config->transferencia, 'banks')) {
                        $payment_getway_default->config->transferencia->banks = $payment_getway_customer->config->transferencia->banks;
                    }
                    
                     if (property_exists($payment_getway_customer->config->transferencia, 'account')) {
                        $payment_getway_default->config->transferencia->account = $payment_getway_customer->config->transferencia->account;
                    }

                    if (property_exists($payment_getway_customer->config->transferencia, 'automatic')) {
                        $payment_getway_default->config->transferencia->automatic = $payment_getway_customer->config->transferencia->automatic;
                    }

                    if (property_exists($payment_getway_customer->config->transferencia, 'cash')) {
                        $payment_getway_default->config->transferencia->cash = $payment_getway_customer->config->transferencia->cash;
                    }

                    if (property_exists($payment_getway_customer->config->transferencia, 'portal')) {
                        $payment_getway_default->config->transferencia->portal = $payment_getway_customer->config->transferencia->portal;
                    }
                }

                if (property_exists($payment_getway_customer->config, 'cobrodigital')) {

                    if (property_exists($payment_getway_customer->config->cobrodigital, 'enabled')) {
                        $payment_getway_default->config->cobrodigital->enabled = $payment_getway_customer->config->cobrodigital->enabled;
                    }

                    if (property_exists($payment_getway_customer->config->cobrodigital, 'hours_execution')) {
                        $payment_getway_default->config->cobrodigital->hours_execution = $payment_getway_customer->config->cobrodigital->hours_execution;
                    }

                    if (property_exists($payment_getway_customer->config->cobrodigital, 'period')) {
                        $payment_getway_default->config->cobrodigital->period = $payment_getway_customer->config->cobrodigital->period;
                    }

                    if (property_exists($payment_getway_customer->config->cobrodigital, 'credentials')) {
                        $payment_getway_default->config->cobrodigital->credentials = $payment_getway_customer->config->cobrodigital->credentials;
                    }

                    if (property_exists($payment_getway_customer->config->cobrodigital, 'account')) {
                        $payment_getway_default->config->cobrodigital->account = $payment_getway_customer->config->cobrodigital->account;
                    }

                    if (property_exists($payment_getway_customer->config->cobrodigital, 'automatic')) {
                        $payment_getway_default->config->cobrodigital->automatic = $payment_getway_customer->config->cobrodigital->automatic;
                    }

                    if (property_exists($payment_getway_customer->config->cobrodigital, 'url')) {
                        $payment_getway_default->config->cobrodigital->url = $payment_getway_customer->config->cobrodigital->url;
                    }

                    if (property_exists($payment_getway_customer->config->cobrodigital, 'cash')) {
                        $payment_getway_default->config->cobrodigital->cash = $payment_getway_customer->config->cobrodigital->cash;
                    }

                    if (property_exists($payment_getway_customer->config->cobrodigital, "portal")) {
                        $payment_getway_default->config->cobrodigital->portal = $payment_getway_customer->config->cobrodigital->portal;
                    }
                }

                if (property_exists($payment_getway_customer->config, 'todopago')) {

                    if (property_exists($payment_getway_customer->config->todopago, 'enabled')) {
                        $payment_getway_default->config->todopago->enabled = $payment_getway_customer->config->todopago->enabled;
                    }

                    if (property_exists($payment_getway_customer->config->todopago, 'MODO')) {
                        $payment_getway_default->config->todopago->MODO = $payment_getway_customer->config->todopago->MODO;
                    }

                    if (property_exists($payment_getway_customer->config->todopago, 'hours_execution')) {
                        $payment_getway_default->config->todopago->hours_execution = $payment_getway_customer->config->todopago->hours_execution;
                    }

                    if (property_exists($payment_getway_customer->config->todopago, 'period')) {
                        $payment_getway_default->config->todopago->period = $payment_getway_customer->config->todopago->period;
                    }

                    if (property_exists($payment_getway_customer->config->todopago, 'account')) {
                        $payment_getway_default->config->todopago->account = $payment_getway_customer->config->todopago->account;
                    }

                    if (property_exists($payment_getway_customer->config->todopago, 'test')) {

                        if (property_exists($payment_getway_customer->config->todopago->test, 'merchant_id')) {
                            $payment_getway_default->config->todopago->test->merchant_id = $payment_getway_customer->config->todopago->test->merchant_id;
                        }

                        if (property_exists($payment_getway_customer->config->todopago->test, 'api_keys')) {
                            $payment_getway_default->config->todopago->test->api_keys = $payment_getway_customer->config->todopago->test->api_keys;
                        }
                    }

                    if (property_exists($payment_getway_customer->config->todopago, 'prod')) {

                        if (property_exists($payment_getway_customer->config->todopago->prod, 'merchant_id')) {
                            $payment_getway_default->config->todopago->prod->merchant_id = $payment_getway_customer->config->todopago->prod->merchant_id;
                        }

                        if (property_exists($payment_getway_customer->config->todopago->prod, 'api_keys')) {
                            $payment_getway_default->config->todopago->prod->api_keys = $payment_getway_customer->config->todopago->prod->api_keys;
                        }
                    }

                    if (property_exists($payment_getway_customer->config->todopago, 'URL_OK')) {
                        $payment_getway_default->config->todopago->URL_OK = $payment_getway_customer->config->todopago->URL_OK;
                    }

                    if (property_exists($payment_getway_customer->config->todopago, 'URL_ERROR')) {
                        $payment_getway_default->config->todopago->URL_ERROR = $payment_getway_customer->config->todopago->URL_ERROR;
                    }

                    if (property_exists($payment_getway_customer->config->todopago, 'automatic')) {
                        $payment_getway_default->config->todopago->automatic = $payment_getway_customer->config->todopago->automatic;
                    }

                    if (property_exists($payment_getway_customer->config->todopago, 'cash')) {
                        $payment_getway_default->config->todopago->cash = $payment_getway_customer->config->todopago->cash;
                    }

                    if (property_exists($payment_getway_customer->config->todopago, 'portal')) {
                        $payment_getway_default->config->todopago->portal = $payment_getway_customer->config->todopago->portal;
                    }

                }

                if (property_exists($payment_getway_customer->config, 'mercadopago')) {

                    if (property_exists($payment_getway_customer->config->mercadopago, 'enabled')) {
                        $payment_getway_default->config->mercadopago->enabled = $payment_getway_customer->config->mercadopago->enabled;
                    }

                    if (property_exists($payment_getway_customer->config->mercadopago, 'hours_execution')) {
                        $payment_getway_default->config->mercadopago->hours_execution = $payment_getway_customer->config->mercadopago->hours_execution;
                    }

                    if (property_exists($payment_getway_customer->config->mercadopago, 'period')) {
                        $payment_getway_default->config->mercadopago->period = $payment_getway_customer->config->mercadopago->period;
                    }

                    if (property_exists($payment_getway_customer->config->mercadopago, 'account')) {
                        $payment_getway_default->config->mercadopago->account = $payment_getway_customer->config->mercadopago->account;
                    }

                    if (property_exists($payment_getway_customer->config->mercadopago, 'mode')) {
                        $payment_getway_default->config->mercadopago->mode = $payment_getway_customer->config->mercadopago->mode;
                    }

                    if (property_exists($payment_getway_customer->config->mercadopago, 'application')) {
                        $payment_getway_default->config->mercadopago->application = $payment_getway_customer->config->mercadopago->application;
                    }

                    if (property_exists($payment_getway_customer->config->mercadopago, 'CLIENT_ID')) {
                        $payment_getway_default->config->mercadopago->CLIENT_ID = $payment_getway_customer->config->mercadopago->CLIENT_ID;
                    }

                    if (property_exists($payment_getway_customer->config->mercadopago, 'CLIENT_SECRET')) {
                        $payment_getway_default->config->mercadopago->CLIENT_SECRET = $payment_getway_customer->config->mercadopago->CLIENT_SECRET;
                    }

                    if (property_exists($payment_getway_customer->config->mercadopago, 'payment_methods')) {
                        $payment_getway_default->config->mercadopago->payment_methods = $payment_getway_customer->config->mercadopago->payment_methods;
                    }

                    if (property_exists($payment_getway_customer->config->mercadopago, 'payment_types')) {
                        $payment_getway_default->config->mercadopago->payment_types = $payment_getway_customer->config->mercadopago->payment_types;
                    }

                    if (property_exists($payment_getway_customer->config->mercadopago, 'excluded_payment_methods')) {
                        $payment_getway_default->config->mercadopago->excluded_payment_methods = $payment_getway_customer->config->mercadopago->excluded_payment_methods;
                    }

                    if (property_exists($payment_getway_customer->config->mercadopago, 'excluded_payment_types')) {
                        $payment_getway_default->config->mercadopago->excluded_payment_types = $payment_getway_customer->config->mercadopago->excluded_payment_types;
                    }

                    if (property_exists($payment_getway_customer->config->mercadopago, 'installments')) {
                        $payment_getway_default->config->mercadopago->installments = $payment_getway_customer->config->mercadopago->installments;
                    }

                    if (property_exists($payment_getway_customer->config->mercadopago, 'sandbox')) {

                        if (property_exists($payment_getway_customer->config->mercadopago->sandbox, 'public_key')) {
                            $payment_getway_default->config->mercadopago->sandbox->public_key = $payment_getway_customer->config->mercadopago->sandbox->public_key;
                        }

                        if (property_exists($payment_getway_customer->config->mercadopago->sandbox, 'access_token')) {
                            $payment_getway_default->config->mercadopago->sandbox->access_token = $payment_getway_customer->config->mercadopago->sandbox->access_token;
                        }
                    }

                    if (property_exists($payment_getway_customer->config->mercadopago, 'prod')) {

                        if (property_exists($payment_getway_customer->config->mercadopago->prod, 'public_key')) {
                            $payment_getway_default->config->mercadopago->prod->public_key = $payment_getway_customer->config->mercadopago->prod->public_key;
                        }

                        if (property_exists($payment_getway_customer->config->mercadopago->prod, 'access_token')) {
                            $payment_getway_default->config->mercadopago->prod->access_token = $payment_getway_customer->config->mercadopago->prod->access_token;
                        }
                    }

                    if (property_exists($payment_getway_customer->config->mercadopago, 'automatic')) {
                        $payment_getway_default->config->mercadopago->automatic = $payment_getway_customer->config->mercadopago->automatic;
                    }

                    if (property_exists($payment_getway_customer->config->mercadopago, 'cash')) {
                        $payment_getway_default->config->mercadopago->cash = $payment_getway_customer->config->mercadopago->cash;
                    }

                    if (property_exists($payment_getway_customer->config->mercadopago, 'portal')) {
                        $payment_getway_default->config->mercadopago->portal = $payment_getway_customer->config->mercadopago->portal;
                    }
                }

                if (property_exists($payment_getway_customer->config, 'chubut_auto_debit')) {

                    if (property_exists($payment_getway_customer->config->chubut_auto_debit, 'enabled')) {
                        $payment_getway_default->config->chubut_auto_debit->enabled = $payment_getway_customer->config->chubut_auto_debit->enabled;
                    }

                    if (property_exists($payment_getway_customer->config->chubut_auto_debit, 'automatic')) {
                        $payment_getway_default->config->chubut_auto_debit->automatic = $payment_getway_customer->config->chubut_auto_debit->automatic;
                    }

                    if (property_exists($payment_getway_customer->config->chubut_auto_debit, 'accounts')) {
                        $payment_getway_default->config->chubut_auto_debit->accounts = $payment_getway_customer->config->chubut_auto_debit->accounts;
                    }

                    if (property_exists($payment_getway_customer->config->chubut_auto_debit, 'cash')) {
                        $payment_getway_default->config->chubut_auto_debit->cash = $payment_getway_customer->config->chubut_auto_debit->cash;
                    }

                    if (property_exists($payment_getway_customer->config->chubut_auto_debit, 'sale')) {
                        $payment_getway_default->config->chubut_auto_debit->sale = $payment_getway_customer->config->chubut_auto_debit->sale;
                    }

                    if (property_exists($payment_getway_customer->config->chubut_auto_debit, 'portal')) {
                        $payment_getway_default->config->chubut_auto_debit->portal = $payment_getway_customer->config->chubut_auto_debit->portal;
                    }
                }

                if (property_exists($payment_getway_customer->config, 'payu')) {

                    if (property_exists($payment_getway_customer->config->payu, 'enabled')) {
                        $payment_getway_default->config->payu->enabled = $payment_getway_customer->config->payu->enabled;
                    }

                    if (property_exists($payment_getway_customer->config->payu, 'automatic')) {
                        $payment_getway_default->config->payu->automatic = $payment_getway_customer->config->payu->automatic;
                    }

                    if (property_exists($payment_getway_customer->config->payu, 'cash')) {
                        $payment_getway_default->config->payu->cash = $payment_getway_customer->config->payu->cash;
                    }

                    if (property_exists($payment_getway_customer->config->payu, "portal")) {
                        $payment_getway_default->config->payu->portal = $payment_getway_customer->config->payu->portal;
                    }

                    if (property_exists($payment_getway_customer->config->payu, "api_key")) {
                        $payment_getway_default->config->payu->portal = $payment_getway_customer->config->payu->portal;
                    }

                    if (property_exists($payment_getway_customer->config->payu, "api_login")) {
                        $payment_getway_default->config->payu->api_login = $payment_getway_customer->config->payu->api_login;
                    }

                    if (property_exists($payment_getway_customer->config->payu, "merchant_id")) {
                        $payment_getway_default->config->payu->merchant_id = $payment_getway_customer->config->payu->merchant_id;
                    }

                    if (property_exists($payment_getway_customer->config->payu, "language")) {
                        $payment_getway_default->config->payu->language = $payment_getway_customer->config->payu->language;
                    }

                    if (property_exists($payment_getway_customer->config->payu, "is_test")) {
                        $payment_getway_default->config->payu->is_test = $payment_getway_customer->config->payu->is_test;
                    }
                }

                if (property_exists($payment_getway_customer->config, 'debit_cards')) {

                    if (property_exists($payment_getway_customer->config->debit_cards, 'enabled')) {
                        $payment_getway_default->config->debit_cards->enabled = $payment_getway_customer->config->debit_cards->enabled;
                    }

                    if (property_exists($payment_getway_customer->config->debit_cards, 'automatic')) {
                        $payment_getway_default->config->debit_cards->automatic = $payment_getway_customer->config->debit_cards->automatic;
                    }

                    if (property_exists($payment_getway_customer->config->debit_cards, 'cash')) {
                        $payment_getway_default->config->debit_cards->cash = $payment_getway_customer->config->debit_cards->cash;
                    }

                    if (property_exists($payment_getway_customer->config->debit_cards, "portal")) {
                        $payment_getway_default->config->debit_cards->portal = $payment_getway_customer->config->debit_cards->portal;
                    }
                }

                if (property_exists($payment_getway_customer->config, 'credit_cards')) {

                    if (property_exists($payment_getway_customer->config->credit_cards, 'enabled')) {
                        $payment_getway_default->config->credit_cards->enabled = $payment_getway_customer->config->credit_cards->enabled;
                    }

                    if (property_exists($payment_getway_customer->config->credit_cards, 'automatic')) {
                        $payment_getway_default->config->credit_cards->automatic = $payment_getway_customer->config->credit_cards->automatic;
                    }

                    if (property_exists($payment_getway_customer->config->credit_cards, 'cash')) {
                        $payment_getway_default->config->credit_cards->cash = $payment_getway_customer->config->credit_cards->cash;
                    }

                    if (property_exists($payment_getway_customer->config->credit_cards, "portal")) {
                        $payment_getway_default->config->credit_cards->portal = $payment_getway_customer->config->credit_cards->portal;
                    }
                }

                if (property_exists($payment_getway_customer->config, 'pim')) {

                    if (property_exists($payment_getway_customer->config->pim, 'enabled')) {
                        $payment_getway_default->config->pim->enabled = $payment_getway_customer->config->pim->enabled;
                    }

                    if (property_exists($payment_getway_customer->config->pim, 'automatic')) {
                        $payment_getway_default->config->pim->automatic = $payment_getway_customer->config->pim->automatic;
                    }

                    if (property_exists($payment_getway_customer->config->pim, 'cash')) {
                        $payment_getway_default->config->pim->cash = $payment_getway_customer->config->pim->cash;
                    }

                    if (property_exists($payment_getway_customer->config->pim, "portal")) {
                        $payment_getway_default->config->pim->portal = $payment_getway_customer->config->pim->portal;
                    }
                }

                if (property_exists($payment_getway_customer->config, 'cuentadigital')) {

                    if (property_exists($payment_getway_customer->config->cuentadigital, 'enabled')) {
                        $payment_getway_default->config->cuentadigital->enabled = $payment_getway_customer->config->cuentadigital->enabled;
                    }

                    if (property_exists($payment_getway_customer->config->cuentadigital, 'account')) {
                        $payment_getway_default->config->cuentadigital->account = $payment_getway_customer->config->cuentadigital->account;
                    }

                    if (property_exists($payment_getway_customer->config->cuentadigital, 'automatic')) {
                        $payment_getway_default->config->cuentadigital->automatic = $payment_getway_customer->config->cuentadigital->automatic;
                    }

                    if (property_exists($payment_getway_customer->config->cuentadigital, 'cash')) {
                        $payment_getway_default->config->cuentadigital->cash = $payment_getway_customer->config->cuentadigital->cash;
                    }

                    if (property_exists($payment_getway_customer->config->cuentadigital, "portal")) {
                        $payment_getway_default->config->cuentadigital->portal = $payment_getway_customer->config->cuentadigital->portal;
                    }

                    // if (property_exists($payment_getway_customer->config->cuentadigital, "control_number")) {
                    //     $payment_getway_default->config->cuentadigital->control_number = $payment_getway_customer->config->cuentadigital->control_number;
                    // }

                    // if (property_exists($payment_getway_customer->config->cuentadigital, "account_number")) {
                    //     $payment_getway_default->config->cuentadigital->account_number = $payment_getway_customer->config->cuentadigital->account_number;
                    // }

                    if (property_exists($payment_getway_customer->config->cuentadigital, 'credentials')) {
                        $payment_getway_default->config->cuentadigital->credentials = $payment_getway_customer->config->cuentadigital->credentials;
                    }

                    if (property_exists($payment_getway_customer->config->cuentadigital, 'hours_execution')) {
                        $payment_getway_default->config->cuentadigital->hours_execution = $payment_getway_customer->config->cuentadigital->hours_execution;
                    }

                    if (property_exists($payment_getway_customer->config->cuentadigital, 'period')) {
                        $payment_getway_default->config->cuentadigital->period = $payment_getway_customer->config->cuentadigital->period;
                    }
                }

                if (property_exists($payment_getway_customer->config, 'visa_auto_debit')) {

                    if (property_exists($payment_getway_customer->config->visa_auto_debit, 'enabled')) {
                        $payment_getway_default->config->visa_auto_debit->enabled = $payment_getway_customer->config->visa_auto_debit->enabled;
                    }

                    if (property_exists($payment_getway_customer->config->visa_auto_debit, 'automatic')) {
                        $payment_getway_default->config->visa_auto_debit->automatic = $payment_getway_customer->config->visa_auto_debit->automatic;
                    }

                    if (property_exists($payment_getway_customer->config->visa_auto_debit, 'cash')) {
                        $payment_getway_default->config->visa_auto_debit->cash = $payment_getway_customer->config->visa_auto_debit->cash;
                    }

                    if (property_exists($payment_getway_customer->config->visa_auto_debit, "portal")) {
                        $payment_getway_default->config->visa_auto_debit->portal = $payment_getway_customer->config->visa_auto_debit->portal;
                    }
                }

                if (property_exists($payment_getway_customer->config, 'mastercard_auto_debit')) {

                    if (property_exists($payment_getway_customer->config->mastercard_auto_debit, 'enabled')) {
                        $payment_getway_default->config->mastercard_auto_debit->enabled = $payment_getway_customer->config->mastercard_auto_debit->enabled;
                    }

                    if (property_exists($payment_getway_customer->config->mastercard_auto_debit, 'automatic')) {
                        $payment_getway_default->config->mastercard_auto_debit->automatic = $payment_getway_customer->config->mastercard_auto_debit->automatic;
                    }

                    if (property_exists($payment_getway_customer->config->mastercard_auto_debit, 'cash')) {
                        $payment_getway_default->config->mastercard_auto_debit->cash = $payment_getway_customer->config->mastercard_auto_debit->cash;
                    }

                    if (property_exists($payment_getway_customer->config->mastercard_auto_debit, "portal")) {
                        $payment_getway_default->config->mastercard_auto_debit->portal = $payment_getway_customer->config->mastercard_auto_debit->portal;
                    }
                }

                if (property_exists($payment_getway_customer->config, 'rapipago')) {

                    if (property_exists($payment_getway_customer->config->rapipago, 'enabled')) {
                        $payment_getway_default->config->rapipago->enabled = $payment_getway_customer->config->rapipago->enabled;
                    }

                    if (property_exists($payment_getway_customer->config->rapipago, 'automatic')) {
                        $payment_getway_default->config->rapipago->automatic = $payment_getway_customer->config->rapipago->automatic;
                    }

                    if (property_exists($payment_getway_customer->config->rapipago, 'account')) {
                        $payment_getway_default->config->rapipago->account = $payment_getway_customer->config->rapipago->account;
                    }

                    if (property_exists($payment_getway_customer->config->rapipago, 'cash')) {
                        $payment_getway_default->config->rapipago->cash = $payment_getway_customer->config->rapipago->cash;
                    }

                    if (property_exists($payment_getway_customer->config->rapipago, 'portal')) {
                        $payment_getway_default->config->rapipago->portal = $payment_getway_customer->config->rapipago->portal;
                    }
                }

            } //end config

            if (property_exists($payment_getway_customer, 'methods')) {

                if (property_exists($payment_getway_customer->methods, 'efectivo')) {
                    if (property_exists($payment_getway_customer->methods->efectivo, 'credential')) {
                        $payment_getway_default->methods->efectivo->credential = $payment_getway_customer->methods->efectivo->credential;
                    }
                    if (property_exists($payment_getway_customer->methods->efectivo, 'edit')) {
                        $payment_getway_default->methods->efectivo->edit = $payment_getway_customer->methods->efectivo->edit;
                    }
                    if (property_exists($payment_getway_customer->methods->efectivo, 'auto_debit')) {
                        $payment_getway_default->methods->efectivo->auto_debit = $payment_getway_customer->methods->efectivo->auto_debit;
                    }
                    if (property_exists($payment_getway_customer->methods->efectivo, 'lbl_debt')) {
                        $payment_getway_default->methods->efectivo->lbl_debt = $payment_getway_customer->methods->efectivo->lbl_debt;
                    }
                }

                if (property_exists($payment_getway_customer->methods, 'cheque')) {
                    if (property_exists($payment_getway_customer->methods->cheque, 'credential')) {
                        $payment_getway_default->methods->cheque->credential = $payment_getway_customer->methods->cheque->credential;
                    }
                    if (property_exists($payment_getway_customer->methods->cheque, 'edit')) {
                        $payment_getway_default->methods->cheque->edit = $payment_getway_customer->methods->cheque->edit;
                    }
                    if (property_exists($payment_getway_customer->methods->cheque, 'auto_debit')) {
                        $payment_getway_default->methods->cheque->auto_debit = $payment_getway_customer->methods->cheque->auto_debit;
                    }
                    if (property_exists($payment_getway_customer->methods->cheque, 'lbl_debt')) {
                        $payment_getway_default->methods->cheque->lbl_debt = $payment_getway_customer->methods->cheque->lbl_debt;
                    }
                }

                if (property_exists($payment_getway_customer->methods, 'transferencia')) {
                    if (property_exists($payment_getway_customer->methods->transferencia, 'credential')) {
                        $payment_getway_default->methods->transferencia->credential = $payment_getway_customer->methods->transferencia->credential;
                    }
                    if (property_exists($payment_getway_customer->methods->transferencia, 'edit')) {
                        $payment_getway_default->methods->transferencia->edit = $payment_getway_customer->methods->transferencia->edit;
                    }
                    if (property_exists($payment_getway_customer->methods->transferencia, 'auto_debit')) {
                        $payment_getway_default->methods->transferencia->auto_debit = $payment_getway_customer->methods->transferencia->auto_debit;
                    }
                    if (property_exists($payment_getway_customer->methods->transferencia, 'lbl_debt')) {
                        $payment_getway_default->methods->transferencia->lbl_debt = $payment_getway_customer->methods->transferencia->lbl_debt;
                    }
                }

                if (property_exists($payment_getway_customer->methods, 'debit_cards')) {
                    if (property_exists($payment_getway_customer->methods->debit_cards, 'credential')) {
                        $payment_getway_default->methods->debit_cards->credential = $payment_getway_customer->methods->debit_cards->credential;
                    }
                    if (property_exists($payment_getway_customer->methods->debit_cards, 'edit')) {
                        $payment_getway_default->methods->debit_cards->edit = $payment_getway_customer->methods->debit_cards->edit;
                    }
                    if (property_exists($payment_getway_customer->methods->debit_cards, 'auto_debit')) {
                        $payment_getway_default->methods->debit_cards->auto_debit = $payment_getway_customer->methods->debit_cards->auto_debit;
                    }
                    if (property_exists($payment_getway_customer->methods->debit_cards, 'lbl_debt')) {
                        $payment_getway_default->methods->debit_cards->lbl_debt = $payment_getway_customer->methods->debit_cards->lbl_debt;
                    }
                }

                if (property_exists($payment_getway_customer->methods, 'credit_cards')) {
                    if (property_exists($payment_getway_customer->methods->credit_cards, 'credential')) {
                        $payment_getway_default->methods->credit_cards->credential = $payment_getway_customer->methods->credit_cards->credential;
                    }
                    if (property_exists($payment_getway_customer->methods->credit_cards, 'edit')) {
                        $payment_getway_default->methods->credit_cards->edit = $payment_getway_customer->methods->credit_cards->edit;
                    }
                    if (property_exists($payment_getway_customer->methods->credit_cards, 'auto_debit')) {
                        $payment_getway_default->methods->credit_cards->auto_debit = $payment_getway_customer->methods->credit_cards->auto_debit;
                    }
                    if (property_exists($payment_getway_customer->methods->credit_cards, 'lbl_debt')) {
                        $payment_getway_default->methods->credit_cards->lbl_debt = $payment_getway_customer->methods->credit_cards->lbl_debt;
                    }
                }

                if (property_exists($payment_getway_customer->methods, 'pim')) {
                    if (property_exists($payment_getway_customer->methods->pim, 'credential')) {
                        $payment_getway_default->methods->pim->credential = $payment_getway_customer->methods->pim->credential;
                    }
                    if (property_exists($payment_getway_customer->methods->pim, 'edit')) {
                        $payment_getway_default->methods->pim->edit = $payment_getway_customer->methods->pim->edit;
                    }
                    if (property_exists($payment_getway_customer->methods->pim, 'auto_debit')) {
                        $payment_getway_default->methods->pim->auto_debit = $payment_getway_customer->methods->pim->auto_debit;
                    }
                    if (property_exists($payment_getway_customer->methods->pim, 'lbl_debt')) {
                        $payment_getway_default->methods->pim->lbl_debt = $payment_getway_customer->methods->pim->lbl_debt;
                    }
                }

                if (property_exists($payment_getway_customer->methods, 'payu_card')) {
                    if (property_exists($payment_getway_customer->methods->payu_card, 'credential')) {
                        $payment_getway_default->methods->payu_card->credential = $payment_getway_customer->methods->payu_card->credential;
                    }
                    if (property_exists($payment_getway_customer->methods->payu_card, 'edit')) {
                        $payment_getway_default->methods->payu_card->edit = $payment_getway_customer->methods->payu_card->edit;
                    }
                    if (property_exists($payment_getway_customer->methods->payu_card, 'auto_debit')) {
                        $payment_getway_default->methods->payu_card->auto_debit = $payment_getway_customer->methods->payu_card->auto_debit;
                    }
                    if (property_exists($payment_getway_customer->methods->payu_card, 'lbl_debt')) {
                        $payment_getway_default->methods->payu_card->lbl_debt = $payment_getway_customer->methods->payu_card->lbl_debt;
                    }
                }

                if (property_exists($payment_getway_customer->methods, 'cobrodigital_card')) {
                    if (property_exists($payment_getway_customer->methods->cobrodigital_card, 'credential')) {
                        $payment_getway_default->methods->cobrodigital_card->credential = $payment_getway_customer->methods->cobrodigital_card->credential;
                    }
                    if (property_exists($payment_getway_customer->methods->cobrodigital_card, 'edit')) {
                        $payment_getway_default->methods->cobrodigital_card->edit = $payment_getway_customer->methods->cobrodigital_card->edit;
                    }
                    if (property_exists($payment_getway_customer->methods->cobrodigital_card, 'auto_debit')) {
                        $payment_getway_default->methods->cobrodigital_card->auto_debit = $payment_getway_customer->methods->cobrodigital_card->auto_debit;
                    }
                    if (property_exists($payment_getway_customer->methods->cobrodigital_card, 'lbl_debt')) {
                        $payment_getway_default->methods->cobrodigital_card->lbl_debt = $payment_getway_customer->methods->cobrodigital_card->lbl_debt;
                    }
                }

                if (property_exists($payment_getway_customer->methods, 'todopago')) {
                    if (property_exists($payment_getway_customer->methods->todopago, 'credential')) {
                        $payment_getway_default->methods->todopago->credential = $payment_getway_customer->methods->todopago->credential;
                    }
                    if (property_exists($payment_getway_customer->methods->todopago, 'edit')) {
                        $payment_getway_default->methods->todopago->edit = $payment_getway_customer->methods->todopago->edit;
                    }
                    if (property_exists($payment_getway_customer->methods->todopago, 'auto_debit')) {
                        $payment_getway_default->methods->todopago->auto_debit = $payment_getway_customer->methods->todopago->auto_debit;
                    }
                    if (property_exists($payment_getway_customer->methods->todopago, 'lbl_debt')) {
                        $payment_getway_default->methods->todopago->lbl_debt = $payment_getway_customer->methods->todopago->lbl_debt;
                    }
                }

                if (property_exists($payment_getway_customer->methods, 'mercadopago')) {
                    if (property_exists($payment_getway_customer->methods->mercadopago, 'credential')) {
                        $payment_getway_default->methods->mercadopago->credential = $payment_getway_customer->methods->mercadopago->credential;
                    }
                    if (property_exists($payment_getway_customer->methods->mercadopago, 'edit')) {
                        $payment_getway_default->methods->mercadopago->edit = $payment_getway_customer->methods->mercadopago->edit;
                    }
                    if (property_exists($payment_getway_customer->methods->mercadopago, 'auto_debit')) {
                        $payment_getway_default->methods->mercadopago->auto_debit = $payment_getway_customer->methods->mercadopago->auto_debit;
                    }
                    if (property_exists($payment_getway_customer->methods->mercadopago, 'lbl_debt')) {
                        $payment_getway_default->methods->mercadopago->lbl_debt = $payment_getway_customer->methods->mercadopago->lbl_debt;
                    }
                }

                if (property_exists($payment_getway_customer->methods, 'chubut_auto_debit')) {
                    if (property_exists($payment_getway_customer->methods->chubut_auto_debit, 'credential')) {
                        $payment_getway_default->methods->chubut_auto_debit->credential = $payment_getway_customer->methods->chubut_auto_debit->credential;
                    }
                    if (property_exists($payment_getway_customer->methods->chubut_auto_debit, 'edit')) {
                        $payment_getway_default->methods->chubut_auto_debit->edit = $payment_getway_customer->methods->chubut_auto_debit->edit;
                    }
                    if (property_exists($payment_getway_customer->methods->chubut_auto_debit, 'auto_debit')) {
                        $payment_getway_default->methods->chubut_auto_debit->auto_debit = $payment_getway_customer->methods->chubut_auto_debit->auto_debit;
                    }
                    if (property_exists($payment_getway_customer->methods->chubut_auto_debit, 'lbl_debt')) {
                        $payment_getway_default->methods->chubut_auto_debit->lbl_debt = $payment_getway_customer->methods->chubut_auto_debit->lbl_debt;
                    }
                }

                if (property_exists($payment_getway_customer->methods, 'cobrodigital_auto_debit')) {
                    if (property_exists($payment_getway_customer->methods->cobrodigital_auto_debit, 'credential')) {
                        $payment_getway_default->methods->cobrodigital_auto_debit->credential = $payment_getway_customer->methods->cobrodigital_auto_debit->credential;
                    }
                    if (property_exists($payment_getway_customer->methods->cobrodigital_auto_debit, 'edit')) {
                        $payment_getway_default->methods->cobrodigital_auto_debit->edit = $payment_getway_customer->methods->cobrodigital_auto_debit->edit;
                    }
                    if (property_exists($payment_getway_customer->methods->cobrodigital_auto_debit, 'auto_debit')) {
                        $payment_getway_default->methods->cobrodigital_auto_debit->auto_debit = $payment_getway_customer->methods->cobrodigital_auto_debit->auto_debit;
                    }
                    if (property_exists($payment_getway_customer->methods->cobrodigital_auto_debit, 'lbl_debt')) {
                        $payment_getway_default->methods->cobrodigital_auto_debit->lbl_debt = $payment_getway_customer->methods->cobrodigital_auto_debit->lbl_debt;
                    }
                }

                if (property_exists($payment_getway_customer->methods, 'cuentadigital')) {
                    if (property_exists($payment_getway_customer->methods->cuentadigital, 'credential')) {
                        $payment_getway_default->methods->cuentadigital->credential = $payment_getway_customer->methods->cuentadigital->credential;
                    }
                    if (property_exists($payment_getway_customer->methods->payu_card, 'edit')) {
                        $payment_getway_default->methods->cuentadigital->edit = $payment_getway_customer->methods->cuentadigital->edit;
                    }
                    if (property_exists($payment_getway_customer->methods->cuentadigital, 'auto_debit')) {
                        $payment_getway_default->methods->cuentadigital->auto_debit = $payment_getway_customer->methods->cuentadigital->auto_debit;
                    }
                    if (property_exists($payment_getway_customer->methods->payu_card, 'lbl_debt')) {
                        $payment_getway_default->methods->cuentadigital->lbl_debt = $payment_getway_customer->methods->cuentadigital->lbl_debt;
                    }
                }

                if (property_exists($payment_getway_customer->methods, 'visa_auto_debit')) {
                    if (property_exists($payment_getway_customer->methods->visa_auto_debit, 'credential')) {
                        $payment_getway_default->methods->visa_auto_debit->credential = $payment_getway_customer->methods->visa_auto_debit->credential;
                    }
                    if (property_exists($payment_getway_customer->methods->visa_auto_debit, 'edit')) {
                        $payment_getway_default->methods->visa_auto_debit->edit = $payment_getway_customer->methods->visa_auto_debit->edit;
                    }
                    if (property_exists($payment_getway_customer->methods->visa_auto_debit, 'auto_debit')) {
                        $payment_getway_default->methods->visa_auto_debit->auto_debit = $payment_getway_customer->methods->visa_auto_debit->auto_debit;
                    }
                    if (property_exists($payment_getway_customer->methods->visa_auto_debit, 'lbl_debt')) {
                        $payment_getway_default->methods->visa_auto_debit->lbl_debt = $payment_getway_customer->methods->visa_auto_debit->lbl_debt;
                    }
                }

                if (property_exists($payment_getway_customer->methods, 'mastercard_auto_debit')) {
                    if (property_exists($payment_getway_customer->methods->mastercard_auto_debit, 'credential')) {
                        $payment_getway_default->methods->mastercard_auto_debit->credential = $payment_getway_customer->methods->mastercard_auto_debit->credential;
                    }
                    if (property_exists($payment_getway_customer->methods->mastercard_auto_debit, 'edit')) {
                        $payment_getway_default->methods->mastercard_auto_debit->edit = $payment_getway_customer->methods->mastercard_auto_debit->edit;
                    }
                    if (property_exists($payment_getway_customer->methods->mastercard_auto_debit, 'auto_debit')) {
                        $payment_getway_default->methods->mastercard_auto_debit->auto_debit = $payment_getway_customer->methods->mastercard_auto_debit->auto_debit;
                    }
                    if (property_exists($payment_getway_customer->methods->mastercard_auto_debit, 'lbl_debt')) {
                        $payment_getway_default->methods->mastercard_auto_debit->lbl_debt = $payment_getway_customer->methods->mastercard_auto_debit->lbl_debt;
                    }
                }

                if (property_exists($payment_getway_customer->methods, 'rapipago')) {
                    if (property_exists($payment_getway_customer->methods->rapipago, 'credential')) {
                        $payment_getway_default->methods->rapipago->credential = $payment_getway_customer->methods->rapipago->credential;
                    }
                    if (property_exists($payment_getway_customer->methods->rapipago, 'edit')) {
                        $payment_getway_default->methods->rapipago->edit = $payment_getway_customer->methods->rapipago->edit;
                    }
                    if (property_exists($payment_getway_customer->methods->rapipago, 'auto_debit')) {
                        $payment_getway_default->methods->rapipago->auto_debit = $payment_getway_customer->methods->rapipago->auto_debit;
                    }
                    if (property_exists($payment_getway_customer->methods->rapipago, 'lbl_debt')) {
                        $payment_getway_default->methods->rapipago->lbl_debt = $payment_getway_customer->methods->rapipago->lbl_debt;
                    }
                }

            } //end methods

        }
        return $payment_getway_default;
    }

    private function loadResources()
    {
        $realod = true;

        $session = $this->request->getSession();

        if ($realod || !$session->check('paraments')) {

            $file = new File(WWW_ROOT . 'paraments.default.json', false, 0644);
            $json = $file->read(true, 'r');
            $parament_default = json_decode($json);
            $file->close();

            $this->loadModel('GeneralParameters');
            $general_paraments_db = $this->GeneralParameters->find()->first();

            $parament_cliente = null;

            if ($general_paraments_db) {
                $parament_cliente = unserialize($general_paraments_db->data);
                $parament = $this->merge_paraments($parament_default, $parament_cliente);
                $general_paraments_db->modified = Time::now();
                $general_paraments_db->data = serialize($parament);
                $this->GeneralParameters->save($general_paraments_db);
                $this->_general_paraments = $parament;
            } else {
                $general_paraments = $this->GeneralParameters->newEntity();
                $general_paraments->created = Time::now();
                $general_paraments->modified = Time::now();
                $parament = $this->merge_paraments($parament_default, $parament_cliente);
                $general_paraments->data = serialize($parament_default);
                $this->GeneralParameters->save($general_paraments);
                $this->_general_paraments = $parament_default;
            }

            $session->write('paraments', $parament);
        }

        if ($realod || !$session->check('payment_getway')) {

            $file = new File(WWW_ROOT . 'payment_getway.default.json', false, 0644);
            $json = $file->read(true, 'r');
            $payment_getway_default =  json_decode($json);
            $file->close();

            $this->loadModel('PaymentGetwayParameters');
            $payment_getway_paraments_db = $this->PaymentGetwayParameters->find()->first();

            $payment_getway_cliente = null;

            if ($payment_getway_paraments_db) {
                $payment_getway_cliente = unserialize($payment_getway_paraments_db->data);
                $payment_getway = $this->merge_payment_getway($payment_getway_default, $payment_getway_cliente);
                $payment_getway_paraments_db->modified = Time::now();
                $payment_getway_paraments_db->data = serialize($payment_getway);
                $this->PaymentGetwayParameters->save($payment_getway_paraments_db);
            } else {
                $payment_getway_paraments = $this->PaymentGetwayParameters->newEntity();
                $payment_getway_paraments->created = Time::now();
                $payment_getway_paraments->modified = Time::now();
                $payment_getway = $this->merge_payment_getway($payment_getway_default, $payment_getway_cliente);
                $payment_getway_paraments->data = serialize($payment_getway);
                $this->PaymentGetwayParameters->save($payment_getway_paraments);
            }

            $session->write('payment_getway', $payment_getway);
        }

        if ($realod || !$session->check('titles')) {
            $file = new File(WWW_ROOT . 'titles.json', false, 0644);
            $json = $file->read(true, 'r');
            $titles =  json_decode($json, true);
            $session->write('titles', $titles);
            $file->close();
        }

        if ($realod || !$session->check('afip_codes')) {
            $file = new File(WWW_ROOT . 'afip_codes.json', false, 0644);
            $json = $file->read(true, 'r');
            $afip_codes =  json_decode($json, true);
            $session->write('afip_codes', $afip_codes);
            $file->close();
        }

        if ($realod || !$session->check('tp_codes')) {
            $file = new File(WWW_ROOT . 'tp_codes.json', false, 0644);
            $json = $file->read(true, 'r');
            $tp_codes = json_decode($json, true);
            $session->write('tp_codes', $tp_codes);
            $file->close();
        }

        if ($realod || !$session->check('mp_codes')) {
            $file = new File(WWW_ROOT . 'mp_codes.json', false, 0644);
            $json = $file->read(true, 'r');
            $mp_codes = json_decode($json, true);
            $session->write('mp_codes', $mp_codes);
            $file->close();
        }

        if ($realod || !$session->check('menu')) {
            $file = new File(WWW_ROOT . 'menu.json', false, 0644);
            $json = $file->read(true, 'r');
            $menu =  json_decode($json, true);
            $session->write('menu', $menu);
            $file->close();
        }

        if ($realod || !$session->check('banks')) {
            $file = new File(WWW_ROOT . 'banks.json', false, 0644);
            $json = $file->read(true, 'r');
            $banks =  json_decode($json, true);
            $session->write('banks', $banks);
            $file->close();
        }
    }

    protected function saveGeneralParaments($parament)
    {
        $general_paraments_db = $this->GeneralParameters->find()->first();
        $general_paraments_db->modified = Time::now();
        $general_paraments_db->data = serialize($parament);

        if ($this->GeneralParameters->save($general_paraments_db)) {

            $this->request->getSession()->write('paraments', $parament);
            // $this->Flash->success(__('Configuración general actualizada.'));
            return true;
        } else {
            $this->Flash->error(__('Error al intentar actualizar la configuración general'));
        }

        return false;
    }

    protected function savePortalParaments($portal)
    {
        $portal_paraments_db = $this->PortalParameters->find()->first();

        $portal_paraments_db->modified = Time::now();
        $portal_paraments_db->data = serialize($portal);

        if ($this->PortalParameters->save($portal_paraments_db)) {

            $this->request->getSession()->write('portal', $portal);

            return true;
        } else {
            $this->Flash->error(__('Error al intentar actualizar la configuración portal'));
        }

        return false;
    }

    protected function savePaymentGetwayParaments($payment_getway)
    {
        $payment_getway_paraments_db = $this->PaymentGetwayParameters->find()->first();

        $payment_getway_paraments_db->modified = Time::now();
        $payment_getway_paraments_db->data = serialize($payment_getway);

        if ($this->PaymentGetwayParameters->save($payment_getway_paraments_db)) {

            $this->request->getSession()->write('payment_getway', $payment_getway);
            
            $this->Flash->success(__('Configuración pasarela de pagos actualizada.'));
            return true;
        } else {
            $this->Flash->error(__('Error al intentar actualizar la configuración de pasarela de pagos'));
        }

        return false;
    }

    public function beforeFilter(Event $event)
    {
        $this->getEventManager()->off($this->Csrf);
    }

    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->getType(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }

        if (!$this->request->is('ajax')) {//Ajax Detection

            $this->set('current_controller', $this->request->getParam('controller'));
            $this->set('current_action', $this->request->getParam('action'));
            $this->set('username', $this->request->getSession()->read('Auth.User')['username']);
            $this->set('version', Configure::Read('project.version'));

            $paymentMethods = [];
            $payment_getway = $this->request->getSession()->read('payment_getway');

            foreach ($payment_getway->methods as $pg) {
                $config = $pg->config;
                if ($payment_getway->config->$config->enabled) {
                    $paymentMethods[$pg->id] = $pg->name;
                }
            }

            $this->set('paymentMethods', $paymentMethods);

            $this->set('responsibles', $this->request->getSession()->read('afip_codes')['responsibles']);
            $this->set('comprobantes', $this->request->getSession()->read('afip_codes')['comprobantes']);
            $this->set('combinations', $this->request->getSession()->read('afip_codes')['combinationsInvoices']);
            $this->set('doc_types', $this->request->getSession()->read('afip_codes')['doc_types']);
            $this->set('units_types', $this->request->getSession()->read('afip_codes')['units_types']);
            $this->set('concept_types', $this->request->getSession()->read('afip_codes')['concept_types']);
            $this->set('accountant_types', $this->request->getSession()->read('afip_codes')['accountant_types']);
            $this->set('alicuotas_types', $this->request->getSession()->read('afip_codes')['alicuotas_types']);
            $this->set('alicuotas_percectage', $this->request->getSession()->read('afip_codes')['alicuotas_percectage']);
            $this->set('comprobantesLetter', $this->request->getSession()->read('afip_codes')['comprobantesLetter']);
            $this->set('cond_venta', $this->request->getSession()->read('afip_codes')['cond_venta']);

            //Para diferenciar usuario tipo: Portal y Sistema.
            $this->set('type_user', $this->request->getSession()->read('type'));

            $paraments = $this->request->getSession()->read('paraments');

            $this->set('paraments', $paraments);

            if (!$paraments->billing->confirm) {
                $this->loadModel('Provinces');
                $provinces = $this->Provinces->find('list')->toArray();
                $this->set('provinces_billing', $provinces);
            }

            $this->set('conff', $this->request->getSession()->read('conff'));

            //check login
            if ($this->request->getSession()->read('Auth.User')) {
                $this->set('loggedIn', true);
            } else {
                $this->set('loggedIn',false);
            }

            //Infomacion de licencia
            $license = [
                'amount_customers' => 300,
                'duedate' => new Time('2017-04-15 00:00:00'),
                'now' => Time::now()
            ];

            $this->set('license', $license);

            $actionsSystemArray  = [];
            $itemsMenuByUser =  [];

            $actionsSystemArray = $this->getActionsSystem();
            $itemsMenuByUser = $this->getItemsMenuByUser();

            $this->set('actionsSystemArray', $actionsSystemArray);
            $this->set('itemsMenuByUser', $itemsMenuByUser);    

        }

        $token = $this->request->getParam('_csrfToken');
        $this->set(compact('token'));
    }

    private function getActionsSystem()
    {
         $this->loadModel('ActionsSystem');
         return $this->ActionsSystem->find()->contain(['Clases'])->toArray();
    }

    private function getItemsMenuByUser()
    {
        $items_menu_show = [];

        if ($this->request->getSession()->check('Auth.User')) {

            $user_id = $this->request->getSession()->read('Auth.User')['id'];
            $paraments = $this->request->getSession()->read('paraments');

            if ($user_id == 100 || !$paraments->gral_config->using_privilegios) {

                $this->loadModel('Roles');

                $roles = $this->Roles
                    ->find()
                    ->contain([
                        'ActionsSystem.Clases'
                    ]);

                foreach ($roles as $role) {
                    foreach ($role->actions_system as $action) {
                        $items_menu_show[] = $action->clase->clase . '-' . $action->action;
                    }
                }

                return $items_menu_show;
            }

            $this->loadModel('Users');
            $user = $this->Users->get($user_id, [
                'contain' => [
                    'Roles.ActionsSystem.Clases'
                ]
            ]);

            if ($user->enabled && !$user->deleted) {
                 foreach ($user->roles as $role) {
                    foreach ($role->actions_system as $action) {
                        $items_menu_show[] = $action->clase->clase . '-' . $action->action;
                    }
                }
            }
        }

        return $items_menu_show;
    }

    public function allowRol($user_id, $c = NULL, $a = NULL)
    {
        $paraments = $this->request->getSession()->read('paraments');

        if ($user_id == 100 || !$paraments->gral_config->using_privilegios) {
            return true;
        }

        $this->loadModel('Users');
        $user = $this->Users->get($user_id, [
            'contain' => [
                    'Roles.ActionsSystem.Clases'
                ]
            ]);

        if (!$user->enabled || $user->deleted) {
            return false;
        }

        $pass = false;

        if (!$c) {

            foreach ($user->roles as $role) {
                foreach ($role->actions_system as $action) {
                    if (strtoupper($action->clase->clase) == strtoupper($this->request->getParam('controller'))) {
                        if (strtoupper($this->request->getParam('action')) == strtoupper($action->action)) {
                            if ($role->enabled && !$role->deleted) {
                                $pass = true;
                            }
                        }
                    }
                }
            }
        } else {

            foreach ($user->roles as $role) {
                 foreach ($role->actions_system as $action) {
                    if (strtoupper($action->clase->clase) == strtoupper($c)) {
                        if (strtoupper($a) == strtoupper($action->action)) {
                            if ($role->enabled && !$role->deleted) {
                                $pass = true;
                            }
                        }
                    }
                }
            }
        }

        return $pass;
    }

    public function registerActivity($action, $detail = '', $customer_code = NULL, $main = FALSE)
    {
        $this->loadModel('ActionLog');
        $actionLog = $this->ActionLog->newEntity();
        $actionLog->created = Time::now();
        $actionLog->detail = $detail;

        $user_id = NULL;

        if (!isset($this->request->getSession()->read('Auth.User')['id'])) {
            $user_id = 100;
        } else {
            $user_id = $this->request->getSession()->read('Auth.User')['id'];
        }

        $actionLog->user_id = $user_id;
        $actionLog->action = $action;
        $actionLog->customer_code = $customer_code;
        $actionLog->main = $main;
        $this->ActionLog->save($actionLog);
    }

    public function isReloadPage()
    {
        $s = $this->request->getSession();

        if ($s->check('hash') && $s->read('hash') == $this->request->getData('hash')) {
            return TRUE;
        } else {
            $this->request->getSession()->write('hash', $this->request->getData('hash'));
        }

        return FALSE;
    }
}
