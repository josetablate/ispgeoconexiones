<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;

class DownloadController extends AppController
{
    public function isAuthorized($user = null)
    {
        return parent::allowRol($user['id']);
    }

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('FiscoAfipCompPdf', [
             'className' => '\App\Controller\Component\Admin\FiscoAfipCompPdf'
        ]);
    }

    public function index()
    {}

    public function invoices()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {

            $ids = $this->request->getData('ids');

            $ids = explode(',', $ids);

            $unix_download = Time::now()->toUnixString();

            foreach ($ids as $id) {

                $data = new \stdClass;
                $data->id = $id;
                $this->FiscoAfipCompPdf->invoice($data, false, true, $unix_download);
            }

            $downloadShell = new \App\Shell\DownloadShell;
            $downloadShell->download($unix_download);

            $this->response->file(WWW_ROOT . "temp_files_downloand/$unix_download.tar.gz", array(
                'download' => true,
                'name' => 'facturas.tar.gz',
            ));

            return $this->response;
        }
    }

    public function receipts()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {

            $ids = $this->request->getData('ids');

            $ids = explode(',', $ids);

            $unix_download = Time::now()->toUnixString();

            foreach ($ids as $id) {

                $data = new \stdClass;
                $data->id = $id;
                $this->FiscoAfipCompPdf->receipt($data, false, true, $unix_download);
            }

            $downloadShell = new \App\Shell\DownloadShell;
            $downloadShell->download($unix_download);

            $this->response->file(WWW_ROOT . "temp_files_downloand/$unix_download.tar.gz", array(
                'download' => true,
                'name' => 'recibos.tar.gz',
            ));

            return $this->response;
        }
    }

    public function creditNotes()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {

            $ids = $this->request->getData('ids');

            $ids = explode(',', $ids);

            $unix_download = Time::now()->toUnixString();

            foreach ($ids as $id) {

                $data = new \stdClass;
                $data->id = $id;
                $this->FiscoAfipCompPdf->creditNote($data, false, true, $unix_download);
            }

            $downloadShell = new \App\Shell\DownloadShell;
            $downloadShell->download($unix_download);

            $this->response->file(WWW_ROOT . "temp_files_downloand/$unix_download.tar.gz", array(
                'download' => true,
                'name' => 'notas_creditos.tar.gz',
            ));

            return $this->response;
        }
    }

    public function debitNotes()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {

            $ids = $this->request->getData('ids');

            $ids = explode(',', $ids);

            $unix_download = Time::now()->toUnixString();

            foreach ($ids as $id) {

                $data = new \stdClass;
                $data->id = $id;
                $this->FiscoAfipCompPdf->debitNote($data, false, true, $unix_download);
            }

            $downloadShell = new \App\Shell\DownloadShell;
            $downloadShell->download($unix_download);

            $this->response->file(WWW_ROOT . "temp_files_downloand/$unix_download.tar.gz", array(
                'download' => true,
                'name' => 'notas_debitos.tar.gz',
            ));

            return $this->response;
        }
    }
}
