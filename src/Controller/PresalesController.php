<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\Component\Admin\CreatePresalePdf;
use Cake\I18n\Time;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

/**
 * Presales Controller
 *
 *
 * @method \App\Model\Entity\Presale[] paginate($object = null, array $settings = [])
 */
class PresalesController extends AppController
{
    public function isAuthorized($user = null) 
    {
        if ($this->request->getParam('action') == 'changePaymentMethod') {
            return true;
        }

        if ($this->request->getParam('action') == 'changeInstallationsDate') {
            return true;
        }

        if ($this->request->getParam('action') == 'changeAvailability') {
            return true;
        }

        if ($this->request->getParam('action') == 'changeMarker') {
            return true;
        }

        if ($this->request->getParam('action') == 'changeComments') {
            return true;
        }

        if ($this->request->getParam('action') == 'addCustomer') {
            return true;
        }

        if ($this->request->getParam('action') == 'addCobrodgital') {
            return true;
        }

        if ($this->request->getParam('action') == 'saveDataInstallation') {
            return true;
        }

        if ($this->request->getParam('action') == 'loadServices') {
            return true;
        }

        if ($this->request->getParam('action') == 'loadProducts') {
            return true;
        }

        if ($this->request->getParam('action') == 'loadPackages') {
            return true;
        }

        if ($this->request->getParam('action') == 'loadCustomer') {
            return true;
        }

        if ($this->request->getParam('action') == 'create') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Customers');
        $this->loadComponent('CreatePresalePdf', [
            'className' => '\App\Controller\Component\Admin\CreatePresalePdf'
        ]);

        $this->loadComponent('Accountant', [
            'className' => '\App\Controller\Component\Admin\Accountant'
        ]);

        $this->loadComponent('CurrentAccount', [
            'className' => '\App\Controller\Component\Admin\CurrentAccount'
        ]);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $presales = $this->Presales->find()->contain(['Debts', 'Users', 'Customers']);
        $this->loadModel('MassEmailsTemplates');
        $mass_emails_templates_model = $this->MassEmailsTemplates
            ->find()
            ->where([
                'enabled' => TRUE
            ]);

        $mass_emails_templates = [
            '' => 'Seleccione'
        ];

        foreach ($mass_emails_templates_model as $mass_email_template_model) {
            $mass_emails_templates[$mass_email_template_model->id] = $mass_email_template_model->name;
        }

        $this->set(compact('presales', 'mass_emails_templates'));
        $this->set('_serialize', ['presales']);
    }

    /**
     * View method
     *
     * @param string|null $id Presale id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $presale = $this->Presales->get($id, [
            'contain' => ['Debts', 'Customers', 'Users']
        ]);
        $range_hours = $this->request->getSession()->read('paraments')->presale->range_hours_installation;

        $range_hours_installation = [];

        foreach ($range_hours as $rh) {
            $range_hours_installation[$rh] = $rh;
        }

        $paraments = $this->request->getSession()->read('paraments');

        $this->set('presale', $presale);
        $this->set('range_hours_installation', $range_hours_installation);
        $this->set('_serialize', ['presale']);
    }

    public function firstAdd($presale_id = null)
    {
        $presale = $this->Presales->newEntity();
        $session = $this->request->getSession();
        $range_hours_installation = $this->request->getSession()->read('paraments')->presale->range_hours_installation;
        $paraments = $this->request->getSession()->read('paraments');
        $banks = $this->request->getSession()->read('banks');

        $continue_sale = null;

        if (!$session->check('presale')) {
            $continue_sale = false;
            $this->create();
            $presale = $session->read('presale');
        } else {

            $presale = $session->read('presale');
            $continue_sale = true;
        }

        $this->set(compact('presale', 'range_hours_installation', 'paymentMethods', 'paraments', 'presale_id', 'continue_sale', 'banks', 'id_comercios'));
        $this->set('_serialize', ['presale']);

        $this->render('add');
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($presale_id = null)
    {
        $presale = $this->Presales->newEntity();
        $session = $this->request->getSession();
        $range_hours_installation = $this->request->getSession()->read('paraments')->presale->range_hours_installation;
        $paraments = $this->request->getSession()->read('paraments');
        $banks = $this->request->getSession()->read('banks');

        if (!$session->check('presale')) {
            $this->create();
            $presale = $session->read('presale');
        } else {
            $presale = $session->read('presale');
        }

        $continue_sale = false;

        $this->set(compact('presale', 'range_hours_installation', 'paymentMethods', 'paraments', 'presale_id', 'continue_sale', 'payment_getway', 'banks', 'id_comercios'));
        $this->set('_serialize', ['presale']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Presale id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $presale = $this->Presales->get(intval($this->request->getData('presale_id')));
            $presale = $this->Presales->patchEntity($presale, $this->request->getData());
            if ($this->Presales->save($presale)) {
                $this->Flash->success(__('Se ha guardado correctamente el comentario.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se ha guardado el comentario. Por favor, intente nuevamente.'));
        }
        $this->set(compact('presale'));
        $this->set('_serialize', ['presale']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Presale id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $presale = $this->Presales->get($id);
        if ($this->Presales->delete($presale)) {
            $this->Flash->success(__('The presale has been deleted.'));
        } else {
            $this->Flash->error(__('The presale could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function changePaymentMethod()
    {
        if ($this->request->is('ajax')) {
            $payment_method_id = $this->request->input('json_decode')->payment_method_id;
            $session = $this->request->getSession();
            $paraments = $this->request->getSession()->read('paraments');
            if ($session->check('presale')) {
                $presale = $session->read('presale');
                $presale->payment_method_id = $payment_method_id;
                $session->write('presale', $presale);
                $payment_name = "";
                foreach ($paraments->payment_getway as $pg) {
                    if ($payment_method_id == $pg->id) {
                        $payment_name = $pg->name;
                        break;
                    }
                }
                $data['message'] = "Cambio de Método de Pago: " . $payment_name;
            } else {
                $data['message'] = "No se creo el objeto Venta.";
            }
            $this->set('data', $data);
        }
    }

    public function changeInstallationsDate()
    {
        if ($this->request->is('ajax')) {
            $installations_date = $this->request->input('json_decode')->installations_date;
            $session = $this->request->getSession();
            if ($session->check('presale')) {
                $presale = $session->read('presale');
                
                $installations_date = explode('/', $installations_date);
                $installations_date = $installations_date[2] . '-' . $installations_date[1] . '-' . $installations_date[0] . ' ' . Time::now()->format('H:i:s');
                
                $presale->installations_date = new Time($installations_date);
                $session->write('presale', $presale);
                $data['installations_date'] = $presale->installations_date;
                $data['message'] = "Cambio de Fecha de instalación correctamente: " . $presale->installations_date->format('Y-m-d H:i:s');
            } else {
                $data['message'] = "No se cambio la fecha de instalación. Por favor intente, nuevamente";
            }
            $this->set('data', $data);
        }
    }

    public function changeAvailability()
    {
        if ($this->request->is('ajax')) {
            $availability = $this->request->input('json_decode')->availability;
            $session = $this->request->getSession();
            $range_hours_installation = $this->request->getSession()->read('paraments')->presale->range_hours_installation;
            if ($session->check('presale')) {
                $presale = $session->read('presale');
                $presale->availability = $availability;
                $session->write('presale', $presale);
                $data['message'] = "Cambio el rango horario correctamente: " . $range_hours_installation[$availability];
            } else {
                $data['message'] = "No se cambio el rango horario. Por favor intente, nuevamente";
            }
            $this->set('data', $data);
        }
    }

    public function changeMarker()
    {
        if ($this->request->is('ajax')) {
            $lat = $this->request->input('json_decode')->lat;
            $lng = $this->request->input('json_decode')->lng;
            $session = $this->request->getSession();

            if ($session->check('presale')) {
                $presale = $session->read('presale');
                $presale->lat = $lat;
                $presale->lng = $lng;
                $session->write('presale', $presale);
                $data['message'] = "Cambio el marcador lat: " . $lat . ' - lng: ' . $lng;
            } else {
                $data['message'] = "No se cambio el marcador. Por favor intente, nuevamente";
            }
            $this->set('data', $data);
        }
    }

    public function changeComments()
    {
        if ($this->request->is('ajax')) {
            $comments = $this->request->input('json_decode')->comments;
            $session = $this->request->getSession();

            if ($session->check('presale')) {
                $presale = $session->read('presale');
                $presale->comments = $comments;
                $session->write('presale', $presale);
                $data['message'] = "Cambio el comentario: " . $comments;
            } else {
                $data['message'] = "No se cambio el comenario. Por favor intente, nuevamente";
            }
            $this->set('data', $data);
        }
    }

    public function deletePackage()
    {
        $this->request->allowMethod(['post', 'delete']);
        $id = $this->request->getData('id');
        $flag = false;
        $session = $this->request->getSession();
        if ($session->check('presale')) {
            $presale = $session->read('presale');
            foreach ($presale->packages as $key => $package) {
                if ($key == $id) {
                    unset($presale->packages[$key]);
                    $flag = true;
                    break;
                }
            }
            $session->write('presale', $presale);
        }

        if ($flag) {
            $this->Flash->success(__('Se ha eliminado el Paquete correctamente.'));
        } else {
            $this->Flash->warngin(__('No se ha eliminado el Paquete. Por favor intente nuevamente.'));
        }

        return $this->redirect(['action' => 'add']);
    }

    public function deleteProduct()
    {
        $this->request->allowMethod(['post', 'delete']);
        $id = $this->request->getData('id');
        $flag = false;
        $session = $this->request->getSession();
        if ($session->check('presale')) {
            $presale = $session->read('presale');
            foreach ($presale->products as $key => $product) {
                if ($key == $id) {
                    unset($presale->products[$key]);
                    $flag = true;
                    break;
                }
            }
            $session->write('presale', $presale);
        }

        if ($flag) {
            $this->Flash->success(__('Se ha eliminado el Producto correctamente.'));
        } else {
            $this->Flash->warning(__('No se ha eliminado el Producto. Por favor intente nuevamente.'));
        }

        return $this->redirect(['action' => 'add']);
    }

    public function deleteService()
    {
        $this->request->allowMethod(['post', 'delete']);
        $id = $this->request->getData('id');
        $flag = false;
        $session = $this->request->getSession();
        if ($session->check('presale')) {
            $presale = $session->read('presale');

            foreach ($presale->services as $key => $service) {
                if ($key == $id) {
                    unset($presale->services[$key]);
                    $flag = true;
                    break;
                }
            }
            $session->write('presale', $presale);
        }

        if ($flag) {
            $this->Flash->success(__('Se ha eliminado el Servicio correctamente.'));
        } else {
            $this->Flash->warning(__('No se ha eliminado el Servicio. Por favor intente nuevamente.'));
        }

        return $this->redirect(['action' => 'add']);
    }

    public function saveDataInstallation()
    {
        if ($this->request->is('post')) {
            $flag = false;
            $session = $this->request->getSession();
            if ($session->check('presale')) {
                $presale = $session->read('presale');
                $presale->installations_date = $this->request->getData('installations_date');
                $presale->availability       = $this->request->getData('availability');
                $presale->comments           = $this->request->getData('comments');
                $presale->lat                = $this->request->getData('lat');
                $presale->lng                = $this->request->getData('lng');
                $session->write('presale', $presale);
                $flag = true;
            }
            if ($flag) {
                $this->Flash->success(__('Se han cargado los Datos de Instalación correctamente.'));
            } else {
                $this->Flash->warning(__('No se ha cargado los Datos de Instalación. Por favor intente nuevamente.'));
            }
        }

        return $this->redirect(['action' => 'add']);
    }

    public function cancelPresale()
    {
        $session = $this->request->getSession();
        $session->delete('presale');

        // if ($this->create()) {
            $this->Flash->success(__('Se ha cancelado con éxito la Venta'));
        // } else {
        //     $this->Flash->warning(__('No se ha cancelado la Venta. Por favor intente nuevamente.'));
        // }
        return $this->redirect(['action' => 'add']);
    }

    private function create()
    {
        $session = $this->request->getSession();
  
        // if ($session->check('presale')) {
        //     $presale = $session->read('presale');
        // }else{
            $presale = $this->Presales->newEntity();
        // }
     
        $presale->payment_method_id = 1;
        $presale->comments = '';
        $presale->customer = null;
        $presale->packages = [];
        $presale->services = [];
        $presale->products = [];
        $presale->packages_error = [];
        $presale->products_error = [];
        $presale->services_error = [];
        $session->write('presale', $presale);
        
        return true;
    }

    public function confirmPresale()
    {
        $session = $this->request->getSession();
        if ($session->check('presale')) {
            $presale = $session->read('presale');

            if (!empty($presale->customer)) {
                if (!$this->loadCustomer()) {
                    return $this->redirect(['action' => 'add']);
                }
            } else {
                $this->Flash->warning(__('Debe cargar un Cliente'));
            }

            $presale = $session->read('presale');
            $session->write('presale', $presale);

            $presalex = $this->Presales->newEntity();
            $data = [
                'customer_code'      => $presale->customer->code,
                'availability'       => $presale->availability,
                'payment_method_id'  => $presale->payment_method_id,
                'comments'           => $presale->comments,
                'user_id'            => $this->Auth->user()['id'],
            ];
            $presalex = $this->Presales->patchEntity($presalex, $data);
            $presale_id = null;

            if ($this->Presales->save($presalex)) {
                $presale_id = $presalex->id;
                $presale->id = $presalex->id;
                $session->write('presale', $presale);

                if (!empty($presale->packages)) {
                    $this->loadPackages();
                }

                if (!empty($presale->products)) {
                    $this->loadProducts();
                }

                if (!empty($presale->services)) {
                    $this->loadServices();
                }

                if (sizeof($presale->packages_error) == 0 
                    && sizeof($presale->products_error) == 0 
                    && sizeof($presale->services_error) == 0) {

                    //log de accion

                    $action =  'Venta Confirmada';
                    $detail =  '';
                    $this->registerActivity($action, $detail, $presale->customer->code, TRUE);

                    $this->Flash->success(__('Se ha generado correctamente la Venta'));

                    $session->delete('presale');

                } else {
                    if (sizeof($presale->packages_error) > 0) {
                        $this->Flash->warning(__('Cantidad de Deudas de Paquetes no generadas: ' . sizeof($presale->packages_error)));
                    }
                    if (sizeof($presale->products_error) > 0) {
                        $this->Flash->warning(__('Cantidad de Deudas de Productos no generadas: ' . sizeof($presale->products_error)));
                    }
                    if (sizeof($presale->services_error) > 0) {
                        $this->Flash->warning(__('Cantidad de Servicios pendientes no generados: ' . sizeof($presale->services_error)));
                    }
                }
            } else {
                $this->Flash->warning(__('No se ha generado la Venta. Por favor intente nuevamente.'));
            }
            return $this->redirect(['action' => 'add', $presale_id]);
        }
    }

    private function loadCustomer()
    {
        $session = $this->request->getSession();
        $presale = $session->read('presale');
        $flag = true;
        $customer = null;

        if (isset($presale->customer->code)) {
            $flag = false;
            $customer = $this->Customers->find()->where(['code' => $presale->customer->code])->first();
        }

        $paraments = $this->request->getSession()->read('paraments');

        if ($customer == null) {
            $customer = $this->Customers->newEntity();
            $customer->user_id = $this->Auth->user()['id']; 
        }

        /*solo datos que se puden editar*/
        $data = [
        	'name'                      => $presale->customer->name,
        	'email'                     => $presale->customer->email,
        	'address'                   => $presale->customer->address,
        	'doc_type'                  => $presale->customer->doc_type,
        	'ident'                     => $presale->customer->ident,
        	'responsible'               => $presale->customer->responsible,
        	'denomination'              => $presale->customer->denomination,
        	'cond_venta'                => $presale->customer->cond_venta,
        	'is_presupuesto'            => $presale->customer->is_presupuesto,
        	'country_id'                => $presale->customer->country_id,
        	'province_id'               => $presale->customer->province_id,
        	'city_id'                   => $presale->customer->city_id,
        	'area_id'                   => $presale->customer->area_id,
        	'phone'                     => $presale->customer->phone,
        	'phone_alt'                 => isset($presale->customer->phone_alt) ? $presale->customer->phone_alt : null,
        	'daydue'                    => $presale->customer->daydue,
        	'clave_portal'              => $presale->customer->clave_portal,
        	'comments'                  => $presale->customer->comments,
        	'lat'                       => isset($presale->customer->lat) ? $presale->customer->lat : null,
        	'lng'                       => isset($presale->customer->lng) ? $presale->customer->lng : null,
        	'cobrodigital'              => $presale->customer->cobrodigital,
        	'cd_auto_debit'             => $presale->customer->cd_auto_debit,
        	'clave_portal'              => $presale->customer->clave_portal,
        	'business_billing'          => $presale->customer->business_billing,
        	'payment_method_default_id' => $presale->customer->payment_method_default_id,
        	'seller'                    => $presale->customer->seller,
        	'labels'                    => [],
        	'acb_enabled'               => $presale->customer->acb_enabled,
        	'acb_due_debt'              => $presale->customer->acb_due_debt,
        	'acb_invoice_no_paid'       => $presale->customer->acb_invoice_no_paid,
        	'acb_type'                  => $presale->customer->acb_type,
        	'billing_for_service'       => $presale->customer->billing_for_service,
        ];

        $customer = $this->Customers->patchEntity($customer, $data);

        if ($this->Customers->save($customer)) {

            if ($customer->cobrodigital && $customer->cd_bar_code) {
                $this->loadModel('CobrodigitalCards');
                $card = $this->CobrodigitalCards->get($customer->cd_electronic_code);
                $card->used = true;
                $this->CobrodigitalCards->save($card);
            }

            $message = __('Cliente modificado.');

            if ($flag) {
                $message = __('Cliente Agregado.');
                $account = $this->Accountant->createAccount([
                    'account_parent' => $paraments->accountant->acounts_parent->customers,
                    'name' => sprintf("%'.05d", $customer->code) . ' - ' .  str_replace("-", "", $customer->ident) . ' - ' . $customer->name,
                    'redirect' => ['action' => 'add']
                ]);

                $customer->account_code = $account->code;

                if (!$this->Customers->save($customer)) {
                    $this->Flash->error(__('Error al actualizar la cuenta del Cliente agregado.'));
                    return false;
                }
            }

            $this->Flash->success($message);
            $presale->customer = $customer;
            $session->write('presale', $presale);
            return true;
        } else {

            $message = __('Error al modificar el Cliente.');
            if ($flag) {
                $message = __('Error al agregrar el Cliente.');
            }
            $this->Flash->error($message);
            return false;
        }
    }

    private function loadPackages()
    {
        $session = $this->request->getSession();

        if ($session->check('presale')) {
            $presale = $session->read('presale');
            $this->loadModel('Packages');
            $this->loadModel('Debts');

            foreach ($presale->packages as $packagex) {
                $packages = $this->Packages->newEntity();
                $package = $this->Packages->get($packagex->id);

                $customer = $this->Debts->Customers->get($presale->customer->code);

                //log de accion

                $action =  'Venta de Paquete';
                $detail =  'Paquete: ' . $package->name . PHP_EOL;
                $detail .= 'Cuotas: ' . $packagex->dues . PHP_EOL;
                $discount = $packagex->discount_name ? h($packagex->discount_name) . ' ' . ($packagex->discount_total * 100) . '%' : '';
                $detail .= 'Descuento: ' . $discount . PHP_EOL;
                $this->registerActivity($action, $detail, $customer->code);

                $ok = $this->CurrentAccount->addDebtPackage([
                    'amount'        => 1,
                    'customer'      => $customer,
                    'package'       => $package,
                    'duedate'       => $packagex->duedate,
                    'period'        => $packagex->period,
                    'dues'          => $packagex->dues,
                    'discount_id'   => $packagex->discount_id,
                    'presale_id'    => $presale->id,
                    'connection_id' => $packagex->connection_id, //null -> si no tiene asociado el pago a una conexion
                    'redirect'      => ['controller' => 'presales', 'action' => 'add'],
                ]);

                if (!$ok) {
                    $packagex->message_error = "Error al generar la Deuda.";
                    array_push($presale->packages_error, $packagex);
                    $session->write('presale', $presale);
                }
            }
        }
    }

    private function loadProducts()
    {
        $session = $this->request->getSession();
        if ($session->check('presale')) {
            $presale = $session->read('presale');

            $this->loadModel('ArticlesStores');
            $this->loadModel('Products');
            $this->loadModel('Debts');

            foreach ($presale->products as $productx) {

                //busco el articulo en los depositos
                $articleStores = $this->ArticlesStores->get($productx->product_article_store_id);

                //si la cantidad en deposito no es la suficiente
                if ($articleStores->current_amount < $productx->amount) {
                    $productx->message_error = 'La cantidad excede los articulos disponibles.';
                    array_push($presale->products_error, $productx);
                }

                //descuento la cantidad vendida
                $articleStores->current_amount -= $productx->amount;

                //actualizo el deposito
                if (!$this->ArticlesStores->save($articleStores)) {
                    $productx->message_error = 'Error al actualizar el articulo.';
                    array_push($presale->products_error, $productx);
                }

                $product = $this->Products->get($productx->id);
                $customer = $this->Debts->Customers->get($presale->customer->code);

                //log de accion

                $action =  'Venta de Producto';
                $detail =  'Producto: ' . $product->name . PHP_EOL;
                $detail .= 'Cantidad: ' . $productx->amount . PHP_EOL;
                $detail .= 'Cuotas: ' . $productx->dues . PHP_EOL;
                $discount = $productx->discount_name ? h($productx->discount_name) . ' ' . ($productx->discount_total * 100) . '%' : '';
                $detail .= 'Descuento: ' . $discount . PHP_EOL;
                $this->registerActivity($action, $detail, $customer->code);

                $ok = $this->CurrentAccount->addDebtProduct([
                    'amount'        => $productx->amount,
                    'customer'      => $customer,
                    'product'       => $product,
                    'duedate'       => $productx->duedate,
                    'period'        => $productx->period,
                    'dues'          => $productx->dues,
                    'presale_id'    => $presale->id,
                    'discount_id'   => $productx->discount_id,
                    'presale_id'    => $presale->id,
                    'connection_id' => $productx->connection_id, //null -> si no tiene asociado el pago a una conexion
                    'redirect'      => ['controller' => 'presales', 'action' => 'add'],
                ]);

                if (!$ok) {
                    $productx->message_error = "Error al generar la Deuda.";
                    array_push($presale->products_error, $productx);
                    $session->write('presale', $presale);
                }   
            }
        }
    }

    private function loadServices()
    {
        $session = $this->request->getSession();

        if ($session->check('presale')) {
            $presale = $session->read('presale');
            $this->loadModel('ServicePending');
            $this->loadModel('AutoDebits');
            $this->loadModel('Tickets');

            foreach ($presale->services as $servicex) {

                $service_pending = $this->ServicePending->newEntity();
                $service_pending->presale_id = $presale->id;
                $service_pending->service_id = isset($servicex) ? $servicex->id : null;
                $service_pending->discount_id = isset($servicex->discount_id) ? $servicex->discount_id : null;
                $service_pending->customer_code = $presale->customer->code;
                $service_pending->lat = $servicex->lat;
                $service_pending->lng = $servicex->lng;
                $service_pending->installation_date = $servicex->installation_date;
                $service_pending->availability = $servicex->availability;
                $service_pending->service_name = $servicex->name;
                $service_pending->service_total = $servicex->service_total;
                $service_pending->discount_name = $servicex->discount_name;
                $service_pending->discount_total = $servicex->discount_total;
                $service_pending->address = $servicex->address;
                $service_pending->ticket_id = $presale->ticket_id;
                $service_pending->payment_connection_id = null;

                if ($this->ServicePending->save($service_pending)) {

                    //log de accion

                    $action =  'Generación de Servicio pendiente desde Venta';
                    $detail =  'Servicio: ' . $servicex->name . PHP_EOL;
                    $detail =  'Fecha de instalación: ' . $servicex->installation_date . PHP_EOL;
                    $detail =  'Disponibilidad: ' . $service_pending->availability . PHP_EOL;
                    $detail .= 'Total: ' . $servicex->service_total . PHP_EOL;
                    $detail .= 'Domicilio: ' . $servicex->address . PHP_EOL;
                    $detail .= 'Descuento: ' . $servicex->discount_name . PHP_EOL;
                    $this->registerActivity($action, $detail, $presale->customer->code);

                    if ($servicex->generate_ticket) {

                        $ticket = $this->Tickets->newEntity();
                        $ticket->service_pending_id = $service_pending->id;
                        $ticket->title = "Nueva instalación. Servicio: " . $service_pending->service_name . ". Disponibilidad: " . $service_pending->availability;
                        $ticket->description = "Ticket generado desde Ventas.";

                        //estado abierto
                        $ticket->status = 1;

                        //para asignar a alguien la instalacion es el mismo que realizo el flujo de venta
                        $ticket->user_id = $this->Auth->user()['id'];
                        $ticket->asigned_user_id = NULL;
                        $ticket->customer_id = $presale->customer->code;

                        //categoria instalacion
                        $ticket->category = 2;

                        //la fecha de inicio es la fecha que se pacto en la venta para la instalacion
                        $ticket->start_task = $service_pending->installation_date;
                        $this->Tickets->save($ticket);
                    }

                } else {
                    $servicex->message_error = "Error al guardar en servicio pendiente.";
                    array_push($presale->services_error, $servicex);
                    $session->write('presale', $presale);
                }
            }
        }
    }

    public function addCobrodgital()
    {
        $session = $this->request->getSession();

        if ($session->check('presale')) {
            $presale = $session->read('presale');
        }   
    }

    public function showprint($id) 
    {
        $presale = $this->Presales->get($id, ['contain' => ['Debts.CustomersHasDiscounts', 'Users', 'Customers.Areas','Customers.Cities', 'ServicePending']]);
        $this->CreatePresalePdf->generate($presale);
    }

    public function addCustomer()
    {
        if ($this->request->is('ajax')) {
            $paraments = $this->request->getSession()->read('paraments');

            $session = $this->request->getSession();
            if ($session->check('presale')) {
                $customer = $this->request->input('json_decode')->customer;
                //$this->log($customer, 'debug');
                $customer = $this->Customers->get($customer->code, ['contain' => [
                    'Areas',
                    'Connections.Services',
                    'ServicePending.Services',
                    'ServicePending.Presales.Users',
                    'Labels']
                ]);

                $presale = $session->read('presale');
                
                //$this->log($customer, 'debug');
                
                $presale->customer = $customer;

                $session->write('presale', $presale);
                $data['message'] = "Se ha agregado correctamente el cliente.";
            }
        }
        $this->set('data', $data);
    }

    public function comodato()
    {
        if ($this->request->is('post')) {

            $folder = new Folder(WWW_ROOT . 'comodato', true, 0755);
            if ($this->request->getData('upload_comodato.name')) {

                $file = $this->request->getData('upload_comodato');
                $from = $file['tmp_name'];
                $to = WWW_ROOT . 'comodato/comodato.pdf';

                if (move_uploaded_file($from, $to)) {
                   $this->Flash->success(__('El archivo cargado correctamente.'));
                } else {
                    $this->Flash->error(__('Error al intentar subir el archivo cargado correctamente.'));
                }
            }
        }
    }

    public function downloadComodato()
    {
        $folder = new Folder(WWW_ROOT . 'comodato', true, 0755);
        if (file_exists(WWW_ROOT . 'comodato/comodato.pdf')) {
            $this->response->file(WWW_ROOT . 'comodato/comodato.pdf', array(
                'download' => true,
                'name' => 'comodato.pdf',
            ));
            return $this->response;
        } else {
            $this->Flash->warning(__('No se encuentra cargado el archivo comodato.pdf.'));
        }
        return $this->redirect(['action' => 'comodato']);
    }
}
