<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Cities Controller
 *
 * @property \App\Model\Table\CitiesTable $Cities
 *
 * @method \App\Model\Entity\City[] paginate($object = null, array $settings = [])
 */
class CitiesController extends AppController
{
    public function isAuthorized($user = null) 
    {
        return parent::allowRol($user['id']);
    }

    public function index()
    {
        $cities = $this->Cities->find()->contain(['Provinces']);

        $provinces = $this->Cities->Provinces->find('list');

        $this->set(compact('cities', 'provinces'));
        $this->set('_serialize', ['cities']);
    }

    public function add()
    {
        $city = $this->Cities->newEntity();

        if ($this->request->is('post')) {
            
            $action = 'Agregado de Ciudad';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $city = $this->Cities->patchEntity($city, $this->request->getData());

            if ($this->Cities->save($city)) {

                $this->loadModel('Provinces');
                $province = $this->Provinces->get($city->province_id);

                $action = 'Ciudad Agregada';
                $detail = '';
                $detail .= 'Habilitada: ' . ($city->enabled ? 'Si' : 'No') . PHP_EOL;
                $detail .= 'Nombre: ' . $city->name;
                $detail .= 'CP: ' . $city->cp;
                $detail .= 'Provincia: ' . $province->name;
                $this->registerActivity($action, $detail, NULL);

                $this->Flash->success(__('Ciudad agregada correctamente.'));

                return $this->redirect(['action' => 'add']);
            }
            $this->Flash->error(__('Error al intentar agrear la ciudad.'));
        }

        $provinces = $this->Cities->Provinces->find('list')->where(['enabled' => TRUE]);

        $this->set(compact('city', 'provinces'));
        $this->set('_serialize', ['city']);
    }

    /**
     * Edit method
     *
     * @param string|null $id City id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Edición de Ciudad';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $city = $this->Cities->get($this->request->getData('id'), [
                'contain' => []
            ]);

            $city = $this->Cities->patchEntity($city, $this->request->getData());
            if ($this->Cities->save($city)) {
                $this->loadModel('Provinces');
                $province = $this->Provinces->get($city->province_id);
                $action = 'Ciudad Editada';
                $detail = '';
                $detail .= 'Habilitada: ' . ($city->enabled ? 'Si' : 'No') . PHP_EOL;
                $detail .= 'Nombre: ' . $city->name;
                $detail .= 'CP: ' . $city->cp;
                $detail .= 'Provincia: ' . $province->name;
                $this->registerActivity($action, $detail, NULL);
                $this->Flash->success(__('Ciuadad editada correctamente.'));
                
            } else {
                $this->Flash->error(__('Error al inetnatar editar la ciudad.'));
            }

            return $this->redirect(['action' => 'index']);
        }
    }
}
