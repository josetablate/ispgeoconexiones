<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Stores Controller
 *
 * @property \App\Model\Table\StoresTable $Stores
 */
class StoresController extends AppController
{
    public function isAuthorized($user = null)
    {
        return parent::allowRol($user['id']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $stores = $this->Stores->find()->contain(['Users'])->where(['Stores.deleted' => false]);

        $parament = $this->request->getSession()->read('paraments');

        $users = $this->Stores->Users->find('list', ['limit' => 200])
        ->where(['id NOT IN' => $parament->system->users]);

        $this->set(compact('stores','users'));
        $this->set('_serialize', ['stores']);
    }

    /**
     * View method
     *
     * @param string|null $id Store id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $store = $this->Stores->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('store', $store);
        $this->set('_serialize', ['store']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $store = $this->Stores->newEntity();
        if ($this->request->is('post')) {

            $action = 'Agregado de Depósito';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            if ($this->Stores->find()
                ->where(['name' => $this->request->getData('name'),
                        'deleted' => false])
                ->first()) {
                $this->Flash->error(__('Existe un depósito con este nombre.'));
                return $this->redirect(['action' => 'add']);
            }

            if ($this->Stores->find()
                ->where(['user_id' => $this->request->getData('user_id'),
                        'deleted' => false])
                ->first()) {
                $this->Flash->error(__('EL usuario seleccionado ya tiene un depósito.'));
                return $this->redirect(['action' => 'add']);
            }

            $this->loadModel('Users');
            $user = $this->Users->get($this->request->getData('user_id'));

            $store = $this->Stores->patchEntity($store, $this->request->getData());
            if ($this->Stores->save($store)) {
                $action = 'Depósito agregado';
                $detail = '';
                $detail .= 'Nombre: ' . $store->name . PHP_EOL;
                $detail .= 'Domicilio: ' . $store->address . PHP_EOL;
                $detail .= 'Ususario: ' . $user->username . PHP_EOL;
                $detail .= 'Nombre del usuario: ' . $user->name . PHP_EOL;
                $this->registerActivity($action, $detail, NULL);
                $this->Flash->success(__('Depósito agregados.'));
                return $this->redirect(['action' => 'add']);
            } else {
                $this->Flash->error(__('Error al agregar los datos.'));
            }
        }

        $parament = $this->request->getSession()->read('paraments');

        $users = $this->Stores->Users->find('list', ['limit' => 200])
        ->where(['id NOT IN' => $parament->system->users, 'deleted' => false, 'enabled' => true]);

        $this->set(compact('store', 'users'));
        $this->set('_serialize', ['store']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Store id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $action = 'Edición de Depósito';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $store = $this->Stores->get($_POST['id'], [
                'contain' => ['Users']
            ]);

            if ($this->Stores->find()
                ->where(['name' => $this->request->getData('name'),
                        'deleted' => false,
                        'id !=' => $store->id])
                ->first()) {
                $this->Flash->error(__('Existe un depósito con el mismo nombre.'));
                return $this->redirect(['action' => 'index']);
            }

            if ($this->Stores->find()
                ->where(['user_id' => $this->request->getData('user_id'),
                        'deleted' => false,
                        'id !=' => $store->id])
                ->first()) {
                $this->Flash->error(__('El usuario seleccionado, ya tiene un despósito asignado.'));
                return $this->redirect(['action' => 'index']);
            }

            $store = $this->Stores->patchEntity($store, $this->request->getData());
            if ($this->Stores->save($store)) {
                $action = 'Depósito editado';
                $detail = '';
                $detail .= 'Nombre: ' . $store->name . PHP_EOL;
                $detail .= 'Domicilio: ' . $store->address . PHP_EOL;
                $detail .= 'Ususario: ' . $store->user->username . PHP_EOL;
                $detail .= 'Nombre del usuario: ' . $store->user->name . PHP_EOL;
                $this->registerActivity($action, $detail, NULL);
                $this->Flash->success(__('Datos actualizados.'));
            } else {
                $this->Flash->error(__('Error al actualizar los datos.'));
            }

            return $this->redirect(['action' => 'index']);
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Store id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete()
    {
        $action = 'Eliminación de Depósito';
        $detail = '';
        $this->registerActivity($action, $detail, NULL, TRUE);

        $this->request->allowMethod(['post', 'delete']);
        $store = $this->Stores->get($_POST['id'], [
            'contain' => ['Users']
        ]);

        $this->loadModel('ArticlesStores');
        $this->loadModel('Articles');

        $articlesStores = $this->ArticlesStores->find()->where(['current_amount >' => 0, 'store_id' => $_POST['id']]);

        $count = 0;

        foreach ($articlesStores as $as) {

            $stock = $this->Articles->get($as->article_id);

            if ($stock && !$stock->deleted) {
                $count++;
            }
        }

        if ($count > 0) {
            $this->Flash->error(__('El depósito tiene ' . $count . ' artículos. Debe mover los artículos a otro depósito.'));
            return $this->redirect(['action' => 'index']);
        }

        $store->deleted = TRUE;
        $store->user_id = NULL;
        $store_name = $store->name;
        $store_username = $store->user->username;
        $store_name_user = $store->user->name;

        if ($this->Stores->save($store)) {
            $action = 'Depósito eliminado';
            $detail = '';
            $detail .= 'Nombre: ' . $store_name . PHP_EOL;
            $detail .= 'Ususario: ' . $store_username . PHP_EOL;
            $detail .= 'Nombre del usuario: ' . $store_name_user . PHP_EOL;
            $this->registerActivity($action, $detail, NULL);
            $this->Flash->success(__('Depósito eliminado.'));
        } else {
            $this->Flash->error(__('Error al eliminar el depósito.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
