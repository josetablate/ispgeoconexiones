<?php
namespace App\Controller;

use App\Controller\IspControllerMikrotik;


abstract class IspControllerMikrotikDhcp extends IspControllerMikrotik
{
     
    public function initialize()
    {
        parent::initialize();        
    
    }   
          
     
    //CONFIGUARATIONS 
     
     
     //dhcp server
    
    abstract protected function getDhcpServersByController();   
    abstract protected function addDhcpServer();  
    abstract protected function editDhcpServer();
    abstract protected function deleteDhcpServer();
//     abstract protected function syncDhcpServer($id);     
    
     
     //networks
   
    abstract protected function getNetworksByController();   
    abstract protected function addNetwork();   
    abstract protected function deleteNetwork();
    abstract protected function getRangeNetwork();
    abstract protected function editNetwork();    
    abstract protected function moveNetwork();    
    
    
    //gateways
    
    abstract protected function getGatewaysByController();    
    abstract protected function addGateway();   
    abstract protected function deleteGateway();     
    abstract protected function editGateway();
    
    
    //pools
   
    abstract protected function getPoolsByController();
    abstract protected function addPool();
    abstract protected function deletePool();
    abstract protected function editPool();
    
    
    //profiles
  
    abstract protected function getProfilesByController();
    abstract protected function addProfile();
    abstract protected function deleteProfile(); 
    abstract protected function editProfile(); 
     
     
     
    //SYNCRONIZATIONS  
     
    //sync DHCP Servers

    abstract protected function syncDhcpServers(); 
    abstract protected function syncDhcpServerIndi();  
    abstract protected function refreshDhcpServers();  
    abstract protected function deleteDhcpServerInController();    
    abstract protected function diffDhcpServers($controller);     
    abstract protected function clearDhcpServers();    


    //sync queues

    abstract protected function syncQueues();  
    abstract protected function syncQueueIndi();
    abstract protected function refreshQueues();  
    abstract protected function deleteQueueInController();  
    abstract protected function diffQueues($controller, $file);    
    abstract protected function clearQueues();   
    
    
    // sync address list

    abstract protected function syncAddressLists();  
    abstract protected function syncAddressListIndi();
    abstract protected function refreshAddressLists(); 
    abstract protected function deleteAddressListInController();    
    abstract protected function diffAddressLists($controller, $file);    
    abstract protected function clearAddressLists(); 
    
    
    
    // sync networks

    abstract protected function syncNetworks(); 
    abstract protected function syncNetworkIndi(); 
    abstract protected function refreshNetworks(); 
    abstract protected function deleteNetworksInController();  
    abstract protected function diffNetworks($controller);    
    abstract protected function clearNetworks();    
    
    
    //pools
    
    abstract protected function syncPools();  
    abstract protected function syncPoolIndi();
    abstract protected function refreshPools(); 
    abstract protected function deletePoolsInController();    
    abstract protected function diffPools($controller);     
    abstract protected function clearPools();   

    
    
    //gateways
    
    abstract protected function syncGateways();  
    abstract protected function syncGatewayIndi(); 
    abstract protected function refreshGateways(); 
    abstract protected function deleteGatewayInController();    
    abstract protected function diffGateways($controller);     
    abstract protected function clearGateways();
   
    
    
    //leases
    
    abstract protected function syncLeases();  
    abstract protected function syncLeaseIndi(); 
    abstract protected function refreshLeases();  
    abstract protected function deleteLeaseInController();    
    abstract protected function diffLeases($controller, $file);    
    abstract protected function clearLeases();
    
  
}

















