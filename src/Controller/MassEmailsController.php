<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\Component\Admin\CreatePresalePdf;
use Cake\I18n\Time;
use Cake\Event\Event;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\Filesystem\Folder;
use Cake\Mailer\Email;
use Cake\Event\EventManager;

/**
 * MassEmails Controller
 *
 * @property \App\Model\Table\MassEmailTable $MassEmails
 */
class MassEmailsController extends AppController
{
    public function isAuthorized($user = null)
    {
        if ($this->request->getParam('action') == 'getMessages') {
            return true;
        }

        if ($this->request->getParam('action') == 'getCustomers') {
            return true;
        }

        if ($this->request->getParam('action') == 'getCustomersWithArchived') {
            return true;
        }

        if ($this->request->getParam('action') == 'sendEmailTest') {
            return true;
        }

        if ($this->request->getParam('action') == 'sendEmail') {
            return true;
        }

        if ($this->request->getParam('action') == 'sendEmailAccountSummary') {
            return true;
        }

        if ($this->request->getParam('action') == 'sendEmailReceipt') {
            return true;
        }

        if ($this->request->getParam('action') == 'getComprobantes') {
            return true;
        }

        if ($this->request->getParam('action') == 'sendEmailPostFacturacion') {
            return true;
        }

        if ($this->request->getParam('action') == 'getEmails') {
            return true;
        }

        if ($this->request->getParam('action') == 'validateEmailsAccounts') {
            return true;
        }

        if ($this->request->getParam('action') == 'createLinkPagos') {
            return true;
        }

		if ($this->request->getParam('action') == 'verifyMassEmails') {
            return true;
        }

		if ($this->request->getParam('action') == 'serverSideInvoicesComprobantes') {
            return true;
        }

		if ($this->request->getParam('action') == 'serverSideCreditNotesComprobantes') {
            return true;
        }

		if ($this->request->getParam('action') == 'serverSideDebitNotesComprobantes') {
            return true;
        }

		if ($this->request->getParam('action') == 'serverSideReceiptsComprobantes') {
            return true;
        }

		if ($this->request->getParam('action') == 'serverSideComprobantes') {
            return true;
        }

        if ($this->request->getParam('action') == 'serverSidePresupuestosComprobantes') {
            return true;
        }

        if ($this->request->getParam('action') == 'sendConfimationEmail') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function initialize()
    {
        parent::initialize();

        $this->loadModel('MassEmailsTemplates');

        $this->loadComponent('CurrentAccount', [
            'className' => '\App\Controller\Component\Admin\CurrentAccount'
        ]);

        $this->loadComponent('CreatePresalePdf', [
            'className' => '\App\Controller\Component\Admin\CreatePresalePdf'
        ]);
        
        $this->loadComponent('FiscoAfipCompPdf', [
             'className' => '\App\Controller\Component\Admin\FiscoAfipCompPdf'
        ]);

        $this->initEventManager();
    }

    public function initEventManager()
    {
        EventManager::instance()->on(
            'MassEmailsController.Error',
            function($event, $msg, $data, $flash = false) {

                $this->loadModel('ErrorLog');
                $log = $this->ErrorLog->newEntity();
                $log->user_id = $this->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Error';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->ErrorLog->save($log);

                if ($flash) {
                    $this->Flash->error(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'MassEmailsController.Warning',
            function ($event, $msg, $data, $flash = false) {

                $this->loadModel('ErrorLog');
                $log = $this->ErrorLog->newEntity();
                $log->user_id = $this->request->getSession()->read('Auth.User')['id'];
                $log->type = 'Warning';
                $log->msg = __($msg);
                $log->data = json_encode($data, JSON_PRETTY_PRINT);
                $log->event = $event->getName();
                $this->ErrorLog->save($log);

                if ($flash) {
                    $this->Flash->warning(__($msg));
                }
            }
        );

        EventManager::instance()->on(
            'MassEmailsController.Log',
            function ($event, $msg, $data) {

            }
        );

        EventManager::instance()->on(
            'MassEmailsController.Notice',
            function ($event, $msg, $data) {

            }
        );
    }

    private function validateEmailsAccounts()
    {
        $paraments = $this->request->getSession()->read('paraments');

        if (count($paraments->mass_emails->emails) < 1) {
            $this->Flash->default(__('Debes contar con al menos una Cuenta de Correo. Por favor verifique la configuración.'));
            return $this->redirect(['action' => 'config']);
        }

        $flag = TRUE;

        foreach ($paraments->mass_emails->emails as $email) {
            if ($email->enabled) {
                $flag = FALSE;
                break;
            }
        }

        if ($flag) {
            $this->Flash->default(__('De tener al menos una Cuenta de Correo habilitada. Por favor verifique la configuración.'));
            return $this->redirect(['action' => 'config']);
        }
    }

    public function index()
    {
        $this->validateEmailsAccounts();

        $templates = $this->MassEmailsTemplates->find();

        $this->set(compact('templates'));
        $this->set('_serialize', ['templates']);
    }

    public function config()
    {
        $paraments = $this->request->getSession()->read('paraments');

        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Edición Configuración de Correo';
            $detail = "";
            $this->registerActivity($action, $detail, NULL, TRUE);

            if ($this->request->getData('limit') > 0) {

                $paraments->mass_emails->limit = $this->request->getData('limit');

                //se guarda los cambios
                $this->saveGeneralParaments($paraments);

                $action = 'Cuenta Guardada' . PHP_EOL;
                $detail = 'Límite: ' . $paraments->mass_emails->limit . PHP_EOL;
                $this->registerActivity($action, $detail, NULL);
            } else {

                $this->Flash->warning(__('El límite debe ser mayor a cero.'));
            }
        }

        $this->set(compact('paraments'));
        $this->set('_serialize', ['paraments']);
    }

    public function getEmails()
    {
        $parament = $this->request->getSession()->read('paraments');

        $emails = $parament->mass_emails->emails;

        $this->set(compact('emails'));
        $this->set('_serialize', ['emails']);
    }

    public function addEmail()
    {
        $paraments = $this->request->getSession()->read('paraments');

        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Agregado Cuenta de Correo';
            $detail = "";
            $this->registerActivity($action, $detail, NULL, TRUE);

            foreach ($paraments->mass_emails->emails as $email) {
                if ($email->contact == $this->request->getData('contact')) {
                    $this->Flash->warning(__('Ya existe una Cuenta de Correo: ' . $this->request->getData('contact')));
                    return $this->redirect(['action' => 'config']);
                }
            }

            $account = new \stdClass;
            $account->id         = Time::now()->toUnixString();
            $account->contact    = $this->request->getData('contact');
            $account->copy       = $this->request->getData('copy');
            $account->user       = $this->request->getData('user');
            $account->pass       = $this->request->getData('pass');
            $account->class_name = $this->request->getData('class_name');
            $account->host       = $this->request->getData('host');
            $account->port       = $this->request->getData('port');
            $account->timeout    = $this->request->getData('timeout');
            $account->client     = $this->request->getData('client');
            $account->tls        = $this->request->getData('tls');
            $account->enabled    = $this->request->getData('enabled');

            $paraments->mass_emails->emails[] = $account;

            $action = 'Cuenta Guardada' . PHP_EOL;
            $detail = 'Habilitar: ' . ($account->enabled ? 'Si' : 'No') . PHP_EOL;
            $detail .= 'Cuenta: ' . $account->contact . PHP_EOL;
            $detail .= 'Copia: ' . $account->copy . PHP_EOL;
            $this->registerActivity($action, $detail, NULL);

            //se guarda los cambios
            $this->saveGeneralParaments($paraments);
            
            return $this->redirect(['action' => 'config']);
        }

        $this->set(compact('paraments'));
        $this->set('_serialize', ['paraments']);
    }

    public function editEmail()
    {
        $paraments = $this->request->getSession()->read('paraments');

        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Edición Cuenta de Correo';
            $detail = "";
            $this->registerActivity($action, $detail, NULL, TRUE);

            foreach ($paraments->mass_emails->emails as $email) {

                if ($email->id != $this->request->getData('id')
                    && $email->contact == $this->request->getData('contact')) {

                    $this->Flash->warning(__('Ya existe una Cuenta de Correo: ' . $this->request->getData('contact')));
                    return $this->redirect(['action' => 'config']);
                }
            }

            foreach ($paraments->mass_emails->emails as $email) {

                if ($email->id == $this->request->getData('id')) {

                    $email->contact    = $this->request->getData('contact');
                    $email->copy       = $this->request->getData('copy');
                    $email->user       = $this->request->getData('user');
                    $email->class_name = $this->request->getData('class_name');
                    $email->pass       = $this->request->getData('pass');
                    $email->host       = $this->request->getData('host');
                    $email->port       = $this->request->getData('port');
                    $email->timeout    = $this->request->getData('timeout');
                    $email->client     = $this->request->getData('client');
                    $email->tls        = $this->request->getData('tls');
                    $email->enabled    = $this->request->getData('enabled') ? TRUE : FALSE;

                    $action = 'Cuenta Guardada' . PHP_EOL;
                    $detail = 'Habilitar: ' . ($email->enabled ? 'Si' : 'No') . PHP_EOL;
                    $detail .= 'Cuenta: ' . $email->contact . PHP_EOL;
                    $detail .= 'Copia: ' . $email->copy . PHP_EOL;
                    $this->registerActivity($action, $detail, NULL);
                }
            }
        }

        //se guarda los cambios
        $this->saveGeneralParaments($paraments);
        return $this->redirect(['action' => 'config']);
    }

    public function addTemplate()
    {
        $template = $this->MassEmailsTemplates->newEntity();
        $paraments = $this->request->getSession()->read('paraments');

        $business = [];
        $business[''] = 'Seleccione Empresa';
        foreach ($paraments->invoicing->business as $b) {
            $business[$b->id] = $b->name;
        }

        $emails = [];
        $emails[''] = 'Seleccione Cuenta de Correo';
        foreach ($paraments->mass_emails->emails as $email) {
            if ($email->enabled) {
                $emails[$email->id] = $email->contact;
            }
        }

        if ($this->request->is('post')) {

            $action = 'Agregado Plantilla Correo';
            $detail = "";
            $this->registerActivity($action, $detail, NULL, TRUE);

            $template = $this->MassEmailsTemplates->find()->where(['name' => $this->request->getData('name')])->first();

            if ($template) {
                $this->Flash->warning(__('Ya existe una Plantilla con este Nombre'));
                return $this->redirect(['action' => 'addTemplate']);
            }
            $template = $this->MassEmailsTemplates->newEntity();
            $template = $this->MassEmailsTemplates->patchEntity($template, $this->request->getData());

            if ($this->MassEmailsTemplates->save($template)) {

                $business_name = "";

                foreach ($paraments->invoicing->business as $b) {
                    if ($b->id == $template->business_billing) {
                        $business_name = $b->name . ' (' . $b->address . ')';
                    }
                }

                $action = 'Plantilla Correo Guardada';
                $detail = "";
                $detail .= "Nombre: " . $template->name . PHP_EOL;
                $detail .= "Asunto: " . $template->subject . PHP_EOL;
                $detail .= "Descripción: " . $template->description . PHP_EOL;
                $detail .= "Mensaje: " . $template->messsage . PHP_EOL;
                $detail .= "Habilitado: " . ($template->enabled ? 'Si' : 'No') . PHP_EOL;
                $detail .= "Empresa: " . $business_name. PHP_EOL;
                $detail .= "Adjuntar Factura: " . ($template->invoice_attach ? 'Si' : 'No') . PHP_EOL;
                $detail .= "Adjuntar Recibo: " . ($template->receipt_attach ? 'Si' : 'No') . PHP_EOL;
                $detail .= "Adjuntar Resumen de Cuenta: " . ($template->ccount_summary_attach ? 'Si' : 'No') . PHP_EOL;
                $this->registerActivity($action, $detail, NULL);
                $this->Flash->success(__('Se ha guardado correctamente la Plantilla.'));

                $this->Flash->success(__('Se ha guardado correctamente la Plantilla.'));
                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(__('No se ha podido guardar la Plantilla. Por favor, intente nuevamente.'));
        }

        $this->set(compact('template', 'business', 'emails'));
        $this->set('_serialize', ['template']);
    }

    public function editTemplate($id)
    {
        $template = $this->MassEmailsTemplates->get($id);
        $paraments = $this->request->getSession()->read('paraments');

        $business = [];
        $business[''] = 'Seleccione Empresa';
        foreach ($paraments->invoicing->business as $b) {
            $business[$b->id] = $b->name;
        }

        $emails = [];
        $emails[''] = 'Seleccione Cuenta de Correo';
        foreach ($paraments->mass_emails->emails as $email) {
            if ($template->email_id == $email->id 
                || $email->enabled) {

                $emails[$email->id] = $email->contact;
            }
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Edición Plantilla Correo';
            $detail = "";
            $this->registerActivity($action, $detail, NULL, TRUE);

            if ($this->MassEmailsTemplates->find()->where(['name' => $this->request->getData('name'), 'id !=' => $id])->first()) {
               $this->Flash->warning(__('Existe un template con este Nombre.'));
            } else {
                $template = $this->MassEmailsTemplates->patchEntity($template, $this->request->getData());

                if ($this->MassEmailsTemplates->save($template)) {

                    $business_name = "";

                    foreach ($paraments->invoicing->business as $b) {
                        if ($b->id == $template->business_billing) {
                            $business_name = $b->name . ' (' . $b->address . ')';
                        }
                    }

                    $action = 'Plantilla Correo Guardada';
                    $detail = "";
                    $detail .= "Nombre: " . $template->name . PHP_EOL;
                    $detail .= "Asunto: " . $template->subject . PHP_EOL;
                    $detail .= "Descripción: " . $template->description . PHP_EOL;
                    $detail .= "Mensaje: " . $template->messsage . PHP_EOL;
                    $detail .= "Habilitado: " . ($template->enabled ? 'Si' : 'No') . PHP_EOL;
                    $detail .= "Empresa: " . $business_name. PHP_EOL;
                    $detail .= "Adjuntar Factura: " . ($template->invoice_attach ? 'Si' : 'No') . PHP_EOL;
                    $detail .= "Adjuntar Recibo: " . ($template->receipt_attach ? 'Si' : 'No') . PHP_EOL;
                    $detail .= "Adjuntar Resumen de Cuenta: " . ($template->ccount_summary_attach ? 'Si' : 'No') . PHP_EOL;
                    $this->registerActivity($action, $detail, NULL);
                    $this->Flash->success(__('Se ha guardado correctamente la Plantilla.'));

                    $this->Flash->success(__('Datos actualizados.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('Error al actualizar los datos.'));
                }
            }
        }

        $this->set(compact('template', 'business', 'emails'));
        $this->set('_serialize', ['template']);
    }

    public function enableTemplate()
    {
        $template = $this->MassEmailsTemplates->get($_POST['id']);

        $template->enabled = true;

        if ($this->MassEmailsTemplates->save($template)) {

            $action = 'Habilitación Plantilla Correo';
            $detail = "";
            $detail .= "Nombre: " . $template->name;
            $this->registerActivity($action, $detail, NULL, TRUE);

            $this->Flash->success(__('La Plantilla ' . $template->name . ' se encuentra habilitado'));
        } else {
            $this->Flash->error(__('Error al habilitar la Plantilla: ' . $template->name . '. Por favor, intente nuevamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function disableTemplate()
    {
        $template = $this->MassEmailsTemplates->get($_POST['id']);

        $template->enabled = false;

        if ($this->MassEmailsTemplates->save($template)) {

            $action = 'Deshabilitación Plantilla Correo';
            $detail = "";
            $detail .= "Nombre: " . $template->name;
            $this->registerActivity($action, $detail, NULL, TRUE);

            $this->Flash->success(__('La Plantilla ' . $template->name . ' se encuentra deshabilitado'));
        } else {
            $this->Flash->error(__('Error al deshabilitar la Plantilla: ' . $template->name . '. Por favor, intente nuevamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function selectCustomers($template_id = NULL)
    {
        $this->validateEmailsAccounts();

        $template = null;
        $paraments = $this->request->getSession()->read('paraments');
        $payment_getway = $this->request->getSession()->read('payment_getway');

        $business = [];
        $business[''] = 'Seleccione Empresa';
        foreach ($paraments->invoicing->business as $b) {
            $business[$b->id] = 'Empresa: ' . $b->name . ' (' . $b->address . ')';
        }

        $areas = [];
        $this->loadModel('Areas');
        $areas_model = $this->Areas->find();
        foreach ($areas_model as $a) {
            $areas[$a->id] = $a->name;
        }

        $controllers = [];
        $this->loadModel('Controllers');
        $controllers_model = $this->Controllers
            ->find()
            ->where([
        		'enabled' => TRUE,
        		'deleted' => FALSE
        	]);
        foreach ($controllers_model as $c) {
            $controllers[$c->id] = $c->name;
        }

        $services = [];
        $this->loadModel('Services');
        $services_model = $this->Services->find();
        foreach ($services_model as $s) {
            $services[$s->id] = $s->name;
        }

        $accounts = [];
        foreach ($payment_getway->methods as $pg) {
            $config = $pg->config;
            if ($payment_getway->config->$config->enabled && $pg->credential) {
                $accounts[$pg->id] = $pg->name;
            }
        }

        if (sizeof($accounts) == 0) {
            $accounts[""] = "Sin Cuentas";
        }

        $this->loadModel('MassEmailsBlocks');
        $block_next_id = $this->MassEmailsBlocks->find()->select(['id'])->order(['id' => 'DESC'])->first();
        $block_next_id = empty($block_next_id) ? 1 : ++$block_next_id->id;

        $templates_model = $this->MassEmailsTemplates
            ->find()
            ->where([
                'enabled' => TRUE
            ]);

        if ($templates_model->count() < 1) {
            $this->Flash->warning('Debe crear una plantilla.');
            return $this->redirect(['action' => 'addTemplate']);
        }

        $templates = [];
        foreach ($templates_model as $tem) {
            $templates[$tem->id] = $tem->name;
            if ($template_id != NULL) {
                if ($tem->id == $template_id) {
                    $template = $tem;
                }
            } else {
                $template = $tem;
            }
        }

        $emails = [];
        $email_account_selected = NULL;
        $emails_model = $paraments->mass_emails->emails;
        foreach ($paraments->mass_emails->emails as $email) {

            if ($template->email_id == $email->id) {
                $email_account_selected = $email;
            }

            if ($email->enabled) {
                $emails[$email->id] = $email->contact;
            }
        }

        $limit = $paraments->mass_emails->limit;

        $this->loadModel('Labels');
        $labels_array = $this->Labels->find()->where(['entity' => 0])->toArray();

        $labels = [];
        $labels[] = [
            'id' => 0,
            'text' => 'Sin etiquetas',
            'entity' => 0,
            'color_back' => '#000000',
            'color_text' => '#000000',
            'enabled' => 1,
        ];

        foreach ($labels_array as $label) {
            $labels[] = $label;
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            $error = $this->isReloadPage();

            if (!$error) {

                $action = 'Envio Masivo de Correos seleccionando Clientes';
                $detail = '';
                $this->registerActivity($action, $detail, NULL, TRUE);

                $this->loadModel('Customers');
                $this->loadModel('MassEmailsSms');
                $this->loadModel('Invoices');
                $this->loadModel('Receipts');

                $data = json_decode($this->request->getData('data'));

                foreach ($paraments->invoicing->business as $b) {
                    if ($b->id == $data->business_billing) {
                        $enterprise = $b;
                    }
                }

                $attach_logo = [];

                if ($data->payment_link_mercadopago || $data->payment_link_cuentadigital) {

                    $logo = $enterprise->logo;

                    if ($logo != "") {

                        $logo_portions = explode(".", $logo);
                        $last_dot = sizeof($logo_portions) - 1;
                        $ext = $logo_portions[$last_dot];

                        $attach_logo = [
                            'file'        => WWW_ROOT . 'img/' . $logo,
                            'mimetype'    => 'image/' . $ext,
                            'contentId'   => $enterprise->id,
                            'disposition' => 'inline'
                        ];
                    }

                    $template = new \stdClass;
                    $template->name = "Link de Pagos";
                    $template->subject = "Link Pago";
                    $template->description = "Link de pago MercadoPago ó Cuenta Digital con Resumen de Cuenta";
                    $template->enabled = TRUE;
                    $template->business_billing = $enterprise->id;
                    $template->invoice_attach = $data->invoice_attach;
                    $template->receipt_attach = $data->receipt_attach;
                    $template->email_id = $data->email_id;
                    $template->account_summary_attach = $data->account_summary_attach;
                } else {
                    foreach ($templates_model as $tem) {
                        if ($tem->id == $data->template_id) {
                            $template = $tem;
                        }
                    }
                }

                $email_data = NULL;
                $flag = FALSE;
                foreach ($paraments->mass_emails->emails as $email) {
                    if ($email->id == $data->email_id) {
                        $email_data = $email;
                    }
                }

                $afip_codes = $this->request->getSession()->read('afip_codes');

                $bloque = $this->MassEmailsBlocks->newEntity();
                $bloque->user_id       = $this->Auth->user()['id'];
                $bloque->template_name = $template->name;
                $bloque->confirm_date  = Time::now();
                $bloque->email_account = $email_data->contact;
                $bloque->status        = '';
                $this->MassEmailsBlocks->save($bloque);

                $bloqueString = "";

                // si usa proveedor de correo
                if ($paraments->emails_accounts->provider == "") {

                    try {
                        Email::configTransport('mass_emails', [
                            'className' => $email_data->class_name,
                            'host'      => $email_data->host,
                            'port'      => $email_data->port,
                            'timeout'   => $email_data->timeout,
                            'username'  => $email_data->user,
                            'password'  => $email_data->pass,
                            'client'    => $email_data->client == "" ? NULL : $email_data->client,
                            'tls'       => $email_data->tls == "" ? NULL : $email_data->tls,
                            'url'       => env('EMAIL_TRANSPORT_DEFAULT_URL', NULL)
                        ]);
                        $flag = TRUE;
                    } catch (\Exception $e) {

                        $data_error = new \stdClass; 
                        $data_error->request_data = $e->getMessage();
                        $data_error->section = 'Envio de correo seleccionando clientes';

                        $event = new Event('MassEmailsController.Error', $this, [
                            'msg'   => __('Hubo un error al enviar los correos.'),
                            'data'  => $data_error,
                            'flash' => true
                        ]);

                        $this->getEventManager()->dispatch($event);
                        Log::write('error', $e->getMessage());
                    }
                } else {
                    $flag = TRUE;
                }

                if ($flag) {

                    $send_email = 0;
                    $error_email = 0;
                    $no_attach_receipts_email = 0;
                    $no_attach_invoices_email = 0;
                    $no_attach_account_summary_email = 0;

                    foreach ($data->ids as $cu_code) {

                        $attachments = [];
                        $connections_invoices = NULL;
                        $invoices_connections = NULL;

                        $customer = $this->Customers->get($cu_code, [
                            'contain' => 'Accounts'
                        ]);

                        $invoice_impagas = FALSE;
                        $periode = NULL;

                        if ($data->invoice_attach) {

                            $where = [];
                            $where['customer_code'] = $cu_code;

                            if ($data->invoice_impagas) {
                                $invoice_impagas = TRUE;
                                $where['paid IS'] = NULL;
                            }

                            if ($data->invoice_periode) {
                                $periode = $data->invoice_periode;
                                $periode = explode("/", $data->invoice_periode);
                                $periode = $periode[1] . '-' . $periode[0];
                                $where['date_start LIKE'] = $periode . '%';
                            }

                            $invoices = $this->Invoices
                                ->find()
                                ->where($where);

                            if ($invoices->count() > 0) {

                                $connections_invoices = [];
                                $invoices_connections = [];

                                foreach ($invoices as $invoice) {

                                    $printed = "";

                                    if (!empty($invoice->connection_id)) {
                                        array_push($connections_invoices, $invoice->connection_id);
                                    }

                                    $printed = $this->FiscoAfipCompPdf->invoice($invoice, TRUE);

                                    if ($printed != "") {
                                        $attach = [
                                            'file'        => $printed,
                                            'mimetype'    => 'application/pdf',
                                            'contentId'   => $invoice->id  . '',
                                            'disposition' => 'attachment'
                                        ];
                                        array_push($attachments, $attach);
                                        array_push($invoices_connections, $invoice->id);
                                    } else {

                                        $no_attach_invoices_email++;

                                        $data_error = new \stdClass; 
                                        $data_error->request_data = $invoice;
                                        $data_error->customer = $customer;
                                        $data_error->section = "Envio de correo seleccionando clientes";

                                        $event = new Event('MassEmailsController.Error', $this, [
                                            'msg'   => __('Hubo un error no se genero dinámicamente el PDF de la Factura. Envio de correo'),
                                            'data'  => $data_error,
                                            'flash' => true
                                        ]);

                                        $this->getEventManager()->dispatch($event);

                                        Log::error('Error attach this -> invoice_id: ' . $invoice->id, ['scope' => ['MassEmailsController']]);
                                        continue;
                                    }
                                }
                            }
                        }

                        $receipt_periode = NULL;

                        if ($data->receipt_attach) {

                            $where = [];
                            $where['customer_code'] = $cu_code;

                            if ($data->receipt_periode) {
                                $receipt_periode = $data->receipt_periode;
                                $receipt_periode = explode("/", $data->receipt_periode);
                                $receipt_periode = $receipt_periode[1] . '-' . $receipt_periode[0];
                                $where['date_start LIKE'] = $receipt_periode . '%';
                            }

                            $receipts = $this->Receipts
                                ->find()
                                ->where($where);

                            if ($receipts->count() > 0) {

                                $connections_receipts = [];
                                $receipts_connections = [];

                                foreach ($receipts as $receipt) {

                                    $printed = "";

                                    if (!empty($receipt->connection_id)) {
                                        array_push($connections_receipts, $receipt->connection_id);
                                    }

                                    $printed = $this->FiscoAfipCompPdf->receipt($receipt, TRUE);

                                    if ($printed != "") {

                                        $attach = [
                                            'file'        => $printed,
                                            'mimetype'    => 'application/pdf',
                                            'contentId'   => $receipt->id  . '',
                                            'disposition' => 'attachment'
                                        ];

                                        array_push($attachments, $attach);
                                        array_push($receipts_connections, $receipt->id);
                                    } else {

                                        $no_attach_receipts_email++;

                                        $data_error = new \stdClass; 
                                        $data_error->request_data = $receipt;
                                        $data_error->customer = $customer;
                                        $data_error->section = "Envio de correo seleccionando clientes";

                                        $event = new Event('MassEmailsController.Error', $this, [
                                            'msg'   => __('Hubo un error no se genero dinámicamente el PDF del Recibo. Envio de correo'),
                                            'data'  => $data_error,
                                            'flash' => true
                                        ]);

                                        $this->getEventManager()->dispatch($event);

                                        Log::error('Error attach this -> receipt_id: ' . $receipt->id, ['scope' => ['MassEmailsController']]);
                                        continue;
                                    }
                                }
                            }
                        }

                        if ($data->account_summary_attach) {

                            $printed = $this->FiscoAfipCompPdf->account_summary($cu_code, TRUE, FALSE, NULL, $paraments->invoicing->add_cobrodigital_account_summary, $paraments->invoicing->add_payu_account_summary, $paraments->invoicing->add_cuentadigital_account_summary);

                            if ($printed != "") {

                                $attach = [
                                    'file'        => $printed,
                                    'mimetype'    => 'application/pdf',
                                    'contentId'   => $cu_code . '',
                                    'disposition' => 'attachment' 
                                ];

                                array_push($attachments, $attach);
                            } else {

                                $no_attach_account_summary_email++;

                                $data_error = new \stdClass; 
                                $data_error->request_data = $customer;
                                $data_error->customer = $customer;
                                $data_error->section = "Envio de correo seleccionando clientes";

                                $event = new Event('MassEmailsController.Error', $this, [
                                    'msg'   => __('Hubo un error no se genero dinámicamente el PDF del Resumen de Cuenta. Envio de correo'),
                                    'data'  => $data_error,
                                    'flash' => TRUE
                                ]);

                                $this->getEventManager()->dispatch($event);

                                Log::error('Error attach this -> customer_code: ' . $cu_code, ['scope' => ['MassEmailsController']]);
                                continue;
                            }
                        }

                        $debts_total = $this->CurrentAccount->calulateCustomerDebtTotal($customer->code);

                        $fantasy_name = $enterprise->name;
                        if (isset($enterprise->fantasy_name)) {
                            if ($enterprise->fantasy_name != "" && $enterprise->fantasy_name != NULL) {
                                $fantasy_name = $enterprise->fantasy_name;
                            }
                        }

                        if ($data->payment_link_mercadopago || $data->payment_link_cuentadigital) {

                            $template->message = $this->createLinkPagos($customer, $attach_logo, $enterprise, $data);

                            if ($template->message) {
                                $temp = $template->message;
                            } else {
                                $error_email++;
                                continue;
                            }

                            array_push($attachments, $attach_logo);
                        } else {
                            $temp = str_replace("%cliente_nombre%", $customer->name, $template->message);
                            $temp = str_replace("%cliente_codigo%", $customer->code, $temp);
                            $doc  = $afip_codes['doc_types'][$customer->doc_type] . ' ' . $customer->ident;
                            $temp = str_replace("%cliente_documento%", $doc, $temp);
                            $temp = str_replace("%cliente_usuario_portal%", $customer->ident, $temp);
                            $temp = str_replace("%cliente_clave_portal%", $customer->clave_portal, $temp);
                            $temp = str_replace("%cliente_saldo_total%", $debts_total, $temp);
                            $temp = str_replace("%cliente_saldo_mes%", $customer->debt_month, $temp);
                            $temp = str_replace("%empresa_nombre%", $fantasy_name, $temp);
                            $temp = str_replace("%empresa_domicilio%", $enterprise->address, $temp);
                            $temp = str_replace("%empresa_web%", $enterprise->web, $temp);
                        }

                        $status = "No enviado";

                        $sms = $this->MassEmailsSms->newEntity();
                        $data_array = [
                            'message'                    => ($data->payment_link_mercadopago || $data->payment_link_cuentadigital) ? 'Resumen de Cuenta' : $temp,
                            'subject'                    => $data->subject,
                            'customer_code'              => $cu_code,
                            'connections_id'             => !empty($connections_invoices) ? implode(",", $connections_invoices) : NULL,
                            'invoices_id'                => !empty($invoices_connections) ? implode(",", $invoices_connections) : NULL,
                            'receipts_id'                => !empty($receipts_connections) ? implode(",", $receipts_connections) : NULL,
                            'bloque_id'                  => $bloque->id,
                            'business_billing'           => $enterprise->id,
                            'invoice_attach'             => $data->invoice_attach,
                            'receipt_attach'             => $data->receipt_attach,
                            'account_summary_attach'     => $data->account_summary_attach,
                            'invoice_impagas'            => $invoice_impagas,
                            'invoice_periode'            => $periode,
                            'receipt_periode'            => $receipt_periode,
                            'status'                     => $status,
                            'email_id'                   => $data->email_id,
                            'payment_link_mercadopago'   => $data->payment_link_mercadopago,
                            'payment_link_cuentadigital' => $data->payment_link_cuentadigital,
                            'template_id'                => $data->template_id,
                            'email'                      => $customer->email,
                            'cd_saldo'                   => isset($data->cd_saldo) ? $data->cd_saldo : NULL,
                            'cd_venc'                    => isset($data->cd_venc) ? $data->cd_venc : NULL,
                            'cd_efectivo'                => isset($data->cd_efectivo) ? $data->cd_efectivo : NULL,
                            'cd_credit_card'             => isset($data->cd_credit_card) ? $data->cd_credit_card : NULL,
                        ];

                        $sms = $this->MassEmailsSms->patchEntity($sms, $data_array);
                        $this->MassEmailsSms->save($sms);

                        //$from = $enterprise->email ? $enterprise->email : $email_data->contact;
                        $from = $email_data->contact;

                        $enterprise_email = $enterprise->web == '' ? $fantasy_name : $enterprise->web;

                        // si usa proveedor de correo
                        if ($paraments->emails_accounts->provider == "") {

                            $email = new Email();
                            $email->transport('mass_emails');

                            $array_email = explode(',', $customer->email);
                            $array_size = sizeof($array_email);

                            if ($array_size == 1) {

                                $email->to([$customer->email => $customer->name]);
                            } else {

                                $email->to([$array_email[0] => $customer->name]);

                                for ($i = 1; $i < $array_size; $i++) {
                                    $email->addTo([$array_email[$i] => $customer->name]);
                                }
                            }

                            $email->from([$from => $enterprise_email]);
                            $email->subject($data->subject);
                            $email->emailFormat('html');
                            $email->template('mass_emails');
                            $email->setViewVars(['message' => $temp]);

                            if (sizeof($attachments) > 0) {
                                $email->attachments($attachments);
                            }

                            try {
                                if ($email->send()) {

                                    $sms->status = "Enviado";
                                    $this->MassEmailsSms->save($sms);
                                    $send_email++;
                                    $customer->last_emails = Time::now();
                                    $this->Customers->save($customer);

                                    $action = 'Envio de Correo exitoso';
                                    $detail = '';
                                    $detail .= 'Código: ' . $customer->code . PHP_EOL;
                                    $detail .= 'Nombre: ' . $customer->name . PHP_EOL;
                                    $detail .= 'Correo: ' . $customer->email . PHP_EOL;
                                    $this->registerActivity($action, $detail, NULL);
                                }
                            } catch (\Exception $e) {
                                $error_email++;

                                $data_error = new \stdClass; 
                                $data_error->request_data = $e->getMessage();
                                $data_error->customer = $customer;
                                $data_error->section = "Envio de correo seleccionando clientes";

                                $event = new Event('MassEmailsController.Error', $this, [
                                    'msg'   => __('Hubo un error al enviar los correos. Envio de correo'),
                                    'data'  => $data_error,
                                    'flash' => true
                                ]);

                                $this->getEventManager()->dispatch($event);

                                Log::write('error', $e->getMessage());
                            }
                        } else {

                            // UTILIZA PROVEEDOR DE CORREO
                            if ($paraments->emails_accounts->provider == 'sendgrid') {

                                $html = '<html><head></head><body>' . $temp . '</body></html>';

                                if ($data->payment_link_mercadopago || $data->payment_link_cuentadigital) {
                                    $html = $temp;
                                }

                                $tos = explode(",", $customer->email);
                                $tos_post = [];
                                foreach ($tos as $to) {
                                    $to_boject = new \stdClass;
                                    $to_boject->email = $to;
                                    $to_boject->name = $customer->name;
                                    array_push($tos_post, $to_boject);
                                }

                                $content = new \stdClass;
                                $content->type = "text/html";
                                $content->value = $html;

                                $from_sendgrid = new \stdClass;
                                $from_sendgrid->email = $from;
                                $from_sendgrid->name = $fantasy_name;

                                $reply_to = new \stdClass;
                                $reply_to->email = $from;
                                $reply_to->name = $fantasy_name;

                                if (isset($email_data->copy)) {
                                    if ($email_data->copy == "") {
                                        $from_copy = $from;
                                        $fantasy_name_copy = $fantasy_name;
                                    } else {
                                        $from_copy = $email_data->copy;
                                        $fantasy_name_copy = "";
                                    }
                                } else {
                                    $from_copy = $from;
                                    $fantasy_name_copy = $fantasy_name;
                                }

                                $bcc = new \stdClass;
                                $bcc->email = $from_copy;
                                $bcc->name = $fantasy_name_copy;

                                $personalizations = [
                                    'to' => $tos_post,
                                    'cc' => [
                                        0 => $bcc
                                    ],
                                    'subject' => $data->subject
                                ];

                                $data_sendgrid = new \stdClass;

                                if (sizeof($attachments) > 0) {

                                    foreach ($attachments as $attach) {

                                        $pdf_file = file_get_contents($attach['file']);
                                        $pdf_explode = explode('/', $attach['file']);
                                        $position_name = sizeof($pdf_explode) - 1;

                                        $attachments_sendgrid = new \stdClass;
                                        $attachments_sendgrid->content = base64_encode($pdf_file);
                                        $attachments_sendgrid->type = $attach['mimetype'];
                                        $attachments_sendgrid->filename = $pdf_explode[$position_name];
                                        $attachments_sendgrid->disposition = $attach['disposition'];
                                        $attachments_sendgrid->content_id = $attach['contentId'];
                                        $data_sendgrid->attachments[] = $attachments_sendgrid;
                                    }
                                }

                                $data_sendgrid->personalizations[] = $personalizations;
                                $data_sendgrid->content[] = $content;
                                $data_sendgrid->from = $from_sendgrid;
                                $data_sendgrid->reply_to = $reply_to;
                                $data_sendgrid->bcc = $bcc;

                                $data_string = json_encode($data_sendgrid);

                                try {

                                    $curl = curl_init();

                                    $endpoint_send = $paraments->emails_accounts->providers->sendgrid->endpoint->api->v3->send;
                                    $api_key = $paraments->emails_accounts->providers->sendgrid->api_key;

                                    curl_setopt_array($curl, array(
                                        CURLOPT_URL            => $endpoint_send,
                                        CURLOPT_RETURNTRANSFER => TRUE,
                                        CURLOPT_ENCODING       => "",
                                        CURLOPT_MAXREDIRS      => 10,
                                        CURLOPT_TIMEOUT        => 30,
                                        CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                                        CURLOPT_CUSTOMREQUEST  => "POST",
                                        CURLOPT_POSTFIELDS     => $data_string,
                                        CURLOPT_SSLVERSION     => CURL_SSLVERSION_TLSv1_2,
                                        CURLOPT_HTTPHEADER => array(
                                            "authorization: Bearer " . $api_key,
                                            "content-type: application/json"
                                        ),
                                    ));

                                    $response = curl_exec($curl);
                                    $err = curl_error($curl);
                                    curl_close($curl);

                                    if ($err) {

                                        $error_email++;

                                        $data_error = new \stdClass;
                                        $data_error->request_data = $err;
                                        $data_error->customer = $customer;
                                        $data_error->section = "Envio de correo seleccionando de clientes";

                                        $event = new Event('MassEmailsController.Error', $this, [
                                            'msg'   => __('Hubo un error al enviar los correos.'),
                                            'data'  => $data_error,
                                            'flash' => true
                                        ]);

                                        $this->getEventManager()->dispatch($event);
                                        Log::write('error', $err);

                                    } else {

                                        $sms->status = "Enviado";
                                        $this->MassEmailsSms->save($sms);
                                        $send_email++;
                                        $customer->last_emails = Time::now();
                                        $this->Customers->save($customer);

                                        $action = 'Envio de Correo exitoso';
                                        $detail = '';
                                        $detail .= 'Código: ' . $customer->code . PHP_EOL;
                                        $detail .= 'Nombre: ' . $customer->name . PHP_EOL;
                                        $detail .= 'Correo: ' . $customer->email . PHP_EOL;
                                        $this->registerActivity($action, $detail, NULL);
                                    }
                                } catch (\Exception $e) {
                                    $error_email++;

                                    $data_error = new \stdClass; 
                                    $data_error->request_data = $e->getMessage();
                                    $data_error->customer = $customer;
                                    $data_error->section = "Envio de correo seleccionando de clientes";

                                    $event = new Event('MassEmailsController.Error', $this, [
                                        'msg'   => __('Hubo un error al enviar los correos.'),
                                        'data'  => $data_error,
                                        'flash' => true
                                    ]);

                                    $this->getEventManager()->dispatch($event);

                                    Log::write('error', $e->getMessage());
                                    Log::write('error', $customer);
                                    Log::write('error', $sms);
                                }
                            }
                        }

                        if (sizeof($attachments) > 0) {
                            foreach ($attachments as $attach) {
                                if ($attach['mimetype'] == 'application/pdf') {
                                    unlink($attach['file']);
                                }
                            }
                        }
                    }

                    $this->Flash->success(__('Correos enviados: ' . $send_email));

                    if ($error_email > 0) {
                        $this->Flash->warning(__('Correos no enviados: ' . $error_email . '. \n Por favor verifique la configuración de Correo Masivo.'));
                    }

                    if ($no_attach_receipts_email > 0) {
                        $this->Flash->warning(__('Correos sin adjuntar Recibos: ' . $no_attach_receipts_email . '. \n Por favor comuníquese con Soporte Técnico.'));
                    }

                    if ($no_attach_invoices_email > 0) {
                        $this->Flash->warning(__('Correos sin adjuntar Facturas: ' . $no_attach_invoices_email . '. \n Por favor comuníquese con Soporte Técnico.'));
                    }

                    if ($no_attach_account_summary_email > 0) {
                        $this->Flash->warning(__('Correos sin adjuntar Resumen de Cuenta \n Por favor comuníquese con Soporte Técnico.'));
                    }

                    $this->loadModel('Users');
                    $user = $this->Users->get($this->Auth->user()['id']);
                    $user_name = $user->name . ' - username: ' . $user->username;

                    $action =  'Envío masivo de Correos';

                    $cobrodigital_add = $paraments->invoicing->add_cobrodigital_account_summary ? 'Si' : 'No';
                    $payu_add = $paraments->invoicing->add_payu_account_summary ? 'Si' : 'No';
                    $cuentadigital_add = $paraments->invoicing->add_cuentadigital_account_summary ? 'Si' : 'No';
                    $link_mercado_pago = $data->payment_link_mercadopago ? 'Si' : 'No';
                    $link_cuentadigital = $data->payment_link_cuentadigital ? 'Si' : 'No';
                    $account_summary_add = $data->account_summary_attach ? 'Si' : 'No';
                    $invoice_add = $data->invoice_attach ? 'Si' : 'No';
                    $receipt_add = $data->receipt_attach ? 'Si' : 'No';
                    $invoice_no_paid_add = $data->invoice_impagas ? 'Si' : 'No';
                    $invoice_periode = $data->invoice_periode;
                    $receipt_periode = $data->receipt_periode;

                    $detail = 'Bloque: #' . $bloque->id . PHP_EOL;
                    $detail .= 'Fecha: ' . $bloque->confirm_date->format('d/m/Y') . PHP_EOL;
                    $detail .= 'Plantilla: ' . $bloque->template_name . PHP_EOL;
                    $detail .= 'Empresa: ' . $enterprise->name . PHP_EOL;
                    $detail .= 'Adj. Factura: ' . $invoice_add . PHP_EOL;
                    $detail .= 'Período de Fact.: ' . $invoice_periode . PHP_EOL;
                    $detail .= 'Adj. Factura impagas: ' . $invoice_no_paid_add . PHP_EOL;
                    $detail .= 'Adj. Recibo: ' . $receipt_add . PHP_EOL;
                    $detail .= 'Período de Recibo: ' . $receipt_periode . PHP_EOL;
                    $detail .= 'Adj. Resumen de Cuenta: ' . $account_summary_add . PHP_EOL;
                    $detail .= 'Cód. barra Cobro Digital: ' . $cobrodigital_add . PHP_EOL;
                    $detail .= 'Cód. barra Payu: ' . $payu_add . PHP_EOL;
                    $detail .= 'Cód. barra Cuenta Digital: ' . $cuentadigital_add . PHP_EOL;
                    $detail .= 'Link de pago Mercado Pago: ' . $link_mercado_pago . PHP_EOL;
                    $detail .= 'Link de pago Cuenta Digital: ' . $link_cuentadigital . PHP_EOL;

                    if ($data->payment_link_cuentadigital) {

                        $cd_vencimientox = [
                            '' => 'Abierto',
                            1  => 'a 1 día',
                            2  => 'a 2 días',
                            3  => 'a 3 días',
                            4  => 'a 4 días',
                            5  => 'a 5 días',
                            6  => 'a 6 días',
                            7  => 'a 7 días',
                            8  => 'a 8 días',
                            9  => 'a 9 días',
                            10 => 'a 10 días',
                            11 => 'a 11 días',
                            12 => 'a 12 días',
                            13 => 'a 13 días',
                            14 => 'a 14 días',
                            15 => 'a 15 días',
                            16 => 'a 16 días',
                            17 => 'a 17 días',
                            18 => 'a 18 días',
                            19 => 'a 19 días',
                            20 => 'a 20 días',
                            21 => 'a 21 días',
                            22 => 'a 22 días',
                            23 => 'a 23 días',
                            24 => 'a 24 días',
                            25 => 'a 25 días',
                            26 => 'a 26 días',
                            27 => 'a 27 días',
                            28 => 'a 28 días',
                            29 => 'a 29 días',
                            30 => 'a 30 días',
                            31 => 'a 31 días',
                        ];

                        $cd_saldox = [
                            0 => 'Abierto',
                            1 => 'Saldo mes',
                            2 => 'Saldo total'
                        ];

                        $efectivo_cd = $data->cd_efectivo ? 'Si' : 'No';
                        $credit_card_cd = $data->cd_credit_card ? 'Si' : 'No';

                        $detail .= 'Cuenta Digital - vencimiento: ' . $cd_vencimientox[$data->cd_venc] . PHP_EOL;
                        $detail .= 'Cuenta Digital - monto a cobrar: ' . $cd_saldox[$data->cd_saldo] . PHP_EOL;
                        $detail .= 'Cuenta Digital - efectivo: ' . $efectivo_cd . PHP_EOL;
                        $detail .= 'Cuenta Digital - tarjetas de crédito y débito: ' . $credit_card_cd . PHP_EOL;
                    }

                    $detail .= 'Usuario: ' . $user_name . PHP_EOL;
                    $detail .= 'Correos enviados: ' . $send_email . PHP_EOL;
                    $detail .= 'Correos no enviados: ' . $error_email . PHP_EOL;
                    $detail .= 'Correos sin adjuntar Recibos: ' . $no_attach_receipts_email . PHP_EOL;
                    $detail .= 'Correos sin adjuntar Facturas: ' . $no_attach_invoices_email . PHP_EOL;
                    $detail .= 'Correos sin adjuntar Resumen de Cuenta: ' . $no_attach_account_summary_email . PHP_EOL;
                    $detail .= '---------------------------' . PHP_EOL;

                    $this->registerActivity($action, $detail, NULL);

                    if ($data->redirect == 'message') {
                        return $this->redirect(['action' => $data->redirect]);
                    } else {
                        return $this->redirect(['action' => $data->redirect, $template_id]);
                    }

                } else {
                    $this->Flash->warning(__('No se han enviado el correo. Por favor intente nuevamente.'));
                }
            } else {

                $this->Flash->warning(__('La misma acción se intento realizar varias veces.'));
                return $this->redirect(['action' => 'selectCustomers']);
            }
        }

        $this->set(compact('templates', 'template', 'limit', 'business', 'block_next_id', 'labels', 'areas', 'templates_model', 'emails', 'email_account_selected', 'emails_model', 'controllers', 'services', 'accounts', 'payment_getway', 'paraments'));
        $this->set('_serialize', ['template']);
    }

    public function selectCustomersWithArchived($template_id = NULL)
    {
        $this->validateEmailsAccounts();

        $template = null;
        $paraments = $this->request->getSession()->read('paraments');
        $payment_getway = $this->request->getSession()->read('payment_getway');

        $business = [];
        $business[''] = 'Seleccione Empresa';
        foreach ($paraments->invoicing->business as $b) {
            $business[$b->id] = 'Empresa: ' . $b->name . ' (' . $b->address . ')';
        }

        $areas = [];
        $this->loadModel('Areas');
        $areas_model = $this->Areas->find();
        foreach ($areas_model as $a) {
            $areas[$a->id] = $a->name;
        }

        $controllers = [];
        $this->loadModel('Controllers');
        $controllers_model = $this->Controllers
            ->find()
            ->where([
        		'enabled' => TRUE,
        		'deleted' => FALSE
        	]);
        foreach ($controllers_model as $c) {
            $controllers[$c->id] = $c->name;
        }

        $services = [];
        $this->loadModel('Services');
        $services_model = $this->Services->find();
        foreach ($services_model as $s) {
            $services[$s->id] = $s->name;
        }

        $accounts = [];
        foreach ($payment_getway->methods as $pg) {
            $config = $pg->config;
            if ($payment_getway->config->$config->enabled && $pg->credential) {
                $accounts[$pg->id] = $pg->name;
            }
        }

        if (sizeof($accounts) == 0) {
            $accounts[""] = "Sin Cuentas";
        }

        $this->loadModel('MassEmailsBlocks');
        $block_next_id = $this->MassEmailsBlocks->find()->select(['id'])->order(['id' => 'DESC'])->first();
        $block_next_id = empty($block_next_id) ? 1 : ++$block_next_id->id;

        $templates_model = $this->MassEmailsTemplates
            ->find()
            ->where([
                'enabled' => TRUE
            ]);

        if ($templates_model->count() < 1) {
            $this->Flash->warning('Debe crear una plantilla.');
            return $this->redirect(['action' => 'addTemplate']);
        }

        $templates = [];
        foreach ($templates_model as $tem) {
            $templates[$tem->id] = $tem->name;
            if ($template_id != NULL) {
                if ($tem->id == $template_id) {
                    $template = $tem;
                }
            } else {
                $template = $tem;
            }
        }

        $emails = [];
        $email_account_selected = NULL;
        $emails_model = $paraments->mass_emails->emails;
        foreach ($paraments->mass_emails->emails as $email) {

            if ($template->email_id == $email->id) {
                $email_account_selected = $email;
            }

            if ($email->enabled) {
                $emails[$email->id] = $email->contact;
            }
        }

        $limit = $paraments->mass_emails->limit;

        $this->loadModel('Labels');
        $labels_array = $this->Labels->find()->where(['entity' => 0])->toArray();

        $labels = [];
        $labels[] = [
            'id' => 0,
            'text' => 'Sin etiquetas',
            'entity' => 0,
            'color_back' => '#000000',
            'color_text' => '#000000',
            'enabled' => 1,
        ];

        foreach ($labels_array as $label) {
            $labels[] = $label;
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            $error = $this->isReloadPage();

            if (!$error) {

                $action = 'Envio Masivo de Correos seleccionando Clientes Archivados';
                $detail = '';
                $this->registerActivity($action, $detail, NULL, TRUE);

                $this->loadModel('Customers');
                $this->loadModel('MassEmailsSms');
                $this->loadModel('Invoices');
                $this->loadModel('Receipts');

                $data = json_decode($this->request->getData('data'));

                foreach ($paraments->invoicing->business as $b) {
                    if ($b->id == $data->business_billing) {
                        $enterprise = $b;
                    }
                }

                $attach_logo = [];

                if ($data->payment_link_mercadopago || $data->payment_link_cuentadigital) {

                    $logo = $enterprise->logo;

                    if ($logo != "") {

                        $logo_portions = explode(".", $logo);
                        $last_dot = sizeof($logo_portions) - 1;
                        $ext = $logo_portions[$last_dot];

                        $attach_logo = [
                            'file'        => WWW_ROOT . 'img/' . $logo,
                            'mimetype'    => 'image/' . $ext,
                            'contentId'   => $enterprise->id,
                            'disposition' => 'inline'
                        ];
                    }

                    $template = new \stdClass;
                    $template->name = "Link de Pagos";
                    $template->subject = "ink Pago";
                    $template->description = "Link de pago MercadoPago ó Cuenta Digital con Resumen de Cuenta";
                    $template->enabled = TRUE;
                    $template->business_billing = $enterprise->id;
                    $template->invoice_attach = $data->invoice_attach;
                    $template->receipt_attach = $data->receipt_attach;
                    $template->email_id = $data->email_id;
                    $template->account_summary_attach = $data->account_summary_attach;
                } else {
                    foreach ($templates_model as $tem) {
                        if ($tem->id == $data->template_id) {
                            $template = $tem;
                        }
                    }
                }

                $email_data = NULL;
                $flag = FALSE;
                foreach ($paraments->mass_emails->emails as $email) {
                    if ($email->id == $data->email_id) {
                        $email_data = $email;
                    }
                }

                $afip_codes = $this->request->getSession()->read('afip_codes');

                $bloque = $this->MassEmailsBlocks->newEntity();
                $bloque->user_id       = $this->Auth->user()['id'];
                $bloque->template_name = $template->name;
                $bloque->confirm_date  = Time::now();
                $bloque->email_account = $email_data->contact;
                $bloque->status        = '';
                $this->MassEmailsBlocks->save($bloque);

                $bloqueString = "";

                // si usa proveedor de correo
                if ($paraments->emails_accounts->provider == "") {

                    try {
                        Email::configTransport('mass_emails', [
                            'className' => $email_data->class_name,
                            'host'      => $email_data->host,
                            'port'      => $email_data->port,
                            'timeout'   => $email_data->timeout,
                            'username'  => $email_data->user,
                            'password'  => $email_data->pass,
                            'client'    => $email_data->client == "" ? NULL : $email_data->client,
                            'tls'       => $email_data->tls == "" ? NULL : $email_data->tls,
                            'url'       => env('EMAIL_TRANSPORT_DEFAULT_URL', NULL)
                        ]);
                        $flag = TRUE;
                    } catch (\Exception $e) {

                        $data_error = new \stdClass; 
                        $data_error->request_data = $e->getMessage();
                        $data_error->section = 'Envio de correo seleccionando clientes archivados';

                        $event = new Event('MassEmailsController.Error', $this, [
                            'msg'   => __('Hubo un error al enviar los correos.'),
                            'data'  => $data_error,
                            'flash' => true
                        ]);

                        $this->getEventManager()->dispatch($event);

                        Log::write('error', $e->getMessage());
                    }
                } else {
                    $flag = TRUE;
                }

                if ($flag) {

                    $send_email = 0;
                    $error_email = 0;
                    $no_attach_receipts_email = 0;
                    $no_attach_invoices_email = 0;
                    $no_attach_account_summary_email = 0;

                    foreach ($data->ids as $cu_code) {

                        $attachments = [];
                        $connections_invoices = NULL;
                        $invoices_connections = NULL;

                        $customer = $this->Customers->get($cu_code, [
                            'contain' => 'Accounts'
                        ]);

                        $invoice_impagas = FALSE;
                        $periode = NULL;

                        if ($data->invoice_attach) {

                            $where = [];
                            $where['customer_code'] = $cu_code;

                            if ($data->invoice_impagas) {
                                $invoice_impagas = TRUE;
                                $where['paid IS'] = NULL;
                            }

                            if ($data->invoice_periode) {
                                $periode = $data->invoice_periode;
                                $periode = explode("/", $data->invoice_periode);
                                $periode = $periode[1] . '-' . $periode[0];
                                $where['date_start LIKE'] = $periode . '%';
                            }

                            $invoices = $this->Invoices
                                ->find()
                                ->where($where);

                            if ($invoices->count() > 0) {

                                $connections_invoices = [];
                                $invoices_connections = [];

                                foreach ($invoices as $invoice) {

                                    $printed = "";

                                    if (!empty($invoice->connection_id)) {
                                        array_push($connections_invoices, $invoice->connection_id);
                                    }

                                    $printed = $this->FiscoAfipCompPdf->invoice($invoice, TRUE);

                                    if ($printed != "") {
                                        $attach = [
                                            'file'        => $printed,
                                            'mimetype'    => 'application/pdf',
                                            'contentId'   => $invoice->id  . '',
                                            'disposition' => 'attachment'
                                        ];
                                        array_push($attachments, $attach);
                                        array_push($invoices_connections, $invoice->id);
                                    } else {

                                        $no_attach_invoices_email++;

                                        $data_error = new \stdClass; 
                                        $data_error->request_data = $invoice;
                                        $data_error->customer = $customer;
                                        $data_error->section = 'Envio de correo seleccionando clientes archivados';

                                        $event = new Event('MassEmailsController.Error', $this, [
                                            'msg'   => __('Hubo un error no se genero dinámicamente el PDF de la Factura. Envio de correo'),
                                            'data'  => $data_error,
                                            'flash' => true
                                        ]);

                                        $this->getEventManager()->dispatch($event);

                                        Log::error('Error attach this -> invoice_id: ' . $invoice->id, ['scope' => ['MassEmailsController']]);
                                        continue;
                                    }
                                }
                            }
                        }

                        $receipt_periode = NULL;

                        if ($data->receipt_attach) {

                            $where = [];
                            $where['customer_code'] = $cu_code;

                            if ($data->receipt_periode) {
                                $receipt_periode = $data->receipt_periode;
                                $receipt_periode = explode("/", $data->receipt_periode);
                                $receipt_periode = $receipt_periode[1] . '-' . $receipt_periode[0];
                                $where['date_start LIKE'] = $receipt_periode . '%';
                            }

                            $receipts = $this->Receipts
                                ->find()
                                ->where($where);

                            if ($receipts->count() > 0) {

                                $connections_receipts = [];
                                $receipts_connections = [];

                                foreach ($receipts as $receipt) {

                                    $printed = "";

                                    if (!empty($receipt->connection_id)) {
                                        array_push($connections_receipts, $receipt->connection_id);
                                    }

                                    $printed = $this->FiscoAfipCompPdf->receipt($receipt, TRUE);

                                    if ($printed != "") {

                                        $attach = [
                                            'file'        => $printed,
                                            'mimetype'    => 'application/pdf',
                                            'contentId'   => $receipt->id  . '',
                                            'disposition' => 'attachment'
                                        ];

                                        array_push($attachments, $attach);
                                        array_push($receipts_connections, $receipt->id);
                                    } else {

                                        $no_attach_receipts_email++;

                                        $data_error = new \stdClass; 
                                        $data_error->request_data = $receipt;
                                        $data_error->customer = $customer;
                                        $data_error->section = 'Envio de correo seleccionando clientes archivados';

                                        $event = new Event('MassEmailsController.Error', $this, [
                                            'msg'   => __('Hubo un error no se genero dinámicamente el PDF del Recibo. Envio de correo'),
                                            'data'  => $data_error,
                                            'flash' => true
                                        ]);

                                        $this->getEventManager()->dispatch($event);

                                        Log::error('Error attach this -> receipt_id: ' . $receipt->id, ['scope' => ['MassEmailsController']]);
                                        continue;
                                    }
                                }
                            }
                        }

                        if ($data->account_summary_attach) {

                            $printed = $this->FiscoAfipCompPdf->account_summary($cu_code, TRUE, FALSE, NULL, $paraments->invoicing->add_cobrodigital_account_summary, $paraments->invoicing->add_payu_account_summary, $paraments->invoicing->add_cuentadigital_account_summary);

                            if ($printed != "") {

                                $attach = [
                                    'file'        => $printed,
                                    'mimetype'    => 'application/pdf',
                                    'contentId'   => $cu_code . '',
                                    'disposition' => 'attachment' 
                                ];

                                array_push($attachments, $attach);
                            } else {

                                $no_attach_account_summary_email++;

                                $data_error = new \stdClass; 
                                $data_error->request_data = $customer;
                                $data_error->section = 'Envio de correo seleccionando clientes archivados';

                                $event = new Event('MassEmailsController.Error', $this, [
                                    'msg'   => __('Hubo un error no se genero dinámicamente el PDF del Resumen de Cuenta. Envio de correo'),
                                    'data'  => $data_error,
                                    'flash' => TRUE
                                ]);

                                $this->getEventManager()->dispatch($event);

                                Log::error('Error attach this -> customer_code: ' . $cu_code, ['scope' => ['MassEmailsController']]);
                                continue;
                            }
                        }

                        $debts_total = $this->CurrentAccount->calulateCustomerDebtTotal($customer->code);

                        $fantasy_name = $enterprise->name;
                        if (isset($enterprise->fantasy_name)) {
                            if ($enterprise->fantasy_name != "" && $enterprise->fantasy_name != NULL) {
                                $fantasy_name = $enterprise->fantasy_name;
                            }
                        }

                        if ($data->payment_link_mercadopago || $data->payment_link_cuentadigital) {
                            $template->message = $this->createLinkPagos($customer, $attach_logo, $enterprise, $data);
                            if ($template->message) {
                                $temp = $template->message;
                            } else {
                                $error_email++;
                                continue;
                            }

                            array_push($attachments, $attach_logo);
                        } else {
                            $temp = str_replace("%cliente_nombre%", $customer->name, $template->message);
                            $temp = str_replace("%cliente_codigo%", $customer->code, $temp);
                            $doc  = $afip_codes['doc_types'][$customer->doc_type] . ' ' . $customer->ident;
                            $temp = str_replace("%cliente_documento%", $doc, $temp);
                            $temp = str_replace("%cliente_usuario_portal%", $customer->ident, $temp);
                            $temp = str_replace("%cliente_clave_portal%", $customer->clave_portal, $temp);
                            $temp = str_replace("%cliente_saldo_total%", $debts_total, $temp);
                            $temp = str_replace("%cliente_saldo_mes%", $customer->debt_month, $temp);
                            $temp = str_replace("%empresa_nombre%", $fantasy_name, $temp);
                            $temp = str_replace("%empresa_domicilio%", $enterprise->address, $temp);
                            $temp = str_replace("%empresa_web%", $enterprise->web, $temp);
                        }

                        $status = "No enviado";

                        $sms = $this->MassEmailsSms->newEntity();
                        $data_array = [
                            'message'                    => ($data->payment_link_mercadopago || $data->payment_link_cuentadigital) ? 'Resumen de Cuenta' : $temp,
                            'subject'                    => $data->subject,
                            'customer_code'              => $cu_code,
                            'connections_id'             => !empty($connections_invoices) ? implode(",", $connections_invoices) : NULL,
                            'invoices_id'                => !empty($invoices_connections) ? implode(",", $invoices_connections) : NULL,
                            'receipts_id'                => !empty($receipts_connections) ? implode(",", $receipts_connections) : NULL,
                            'bloque_id'                  => $bloque->id,
                            'business_billing'           => $enterprise->id,
                            'invoice_attach'             => $data->invoice_attach,
                            'receipt_attach'             => $data->receipt_attach,
                            'account_summary_attach'     => $data->account_summary_attach,
                            'invoice_impagas'            => $invoice_impagas,
                            'invoice_periode'            => $periode,
                            'receipt_periode'            => $receipt_periode,
                            'status'                     => $status,
                            'email_id'                   => $data->email_id,
                            'payment_link_mercadopago'   => $data->payment_link_mercadopago,
                            'payment_link_cuentadigital' => $data->payment_link_cuentadigital,
                            'template_id'                => $data->template_id,
                            'email'                      => $customer->email,
                            'cd_saldo'                   => isset($data->cd_saldo) ? $data->cd_saldo : NULL,
                            'cd_venc'                    => isset($data->cd_venc) ? $data->cd_venc : NULL,
                            'cd_efectivo'                => isset($data->cd_efectivo) ? $data->cd_efectivo : NULL,
                            'cd_credit_card'             => isset($data->cd_credit_card) ? $data->cd_credit_card : NULL,
                        ];

                        $sms = $this->MassEmailsSms->patchEntity($sms, $data_array);
                        $this->MassEmailsSms->save($sms);

                        //$from = $enterprise->email ? $enterprise->email : $email_data->contact;
                        $from = $email_data->contact;

                        $enterprise_email = $enterprise->web == '' ? $fantasy_name : $enterprise->web;

                        // si usa proveedor de correo
                        if ($paraments->emails_accounts->provider == "") {

                            $email = new Email();
                            $email->transport('mass_emails');
                            $array_email = explode(',', $customer->email);
                            $array_size = sizeof($array_email);

                            if ($array_size == 1) {

                                $email->to([$customer->email => $customer->name]);
                            } else {

                                $email->to([$array_email[0] => $customer->name]);

                                for ($i = 1; $i < $array_size; $i++) {
                                    $email->addTo([$array_email[$i] => $customer->name]);
                                }
                            }
                            $email->from([$from => $enterprise_email]);
                            $email->subject($data->subject);
                            $email->emailFormat('html');
                            $email->template('mass_emails');
                            $email->setViewVars(['message' => $temp]);

                            if (sizeof($attachments) > 0) {
                                $email->attachments($attachments);
                            }

                            try {
                                if ($email->send()) {

                                    $sms->status = "Enviado";
                                    $this->MassEmailsSms->save($sms);
                                    $send_email++;

                                    $action = 'Envio de Correo exitoso';
                                    $detail = '';
                                    $detail .= 'Código: ' . $customer->code . PHP_EOL;
                                    $detail .= 'Nombre: ' . $customer->name . PHP_EOL;
                                    $detail .= 'Correo: ' . $customer->email . PHP_EOL;
                                    $this->registerActivity($action, $detail, NULL);
                                }
                            } catch (\Exception $e) {
                                $error_email++;

                                $data_error = new \stdClass; 
                                $data_error->request_data = $e->getMessage();
                                $data_error->customer = $customer;
                                $data_error->section = 'Envio de correo seleccionando clientes archivados';

                                $event = new Event('MassEmailsController.Error', $this, [
                                    'msg'   => __('Hubo un error al enviar los correos.'),
                                    'data'  => $data_error,
                                    'flash' => true
                                ]);

                                $this->getEventManager()->dispatch($event);

                                Log::write('error', $e->getMessage());
                            }
                        } else {

                            // UTILIZA PROVEEDOR DE CORREO
                            if ($paraments->emails_accounts->provider == 'sendgrid') {

                                $html = '<html><head></head><body>' . $temp . '</body></html>';

                                if ($data->payment_link_mercadopago || $payment_link_cuentadigital) {
                                    $html = $temp;
                                }

                                $tos = explode(",", $customer->email);
                                $tos_post = [];
                                foreach ($tos as $to) {
                                    $to_boject = new \stdClass;
                                    $to_boject->email = $to;
                                    $to_boject->name = $customer->name;
                                    array_push($tos_post, $to_boject);
                                }

                                $content = new \stdClass;
                                $content->type = "text/html";
                                $content->value = $html;

                                $from_sendgrid = new \stdClass;
                                $from_sendgrid->email = $from;
                                $from_sendgrid->name = $fantasy_name;

                                $reply_to = new \stdClass;
                                $reply_to->email = $from;
                                $reply_to->name = $fantasy_name;

                                if (isset($email_data->copy)) {
                                    if ($email_data->copy == "") {
                                        $from_copy = $from;
                                        $fantasy_name_copy = $fantasy_name;
                                    } else {
                                        $from_copy = $email_data->copy;
                                        $fantasy_name_copy = "";
                                    }
                                } else {
                                    $from_copy = $from;
                                    $fantasy_name_copy = $fantasy_name;
                                }

                                $bcc = new \stdClass;
                                $bcc->email = $from_copy;
                                $bcc->name = $fantasy_name_copy;

                                $personalizations = [
                                    'to' => $tos_post,
                                    'cc' => [
                                        0 => $bcc
                                    ],
                                    'subject' => $data->subject
                                ];

                                $data_sendgrid = new \stdClass;

                                if (sizeof($attachments) > 0) {

                                    foreach ($attachments as $attach) {

                                        $pdf_file = file_get_contents($attach['file']);
                                        $pdf_explode = explode('/', $attach['file']);
                                        $position_name = sizeof($pdf_explode) - 1;

                                        $attachments_sendgrid = new \stdClass;
                                        $attachments_sendgrid->content = base64_encode($pdf_file);
                                        $attachments_sendgrid->type = $attach['mimetype'];
                                        $attachments_sendgrid->filename = $pdf_explode[$position_name];
                                        $attachments_sendgrid->disposition = $attach['disposition'];
                                        $attachments_sendgrid->content_id = $attach['contentId'];
                                        $data_sendgrid->attachments[] = $attachments_sendgrid;
                                    }
                                }

                                $data_sendgrid->personalizations[] = $personalizations;
                                $data_sendgrid->content[] = $content;
                                $data_sendgrid->from = $from_sendgrid;
                                $data_sendgrid->reply_to = $reply_to;
                                $data_sendgrid->bcc = $bcc;

                                $data_string = json_encode($data_sendgrid);

                                try {

                                    $curl = curl_init();

                                    $endpoint_send = $paraments->emails_accounts->providers->sendgrid->endpoint->api->v3->send;
                                    $api_key = $paraments->emails_accounts->providers->sendgrid->api_key;

                                    curl_setopt_array($curl, array(
                                        CURLOPT_URL            => $endpoint_send,
                                        CURLOPT_RETURNTRANSFER => TRUE,
                                        CURLOPT_ENCODING       => "",
                                        CURLOPT_MAXREDIRS      => 10,
                                        CURLOPT_TIMEOUT        => 30,
                                        CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                                        CURLOPT_CUSTOMREQUEST  => "POST",
                                        CURLOPT_POSTFIELDS     => $data_string,
                                        CURLOPT_SSLVERSION     => CURL_SSLVERSION_TLSv1_2,
                                        CURLOPT_HTTPHEADER => array(
                                            "authorization: Bearer " . $api_key,
                                            "content-type: application/json"
                                        ),
                                    ));

                                    $response = curl_exec($curl);
                                    $err = curl_error($curl);
                                    curl_close($curl);

                                    if ($err) {

                                        $error_email++;

                                        $data_error = new \stdClass; 
                                        $data_error->request_data = $err;
                                        $data_error->customer = $customer;
                                        $data_error->section = 'Envio de correo seleccionando clientes archivados';

                                        $event = new Event('MassEmailsController.Error', $this, [
                                            'msg'   => __('Hubo un error al enviar los correos.'),
                                            'data'  => $data_error,
                                            'flash' => true
                                        ]);

                                        $this->getEventManager()->dispatch($event);

                                        Log::write('error', $err);
                                        
                                    } else {

                                        $sms->status = "Enviado";
                                        $this->MassEmailsSms->save($sms);
                                        $send_email++;

                                        $action = 'Envio de Correo exitoso';
                                        $detail = '';
                                        $detail .= 'Código: ' . $customer->code . PHP_EOL;
                                        $detail .= 'Nombre: ' . $customer->name . PHP_EOL;
                                        $detail .= 'Correo: ' . $customer->email . PHP_EOL;
                                        $this->registerActivity($action, $detail, NULL);
                                    }
                                } catch (\Exception $e) {
                                    $error_email++;

                                    $data_error = new \stdClass; 
                                    $data_error->request_data = $e->getMessage();
                                    $data_error->customer = $customer;
                                    $data_error->section = 'Envio de correo seleccionando clientes archivados';

                                    $event = new Event('MassEmailsController.Error', $this, [
                                        'msg'   => __('Hubo un error al enviar los correos.'),
                                        'data'  => $data_error,
                                        'flash' => true
                                    ]);

                                    $this->getEventManager()->dispatch($event);

                                    Log::write('error', $e->getMessage());
                                }
                            }
                        }

                        if (sizeof($attachments) > 0) {
                            foreach ($attachments as $attach) {
                                if ($attach['mimetype'] == 'application/pdf') {
                                    unlink($attach['file']);
                                }
                            }
                        }
                    }

                    $this->Flash->success(__('Correos enviados: ' . $send_email));

                    if ($error_email > 0) {
                        $this->Flash->warning(__('Correos no enviados: ' . $error_email . '. \n Por favor verifique la configuración de Correo Masivo.'));
                    }

                    if ($no_attach_receipts_email > 0) {
                        $this->Flash->warning(__('Correos sin adjuntar Recibos: ' . $no_attach_receipts_email . '. \n Por favor comuníquese con Soporte Técnico.'));
                    }

                    if ($no_attach_invoices_email > 0) {
                        $this->Flash->warning(__('Correos sin adjuntar Facturas: ' . $no_attach_invoices_email . '. \n Por favor comuníquese con Soporte Técnico.'));
                    }

                    if ($no_attach_account_summary_email > 0) {
                        $this->Flash->warning(__('Correos sin adjuntar Resumen de Cuenta \n Por favor comuníquese con Soporte Técnico.'));
                    }

                    $this->loadModel('Users');
                    $user = $this->Users->get($this->Auth->user()['id']);
                    $user_name = $user->name . ' - username: ' . $user->username;

                    $action =  'Envío masivo de Correos';

                    $cobrodigital_add = $paraments->invoicing->add_cobrodigital_account_summary ? 'Si' : 'No';
                    $payu_add = $paraments->invoicing->add_payu_account_summary ? 'Si' : 'No';
                    $cuentadigital_add = $paraments->invoicing->add_cuentadigital_account_summary ? 'Si' : 'No';
                    $link_mercado_pago = $data->payment_link_mercadopago ? 'Si' : 'No';
                    $link_cuentadigital = $data->payment_link_cuentadigital ? 'Si' : 'No';
                    $account_summary_add = $data->account_summary_attach ? 'Si' : 'No';
                    $invoice_add = $data->invoice_attach ? 'Si' : 'No';
                    $receipt_add = $data->receipt_attach ? 'Si' : 'No';
                    $invoice_no_paid_add = $data->invoice_impagas ? 'Si' : 'No';
                    $invoice_periode = $data->invoice_periode;
                    $receipt_periode = $data->receipt_periode;

                    $detail = 'Bloque: #' . $bloque->id . PHP_EOL;
                    $detail .= 'Fecha: ' . $bloque->confirm_date->format('d/m/Y') . PHP_EOL;
                    $detail .= 'Plantilla: ' . $bloque->template_name . PHP_EOL;
                    $detail .= 'Empresa: ' . $enterprise->name . PHP_EOL;
                    $detail .= 'Adj. Factura: ' . $invoice_add . PHP_EOL;
                    $detail .= 'Período de Fact.: ' . $invoice_periode . PHP_EOL;
                    $detail .= 'Adj. Factura impagas: ' . $invoice_no_paid_add . PHP_EOL;
                    $detail .= 'Adj. Recibo: ' . $receipt_add . PHP_EOL;
                    $detail .= 'Período de Recibo: ' . $receipt_periode . PHP_EOL;
                    $detail .= 'Adj. Resumen de Cuenta: ' . $account_summary_add . PHP_EOL;
                    $detail .= 'Cód. barra Cobro Digital: ' . $cobrodigital_add . PHP_EOL;
                    $detail .= 'Cód. barra Payu: ' . $payu_add . PHP_EOL;
                    $detail .= 'Cód. barra Cuenta Digital: ' . $cuentadigital_add . PHP_EOL;
                    $detail .= 'Link de pago Mercado Pago: ' . $link_mercado_pago . PHP_EOL;
                    $detail .= 'Link de pago Cuenta Digital: ' . $link_cuentadigital . PHP_EOL;

                    if ($data->payment_link_cuentadigital) {

                        $cd_vencimientox = [
                            '' => 'Abierto',
                            1  => 'a 1 día',
                            2  => 'a 2 días',
                            3  => 'a 3 días',
                            4  => 'a 4 días',
                            5  => 'a 5 días',
                            6  => 'a 6 días',
                            7  => 'a 7 días',
                            8  => 'a 8 días',
                            9  => 'a 9 días',
                            10 => 'a 10 días',
                            11 => 'a 11 días',
                            12 => 'a 12 días',
                            13 => 'a 13 días',
                            14 => 'a 14 días',
                            15 => 'a 15 días',
                            16 => 'a 16 días',
                            17 => 'a 17 días',
                            18 => 'a 18 días',
                            19 => 'a 19 días',
                            20 => 'a 20 días',
                            21 => 'a 21 días',
                            22 => 'a 22 días',
                            23 => 'a 23 días',
                            24 => 'a 24 días',
                            25 => 'a 25 días',
                            26 => 'a 26 días',
                            27 => 'a 27 días',
                            28 => 'a 28 días',
                            29 => 'a 29 días',
                            30 => 'a 30 días',
                            31 => 'a 31 días',
                        ];

                        $cd_saldox = [
                            0 => 'Abierto',
                            1 => 'Saldo mes',
                            2 => 'Saldo total'
                        ];

                        $efectivo_cd = $data->cd_efectivo ? 'Si' : 'No';
                        $credit_card_cd = $data->cd_credit_card ? 'Si' : 'No';

                        $detail .= 'Cuenta Digital - vencimiento: ' . $cd_vencimientox[$data->cd_venc] . PHP_EOL;
                        $detail .= 'Cuenta Digital - monto a cobrar: ' . $cd_saldox[$data->cd_saldo] . PHP_EOL;
                        $detail .= 'Cuenta Digital - efectivo: ' . $efectivo_cd . PHP_EOL;
                        $detail .= 'Cuenta Digital - tarjetas de crédito y débito: ' . $credit_card_cd . PHP_EOL;
                    }

                    $detail .= 'Usuario: ' . $user_name . PHP_EOL;
                    $detail .= 'Correos enviados: ' . $send_email . PHP_EOL;
                    $detail .= 'Correos no enviados: ' . $error_email . PHP_EOL;
                    $detail .= 'Correos sin adjuntar Recibos: ' . $no_attach_receipts_email . PHP_EOL;
                    $detail .= 'Correos sin adjuntar Facturas: ' . $no_attach_invoices_email . PHP_EOL;
                    $detail .= 'Correos sin adjuntar Resumen de Cuenta: ' . $no_attach_account_summary_email . PHP_EOL;
                    $detail .= '---------------------------' . PHP_EOL;

                    $this->registerActivity($action, $detail, NULL);

                    if ($data->redirect == 'message') {
                        return $this->redirect(['action' => $data->redirect]);
                    } else {
                        return $this->redirect(['action' => $data->redirect, $template_id]);
                    }

                } else {
                    $this->Flash->warning(__('No se han enviado el correo. Por favor intente nuevamente.'));
                }
            } else {

                $this->Flash->warning(__('La misma acción se intento realizar varias veces.'));
                return $this->redirect(['action' => 'selectCustomersWithArchived']);
            }
        }

        $this->set(compact('templates', 'template', 'limit', 'business', 'block_next_id', 'labels', 'areas', 'templates_model', 'emails', 'email_account_selected', 'emails_model', 'controllers', 'services', 'accounts', 'payment_getway', 'paraments'));
        $this->set('_serialize', ['template']);
    }

    public function selectCustomersReSend()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {

            $error = $this->isReloadPage();

            if (!$error) {

                $action = 'Reenvio Masivo de Correos seleccionando Correos Enviados';
                $detail = '';
                $this->registerActivity($action, $detail, NULL, TRUE);

                $this->loadModel('Customers');
                $this->loadModel('MassEmailsSms');
                $this->loadModel('MassEmailsBlocks');
                $this->loadModel('MassEmailsTemplates');
                $this->loadModel('Invoices');
                $this->loadModel('Receipts');

                $templates_model = $this->MassEmailsTemplates
                    ->find()
                    ->where([
                        'enabled' => TRUE
                    ]);

                $paraments = $this->request->getSession()->read('paraments');
                $payment_getway = $this->request->getSession()->read('payment_getway');

                $data = json_decode($this->request->getData('data'));

                foreach ($data->ids as $id) {

                    $sms_mass_email_sms = $this->MassEmailsSms
                        ->find()
                        ->where([
                            'id' => $id
                        ])->first();

                    if ($sms_mass_email_sms->email_id == NULL 
                        || $sms_mass_email_sms->template_id == NULL) {
                        continue;
                    }

                    foreach ($paraments->invoicing->business as $b) {
                        if ($b->id == $sms_mass_email_sms->business_billing) {
                            $enterprise = $b;
                        }
                    }

                    $attach_logo = [];

                    if ($sms_mass_email_sms->payment_link_mercadopago || $sms_mass_email_sms->payment_link_cuentadigital) {

                        $logo = $enterprise->logo;

                        if ($logo != "") {

                            $logo_portions = explode(".", $logo);
                            $last_dot = sizeof($logo_portions) - 1;
                            $ext = $logo_portions[$last_dot];

                            $attach_logo = [
                                'file'        => WWW_ROOT . 'img/' . $logo,
                                'mimetype'    => 'image/' . $ext,
                                'contentId'   => $enterprise->id,
                                'disposition' => 'inline'
                            ];
                        }

                        $template = new \stdClass;
                        $template->name = "MercadoPago";
                        $template->subject = "Link Pago MercadoPago";
                        $template->description = "Link de pago MercadoPago con Resumen de Cuenta";
                        $template->enabled = TRUE;
                        $template->business_billing = $enterprise->id;
                        $template->invoice_attach = $sms_mass_email_sms->invoice_attach;
                        $template->receipt_attach = $sms_mass_email_sms->receipt_attach;
                        $template->email_id = $sms_mass_email_sms->email_id;
                        $template->account_summary_attach = $sms_mass_email_sms->account_summary_attach;

                    } else {

                        foreach ($templates_model as $tem) {
                            if ($tem->id == $sms_mass_email_sms->template_id) {
                                $template = $tem;
                            }
                        }
                    }

                    $email_data = NULL;
                    $flag = FALSE;
                    foreach ($paraments->mass_emails->emails as $email) {
                        if ($email->id == $sms_mass_email_sms->email_id) {
                            $email_data = $email;
                        }
                    }

                    $afip_codes = $this->request->getSession()->read('afip_codes');

                    $bloque = $this->MassEmailsBlocks
                        ->find()
                        ->where([
                            'id' => $sms_mass_email_sms->bloque_id
                        ])->first();

                    $bloque->user_id       = $this->Auth->user()['id'];
                    $bloque->confirm_date  = Time::now();
                    $bloque->status        = '';
                    $this->MassEmailsBlocks->save($bloque);

                    $bloqueString = "";

                    // si usa proveedor de correo
                    if ($paraments->emails_accounts->provider == "") {

                        try {
                            Email::configTransport('mass_emails', [
                                'className' => $email_data->class_name,
                                'host'      => $email_data->host,
                                'port'      => $email_data->port,
                                'timeout'   => $email_data->timeout,
                                'username'  => $email_data->user,
                                'password'  => $email_data->pass,
                                'client'    => $email_data->client == "" ? NULL : $email_data->client,
                                'tls'       => $email_data->tls == "" ? NULL : $email_data->tls,
                                'url'       => env('EMAIL_TRANSPORT_DEFAULT_URL', NULL)
                            ]);
                            $flag = TRUE;
                        } catch (\Exception $e) {
                            $data_error = new \stdClass; 
                            $data_error->request_data = $e->getMessage();
                            $data_error->section = 'Reenvio de correo';

                            $event = new Event('MassEmailsController.Error', $this, [
                                'msg'   => __('Hubo un error al reenviar los correos.'),
                                'data'  => $data_error,
                                'flash' => true
                            ]);

                            $this->getEventManager()->dispatch($event);
                            Log::write('error', $e->getMessage());
                        }
                    } else {
                        $flag = TRUE;
                    }

                    if ($flag) {

                        $send_email = 0;
                        $error_email = 0;
                        $no_attach_receipts_email = 0;
                        $no_attach_invoices_email = 0;
                        $no_attach_account_summary_email = 0;

                        $attachments = [];
                        $connections_invoices = NULL;
                        $invoices_connections = NULL;

                        $customer = $this->Customers->get($sms_mass_email_sms->customer_code, [
                            'contain' => 'Accounts'
                        ]);

                        $invoice_impagas = FALSE;
                        $periode = NULL;

                        $cu_code = $sms_mass_email_sms->customer_code;

                        if ($sms_mass_email_sms->invoice_attach) {

                            $where = [];
                            $where['customer_code'] = $cu_code;

                            if ($sms_mass_email_sms->invoice_impagas) {
                                $invoice_impagas = TRUE;
                                $where['paid IS'] = NULL;
                            }

                            if ($sms_mass_email_sms->invoice_periode) {
                                $periode = $sms_mass_email_sms->invoice_periode;
                                $periode = explode("/", $sms_mass_email_sms->invoice_periode);
                                $periode = $periode[1] . '-' . $periode[0];
                                $where['date_start LIKE'] = $periode . '%';
                            }

                            $invoices = $this->Invoices
                                ->find()
                                ->where($where);

                            if ($invoices->count() > 0) {

                                $connections_invoices = [];
                                $invoices_connections = [];

                                foreach ($invoices as $invoice) {

                                    $printed = "";

                                    if (!empty($invoice->connection_id)) {
                                        array_push($connections_invoices, $invoice->connection_id);
                                    }

                                    $printed = $this->FiscoAfipCompPdf->invoice($invoice, TRUE);

                                    if ($printed != "") {
                                        $attach = [
                                            'file'        => $printed,
                                            'mimetype'    => 'application/pdf',
                                            'contentId'   => $invoice->id  . '',
                                            'disposition' => 'attachment'
                                        ];
                                        array_push($attachments, $attach);
                                        array_push($invoices_connections, $invoice->id);
                                    } else {

                                        $no_attach_invoices_email++;

                                        $data_error = new \stdClass; 
                                        $data_error->request_data = $invoice;
                                        $data_error->customer = $customer;
                                        $data_error->section = 'Reenvio de correo';

                                        $event = new Event('MassEmailsController.Error', $this, [
                                            'msg'   => __('Hubo un error no se genero dinámicamente el PDF de la Factura. Reenvio de correo'),
                                            'data'  => $data_error,
                                            'flash' => true
                                        ]);

                                        $this->getEventManager()->dispatch($event);

                                        Log::error('Error attach this -> invoice_id: ' . $invoice->id, ['scope' => ['MassEmailsController']]);
                                        continue;
                                    }
                                }
                            }
                        }

                        $receipt_periode = NULL;

                        if ($sms_mass_email_sms->receipt_attach) {

                            $where = [];
                            $where['customer_code'] = $cu_code;

                            if ($sms_mass_email_sms->receipt_periode) {
                                $receipt_periode = $sms_mass_email_sms->receipt_periode;
                                $receipt_periode = explode("/", $sms_mass_email_sms->receipt_periode);
                                $receipt_periode = $receipt_periode[1] . '-' . $receipt_periode[0];
                                $where['date_start LIKE'] = $receipt_periode . '%';
                            }

                            $receipts = $this->Receipts
                                ->find()
                                ->where($where);

                            if ($receipts->count() > 0) {

                                $connections_receipts = [];
                                $receipts_connections = [];

                                foreach ($receipts as $receipt) {

                                    $printed = "";

                                    if (!empty($receipt->connection_id)) {
                                        array_push($connections_receipts, $receipt->connection_id);
                                    }

                                    $printed = $this->FiscoAfipCompPdf->receipt($receipt, TRUE);

                                    if ($printed != "") {

                                        $attach = [
                                            'file'        => $printed,
                                            'mimetype'    => 'application/pdf',
                                            'contentId'   => $receipt->id  . '',
                                            'disposition' => 'attachment'
                                        ];

                                        array_push($attachments, $attach);
                                        array_push($receipts_connections, $receipt->id);
                                    } else {

                                        $no_attach_receipts_email++;

                                        $data_error = new \stdClass; 
                                        $data_error->request_data = $receipt;
                                        $data_error->customer = $customer;
                                        $data_error->section = 'Reenvio de correo';

                                        $event = new Event('MassEmailsController.Error', $this, [
                                            'msg'   => __('Hubo un error no se genero dinámicamente el PDF del Recibo. Reenvio de correo'),
                                            'data'  => $data_error,
                                            'flash' => true
                                        ]);

                                        $this->getEventManager()->dispatch($event);

                                        Log::error('Error attach this -> receipt_id: ' . $receipt->id, ['scope' => ['MassEmailsController']]);
                                        continue;
                                    }
                                }
                            }
                        }

                        if ($sms_mass_email_sms->account_summary_attach) {

                            $printed = $this->FiscoAfipCompPdf->account_summary($cu_code, TRUE, FALSE, NULL, $paraments->invoicing->add_cobrodigital_account_summary, $paraments->invoicing->add_payu_account_summary, $paraments->invoicing->add_cuentadigital_account_summary);

                            if ($printed != "") {

                                $attach = [
                                    'file'        => $printed,
                                    'mimetype'    => 'application/pdf',
                                    'contentId'   => $cu_code . '',
                                    'disposition' => 'attachment' 
                                ];

                                array_push($attachments, $attach);
                            } else {

                                $no_attach_account_summary_email++;

                                $data_error = new \stdClass; 
                                $data_error->request_data = $customer;
                                $data_error->section = 'Reenvio de correo';

                                $event = new Event('MassEmailsController.Error', $this, [
                                    'msg'   => __('Hubo un error no se genero dinámicamente el PDF del Resumen de Cuenta. Reenvio de coreo'),
                                    'data'  => $data_error,
                                    'flash' => TRUE
                                ]);

                                $this->getEventManager()->dispatch($event);

                                Log::error('Error attach this -> customer_code: ' . $cu_code, ['scope' => ['MassEmailsController']]);
                                continue;
                            }
                        }

                        $debts_total = $this->CurrentAccount->calulateCustomerDebtTotal($customer->code);

                        $fantasy_name = $enterprise->name;
                        if (isset($enterprise->fantasy_name)) {
                            if ($enterprise->fantasy_name != "" && $enterprise->fantasy_name != NULL) {
                                $fantasy_name = $enterprise->fantasy_name;
                            }
                        }

                        if ($sms_mass_email_sms->payment_link_mercadopago || $sms_mass_email_sms->payment_link_cuentadigital) {

                            $data = new \stdClass;
                            $data->payment_link_mercadopago   = $sms_mass_email_sms->payment_link_mercadopago;
                            $data->payment_link_cuentadigital = $sms_mass_email_sms->payment_link_cuentadigital;
                            $data->cd_venc                    = $sms_mass_email_sms->cd_venc;
                            $data->cd_saldo                   = $sms_mass_email_sms->cd_saldo;
                            $data->cd_efectivo                = $sms_mass_email_sms->cd_efectivo;
                            $data->cd_credit_card             = $sms_mass_email_sms->cd_credit_card;

                            $template->message = $this->createLinkPagos($customer, $attach_logo, $enterprise, $data);
                            if ($template->message) {
                                $temp = $template->message;
                            } else {
                                $error_email++;
                                continue;
                            }

                            array_push($attachments, $attach_logo);
                        } else {
                            $temp = str_replace("%cliente_nombre%", $customer->name, $template->message);
                            $temp = str_replace("%cliente_codigo%", $customer->code, $temp);
                            $doc  = $afip_codes['doc_types'][$customer->doc_type] . ' ' . $customer->ident;
                            $temp = str_replace("%cliente_documento%", $doc, $temp);
                            $temp = str_replace("%cliente_usuario_portal%", $customer->ident, $temp);
                            $temp = str_replace("%cliente_clave_portal%", $customer->clave_portal, $temp);
                            $temp = str_replace("%cliente_saldo_total%", $debts_total, $temp);
                            $temp = str_replace("%cliente_saldo_mes%", $customer->debt_month, $temp);
                            $temp = str_replace("%empresa_nombre%", $fantasy_name, $temp);
                            $temp = str_replace("%empresa_domicilio%", $enterprise->address, $temp);
                            $temp = str_replace("%empresa_web%", $enterprise->web, $temp);
                        }

                        $status = "No enviado";

                        $sms_mass_email_sms->status = $status;
                        $this->MassEmailsSms->save($sms_mass_email_sms);

                        //$from = $enterprise->email ? $enterprise->email : $email_data->contact;
                        $from = $email_data->contact;

                        $enterprise_email = $enterprise->web == '' ? $fantasy_name : $enterprise->web;

                        // si usa proveedor de correo
                        if ($paraments->emails_accounts->provider == "") {

                            $email = new Email();
                            $email->transport('mass_emails');
                            $array_email = explode(',', $customer->email);
                            $array_size = sizeof($array_email);

                            if ($array_size == 1) {

                                $email->to([$customer->email => $customer->name]);
                            } else {

                                $email->to([$array_email[0] => $customer->name]);

                                for ($i = 1; $i < $array_size; $i++) {
                                    $email->addTo([$array_email[$i] => $customer->name]);
                                }
                            }
                            $email->from([$from => $enterprise_email]);
                            $email->subject($sms_mass_email_sms->subject);
                            $email->emailFormat('html');
                            $email->template('mass_emails');
                            $email->setViewVars(['message' => $temp]);

                            if (sizeof($attachments) > 0) {
                                $email->attachments($attachments);
                            }

                            try {
                                if ($email->send()) {

                                    $sms_mass_email_sms->status = "Enviado";
                                    $this->MassEmailsSms->save($sms_mass_email_sms);
                                    $send_email++;

                                    $action = 'Envio de Correo exitoso';
                                    $detail = '';
                                    $detail .= 'Código: ' . $customer->code . PHP_EOL;
                                    $detail .= 'Nombre: ' . $customer->name . PHP_EOL;
                                    $detail .= 'Correo: ' . $customer->email . PHP_EOL;
                                    $this->registerActivity($action, $detail, NULL);
                                }
                            } catch (\Exception $e) {
                                $error_email++;
                                $data_error = new \stdClass; 
                                $data_error->request_data = $e->getMessage();
                                $data_error->customer = $customer;
                                $data_error->section = 'Reenvio de correo';

                                $event = new Event('MassEmailsController.Error', $this, [
                                    'msg'   => __('Hubo un error al reenviar los correos.'),
                                    'data'  => $data_error,
                                    'flash' => true
                                ]);

                                $this->getEventManager()->dispatch($event);
                                Log::write('error', $e->getMessage());
                            }
                        } else {

                            // UTILIZA PROVEEDOR DE CORREO
                            if ($paraments->emails_accounts->provider == 'sendgrid') {

                                $html = '<html><head></head><body>' . $temp . '</body></html>';

                                if ($sms_mass_email_sms->payment_link_mercadopago || $sms_mass_email_sms->payment_link_cuentadigital) {
                                    $html = $temp;
                                }
                                
                                $tos = explode(",", $customer->email);
                                $tos_post = [];
                                foreach ($tos as $to) {
                                    $to_boject = new \stdClass;
                                    $to_boject->email = $to;
                                    $to_boject->name = $customer->name;
                                    array_push($tos_post, $to_boject);
                                }

                                $content = new \stdClass;
                                $content->type = "text/html";
                                $content->value = $html;

                                $from_sendgrid = new \stdClass;
                                $from_sendgrid->email = $from;
                                $from_sendgrid->name = $fantasy_name;

                                $reply_to = new \stdClass;
                                $reply_to->email = $from;
                                $reply_to->name = $fantasy_name;

                                if (isset($email_data->copy)) {
                                    if ($email_data->copy == "") {
                                        $from_copy = $from;
                                        $fantasy_name_copy = $fantasy_name;
                                    } else {
                                        $from_copy = $email_data->copy;
                                        $fantasy_name_copy = "";
                                    }
                                } else {
                                    $from_copy = $from;
                                    $fantasy_name_copy = $fantasy_name;
                                }

                                $bcc = new \stdClass;
                                $bcc->email = $from_copy;
                                $bcc->name = $fantasy_name_copy;

                                $personalizations = [
                                    'to' => $tos_post,
                                    'cc' => [
                                        0 => $bcc
                                    ],
                                    'subject' => $sms_mass_email_sms->subject
                                ];

                                $data_sendgrid = new \stdClass;

                                if (sizeof($attachments) > 0) {

                                    foreach ($attachments as $attach) {

                                        $pdf_file = file_get_contents($attach['file']);
                                        $pdf_explode = explode('/', $attach['file']);
                                        $position_name = sizeof($pdf_explode) - 1;

                                        $attachments_sendgrid = new \stdClass;
                                        $attachments_sendgrid->content = base64_encode($pdf_file);
                                        $attachments_sendgrid->type = $attach['mimetype'];
                                        $attachments_sendgrid->filename = $pdf_explode[$position_name];
                                        $attachments_sendgrid->disposition = $attach['disposition'];
                                        $attachments_sendgrid->content_id = $attach['contentId'];
                                        $data_sendgrid->attachments[] = $attachments_sendgrid;
                                    }
                                }

                                $data_sendgrid->personalizations[] = $personalizations;
                                $data_sendgrid->content[] = $content;
                                $data_sendgrid->from = $from_sendgrid;
                                $data_sendgrid->reply_to = $reply_to;
                                $data_sendgrid->bcc = $bcc;

                                $data_string = json_encode($data_sendgrid);

                                try {

                                    $curl = curl_init();

                                    $endpoint_send = $paraments->emails_accounts->providers->sendgrid->endpoint->api->v3->send;
                                    $api_key = $paraments->emails_accounts->providers->sendgrid->api_key;

                                    curl_setopt_array($curl, array(
                                        CURLOPT_URL            => $endpoint_send,
                                        CURLOPT_RETURNTRANSFER => TRUE,
                                        CURLOPT_ENCODING       => "",
                                        CURLOPT_MAXREDIRS      => 10,
                                        CURLOPT_TIMEOUT        => 30,
                                        CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                                        CURLOPT_CUSTOMREQUEST  => "POST",
                                        CURLOPT_POSTFIELDS     => $data_string,
                                        CURLOPT_SSLVERSION     => CURL_SSLVERSION_TLSv1_2,
                                        CURLOPT_HTTPHEADER => array(
                                            "authorization: Bearer " . $api_key,
                                            "content-type: application/json"
                                        ),
                                    ));

                                    $response = curl_exec($curl);
                                    $err = curl_error($curl);
                                    curl_close($curl);

                                    if ($err) {

                                        $error_email++;
                                        $data_error = new \stdClass; 
                                        $data_error->request_data = $err;
                                        $data_error->customer = $customer;
                                        $data_error->section = 'Reenvio de correo';

                                        $event = new Event('MassEmailsController.Error', $this, [
                                            'msg'   => __('Hubo un error al reenviar los correos.'),
                                            'data'  => $data_error,
                                            'flash' => true
                                        ]);

                                        $this->getEventManager()->dispatch($event);
                                        Log::write('error', $err);

                                    } else {

                                        $sms_mass_email_sms->status = "Enviado";
                                        $this->MassEmailsSms->save($sms_mass_email_sms);
                                        $send_email++;

                                        $action = 'Envio de Correo exitoso';
                                        $detail = '';
                                        $detail .= 'Código: ' . $customer->code . PHP_EOL;
                                        $detail .= 'Nombre: ' . $customer->name . PHP_EOL;
                                        $detail .= 'Correo: ' . $customer->email . PHP_EOL;
                                        $this->registerActivity($action, $detail, NULL);
                                    }
                                } catch (\Exception $e) {
                                    $error_email++;
                                    $data_error = new \stdClass; 
                                    $data_error->request_data = $e->getMessage();
                                    $data_error->customer = $customer;
                                    $data_error->section = 'Reenvio de correo';

                                    $event = new Event('MassEmailsController.Error', $this, [
                                        'msg'   => __('Hubo un error al reenviar los correos.'),
                                        'data'  => $data_error,
                                        'flash' => true
                                    ]);

                                    $this->getEventManager()->dispatch($event);
                                    Log::write('error', $e->getMessage());
                                }
                            }
                        }

                        if (sizeof($attachments) > 0) {
                            foreach ($attachments as $attach) {
                                if ($attach['mimetype'] == 'application/pdf') {
                                    unlink($attach['file']);
                                }
                            }
                        }

                        $this->Flash->success(__('Correos enviados: ' . $send_email));

                        if ($error_email > 0) {
                            $this->Flash->warning(__('Correos no enviados: ' . $error_email . '. \n Por favor verifique la configuración de Correo Masivo.'));
                        }

                        if ($no_attach_receipts_email > 0) {
                            $this->Flash->warning(__('Correos sin adjuntar Recibos: ' . $no_attach_receipts_email . '. \n Por favor comuníquese con Soporte Técnico.'));
                        }

                        if ($no_attach_invoices_email > 0) {
                            $this->Flash->warning(__('Correos sin adjuntar Facturas: ' . $no_attach_invoices_email . '. \n Por favor comuníquese con Soporte Técnico.'));
                        }

                        if ($no_attach_account_summary_email > 0) {
                            $this->Flash->warning(__('Correos sin adjuntar Resumen de Cuenta \n Por favor comuníquese con Soporte Técnico.'));
                        }

                        $this->loadModel('Users');
                        $user = $this->Users->get($this->Auth->user()['id']);
                        $user_name = $user->name . ' - username: ' . $user->username;

                        $action =  'Envío masivo de Correos';

                        $cobrodigital_add = $paraments->invoicing->add_cobrodigital_account_summary ? 'Si' : 'No';
                        $payu_add = $paraments->invoicing->add_payu_account_summary ? 'Si' : 'No';
                        $cuentadigital_add = $paraments->invoicing->add_cuentadigital_account_summary ? 'Si' : 'No';
                        $link_mercado_pago = $sms_mass_email_sms->payment_link_mercadopago ? 'Si' : 'No';
                        $link_cuentadigital = $sms_mass_email_sms->payment_link_cuentadigital ? 'Si' : 'No';
                        $account_summary_add = $sms_mass_email_sms->account_summary_attach ? 'Si' : 'No';
                        $invoice_add = $sms_mass_email_sms->invoice_attach ? 'Si' : 'No';
                        $receipt_add = $sms_mass_email_sms->receipt_attach ? 'Si' : 'No';
                        $invoice_no_paid_add = $sms_mass_email_sms->invoice_impagas ? 'Si' : 'No';
                        $invoice_periode = $sms_mass_email_sms->invoice_periode;
                        $receipt_periode = $sms_mass_email_sms->receipt_periode;

                        $detail = 'Bloque: #' . $bloque->id . PHP_EOL;
                        $detail .= 'Fecha: ' . $bloque->confirm_date->format('d/m/Y') . PHP_EOL;
                        $detail .= 'Plantilla: ' . $bloque->template_name . PHP_EOL;
                        $detail .= 'Empresa: ' . $enterprise->name . PHP_EOL;
                        $detail .= 'Adj. Factura: ' . $invoice_add . PHP_EOL;
                        $detail .= 'Período de Fact.: ' . $invoice_periode . PHP_EOL;
                        $detail .= 'Adj. Factura impagas: ' . $invoice_no_paid_add . PHP_EOL;
                        $detail .= 'Adj. Recibo: ' . $receipt_add . PHP_EOL;
                        $detail .= 'Período de Recibo: ' . $receipt_periode . PHP_EOL;
                        $detail .= 'Adj. Resumen de Cuenta: ' . $account_summary_add . PHP_EOL;
                        $detail .= 'Cód. barra Cobro Digital: ' . $cobrodigital_add . PHP_EOL;
                        $detail .= 'Cód. barra Payu: ' . $payu_add . PHP_EOL;
                        $detail .= 'Cód. barra Cuenta Digital: ' . $cuentadigital_add . PHP_EOL;
                        $detail .= 'Link de pago Mercado Pago: ' . $link_mercado_pago . PHP_EOL;
                        $detail .= 'Link de pago Cuenta Digital: ' . $link_cuentadigital . PHP_EOL;

                        if ($sms_mass_email_sms->payment_link_cuentadigital) {

                            $cd_vencimientox = [
                                '' => 'Abierto',
                                1  => 'a 1 día',
                                2  => 'a 2 días',
                                3  => 'a 3 días',
                                4  => 'a 4 días',
                                5  => 'a 5 días',
                                6  => 'a 6 días',
                                7  => 'a 7 días',
                                8  => 'a 8 días',
                                9  => 'a 9 días',
                                10 => 'a 10 días',
                                11 => 'a 11 días',
                                12 => 'a 12 días',
                                13 => 'a 13 días',
                                14 => 'a 14 días',
                                15 => 'a 15 días',
                                16 => 'a 16 días',
                                17 => 'a 17 días',
                                18 => 'a 18 días',
                                19 => 'a 19 días',
                                20 => 'a 20 días',
                                21 => 'a 21 días',
                                22 => 'a 22 días',
                                23 => 'a 23 días',
                                24 => 'a 24 días',
                                25 => 'a 25 días',
                                26 => 'a 26 días',
                                27 => 'a 27 días',
                                28 => 'a 28 días',
                                29 => 'a 29 días',
                                30 => 'a 30 días',
                                31 => 'a 31 días',
                            ];

                            $cd_saldox = [
                                0 => 'Abierto',
                                1 => 'Saldo mes',
                                2 => 'Saldo total'
                            ];

                            $efectivo_cd = $data->cd_efectivo ? 'Si' : 'No';
                            $credit_card_cd = $data->cd_credit_card ? 'Si' : 'No';

                            $detail .= 'Cuenta Digital - vencimiento: ' . $cd_vencimientox[$data->cd_venc] . PHP_EOL;
                            $detail .= 'Cuenta Digital - monto a cobrar: ' . $cd_saldox[$data->cd_saldo] . PHP_EOL;
                            $detail .= 'Cuenta Digital - efectivo: ' . $efectivo_cd . PHP_EOL;
                            $detail .= 'Cuenta Digital - tarjetas de crédito y débito: ' . $credit_card_cd . PHP_EOL;
                        }

                        $detail .= 'Usuario: ' . $user_name . PHP_EOL;
                        $detail .= 'Correos enviados: ' . $send_email . PHP_EOL;
                        $detail .= 'Correos no enviados: ' . $error_email . PHP_EOL;
                        $detail .= 'Correos sin adjuntar Recibos: ' . $no_attach_receipts_email . PHP_EOL;
                        $detail .= 'Correos sin adjuntar Facturas: ' . $no_attach_invoices_email . PHP_EOL;
                        $detail .= 'Correos sin adjuntar Resumen de Cuenta: ' . $no_attach_account_summary_email . PHP_EOL;
                        $detail .= '---------------------------' . PHP_EOL;

                        $this->registerActivity($action, $detail, NULL);

                        return $this->redirect(['action' => $data->redirect]);

                    } else {
                        $this->Flash->warning(__('No se han enviado el correo. Por favor intente nuevamente.'));
                    }
                }
            } else {

                $this->Flash->warning(__('La misma acción se intento realizar varias veces.'));
                return $this->redirect(['action' => 'selectCustomersReSend']);
            }
        }

        return $this->redirect(['action' => $data->redirect]);
    }

    public function messages()
    {
        $paraments = $this->request->getSession()->read('paraments');

        $business = [];
        foreach ($paraments->invoicing->business as $b) {
            $business[$b->id] = $b->name;
        }

        $templates = [];
        $templates_model = $this->MassEmailsTemplates->find();
        foreach ($templates_model as $temp) {
            $templates[$temp->name] = $temp->name;
        }

        $account_emails = [];
        foreach ($paraments->mass_emails->emails as $email) {
            $account_emails[$email->contact] = $email->contact;
        }

        $users = [];
        $this->loadModel('Users');
        $users_model = $this->Users->find();
        foreach ($users_model as $u) {
            $users[$u->id] = $u->name; 
        }

        $this->set(compact('business', 'templates', 'users', 'account_emails'));
    }

    public function getMessages()
    {
        if ($this->request->is('ajax')) {

            $response = new \stdClass();

            $response->draw = intval($this->request->getQuery('draw'));
            $archived = filter_var($this->request->getQuery('archived'), FILTER_VALIDATE_BOOLEAN);
            $response->recordsTotal = 0;
            $response->data = [];
            $response->recordsFiltered = 0;

            $this->loadModel('MassEmailsSms');

            $response->recordsTotal = $this->MassEmailsSms
                ->find()
                ->contain([
                    'MassEmailsBlocks',
                    'Customers'
                ])->where([
                    'MassEmailsSms.archived' => $archived
                ])->count();

            $response->data = $this->MassEmailsSms->find('ServerSideData', [
                'params' => $this->request->getQuery(),
                'where' => ['MassEmailsSms.archived' => $archived] 
            ]);

            $response->recordsFiltered = $this->MassEmailsSms->find('RecordsFiltered', [
                'params' => $this->request->getQuery(),
                'where' => ['MassEmailsSms.archived' => $archived] 
            ]);

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function archivedMessages()
    {
        $paraments = $this->request->getSession()->read('paraments');

        $business = [];
        foreach ($paraments->invoicing->business as $b) {
            $business[$b->id] = $b->name;
        }

        $templates = [];
        $templates_model = $this->MassEmailsTemplates->find();
        foreach ($templates_model as $temp) {
            $templates[$temp->name] = $temp->name;
        }

        $account_emails = [];
        foreach ($paraments->mass_emails->emails as $email) {
            $account_emails[$email->id] = $email->contact;
        }

        $users = [];
        $this->loadModel('Users');
        $users_model = $this->Users->find();
        foreach ($users_model as $u) {
            $users[$u->id] = $u->name;
        }

        $this->set(compact('business', 'templates', 'users', 'account_emails'));
    }

    public function archiveMessages()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Arhivado de Corero';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);
            
            $this->loadModel('MassEmailsSms');

            $data = json_decode($this->request->getData('data'));

            foreach ($data->ids as $id) {
                $massEmailsSms = $this->MassEmailsSms->get($id);
                $massEmailsSms->archived = true;
                $this->MassEmailsSms->save($massEmailsSms);
            }

            $this->Flash->success(__('Correo archivados correctamente.'));
            return $this->redirect(['action' => $data->redirect]);
        }
    }

    public function getCustomers()
    {
        if ($this->request->is('ajax')) {

            $response = new \stdClass();
            $response->draw = intval($this->request->getQuery('draw'));
            $response->recordsTotal = 0;
            $response->data = [];
            $response->recordsFiltered = [];

            $this->loadModel('Customers');

            $customers_total = $this->Customers
                ->find()
                ->contain([
                    'Connections'
                ])->where([
                    'deleted' => FALSE]
                );
                
            $params = $this->request->getQuery();

            // Cuentas
            if ($params['columns'][24]['search']['value'] != '') {

                $payment_getway_id = $params['columns'][24]['search']['value'];

                $customers_total = $this->Customers
                    ->find()
                    ->matching('CustomersAccounts', function ($q) use ($payment_getway_id) {
                        return $q->where([
                            'CustomersAccounts.payment_getway_id' => $payment_getway_id,
                            'CustomersAccounts.deleted'           => 0
                        ]);
                    })
                    ->where([
                        'Customers.deleted' => FALSE
                    ]);
            }

            // Servicio
            if ($params['columns'][9]['search']['value'] != '') {
                
                $customers_ok = [];

                foreach ($customers_total as $customer) {

                    if (sizeof($customer->connections) > 0) {

                        foreach ($customer->connections as $connection) {

                            $enabled = $params['columns'][9]['search']['value'] ? TRUE : FALSE;

                            if ($connection->deleted == FALSE && $connection->enabled == $enabled) {
                                array_push($customers_ok, $customer);
                            }
                        }
                    }
                }
                $customers_total = $customers_ok;
            }

            // controlador
            if ($params['columns'][22]['search']['value'] != '') {

                $customers_ok = [];

                foreach ($customers_total as $customer) {

                    if (sizeof($customer->connections) > 0) {

                        foreach ($customer->connections as $connection) {

                            $controller_id = $params['columns'][22]['search']['value'];

                            if ($connection->deleted == FALSE && $connection->controller_id == $controller_id) {

                                array_push($customers_ok, $customer);
                            }
                        }
                    }
                }
                $customers_total = $customers_ok;
            }

            // servicio
            if ($params['columns'][23]['search']['value'] != '') {

                $customers_ok = [];

                foreach ($customers_total as $customer) {

                    if (sizeof($customer->connections) > 0) {

                        foreach ($customer->connections as $connection) {

                            $service_id = $params['columns'][23]['search']['value'];

                            if ($connection->deleted == FALSE && $connection->service_id == $service_id) {

                                array_push($customers_ok, $customer);
                            }
                        }
                    }
                }
                $customers_total = $customers_ok;
            }

            foreach ($customers_total as $customer_total) {
                if (!$this->verifyMassEmails($customer_total->email)) {
                    $response->recordsTotal++;
                }
            }

            $response->data = $this->Customers->find('ServerSideDataCheckSelection', [
                'params' => $this->request->getQuery(),
                'where'  => ['Customers.deleted' => FALSE],
                'type'   => 'mass_email'
            ]);

            $response->recordsFiltered = $this->Customers->find('RecordsFilteredCheckSelection', [
                'params' => $this->request->getQuery(),
                'where'  => ['Customers.deleted' => FALSE],
                'type'   => 'mass_email'
            ]);

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function getCustomersWithArchived()
    {
        if ($this->request->is('ajax')) {

            $response = new \stdClass();
            $response->draw = intval($this->request->getQuery('draw'));
            $response->recordsTotal = 0;
            $response->data = [];
            $response->recordsFiltered = [];

            $this->loadModel('Customers');

            $customers_total = $this->Customers
                ->find()
                ->contain([
                    'Connections'
                ])->where([
                    'super_deleted' => FALSE]
                );
                
            $params = $this->request->getQuery();

            // Cuentas
            if ($params['columns'][24]['search']['value'] != '') {

                $payment_getway_id = $params['columns'][24]['search']['value'];

                $customers_total = $this->Customers
                    ->find()
                    ->matching('CustomersAccounts', function ($q) use ($payment_getway_id) {
                        return $q->where([
                            'CustomersAccounts.payment_getway_id' => $payment_getway_id,
                            'CustomersAccounts.deleted'           => 0
                        ]);
                    })
                    ->where([
                        'Customers.super_deleted' => FALSE
                    ]);
            }

            // Servicio
            if ($params['columns'][9]['search']['value'] != '') {
                
                $customers_ok = [];

                foreach ($customers_total as $customer) {

                    if (sizeof($customer->connections) > 0) {

                        foreach ($customer->connections as $connection) {

                            $enabled = $params['columns'][9]['search']['value'] ? TRUE : FALSE;

                            if ($connection->deleted == FALSE && $connection->enabled == $enabled) {
                                array_push($customers_ok, $customer);
                            }
                        }
                    }
                }
                $customers_total = $customers_ok;
            }

            // controlador
            if ($params['columns'][22]['search']['value'] != '') {

                $customers_ok = [];

                foreach ($customers_total as $customer) {

                    if (sizeof($customer->connections) > 0) {

                        foreach ($customer->connections as $connection) {

                            $controller_id = $params['columns'][22]['search']['value'];

                            if ($connection->deleted == FALSE && $connection->controller_id == $controller_id) {

                                array_push($customers_ok, $customer);
                            }
                        }
                    }
                }
                $customers_total = $customers_ok;
            }

            // servicio
            if ($params['columns'][23]['search']['value'] != '') {

                $customers_ok = [];

                foreach ($customers_total as $customer) {

                    if (sizeof($customer->connections) > 0) {

                        foreach ($customer->connections as $connection) {

                            $service_id = $params['columns'][23]['search']['value'];

                            if ($connection->deleted == FALSE && $connection->service_id == $service_id) {

                                array_push($customers_ok, $customer);
                            }
                        }
                    }
                }
                $customers_total = $customers_ok;
            }

            foreach ($customers_total as $customer_total) {
                if (!$this->verifyMassEmails($customer_total->email)) {
                    $response->recordsTotal++;
                }
            }

            $response->data = $this->Customers->find('ServerSideDataCheckSelectionWithArchived', [
                'params' => $this->request->getQuery(),
                'where'  => ['Customers.super_deleted' => FALSE],
                'type'   => 'mass_email'
            ]);

            $response->recordsFiltered = $this->Customers->find('RecordsFilteredCheckSelectionWithArchived', [
                'params' => $this->request->getQuery(),
                'where'  => ['Customers.super_deleted' => FALSE],
                'type'   => 'mass_email'
            ]);

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function fixCustomers()
    {
        $this->loadModel('Customers');

        if ($this->request->is('post')) {

            $action = 'Corrección de Correos';
            $detail = "";
            $this->registerActivity($action, $detail, NULL, TRUE);

            $customs = json_decode($this->request->getData('customers'));
            $no_customers = json_decode($this->request->getData('no_customers'));
            $count = 0;
            foreach ($customs as $custom) {
                $customer = $this->Customers->get($custom->id);
                $customer->email = $custom->email;
                $this->Customers->save($customer);
                $count++;
            }
            if (!empty($no_customers)) {
                $message = "Cliente cargados: $count - Clientes no cargados: ";
                foreach ($no_customers as $no_customer) {
                    $message .= 'Código: ' . $no_customer->id . ' | Correo: ' . $no_customer->email . ' - ';
                }
                $this->Flash->warning($message);
            } else {
                $this->Flash->success(__("Se ha modificado correctamente $count Correos"));
            }
            return $this->redirect(['action' => 'fixCustomers']);
        }

        $customers_complete = $this->Customers
            ->find()
            ->contain([
                'Areas'
            ])->where([
                'Customers.deleted'       => FALSE, 
                'Customers.super_deleted' => FALSE
            ]);
        $customers = [];
        foreach ($customers_complete as $customer_complete) {
            if ($this->verifyMassEmails($customer_complete->email)) {
                array_push($customers, $customer_complete);
            }
        }

        $this->set(compact('customers'));
        $this->set('_serialize', ['customers']);
    }

    function verifyMassEmails($email)
    {
        $email_part = explode(",", $email);
        $validate = false;
        foreach ($email_part as $e) {
            $validate = !filter_var(trim($e), FILTER_VALIDATE_EMAIL);
        }
        return $validate;
    }

    public function sendEmailTest()
    {
        //ajax Detection
        if ($this->request->is('ajax')) {

            $paraments = $this->request->getSession()->read('paraments');
            $to = $this->request->input('json_decode')->email;
            $html = $this->request->input('json_decode')->html;
            $subject = $this->request->input('json_decode')->subject;
            $business_billing = $this->request->input('json_decode')->business_billing;
            $email_id = $this->request->input('json_decode')->email_id;

            $email_data = NULL;
            foreach ($paraments->invoicing->business as $b) {
                if ($b->id == $business_billing) {
                    $enterprise = $b;
                }
            }

            foreach ($paraments->mass_emails->emails as $email) {
                if ($email->id == $email_id) {
                    $email_data = $email;
                }
            }

            if ($email_data == NULL) {
                $this->Flash->error(__('No se ha enviado el correo. No coincide su cuenta de correo con la configuración.'));
                return $this->redirect($this->referer());
            }

            $flag = FALSE;
            $message = "No se ha podido enviar el Correo: " . $to;
            $type = "warning";

            // si usa proveedor de correo
            if ($paraments->emails_accounts->provider == "") {
                try {
                    Email::configTransport('mass_emails', [
                        'className' => $email_data->class_name,
                        'host'      => $email_data->host,
                        'port'      => $email_data->port,
                        'timeout'   => $email_data->timeout,
                        'username'  => $email_data->user,
                        'password'  => $email_data->pass,
                        'client'    => $email_data->client == "" ? NULL : $email_data->client,
                        'tls'       => $email_data->tls == "" ? NULL : $email_data->tls,
                        'url'       => env('EMAIL_TRANSPORT_DEFAULT_URL', NULL)
                    ]);
                    $flag = true;
                } catch (\Exception $e) {
                    Log::write('error', $e->getMessage());
                    $message = "No se ha podido enviar el Correo: " . $to;
                    $type = "warning";
                }
            } else {
                $flag = TRUE;
            }

            $type = "warning";
            $message = "El Correo no se ha enviado.";

            if ($flag) {

                $fantasy_name = $enterprise->name;
                if (isset($enterprise->fantasy_name)) {
                    if ($enterprise->fantasy_name != "" && $enterprise->fantasy_name != NULL) {
                        $fantasy_name = $enterprise->fantasy_name;
                    }
                }
                $html = str_replace("%empresa_nombre%", $fantasy_name, $html);
                $html = str_replace("%empresa_domicilio%", $enterprise->address, $html);
                $html = str_replace("%empresa_web%", $enterprise->web, $html);

                //$from = $enterprise->email ? $enterprise->email : $email_data->contact;
                $from = $email_data->contact;

                // si usa proveedor de correo
                if ($paraments->emails_accounts->provider == "") {

                    $email = new Email();
                    $email->transport('mass_emails');

                    $array_email = explode(',', $to);
                    $array_size = sizeof($array_email);

                    if ($array_size == 1) {

                        $email->to($to);
                    } else {

                        $email->to($array_email[0]);

                        for ($i = 1; $i < $array_size; $i++) {
                            $email->addTo($array_email[$i]);
                        }
                    }
                    $email->from($from);
                    $email->subject($subject);
                    $email->emailFormat('html');
                    $email->template('mass_emails');
                    $email->setViewVars(['message' => $html]);

                    if ($email->send()) {
                        $type = "success";
                        $message = "El Correo se ha enviado correctamente.";
                    }
                } else {
                    
                    // si usa proveedor de correo
                    if ($paraments->emails_accounts->provider == "sendgrid") {

                        $html = '<html><head></head><body>' . $html . '</body></html>';

                        $tos = explode(",", $to);
                        $tos_post = [];
                        foreach ($tos as $to) {
                        	$to_boject = new \stdClass;
                        	$to_boject->email = $to;
                        	$to_boject->name = "prueba";
                        	array_push($tos_post, $to_boject);
                        }

                        $content = new \stdClass;
                        $content->type = "text/html";
                        $content->value = $html;

                        $from_sendgrid = new \stdClass;
                        $from_sendgrid->email = $from;
                        $from_sendgrid->name = $fantasy_name;

                        $reply_to = new \stdClass;
                        $reply_to->email = $from;
                        $reply_to->name = $fantasy_name;

                        if (isset($email_data->copy)) {
                            if ($email_data->copy == "") {
                                $from_copy = $from;
                                $fantasy_name_copy = $fantasy_name;
                            } else {
                                $from_copy = $email_data->copy;
                                $fantasy_name_copy = "";
                            }
                        } else {
                            $from_copy = $from;
                            $fantasy_name_copy = $fantasy_name;
                        }

                        $bcc = new \stdClass;
                        $bcc->email = $from_copy;
                        $bcc->name = $fantasy_name_copy;

                        $personalizations = [
                            'to' => $tos_post,
                            'cc' => [
                                0 => $bcc
                            ],
                            'subject' => $subject
                        ];

                        $data = new \stdClass;
                        $data->personalizations[] = $personalizations;
                        $data->content[] = $content;
                        $data->from = $from_sendgrid;
                        $data->reply_to = $reply_to;
                        $data->bcc = $bcc;

                        $data_string = json_encode($data);

                        try {

                            $curl = curl_init();

                            $endpoint_send = $paraments->emails_accounts->providers->sendgrid->endpoint->api->v3->send;
                            $api_key = $paraments->emails_accounts->providers->sendgrid->api_key;

                            curl_setopt_array($curl, array(
                                CURLOPT_URL            => $endpoint_send,
                                CURLOPT_RETURNTRANSFER => TRUE,
                                CURLOPT_ENCODING       => "",
                                CURLOPT_MAXREDIRS      => 10,
                                CURLOPT_TIMEOUT        => 30,
                                CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST  => "POST",
                                CURLOPT_POSTFIELDS     => $data_string,
                                CURLOPT_SSLVERSION     => CURL_SSLVERSION_TLSv1_2,
                                CURLOPT_HTTPHEADER => array(
                                    "authorization: Bearer " . $api_key,
                                    "content-type: application/json"
                                ),
                            ));

                            $response = curl_exec($curl);
                            $err = curl_error($curl);
                            curl_close($curl);

                            if ($err) {

                                //$this->log($err, 'debug');
                            } else {

                                $type = "success";
                                $message = "El Correo se ha enviado correctamente.";
                            }

                        } catch (\Exception $e) {
                            //$this->log($e->getMessage(), 'debug');
                        }
                    }
                }
            }

            $this->set(compact('message', 'type'));
        }
    }

    public function disableEmail()
    {
        $paraments = $this->request->getSession()->read('paraments');

        if ($this->request->is(['patch', 'post', 'put'])) {

            $action = 'Deshabilitación de Cuenta de Correos';
            $detail = "";
            $this->registerActivity($action, $detail, NULL, TRUE);

            foreach ($paraments->mass_emails->emails as $email) {

                if ($email->id == $_POST['id']) {
                    $email->enabled = FALSE;

                    $action = 'Cuenta Guardada' . PHP_EOL;
                    $detail = 'Habilitar: ' . ($email->enabled ? 'Si' : 'No') . PHP_EOL;
                    $detail .= 'Cuenta: ' . $email->contact . PHP_EOL;
                    $this->registerActivity($action, $detail, NULL);
                }
            }

            //se guarda los cambios
            $this->saveGeneralParaments($paraments);
        }

        return $this->redirect(['action' => 'config']);
    }

    public function sendEmail()
    {
        //ajax Detection
        if ($this->request->is('ajax')) {

            $action = 'Envio de Correo seleccionando comprobante';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $paraments = $this->request->getSession()->read('paraments');
            $model = $this->request->input('json_decode')->model;
            $model_id = $this->request->input('json_decode')->model_id;
            $template_id = $this->request->input('json_decode')->template_id;

            $this->loadModel($model);

            $contain = ['Customers'];
            $where = [
                'id' => $model_id
            ];

            if ($model == 'Presales') {

                $contain = [
                    'Debts.CustomersHasDiscounts',
                    'Users',
                    'Customers.Areas',
                    'Customers.Cities',
                    'ServicePending'
                ];

                $where = [
                    'Presales.id' => $model_id
                ];
            }

            $object_model = $this->$model
                ->find()
                ->contain($contain)
                ->where($where)
                ->first();

            $to = $object_model->customer->email;

            if ($to) {

                $this->loadModel('MassEmailsTemplates');
                $template = $this->MassEmailsTemplates
                    ->find()
                    ->where([
                        'id' => $template_id
                    ])
                    ->first();

                $email_data = NULL;
                foreach ($paraments->mass_emails->emails as $email) {
                    if ($email->id == $template->email_id) {
                        $email_data = $email;
                    }
                }

                if ($email_data == NULL) {

                    $response = [
                        'type'    => "error",
                        'message' => "No se ha enviado el Correo. No coincide su cuenta de Correo con la configuración.",
                        'error'   => TRUE
                    ];
                } else {

                    $enterprise = NULL;
                    foreach ($paraments->invoicing->business as $b) {
                        if ($b->id == $template->business_billing) {
                            $enterprise = $b;
                        }
                    }

                    if ($enterprise == NULL) {

                        $response = [
                            'type'    => "error",
                            'message' => "No se ha enviado el Correo. No se encuentra la plantilla seleccionada.",
                            'error'   => TRUE
                        ];
                    } else {

                        $response = [
                            'type'    => "warning",
                            'message' => "No se ha podido enviar el Correo: " . $to,
                            'error'   => TRUE
                        ];

                        $flag = FALSE;

                        // UTILIZA PROVEEDOR DE CORREO
                        if ($paraments->emails_accounts->provider == '') {

                            try {
                                Email::configTransport('mass_emails', [
                                    'className' => $email_data->class_name,
                                    'host'      => $email_data->host,
                                    'port'      => $email_data->port,
                                    'timeout'   => $email_data->timeout,
                                    'username'  => $email_data->user,
                                    'password'  => $email_data->pass,
                                    'client'    => $email_data->client == "" ? NULL : $email_data->client,
                                    'tls'       => $email_data->tls == "" ? NULL : $email_data->tls,
                                    'url'       => env('EMAIL_TRANSPORT_DEFAULT_URL', NULL)
                                ]);
                                $flag = TRUE;
                            } catch (\Exception $e) {
                                $data_error = new \stdClass;
                                $data_error->request_data = $e->getMessage();

                                $event = new Event('MassEmailsController.Error', $this, [
                                    'msg'   => __('Hubo un error al enviar los correos.'),
                                    'data'  => $data_error,
                                    'flash' => true
                                ]);

                                $this->getEventManager()->dispatch($event);
                                Log::write('error', $e->getMessage());
                                $response = [
                                    'type'    => "warning",
                                    'message' => "No se ha podido enviar el Correo: " . $to,
                                    'error'   => TRUE
                                ];
                            }
                        } else {
                            $flag = TRUE;
                        }

                        if ($flag) {

                            $afip_codes = $this->request->getSession()->read('afip_codes');

                            $debts_total = $this->CurrentAccount->calulateCustomerDebtTotal($object_model->customer->code);

                            $temp = str_replace("%cliente_nombre%", $object_model->customer->name, $template->message);
                            $temp = str_replace("%cliente_codigo%", $object_model->customer->code, $temp);
                            $doc  = $afip_codes['doc_types'][$object_model->customer->doc_type] . ' ' . $object_model->customer->ident;
                            $temp = str_replace("%cliente_documento%", $doc, $temp);
                            $temp = str_replace("%cliente_usuario_portal%", $object_model->customer->ident, $temp);
                            $temp = str_replace("%cliente_clave_portal%", $object_model->customer->clave_portal, $temp);
                            $temp = str_replace("%cliente_saldo_total%", $debts_total, $temp);
                            $temp = str_replace("%cliente_saldo_mes%", $object_model->customer->debt_month, $temp);

                            $fantasy_name = $enterprise->name;
                            if (isset($enterprise->fantasy_name)) {
                                if ($enterprise->fantasy_name != "" && $enterprise->fantasy_name != NULL) {
                                    $fantasy_name = $enterprise->fantasy_name;
                                }
                            }

                            $temp = str_replace("%empresa_nombre%", $fantasy_name, $temp);
                            $temp = str_replace("%empresa_domicilio%", $enterprise->address, $temp);
                            $temp = str_replace("%empresa_web%", $enterprise->web, $temp);

                            //$from = $enterprise->email ? $enterprise->email : $email_data->contact;
                            $from = $email_data->contact;

                            $printed = "";
                            $comprobante = "";

                            switch ($model) {

                                case 'Presales':
                                    $comprobante = "Venta";
                                    $printed = $this->CreatePresalePdf->generate($object_model, TRUE);
                                    break;

                                case 'Invoices':
                                    $comprobante = "Factura";
                                    $printed = $this->FiscoAfipCompPdf->invoice($object_model, TRUE);
                                    break;

                                case 'DebitNotes':
                                    $comprobante = "Nota de Débito";
                                    $printed = $this->FiscoAfipCompPdf->debitNote($object_model, TRUE);
                                    break;

                                case 'CreditNotes':
                                    $comprobante = "Nota de Crédito";
                                    $printed = $this->FiscoAfipCompPdf->creditNote($object_model, TRUE);
                                    break;

                                case 'Receipts':
                                    $comprobante = "Recibo";
                                    $printed = $this->FiscoAfipCompPdf->receipt($object_model, TRUE);
                                    break;

                                case 'Presupuestos':
                                    $comprobante = "Presupuesto";
                                    $printed = $this->FiscoAfipCompPdf->presupuesto($object_model, TRUE);
                                    break;

                                case 'Presume':
                                    break;
                            }

                            $attachments = [];
                            $todo_ok = TRUE;

                            if ($printed != "") {

                                $attach = [
                                    'file'      => $printed,
                                    'mimetype'  => 'application/pdf',
                                    'contentId' => $object_model->id
                                ];
                                array_push($attachments, $attach);
                            } else {
                                $data_error = new \stdClass; 
                                $data_error->request_data = $object_model->customer;
                                $data_error->section = 'Envio de correo adjuntando comprobante';

                                $event = new Event('MassEmailsController.Error', $this, [
                                    'msg'   => __('Hubo un error no se genero dinámicamente el PDF ' . $comprobante),
                                    'data'  => $data_error,
                                    'flash' => TRUE
                                ]);

                                $this->getEventManager()->dispatch($event);

                                $response = [
                                    'type'    => "error",
                                    'message' => "Error al adjuntar el comprobante al correo.",
                                    'error'   => FALSE
                                ];
                                
                                $todo_ok = FALSE;;
                            }

                            if ($todo_ok) {
                                // si usa proveedor de correo
                                if ($paraments->emails_accounts->provider == "") {

                                    $email = new Email();
                                    $email->transport('mass_emails');
                                    $array_email = explode(',', $to);
                                    $array_size = sizeof($array_email);

                                    if ($array_size == 1) {

                                        $email->to($to);
                                    } else {

                                        $email->to($array_email[0]);

                                        for ($i = 1; $i < $array_size; $i++) {
                                            $email->addTo($array_email[$i]);
                                        }
                                    }
                                    $email->from($from);
                                    $email->subject($template->subject);
                                    $email->emailFormat('html');
                                    $email->template('mass_emails');
                                    $email->setViewVars(['message' => $temp]);

                                    if (sizeof($attachments) > 0) {
                                        $email->attachments($attachments);
                                    }

                                    if ($email->send()) {

                                        $response = [
                                            'type'    => "success",
                                            'message' => "El Correo se ha enviado correctamente.",
                                            'error'   => FALSE
                                        ];

                                        $action = 'Envio de Correo exitoso';
                                        $detail = '';
                                        $detail .= 'Código: ' . $object_model->customer->code . PHP_EOL;
                                        $detail .= 'Nombre: ' . $object_model->customer->name . PHP_EOL;
                                        $detail .= 'Correo: ' . $object_model->customer->email . PHP_EOL;
                                        $this->registerActivity($action, $detail, NULL);
                                    } else {

                                        $data_error = new \stdClass; 
                                        $data_error->customer = $object_model->customer;
                                        $data_error->section = 'Envio de correo adjuntando comprobante';

                                        $event = new Event('MassEmailsController.Error', $this, [
                                            'msg'   => __('Hubo un error el correo no se ha enviado.'),
                                            'data'  => $data_error,
                                            'flash' => true
                                        ]);

                                        $this->getEventManager()->dispatch($event);

                                        $response = [
                                            'type'    => "error",
                                            'message' => "El Correo no se ha enviado.",
                                            'error'   => FALSE
                                        ];
                                    }
                                } else {

                                    // UTILIZA PROVEEDOR DE CORREO
                                    if ($paraments->emails_accounts->provider == 'sendgrid') {

                                        $html = '<html><head></head><body>' . $temp . '</body></html>';

                                        $tos = explode(",", $object_model->customer->email);
                                        $tos_post = [];
                                        foreach ($tos as $to) {
                                        	$to_boject = new \stdClass;
                                        	$to_boject->email = $to;
                                        	$to_boject->name = $object_model->customer->name;
                                        	array_push($tos_post, $to_boject);
                                        }

                                        $content = new \stdClass;
                                        $content->type = "text/html";
                                        $content->value = $html;

                                        $from_sendgrid = new \stdClass;
                                        $from_sendgrid->email = $from;
                                        $from_sendgrid->name = $fantasy_name;

                                        $reply_to = new \stdClass;
                                        $reply_to->email = $from;
                                        $reply_to->name = $fantasy_name;

                                        if (isset($email_data->copy)) {
                                            if ($email_data->copy == "") {
                                                $from_copy = $from;
                                                $fantasy_name_copy = $fantasy_name;
                                            } else {
                                                $from_copy = $email_data->copy;
                                                $fantasy_name_copy = "";
                                            }
                                        } else {
                                            $from_copy = $from;
                                            $fantasy_name_copy = $fantasy_name;
                                        }

                                        $bcc = new \stdClass;
                                        $bcc->email = $from_copy;
                                        $bcc->name = $fantasy_name_copy;

                                        $personalizations = [
                                            'to' => $tos_post,
                                            'cc' => [
                                                0 => $bcc
                                            ],
                                            'subject' => $template->subject
                                        ];

                                        $data = new \stdClass;

                                        if (sizeof($attachments) > 0) {

                                            foreach ($attachments as $attach) {

                                                $pdf_file = file_get_contents($attach['file']);
                                                $pdf_explode = explode('/', $attach['file']);
                                                $position_name = sizeof($pdf_explode) - 1;

                                                $attachments_sendgrid = new \stdClass;
                                                $attachments_sendgrid->content = base64_encode($pdf_file);
                                                $attachments_sendgrid->type = "application/pdf";
                                                $attachments_sendgrid->filename = $pdf_explode[$position_name];
                                                $attachments_sendgrid->disposition = "attachment";
                                                $attachments_sendgrid->content_id = "";
                                                $data->attachments[] = $attachments_sendgrid;
                                            }
                                        }

                                        $data->personalizations[] = $personalizations;
                                        $data->content[] = $content;
                                        $data->from = $from_sendgrid;
                                        $data->reply_to = $reply_to;
                                        $data->bcc = $bcc;

                                        $data_string = json_encode($data);

                                        try {

                                            $curl = curl_init();

                                            $endpoint_send = $paraments->emails_accounts->providers->sendgrid->endpoint->api->v3->send;
                                            $api_key = $paraments->emails_accounts->providers->sendgrid->api_key;

                                            curl_setopt_array($curl, array(
                                                CURLOPT_URL            => $endpoint_send,
                                                CURLOPT_RETURNTRANSFER => TRUE,
                                                CURLOPT_ENCODING       => "",
                                                CURLOPT_MAXREDIRS      => 10,
                                                CURLOPT_TIMEOUT        => 30,
                                                CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                                                CURLOPT_CUSTOMREQUEST  => "POST",
                                                CURLOPT_POSTFIELDS     => $data_string,
                                                CURLOPT_SSLVERSION     => CURL_SSLVERSION_TLSv1_2,
                                                CURLOPT_HTTPHEADER => array(
                                                    "authorization: Bearer " . $api_key,
                                                    "content-type: application/json"
                                                ),
                                            ));

                                            $response = curl_exec($curl);
                                            $err = curl_error($curl);
                                            curl_close($curl);

                                            if ($err) {

                                                $data_error = new \stdClass; 
                                                $data_error->request_data = $err;
                                                $data_error->customer = $object_model->customer;
                                                $data_error->section = 'Envio de correo adjuntando comprobante';

                                                $event = new Event('MassEmailsController.Error', $this, [
                                                    'msg'   => __('Hubo un error al enviar los correos.'),
                                                    'data'  => $data_error,
                                                    'flash' => true
                                                ]);

                                                $this->getEventManager()->dispatch($event);
                                                $response = [
                                                    'type'    => "error",
                                                    'message' => "El Correo no se ha enviado.",
                                                    'error'   => FALSE
                                                ];
                                                Log::write('error', $err);
                                            } else {

                                                $response = [
                                                    'type'    => "success",
                                                    'message' => "El Correo se ha enviado correctamente.",
                                                    'error'   => FALSE
                                                ];

                                                $action = 'Envio de Correo exitoso';
                                                $detail = '';
                                                $detail .= 'Código: ' . $object_model->customer->code . PHP_EOL;
                                                $detail .= 'Nombre: ' . $object_model->customer->name . PHP_EOL;
                                                $detail .= 'Correo: ' . $object_model->customer->email . PHP_EOL;
                                                $this->registerActivity($action, $detail, NULL);
                                            }

                                        } catch (\Exception $e) {
                                            $data_error = new \stdClass; 
                                            $data_error->request_data = $e->getMessage();
                                            $data_error->customer = $object_model->customer;
                                            $data_error->section = 'Envio de correo adjuntando comprobante';

                                            $event = new Event('MassEmailsController.Error', $this, [
                                                'msg'   => __('Hubo un error al enviar los correos.'),
                                                'data'  => $data_error,
                                                'flash' => true
                                            ]);

                                            $this->getEventManager()->dispatch($event);
                                            $response = [
                                                'type'    => "error",
                                                'message' => "El Correo no se ha enviado.",
                                                'error'   => FALSE
                                            ];
                                            Log::write('error', $e->getMessage());
                                        }
                                    }
                                }

                                if (sizeof($attachments) > 0) {
                                    unlink($printed);
                                }
                            }
                        }
                    }
                }
            } else {

                $response = [
                    'type'    => "error",
                    'message' => "No tiene Correo Electrónico el Cliente.",
                    'error'   => TRUE
                ];

                $data_error = new \stdClass; 
                $data_error->request_data = $object_model->customer;

                $event = new Event('MassEmailsController.Error', $this, [
                    'msg'   => __('Error al enviar el correo, el cliente no tiene correo'),
                    'data'  => $data_error,
                    'flash' => TRUE
                ]);

                $this->getEventManager()->dispatch($event);
            }

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function sendEmailAccountSummary()
    {
        //ajax Detection
        if ($this->request->is('ajax')) {

            $action = 'Envio de Resumen de Cuenta por Correo';
            $detail = '';
            $this->registerActivity($action, $detail, NULL, TRUE);

            $paraments = $this->request->getSession()->read('paraments');
            $customer_code = $this->request->input('json_decode')->customer_code;
            $template_id = $this->request->input('json_decode')->template_id;

            $this->loadModel('Customers');
            $customer = $this->Customers
                ->find()
                ->where([
                    'code' => $customer_code
                ])
                ->first();

            $to = $customer->email;

            if ($to) {

                $this->loadModel('MassEmailsTemplates');
                $template = $this->MassEmailsTemplates
                    ->find()
                    ->where([
                        'id' => $template_id
                    ])
                    ->first();

                $email_data = NULL;
                foreach ($paraments->mass_emails->emails as $email) {
                    if ($email->id == $template->email_id) {
                        $email_data = $email;
                    }
                }

                if ($email_data == NULL) {

                    $response = [
                        'type'    => "error",
                        'message' => "No se ha enviado el Correo. No coincide su cuenta de Correo con la configuración.",
                        'error'   => TRUE
                    ];
                } else {

                    $enterprise = NULL;
                    foreach ($paraments->invoicing->business as $b) {
                        if ($b->id == $template->business_billing) {
                            $enterprise = $b;
                        }
                    }

                    if ($enterprise == NULL) {

                        $response = [
                            'type'    => "error",
                            'message' => "No se ha enviado el Correo. No se encuentra la plantilla seleccionada.",
                            'error'   => TRUE
                        ];
                    } else {

                        $response = [
                            'type'    => "warning",
                            'message' => "No se ha podido enviar el Correo: " . $to,
                            'error'   => TRUE
                        ];

                        $flag = FALSE;

                        // UTILIZA PROVEEDOR DE CORREO
                        if ($paraments->emails_accounts->provider == '') {

                            try {
                                Email::configTransport('mass_emails', [
                                    'className' => $email_data->class_name,
                                    'host'      => $email_data->host,
                                    'port'      => $email_data->port,
                                    'timeout'   => $email_data->timeout,
                                    'username'  => $email_data->user,
                                    'password'  => $email_data->pass,
                                    'client'    => $email_data->client == "" ? NULL : $email_data->client,
                                    'tls'       => $email_data->tls == "" ? NULL : $email_data->tls,
                                    'url'       => env('EMAIL_TRANSPORT_DEFAULT_URL', NULL)
                                ]);
                                $flag = TRUE;
                            } catch (\Exception $e) {
                                $data_error = new \stdClass; 
                                $data_error->request_data = $e->getMessage();
                                $data_error->customer = $customer;
                                $data_error->section = 'Envio de Resumen de Cuenta';

                                $event = new Event('MassEmailsController.Error', $this, [
                                    'msg'   => __('Hubo un error al enviar los correos.'),
                                    'data'  => $data_error,
                                    'flash' => true
                                ]);

                                $this->getEventManager()->dispatch($event);
                                Log::write('error', $e->getMessage());
                                $response = [
                                    'type'    => "warning",
                                    'message' => "No se ha podido enviar el Correo: " . $to,
                                    'error'   => TRUE
                                ];
                            }
                        } else {
                            $flag = TRUE;
                        }

                        if ($flag) {

                            $afip_codes = $this->request->getSession()->read('afip_codes');

                            $debts_total = $this->CurrentAccount->calulateCustomerDebtTotal($customer->code);

                            $temp = str_replace("%cliente_nombre%", $customer->name, $template->message);
                            $temp = str_replace("%cliente_codigo%", $customer->code, $temp);
                            $doc  = $afip_codes['doc_types'][$customer->doc_type] . ' ' . $customer->ident;
                            $temp = str_replace("%cliente_documento%", $doc, $temp);
                            $temp = str_replace("%cliente_usuario_portal%", $customer->ident, $temp);
                            $temp = str_replace("%cliente_clave_portal%", $customer->clave_portal, $temp);
                            $temp = str_replace("%cliente_saldo_total%", $debts_total, $temp);
                            $temp = str_replace("%cliente_saldo_mes%", $customer->debt_month, $temp);

                            $fantasy_name = $enterprise->name;
                            if (isset($enterprise->fantasy_name)) {
                                if ($enterprise->fantasy_name != "" && $enterprise->fantasy_name != NULL) {
                                    $fantasy_name = $enterprise->fantasy_name;
                                }
                            }

                            $temp = str_replace("%empresa_nombre%", $fantasy_name, $temp);
                            $temp = str_replace("%empresa_domicilio%", $enterprise->address, $temp);
                            $temp = str_replace("%empresa_web%", $enterprise->web, $temp);

                            //$from = $enterprise->email ? $enterprise->email : $email_data->contact;
                            $from = $email_data->contact;

                            $printed = $this->FiscoAfipCompPdf->account_summary($customer_code, TRUE, FALSE, NULL, $paraments->invoicing->add_cobrodigital_account_summary, $paraments->invoicing->add_payu_account_summary, $paraments->invoicing->add_cuentadigital_account_summary);

                            $attachments = [];
                            $todo_ok = TRUE;

                            if ($printed != "") {

                                $attach = [
                                    'file'      => $printed,
                                    'mimetype'  => 'application/pdf',
                                    'contentId' => $customer_code
                                ];
                                array_push($attachments, $attach);
                            } else {
                                $todo_ok = FALSE;
                                $data_error = new \stdClass;
                                $data_error->request_data = $customer;
                                $data_error->section = 'Envio de Resumen de Cuenta';

                                $event = new Event('MassEmailsController.Error', $this, [
                                    'msg'   => __('Hubo un error no se genero dinámicamente el PDF de Resumen de Cuenta.'),
                                    'data'  => $data_error,
                                    'flash' => TRUE
                                ]);

                                $this->getEventManager()->dispatch($event);
                            }

                            if ($todo_ok) {
                                // si usa proveedor de correo
                                if ($paraments->emails_accounts->provider == "") {

                                    $email = new Email();
                                    $email->transport('mass_emails');
                                    $array_email = explode(',', $to);
                                    $array_size = sizeof($array_email);

                                    if ($array_size == 1) {

                                        $email->to($to);
                                    } else {

                                        $email->to($array_email[0]);

                                        for ($i = 1; $i < $array_size; $i++) {
                                            $email->addTo($array_email[$i]);
                                        }
                                    }
                                    $email->from($from);
                                    $email->subject($template->subject);
                                    $email->emailFormat('html');
                                    $email->template('mass_emails');
                                    $email->setViewVars(['message' => $temp]);

                                    if (sizeof($attachments) > 0) {
                                        $email->attachments($attachments);
                                    }

                                    if ($email->send()) {

                                        $response = [
                                            'type'    => "success",
                                            'message' => "El Correo se ha enviado correctamente.",
                                            'error'   => FALSE
                                        ];

                                        $action = 'Envio de Correo exitoso';
                                        $detail = '';
                                        $detail .= 'Código: ' . $customer->code . PHP_EOL;
                                        $detail .= 'Nombre: ' . $customer->name . PHP_EOL;
                                        $detail .= 'Correo: ' . $customer->email . PHP_EOL;
                                        $this->registerActivity($action, $detail, NULL);
                                    } else {

                                        $response = [
                                            'type'    => "error",
                                            'message' => "El Correo no se ha enviado.",
                                            'error'   => FALSE
                                        ];
                                    }
                                } else {

                                    // UTILIZA PROVEEDOR DE CORREO
                                    if ($paraments->emails_accounts->provider == 'sendgrid') {

                                        $html = '<html><head></head><body>' . $temp . '</body></html>';

                                        $tos = explode(",", $customer->email);
                                        $tos_post = [];
                                        foreach ($tos as $to) {
                                        	$to_boject = new \stdClass;
                                        	$to_boject->email = $to;
                                        	$to_boject->name = $customer->name;
                                        	array_push($tos_post, $to_boject);
                                        }

                                        $content = new \stdClass;
                                        $content->type = "text/html";
                                        $content->value = $html;

                                        $from_sendgrid = new \stdClass;
                                        $from_sendgrid->email = $from;
                                        $from_sendgrid->name = $fantasy_name;

                                        $reply_to = new \stdClass;
                                        $reply_to->email = $from;
                                        $reply_to->name = $fantasy_name;

                                        if (isset($email_data->copy)) {
                                            if ($email_data->copy == "") {
                                                $from_copy = $from;
                                                $fantasy_name_copy = $fantasy_name;
                                            } else {
                                                $from_copy = $email_data->copy;
                                                $fantasy_name_copy = "";
                                            }
                                        } else {
                                            $from_copy = $from;
                                            $fantasy_name_copy = $fantasy_name;
                                        }

                                        $bcc = new \stdClass;
                                        $bcc->email = $from_copy;
                                        $bcc->name = $fantasy_name_copy;

                                        $personalizations = [
                                            'to' => $tos_post,
                                            'cc' => [
                                                0 => $bcc
                                            ],
                                            'subject' => $template->subject
                                        ];

                                        $data = new \stdClass;

                                        if (sizeof($attachments) > 0) {

                                            foreach ($attachments as $attach) {

                                                $pdf_file = file_get_contents($attach['file']);
                                                $pdf_explode = explode('/', $attach['file']);
                                                $position_name = sizeof($pdf_explode) - 1;

                                                $attachments_sendgrid = new \stdClass;
                                                $attachments_sendgrid->content = base64_encode($pdf_file);
                                                $attachments_sendgrid->type = "application/pdf";
                                                $attachments_sendgrid->filename = $pdf_explode[$position_name];
                                                $attachments_sendgrid->disposition = "attachment";
                                                $attachments_sendgrid->content_id = "";
                                                $data->attachments[] = $attachments_sendgrid;
                                            }
                                        }

                                        $data->personalizations[] = $personalizations;
                                        $data->content[] = $content;
                                        $data->from = $from_sendgrid;
                                        $data->reply_to = $reply_to;
                                        $data->bcc = $bcc;

                                        $data_string = json_encode($data);

                                        try {

                                            $curl = curl_init();

                                            $endpoint_send = $paraments->emails_accounts->providers->sendgrid->endpoint->api->v3->send;
                                            $api_key = $paraments->emails_accounts->providers->sendgrid->api_key;

                                            curl_setopt_array($curl, array(
                                                CURLOPT_URL            => $endpoint_send,
                                                CURLOPT_RETURNTRANSFER => TRUE,
                                                CURLOPT_ENCODING       => "",
                                                CURLOPT_MAXREDIRS      => 10,
                                                CURLOPT_TIMEOUT        => 30,
                                                CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                                                CURLOPT_CUSTOMREQUEST  => "POST",
                                                CURLOPT_POSTFIELDS     => $data_string,
                                                CURLOPT_SSLVERSION     => CURL_SSLVERSION_TLSv1_2,
                                                CURLOPT_HTTPHEADER => array(
                                                    "authorization: Bearer " . $api_key,
                                                    "content-type: application/json"
                                                ),
                                            ));

                                            $response = curl_exec($curl);
                                            $err = curl_error($curl);
                                            curl_close($curl);

                                            if ($err) {
                                                $data_error = new \stdClass; 
                                                $data_error->request_data = $err;
                                                $data_error->customer = $customer;
                                                $data_error->section = 'Envio de Resumen de Cuenta';

                                                $event = new Event('MassEmailsController.Error', $this, [
                                                    'msg'   => __('Hubo un error al enviar los correos.'),
                                                    'data'  => $data_error,
                                                    'flash' => true
                                                ]);

                                                $this->getEventManager()->dispatch($event);
                                                $response = [
                                                    'type'    => "error",
                                                    'message' => "El Correo no se ha enviado.",
                                                    'error'   => FALSE
                                                ];
                                                Log::write('error', $err);
                                            } else {

                                                $response = [
                                                    'type'    => "success",
                                                    'message' => "El Correo se ha enviado correctamente.",
                                                    'error'   => FALSE
                                                ];

                                                $action = 'Envio de Correo exitoso';
                                                $detail = '';
                                                $detail .= 'Código: ' . $customer->code . PHP_EOL;
                                                $detail .= 'Nombre: ' . $customer->name . PHP_EOL;
                                                $detail .= 'Correo: ' . $customer->email . PHP_EOL;
                                                $this->registerActivity($action, $detail, NULL);
                                            }

                                        } catch (\Exception $e) {
                                            $data_error = new \stdClass; 
                                            $data_error->request_data = $e->getMessage();
                                            $data_error->customer = $customer;
                                            $data_error->section = 'Envio de Resumen de Cuenta';

                                            $event = new Event('MassEmailsController.Error', $this, [
                                                'msg'   => __('Hubo un error al enviar los correos.'),
                                                'data'  => $data_error,
                                                'flash' => true
                                            ]);

                                            $this->getEventManager()->dispatch($event);
                                            $response = [
                                                'type'    => "error",
                                                'message' => "El Correo no se ha enviado.",
                                                'error'   => FALSE
                                            ];
                                            Log::write('error', $e->getMessage());
                                        }
                                    }
                                }

                                if (sizeof($attachments) > 0) {
                                    unlink($printed);
                                }
                            }
                        }
                    }
                }
            } else {

                $data_error = new \stdClass; 
                $data_error->request_data = "";
                $data_error->customer = $customer;
                $data_error->section = 'Envio de Resumen de Cuenta';

                $event = new Event('MassEmailsController.Error', $this, [
                    'msg'   => __('No tiene Correo Electrónico el Cliente.'),
                    'data'  => $data_error,
                    'flash' => true
                ]);

                $this->getEventManager()->dispatch($event);

                $response = [
                    'type'    => "error",
                    'message' => "No tiene Correo Electrónico el Cliente.",
                    'error'   => TRUE
                ];
            }

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function sendEmailReceipt($receipt_id, $template_id)
    {
        $action = 'Envio de Recibo por Correo desde Cobranza';
        $detail = '';
        $this->registerActivity($action, $detail, NULL, TRUE);

        $paraments = $this->request->getSession()->read('paraments');

        $this->loadModel('Receipts');
        $receipt = $this->Receipts
            ->find()
            ->contain([
                'Customers'
            ])
            ->where([
                'id' => $receipt_id
            ])
            ->first();

        $to = $receipt->customer->email;

        if ($to) {

            $this->loadModel('MassEmailsTemplates');
            $template = $this->MassEmailsTemplates
                ->find()
                ->where([
                    'id' => $template_id
                ])
                ->first();

            $email_data = NULL;
            foreach ($paraments->mass_emails->emails as $email) {
                if ($email->id == $template->email_id) {
                    $email_data = $email;
                }
            }

            if ($email_data == NULL) {

                $data_error = new \stdClass; 
                $data_error->request_data = "";
                $data_error->customer = $receipt->customer;
                $data_error->section = 'Envio de Recibo desde Cobranza';

                $event = new Event('MassEmailsController.Error', $this, [
                    'msg'   => __('No tiene Cuenta Correo la Empresa. Envio de correo desde Cobranza'),
                    'data'  => $data_error,
                    'flash' => true
                ]);

                $this->getEventManager()->dispatch($event);
                return FALSE;
            } else {

                $enterprise = NULL;
                foreach ($paraments->invoicing->business as $b) {
                    if ($b->id == $template->business_billing) {
                        $enterprise = $b;
                    }
                }

                if ($enterprise == NULL) {
                    $data_error = new \stdClass; 
                    $data_error->request_data = "";
                    $data_error->customer = $receipt->customer;
                    $data_error->section = 'Envio de Recibo desde Cobranza';

                    $event = new Event('MassEmailsController.Error', $this, [
                        'msg'   => __('La Cuenta de Correo no tiene Empresa asignada.'),
                        'data'  => $data_error,
                        'flash' => true
                    ]);

                    $this->getEventManager()->dispatch($event);
                    return FALSE;
                } else {

                    $flag = FALSE;

                    // UTILIZA PROVEEDOR DE CORREO
                    if ($paraments->emails_accounts->provider == '') {

                        try {
                            Email::configTransport('mass_emails', [
                                'className' => $email_data->class_name,
                                'host'      => $email_data->host,
                                'port'      => $email_data->port,
                                'timeout'   => $email_data->timeout,
                                'username'  => $email_data->user,
                                'password'  => $email_data->pass,
                                'client'    => $email_data->client == "" ? NULL : $email_data->client,
                                'tls'       => $email_data->tls == "" ? NULL : $email_data->tls,
                                'url'       => env('EMAIL_TRANSPORT_DEFAULT_URL', NULL)
                            ]);
                            $flag = TRUE;
                        } catch (\Exception $e) {
                            $data_error = new \stdClass; 
                            $data_error->request_data = $e->getMessage();
                            $data_error->customer = $receipt->customer;
                            $data_error->section = 'Envio de Recibo desde Cobranza';

                            $event = new Event('MassEmailsController.Error', $this, [
                                'msg'   => __('No se ha enviado el Correo.'),
                                'data'  => $data_error,
                                'flash' => true
                            ]);

                            $this->getEventManager()->dispatch($event);
                            return FALSE;
                        }
                    } else {
                        $flag = TRUE;
                    }

                    if ($flag) {

                        $afip_codes = $this->request->getSession()->read('afip_codes');

                        $debts_total = $this->CurrentAccount->calulateCustomerDebtTotal($receipt->customer->code);

                        $temp = str_replace("%cliente_nombre%", $receipt->customer->name, $template->message);
                        $temp = str_replace("%cliente_codigo%", $receipt->customer->code, $temp);
                        $doc  = $afip_codes['doc_types'][$receipt->customer->doc_type] . ' ' . $receipt->customer->ident;
                        $temp = str_replace("%cliente_documento%", $doc, $temp);
                        $temp = str_replace("%cliente_usuario_portal%", $receipt->customer->ident, $temp);
                        $temp = str_replace("%cliente_clave_portal%", $receipt->customer->clave_portal, $temp);
                        $temp = str_replace("%cliente_saldo_total%", $receipt->customer->debt_total, $temp);
                        $temp = str_replace("%cliente_saldo_mes%", $receipt->customer->debt_month, $temp);

                        $fantasy_name = $enterprise->name;
                        if (isset($enterprise->fantasy_name)) {
                            if ($enterprise->fantasy_name != "" && $enterprise->fantasy_name != NULL) {
                                $fantasy_name = $enterprise->fantasy_name;
                            }
                        }

                        $temp = str_replace("%empresa_nombre%", $fantasy_name, $temp);
                        $temp = str_replace("%empresa_domicilio%", $enterprise->address, $temp);
                        $temp = str_replace("%empresa_web%", $enterprise->web, $temp);

                        $from = $email_data->contact;

                        $printed = $this->FiscoAfipCompPdf->receipt($receipt, TRUE);

                        $attachments = [];

                        if ($printed != "") {

                            $attach = [
                                'file'      => $printed,
                                'mimetype'  => 'application/pdf',
                                'contentId' => $receipt->id
                            ];
                            array_push($attachments, $attach);
                        } else {
                            $data_error = new \stdClass; 
                            $data_error->customer = $receipt->customer;
                            $data_error->section = 'Envio de Recibo desde Cobranza';

                            $event = new Event('MassEmailsController.Error', $this, [
                                'msg'   => __('Hubo un error no se genero dinámicamente el PDF de Recibo. Envio de correo desde Cobranza'),
                                'data'  => $data_error,
                                'flash' => TRUE
                            ]);

                            $this->getEventManager()->dispatch($event);
                            return FALSE;
                        }

                        // si usa proveedor de correo
                        if ($paraments->emails_accounts->provider == "") {

                            $email = new Email();
                            $email->transport('mass_emails');
                            $array_email = explode(',', $to);
                            $array_size = sizeof($array_email);

                            if ($array_size == 1) {

                                $email->to($to);
                            } else {

                                $email->to($array_email[0]);

                                for ($i = 1; $i < $array_size; $i++) {
                                    $email->addTo($array_email[$i]);
                                }
                            }
                            $email->from($from);
                            $email->subject($template->subject);
                            $email->emailFormat('html');
                            $email->template('mass_emails');
                            $email->setViewVars(['message' => $temp]);

                            if (sizeof($attachments) > 0) {
                                $email->attachments($attachments);
                            }

                            if ($email->send()) {
                                $action = 'Envio de Correo exitoso';
                                $detail = '';
                                $detail .= 'Código: ' . $receipt->customer->code . PHP_EOL;
                                $detail .= 'Nombre: ' . $receipt->customer->name . PHP_EOL;
                                $detail .= 'Correo: ' . $receipt->customer->email . PHP_EOL;
                                $this->registerActivity($action, $detail, NULL);
                            } else {
                                $data_error = new \stdClass; 
                                $data_error->request_data = "";
                                $data_error->customer = $receipt->customer;
                                $data_error->section = 'Envio de Recibo desde Cobranza';

                                $event = new Event('MassEmailsController.Error', $this, [
                                    'msg'   => __('No se ha enviado el Correo. Envio de correo desde Cobranza'),
                                    'data'  => $data_error,
                                    'flash' => true
                                ]);

                                $this->getEventManager()->dispatch($event);
                                return FALSE;
                            }
                        } else {

                            // UTILIZA PROVEEDOR DE CORREO
                            if ($paraments->emails_accounts->provider == 'sendgrid') {

                                $html = '<html><head></head><body>' . $temp . '</body></html>';

                                $tos = explode(",", $receipt->customer->email);
                                $tos_post = [];
                                foreach ($tos as $to) {
                                	$to_boject = new \stdClass;
                                	$to_boject->email = $to;
                                	$to_boject->name = $receipt->customer->name;
                                	array_push($tos_post, $to_boject);
                                }

                                $content = new \stdClass;
                                $content->type = "text/html";
                                $content->value = $html;

                                $from_sendgrid = new \stdClass;
                                $from_sendgrid->email = $from;
                                $from_sendgrid->name = $fantasy_name;

                                $reply_to = new \stdClass;
                                $reply_to->email = $from;
                                $reply_to->name = $fantasy_name;

                                if (isset($email_data->copy)) {
                                    if ($email_data->copy == "") {
                                        $from_copy = $from;
                                        $fantasy_name_copy = $fantasy_name;
                                    } else {
                                        $from_copy = $email_data->copy;
                                        $fantasy_name_copy = "";
                                    }
                                } else {
                                    $from_copy = $from;
                                    $fantasy_name_copy = $fantasy_name;
                                }

                                $bcc = new \stdClass;
                                $bcc->email = $from_copy;
                                $bcc->name = $fantasy_name_copy;

                                $personalizations = [
                                    'to' => $tos_post,
                                    'cc' => [
                                        0 => $bcc
                                    ],
                                    'subject' => $template->subject
                                ];

                                $data = new \stdClass;

                                if (sizeof($attachments) > 0) {

                                    foreach ($attachments as $attach) {

                                        $pdf_file = file_get_contents($attach['file']);
                                        $pdf_explode = explode('/', $attach['file']);
                                        $position_name = sizeof($pdf_explode) - 1;

                                        $attachments_sendgrid = new \stdClass;
                                        $attachments_sendgrid->content = base64_encode($pdf_file);
                                        $attachments_sendgrid->type = "application/pdf";
                                        $attachments_sendgrid->filename = $pdf_explode[$position_name];
                                        $attachments_sendgrid->disposition = "attachment";
                                        $attachments_sendgrid->content_id = "";
                                        $data->attachments[] = $attachments_sendgrid;
                                    }
                                }

                                $data->personalizations[] = $personalizations;
                                $data->content[] = $content;
                                $data->from = $from_sendgrid;
                                $data->reply_to = $reply_to;
                                $data->bcc = $bcc;

                                $data_string = json_encode($data);

                                try {

                                    $curl = curl_init();

                                    $endpoint_send = $paraments->emails_accounts->providers->sendgrid->endpoint->api->v3->send;
                                    $api_key = $paraments->emails_accounts->providers->sendgrid->api_key;

                                    curl_setopt_array($curl, array(
                                        CURLOPT_URL            => $endpoint_send,
                                        CURLOPT_RETURNTRANSFER => TRUE,
                                        CURLOPT_ENCODING       => "",
                                        CURLOPT_MAXREDIRS      => 10,
                                        CURLOPT_TIMEOUT        => 30,
                                        CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                                        CURLOPT_CUSTOMREQUEST  => "POST",
                                        CURLOPT_POSTFIELDS     => $data_string,
                                        CURLOPT_SSLVERSION     => CURL_SSLVERSION_TLSv1_2,
                                        CURLOPT_HTTPHEADER => array(
                                            "authorization: Bearer " . $api_key,
                                            "content-type: application/json"
                                        ),
                                    ));

                                    $response = curl_exec($curl);
                                    $err = curl_error($curl);
                                    curl_close($curl);

                                    if ($err) {
                                        $data_error = new \stdClass;
                                        $data_error->request_data = $err;
                                        $data_error->customer = $receipt->customer;
                                        $data_error->section = 'Envio de Recibo desde Cobranza';

                                        $event = new Event('MassEmailsController.Error', $this, [
                                            'msg'   => __('No se ha enviado el correo.  Envio de correo desde Cobranza'),
                                            'data'  => $data_error,
                                            'flash' => true
                                        ]);

                                        $this->getEventManager()->dispatch($event);
                                        return FALSE;
                                    } else {
                                        $action = 'Envio de Correo exitoso';
                                        $detail = '';
                                        $detail .= 'Código: ' . $receipt->customer->code . PHP_EOL;
                                        $detail .= 'Nombre: ' . $receipt->customer->name . PHP_EOL;
                                        $detail .= 'Correo: ' . $receipt->customer->email . PHP_EOL;
                                        $this->registerActivity($action, $detail, NULL);
                                    }

                                } catch (\Exception $e) {

                                    $data_error = new \stdClass; 
                                    $data_error->request_data = $e->getMessage();
                                    $data_error->customer = $receipt->customer;
                                    $data_error->section = 'Envio de Recibo desde Cobranza';

                                    $event = new Event('MassEmailsController.Error', $this, [
                                        'msg'   => __('No se ha enviado el correo. Envio de correo desde Cobranza'),
                                        'data'  => $data_error,
                                        'flash' => true
                                    ]);

                                    $this->getEventManager()->dispatch($event);
                                    return FALSE;
                                }
                            }
                        }

                        if (sizeof($attachments) > 0) {
                            unlink($printed);
                        }
                    }
                }
            }
        } else {
            $data_error = new \stdClass; 
            $data_error->request_data = "Error al enviar correo luego de generar un recibo";
            $data_error->customer = $receipt->customer;

            $event = new Event('MassEmailsController.Error', $this, [
                'msg'   => __('No tiene Correo Electrónico el Cliente. Envio de correo desde Cobanza'),
                'data'  => $data_error,
                'flash' => true
            ]);

            $this->getEventManager()->dispatch($event);
            return FALSE;
        }
        return TRUE;
    }

    private function createLinkPagos($customerx, $attach_logo, $enterprise, $data)
    {
        $customer = clone $customerx;
        $parament = $this->request->getSession()->read('paraments');
        $afip_codes = $this->request->getSession()->read('afip_codes');

        $this->loadComponent('MercadoPago');
        $this->loadComponent('CuentaDigital');
        $this->loadModel('Customers');

        $year = Time::now()->year;

        $customer_code = $customer->code;

        if ($customer->billing_for_service) {

            $this->loadModel('Connections');
            $connections = $this->Connections
                ->find()
                ->contain([
                    'Services'
                ])
                ->where([
                    'customer_code'       => $customer_code,
                    'Connections.deleted' => FALSE,
                ])->toArray();
            $customer->connections = $connections;
        }

        //DATOS CLIENTE
        $customer_name = $customer->name;
        $customer_address = $customer->address;
        $customer_phone = $customer->phone;
        $customer_doc_type = $afip_codes['doc_types'][$customer->doc_type];
        $customer_ident = $customer->ident;
        $customer_type = $afip_codes['responsibles'][$customer->responsible];
        $enterprise_id = $enterprise->id;
        $enterprise_name = isset($enterprise->fantasy_name) ? $enterprise->fantasy_name : $enterprise->name;

        $this->loadModel('Payments');
        $this->loadModel('Invoices');
        $this->loadModel('CreditNotes');
        $this->loadModel('DebitNotes');

        if ($parament->gral_config->billing_for_service) {

            $comprobantes = [];

            $duedate = Time::now();
            $duedate->day(1);
            $duedate->modify('+1 month');

            $invoices = $this->Invoices
                ->find()
                ->select([
                    'tipo_comp',
                    'total',
                    'comments',
                    'connection_id'
                ])
                ->where([
                    'customer_code'       => $customer_code,
                    'Invoices.duedate <=' => $duedate
                ])->toArray();

            $credit_notes = $this->CreditNotes
                ->find()
                ->select([
                    'tipo_comp',
                    'total',
                    'comments',
                    'connection_id'
                ])
                ->where([
                    'customer_code' => $customer_code
                ])->toArray();

            $debit_notes = $this->DebitNotes
                ->find()
                ->select([
                    'tipo_comp',
                    'total',
                    'comments',
                    'connection_id'
                ])
                ->where([
                    'customer_code'       => $customer_code,
                    'DebitNotes.duedate <=' => $duedate
                ])->toArray();

            $payments = $this->Payments
                ->find()
                ->select([
                    'import',
                    'concept',
                    'connection_id'
                ])
                ->where([
                    'customer_code' => $customer_code,
                    'anulated IS'   => NULL
                ])->toArray();

            $comprobantes = array_merge($invoices, $credit_notes, $debit_notes, $payments);

            $order_asc = function($a, $b)
            {
                $date_a = isset($a->date) ? $a->date : $a->created;
                $date_b = isset($b->date) ? $b->date : $b->created;

                if ($date_a < $date_b) {
                    return -1;
                } else if ($date_a > $date_b) {
                    return 1;
                } else {
                    return 0;
                }
            };

            usort($comprobantes, $order_asc);

            $newData = [];

            $saldo = 0;

            $customer->comprobantes = [];

            foreach ($comprobantes as $comprobante) {

                if (isset($comprobante->total)) {
                    if (in_array($comprobante->tipo_comp, ['NCX', '008', '003', '013'])) {
                        $total = $comprobante->total * -1;
                    } else {
                        $total = $comprobante->total;
                    }
                } else {
                    $total = $comprobante->import * -1;
                }

                $saldo += $total;

                if ($comprobante->connection_id == NULL) {

                    if (isset($comprobante->total)) {
                        if (in_array($comprobante->tipo_comp, ['NCX', '008', '003', '013'])) {
                            $total = $comprobante->total * -1;
                        } else {
                            $total = $comprobante->total;
                        }
                    } else {
                        $total = $comprobante->import * -1;
                    }

                    $customer->other_saldo += $total;
                    $comprobante->saldo = $total;
                    $customer->comprobantes[] = $comprobante;

                    if ($comprobante->saldo == 0 && !$parament->invoicing->account_summary_complete) {
                        $customer->comprobantes = [];
                    }

                } else {

                    foreach ($customer->connections as $connection) {

                        if (!isset($connection->comprobantes)) {
                            $connection->comprobantes = [];
                        }

                        if ($connection->id == $comprobante->connection_id) {

                            if (isset($comprobante->total)) {
                                if (in_array($comprobante->tipo_comp, ['NCX', '008', '003', '013'])) {
                                    $total = $comprobante->total * -1;
                                } else {
                                    $total = $comprobante->total;
                                }
                            } else {
                                $total = $comprobante->import * -1;
                            }

                            $connection->saldo += $total;
                            $comprobante->saldo = $total;
                            $connection->comprobantes[] = $comprobante;

                            if ($connection->saldo == 0 && !$parament->invoicing->account_summary_complete) {
                                $connection->comprobantes = [];
                            }

                            break;
                        }
                    }
                }
            }

            $comprobantes_show = FALSE;

            if (sizeof($customer->comprobantes) > 0) {
                $comprobantes_show = TRUE;
                $customer->comprobantes = array_reverse($customer->comprobantes);
            }

            foreach ($customer->connections as $connection) {
                if (sizeof($connection->comprobantes) > 0) {
                    $comprobantes_show = TRUE;
                    $connection->comprobantes = array_reverse($connection->comprobantes);
                } else {
                    $connection->comprobantes = [];
                }
            }

            $saldo = number_format((float)$saldo, 2, ',', '');

            $message = '
            <!DOCTYPE html>
            <html>
            	<head>
            		<title></title>
            		<!--

            			An email present from your friends at Litmus (@litmusapp)

            			Email is surprisingly hard. While this has been thoroughly tested, your mileage may vary.
            			It\'s highly recommended that you test using a service like Litmus (http://litmus.com) and your own devices.

            			Enjoy!

            		 -->
            		<meta charset="utf-8">
            		<meta name="viewport" content="width=device-width, initial-scale=1">
            		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
            		<style type="text/css">
            			/* CLIENT-SPECIFIC STYLES */
            			body, table, td, a{-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
            			table, td{mso-table-lspace: 0pt; mso-table-rspace: 0pt;} /* Remove spacing between tables in Outlook 2007 and up */
            			img{-ms-interpolation-mode: bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */

            			/* RESET STYLES */
            			img{border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;}
            			table{border-collapse: collapse !important;}
            			body{height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important;}

            			/* iOS BLUE LINKS */
            			a[x-apple-data-detectors] {
            				color: inherit !important;
            				text-decoration: none !important;
            				font-size: inherit !important;
            				font-family: inherit !important;
            				font-weight: inherit !important;
            				line-height: inherit !important;
            			}

            			/* MOBILE STYLES */
            			@media screen and (max-width: 525px) {

            				/* ALLOWS FOR FLUID TABLES */
            				.wrapper {
            				  width: 100% !important;
            					max-width: 100% !important;
            				}

            				/* ADJUSTS LAYOUT OF LOGO IMAGE */
            				.logo img {
            				  margin: 0 auto !important;
            				}

            				/* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */
            				.mobile-hide {
            				  display: none !important;
            				}

            				.img-max {
            				  max-width: 100% !important;
            				  width: 100% !important;
            				  height: auto !important;
            				}

            				/* FULL-WIDTH TABLES */
            				.responsive-table {
            				  width: 100% !important;
            				}

            				/* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */
            				.padding {
            				  padding: 10px 5% 15px 5% !important;
            				}

            				.padding-meta {
            				  padding: 30px 5% 0px 5% !important;
            				  text-align: center;
            				}

            				.padding-copy {
            					 padding: 10px 5% 10px 5% !important;
            				  text-align: center;
            				}

            				.no-padding {
            				  padding: 0 !important;
            				}

            				.section-padding {
            				  padding: 50px 15px 50px 15px !important;
            				}

            				/* ADJUST BUTTONS ON MOBILE */
            				.mobile-button-container {
            				  margin: 0 auto;
            				  width: 100% !important;
            				}

            				.mobile-button {
            				  padding: 15px !important;
            				  border: 0 !important;
            				  font-size: 16px !important;
            				  display: block !important;
            				}

            			}

            			/* ANDROID CENTER FIX */
            			div[style*="margin: 16px 0;"] { margin: 0 !important; }
            		</style>
            	</head>
            	<body style="margin: 0 !important; padding: 0 !important;">

            		<!-- HIDDEN PREHEADER TEXT -->
            		<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
            			Resumen de cuenta
            		</div>

            		<!-- HEADER -->
            		<table border="0" cellpadding="0" cellspacing="0" width="100%">';
            		    if (sizeof($attach_logo) > 0) {

            		        $message .= '
            			<tr>
            				<td bgcolor="#ffffff" align="center">
            					<!--[if (gte mso 9)|(IE)]>
            					<table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
            					<tr>
            					<td align="center" valign="top" width="500">
            					<![endif]-->
            					<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;" class="wrapper">
            						<tr>
            							<td align="center" valign="top" style="padding: 15px 0;" class="logo">
            								<a href="javascript:void(0)">
            									<img alt="Logo" src="cid:' . $enterprise_id . '" width="300" style="display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;" border="0">
            								</a>
            							</td>
            						</tr>
            					</table>
            					<!--[if (gte mso 9)|(IE)]>
            					</td>
            					</tr>
            					</table>
            					<![endif]-->
            				</td>
            			</tr>';
            		    }
            			$message .= '
            			<tr>
            				<td bgcolor="#ffffff" align="center" style="padding: 15px;">
            					<!--[if (gte mso 9)|(IE)]>
            					<table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
            					<tr>
            					<td align="center" valign="top" width="500">
            					<![endif]-->
            					<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;" class="responsive-table">
            						<tr>
            							<td>
            								<!-- COPY -->
            								<table width="100%" border="0" cellspacing="0" cellpadding="0">
            									<tr>
            										<td align="center" style="font-size: 32px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 5px;" class="padding-copy">
            											¡Tu resumen de cuenta!
            										</td>
            									</tr>
            									<tr>
            										<td bgcolor="#ffffff" align="center" style="padding: 15px;">
            											<table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
            												<tr>
            													<td>
            														<table width="100%" border="0" cellspacing="0" cellpadding="4" style="border: 1px solid #686867; ">
            															
            															<tr>
            																<td valign="top" class="mobile-wrapper">
            																	<!-- LEFT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
            																		<tr>
            																			<td style="padding: 0 0 5px 0;">
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="left" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: bold;">
            																							Nombre:
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																	<!-- RIGHT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
            																		<tr>
            																			<td>
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="right" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: normal;">
            																							' . $customer_name . '
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																</td>
            															</tr>

            															<tr>
            																<td valign="top" class="mobile-wrapper">
            																	<!-- LEFT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
            																		<tr>
            																			<td style="padding: 0 0 5px 0;">
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="left" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: bold;">
            																							Cód.:
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																	<!-- RIGHT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
            																		<tr>
            																			<td>
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="right" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: normal;">
            																							' . $customer_code . '
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																</td>
            															</tr>

            															<tr>
            																<td valign="top" class="mobile-wrapper">
            																	<!-- LEFT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
            																		<tr>
            																			<td style="padding: 0 0 5px 0;">
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="left" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: bold;">
            																							' . $customer_doc_type . ':
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																	<!-- RIGHT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
            																		<tr>
            																			<td>
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="right" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: normal;">
            																							' . $customer_ident . '
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																</td>
            															</tr>

            															<tr>
            																<td valign="top" class="mobile-wrapper">
            																	<!-- LEFT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
            																		<tr>
            																			<td style="padding: 0 0 5px 0;">
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="left" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: bold;">
            																							Domicilio: 
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																	<!-- RIGHT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
            																		<tr>
            																			<td>
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="right" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: normal;">
            																							' . $customer_address . '
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																</td>
            															</tr>

            															<tr>
            																<td valign="top" class="mobile-wrapper">
            																	<!-- LEFT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
            																		<tr>
            																			<td style="padding: 0 0 5px 0;">
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="left" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: bold;">
            																							Tel.:  
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																	<!-- RIGHT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
            																		<tr>
            																			<td>
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="right" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: normal;">
            																							' . $customer_phone . '
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																</td>
            															</tr>

            															<tr>
            																<td valign="top" class="mobile-wrapper">
            																	<!-- LEFT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
            																		<tr>
            																			<td style="padding: 0 0 5px 0;">
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="left" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: bold;">
            																							Tipo:  
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																	<!-- RIGHT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
            																		<tr>
            																			<td>
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="right" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: normal;">
            																							' . $customer_type . '
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																</td>
            															</tr>

            															<tr>
            																<td valign="top" class="mobile-wrapper">
            																	<!-- LEFT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
            																		<tr>
            																			<td style="padding: 0 0 5px 0;">
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="left" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: bold;">
            																							Saldo:
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																	<!-- RIGHT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
            																		<tr>
            																			<td>
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="right" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: normal;">
            																							$' . $saldo . '
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																</td>
            															</tr>

            														</table>
            													</td>
            												</tr>
            											</table>
            											<!--[if (gte mso 9)|(IE)]>
            											</td>
            											</tr>
            											</table>
            											<![endif]-->
            										</td>
            									</tr>';
            									if ($comprobantes_show) {
            									    $message .= '
            									<tr>
            										<td align="center" style="font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">
            										   A continuación describímos un resumen de su cuenta.
            										</td>
            									</tr>';
            									}
            							        $message .= '
            								</table>
            							</td>
            						</tr>
            					</table>
            					<!--[if (gte mso 9)|(IE)]>
            					</td>
            					</tr>
            					</table>
            					<![endif]-->
            				</td>
            			</tr>';
            			foreach ($customer->connections as $connection) {

            			    if (sizeof($connection->comprobantes) < 1) {
                                continue;
                            }

                            $plan = $connection->service->name;
                            $address = $connection->address;
                            $saldo_connection = number_format((float)$connection->saldo, 2, ',', '');
            			    $message .= '
        			    <tr>
            				<td bgcolor="#ffffff" align="center" style="padding: 15px;">
            					<!--[if (gte mso 9)|(IE)]>
            					<table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
            					<tr>
            					<td align="center" valign="top" width="500">
            					<![endif]-->
            					<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;" class="responsive-table">
            						<tr>
            							<td>
            								<!-- COPY -->
            								<table width="100%" border="0" cellspacing="0" cellpadding="0">
            									<tr>
            										<td align="center" style="font-size: 22px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 5px;" class="padding-copy">
            											Servicio
            										</td>
            									</tr>
            									<tr>
            										<td bgcolor="#ffffff" align="center" style="padding: 15px;">
            											<table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
            												<tr>
            													<td>
            														<table width="100%" border="0" cellspacing="0" cellpadding="4" style="border: 1px solid #686867; ">
            															
            															<tr>
            																<td valign="top" class="mobile-wrapper">
            																	<!-- LEFT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
            																		<tr>
            																			<td style="padding: 0 0 5px 0;">
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="left" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: bold;">
            																							Plan:
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																	<!-- RIGHT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
            																		<tr>
            																			<td>
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="right" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: normal;">
            																							' . $plan . '
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																</td>
            															</tr>

            															<tr>
            																<td valign="top" class="mobile-wrapper">
            																	<!-- LEFT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
            																		<tr>
            																			<td style="padding: 0 0 5px 0;">
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="left" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: bold;">
            																							Domicilio:
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																	<!-- RIGHT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
            																		<tr>
            																			<td>
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="right" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: normal;">
            																							' . $address . '
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																</td>
            															</tr>

            															<tr>
            																<td valign="top" class="mobile-wrapper">
            																	<!-- LEFT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
            																		<tr>
            																			<td style="padding: 0 0 5px 0;">
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="left" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: bold;">
            																							Saldo:
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																	<!-- RIGHT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
            																		<tr>
            																			<td>
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="right" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: normal;">
            																							$' . $saldo_connection . '
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																</td>
            															</tr>

            														</table>
            													</td>
            												</tr>
            											</table>
            											<!--[if (gte mso 9)|(IE)]>
            											</td>
            											</tr>
            											</table>
            											<![endif]-->
            										</td>
            									</tr>
            								</table>
            							</td>
            						</tr>
            					</table>
            					<!--[if (gte mso 9)|(IE)]>
            					</td>
            					</tr>
            					</table>
            					<![endif]-->
            				</td>
            			</tr>
            			<tr>
            				<td bgcolor="#ffffff" align="center" style="padding: 15px;" class="padding">
            					<!--[if (gte mso 9)|(IE)]>
            					<table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
            					<tr>
            					<td align="center" valign="top" width="500">
            					<![endif]-->
            					<table border="0" cellpadding="0" cellspacing="0" width="500" style="max-width: 500px;" class="responsive-table">

            						<!-------------------ACA LOS CONCEPTOS--------------------->';
            						foreach ($connection->comprobantes as $comprobante) {

                                        $comments = "";

                                        if (isset($comprobante->comments)) {
                                            $comments = $comprobante->comments;
                                        } else {
                                            $comments = $comprobante->concept;
                                        }

                                        $saldo = $comprobante->saldo;
                                        $saldo = number_format((float)$saldo, 2, ',', '');

            						    $message .= '
            						<tr>
            							<td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">
            								<!-- TWO COLUMNS -->
            								<table cellspacing="0" cellpadding="0" border="0" width="100%">

            									<tr>
            										<td valign="top" class="mobile-wrapper">
            											<!-- LEFT COLUMN -->
            											<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 80%;" align="left">
            												<tr>
            													<td style="padding: 0 0 10px 0;">
            														<table cellpadding="0" cellspacing="0" border="0" width="100%">
            															<tr>
            																<td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">
            																	 ' . $comments . '
            																</td>
            															</tr>
            														</table>
            													</td>
            												</tr>
            											</table>
            											<!-- RIGHT COLUMN -->
            											<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 20%;" align="right">
            												<tr>
            													<td style="padding: 0 0 10px 0;">
            														<table cellpadding="0" cellspacing="0" border="0" width="100%">
            															<tr>
            																<td align="right" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">
            																	$' . $saldo . '
            																</td>
            															</tr>
            														</table>
            													</td>
            												</tr>
            											</table>
            										</td>
            									</tr>

            								</table>
            							</td>
            						</tr>';
                                    }

                                    $link_btn_mp = "";

                                    if ($saldo_connection > 0 && $data->payment_link_mercadopago) {

                                        $connection_id = $connection->id;

                                        $item = new \stdClass;
                                        $item->title      = $plan;
                                        $item->unit_price = $saldo_connection;

                                        $payer = new \stdClass;
                                        $payer->name   = $customer_name;
                                        $payer->email  = $customer->email;
                                        $payer->type   = $customer_doc_type;
                                        $payer->number = $customer_ident;
                                        $payer->code   = $customer->code;

                                        $external_reference = 'servicio-' . $connection_id;

                                        $button_mp = $this->MercadoPago->createPaymentButton($item, $payer, $external_reference);

                                        if ($button_mp) {
                                            $link_btn_mp = $button_mp["response"]["init_point"];
                                        }
                                    }

                                    $link_btn_cuentadigital = "";

                                    if ($data->payment_link_cuentadigital) {

                                        $data->saldo_customer = $saldo;
                                        $link_btn_cuentadigital = $this->CuentaDigital->createLinkPago($customer_code, $data);
                                    }

                                    $message .= '
            						<!------------------------------------------------------>

            						<!-------------TOTAL------------------------------------->

            						<tr>
            							<td style="padding: 10px 0 0px 0; border-top: 1px solid #eaeaea; border-bottom: 1px dashed #aaaaaa;">
            								<!-- TWO COLUMNS -->
            								<table cellspacing="0" cellpadding="0" border="0" width="100%">
            									<tr>
            										<td valign="top" class="mobile-wrapper">
            											<!-- LEFT COLUMN -->
            											<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
            												<tr>
            													<td style="padding: 0 0 10px 0;">
            														<table cellpadding="0" cellspacing="0" border="0" width="100%">
            															<tr>
            																<td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px; font-weight: bold;">
            																	Saldo
            																</td>
            															</tr>
            														</table>
            													</td>
            												</tr>
            											</table>
            											<!-- RIGHT COLUMN -->
            											<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
            												<tr>
            													<td>
            														<table cellpadding="0" cellspacing="0" border="0" width="100%">
            															<tr>
            																<td align="right" style="font-family: Arial, sans-serif; color: #7ca230; font-size: 16px; font-weight: bold;">
            																	$' . $saldo_connection . '
            																</td>
            															</tr>
            														</table>
            													</td>
            												</tr>
            											</table>
            										</td>
            									</tr>
            								</table>
            							</td>
            						</tr>';

                                    if ($link_btn_mp) {

                                        $message .= '
            						<!-------------LINK DE PAGO------------------------------------->

            						<tr>
            							<td style="padding: 10px 0 0px 0; border-top: 1px solid #eaeaea; border-bottom: 1px dashed #aaaaaa;">
            								<!-- TWO COLUMNS -->
            								<table cellspacing="0" cellpadding="0" border="0" width="100%">
            									<tr>
            										<td valign="top" class="mobile-wrapper">
            											<!-- LEFT COLUMN -->
            											<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
            												<tr>
            													<td style="padding: 0 0 10px 0;">
            														<table cellpadding="0" cellspacing="0" border="0" width="100%">
            															<tr>
            																<td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px; font-weight: bold;">
            																	Link de pago MercadoPago
            																</td>
            															</tr>
            														</table>
            													</td>
            												</tr>
            											</table>
            											<!-- RIGHT COLUMN -->
            											<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
            												<tr>
            													<td>
            														<table cellpadding="0" cellspacing="0" border="0" width="100%">
            															<tr>
            																<td align="right" style="font-family: Arial, sans-serif; color: #7ca230; font-size: 16px; font-weight: bold;">
            																	<a href="' . $link_btn_mp . '" name="MP-Checkout" class="orange-ar-m-sq-arall">Pagar</a>
            																</td>
            															</tr>
            														</table>
            													</td>
            												</tr>
            											</table>
            										</td>
            									</tr>
            								</table>
            							</td>
            						</tr>

            						<!------------------------------------------------>';
                                    }

                                    if ($link_btn_cuentadigital) {

                                        $message .= '
            						<!-------------LINK DE PAGO------------------------------------->

            						<tr>
            							<td style="padding: 10px 0 0px 0; border-top: 1px solid #eaeaea; border-bottom: 1px dashed #aaaaaa;">
            								<!-- TWO COLUMNS -->
            								<table cellspacing="0" cellpadding="0" border="0" width="100%">
            									<tr>
            										<td valign="top" class="mobile-wrapper">
            											<!-- LEFT COLUMN -->
            											<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
            												<tr>
            													<td style="padding: 0 0 10px 0;">
            														<table cellpadding="0" cellspacing="0" border="0" width="100%">
            															<tr>
            																<td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px; font-weight: bold;">
            																	Link de pago Cuenta Digital
            																</td>
            															</tr>
            														</table>
            													</td>
            												</tr>
            											</table>
            											<!-- RIGHT COLUMN -->
            											<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
            												<tr>
            													<td>
            														<table cellpadding="0" cellspacing="0" border="0" width="100%">
            															<tr>
            																<td align="right" style="font-family: Arial, sans-serif; color: #7ca230; font-size: 16px; font-weight: bold;">
            																	<a href="' . $link_btn_cuentadigital . '" name="CUENTADIGITAL-Checkout" class="orange-ar-m-sq-arall">Pagar</a>
            																</td>
            															</tr>
            														</table>
            													</td>
            												</tr>
            											</table>
            										</td>
            									</tr>
            								</table>
            							</td>
            						</tr>

            						<!------------------------------------------------>';
                                    }

                                    $message .= '
            					</table>
            					<!--[if (gte mso 9)|(IE)]>
            					</td>
            					</tr>
            					</table>
            					<![endif]-->
            				</td>
            			</tr>';
            			}

            			if (sizeof($customer->comprobantes) > 0) {

                            $saldo_other = number_format((float)$customer->other_saldo, 2, ',', '');

                            $message .= '
                        <tr>
            				<td bgcolor="#ffffff" align="center" style="padding: 15px;">
            					<!--[if (gte mso 9)|(IE)]>
            					<table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
            					<tr>
            					<td align="center" valign="top" width="500">
            					<![endif]-->
            					<table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
            						<tr>
            							<td>
            								<!-- COPY -->
            								<table width="100%" border="0" cellspacing="0" cellpadding="0">
            									<tr>
            										<td align="center" style="font-size: 22px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 5px;" class="padding-copy">
            											Otras Ventas
            										</td>
            									</tr>
            									<tr>
            										<td bgcolor="#ffffff" align="center" style="padding: 15px;">
            											<table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
            												<tr>
            													<td>
            														<table width="100%" border="0" cellspacing="0" cellpadding="4" style="border: 1px solid #686867; ">
            															<tr>
            																<td valign="top" class="mobile-wrapper">
            																	<!-- LEFT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
            																		<tr>
            																			<td style="padding: 0 0 5px 0;">
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="left" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: bold;">
            																							Saldo:
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																	<!-- RIGHT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
            																		<tr>
            																			<td>
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="right" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: normal;">
            																							$' . $saldo_other . '
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																</td>
            															</tr>

            														</table>
            													</td>
            												</tr>
            											</table>
            											<!--[if (gte mso 9)|(IE)]>
            											</td>
            											</tr>
            											</table>
            											<![endif]-->
            										</td>
            									</tr>
            								</table>
            							</td>
            						</tr>
            					</table>
            					<!--[if (gte mso 9)|(IE)]>
            					</td>
            					</tr>
            					</table>
            					<![endif]-->
            				</td>
            			</tr>
            			<tr>
            				<td bgcolor="#ffffff" align="center" style="padding: 15px;" class="padding">
            					<!--[if (gte mso 9)|(IE)]>
            					<table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
            					<tr>
            					<td align="center" valign="top" width="500">
            					<![endif]-->
            					<table border="0" cellpadding="0" cellspacing="0" width="500" style="max-width: 500px;" class="responsive-table">

            						<!-------------------ACA LOS CONCEPTOS--------------------->';
            						foreach ($customer->comprobantes as $comprobante) {

                                        $comments = "";

                                        if (isset($comprobante->comments)) {
                                            $comments = $comprobante->comments;
                                        } else {
                                            $comments = $comprobante->concept;
                                        }

                                        $saldo = $comprobante->saldo;
                                        $saldo = number_format((float)$saldo, 2, ',', '');

            						    $message .= '
            						<tr>
            							<td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">
            								<!-- TWO COLUMNS -->
            								<table cellspacing="0" cellpadding="0" border="0" width="100%">

            									<tr>
            										<td valign="top" class="mobile-wrapper">
            											<!-- LEFT COLUMN -->
            											<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 80%;" align="left">
            												<tr>
            													<td style="padding: 0 0 10px 0;">
            														<table cellpadding="0" cellspacing="0" border="0" width="100%">
            															<tr>
            																<td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">
            																	 ' . $comments . '
            																</td>
            															</tr>
            														</table>
            													</td>
            												</tr>
            											</table>
            											<!-- RIGHT COLUMN -->
            											<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 20%;" align="right">
            												<tr>
            													<td style="padding: 0 0 10px 0;">
            														<table cellpadding="0" cellspacing="0" border="0" width="100%">
            															<tr>
            																<td align="right" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">
            																	$' . $saldo . '
            																</td>
            															</tr>
            														</table>
            													</td>
            												</tr>
            											</table>
            										</td>
            									</tr>

            								</table>
            							</td>
            						</tr>';
                                    }

                                    $link_btn_mp = "";

                                    if ($saldo_other > 0 && $data->payment_link_mercadopago) {

                                        $customer_code = $customer->code;

                                        $item = new \stdClass;
                                        $item->title      = 'Otras Ventas';
                                        $item->unit_price = $saldo_other;

                                        $payer = new \stdClass;
                                        $payer->name   = $customer_name;
                                        $payer->email  = $customer->email;
                                        $payer->type   = $customer_doc_type;
                                        $payer->number = $customer_ident;
                                        $payer->code   = $customer->code;

                                        $external_reference = "venta-" . $customer_code;

                                        $button_mp = $this->MercadoPago->createPaymentButton($item, $payer, $customer_code);

                                        if ($button_mp) {
                                            $link_btn_mp = $button_mp["response"]["init_point"];
                                        }
                                    }

                                    $message .= '
            						<!------------------------------------------------------>

            						<!-------------TOTAL------------------------------------->

            						<tr>
            							<td style="padding: 10px 0 0px 0; border-top: 1px solid #eaeaea; border-bottom: 1px dashed #aaaaaa;">
            								<!-- TWO COLUMNS -->
            								<table cellspacing="0" cellpadding="0" border="0" width="100%">
            									<tr>
            										<td valign="top" class="mobile-wrapper">
            											<!-- LEFT COLUMN -->
            											<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
            												<tr>
            													<td style="padding: 0 0 10px 0;">
            														<table cellpadding="0" cellspacing="0" border="0" width="100%">
            															<tr>
            																<td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px; font-weight: bold;">
            																	Saldo
            																</td>
            															</tr>
            														</table>
            													</td>
            												</tr>
            											</table>
            											<!-- RIGHT COLUMN -->
            											<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
            												<tr>
            													<td>
            														<table cellpadding="0" cellspacing="0" border="0" width="100%">
            															<tr>
            																<td align="right" style="font-family: Arial, sans-serif; color: #7ca230; font-size: 16px; font-weight: bold;">
            																	$' . $saldo_other . '
            																</td>
            															</tr>
            														</table>
            													</td>
            												</tr>
            											</table>
            										</td>
            									</tr>
            								</table>
            							</td>
            						</tr>';

                                    if ($link_btn_mp) {

                                        $message .= '
            						<!-------------LINK DE PAGO------------------------------------->

            						<tr>
            							<td style="padding: 10px 0 0px 0; border-top: 1px solid #eaeaea; border-bottom: 1px dashed #aaaaaa;">
            								<!-- TWO COLUMNS -->
            								<table cellspacing="0" cellpadding="0" border="0" width="100%">
            									<tr>
            										<td valign="top" class="mobile-wrapper">
            											<!-- LEFT COLUMN -->
            											<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
            												<tr>
            													<td style="padding: 0 0 10px 0;">
            														<table cellpadding="0" cellspacing="0" border="0" width="100%">
            															<tr>
            																<td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px; font-weight: bold;">
            																	Link de pago MercadoPago
            																</td>
            															</tr>
            														</table>
            													</td>
            												</tr>
            											</table>
            											<!-- RIGHT COLUMN -->
            											<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
            												<tr>
            													<td>
            														<table cellpadding="0" cellspacing="0" border="0" width="100%">
            															<tr>
            																<td align="right" style="font-family: Arial, sans-serif; color: #7ca230; font-size: 16px; font-weight: bold;">
            																	<a href="' . $link_btn_mp . '" name="MP-Checkout" class="orange-ar-m-sq-arall">Pagar</a>
            																</td>
            															</tr>
            														</table>
            													</td>
            												</tr>
            											</table>
            										</td>
            									</tr>
            								</table>
            							</td>
            						</tr>

            						<!------------------------------------------------>';
                                    }

                                    $message .= '
            					</table>
            					<!--[if (gte mso 9)|(IE)]>
            					</td>
            					</tr>
            					</table>
            					<![endif]-->
            				</td>
            			</tr>
                            ';
            			}

    			            $message .= '
            			<tr>
            				<td bgcolor="#ffffff" align="center" style="padding: 7px;">
            					<table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
            						<tr>
            							<td>
            								<table width="100%" border="0" cellspacing="0" cellpadding="0">
            									<tr>
            										<!-- COPY -->
            										<td align="center" style="font-size: 32px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding-copy">
            											¿Cómo Pago?
            										</td>
            									</tr>
            									<tr>
            										<td align="left" style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">
            											El pago podés realizarlo haciendo click sobre el link de pago, te redireccionará al sitio de pago que hayas seleccionado, para que puedas elegir la opción de pago que te resulte más práctico.
            										</td>
            									</tr>
            								 
            								</table>
            							</td>
            						</tr>
            					</table>
            					<!--[if (gte mso 9)|(IE)]>
            					</td>
            					</tr>
            					</table>
            					<![endif]-->
            				</td>
            			</tr>

            			<tr>
            				<td bgcolor="#ffffff" align="center" style="padding: 7px;">
            					<table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
            						<tr>
            							<td>
            								<table width="100%" border="0" cellspacing="0" cellpadding="0">
            									<tr>
            										<!-- COPY -->
            										<td align="left" style="font-size: 16px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding-copy">
            											Gracias por elegirnos. Por favor contáctanos con cualquier pregunta con respecto a tu resumen de cuenta.
            										</td>
            									</tr>
            									<tr>
            										<td align="left" style=" font-weight: bold; padding: 16px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">
            										   ' . $enterprise_name . ' - Administración
            										</td>
            									</tr>
            								 
            								</table>
            							</td>
            						</tr>
            					</table>
            					<!--[if (gte mso 9)|(IE)]>
            					</td>
            					</tr>
            					</table>
            					<![endif]-->
            				</td>
            			</tr>

            			<tr>
            				<td bgcolor="#ffffff" align="center" style="padding: 20px 0px;">
            					<!--[if (gte mso 9)|(IE)]>
            					<table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
            					<tr>
            					<td align="center" valign="top" width="500">
            					<![endif]-->
            					<!-- UNSUBSCRIBE COPY -->
            					<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="max-width: 500px;" class="responsive-table">
            						<tr>
            							<td align="center" style="font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;">
            								© ' . $year . ' Todos los derechos reservados
            								<br>
            							</td>
            						</tr>
            					</table>
            					<!--[if (gte mso 9)|(IE)]>
            					</td>
            					</tr>
            					</table>
            					<![endif]-->
            				</td>
            			</tr>
            		</table>

            	</body>
            </html>
            ';

        } else {

            $comprobantes = [];

            $duedate = Time::now();
            $duedate->day(1);
            $duedate->modify('+1 month');

            $invoices = $this->Invoices
                ->find()
                ->select([
                    'tipo_comp',
                    'total',
                    'comments'
                ])
                ->where([
                    'customer_code'       => $customer_code,
                    'Invoices.duedate <=' => $duedate
                ])->toArray();

            $credit_notes = $this->CreditNotes
                ->find()
                ->select([
                    'tipo_comp',
                    'total',
                    'comments'
                ])
                ->where([
                    'customer_code' => $customer_code
                ])->toArray();

            $debit_notes = $this->DebitNotes
                ->find()
                ->select([
                    'tipo_comp',
                    'total',
                    'comments'
                ])
                ->where([
                    'customer_code'         => $customer_code,
                    'DebitNotes.duedate <=' => $duedate
                ])->toArray();

            $payments = $this->Payments
                ->find()
                ->select([
                    'import',
                    'concept'
                ])
                ->where([
                    'customer_code' => $customer_code,
                    'anulated IS'   => NULL
                ])->toArray();

            $comprobantes = array_merge($invoices, $credit_notes, $debit_notes, $payments);

            $order_asc = function($a, $b)
            {
                $date_a = isset($a->date) ? $a->date : $a->created;
                $date_b = isset($b->date) ? $b->date : $b->created;

                if ($date_a < $date_b) {
                    return -1;
                } else if ($date_a > $date_b) {
                    return 1;
                } else {
                    return 0;
                }
            };

            usort($comprobantes, $order_asc);

            $newData = [];

            $saldo_customer = 0;

            foreach ($comprobantes as $comprobante) {

                if (isset($comprobante->total)) {
                    if (in_array($comprobante->tipo_comp, ['NCX', '008', '003', '013'])) {
                        $total = $comprobante->total * -1;
                    } else {
                        $total = $comprobante->total;
                    }
                } else {
                    $total = $comprobante->import * -1;
                }

                $saldo_customer += $total;

                $comprobante->saldo = $total;

                $newData[] = $comprobante;

                if ($comprobante->saldo == 0 && !$parament->invoicing->account_summary_complete) {
                    $newData = [];
                }
            }

            $comprobantes = array_reverse($newData);

            $saldo_customer = number_format((float)$saldo_customer, 2, ',', '');

            $message = '
            <!DOCTYPE html>
            <html>
            	<head>
            		<title></title>
            		<!--

            			An email present from your friends at Litmus (@litmusapp)

            			Email is surprisingly hard. While this has been thoroughly tested, your mileage may vary.
            			It\'s highly recommended that you test using a service like Litmus (http://litmus.com) and your own devices.

            			Enjoy!

            		 -->
            		<meta charset="utf-8">
            		<meta name="viewport" content="width=device-width, initial-scale=1">
            		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
            		<style type="text/css">
            			/* CLIENT-SPECIFIC STYLES */
            			body, table, td, a{-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
            			table, td{mso-table-lspace: 0pt; mso-table-rspace: 0pt;} /* Remove spacing between tables in Outlook 2007 and up */
            			img{-ms-interpolation-mode: bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */

            			/* RESET STYLES */
            			img{border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;}
            			table{border-collapse: collapse !important;}
            			body{height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important;}

            			/* iOS BLUE LINKS */
            			a[x-apple-data-detectors] {
            				color: inherit !important;
            				text-decoration: none !important;
            				font-size: inherit !important;
            				font-family: inherit !important;
            				font-weight: inherit !important;
            				line-height: inherit !important;
            			}

            			/* MOBILE STYLES */
            			@media screen and (max-width: 525px) {

            				/* ALLOWS FOR FLUID TABLES */
            				.wrapper {
            				  width: 100% !important;
            					max-width: 100% !important;
            				}

            				/* ADJUSTS LAYOUT OF LOGO IMAGE */
            				.logo img {
            				  margin: 0 auto !important;
            				}

            				/* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */
            				.mobile-hide {
            				  display: none !important;
            				}

            				.img-max {
            				  max-width: 100% !important;
            				  width: 100% !important;
            				  height: auto !important;
            				}

            				/* FULL-WIDTH TABLES */
            				.responsive-table {
            				  width: 100% !important;
            				}

            				/* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */
            				.padding {
            				  padding: 10px 5% 15px 5% !important;
            				}

            				.padding-meta {
            				  padding: 30px 5% 0px 5% !important;
            				  text-align: center;
            				}

            				.padding-copy {
            					 padding: 10px 5% 10px 5% !important;
            				  text-align: center;
            				}

            				.no-padding {
            				  padding: 0 !important;
            				}

            				.section-padding {
            				  padding: 50px 15px 50px 15px !important;
            				}

            				/* ADJUST BUTTONS ON MOBILE */
            				.mobile-button-container {
            				  margin: 0 auto;
            				  width: 100% !important;
            				}

            				.mobile-button {
            				  padding: 15px !important;
            				  border: 0 !important;
            				  font-size: 16px !important;
            				  display: block !important;
            				}

            			}

            			/* ANDROID CENTER FIX */
            			div[style*="margin: 16px 0;"] { margin: 0 !important; }
            		</style>
            	</head>
            	<body style="margin: 0 !important; padding: 0 !important;">

            		<!-- HIDDEN PREHEADER TEXT -->
            		<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
            			Resumen de cuenta
            		</div>

            		<!-- HEADER -->
            		<table border="0" cellpadding="0" cellspacing="0" width="100%">';
            		    if (sizeof($attach_logo) > 0) {

            		        $message .= '
            			<tr>
            				<td bgcolor="#ffffff" align="center">
            					<!--[if (gte mso 9)|(IE)]>
            					<table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
            					<tr>
            					<td align="center" valign="top" width="500">
            					<![endif]-->
            					<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;" class="wrapper">
            						<tr>
            							<td align="center" valign="top" style="padding: 15px 0;" class="logo">
            								<a href="javascript:void(0)">
            									<img alt="Logo" src="cid:' . $enterprise_id . '" width="300" style="display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;" border="0">
            								</a>
            							</td>
            						</tr>
            					</table>
            					<!--[if (gte mso 9)|(IE)]>
            					</td>
            					</tr>
            					</table>
            					<![endif]-->
            				</td>
            			</tr>';
            		    }
            			$message .= '
            			<tr>
            				<td bgcolor="#ffffff" align="center" style="padding: 15px;">
            					<!--[if (gte mso 9)|(IE)]>
            					<table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
            					<tr>
            					<td align="center" valign="top" width="500">
            					<![endif]-->
            					<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;" class="responsive-table">
            						<tr>
            							<td>
            								<!-- COPY -->
            								<table width="100%" border="0" cellspacing="0" cellpadding="0">
            									<tr>
            										<td align="center" style="font-size: 32px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 5px;" class="padding-copy">
            											¡Tu resumen de cuenta!
            										</td>
            									</tr>
            									<tr>
            										<td bgcolor="#ffffff" align="center" style="padding: 15px;">
            											<table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
            												<tr>
            													<td>
            														<table width="100%" border="0" cellspacing="0" cellpadding="4" style="border: 1px solid #686867; ">
            															
            															<tr>
            																<td valign="top" class="mobile-wrapper">
            																	<!-- LEFT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
            																		<tr>
            																			<td style="padding: 0 0 5px 0;">
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="left" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: bold;">
            																							Nombre:
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																	<!-- RIGHT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
            																		<tr>
            																			<td>
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="right" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: normal;">
            																							' . $customer_name . '
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																</td>
            															</tr>

            															<tr>
            																<td valign="top" class="mobile-wrapper">
            																	<!-- LEFT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
            																		<tr>
            																			<td style="padding: 0 0 5px 0;">
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="left" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: bold;">
            																							Cód.:
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																	<!-- RIGHT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
            																		<tr>
            																			<td>
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="right" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: normal;">
            																							' . $customer_code . '
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																</td>
            															</tr>

            															<tr>
            																<td valign="top" class="mobile-wrapper">
            																	<!-- LEFT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
            																		<tr>
            																			<td style="padding: 0 0 5px 0;">
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="left" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: bold;">
            																							' . $customer_doc_type . ':
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																	<!-- RIGHT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
            																		<tr>
            																			<td>
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="right" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: normal;">
            																							' . $customer_ident . '
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																</td>
            															</tr>

            															<tr>
            																<td valign="top" class="mobile-wrapper">
            																	<!-- LEFT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
            																		<tr>
            																			<td style="padding: 0 0 5px 0;">
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="left" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: bold;">
            																							Domicilio: 
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																	<!-- RIGHT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
            																		<tr>
            																			<td>
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="right" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: normal;">
            																							' . $customer_address . '
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																</td>
            															</tr>

            															<tr>
            																<td valign="top" class="mobile-wrapper">
            																	<!-- LEFT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
            																		<tr>
            																			<td style="padding: 0 0 5px 0;">
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="left" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: bold;">
            																							Tel.:  
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																	<!-- RIGHT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
            																		<tr>
            																			<td>
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="right" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: normal;">
            																							' . $customer_phone . '
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																</td>
            															</tr>

            															<tr>
            																<td valign="top" class="mobile-wrapper">
            																	<!-- LEFT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
            																		<tr>
            																			<td style="padding: 0 0 5px 0;">
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="left" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: bold;">
            																							Tipo:  
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																	<!-- RIGHT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
            																		<tr>
            																			<td>
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="right" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: normal;">
            																							' . $customer_type . '
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																</td>
            															</tr>

            															<tr>
            																<td valign="top" class="mobile-wrapper">
            																	<!-- LEFT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
            																		<tr>
            																			<td style="padding: 0 0 5px 0;">
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="left" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: bold;">
            																							Saldo:  
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																	<!-- RIGHT COLUMN -->
            																	<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
            																		<tr>
            																			<td>
            																				<table cellpadding="0" cellspacing="0" border="0" width="100%">
            																					<tr>
            																						<td align="right" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: normal;">
            																							$' . $saldo_customer . '
            																						</td>
            																					</tr>
            																				</table>
            																			</td>
            																		</tr>
            																	</table>
            																</td>
            															</tr>

            														</table>
            													</td>
            												</tr>
            											</table>
            											<!--[if (gte mso 9)|(IE)]>
            											</td>
            											</tr>
            											</table>
            											<![endif]-->
            										</td>
            									</tr>';
            									if (sizeof($comprobantes) > 0) {
            									    $message .= '
            									<tr>
            										<td align="center" style="font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">
            										   A continuación describímos un resumen de su cuenta.
            										</td>
            									</tr>';
            									}
            							        $message .= '
            								</table>
            							</td>
            						</tr>
            					</table>
            					<!--[if (gte mso 9)|(IE)]>
            					</td>
            					</tr>
            					</table>
            					<![endif]-->
            				</td>
            			</tr>';
            			if (sizeof($comprobantes) > 0) {
            			    $message .= '
            			<tr>
            				<td bgcolor="#ffffff" align="center" style="padding: 15px;" class="padding">
            					<!--[if (gte mso 9)|(IE)]>
            					<table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
            					<tr>
            					<td align="center" valign="top" width="500">
            					<![endif]-->
            					<table border="0" cellpadding="0" cellspacing="0" width="500" style="max-width: 500px;" class="responsive-table">

            						<!-------------------ACA LOS CONCEPTOS--------------------->';
            						foreach ($comprobantes as $comprobante) {

                                        $comments = "";

                                        if (isset($comprobante->comments)) {
                                            $comments = $comprobante->comments;
                                        } else {
                                            $comments = $comprobante->concept;
                                        }

                                        $saldo = $comprobante->saldo;
                                        $saldo = number_format((float)$saldo, 2, ',', '');

            						    $message .= '
            						<tr>
            							<td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">
            								<!-- TWO COLUMNS -->
            								<table cellspacing="0" cellpadding="0" border="0" width="100%">

            									<tr>
            										<td valign="top" class="mobile-wrapper">
            											<!-- LEFT COLUMN -->
            											<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 80%;" align="left">
            												<tr>
            													<td style="padding: 0 0 10px 0;">
            														<table cellpadding="0" cellspacing="0" border="0" width="100%">
            															<tr>
            																<td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">
            																	 ' . $comments . '
            																</td>
            															</tr>
            														</table>
            													</td>
            												</tr>
            											</table>
            											<!-- RIGHT COLUMN -->
            											<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 20%;" align="right">
            												<tr>
            													<td style="padding: 0 0 10px 0;">
            														<table cellpadding="0" cellspacing="0" border="0" width="100%">
            															<tr>
            																<td align="right" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">
            																	$' . $saldo . '
            																</td>
            															</tr>
            														</table>
            													</td>
            												</tr>
            											</table>
            										</td>
            									</tr>

            								</table>
            							</td>
            						</tr>';
                                    }
                                    
                                    $link_btn_mp = "";

                                    if ($saldo_customer > 0 && $data->payment_link_mercadopago) {

                                        $item = new \stdClass;
                                        $item->title      = 'Servicio Internet';
                                        $item->unit_price = $saldo_customer;

                                        $payer = new \stdClass;
                                        $payer->name   = $customer_name;
                                        $payer->email  = $customer->email;
                                        $payer->type   = $customer_doc_type;
                                        $payer->number = $customer_ident;

                                        $external_reference = $customer_code . '';

                                        $button_mp = $this->MercadoPago->createPaymentButton($item, $payer, $external_reference);

                                        if ($button_mp) {
                                            $link_btn_mp = $button_mp["response"]["init_point"];
                                        }
                                    }

                                    $link_btn_cuentadigital = "";

                                    if ($data->payment_link_cuentadigital) {

                                        $data->saldo_customer = $saldo_customer;
                                        $link_btn_cuentadigital = $this->CuentaDigital->createLinkPago($customer_code, $data);
                                    }

                                    $message .= '
            						<!------------------------------------------------------>

            						<!-------------TOTAL------------------------------------->

            						<tr>
            							<td style="padding: 10px 0 0px 0; border-top: 1px solid #eaeaea; border-bottom: 1px dashed #aaaaaa;">
            								<!-- TWO COLUMNS -->
            								<table cellspacing="0" cellpadding="0" border="0" width="100%">
            									<tr>
            										<td valign="top" class="mobile-wrapper">
            											<!-- LEFT COLUMN -->
            											<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
            												<tr>
            													<td style="padding: 0 0 10px 0;">
            														<table cellpadding="0" cellspacing="0" border="0" width="100%">
            															<tr>
            																<td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px; font-weight: bold;">
            																	Saldo
            																</td>
            															</tr>
            														</table>
            													</td>
            												</tr>
            											</table>
            											<!-- RIGHT COLUMN -->
            											<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
            												<tr>
            													<td>
            														<table cellpadding="0" cellspacing="0" border="0" width="100%">
            															<tr>
            																<td align="right" style="font-family: Arial, sans-serif; color: #7ca230; font-size: 16px; font-weight: bold;">
            																	$' . $saldo_customer . '
            																</td>
            															</tr>
            														</table>
            													</td>
            												</tr>
            											</table>
            										</td>
            									</tr>
            								</table>
            							</td>
            						</tr>';

                                    if ($link_btn_mp) {

                                        $message .= '
            						<!-------------LINK DE PAGO------------------------------------->

            						<tr>
            							<td style="padding: 10px 0 0px 0; border-top: 1px solid #eaeaea; border-bottom: 1px dashed #aaaaaa;">
            								<!-- TWO COLUMNS -->
            								<table cellspacing="0" cellpadding="0" border="0" width="100%">
            									<tr>
            										<td valign="top" class="mobile-wrapper">
            											<!-- LEFT COLUMN -->
            											<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
            												<tr>
            													<td style="padding: 0 0 10px 0;">
            														<table cellpadding="0" cellspacing="0" border="0" width="100%">
            															<tr>
            																<td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px; font-weight: bold;">
            																	Link de pago MercadoPago
            																</td>
            															</tr>
            														</table>
            													</td>
            												</tr>
            											</table>
            											<!-- RIGHT COLUMN -->
            											<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
            												<tr>
            													<td>
            														<table cellpadding="0" cellspacing="0" border="0" width="100%">
            															<tr>
            																<td align="right" style="font-family: Arial, sans-serif; color: #7ca230; font-size: 16px; font-weight: bold;">
            																	<a href="' . $link_btn_mp . '" name="MP-Checkout" class="orange-ar-m-sq-arall">Pagar</a>
            																</td>
            															</tr>
            														</table>
            													</td>
            												</tr>
            											</table>
            										</td>
            									</tr>
            								</table>
            							</td>
            						</tr>

            						<!------------------------------------------------>';
                                    }

                                    if ($link_btn_cuentadigital) {

                                        $message .= '
            						<!-------------LINK DE PAGO------------------------------------->

            						<tr>
            							<td style="padding: 10px 0 0px 0; border-top: 1px solid #eaeaea; border-bottom: 1px dashed #aaaaaa;">
            								<!-- TWO COLUMNS -->
            								<table cellspacing="0" cellpadding="0" border="0" width="100%">
            									<tr>
            										<td valign="top" class="mobile-wrapper">
            											<!-- LEFT COLUMN -->
            											<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
            												<tr>
            													<td style="padding: 0 0 10px 0;">
            														<table cellpadding="0" cellspacing="0" border="0" width="100%">
            															<tr>
            																<td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px; font-weight: bold;">
            																	Link de pago Cuenta Digital
            																</td>
            															</tr>
            														</table>
            													</td>
            												</tr>
            											</table>
            											<!-- RIGHT COLUMN -->
            											<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
            												<tr>
            													<td>
            														<table cellpadding="0" cellspacing="0" border="0" width="100%">
            															<tr>
            																<td align="right" style="font-family: Arial, sans-serif; color: #7ca230; font-size: 16px; font-weight: bold;">
            																	<a href="' . $link_btn_cuentadigital . '" name="CUENTADIGITAL-Checkout" class="orange-ar-m-sq-arall">Pagar</a>
            																</td>
            															</tr>
            														</table>
            													</td>
            												</tr>
            											</table>
            										</td>
            									</tr>
            								</table>
            							</td>
            						</tr>

            						<!------------------------------------------------>';
                                    }

                                    $message .= '
            					</table>
            					<!--[if (gte mso 9)|(IE)]>
            					</td>
            					</tr>
            					</table>
            					<![endif]-->
            				</td>
            			</tr>';
            			}
    			        $message .= '
            			<tr>
            				<td bgcolor="#ffffff" align="center" style="padding: 7px;">
            					<table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
            						<tr>
            							<td>
            								<table width="100%" border="0" cellspacing="0" cellpadding="0">
            									<tr>
            										<!-- COPY -->
            										<td align="center" style="font-size: 32px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding-copy">
            											¿Cómo Pago?
            										</td>
            									</tr>
            									<tr>
            										<td align="left" style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">
                                                    El pago podés realizarlo haciendo click sobre el link de pago, te redireccionará al sitio de pago que hayas seleccionado, para que puedas elegir la opción de pago que te resulte más práctico.
            										</td>
            									</tr>
            								 
            								</table>
            							</td>
            						</tr>
            					</table>
            					<!--[if (gte mso 9)|(IE)]>
            					</td>
            					</tr>
            					</table>
            					<![endif]-->
            				</td>
            			</tr>

            			<tr>
            				<td bgcolor="#ffffff" align="center" style="padding: 7px;">
            					<table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
            						<tr>
            							<td>
            								<table width="100%" border="0" cellspacing="0" cellpadding="0">
            									<tr>
            										<!-- COPY -->
            										<td align="left" style="font-size: 16px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding-copy">
            											Gracias por elegirnos. Por favor contáctanos con cualquier pregunta con respecto a tu resumen de cuenta.
            										</td>
            									</tr>
            									<tr>
            										<td align="left" style=" font-weight: bold; padding: 16px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">
            										   ' . $enterprise_name . ' - Administración
            										</td>
            									</tr>
            								 
            								</table>
            							</td>
            						</tr>
            					</table>
            					<!--[if (gte mso 9)|(IE)]>
            					</td>
            					</tr>
            					</table>
            					<![endif]-->
            				</td>
            			</tr>

            			<tr>
            				<td bgcolor="#ffffff" align="center" style="padding: 20px 0px;">
            					<!--[if (gte mso 9)|(IE)]>
            					<table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
            					<tr>
            					<td align="center" valign="top" width="500">
            					<![endif]-->
            					<!-- UNSUBSCRIBE COPY -->
            					<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="max-width: 500px;" class="responsive-table">
            						<tr>
            							<td align="center" style="font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;">
            								© ' . $year . ' Todos los derechos reservados
            								<br>
            							</td>
            						</tr>
            					</table>
            					<!--[if (gte mso 9)|(IE)]>
            					</td>
            					</tr>
            					</table>
            					<![endif]-->
            				</td>
            			</tr>
            		</table>

            	</body>
            </html>
            ';
        }

        return $message;
    }

    public function comprobantes()
    {
        $users = [];
        $this->loadModel('Users');
        $users_model = $this->Users->find();
        foreach ($users_model as $u) {
            $users[$u->username] = $u->username; 
        }

        $templates = [];
        $templates_model = $this->MassEmailsTemplates->find();
        foreach ($templates_model as $temp) {
            $templates[$temp->id] = $temp->name;
        }

        $this->loadModel('MassEmailsTemplates');
        $mass_emails_templates_model = $this->MassEmailsTemplates
            ->find()
            ->where([
                'enabled' => TRUE
            ]);

        $paraments = $this->request->getSession()->read('paraments');

        $business = [];
        foreach ($paraments->invoicing->business as $b) {
            $business[$b->id] = $b->name . ' (' . $b->address. ')';
        }

        $payment_getway = $this->request->getSession()->read('payment_getway');
        $payment_methods = [];
        foreach ($payment_getway->methods as $payment_method) {
            $config = $payment_method->config;
            if ($payment_getway->config->$config->enabled) {
                $payment_methods[$payment_method->name] = $payment_method->name;
            }
        }

        $this->set(compact('business', 'payment_getway', 'payment_methods', 'users', 'templates'));
    }

    public function getComprobantes()
    {
        if ($this->request->is('ajax')) {

            $response = new \stdClass();
            $response->draw = intval($this->request->getQuery('draw'));
            $response->recordsTotal = 0;
            $response->data = [];
            $response->recordsFiltered = 0;

            $response = $this->serverSideComprobantes($response, $this->request->getQuery());

            $this->set(compact('response'));
            $this->set('_serialize', ['response']);
        }
    }

    public function serverSideComprobantes($response, $params)
    {
        if ($params['columns'][5]['search']['value'] == '') {

            $response = $this->serverSideInvoicesComprobantes($response, $params);
            $response = $this->serverSideCreditNotesComprobantes($response, $params);
            $response = $this->serverSideDebitNotesComprobantes($response, $params);
            $response = $this->serverSideReceiptsComprobantes($response, $params);
            $response = $this->serverSidePresupuestosComprobantes($response, $params);
        } else {

            switch ($params['columns'][5]['search']['value']) {

                case 'XXX':
                case '001':
                case '006':
                case '011':
                    $response = $this->serverSideInvoicesComprobantes($response, $params);
                    break;

                case 'NCX':
                case '003':
                case '008':
                case '013':
                    $response = $this->serverSideCreditNotesComprobantes($response, $params);
                    break;

                case 'NDX':
                case '002':
                case '007':
                case '012':
                    $response = $this->serverSideDebitNotesComprobantes($response, $params);
                    break;

                case 'XRX':
                case 'FRX':
                case '015':
                case '004':
                case '009':
                case '015':
                    $response = $this->serverSideReceiptsComprobantes($response, $params);
                    break;

               case '000':
                    $response = $this->serverSidePresupuestosComprobantes($response, $params);
                    break;
            }
        }

        $order_asc = function($a, $b)
        {
              if ($a->date < $b->date) {
                  return -1;
              } else if ($a->date > $b->date) {
                  return 1;
              } else {
                  return 0;
              }
        };

        usort($response->data, $order_asc);

        $newData = [];

        foreach ($response->data as $data) {
            $newData[] = $data;
        }

        $response->data = array_reverse($newData); 

        return $response;
    }

    private function serverSideInvoicesComprobantes($response, $params)
    {
        $this->loadModel('Invoices');

        if (array_key_exists('where', $params)) {
            $response->recordsTotal += $this->Invoices->find()->where($params['where'])->count();
        } else {
            $response->recordsTotal += $this->Invoices->find()->where()->count();
        }

        $invoices_comprobantes = $this->Invoices->find('ServerSideDataComprobantes', [
            'params' => $params,
            'where'  => array_key_exists('where', $params) ? $params['where'] : FALSE,
        ]);

        $response->recordsFiltered += $this->Invoices->find('RecordsFilteredComprobantes', [
            'params' => $params,
            'where'  => array_key_exists('where', $params) ? $params['where'] : FALSE,
        ]);

        foreach ($invoices_comprobantes as $invoice_comprobante) {
            $response->data[] = new Comprobantes($invoice_comprobante, 'invoices'); 
        }

        return $response;
    }

    private function serverSideCreditNotesComprobantes($response, $params)
    {
        $this->loadModel('CreditNotes');

        if (array_key_exists('where', $params)) {
            $response->recordsTotal += $this->CreditNotes->find()->where($params['where'])->count();
        } else {
            $response->recordsTotal += $this->CreditNotes->find()->where()->count();
        }

        $credit_notes_comprobantes = $this->CreditNotes->find('ServerSideDataComprobantes', [
            'params' => $params,
            'where'  => array_key_exists('where', $params) ? $params['where'] : FALSE,
        ]);

        $response->recordsFiltered += $this->CreditNotes->find('RecordsFilteredComprobantes', [
            'params' => $params,
            'where'  => array_key_exists('where', $params) ? $params['where'] : FALSE,
        ]);

        foreach ($credit_notes_comprobantes as $credit_note_comprobante) {
            $response->data[] = new Comprobantes($credit_note_comprobante, 'credit_notes'); 
        }

        return $response;
    }

    private function serverSideDebitNotesComprobantes($response, $params)
    {
        $this->loadModel('DebitNotes');

        if (array_key_exists('where', $params)) {
            $response->recordsTotal += $this->DebitNotes->find()->where($params['where'])->count();
        } else {
            $response->recordsTotal += $this->DebitNotes->find()->where()->count();
        }

        $debit_notes_comprobantes = $this->DebitNotes->find('ServerSideDataComprobantes', [
            'params' => $params,
            'where'  => array_key_exists('where', $params) ? $params['where'] : FALSE,
        ]);

        $response->recordsFiltered += $this->DebitNotes->find('RecordsFilteredComprobantes', [
            'params' => $params,
            'where'  => array_key_exists('where', $params) ? $params['where'] : FALSE,
        ]);

        foreach ($debit_notes_comprobantes as $debit_note_comprobante) {
            $response->data[] = new Comprobantes($debit_note_comprobante, 'debit_notes'); 
        }

        return $response;
    }

    private function serverSideReceiptsComprobantes($response, $params)
    {
        $this->loadModel('Receipts');

        if (array_key_exists('where', $params)) {
            $response->recordsTotal += $this->Receipts->find()->where($params['where'])->count();
        } else {
            $response->recordsTotal += $this->Receipts->find()->where()->count();
        }

        $receipts_comprobantes = $this->Receipts->find('ServerSideDataComprobantes', [
            'params' => $params,
            'where'  => array_key_exists('where', $params) ? $params['where'] : FALSE,
        ]);

        $response->recordsFiltered += $this->Receipts->find('RecordsFilteredComprobantes', [
            'params' => $params,
            'where'  => array_key_exists('where', $params) ? $params['where'] : FALSE,
        ]);

        foreach ($receipts_comprobantes as $receipt_comprobante) {
            $response->data[] = new Comprobantes($receipt_comprobante, 'receipts'); 
        }

        return $response;
    }

    private function serverSidePresupuestosComprobantes($response, $params)
    {
        $this->loadModel('Presupuestos');

        if (array_key_exists('where', $params)) {
            $response->recordsTotal += $this->Presupuestos->find()->where($params['where'])->count();
        } else {
            $response->recordsTotal += $this->Presupuestos->find()->where()->count();
        }

        $presupuestos_comprobantes = $this->Presupuestos->find('ServerSideDataComprobantes', [
            'params' => $params,
            'where'  => array_key_exists('where', $params) ? $params['where'] : FALSE,
        ]);

        $response->recordsFiltered += $this->Presupuestos->find('RecordsFilteredComprobantes', [
            'params' => $params,
            'where'  => array_key_exists('where', $params) ? $params['where'] : FALSE,
        ]);

        foreach ($presupuestos_comprobantes as $presupuesto_comprobante) {
            $response->data[] = new Comprobantes($presupuesto_comprobante, 'presupuestos'); 
        }

        return $response;
    }

    public function selectComprobantesSend()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {

            $error = $this->isReloadPage();

            if (!$error) {

                $action = 'Envio Masivo de Correo seleccionando comprobantes';
                $detail = '';
                $this->registerActivity($action, $detail, NULL, TRUE);

                $paraments = $this->request->getSession()->read('paraments');

                $data = json_decode($this->request->getData('data'));

                $template_id = $data->template_id;

                $ok = 0;
                $error = 0;

                foreach ($data->ids as $id) {

                    $comprobante = explode("-", $id);
                    $model_id = $comprobante[0];
                    $model = $comprobante[1];

                    $this->loadModel($model);

                    $contain = ['Customers'];
                    $where = [
                        'id' => $model_id
                    ];

                    $object_model = $this->$model
                        ->find()
                        ->contain($contain)
                        ->where($where)
                        ->first();

                    $to = $object_model->customer->email;

                    if ($to) {

                        $this->loadModel('MassEmailsTemplates');
                        $template = $this->MassEmailsTemplates
                            ->find()
                            ->where([
                                'id' => $template_id
                            ])
                            ->first();

                        $email_data = NULL;
                        foreach ($paraments->mass_emails->emails as $email) {
                            if ($email->id == $template->email_id) {
                                $email_data = $email;
                            }
                        }

                        if ($email_data == NULL) {
                            $data_error = new \stdClass; 
                            $data_error->request_data = "No se encuentra la cuenta de correo. Cuenta ID correo: " . $template->email_id;
                            $data_error->customer = $object_model->customer;
                            $data_error->section = 'Envio de correo seleccionando comprobantes';

                            $event = new Event('MassEmailsController.Error', $this, [
                                'msg'   => __('La empresa no tiene configurada cuenta de correo. Envio de correo seleccionando comprobante'),
                                'data'  => $data_error,
                                'flash' => true
                            ]);

                            $this->getEventManager()->dispatch($event);
                            $error++;
                        } else {

                            $enterprise = NULL;
                            foreach ($paraments->invoicing->business as $b) {
                                if ($b->id == $template->business_billing) {
                                    $enterprise = $b;
                                }
                            }

                            if ($enterprise == NULL) {
                                $data_error = new \stdClass;
                                $data_error->request_data = "No encuentra la empresa. Empresa ID: " . $template->business_billing;

                                $event = new Event('MassEmailsController.Error', $this, [
                                    'msg'   => __('La Cuenta de Correo no tiene asignado una Empresa.'),
                                    'data'  => $data_error,
                                    'flash' => true
                                ]);

                                $this->getEventManager()->dispatch($event);
                                $error++;
                            } else {

                                $flag = FALSE;

                                // UTILIZA PROVEEDOR DE CORREO
                                if ($paraments->emails_accounts->provider == '') {

                                    try {
                                        Email::configTransport('mass_emails', [
                                            'className' => $email_data->class_name,
                                            'host'      => $email_data->host,
                                            'port'      => $email_data->port,
                                            'timeout'   => $email_data->timeout,
                                            'username'  => $email_data->user,
                                            'password'  => $email_data->pass,
                                            'client'    => $email_data->client == "" ? NULL : $email_data->client,
                                            'tls'       => $email_data->tls == "" ? NULL : $email_data->tls,
                                            'url'       => env('EMAIL_TRANSPORT_DEFAULT_URL', NULL)
                                        ]);
                                        $flag = TRUE;
                                    } catch (\Exception $e) {
                                        $data_error = new \stdClass; 
                                        $data_error->request_data = $e->getMessage();
                                        $data_error->customer = $object_model->customer;
                                        $data_error->section = 'Envio de correo seleccionando comprobantes';

                                        $event = new Event('MassEmailsController.Error', $this, [
                                            'msg'   => __('No se ha enviado el correo. Envio de correo seleccionando comprobante'),
                                            'data'  => $data_error,
                                            'flash' => true
                                        ]);

                                        $this->getEventManager()->dispatch($event);
                                        Log::write('error', $e->getMessage());
                                        $error++;
                                    }
                                } else {
                                    $flag = TRUE;
                                }

                                if ($flag) {

                                    $afip_codes = $this->request->getSession()->read('afip_codes');

                                    $debts_total = $this->CurrentAccount->calulateCustomerDebtTotal($object_model->customer->code);

                                    $temp = str_replace("%cliente_nombre%", $object_model->customer->name, $template->message);
                                    $temp = str_replace("%cliente_codigo%", $object_model->customer->code, $temp);
                                    $doc  = $afip_codes['doc_types'][$object_model->customer->doc_type] . ' ' . $object_model->customer->ident;
                                    $temp = str_replace("%cliente_documento%", $doc, $temp);
                                    $temp = str_replace("%cliente_usuario_portal%", $object_model->customer->ident, $temp);
                                    $temp = str_replace("%cliente_clave_portal%", $object_model->customer->clave_portal, $temp);
                                    $temp = str_replace("%cliente_saldo_total%", $debts_total, $temp);
                                    $temp = str_replace("%cliente_saldo_mes%", $object_model->customer->debt_month, $temp);

                                    $fantasy_name = $enterprise->name;
                                    if (isset($enterprise->fantasy_name)) {
                                        if ($enterprise->fantasy_name != "" && $enterprise->fantasy_name != NULL) {
                                            $fantasy_name = $enterprise->fantasy_name;
                                        }
                                    }

                                    $temp = str_replace("%empresa_nombre%", $fantasy_name, $temp);
                                    $temp = str_replace("%empresa_domicilio%", $enterprise->address, $temp);
                                    $temp = str_replace("%empresa_web%", $enterprise->web, $temp);

                                    //$from = $enterprise->email ? $enterprise->email : $email_data->contact;
                                    $from = $email_data->contact;

                                    $printed = "";
                                    $comprobante = "";

                                    switch ($model) {

                                        case 'Invoices':
                                            $comprobante = "Factura";
                                            $printed = $this->FiscoAfipCompPdf->invoice($object_model, TRUE);
                                            break;

                                        case 'DebitNotes':
                                            $comprobante = "Nota de Débito";
                                            $printed = $this->FiscoAfipCompPdf->debitNote($object_model, TRUE);
                                            break;

                                        case 'CreditNotes':
                                            $comprobante = "Nota de Crédito";
                                            $printed = $this->FiscoAfipCompPdf->creditNote($object_model, TRUE);
                                            break;

                                        case 'Receipts':
                                            $comprobante = "Nota de Recibo";
                                            $printed = $this->FiscoAfipCompPdf->receipt($object_model, TRUE);
                                            break;

                                        case 'Presupuestos':
                                            $comprobante = "Presupuesto";
                                            $printed = $this->FiscoAfipCompPdf->presupuesto($object_model, TRUE);
                                            break;
                                    }

                                    $attachments = [];

                                    if ($printed != "") {

                                        $attach = [
                                            'file'      => $printed,
                                            'mimetype'  => 'application/pdf',
                                            'contentId' => $object_model->id
                                        ];
                                        array_push($attachments, $attach);
                                    } else {
                                        $data_error = new \stdClass; 
                                        $data_error->customer = $object_model->customer;
                                        $data_error->section = 'Envio de correo seleccionando comprobantes';

                                        $event = new Event('MassEmailsController.Error', $this, [
                                            'msg'   => __('Hubo un error no se genero dinámicamente el PDF ' . $comprobante),
                                            'data'  => $data_error,
                                            'flash' => TRUE
                                        ]);

                                        $this->getEventManager()->dispatch($event);
                                        continue;
                                    }

                                    // si usa proveedor de correo
                                    if ($paraments->emails_accounts->provider == "") {

                                        $email = new Email();
                                        $email->transport('mass_emails');
                                        $array_email = explode(',', $to);
                                        $array_size = sizeof($array_email);

                                        if ($array_size == 1) {

                                            $email->to($to);
                                        } else {

                                            $email->to($array_email[0]);

                                            for ($i = 1; $i < $array_size; $i++) {
                                                $email->addTo($array_email[$i]);
                                            }
                                        }
                                        $email->from($from);
                                        $email->subject($template->subject);
                                        $email->emailFormat('html');
                                        $email->template('mass_emails');
                                        $email->setViewVars(['message' => $temp]);

                                        if (sizeof($attachments) > 0) {
                                            $email->attachments($attachments);
                                        }

                                        if ($email->send()) {

                                            $ok++;
                                            $action = 'Envio de Correo exitoso';
                                            $detail = '';
                                            $detail .= 'Código: ' . $object_model->customer->code . PHP_EOL;
                                            $detail .= 'Nombre: ' . $object_model->customer->name . PHP_EOL;
                                            $detail .= 'Correo: ' . $object_model->customer->email . PHP_EOL;
                                            $this->registerActivity($action, $detail, NULL);
                                        } else {
                                            $data_error = new \stdClass; 
                                            $data_error->request_data = "";
                                            $data_error->customer = $object_model->customer;
                                            $data_error->section = 'Envio de correo seleccionando comprobantes';

                                            $event = new Event('MassEmailsController.Error', $this, [
                                                'msg'   => __('No se ha enviado el Correo. Envio de correo seleccionando comprobante'),
                                                'data'  => $data_error,
                                                'flash' => true
                                            ]);

                                            $this->getEventManager()->dispatch($event);
                                            return FALSE;
                                            $error++;
                                        }
                                    } else {

                                        // UTILIZA PROVEEDOR DE CORREO
                                        if ($paraments->emails_accounts->provider == 'sendgrid') {

                                            $html = '<html><head></head><body>' . $temp . '</body></html>';

                                            $tos = explode(",", $object_model->customer->email);
                                            $tos_post = [];
                                            foreach ($tos as $to) {
                                                $to_boject = new \stdClass;
                                                $to_boject->email = $to;
                                                $to_boject->name = $object_model->customer->name;
                                                array_push($tos_post, $to_boject);
                                            }

                                            $content = new \stdClass;
                                            $content->type = "text/html";
                                            $content->value = $html;

                                            $from_sendgrid = new \stdClass;
                                            $from_sendgrid->email = $from;
                                            $from_sendgrid->name = $fantasy_name;

                                            $reply_to = new \stdClass;
                                            $reply_to->email = $from;
                                            $reply_to->name = $fantasy_name;

                                            if (isset($email_data->copy)) {
                                                if ($email_data->copy == "") {
                                                    $from_copy = $from;
                                                    $fantasy_name_copy = $fantasy_name;
                                                } else {
                                                    $from_copy = $email_data->copy;
                                                    $fantasy_name_copy = "";
                                                }
                                            } else {
                                                $from_copy = $from;
                                                $fantasy_name_copy = $fantasy_name;
                                            }

                                            $bcc = new \stdClass;
                                            $bcc->email = $from_copy;
                                            $bcc->name = $fantasy_name_copy;

                                            $personalizations = [
                                                'to' => $tos_post,
                                                'cc' => [
                                                    0 => $bcc
                                                ],
                                                'subject' => $template->subject
                                            ];

                                            $data = new \stdClass;

                                            if (sizeof($attachments) > 0) {

                                                foreach ($attachments as $attach) {

                                                    $pdf_file = file_get_contents($attach['file']);
                                                    $pdf_explode = explode('/', $attach['file']);
                                                    $position_name = sizeof($pdf_explode) - 1;

                                                    $attachments_sendgrid = new \stdClass;
                                                    $attachments_sendgrid->content = base64_encode($pdf_file);
                                                    $attachments_sendgrid->type = "application/pdf";
                                                    $attachments_sendgrid->filename = $pdf_explode[$position_name];
                                                    $attachments_sendgrid->disposition = "attachment";
                                                    $attachments_sendgrid->content_id = "";
                                                    $data->attachments[] = $attachments_sendgrid;
                                                }
                                            }

                                            $data->personalizations[] = $personalizations;
                                            $data->content[] = $content;
                                            $data->from = $from_sendgrid;
                                            $data->reply_to = $reply_to;
                                            $data->bcc = $bcc;

                                            $data_string = json_encode($data);

                                            try {

                                                $curl = curl_init();

                                                $endpoint_send = $paraments->emails_accounts->providers->sendgrid->endpoint->api->v3->send;
                                                $api_key = $paraments->emails_accounts->providers->sendgrid->api_key;

                                                curl_setopt_array($curl, array(
                                                    CURLOPT_URL            => $endpoint_send,
                                                    // CURLOPT_RETURNTRANSFER => TRUE,
                                                    // CURLOPT_ENCODING       => "",
                                                    // CURLOPT_MAXREDIRS      => 10,
                                                    // CURLOPT_TIMEOUT        => 30,
                                                    // CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                                                    //CURLOPT_CUSTOMREQUEST  => "POST",
                                                    CURLOPT_POSTFIELDS     => $data_string,
                                                    //CURLOPT_SSLVERSION     => CURL_SSLVERSION_TLSv1_2,
                                                    CURLOPT_HTTPHEADER => array(
                                                        "authorization: Bearer " . $api_key,
                                                        "content-type: application/json"
                                                    ),
                                                ));

                                                $response = curl_exec($curl);
                                                $err = curl_error($curl);
                                                curl_close($curl);

                                                if ($err) {

                                                    $error++;
                                                    $data_error = new \stdClass; 
                                                    $data_error->request_data = $err;
                                                    $data_error->customer = $object_model->customer;
                                                    $data_error->section = 'Envio de correo seleccionando comprobantes';

                                                    $event = new Event('MassEmailsController.Error', $this, [
                                                        'msg'   => __('No se ha podido enviar correo. Envio de correo seleccionando comprobante'),
                                                        'data'  => $data_error,
                                                        'flash' => true
                                                    ]);

                                                    $this->getEventManager()->dispatch($event);
                                                    Log::write('error', $err);
                                                } else {
                                                    $action = 'Envio de Correo exitoso';
                                                    $detail = '';
                                                    $detail .= 'Código: ' . $object_model->customer->code . PHP_EOL;
                                                    $detail .= 'Nombre: ' . $object_model->customer->name . PHP_EOL;
                                                    $detail .= 'Correo: ' . $object_model->customer->email . PHP_EOL;
                                                    $this->registerActivity($action, $detail, NULL);
                                                    $ok++;
                                                }

                                            } catch (\Exception $e) {

                                                $error++;
                                                $data_error = new \stdClass; 
                                                $data_error->request_data = $e->getMessage();
                                                $data_error->customer = $object_model->customer;
                                                $data_error->section = 'Envio de correo seleccionando comprobantes';

                                                $event = new Event('MassEmailsController.Error', $this, [
                                                    'msg'   => __('No se ha podido enviar correo. Envio de correo seleccionando comprobante'),
                                                    'data'  => $data_error,
                                                    'flash' => true
                                                ]);

                                                $this->getEventManager()->dispatch($event);
                                                Log::write('error', $e->getMessage());
                                            }
                                        }
                                    }

                                    if (sizeof($attachments) > 0) {
                                        unlink($printed);
                                    }
                                }
                            }
                        }
                    } else {
                        $data_error = new \stdClass; 
                        $data_error->request_data = "El cliente no tiene correo";
                        $data_error->customer = $object_model->customer;
                        $data_error->section = 'Envio de correo seleccionando comprobantes';

                        $event = new Event('MassEmailsController.Error', $this, [
                            'msg'   => __('El cliente no tiene correo. Envio de correo seleccionando comprobante'),
                            'data'  => $data_error,
                            'flash' => true
                        ]);

                        $this->getEventManager()->dispatch($event);
                        $error++;
                    }
                }
                $message = "Correos enviados: " . $ok;
                if ($error > 0) {
                    $message .= "\n" . "Correos no enviados: " . $error; 
                }
                $this->Flash->success(__($message));
            } else {

                $this->Flash->warning(__('La misma acción se intento realizar varias veces.'));
                return $this->redirect(['action' => 'selectComprobantesSend']);
            }
        }

        return $this->redirect(['action' => 'comprobantes']);
    }

    public function sendEmailPostFacturacion($ids)
    {
        $action = 'Envio de Correo Post Facturación';
        $detail = '';
        $this->registerActivity($action, $detail, NULL, TRUE);

        $ids = explode('.', $ids);

        if (sizeof($ids) > 0) {

            $paraments = $this->request->getSession()->read('paraments');

            $this->loadModel('Invoices');
            $invoices = $this->Invoices
                ->find()
                ->contain([
                    'Customers'
                ])
                ->where([
                    'id IN' => $ids
                ]);

            $template_id = $paraments->invoicing->email_emplate_invoice;

            foreach ($invoices as $invoice) {

                if (empty($invoice->customer->email)) {
                    continue;
                }

                $to = $invoice->customer->email;

                $this->loadModel('MassEmailsTemplates');
                $template = $this->MassEmailsTemplates
                    ->find()
                    ->where([
                        'id' => $template_id
                    ])
                    ->first();

                $email_data = NULL;
                foreach ($paraments->mass_emails->emails as $email) {
                    if ($email->id == $template->email_id) {
                        $email_data = $email;
                    }
                }

                if ($email_data == NULL) {
                    continue;
                }

                $enterprise = NULL;
                foreach ($paraments->invoicing->business as $b) {
                    if ($b->id == $template->business_billing) {
                        $enterprise = $b;
                    }
                }

                if ($enterprise == NULL) {
                    continue;
                }

                $flag = FALSE;

                // UTILIZA PROVEEDOR DE CORREO
                if ($paraments->emails_accounts->provider == '') {

                    try {
                        Email::configTransport('mass_emails', [
                            'className' => $email_data->class_name,
                            'host'      => $email_data->host,
                            'port'      => $email_data->port,
                            'timeout'   => $email_data->timeout,
                            'username'  => $email_data->user,
                            'password'  => $email_data->pass,
                            'client'    => $email_data->client == "" ? NULL : $email_data->client,
                            'tls'       => $email_data->tls == "" ? NULL : $email_data->tls,
                            'url'       => env('EMAIL_TRANSPORT_DEFAULT_URL', NULL)
                        ]);
                        $flag = TRUE;
                    } catch (\Exception $e) {
                        $data_error = new \stdClass; 
                        $data_error->request_data = $e->getMessage();
                        $data_error->customer = $invoice->customer;
                        $data_error->section = 'Envio de correo post facturación';

                        $event = new Event('MassEmailsController.Error', $this, [
                            'msg'   => __('No se ha podido enviar el correo. Post facturación.'),
                            'data'  => $data_error,
                            'flash' => true
                        ]);

                        $this->getEventManager()->dispatch($event);
                        Log::write('error', $e->getMessage());
                    }
                } else {
                    $flag = TRUE;
                }

                if ($flag) {

                    $afip_codes = $this->request->getSession()->read('afip_codes');

                    $debts_total = $this->CurrentAccount->calulateCustomerDebtTotal($invoice->customer->code);

                    $temp = str_replace("%cliente_nombre%", $invoice->customer->name, $template->message);
                    $temp = str_replace("%cliente_codigo%", $invoice->customer->code, $temp);
                    $doc  = $afip_codes['doc_types'][$invoice->customer->doc_type] . ' ' . $invoice->customer->ident;
                    $temp = str_replace("%cliente_documento%", $doc, $temp);
                    $temp = str_replace("%cliente_usuario_portal%", $invoice->customer->ident, $temp);
                    $temp = str_replace("%cliente_clave_portal%", $invoice->customer->clave_portal, $temp);
                    $temp = str_replace("%cliente_saldo_total%", $debts_total, $temp);
                    $temp = str_replace("%cliente_saldo_mes%", $invoice->customer->debt_month, $temp);

                    $fantasy_name = $enterprise->name;
                    if (isset($enterprise->fantasy_name)) {
                        if ($enterprise->fantasy_name != "" && $enterprise->fantasy_name != NULL) {
                            $fantasy_name = $enterprise->fantasy_name;
                        }
                    }

                    $temp = str_replace("%empresa_nombre%", $fantasy_name, $temp);
                    $temp = str_replace("%empresa_domicilio%", $enterprise->address, $temp);
                    $temp = str_replace("%empresa_web%", $enterprise->web, $temp);

                    //$from = $enterprise->email ? $enterprise->email : $email_data->contact;
                    $from = $email_data->contact;

                    $printed = $this->FiscoAfipCompPdf->invoice($invoice, TRUE);

                    $attachments = [];

                    if ($printed != "") {

                        $attach = [
                            'file'      => $printed,
                            'mimetype'  => 'application/pdf',
                            'contentId' => $invoice->id
                        ];
                        array_push($attachments, $attach);
                    } else {
                        $data_error = new \stdClass; 
                        $data_error->customer = $invoice->customer;
                        $data_error->section = 'Envio de correo post facturación';

                        $event = new Event('MassEmailsController.Error', $this, [
                            'msg'   => __('Hubo un error no se genero dinámicamente el PDF de Factura. Post facturación.'),
                            'data'  => $data_error,
                            'flash' => TRUE
                        ]);

                        $this->getEventManager()->dispatch($event);
                        continue;
                    }

                    // si usa proveedor de correo
                    if ($paraments->emails_accounts->provider == "") {

                        $email = new Email();
                        $email->transport('mass_emails');
                        $array_size = sizeof($array_email);

                        if ($array_size == 1) {

                            $email->to([$invoice->customer->email => $invoice->customer->name ]);
                        } else {

                            $email->to([$array_email[0] => $invoice->customer->name ]);

                            for ($i = 1; $i < $array_size; $i++) {
                                $email->addTo([$array_email[$i] => $invoice->customer->name ]);
                            }
                        }
                        $email->from($from);
                        $email->subject($template->subject);
                        $email->emailFormat('html');
                        $email->template('mass_emails');
                        $email->setViewVars(['message' => $temp]);

                        if (sizeof($attachments) > 0) {
                            $email->attachments($attachments);
                        }

                        if (!$email->send()) {
                            $data_error = new \stdClass; 
                            $data_error->request_data = "";
                            $data_error->customer = $invoice->customer;
                            $data_error->section = 'Envio de correo post facturación';

                            $event = new Event('MassEmailsController.Error', $this, [
                                'msg'   => __('No se ha enviado el Correo. Post facturación.'),
                                'data'  => $data_error,
                                'flash' => true
                            ]);

                            $this->getEventManager()->dispatch($event);
                            return FALSE;
                        }
                    } else {

                        // UTILIZA PROVEEDOR DE CORREO
                        if ($paraments->emails_accounts->provider == 'sendgrid') {

                            $html = '<html><head></head><body>' . $temp . '</body></html>';

                            $tos = explode(",", $invoice->customer->email);
                            $tos_post = [];
                            foreach ($tos as $to) {
                            	$to_boject = new \stdClass;
                            	$to_boject->email = $to;
                            	$to_boject->name = $invoice->customer->name;
                            	array_push($tos_post, $to_boject);
                            }

                            $content = new \stdClass;
                            $content->type = "text/html";
                            $content->value = $html;

                            $from_sendgrid = new \stdClass;
                            $from_sendgrid->email = $from;
                            $from_sendgrid->name = $fantasy_name;

                            $reply_to = new \stdClass;
                            $reply_to->email = $from;
                            $reply_to->name = $fantasy_name;

                            if (isset($email_data->copy)) {
                                if ($email_data->copy == "") {
                                    $from_copy = $from;
                                    $fantasy_name_copy = $fantasy_name;
                                } else {
                                    $from_copy = $email_data->copy;
                                    $fantasy_name_copy = "";
                                }
                            } else {
                                $from_copy = $from;
                                $fantasy_name_copy = $fantasy_name;
                            }

                            $bcc = new \stdClass;
                            $bcc->email = $from_copy;
                            $bcc->name = $fantasy_name_copy;

                            $personalizations = [
                                'to' => $tos_post,
                                'cc' => [
                                    0 => $bcc
                                ],
                                'subject' => $template->subject
                            ];

                            $data = new \stdClass;

                            if (sizeof($attachments) > 0) {

                                foreach ($attachments as $attach) {

                                    $pdf_file = file_get_contents($attach['file']);
                                    $pdf_explode = explode('/', $attach['file']);
                                    $position_name = sizeof($pdf_explode) - 1;

                                    $attachments_sendgrid = new \stdClass;
                                    $attachments_sendgrid->content = base64_encode($pdf_file);
                                    $attachments_sendgrid->type = "application/pdf";
                                    $attachments_sendgrid->filename = $pdf_explode[$position_name];
                                    $attachments_sendgrid->disposition = "attachment";
                                    $attachments_sendgrid->content_id = "";
                                    $data->attachments[] = $attachments_sendgrid;
                                }
                            }

                            $data->personalizations[] = $personalizations;
                            $data->content[] = $content;
                            $data->from = $from_sendgrid;
                            $data->reply_to = $reply_to;
                            $data->bcc = $bcc;

                            $data_string = json_encode($data);

                            try {

                                $curl = curl_init();

                                $endpoint_send = $paraments->emails_accounts->providers->sendgrid->endpoint->api->v3->send;
                                $api_key = $paraments->emails_accounts->providers->sendgrid->api_key;

                                curl_setopt_array($curl, array(
                                    CURLOPT_URL            => $endpoint_send,
                                    CURLOPT_RETURNTRANSFER => TRUE,
                                    CURLOPT_ENCODING       => "",
                                    CURLOPT_MAXREDIRS      => 10,
                                    CURLOPT_TIMEOUT        => 30,
                                    CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                                    CURLOPT_CUSTOMREQUEST  => "POST",
                                    CURLOPT_POSTFIELDS     => $data_string,
                                    CURLOPT_SSLVERSION     => CURL_SSLVERSION_TLSv1_2,
                                    CURLOPT_HTTPHEADER => array(
                                        "authorization: Bearer " . $api_key,
                                        "content-type: application/json"
                                    ),
                                ));

                                $response = curl_exec($curl);
                                $err = curl_error($curl);
                                curl_close($curl);

                                if ($err) {
                                    $data_error = new \stdClass; 
                                    $data_error->request_data = $err;
                                    $data_error->customer = $invoice->customer;
                                    $data_error->section = 'Envio de correo post facturación';

                                    $event = new Event('MassEmailsController.Error', $this, [
                                        'msg'   => __('No se ha enviado el Correo. Post facturación.'),
                                        'data'  => $data_error,
                                        'flash' => true
                                    ]);

                                    $this->getEventManager()->dispatch($event);
                                    Log::write('error', $err);
                                } else {
                                    $action = 'Envio de Correo exitoso';
                                    $detail = '';
                                    $detail .= 'Código: ' . $invoice->customer->code . PHP_EOL;
                                    $detail .= 'Nombre: ' . $invoice->customer->name . PHP_EOL;
                                    $detail .= 'Correo: ' . $invoice->customer->email . PHP_EOL;
                                    $this->registerActivity($action, $detail, NULL);
                                }

                            } catch (\Exception $e) {
                                $data_error = new \stdClass; 
                                $data_error->request_data = $e->getMessage();
                                $data_error->customer = $invoice->customer;
                                $data_error->section = 'Envio de correo post facturación';

                                $event = new Event('MassEmailsController.Error', $this, [
                                    'msg'   => __('No se ha enviado el Correo. Post facturación.'),
                                    'data'  => $data_error,
                                    'flash' => true
                                ]);

                                $this->getEventManager()->dispatch($event);
                                Log::write('error', $e->getMessage());
                            }
                        }
                    }

                    if (sizeof($attachments) > 0) {
                        unlink($printed);
                    }
                }
            }
        }
    }

    public function sendConfimationEmail($email_data)
    {      
        $email = $email_data->email;
        $address = $email_data->address;
        $razon_social = $email_data->razon_social;
        $responsible = $email_data->responsible;
        $province = $email_data->province;
        $localidad = $email_data->localidad;
        //$doc_type = $email_data->doc_type;
        $ident = $email_data->ident;

        $paraments = $this->request->getSession()->read('paraments');

        /**armar y obtener el html para el mail */

         $builder = $this->viewBuilder();
         $builder->setLayout(false);
         $builder->setTemplate('/Email/html/confirmation');
         $builder->setHelpers(['Html']);
         $view = $builder->build(compact('razon_social', 'province', 'localidad', 'address', 'responsible', 'ident', 'email'));
         $html = $view->render();

        /*** */

        $to = 'admin@ispbrain.io';
        $name = 'Administración';

        $tos = explode(",", $to);
        $tos_post = [];
        foreach ($tos as $to) {
            $to_boject = new \stdClass;
            $to_boject->email = $to;
            $to_boject->name = $name;
            array_push($tos_post, $to_boject);
        }

        $content = new \stdClass;
        $content->type = "text/html";
        $content->value = $html;

        $from_sendgrid = new \stdClass;
        $from_sendgrid->email = 'contact@.io';
        $from_sendgrid->name = 'ISPBrain';

        $reply_to = new \stdClass;
        $reply_to->email = 'contact@ispbrain.io';
        $reply_to->name = 'ISPBrain';

        $subject = $email_data->confirm ? 'ISPBrain: Edición de datos de facturación' : 'ISPBrain: confirmación de Instancia';

        $personalizations = [
            'to' => $tos_post,
            'subject' => $subject
        ];

        $data = new \stdClass;

        $attachments = [];

        $attach_logo = [
            'file'        => WWW_ROOT . 'img' . DIRECTORY_SEPARATOR . 'logo.png',
            'mimetype'    => 'image/png',
            'contentId'   => '1234',
            'disposition' => 'inline'
        ];

        array_push($attachments, $attach_logo);
        if (sizeof($attachments) > 0) {

            foreach ($attachments as $attach) {

                $pdf_file = file_get_contents($attach['file']);
                $pdf_explode = explode('/', $attach['file']);
                $position_name = sizeof($pdf_explode) - 1;

                $attachments_sendgrid = new \stdClass;
                $attachments_sendgrid->content = base64_encode($pdf_file);
                $attachments_sendgrid->type = $attach['mimetype'];
                $attachments_sendgrid->filename = $pdf_explode[$position_name];
                $attachments_sendgrid->disposition = $attach['disposition'];
                $attachments_sendgrid->content_id = $attach['contentId'];
                $data->attachments[] = $attachments_sendgrid;
            }
        }

        $data->personalizations[] = $personalizations;
        $data->content[] = $content;
        $data->from = $from_sendgrid;
        $data->reply_to = $reply_to;

        $data_string = json_encode($data);

        try {

            $curl = curl_init();

            $endpoint_send = $paraments->emails_accounts->providers->sendgrid->endpoint->api->v3->send;
            $api_key = $paraments->emails_accounts->providers->sendgrid->api_key;

            curl_setopt_array($curl, array(
                CURLOPT_URL            => $endpoint_send,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_ENCODING       => "",
                CURLOPT_MAXREDIRS      => 10,
                CURLOPT_TIMEOUT        => 30,
                CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST  => "POST",
                CURLOPT_POSTFIELDS     => $data_string,
                CURLOPT_SSLVERSION     => CURL_SSLVERSION_TLSv1_2,
                CURLOPT_HTTPHEADER => array(
                    "authorization: Bearer " . $api_key,
                    "content-type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);

            if ($err) {

                //$this->log($err, 'debug');
                return false;
            } else {

                $type = "success";
                $message = "El Correo se ha enviado correctamente.";
                return true;
            }

        } catch (\Exception $e) {
            //$this->log($e->getMessage(), 'debug');
            return false;
        }
    }
}

class Comprobantes {

    public $id;
    public $date;
    public $date_start;
    public $date_end;
    public $tipo_comp;
    public $pto_vta;
    public $num;
    public $comment;
    public $duedate;
    public $total;
    public $user_id;
    public $customer_code;
    public $customer_doc_type;
    public $customer_ident;
    public $customer_name;
    public $customer_email;
    public $customer_address;
    public $customer_city;
    public $payment_method; //only receipt
    public $business_id;

    public $type_comprobante;
    public $anulated;

    public function __construct($data, $type) {

        switch ($type) {

            case 'invoices':

                $this->id                = $data->id;
                $this->date              = $data->date;
                $this->date_start        = $data->date_start;
                $this->date_end          = $data->date_end;
                $this->tipo_comp         = $data->tipo_comp;
                $this->pto_vta           = $data->pto_vta;
                $this->num               = $data->num;
                $this->comment           = $data->comments;
                $this->duedate           = $data->duedate;
                $this->total             = $data->total;
                $this->user_name         = $data->user->username;
                $this->customer_code     = $data->customer_code;
                $this->customer_doc_type = $data->customer_doc_type;
                $this->customer_ident    = $data->customer_ident;
                $this->customer_name     = $data->customer_name;
                $this->customer_email    = $data->customer->email;
                $this->customer_address  = $data->customer_address;
                $this->customer_city     = $data->customer_city;
                $this->payment_method    = "";
                $this->business_id       = $data->business_id;
                $this->anulated          = $data->anulated;

                $this->type_comprobante = $type;
                $this->controller = 'Invoices';
                break;

            case 'credit_notes':

                $this->id                = $data->id;
                $this->date              = $data->date;
                $this->date_start        = $data->date_start;
                $this->date_end          = $data->date_end;
                $this->tipo_comp         = $data->tipo_comp;
                $this->pto_vta           = $data->pto_vta;
                $this->num               = $data->num;
                $this->comment           = $data->comments;
                $this->duedate           = $data->duedate;
                $this->total             = $data->total;
                $this->user_name         = $data->user->username;
                $this->customer_code     = $data->customer_code;
                $this->customer_doc_type = $data->customer_doc_type;
                $this->customer_ident    = $data->customer_ident;
                $this->customer_name     = $data->customer_name;
                $this->customer_email    = $data->customer->email;
                $this->customer_address  = $data->customer_address;
                $this->customer_city     = $data->customer_city;
                $this->payment_method    = "";
                $this->business_id       = $data->business_id;
                $this->anulated          = "";

                $this->type_comprobante = $type;
                $this->controller = 'CreditNotes';
                break;

            case 'debit_notes':

                $this->id                = $data->id;
                $this->date              = $data->date;
                $this->date_start        = $data->date_start;
                $this->date_end          = $data->date_end;
                $this->tipo_comp         = $data->tipo_comp;
                $this->pto_vta           = $data->pto_vta;
                $this->num               = $data->num;
                $this->comment           = $data->comments;
                $this->duedate           = $data->duedate;
                $this->total             = $data->total;
                $this->user_name         = $data->user->username;
                $this->customer_code     = $data->customer_code;
                $this->customer_doc_type = $data->customer_doc_type;
                $this->customer_ident    = $data->customer_ident;
                $this->customer_name     = $data->customer_name;
                $this->customer_email    = $data->customer->email;
                $this->customer_address  = $data->customer_address;
                $this->customer_city     = $data->customer_city;
                $this->payment_method    = "";
                $this->business_id       = $data->business_id;
                $this->anulated          = "";

                $this->type_comprobante = $type;
                $this->controller = 'DebitNotes';
                break;

            case 'receipts':

                $this->id                = $data->id;
                $this->date              = $data->date;
                $this->date_start        = $data->date_start;
                $this->date_end          = $data->date_end;
                $this->tipo_comp         = $data->tipo_comp;
                $this->pto_vta           = $data->pto_vta;
                $this->num               = $data->num;
                $this->comment           = $data->concept;
                $this->duedate           = "";
                $this->total             = $data->total;
                $this->user_name         = $data->user->username;
                $this->customer_code     = $data->customer_code;
                $this->customer_doc_type = $data->customer_doc_type;
                $this->customer_ident    = $data->customer_ident;
                $this->customer_name     = $data->customer_name;
                $this->customer_email    = $data->customer->email;
                $this->customer_address  = $data->customer_address;
                $this->customer_city     = $data->customer_city;
                $this->payment_method    = $data->payment_method;
                $this->business_id       = $data->business_id;
                $this->anulated          = "";

                $this->type_comprobante = $type;
                $this->controller = 'Receipts';
                break;

            case 'presupuestos':

                $this->id                = $data->id;
                $this->date              = $data->date;
                $this->date_start        = $data->date_start;
                $this->date_end          = $data->date_end;
                $this->tipo_comp         = $data->tipo_comp;
                $this->pto_vta           = $data->pto_vta;
                $this->num               = $data->num;
                $this->comment           = $data->comments;
                $this->duedate           = $data->duedate;
                $this->total             = $data->total;
                $this->user_name         = $data->user->username;
                $this->customer_code     = $data->customer_code;
                $this->customer_doc_type = $data->customer_doc_type;
                $this->customer_ident    = $data->customer_ident;
                $this->customer_name     = $data->customer_name;
                $this->customer_email    = $data->customer->email;
                $this->customer_address  = $data->customer_address;
                $this->customer_city     = $data->customer_city;
                $this->payment_method    = "";
                $this->business_id       = $data->business_id;
                $this->anulated          = "";

                $this->type_comprobante = $type;
                $this->controller = 'Presupuesto';
                break;
        }
    }
}
