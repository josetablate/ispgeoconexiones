<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use Cake\Core\Configure;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Accountant', [
             'className' => '\App\Controller\Component\Admin\Accountant'
        ]);
    }

    public function isAuthorized($user = null) 
    {
        if ($this->request->getParam('action') == 'login') {
            return true;
        }

        if ($this->request->getParam('action') == 'logout') {
            return true;
        }

        if ($this->request->getParam('action') == 'getUsers') {
            return true;
        }

        return parent::allowRol($user['id']);
    }

    public function index()
    {}

    public function view($id = null) 
    {
        $user = $this->Users->get($id, [
            'contain' => ['Roles']
        ]);

        $i = 0;
        foreach ($user->roles as $role) {
            if ($i == 0) {
                $i++;
                $user->roles_names_list = $role->name;
            } else {
                $user->roles_names_list .= ', ' . $role->name;
            }
        }

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    public function add() 
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {

            $parament =  $this->request->getSession()->read('paraments');

            if ($this->Accountant->isEnabled()) {

                if (array_key_exists('cash',$this->request->getData())) {

                    if (!$parament->accountant->acounts_parent->cashs) {
                        $this->Flash->warning(__('Debe espeficar la cuenta padre de Cajas.'));
                        return $this->redirect(['action' => 'add']);
                    }
                }
            }

            $user = $this->Users->patchEntity($user, $this->request->getData());

            if ($this->Users->save($user)) {

                if (array_key_exists('store',$this->request->getData())) {

                    $this->loadModel('Stores');

                    $store = $this->Stores->newEntity();
                    $store->name = 'Depósito ' . $user->username; 
                    $store->address = 'No especificado';
                    $store->user_id = $user->id;

                    if ($this->Stores->save($store)) {
                        $this->Flash->success(__('Depósito agregado.'));
                    } else {
                        $this->Flash->error(__('No se pudo agregar el Depósito.'));
                    }
                }

                if (array_key_exists('cash', $this->request->getData())) {

                    $this->loadModel('CashEntities');

                    $cash = $this->CashEntities->newEntity();

                    if ($this->Accountant->isEnabled()) {

                        $account = $this->Accountant->createAccount([
                            'account_parent' => $parament->accountant->acounts_parent->cashs,
                            'name' =>  'Caja ' . $user->username,
                            'redirect' => ['action' => 'add']
                        ]);

                        $cash->account_code = $account->code;
                    }

                    $cash->name = 'Caja ' . $user->username; 
                    $cash->user_id = $user->id;

                    if ($this->CashEntities->save($cash)) {
                        $this->Flash->success(__('Caja agregada.'));
                    } else {
                        $this->Flash->error(__('No se pudo crear la Caja.'));
                    }
                }

                $this->Flash->success(__('El usuario ha sido agregado.'));
                return $this->redirect(['action' => 'index']);

            } else {
                $this->Flash->error(__('No se puedo agregar. Por favor intente de nuevo.'));
            }
        }

        $parament = $this->request->getSession()->read('paraments');

        $roles = $this->Users->Roles
            ->find('list')
            ->where([
                'enabled'   => TRUE, 
                'deleted'   => FALSE, 
                'id NOT IN' => $parament->system->users
            ])
            ->order([
                'name' => 'ASC'
            ]); 

        $this->set(compact('user', 'roles'));
        $this->set('_serialize', ['user']);
    }

    public function edit($id = null)
    {
        $parament = $this->request->getSession()->read('paraments');

        $user = $this->Users->get($id, [
            'contain' => ['Roles']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {

            if ( $this->request->getData('roles._ids') == '' || !in_array(99, $this->request->getData('roles._ids'))){

                if (!in_array($user->id, $parament->system->users)) {
                    $user = $this->Users->patchEntity($user, $this->request->getData());
                } else {

                    $this->Flash->warning(__('Solo puede editarse la clave de este usuario.'));
                    $user->password = $this->request->getData('password');
                }

                if ($this->request->getData('password') != '') {

                    $action = 'Cambio de Clave de Usuario';
                    $this->registerActivity($action);
                }

                if ($this->Users->save($user)) {

                    $this->Flash->success(__('Cambios Guardados.'));
                    return $this->redirect(['action' => 'index']);

                } else {
                    $this->Flash->error(__('Los Cambios no se guardaron. Por favor intente de nuevo.'));
                }
            } else{
                $this->Flash->warning(__('Los privilegios de Cobro Digital no pueden asignarse a otro usuario.'));
            }
        }

        $roles = $this->Users->Roles->find()
            ->where(['id !=' => 100])
            ->order(['name' => 'ASC']); 

        $this->set(compact('user', 'roles'));
        $this->set('_serialize', ['user']);
    }

    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($_POST['id']);
        $username = $user->username;
        $user->deleted = true;

        $parament = $this->request->getSession()->read('paraments');

        if (in_array($user->id, $parament->system->users)) {
            $this->Flash->warning(__('Este usuario no puede eliminarse.'));
            return $this->redirect(['action' => 'index']);
        }

        if ($this->Users->save($user)) {

            $this->Flash->success(__('El usuario ha sido borrado.'));
        } else {
            $this->Flash->error(__('El usuario no se elimino. Por favor intente de nuevo.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function login() 
    {
        if ($this->request->getSession()->check('Auth.User')) {
            return $this->redirect(['controller' => 'Home','action' => 'dashboard']);
        }

        if ($this->request->is('post')) {            

            $user = $this->Auth->identify(); //esto valida el usuario en la base

            if ($user) {

                if ($user['enabled'] && $user['deleted'] == false) {

                    $user_r = $this->Users->get($user['id'], [
                        'contain' => [
                                'Roles.ActionsSystem.Clases'
                            ]
                        ]);

                    $pass = false;
                    $pass_billing = false;

                    foreach ($user_r->roles as $role) {
                        foreach ($role->actions_system as $action) {
                            if (strtoupper($action->clase->clase) == strtoupper('Settings')) {

                                if (strtoupper('chat') == strtoupper($action->action)) {
                                    if ($role->enabled && !$role->deleted) {
                                        $pass = true;
                                    }
                                }

                                if (strtoupper('billing') == strtoupper($action->action)) {
                                    if ($role->enabled && !$role->deleted) {
                                        $pass_billing = true;
                                    }
                                }
                            }
                        }
                    }

                    $user['chat'] = $pass;
                    $user['billing'] = $pass_billing;
                    $user['show_chat'] = !$this->request->is('mobile');

                    $this->Auth->setUser($user);

                    $this->request->getSession()->write('type', 'sistema');

                    $this->Flash->default(__('Hola! ' . $this->Auth->user()['name']));

                    $action = 'Ingreso de Usuario';
                    $this->registerActivity($action);

                    $cookie = [];

                    if ($this->request->getData('remember_me')) {
                        $cookie['username'] = $this->request->getData('username');
                        $cookie['password'] = $this->request->getData('password');
                        $cookie['remember_me'] = $this->request->getData('remember_me');
                    }

                    $this->Cookie->write('remember_me', $cookie, true, "1 week");

                    return $this->redirect(['controller' => 'Home','action' => 'dashboard']);
                }

            } else {
                $this->Flash->warning(__('El usuario o la clave son incorrectas.'));
            }            
        }

        $cookie = $this->Cookie->read('remember_me');

        $this->set(compact('cookie'));
        $this->set('_serialize', ['cookie']);
    }

    public function logout()
    {
        $this->request->getSession()->write('demo_suffix', null);
     
        $this->Auth->logout();        

        $this->request->getSession()->destroy();

        $this->Cookie->delete('remember_me');
        return $this->redirect(['controller' => 'Users','action' => 'login']);
    }

    public function getUsers()
    {
        $users = $this->Users->find()->contain(['Roles'])
            ->where(['deleted' => false, 'id !=' => 100]);

        foreach ($users as $user) {

            $i = 0;

            foreach ($user->roles as $role) {
                if ($i == 0) {
                    $i++;
                    $user->roles_names_list = $role->name;
                } else {
                    $user->roles_names_list .= ', ' . $role->name;
                }
            }
        }

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }
}
