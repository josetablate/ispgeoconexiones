<style type="text/css">

    legend {
        display: block;
        width: 100%;
        padding: 0;
        margin-bottom: 20px;
        font-size: 21px;
        line-height: inherit;
        color: #333;
        border: 0;
        border-bottom: 1px solid #e5e5e5;
    }

    .title-modal-full-name {
        font-weight: bold;
    }

    #map {
		height: 380px;
	}

	#floating-panel {
		top: 10px;
		left: 25%;
		z-index: 5;
		background-color: #fff;
		padding: 5px;
		border: 1px solid #999;
		text-align: center;
		font-family: 'Roboto','sans-serif';
		line-height: 30px;
		padding-left: 10px;
	}

    .input-group-addon {
        padding: 1px 12px;
    }

    #submit-map {
        padding: 10px;
        top: 2px;
    }

    #update-map {
        padding: 10px;
        top: 2px;
    }

    #btn-find-ip {
        padding: 10px;
    }

    .bootstrap-select.btn-group .dropdown-menu.inner {
        max-height: 350px !important;
    }

</style>

<div class="col-12">
    <legend class="sub-title-sm">
        Cliente: 
        <span class="text-primary">
            <?= __(' {0} - {1} {2} - {3}', sprintf("%'.05d", $connection->customer->code), $doc_types[$connection->customer->doc_type], ($connection->customer->ident ? $connection->customer->ident : ''),  $connection->customer->name ) ?>
        </span>
        | Servicio actual:
        <span class="text-primary">
            <?= __(' {0}',  $connection->service->name ) ?>
        </span>
    </legend>
</div>

<div class="col-xl-12">

    <?= $this->Form->create($connection,  ['class' => 'form-edit-connection']) ?>
    <fieldset>

        <div class="row mb-3">

            <div class="col-xl-7 pb-1">

                <?php
                     echo $this->Form->input('lat', ['type' => 'hidden', 'id' => 'coodmapslat']);
                     echo $this->Form->input('lng', ['type' => 'hidden', 'id' => 'coodmapslng']);
                     echo $this->Form->input('customer_code', ['type' => 'hidden', 'value' => $connection->customer_code]);
                ?>

                <div class="row">

                    <div class="col-6">

                        <div class="row">

                            <!--seleccion de controlador-->
                            <div class="col-12">
                                <?php
                                    echo $this->Form->input('controller_id', [
                                        'label' => 'Controlador', 
                                        'class' => 'selectpicker', 
                                        'data-live-search' => "true",
                                	    'required' => true,
                                    ]);
                                ?>
                            </div>    

                            <!--seleccion de servicio-->
                            <div class="col-12">
                                <?php
                                    echo $this->Form->input('plan_id', [
                                        'label' => 'Servicios', 
                                        'class' => 'selectpicker', 
                                        'data-live-search' => "true",
                                	    'required' => true,
                                    ]);
                                ?>
                            </div>

                            <!--selccion de pool-->
                            <div class="col-12" id="pool-select-container">
                                <?php
                                    echo $this->Form->input('pool_id', [
                                        'label' => 'Redes',
                                        'class' => 'selectpicker',
                                        'data-live-search' => "true",
                                	    'required' => true,
                                    ]);
                                ?>
                            </div>

                            <!--selccion de interface (ip fija)-->
                            <div class="col-12 pr-0" id="interface-select-container" >
                                <?php
                                    echo $this->Form->input('interface', [
                                        'label' => 'Interface', 
                                	    'required' => false,
                                    ]);
                                ?>
                            </div>

                        </div>

                    </div>
                    <div class="col-6">

                        <div class="row">

                            <!--ip de la conexion-->
                            <div class="col-12">

                                <div class="row">
                                    <div class="col-xl-12">

                                        <label for="">IP</label>
                                        <div class="form-group input-group">

                                            <label  class="input-group-addon input-group-text">
                                                <i id="label-ip-spin" class="fas fa-cog fa-spin" style="display: none;"></i>
                                                <i id="label-ip-text" class="fas fa-cog "></i>
                                            </label>

                                            <!--ip de conexion-->
                                            <input type="text" class="form-control" name="ip" id="ip" autocomplete="off" required>

                                            <!--buscador de ip libre-->
                                            <span class="input-group-btn">
                                                <button id="btn-find-ip" title="Buscar IP" class="btn btn-success" type="button"><span class="text-white glyphicon icon-search" aria-hidden="true"></span></button>
                                            </span>

                                        </div>

                                    </div>

                                    <div class="col-sm-12 col-md-5 col-lg-5 col-xl-5 border border-secondary  rounded mb-3 pt-2 mt-2 ml-3">
                                        <?php echo $this->Form->input('validate_ip', ['label' => ['text' => 'Validar IP', 'class' => 'mt-1'], 'checked' => true, 'class' => 'mt-0', 'escape' => false]); ?>
                                    </div>

                                </div>

                            </div> 

                            <!--mac del equipo cliente-->
                            <div class="col-xl-12">
                               	<?php
                                    echo $this->Form->input('mac', [
                                        'type' => 'text',
                                        'label' => 'Dirección MAC',
                                        'autocomplete' => 'off',
                                        'title' => 'Puede ingresar la MAC en distintos formatos, ejemplos: 02:AE:FC:02:F3:E4, 02-AE-FC-02-F3-E4, 02AEFC02F3E4',
                                        'maxlength' => 17 ,
                                        'minlength' => 12
                                    ]);
                                ?>
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <?php
                                    echo $this->Form->input('address', ['label' => 'Domicilio', 'required' => true, 'autocomplete' => 'off' ]);
                                ?>
                            </div>

                        </div>

                    </div>

                </div>

                <!--solo para conexion de tipo pppoe-->
                <div class="row mb-2" id="credential-pppoe">

                    <div class="col-12">

                         <div class="card border-secondary">
                            <div class="card-header p-2">PPPoE</div>
                            <div class="card-body text-secondary pt-2 pl-2  pr-2 pb-0">

                                <div class="row">

                                    <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">

                                        <?php

                                            echo $this->Form->input('name', [
                                                'id' => 'pppoe_name',
                                                'placeholder' => 'Usuario',
                                                'label' => false,
                                                'required' => false,
                                                'readonly' => false,
                                                'type' => 'text',
                                                'autocomplete' => 'off',
                                                'class' => 'ppp-credenctial'
                                            ]);
                                        ?>

                                    </div>
                                    <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">

                                        <!--clave pppoe-->
                                        <?php
                                            echo $this->Form->input('password', [
                                                'id' => 'pppoe_pass',
                                                'placeholder' => 'Clave',
                                                'label' => false,
                                                'required' => false,
                                                'readonly' => false,
                                                'type' => 'text',
                                                'autocomplete' => 'off',
                                                'class' => 'ppp-credenctial'
                                            ]);
                                        ?>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>  

                </div>

                <!--otros datos para documentacion-->
                <div class="row">

                    <div class="col-12">

                        <div class="accordion" id="accordionExample">
                            <div class="card">
                                <div class="card-header p-3" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-secondary" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Otro datos
                                        </button>
                                    </h5>
                                </div>
    
                                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body p-3">

                                        <div class="row">

                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                               	<?php
                                                    echo $this->Form->input('area_id', ['label' => 'Área', 'options' => $zoneArray, 'required' => true ]);
                                                ?>
                                            </div>

                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                               	<?php
                                                    echo $this->Form->input('clave_wifi', [ 'label' => 'Clave WiFi',  'type' => 'txt', 'required' => false, 'autocomplete' => 'off' ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                               	<?php
                                                    echo $this->Form->input('ports', [ 'label' => 'Puertos', 'type' => 'txt', 'required' => false, 'autocomplete' => 'off' ]);
                                                ?>
                                            </div>
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                               	<?php
                                                    echo $this->Form->input('ip_alt', [ 'label' => 'IP Alternativo', 'type' => 'txt', 'required' => false, 'autocomplete' => 'off' ]);
                                                ?>
                                            </div>

                                            <div class="col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                               	<?php
                                                    echo $this->Form->input('ing_traffic', [ 'label' => 'Ing. Tráfico', 'type' => 'txt', 'required' => false, 'autocomplete' => 'off' ]);
                                                ?>
                                            </div>

                                            <!--name de la queue-->
                                            <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4" id="queue-name-container">
                                               	<?php
                                                    echo $this->Form->input('queue_name', [
                                                        'label' => 'Nombre (queue)',
                                                        'required' => false,
                                                        'readonly' => false,
                                                        'type' => 'text',
                                                        'autocomplete' => 'off'
                                                    ]);
                                                ?>
                                            </div>

                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                               	<?php
                                                    echo $this->Form->input('comments', ['label' => 'Comentario', 'type' => 'textarea', 'rows' => 1, 'autocomplete' => 'off']); 
                                                ?>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

            <!--el mapa-->
            <div class="col-xl-5 pt-3 pb-1">

                <!--buscado de direcciones-->
        		<div id="floating-panel">
        		    <div class="row">
        		        <div class="col-md-12">
            		        <div class="input-group">
            		            <span class="input-group-btn">
                                    <button id="update-map" title="Recargar Mapa" class="btn btn-info" type="button"><span class="text-white glyphicon icon-map2" aria-hidden="true"></span></button>
                                </span>
                                <input id="addressmap" type="text" class="form-control margin-top-5" value="" placeholder="Calle #, Ciudad, Provincia"  autocomplete='off'>
                                <span class="input-group-btn">
                                    <button id="submit-map" title="Buscar Dirección" class="btn btn-success" type="button"><span class="text-white glyphicon icon-search" aria-hidden="true"></span></button>
                                </span>
                            </div>
        		        </div>
        		    </div>
        		</div>

        		<div id="map" class=""></div>

            </div>

        </div>

    </fieldset>

    <div class="row">

        <div class="col-xl-7">

            <div class="row justify-content-between">

                <div class="col-xl-4">

                    <?=
                        $this->Html->link(
                            'Cancelar',
                            ['action' => 'index'],
                            [
                            'title' => 'Volver a la tabla de clientes',
                            'class' => 'btn btn-default',
                            'escape' => false,
                        ]);
                    ?>

                </div>
                <div class="col-xl-4 text-right">

                     <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success mb-5']) ?>

                </div>

            </div>

        </div>
    </div>

   <?= $this->Form->end() ?>
</div>

<?php
    echo $this->element('modal_preloader');
?>

<script 
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2K3zoK46JybWPzQbhfQQqIIbdZ_3WcQs">
</script>

<script type="text/javascript">

    var controllers;
    var connection;
    var paraments;

    $(document).ready(function() {

        $('#validate-ip').change(function() {

            if ($('#validate-ip').is(':checked')) {

                $('#ip').prop('readonly', true);
                $('input#ip').val('');
                $('#btn-find-ip').click();
                $('#interface-select-container').hide();
                $("#pool-id").prop('required', true);

            } else {

                $('#ip').prop('readonly', false);
                $('input#ip').val('');
                $('#interface-select-container').show();
                $("#pool-id").prop('required', false);
            }
        });

        controllers = <?= json_encode($controllers) ?>;
 
        connection = <?= json_encode($connection) ?>;

        paraments = <?= json_encode($paraments) ?>;

        if (sessionPHP.paraments.connection.pool_plan) {
            $('#pool-select-container').hide();
        }

        $('#controller-id').html('');

        $('#controller-id').selectpicker({
            noneSelectedText: 'Seleccione'
        });

        $('#controller-id').append($('<option>', { 
            value: '',
            text : 'Seleccione'
        }));

        $.each(controllers, function (i, controller) {
            $('#controller-id').append($('<option>', { 
                value: controller.id,
                text : controller.name 
            }));
        });

        $('#controller-id').selectpicker('refresh');

        $('#plan-id').selectpicker({
            noneSelectedText: 'Seleccione'
        });

        $('#plan-id').change(function() { //cuando selecciono el plan

            $.each(controllers, function (i, controller) {

                if (controller.id == $('#controller-id').val()) {

                    $.each(controller.tplans, function (i, plan) {

                        if ($('#plan-id').val() == plan.id) { // selecicone un plan

                            $('#pool-id').val(plan.pool_id);
                            $('#pool-id').change();
                        } 
                    });
                }
            });
        });

        $('#plan-id').selectpicker('refresh');

        $('#pool-id').selectpicker({
            noneSelectedText: 'Seleccionar red'
        });

        $('#pool-id').append($('<option>', { 
            value: '',
            text : 'Seleccionar red' 
        }));

        $('#pool-id').change(function() { //cuando seleccionó la red

            if ($('#validate-ip').is(':checked')) {

                $('input#ip').val('');
                $('#btn-find-ip').click();
            }

            $.each(controllers, function (i, controller) {

                if (controller.id == $('#controller-id').val()) {

                    if (controller.tcontroller.arp) {

                        $('#interface').val('');

                        $.each(controller.tpools, function (i, pool) {

                            if ($('#pool-id').val() == pool.id) { // selecicone un pool

                                $.each(controller.tgateways, function (i, gateway) {

                                    if (pool.id == gateway.pool_id) {

                                        $('#interface').val(gateway.interface);

                                    }
                                });
                            }
                        });

                    }
                }
            });

        });

        $('#controller-id').change(function() { //cuando selecciono el controlador

            $('input#ip').val('');
            $('.ppppoe-credencials').hide();
            $('#name').closest('div').find('label').html('Nombre');            

            $.each(controllers, function (i, controller) {

                if (controller.id == $('#controller-id').val()) {

                    switch (controller.template) {

                        case 'MikrotikIpfijaTa':

                            $('#credential-pppoe').hide();
                            $(".ppp-credenctial").prop('required', false);
                              
                            $('#queue-name-container').show()
                            $("#queue-name").prop('required', true);
 
                            //si esta hablitado la creacion de ARP
                            if (controller.tcontroller.arp) {

                                $("#mac").prop('required', true);

                                //cargo interfaces del conmtrolador

                                $('#interface').val('');
                                $("#interface").prop('required', true);

                            } else {
                                 $("#mac").prop('required', false);
                                 $("#interface").prop('required', false);
                            }
                            break;

                        case 'MikrotikPppoeTa':                        

                            $('#credential-pppoe').show();
                            $(".ppp-credenctial").prop('required', true); 

                            $('#name').closest('div').find('label').html('Usuario PPPoE');
                            break;

                        case 'MikrotikDhcpTa': 

                            $('#credential-pppoe').hide();
                            $(".ppp-credenctial").prop('required', false); 

                            $('#queue-name-container').show()
                            $("#queue-name").prop('required', true);
   
                            $("#mac").prop('required', true);
                    }

                    $('#plan-id').html('');

                    $('#plan-id').append($('<option>', { 
                        value: '',
                        text : 'Seleccione' 
                    }));

                    $('#plan-id').selectpicker('refresh');

                    $.each(controller.tplans, function (i, plan) {

                        $('#plan-id').append($('<option>', { 
                            value: plan.id,
                            text : plan.service.name
                        }));

                    });

                    $('#plan-id').selectpicker('refresh');

                    $('#pool-id').html('');

                    $('#pool-id').append($('<option>', { 
                        value: '',
                        text : 'Seleccionar red' 
                    }));

                    $('#pool-id').selectpicker('refresh');

                    $.each(controller.tpools, function (i, pool) {

                        $('#pool-id').append($('<option>', { 
                            value: pool.id,
                            text : pool.name 
                        }));

                        $('#pool-id').selectpicker('refresh');
                    });

                }
            });
        });

        $('#controller-id').val(connection.controller.id);
        $('#controller-id').selectpicker('val', connection.controller.id);
        $('#controller-id').selectpicker('refresh');
        $('#controller-id').change();

        switch (connection.controller.template) {

            case 'MikrotikIpfijaTa':

                $('#credential-pppoe').hide();
 
                $('#queue-name-container').show();
                $('#queue-name').val(connection.queue.name);
                $("#queue-name").prop('required', true);

                $('#plan-id').val(connection.queue.plan_id);
                $('#plan-id').selectpicker('val', connection.queue.plan_id);
                $('#plan-id').selectpicker('refresh');

                $('#pool-id').val(connection.queue.pool_id);
                $('#pool-id').selectpicker('val', connection.queue.pool_id);
                $('#pool-id').selectpicker('refresh');

                if (connection.arp) {

                    $("#mac").prop('required', true);

                    $('#interface').val(connection.arp.interface);

                } else {

                    $("#mac").prop('required', false);

                    $('#interface').val('');

                }

                if (connection.queue.pool_id == null) {
                    $('#validate-ip').attr('checked', false);
                    $("#pool-id").prop('required', false);
                    $('#ip').attr('readonly', false);
                    $('#interface-select-container').show();
                } else {
                    $('#validate-ip').attr('checked', true);
                    $("#pool-id").prop('required', true);
                    $('#ip').attr('readonly', true);
                    $('#interface-select-container').hide();
                }
                break;

            case 'MikrotikPppoeTa': 

                $('#plan-id').val(connection.queue.plan_id);
                $('#plan-id').selectpicker('val', connection.queue.plan_id);
                $('#plan-id').selectpicker('refresh');

                $('#pool-id').val(connection.queue.plan_id);
                $('#pool-id').selectpicker('val', connection.queue.pool_id);
                $('#pool-id').selectpicker('refresh');

                $('#pppoe_name').val(connection.secret.name);
                $('#pppoe_pass').val(connection.secret.password);
                  
                $('#validate-ip').closest('label').show();

                $("#mac").prop('required', false);
                $("#mac").val(connection.mac);
 
                if (connection.queue.pool_id == null) {
                    $('#validate-ip').attr('checked', false);
                    $("#pool-id").prop('required', false);
                    $('#ip').attr('readonly', false);
                } else {
                    $('#validate-ip').attr('checked', true);
                    $("#pool-id").prop('required', true);
                    $('#ip').attr('readonly', true);
                }

                $('#queue-name-container').hide()
                $('#queue-name').val(connection.queue.name);
                $("#queue-name").prop('required', false);
                break;

             case 'MikrotikDhcpTa':

                $('#credential-pppoe').hide();

                $('#queue-name-container').show();
                $('#queue-name').val(connection.queue.name);
                $("#queue-name").prop('required', true);

                $('#plan-id').val(connection.queue.plan_id);
                $('#plan-id').selectpicker('val', connection.queue.plan_id);
                $('#plan-id').selectpicker('refresh');

                $('#pool-id').val(connection.queue.network_id);
                $('#pool-id').selectpicker('val', connection.queue.network_id);
                $('#pool-id').selectpicker('refresh');

                $("#mac").prop('required', true);

                if (connection.queue.network_id == null) {
                    $('#validate-ip').attr('checked', false);
                    $("#pool-id").prop('required', false);
                    $('#ip').attr('readonly', false);
                } else {
                    $('#validate-ip').attr('checked', true);
                    $("#pool-id").prop('required', true);
                    $('#ip').attr('readonly', true);
                }
                break;
        }

        $('#ip').val(connection.ip);

        $('#btn-find-ip').click(function() {

            var controller_id = $('select#controller-id').val();
            var pool_id = $('select#pool-id').val();

            if (controller_id != '' && pool_id != '') {

                $('#label-ip-text').hide();
                $('#label-ip-spin').show();

                $.ajax({
                    url: "<?= $this->Url->build(['controller' => 'connections', 'action' => 'findIPAjax']) ?>",
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify({ controller_id: controller_id, pool_id: pool_id}),
                    success: function(data) {

                        if (data.data.error) {
                             generateNoty('error', data.data.error);
                        } else {
                            $('input#ip').val(data.data.ip);
                            $('#pool-id').val(data.data.pool_id);    
                            $('#pool-id').selectpicker('refresh');
                        }

                        $('#label-ip-text').show();
                        $('#label-ip-spin').hide();
                    },
                    error: function (jqXHR) {
                        if (jqXHR.status == 403) {
                        	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                        	setTimeout(function() { 
                                window.location.href = "/ispbrain";
                        	}, 3000);
    
                        } else {
                        	generateNoty('error', 'Error al intentar buscar IP');
                        }
                    }
                });

            }
        });

        $('#label-ip-text').show();
        $('#label-ip-spin').hide();

        initMap();
    });

    $('.form-edit-connection').submit(function() {
        openModalPreloader("Editando Conexión ...");
    });

    $('#update-map').click(function() {
        initMap();
    });

    $('form input').on('keypress', function(e) {
        return e.which !== 13;
    });

    var map = null;
    var geocoder = null;
    var marker = null;

    function isEmpty(val) {
        return (val === undefined || val == null || val == 0 || val.length <= 0) ? true : false;
    }

	function initMap() {

		geocoder = new google.maps.Geocoder;

		var myLatlng = {lat: paraments.system.map.lat, lng: paraments.system.map.lng};

		var mapOptions = {
			zoom: 15,
			center: myLatlng,
			mapTypeId: 'hybrid'   // roadmap, satellite, hybrid, terrain 
		};

		map = new google.maps.Map(document.getElementById('map'), mapOptions);

		document.getElementById('submit-map').addEventListener('click', function() {
			geocodeAddress(geocoder, map);
		});

	    var init = myLatlng;

        var haveMarker = false;
        if (!isEmpty(connection.lat) || !isEmpty(connection.lng)) {
            haveMarker = true;
            init.lat = connection.lat;
            init.lng = connection.lng;
        }

        var latLng = new google.maps.LatLng(init.lat, init.lng);
        if (haveMarker) {
           addMarker(latLng);
        }

        map.setCenter(latLng);

		map.addListener('click', function(event) {
			addMarker(event.latLng);	    
		});
    }

    function refreshMap() {
        google.maps.event.trigger(map, 'resize');
    }

    $('input#addressmap').keypress(function(event) {
        if ( event.which == 13 ) {
            geocodeAddress(geocoder, map);
        }
    });

	function addMarker(location) {

		if (marker != null) {
			clearMarkers();
		}

		marker = new google.maps.Marker({
			position: location,
			map: map
		});
		marker.setMap(map);	

        var lat = location.lat();

        var lng = location.lng();

		$('#coodmapslat').val(lat);
		$('#coodmapslng').val(lng);	
	}

	function clearMarkers() {
		marker.setMap(null);
	}			

	function geocodeAddress(geocoder, map) {

		var address = document.getElementById('addressmap').value;

		geocoder.geocode({
			'address': address
			}, function(results, status) {

				if (status === google.maps.GeocoderStatus.OK) {	
					addMarker(results[0].geometry.location)  	    
					map.setCenter(results[0].geometry.location);
					map.setZoom(14);

    			} else {
    				if (status == google.maps.GeocoderStatus.ZERO_RESULTS) {
    			        bootbox.alert('No se encontraron Resultados');
    			    } else {
    			        window.alert('No se encontraron Resultados. Estado: ' + status);
    			    }
    			}
		});
	}

	// Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
