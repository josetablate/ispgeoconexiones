<style type="text/css">

    tr.locked > td.actions {
        background-color: white;
    }

    .font-size-13px {
        font-size: 13px;
    }

    .info-contain {
        background: #f1f4f7;
        border-bottom: 1px solid #FF5722;
        border-top: 1px solid #FF5722;
        padding-top: 12px;
        margin-bottom: 10px;
    }

</style>

<div class="row">
    <div class="col-xl-12">

        <div class="card border-secondary mb-2">

            <div class="card-body p-2 d-auto justify-content-around">

                <?php

                    echo $this->Html->link(
                        '<span class="glyphicon icon-checkbox-checked text-white" aria-hidden="true"></span> Seleccionar todos',
                        'javascript:void(0)',
                        [
                        'id' => 'btn-select-all',
                        'title' => 'Seleccionar todos',
                        'class' => 'btn btn-info ml-2',
                        'escape' => false
                    ]);

                    echo $this->Html->link(
                        '<span class="glyphicon icon-checkbox-unchecked text-white" aria-hidden="true"></span> Quitar selección',
                        'javascript:void(0)',
                        [
                        'id' => 'btn-unselect-all',
                        'title' => 'Quitar selección',
                        'class' => 'btn btn-info ml-2',
                        'escape' => false
                    ]);

                    echo $this->Html->link(
                        '<span class="far fa-play-circle" aria-hidden="true"></span> Acciones',
                        'javascript:void(0)',
                        [
                        'id' => 'btn-actions',
                        'title' => 'Acciones',
                        'class' => 'btn btn-dark ml-2',
                        'escape' => false
                    ]);

                ?>

            </div>
        </div>
    </div>
</div>

<div id="btns-tools">

    <div class="pull-right btns-tools margin-bottom-5">

        <?php

           echo $this->Html->link(
                '<span class="fas fa-sync-alt" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Actualizar la tabla',
                'class' => 'btn btn-default btn-update-table ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-eye" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Ocultar o Mostrar Columnas',
                'class' => 'btn btn-default btn-hide-column  ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon fas fa-search" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Buscar por Columnas',
                'class' => 'btn btn-default btn-search-column ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export ml-1',
                'escape' => false
            ]);

        ?>

    </div>
</div>

<?php
    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-lock',
        'name' =>  'Bloquear',
        'icon' =>  'fas fa-lock',
        'type' =>  'btn-danger'
    ];

    $buttons[] = [
        'id'   =>  'btn-unlock',
        'name' =>  'Habilitar',
        'icon' =>  'fas fa-unlock',
        'type' =>  'btn-success'
    ];

    $buttons[] = [
        'id'   =>  'btn-apply-debts',
        'name' =>  'Aplicar Recargos',
        'icon' =>  'far fa-plus-square',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-apply-discounts',
        'name' =>  'Aplicar Descuentos',
        'icon' =>  'far fa-minus-square',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-actions', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<div class="row">
    <div class="col-xl-12">

        <table id="table-connections-administrative" class="table table-bordered">
            <thead>
                <tr>
                    <th>Creado</th>                          <!--0-->
                    <th>Servicio</th>                        <!--1-->
                    <th>Controlador</th>                     <!--2-->
                    <th>Etiquetas</th>                       <!--3-->
                    <th>Domicilio</th>                       <!--4-->
                    <th>Área</th>                            <!--5-->
                    <th class="text-primary">Código</th>     <!--6-->
                    <th class="text-primary">Nombre</th>     <!--7-->
                    <th class="text-primary">Documento</th>  <!--8-->
                    <th class="text-primary">Teléfono</th>   <!--9-->
                    <th class="text-primary">Correo</th>     <!--10-->
                    <th class="text-primary">Día Venc.</th>  <!--11-->
                    <th>Impagas</th>                         <!--12-->
                    <th class="debt_month">Saldo</th>        <!--13-->
                    <th>Descuento</th>                       <!--14-->
                    <th>Deuda</th>                           <!--15-->
                    <th>Tarjeta Cobro Digital</th>           <!--16-->
                    <th>Débito Automático Cobro Digital</th> <!--17-->
                    <th>Débito Automático Chubut</th>        <!--18-->
                    <th>Tarjeta PayU</th>                    <!--19-->
                    <th>Comentario</th>                      <!--20-->
                    <th>Usuario</th>                         <!--21-->
                    <th>Habilitado</th>                      <!--22-->
                    <th>IP</th>                              <!--23-->
                </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
                <tr>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

<?php
    echo $this->element('labels', ['modal'=> 'modal-labels-connections',  'buttons' => $labels, 'save' => true ]);
    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-connections']);
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-connections']);
    echo $this->element('modal_preloader');
?>

<div class="tooltip" role="tooltip">
    <div class="arrow">
    </div>
    <div class="tooltip-inner">
    </div>
</div>

<div class="modal fade dont-fade bs-example-modal-sm" id="modal-debts" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Acción: Aplicar Recargos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">

                     <div class="col-xl-12">
                        <div class="row info-contain">
                            <div class="col-12">
                                <legend class="sub-title">Información</legend>
                            </div>
                            <div class="col-auto info-service">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Servicio: </label> <span class="lbl-service-selected"></span>
                                </span>
                            </div>
                            <div class="col-auto info-controller">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Controlador: </label> <span class="lbl-controller-selected"></span>
                                </span>
                            </div>
                            <div class="col-xl-12 info-area">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Área: </label> <span class="lbl-area-selected"></span>
                                </span>
                            </div>
                            <div class="col-auto info-debt">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Saldo: </label> <span class="lbl-debt-selected"></span>
                                </span>
                            </div>
                            <div class="col-auto info-invoice-no-paid">
                                <span class="badge badge-info">
                                    <label><span class="fas fa-search"></span> Impagas: </label> <span class="lbl-invoice-no-paid-selected"></span>
                                </span>
                            </div>
                            <div class="col-12">
                                <label>Cant. Conexiones: </label> <span class="lbl-connections-selected"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12">
                        <legend class="sub-title">Recargo</legend>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                        <?= $this->Form->input('surcharge_type', [ 'label' => 'Para', 'options' => ['next_month' => 'Mes próximo', 'this_month' => 'Adelantado']]); ?>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                        <?php echo $this->Form->input('value', ['type' => 'number', 'label' => 'Importe', 'min' => 0.01, 'step' => 0.01]); ?>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                        <label  class="control-label">Período</label> 
                        <div class='input-group date' id='period-datetimepicker'>
                            <input name='period' id="period" type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar">
                                <span class="glyphicon icon-calendar"></span>
                            </span>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                        <label  class="control-label">Vencimiento</label> 
                        <div class='input-group date' id='duedate-datetimepicker'>
                            <input name='duedate' id="duedate" type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar">
                                <span class="glyphicon icon-calendar"></span>
                            </span>
                        </div>
                    </div>

                    <div class="col-xl-6">
                        <?= $this->Form->input('presupuesto', ['type' => 'checkbox', 'label' => 'Forzar presupuesto', 'checked' => TRUE]); ?>
                    </div>

                    <div class="col-xl-12">
                        <?php echo $this->Form->input('concept', ['label' => 'Concepto', "autocomplete" => "off", 'value' => 'Recargo']); ?>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="btn-confirm-debt">Confirmar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade dont-fade bs-example-modal-sm" id="modal-discounts" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Acción: Aplicar Descuentos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">

                     <div class="col-xl-12">
                        <div class="row info-contain">
                            <div class="col-12">
                                <legend class="sub-title">Información</legend>
                            </div>
                            <div class="col-auto info-service">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Servicio: </label> <span class="lbl-service-selected"></span>
                                </span>
                            </div>
                            <div class="col-auto info-controller">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Controlador: </label> <span class="lbl-controller-selected"></span>
                                </span>
                            </div>
                            <div class="col-xl-12 info-area">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Área: </label> <span class="lbl-area-selected"></span>
                                </span>
                            </div>
                            <div class="col-auto info-debt">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Saldo: </label> <span class="lbl-debt-selected"></span>
                                </span>
                            </div>
                            <div class="col-auto info-invoice-no-paid">
                                <span class="badge badge-info">
                                    <label><span class="fas fa-search"></span> Impagas: </label> <span class="lbl-invoice-no-paid-selected"></span>
                                </span>
                            </div>
                            <div class="col-12">
                                <label>Cant. Conexiones: </label> <span class="lbl-connections-selected"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12">
                        <legend class="sub-title">Descuento</legend>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                        <?= $this->Form->input('surcharge_type', [ 'label' => 'Para', 'options' => ['next_month' => 'Mes próximo', 'this_month' => 'Adelantado']]); ?>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                        <?php echo $this->Form->input('value', ['type' => 'number', 'label' => 'Importe', 'min' => 0.01, 'step' => 0.01]); ?>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                        <label  class="control-label">Período</label> 
                        <div class='input-group date' id='period-datetimepicker'>
                            <input name='period' id="period" type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar">
                                <span class="glyphicon icon-calendar"></span>
                            </span>
                        </div>
                    </div>

                    <div class="col-xl-6">
                        <?= $this->Form->input('presupuesto', ['type' => 'checkbox', 'label' => 'Forzar presupuesto', 'checked' => TRUE]); ?>
                    </div>

                    <div class="col-xl-12">
                        <?php echo $this->Form->input('concept', ['label' => 'Concepto', "autocomplete" => "off", 'value' => 'Bonificación']); ?>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="btn-confirm-discount">Confirmar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade dont-fade bs-example-modal-sm" id="modal-block" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Acción: Bloquear</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">

                     <div class="col-xl-12">
                        <div class="row info-contain">
                            <div class="col-12">
                                <legend class="sub-title">Información</legend>
                            </div>
                            <div class="col-auto info-service">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Servicio: </label> <span class="lbl-service-selected"></span>
                                </span>
                            </div>
                            <div class="col-auto info-controller">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Controlador: </label> <span class="lbl-controller-selected"></span>
                                </span>
                            </div>
                            <div class="col-xl-12 info-area">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Área: </label> <span class="lbl-area-selected"></span>
                                </span>
                            </div>
                            <div class="col-auto info-debt">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Saldo: </label> <span class="lbl-debt-selected"></span>
                                </span>
                            </div>
                            <div class="col-auto info-invoice-no-paid">
                                <span class="badge badge-info">
                                    <label><span class="fas fa-search"></span> Impagas: </label> <span class="lbl-invoice-no-paid-selected"></span>
                                </span>
                            </div>
                            <div class="col-12">
                                <label>Cant. Conexiones: </label> <span class="lbl-connections-selected"></span>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="btn-confirm-block">Confirmar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade dont-fade bs-example-modal-sm" id="modal-unblock" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Acción: Habilitar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">

                     <div class="col-xl-12">
                        <div class="row info-contain">
                            <div class="col-12">
                                <legend class="sub-title">Información</legend>
                            </div>
                            <div class="col-auto info-service">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Servicio: </label> <span class="lbl-service-selected"></span>
                                </span>
                            </div>
                            <div class="col-auto info-controller">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Controlador: </label> <span class="lbl-controller-selected"></span>
                                </span>
                            </div>
                            <div class="col-xl-12 info-area">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Área: </label> <span class="lbl-area-selected"></span>
                                </span>
                            </div>
                            <div class="col-auto info-debt">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Saldo: </label> <span class="lbl-debt-selected"></span>
                                </span>
                            </div>
                            <div class="col-auto info-invoice-no-paid">
                                <span class="badge badge-info">
                                    <label><span class="fas fa-search"></span> Impagas: </label> <span class="lbl-invoice-no-paid-selected"></span>
                                </span>
                            </div>
                            <div class="col-12">
                                <label>Cant. Conexiones: </label> <span class="lbl-connections-selected"></span>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="btn-confirm-unblock">Confirmar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var table_connections = null;
    var connection_selected = null;
    var curr_pos_scroll = null;

    var labels = <?= json_encode($labels) ?>;

    var areas = null;
    var controllers = null;
    var users = null;
    var services = null;
    var hide_columns = [];

    $(document).ready(function () {

        areas = <?= json_encode($areas) ?>;
        controllers = <?= json_encode($controllers) ?>;
        services = <?= json_encode($services) ?>;
        users = <?= json_encode($users) ?>;

        var class_none = sessionPHP.paraments.gral_config.billing_for_service ? '' : 'd-none';
        var class_cobrodigital = 'd-none';
        var class_payu = 'd-none';
        var class_chubut_auto_debit = 'd-none';
        var search_cobrodigital = '';
        var search_chubut_auto_debit = '';
        var search_payu = '';
        var no_check = [14, 15, 16, 17];

        if (sessionPHP.paraments.gral_config.billing_for_service) {

            class_cobrodigital = sessionPHP.payment_getway.config.cobrodigital.enabled ? '' : 'd-none';
            class_payu = sessionPHP.payment_getway.config.payu.enabled ? '' : 'd-none';
            class_chubut_auto_debit = sessionPHP.payment_getway.config.chubut_auto_debit.enabled ? '' : 'd-none';

            search_cobrodigital = sessionPHP.payment_getway.config.cobrodigital.enabled ? 'options' : '';
            search_payu = sessionPHP.payment_getway.config.payu.enabled ? 'options' : '';
            search_chubut_auto_debit = sessionPHP.payment_getway.config.chubut_auto_debit.enabled ? 'options' : '';

            if (sessionPHP.payment_getway.config.cobrodigital.enabled) {
                removeItem = 14;
                no_check = jQuery.grep(no_check, function(value) {
                  return value != removeItem;
                });

                removeItem = 15;
                no_check = jQuery.grep(no_check, function(value) {
                  return value != removeItem;
                });
            }

            if (sessionPHP.payment_getway.config.chubut_auto_debit.enabled) {
                removeItem = 16;
                no_check = jQuery.grep(no_check, function(value) {
                  return value != removeItem;
                });
            }

            if (sessionPHP.payment_getway.config.payu.enabled) {
                removeItem = 17;
                no_check = jQuery.grep(no_check, function(value) {
                  return value != removeItem;
                });
            }
        }

        $('.btn-hide-column').click(function() {
           $('.modal-hide-columns-connections').modal('show');
        });

        $('.btn-search-column').click(function() {
           $('.modal-search-columns-connections').modal('show');
        });

        $('#table-connections-administrative').removeClass( 'display' );

		$('#btns-tools').hide();

		loadPreferences('connections-locked', [0, 12, 14, 15, 16, 17, 18, 19, 20]);

		table_connections = $('#table-connections-administrative').DataTable({
		    "order": [[ 0, 'desc' ]],
            "deferRender": true,
            "responsive": true,
            "processing": true,
            "serverSide": true,
		    "ajax": {
                "url": "/ispbrain/connections/get_connections_administrative.json",
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
                "error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "drawCallback": function( settings ) {
                var flag = true;
            	switch (settings.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
            	    if (table_connections) {

                        var tableinfo = table_connections.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }

                        if (curr_pos_scroll) {
                            $(table_connections.settings()[0].nScrollBody).scrollTop( curr_pos_scroll.top );
                            $(table_connections.settings()[0].nScrollBody).scrollLeft( curr_pos_scroll.left );
                        }
                    }
            	}

            	this.api().columns('.debt_month').every(function() {
                    var column = this;

                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    var sum = column
                        .data()
                        .reduce(function (a, b) { 

                           return intVal(a) + intVal(b);
                        }, 0);

                    $(column.footer()).html('$' + sum.toFixed(2));
                });
            },
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": false,
		    "columns": [
                {
                    "data": "created",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                {
                    "data": "service_id",
                    "type": "options",
                    "options": services,
                    "render": function ( data, type, row ) {
                        return data ? services[data] : '';
                    }
                },
                {
                    "data": "controller_id",
                    "type": "options",
                    "options": controllers,
                    "render": function ( data, type, row ) {
                        return data ? controllers[data] : '';
                    }
                },
                { 
                    "type": "options_colors",
                    "options": labels,
                    "render": function ( data, type, row ) {
                       if (row.labels.length > 0) {
                            var spans = "";
                            var names = "";
                            $.each(row.labels, function(i, val) {
                                names += "<span>" + val.text + "</span><br>"; 
                                spans += "<span class='status' style='background-color: " + val.color_back + "' > </span>"; 
                            });
                            return "<div data-toggle='tooltip_etiquetas' title='" + names + "'>" + spans + "</div>";
                        }
                        return '';
                    }
                },
                {
                    "data": "address",
                    "type": "string"
                },
                {
                    "data": "area_id",
                    "type": "options",
                    "options": areas,
                    "render": function ( data, type, row ) {
                        return data ? areas[data] : '';
                    }
                },
                {
                    "data": "customer_code",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                {
                    "data": "customer.name",
                    "type": "string"
                    
                },
                {
                    "data": "customer.ident",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (data) {
                            ident = data;
                        }
                        return sessionPHP.afip_codes.doc_types[row.customer.doc_type] + ' ' + ident;
                    }
                },
                {
                    "data": "customer.phone",
                    "type": "string"
                },
                {
                    "data": "customer.email",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                {
                    "data": "customer.daydue",
                    "type": "integer",
                },
                {
                    "data": "invoices_no_paid",
                    "type": "decimal",
                    "render": function ( data, type, row ) {

                        if (row.customer.billing_for_service) {

                           if (row.invoices_no_paid > 0) {
                               return "<span class='badge badge-danger font-size-13px' >" + row.invoices_no_paid + "</span>";
                           }
                           return "<span class='badge badge-success font-size-13px'>" + row.invoices_no_paid + "</span>";
                       }
                       return '-';
                    }
                },
                { 
                    "class": "right " + class_none,
                    "data": "debt_month",
                    "type": "decimal",
                    "render": function ( data, type, row ) {

                      if (row.customer.billing_for_service) {
                          return number_format(data, true);
                      }
                      return '-';
                    }
                },
                { 
                    "data": "discount_description " + class_none,
                    "type": "string",
                    "render": function ( data, type, row ) {

                        if (row.discount_always) {
                            return row.discount_description + ' (siempre)';
                        } else if (row.discount_month > 0) {
                            return row.discount_description + ' (' + row.discount_month + '-' + row.discount_total_month + ')';
                        }
                        return '';
                    }
                },
                {
                    "data": "force_debt",
                    "type": "options",
                    "options": {  
                        "1":"SI",
                        "0":"NO",
                    },
                    "render": function ( data, type, row ) {
                       if (data) {
                           return 'SI';
                       }
                       return 'NO';
                    }
                },
                {
                    "class": "center " + class_cobrodigital,
                    "data": "cobrodigital_card",
                    "type": search_cobrodigital,
                    "options": {  
                        "1":"SI",
                        "0":"NO",
                    },
                    "render": function ( data, type, row ) {
                       if (data) {
                           return 'SI';
                       }
                       return 'NO';
                    }
                },
                {
                    "class": "center " + class_cobrodigital,
                    "data": "cobrodigital_auto_debit",
                    "type": search_cobrodigital,
                    "options": {
                        "1":"SI",
                        "0":"NO",
                    },
                    "render": function ( data, type, row ) {
                       if (data) {
                           return 'SI';
                       }
                       return 'NO';
                    }
                },
                {
                    "class": "center " + class_chubut_auto_debit,
                    "data": "chubut_auto_debit",
                    "type": search_chubut_auto_debit,
                    "options": {  
                        "1":"SI",
                        "0":"NO",
                    },
                    "render": function ( data, type, row ) {
                       if (data) {
                           return 'SI';
                       }
                       return 'NO';
                    }
                },
                {
                    "class": "center " + class_payu,
                    "data": "payu_card",
                    "type": search_payu,
                    "options": {  
                        "1":"SI",
                        "0":"NO",
                    },
                    "render": function ( data, type, row ) {
                       if (data) {
                           return 'SI';
                       }
                       return 'NO';
                    }
                },
                {
                    "data": "comments",
                    "type": "string"
                },
                {
                    "data": "user_id",
                    "type": "options",
                    "options": users,
                    "render": function ( data, type, row ) {
                        return data ? users[data] : '';
                    }
                },
                {
                    "data": "enabled",
                    "type": "options",
                    "options": {  
                        "0":"Bloqueados",
                        "1":"Habilitados",
                    },
                    "render": function ( data, type, row ) {
                        var enabled = 'Bloqueado';
                        if (data) {
                            enabled = 'Habilitado';
                        }
                        return enabled;
                    }
                },
                { 
                    "data": "ip",
                    "type": "string"
                },
            ],
            "colReorder": {
                "order": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 ,18, 19, 20, 21]
            },
		    "columnDefs": [
		        { "type": 'date-custom', targets: [0] },
		        { "width": '8%', targets: [3, 6, 10, 11, 13] },
                { 
                    "class": "left", targets: [1, 2, 4, 5, 7, 12, 14, 16]
                },
                { 
                    "visible": false, targets: hide_columns
                },
                { "orderable": false, "targets": [3] }
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
                if (!data.enabled) {
                    $(row).addClass('locked');
                }
                if (data.error) {
                    $(row).addClass('error-custom');
                }
            },
		    "language": dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            "dom":
    	    	"<'row'<'col-auto mr-auto'f><'col-auto tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
		})
		.on('draw.dt', function () {
            $('[data-toggle="tooltip_etiquetas"]').tooltip({html: true});
            $('[data-toggle="tooltip_services"]').tooltip({html: true});
        });

		$('#table-connections-administrative').on( 'init.dt', function () {
             createModalHideColumn(table_connections, '.modal-hide-columns-connections', no_check);
             createModalSearchColumn(table_connections, '.modal-search-columns-connections');
        });

		$('#table-connections-administrative_wrapper .tools').append($('#btns-tools').contents());

        $('#btns-tools').show();

        $('#btn-actions').click(function() {
            var connections_selected = table_connections.$('tr.selected');
            if (connections_selected.length == 0) {
                generateNoty('warning', 'Debe seleccionar al menos una Conexión para realizar alguna acción.');
            } else {
                $('.modal-actions').modal('show');
            }
        });

        $('#modal-debts').on('shown.bs.modal', function (e) {

            $('#modal-debts #concept').val("Recargo");
            $('#modal-debts #value').val("");

            $("#modal-debts .info-service").addClass("d-none");
            $("#modal-debts .info-controller").addClass("d-none");
            $("#modal-debts .info-area").addClass("d-none");
            $("#modal-debts .info-debt").addClass("d-none");
            $("#modal-debts .info-invoice-no-paid").addClass("d-none");

            $("#modal-debts .lbl-service-selected").text("");
            $("#modal-debts .lbl-controller-selected").text("");
            $("#modal-debts .lbl-area-selected").text("");
            $("#modal-debts .lbl-debt-selected").text("");
            $("#modal-debts .lbl-invoice-no-paid-selected").text("");

            if ($('.btn-search-column').hasClass('search-apply')) {

                if ($('#service_id').val() != "") {
                    var service_filter = services[$('#service_id').val()];
                    $("#modal-debts .lbl-service-selected").text(service_filter);
                    $("#modal-debts .info-service").removeClass("d-none");
                }

                if ($('#controller_id').val() != "") {
                    var controller_filter = controllers[$('#controller_id').val()];
                    $("#modal-debts .lbl-controller-selected").text(controller_filter);
                    $("#modal-debts .info-controller").removeClass("d-none");
                }

                if ($('#area_id').val() != "") {
                    var area_filter = areas[$('#area_id').val()];
                    $("#modal-debts .lbl-area-selected").text(area_filter);
                    $("#modal-debts .info-area").removeClass("d-none");
                }

                if ($('#debt_month_min').val() != "" || $('#debt_month_max').val() != "") {
                    var debt_filter = 'Min: ' + $('#debt_month_min').val() + ' - Max: ' + $('#debt_month_max').val();
                    $("#modal-debts .lbl-debt-selected").text(debt_filter);
                    $("#modal-debts .info-debt").removeClass("d-none");
                }

                if ($('#invoices_no_paid_min').val() != "" || $('#invoices_no_paid_max').val() != "") {
                    var invoice_no_paid_filter = 'Min: ' + $('#invoices_no_paid_min').val() + ' - Max: ' + $('#invoices_no_paid_max').val();
                    $("#modal-debts .lbl-invoice-no-paid-selected").text(invoice_no_paid_filter);
                    $("#modal-debts .info-invoice-no-paid").removeClass("d-none");
                }
            }

            $('#modal-debts #period-datetimepicker').datetimepicker({
                locale: 'es',
                defaultDate: new Date(),
                format: 'MM/YYYY',
                icons: {
                    time: "glyphicon icon-alarm",
                    date: "glyphicon icon-calendar",
                    up: "glyphicon icon-arrow-right",
                    down: "glyphicon icon-arrow-left"
                }
            });

            $('#modal-debts #duedate-datetimepicker').datetimepicker({
                locale: 'es',
                defaultDate: new Date(),
                format: 'DD/MM/YYYY',
                icons: {
                    time: "glyphicon icon-alarm",
                    date: "glyphicon icon-calendar",
                    up: "glyphicon icon-arrow-right",
                    down: "glyphicon icon-arrow-left"
                }
            });

            var connections_selected = table_connections.$('tr.selected');
            $("#modal-debts .lbl-connections-selected").text(connections_selected.length);
        });

        $('#btn-confirm-debt').click(function() {

            var importe = $('#modal-debts #value').val();
            var periode = $('#modal-debts #period').val();
            var duedate = $('#modal-debts #duedate').val();
            var concept = $('#modal-debts #concept').val();
            var presupuesto = $('#modal-debts #presupuesto').prop('checked');

            if (importe == "" || importe <= 0  || periode == "" || duedate == "" || concept == "") {

                var message = "";

                if (importe == "" || importe <= 0) {
                    message += "*Debe ingresar un importe mayor a 0 \n";
                    message.replace(/\\n/g,"\n");
                }

                if (periode == "") {
                    message += "*Debe ingresar un período \n";
                    message.replace(/\\n/g,"\n");
                }

                if (duedate == "") {
                    message += "*Debe ingresar una fecha de vencimiento \n";
                    message.replace(/\\n/g,"\n");
                }

                if (concept == "") {
                    message += "*Debe ingresar un concepto \n";
                    message.replace(/\\n/g,"\n");
                }

                generateNoty('warning', message);
            } else {

                var text = '¿Está seguro qué desea aplicar recargos a las Conexiones seleccionadas?';

                bootbox.confirm(text, function(result) {

                    if (result) {

                        $('#modal-debts').modal('hide');

                        openModalPreloader('Generando recargos a las Conexiones ... ');

                        var selected_ids = [];
                        var connections_selected = table_connections.$('tr.selected');

                        $.each(connections_selected, function(i, connection) {
                            selected_ids.push(connection.id);
                        });

                        var data = {
                            surcharge_type:  $('#modal-debts #surcharge-type').val(),
                            concept: concept,
                            periode: periode,
                            importe: importe,
                            duedate: duedate,
                            ids: selected_ids,
                            presupuesto: presupuesto
                        }

                        var request = $.ajax({
                            url: "<?= $this->Url->build(['controller' => 'Connections', 'action' => 'applyMassiveDebt']) ?>",  
                            type: 'POST',
                            dataType: "json",
                            data: JSON.stringify(data),
                        });

                        request.done(function( data ) {
                            closeModalPreloader();

                            generateNoty(data.response.typeMsg, data.response.msg);

                            if (!data.response.error) {
                                table_connections.ajax.reload();
                            }
                        });

                        request.fail(function( jqXHR, textStatus ) {
                            if (jqXHR.status == 403) {

                            	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                            	setTimeout(function() { 
                                    window.location.href = "/ispbrain";
                            	}, 3000);

                            } else {
                            	closeModalPreloader();
                                generateNoty('error', 'Error al intentar generar recargos a las conexiones.');
                            }
                        });
                    }
                });
            }
        });

        $('#modal-discounts').on('shown.bs.modal', function (e) {

            $('#modal-discounts #concept').val("Bonificación");
            $('#modal-discounts #value').val("");

            $("#modal-discounts .info-service").addClass("d-none");
            $("#modal-discounts .info-controller").addClass("d-none");
            $("#modal-discounts .info-area").addClass("d-none");
            $("#modal-discounts .info-debt").addClass("d-none");
            $("#modal-discounts .info-invoice-no-paid").addClass("d-none");

            $("#modal-discounts .lbl-service-selected").text("");
            $("#modal-discounts .lbl-controller-selected").text("");
            $("#modal-discounts .lbl-area-selected").text("");
            $("#modal-discounts .lbl-debt-selected").text("");
            $("#modal-discounts .lbl-invoice-no-paid-selected").text("");

            if ($('.btn-search-column').hasClass('search-apply')) {

                if ($('#service_id').val() != "") {
                    var service_filter = services[$('#service_id').val()];
                    $("#modal-discounts .lbl-service-selected").text(service_filter);
                    $("#modal-discounts .info-service").removeClass("d-none");
                }

                if ($('#controller_id').val() != "") {
                    var controller_filter = controllers[$('#controller_id').val()];
                    $("#modal-discounts .lbl-controller-selected").text(controller_filter);
                    $("#modal-discounts .info-controller").removeClass("d-none");
                }

                if ($('#area_id').val() != "") {
                    var area_filter = areas[$('#area_id').val()];
                    $("#modal-discounts .lbl-area-selected").text(area_filter);
                    $("#modal-discounts .info-area").removeClass("d-none");
                }

                if ($('#debt_month_min').val() != "" || $('#debt_month_max').val() != "") {
                    var debt_filter = 'Min: ' + $('#debt_month_min').val() + ' - Max: ' + $('#debt_month_max').val();
                    $("#modal-discounts .lbl-debt-selected").text(debt_filter);
                    $("#modal-discounts .info-debt").removeClass("d-none");
                }

                if ($('#invoices_no_paid_min').val() != "" || $('#invoices_no_paid_max').val() != "") {
                    var invoice_no_paid_filter = 'Min: ' + $('#invoices_no_paid_min').val() + ' - Max: ' + $('#invoices_no_paid_max').val();
                    $("#modal-discounts .lbl-invoice-no-paid-selected").text(invoice_no_paid_filter);
                    $("#modal-discounts .info-invoice-no-paid").removeClass("d-none");
                }
            }

            $('#modal-discounts #period-datetimepicker').datetimepicker({
                locale: 'es',
                defaultDate: new Date(),
                format: 'MM/YYYY',
                icons: {
                    time: "glyphicon icon-alarm",
                    date: "glyphicon icon-calendar",
                    up: "glyphicon icon-arrow-right",
                    down: "glyphicon icon-arrow-left"
                }
            });

            var connections_selected = table_connections.$('tr.selected');
            $("#modal-discounts .lbl-connections-selected").text(connections_selected.length);
        });

        $('#btn-confirm-discount').click(function() {

            var importe = $('#modal-discounts #value').val();
            var periode = $('#modal-discounts #period').val();
            var concept = $('#modal-discounts #concept').val();
            var presupuesto = $('#modal-discounts #presupuesto').prop('checked');

            if (importe == "" || importe <= 0  || periode == "" || concept == "") {

                var message = "";

                if (importe == "" || importe <= 0) {
                    message += "*Debe ingresar un importe mayor a 0 \n";
                    message.replace(/\\n/g,"\n");
                }

                if (periode == "") {
                    message += "*Debe ingresar un período \n";
                    message.replace(/\\n/g,"\n");
                }

                if (concept == "") {
                    message += "*Debe ingresar un concepto \n";
                    message.replace(/\\n/g,"\n");
                }

                generateNoty('warning', message);
            } else {

                var text = '¿Está seguro qué desea aplicar descuentos a las Conexiones seleccionadas?';

                bootbox.confirm(text, function(result) {

                    if (result) {

                        $('#modal-discounts').modal('hide');

                        openModalPreloader('Generando descuentos a las Conexiones ... ');

                        var selected_ids = [];
                        var connections_selected = table_connections.$('tr.selected');

                        $.each(connections_selected, function(i, connection) {
                            selected_ids.push(connection.id);
                        });

                        var data = {
                            surcharge_type:  $('#modal-discounts #surcharge-type').val(),
                            concept: concept,
                            periode: periode,
                            importe: importe,
                            ids: selected_ids,
                            presupuesto: presupuesto
                        }

                        var request = $.ajax({
                            url: "<?= $this->Url->build(['controller' => 'Connections', 'action' => 'applyMassiveDiscount']) ?>",  
                            type: 'POST',
                            dataType: "json",
                            data: JSON.stringify(data),
                        });

                        request.done(function( data ) {
                            closeModalPreloader();

                            generateNoty(data.response.typeMsg, data.response.msg);

                            if (!data.response.error) {
                                table_connections.ajax.reload();
                            }
                        });

                        request.fail(function( jqXHR, textStatus ) {
                            if (jqXHR.status == 403) {

                            	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                            	setTimeout(function() { 
                                    window.location.href = "/ispbrain";
                            	}, 3000);

                            } else {
                            	closeModalPreloader();
                                generateNoty('error', 'Error al intentar generar descuentos a las conexiones.');
                            }
                        });
                    }
                });
            }
        });

        $('#modal-block').on('shown.bs.modal', function (e) {

            $("#modal-block .info-service").addClass("d-none");
            $("#modal-block .info-controller").addClass("d-none");
            $("#modal-block .info-area").addClass("d-none");
            $("#modal-block .info-debt").addClass("d-none");
            $("#modal-block .info-invoice-no-paid").addClass("d-none");

            $("#modal-block .lbl-service-selected").text("");
            $("#modal-block .lbl-controller-selected").text("");
            $("#modal-block .lbl-area-selected").text("");
            $("#modal-block .lbl-debt-selected").text("");
            $("#modal-block .lbl-invoice-no-paid-selected").text("");

            if ($('.btn-search-column').hasClass('search-apply')) {

                if ($('#service_id').val() != "") {
                    var service_filter = services[$('#service_id').val()];
                    $("#modal-block .lbl-service-selected").text(service_filter);
                    $("#modal-block .info-service").removeClass("d-none");
                }

                if ($('#controller_id').val() != "") {
                    var controller_filter = controllers[$('#controller_id').val()];
                    $("#modal-block .lbl-controller-selected").text(controller_filter);
                    $("#modal-block .info-controller").removeClass("d-none");
                }

                if ($('#area_id').val() != "") {
                    var area_filter = areas[$('#area_id').val()];
                    $("#modal-block .lbl-area-selected").text(area_filter);
                    $("#modal-block .info-area").removeClass("d-none");
                }

                if ($('#debt_month_min').val() != "" || $('#debt_month_max').val() != "") {
                    var debt_filter = 'Min: ' + $('#debt_month_min').val() + ' - Max: ' + $('#debt_month_max').val();
                    $("#modal-block .lbl-debt-selected").text(debt_filter);
                    $("#modal-block .info-debt").removeClass("d-none");
                }

                if ($('#invoices_no_paid_min').val() != "" || $('#invoices_no_paid_max').val() != "") {
                    var invoice_no_paid_filter = 'Min: ' + $('#invoices_no_paid_min').val() + ' - Max: ' + $('#invoices_no_paid_max').val();
                    $("#modal-block .lbl-invoice-no-paid-selected").text(invoice_no_paid_filter);
                    $("#modal-block .info-invoice-no-paid").removeClass("d-none");
                }
            }

            var connections_selected = table_connections.$('tr.selected');
            $("#modal-block .lbl-connections-selected").text(connections_selected.length);
        });

        $('#btn-confirm-block').click(function() {

            var text = '¿Está seguro qué desea Bloquear las Conexiones seleccionadas?';

            bootbox.confirm(text, function(result) {

                if (result) {

                    $('#modal-block').modal('hide');

                    openModalPreloader('Bloqueando Conexiones ... ');

                    var selected_ids = [];
                    var connections_selected = table_connections.$('tr.selected');

                    $.each(connections_selected, function(i, connection) {
                        selected_ids.push(connection.id);
                    }); 

                    $.ajax({
                        url: "<?= $this->Url->build(['action' => 'lockedMasive']) ?>",
                        type: 'POST',
                        dataType: "json",
                        data: JSON.stringify({ids: selected_ids}),
                        success: function(data) {

                            closeModalPreloader();

                            generateNoty(data.response.typeMsg, data.response.msg);

                            if (!data.response.error) {
                                table_connections.ajax.reload();
                            }
                        },
                        error: function (jqXHR) {

                            if (jqXHR.status == 403) {

                            	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                            	setTimeout(function() { 
                                    window.location.href = "/ispbrain";
                            	}, 3000);

                            } else {
                            	closeModalPreloader();
                                generateNoty('error', 'Error al intentar Bloquear las Conexiones.');
                            }
                        }
                    });
                }
            });
        });

        $('#modal-unblock').on('shown.bs.modal', function (e) {

            $("#modal-unblock .info-service").addClass("d-none");
            $("#modal-unblock .info-controller").addClass("d-none");
            $("#modal-unblock .info-area").addClass("d-none");
            $("#modal-unblock .info-debt").addClass("d-none");
            $("#modal-unblock .info-invoice-no-paid").addClass("d-none");

            $("#modal-unblock .lbl-service-selected").text("");
            $("#modal-unblock .lbl-controller-selected").text("");
            $("#modal-unblock .lbl-area-selected").text("");
            $("#modal-unblock .lbl-debt-selected").text("");
            $("#modal-unblock .lbl-invoice-no-paid-selected").text("");

            if ($('.btn-search-column').hasClass('search-apply')) {

                if ($('#service_id').val() != "") {
                    var service_filter = services[$('#service_id').val()];
                    $("#modal-unblock .lbl-service-selected").text(service_filter);
                    $("#modal-unblock .info-service").removeClass("d-none");
                }

                if ($('#controller_id').val() != "") {
                    var controller_filter = controllers[$('#controller_id').val()];
                    $("#modal-unblock .lbl-controller-selected").text(controller_filter);
                    $("#modal-unblock .info-controller").removeClass("d-none");
                }

                if ($('#area_id').val() != "") {
                    var area_filter = areas[$('#area_id').val()];
                    $("#modal-unblock .lbl-area-selected").text(area_filter);
                    $("#modal-unblock .info-area").removeClass("d-none");
                }

                if ($('#debt_month_min').val() != "" || $('#debt_month_max').val() != "") {
                    var debt_filter = 'Min: ' + $('#debt_month_min').val() + ' - Max: ' + $('#debt_month_max').val();
                    $("#modal-unblock .lbl-debt-selected").text(debt_filter);
                    $("#modal-unblock .info-debt").removeClass("d-none");
                }

                if ($('#invoices_no_paid_min').val() != "" || $('#invoices_no_paid_max').val() != "") {
                    var invoice_no_paid_filter = 'Min: ' + $('#invoices_no_paid_min').val() + ' - Max: ' + $('#invoices_no_paid_max').val();
                    $("#modal-unblock .lbl-invoice-no-paid-selected").text(invoice_no_paid_filter);
                    $("#modal-unblock .info-invoice-no-paid").removeClass("d-none");
                }
            }

            var connections_selected = table_connections.$('tr.selected');
            $("#modal-unblock .lbl-connections-selected").text(connections_selected.length);
        });

        $('#btn-confirm-unblock').click(function() {

            var text = '¿Está seguro qué desea Habilitar las Conexiones seleccionadas?';

            bootbox.confirm(text, function(result) {

                if (result) {

                    $('#modal-unblock').modal('hide');

                    openModalPreloader('Habilitando conexiones ... ');

                    var selected_ids = [];
                    var connections_selected = table_connections.$('tr.selected');

                    $.each(connections_selected, function(i, connection) {
                        selected_ids.push(connection.id);
                    }); 

                    $.ajax({
                        url: "<?= $this->Url->build(['action' => 'unlockedMasive']) ?>",
                        type: 'POST',
                        dataType: "json",
                        data: JSON.stringify({ids: selected_ids}),
                        success: function(data) {

                            closeModalPreloader();

                            generateNoty(data.response.typeMsg, data.response.msg);

                            if (!data.response.error) {
                                table_connections.ajax.reload();
                            }
                        },
                        error: function (jqXHR) {

                            if (jqXHR.status == 403) {

                            	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                            	setTimeout(function() { 
                                    window.location.href = "/ispbrain";
                            	}, 3000);

                            } else {
                            	closeModalPreloader();
                                generateNoty('error', 'Error al intentar Habilitar las Conexiones.');
                            }
                        }
                    });
                }
            });
        });

    });

    $('.btn-update-table').click(function() {
         if (table_connections) {
            table_connections.ajax.reload();
        }
    });

    $(".btn-export").click(function() {

        bootbox.confirm('Se descargará un archivo Excel', function(result) {
            if (result) {
                $('#table-connections-administrative').tableExport({tableName: 'Conexiones', type:'excel', escape:'false'});
            }
        }).find("div.modal-content").addClass("confirm-width-md");

    });

    $('#table-connections-administrative tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                $(this).addClass('selected');
            }

            connection_selected = table_connections.row( this ).data();
        }
    });

    $('#btn-lock').click(function() {
        $('.modal-actions').modal('hide');
        $('#modal-block').modal('show');
    });

    $('#btn-unlock').click(function() {
        $('.modal-actions').modal('hide');
        $('#modal-unblock').modal('show');
    });

    $('#btn-select-all').click(function() {
        var i = 0;
        table_connections.rows().every(function(rowIdx, tableLoop, rowLoop) {
            $(this.node()).addClass('selected');
        });
    });

    $('#btn-unselect-all').click(function() {
        table_connections.rows().every(function(rowIdx, tableLoop, rowLoop) {
            $(this.node()).removeClass('selected');
        });
    });

    $('#btn-apply-debts').click(function() {
        $('.modal-actions').modal('hide');
        $('#modal-debts').modal('show');
    });

    $('#btn-apply-discounts').click(function() {
        $('.modal-actions').modal('hide');
        $('#modal-discounts').modal('show');
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
