<style type="text/css">

    legend {
        display: block;
        width: 100%;
        padding: 0;
        margin-bottom: 20px;
        font-size: 21px;
        line-height: inherit;
        color: #333;
        border: 0;
        border-bottom: 1px solid #e5e5e5;
    }

    .title-modal-full-name {
        font-weight: bold;
    }

    #map {
		height: 480px;
	}

	#floating-panel {
		top: 10px;
		left: 25%;
		z-index: 5;
		background-color: #fff;
		padding: 5px;
		border: 1px solid #999;
		text-align: center;
		font-family: 'Roboto','sans-serif';
		line-height: 30px;
		padding-left: 10px;
	}

    textarea.form-control {
        height: 80px !important;
    }

    tr.selected {
        background-color: #8eea99;
        color: #333 !important;
    }

    .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
        background-color: #d2d2d2;
    }

    #submit-map {
        padding: 10px;
        top: 2px;
    }

    #update-map {
        padding: 10px;
        top: 2px;
    }

    #btn-find-ip {
        padding: 10px;
    }

    a[disabled] {
        pointer-events: none;
        color: grey; 
    }

    .modal-header .close {
        padding: 0;
        margin: 0;
    }

    tr.row-service-pending {
        background-color: #2196F3;
        color: white;
    }

    #table-service_pending tr {
        font-size: 20px !important;
    }

    .bootstrap-select.btn-group .dropdown-menu.inner {
        max-height: 350px !important;
    }

    .font-size-13px {
        font-size: 13px;
    }

    #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
    }

    #infowindow-content .title {
        font-weight: bold;
    }

    #infowindow-content {
        display: none;
    }

    #map #infowindow-content {
        display: inline;
    }

    .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
    }

    #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
    }

    .pac-controls {
        display: inline-block;
        padding: 5px 11px;
    }

    .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
    }

    #pac-input:focus {
        border-color: #4d90fe;
    }

    #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
    }

    #target {
        width: 345px;
    }

</style>

<?php

    $status = [
        'CP' => ['class' => 'cp', 'text' => 'Conexión Pendiente'],
        'CC' => ['class' => 'cc', 'text' => 'Conexión Creada'],
        'I' => ['class' => 'i', 'text' => 'Instalación Registrada'],
    ];

?>

<div id="btns-tools">

    <div class="text-right btns-tools margin-bottom-5" >

        <?php

            echo $this->Html->link(
                '<span class="fas fa-sync-alt"  aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Actualizar la tabla',
                'class' => 'btn btn-default btn-update-table ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-eye" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Ocultar o Mostrar Columnas',
                'class' => 'btn btn-default btn-hide-column ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon fas fa-search"  aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Buscar por Columnas',
                'class' => 'btn btn-default btn-search-column ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<i class="fas fa-list-ul"></i>',
                ['controller' => 'connections', 'action' => 'index'],
                [
                'title' => 'Ir a la tabla de Conexiones',
                'class' => 'btn btn-default  ml-1',
                'escape' => false
            ]);
        ?>

    </div>
</div>

<div class="row">

    <div class="col-12">
        <legend class="sub-title-sm"><?= __('Seleccione el Cliente') ?></legend>
    </div>

    <div class="col-md-12">
        <table id="table-customers" class="table table-bordered">
            <thead>
                <tr>
                    <th></th>                     <!--0-->
                    <th >Creado</th>              <!--1-->
                    <th >Código</th>              <!--2-->
                    <th >Cuenta</th>              <!--3-->
                    <th >Etiquetas</th>           <!--4-->
                    <th >Impagas</th>             <!--5-->
                    <th >Saldo mes</th>           <!--6-->
                    <th >Nombre</th>              <!--7-->
                    <th >Tipo</th>                <!--8-->
                    <th >Nro</th>                 <!--9-->
                    <th >Servicios</th>           <!--10-->
                    <th >Domicilio</th>           <!--11-->
                    <th >Área</th>                <!--12-->
                    <th >Teléfono</th>            <!--13-->
                    <th >Vendedor</th>            <!--14-->
                    <th >Resp. Iva</th>           <!--15-->
                    <th >Comp.</th>               <!--16-->
                    <th >Deno.</th>               <!--17-->
                    <th >Correo electrónico</th>  <!--18-->
                    <th >Comentarios</th>         <!--19-->
                    <th >Día Venc.</th>           <!--20-->
                    <th >Clave Portal</th>        <!--21-->
                    <th >Empresa Facturación</th> <!--22-->
                    <th >Compromiso de Pago</th>  <!--23-->
                </tr>
            </thead>
            <tbody class="searchable-customers">
            </tbody>
        </table>  
    </div>
</div>

<div class="modal fade" id="modelAddConnection" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <div class="col-xl-7">

                    <div class="row">
                        <div class="col-12">
                            <h4 class="modal-title">Cliente: <span id="lbl_name" class="font-weight-bold text-primary" ></span></h4>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <span class="modal-title">Código: <span id="lbl_code" class="font-weight-bold text-primary" ></span></span>
                        </div>
                    </div>

                </div>
                <div class="col-xl-4">
                    <h4 class="modal-title d-sm-none d-lg-none d-md-block d-xl-block">Nueva Conexión</h4>
                </div>
                <div class="col-xl-1">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>

            </div>
            <div class="modal-body p-2">
                <?= $this->Form->create($connection, ['class' => 'form-add-connection']) ?>
                <fieldset>

                    <div class="row mb-2">

                        <div class="col-xl-6">

                            <!--fecha de creacion-->
                            <div class="row mb-3" id="input-created">
                                <div class="col-xl-6">
                                    <label for="">Fecha</label>
                                    <div class='input-group date' id='created-datetimepicker'>
                                        <input name='created' id="created" type='text' class="form-control" required />
                                        <span class="input-group-addon input-group-text calendar">
                                            <span class="glyphicon icon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <!--que plan solicito-->
                            <div class="row d-none">
                                <div class="col-xl-12">
                                    <label class="control-label mb-0 text-success">Solicitó el Plan:</label>
                                    <span id="planask"  class="badge badge-success ml-1 mb-1"></span>
                                </div>
                            </div>

                            <div class="row justify-content-center">

                                <!--seleccion de controlador-->
                                <div class="col-12 pr-0">
                                    <?php
                                        echo $this->Form->input('controller_id', [
                                            'label' => 'Controlador', 
                                            'class' => 'selectpicker', 
                                            'data-live-search' => "true",
                                    	    'required' => true,
                                        ]);
                                    ?>
                                </div>

                                <!--selccion de plan-->
                                <div class="col-12 plan-select-container pr-0" >
                                    <?php
                                        echo $this->Form->input('plan_id', [
                                            'label' => 'Servicio', 
                                            'class' => 'selectpicker', 
                                            'data-live-search' => "true",
                                    	    'required' => true,
                                        ]);
                                    ?>
                                </div>

                                <!--selccion de pool-->
                                <div class="col-12 pr-0" id="pool-select-container" >
                                    <?php
                                        echo $this->Form->input('pool_id', [
                                            'label' => 'Red', 
                                            'class' => 'selectpicker', 
                                            'data-live-search' => "true",
                                    	    'required' => true,
                                        ]);
                                    ?>
                                </div>

                            </div>

                            <!--si es controlador de tipo ppooe -->
                            <div class="row" id="credential-pppoe">

                                <div class="col-12 pr-0 mb-3">

                                    <div class="card border-secondary">
                                        <div class="card-header p-2">PPPoE</div>
                                        <div class="card-body text-secondary pt-2 pl-2  pr-2 pb-0">

                                            <div class="row">

                                                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">

                                                    <div class="input-group">
                                                        <!--usuario pppoe-->
                                                        <input type="text" name="name" id="pppoe_user" placeholder="Usuario" required="required" autocomplete="off" class="form-control ppp-credenctial">
                                                    </div>

                                                </div>
                                                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">

                                                    <!--clave pppoe-->
                                                    <?php
                                                        echo $this->Form->input('password', [
                                                            'id' => 'pppoe_pass',
                                                            'placeholder' => 'Clave',
                                                            'label' => false,
                                                            'required' => true,
                                                            'readonly' => false,
                                                            'type' => 'text',
                                                            'autocomplete' => 'off',
                                                            'class' => 'ppp-credenctial'
                                                        ]);
                                                    ?>
                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-sm-12 col-md-8 col-lg-8 col-xl-8">

                                    <div class="form-group input-group">

                                        <label  class="input-group-addon input-group-text">
                                            <i id="label-ip-spin" class="fas fa-cog fa-spin"></i>
                                            <span id="label-ip-text">IP</span>
                                        </label>

                                        <!--ip de conexion-->
                                        <input type="text" class="form-control" name="ip" id="ip" autocomplete="off" required>

                                        <!--buscador de ip libre-->
                                        <span class="input-group-btn">
                                            <button id="btn-find-ip" title="Buscar IP" class="btn btn-success" type="button"><span class="text-white glyphicon icon-search" aria-hidden="true"></span></button>
                                        </span>
                                    </div>

                                </div>

                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 border border-secondary  rounded mb-3">

                                    <!--marac si la ip es publica-->
                                    <?php echo $this->Form->input('validate_ip', ['label' => ['text' => 'validar IP', 'class' => 'pt-2'], 'checked' => true, 'class' => 'mt-0']);?>

                                </div>

                            </div>

                            <div class="row" id="interface-select-container">

                                <!--selccion de interface (ip fija)-->
                                <div class="col-12 pr-0" >
                                    <?php
                                        echo $this->Form->input('interface', [
                                            'label' => 'Interface', 
                                    	    'required' => false,
                                        ]);
                                    ?>
                                </div>

                            </div>

                            <div class="row">

                                 <div class="col-sm-12 col-md-8 col-lg-8 col-xl-8">

                                    <!--mac addres del equipo cliente-->

                                    <?php
                                      echo $this->Form->input('mac', [
                                            'placeholder' => '',
                                            'data-toggle' => 'tooltip',
                                            'data-placement' => 'top',
                                            'title' => 'Puede ingresar la MAC en distintos formatos, ejemplos: 02:AE:FC:02:F3:E4, 02-AE-FC-02-F3-E4, 02AEFC02F3E4',
                                            'label' => 'Dirección MAC',
                                            'minlength' => 12,
                                            'maxlength' => 17,
                                            'required' => false,
                                            'autocomplete' => 'off'
                                        ]);
                                    ?>
                                </div>

                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 border border-secondary pt-2 rounded mb-3  pt-0">

                                    <!--habilitar la generacion de proporcional-->
                                    <?= $this->Form->input('proportional', ['label' => 'Generar Proporcional', 'type' => 'checkbox', 'checked' => $paraments->accountant->service_debt])?>

                                </div>

                                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-12">
                                    <!--domicilio de la conexion-->
                                    <?php
                                        echo $this->Form->input('address', ['type' => 'txt', 'label' => 'Domicilio', 'required' => true, 'autocomplete' => 'off' ]);
                                    ?>
                                </div>

                            </div>

                            <!--otros datos para documentacion-->
                            <div class="row">

                                <div class="col-12">

                                    <div class="accordion" id="accordionExample">
                                        <div class="card">
                                            <div class="card-header p-2" id="headingOne">
                                                <h5 class="mb-0">
                                                    <button class="btn btn-secondary" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                        Otro datos
                                                    </button>
                                                </h5>
                                            </div>

                                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                                <div class="card-body p-1">

                                                    <div class="row">

                                                        <div class="col-sm-12 col-md-6 col-lg-6 col-xl-12">
                                                            <!--area de la conexion-->
                                                            <?php
                                                                echo $this->Form->input('area_id', ['options' => $areas, 'label' => 'Área',  'id' => 'area_id']);
                                                            ?>
                                                        </div>

                                                        <!--name de la queue-->
                                                        <div class="col-xl-12 d-none" id="queue-name-container">
                                                           	<?php
                                                                echo $this->Form->input('queue_name', [
                                                                    'placeholder' => 'Automático',
                                                                    'label' => 'Nombre (queue)',
                                                                    'required' => false,
                                                                    'readonly' => false,
                                                                    'type' => 'text',
                                                                    'autocomplete' => 'off'
                                                                ]);
                                                            ?>
                                                        </div>

                                                        <div class="col-xl-6">
                                                            <?php
                                                               echo $this->Form->input('clave_wifi', [ 'placeholder' => 'Clave WiFi', 'label' => false, 'type' => 'text', 'required' => false, 'autocomplete' => 'off' ]);
                                                            ?>
                                                        </div>
                                                        <div class="col-xl-6">
                                                            <?php
                                                               echo $this->Form->input('ports', [ 'placeholder' => 'Puertos', 'label' => false, 'type' => 'text', 'required' => false, 'autocomplete' => 'off' ]);
                                                            ?>
                                                        </div>
                                                        <div class="col-xl-6">
                                                            <?php
                                                               echo $this->Form->input('ip_alt', [ 'placeholder' => 'IP Alternativo', 'label' => false, 'type' => 'text', 'required' => false, 'autocomplete' => 'off' ]);
                                                            ?>
                                                        </div>
                                                        <div class="col-xl-12">
                                                            <?php
                                                               echo $this->Form->input('ing_traffic', [ 'placeholder' => 'Ing. Tráfico', 'label' => false, 'type' => 'text', 'required' => false, 'autocomplete' => 'off' ]);
                                                            ?>
                                                        </div>
                                                        <div class="col-xl-12">
                                                            <?php
                                                               echo $this->Form->input('comments', [ 'placeholder' => 'Comentario', 'label' => false, 'type' => 'textarea', 'required' => false, 'autocomplete' => 'off' ]);
                                                            ?>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="col-lg-12 col-xl-6">

                            <!--buscador de direccion en el mapa  -->
                        	<div id="floating-panel">

                    		    <div class="row">
                    		        <div class="col-md-12">
                        		        <div class="input-group">
                        		            <span class="input-group-btn">
                                                <button id="update-map" title="Recargar Mapa" class="btn btn-info" type="button"><span class="text-white glyphicon icon-map2" aria-hidden="true"></span></button>
                                            </span>
                                            <input id="addressmap" type="text" class="form-control margin-top-5" value="" placeholder="Calle #, Ciudad, Provincia">
                                            <span class="input-group-btn">
                                                <button id="submit-map" title="Buscar Dirección" class="btn btn-success" type="button"><span class="text-white glyphicon icon-search" aria-hidden="true"></span></button>
                                            </span>
                                        </div>
                    		        </div>
                    		    </div>
                    		</div>

                    		<div id="map" class="mb-3"></div>
                        </div>

                        <!--datos ocultos-->

                        <?php
                            echo $this->Form->input('customer_name', [ 'id' => 'name', 'type' => 'hidden']);
                            echo $this->Form->input('customer_code', [ 'id' => 'code', 'type' => 'hidden']);
                            echo $this->Form->input('service_pending_id', ['type' => 'hidden', 'id' => 'service_pending_id']);
                            echo $this->Form->input('lat', ['type' => 'hidden', 'id' => 'coodmapslat']);
                            echo $this->Form->input('lng', ['type' => 'hidden', 'id' => 'coodmapslng']);
                        ?>

                    </div>

                </fieldset>

                <button type="button" class="btn btn-default float-left" data-dismiss="modal">Cancelar</button>

                <?= $this->Form->button(__('Agregar y Salir'), ['class' => 'btn-success float-right', 'name' => 'btn-add', 'value' => 'exit']) ?>
                <?= $this->Form->button(__('Agregar'), ['class' => 'btn-success float-right mr-2', 'name' => 'btn-add', 'value' => '']) ?>

                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-service_pending" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header"> 
                <h4 class="modal-title">Seleccione servicio pendiente</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body p-2">
    
                <div class="row">
                    <div class="col-12">
                    
                        <table id="table-service_pending" class="table table-bordered w-100" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th >Pedido</th>    
                                    <th >Servicio</th>      
                                    <th >Domicilio</th>    
                                    <th >Vendor</th>         
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>  

                    </div>
                </div>
    
                <div class="row justify-content-between mt-2">
                    <div class="col-auto">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    </div>
                    <div class="col-auto">
                        <button type="button" class="btn btn-success" id="btn-select-service-pending">Cargar datos</button>
                    </div>
                </div>
    
            </div>
        </div>
    </div>
</div>

<?php
    echo $this->element('modal_preloader');
    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-customers-addconnection']);
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-customers-addconnection']);
?>

<div id="last_pppoe" class="modal fade" tabindex="-1" role="dialog">

    <div class="modal-dialog modal-md" role="document">

        <div class="modal-content">

            <div class="modal-header p-2">
                <h4 class="modal-title">Última credencial PPPoE generada.</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <div class="modal-body p-2">

                <?php if (count($last_pppoe) > 0): ?>

                <div class="row">

                    <div class="col-xl-12">

                        <div class="form-group text">
                            <label class="control-label m-0">Cliente <span class="badge badge-success">Código: <?= $last_pppoe['last-pppoe-customer-code'] ?></span></label>
                            <span class="badge badge-secondary w-100 m-0" style="font-size: 15px;" > <?= $last_pppoe['last-pppoe-customer'] ?> </span>
                        </div>

                        <div class="form-group text">
                            <label class="control-label m-0">Domicilio: </label>
                            <span class="badge badge-secondary w-100 m-0" style="font-size: 15px;" > <?= $last_pppoe['last-pppoe-address'] ?> </span>
                        </div>

                    </div>

                    <div class="col-xl-6">
                        <div class="form-group text">
                            <label class="control-label m-0">Controlador: </label>
                            <span class="badge badge-secondary w-100 m-0" style="font-size: 15px;"  > <?= $last_pppoe['last-pppoe-controller'] ?> </span>
                        </div>  
                    </div>

                    <div class="col-xl-6">
                        <div class="form-group text">
                            <label class="control-label m-0">Servicio: </label>
                            <span class="badge badge-secondary w-100 m-0" style="font-size: 15px;" > <?= $last_pppoe['last-pppoe-service'] ?> </span>
                        </div>      
                    </div>

                    <div class="col-xl-12 text-center">
                         <div class="form-group text m-0">
                            <label class="control-label m-0" >IP: </label>
                            <label class="text-primary font-weight-bold w-100 m-0" style="font-size: 25px;" > <?= $last_pppoe['last-pppoe-ip'] ?> </label>
                        </div> 
                    </div>

                    <div class="col-xl-6 text-center">
                        <div class="form-group text m-0">
                            <label class="control-label m-0" >Usuario: </label>
                            <label class="text-success font-weight-bold w-100 m-0" style="font-size: 25px;" > <?= $last_pppoe['last-pppoe-name'] ?> </label>
                        </div>  
                    </div>

                    <div class="col-xl-6 text-center">
                        <div class="form-group text m-0">
                            <label class="control-label m-0" >Clave: </label>
                            <label class="text-success font-weight-bold w-100 m-0" style="font-size: 25px;" > <?= $last_pppoe['last-pppoe-pass'] ?> </label>
                        </div>     
                    </div>

                </div>

                <?php endif; ?>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2K3zoK46JybWPzQbhfQQqIIbdZ_3WcQs&libraries=places">
</script>

<script type="text/javascript">

    var table_customers;
    var controllers;
    var customer_selected = null;
    var table_service_pending = null;
    var service_pending_selected = null;
    var areas = null;
    var services = null;
    var users = null;

    var last_pppoe = <?= json_encode($last_pppoe) ?>;
    var labels = <?= json_encode($labels) ?>;

    $(document).ready(function() {

        if (last_pppoe.length != 0) {

             $('#last_pppoe').modal({
                keyboard: true,
                backdrop: 'static',
                show: true
            });
        }

        areas = <?= json_encode($areas) ?>;
        services = <?= json_encode($services) ?>;
        users = <?= json_encode($users) ?>;

        $('#validate-ip').change(function(){

            if ($('#validate-ip').is(':checked')) {

                $('#ip').prop('readonly', true);
                $('input#ip').val('');
                $('#btn-find-ip').click();
                $('#interface-select-container').hide();

            } else {

                $('#ip').prop('readonly', false);
                $('input#ip').val('');
                $('#interface-select-container').show();
            }

        });

        $('#validate-ip').attr('checked', true);
        $('#ip').prop('readonly', true);

        $('#label-ip-text').show();
        $('#label-ip-spin').hide();

        $('#label-credentials-text').show();
        $('#label-credentials-spin').hide();

        if (sessionPHP.paraments.connection.pool_plan) {
            $('#pool-select-container').hide();
        }

        $('.btn-hide-column').click(function() {

           $('.modal-hide-columns-customers-addconnection').modal('show');

        });

        $('.btn-search-column').click(function() {

           $('.modal-search-columns-customers-addconnection').modal('show');

        });

        var mode_migration =  sessionPHP.paraments.system.mode_migration;

        if (mode_migration) {
            $('#created-datetimepicker').datetimepicker({
                locale: 'es',
                defaultDate: new Date(),
                format: 'DD/MM/YYYY'
            });
        } else {
             $('#input-created').remove();
        }

        controllers = <?= json_encode($controllers) ?>;

        $('#controller-id').html('');

        $('#controller-id').selectpicker({
            noneSelectedText: 'Seleccionar controlador'
        });

        $('#controller-id').append($('<option>', { 
            value: '',
            text : 'Seleccionar controlador'
        }));

        $.each(controllers, function (i, controller) {
            $('#controller-id').append($('<option>', { 
                value: controller.id,
                text : controller.name 
            }));
        });

        $('#controller-id').selectpicker('refresh');

        $('#plan-id').selectpicker({
            noneSelectedText: 'Seleccionar servicio'
        });

        $('#plan-id').append($('<option>', { 
            value: '',
            text : 'Seleccionar servicio' 
        }));

        $('#plan-id').change(function() { //cuando selecciono el plan

            $.each(controllers, function (i, controller) {

                if (controller.id == $('#controller-id').val()) {

                    $.each(controller.tplans, function (i, plan) {

                        if ($('#plan-id').val() == plan.id) { // selecicone un plan

                            $('#pool-id').val(plan.pool_id);
                            $('#pool-id').change();
                        } 
                    });
                }
            });
        });

        $('#plan-id').selectpicker('refresh');

        $('#pool-id').selectpicker({
            noneSelectedText: 'Seleccionar red'
        });

        $('#pool-id').append($('<option>', { 
            value: '',
            text : 'Seleccionar red' 
        }));

        $('#pool-id').change(function() { //cuando seleccionó la red

            if ($('#validate-ip').is(':checked')) {
                $('input#ip').val('');
                $('#btn-find-ip').click();
            }

            $.each(controllers, function (i, controller) {

                if (controller.id == $('#controller-id').val()) {

                    if (controller.tcontroller.arp) {

                        $('#interface').val('');

                        $.each(controller.tpools, function (i, pool) {

                            if ($('#pool-id').val() == pool.id) { // selecicone un pool

                                $.each(controller.tgateways, function (i, gateway) {

                                    console.log(controller.tgateways);

                                    if (pool.id == gateway.pool_id) {

                                       

                                        $('#interface').val(gateway.interface);

                                        console.log($('#interface').val());

                                    }
                                });
                            } 
                        });
                    }
                }
            });
        });

        $('#controller-id').change(function() { //cuando selecciono el controlador

            $('input#ip').val('');

            $.each(controllers, function (i, controller) {

                if (controller.id == $('#controller-id').val()) {

                    switch (controller.template) {

                        case 'MikrotikIpfijaTa':

                            $('#credential-pppoe').hide();
                            $(".ppp-credenctial").prop('required', false);
                            $('#queue-name-container').show()

                            //si esta hablitado la creacion de ARP 
                            if (controller.tcontroller.arp) {

                                $("#mac").prop('required', true);

                                //cargo interfaces del conmtrolador

                                $('#interface').val('');
                                $("#interface").prop('required', true);

                            } else {
                                 $("#mac").prop('required', false);
                                 $("#interface").prop('required', false);
                            }

                            break;

                        case 'MikrotikPppoeTa':

                            if (sessionPHP.paraments.connection.pppoe_credential) {
                                
                                $('#credential-pppoe').hide();
                                $(".ppp-credenctial").prop('required', false);
                            } else {
                                
                                $('#credential-pppoe').show();
                                $(".ppp-credenctial").prop('required', true);
                            }

                            $('#queue-name-container').hide()
                            $("#mac").prop('required', false);
                            $("#interface").prop('required', false);

                            break;

                        case 'MikrotikDhcpTa':              

                            $('#credential-pppoe').hide();
                            $(".ppp-credenctial").prop('required', false);
                            $('#queue-name-container').show()
                            $("#mac").prop('required', true);
                            $("#interface").prop('required', false);
                    }

                    //cargo planes del controlador

                    $('#plan-id').html('');

                    $('#plan-id').append($('<option>', { 
                        value: '',
                        text : 'Seleccionar Servicio' 
                    }));

                    $('#plan-id').selectpicker('refresh');

                    $.each(controller.tplans, function (i, plan) {

                        if (service_pending_selected) {

                            if (service_pending_selected.service_id == plan.service.id) {

                                $('#plan-id').append($('<option>', { 
                                    value: plan.id,
                                    text : plan.service.name 
                                }));

                                $('#plan-id').selectpicker('refresh');

                                $('#plan-id').val(plan.id);
                            }

                        } else {

                            $('#plan-id').append($('<option>', { 
                                value: plan.id,
                                text : plan.service.name 
                            }));

                            $('#plan-id').selectpicker('refresh');
                        }
                    });

                    //cargo pools del conmtrolador

                    $('#pool-id').html('');

                    $('#pool-id').append($('<option>', { 
                        value: '',
                        text : 'Seleccionar red' 
                    }));

                    $('#pool-id').selectpicker('refresh');

                    $.each(controller.tpools, function (i, pool) {

                        $('#pool-id').append($('<option>', { 
                            value: pool.id,
                            text : pool.name 
                        }));

                        $('#pool-id').selectpicker('refresh');
                    });

                    if (service_pending_selected) {
                        $('#plan-id').change();
                    }
                }
            });
        });

        $('#controller-id').change();

        $('#btn-find-ip').click(function() {

            var controller_id = $('select#controller-id').val();

            var pool_id = $('select#pool-id').val();

            if (controller_id != '' && pool_id != '') {

                $('#label-ip-text').hide();
                $('#label-ip-spin').show();

                $.ajax({
                    url: "<?= $this->Url->build(['controller' => 'connections', 'action' => 'findIPAjax']) ?>",
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify({ controller_id: controller_id, pool_id: pool_id}),
                    success: function(data) {

                        if (data.data.error) {
                             generateNoty('warning', data.data.error);
                        } else {
                            $('input#ip').val(data.data.ip);
                            $('#pool-id').val(data.data.pool_id);    
                            $('#pool-id').selectpicker('refresh');
                        }

                        $('#label-ip-text').show();
                        $('#label-ip-spin').hide();
                    },
                    error: function (jqXHR) {

                        if (jqXHR.status == 403) {

                        	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                        	setTimeout(function() { 
                                window.location.href = "/ispbrain";
                        	}, 3000);

                        } else {
                        	generateNoty('error', 'Error al intentar buscar IP');
                        }
                    }
                });
            }

        });

        $('#table-customers').removeClass( 'display' );

        $('#btns-tools').hide();

        loadPreferences('customers-addconnection1', [3, 11, 13, 14, 15, 16, 17, 18, 19, 20, 21]);

	    table_customers = $('#table-customers').DataTable({
		    "order": [[ 1, 'desc' ]],
            "deferRender": true,
            "processing": true,
            "serverSide": true,
		    "ajax": {
                "url": "/ispbrain/customers/get_customers.json",
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "drawCallback": function( settings ) {
                var flag = true;
            	switch (settings.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
            	    if (table_customers) {

                        var tableinfo = table_customers.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }
                    }
            	}
            },
            "scrollY": true,
		    "scrollY": '400px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
		    "columns": [
                {
                    "className": 'details-control ',
                    "orderable": false,
                    "data":      'connections',
                    "render": function ( data, type, full, meta ) {
                        return '';
                    },
                    "width": '3%'
                },
                { 
                    "data": "created",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                { 
                    "data": "code",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                { 
                    "data": "account_code",
                    "type": "string"
                },
                { 
                    "data": "status",
                    "type": "options_colors",
                    "options": labels,
                    "render": function ( data, type, row ) {
                        if (row.labels.length > 0) {
                            var spans = "";
                            var names = "";
                            $.each(row.labels, function(i, val) {

                                names += "<span>" + val.text + "</span><br>"; 
                                spans += "<span class='status' style='background-color: " + val.color_back + "' > </span>"; 
                            });
                            return "<div data-toggle='tooltip_etiquetas' title='" + names + "'>" + spans + "</div>";
                        }
                        return '';
                    }
                },
                { 
                    "data": "invoices_no_paid",
                    "type": "decimal",
                    "render": function ( data, type, row ) {
                       if (row.invoices_no_paid > 0) {
                           return "<span class='badge badge-danger font-size-13px' >" + row.invoices_no_paid + "</span>";
                       }
                       return "<span class='badge badge-success font-size-13px'>" + row.invoices_no_paid + "</span>";
                    }
                    
                },
                { 
                    "data": "debt_month",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' ),
                },
                { 
                    "data": "name",
                    "type": "string",
                },
                {
                    "data": "doc_type",
                    "type": "options",
                    "options": sessionPHP.afip_codes.doc_types,
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.doc_types[data];
                    }
                },
                {
                    "data": "ident",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return data;
                    }
                },
                { 
                    "data": "services_active",
                    "render": function ( data, type, row ) {
                       return "<span class='font-size-13px'>" + row.services_active + '/' + row.services_total + "</span>";
                    }
                    
                },
                { 
                    "data": "address",
                    "type": "string",
                },
                { 
                    "data": "area_id",
                    "type": "options",
                    "options": areas,
                    "render": function ( data, type, row ) {
                        return data ? areas[data] : '';
                    }
                },
                { 
                    "data": "phone",
                    "type": "integer",
                },
                { 
                    "data": "seller",
                    "type": "string",
                },
                { 
                    "data": "responsible",
                    "type": "options",
                    "options": sessionPHP.afip_codes.responsibles,
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.responsibles[data];
                    }
                },
                { 
                    "data": "is_presupuesto",
                    "type": "options",
                    "options": {  
                        "0":"Factura",
                        "1":"Presupuesto",
                    },
                    "render": function ( data, type, row ) {
                        if (data) {
                            return 'PRESU';
                        }
                        return 'FACTURA';
                    }
                },
                { 
                    "data": "denomination",
                    "type": "options",
                    "options": {  
                        "0":"No Denominado",
                        "1":"Denominado",
                    },
                    "render": function ( data, type, row ) {
                        if (data) {
                            return 'SI';
                        }
                        return 'NO';
                    }
                },
                { 
                    "data": "email",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                { 
                    "data": "comments",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                { 
                    "data": "daydue",
                    "type": "integer",
                },
                { 
                    "data": "clave_portal",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                { 
                    "data": "business_billing",
                    "type": "options_obj",
                    "options": sessionPHP.paraments.invoicing.business,
                    "render": function ( data, type, row ) {
                        var business_name = '';
                        $.each(sessionPHP.paraments.invoicing.business, function (i, b) {
                            if (b.id == data) {
                                business_name = b.name;
                            }
                        });
                        return business_name;
                    }
                },
                { 
                    "data": "payment_commitment",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        var created = '';
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                        return created;
                    }
                },
            ],
		    "columnDefs": [
                {
                    "targets": [1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 18, 20, 21, 23],
                    "width": '5%'
                },
                {
                    "targets": [22],
                    "width": '8%'
                },
                {
                    "targets": [10, 18],
                    "width": '13%'
                },
                {
                    "targets": [7],
                    "width": '12%'
                },
                { 
                    "class": "left", targets: [7, 11]
                },
                { 
                    "visible": false, targets: hide_columns
                },
                { "orderable": false, "targets": [4, 10] }
            ],
            "createdRow" : function( row, data, index ) {
               row.id = data.code;
               if (data.service_pending.length > 0) {
                   $(row).addClass('row-service-pending');
               }
            },
		    "language": dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
        	"dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
		});

        $('#table-customers_wrapper .tools').append($('#btns-tools').contents());

        $('#table-customers').on( 'init.dt', function () {
            createModalHideColumn(table_customers, '.modal-hide-columns-customers-addconnection');
            createModalSearchColumn(table_customers, '.modal-search-columns-customers-addconnection');
        });

        $('#btns-tools').show();

        $('.btn-update-table').click(function() {
             if (table_customers) {
                table_customers.ajax.reload();
            }
        });

        initMap();
    });

    $('.form-add-connection').submit(function(){
        openModalPreloader("Creando Conexión  ...");
    }); 

    function ClearCredentials() {

        $('input#pppoe_user').val('');
        $('input#pppoe_pass').val('');

    }

    table_service_pending = $('#table-service_pending ').DataTable({
		    "deferRender": true,
    	    "autoWidth": true,
    	    "scrollX": true,
    	    "searching" : false,
            "ordering": false,
    	    "paging": false,
    	    "info": false,

            "drawCallback": function( settings ) {
            },
            "scrollY": true,
		    "scrollY": '400px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "columns": [
                { 
                    "data": "created",
                },
                { 
                    "data": "name",
                },
                { 
                    "data": "address"
                },
                { 
                    "data": "user_name"
                }
            ],
		    "columnDefs": [
            ],
            "createdRow" : function( row, data, index ) {
                
               row.id = data.id;
            },
		    "language": dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
        	"dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
	});

	function openModalAddConnection() {

	    $('input#code').val(customer_selected.code);
        $('input#name').val(customer_selected.name);
        $('span#lbl_code').html(pad(customer_selected.code, 5));
        $('span#lbl_name').html(customer_selected.name);
        $('#address').val(customer_selected.address);
        $('#area_id').val(customer_selected.area_id);      

        $('#pool-id').html('');

        $('#pool-id').append($('<option>', { 
            value: '',
            text : 'Seleccionar red' 
        }));

        $('#pool-id').selectpicker('val', '');
        $('#pool-id').change();
        $('#pool-id').selectpicker('refresh');

        $('#plan-id').html('');

        $('#plan-id').append($('<option>', { 
            value: '',
            text : 'Seleccionar servicio' 
        }));

        $('#plan-id').selectpicker('val', '');
        $('#plan-id').change();
        $('#plan-id').selectpicker('refresh');

        $('#controller-id').val('');

        if (service_pending_selected) {

            $('#service_pending_id').val(service_pending_selected.id);

            $('#address').val(service_pending_selected.address);

            $('#planask').html(service_pending_selected.name);

            $('#plan-id').attr('readonly', true);

            $('#plan-id').attr("style", "pointer-events: none;");

        } else {

            $('#service_pending_id').val('');

            $('#plan-id').attr('readonly', false);

            $('#plan-id').attr("style", "");
        }

        $('#label-credentials-text').show();
        $('#label-credentials-spin').hide();

        $('#credential-pppoe').hide();

        $('#label-ip-text').show();
        $('#label-ip-spin').hide();

        $('#interface-select-container').hide();

        $('#modelAddConnection').modal('show');

        initMap();
	}

	$('#btn-select-service-pending').click(function() {

	   if (service_pending_selected) {

	       $('#modal-service_pending').modal('hide');

           openModalAddConnection();

	   } else {
	       generateNoty('warning', 'debe seleccionar un servicio.');
	   }
	});

    $('#table-service_pending tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_service_pending.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');

            service_pending_selected = table_service_pending.row( this ).data();
        }
    });

    $('#table-customers tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_customers.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');

            customer_selected = table_customers.row( this ).data();

            table_service_pending.clear().draw();

            var flag = false;

            if (customer_selected.service_pending.length > 0) {

                $.each(customer_selected.service_pending, function(i, service_pending) {

                    var created = service_pending.presale.created.split('T')[0];
                    created = created.split('-');
                    created = created[2] + '/' + created[1] + '/' + created[0];

                    table_service_pending.rows.add([{
                        'id': service_pending.id,
                        'created': created,
                        'name': service_pending.service.name,
                        'address': service_pending.address,
                        'user_name': service_pending.presale.user.username,
                        'service_id': service_pending.service_id,
                        'discunt_id': service_pending.discunt_id,
                        'address': service_pending.address,
                        'lat': service_pending.lat,
                        'lng': service_pending.lng,
                        'discount_name': service_pending.discount_name,
                        'discount_total': service_pending.discount_total,

                    }]).draw();

                });

                $('#modal-service_pending').modal('show');

            } else {

                service_pending_selected = null;
                openModalAddConnection();
            }
        }
    });

    $('#modal-service_pending').on('shown.bs.modal', function (e) {
        table_service_pending.draw();
    });

    $('#modelAddConnection').on('shown.bs.modal', function (e) {
        $('#controller-id').change();
        $('#plan-id').selectpicker('val', '');
        $('#plan-id').change();
        $('#plan-id').selectpicker('refresh');
        refreshMap();
        clearModalAddConnection();
    });

    function clearModalAddConnection() {
        $('#controller-id').val('');
        $('input#pppoe_user').val('');
        $('input#pppoe_pass').val('');
        $('input#ip').val('');
        $('input#comments').val('');
        $('input#mac').val('');
        $('input#addressmap').val('');
    }

    $('#update-map').click(function() {
        initMap();
    });

    $('form input').on('keypress', function(e) {
        return e.which !== 13;
    });

    var map = null;
    var geocoder = null;
    var marker = null;

    function isEmpty(val) {
        return (val === undefined || val == null || val == 0 || val.length <= 0) ? true : false;
    }

	function initMap() {

		geocoder = new google.maps.Geocoder;

		var myLatlng = {lat: sessionPHP.paraments.system.map.lat, lng: sessionPHP.paraments.system.map.lng};

		if (service_pending_selected && service_pending_selected.lat && service_pending_selected.lng) {

		    myLatlng = {lat: service_pending_selected.lat, lng: service_pending_selected.lng};
		}

		var mapOptions = {
			zoom: sessionPHP.paraments.system.map.zoom,
			center: myLatlng,
			mapTypeId: 'hybrid'   // roadmap, satellite, hybrid, terrain 
		};

		map = new google.maps.Map(document.getElementById('map'), mapOptions);

		document.getElementById('submit-map').addEventListener('click', function() {
			geocodeAddress(geocoder, map);
		});

		var init = {lat: sessionPHP.paraments.system.map.lat, lng: sessionPHP.paraments.system.map.lng};
        var latLng = new google.maps.LatLng(init.lat, init.lng);

        if (service_pending_selected && service_pending_selected.lat && service_pending_selected.lng) {

		    latLng = new google.maps.LatLng(service_pending_selected.lat, service_pending_selected.lng); 

		    addMarker(latLng);
		}

        map.setCenter(latLng);

		map.addListener('click', function(event) {
			addMarker(event.latLng);	    
		});
    }

    function refreshMap() {
        google.maps.event.trigger(map, 'resize');
    }

    $('input#addressmap').keypress(function(event) {

        if ( event.which == 13 ) {
            geocodeAddress(geocoder, map);
        }
    });

	function addMarker(location) {

		if (marker != null) {
			clearMarkers();
		}

		marker = new google.maps.Marker({
			position: location,
			map: map
		});
		marker.setMap(map);	

        var lat = location.lat();

        var lng = location.lng();

		$('#coodmapslat').val(lat);
		$('#coodmapslng').val(lng);	
	}

	function clearMarkers() {
		marker.setMap(null);
	}

	function geocodeAddress(geocoder, map) {

		var address = document.getElementById('addressmap').value;

		geocoder.geocode({
			'address': address
			}, function(results, status) {

				if (status === google.maps.GeocoderStatus.OK) {	
					addMarker(results[0].geometry.location)  	    
					map.setCenter(results[0].geometry.location);
					map.setZoom(14);

    			} else {
    				if (status == google.maps.GeocoderStatus.ZERO_RESULTS) {
    			        bootbox.alert('No se encontraron Resultados');
    			    } else {
    			        window.alert('No se encontraron Resultados. Estado: ' + status);
    			    }
    			}
		});
	}

</script>

<script type='text/javascript'>

    function initialize() {

        var input = document.getElementById('addressmap');

        var autocomplete = new google.maps.places.Autocomplete(input);
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>
