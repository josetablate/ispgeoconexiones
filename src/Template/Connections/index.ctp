<style type="text/css">

    tr.locked > td.actions {
        background-color: white;
    }

</style>

<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5" >

        <?php

           echo $this->Html->link(
                '<span class="fas fa-sync-alt" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Actualizar la tabla',
                'class' => 'btn btn-default btn-update-table ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-eye" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Ocultar o Mostrar Columnas',
                'class' => 'btn btn-default btn-hide-column ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon fas fa-search" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Buscar por Columnas',
                'class' => 'btn btn-default btn-search-column ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                ['controller' => 'connections', 'action' => 'add'],
                [
                'title' => 'Agregar una nueva conexión',
                'class' => 'btn btn-default btn-add-connection ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export ml-1',
                'escape' => false
            ]);
        ?>

    </div>
</div>

<div class="row">
    <div class="col-xl-12">

        <table id="table-connections" class="table table-bordered">
            <thead>
                <tr>
                    <th>Creado</th>            <!--0-->
                    <th>Código</th>            <!--1-->
                    <th>Etiquetas</th>         <!--2-->
                    <th>Nombre</th>            <!--3-->
                    <th>Documento</th>         <!--4-->
                    <th>Domicilio</th>         <!--5-->
                    <th>Área</th>              <!--6-->
                    <th>Teléfono</th>          <!--7-->
                    <th>Controlador</th>       <!--8-->
                    <th>Servicio</th>          <!--9-->
                    <th>IP</th>                <!--10-->
                    <th>MAC</th>               <!--11-->
                    <th>Modificado</th>        <!--12-->
                    <th>Usuario</th>           <!--13-->
                    <th>Estado</th>            <!--14-->
                    <th>Comentario</th>        <!--15-->
                    <th>Clave WiFi</th>        <!--16-->
                    <th>Puertos</th>           <!--17-->
                    <th>IP Alt.</th>           <!--18-->
                    <th>Ing. Tráfico</th>      <!--19-->
                    <th>IP anterior</th>       <!--20-->
                    <th>Diferencia</th>        <!--21-->
                    <th>PPPoE Nombre</th>      <!--22-->
                    <th>PPPoE Clave</th>       <!--23-->
                    <th>Queue Nombre</th>      <!--24-->
                    <th>Últ. Bloqueo</th>      <!--25-->
                    <th>Últ. Habilitación</th> <!--26-->
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-info',
        'name' =>  'Información',
        'icon' =>  'icon-eye',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-edit',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id' =>  'btn-label',
        'name' =>  'Etiquetas',
        'icon' =>  'fas fa-tag',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-disable-connection',
        'name' =>  'Bloquear',
        'icon' =>  'glyphicon icon-lock',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-enable-connection',
        'name' =>  'Habilitar',
        'icon' =>  'glyphicon icon-unlocked',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-go-ip',
        'name' =>  'Abrir Antena',
        'icon' =>  'fas fa-broadcast-tower',
        'type' =>  'btn-secondary'
    ];

    // $buttons[] = [
    //     'id'   =>  'btn-go-router',
    //     'name' =>  'Abrir Router',
    //     'icon' =>  'glyphicon icon-eye d-none',
    //     'type' =>  'btn-secondary'
    // ];

    $buttons[] = [
        'id'   =>  'btn-view-realTimeGraph',
        'name' =>  'Gráfica en tiempo real',
        'icon' =>  'fas fa-chart-area',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-view-speed',
        'name' =>  'Gráfica historial',
        'icon' =>  'glyphicon icon-plus',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-go-customer',
        'name' =>  'Ficha del Cliente',
        'icon' =>  'glyphicon icon-profile',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    $buttons[] = [
        'id'   =>  'btn-sync',
        'name' =>  'Sincronizar',
        'icon' =>  'icon-tab',
        'type' =>  'btn-warning'
    ];

    $buttons[] = [
        'id'   =>  'btn-observations',
        'name' =>  'Observaciones',
        'icon' =>  'far fa-edit',
        'type' =>  'btn-dark'
    ];

    echo $this->element('actions', ['modal'=> 'modal-connections', 'title' => 'Acciones', 'buttons' => $buttons ]);
    echo $this->element('labels', ['modal'=> 'modal-labels-connections',  'buttons' => $labels, 'save' => true ]);
    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-connections']);
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-connections']);
    echo $this->element('modal_preloader');

    echo $this->element('Observations/add', ['modal' => 'modal-add-observation', 'title' => 'Agregar Observación', 'records' => TRUE]);
    echo $this->element('Observations/edit', ['modal' => 'modal-edit-observation', 'title' => 'Observación']);
?>

<div id="last_pppoe" class="modal fade" tabindex="-1" role="dialog">

    <div class="modal-dialog modal-md" role="document">

        <div class="modal-content">

            <div class="modal-header p-2">
                <h4 class="modal-title">Última credencial PPPoE generada.</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <div class="modal-body p-2">

                <?php if (count($last_pppoe) > 0): ?>

                <div class="row">

                    <div class="col-xl-12">
                        <div class="form-group text">
                            <label class="control-label m-0">Cliente <span class="badge badge-success">Código: <?= $last_pppoe['last-pppoe-customer-code'] ?></span></label>
                            <span class="badge badge-secondary w-100 m-0" style="font-size: 15px;"> <?= $last_pppoe['last-pppoe-customer'] ?> </span>
                        </div>

                        <div class="form-group text">
                            <label class="control-label m-0">Domicilio: </label>
                            <span class="badge badge-secondary w-100 m-0" style="font-size: 15px;" > <?= $last_pppoe['last-pppoe-address'] ?> </span>
                        </div>  
                    </div>

                    <div class="col-xl-6">
                        <div class="form-group text">
                            <label class="control-label m-0">Controlador: </label>
                            <span class="badge badge-secondary w-100 m-0" style="font-size: 15px;"  > <?= $last_pppoe['last-pppoe-controller'] ?> </span>
                        </div>  
                    </div>

                    <div class="col-xl-6">
                        <div class="form-group text">
                            <label class="control-label m-0">Servicio: </label>
                            <span class="badge badge-secondary w-100 m-0" style="font-size: 15px;" > <?= $last_pppoe['last-pppoe-service'] ?> </span>
                        </div>      
                    </div>

                    <div class="col-xl-12 text-center">
                         <div class="form-group text m-0">
                            <label class="control-label m-0" >IP: </label>
                            <label class="text-primary font-weight-bold w-100 m-0" style="font-size: 25px;" > <?= $last_pppoe['last-pppoe-ip'] ?> </label>
                        </div> 
                    </div>

                    <div class="col-xl-6 text-center">
                        <div class="form-group text m-0">
                            <label class="control-label m-0" >Usuario: </label>
                            <label class="text-success font-weight-bold w-100 m-0" style="font-size: 25px;" > <?= $last_pppoe['last-pppoe-name'] ?> </label>
                        </div>  
                    </div>

                    <div class="col-xl-6 text-center">
                        <div class="form-group text m-0">
                            <label class="control-label m-0" >Clave: </label>
                            <label class="text-success font-weight-bold w-100 m-0" style="font-size: 25px;" > <?= $last_pppoe['last-pppoe-pass'] ?> </label>
                        </div>     
                    </div>

                </div>

                <?php endif; ?>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">

    var table_connections = null;
    var connection_selected = null;

    var curr_pos_scroll = null;

    var labels = <?= json_encode($labels) ?>;

    var areas = null;
    var controllers = null;
    var services = null;
    var users = null;

    var last_pppoe = <?= json_encode($last_pppoe) ?>;

    $(document).ready(function () {

        if (last_pppoe.length != 0) {

            $('#last_pppoe').modal({
                keyboard: true,
                backdrop: 'static',
                show: true
            });
        }

        areas = <?= json_encode($areas) ?>;
        controllers = <?= json_encode($controllers) ?>;
        services = <?= json_encode($services) ?>;
        users = <?= json_encode($users) ?>;

        $('.btn-hide-column').click(function() {
            $('.modal-hide-columns-connections').modal('show');
        });

        $('.btn-search-column').click(function() {
            $('.modal-search-columns-connections').modal('show');
        });

        $('#table-connections').removeClass( 'display' );

		$('#btns-tools').hide();

		loadPreferences('connections4-index', [11 ,12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]);

		table_connections = $('#table-connections').DataTable({
		    "order": [[ 12, 'desc' ]],
            "deferRender": true,
            "processing": true,
            "serverSide": true,
		    "ajax": {
                "url": "/ispbrain/connections/get_connections.json",
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
                "error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "drawCallback": function( settings ) {
                var flag = true;
            	switch (settings.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
            	    if (table_connections) {

                        var tableinfo = table_connections.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }

                        if (curr_pos_scroll) {
                            $(table_connections.settings()[0].nScrollBody).scrollTop( curr_pos_scroll.top );
                            $(table_connections.settings()[0].nScrollBody).scrollLeft( curr_pos_scroll.left );
                        }
                    }
            	}
            },
		    "autoWidth": true,
		    "scrollY": '420px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "columns": [
                { 
                    "data": "created",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                { 
                    "data": "customer_code",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                { 
                    "type": "options_colors",
                    "options": labels,
                    "render": function ( data, type, row ) {
                       if (row.labels.length > 0) {
                            var spans = "";
                            var names = "";
                            $.each(row.labels, function(i, val) {
                                names += "<span>" + val.text + "</span><br>"; 
                                spans += "<span class='status' style='background-color: " + val.color_back + "' > </span>"; 
                            });
                            return "<div data-toggle='tooltip_etiquetas' title='" + names + "'>" + spans + "</div>";
                        }
                        return '';
                    }
                },
                { 
                    "data": "customer.name",
                    "type": "string"
                    
                },
                { 
                    "data": "customer.ident",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (data) {
                            ident = data;
                        }
                        return sessionPHP.afip_codes.doc_types[row.customer.doc_type] + ' ' + ident;
                    }
                },
                { 
                    "data": "address",
                    "type": "string"
                },
                { 
                    "data": "area_id",
                    "type": "options",
                    "options": areas,
                    "render": function ( data, type, row ) {
                        return data ? areas[data] : '';
                    }
                },
                { 
                    "data": "customer.phone",
                    "type": "string"
                },
                { 
                    "data": "controller_id",
                    "type": "options",
                    "options": controllers,
                    "render": function ( data, type, row ) {
                        return data ? controllers[data] : '';
                    }
                },
                { 
                    "data": "service_id",
                    "type": "options",
                    "options": services,
                    "render": function ( data, type, row ) {
                        return data ? services[data] : '';
                    }
                },
                { 
                    "data": "ip",
                    "type": "string"
                },
                { 
                    "data": "mac",
                    "type": "string",
                },
                { 
                    "data": "modified",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var modified = data.split('T')[0];
                            modified = modified.split('-');
                            return modified[2] +' /' + modified[1] + '/' + modified[0];
                        }
                    }
                },
                { 
                    "data": "user_id",
                    "type": "options",
                    "options": users,
                    "render": function ( data, type, row ) {
                        return data ? users[data] : '';
                    }
                },
                { 
                    "data": "enabled",
                    "type": "options",
                    "options": {  
                        "0":"Bloqueados",
                        "1":"Habilitados",
                    },
                    "render": function ( data, type, row ) {
                        var enabled = 'Bloqueado';
                        if (data) {
                            enabled = 'Habilitado';
                        }
                        return enabled;
                    }
                },
                { 
                    "data": "comments",
                    "type": "string"
                },
                { 
                    "data": "clave_wifi",
                    "type": "string"
                },
                { 
                    "data": "ports",
                    "type": "string"
                },
                { 
                    "data": "ip_alt",
                    "type": "string"
                },
                { 
                    "data": "ing_traffic",
                    "type": "string"
                },
                { 
                    "data": "before_ip",
                    "type": "string"
                },
                { 
                    "data": "diff",
                    "type": "options",
                    "options": {  
                        "0":"Sin diferencia",
                        "1":"Con diferencia",
                    },
                    "render": function ( data, type, row ) {
                        var enabled = 'Sin diferencia';
                        if (data) {
                            enabled = 'Con diferencia';
                        }
                        return enabled;
                    }
                },
                {
                    "data": "pppoe_name",
                    "type": "string"
                },
                {
                    "data": "pppoe_pass",
                    "type": "string"
                },
                { 
                    "data": "queue_name",
                    "type": "string"
                },
                {
                    "data": "blocking_date",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                        return "";
                    }
                },
                {
                    "data": "enabling_date",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                        return "";
                    }
                }
            ],
		    "columnDefs": [
		        { "width": '8%', targets: [1] },
		        { "width": '14%', targets: [9] },
                { 
                    "class": "left", targets: [3, 8]
                },
                { 
                    "width": "15%", targets: [3, 4]
                },
                { 
                    "width": "20%", targets: [5]
                },
                { 
                    "visible": false, targets: hide_columns
                },
                { "orderable": false, "targets": 2 }
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
                if (!data.enabled) {
                    $(row).addClass('locked');
                }
                if (data.diff /*|| data.api_id*/) {
                    $(row).addClass('error-custom');
                }
            },
		    "language": dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            "dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
		}).on( 'draw.dt', function () {
            $('[data-toggle="tooltip_etiquetas"]').tooltip({html: true});
        });

		$('#table-connections').on( 'init.dt', function () {
             createModalHideColumn(table_connections, '.modal-hide-columns-connections');
             createModalSearchColumn(table_connections, '.modal-search-columns-connections');
        });

		$('#table-connections_wrapper .tools').append($('#btns-tools').contents());

        $('#btns-tools').show();

        $('#btn-observations').click(function() {
            customer_code = connection_selected.customer_code;
            $('.modal-connections').modal('hide');
            $('#modal-add-observation').modal('show');
        });
    });

    $('.btn-update-table').click(function() {
         if (table_connections) {
            table_connections.ajax.reload();
        }
    });

    $('#btn-label').click(function() {

        $('.modal-labels-connections .check').addClass('d-none');

        $.each(connection_selected.labels, function(c, label) {

            $('.modal-labels-connections .label-btn').each(function(i, btn) {

                if (label.id == $(this).data('labelid')) {
                    $(this).find('.check').removeClass('d-none');
                }
            });
        });

        $('.modal-connections').modal('hide');
        $('.modal-labels-connections').modal('show');
    });

    $('.modal-labels-connections #label-save').click(function() {

        var send_data = {};
        send_data.connection_id = connection_selected.id; 
        send_data.labels = [];

        $('.modal-labels-connections .label-btn').each(function(a, lala) {
            if (!$(this).find('.check').hasClass('d-none')) {
                send_data.labels.push($(this).data('labelid'));
            }
        });  

        curr_pos_scroll = {
            'top': $(table_connections.settings()[0].nScrollBody).scrollTop(),
            'left': $(table_connections.settings()[0].nScrollBody).scrollLeft()
        };

        $.ajax({
            url: "<?= $this->Url->build(['controller' => 'connections', 'action' => 'changeLabels']) ?>",
            type: 'POST',
            dataType: "json",
            data: JSON.stringify(send_data),
            success: function(data) {

                if (data.connection) {

                    customer_selected = data.connection;

                    table_connections.row( $('#table-connections tr#' + connection_selected.id) ).data( connection_selected ).draw();
                    $('.modal-labels-connections').modal('hide');
                }
            },
            error: function (jqXHR) {
                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() { 
                        window.location.href = "/ispbrain";
                	}, 3000);

                } else {
                	generateNoty('error', 'Error al intentar cambiar las etiquetas');
                }
            }
        });
    });

    $('#btn-sync').click(function() {
        var action = '/ispbrain/connections/syncConnection/' + connection_selected.id;
        window.open(action, '_self');
    });

    $(".btn-export").click(function() {

        bootbox.confirm('Se descargará un archivo Excel', function(result) {
            if (result) {
                $('#table-connections').tableExport({tableName: 'Conexiones', type:'excel', escape:'false'});
            }
        }).find("div.modal-content").addClass("confirm-width-md");

    });

    $('#table-connections tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_connections.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            connection_selected = table_connections.row( this ).data();

            if (connection_selected.enabled == 1) {
                $('btn-enable-connection').hide();
                $('btn-disable-connection').show();
            } else {
                $('btn-disable-connection').hide();
                $('btn-enable-connection').show();
            }

            $('.modal-connections').modal('show');
        }
    });

    $('#btn-info').click(function() {
        var action = '/ispbrain/connections/view/' + connection_selected.id;
        window.open(action, '_blank');
    });

    $('#btn-clear-error').click(function() {

         $('.modal-connections').modal('hide');

         $.ajax({
            url: "<?= $this->Url->build(['controller' => 'connections', 'action' => 'clearErrors']) ?>",
            type: 'POST',
            dataType: "json",
            data: JSON.stringify({ connection_id: connection_selected.id}),
            success: function(data) {
 
                if (!data.data) {
                     generateNoty('error', 'No se pudo limpiar los errores de la conexión');
                } else {
                    table_connections.ajax.reload();
                    table_connections.draw();
                    generateNoty('success', 'Limpieza de errores completa');
                }
            },
            error: function (jqXHR) {
                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() { 
                        window.location.href = "/ispbrain";
                	}, 3000);

                } else {
                    generateNoty('error', 'No se pudo limpiar los errores de la conexión');
                }
            }
        });
    });

    $('#btn-edit').click(function() {
        var action = '/ispbrain/connections/edit/' + connection_selected.id;
        window.open(action, '_blank');
    });

    $('#btn-go-customer').click(function() {
        var action = '/ispbrain/customers/view/' + connection_selected.customer_code;
        window.open(action, '_blank');
    });

    $('#btn-go-ip').click(function() {
        var ip = connection_selected.ip;
        window.open("http://" + ip, '_blank');
    });

    $('#btn-view-traffic').click(function() {
        var action = '/ispbrain/ConnectionsTraffic/view/' + connection_selected.id;
        window.open(action, '_blank');
    });

    $('#btn-view-speed').click(function() {
        var action = '/ispbrain/ConnectionsGraphic/queueHistoryGraph/' + connection_selected.id;
        window.open(action, '_blank');
    });

    $('#btn-view-realTimeGraph').click(function() {
        var action = '/ispbrain/ConnectionsGraphic/queueRealTimeGraph/' + connection_selected.id;
        window.open(action, '_blank');
    });

    $('#btn-delete').click(function() {

        var text = '¿Está Seguro que desea eliminar la Conexión?';
        var controller = 'connections';
        var id = connection_selected.id;

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                    .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/delete/', 'method': 'post', 'id': 'replacer'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer').submit();
            }
        });
    });

    $('#btn-disable-connection').click(function() {

        var text = '¿Está Seguro que desea bloquear está Conexión?';
        var controller = 'connections';
        var id = connection_selected.id;

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                    .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/disable/', 'method': 'post', 'id': 'replacer'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer').submit();
            }
        });
    });

    $('#btn-enable-connection').click(function() {

        var text = "¿Está Seguro que desea habilitar está Conexión?";
        var id  = connection_selected.id;

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                    .append( $('<form/>').attr({'action': '/ispbrain/connections/enable/', 'method': 'post', 'id': 'form-block'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#form-block').submit();
            }
        });
    });

    $(document).click(function() {
        $('[data-toggle="tooltip_etiquetas"]').tooltip('hide');
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
