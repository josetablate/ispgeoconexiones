<style type="text/css">

    tr.locked > td.actions {
        background-color: white;
    }

    .font-size-13px {
        font-size: 13px;
    }

</style>

<div id="btns-tools">

    <div class="pull-right btns-tools margin-bottom-5">

        <?php

           echo $this->Html->link(
                '<span class="fas fa-sync-alt" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Actualizar la tabla',
                'class' => 'btn btn-default btn-update-table ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-eye" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Ocultar o Mostrar Columnas',
                'class' => 'btn btn-default btn-hide-column  ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon fas fa-search" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Buscar por Columnas',
                'class' => 'btn btn-default btn-search-column ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                ['controller' => 'connections', 'action' => 'add'],
                [
                'title' => 'Agregar una nueva conexión',
                'class' => 'btn btn-default btn-add-connection ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export ml-1',
                'escape' => false
            ]);
        ?>

    </div>
</div>

<div class="row">
    <div class="col-xl-12">

        <table id="table-connections-administrative" class="table table-bordered">
            <thead>
                <tr>
                    <th>Creado</th>                          <!--0-->
                    <th>Servicio</th>                        <!--1-->
                    <th>Controlador</th>                     <!--2-->
                    <th>Etiquetas</th>                       <!--3-->
                    <th>Domicilio</th>                       <!--4-->
                    <th>Área</th>                            <!--5-->
                    <th class="text-primary">Código</th>     <!--6-->
                    <th class="text-primary">Nombre</th>     <!--7-->
                    <th class="text-primary">Documento</th>  <!--8-->
                    <th class="text-primary">Teléfono</th>   <!--9-->
                    <th class="text-primary">Correo</th>     <!--10-->
                    <th class="text-primary">Día Venc.</th>  <!--11-->
                    <th>Impagas</th>                         <!--12-->
                    <th class="debt_month">Saldo</th>        <!--13-->
                    <th>Descuento</th>                       <!--14-->
                    <th>Deuda</th>                           <!--15-->
                    <th>Tarjeta Cobro Digital</th>           <!--16-->
                    <th>Débito Automático Cobro Digital</th> <!--17-->
                    <th>Débito Automático Chubut</th>        <!--18-->
                    <th>Tarjeta PayU</th>                    <!--19-->
                    <th>Comentario</th>                      <!--20-->
                    <th>Usuario</th>                         <!--21-->
                    <th>Habilitado</th>                      <!--22-->
                    <th>IP</th>                              <!--23-->
                    <th>Últ. Bloqueo</th>                    <!--24-->
                    <th>Últ. Habilitación</th>               <!--25-->
                </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
                <tr>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-info-administrative',
        'name' =>  'Editar',
        'icon' =>  'icon-eye',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-label',
        'name' =>  'Etiquetas',
        'icon' =>  'fas fa-tag',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-disable-connection',
        'name' =>  'Bloquear',
        'icon' =>  'glyphicon icon-lock',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-enable-connection',
        'name' =>  'Habilitar',
        'icon' =>  'glyphicon icon-unlocked',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-go-customer',
        'name' =>  'Ficha del Cliente',
        'icon' =>  'glyphicon icon-profile',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-info-technical',
        'name' =>  'Información Técnica',
        'icon' =>  'icon-eye',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    $buttons[] = [
        'id'   =>  'btn-observations',
        'name' =>  'Observaciones',
        'icon' =>  'far fa-edit',
        'type' =>  'btn-dark'
    ];

    echo $this->element('actions', ['modal'=> 'modal-connections', 'title' => 'Acciones', 'buttons' => $buttons ]);
    echo $this->element('labels', ['modal'=> 'modal-labels-connections',  'buttons' => $labels, 'save' => true ]);
    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-connections']);
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-connections']);
    echo $this->element('modal_preloader');

    echo $this->element('Observations/add', ['modal' => 'modal-add-observation', 'title' => 'Agregar Observación', 'records' => TRUE]);
    echo $this->element('Observations/edit', ['modal' => 'modal-edit-observation', 'title' => 'Observación']);
?>

<div class="tooltip" role="tooltip">
    <div class="arrow">
    </div>
    <div class="tooltip-inner">
    </div>
</div>

<script type="text/javascript">

    var table_connections = null;
    var connection_selected = null;
    var curr_pos_scroll = null;

    var labels = <?= json_encode($labels) ?>;

    var areas = null;
    var controllers = null;
    var users = null;
    var services = null;

    $(document).ready(function () {

        areas = <?= json_encode($areas) ?>;
        controllers = <?= json_encode($controllers) ?>;
        services = <?= json_encode($services) ?>;
        users = <?= json_encode($users) ?>;

        var class_none = sessionPHP.paraments.gral_config.billing_for_service ? '' : 'd-none';
        var class_cobrodigital = 'd-none';
        var class_payu = 'd-none';
        var class_chubut_auto_debit = 'd-none';
        var search_cobrodigital = '';
        var search_chubut_auto_debit = '';
        var search_payu = '';
        var no_check = [15, 16, 17];

        if (sessionPHP.paraments.gral_config.billing_for_service) {

            class_cobrodigital = sessionPHP.payment_getway.config.cobrodigital.enabled ? '' : 'd-none';
            class_payu = sessionPHP.payment_getway.config.payu.enabled ? '' : 'd-none';
            class_chubut_auto_debit = sessionPHP.payment_getway.config.chubut_auto_debit.enabled ? '' : 'd-none';

            search_cobrodigital = sessionPHP.payment_getway.config.cobrodigital.enabled ? 'options' : '';
            search_payu = sessionPHP.payment_getway.config.payu.enabled ? 'options' : '';
            search_chubut_auto_debit = sessionPHP.payment_getway.config.chubut_auto_debit.enabled ? 'options' : '';

            if (sessionPHP.payment_getway.config.cobrodigital.enabled) {
                // removeItem = 14;
                // no_check = jQuery.grep(no_check, function(value) {
                //   return value != removeItem;
                // });

                removeItem = 15;
                no_check = jQuery.grep(no_check, function(value) {
                  return value != removeItem;
                });
            }

            if (sessionPHP.payment_getway.config.chubut_auto_debit.enabled) {
                removeItem = 16;
                no_check = jQuery.grep(no_check, function(value) {
                  return value != removeItem;
                });
            }

            if (sessionPHP.payment_getway.config.payu.enabled) {
                removeItem = 17;
                no_check = jQuery.grep(no_check, function(value) {
                  return value != removeItem;
                });
            }
        }

        $('.btn-hide-column').click(function() {
           $('.modal-hide-columns-connections').modal('show');
        });

        $('.btn-search-column').click(function() {
           $('.modal-search-columns-connections').modal('show');
        });

        $('#table-connections-administrative').removeClass( 'display' );

		$('#btns-tools').hide();

		loadPreferences('connections-administrative-index', [0, 2, 5, 9, 12, 14, 16, 17]);

		table_connections = $('#table-connections-administrative').DataTable({
		    "order": [[ 0, 'desc' ]],
            "deferRender": true,
            "processing": true,
            "serverSide": true,
		    "ajax": {
                "url": "/ispbrain/connections/get_connections_administrative.json",
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
                "error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "drawCallback": function( settings ) {
                var flag = true;
            	switch (settings.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
            	    if (table_connections) {

                        var tableinfo = table_connections.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }

                        if (curr_pos_scroll) {
                            $(table_connections.settings()[0].nScrollBody).scrollTop( curr_pos_scroll.top );
                            $(table_connections.settings()[0].nScrollBody).scrollLeft( curr_pos_scroll.left );
                        }
                    }
            	}

            	this.api().columns('.debt_month').every(function() {
                    var column = this;

                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    var sum = column
                        .data()
                        .reduce(function (a, b) { 

                           return intVal(a) + intVal(b);
                        }, 0);

                    $(column.footer()).html('$' + sum.toFixed(2));
                });
            },
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "columns": [
                {
                    "data": "created",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                {
                    "data": "service_id",
                    "type": "options",
                    "options": services,
                    "render": function ( data, type, row ) {
                        return data ? services[data] : '';
                    }
                },
                {
                    "data": "controller_id",
                    "type": "options",
                    "options": controllers,
                    "render": function ( data, type, row ) {
                        return data ? controllers[data] : '';
                    }
                },
                { 
                    "type": "options_colors",
                    "options": labels,
                    "render": function ( data, type, row ) {
                       if (row.labels.length > 0) {
                            var spans = "";
                            var names = "";
                            $.each(row.labels, function(i, val) {
                                names += "<span>" + val.text + "</span><br>"; 
                                spans += "<span class='status' style='background-color: " + val.color_back + "' > </span>"; 
                            });
                            return "<div data-toggle='tooltip_etiquetas' title='" + names + "'>" + spans + "</div>";
                        }
                        return '';
                    }
                },
                {
                    "data": "address",
                    "type": "string"
                },
                {
                    "data": "area_id",
                    "type": "options",
                    "options": areas,
                    "render": function ( data, type, row ) {
                        return data ? areas[data] : '';
                    }
                },
                {
                    "data": "customer_code",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                {
                    "data": "customer.name",
                    "type": "string"
                    
                },
                {
                    "data": "customer.ident",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (data) {
                            ident = data;
                        }
                        return sessionPHP.afip_codes.doc_types[row.customer.doc_type] + ' ' + ident;
                    }
                },
                {
                    "data": "customer.phone",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        if (row.customer.phone_alt != null && row.customer.phone_alt != "") {
                            data += ' | ' + row.customer.phone_alt;
                        }
                        return data;
                    }
                },
                {
                    "data": "customer.email",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                {
                    "data": "customer.daydue",
                    "type": "integer",
                },
                {
                    "data": "invoices_no_paid",
                    "type": "decimal",
                    "render": function ( data, type, row ) {

                        if (row.customer.billing_for_service) {

                           if (row.invoices_no_paid > 0) {
                               return "<span class='badge badge-danger font-size-13px' >" + row.invoices_no_paid + "</span>";
                           }
                           return "<span class='badge badge-success font-size-13px'>" + row.invoices_no_paid + "</span>";
                       }
                       return '-';
                    }
                    
                },
                { 
                    "class": "right " + class_none,
                    "data": "debt_month",
                    "type": "decimal",
                    "render": function ( data, type, row ) {

                       if (row.customer.billing_for_service) {
                           return number_format(data, true);
                       }
                       return '-';
                       
                    }
                },
                { 
                    "data": "discount_description",
                    "type": "string",
                    "render": function ( data, type, row ) {

                        if (row.discount_always) {
                            return row.discount_description + ' (siempre)';
                        } else if (row.discount_month > 0) {
                            return row.discount_description + ' (' + row.discount_month + '-' + row.discount_total_month + ')';
                        }
                        return '';
                    }
                },
                {
                    "data": "force_debt",
                    "type": "options",
                    "options": {  
                        "1":"SI",
                        "0":"NO",
                    },
                    "render": function ( data, type, row ) {
                       if (data) {
                           return 'SI';
                       }
                       return 'NO';
                    }
                },
                {
                    "class": "center " + class_cobrodigital,
                    "data": "cobrodigital_card",
                    "type": search_cobrodigital,
                    "options": {  
                        "1":"SI",
                        "0":"NO",
                    },
                    "render": function ( data, type, row ) {
                       if (data) {
                           return 'SI';
                       }
                       return 'NO';
                    }
                },
                {
                    "class": "center " + class_cobrodigital,
                    "data": "cobrodigital_auto_debit",
                    "type": search_cobrodigital,
                    "options": {
                        "1":"SI",
                        "0":"NO",
                    },
                    "render": function ( data, type, row ) {
                       if (data) {
                           return 'SI';
                       }
                       return 'NO';
                    }
                },
                {
                    "class": "center " + class_chubut_auto_debit,
                    "data": "chubut_auto_debit",
                    "type": search_chubut_auto_debit,
                    "options": {  
                        "1":"SI",
                        "0":"NO",
                    },
                    "render": function ( data, type, row ) {
                       if (data) {
                           return 'SI';
                       }
                       return 'NO';
                    }
                },
                {
                    "class": "center " + class_payu,
                    "data": "chubut_auto_debit",
                    "type": search_payu,
                    "options": {  
                        "1":"SI",
                        "0":"NO",
                    },
                    "render": function ( data, type, row ) {
                       if (data) {
                           return 'SI';
                       }
                       return 'NO';
                    }
                },
                {
                    "data": "comments",
                    "type": "string"
                },
                {
                    "data": "user_id",
                    "type": "options",
                    "options": users,
                    "render": function ( data, type, row ) {
                        return data ? users[data] : '';
                    }
                },
                {
                    "data": "enabled",
                    "type": "options",
                    "options": {  
                        "0":"Bloqueados",
                        "1":"Habilitados",
                    },
                    "render": function ( data, type, row ) {
                        var enabled = 'Bloqueado';
                        if (data) {
                            enabled = 'Habilitado';
                        }
                        return enabled;
                    }
                },
                { 
                    "data": "ip",
                    "type": "string"
                },
                {
                    "data": "blocking_date",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                        return "";
                    }
                },
                {
                    "data": "enabling_date",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                        return "";
                    }
                }
            ],
		    "columnDefs": [
		        { "type": 'date-custom', targets: [0] },
		        { "width": '8%', targets: [3, 6, 10, 11, 13] },
                { 
                    "class": "left", targets: [1, 2, 4, 5, 7, 12, 14, 16]
                },
                { 
                    "visible": false, targets: hide_columns
                },
                { "orderable": false, "targets": [3] }
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
                if (!data.enabled) {
                    $(row).addClass('locked');
                }
                if (data.error) {
                    $(row).addClass('error-custom');
                }
            },
		    "language": dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            "dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
		})
		.on('draw.dt', function () {
            $('[data-toggle="tooltip_etiquetas"]').tooltip({html: true});
            $('[data-toggle="tooltip_services"]').tooltip({html: true});
        });

		$('#table-connections-administrative').on( 'init.dt', function () {
             createModalHideColumn(table_connections, '.modal-hide-columns-connections', no_check);
             createModalSearchColumn(table_connections, '.modal-search-columns-connections');
        });

		$('#table-connections-administrative_wrapper .tools').append($('#btns-tools').contents());

        $('#btns-tools').show();

        $('#btn-observations').click(function() {
            customer_code = connection_selected.customer_code;
            $('.modal-connections').modal('hide');
            $('#modal-add-observation').modal('show');
        });
    });

    $('.btn-update-table').click(function() {
         if (table_connections) {
            table_connections.ajax.reload();
        }
    });

    $('#btn-label').click(function() {

        $('.modal-labels-connections .check').addClass('d-none');

        $.each(connection_selected.labels, function(c, label) {

            $('.modal-labels-connections .label-btn').each(function(i, btn) {

                if (label.id == $(this).data('labelid')) {
                    $(this).find('.check').removeClass('d-none');
                }
            });
        });

        $('.modal-connections').modal('hide');
        $('.modal-labels-connections').modal('show');
    });

    $('.modal-labels-connections #label-save').click(function() {

        var send_data = {};
        send_data.connection_id = connection_selected.id; 
        send_data.labels = [];

        $('.modal-labels-connections .label-btn').each(function(a, lala) {
            if (!$(this).find('.check').hasClass('d-none')) {
                send_data.labels.push($(this).data('labelid'));
            }
        });  

        curr_pos_scroll = {
            'top': $(table_connections.settings()[0].nScrollBody).scrollTop(),
            'left': $(table_connections.settings()[0].nScrollBody).scrollLeft()
        };

        $.ajax({
            url: "<?= $this->Url->build(['controller' => 'connections', 'action' => 'changeLabels']) ?>",
            type: 'POST',
            dataType: "json",
            data: JSON.stringify(send_data),
            success: function(data) {

                if (data.connection) {

                    customer_selected = data.connection;

                    table_connections.row( $('#table-connections-administrative tr#' + connection_selected.id) ).data( connection_selected ).draw();
                    $('.modal-labels-connections').modal('hide');
                }
            },
            error: function (jqXHR) {
                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() { 
                        window.location.href = " /ispbrain";
                	}, 3000);

                } else {
                	generateNoty('error', 'Error al intentar cambiar las etiquetas');
                }
            }
        });
    });

    $('#btn-sync').click(function() {
        var action = '/ispbrain/connections/syncConnection/' + connection_selected.id;
        window.open(action, '_self');
    });

    $(".btn-export").click(function() {

        bootbox.confirm('Se descargará un archivo Excel', function(result) {
            if (result) {
                $('#table-connections-administrative').tableExport({tableName: 'Conexiones', type:'excel', escape:'false'});
            }
        }).find("div.modal-content").addClass("confirm-width-md");

    });

    $('#table-connections-administrative tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_connections.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            connection_selected = table_connections.row( this ).data();

            if (connection_selected.enabled == 1) {
                $('btn-enable-connection').hide();
                $('btn-disable-connection').show();
            } else {
                $('btn-disable-connection').hide();
                $('btn-enable-connection').show();
            }

            $('.modal-connections').modal('show');
        }
    });

    $('#btn-info-technical').click(function() {
        var action = '/ispbrain/connections/view/' + connection_selected.id;
        window.open(action, '_blank');
    });

    $('#btn-info-administrative').click(function() {
        var action = '/ispbrain/connections/administrative/' + connection_selected.id;
        window.open(action, '_blank');
    });

    $('#btn-edit').click(function() {
        var action = '/ispbrain/connections/edit/' + connection_selected.id;
        window.open(action, '_blank');
    });

    $('#btn-go-customer').click(function() {
        var action = '/ispbrain/customers/view/' + connection_selected.customer_code;
        window.open(action, '_blank');
    });

    $('#btn-delete').click(function() {

        var text = '¿Está Seguro que desea eliminar la Conexión?';
        var controller = 'connections';
        var id = connection_selected.id;

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                    .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/delete/', 'method': 'post', 'id': 'replacer'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer').submit();
            }
        });
    });

    $('#btn-disable-connection').click(function() {

        var text = '¿Está Seguro que desea bloquear está conexión?';
        var controller = 'connections';
        var id = connection_selected.id;

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                    .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/disable/', 'method': 'post', 'id': 'replacer'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer').submit();
            }
        });
    });

    $('#btn-enable-connection').click(function() {

        var text = "¿Está Seguro que desea habilitar está conexión?";
        var id  = connection_selected.id;

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                    .append( $('<form/>').attr({'action': '/ispbrain/connections/enable/', 'method': 'post', 'id': 'form-block'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#form-block').submit();
            }
        });
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
