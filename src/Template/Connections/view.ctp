<style type="text/css">

    legend {
        display: block;
        width: 100%;
        padding: 0;
        margin-bottom: 20px;
        font-size: 21px;
        line-height: inherit;
        color: #333;
        border: 0;
        border-bottom: 1px solid #e5e5e5;
    }

    .vertical-table {
        width: 100%;
        color: #696868;
    }

    table > tbody > tr {
        border-bottom: 1px solid #e5e5e5;
    }

    #map {
		height: 400px;
	}

</style>

     <div class="row">
        <div class="col">
            <legend class="sub-title"><?= __("Conexión del Cliente: {0} ", $connection->customer->name) ?></legend>
        </div>

        <div class="col">
            <div class="float-right">
             
                <a class="btn btn-default" title="Editar" href="<?= $this->Url->build(["action" => "edit", $connection->id]) ?>" role="button">
                    <span class="glyphicon icon-pencil2" aria-hidden="true"></span>
                </a>
                <?= $this->Html->link(__("<span class='glyphicon icon-bin' title='Eliminar conexión' aria-hidden='true'></span>"), 
                    '#',
                    [
                        'id' => 'btn-delete',
                        'class' => 'btn btn-default', 
                        'role' => 'button',
                        'data-id' => $connection->id,
                        'data-text' =>  __('¿Está Seguro que desea eliminar la conexión del cliente <strong>{0}</strong>?', $connection->customer->name),
                        'escape' => false
                    ]) ?>

                    <?= $this->Html->link(__("<span class='glyphicon icon-unlocked' title='Habilitar conexión' aria-hidden='true'></span>"),
                    '#',
                    ['class' => 'btn btn-default btn-enable-connection', 'data-id' => $connection->id, 'data-text' =>  __('¿Está Seguro que desea habilitar la conexión del Cliente: <strong>{0}</strong>?', $connection->customer->name),'escape' => false]) 
                    ?>
           
                    <?= $this->Html->link(__("<span class='glyphicon icon-lock' title='Bloquear conexión' aria-hidden='true'></span>"),
                    '#',
                    ['class' => 'btn btn-default btn-disable-connection', 'data-id' => $connection->id, 'data-text' =>  __('¿Está Seguro que desea bloquear la conexión del Cliente: <strong>{0}</strong>?', $connection->customer->name),'escape' => false]) 
                    ?>

            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4">

            <legend class="sub-title-sm"><?=  __('Información') ?></legend>
            <table class="vertical-table">
                <tr>
                    <th><?= __('Creado por el Usuario') ?></th>
                    <td class="text-right"><?= h($connection->user->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Controlador') ?></th>
                    <td class="text-right"><?= h($connection->controller->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Servicio') ?></th>
                    <td class="text-right"><?= h($connection->service->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('IP') ?></th>
                    <td class="text-right"><?= $connection->ip ?></td>
                </tr>
                <?php if($connection->secret):?>
                <tr>
                    <th><?= __('Usuario PPPoE') ?></th>
                    <td class="text-right"><b><?= h($connection->secret->name)?></b></td>
                </tr>
                <tr>
                    <th><?= __('Clave PPPoE') ?></th>
                    <td class="text-right"><b><?= h($connection->secret->password)?></b></td>
                </tr>
                <?php endif;?>
                 <tr>
                    <th><?= __('Dirección MAC') ?></th>
                    <td class="text-right"><?= h(strtoupper($connection->mac)) ?></td>
                </tr>
                <tr>
                    <th><?= __('Domicilio') ?></th>
                    <td class="text-right"><?= h($connection->address) ?></td>
                </tr>
                <tr>
                    <th><?= __('Área') ?></th>
                    <td class="text-right"><?= $connection->area->name ?></td>
                </tr>
            
                <tr>
                    <th><?= __('Código del Cliente') ?></th>
                    <td class="text-right"><?= sprintf("%'.05d", $connection->customer_code)  ?></td>
                </tr>
                    
                <tr>
                    <th><?= __('Lat.') ?></th>
                    <td class="text-right"><?= $connection->lat ?></td>
                </tr>
                <tr>
                    <th><?= __('Long.') ?></th>
                    <td class="text-right"><?= $connection->lng ?></td>
                </tr>

                <tr>
                    <th><?= __('Creado') ?></th>
                    <td class="text-right"><?= date('d-m-Y H:i', strtotime($connection->created))?></td>
                </tr>
                <tr>
                    <th><?= __('Modificado') ?></th>
                    <td class="text-right"><?= date('d-m-Y H:i', strtotime($connection->modified))?></td>
                </tr>
             
                <tr>
                    <th><?= __('Habilitado') ?></th>
                    <?php if($connection->enabled):?>
                        <td class="text-right green"><?=  __('Si') ?></td>
                    <?php else:?>
                        <td class="text-right red"><?=  __('No') ?></td>
                    <?php endif;?>
                </tr>
                 <tr>
                    <th><?= __('Clave WiFi') ?></th>
                    <td class="text-right"><?= $connection->clave_wifi ?></td>
                </tr>
                 <tr>
                    <th><?= __('Puertos') ?></th>
                    <td class="text-right"><?= $connection->ports ?></td>
                </tr>
                 <tr>
                    <th><?= __('IP Alternativo') ?></th>
                    <td class="text-right"><?= $connection->ip_alt ?></td>
                </tr>
                 <tr>
                    <th><?= __('Ing. Tráfico') ?></th>
                    <td class="text-right"><?= $connection->ing_traffic ?></td>
                </tr>
                <tr>
                    <th><?= __('Comentario') ?></th>
                    <td class="text-right"><?= $connection->comments ?></td>
                </tr>

            </table>

        </div>

        <div class="col-sm-12 col-md-12 col-lg-8 col-xl-8">
            <?php if ($connection->lat != null && $connection->lng != null): ?>
            <div style="display: -webkit-inline-box;">
                <legend class="sub-title-sm"><?=  __('Ubicación') ?></legend>
                <button id="share" type="button" class="btn btn-primary text-l"><i class="fas fa-share-alt"></i></button>
            </div>
            <div id="map"></div>
            <?php endif; ?>
        </div>

    </div>

</div>    

<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2K3zoK46JybWPzQbhfQQqIIbdZ_3WcQs">
</script>

<script type="text/javascript">

    var connection = <?= json_encode($connection) ?>;

    $('#btn-delete').click(function() {

        var text = $(this).data('text');
        var id = $(this).data('id');

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/connections/delete/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#replacer').submit();
            }
        });
    });

    $(document).on("click", ".btn-disable-connection", function(e) {

        var text = $(this).data('text');
        var user_id  = $(this).data('id');
        var url = '<?= $this->Url->build(["controller" => "connections", "action" => "disable"]); ?>';

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                    .append( $('<form/>').attr({'action': url, 'method': 'post', 'id': 'form-block'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': user_id}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#form-block').submit();
            }
        });

    });

    $(document).on("click", ".btn-enable-connection", function(e) {

        var text = $(this).data('text');
        var user_id  = $(this).data('id');
        var url = '<?= $this->Url->build(["controller" => "connections", "action" => "enable"]); ?>';

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                    .append( $('<form/>').attr({'action': url, 'method': 'post', 'id': 'form-block'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': user_id}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#form-block').submit();
            }
        });

    });

    $(document).ready(function () {
        if (connection.lat != null && connection.lng != null) {
            initMap();
        }
    });

    var map = null;
    var geocoder = null;
    var marker = null;

	function initMap() {

		geocoder = new google.maps.Geocoder;

		var Resistencia = {lat: -27.451348, lng: -58.986807};

		var mapOptions = {
			zoom: 15,
			center: Resistencia	    
		};

		map = new google.maps.Map(document.getElementById('map'), mapOptions);
		
		var init = {
            lat: '<?php echo $connection->lat; ?>',
            lng: '<?php echo $connection->lng; ?>'
        };
        
        var latLng = new google.maps.LatLng(init.lat, init.lng);
        
        addMarker(latLng);
        map.setCenter(latLng);
	}
	
	function addMarker(location) {

		marker = new google.maps.Marker({
			position: location,
			map: map
		});
		marker.setMap(map);	
        var lat = location.lat();  
        var lng = location.lng();
	}	

</script>

<script>

    'use strict';

    function sleep(delay) {
        return new Promise(resolve => {
            setTimeout(resolve, delay);
        });
    }

    function logText(message, isError) {
        if (isError)
            console.error(message);
        else
            console.log(message);
            const p = document.createElement('p');
        if (isError) {
            generateNoty('warning', message);
        } else {
            generateNoty('success', message);
        }
    }

    function logError(message) {
        logText(message, true);
    }

    function checkboxChanged(e) {
        const checkbox = e.target;
        const textfield = document.querySelector('#' + checkbox.id.split('_')[0]);
        textfield.disabled = !checkbox.checked;
        if (!checkbox.checked)
            textfield.value = '';
    }

    async function testWebShare() {
        if (navigator.share === undefined) {
            logError('Error: característica no soportada');
            return;
        }

        var title = "Compartir ubicación";
        var customer = connection.customer;
        var customer_name = customer.name + ' - Cód.: ' + customer.code + ' - Dom.: ' + customer.address; 
        var text = "Cliente: " + customer_name + '\n' + "Conexión: " + connection.service.name + ' - Dom.: ' + connection.address;
        var lat = connection.customer.lat;
        var lng = connection.customer.lng;
        var url = "https://www.google.com/maps/search/?api=1&query=" + lat + "," + lng;

        try {
            
            await navigator.share({title, text, url});
        } catch (error) {
            logError('Error al compartir: ' + error);
            return;
        }
        logText('Compartido correctamente');
    }

    async function testWebShareDelay() {
        await sleep(2000);
        testWebShare();
    }

    function onLoad() {
        if (document.querySelector('#share')) {
            document.querySelector('#share').addEventListener('click', testWebShare);
        }
    }

    window.addEventListener('load', onLoad);

  </script>
