<style type="text/css">

    .payment-method-info-background {
        background: rgba(0, 0, 0, .03);
        border-radius: 5px;
    }

    .payment-method-title {
        font-size: 18px;
    }

    .my-hidden {
        display: none;
    }

</style>

<div class="row pb-1">

    <div class="col-xl-6">

        <div class="card border-success ">

            <div class="card-header border-success p-2">

                <div class="row">
                    <div class="col-auto">
                        <h5 class="card-title">Cliente</h5>
                    </div>
                </div>

            </div>

            <div class="card-body p-2">

                <div class="row justify-content-between">
                    <div class="col-xl-6">
                        <h6>Código <span class="badge badge-secondary"><?= str_pad($connection->customer->code, 5, "0", STR_PAD_LEFT) ?></span></h6>
                    </div>
                    <div class="col-xl-6 text-right">
                        <h6>Documento <span class="badge badge-secondary"><?= $doc_types[$connection->customer->doc_type] . ' ' . ($connection->customer->ident ? $connection->customer->ident : '') ?></span></h6>
                    </div>
                    <div class="col-12">
                        <h6>Nombre <span class="badge badge-secondary"><?= $connection->customer->name ?></span></h6>
                    </div>
                </div>
           
            </div>
        </div>
    </div>

    <div class="col-xl-6">

        <div class="card border-info ">

            <div class="card-header border-info   p-2">

                <div class="row">
                    <div class="col-auto">
                        <h5 class="card-title">Conexión</h5>
                    </div>
                </div>

            </div>

            <div class="card-body p-2">

                <div class="row justify-content-between">
                    <div class="col-xl-6">
                        <h6>Controlador <span class="badge badge-secondary"><?= $connection->controller->name ?></span></h6>
                    </div>
                    <div class="col-xl-6 text-right">
                        <h6>Servicio <span class="badge badge-secondary"><?= $connection->service->name ?></span></h6>
                    </div>
                    <div class="col-12">
                         <h6>Domicilio <span class="badge badge-secondary"><?= $connection->address ?></span></h6>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-xl-12">

        <div class="card">

            <div class="card-header  p-2">

                <div class="row">
                    <div class="col-auto">
                        <h5 class="card-title">Editar</h5>
                    </div>
                </div>

            </div>

            <div class="card-body p-2">

                <div class="row">

                    <div class="col-xl-12">

                        <?= $this->Form->create($connection) ?>

                            </fieldset>

                            <div class="row">    

                                <div class="col-xl-12 pl-4">

                                    <?= $this->Form->input('force_debt', [
                                        'type'  => 'checkbox',
                                        'label' => 'Generar deuda'
                                    ])?>

                                </div>

                                <div class="col-xl-8">

                                    <div class="card">
                                        <div class="card-body">

                                            <h5 class="card-title border-bottom">Descuento</h5>

                                            <div class="row">

                                                <div class="col-xl-4">
                                                    <?=$this->Form->input('discount_code', [
                                                        'type'      => 'text',
                                                        'label'     => 'Código', 
                                                        'maxlength' => 45
                                                    ]); ?>
                                                </div>

                                                <div class="col-xl-8">
                                                    <?=$this->Form->input('discount_description', [
                                                        'type'  => 'text',
                                                        'label' => 'Descripción',
                                                    ]); ?>
                                                </div>

                                                <div class="col-xl-12 pl-4">

                                                    <?=$this->Form->input('discount_always', [
                                                        'type'  => 'checkbox',
                                                        'label' => 'Descuento por siempre'
                                                    ]); ?>

                                                </div>
     
                                                <div class="col-xl-3">
                                                    <?=$this->Form->input('discount_total_month', [
                                                        'type'  => 'text',
                                                        'label' => 'Total de meses',
                                                    ]); ?>
                                                </div>
                                                <div class="col-xl-3">
                                                    <?=$this->Form->input('discount_month', [
                                                        'type'  => 'text',
                                                        'label' => 'Meses restantes',
                                                        'min'   => 0,
                                                    ]); ?>
                                                </div>
                                                <div class="col-xl-3">
                                                    <?= $this->Form->input('discount_value', [
                                                        'type'  => 'number',
                                                        'label' => 'Porcentaje',
                                                        'min'   => 0.00,
                                                        'max'   => 1,
                                                        'step'  => 0.01,
                                                    ]); ?>
                                                </div>
                                                <div class="col-xl-3">
                                                    <?= $this->Form->input('discount_alicuot', [
                                                        'options' => $alicuotas_types,
                                                        'label' => 'Alícuota'
                                                    ]); ?>
                                                </div>

                                                <div class="col-xl-12">
                                                    <?= $this->Form->input('new_discount_id', [ 
                                                        'label'    => [
                                                            'class'  => 'thingy',
                                                            'text'   => 'Aplicar un nuevo descuento <i class="fas fa-exclamation-triangle text-warning" title="La selección de un nuevo descuento sobrescribe los valores anteriores" data-toggle="tooltip"></i>',
                                                            'escape' => false,
                                                        ],
                                                        'type'     => 'select', 
                                                        'empty'    => 'Seleccionar descuento', 
                                                        'options'  => $discounts_list
                                                    ]); ?>
                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                </div>

                                <?php if ($paraments->gral_config->billing_for_service): ?>
                                <div class="col-xl-4">

                                    <div class="card">
                                        <div class="card-body">

                                            <h5 class="card-title border-bottom">Pasarela de Pagos: </h5>
                                            <p id="passwordHelpBlock" class="form-text text-muted">
                                                Tildar: para Recibir Pagos desde la Pasarela de Pagos, así como la posibilidad de Generar Deudas.
                                            </p>
                                            <?php if (sizeof($connection->customer->customers_accounts) > 0): ?>
                                            <?php foreach ($connection->customer->customers_accounts as $customer_account): ?>
                                                <?php if (!$customer_account->deleted): ?>
                                                    <?php foreach ($payment_getway->methods as $key => $pg): ?>
                                                        <?php if ($pg->id == $customer_account->payment_getway_id): ?>
                                                            <div class="row mt-3">
                                                                <div class="col-xl-12">
                                                                    <?php
                                                                        echo $this->Form->input($key, ['label' => $pg->lbl_debt, 'type' => 'checkbox', 'checked' => $connection->$key]);
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                            <?php else: ?>
                                                <label>Sin Cuentas.</label>
                                            <?php endif; ?>

                                        </div>
                                    </div>

                                </div>
                                <?php endif; ?>

                            </div>

                            </fieldset>

                            <?= $this->Html->link(__('Cancelar'), ["controller" => "Customers", "action" => "view", $connection->customer_code], [  'class' => 'btn btn-default mt-2']) ?>
                            <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success mt-2 float-right' ]) ?>

                        <?= $this->Form->end() ?>

                    </div>

                </div>

            </div>

        </div>
    </div>
</div>

<script type="text/javascript">

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    $(document).click(function() {
         $('[data-toggle="tooltip"]').tooltip('hide');
    });

    var connection = null;

    $(document).ready(function() {

        connection = <?= json_encode($connection) ?>;

        $('#discount-always').change(function() {

            if ($(this).is(':checked')) {

               $('#discount-total-month').closest('div').hide();
               $('#discount-month').closest('div').hide();
            } else {

               $('#discount-total-month').closest('div').show();
               $('#discount-month').closest('div').show();
            }
        });

        if ($('#discount-always').is(':checked')) {

            $('#discount-total-month').closest('div').hide();
            $('#discount-month').closest('div').hide();
        } else {
            $('#discount-total-month').closest('div').show();
            $('#discount-month').closest('div').show();
        }

    });

</script>
