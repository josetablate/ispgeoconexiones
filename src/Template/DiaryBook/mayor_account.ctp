<style type="text/css">
   
    #table-seatings-detail{
        width: 100% !important;
    }
        
    #table-seatings-detail td{
        margin: 0px 0px 0px 0px !important;
        padding-bottom:  0px !important;
        padding-top:  0px !important;
        vertical-align: middle;
    }
    
    .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
        background-color: #d2d2d2;
    }
    
    tr.group, tr.group:hover {
        background-color: #ddd !important;
        font-weight: bold;
        color: #696969;
    }
    
    td.left{
        text-align: left !important;
        padding-left: 5px !important;
    }
    
    td.right{
        text-align: right !important;
        padding-right: 5px !important;
    }
    
    td.total{
        font-weight: bold;
    }
    
    
    .container-fluid {
   
        width: 100% !important;
        left: 0 !important;

    }
    
     tr{
         font-stretch: condensed;
    }   

    
</style>
        
    <div class="row">
        
        <div class="col-sm-12">
            
            <legend class="sub-title">Mayor <?= ($data['account_code'] != '') ? __(' Cuenta: {0} {1}', $data['account_code'], $data['name']) : ''?></legend>
            
            <table class="table table-bordered" id="table-seatings-detail">
                <thead>
                    <tr>
                        <th>Fecha</th>
                        <th>Asiento</th>
                        <th>Concepto</th>
                        <th>DEBE</th>
                        <th>HABER</th>
                        <th>SALDO</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <?php if ($seatingsDetails): ?>
                    
                    <?php 
                    
                        $saldo = 0;  
                        foreach($sDSaldoAnterior as $value){
                            $saldo += $value['debe'];
                            $saldo -= $value['haber'];
                        }
                    ?>
                    
                    <?php 
                    
                    $count = count($seatingsDetails);
                    $counter = 0;
                    
                    foreach ($seatingsDetails as $seatingDetail): ?>
                    
                    <?php
                    
                        $counter++;
                        
                        $saldo += $seatingDetail['debe'];
                        $saldo -= $seatingDetail['haber'];
                    
                    ?>
                 
                        <tr data-id="<?=$seatingDetail['number']?>">
                            
                            <td><?= date('d/m/Y', strtotime($seatingDetail['created']))?></td>
                            <td><?= sprintf("%'.05d", $seatingDetail['number']) ?></td>
                            <td class='left' ><?= $seatingDetail['concept'] ?></td>
                            <td class='right' >&nbsp;&nbsp;<?= number_format($seatingDetail['debe'],2,',','.') ?></td>
                            <td class='right' >&nbsp;&nbsp;<?= number_format($seatingDetail['haber'],2,',','.') ?></td>
                            <td class='right <?=($count == $counter) ? 'total' : ''?> ' > &nbsp;&nbsp;<?= number_format($saldo,2,',','.') ?></td>
                        </tr>
                    <?php endforeach; ?>
                    
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<script type="text/javascript">

    var table_seatings_detail = null;
   
    $(document).ready(function () {
        
        $('.header-title').hide();
        $( '#menu' ).hide();
      
        $('#table-seatings-detail').removeClass('display');
		
		table_seatings_detail = $('#table-seatings-detail').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '420px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "ordering": false,
		    "columnDefs": [
                 { "type": "date-custom", "targets": [0] },
                 { "type": "numeric-comma", "targets": [3,4,5] },
             ],
		    "language": {
                "decimal":        "",
                "emptyTable":     "Sin resultados.",
                "info":           "_START_ al _END_ de _TOTAL_",
                "infoEmpty":      "0 al 0 de 0",
                "infoFiltered":   "",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ filas",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "Sin resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguinte",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                    "sortDescending": ": Activar para ordenar la columna descendente"
                }
            },
            "lengthMenu": [[50, 100, -1], [50, 100, "Todas"]],
            /*"dom": '<"toolbar">frtip'*/
		});
		
		table_seatings_detail.page( 'last' ).draw( 'page' );
		
	    var $scrollBody = $(table_seatings_detail.table().node()).parent();
        $scrollBody.scrollTop($scrollBody.get(0).scrollHeight);
        
    });
    
    
</script>

