<style type="text/css">
    
    tr.group, tr.group:hover {
        background-color: #ddd;
        color: #696969;
    }
    
    tr.manual, tr.manual:hover {
        background-color: #89e9ff !important;
        color: #696969;
    }
    
    .modal {
        background-color: rgba(127, 127, 127, 0.5) !important;
    }
    
     .error {
        color: red !important;
    }

</style>

<script type="text/javascript">

    var account_code_selected = '';
    var seating_accounts_selected = [];
    
    function remove_row(row_nro) {
        
        var codeRemove =  $('#row-' + row_nro).find('.code').val();
        
        $('#row-' + row_nro).remove();
        
        if (codeRemove.length > 0) {
            
            var exist = false;
            
             $.each($('#table-seating-data-body tr'), function(i, val) {
                
                var code = $(this).find('.code').val();
                
                if (code.length > 0 && codeRemove == code) {
                   exist = true;
                }
            });
            
            if (!exist) {
                seating_accounts_selected.splice(codeRemove, 1);
            }
        }
        
        update();
        
    }
    
    var row_selected = null;
    
    function search_account(row_nro) {
        
        row_selected = row_nro;
        $('.modal-search-account').modal('show');
    }
    
    function open_search(event, row_nro) {
     
        if (event.keyCode == 66) { //b
            
            row_selected = row_nro;
            $('.modal-search-account').modal('show');
            
        }
        
        if (event.keyCode == 77) { //m
            
            row_selected = row_nro;
            account_code_selected = $('input#account-'+row_selected).val();
            
            if (account_code_selected.length > 0) {
                var url = "/ispbrain/DiaryBook/mayorAccount/" + account_code_selected;
                window.open(url, 'Mayor - Cuenta: ' +account_code_selected , "width=600,height=500");
            }
        }
    }
    
    function isFloat(n) {
       return ((typeof n==='number') && (n%1!==0));
    }
    
    function update() {
           
        $.each($('#table-seating-data-body tr'), function(i, val) {
            
            var code = $(this).find('.code').val();
            
            if (code.length > 0) {
                seating_accounts_selected['c' + code].saldo_row = parseFloat(seating_accounts_selected['c' + code].saldo);
            }
        });
         
        var total_debe = 0;
        var total_haber = 0;
        
        $.each($('#table-seating-data-body tr'), function(i, val) {
            
            var code = $(this).find('.code').val();
            var debe = $(this).find('.debe').val();
            var haber = $(this).find('.haber').val();
            
            if (code.length > 0) {
        
                if (debe != '') {
                    debe = parseFloat(debe);
                    seating_accounts_selected['c'+code].saldo_row += debe;
                    total_debe += debe;
                }
                
                if (haber != '') {
                    haber = parseFloat(haber);
                    seating_accounts_selected['c'+code].saldo_row -= haber;
                    total_haber += haber;
                }
                
                if (isFloat(seating_accounts_selected['c'+code].saldo_row)) {
                    $(this).find('.saldo').val(seating_accounts_selected['c'+code].saldo_row.toFixed(2));
                } else {
                    $(this).find('.saldo').val(seating_accounts_selected['c'+code].saldo_row);
                }
                 
            }
            
        });

        total_debe  = total_debe.toFixed(2);
        total_haber = total_haber.toFixed(2);

      
        $('#total-debe').html(total_debe);
        $('#total-haber').html(total_haber);
        
        if (total_debe != total_haber) {
            $('#total-debe').addClass('error');
            $('#total-haber').addClass('error');
        } else {
            $('#total-debe').removeClass('error');
            $('#total-haber').removeClass('error');
        }
        
    } 
    
    function keyup_debe(row) {
        
        if (parseFloat($('#debe-'+row).val()) > 0) {
            $('#haber-'+row).val('');
        }
        
         update();
    }
    
    function keyup_haber(e, row) {
     
        if (parseFloat($('#haber-'+row).val()) > 0) {
            $('#debe-' + row).val('');
        }
        
         update();
    }
    
    var key_tab_pessed = false;
    
    $(document).keydown(function(e) {
        key_tab_pessed = (e.keyCode == 9) ? true : false;
    });
    
    function focusout_account(row) {
        
        row_selected = row;
        
        var code = $('#account-'+row).val();

        $.ajax({
            url: "<?= $this->Url->build(['controller' => 'Accounts', 'action' => 'getAccountByCodeAjax']) ?>" ,
            type: 'POST',
            dataType: "json",
            data: JSON.stringify({ code: code}),
            success: function(data){

                if (data.account != false) {
                   
                    $('#account-name-' + row).val('');
                
                    account_selected = data.account;
                    $('#card-accounts-seeker').trigger('ACCOUNT_SELECTED');
                    
                } else {
                     console.log('error - No se  puedo encontrar la cuenta '+code);
                }
            },
            error: function (jqXHR) {
                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() { 
                	  window.location.href = "/ispbrain";
                	}, 3000);

                } else {
                	generateNoty('error - No se  puedo encontrar la cuenta ' + code);
                }
            }
        });
    }
    
    function focusout_debe( row) {
        
    }
    
    function focusout_haber( row) {
      
        if (key_tab_pessed && row == count_rows) { //tab
            $('#btn-add-row').click();
            key_tab_pessed = false;
            
            $('#account-'+count_rows).focus();
        }
    }
  
</script>

<div class="row">
    
    <div class="col-xl-4 pr-0">

        <div class="card border-secondary mb-3">
            <h5 class="card-header">Filtros</h5>
            <div class="card-body text-secondary p-2">
                
                 <?php echo $this->Form->create(null, ['id' => 'form-filter']); ?>
        
                    <div class="row mb-2">
                        <div class="col-xl-6 pr-0">
                            <label  class="control-label">Desde</label> 
                            <div class='input-group date' id='from-datetimepicker'>
                                <input name='from' id="from" type='text' class="form-control" required />
                                <span class="input-group-addon input-group-text calendar">
                                    <span class="glyphicon icon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-xl-6 pl-0">
                            <label  class="control-label">Hasta</label> 
                            <div class='input-group date' id='to-datetimepicker'>
                                <input name='to' id="to" type='text' class="form-control" required />
                                <span class="input-group-addon input-group-text calendar">
                                    <span class="glyphicon icon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    
                    <?php echo $this->Form->input('number', ['type' => 'number', 'label' => 'Nº Asiento', 'min' => 0]); ?>
                    <?php echo $this->Form->input('concept', ['type' => 'text', 'label' => 'Concepto']); ?>
                  
                    <label class="control-label" for="account-code">Nº Cuenta</label>
                    <div class="row">
                        <div class="col-6">
                             <?php echo $this->Form->input('account_code_parent', ['type' => 'number', 'label' => false, 'min' => 0, 'max' => 9999]); ?>
                        </div>
                        <div class="col-6">
                            <?php echo $this->Form->input('account_code_child', ['type' => 'number', 'label' => false, 'min' => 0, 'max' => 99999]); ?>
                        </div>
                    </div>
                    
                    <?php echo $this->Form->input('account_name', ['type' => 'text', 'label' => 'Nombre Cuenta']); ?>
                    
                    <?=$this->Form->input('manual', ['type' => 'checkbox', 'label' => 'Manuales', 'checked' => false]) ?>
                  
                    <?= $this->Form->button(__('Limpiar'), ['class' => 'btn-default', 'type' => 'button', 'id' => 'btn-clear-filter']) ?>
                    <?= $this->Form->button(__('Buscar'), ['class' => 'btn-success float-right', 'id' => 'btn-search']) ?>

                <?php echo $this->Form->end(); ?>
                
            </div>
        </div>
    </div>
    
    
    <div class="col-xl-8 pl-2">

        <div class="card border-secondary mb-3">
            
            <div class="card-header w-100 pb-1 pt-2">
                 <div class="row">
                    <div class="col-4">
                        <h5> Asientos </h5>
                    </div>
                    <div class="col-8">
                        <?php
                 
                        echo $this->Html->link(
                            '<span class="glyphicon icon-file-pdf"  aria-hidden="true"></span>',
                            'javascript:void(0)',
                            [
                                'title' => 'Exportar en formato PDF el contenido de la tabla',
                                'class' => 'btn btn-default btn-export-pdf ml-1 float-right',
                                'escape' => false
                            ]);
                            
                        echo $this->Html->link(
                            '<span class="glyphicon icon-file-excel"  aria-hidden="true"></span>',
                            'javascript:void(0)',
                            [
                                'title' => 'Exportar tabla',
                                'class' => 'btn btn-default btn-export-excel ml-1 float-right',
                                'escape' => false
                            ]);
                            
                            
                       echo $this->Html->link(
                            '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                            'javascript:void(0)',
                            [
                                    'id' => 'btn-add-seating',
                                    'title' => 'Agregar nuevo asiento"',
                                    'class' => 'btn btn-default btn-hide-column float-right',
                                    'escape' => false
                            ]);
                        
                        ?>
                    </div>
                </div>
            </div>
            
            <div class="card-body text-secondary p-2">
                
                <table class="table table-bordered" id="table-seatings-detail">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Asiento</th>
                            <th>Concepto</th>
                            <th>Cuenta</th>
                            <th>Nombre de Cuenta</th>
                            <th>DEBE</th>
                            <th>HABER</th>
                            <th>Comentario</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                       
                    </tbody>
                </table>
                    
            </div>
        </div>
    </div>
    
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-edit',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete',
        'name' =>  'Eliminar',
        'icon' =>  'fa fa-times',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-seating', 'title' => 'Acciones', 'buttons' => $buttons ]);

?>

<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel">Editar Comentario</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            
        </div>
        <div class="modal-body">
            <div class="row">
                 <div class="col-md-12">
            
                    <form method="post" accept-charset="utf-8" role="form" action="<?=$this->Url->build(["controller" => "Seating", "action" => "edit"])?>">
                        <div style="display:none;">
                            <input type="hidden" name="_method" value="POST">
                        </div>
                       
                        <div class="form-group text">
                            <label class="control-label" for="name">Comentario</label>
                           <textarea class="form-control" name="comments" id="comments" rows="2"></textarea>
                        </div>
                      
                        <input type="hidden" name="id" id="id"/>
                        <?php echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]); ?>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success float-right" id="btn-confirm-edit ">Guardar</button> 
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
 
<div id="modal-add-seating" class="modal bs-example-modal-sm" tabindex="-1" role="dialog" data-backdrop="false" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header p-2">
            <h4 class="modal-title">Fecha: &nbsp;<?=date('d/m/Y', time())?>  &nbsp; &nbsp;</h4>
            <h4 class="modal-title">Crear Asiento</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body p-2">
       
            <div class="row">
                
                <div class="col-xl-12">
                    
                    <form method="post" accept-charset="utf-8" role="form" class="mb-0" action="<?=$this->Url->build(['controller' => 'DiaryBook',  "action" => "add"])?>">
                        <div style="display:none;">
                            <input type="hidden" name="_method" value="POST">
                        </div>
                        
                        <div class="row">
                            <div class="col-xl-6">
                                <label class="control-label" for="">Concepto</label>
                                <?php echo $this->Form->text('concept', ['required' => true, 'autocomplete' => 'off']);?>
                            </div>
                            <div class="col-xl-6">
                                 <label class="control-label" for="">Comentarios</label>
                                 <?php echo $this->Form->text('comments', ['required' => false, 'autocomplete' => 'off']);?>
                            </div>
                        </div>
                        
                        <?php echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]); ?>
                        
                        <hr class="custom">
                    
                        <div class="row">
                            <div class="col-xl-12">
                                
                                <table id="table-seating-data">
                                    <thead>
                                        <tr>
                                            <th>N° Cuenta</th>
                                            <th></th>
                                            <th></th>
                                            <th>Debe</th>
                                            <th>Haber</th>
                                            <th style="display: none;">Saldo</th>
                                            <th>Saldo</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-seating-data-body" >
                                      
                                    </tbody>
                                </table>
                        
                                <table style="width:94.6%; background-color: #fff0c1;">
                                    <tr>
                                        <td style="width:735px">
                                            <label for="control-label" class="" style="padding-top: -1px;margin-bottom: 1px;margin-left: 7px;">TOTAL</label></td>
                                        <td id="total-debe"  style="width:270px; text-align:right; color:green; font-weight: bold; ">0</td>
                                        <td id="total-haber" style="width:174px; text-align:right;  color:green; font-weight: bold; ">0</td>
                                        <td style="width:300px"><input style="display: none;" type="text"/></td>
                                    </tr>
                                </table>
                               
                                <button type="button" class="btn btn-default mt-1"  id="btn-add-row" >
                                    <span class="glyphicon icon-plus"  aria-hidden="true" ></span> Agregar Fila
                                </button>
                            </div>
                        </div>
                        
                        <div class="row mt-3">
                            <div class="col-xl-12">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button id="btn-search" type="submit"  class="btn btn-success float-right"> Agregar</button>
                            </div>
                        </div>
                    
                    </form>
                </div>
            </div>
            <br>
        </div>
    </div>
  </div>
</div>

<div class="modal fade modal-search-account" tabindex="-1" role="dialog"  data-backdrop="false" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
            <div class="col-xl-12">
                <?=$this->element('Accountant/accounts_seeker')?>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
    echo $this->element('modal_preloader');
?>

<script type="text/javascript">

    var seating_number_preseleted = <?= json_encode($seating_number_preseleted) ?>;
    
    var action_preselected = <?= json_encode($action_preselected) ?>;

    $('#from-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'DD/MM/YYYY',
    });

    $('#to-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'DD/MM/YYYY',
    });

    var table_seating_data = null;
    var count_rows = 0;

    $('#btn-add-seating').click(function(){
    
        $('.modal-actions').modal('hide');
        $('#modal-add-seating').modal('show');
    });
    
    $('#card-accounts-seeker').on('ACCOUNT_SELECTED', function() {
        
        $('.modal-search-account').modal('hide');
      
        if (row_selected != null && account_selected) {
            
            if (!seating_accounts_selected.hasOwnProperty('c' + account_selected.code)) {
                
                account_selected.saldo_row = parseFloat(account_selected.saldo);
                account_selected.debe = 0.00;
                account_selected.haber = 0.00;
                
                seating_accounts_selected['c'+account_selected.code] = account_selected; 
            }
            
            $('#account-'+row_selected).val(account_selected.code);
            
            $('#account-name-'+row_selected).val(account_selected.name.split('&nbsp;').join('').split('&nbsp').join(''));
            $('#debe-' + row_selected).val(0.00);
            $('#haber-' + row_selected).val(0.00);
            $('#account-saldo-' + row_selected).val(account_selected.saldo_row);
            
            update();
       
        }
       
    });
    
    $('#card-accounts-seeker').on('ACCOUNT_CANCEL', function() {
        
        $('.modal-search-account').modal('hide');
       
    });

    $(document).ready(function() {
        
        console.log(seating_number_preseleted);
        console.log(action_preselected);
        
        
      
      //para agregar filas en el creador de asientos
        $('#btn-add-row').click(function() {
            
            count_rows++;
            
            var row_html= " <tr id='row-" + count_rows + "'>";
            row_html += "       <td><input autocomplete='off' type='text' maxlength='9' style='width:100px;' name='account[]' onfocusout='focusout_account("+count_rows+")' onkeyup='open_search(event,"+count_rows+");' id='account-"+count_rows+"' class='code' required></td>";
            row_html += "       <td><span style='margin: 0 2px;' class='glyphicon icon-search' onclick='search_account("+count_rows+");'  aria-hidden='true' ></span></td>";
            row_html += "       <td><input style='width:300px;' type='text' id='account-name-"+count_rows+"' readonly /></td>";
            row_html += "       <td><input style='width:100px; margin: 0 2px;' type='number' onfocusout='focusout_debe("+count_rows+")' onkeyup='keyup_debe("+count_rows+");' id='debe-"+count_rows+"' class='debe' name='debe[]'  value='0' min='0' step='0.01'/></td>";
            row_html += "       <td><input style='width:100px; margin: 0 2px;' type='number' onfocusout='focusout_haber("+count_rows+")' onkeyup='keyup_haber(event,"+count_rows+");' id='haber-"+count_rows+"' class='haber' name='haber[]' value='0' min='0' step='0.01'/></td>";
            row_html += "       <td><input style='width:100px;' type='number' id='account-saldo-"+count_rows+"' class='saldo' readonly /></td>";
            row_html += "       <td><span style='margin: 0 2px;' class='glyphicon icon-bin' onclick='remove_row("+count_rows+");'  aria-hidden='true' ></span></td>";
            row_html += "   </tr>";
            
            $('#table-seating-data-body').append(row_html);
            
        });
        
        $('#modal-add-seating').submit(function(e) {
           
            var total_debe = parseFloat($('#total-debe').html());
            var total_haber = parseFloat($('#total-haber').html());
            
            if (total_debe != total_haber) {
                generateNoty('warning', 'El asiento esta desbalanceado.');
                e.preventDefault();
                return false;
            } else {
                return true;
            }
        })
        
        var seating_amount = 0;
       
        table_seatings_detail = $('#table-seatings-detail').DataTable({
            "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/DiaryBook/get_diary_by_filter.json",
                "dataSrc": "seatingsDetails",
                "data": function ( d ) {
                  return $.extend( {}, d, {
                    "filter_data": $('#form-filter').serializeArray()
                  } );
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
            				  window.location.href ="/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "ordering": false,
		    "scrollY": true,
		    "scrollY": '383px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": false,
		    "searching": false,
		    "columns": [
                { 
                    "data": "seating.created",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var date = data.split('T')[0];
                            date = date.split('-');
                            return date[2] + '/' + date[1] + '/' + date[0]
                        }
                    }
                },
                { 
                    "data": "seating.number",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                { 
                    "data": "seating.concept"
                },
                { 
                    "data": "account_code"
                },
                { 
                    "data": "account.name"
                },
                { 
                    "data": "debe",
                    "render": function ( data, type, row ) {
                        return number_format(data);
                    }
                },
                { 
                    "data": "haber",
                    "render": function ( data, type, row ) {
                        return number_format(data);
                    }
                },
                { 
                    "data": "seating.comments"
                },
                { 
                    "data": "seating.number"
                }
            ],
		    "columnDefs": [
		        { "type": 'date-custom', targets: [0] },
		        { "type": 'numeric-comma', targets: [5, 6] },
		        { 
                    "width": "12%", targets: [3]
                },
                { 
                    "class": "left", targets: [2, 4, 7]
                },
                { 
                    "class": "right", targets: [5, 6]
                },
                { 
                    "visible": false, targets: [0, 1, 2, 7, 8]
                },
            ],
            "drawCallback": function ( settings ) {
                
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last=null;
                
                seating_amount = 0;
     
                api.column(1, {page:'current'} ).data().each( function ( number, i ) {
                    
                    if ( last !== number ) {
                        
                        var created = api.rows( i ).data()[0].seating.created;
                        
                        created = created.split('T')[0].split('-');
                        created = created[2]+'/'+created[1]+'/'+created[0];
                        
                        var concept = api.rows( i ).data()[0].seating.concept;
                        var comments = api.rows( i ).data()[0].seating.comments;
                        
                        var type = api.rows( i ).data()[0].seating.type;
                        
                        var bg_class =  'group';
                        
                        if (type == 'm') {
                            bg_class += ' manual';
                        } 
                        
                        $(rows).eq( i ).before(
                            '<tr class="font-weight-bold '+bg_class+'" data-id="'+number+'"  data-comments="'+comments+'" ><td colspan="7" class="left">'+created+'&nbsp;&nbsp;&nbsp;&nbsp;'+pad(number,5)+'&nbsp;&nbsp;&nbsp;&nbsp;'+concept+ '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="comment-text">'+comments+'</span></td></tr>'
                        );
                        seating_amount++;
                        last = number;
                    }
                });
                
                $('#table-seatings-detail_info').html('Total de Asientos: ' + seating_amount);
                
                closeModalPreloader();
            },
            "initComplete": function(settings, json) {
                 autoScroll();
                 
            },
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
        	"dom":
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-12'i>>",
   
		});

        var seating_id = null;
        var seating_comments = null;
        
        $('#table-seatings-detail tbody').on( 'click', 'tr.group', function () {
        
            seating_id = $(this).data('id');
            seating_comments = $(this).data('comments');
            
            $('.modal-seating').modal('show');
        
        } );
        
        $('a#btn-edit').click(function() {
            
            $('.modal-seating').modal('hide');
            
            $('#modal-edit #id').val(seating_id);
            $('#modal-edit #comments').val(seating_comments);
            
            $('#modal-edit').modal('show');
        
        });
        
        $('#btn-delete').click(function() {
            
            $('.modal-seating').modal('hide');
            
            console.log(seating_id);
            
            
            bootbox.confirm("¿Está Seguro que desea eliminar el asiento?", function(result) {
            
            if (result) {
                    
                var request = $.ajax({
                        url: "<?= $this->Url->build(['controller' => 'DiaryBook', 'action' => 'delete']) ?>",  
                        type: 'POST',
                        dataType: "json",
                        data: JSON.stringify({number:seating_id}),
                    });
                    
                    request.done(function( data ) {
                        
                        console.log(data.ok);
                        
                        if(data.ok){
                            generateNoty('success', 'El asiento sea eliminado correctamente');
                        }else{
                            generateNoty('error', 'Error al intenetar eliminar el asiento.');
                        }
           
                        table_seatings_detail.ajax.reload();
         
                    });
                     
                    request.fail(function( jqXHR, textStatus ) {
                      generateNoty('error', 'Error al intenetar eliminar el asiento');
                    });
                }
            });
    
        });
        
        $('#form-filter').submit(function() {
            
            console.log('form-filter submit ....');
            
            openModalPreloader("Realizando Busqueda ...");
            
            event.preventDefault();
            table_seatings_detail.ajax.reload();
        });
        
        if(seating_number_preseleted){
            
            $('#form-filter #number').val(seating_number_preseleted)
            
            $('#btn-search').click();
        }
        
        if(action_preselected == 'add'){
            
            $('#btn-add-seating').click();  
        }
       
        
    });

    function autoScroll() {
        var $scrollBody = $(table_seatings_detail.table().node()).parent();
        $scrollBody.scrollTop($scrollBody.get(0).scrollHeight);
    }
  
    //cuando se cierra el modal de busqueda de cuentas
    $('#modal-search-account').on('hidden.bs.modal', function (e) {
        $('#modal-add-seating').focus();
    });
    
    //cuando se muestra el modal de add seating
    var i = 0;
    $('#modal-add-seating').on('shown.bs.modal', function (e) {
       
        $('#modal-add-seating #concept').focus();
        
        if(i == 0){
            i++;
            $('#btn-add-row').click();
            $('#btn-add-row').click();
        }
    });
    
    //cuando se cierra el modal de busqueda de cuentas
    $('.modal-search-account').on('hidden.bs.modal', function (e) {
        
        if(row_selected != null && $('#account-selected').val() != '' ){
            
             $('#debe-'+row_selected).val('');
             $('#debe-'+row_selected).focus();
        }
     
    });
    
    //cuando se muestra el modal de busqueda de cuentas
    $('.modal-search-account').on('shown.bs.modal', function (e) {
     
        // $('#accounts-titles').focus(); 
        
        if(table_accounts){
            table_accounts.draw();
        }
        
    });

    $('#btn-clear-filter').click(function() {
        
        $('#form-filter input').val('');
        
        var d = new Date();
        var mydatestr = pad(d.getDate(), 2) + '/' +  pad(d.getMonth()+1, 2)  + '/'+ d.getFullYear();
        $('#from').val(mydatestr);
        $('#to').val(mydatestr);
        
    });
    
    $(".btn-export-excel").click(function() {
        
        bootbox.confirm('Se descargará un archivo Excel', function(result) {
            if(result){
                $('#table-seatings-detail').tableExport({tableName: 'Libro Diario', type:'excel', escape:'false', columnNumber: [5,6]});
            }
        });
    });
    
    $(".btn-export-pdf").click(function() {
            
            var from = $('#from').val();
            var to = $('#to').val();
            var number  = $('#number').val();
            var concept  = $('#concept').val();
            var account_code_parent = $('#account-code-parent').val();
            var account_code_child = $('#account-code-child').val();
            var account_name = $('#account-name').val();
            var manual = $('#manual').is(':checked'); 

            $('body')
            .append( $('<form/>').attr({'action': '/ispbrain/diary-book/printdiary/', 'method': 'post', 'id': 'replacer', 'target': '_blank' })
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'from', 'value': from}))
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'to', 'value': to}))
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'number', 'value': number}))
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'concept', 'value': concept}))
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'manual', 'value': manual}))
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'account_code_parent', 'value': account_code_parent}))
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'account_code_child', 'value': account_code_child}))
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'account_name', 'value': account_name}))
            .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
            .find('#replacer').submit();
            
            $('#replacer').remove();
            
        });
        
    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });
    
</script>
