<style type="text/css">

    .font-size-13px {
        font-size: 13px;
    }

    .table-hover tbody tr:hover {
        background-color: white !important;
    }

    .table-hover tbody tr.selectable:hover {
        background-color: rgba(105, 104, 104, 0.28) !important;
    }
    

</style>

    <div id="btns-tools">
        <div class="text-right btns-tools margin-bottom-5">

            <?php 

                echo $this->Html->link(
                    '<span class="fas fa-sync-alt" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Actualizar la tabla',
                    'class' => 'btn btn-default btn-update-table ml-1',
                    'escape' => false
                ]);

                echo $this->Html->link(
                    '<span class="glyphicon fas fa-search" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Buscar por columnas',
                    'class' => 'btn btn-default btn-search-column ml-1',
                    'escape' => false
                ]);

                echo $this->Html->link(
                    '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Exportar tabla',
                    'class' => 'btn btn-default btn-export ml-1',
                    'escape' => false
                ]);

            ?>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-hover" id="table-customers">
                <thead>
                    <tr>
                        <th >Código</th>              <!--0-->
                        <th >Cuenta</th>              <!--1-->
                        <th >Nombre</th>              <!--2-->
                        <th >Tipo</th>                <!--3-->
                        <th >Nro</th>                 <!--4-->
                        <th >Saldo Total</th>         <!--5-->
                        <th >Saldo cuenta</th>         <!--5-->
                     
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

<?php

    $buttons = [];
    
    
    $buttons[] = [
        'id'   =>  'btn-go-customer',
        'name' =>  'Ir al Cliente',
        'icon' =>  'glyphicon icon-profile',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-go-mayor',
        'name' =>  'ir al Mayor',
        'icon' =>  'glyphicon icon-eye',
        'type' =>  'btn-secondary'
    ];
    
    $buttons[] = [
        'id'   =>  'btn-add-seating',
        'name' =>  'Agregar asiento',
        'icon' =>  'far fa-edit',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-customers', 'title' => 'Acciones', 'buttons' => $buttons ]);

    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-customers']);
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-customers']);
    echo $this->element('modal_preloader');

?>

<div class="tooltip" role="tooltip">
    <div class="arrow">
    </div>
    <div class="tooltip-inner">
    </div>
</div>

<script type="text/javascript">

    var table_customers = null;
    var customer_selected = null;

    var curr_pos_scroll = null;


    $(document).ready(function () {

        $('.btn-hide-column').click(function() {
            $('.modal-hide-columns-customers').modal('show');
        });

        $('.btn-search-column').click(function() {
            $('.modal-search-columns-customers').modal('show');
        });

        $('#table-customers').removeClass('display');

        $('#btns-tools').hide();
        
        var hide_force = [];
        var no_seach = [];
        
        loadPreferences('customers-index5', []);
        

		table_customers = $('#table-customers').DataTable({
		    "order": [[ 1, 'desc' ]],
            // "deferRender": true,
            // "processing": true,
            // "serverSide": true,
            "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/DiaryBook/get_comp_admin_customers.json",
                "dataSrc": "customersdiff",
                "error": function(c) {
                    switch (c.status) {
                        case 400:
                            flag = false;
                            generateNoty('warning', 'Ha ocurrido un error.');
                            break;
                        case 401:
                            flag = false;
                            generateNoty('warning', 'Error, no posee una autorización.');
                            break;
                        case 403:
                            flag = false;
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                        	setTimeout(function() { 
                        	  window.location.href ="/ispbrain";
                        	}, 3000);
                            break;
                    }
                }
            },
            
            "scrollY": true,
		    "scrollY": '430px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
		    "columns": [
		       
                { 
                    "data": "code",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                { 
                    "data": "account_code",
                    "type": "string"
                },
                
                { 
                    "data": "name",
                    "type": "string",
                },
                {
                    "data": "doc_type",
                    "type": "options",
                    "options": sessionPHP.afip_codes.doc_types,
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.doc_types[data];
                    }
                },
                {
                    "data": "ident",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (data) {
                            ident = data;
                        }
                        return ident;
                    }
                },
                { 
                    "data": "debt_total",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' ),
                },
                { 
                    "data": "saldo_account",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' ),
                },
            ],
		    "columnDefs": [
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.code;
            },
		    "language": dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
        	"dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
		}).on( 'draw.dt', function () {
         
        });

		$('.btn-update-table').click(function() {
		    
            if (table_customers) {
                table_customers.ajax.reload();
            }
        });

        $('#table-customers_wrapper .tools').append($('#btns-tools').contents());

        $('#table-customers').on( 'init.dt', function () {
            createModalHideColumn(table_customers, '.modal-hide-columns-customers', hide_force);
            createModalSearchColumn(table_customers, '.modal-search-columns-customers', no_seach);
        });

        $('#btns-tools').show();

        $('#table-customers tbody').on( 'click', 'tr', function (e) {
            
            if (!$(this).find('.dataTables_empty').length) {

                table_customers.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
                customer_selected = table_customers.row( this ).data();
                $('.modal-customers').modal('show');
            }
            
        });
        
        
         $('#btn-go-customer').click(function() {
            var action = '/ispbrain/customers/view/' + customer_selected.code;
            window.open(action, '_blank');
        });
    
    
         $('#btn-go-mayor').click(function() {
            var action = '/ispbrain/DiaryBook/mayor/' + customer_selected.account_code;
            window.open(action, '_blank');
        });
        
          $('#btn-add-seating').click(function() {
            var action = '/ispbrain/DiaryBook/index/0/add';
            window.open(action, '_blank');
        });
        
        
      

     
    });

  

    $(".btn-export").click(function() {

        bootbox.confirm('Se descargará un archivo Excel', function(result) {
            if (result) {
                $('#table-customers').tableExport({tableName: 'Clientes', type:'excel', escape:'false', columnNumber: [5]});
            }
        }).find("div.modal-content").addClass("confirm-width-md");
    });

  



</script>
