

<style type="text/css">
    
    th {
        padding: 2px !important;
    }

</style>

<div class="row">
    
    <div class="col-xl-3 pr-0">

        <div class="card border-secondary mb-3">
            <h5 class="card-header">Filtros</h5>
            <div class="card-body text-secondary p-2">
                
                 <?php echo $this->Form->create(null, ['id' => 'form-filter']); ?>
                    
                    <div class="row mb-2">
                        <div class="col-xl-12">
                            <label  class="control-label">Desde</label> 
                            <div class='input-group date' id='from-datetimepicker'>
                                <input name='from' id="from" type='text' class="form-control" required />
                                <span class="input-group-addon input-group-text calendar">
                                    <span class="glyphicon icon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-xl-12">
                            <label  class="control-label">Hasta</label> 
                            <div class='input-group date' id='to-datetimepicker'>
                                <input name='to' id="to" type='text' class="form-control" required />
                                <span class="input-group-addon input-group-text calendar">
                                    <span class="glyphicon icon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    
                    <?=$this->Form->input('show_customers', ['type' => 'checkbox', 'label' => 'Mostrar Cuentas Clientes', 'checked' => false]) ?>
                    
                    <?=$this->Form->input('show_saldo', ['type' => 'checkbox', 'label' => 'Cuentas con Saldo', 'checked' => false]) ?>
                    
                   <br>
                    <?= $this->Form->button(__('Limpiar'), ['class' => 'btn-default', 'type' => 'button', 'id' => 'btn-clear-filter']) ?>
                    <?= $this->Form->button(__('Buscar'), ['class' => 'btn-success float-right', 'id' => 'btn-search']) ?>
                 
                 
                <?php echo $this->Form->end(); ?>
                
            </div>
        </div>
    </div>
    
    
    <div class="col-xl-9 pl-2">

        <div class="card border-secondary mb-3">
            
            <div class="card-header w-100 pb-1 pt-2">
                 <div class="row">
                    <div class="col-4">
                        <h5> Cuentas </h5>
                    </div>
                    <div class="col-8">
                        <?php
                 
                        echo $this->Html->link(
                            '<span class="glyphicon icon-file-pdf"  aria-hidden="true"></span>',
                            'javascript:void(0)',
                            [
                                'title' => 'Exportar en formato PDF el contenido de la tabla',
                                'class' => 'btn btn-default btn-export-pdf ml-1 float-right',
                                'escape' => false
                            ]);
                            
                        echo $this->Html->link(
                            '<span class="glyphicon icon-file-excel"  aria-hidden="true"></span>',
                            'javascript:void(0)',
                            [
                                'title' => 'Exportar en formato Excel el contenido de la tabla',
                                'class' => 'btn btn-default btn-export-excel ml-1 float-right',
                                'escape' => false
                            ]);
                       
                        ?>
                    </div>
                </div>
            </div>
            
            <div class="card-body text-secondary p-2">
                
                <table class="table table-bordered" id="table-sumas-saldos">
                    <thead>
                        <tr>
                            <th rowspan="2">Cuentas</th>
                            <th rowspan="2">Nombres</th>
                            <!--<th rowspan="2">Acumulado</th>-->
                            <th colspan="2">Sumas</th>
                            <th colspan="2">Saldos</th>
                        </tr>
                        <tr>
                            <th>Debe</th>
                            <th>Haber</th>
                            <th>Deudor</th>
                            <th>Acreedor</th>
                        </tr>
                    </thead>
                    <tbody>
                       
                    </tbody>
                </table>
                    
            </div>
        </div>
    </div>
    
</div>


<?php

 echo $this->element('modal_preloader');

?>

<script type="text/javascript">

    var table_sumas_saldos = null;
    var from = new Date();
    from.setDate(1);

    $('#from-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: from,
        format: 'DD/MM/YYYY',
    });
    
    $('#to-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'DD/MM/YYYY',
    });
    
    table_sumas_saldos = $('#table-sumas-saldos').DataTable({
            "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/DiaryBook/get_suma.json",
                "dataSrc": "sumasSaldos",
                "data": function ( d ) {
                  return $.extend( {}, d, {
                    "filter_data":  $('#form-filter').serializeArray() 
                    });
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
            				  window.location.href ="/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "ordering": false,
		    "scrollY": true,
		    "scrollY": '383px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": false,
		    "searching": false,
		    "columns": [
                
                { 
                    "data": "code"
                },
                { 
                    "data": "name"
                },
                { 
                    "data": "debe",
                    "render": function ( data, type, row ) {
                        return number_format(data);
                    }
                },
                { 
                    "data": "haber",
                    "render": function ( data, type, row ) {
                        return number_format(data);
                    }
                },
                { 
                    "data": "debe",
                    "render": function ( data, type, row ) {
                        var saldo =  data - row.haber;
                        if(saldo > 0){
                            return number_format(saldo);
                        }
                        return number_format(0);
                    }
                },
                { 
                    "data": "haber",
                    "render": function ( data, type, row ) {
                        var saldo = row.debe - data;
                        if(saldo > 0){
                            return number_format(0);
                        }
                        return number_format(Math.abs(saldo));
                    }
                },
               
            ],
		    "columnDefs": [
		        { "type": 'numeric-comma', targets: [2,3,4,5] },
		        { 
                    "width": "10%", targets: [2,3,4,5]
                },
                { 
                    "class": "left", targets: [1]
                },
                { 
                    "class": "right", targets: [2,3,4,5]
                },
            ],
            "drawCallback": function ( settings ) {
              
                closeModalPreloader();
                
                 $('#table-sumas-saldos thead').addClass('bg-light');
            },
            "initComplete": function(settings, json) {
               
              $('#table-sumas-saldos thead').addClass('bg-light');
               
            },
            "createdRow" : function( row, data, index ) {
                row.id = data.code;
                if(data.title == '1'){
                    $(row).addClass('font-weight-bold title');
                }
            },
		    "language": dataTable_lenguage,
        	"dom":
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-12'i>>",
   
		});
    
    $('#form-filter').submit(function(event){
        
        event.preventDefault();
        
        openModalPreloader("Realizando Busqueda ...");
        
        table_sumas_saldos.ajax.reload();
      
    });
    
    $('#btn-clear-filter').click(function(){
        from = new Date();
        from = '01/'+pad(from.getMonth()+1,2)+'/'+from.getFullYear();
        
        $('#from').val(from);
        
        var to = new Date();
        to = pad(to.getDate(),2)  +'/'+pad(to.getMonth()+1,2)+'/'+to.getFullYear();
        
        $('#to').val(to);
        
    });
    
    $(".btn-export-excel").click(function() {
        
        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if(result){
                $('#table-sumas-saldos').tableExport({tableName: 'Mayor', type:'excel', escape:'false', columnNumber: [2,3,4,5]});
            }
        });
    });
    
    $(".btn-export-pdf").click(function() {
            
            var from = $('#from').val();
            var to = $('#to').val();
            var show_customers = $('#show-customers').is(':checked'); 
            var show_saldo = $('#show-saldo').is(':checked'); 
            
            console.log(show_saldo);
       
            $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/diary-book/printsuma/', 'method': 'post', 'id': 'replacer', 'target': '_blank' })
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'from', 'value': from}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'to', 'value': to}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'show_saldo', 'value': show_saldo}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'show_customers', 'value': show_customers}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#replacer').submit();
            
            $('#replacer').remove();
            
        });
        
    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });
    
</script>