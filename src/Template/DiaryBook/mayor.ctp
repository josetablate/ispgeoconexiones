
<div class="row">
    
    <div class="col-xl-4 pr-0">

        <div class="card border-secondary mb-3">
            <h5 class="card-header">Filtros</h5>
            <div class="card-body text-secondary p-2">
                
                 <?php echo $this->Form->create(null, ['id' => 'form-filter']); ?>
        
                    <div class="row mb-2">
                        <div class="col-xl-6 pr-0">
                            <label  class="control-label">Desde</label> 
                            <div class='input-group date' id='from-datetimepicker'>
                                <input name='from' id="from" type='text' class="form-control" required />
                                <span class="input-group-addon input-group-text calendar">
                                    <span class="glyphicon icon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-xl-6 pl-0">
                            <label  class="control-label">Hasta</label> 
                            <div class='input-group date' id='to-datetimepicker'>
                                <input name='to' id="to" type='text' class="form-control" required />
                                <span class="input-group-addon input-group-text calendar">
                                    <span class="glyphicon icon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                   
                    <br>
                     <?= $this->Form->button(__('Buscar Cuenta'), ['class' => 'btn-info w-100 ', 'id' => 'btn-open-search-account', 'type' => 'button']) ?>
                    <br>  <br>
                    <label class="control-label" for="account-code">Nº Cuenta</label>
                    <div class="row">
                        <div class="col-6">
                             <?php echo $this->Form->input('account_code_parent', ['type' => 'number', 'label' => false, 'min' => 0, 'max' => 9999, 'readonly' => true]); ?>
                        </div>
                        <div class="col-6">
                            <?php echo $this->Form->input('account_code_child', ['type' => 'number', 'label' => false, 'min' => 0, 'max' => 99999, 'readonly' => true]); ?>
                        </div>
                    </div>
                    
                    <?php echo $this->Form->input('account_name', ['type' => 'text', 'label' => 'Nombre Cuenta', 'readonly' => true]); ?>
                  
                    <?= $this->Form->button(__('Limpiar'), ['class' => 'btn-default', 'type' => 'button', 'id' => 'btn-clear-filter']) ?>
                    <?= $this->Form->button(__('Buscar'), ['class' => 'btn-success float-right', 'id' => 'btn-search']) ?>
                 
                 
                <?php echo $this->Form->end(); ?>
                
            </div>
        </div>
    </div>
    
    
    <div class="col-xl-8 pl-2">

        <div class="card border-secondary mb-3">
            
            <div class="card-header w-100 pb-1 pt-2">
                 <div class="row">
                    <div class="col-4">
                        <h5> Mayor </h5>
                    </div>
                    <div class="col-8">
                        <?php
                 
                        echo $this->Html->link(
                            '<span class="glyphicon icon-file-pdf"  aria-hidden="true"></span>',
                            'javascript:void(0)',
                            [
                                'title' => 'Exportar en formato PDF el contenido de la tabla',
                                'class' => 'btn btn-default btn-export-pdf ml-1 float-right',
                                'escape' => false
                            ]);
                            
                        echo $this->Html->link(
                            '<span class="glyphicon icon-file-excel"  aria-hidden="true"></span>',
                            'javascript:void(0)',
                            [
                                'title' => 'Exportar en formato Excel el contenido de la tabla',
                                'class' => 'btn btn-default btn-export-excel ml-1 float-right',
                                'escape' => false
                            ]);
                       
                        ?>
                    </div>
                </div>
            </div>
            
            <div class="card-body text-secondary p-2">
                
                <table class="table table-bordered" id="table-seatings-detail">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Asiento</th>
                            <th>Concepto</th>
                            <th>Debe</th>
                            <th>Haber</th>
                            <th>Saldo</th>
                        </tr>
                    </thead>
                    <tbody>
                       
                    </tbody>
                </table>
                    
            </div>
        </div>
    </div>
    
</div>


<div class="modal fade modal-search-account" tabindex="-1" role="dialog"  data-backdrop="false" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
            <div class="col-xl-12">
                <?=$this->element('Accountant/accounts_seeker')?>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-go-seating',
        'name' =>  'Ir al asiento',
        'icon' =>  'glyphicon icon-eye',
        'type' =>  'btn-secondary'
    ];

 echo $this->element('actions', ['modal'=> 'modal-seatings-detail', 'title' => 'Acciones', 'buttons' => $buttons ]);
 echo $this->element('modal_preloader');

?>

<script type="text/javascript">

    var account_preseleted = <?= json_encode($account_preseleted) ?>;
    
    $(document).ready(function(){
        
        // console.log(account_preseleted);
    
        var table_seatings_detail = null;
        var filter_active = false;
    
        var from = new Date();
        from.setDate(1);
        
        $('#from-datetimepicker').datetimepicker({
            defaultDate: from,
            format: 'DD/MM/YYYY',
        });
        
        $('#to-datetimepicker').datetimepicker({
            defaultDate: new Date(),
            format: 'DD/MM/YYYY',
        });
        
        $('#btn-open-search-account').click(function(){
           $('.modal-search-account').modal('show');
        });
        
        $('#card-accounts-seeker').on('ACCOUNT_SELECTED', function(){
            
            $('.modal-search-account').modal('hide');
          
            if(account_selected){
                
                account_selected.code = account_selected.code.toString();
                
                $('#account-code-parent').val(account_selected.code.substring(0, 4));
                $('#account-code-child').val(account_selected.code.substring(4, 9));
                
                account_selected.name = account_selected.name.split("&nbsp;").join("");
                account_selected.name = account_selected.name.split("&nbsp").join("");
                
                $('#account-name').val(account_selected.name.split("&nbsp;").join(""));
                
                $('#form-filter').submit();
    
            }
           
        });
        
        $('#card-accounts-seeker').on('ACCOUNT_CANCEL', function(){
            
            $('.modal-search-account').modal('hide');
           
        });
        
        $('.modal-search-account').on('shown.bs.modal', function (e) {
            
            if(table_accounts){
                table_accounts.draw();
            }
            
        });
        
        table_seatings_detail = $('#table-seatings-detail').DataTable({
                "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/DiaryBook/get_mayor_by_filter.json",
                    "dataSrc": "seatingsDetailsArray",
                    "data": function ( d ) {
                      return $.extend( {}, d, {
                        "filter_data": (filter_active) ? $('#form-filter').serializeArray() : ''
                      } );
                    },
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
    
                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "ordering": false,
    		    "scrollY": true,
    		    "scrollY": '383px',
    		    "scrollX": true,
    		    "scrollCollapse": true,
    		    "paging": false,
    		    "searching": false,
    		    "columns": [
                    { 
                        "data": "created",
                        "render": function ( data, type, row ) {
                            if(data){
                                var date = data.split(' ')[0];
                                date = date.split('-');
                                return date[2]+'/'+date[1]+'/'+date[0]
                            }
                        }
                    },
                    { 
                        "data": "number",
                        "render": function ( data, type, row ) {
                            // console.log(row); 
                            if(row.exist_in_admin){
                                return "<span class='text-success'>" + pad(data, 5) + "</span>";
                            }
                            return  pad(data, 5);
                        }
                    },
                    { 
                        "data": "concept"
                    },
                    { 
                        "data": "debe",
                       "render": function ( data, type, row ) {
                            return number_format(data);
                        }
                    },
                    { 
                        "data": "haber",
                        "render": function ( data, type, row ) {
                            return number_format(data);
                        }
                    },
                    { 
                        "data": "saldo",
                        "render": function ( data, type, row ) {
                            return number_format(data);
                        }
                    },
                    
                ],
    		    "columnDefs": [
    		        { "type": 'date-custom', targets: [0] },
    		        { "type": 'numeric-comma', targets: [3,4,5] },
    		        { 
                        "width": "12%", targets: [0,1,3,4,5]
                    },
                    { 
                        "class": "left", targets: [2]
                    },
                    { 
                        "class": "right", targets: [3,4,5]
                    },
                ],
                "drawCallback": function ( settings ) {
                  
                    closeModalPreloader();
                },
                "initComplete": function(settings, json) {
                     autoScroll();
                     
                },
    		    "language": dataTable_lenguage,
            	"dom":
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-12'i>>",
       
    		});
        
        $('#form-filter').submit(function(event){
            
            event.preventDefault();
            
            if(account_selected){
                
                filter_active = true;
                
                openModalPreloader("Realizando Busqueda ...");
                
                table_seatings_detail.ajax.reload();
            }else{
                generateNoty('warning', 'Primero debe buscar una cuenta.');
            }
           
        });
        
         function autoScroll(){
            var $scrollBody = $(table_seatings_detail.table().node()).parent();
            $scrollBody.scrollTop($scrollBody.get(0).scrollHeight);
        }
        
        var seatings_detail_selected = null;
        
        $('#table-seatings-detail tbody').on( 'click', 'tr', function () {

            if (!$(this).closest('tr').find('.dataTables_empty').length) {

                table_seatings_detail.$('tr.selected').removeClass('selected');
                $(this).closest('tr').addClass('selected');

                seatings_detail_selected = table_seatings_detail.row( this ).data();

                $('.modal-seatings-detail').modal('show');
            }
        });
        
        
        
        $('#btn-go-seating').click(function() {
        
            $('.modal-seatings-detail').modal('hide');
            
            // console.log(seatings_detail_selected);
            
            var action = '/ispbrain/DiaryBook/index/' + seatings_detail_selected.number;
            window.open(action, '_self');
        });
        
         $('#btn-go-customer').click(function() {
        var action = '/ispbrain/customers/view/' + invoice_selected.customer_code;
        window.open(action, '_self');
    });

            
        
        $('#btn-clear-filter').click(function(){
            
            clear_card_accounts_seeker();
            
            $('#form-filter input').val('');
            
            from = new Date();
            from = '01/'+pad(from.getMonth()+1,2)+'/'+from.getFullYear();
            
            $('#from').val(from);
            
            var to = new Date();
            to = pad(to.getDate(),2)  +'/'+pad(to.getMonth()+1,2)+'/'+to.getFullYear();
            
            $('#to').val(to);
            
        });
        
        $(".btn-export-excel").click(function() {
            
            bootbox.confirm('Se descargara un archivo Excel', function(result) {
                if(result){
                    $('#table-seatings-detail').tableExport({tableName: 'Mayor', type:'excel', escape:'false', columnNumber: [3,4,5]});
                }
            });
        });
        
        $(".btn-export-pdf").click(function(){
                
                var from = $('#from').val();
                var to = $('#to').val();
                var number  = $('#number').val();
                var concept  = $('#concept').val();
                var account_code_parent = $('#account-code-parent').val();
                var account_code_child = $('#account-code-child').val();
                var account_name = $('#account-name').val();
           
           
                $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/diary-book/printmayor/', 'method': 'post', 'id': 'replacer', 'target': '_blank' })
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'from', 'value': from}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'to', 'value': to}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'number', 'value': number}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'concept', 'value': concept}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'account_code_parent', 'value': account_code_parent}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'account_code_child', 'value': account_code_child}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'account_name', 'value': account_name}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#replacer').submit();
                
                $('#replacer').remove();
                
            });
            
        // Jquery draggable modals
        $('.modal-dialog').draggable({
            handle: ".modal-header"
        });
        
        if(account_preseleted){
            
            account_selected = account_preseleted;
            
            $('#card-accounts-seeker').trigger('ACCOUNT_SELECTED');
            
        }
            
        
    });
    
    
    
    
    
</script>