<!5 html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="ispbrain-icon.ico" type="image/x-icon">

    <?php 
        echo $this->Html->meta(
            'ispbrain-icon.ico',
            '/ispbrain-icon.ico',
            array('type' => 'icon')
        );
    ?>
    <title>
    </title>

    <link rel="manifest" href="/ispbrain/manifest.json">

    <?= $this->Html->css([
        '/vendor/Animate/animate',
        '/vendor/noty/lib/noty',
        '/vendor/DataTables/datatables.min',
        '/vendor/jquery-ui-1.12.1.custom/jquery-ui.min',
        '/vendor/icomoon/style',
        '/vendor/multi-level-push-menu-master/jquery.multilevelpushmenu',
        'jquery-te/jquery-te-1.4.0',
        '/vendor/bootstrap-4.0.0/dist/css/bootstrap.min',
        '/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker',
        'common',
        'layout',
        'forms',
        'tables',
        'multi-level-push-menu-master',
        '/vendor/bootstrap-select/bootstrap-select/dist/css/bootstrap-select',
    ]) ?>

    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700&subset=latin,cyrillic-ext,latin-ext,cyrillic' rel='stylesheet' type='text/css'>

    <?= $this->Html->script([
        '/vendor/jQuery-3.2.1/jquery-3.2.1',
        '/vendor/jquery-ui-1.12.1.custom/jquery-ui.min',
        '/vendor/multi-level-push-menu-master/jquery.multilevelpushmenu1',
        '/vendor/popper.js-1.12.5/dist/umd/popper.min',
        '/vendor/bootstrap-datetimepicker/moment-with-locales',
        '/vendor/bootstrap-4.0.0/dist/js/bootstrap.min',
        '/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker',
        'bootbox.min',
        '/vendor/noty/lib/noty',
        '/vendor/DataTables/datatables',
        '/vendor/dataTables-custom/sort',
        '/vendor/dataTables-custom/pipeline',
        '/vendor/dataTables-custom/lenguage',
        'table2excel/jquery.table2excel',
        'tableExpor/jquery.base64',
        'tableExpor/tableExport',
        'tableExpor/html2canvas',
        'tableExpor/jspdf/libs/base64',
        'tableExpor/jspdf/libs/sprintf',
        'tableExpor/jspdf/jspdf',
        'jquery-te/jquery-te-1.4.0.min',
        'multi-level-push-menu-master1',
        'common',
        '/vendor/bootstrap-select/bootstrap-select/dist/js/bootstrap-select',
        'googlemap/markerclusterer',
        '/vendor/fontawesome-free-5.5.0-web/js/all.min',
        // '/vendor/modernizr/2.6.2/modernizr.min',
        '/vendor/jquery-base64/jquery.base64',
    ]) ?>

    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>-->
    <!--<script type="text/javascript" src="https://oss.maxcdn.com/libs/modernizr/2.6.2/modernizr.min.js"></script>-->

    <!--<script src="/ispbrain/Highstock/code/highstock.js"></script>-->
    <!--<script src="/ispbrain/Highstock/code/modules/exporting.js"></script>-->

    <!--<script src="https://code.highcharts.com/highcharts.js"></script>-->
    <!--<script src="https://code.highcharts.com/modules/exporting.js"></script>-->
    <!--<script src="https://code.highcharts.com/modules/export-data.js"></script>-->

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script type="text/javascript" src="/ispbrain/ckeditor/ckeditor.js"></script>

    <script type="text/javascript">
        var sessionPHP = <?= json_encode($_SESSION) ?>;
        var version = <?= json_encode($version) ?>;
    </script>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>

    <!--<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js" integrity="sha384-kW+oWsYx3YpxvjtZjFXqazFpA7UP/MbiY4jvs+RWZo2+N94PFZ36T6TFkc9O3qoB" crossorigin="anonymous"></script>-->

</head>

<body>

    <?php echo $this->element('preloader') ?>

    <?php

      /*if ($_SESSION['conff']->status == 'asc') {
          $message = "<b>Advertencia</b>: El sistema está sin conexión con la central ISPBrain. Por favor comuníquese con los administradores del Sistema.";
          echo $this->element('warning_message', ['message' => $message, 'status' => 'warning']); 
      }*/

      /*if ($_SESSION['conff']->status == 'mo') {
          $message = "Se registran deudas vencidas. Por favor regularice su situación para evitar suspención del servicio. Si esto no es correcto, por favor comuníquese con los administradores del Sistema.";
          echo $this->element('warning_message', ['message' => $message, 'status' => 'danger']); 
      }*/

      if ($_SESSION['conff']->status == 'vp') {
        $message = "Validación de correo pendiente. Por favor verifica tu cuenta de correo, encontrarás un correo de Bienvenida y confirmación. En caso de no encontrar dicho correo comunícate con nosotros informando tu situación.";
        echo $this->element('warning_message', ['message' => $message, 'status' => 'danger']); 
      }

    ?>

    <?= $this->Flash->render() ?>
    <?= $this->Flash->render('default') ?>
    <?= $this->Flash->render('auth') ?>

       <?php if ($loggedIn): ?>
          <div class="container-fluid">
              <div class="row">
                  <div class="col-12">
                      <?= $this->element('Layout/title') ?>
                  </div>
                  <div class="col-4 col-sm-3 col-md-3 col-lg-2 col-xl-2 pr-2">
                      <div id="menu"></div>
                  </div>
                  <div class="col-8 col-sm-9 col-md-9 col-lg-10 pl-0" id="content1">
                      <?= $this->fetch('content') ?>    
                  </div>
              </div>
          </div>
      <?php else: ?>
          <div class="container">
              <?= $this->fetch('content') ?>
          
          </div>
      <?php endif; ?>

    <?= $this->fetch('script') ?>

    <script>

        $(document).ready(function() {

            var actionsSystemArray = <?= json_encode($actionsSystemArray) ?>;
            var itemsMenuByUser = <?php echo json_encode($itemsMenuByUser); ?>;

            $.each(actionsSystemArray, function(i, val) {
              $('#' + val.clase.clase + '-' + val.action).hide();
            });

            $.each(itemsMenuByUser, function(i, val) {
              $('#' + val).show();
            });

            $('#menuID h2').append("<small>&nbsp&nbspv"+version+"</small>");

            if (!sessionPHP.paraments.accountant.account_enabled) {
                $('#Contable').hide();
            }

            if (sessionPHP.paraments.gral_config.billing_for_service) {
                $('#Customers-lockeds').hide();
            } else {
                $('#Connections-lockeds').hide();
            }

            
            if(sessionPHP.paraments.gral_config.using_avisos){
                $('#AvisoHttp-admin').show();
            }else{
                $('#AvisoHttp-admin').hide();
            }           

        });
    </script>
    <br><br>
</body>
</html>
