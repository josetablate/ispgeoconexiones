<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">

<style type="text/css">

    .text-Oswald {
        font-family: 'Oswald', sans-serif;
    }

    body {
        overflow-x: hidden;
    }

</style>

<?= 
    $this->Html->css([
        '/vendor/fontawesome-free-5.0.6/on-server/css/fa-regular.min',
        '/vendor/fontawesome-free-5.0.6/on-server/css/fontawesome-all.min',
        '/vendor/Bootstrap/css/bootstrap.min',
    ]) 
?>

<div class="row justify-content-center text-Oswald align-middle mt-5">
    <div class="col-11 col-sm-8 col-md-6 col-lg-5 col-xl-5">

        <div class="card border-secondary">
            <div class="card-body text-secondary">
                 <h1 class="card-title">  <i class="fas fa-exclamation-triangle fa-2x mr-2"></i> Algo salió mal.</h1>
                 <br>
                 <h4 class="card-title">
                     Puede regresar al <a href="<?= $this->Url->build(["controller" => "home", "action" => "dashboard"]) ?>" target="_top" class="text-secondary">tablero <i class="fas fa-external-link-alt mr-1"i></i></a>
                 </h4>
                 <br>
                 <p class="card-text">Si el error persiste. Comuníquese con el administardor del sistema.</p>
                 <p><a href="mailto:ayuda@ispbrain.io" target="_top" class="text-secondary">contacto@ispbrain.io</a></p>
            </div>
        </div>
    </div>
</div>
