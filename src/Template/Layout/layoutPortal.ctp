<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = $paraments->invoicing->company->name;
?>
<!5 html>
<html>
  <head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="ispbrain-icon.ico" type="image/x-icon">

    <?php echo $this->Html->meta(
      'ispbrain-icon.ico',
      '/ispbrain-icon.ico',
      array('type' => 'icon')
    ); ?>
    <title>
      <?= $cakeDescription ?>
    </title>

    <?= $this->Html->css([
      'font-awesome/css/font-awesome.min',
      '/vendor/Animate/animate',
      '/vendor/noty/lib/noty',
      'flaticon/flaticon',
      '/vendor/DataTables/datatables.min',
      '/vendor/jquery-ui-1.12.1.custom/jquery-ui.min',
      '/vendor/icomoon/style',
      '/vendor/Bootstrap/css/bootstrap.min',
      '/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker',
      'portal/portal'
    ]) ?>

    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700&subset=latin,cyrillic-ext,latin-ext,cyrillic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.1/css/font-awesome.min.css">

    <?= $this->Html->script([
      '/vendor/jQuery-3.2.1/jquery-3.2.1',
      '/vendor/popper.js-1.12.5/dist/umd/popper.min',
      '/vendor/bootstrap-datetimepicker/moment-with-locales',
      '/vendor/Bootstrap/js/bootstrap',
      '/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker',
      'bootbox.min',
      '/vendor/noty/lib/noty',
      '/vendor/jquery-ui-1.12.1.custom/jquery-ui.min',
      'table2excel/jquery.table2excel',
      'tableExpor/jquery.base64',
      'tableExpor/tableExport',
      'tableExpor/html2canvas',
      'tableExpor/jspdf/libs/base64',
      'tableExpor/jspdf/libs/sprintf',
      'tableExpor/jspdf/jspdf',
      'common',
      '/vendor/jquery-base64/jquery.base64',
    ]) ?>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>
    <script type="text/javascript" src="https://oss.maxcdn.com/libs/modernizr/2.6.2/modernizr.min.js"></script>

    <script src="/ispbrain/Highstock/code/highstock.js"></script>
    <script src="/ispbrain/Highstock/code/modules/exporting.js"></script>

    <script type="text/javascript" src="/ispbrain/ckeditor/ckeditor.js"></script>

    <script type="text/javascript">

      $(document).ready(function () {

          var desc = '<?= $cakeDescription ?>';

          $('.dropdown-toggle').dropdown();
          $('#userlogin').dropdown('toggle');

          var current_controller = $('#current_controller').val();
          var current_action = $('#current_action').val();

          var text = current_action;
          $('#head-title').html(text);
          document.title = text  + ' | ' + desc;
      });

      window.onload = function(e) {
          $(".se-pre-con").fadeOut("slow");
      }

      window.onbeforeunload = function(e) {
          $(".se-pre-con").show();
      };

      var sessionPHP = <?= json_encode($_SESSION) ?>;
    
    </script>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

  </head>
  <body  id="page-top">

    <div class="se-pre-con"></div>

    <form>
        <input type="hidden" name="current_controller" id="current_controller" value="<?=$current_controller?>"  />
        <input type="hidden" name="current_action" id="current_action" value="<?=$current_action?>"  />
    </form>

    <?= $this->Flash->render() ?>
    <?= $this->Flash->render('auth') ?>

    <!-- header -->
    <?php if ($loggedIn): ?>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <div class="container">
        <a class="navbar-brand" href="/ispbrain/portal/">
          <?php echo $this->Html->image('logo.png', ['alt' => 'ISPGEO', 'width' => 140]);?>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown2" aria-controls="navbarNavDropdown2" aria-expanded="false" aria-label="Toggle navigation">
          <span class="fa fa-user"></span>
        </button>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav">
            <li class="nav-item <?= ($current_controller == 'Portal' && $current_action == 'index') ? 'active' : '' ?>">
              <a class="nav-link" href="/ispbrain/portal/"><?= __('Inicio') ?></a>
            </li>
            <?php if ($paraments->portal->show_receipt || $paraments->portal->show_resume): ?>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="javascript:void(0)" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?= __('Informes') ?>
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <?php if ($paraments->portal->show_resume): ?>
                <a class="dropdown-item" href="/ispbrain/portal/resumes"><?= __('Ver Resumen de Cuenta') ?></a>
                <?php endif; ?>
                <?php if ($paraments->portal->show_receipt): ?>
                <a class="dropdown-item" href="/ispbrain/portal/receipts"><?= __('Ver Recibos') ?></a>
                <?php endif; ?>
              </div>
            </li>
            <?php endif; ?>
          </ul>
      </div>
      <div class="collapse navbar-collapse" id="navbarNavDropdown2">
        <ul class="navbar-nav">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="javascript:void(0)" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-user" aria-hidden="true"></i> <?= $customer->name ?>
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="/ispbrain/portal/profile"><?= __('Ver Mi Cuenta') ?></a>
                <a class="dropdown-item" href="/ispbrain/portal/logout"><?= __('Cerrar Sesión') ?></a>
              </div>
            </li>
          </div>
        </ul>
      </div>
    </nav>
    <?php endif; ?>
    <!-- //header -->

    <!-- body -->
    <div class="container portal-container">
      <?= $this->fetch('content') ?>
    </div>
    <!-- //body -->

    <!-- footer -->
    <?php if ($loggedIn): ?>
  	<div class="footer">
  		<div class="container">
  			<div class="row w3_footer_grids">
  				<div class="col-md-4 w3_footer_grid">
  					<h3><?= __('Contactos') ?></h3>
  					<p><?= __('Puedes contactarnos por estos medios.') ?></p>
  					<ul class="address">
  						<li><i class="fa fa-map-marker" aria-hidden="true"></i><?= $paraments->invoicing->company->address . ', ' . $paraments->invoicing->company->city ?></span></li>
  						<li><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="javascript:void(0)"><?= $paraments->invoicing->company->email ?></a></li>
  						<li><i class="fa fa-phone" aria-hidden="true"></i><?= $paraments->invoicing->company->phone ?></li>
  					</ul>
  				</div>
  				<?php if ($paraments->portal->show_receipt || $paraments->portal->show_resume): ?>
  				<div class="col-md-3 w3_footer_grid">
  					<h3><?=  __('Informes') ?></h3>
  					<ul class="info">
  					  <?php if ($paraments->portal->show_resume): ?>
  						<li><a href="/ispbrain/portal/resumes"><?= __('Ver Resumen de Cuenta') ?></a></li>
							<?php endif; ?>
							<?php if ($paraments->portal->show_receipt): ?>
  						<li><a href="/ispbrain/portal/receipts"><?= __('Ver Recibos') ?></a></li>
  						<?php endif; ?>
  					</ul>
  				</div>
  				<?php endif; ?>
  				<div class="col-md-4 w3_footer_grid">
  					<h3><?= __('Redes Sociales') ?></h3>
  					<h4><?= __('Síguenos') ?></h4>
  					<div class="agileits_social_button">
  						<ul>
  							<li><a href="https://web.facebook.com/<?= $paraments->invoicing->company->name ?>/" class="facebook fa fa-facebook"> </a></li>
  							<li><a href="https://www.instagram.com/<?= $paraments->invoicing->company->name ?>/" class="instagram fa fa-instagram"> </a></li>
  							<li><a href="https://www.youtube.com/channel/<?= $paraments->invoicing->company->name ?>" class="google fa fa-google-plus"> </a></li>
  							<li><a href="https://es.pinterest.com/<?= $paraments->invoicing->company->name ?>/" class="pinterest fa fa-pinterest-p"> </a></li>
  						</ul>
  					</div>
  				</div>
  				<div class="clearfix"> </div>
  			</div>
  		</div>
  		<div class="footer-copy">
  			<div class="footer-copy1">

  			</div>
  			<div class="container">
  				<p>&copy; <span id="lbl_current_year"></span> <?= __($paraments->invoicing->company->name . '.Todos los derechos reservados | Design by') ?> <a href="http://w3layouts.com/"><?= __('Pim Pam Pum') ?> </a></p>
  			</div>
  		</div>
  	</div>
  	<?php endif;?>
    <!-- //footer -->
    <script type="text/javascript">

      $(document).ready(function() {

        var current_year = new Date().getFullYear();
        $('#lbl_current_year').text(current_year);

        $('body').css('overflow', 'auto');

        var paraments = <?= json_encode($paraments) ?>;

        console.log(paraments);

        var actionsSystemArray = <?= json_encode($actionsSystemArray) ?>;

        $.each(actionsSystemArray, function(i, val) {
          $('#'+val.clase.clase+'-'+val.action).hide();
        });

        var itemsMenuByUser =  <?php echo json_encode($itemsMenuByUser); ?>;

        $.each(itemsMenuByUser, function(i, val) {
          $('#' + val).show();
        });

        if (!paraments.customer.searching_code) {
          $('#input-searching-code-customer').hide();
        }
      });

    </script>

  </body>
</html>
