<!5 html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="ispbrain-icon.ico" type="image/x-icon">

    <?php 
        echo $this->Html->meta(
            'ispbrain-icon.ico',
            '/ispbrain-icon.ico',
            array('type' => 'icon')
        );
    ?>
    <title>
    </title>

    <link rel="manifest" href="/ispbrain/manifest.json">

    <?= $this->Html->css([
        '/vendor/fontawesome-free-5.0.6/on-server/css/fa-regular.min',
        '/vendor/fontawesome-free-5.0.6/on-server/css/fontawesome-all.min',
        '/vendor/Animate/animate',
        '/vendor/noty/lib/noty',
        'flaticon/flaticon',
        '/vendor/DataTables/datatables.min',
        '/vendor/jquery-ui-1.12.1.custom/jquery-ui.min',
        '/vendor/icomoon/style',
        '/vendor/multi-level-push-menu-master/jquery.multilevelpushmenu',
        'jquery-te/jquery-te-1.4.0',
        '/vendor/Bootstrap/css/bootstrap.min',
        '/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker',
        'common.mobile',
        'layout',
        'forms',
        'tables',
        'multi-level-push-menu-master.mobile',
        '/vendor/bootstrap-select/bootstrap-select/dist/css/bootstrap-select'
    ]) ?>

    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700&subset=latin,cyrillic-ext,latin-ext,cyrillic' rel='stylesheet' type='text/css'>

    <!--<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js" integrity="sha384-kW+oWsYx3YpxvjtZjFXqazFpA7UP/MbiY4jvs+RWZo2+N94PFZ36T6TFkc9O3qoB" crossorigin="anonymous"></script>-->

    <?= $this->Html->script([
        '/vendor/jQuery-3.2.1/jquery-3.2.1',
        '/vendor/jquery-ui-1.12.1.custom/jquery-ui.min',
        '/vendor/multi-level-push-menu-master/jquery.multilevelpushmenu1',
        '/vendor/popper.js-1.12.5/dist/umd/popper.min',
        '/vendor/bootstrap-datetimepicker/moment-with-locales',
        '/vendor/Bootstrap/js/bootstrap',
        '/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker',
        'bootbox.min',
        '/vendor/noty/lib/noty',
        '/vendor/DataTables/datatables',
        '/vendor/dataTables-custom/sort',
        '/vendor/dataTables-custom/pipeline',
        '/vendor/dataTables-custom/lenguage',
        'table2excel/jquery.table2excel',
        'tableExpor/jquery.base64',
        'tableExpor/tableExport',
        'tableExpor/html2canvas',
        'tableExpor/jspdf/libs/base64',
        'tableExpor/jspdf/libs/sprintf',
        'tableExpor/jspdf/jspdf',
        'jquery-te/jquery-te-1.4.0.min',
        'multi-level-push-menu-master.mobile1',
        'common.mobile',
        '/vendor/bootstrap-select/bootstrap-select/dist/js/bootstrap-select',
        'googlemap/markerclusterer',
        '/vendor/fontawesome-free-5.5.0-web/js/all.min',
        '/vendor/jquery-base64/jquery.base64',
    ]) ?>

    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>-->
    <!--<script type="text/javascript" src="https://oss.maxcdn.com/libs/modernizr/2.6.2/modernizr.min.js"></script>-->

    <script src="/ispbrain/Highstock/code/highstock.js"></script>
    <script src="/ispbrain/Highstock/code/modules/exporting.js"></script>

    <script type="text/javascript" src="/ispbrain/ckeditor/ckeditor.js"></script>

    <script type="text/javascript">
    var sessionPHP = <?= json_encode($_SESSION) ?>;
    var version = <?= json_encode($version) ?>;
    </script>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>

    <style type="text/css">

      .menu-modal {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        display: block;
        overflow: hidden;
        outline: 0;
        background-color: black;
        opacity: 0.5;
      }

    </style>
</head>

<body>

    <?php echo $this->element('preloader') ?>

    <?= $this->Flash->render() ?>
    <?= $this->Flash->render('default') ?>
    <?= $this->Flash->render('auth') ?>

    <?php if ($loggedIn): ?>
        <div class="container-fluid overflow-off">

            <?php

               if ($_SESSION['conff']->status == 'asc') {
                    $message = "<b>Advertencia</b>: El sistema está sin conexión con al central ISPBrain. Por favor comuníquese con los administradores del Sistema.";
                    echo $this->element('warning_message', ['message'=> $message, 'status' => 'warning']); 
                }

                if ($_SESSION['conff']->status == 'mo') {
                    $message = "Se registran deudas vencidas. Por favor regularice su situación para evitar suspención del servicio. Si esto no es correcto, por favor comuníquese con los administradores del Sistema.";
                    echo $this->element('warning_message', ['message'=> $message, 'status' => 'danger']); 
                }
            ?>

            <?= $this->element('Layout/title.mobile') ?>

            <?= $this->fetch('content') ?>
            <div class="menu-modal"></div>
            <div id="menu" class="fixed"></div>

        </div>
    <?php else: ?>
        <div class="container">
            <?= $this->fetch('content') ?>
        </div>
    <?php endif; ?>

    <?= $this->fetch('script') ?>

    <script>

        $(document).ready(function() {

            $('.menu-modal').fadeOut();

            var actionsSystemArray = <?= json_encode($actionsSystemArray) ?>;
            var itemsMenuByUser = <?php echo json_encode($itemsMenuByUser); ?>;

            $.each(actionsSystemArray, function(i, val) {
                $('#' + val.clase.clase + '-' + val.action).hide();
            });

            $.each(itemsMenuByUser, function(i, val) {
                $('#' + val).show();
            });

            if (!sessionPHP.paraments.accountant.account_enabled) {
                $('#Contable').hide();
            }

            if (sessionPHP.paraments.gral_config.billing_for_service) {
                $('#Customers-lockeds').hide();
            } else {
                $('#Connections-lockeds').hide();
            }

        });
    </script>

</body>
</html>
