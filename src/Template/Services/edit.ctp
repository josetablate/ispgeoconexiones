
    
    <div class="row">
        <div class="col-xl-6">
            <div class="text-right">
               
                <?php
                
                    echo $this->Html->link(
                        '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                        ['controller' => 'services', 'action' => 'add' ],
                        [
                        'class' => 'btn btn-default', 
                        'title' => 'Editar Servicio',
                        'role' => 'button',
                        'escape' => false
                    ]);
                
                    echo $this->Html->link(
                        '<span class="glyphicon icon-pencil2" aria-hidden="true"></span>',
                        ['controller' => 'services', 'action' => 'edit',$service->id ],
                        [
                        'class' => 'btn btn-default ml-1', 
                        'title' => 'Editar Servicio',
                        'role' => 'button',
                        'escape' => false
                    ]);
                
                    echo $this->Html->link(
                        '<span class="glyphicon icon-bin" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'class' => 'btn btn-default ml-1', 
                        'title' => 'Eliminar Servicio',
                        'role' => 'button',
                        'id' =>  'btn-delete',
                        'data-id' => $service->id,
                        'data-text' =>  __('¿Está Seguro que desea eliminar el servicio <strong>{0}</strong>?', $service->name),
                        'escape' => false
                    ]);
                ?>
            </div>
        </div>
    </div>


    <div class="row">
                
        <div class="col-xl-6">
            <?= $this->Form->create($service, ['class' => 'form-load']) ?>
            <fieldset>
                
              <div class="row">
                <div class="col-xl-6">
                    <?php
                        echo $this->Form->input('name', ['label' => 'Nombre']);
                    ?>
                </div>
                <div class="col-xl-6">
                    <?php
                        echo $this->Form->input('description', ['label' => 'Descripción']);
                    ?>
                </div>
                <div class="col-xl-6">
                    <?php
                        echo $this->Form->input('price', ['label' => 'Precio Final']);
                    ?>
                </div>
                <div class="col-xl-6">
                    <?php
                        echo $this->Form->input('aliquot', ['label' => 'Alícuota', 'options' => $alicuotas_types]);
                    ?>
                </div>
                <div class="col-xl-6">
                    <?php
                        echo $this->Form->input('enabled', ['label' => 'Habilitado']);
                    ?>
                </div>
                <div class="col-xl-6">
                    <?php
                        echo $this->Form->input('account_code', ['label' => 'Cuenta', 'required' => false]);
                    ?>
                </div>
            </div>
              
                
            </fieldset>
             <?= $this->Html->link(__('Cancelar'),["controller" => "Services", "action" => "index"], ['class' => 'btn btn-default']) ?>
            <?= $this->Form->button(__('Guardar'), ['class' => 'btn-submit btn-success float-right']) ?>
            <?= $this->Form->end() ?>
        </div>
        
    </div>
    
</div>

<script type="text/javascript">
    
    
      if(!sessionPHP.paraments.accountant.account_enabled){
            $('#account-code').closest('div').hide();
        }
    
     $('#btn-delete').click(function() {

        var text = $(this).data('text');
        var id  = $(this).data('id');

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/services/delete/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#replacer').submit();
            }
        });

    });
    
</script>
