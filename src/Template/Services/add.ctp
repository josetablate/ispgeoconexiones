
    <div class="row">
        <div class="col-xl-6">
            <?= $this->Form->create($service,  ['class' => 'form-load']) ?>
                <fieldset>
                    
                    <div class="row">
                        <div class="col-xl-6">
                            <?php
                                echo $this->Form->input('name', ['label' => 'Nombre']);
                            ?>
                        </div>
                        <div class="col-xl-6">
                            <?php
                                echo $this->Form->input('description', ['label' => 'Descripción']);
                            ?>
                        </div>
                        <div class="col-xl-6">
                            <?php
                                echo $this->Form->input('price', ['label' => 'Precio Final']);
                            ?>
                        </div>
                        <div class="col-xl-6">
                            <?php
                                echo $this->Form->input('aliquot', ['label' => 'Alícuota', 'options' => $alicuotas_types, 'value' => 5]);
                            ?>
                        </div>
                        <div class="col-xl-6">
                            <?php
                                echo $this->Form->input('enabled', ['label' => 'Habilitado', 'checked' => true]);
                            ?>
                        </div>
                       
                    </div>
                    
                    
                    
                    
                 
                </fieldset>
                <?= $this->Html->link(__('Cancelar'),["controller" => "Services", "action" => "index"], ['class' => 'btn btn-default']) ?>
                <?= $this->Form->button(__('Agregar'), ['class' => 'btn-submit btn-success float-right']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
            
            
          