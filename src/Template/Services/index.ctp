<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5" >
         <?php

            echo $this->Html->link(
                '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                ['controller' => 'services', 'action' => 'add'],
                [
                'title' => 'Agregar nuevo servicio',
                'class' => 'btn btn-default',
                'escape' => false
                ]);

             echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export ml-1',
                'escape' => false
                ]);
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-hover table-bordered" id="table-services">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Precio</th>
                    <th>Alícuota</th>
                    <th>Cuenta</th>
                    <th>Creado</th>
                    <th>Clientes</th>
                    <th>Conexiones</th>
                </tr> 
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-edit',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-services', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<script type="text/javascript">
    
    var table_services = null;
    var service_selected = null;

    $(document).ready(function () {

        $('#table-services').removeClass('display');

        $('#btns-tools').hide();

        var hide_column = [];

        if (!sessionPHP.paraments.accountant.account_enabled) {
            hide_column = [3];
        }

    	table_services = $('#table-services').DataTable({
    		    "order": [[ 1, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ajax": {
                    "url": "/ispbrain/services/get_all.json",
                    "dataSrc": "services",
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { "data": "name" },
                    { "data": "description" },
                    {
                        "data": "price",
                        "render": $.fn.dataTable.render.number( '.', ',', 2, '' )
                    },
                    {
                        "data": "aliquot",
                        "render": function ( data, type, row ) {
                            return sessionPHP.afip_codes.alicuotas_types[data];
                        }
                    },
                    {
                        "data": "account",
                        "render": function ( data, type, row ) {
                             if (data) {
                                return data.code + ' ' + data.name;;
                            }
                            return '';
                        }
                    },
                    {
                        "data": "created",
                        "render": function ( data, type, row ) {
                            if (data) {
                                var created = data.split('T')[0];
                                created = created.split('-');
                                return created[2] + '/'+ created[1] + '/' + created[0];
                            }
                        }
                    },
                    { "data": "customers_count" },
                    { "data": "connections_count" },
                ],
    		     "columnDefs": [
                 { "type": "date-custom", "targets": [5] },
                 { "type": "numeric-comma", "targets": 2 },
                 { "class": "left", "targets": [0,1,4] },
                 { "class": "right", "targets": [2] },
                 { 
                    "visible": false, targets: hide_column
                 },
                 ],
    		    "language": dataTable_lenguage,
    		    "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            	"dom":
        	    	"<'row'<'col-xl-6'l><'col-xl-4'f><'col-xl-2 tools'>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});

		$('#btns-tools').show();

		$('#table-services_wrapper .tools').append($('#btns-tools').contents());

		$('#table-services tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

                table_services.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');

                service_selected = table_services.row( this ).data();
                $('.modal-services').modal('show');
            }
        });
    });

    $(".btn-export").click(function() {

        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if(result){
                $('#table-services').tableExport({tableName: 'Servicios', type:'excel', escape:'false', columnNumber: [1]});
            }
        });
    });

    $('#btn-delete').click(function() {

        var text = "¿Está Seguro que desea eliminar el Servicio?";
        var id  = service_selected.id;

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                    .append( $('<form/>').attr({'action': '/ispbrain/services/delete/', 'method': 'post', 'id': 'replacer'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer').submit();
            }
        });
    });

    $('a#btn-edit').click(function() {
       var action = '/ispbrain/services/edit/' + service_selected.id;
       window.open(action, '_self');
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
