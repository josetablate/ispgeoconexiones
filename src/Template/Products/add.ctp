<div class="row">
    <div class="col-xl-3">

        <?= $this->Form->create($product,  ['class' => 'form-load']) ?>
            <fieldset>
                <div class="form-group text required">
                    <label class="control-label" for="code">Artículo<span class='fa fa-question-circle' title='Ingrese el código o nombre del articulo.&nbsp;Con * despliega la lista completa.'  aria-hidden='true' ></span></label>
                    <input type="text" name="code" required="required" maxlength="45" id="autocomplete" class="form-control">
                    <input type="hidden" name="article_id" id="article_id" class="form-control">
                </div>
                <div class="form-group text required">
                    <label class="control-label" for="name">Nombre</label>
                    <input type="text" name="name" maxlength="45" id="name" class="form-control" required>
                </div>

                <?php
                    echo $this->Form->input('unit_price', ['label' => 'Precio Final', 'min' => 0]);
                    echo $this->Form->input('aliquot', ['label' => 'Alícuota', 'options' => $alicuotas_types, 'value' => 5]);
                    echo $this->Form->input('enabled', ['label' => 'Habilitar/Deshabilitar', 'class' => 'input-checkbox' , 'checked' => true]);
                ?>
            </fieldset>
            <?= $this->Html->link(__('Cancelar'),["controller" => "Products", "action" => "index"], ['class' => 'btn btn-default']) ?>
            <?= $this->Form->button(__('Agregar'), ['class' => 'btn-submit btn-success float-right']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function() {

        var codes = [];

         $.ajax({
            url: "<?= $this->Url->build(['controller' => 'articles', 'action' => 'getAllCodeWithoutAjax']) ?>",
            type: 'POST',
            dataType: "json",
            success: function(data) {
                $.each(data.codes, function(i, val) {
                    var ui = {id: val.id, label:val.code + ' - ' + val.name + '*', value:val.code, name:val.name};
                    codes.push(ui);
                });
            },
            error: function (jqXHR) {
                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() { 
                	  window.location.href ="/ispbrain";
                	}, 3000);

                } else {
                	generateNoty('error', 'Error al obtener los códigos.');
                }
            }
        });

        var selected = false;

        $( "#autocomplete" ).autocomplete({
        	source: codes,
            select: function( event, ui ) {
                $('#name').val(ui.item.name);
                $('#article_id').val(ui.item.id);
                $('#name').attr('desabled', true);
            }
        });
    });

</script>
