
<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5" >

        <?php

            echo $this->Html->link(
                '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                ['controller' => 'products', 'action' => 'add'],
                [
                'title' => 'Agregar nuevo producto',
                'class' => 'btn btn-default btn-add',
                'escape' => false
                ]);

             echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export',
                'escape' => false
                ]);
        ?>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered" id="table-products">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Precio ($)</th>
                    <th>Alícuota</th>
                    <th>Artículo</th>
                    <th>Cuenta</th>
                    <th>Creado</th>
                    <th>Modificado</th>
                    <th>Activo</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-edit',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-products', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Editar Producto</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                

                <form method="post" accept-charset="utf-8" role="form" action="<?=$this->Url->build(["controller" => "Products", "action" => "edit"])?>">
                    <div style="display:none;">
                        <input type="hidden" name="_method" value="POST">
                    </div>
                    
                    <div class="row">
                         <div class="col-xl-6">
                            
                            <div id="container_autocomplete">
                                 <div id="code_autocomplete">
                                    <div class="form-group text required">
                                        <label class="control-label" for="code">Articulo * <span class='fa fa-question-circle' title='Ingrese el código o nombre del articulo.&nbsp;Con * desplega la lista completa.'  aria-hidden='true' ></span></label>
                                        <input type="text" required="required" maxlength="45" id="autocomplete" class="form-control">
                                        <input type="hidden" name="article_id" id="article_id" class="form-control" >
                                        <?php echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6">

                            <div class="form-group text required">
                                <label class="control-label" for="name">Name</label>
                                <input type="text" name="name" maxlength="45" id="name" class="form-control">
                            </div>
                        </div>
                        
                        <div class="col-xl-6">

                            <?php
                                echo $this->Form->input('unit_price', ['label' => 'Percio Final', 'min' => 0]);
                            ?>
                        </div>
                        
                        <div class="col-xl-6">

                            <?php
                               
                                echo $this->Form->input('aliquot', ['label' => 'Alícuota', 'options' => $alicuotas_types]);
                                
                            ?>
                        </div>
                        
                        <div class="col-xl-6">

                            <?php
                              
                                echo $this->Form->input('account_code', ['label' => 'Código Cuenta', 'id' => 'account', 'required' => false]);
                            ?>
                        </div>
                        
                        <div class="col-xl-6">
                            
                            <?php echo $this->Form->input('enabled', ['type' => 'checkbox', 'label' => 'Habilitar/Deshabilitar']); ?>

                        </div>
                        <div class="col-xl-12">

                            <input type="hidden" name="id" id="id"/>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-success float-right" id="btn-confirm-edit">Guardar</button> 
                        </div>
                        
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var table_products = null;
    var product_selected = null;

    var codes = [];

    $(document).ready(function () {

        $.ajax({
            url: "<?= $this->Url->build(['controller' => 'Articles', 'action' => 'getAllCodeWithoutAjax']) ?>" ,
            type: 'POST',
            dataType: "json",
            success: function(data) {
                $.each(data.codes, function(i, val) {
                    var ui = {id: val.id, label:val.code + ' - ' + val.name + '*', value:val.code, name:val.name};
                    codes.push(ui);
                });
            },
            error: function (jqXHR) {
                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() { 
                	  window.location.href ="/ispbrain";
                	}, 3000);
                
                } else {
                	generateNoty('error', 'Error al obtener los códigos.');
                }
            }
        });

        var selected = false;

        $( "#autocomplete" ).autocomplete({
            appendTo: "#container_autocomplete",
        	source: codes,
            select: function( event, ui ) {
                $('#name').val(ui.item.name);
                $('#article_id').val(ui.item.id);
            }
        });

        $('#table-products').removeClass('display');
        
        $('#btns-tools').hide();
        
        var hide_column = [];
        
        if(!sessionPHP.paraments.accountant.account_enabled){
            hide_column = [4];
        }

		table_products = $('#table-products').DataTable({
		    "order": [[ 4, 'desc' ]],
		    "autoWidth": true,
		    "scrollY": '350px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "sWrapper":  "dataTables_wrapper dt-bootstrap4",
            "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/products/index.json",
                "dataSrc": "products",
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
            				  window.location.href ="/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "columns": [
                { "data": "name" },
                {
                    "data": "unit_price",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' )
                },
                {
                    "data": "aliquot",
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.alicuotas_types[data];
                    }
                },
                { "data": "article.code" },
                {
                    "data": "account",
                    "render": function ( data, type, row ) {
                         if(data){
                            return data.code + ' ' + data.name;;
                        }
                        return '';
                    }
                },
                {
                    "data": "created",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/'+ created[1] + '/' + created[0];
                        }
                    }
                },
                {
                    "data": "modified",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var modified = data.split('T')[0];
                            modified = modified.split('-');
                            return modified[2] + '/'+ modified[1] + '/' + modified[0];
                        }
                    }
                },
                {
                    "data": "enabled",
                    "render": function ( data, type, row ) {
                        var enabled = '<i class="fa fa-times" aria-hidden="true"></i>';
                        if (data) {
                            enabled = '<i class="fa fa-check" aria-hidden="true"></i>';
                        }
                        return enabled;
                    }
                },
            ],
            "columnDefs": [
                { "type": "date-custom", "targets": [5,6] },
                { "type": "numeric-comma", "targets": 1 },
                { 
                    "visible": false, targets: hide_column
                 },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            "dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-4'f><'col-xl-2 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
		});

        $('#table-products_wrapper .tools').append($('#btns-tools').contents());

        $('#btns-tools').show();
    });

    $(".btn-export").click(function() {

        bootbox.confirm('Se descargará un archivo Excel', function(result) {
            if (result) {
                $('#table-products').tableExport({tableName: 'Productos', type:'excel', escape:'false', columnNumber: [1]});
            }
        });
    });

    $('#table-products tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_products.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            product_selected = table_products.row( this ).data();

            $('.modal-products').modal('show');
        }
    });

    $('a#btn-edit').click(function() {

        $('.modal-actions').modal('hide');

        $('#modal-edit #id').val(product_selected.id);
        $('#modal-edit #name').val(product_selected.name);
        $('#modal-edit #unit-price').val(product_selected.unit_price);
        $('#modal-edit #aliquot').val(product_selected.aliquot);

        $('#modal-edit #account').val(product_selected.account_code);
        
        if(!sessionPHP.paraments.accountant.account_enabled){
            $('#modal-edit #account').closest('div').hide();
        }

        var article_id = product_selected.article_id;
    
        $.each(codes, function(i, val) {
            if (val.id == article_id) {
                $('#modal-edit #article_id').val(val.id);
                $('#modal-edit #autocomplete').val(val.value);
            }
        });

        $('#modal-edit #enabled').attr('checked', product_selected.enabled);
        // $('#modal-edit #enabled').val(product_selected.enabled ? 1 : 0);
        
        $('#modal-edit').modal('show');

    });
    
    // Toggle
    // $(':checkbox').prop('checked', function (i, value) {
    //     return !value;
    // });

    // $('#modal-edit #enabled').click(function() {

    //     if ($('#modal-edit #enabled').val() == '1') {
    //         $('#modal-edit #enabled').val(0);
    //     } else {
    //         $('#modal-edit #enabled').val(1);
    //     }
    // });
 
	$('a#btn-delete').click(function() {

        var text = "¿Está Seguro que desea eliminar el Producto?";
        var id  = product_selected.id;

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                    .append( $('<form/>').attr({'action': '/ispbrain/products/delete/', 'method': 'post', 'id': 'replacer'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer').submit();
            }
        });
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
