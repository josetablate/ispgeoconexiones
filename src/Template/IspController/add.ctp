<style type="text/css">

    #map {
		height: 370px;
	}

	#floating-panel {
		top: 10px;
		left: 25%;
		z-index: 5;
		background-color: #fff;
		padding: 5px;
		border: 1px solid #999;
		text-align: center;
		font-family: 'Roboto','sans-serif';
		line-height: 30px;
		padding-left: 10px;
	}

	#submit-map {
        padding: 10px;
        top: 2px;
    }

    #update-map {
        padding: 10px;
        top: 2px;
    }

    .slidecontainer {
        width: 100%; /* Width of the outside container */
    }

    /* The slider itself */
    .slider {
        -webkit-appearance: none;  /* Override default CSS styles */
        appearance: none;
        width: 100%; /* Full-width */
        height: 5px; /* Specified height */
        background: #d3d3d3; /* Grey background */
        outline: none; /* Remove outline */
        opacity: 0.7; /* Set transparency (for mouse-over effects on hover) */
        -webkit-transition: .2s; /* 0.2 seconds transition on hover */
        transition: opacity .2s;
        margin-bottom:15px;
    }

    /* Mouse-over effects */
    .slider:hover {
        opacity: 1; /* Fully shown on mouse-over */
    }

    /* The slider handle (use -webkit- (Chrome, Opera, Safari, Edge) and -moz- (Firefox) to override default look) */ 
    .slider::-webkit-slider-thumb {
        -webkit-appearance: none; /* Override default look */
        appearance: none;
        width: 25px; /* Set a specific slider handle width */
        height: 25px; /* Slider handle height */
        background: #4CAF50; /* Green background */
        cursor: pointer; /* Cursor on hover */
        border-radius: 30px;
    }

    .slider::-moz-range-thumb {
        width: 25px; /* Set a specific slider handle width */
        height: 25px; /* Slider handle height */
        background: #4CAF50; /* Green background */
        cursor: pointer; /* Cursor on hover */
        border-radius: 30px;
    }

    .my-hidden {
        display: none;
    }

    .radio-color {
        width: 15% !important;
        height: 35px;
    }

</style>

<div class="row">
    <div class="col-xl-12">

        <?= $this->Form->create($controller, ['id' => 'form-add-controller']) ?>
        <fieldset>

            <div class="row">

                <div class="col-xl-3">
                    <?php 
                        echo $this->Form->input('name', ['label' => 'Nombre', 'required' => true]);
                        echo $this->Form->input('description', ['label' => 'Descripción', 'type' => 'textarea', 'rows' => 2]);
                        echo $this->Form->input('trademark', ['type' => 'hidden', 'label' => 'Marca', "value" => 'mikrotik', 'id' => 'trademark']);
                        echo $this->Form->input('message_method', ['type' => 'hidden', "value" => 'met_a']);
                        echo $this->Form->input('connect_to', ['label' => 'Conectar a', 'autocomplete' => 'off']);
                        echo $this->Form->input('port_grap', ['label' => 'Puerto Gráficas']);
                    ?>
                </div>

                <div class="col-xl-3">
                    <?php

                        echo $this->Form->input('username', ['label' => 'Usuario', 'value' => '']);
                        echo $this->Form->input('password', ['label' => 'Contraseña', 'value' => '']);
                        echo $this->Form->input('enabled', ['label' => 'Habilitado']);
                        echo $this->Form->input('port', ['label' => 'Puerto API']);
                        echo $this->Form->input('port_ssl', ['label' => 'Puerto API SSL']);
                        echo $this->Form->input('template', ['type' => 'hidden']);
                        echo $this->Form->input('integration', ['label' => 'Sincronización', "type" => 'checkbox', 'checked' => true]);
                    ?>
                </div>

                <div class="col-xl-6">

                    <table class="table table-hover table-bordered" id="table-templates">
                        <thead>
                            <tr>
                                <th>Tipo de conexiones</th>
                            </tr>
                        </thead>
                    </table>

                </div>

                <div class="data-map my-hidden col-lg-5 col-xl-5">
                    <div class="slidecontainer">
                        <input type="range" min="500" max="6000" step="100" value="" class="slider" id="myRange">
                        <p>Radio: <span id="demo"></span></p>
                    </div>
                </div>

                <div class="data-map my-hidden col-xl-4">
                    <div class="form-inline color">
                        <label class="control-label" for="color">Color</label>
                        <?= $this->Form->number('color', ['id' => 'color', 'label' => 'Color', 'type' => 'color', 'value' => '#FF0000', 'class' => 'ml-2 radio-color'])?>
                    </div>
                </div>

                <div class="col-lg-12 col-xl-12">

                    <?php
                        echo $this->Form->input('lat', ['type' => 'hidden', 'id' => 'coodmapslat']);
                        echo $this->Form->input('lng', ['type' => 'hidden', 'id' => 'coodmapslng']);
                        echo $this->Form->input('radius', ['type' => 'hidden', 'id' => 'radius']);
                    ?>
                	<div id="floating-panel">
            		    <div class="row">
            		        <div class="col-md-12">
                		        <div class="input-group">
                		            <span class="input-group-btn">
                                        <button id="update-map" title="Recargar Mapa" class="btn btn-info" type="button"><span class="text-white glyphicon icon-map2" aria-hidden="true"></span></button>
                                    </span>
                                    <input id="addressmap" type="text" class="form-control margin-top-5" value="" placeholder="Calle #, Ciudad, Provincia">
                                    <span class="input-group-btn">
                                        <button id="submit-map" title="Buscar Dirección" class="btn btn-success" type="button"><span class="text-white glyphicon icon-search" aria-hidden="true"></span></button>
                                    </span>
                                </div>
            		        </div>
            		    </div>
            		</div>
    
            		<div id="map" class="mb-3"></div>
                </div>
            </div>

        </fieldset>

        <?= $this->Form->button(__('Agregar'), ['class' => 'mt-2']) ?>
        <?= $this->Form->end() ?>

    </div>
</div>

<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2K3zoK46JybWPzQbhfQQqIIbdZ_3WcQs&libraries=places&callback=initAutocomplete">
</script>

<script type="text/javascript">

    $(document).ready(function() {
       
        var table_templates = null;
        var template_selected = null;

        $('#table-templates').removeClass('display');

		table_templates = $('#table-templates').DataTable({
		    "ajax": {
                "url": "/ispbrain/isp-controller/get_templates.json",
                "dataSrc": "templates",
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
            				  window.location.href ="/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "columns": [
                { 
                    "data": 'name',
                    "className": 'left',
                    "render": function ( data, type, row ) {
                        return "<span class='font-weight-bold text-left'>" + data + "</span><br><span class='text-muted'>" + row.description+"</span>";
                    }
                },
            ],
		    "autoWidth": true,
		    "scrollY": '305px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "paging": false,
		    "ordering": false,
		    "info": false,
		    "searching": false,
		    "createdRow" : function( row, data, index ) {
               row.id = data.value;
            },
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            "dom":
    	    	"<'row'<'col-xl-12'f>>" +
        		"<'row'<'col-xl-12'tr>>"
		});


        $('#table-templates tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                table_templates.$('tr.selected').removeClass('selected');
                $('#template').val('');
                $(this).addClass('selected');
                template_selected = table_templates.row( this ).data();
                $('#template').val(template_selected.value);
               
            }
        });

        $( "#form-add-controller" ).submit(function( event ) {

            if (!template_selected) {
                generateNoty('warning', 'Debe seleccionar un Template');
                event.preventDefault();
            }
        });

        initMap();

    });

    $('#update-map').click(function() {
        initMap();
    });

    $('form input').on('keypress', function(e) {
        return e.which !== 13;
    });

    var map = null;
    var geocoder = null;
    var marker = null;
    var paraments;
    var slider = document.getElementById("myRange");
    var output = document.getElementById("demo");
    output.innerHTML = slider.value;

    slider.oninput = function() {
        output.innerHTML = this.value;
        marker.setRadius(parseInt(this.value));
        $('#radius').val(this.value);
    }

    $("#color").on("change",function() {
        marker.setOptions({
            fillColor: $(this).val(),
            strokeColor: $(this).val()
        });
    });

	function initMap() {

	    console.log('initMap');

		geocoder = new google.maps.Geocoder;

		var myLatlng = {lat: sessionPHP.paraments.system.map.lat, lng: sessionPHP.paraments.system.map.lng};

		var mapOptions = {
			zoom: 15,
			center: myLatlng,
			mapTypeId: 'hybrid'   // roadmap, satellite, hybrid, terrain 
		};

		map = new google.maps.Map(document.getElementById('map'), mapOptions);

		document.getElementById('submit-map').addEventListener('click', function() {
			geocodeAddress(geocoder, map);
		});

		var init = {lat: sessionPHP.paraments.system.map.lat, lng: sessionPHP.paraments.system.map.lng};

        var latLng = new google.maps.LatLng(init.lat, init.lng);

        map.setCenter(latLng);

		map.addListener('click', function(event) {
			addMarker(event.latLng);	    
		});
    }

    function refreshMap() {
        console.log('refreshMap');
        google.maps.event.trigger(map, 'resize');
    }

    $('input#addressmap').keypress(function(event) {

        if ( event.which == 13 ) {
            geocodeAddress(geocoder, map);
        }
    });

	function addMarker(location) {

		if (marker != null) {
			clearMarkers();
		} else {
		    $('.data-map').removeClass('my-hidden');
		}
		$('#myRange').val(500);
		$('#radius').val(500);
		output.innerHTML = 500;

		marker = new google.maps.Circle({
            strokeColor: '#000000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#000000',
            fillOpacity: 0.35,
            map: map,
            center: location,
            radius: 500
        });
		marker.setMap(map);	

        var lat = location.lat();

        var lng = location.lng();

		$('#coodmapslat').val(lat);
		$('#coodmapslng').val(lng);	
	}

	function clearMarkers() {
		marker.setMap(null);
	}

	function geocodeAddress(geocoder, map) {

		var address = document.getElementById('addressmap').value;

		geocoder.geocode({
			'address': address
			}, function(results, status) {

				if (status === google.maps.GeocoderStatus.OK) {	
					addMarker(results[0].geometry.location)  	    
					map.setCenter(results[0].geometry.location);
					map.setZoom(14);

    			} else {
    				if (status == google.maps.GeocoderStatus.ZERO_RESULTS) {
    			        bootbox.alert('No se encontraron Resultados');
    			    } else {
    			        window.alert('No se encontraron Resultados. Estado: ' + status);
    			    }
    			}
		});
	}

</script>

<script type='text/javascript'>

    function initialize() {

        var input = document.getElementById('addressmap');

        var autocomplete = new google.maps.places.Autocomplete(input);
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>
