<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5" >

        <?php
            echo $this->Html->link(
                '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                ['controller' => 'IspController', 'action' => 'add'],
                [
                'title' => 'Agregar nuevo controlador',
                'class' => 'btn btn-default btn-add',
                'escape' => false
                ]);
                
            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel"  aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export',
                'escape' => false
                ]);
        ?>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-hover" id="table-controlers">
            <thead>
                <tr>
                    <th>#</th>
                    <th></th>
                    <th>Nombre</th>
                    <th>Marca</th>
                    <th>Conexión + IP + Límite</th>
                    <th>Conectar a</th>
                    <th>Conexiones</th>
                    <th>Usuario</th>
                    <th>TLS</th>
                    <th>Sinc</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id' =>  'btn-edit',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];
    
     $buttons[] = [
        'id' =>  'btn-config',
        'name' =>  'Configurar',
        'icon' =>  'icon-wrench',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id' =>  'btn-sync',
        'name' =>  'Sincronizar',
        'icon' =>  'icon-loop2',
        'type' =>  'btn-secondary'
    ];

     $buttons[] = [
        'id' =>  'btn-clone',
        'name' =>  'Duplicar',
        'icon' =>  'far fa-copy',
        'type' =>  'btn-info'
    ];
    
    $buttons[] = [
        'id' =>  'btn-test',
        'name' =>  'Probar Conexiòn',
        'icon' =>  'icon-cloud-check',
        'type' =>  'btn-secondary'
    ];
    
     $buttons[] = [
        'id' =>  'btn-delete',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-controllers', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<script type="text/javascript">

    var table_controlers = null;
    var controller_selected = null;

    $(document).ready(function () {

        $('#table-controlers').removeClass('display');

		table_controlers = $('#table-controlers').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '350px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/IspController/get_controllers.json",
                "dataSrc": "controllers",
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
            				  window.location.href ="/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "columns": [
                { "data": "id" },
                { 
                    "data": "enabled",
                    "render": function ( data, type, row ) {
                        var enabled = '<i class="fa fa-times" aria-hidden="true"></i>';

                        if (data) {
                            enabled = '<i class="fa fa-check" aria-hidden="true"></i>';
                        }

                        return enabled;
                    }
                },
                { "data": "name" },
                { "data": "trademark_name" },
                { "data": "template_name" },
                { 
                    "data": "connect_to",
                    "render": function ( data, type, row ) {
                        if(type == 'display'){
                            
                            if(row.use_tls){
                                return data + ':' + row.port_ssl;
                            }
                            
                            return data + ':' + row.port;
                        }
                        return data;
                    }
                    
                },
                { "data": "connections_amount" },
                { "data": "username" },
                { 
                    "data": "use_tls",
                    "render": function ( data, type, row ) {
                        var enabled = '<i class="glyphicon glyphicon icon-unlocked red" aria-hidden="true"></i>';

                        if (data) {
                            enabled = '<i class="glyphicon glyphicon icon-lock green" aria-hidden="true"></i>';
                        }

                        return enabled;
                    }
                },
                { 
                    "data": "integration",
                    "render": function ( data, type, row ) {
                        var enabled = 'NO';

                        if (data) {
                            enabled = 'SI';
                        }

                        return enabled;
                    }
                },
            ],
            "columnDefs": [
                { "type": "ip-address", "targets": [5] },
            ],
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
		});

        $('#table-controlers_filter').closest('div').before($('#btns-tools').contents());
    });

    $(".btn-export").click(function() {

        bootbox.confirm('Se descargará un archivo Excel', function(result) {
            if (result) {
                $('#table-controlers').tableExport({tableName: 'Controladores', type:'excel', escape:'false'});
            }
        });
        
    });

    $('#table-controlers tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_controlers.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            controller_selected = table_controlers.row( this ).data();

            $('.modal-controllers').modal('show');
        }
    });
     

    $('#btn-delete').click(function() {

        var text = '¿Está seguro que desea eliminar el controlador?';
        var id  = controller_selected.id;

        bootbox.confirm(text, function(result) {
            if(result){
                $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/IspController/delete/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#replacer').submit();
            }
        });
    });
     
    $('#btn-clone').click(function() {

        var text = '¿Está seguro que desea duplicar el controllador: '+controller_selected.name+' ?';
        bootbox.confirm(text, function(result) {
             
            if(result){               
                       
                 var id  = controller_selected.id;
                 
                $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/IspController/cloneController/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#replacer').submit();
            }
        });
    });

    $('a#btn-edit').click(function() {
        var template = controller_selected.template;
        var action = '/ispbrain/IspController' + template +'/edit/' + controller_selected.id;
        window.open(action, '_blank');
    });
    
    $('a#btn-config').click(function() {
        var template = controller_selected.template;
        var action = '/ispbrain/IspController' + template +'/config/' + controller_selected.id;
        window.open(action, '_blank');
    });
    
     $('a#btn-sync').click(function() {
        var template = controller_selected.template;
        var action = '/ispbrain/IspController' + template + '/sync/' + controller_selected.id;
        window.open(action, '_blank');
    });
    
     $('a#btn-test').click(function() {
        var template = controller_selected.template;
        var action = '/ispbrain/IspController/getStatus/' + controller_selected.id;
        window.open(action, '_self');
    });
    
    


    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

   

</script>
