<style type="text/css">

    .vertical-table {
        width: 100%;
        color: #696868;
    }

    table > tbody > tr {
        border-bottom: 1px solid #e5e5e5;
    }

    .my-hidden {
        display: none;
    }

</style>

<div class="row">
    <div class="col-xl-8">
        <legend class="sub-title"><?=  __('Caja: ' . $cashEntity->name) . ' - ' . __(' Parte N° {0}', $number) ?></legend>
    </div>

    <div class="col-xl-9">
        <ul class="nav nav-tabs" id="myTab" role="tablist">

            <li class="nav-item">
                <a class="nav-link active" id="efectivo-tab" data-target="#efectivo" role="tab" aria-controls="efectivo" aria-expanded="true">Efectivo</a>
            </li>

            <li class="nav-item">
                <a class="nav-link " id="other_payment-tab" data-target="#other_payment" role="tab" aria-controls="other_payment" aria-expanded="false">Otros medios de pagos</a>
            </li>

        </ul>

        <div class="tab-content" id="myTabContent">

            <div class="tab-pane fade show active" id="efectivo" role="tabpanel" aria-labelledby="efectivo-tab">
                <?= $this->fetch('efectivo') ?>
            </div>

            <div class="tab-pane fade" id="other_payment" role="tabpanel" aria-labelledby="other_payment-tab">
                <?= $this->fetch('other_payment') ?>
            </div>

        </div>
    </div>

</div>

<script type="text/javascript">

    $(document).ready(function() {

        $('#myTab a').click(function (e) {

            e.preventDefault();

            var target = $(this).data('target');
            $('#myTab a.active').removeClass('active');

            $('#myTabContent .active').fadeOut(100, function() {
                $('#myTabContent .active').removeClass('active');

                $(target).fadeIn(100, function() {
                    $(target).addClass('active show');
                    $(target + '-tab').addClass('active');
                    $(target + '-tab').trigger('shown.bs.tab');
                });
            });
        });
    });

</script>

