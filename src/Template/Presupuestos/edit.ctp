<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Presupuesto $presupuesto
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $presupuesto->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $presupuesto->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Presupuestos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Connections'), ['controller' => 'Connections', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Connection'), ['controller' => 'Connections', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Presupuesto Concepts'), ['controller' => 'PresupuestoConcepts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Presupuesto Concept'), ['controller' => 'PresupuestoConcepts', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="presupuestos form large-9 medium-8 columns content">
    <?= $this->Form->create($presupuesto) ?>
    <fieldset>
        <legend><?= __('Edit Presupuesto') ?></legend>
        <?php
            echo $this->Form->control('concept_type');
            echo $this->Form->control('date');
            echo $this->Form->control('date_start', ['empty' => true]);
            echo $this->Form->control('date_end', ['empty' => true]);
            echo $this->Form->control('duedate');
            echo $this->Form->control('tipo_comp');
            echo $this->Form->control('pto_vta');
            echo $this->Form->control('num');
            echo $this->Form->control('subtotal');
            echo $this->Form->control('sum_tax');
            echo $this->Form->control('discount');
            echo $this->Form->control('total');
            echo $this->Form->control('company_name');
            echo $this->Form->control('company_address');
            echo $this->Form->control('company_cp');
            echo $this->Form->control('company_city');
            echo $this->Form->control('company_phone');
            echo $this->Form->control('company_fax');
            echo $this->Form->control('company_ident');
            echo $this->Form->control('company_email');
            echo $this->Form->control('company_web');
            echo $this->Form->control('company_responsible');
            echo $this->Form->control('customer_code');
            echo $this->Form->control('customer_name');
            echo $this->Form->control('customer_address');
            echo $this->Form->control('customer_cp');
            echo $this->Form->control('customer_city');
            echo $this->Form->control('customer_country');
            echo $this->Form->control('customer_ident');
            echo $this->Form->control('customer_doc_type');
            echo $this->Form->control('customer_responsible');
            echo $this->Form->control('cond_vta');
            echo $this->Form->control('comments');
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('printed');
            echo $this->Form->control('resto');
            echo $this->Form->control('portal_sync');
            echo $this->Form->control('connection_id', ['options' => $connections, 'empty' => true]);
            echo $this->Form->control('business_id');
            echo $this->Form->control('company_ing_bruto');
            echo $this->Form->control('company_init_act');
            echo $this->Form->control('period');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
