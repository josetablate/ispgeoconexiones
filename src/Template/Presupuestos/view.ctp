<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Presupuesto $presupuesto
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Presupuesto'), ['action' => 'edit', $presupuesto->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Presupuesto'), ['action' => 'delete', $presupuesto->id], ['confirm' => __('Are you sure you want to delete # {0}?', $presupuesto->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Presupuestos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Presupuesto'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Connections'), ['controller' => 'Connections', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Connection'), ['controller' => 'Connections', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Presupuesto Concepts'), ['controller' => 'PresupuestoConcepts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Presupuesto Concept'), ['controller' => 'PresupuestoConcepts', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="presupuestos view large-9 medium-8 columns content">
    <h3><?= h($presupuesto->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Tipo Comp') ?></th>
            <td><?= h($presupuesto->tipo_comp) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Company Name') ?></th>
            <td><?= h($presupuesto->company_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Company Address') ?></th>
            <td><?= h($presupuesto->company_address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Company Cp') ?></th>
            <td><?= h($presupuesto->company_cp) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Company City') ?></th>
            <td><?= h($presupuesto->company_city) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Company Phone') ?></th>
            <td><?= h($presupuesto->company_phone) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Company Fax') ?></th>
            <td><?= h($presupuesto->company_fax) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Company Ident') ?></th>
            <td><?= h($presupuesto->company_ident) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Company Email') ?></th>
            <td><?= h($presupuesto->company_email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Company Web') ?></th>
            <td><?= h($presupuesto->company_web) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Customer Name') ?></th>
            <td><?= h($presupuesto->customer_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Customer Address') ?></th>
            <td><?= h($presupuesto->customer_address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Customer Cp') ?></th>
            <td><?= h($presupuesto->customer_cp) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Customer City') ?></th>
            <td><?= h($presupuesto->customer_city) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Customer Country') ?></th>
            <td><?= h($presupuesto->customer_country) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Customer Ident') ?></th>
            <td><?= h($presupuesto->customer_ident) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cond Vta') ?></th>
            <td><?= h($presupuesto->cond_vta) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Comments') ?></th>
            <td><?= h($presupuesto->comments) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $presupuesto->has('user') ? $this->Html->link($presupuesto->user->username, ['controller' => 'Users', 'action' => 'view', $presupuesto->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Printed') ?></th>
            <td><?= h($presupuesto->printed) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Connection') ?></th>
            <td><?= $presupuesto->has('connection') ? $this->Html->link($presupuesto->connection->id, ['controller' => 'Connections', 'action' => 'view', $presupuesto->connection->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Company Ing Bruto') ?></th>
            <td><?= h($presupuesto->company_ing_bruto) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Company Init Act') ?></th>
            <td><?= h($presupuesto->company_init_act) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Period') ?></th>
            <td><?= h($presupuesto->period) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($presupuesto->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Concept Type') ?></th>
            <td><?= $this->Number->format($presupuesto->concept_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pto Vta') ?></th>
            <td><?= $this->Number->format($presupuesto->pto_vta) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Num') ?></th>
            <td><?= $this->Number->format($presupuesto->num) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Subtotal') ?></th>
            <td><?= $this->Number->format($presupuesto->subtotal) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sum Tax') ?></th>
            <td><?= $this->Number->format($presupuesto->sum_tax) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Discount') ?></th>
            <td><?= $this->Number->format($presupuesto->discount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Total') ?></th>
            <td><?= $this->Number->format($presupuesto->total) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Company Responsible') ?></th>
            <td><?= $this->Number->format($presupuesto->company_responsible) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Customer Code') ?></th>
            <td><?= $this->Number->format($presupuesto->customer_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Customer Doc Type') ?></th>
            <td><?= $this->Number->format($presupuesto->customer_doc_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Customer Responsible') ?></th>
            <td><?= $this->Number->format($presupuesto->customer_responsible) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Resto') ?></th>
            <td><?= $this->Number->format($presupuesto->resto) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Business Id') ?></th>
            <td><?= $this->Number->format($presupuesto->business_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($presupuesto->date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date Start') ?></th>
            <td><?= h($presupuesto->date_start) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date End') ?></th>
            <td><?= h($presupuesto->date_end) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Duedate') ?></th>
            <td><?= h($presupuesto->duedate) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Portal Sync') ?></th>
            <td><?= $presupuesto->portal_sync ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Presupuesto Concepts') ?></h4>
        <?php if (!empty($presupuesto->presupuesto_concepts)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col"><?= __('Code') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Price') ?></th>
                <th scope="col"><?= __('Quantity') ?></th>
                <th scope="col"><?= __('Sum Price') ?></th>
                <th scope="col"><?= __('Sum Tax') ?></th>
                <th scope="col"><?= __('Discount') ?></th>
                <th scope="col"><?= __('Total') ?></th>
                <th scope="col"><?= __('Unit') ?></th>
                <th scope="col"><?= __('Presupuesto Id') ?></th>
                <th scope="col"><?= __('Tax') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($presupuesto->presupuesto_concepts as $presupuestoConcepts): ?>
            <tr>
                <td><?= h($presupuestoConcepts->id) ?></td>
                <td><?= h($presupuestoConcepts->type) ?></td>
                <td><?= h($presupuestoConcepts->code) ?></td>
                <td><?= h($presupuestoConcepts->description) ?></td>
                <td><?= h($presupuestoConcepts->price) ?></td>
                <td><?= h($presupuestoConcepts->quantity) ?></td>
                <td><?= h($presupuestoConcepts->sum_price) ?></td>
                <td><?= h($presupuestoConcepts->sum_tax) ?></td>
                <td><?= h($presupuestoConcepts->discount) ?></td>
                <td><?= h($presupuestoConcepts->total) ?></td>
                <td><?= h($presupuestoConcepts->unit) ?></td>
                <td><?= h($presupuestoConcepts->presupuesto_id) ?></td>
                <td><?= h($presupuestoConcepts->tax) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PresupuestoConcepts', 'action' => 'view', $presupuestoConcepts->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PresupuestoConcepts', 'action' => 'edit', $presupuestoConcepts->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PresupuestoConcepts', 'action' => 'delete', $presupuestoConcepts->id], ['confirm' => __('Are you sure you want to delete # {0}?', $presupuestoConcepts->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
