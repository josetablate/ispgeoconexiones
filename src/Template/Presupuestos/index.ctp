<style type="text/css">

    .my-hidden {
        display: none;
    }

</style>

<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5">

        <?php 

            echo $this->Html->link(
                '<span class="fas fa-sync-alt" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Actualizar la tabla',
                'class' => 'btn btn-default btn-update-table ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-eye" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Ocultar o Mostrar Columnas',
                'class' => 'btn btn-default btn-hide-column ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon fas fa-search" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Buscar por Columnas',
                'class' => 'btn btn-default btn-search-column ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla Excell',
                'class' => 'btn btn-default btn-export ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<i class="fas fa-download"></i>',
                'javascript:void(0)',
                [
                'title' => 'Descargar facturas',
                'class' => 'btn btn-default btn-download ml-1',
                'escape' => false
            ]);

        ?>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
         <table class="table table-bordered table-hover" id="table-presupuestos">
            <thead>
                <tr>
                    <th>Fecha</th>               <!--0-->
                    <th>Pto. Vta.</th>           <!--1-->
                    <th>Número</th>              <!--2-->
                    <th>Comentario</th>          <!--3-->
                    <th>Vencimiento</th>         <!--4-->
                    <th>Subtotal</th>            <!--5-->
                    <th>Impuestos</th>           <!--6-->
                    <th>Total</th>               <!--7-->
                    <th>Usuario</th>             <!--8-->
                    <th>Código</th>              <!--9-->
                    <th>Tipo</th>                <!--10-->
                    <th>Nro</th>                 <!--11-->
                    <th>Nombre</th>              <!--12-->
                    <th>Domicilio</th>           <!--13-->
                    <th>Ciudad</th>              <!--14-->
                    <th>Empresa Facturación</th> <!--15-->
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade modal-generate-invoice-from-presupuesto" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabel">Facturar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                
                <div class="row">
                    
                    <div class="col-6">
                        <?php
                         echo $this->Form->input('business_id', ['options' => $business,  'label' =>  'Empresa', 'value' => '']); 
                        ?>
                    </div>
                    <div class="col-6">
                         <?php
                         echo $this->Form->input('type', ['options' => [],  'label' =>  'Tipo comprobante', 'required' => true]);
                        ?>
                    </div>
                    
                 </div>  
                 
                <div class="row">
                    
                    <div class="col-6">
                        <label class="control-label" for="">Fecha de emisión</label>
                        <div class='input-group date' id='created-datetimepicker'>
                            <input name="created" id="created"  type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar mr-0">
                                <span class="glyphicon icon-calendar mr-0"></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-6">
                         <?php
                         echo $this->Form->input('concept_type', ['label' => 'Tipo concepto', 'options' => $concept_types, 'value' => 2, 'id' => 'concept_type'])
                        ?>
                    </div>
                    
                 </div>
                 
                <div class="row">
                     <div class="col-12">
                         <legend class="sub-title-sm">Período</legend>
                     </div>
                 </div>
                 
                <div class="row">
                     <div class="col-6">
                        <label class="control-label" for="">Desde</label>
                        <div class='input-group date' id='date_start-datetimepicker'>
                            <input name="date_start" id="date_start"  type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar mr-0">
                                <span class="glyphicon icon-calendar mr-0"></span>
                            </span>
                        </div>
                     </div>
                     <div class="col-6">
                        <label class="control-label" for="">Hasta</label>
                        <div class='input-group date' id='date_end-datetimepicker'>
                            <input name="date_end" id="date_end"  type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar mr-0">
                                <span class="glyphicon icon-calendar mr-0"></span>
                            </span>
                        </div>
                     </div>
                 </div>
                 
                <div class="row mt-2">
                     <div class="col-12">
                        <label class="control-label" for="">Venc. de Pago</label>
                        <div class='input-group date' id='duedate-datetimepicker'>
                            <input name="duedate" id="duedate"  type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar mr-0">
                                <span class="glyphicon icon-calendar mr-0"></span>
                            </span>
                        </div>
                     </div>
                </div>
                 
                <div class="row mt-4">
                     <div class="col-12">
                         <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-danger btn-generate float-right">Confirmar</button>
                     </div>
                </div>
       
            </div>
        </div>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id' =>  'btn-print-presupuesto',
        'name' =>  'Imprimir',
        'icon' =>  'icon-printer',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-go-customer',
        'name' =>  'Ficha del Cliente',
        'icon' =>  'glyphicon icon-profile',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-edit-administrative-movement',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-success'
    ];
    
    $buttons[] = [
        'id'   =>  'btn-generate-invoice-from-presupuesto',
        'name' =>  'Convertir',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-info'
    ];

    $buttons[] = [
        'id'   =>  'btn-send-email-presupuesto',
        'name' =>  'Enviar Correo',
        'icon' =>  'fab fa-telegram-plane',
        'type' =>  'btn-success'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete',
        'name' =>  'Eliminar',
        'icon' =>  'fa fa-times',
        'type' =>  'btn-danger'
    ];
    
    echo $this->element('modal_preloader');

    echo $this->element('actions', ['modal'=> 'modal-presupuestos', 'title' => 'Acciones', 'buttons' => $buttons ]);
    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-presupuestos']);
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-presupuestos']);
    echo $this->element('AdministrativeMovement/edit_administrative_movement', ['modal' => 'modal-edit-administrative-movement', 'title' => 'Editar']);
    echo $this->element('Email/send', ['modal'=> 'modal-send-email', 'title' => 'Seleccionar Plantilla', 'mass_emails_templates' => $mass_emails_templates ]);
?>

<script type="text/javascript">

        var table_presupuestos = null;
        var presupuesto_selected  = null;
    
        var customer_selected = null;
        var move_selected = null;
        var table_selected = null;
    
        var model = null;
        var model_id = null;
        var mass_emails_templates = null;
        
         $('.modal-generate-invoice-from-presupuesto #created-datetimepicker').datetimepicker({
            locale: 'es',
            defaultDate: new Date(),
            format: 'DD/MM/YYYY'
        });
    
        $('.modal-generate-invoice-from-presupuesto #date_start-datetimepicker').datetimepicker({
            locale: 'es',
            defaultDate: new Date(),
            format: 'DD/MM/YYYY'
        });
    
        $('.modal-generate-invoice-from-presupuesto #date_end-datetimepicker').datetimepicker({
            locale: 'es',
            defaultDate: new Date(),
            format: 'DD/MM/YYYY'
        });
    
        var users = null;
         
        $('.modal-generate-invoice-from-presupuesto #duedate-datetimepicker').datetimepicker({
            locale: 'es',
            defaultDate: new Date(),
            format: 'DD/MM/YYYY'
        });
    
        $(document).ready(function () {
        
        $('.modal-generate-invoice-from-presupuesto #business-id').change(function() {

           var selected = $(this).val() ;

           business_selected = null;

           $('.modal-generate-invoice-from-presupuesto #type').empty();

           $.each(sessionPHP.paraments.invoicing.business, function(i, business) {

                if (selected == business.id) {

                    business_selected = business;
                    
                    $('.modal-generate-invoice-from-presupuesto #type').append('<option value="">Seleccione Tipo</option>');

                    if (business.responsible == 1) { //ri

                        if (business.webservice_afip.crt != '') {
                            $('.modal-generate-invoice-from-presupuesto #type').append('<optgroup label="Facturas"><option value="XXX">PRESU X</option><option value="001">FACTURA A</option><option value="006">FACTURA B</option></optgroup>');
                        } else {
                            $('.modal-generate-invoice-from-presupuesto #type').append('<optgroup label="Facturas"><option value="XXX">PRESU X</option></optgroup>');
                        }
                    } else if (business.responsible == 6) { //monotributerou
                       
                        if (business.webservice_afip.crt != '') {
                            $('.modal-generate-invoice-from-presupuesto #type').append('<optgroup label="Facturas"><option value="XXX">PRESU X</option><option value="011">FACTURA C</option></optgroup>');
                        } else {
                            $('.modal-generate-invoice-from-presupuesto #type').append('<optgroup label="Facturas"><option value="XXX">PRESU X</option></optgroup>');
                        }
                   }
               }
           });
        });

        $('.modal-generate-invoice-from-presupuesto #business-id').change();

        mass_emails_templates = <?= json_encode($mass_emails_templates) ?>;

        $('.btn-hide-column').click(function() {
            $('.modal-hide-columns-presupuestos').modal('show');
        });

        $('.btn-search-column').click(function() {
            $('.modal-search-columns-presupuestos').modal('show');
        });

        $('#table-presupuestos').removeClass('display');

        $('#btns-tools').hide();

        loadPreferences('presupuestos-index', [5, 6, 8, 13, 14]);

		table_presupuestos = $('#table-presupuestos').DataTable({
		    "order": [[ 0, 'desc' ]],
            "deferRender": true,
            "processing": true,
            "serverSide": true,
             "ajax": {
                "url": "/ispbrain/Presupuestos/get_presupuestos.json",
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "drawCallback": function( settings ) {
                var flag = true;
            	switch (settings.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
            	    if (table_presupuestos) {

                        var tableinfo = table_presupuestos.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }
                    }
            	}
            },
		    "scrollY": true,
		    "scrollY": '450px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
		    "columns": [
                { 
                    "className": "",
                    "data": "date",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        var date = data.split('T')[0];
                        date = date.split('-');
                        return date[2] + '/' + date[1] + '/' + date[0];
                    }
                },
                { 
                    "className": "",
                    "data": "pto_vta",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return  pad(data, 4);
                    }
                },
                { 
                    "className": "",
                    "data": "num",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 8);
                    }
                },
                { 
                    "className": " left",
                    "data": "comments",
                    "type": "string"
                },
                { 
                    "className": "",
                    "data": "duedate",
                    "type": "date",
                    "render": function ( data, type, row ) {

                        var duedate = data.split('T')[0];
                        duedate = duedate.split('-');

                        // if (type == 'display') {

                        //     if (row.paid == null) {

                        //         var date = new Date(duedate[1] + '/' + duedate[2] + '/' + duedate[0]);
                        //         var todayDate = new Date();

                        //         duedate =  duedate[2] + '/' + duedate[1] + '/' + duedate[0];

                        //         if (date < todayDate) {
                        //             return "<span class='text-danger'>" + duedate + "</span>";
                        //         } else {
                        //             return "<span class='text-info'>" + duedate + "</span>";
                        //         }
                        //     }
                        // }

                        duedate =  duedate[2] + '/' + duedate[1] + '/' + duedate[0];
                        return duedate;
                    }
                },
                { 
                    "className": " right",
                    "data": "subtotal",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' )
                },
                { 
                    "className": " right",
                    "data": "sum_tax",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' )
                },
                { 
                    "className": " right text-info",
                    "data": "total",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' )
                },
                { 
                    "className": " left",
                    "data": "user.name",
                    "type": "string"
                },
                { 
                    "className": "",
                    "data": "customer_code",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                {
                    "className": " center",
                    "data": "customer_doc_type",
                    "type": "options",
                    "options": sessionPHP.afip_codes.doc_types,
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.doc_types[data];
                    }
                },
                {
                    "className": " center",
                    "data": "customer_ident",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (data) {
                            ident = data;
                        }
                        return ident;
                    }
                },
                { 
                    "className": " left",
                    "data": "customer_name",
                    "type": "string"
                },
                { 
                    "className": " left",
                    "data": "customer_address",
                    "type": "string"
                },
                {
                    "className": " left",
                    "data": "customer_city",
                    "type": "string"
                },
                { 
                    "data": "business_id",
                    "type": "options_obj",
                    "options": sessionPHP.paraments.invoicing.business,
                    "render": function ( data, type, row ) {
                        var business_name = '';
                        $.each(sessionPHP.paraments.invoicing.business, function (i, b) {
                            if (b.id == data) {
                                business_name = b.name;
                            }
                        });
                        return business_name;
                    }
                },
            ],
		    "columnDefs": [
                { 
                    "visible": false, targets: hide_columns
                },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
            "initComplete": function(settings, json) {
            },
		    "language": dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
        	"dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
		});

		$('.btn-update-table').click(function() {
             if (table_presupuestos) {
                table_presupuestos.ajax.reload();
            }
        });
 
        $('#table-presupuestos_wrapper .tools').append($('#btns-tools').contents());

        $('#table-presupuestos').on( 'init.dt', function () {
            createModalHideColumn(table_presupuestos, '.modal-hide-columns-presupuestos');
            createModalSearchColumn(table_presupuestos, '.modal-search-columns-presupuestos');
        });

        $('#btns-tools').show();

        $('#table-presupuestos tbody').on( 'click', 'tr', function () {

            if (!$(this).closest('tr').find('.dataTables_empty').length) {

                table_presupuestos.$('tr.selected').removeClass('selected');
                $(this).closest('tr').addClass('selected');
                $('#btn-edit-administrative-movement').addClass('my-hidden');

                presupuesto_selected = table_presupuestos.row( this ).data();
                if (presupuesto_selected.customer.billing_for_service) {
                    move_selected = {
                        'id': presupuesto_selected.id,
                        'model': 'Presupuestos'
                    };
                    customer_selected = {
                        code: presupuesto_selected.customer_code
                    }
                    $('#btn-edit-administrative-movement').removeClass('my-hidden');
                }

                $('.modal-presupuestos').modal('show');
            }
        });
        
        $('#btn-generate-invoice-from-presupuesto').click(function() {
             
             $('.modal-presupuestos').modal('hide');
             
             $('.modal-generate-invoice-from-presupuesto').modal('show');
         });
         
         $(document).on("click", ".modal-generate-invoice-from-presupuesto .btn-generate", function(e) {
             
             var rows_selected_ids = [];
             rows_selected_ids.push(presupuesto_selected.id);
             
              if (rows_selected_ids) {
                
                var d = new Date(); 
                var created = $('.modal-generate-invoice-from-presupuesto #created').val().split('/');
                
                var h = addZero(d.getHours());
                var m = addZero(d.getMinutes());
                var s = addZero(d.getSeconds());
                
                created = created[2] + '-' + created[1] + '-' + created[0] + ' ' + h + ':' + m + ':' + s;
                
                var duedate = $('.modal-generate-invoice-from-presupuesto #duedate').val().split('/');
                duedate = duedate[2] + '-' + duedate[1] + '-' + duedate[0] + ' ' + h + ':'+ m + ':' + s;
                
                var date_start = $('.modal-generate-invoice-from-presupuesto #date_start').val().split('/');
                date_start = date_start[2] + '-' + date_start[1] + '-' + date_start[0] + ' ' + h + ':' + m + ':' + s;
                
                var date_end = $('.modal-generate-invoice-from-presupuesto #date_end').val().split('/');
                date_end = date_end[2] + '-' + date_end[1] + '-' + date_end[0] + ' ' + h + ':' + m + ':' + s;
  
                var data = {
                    ids: rows_selected_ids,
                    date: created,
                    duedate: duedate,
                    concept_type: parseInt($('.modal-generate-invoice-from-presupuesto #concept_type').val()),
                    date_start: date_start,
                    date_end: date_end,
                    type: $('.modal-generate-invoice-from-presupuesto #type').val(),
                    business_id: $('.modal-generate-invoice-from-presupuesto #business-id').val(),
                    from: 'presupuestos-index'
                };
                
                created = new Date(data.date);
                duedate = new Date(data.duedate);
                date_start = new Date(data.date_start);
                date_end = new Date(data.date_end);
                
                var now = new Date();
                now.setHours(0);
                now.setMinutes(0);
                now.setSeconds(0);
              
                created.setHours(0);
                created.setMinutes(0);
                created.setSeconds(0);
                
                if (created > now) {
                    generateNoty('warning', 'La fecha de la factura no puede ser mayor a la fecha actual.');
                    return false;
                }
                
                if (duedate < created) {
                    generateNoty('warning', 'La fecha de vencimiento no puede ser menor a la fecha actual.');
                    return false;
                }
                
                if (data.concept_type != 1 && date_end < date_start) {
                    generateNoty('warning', 'Las fechas del período no son correctas.');
                    return false;
                }

                bootbox.confirm('Se generará la facturas del presupuesto seleccionado. ¿Desea continuar?', function(result) {
                    if (result) {
                
                        $('body')
                            .append( $('<form/>').attr({'action': '/ispbrain/invoices/generateIndiFromPresu/', 'method': 'post', 'id': 'form-block'})
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'data', 'value': JSON.stringify(data)}))
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                            .find('#form-block').submit();
                        
                        openModalPreloader("Generando Facturas. Espere Por favor ...");
                        
                    }
                });
                
            } else {
                bootbox.alert('Debe seleccionar al menos una deuda para continuar.');
            }
            
       
        });
        
    });

    $('.btn-download').click(function() {

        var ids = [];

        table_presupuestos.rows({search: 'applied'}).every( function ( rowIdx, tableLoop, rowLoop ) {

            ids.push( this.data().id );  

        });

        if (ids.length > 0) {

            $('#replacer').remove();

            $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/download/presupuestos/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'ids', 'value': ids}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#replacer').submit();
        } else {
            generateNoty('information', 'No hay facturas para descargar');
        }
    });

    $('#btn-go-customer').click(function() {
        var action = '/ispbrain/customers/view/' + presupuesto_selected.customer_code;
        window.open(action, '_self');
    });

    $('#btn-print-presupuesto').click(function() {
        var action = '/ispbrain/Presupuestos/showprint/' + table_presupuestos.$('tr.selected').attr('id');
        window.open(action, '_blank');
    });

    $('#btn-edit-administrative-movement').click(function() {
        $('.modal-presupuestos').modal('hide');
        table_selected = table_presupuestos;
        $('.modal-edit-administrative-movement').modal('show');
    });

    $('#btn-send-email-presupuesto').click(function() {
        if (presupuesto_selected.customer.email == "" || presupuesto_selected.customer.email == null) {
            generateNoty('warning', 'Debe agregar el Correo Eectrónico del Cliente al cual pertenece el presupuesto.');
        } else {
            var count = 0;
            $.each(mass_emails_templates, function( index, value ) {
                count++;
            });
            if (count > 1) {
                model = 'Presupuestos';
                model_id = presupuesto_selected.id;
                $('.modal-presupuestos').modal('hide');
                $('.modal-send-email').modal('show');
            } else {
                generateNoty('warning', 'No cuenta con plantillas de Correos.');
            }
        }
    });

    $('#btn-delete').click(function() {
        
        $('.modal-presupuestos').modal('hide');
        
        bootbox.confirm("¿Está Seguro que desea eliminar el presupuesto?", function(result) {
    
            if (result) {
                    
                var request = $.ajax({
                    url: "<?= $this->Url->build(['controller' => 'Presupuestos', 'action' => 'delete']) ?>",  
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify(presupuesto_selected),
                });
                
                request.done(function( data ) {
                  table_presupuestos.draw();
                });
                 
                request.fail(function( jqXHR, textStatus ) {
                  generateNoty('error', 'Error al intenetar eliminar el presupuesto');
                });
            }
        });
    });

    $(".btn-export").click(function() {

        bootbox.confirm('Se descargará un archivo Excel', function(result) {
            if (result) {
                $('#table-presupuestos').tableExport({tableName: 'Facturas', type:'excel', escape:'false', columnNumber: [10, 11, 12, 13]});
            }
        }).find("div.modal-content").addClass("confirm-width-md");
    });
    
    function addZero(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

</script>
