<style type="text/css">

    .sub-title-md {
        color: #696868;
        font-size: 30px;
        font-weight: bold;
        margin-bottom: 10px;
        margin-top: 20px;
    }

    legend {
        display: block;
        width: 100%;
        padding: 0;
        margin-bottom: 20px;
        font-size: 21px;
        line-height: inherit;
        color: #333;
        border: 0;
        border-bottom: 1px solid #e5e5e5;
    }

    .lbl-no-notifications {
        font-size: 20px;
        font-family: sans-serif;
        font-weight: bold !important;
    }

</style>

<?= $this->Form->create($paraments,  ['class' => 'form-load']) ?>

<fieldset>

    <div class="row justify-content-center">
        <div class="col-auto mb-1 ">
            <a href="javascript:void(0)" class="btn btn-default btn-add-notificator">
                <i class="fa fa-plus" aria-hidden="true"></i> Agregar Notificador
            </a>
        </div>
        <div class="col-auto">
            <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success' ]) ?>
        </div>
    </div>

    <div class="row">

        <div class="col-xl-12 col-xl-12">

            <?php if (sizeof($paraments->notificators) > 0): ?>
                <?php foreach ($paraments->notificators as $key => $notificator): ?>

                    <legend class="sub-title-md" style="font-size: 24px;"><?= $notificator->name ?> <a data-notificator-id="<?= $notificator->id ?>" title="Agregar Notificación" href="javascript:void(0)" class="btn btn-default ml-1 mb-2 btn-add-notification"><i class="fa fa-plus" aria-hidden="true"></i></a> <a data-notificator-name="<?= $notificator->name ?>" data-notificator-id="<?= $notificator->id ?>" title="Editar Notificador" href="javascript:void(0)" class="btn btn-default ml-1 mb-2 btn-edit-notificator"><i class="fa fa-edit" aria-hidden="true"></i></a> <a data-notificator-id="<?= $notificator->id ?>" title="Eliminar Notificador" href="javascript:void(0)" class="btn btn-default ml-1 mb-2 btn-delete-notificator"><i class="far fa-trash-alt" aria-hidden="true"></i></a></legend>

                    <div class="row">

                        <?php if (sizeof($notificator->notifications) > 0): ?>

                            <?php foreach ($notificator->notifications as $k => $notification): ?>

                                <div class="col-sm-12 col-md-6 col-xl-6">

                                    <div class="card border-secondary mb-2">
                                        <div class="card-header bg-transparent border-secondary pt-2 pb-1">
                                            <h5 class="sub-title-sm"><?= $notification->name ?> <a data-id="<?= $notification->id ?>" data-name="<?= $notification->name ?>" data-notificator-id="<?= $notificator->id ?>" title="Editar Notificación" href="javascript:void(0)" class="btn btn-default ml-1 mb-2 btn-edit-notification"><i class="fa fa-edit" aria-hidden="true"></i></a> <a data-id="<?= $notification->id ?>" data-notificator-id="<?= $notificator->id ?>" title="Eliminar Notificación" href="javascript:void(0)" class="btn btn-default ml-1 mb-2 btn-delete-notification text-right"><i class="far fa-trash-alt" aria-hidden="true"></i></a></h5>
                                        </div>
                                        <div class="card-body pt-2 pb-1">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-12 col-xl-4">
                                                    <?= $this->Form->input('notifications.' . $notificator->id . '.' . $notification->id . '.start', ['label' => 'Día inicio', 'type' => 'number', 'min' => '0', 'value' => $notification->start]) ?>
                                                    <?= $this->Form->input('notifications.' . $notificator->id . '.' . $notification->id . '.dueday', ['label' => 'Día vencimiento', 'type' => 'number', 'min' => '0', 'value' => $notification->dueday]) ?>
                                                   <?= $this->Form->input('notifications.' . $notificator->id . '.' . $notification->id . '.enabled', ['label' => 'Habilitar', 'type' => 'checkbox', 'checked' => $notification->enabled]) ?>
                                                </div>
                                                <div class="col-sm-12 col-md-12 col-xl-8">
                                                    <?= $this->Form->input('notifications.' . $notificator->id . '.' . $notification->id . '.message', ['label' => 'Mensaje', 'type' => 'textarea', 'placeholder' => 'Escriba un mensaje...', 'value' => $notification->message, 'class' => 'w-100', 'rows' => 7  ]) ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            <?php endforeach; ?>

                        <?php else: ?>
                            <div class="col-sm-12 col-md-6 col-xl-6">
                                <label class="lbl-no-notifications"><?= __('Sin Notificaciones') ?></label>
                            </div>
                        <?php endif; ?>

                    </div>

                 <?php endforeach; ?>
            <?php else: ?>
                <label class="lbl-no-notifications"><?= __('Sin Notificaciones') ?></label>
            <?php endif; ?>
        </div>
    </div>

</fieldset>

<?= $this->Form->end() ?>

<div class="modal fade" id="modal-add-notificator" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Agregar Notificador</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-md-12">

                        <form method="post" accept-charset="utf-8" role="form" action="<?= $this->Url->build(["controller" => "Notifications", "action" => "add"]) ?>">
                            <div style="display:none;">
                                <input type="hidden" name="_method" value="POST">
                            </div>
                            <div class="form-group text mb-3">
                                <label class="control-label" for="name">Nombre</label>
                                <input type="text" name="name" id="name" class="form-control">
                            </div>
                            <input type="hidden" name="id" id="id"/>
                            <?php echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]); ?>
                            <div class="mt-2">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="submit" class="btn btn-success" id="btn-confirm-add-notificator">Agregar</button> 
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-edit-notificator" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Editar Notificador</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-md-12">

                        <form method="post" accept-charset="utf-8" role="form" action="<?= $this->Url->build(["controller" => "Notifications", "action" => "edit"])?>">
                            <div style="display:none;">
                                <input type="hidden" name="_method" value="POST">
                            </div>
                            <div class="form-group text mb-3">
                                <label class="control-label" for="name">Nombre</label>
                                <input type="text" name="name" id="name" class="form-control">
                            </div>
                            <input type="hidden" name="id" id="id"/>
                            <?php echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]); ?>
                            <div class="mt-2">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="submit" class="btn btn-success" id="btn-confirm-edit-notificator">Guardar</button> 
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-add-notification" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Agregar Notificación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-md-12">

                        <form method="post" accept-charset="utf-8" role="form" action="<?= $this->Url->build(["controller" => "Notifications", "action" => "addNotification"]) ?>">
                            <div style="display:none;">
                                <input type="hidden" name="_method" value="POST">
                            </div>
                            <?php
                                echo $this->Form->hidden('notificator_id', ['value' => '']);
                            ?>
                            <div class="form-group text mb-3">
                                <label class="control-label" for="name">Nombre</label>
                                <input type="text" name="name" id="name" class="form-control">
                            </div>

                            <input type="hidden" name="id" id="id"/>
                            <?php echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]); ?>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-success" id="btn-confirm-add-notification">Agregar</button> 

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-edit-notification" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Editar Notificación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-md-12">

                        <form method="post" accept-charset="utf-8" role="form" action="<?= $this->Url->build(["controller" => "Notifications", "action" => "editNotification"]) ?>">
                            <div style="display:none;">
                                <input type="hidden" name="_method" value="POST">
                            </div>
                            <?php
                                echo $this->Form->hidden('notificator_id', ['value' => '']);
                            ?>
                            <div class="form-group text mb-3">
                                <label class="control-label" for="name">Nombre</label>
                                <input type="text" name="name" id="name" class="form-control">
                            </div>

                            <input type="hidden" name="id" id="id"/>
                            <input type="hidden" name="notificator_id" id="notificator_id"/>
                            <?php echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]); ?>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-success" id="btn-confirm-add-notification">Guardar</button> 

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

        $('.btn-add-notificator').click(function() {
            $('#modal-add-notificator').modal('show');
        });

        $('.btn-edit-notificator').click(function() {
            var name = $(this).data('notificator-name');
            $('#modal-edit-notificator #name').val(name);
            var id = $(this).data('notificator-id');
            $('#modal-edit-notificator #id').val(id);
            $('#modal-edit-notificator').modal('show');
        });

        $('.btn-add-notification').click(function() {
            var notificator_id = $(this).data('notificator-id');
            $('input[name="notificator_id"]').val(notificator_id);
            $('#modal-add-notification').modal('show');
        });

        $('.btn-edit-notification').click(function() {
            var name = $(this).data('name');
            $('#modal-edit-notification #name').val(name);
            var notificator_id = $(this).data('notificator-id');
             $('#modal-edit-notification #notificator_id').val(notificator_id);
            var id = $(this).data('id');
            $('#modal-edit-notification #id').val(id);
            $('#modal-edit-notification').modal('show');
        });

        $('.btn-delete-notificator').click(function() {

            var text = "¿Está Seguro que desea eliminar el Notificador?";
            var controller = "Notifications";
            var notificator_id = $(this).data('notificator-id');

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/delete/', 'method': 'post', 'id': 'replacer'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'notificator_id', 'value': notificator_id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer').submit();
                }
            });
        });

        $('.btn-delete-notification').click(function() {

            var text = "¿Está Seguro que desea eliminar la Notificación?";
            var controller = "Notifications";
            var id = $(this).data('id');
            var notificator_id = $(this).data('notificator-id');

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/deleteNotification/', 'method': 'post', 'id': 'replacer'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'notificator_id', 'value': notificator_id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer').submit();
                }
            });
        });

    });

</script>
