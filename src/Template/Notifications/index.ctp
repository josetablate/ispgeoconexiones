<style type="text/css">

    tr.danger {
        background-color: #dc3545;
        color: white;
    }

</style>

<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5" >

        <?php
             echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export',
                'escape' => false
                ]);
        ?>
        
        <?php
             echo $this->Html->link(
                '<span class="fa fa-sliders-h" aria-hidden="true"></span>',
                ['controller' => 'Notifications', 'action' => 'config'],
                [
                'title' => 'Ir a Configuración',
                'class' => 'btn btn-default btn-go-config',
                'escape' => false
                ]);
        ?>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered" id="table-notifications">
            <thead>
                <tr>
                    <th>Notificación</th>
                    <th>Mensaje</th>
                    <th>Día inicio</th>
                    <th>Día vencimiento</th>
                    <th>Visto</th>
                    <th>Habilitado</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>   
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-check',
        'name' =>  'Marcar visto',
        'icon' =>  'fa fa-check',
        'type' =>  'btn-info'
    ];

    $buttons[] = [
        'id'   =>  'btn-uncheck',
        'name' =>  'Quitar visto',
        'icon' =>  'fa fa-circle',
        'type' =>  'btn-dark'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete-notification',
        'name' =>  'Eliminar',
        'icon' =>  'far fa-trash-alt',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-actions-notifications', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<script type="text/javascript">

    var table_notifications = null;
    var notification_selected = null;

    $(document).ready(function () {

        $('#table-notifications').removeClass('display');

    	table_notifications = $('#table-notifications').DataTable({
    	    "order": [[ 0, 'asc' ]],
    	    "sWrapper":  "dataTables_wrapper dt-bootstrap4",
    	    "autoWidth": true,
    	    "scrollY": '450px',
    	    "scrollCollapse": true,
    	    "scrollX": true,
    	    "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/Notifications/index.json",
                "dataSrc": "notifications",
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
            				  window.location.href ="/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "columns": [
                { "data": "name" },
                { "data": "message" },
                { "data": "start" },
                { "data": "dueday" },
                { 
                    "data": "checked",
                    "render": function ( data, type, row ) {
                        var enabled = '<i class="fa fa-times" aria-hidden="true"></i>';
                        if (data) {
                            enabled = '<i class="fa fa-check" aria-hidden="true"></i>';
                        }
                        return enabled;
                    }
                },
                { 
                    "data": "enabled",
                    "render": function ( data, type, row ) {
                        var enabled = '<i class="fa fa-times" aria-hidden="true"></i>';
                        if (data) {
                            enabled = '<i class="fa fa-check" aria-hidden="true"></i>';
                        }
                        return enabled;
                    }
                }
            ],
    	    "columnDefs": [
            ],
            "createdRow" : function( row, data, index ) {
                var date = new Date();
                var current_day = date.getDate();

                if (current_day >= data.dueday && !data.checked) {
                    $(row).addClass('danger');
                } else {
                    $(row).removeClass('danger');
                }
            },
    	    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
    	});

    	$('#table-notifications_filter').closest('div').before($('#btns-tools').contents());

    });

    $(".btn-export").click(function() {

        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if (result) {
                $('#table-notifications').tableExport({tableName: 'Notificaciones', type:'excel', escape:'false'});
            }
        });
    });

    $('#table-notifications tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_notifications.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            notification_selected = table_notifications.row( this ).data();
            if (notification_selected.checked) {
                $('#btn-check').addClass('my-hidden');
                $('#btn-uncheck').removeClass('my-hidden');
            } else {
                $('#btn-uncheck').addClass('my-hidden');
                $('#btn-check').removeClass('my-hidden');
            }
            $('.modal-actions-notifications').modal('show');
        }
    });

    $('#btn-check').click(function() {

        var text = "¿Está Seguro que desea marcar como visto?";
        var val = {
            'notificator_id': notification_selected.notificator_id,
            'id': notification_selected.id
        };
        var arr = [];
        arr.push(val);

        $('body')
            .append( $('<form/>').attr({'action': '/ispbrain/Notifications/check/', 'method': 'post', 'id': 'replacer1'})
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'data', 'value': JSON.stringify(arr)}))
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
            .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
            .find('#replacer1').submit();
    });

    $('#btn-uncheck').click(function() {

        var text = "¿Está Seguro que desea quitar visto?";
        var val = {
            'notificator_id': notification_selected.notificator_id,
            'id': notification_selected.id
        };
        var arr = [];
        arr.push(val);

        $('body')
            .append( $('<form/>').attr({'action': '/ispbrain/Notifications/uncheck/', 'method': 'post', 'id': 'replacer2'})
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'data', 'value': JSON.stringify(arr)}))
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
            .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
            .find('#replacer2').submit();
    });

    $('#btn-delete-notification').click(function() {

            var text = "¿Está Seguro que desea eliminar la Notificación?";
            var controller = "Notifications";
            var id = notification_selected.id;
            var notificator_id = notification_selected.notificator_id;;

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/deleteNotification/', 'method': 'post', 'id': 'replacer3'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'notificator_id', 'value': notificator_id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer3').submit();
                }
            });
        });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
