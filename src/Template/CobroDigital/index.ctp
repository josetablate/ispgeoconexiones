<style type="text/css">

    .input-enabled {
        width: 100%;
    }

    .modal span {
        margin-right: 0 !important;
    }

    tr.title{
        color: #696868 !important;
        font-weight: bold;
    }

    .btn-small {
        padding: 2px;
        margin: 5px;
    }

    .contain-block {
        background-color: #ececec;
        border: 1px solid #c3c2c2;
        border-radius: 5px;
        margin-left: 15px;
        padding-top: 5px;
    }

    .form-contain {
        border: 1px solid #d2cfcf;
        background: #f0f0f1;
        margin-left: 15px;
        padding-top: 15px;
        border-radius: 5px;
    }

</style>

<?php
    $accountsArray = [];
    $accountsArray[''] = 'Raíz';

    foreach ($accounts as $a) {
        $accountsArray[$a->code] = $a->code . ' ' . $a->name;
    }
?>

<div id="btns-tools">
    <div class="pull-right btns-tools margin-bottom-5" >

        <?php

            echo $this->Html->link(
                '<span class="fas fa-play" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Forzar Control',
                'class' => 'btn btn-danger btn-force-control',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Agregar Cuenta',
                'class' => 'btn btn-default btn-add-credential ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export ml-1',
                'escape' => false
            ]);
        ?>

    </div>
</div>

<div class="row">
    <div class="col-md-10 ml-3">
        <legend class="sub-title">Cuentas de cobro digital</legend>
        <table class="table table-hover table-bordered" id="table-credentials">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>SID</th>
                    <th>ID Comercio</th>
                    <th>Tarjeta</th>
                    <!--<th>Boleta</th>-->
                    <th>Débito Automático</th>
                    <th>Habilitado</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <div class="col-md-10">
        <legend class="sub-title ml-3">Configuración</legend>
        <?= $this->Form->create($payment_getway,  ['class' => ['form-load']]) ?>
        <fieldset>
            
            <div class="row mb-3 form-contain">

                <div class="col-md-3 contain-block">
                    <label class="control-label" for="">Visualización</label>
                    <?= $this->Form->input('enabled', ['label' => 'Habilitar', 'type' => 'checkbox', 'checked' => $payment_getway->config->cobrodigital->enabled]) ?>
                    <?= $this->Form->input('cash', ['label' => 'Ver Cobranza', 'type' => 'checkbox', 'checked' => $payment_getway->config->cobrodigital->cash]); ?>
                    <?= $this->Form->input('portal', ['label' => 'Ver Portal', 'type' => 'checkbox', 'checked' => $payment_getway->config->cobrodigital->portal]); ?>
                </div>

                <div class="col-md-4 contain-block">
                    <div class="form-group required">
                        <label class="control-label" for="hours-execution">Ejecución para Consultar Transacciones</label>
                        <div class='input-group date' id='hours-execution-datetimepicker'>
                            <span class="input-group-addon input-group-text mr-0">Horario</span>
                            <input  name="hours_execution" required="required" id="hours-execution"  type='text' class="form-control" />
                            <span class="input-group-addon input-group-text mr-0">
                                <span class="glyphicon icon-calendar"></span>
                            </span>
                        </div>
                        <?= $this->Form->input('period', ['label' => 'Período', 'type' => 'text', 'value' => $payment_getway->config->cobrodigital->period]) ?>
                        <button type="button" id="force-execution" class="btn btn-dark mb-3">Forzar ejecución</button>
                        <button type="button" id="clean-transactions" class="btn btn-danger mb-3">Borrar transacción sin asignación</button>
                    </div>
                    <!--<?= $this->Form->input('automatic', ['label' => 'Automático', 'type' => 'checkbox', 'checked' => $payment_getway->config->cobrodigital->automatic]); ?>-->
                </div>

                <div class="col-md-3 contain-block">
                    <label class="control-label" for="">Servicio Web</label>
                    <?= $this->Form->input('url', ['label' => 'URL', 'type' => 'text', 'value' => $payment_getway->config->cobrodigital->url, 'title' => $payment_getway->config->cobrodigital->url]) ?>
                    <small id="info" class="form-text text-muted">
                        https://www.cobrodigital.com:14365/ws3/, https://cobrodigital.com.ar:14365/ws3/
                    </small>
                </div>

                <?php if ($account_enabled): ?>
                    <div class="col-md-4 contain-block">
                        <label for="">Cuenta contable</label>
                        <table class="mb-3">
                             <tr>
                                <td style="width:400px">
                                    <input type="hidden" name="account" id="account_code" value="<?= $payment_getway->config->cobrodigital->account ?>" >
                                    <input type="text" id="account_code-show" class="form-control" readonly value="<?= $accountsArray[$payment_getway->config->cobrodigital->account] ?>" placeholder="Cuenta">
                                </td>
                                <td> 
                                    <a class="btn btn-default btn-search-account btn-small" href="#" data-input="account_code" role="button">
                                        <span class="glyphicon icon-search" aria-hidden="true"></span>
                                    </a>
                                </td>
                                 <td> 
                                    <a class="btn btn-default btn-bin-account btn-small" href="#" data-input="account_code" role="button">
                                        <span class="glyphicon icon-bin" aria-hidden="true"></span>
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                <?php endif; ?>

                <div class="col-md-5 mt-3 mb-3">
                    <?= $this->Html->link(__('Cancelar'), ["controller" => "PaymentMethods", "action" => "index"], ['class' => 'btn btn-default ']) ?>
                    <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success' ]) ?>
                </div>

            </div>

            </fieldset>
            
            <?= $this->Form->end() ?>
    </div>

</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-edit',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-default'
    ];

    $buttons[] = [
        'id'   =>  'test-config',
        'name' =>  'Probar configuración',
        'icon' =>  'fas fa-check',
        'type' =>  'btn-dark'
    ];

    echo $this->element('actions', ['modal'=> 'modal-credentials', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<div class="modal fade" id="modal-accounts" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Seleccionar Cuenta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">

                        <table class="table table-hover" id="table-accounts">
                            <thead>
                                <tr>
                                    <th>Cód.</th>
                                    <th>Nombre</th>
                                    <th>Descripción</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                foreach ($accounts as $acc): ?>
                                <tr class="<?= ($acc->title) ? 'title' : '' ?> <?= (!$acc->status) ? ' font-through' : '' ?>"
                                    data-code="<?= $acc->code ?>"
                                    data-name="<?= $acc->name ?>"
                                    >
                                    <td class="left"><?= $acc->code ?></td>
                                    <td class="left"><?php

                                        $hrchy = substr(strval($acc->code), 0, 4);
                                        $hrchy = str_split($hrchy);
                                        foreach ($hrchy as $h) {
                                            if ($h != '0') {
                                                echo '&nbsp;&nbsp;&nbsp;'; 
                                            }
                                        }
                                        if (!$acc->title) {
                                            echo '&nbsp;&nbsp;&nbsp;'; 
                                        }
                                        echo $acc->name; 
                                    ?>
                                    </td>
                                    <td class="left"><?= $acc->description ?></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <br>

                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" id="btn-account-selected" class="btn btn-success">Seleccionar</button> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="label-modal-add">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Agregar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-md-12">

                        <form method="post" accept-charset="utf-8" role="form" action="<?= $this->Url->build(["controller" => "CobroDigital", "action" => "addCredential"]) ?>">
                            <div style="display:none;">
                                <input type="hidden" name="_method" value="POST">
                            </div>

                            <?php
                                echo $this->Form->input('name', ['id' => 'input-name-credential-add', 'label' => 'Nombre']);
                                echo $this->Form->input('sid', ['id' => 'input-sid-credential-add', 'label' => 'SID']);
                                echo $this->Form->input('idComercio', ['id' => 'input-id-comercio-credential-add', 'label' => 'ID Comercio']);
                                echo $this->Form->input('enabled', ['id' => 'input-enabled-credential-add', 'label' => 'Habilitado', 'type' => 'checkbox', 'checked' => TRUE]);
                                echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]);
                                echo $this->Form->input('card', ['id' => 'input-card-credential-add', 'label' => 'Tarjeta', 'type' => 'checkbox', 'checked' => false]);
                                echo $this->Form->input('auto_debit', ['id' => 'input-auto-debit-credential-add', 'label' => 'Débito Automático', 'type' => 'checkbox', 'checked' => false]);
                            ?>

                            <input type="hidden" name="id" id="id"/>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-success" id="btn-confirm-edit">Agregar</button> 

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Editar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-md-12">

                        <form method="post" accept-charset="utf-8" role="form" action="<?= $this->Url->build(["controller" => "CobroDigital", "action" => "editCredential"]) ?>">
                            <div style="display:none;">
                                <input type="hidden" name="_method" value="POST">
                            </div>

                            <?php
                                echo $this->Form->hidden('id', ['value' => '']);
                                echo $this->Form->input('name', ['id' => 'input-name-credential-edit', 'label' => 'Nombre']);
                                echo $this->Form->input('sid', ['id' => 'input-sid-credential-edit', 'label' => 'SID']);
                                echo $this->Form->input('idComercio', ['id' => 'input-id-comercio-credential-edit', 'label' => 'ID Comercio']);
                                echo $this->Form->input('enabled', ['id' => 'input-enabled-credential-edit', 'label' => 'Habilitado', 'type' => 'checkbox', 'checked' => true]);
                                echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]);
                                echo $this->Form->input('card', ['id' => 'input-card-credential-edit', 'label' => 'Tarjeta', 'type' => 'checkbox', 'checked' => false]);
                                echo $this->Form->input('auto_debit', ['id' => 'input-auto-debit-credential-edit', 'label' => 'Débito Automático', 'type' => 'checkbox', 'checked' => false]);
                            ?>

                            <input type="hidden" name="id" id="id"/>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-success" id="btn-confirm-edit">Guardar</button> 

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var parament;
    var payment_getway;
    var table_credentials = null;
    var credential_selected = null;

    var table_acccounts = null;

    $(document).ready(function () {

        paraments = <?= json_encode($paraments) ?>;
        payment_getway = <?= json_encode($payment_getway) ?>;
        var year = new Date().getFullYear(); 
        var defaultDateCD = year + "-10-01T" + payment_getway.config.cobrodigital.hours_execution;
        $('#hours-execution-datetimepicker').datetimepicker({
            locale: 'es',
            defaultDate: defaultDateCD,
            format: 'HH:mm',
        });

        $('#table-credentials').removeClass('display');

        $('#btns-tools').hide();

		table_credentials = $('#table-credentials').DataTable({
		    "autoWidth": true,
		    "scrollY": '350px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "sWrapper":  "dataTables_wrapper dt-bootstrap4",
            "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/CobroDigital/get_credentials.json",
                "dataSrc": "credentials",
                "error": function(c) {
                    switch (c.status) {
                        case 400:
                            flag = false;
                            generateNoty('warning', 'Ha ocurrido un error.');
                            break;
                        case 401:
                            flag = false;
                            generateNoty('warning', 'Error, no posee una autorización.');
                            break;
                        case 403:
                            flag = false;
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                        	setTimeout(function() { 
                                window.location.href = "/ispbrain";
                        	}, 3000);
                            break;
                    }
                }
            },
            "columns": [
                { "data": "name" },
                { "data": "sid" },
                { "data": "idComercio"},
                { 
                    "data": "card",
                    "render": function ( data, type, row ) {
                        var enabled = '<i class="fa fa-times red" aria-hidden="true"></i>';
                        if (data) {
                            enabled = '<i class="fa fa-check green" aria-hidden="true"></i>';
                        }
                        return enabled;
                    }
                },
                // { 
                //     "data": "boleta",
                //     "render": function ( data, type, row ) {
                //         var enabled = '<i class="fa fa-times red" aria-hidden="true"></i>';
                //         if (data) {
                //             enabled = '<i class="fa fa-check green" aria-hidden="true"></i>';
                //         }
                //         return enabled;
                //     }
                // },
                { 
                    "data": "auto_debit",
                    "render": function ( data, type, row ) {
                        var enabled = '<i class="fa fa-times red" aria-hidden="true"></i>';
                        if (data) {
                            enabled = '<i class="fa fa-check green" aria-hidden="true"></i>';
                        }
                        return enabled;
                    }
                },
                { 
                    "data": "enabled",
                    "render": function ( data, type, row ) {
                        var enabled = '<i class="fa fa-times red" aria-hidden="true"></i>';
                        if (data) {
                            enabled = '<i class="fa fa-check green" aria-hidden="true"></i>';
                        }
                        return enabled;
                    }
                },
            ],
            "columnDefs": [
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
		    "language":  dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            "dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
		});

		$('#table-credentials_wrapper .tools').append($('#btns-tools').contents());

        $(".btn-export").click(function() {
            bootbox.confirm('Se descargará un archivo Excel', function(result) {
                if (result) {
                    $('#table-credentials').tableExport({tableName: 'Credenciales', type:'excel', escape:'false'});
                }
            });
        });

        $('#btns-tools').show();

        $('#table-credentials tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                table_credentials.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
                credential_selected = table_credentials.row( this ).data();

                $('.modal-credentials').modal('show');
            }
        });

        $('.btn-add-credential').click(function() {
            $('#input-name-credential-add').val("");
            $('#input-sid-credential-add').val("");
            $('#input-id-comercio-credential-add').val("");
            $('#input-enabled-credential-add').prop('checked', true);
            $('#input-boleta-add').prop('checked', false);
            $('#input-card-add').prop('checked', false);
            $('#input-auto-debit-add').prop('checked', false);
            $('#modal-add').modal('show');
        });

        $('a#btn-edit').click(function() {

            $('.modal-credentials').modal('hide');

            $('input[name="id"]').val(credential_selected.id);
            $('#input-name-credential-edit').val(credential_selected.name);
            $('#input-sid-credential-edit').val(credential_selected.sid);
            $('#input-id-comercio-credential-edit').val(credential_selected.idComercio);
            $('#input-enabled-credential-edit').prop('checked', credential_selected.enabled);
            $('#input-boleta-credential-edit').prop('checked', credential_selected.boleta);
            $('#input-card-credential-edit').prop('checked', credential_selected.card);
            $('#input-auto-debit-credential-edit').prop('checked', credential_selected.auto_debit);

            $('#modal-edit').modal('show');
        });

        $('#table-accounts').removeClass('display');

		table_acccounts = $('#table-accounts').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '480px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
		});

		$('#table-accounts_filter').closest('div').before($('#btns-tools').contents());   

		var input_select = null;

		$('.btn-search-account').click(function() {

		    input_select = $(this).data('input');
		    $('#modal-accounts').modal('show');
		});

		$('.btn-bin-account').click(function() {

		    var input_select_temp = $(this).data('input');
		    $('#' + input_select_temp).val('');
		    $('#' + input_select_temp + '-show').val('');
		});

		$('#btn-account-selected').click(function() {

		    var code = table_acccounts.$('tr.selected').data('code');
		    var name = table_acccounts.$('tr.selected').data('name');

		    $('#' + input_select).val(code);
		    $('#' + input_select + '-show').val(code + ' ' + name);
		    $('#account_name').val(name);

		    $('#modal-accounts').modal('hide');
		});

		$('#modal-accounts').on('shown.bs.modal', function (e) {
		    table_acccounts.draw();
        });

        $('#table-accounts tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                table_acccounts.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });

        $(".btn-force-control").click(function() {

            bootbox.prompt({
                title: "¿Está seguro que desea ejecutar el Control de Transacción?",
                inputType: 'text',
                placeholder: 'YYYYmmdd,YYYYmmdd',
                callback: function (result) {
                    if (result != null) {
                        if (result.indexOf(',') > -1) {

                            var dates = result.split(',');

                            var request = $.ajax({
                                url: "/ispbrain/CobroDigital/ticketsControl/1/" + dates[0] + '/' + dates[1],
                                method: "POST",
                                data: JSON.stringify({}),
                                dataType: "json"
                            });

                            request.done(function(response) {
                                if ( response.data.code == 200) {
                                    generateNoty('success', 'Procesado correctamente');
                                } else {
                                    generateNoty('error', 'No se ha procesado correctamente');
                                }
                            });

                            request.fail(function(jqXHR) {

                                if (jqXHR.status == 403) {

                                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                                	setTimeout(function() {
                                        window.location.href = "/ispbrain";
                                	}, 3000);

                                } else {
                                	generateNoty('error', 'Error al procesar las transacciones');
                                }
                            });
                        }
                    }
                }
            });
        });

        $('#test-config').click(function() {

            var request = $.ajax({
                url: "/ispbrain/CobroDigital/test.json",
                method: "POST",
                data: JSON.stringify({
                    id: credential_selected.id
                }),
                dataType: "json"
            });

            request.done(function(response) {
                generateNoty('success', response.messagelala);
            });

            request.fail(function(jqXHR) {

                if (jqXHR.status == 403) {

                    generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    setTimeout(function() { 
                        window.location.href = "/ispbrain";
                    }, 3000);

                } else {
                    generateNoty('error', 'Error al intentar obtener los totales');
                }
            });
        });

        $('#force-execution').click(function() {

            var text = "¿Está Seguro que desea ejecutar?";

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/CobroDigital/forceExecution/', 'method': 'post', 'id': 'replacer'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer').submit();
                }
            });
        });

        $('#clean-transactions').click(function() {

            var text = "¿Está Seguro que desea borrar las transacciones con estado: sin asignar?";

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/CobroDigital/cleanTransactions/', 'method': 'post', 'id': 'replacer'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer').submit();
                }
            });
        });
    });

</script>
