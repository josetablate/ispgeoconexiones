<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5">

        <?php

            echo $this->Html->link(
                '<span class="glyphicon fas fa-search" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Buscar por Columnas',
                'class' => 'btn btn-default btn-search-column ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export ml-1',
                'escape' => false
            ]);

        ?>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-hover" id="table-cards">
            <thead>
                <tr>
                    <th></th>             <!--0-->
                    <th>Cód. Barra</th>   <!--1-->
                    <th>Cód. Electr.</th> <!--2-->
                    <th>ID Comercio</th>  <!--3-->
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-delete',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-cards', 'title' => 'Acciones', 'buttons' => $buttons ]);
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-cards']);
    echo $this->element('modal_preloader');
?>

<script type="text/javascript">

    var table_cards = null;
    var card_selected = null;
    var id_comercios = null;
    var curr_pos_scroll = null;

    $(document).ready(function () {

        id_comercios = <?= json_encode($id_comercios) ?>;

        $('.btn-search-column').click(function() {
            $('.modal-search-columns-cards').modal('show');
        });

        $('#table-cards').removeClass('display');

        $('#btns-tools').hide();

    	table_cards = $('#table-cards').DataTable({
            "order": [[ 1, 'desc' ]],
            "deferRender": true,
            "processing": true,
            "serverSide": true,
		    "ajax": {
                "url": "/ispbrain/CobroDigital/retrieves.json",
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "used": 0,
                        "deleted": 0,
                        "date_exported": 0,
                        "payment_getway_id": 99
                    });
                },
                "error": function(c) {
                    switch (c.status) {
                        case 400:
                            flag = false;
                            generateNoty('warning', 'Ha ocurrido un error.');
                            break;
                        case 401:
                            flag = false;
                            generateNoty('warning', 'Error, no posee una autorización.');
                            break;
                        case 403:
                            flag = false;
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                        	setTimeout(function() { 
                                window.location.href = "/ispbrain";
                        	}, 3000);
                            break;
                    }
                }
            },
            "drawCallback": function( c ) {
                var flag = true;
            	switch (c.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
	            if (flag) {
                    if (table_cards) {

                        var tableinfo = table_cards.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }
                    }
            	}
            },
            "scrollY": true,
		    "scrollY": '430px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
            "columns": [
                { 
                    "data": "id",
                    "orderable": false,
                    "render": function ( data, type, row ) {
                        return "";
                    }
                },
                { 
                    "data": "barcode",
                    "type": "string"
                },
                { 
                    "data": "electronic_code",
                    "type": "string"
                },
                {
                    "data": "id_comercio",
                    "type": "options",
                    "options": id_comercios,
                    "render": function ( data, type, row ) {
                        return data;
                    }
                },
            ],
    	    "columnDefs": [
    	        { "visible": true },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
    	    "language": dataTable_lenguage,
    	    "pagingType": "numbers",
            "lengthMenu": [[ 50, 100, -1], [ 50, 100, "Todas"]],
            "dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
    	});

    	$('#table-cards_wrapper .tools').append($('#btns-tools').contents());

        $('#table-cards').on( 'init.dt', function () {
            createModalSearchColumn(table_cards, '.modal-search-columns-cards');
        });

        $('#btns-tools').show();

        $('#table-cards tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                table_cards.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
                card_selected = table_cards.row( this ).data();

                $('.modal-cards').modal('show');
            }
        });

        $(".btn-export").click(function() {

            bootbox.confirm('Se descargara un archivo Excel', function(result) {
                if (result) {
                    $('#table-cards').tableExport({tableName: 'Tarjetas', type:'excel', escape:'false'});
                }
            });
        });

        $('#btn-delete').click(function() {

            var text = "¿Está Seguro que desea eliminar la Tarjeta?";
            var id  = card_selected.id;

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/CobroDigital/deleteCard/' + id, 'method': 'post', 'id': 'replacer'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer').submit();
                }
            });
        });

        // Jquery draggable modals
        $('.modal-dialog').draggable({
            handle: ".modal-header"
        });
    });

</script>
