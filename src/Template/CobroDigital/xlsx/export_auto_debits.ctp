<?php

    $rowsCount = count($invoices);

    $this->PhpExcel->getActiveSheet()->setTitle('Débito Automático');

    $this->PhpExcel->getActiveSheet()->fromArray($invoices, NULL, 'A1');

    $this->PhpExcel->getActiveSheet()->getStyle('A1:G1')->getFont()->setBold(true);

    $rowCount = 1;
    $customTitle = [
        'Nombre',
        'Apellido',
        'Cuit',
        //'Email',
        'Cbu',
        'Importe',
        'Fecha',
        'Concepto',
        //'Cuotas',
        //'Modalidad_cuotas',
    ];

    $this->PhpExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $customTitle[0]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $customTitle[1]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $customTitle[2]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $customTitle[3]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $customTitle[4]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $customTitle[5]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $customTitle[6]);

    $colsAttr = [
        'A' => [
            'setAutoSize' => true,
        ],
        'B' => [
            'setAutoSize' => true,
        ],
        'C' => [
            'setAutoSize' => true,
        ],
        'D' => [
            'setAutoSize' => true,
        ],
        'E' => [
            'setAutoSize' => true,
        ],
        'F' => [
            'setAutoSize' => true,
        ],
        'G' => [
            'setAutoSize' => true,
        ]
    ];

    foreach ($colsAttr as $col => $attr) {

        $activeSheet = $this->PhpExcel->getActiveSheet();

        //align center head columns
        $activeSheet->getStyle("$col"."1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $column = $activeSheet->getColumnDimension($col);

        $column->setAutoSize($attr['setAutoSize']);

        if (array_key_exists('setWidth', $attr)) {
            $column->setWidth($attr['setWidth']);
        }

        $alignment = $activeSheet->getStyle("$col" . "2:$col$rowsCount")->getAlignment();

        if (array_key_exists('horizontal', $attr)) {
            if ($attr['horizontal'] == 'right') {
                $alignment->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            } else if ($attr['horizontal'] == 'left') {
                $alignment->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            } else {
                $alignment->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }
        }
    }

?>
