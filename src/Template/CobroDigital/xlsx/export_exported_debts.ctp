<?php

    $rowsCount = count($invoices);

    $this->PhpExcel->getActiveSheet()->setTitle('Deudas');

    $this->PhpExcel->getActiveSheet()->fromArray($invoices, NULL, 'A1');

    $this->PhpExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true);

    $rowCount = 1;
    $customTitle = [
        'sap_pmc',
        'sap_barcode',
        'sap_apellido',
        'sap_identificador',
        'importe',
        'vencimiento'
    ];

    $this->PhpExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $customTitle[0]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $customTitle[1]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $customTitle[2]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $customTitle[3]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $customTitle[4]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $customTitle[5]);

    $key = 1;
    foreach ($invoices as $invoice) {
        if ($key == 1) {
            $key++;
            continue;
        }
        $cell = 'A' . $key;
        $this->PhpExcel->getActiveSheet()->setCellValueExplicit($cell, $invoice[0], PHPExcel_Cell_DataType::TYPE_STRING);
        $key++;
    }

    $colsAttr = [
        'A' => [
            'setAutoSize' => true
        ],
        'B' => [
            'setAutoSize' => true
        ],
        'C' => [
            'setAutoSize' => true,
        ],
        'D' => [
            'setAutoSize' => true,
        ],
        'E' => [
            'setAutoSize' => true,
        ],
        'F' => [
            'setAutoSize' => true,
        ]
    ];

    foreach ($colsAttr as $col => $attr) {

        $activeSheet = $this->PhpExcel->getActiveSheet();

        //align center head columns
        $activeSheet->getStyle("$col"."1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $column = $activeSheet->getColumnDimension($col);

        $column->setAutoSize($attr['setAutoSize']);

        if (array_key_exists('setWidth', $attr)) {
            $column->setWidth($attr['setWidth']);
        }

        $alignment = $activeSheet->getStyle("$col" . "2:$col$rowsCount")->getAlignment();

        if (array_key_exists('horizontal', $attr)) {
            if ($attr['horizontal'] == 'right') {
                $alignment->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            } else if ($attr['horizontal'] == 'left') {
                $alignment->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            } else {
                $alignment->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }
        }
    }

?>
