<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5" >

        <?php

             echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar en formato excel el contenido de la tabla',
                'class' => 'btn btn-default btn-export',
                'escape' => false
                ]);
        ?>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered" id="table-auto-debits">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Usuario</th>
                    <th>Descripción</th>
                    <th>Privilegios</th>
                    <th>Teléfono</th>
                    <th>Creado</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>   
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-info',
        'name' =>  'Información',
        'icon' =>  'icon-eye',
        'type' =>  'btn-default'
    ];

    $buttons[] = [
        'id'   =>  'btn-edit',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-default'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-users', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<script type="text/javascript">

    var table_users = null;
    var user_selected = null;

    $(document).ready(function () {

        $('#table-users').removeClass('display');

    	table_users = $('#table-users').DataTable({
    	    "order": [[ 0, 'asc' ]],
    	    "sWrapper":  "dataTables_wrapper dt-bootstrap4",
    	    "autoWidth": true,
    	    "scrollY": '450px',
    	    "scrollCollapse": true,
    	    "scrollX": true,
    	    "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/users/get_users.json",
                "dataSrc": "users",
                "error": function(c) {
                    switch (c.status) {
                        case 400:
                            flag = false;
                            generateNoty('warning', 'Ha ocurrido un error.');
                            break;
                        case 401:
                            flag = false;
                            generateNoty('warning', 'Error, no posee una autorización.');
                            break;
                        case 403:
                            flag = false;
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                        	setTimeout(function() { 
                        	  window.location.href ="/ispbrain";
                        	}, 3000);
                            break;
                    }
                }
            },
            "columns": [
                { "data": "name" },
                { "data": "username" },
                { "data": "description" },
                { 
                    "data": "roles_names_list",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                    
                },
                { "data": "phone" },
                { 
                    "data": "created",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                { 
                    "data": "enabled",
                    "render": function ( data, type, row ) {
                        var enabled = '<i class="fa fa-times" aria-hidden="true"></i>';
                        if (data) {
                            enabled = '<i class="fa fa-check" aria-hidden="true"></i>';
                        }
                        return enabled;
                    }
                }
            ],
    	    "columnDefs": [
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
    	    "language": dataTable_lenguage,
            "lengthMenu": [[ 50, 100, -1], [ 50, 100, "Todas"]],
    	});

    	$('#table-users_filter').closest('div').before($('#btns-tools').contents());

    });

    $(".btn-export").click(function() {

        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if (result) {
                $('#table-users').tableExport({tableName: 'Usuarios', type:'excel', escape:'false'});
            }
        });
    });

    $('#table-users tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_users.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            user_selected = table_users.row( this ).data();

            $('.modal-users').modal('show');
        }
    });

    $('#btn-delete').click(function() {

        var text = "¿Está Seguro que desea eliminar el usuario?";
        var id  = user_selected.id;

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/users/delete/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#replacer').submit();
            }
        });
    });

    $('a#btn-info').click(function() {
        var action = '/ispbrain/users/view/' + user_selected.id;
        window.open(action, '_self');
    });

    $('a#btn-edit').click(function() {
       var action = '/ispbrain/users/edit/' + user_selected.id;
       window.open(action, '_self');
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
