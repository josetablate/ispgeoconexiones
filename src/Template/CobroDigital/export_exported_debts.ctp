<style type="text/css">

    .btn-export span {
        color: #fff !important;
    }

    tr.selected {
        background-color: #8eea99 !important;
        color: #333 !important;
    }

    .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
        background-color: #d2d2d2;
    }

</style>

<div class="row">
    <div class="col-xl-6">

        <div class="card border-secondary mb-2">

            <div class="card-body p-2">

                <div class="row">
                    <div class="col-xl-2 pr-0">
                        <div class="text-secondary p-2">Período</div>
                    </div>
                    <div class="col-xl-5">
                        <div class="input-group mr-2">
                          <span class="input-group-addon input-group-text">DESDE</span>
                          <input type="text" id="date_from" class="form-control font-weight-bold" readonly>
                        </div>
                    </div>
                    <div class="col-xl-5">
                        <div class="input-group">
                          <span class="input-group-addon input-group-text">HASTA</span>
                          <input type="text" id="date_to" class="form-control font-weight-bold" readonly>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-xl-6 ">

        <div class="card border-secondary p-1 mb-2">

            <div class="card-body p-2 d-flex justify-content-around">

                <?php

                    echo  $this->Html->link(
                        '<span class="glyphicon icon-eye-plus" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => '',
                        'class' => 'btn btn-default btn-hide-column ml-2',
                        'escape' => false
                    ]);

                    echo  $this->Html->link(
                        '<span class="glyphicon icon-checkbox-checked" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Seleccionar todos',
                        'class' => 'btn btn-default btn-selected-all ml-2',
                        'escape' => false
                    ]);

                    echo  $this->Html->link(
                        '<span class="glyphicon icon-checkbox-unchecked" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Limpiar Selección',
                        'class' => 'btn btn-default btn-clear-selected-all ml-2',
                        'escape' => false
                    ]);

                    echo  $this->Html->link(
                        '<span class="glyphicon icon-calendar" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Filtrar período',
                        'class' => 'btn btn-default btn-filter-period ml-2',
                        'data-toggle' => 'modal', 'data-target' => '#modal-filter',
                        'escape' => false
                    ]);

                    echo $this->Html->link(
                        '<span class="glyphicon fas fa-search" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Buscar por columnas',
                        'class' => 'btn btn-default btn-search-column ml-1',
                        'escape' => false
                    ]);

                    echo  $this->Html->link(
                        '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Exportar en formato excel el contenido de la tabla',
                        'class' => 'btn btn-info btn-export ml-2',
                        'escape' => false
                    ]);
                ?>

            </div>

        </div>
    </div>
</div>

<div class="modal fade dont-fade bs-example-modal-sm" id="modal-filter" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Filtrar por Período</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <?= $this->Form->create() ?>

                    <div class="row">
                        <div class="col-md-12">

                            <div class="input-group">
                                <div class='input-group date' id='date-from-datetimepicker'>
                                    <span class="input-group-addon input-group-text mr-0">DESDE</span>
                                    <input name="from" id="date-from" type='text' class="column_search form-control datetimepicker" data-column="2" required />
                                    <span class="input-group-addon input-group-text mr-0">
                                        <span class="glyphicon icon-calendar"></span>
                                    </span>
                                </div>
                            </div>

                            <div class="input-group">
                              <div class='input-group date' id='date-to-datetimepicker'>
                                    <span class="input-group-addon input-group-text mr-0">HASTA</span>
                                    <input name="to" id="date-to" type='text' class="column_search form-control datetimepicker" data-column="3" required />
                                    <span class="input-group-addon input-group-text mr-0">
                                        <span class="glyphicon icon-calendar"></span>
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div>

                <?= $this->Form->end() ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-clear-filter" >Limpiar</button>
                <button type="button" class="btn btn-primary btn-periode-search">Buscar</button>
            </div>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-12">
       <table class="table table-bordered table-hover" id="table-debts">
            <thead>
                <tr>
                    <th></th>               <!--0-->
                    <th>Fecha</th>          <!--1-->
                    <th>Comprobante</th>    <!--2-->
                    <th>Número</th>         <!--3-->
                    <th>Desde</th>          <!--4-->
                    <th>Hasta</th>          <!--5-->
                    <th>Venc.</th>          <!--6-->
                    <th>Total</th>          <!--7-->
                    <th>Cliente</th>        <!--8-->
                    <th>Documento</th>      <!--9-->
                    <th>Código</th>         <!--10-->
                    <th>Servicio</th>       <!--11-->
                    <th>ID Comercio</th>    <!--12-->
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

</div>

<div class="modal fade confirm-export-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabel">Confirmar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <small class="text-muted">Fecha de Venc. debe ser 4 días hábiles antes de enviar a Cobrodigital</small>
                        <table id="table-period" style="width:100%" >
                            <tr>
                                <td style="width:40%"><label class="control-label" for="">Venc. de Pago</label></td>
                                <td style="width:60%">
                                    <div class='input-group date' id='duedate-datetimepicker'>
                                        <input name="duedate" id="duedate"  type='text' class="form-control" required />
                                        <span class="input-group-addon input-group-text mr-0">
                                            <span class="glyphicon icon-calendar mr-0"></span>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <br>

                    	<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-danger btn-generate float-right">Confirmar</button>
               
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<form action="/ispbrain/CobroDigital/export_exported_debts.xlsx" method="post" id="form-block" class="d-none">
    <input type="hidden" name="data" value="">
    <input type="hidden" name="_csrfToken" autocomplete="off" value="token">
</form>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-go-customer',
        'name' =>  'Ficha del Cliente',
        'icon' =>  'glyphicon icon-profile',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-actions-debts', 'title' => 'Acciones', 'buttons' => $buttons ]);
    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-debts']);
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-debts']);
    echo $this->element('modal_preloader');

?>

<script type="text/javascript">

    var table_debts = null;
    var duedate = <?= json_encode($duedate) ?>;
    var id_comercios = null;
    var curr_pos_scroll = null;
    var ids_selected = [];
    var hash = null;

    $('.confirm-export-modal #duedate-datetimepicker').datetimepicker({
        defaultDate: duedate,
        format: 'DD/MM/YYYY'
    });

    $(document).ready(function() {

        id_comercios = <?= json_encode($id_comercios) ?>;
        hash = <?= time() ?>;

        $('.btn-hide-column').click(function() {
            $('.modal-hide-columns-debts').modal('show');
        });

        $('.btn-search-column').click(function() {
            $('.modal-search-columns-debts').modal('show');
        });

        $('#table-debts').removeClass('display');

        $('#btns-tools').hide();

        loadPreferences('invoices-generate', [0, 1, 2, 3, 4, 5, 6, 12]);

        table_debts = $('#table-debts').DataTable({
		    "order": [[ 1, 'desc' ]],
            "deferRender": true,
            "processing": true,
            "serverSide": true,
		    "ajax": {
                "url": "/ispbrain/CobroDigital/get_export_debts_cobrodigital_card.json",
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "date_exported": 1,
                        "payment_getway_id": 99
                    });
                },
                "error": function(c) {
                    switch (c.status) {
                        case 400:
                            flag = false;
                            generateNoty('warning', 'Ha ocurrido un error.');
                            break;
                        case 401:
                            flag = false;
                            generateNoty('warning', 'Error, no posee una autorización.');
                            break;
                        case 403:
                            flag = false;
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                        	setTimeout(function() { 
                                window.location.href = "/ispbrain";
                        	}, 3000);
                            break;
                    }
                }
            },
		    "drawCallback": function( settings ) {
                var flag = true;
            	switch (settings.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
            	    if (table_debts) {

                        var tableinfo = table_debts.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }
                    }
            	}
            },
		    "scrollY": true,
		    "scrollY": '450px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
		    "columns": [
		         {
                    "className": 'checkbox-control',
                    "orderable": false,
                    "data":      'id',
                    "render": function ( data, type, full, meta ) {
                        return '<label class="custom-control custom-checkbox pl-3 pr-3 m-0 align-middle"><input type="checkbox" class="custom-control-input checkbox-select p-0 m-0 " id="row-checkbox-'+data+'"><span class="custom-control-indicator w-100 align-middle"></span></label>';
                    },
                    "width": '3%'
                },
                { 
                    "className": " row-control",
                    "data": "date",
                    "render": function ( data, type, row ) {
                        var date = data.split('T')[0];
                        date = date.split('-');
                        return date[2] + '/' + date[1] + '/' + date[0];
                    }
                },
                { 
                    "className": " row-control",
                    "orderable": false,
                    "data": "tipo_comp",
                    "type": "options",
                    "options": {
                        "XXX": "PRESU X",
                        "001": "FACTURA A",
                        "006": "FACTURA B",
                        "011": "FACTURA C"
                    },
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.comprobantes[data];
                    }
                },
                { 
                    "className": " row-control",
                    "data": "num",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 8);
                    }
                },
                { 
                    "className": " row-control",
                    "data": "date_start",
                    "render": function ( data, type, row ) {
                        var date_start = data.split('T')[0];
                        date_start = date_start.split('-');
                        return date_start[2] + '/' + date_start[1] + '/' + date_start[0];
                    }
                },
                { 
                    "className": " row-control",
                    "data": "date_end",
                    "render": function ( data, type, row ) {
                        var date_end = data.split('T')[0];
                        date_end = date_end.split('-');
                        return date_end[2] + '/' + date_end[1] + '/' + date_end[0];
                    }
                },
                { 
                    "className": " row-control text-info",
                    "data": "duedate",
                    "type": "date",
                    "render": function ( data, type, row ) {

                        var duedate = data.split('T')[0];
                        duedate = duedate.split('-');

                        if (type == 'display') {

                            if (row.paid == null) {

                                var date = new Date(duedate[1] + '/' + duedate[2] + '/' + duedate[0]);
                                var todayDate = new Date();

                                duedate =  duedate[2] + '/' + duedate[1] + '/' + duedate[0];

                                if (date < todayDate) {
                                    return "<span class='text-danger'>" + duedate + "</span>";
                                } else {
                                    return "<span class='text-info'>" + duedate + "</span>";
                                }
                            }
                        }

                        duedate =  duedate[2] + '/' + duedate[1] + '/' + duedate[0];
                        return duedate;
                    }
                },
                { 
                    "className": " row-control right text-info",
                    "data": "total",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' )
                },
                { 
                    "className": " row-control left",
                    "data": "customer_name",
                    "type": "string"
                },
                {
                    "orderable": false,
                    "data": "customer_ident",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (data) {
                            ident = data;
                        }
                        return sessionPHP.afip_codes.doc_types[row.customer_doc_type] + ' ' + ident;
                    }
                },
                { 
                    "className": " row-control",
                    "data": "customer_code",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                {
                    "className": ' row-control left',
                    "orderable": false,
                    "data": "connection",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return "";
                    }
                },
                {
                    "className": " row-control",
                    "orderable": false,
                    "data": "id_comercio",
                    "type": "options",
                    "options": id_comercios,
                    "render": function ( data, type, row ) {
                        return data;
                    }
                },
            ],
		    "columnDefs": [
                { 
                    "type": 'date-custom',
                    "targets":  [3, 4, 5]
                },
                { 
                    "type": 'numeric-comma', 
                    "targets": [6]
                }
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
                $.each(ids_selected, function( index, value ) {
                    if (row.id == value) {
                        $(row).addClass('selected');
                        $(row).find('input:checkbox').attr('checked', 'checked')
                    }
                });
            },
		    "language": dataTable_lenguage,
            "lengthMenu": [[500, 1000, -1], [500, 1000, "Todas"]],
        	"dom":
    	    	"<'row'<'col-md-6'l><'col-md-6'f>>" +
        		"<'row'<'col-md-12'tr>>" +
        		"<'row'<'col-md-5'i><'col-md-7'p>>",
		});

        $('#table-debts_wrapper .tools').append($('#btns-tools').contents());

		$('#table-debts').on( 'init.dt', function () {

		    createModalHideColumn(table_debts, '.modal-hide-columns-debts');
            createModalSearchColumn(table_debts, '.modal-search-columns-debts');

            $( "#table-debts tbody" ).on( "click",  ".checkbox-select", function() {

                if ($(this).is(':checked')) {

                    $(this).closest('tr').addClass('selected');
                    ids_selected.push(table_debts.row( $(this).closest('tr') ).data().id);
                } else {

                    $(this).closest('tr').removeClass('selected');
                    ids_selected.splice($.inArray(table_debts.row( $(this).closest('tr') ).data().id, ids_selected), 1);
                }
            });

            $( "#table-debts tbody" ).on( "click",  ".row-control", function() {
                debt_selected = table_debts.row( $(this).closest('tr') ).data();
                $('.modal-actions-debts').modal('show');
            });
        });

        $('#btns-tools').show();

        $('#date-from-datetimepicker').datetimepicker({
            locale: 'es',
            format: 'DD/MM/YYYY'
        });

        $('#date-to-datetimepicker').datetimepicker({
            locale: 'es',
            format: 'DD/MM/YYYY'
        });

        $('.btn-selected-all').click(function() {

            $('.checkbox-select:checkbox').prop('checked', false);
            $('.checkbox-select').click();

            ids_selected = [];
            table_debts.rows().eq(0).each( function ( idx ) {
                var row = table_debts.row( idx );
                if ($(row.node()).hasClass('selected')) {
                    ids_selected.push(table_debts.row( idx ).data().id);
                }
            });
        });

        $('.btn-clear-selected-all').click(function() {

            $('.checkbox-select:checkbox').prop('checked', true);
            $('.checkbox-select').click();

            ids_selected = [];
        });

        $('#btn-go-customer').click(function() {
            var action = '/ispbrain/customers/view/' + debt_selected.customer_code;
            window.open(action, '_blank');
        });

        $("#date-from-datetimepicker").on("dp.change",function (e) {
            $('#date_from').val($('#date-from').val());
        });

        $("#date-to-datetimepicker").on("dp.change",function (e) {
            $('#date_to').val($('#date-to').val());
        });

        $('.btn-clear-filter').click(function() {
            table_debts.search( '' ).columns().search( '' ).draw();
            $('#date-from').val('');
            $('#date-to').val('');
            $('#date_from').val($('#date-from').val());
            $('#date_to').val($('#date-to').val());
        });

        $('.btn-periode-search').click(function() {
            if ($('#date-from').val() != '' && $('#date-to').val() != '') {
                table_debts.columns( 4 ).search( $('#date-from').val() );
                table_debts.columns( 5 ).search( $('#date-to').val() );
                table_debts.draw();
                $('#modal-filter').modal('hide');
            } else {
                generateNoty('warning', 'Debe cargar las fechas de período: Desde - Hasta para poder aplicar el filtro.');
            }
        });

        $(document).on("click", ".btn-export", function(e) {
            $('.confirm-export-modal').modal('show');
        });

        $(document).on("click", ".btn-generate", function(e) {

            if (ids_selected.length > 0) {

                var duedate =  $('.confirm-export-modal #duedate').val().split('/');
                duedate = duedate[2] + '-' + duedate[1] + '-' + duedate[0] + ' 00:00:00';

                var data = {
                    ids:     ids_selected,
                    duedate: duedate
                };

                $("#form-block input[name=data]").val(JSON.stringify(data));
                $("#form-block input[name=hash]").val(hash);
                $("#form-block input[name=_csrfToken]").val(token);
                $('#form-block').submit();

                table_debts.ajax.reload();
                ids_selected = [];

                $('.confirm-export-modal').modal('hide');

            } else {
                 bootbox.alert('Debe seleccionar al menos 1 deuda para continuar.');
            }
        });

    });

</script>
