<style type="text/css">

    .logo {
        color: #FF5722;
        margin-top: 50px;
        margin-bottom: 50px;
    }

    .panel-cards-import {
        background-color: #8BC34A !important;
        color: white !important;
        padding: 10px;
    }

</style>

<div class="col-md-8" style="margin-top: 50px;">
    <p class="lead"><?= __('Cosas a tener') ?> <span class="text-success"><?= __('EN CUENTA') ?></span></p>
    <ul class="list-unstyled" style="line-height: 2">
        <li><span class="fa fa-check text-success"></span> <?= __('La 1er columna, debe ser fecha de pago (formato: 08/01/2019)') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('La 2da columna, debe ser código de cliente') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('La 3da columna, importe del pago (formato: 8.191,00)') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Únicamente archivo con extensión') ?> <strong><?= __('CSV') ?></strong></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Columnas del Excel: fecha de pago, código de cliente, importe, concepto (opcional), método de pago (opcional)') ?></strong></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Recomendación: exportador los clientes que pagaron desde la vista: Menú/Clientes/Ver Clientes, luego mostrar solamente las columnas: Creado (modificar por la fecha de pago), Código, Saldo mes (modificar por el monto que pago), por último realizar el filtro necesario y exportar el Excel') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Tener en cuenta: lo mencionado en el punto anterior, si no se agrega la columna concepto por defecto dirá: Pago y luego si no se agrega la columna método de pago por defecto el valor será efectivo.') ?></li>
    </ul>
</div>

<div class="col-md-8" style="margin-top: 50px;">
    <p class="lead"><?= __('Referencia de valores para métodos de pagos (opcional)') ?></p>
    <ul class="list-unstyled" style="line-height: 2">
        <li><span class="fa fa-check text-success"></span> <?= __('Efectivo                        => 1') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Cheque                          => 2') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Transferencia                   => 3') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Tarjeta de Débito               => 4') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Tarjeta de Crédito              => 5') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Pim                             => 6') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('PayU                            => 7') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Tarjeta Cobro Digital           => 99') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Todo Pago                       => 100') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Mercado Pago                    => 101') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Débito Automático Chubut        => 102') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Débito Automático Cobro Digital => 104') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Cuenta Digital                  => 105') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Débito Automático Visa          => 106') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Débito Automático Mastercard    => 107') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Rapipago                        => 108') ?></li>
    </ul>
</div>

<div class="col-md-4" style="margin-top: 50px;">

    <div class="cards cards-default">
        <div class="cards-heading panel-cards-import"><?= __('Importar Pagos') ?></div>
        <div class="cards-body">
            <?= $this->Form->create(null, ['url' => ['controller' => 'Payments' , 'action' => 'uploadPayments'], 'name' => 'cards-form', 'type' => 'file','role'=>'form']) ?>

                <div class="form-group">
                    <?= $this->Form->hidden('hash', ['value' => time()] ); ?>
                    <label class="sr-only" for="csv"><?= __('CSV') ?></label>
                    <?php echo $this->Form->input('csv', ['type'=>'file', 'accept' => '.csv', 'class' => 'form-control', 'label' => false, 'placeholder' => __('csv upload'), 'required' => true]); ?>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <button type="submit" class="btn btn-default btn-import"><?= __('Importar') ?></button>
                </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
