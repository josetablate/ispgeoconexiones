<style type="text/css">

    .last-cell-saldo-red {
        color: #f05f40;
        font-weight: bold;
        font-size: 17px;
    }

    .last-cell-saldo-green {
        color: #35d058;
        font-weight: bold;
        font-size: 17px;
    }

    .btn-next-due-dates-selected {
        color: #f05f40;
        background-color: #e6e6e6;
        border-color: #f05f40;
    }

    .vertical-table {
        width: 100%;
    }

    table.vertical-table tbody tr {
        border-bottom: 1px solid #d8d8d8;
    }

    .btn-add-recargo, .btn-add-credit {
        padding: 2.25px 6.25px;
    }

    #labels {
        max-width: 200px;
    }

    .comment-text {
        display: block;
        text-overflow: ellipsis;
        word-wrap: break-word;
        overflow: hidden;
        line-height: 1.8em;
        max-height: 115px;
        overflow-y: auto;
    }

    .border-bottom {
        border-bottom: 1px solid #dee2e6!important;
    }

    .btn {
        white-space:normal !important;
        word-wrap: break-word; 
    }

</style>

<div id="btns-tools-moves">

    <div class="text-right btns-tools margin-bottom-5">

        <?php

            echo $this->Html->link(
                '<span class="fas fa-dollar-sign" aria-hidden="true"></span> Agregar crédito',
                'javascript:void(0)',
                [
                'title' => '',
                'class' => 'btn btn-success btn-add-credit ml-1 mt-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="fas fa-dollar-sign" aria-hidden="true"></span> Agregar recargo',
                'javascript:void(0)',
                [
                'title' => '',
                'class' => 'btn btn-danger btn-add-recargo ml-1 mt-1 ',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="fa fa-file-alt" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Ir a Recibo',
                'class' => 'btn btn-default btn-go-to-receipts ml-1 mt-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="fas fa-sync-alt" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Actualizar la tabla',
                'class' => 'btn btn-default btn-update-table-move ml-1 mt-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="far fa-calendar-plus" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Mostrar próximos vencimientos',
                'class' => 'btn btn-default btn-next-due-dates ml-1 mt-1',
                'escape' => false
            ]);

        ?>

    </div>
</div>

<div class="row mb-3" id="input-created">
    <div class="col-xl-3">
        <label for="">Fecha</label>
        <div class='input-group date' id='created-datetimepicker'>
            <input name='created' id="created" type='text' class="form-control" required />
            <span class="input-group-addon input-group-text calendar">
                <span class="glyphicon icon-calendar"></span>
            </span>
        </div>
    </div>
</div>

<?php if ($paraments->customer->search_list_customers): ?>

<div class="modal fade" id="modal-customer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">

                <div class="row">
                    <div class="col-12">
                        <button type="button" class="close" aria-label="Close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <?= $this->element('customer_list_seeker') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php else: ?>

<div class="modal fade" id="modal-customer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">

                <div class="row">
                    <div class="col-12">
                        <button type="button" class="close" aria-label="Close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <?= $this->element('customer_seeker') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php endif; ?>

<div class="row">

    <div class="col-sm-12 col-md-6 col-lg-5 col-xl-4">

        <div class="card border-secondary mb-3" id="customer-info">

            <div class="card-header">

                 <div class="row">
                    <div class="col-12">
                        <button type="button" class="btn btn-default " data-toggle="modal" data-target="#modal-customer">
                          Buscar Cliente
                        </button>
                        <button type="button" title="Observación" class="btn btn-default btn-observations float-right my-hidden">
                            <span class="fas fa-file-signature" aria-hidden="true"></span>
                        </button>
                        <button type="button" title="Cuentas" class="btn btn-default btn-accounts float-right my-hidden">
                            <span class="fas fa-credit-card" aria-hidden="true"></span>
                        </button>
                        <button type="button" title="Ficha del Cliente" class="btn btn-default btn-go-customer float-right my-hidden" data-toggle="tooltip" data-html="true">
                            <span class="fas fa-user" aria-hidden="true"></span>
                        </button>
                        <button type="button" title="Editar Cliente" class="btn btn-default btn-edit-customer float-right my-hidden">
                            <span class="fas fa-user-edit" aria-hidden="true"></span>
                        </button>
                    </div>

                </div>
            </div>
            <div class="card-block card-data-block p-4">

                <legend class="sub-title-sm">Cliente seleccionado</legend>

                <div class="row">

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Nombre') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right comment-text" id="name"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Código') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right comment-text" id="code"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Documento') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right comment-text" id="ident"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Teléfono') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right comment-text" id="phone"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Domicilio') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right comment-text" id="address"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Presupuesto') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right comment-text" id="is_presupuesto"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Denominación') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right comment-text" id="denomination"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Facturar con') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right comment-text" id="business_billing"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Etiquetas') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right" id="labels"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Comentario') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right comment-text" id="comments"></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="accordion" class="mt-3 d-none">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
        
                                <button class="btn btn-secondary" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <?= $paraments->gral_config->billing_for_service ? 'Estado de cuenta de conexiones y otras ventas' : 'Servicios' ?>
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <table id="table-connections" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th><?= $paraments->gral_config->billing_for_service ? 'Servicios y otras ventas' : 'Servicios' ?></th>
                                                    <th>Deuda mes</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-12">
                </div>
            </div>
        </div>

    </div>

    <div class="col-sm-12 col-md-6 col-lg-7 col-xl-8">

        <div class="row">

            <div class="col-xl-12">
                <div class="card border-secondary mb-2" >

                    <div class="card-body text-secondary p-2">

                        <div class="row billing-_for-service-elements <?= $paraments->gral_config->billing_for_service ? '' : 'd-none' ?>">
                            <div class="col-md-12 col-lg-2 col-xl-12">
                                <?php echo $this->Form->input('connection_id', ['options' => [], 'label' => 'Servicios y otras ventas']); ?>
                                <small id="emailHelp" class="form-text text-muted">Seleccione un 'Servicio' o 'Otras ventas' para generar un comprobante relacionado.</small>
                            </div>
                        </div>

                        <hr style="margin: 15px;">

                        <div class="row">

                            <div class="col-12">

                                <table class="table table-bordered" id="table-moves">
                                    <thead>
                                        <tr>
                                            <th>Fecha</th>          <!--0-->
                                            <th>Tipo</th>           <!--1-->
                                            <th>Destino</th>        <!--2-->
                                            <th>Pto. Vta.</th>      <!--3-->
                                            <th>Número</th>         <!--4-->
                                            <th>CAE</th>            <!--5-->
                                            <th>Cant.</th>          <!--6-->
                                            <th>Descripción</th>    <!--7-->
                                            <th>Subtotal</th>       <!--8-->
                                            <th>Impuestos</th>      <!--9-->
                                            <th>Total</th>          <!--10-->
                                            <th>Venc.</th>          <!--11-->
                                            <th>Saldo</th>          <!--12-->
                                            <th>Usuario</th>        <!--13-->
                                            <th>Caja</th>           <!--14-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                                <small class='form-text text-muted billing-_for-service-elements'>Últimos movimientos relacionados al servicio seleccionado.</small>

                            </div>

                        </div>

                    </div>

                </div>
            </div>
            <div class="col-xl-12">
                <div class="card border-secondary mb-3" >

                    <div class="card-body text-secondary pb-0 p-2">

                        <?= $this->Form->create($payment, ['id' => 'form-pay', 'class' => 'mb-0']) ?>

                        <div class="row">

                            <?= $this->Form->hidden('account_id', ['id' => 'account-id']); ?>
                            <?= $this->Form->hidden('hash', ['value' => time()] ); ?>

                            <div class="account-info">
                            </div>

                            <div class="col-xl-8">
                                <?php echo $this->Form->input('concept', ['options' => $paymentsConcepts, 'label' => 'Concepto', 'required' => true, 'value' => 1]); ?>
                                <?php echo $this->Form->input('concept_custom', ['type' => 'text', 'label' => false, 'required' => false, 'readonly' => true, 'placeholder' => 'Escriba el concepto...' ]); ?>
                            </div>
                            <div class="col-xl-4">
                                <?php echo $this->Form->input('payment_method_id', ['options' => $paymentMethodsx, 'label' => 'Método de Pago', 'empty' => false, 'required' => true]); ?>

                                <?php echo $this->Form->input('accounts', ['options' => [], 'label' => false, 'required' => false]); ?>
 
                                <?php echo $this->Form->input('bank', ['options' => $banks, 'label' => false, 'required' => false]); ?>

                                <?php echo $this->Form->input('account', ['options' => $accounts, 'label' => false, 'required' => false]); ?>

                                <?php echo $this->Form->input('number_transaction', ['label' => false, 'type' => 'text', 'required' => false, 'placeholder' => 'Ingrese nro de referencia']); ?>

                                <div class='input-group date' id='payment_date-datetimepicker'>
                                    <input name='payment_date' id="payment-date" type='text' placeholder="Fecha de cobro" class="form-control"  />
                                    <span class="input-group-addon input-group-text calendar">
                                        <span class="glyphicon icon-calendar"></span>
                                    </span>
                                </div>

                            </div>

                            <div class="col-xl-3">
                                <?php echo $this->Form->input('total_to_pay', ['type' => 'number', 'label' => 'Total ($)', 'readonly' => TRUE]);?>
                            </div>
                            <div class="col-xl-3">
                                <?php echo $this->Form->input('import', ['type' => 'number', 'label' => 'Pago ($)', 'min' => 0, 'step' => 0.01, 'required' => TRUE]);?>
                            </div>
                            <div class="col-xl-3">
                                <?php echo $this->Form->input('vuelto', ['type' => 'number', 'label' => 'Vuelto ($)', 'min' => 0, 'step' => 0.01, 'readonly' => TRUE]);?>
                            </div>
                            <div class="col-xl-3 mt-2">
                                <?php echo $this->Form->input('is_credit', ['type' => 'checkbox', 'label' => 'Vuelto como crédito', 'checked' => TRUE]);?>
                                <?php echo $this->Form->input('get_receipt', ['type' => 'checkbox', 'label' => 'Imprimir recibo', 'checked' => TRUE]);?>
                            </div>
                            <div class="col-xl-12">
                                <?php echo $this->Form->input('observations', ['type' => 'text', 'label' => FALSE, 'value' => $paraments->invoicing->observations_receipt, 'placeholder' => 'Observaciones', 'required' => FALSE, 'max' => 200]); ?>
                            </div>
                            <div class="col-xl-6">
                               <?= $this->Form->button(__('Cargar Pago'), ['class' => 'btn-submit btn-success']) ?>
                            </div>

                        </div>

                        <?= $this->Form->end() ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal-add-recargo" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Agregar Recargo</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-12">

                        <?= $this->Form->create(null,  ['id' => 'form-add-recargo', 'class' => 'mb-0']) ?>
                            <?= $this->Form->input('import', ['type' => 'number', 'label' => 'Valor ($)', 'min' => 0, 'step' => 0.01, 'required' => true]); ?>
                            <?= $this->Form->input('concept', ['type' => 'text', 'label' => 'Concepto', 'required' => true]); ?>
                            <?= $this->Form->input('type', ['options' => [],  'label' =>  'Tipo', 'required' => true, 'class' => 'is-valid']); ?>
                            <?= $this->Form->button(__('Agregar'), ['class' => 'btn-add-recargo-popup btn-success mt-3']) ?>
                        <?= $this->Form->end() ?>

                    </div>
                </div>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="modal-add-credit" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Agregar Crédito</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-12">

                        <?= $this->Form->create(null,  ['id' => 'form-add-credit', 'class' => 'mb-0']) ?>
                            <?= $this->Form->input('import', ['type' => 'number', 'label' => 'Valor ($)', 'min' => 0, 'step' => 0.01, 'required' => true]); ?>
                            <?= $this->Form->input('concept', ['type' => 'text', 'label' => 'Concepto', 'required' => true]); ?>
                            <?= $this->Form->input('type', ['options' => [],  'label' =>  'Tipo', 'required' => true, 'class' => 'is-valid']); ?>
                            <?= $this->Form->button(__('Agregar'), ['class' => 'btn-add-credit-popup btn-success mt-3']) ?>
                        <?= $this->Form->end() ?>

                    </div>
                </div>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="myModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Último recibo generado Correctamente.</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
        <div class="modal-body">
            <center>
                <button type="button" class="btn btn-success btn-print-receipt" data-receiptid="<?=$receipt_id?>" >
                    Imprimir &nbsp;&nbsp; <span class="glyphicon icon-printer" aria-hidden="true"></span>
                </button>
            </center>
        </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php

    $buttons[] = [
        'id'   =>  'btn-print-move',
        'name' =>  'Imprimir',
        'icon' =>  'icon-printer',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-actions-move', 'title' => 'Acciones', 'buttons' => $buttons ]);
    echo $this->element('Observations/add', ['modal' => 'modal-add-observation', 'title' => 'Agregar Observación', 'records' => TRUE]);
    echo $this->element('Observations/edit', ['modal' => 'modal-edit-observation', 'title' => 'Observación']);
    echo $this->element('pay_with_account/accounts', ['modal' => 'modal-accounts', 'title' => 'Elegir Cuenta', 'credentials' => $credentials, 'payment_getway' => $payment_getway]);
?>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-info-conn',
        'name' =>  'Información',
        'icon' =>  'icon-eye',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-edit-conn',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-administrative-conn',
        'name' =>  'Administrativa ',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-disable-conn',
        'name' =>  'Deshabilitar',
        'icon' =>  'icon-lock',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-enable-conn',
        'name' =>  'Habilitar',
        'icon' =>  'icon-unlocked',
        'type' =>  'btn-secondary'
    ];
  
    $buttons[] = [
        'id'   =>  'btn-delete-conn',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-connection', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<script type="text/javascript">

    var receipt_id = <?= json_encode($receipt_id) ?>;
    var customer_code = <?= json_encode($customer_code) ?>;
    var customer_load = <?= json_encode($customer_load) ?>;
    var customer_selected = null;

    var table_moves = null;
    var table_connections = null;
    var connection_selected = null;

    var response_data = null;

    var move_selected = null;
    var users = null;
    var payment_getway = null;
    var credentials = null;

    if (receipt_id != null && receipt_id != 0) {

        $('#myModal').modal({
            keyboard: true,
            backdrop: 'static',
            show: receipt_id ? true : false
        });
    }

    $('.btn-print-receipt').click(function() {
        var url = "/ispbrain/payments/print-receipt/" + $(this).data('receiptid');
        window.open(url, 'Recibo', "width=600,height=500");
    });

    function clear_card_customer_seeker() {
        $('#customer-info #name').html('');
        $('#customer-info #debt_month').html('');
        $('#customer-info #code').html('');
        $('#customer-info #ident').html('');
        $('#customer-info #phone').html('');
        $('#customer-info #address').html('');
        $('#customer-info #is_presupuesto').html('');
        $('#customer-info #denomination').html('');
        $('#customer-info #business_billing').html('');
        $('#customer-info #labels').html('');
        $('#customer-info #comments').html('');

        //avisos
        $('.btn-go-customer').prop('title', 'Ficha del Cliente');
        $('.btn-go-customer').prop('original-title', 'Ficha del Cliente');
        //aviso

        $('.btn-go-customer').addClass('my-hidden');
        $('.btn-observations').addClass('my-hidden');
        $('.btn-edit-customer').addClass('my-hidden');
        $('.btn-accounts').addClass('my-hidden');
        $('#accordion').addClass('d-none');
    }

    $(document).ready(function() {

        var class_debt = sessionPHP.paraments.gral_config.billing_for_service ? '' : 'd-none';

        $('[data-toggle="tooltip_notice"]').tooltip({
            html: true
        });

        credentials = <?= json_encode($credentials) ?>;
        payment_getway = <?= json_encode($payment_getway) ?>;

        users = <?= json_encode($users) ?>;
        var mode_migration = sessionPHP.paraments.system.mode_migration;

        if (mode_migration) {
            $('#created-datetimepicker').datetimepicker({
                locale: 'es',
                defaultDate: new Date(),
                format: 'DD/MM/YYYY'
            });
        } else {
             $('#input-created').remove();
        }

        $('#is-credit').attr('disabled', true);

        clear_form_pay();
        clear_card_customer_seeker(); 

        $('#connection-id').change(function() {
            $(this).closest('div').addClass('mb-0');
            table_moves.draw();
        });

        $('#connection-id').closest('div').addClass('mb-0');

        $('#card-customer-seeker').on('CUSTOMER_SELECTED', function() {
            afterSelectedCustomer();
        });

        $('.btn-go-customer').prop('data-toggle', 'tooltip');

        $("#accordion").on('shown.bs.collapse', function() {
            if (table_connections) {
                table_connections.draw();
            }
        });

        function afterSelectedCustomer() {

            clear_card_customer_seeker();
            $('.btn-go-customer').removeClass('my-hidden');
            $('.btn-observations').removeClass('my-hidden');
            $('.btn-edit-customer').removeClass('my-hidden');
            $('.btn-accounts').removeClass('my-hidden');

            var doc_type_name = sessionPHP.afip_codes.doc_types[customer_selected.doc_type];
            $('#customer-info #name').html(customer_selected.name);
            $('#customer-info #debt_month').html(number_format(customer_selected.debt_month, true));
            $('#customer-info #code').html(customer_selected.code);
            var ident = customer_selected.ident ? customer_selected.ident : '-';
            $('#customer-info #ident').html(doc_type_name + ' ' + ident);
            $('#customer-info #phone').html(customer_selected.phone);
            $('#customer-info #address').html(customer_selected.address);
            var is_presupuesto = customer_selected.is_presupuesto ? 'Si' : 'No';
            $('#customer-info #is_presupuesto').html(is_presupuesto);
            var denomination = customer_selected.denomination ? 'Si' : 'No';
            $('#customer-info #denomination').html(denomination);
            $('#customer-info #comments').html(customer_selected.comments);
            var business_name = '';
            $.each(sessionPHP.paraments.invoicing.business, function (i, b) {
                if (b.id == customer_selected.business_billing) {
                    business_name = b.name;
                }
            });
            $('#customer-info #business_billing').html(business_name);
            $.each(customer_selected.labels, function( index, label ) {
                $('#customer-info #labels').append("<span class='badge m-1' style='background-color: " + label.color_back + "; color: " + label.color_text + "' >" + label.text + "</span>");
            });

            business_selected = null;

            $.each(sessionPHP.paraments.invoicing.business, function(i, val) {

                if (customer_selected.business_billing == val.id) {
                    business_selected = val;
                }
            });

            if (business_selected) {

                $('#modal-add-recargo #type').empty();
                $('#modal-add-credit #type').empty();

                if (business_selected.responsible == 1) { //ri

                    $('#modal-add-recargo #type').append('<option value="">Seleccione Tipo</option>');
                    $('#modal-add-credit #type').append('<option value="">Seleccione Tipo</option>');

                    if (business_selected.webservice_afip.crt != '') {
                        $('#modal-add-recargo #type').append('<optgroup label="Nota Débito"><option value="NDX">NOTA DE DÉBITO X</option><option value="002">NOTA DE DÉBITO A</option><option value="007">NOTA DE DÉBITO B</option></optgroup>');
                        $('#modal-add-credit #type').append('<optgroup label="Nota Crédito"><option value="NCX">NOTA DE CRÉDITO X</option><option value="003">NOTA DE CRÉDITO A</option><option value="008">NOTA DE CRÉDITO B</option></optgroup>');
                    } else {
                        $('#modal-add-recargo #type').append('<optgroup label="Nota Débito"><option value="NDX">NOTA DE DÉBITO X</option>/optgroup>');
                        $('#modal-add-credit #type').append('<optgroup label="Nota Crédito"><option value="NCX">NOTA DE CRÉDITO X</option>/optgroup>');
                    }
                } else if (business_selected.responsible == 6) { //monotributerou

                    $('#modal-add-recargo #type').append('<option value="">Seleccione Tipo</option>');
                    $('#modal-add-credit #type').append('<option value="">Seleccione Tipo</option>');

                    if (business_selected.webservice_afip.crt != '') {
                        $('#modal-add-recargo #type').append('<optgroup label="Nota Débito"><option value="NDX">NOTA DE DÉBITO X</option><option value="012">NOTA DE DÉBITO C</option></optgroup>');
                        $('#modal-add-credit #type').append('<optgroup label="Nota Crédito"><option value="NCX">NOTA DE CRÉDITO X</option><option value="013">NOTA DE CRÉDITO C</option></optgroup>');
                    } else {
                        $('#modal-add-recargo #type').append('<optgroup label="Nota Débito"><option value="NDX">NOTA DE DÉBITO X</option></optgroup>');
                        $('#modal-add-credit #type').append('<optgroup label="Nota Crédito"><option value="NCX">NOTA DE CRÉDITO X</option></optgroup>');
                    }
                }
            }

            $.ajax({
                url: "<?= $this->Url->build(['controller' => 'Connections', 'action' => 'getConnectionsSelectByCustomer']) ?>",
                type: 'POST',
                dataType: "json",
                data: JSON.stringify({customer_code: customer_selected.code }),
                success: function(data) {

                     $('#connection-id').prop('required', true);
                     $('#connection-id').find('option').remove();

                     if (customer_selected.billing_for_service) {

                        $('.billing-_for-service-elements').show();

                        $('#connection-id').append('<option value="">Seleccione Servicio</option>');

                        var flag = true;
                        var first = -1;

                        $.each(data.connectionsArray, function(i, val) {

                            if (i != 0 && !(val.indexOf('Eliminado') != -1)) {
                                if (flag) {
                                    first = i;
                                    flag = false;
                                }
                                $('#connection-id').append('<option value="' + i + '">' + val + '</option>');
                            }
                        });

                        $.each(data.connectionsArray, function(i, val) {

                            if (i != 0 && val.indexOf('Eliminado') != -1) {
                                
                                $('#connection-id').append('<option value="' + i + '">' + val + '</option>');
                            }
                        });

                        $('#connection-id').append('<option value="0">Otras ventas</option>');

                        if (sessionPHP.paraments.invoicing.connection_auto_selected_cobranza) {
                            if (first != -1) {
                                $('#connection-id').val(first);
                            } else {
                                $('#connection-id').val(0);
                            }
                        }
                        
                        $('#connection-id').change();

                    } else {
                        $('.billing-_for-service-elements').hide();
                        $('#connection-id').append('<option value="0">Otras ventas</option>');
                        $('#connection-id').val(0);
                        $('#connection-id').change();
                    }
                }, 
                error: function (jqXHR) {
                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al intentar obtener las conexiones del cliente.');
                    }
                }
            });

            $.ajax({
                url: "<?= $this->Url->build(['controller' => 'Customers', 'action' => 'getDebtWithInvoice']) ?>",
                type: 'POST',
                dataType: "json",
                data: JSON.stringify({customer_code: customer_selected.code }),
                success: function(data) {

                    if (data.debts.total > 0) {
                        var value_total = '<span>Deudas sin Facturar. </span><br>';
                        value_total += '<span>Total: $ ' + data.debts.total + '</span>';
                        $('.btn-go-customer').prop('title', value_total);
                        $('.btn-go-customer').prop('data-original-title', value_total);
                        $('.btn-go-customer').attr('data-original-title', value_total);
                        $('.btn-go-customer').tooltip('show');

                    } else {
                        $('.btn-go-customer').prop('title', 'Ficha del Cliente');
                        $('.btn-go-customer').prop('data-original-title', 'Ficha del Cliente');
                        $('.btn-go-customer').attr('data-original-title', 'Ficha del Cliente');
                    }
                },
                error: function (jqXHR) {
                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al intentar obtener las Deudas sin Facturar del Cliente.');
                    }
                }
            });

            table_connections = $('#table-connections').DataTable({
    		    "order": [[ 0, 'desc' ]],
                "deferRender": true,
                "searching": false,
                "destroy": true,
                "info": false,
                "bDestroy": true,
                "ajax": {
                    "url": "/ispbrain/connections/get_accounts_status.json",
                    "dataSrc": "connections",
                    "data": function ( d ) {
                      return $.extend( {}, d, {
                        "customer_code": customer_selected ? customer_selected.code : 0,
                        "deleted": 0
                      });
                    },
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                				setTimeout(function() { 
                                    window.location.href = "/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
    		    "autoWidth": true,
    		    "scrollY": '450px',
    		    "scrollX": true,
    		    "scrollCollapse": true,
    		    "paging": false,
    		    "columns": [
                    { 
                        "data": "created",
                        "render": function ( data, type, row ) {
                            var result = "";
                            if (data != "") {

                                var created = data.split('T')[0];
                                created = created.split('-');
                                created =  created[2] + '/' + created[1] + '/' + created[0];

                                result += created + ' - ' + row.service.name + ' - ' + row.address;

                                var blocking = "";
                                if (row.blocking_date) {
                                    blocking = row.blocking_date.split('T')[0];
                                    blocking = blocking.split('-');
                                    blocking =  blocking[2] + '/' + blocking[1] + '/' + blocking[0];
                                    result += ' - Últ. bloq.: ' + blocking;
                                }

                                var enabling = "";
                                if (row.enabling_date) {
                                    enabling = row.enabling_date.split('T')[0];
                                    enabling = enabling.split('-');
                                    enabling =  enabling[2] + '/' + enabling[1] + '/' + enabling[0];
                                    result += ' - Últ. habilitación: ' + enabling;
                                }

                            } else {
                                result += 'Otras ventas';
                            }
                            return result;
                        }
                    },
                    {
                        "class": "right " + class_debt,
                        "data": "debt_month",
                        "render": function ( data, type, row ) {

                           if (customer_selected.billing_for_service) {
                               return number_format(data, true);
                           }
                           return '-';
                        }
                    }
                ],
    		    "columnDefs": [
    		        { "type": 'date-custom', targets: 0 },
                ],
                "createdRow" : function( row, data, index ) {
                    row.id = data.id;

                    if (!data.enabled) {
                        $(row).addClass('locked');
                    }
                    if (data.error) {
                        $(row).addClass('error-custom');
                    }
                },
    		    "language": dataTable_lenguage,
                "lengthMenu": [[ -1], ["Todas"]],
            	"dom":
        	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
       
    		});

            $('#table-connections tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {

                    table_connections.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');

                    var move_selected = table_connections.row( this ).data() ;

                    if (move_selected.created != "") {
                        $('.modal-connection').modal('show');
                    }
                }
            });

            $('#accordion').removeClass('d-none');
            $('.collapse').collapse('show');

            $('#modal-customer').modal('hide');
            clear_form_pay();
        }

        $('#btn-info-conn').click(function() {
            var action = '/ispbrain/connections/view/' + table_connections.$('tr.selected').attr('id');
            window.open(action, '_blank');
        });

        $('#btn-administrative-conn').click(function() {
            var action = '/ispbrain/connections/administrative/' + table_connections.$('tr.selected').attr('id');
            window.open(action, '_blank');
        });

        $('#btn-edit-conn').click(function() {
            var action = '/ispbrain/connections/edit/' + table_connections.$('tr.selected').attr('id');
            window.open(action, '_blank');
        });

        $('#btn-disable-conn').click(function() {

            if (!table_connections.$('tr.selected').hasClass('locked')) {

                var text = '¿Está seguro que desea deshabilitar está conexión?';
                var id  = table_connections.$('tr.selected').attr('id');

                bootbox.confirm(text, function(result) {
                    if (result) {
                        $('body')
                            .append( $('<form/>').attr({'action': '/ispbrain/connections/disableCobranza/', 'method': 'post', 'id': 'form-block'})
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                            .find('#form-block').submit();
                    }
                });
            } else {
                generateNoty('information', 'La conexión ya se encuentra deshabilitada');
            }
        });

        $('#btn-enable-conn').click(function() {

            if (table_connections.$('tr.selected').hasClass('locked')) {

                var text = '¿Está seguro que desea habilitar está conexión?';
                var id  = table_connections.$('tr.selected').attr('id');

                bootbox.confirm(text, function(result) {
                    if (result) {
                        $('body')
                            .append( $('<form/>').attr({'action': '/ispbrain/connections/enableCobranza/', 'method': 'post', 'id': 'form-block'})
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                            .find('#form-block').submit();
                    }
                });
            } else {
                generateNoty('information', 'La conexión ya se encuentra habilitada');
            }
        });

        $('#btn-delete-conn').click(function() {

            var text = '¿Está Seguro que desea eliminar la conexión ?';
            var id  = table_connections.$('tr.selected').attr('id');

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/connections/delete/', 'method': 'post', 'id': 'replacer'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer').submit();
                }
            });
        });

        $('#btns-tools-moves').hide();

        $('.btn-next-due-dates').click(function() {

            if ($(this).hasClass('btn-next-due-dates-selected')) {
                $(this).removeClass('btn-next-due-dates-selected');
            } else {
                $(this).addClass('btn-next-due-dates-selected');
            }

            table_moves.draw();
        });

        table_moves = $('#table-moves').DataTable({
            "deferRender": false,
            "processing": true,
            "serverSide": true,
            "searching": false,
            "paging": false,
            "ordering": false,
            "info": false,
            "ajax": {
                "url": "/ispbrain/Customers/get_moves.json",
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "where": {
                            "customer_code": (customer_selected) ? parseInt(customer_selected.code) : '',
                            "connection_id": $('#connection-id').val(),
                        },
                        "duedate": $('.btn-next-due-dates').hasClass('btn-next-due-dates-selected') ? 1 : 0,
                        "complete": 0
                    });
                },
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    response_data = json.response;

                    if (json.response.connection_saldo_month > 0) {
                        $('#total-to-pay').val(json.response.connection_saldo_month.toFixed(2));
                        $('#import').val(json.response.connection_saldo_month.toFixed(2));
                    } else {
                        $('#total-to-pay').val(0);
                        $('input#import').val(0);
                    }

                    $('#concept').focus();

                    return JSON.stringify( json.response );
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "drawCallback": function( settings ) {
                var flag = true;
            	switch (settings.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
            	    if (table_moves) {

                        var last_row = table_moves.row(':first').data();

                        if (last_row) {
                            if (last_row.saldo > 0) {
                                $('#table-moves tr#' + last_row.id).find('.column-saldo').addClass('last-cell-saldo-red');
                            } else {
                                $('#table-moves tr#' + last_row.id).find('.column-saldo').addClass('last-cell-saldo-green');
                            }
                        }
                    }
            	}
            },
		    "scrollY": true,
		    "scrollY": '200px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "columns": [
		        { 
                    "data": "date",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var date = data.split('T')[0];
                            date = date.split('-');
                            return date[2] + '/' + date[1] + '/' + date[0];
                        }
                    }
                },
                { 
                    "data": "tipo_comp",
                    "type": "options",
                    "options": {
                        'debt': 'Deuda sin facturar',
                    },
                    "render": function ( data, type, row ) {

                        if (row.type_move == 'debt') {
                            return 'Deuda sin facturar';
                        }
                        return sessionPHP.afip_codes.comprobantes[data];
                    }
                },
                { 
                    "data": "destino",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return sessionPHP.afip_codes.comprobantes[data];
                        }
                        return '';
                    }
                },
                { 
                    "data": "pto_vta",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return pad(data, 4);
                        }
                        return '';
                    }
                },
                { 
                    "data": "num",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return pad(data, 8);
                        }
                        return '';
                    }
                },
                { 
                    "data": "cae",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                { 
                    "data": "quantity"
                },
                { 
                     "class": "left",
                    "data": "description",
                    "type": "string"
                },
                { 
                    "class": "right",
                    "data": "subtotal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '$ ' )
                },
                { 
                    "class": "right",
                    "data": "sum_tax",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '$ ' )
                },
                { 
                    "class": "right",
                    "data": "total",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '$ ' )
                },
                { 
                    "data": "duedate",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {

                            var duedate = data.split('T')[0];
                          
                            var duedateview = duedate.split('-');
                            duedateview =  duedateview[2] + '/' + duedateview[1] + '/' + duedateview[0];

                            if (type == 'display') {

                                var date = new Date(duedate + ' 00:00:00');
                                var todayDate = new Date();
                                
                                if (date < todayDate) {
                                    return "<span class='text-danger'>" + duedateview + "</span>";
                                } else {
                                    return "<span class='text-info'>" + duedateview + "</span>";
                                }
                            }
                            return duedateview;
                        }

                        return '';
                    }
                },
                { 
                    "class": "column-saldo right",
                    "data": "saldo",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '$ ' )
                },
                { 
                    "data": "username",
                    "type": "string"
                },
                { 
                    "data": "cash_entity_name",
                    "type": "string"
                },
            ],
		    "columnDefs": [
                { 
                    "visible": false, targets: [2, 3, 4, 5, 6, 8, 9, 13, 14]
                },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
            "initComplete": function(settings, json) {
            },
		    "language": dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
        	"dom":
    	    	"<'row'<'col-xl-4 title'l><'col-xl-8 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-12'>>",
		});

		$('#table-moves_wrapper .tools').append($('#btns-tools-moves').contents());

		$('#btns-tools-moves').show();

		$('#table-moves tbody').on( 'click', 'tr', function () {

            if (!$(this).find('.dataTables_empty').length) {

                table_moves.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');

                move_selected = table_moves.row( this ).data();

                $('.modal-actions-move').modal('show');
            }
        });

		$('.btn-update-table-move').click(function() {
            if (table_moves) {
                table_moves.draw();
            }
        });

        $('.btn-go-to-receipts').click(function() {
            var action = '/ispbrain/Payments/index';
            window.open(action, '_blank');
        });

        $('.btn-go-customer').click(function() {
            var action = '/ispbrain/Customers/view/' + customer_selected.code;
            window.open(action, '_blank');
        });

        $('.btn-edit-customer').click(function() {
            var action = '/ispbrain/Customers/edit/' + customer_selected.code;
            window.open(action, '_blank');
        });

		$('#table-moves_wrapper .title').append('<h6>Historial</h6>');

		$('#concept').change(function() {

		   if ($(this).val() == 0) {
		       	$('#concept-custom').show();
		       	$('#concept-custom').attr('readonly', false);
		       	$('#concept-custom').attr('required', true);
		   } else {
		       $('#concept-custom').hide();
		       	$('#concept-custom').attr('readonly', true);
		       	$('#concept-custom').attr('required', false);
		   }
		});

		$('#concept').change();

		$('#payment-method-id').change(function() {

		    $('div.select').addClass('mb-0');

		    $('#payment_date-datetimepicker').hide(); //fecha de cobro del cheque
		    $('#bank').hide(); //seleccionde banco para los cheques
		    $('#account').hide(); //seleccionde banco para los tranferencias
		    $('#number-transaction').hide();
		    $('.account-info').html('');

		    showPaymentMethodData($(this).val());

		    $('#accounts').hide();
		    $('#account-id').val('');
            $('input#payment-method-id').val(1);
		});

		setTimeout(function() { 
		    $('#payment-method-id').change();
		}, 500);

		$('#payment_date-datetimepicker').datetimepicker({
            format: 'DD/MM/YYYY'
        });

		$('#btn-debt-month').click(function() {

		   if (response_data) {

		       $(this).addClass('btn-debt-selected');
		       $('#btn-debt-total').removeClass('btn-debt-selected');

		       if (response_data.connection_saldo_month > 0) {
		          $('#total-to-pay').val(response_data.connection_saldo_month);
		       } else {
		           $('#total-to-pay').val(0);
		       }

		       calculateVuelto();
		   }
		});

		$('#btn-debt-total').click(function() {

		   if (response_data) {

		       $(this).addClass('btn-debt-selected');
		       $('#btn-debt-month').removeClass('btn-debt-selected');

		       if (response_data.connection_saldo_total > 0) {
		          $('#total-to-pay').val(response_data.connection_saldo_total);
		       } else {
		           $('#total-to-pay').val(0);
		       }
		       calculateVuelto();
		   }
		});

        $('#card-customer-seeker').on('CUSTOMER_CLEAR', function() {
            clear_form_pay();
            clear_card_customer_seeker();
        });

        if ($('#card-customer-seeker #customer-code').val() != '') {
            $('#card-customer-seeker .btn-search').click(); 
        }

        $('input#import').on('keyup change keydown', function(event) {
            calculateVuelto();
        });

        $('#modal-customer').on('shown.bs.modal', function (e) {

            $('.btn-go-customer').tooltip('hide');

            if (sessionPHP.paraments.customer.search_list_customers) {

                enableLoad = 1;
                table_customers.draw();
            } else {
                clear_card_customer_seeker();
                $('#card-customer-seeker input#customer-ident').focus();
            }
        });

        $('.btn-submit').click(function(e) {

            $('.btn-submit').hide();

            var flag = true;

            var importe = $('#import').val();

            e.preventDefault();

            if (!customer_selected) {
                generateNoty('warning', 'Debe especificar un cliente.');
                flag = false;
            }

            if (importe.length == 0 || importe == 0 ) {
                generateNoty('warning', 'El importe debe ser mayor a 0');
                flag = false;
            }

            if (customer_selected && customer_selected.billing_for_service) {
                if ($('#connection-id').val() == '') {
                    generateNoty('warning', 'Debe seleccionar un servicio o sino marcar como otras ventas');
                    flag = false;
                }
            }

            if (flag) {

                bootbox.confirm('Confirmar Pago', function(result) {
                    if (result) {

                        var moves = table_moves.rows().data();
                        var send_data = {};  
                        send_data.moves = '';
                        var first = true;

                        $.each(moves, function(i, val) {

                            var move = '';

                            if (!first) {
                               move += '|'
                            }

                            move +=  val.date;
                            move +=  '&' + val.description;
                            move +=  '&' + val.total;
                            move +=  '&' + val.saldo;

                            send_data.moves += move;

                            first = false;
                        });

                        send_data.connection_id = null;

                        if (customer_selected.billing_for_service) {
                            send_data.connection_id = $('#connection-id').val() != 0 ? $('#connection-id').val() : null;
                        }

                        send_data.total_to_pay = importe;

                        send_data.customer_code = customer_selected.code;
                        send_data.customer_account_code = customer_selected.account_code;

                        var form_data = $('#form-pay').serializeArray();

                        $.each(form_data, function(i, val) {
                            send_data[val.name] = val.value;
                        });

                        if (mode_migration) {
                            send_data['created'] = $('#created').val();
                        }

                        $('body')
                            .append( $('<form/>').attr({'action': '/ispbrain/payments/add/', 'method': 'post', 'id': 'form-block'})
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'data', 'value': JSON.stringify(send_data)}))
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                            .find('#form-block').submit();
                    } else {
                        $('.btn-submit').show();
                    }
                });
            } else {
                $('.btn-submit').show();
            }
        });

        $('#btn-print-move').click(function() {

            var url = '';
            
            switch (move_selected.type_move) {
                case 'invoice':
                    url = '/ispbrain/Invoices/showprint/' + move_selected.id;
                    break;
                case 'payment':
                    url = '/ispbrain/payments/printReceipt/' + move_selected.id;
                    break;
                case 'debit':
                    url = '/ispbrain/DebitNotes/showprint/' + move_selected.id;
                    break;
                case 'credit':
                    url = '/ispbrain/CreditNotes/showprint/' + move_selected.id;
                    break;
            }
            window.open(url, 'Imprimir Comprobante', "width=600, height=500");
        });

        $('.btn-add-recargo').click(function() {
            if (customer_selected) {
                if (customer_selected.billing_for_service) {
                    if ($('#connection-id').val() == '') {
                        generateNoty('warning', 'Debe seleccionar un servicio o sino marcar como otras ventas');
                    } else {
                        $('#modal-add-recargo').modal('show');
                    }
                } else {
                    $('#modal-add-recargo').modal('show');
                }
            } else {
                generateNoty('warning', 'Debe especificar un cliente.');
            }
        });

        $('#form-add-recargo').submit(function(e) {

            e.preventDefault();

            if ($('#modal-add-recargo #import').val() == 0) {
                generateNoty('warning', 'El importe debe ser mayor a cero.');
            } else {

                $('.btn-add-recargo-popup').hide();

                var connection_id = null;

                if (customer_selected.billing_for_service) {
                    connection_id = $('#connection-id').val() != 0 ? $('#connection-id').val() : null;
                }

                var payload = {
                    customer_code: customer_selected.code,
                    type:          $('#modal-add-recargo #type').val(),
                    total:         $('#modal-add-recargo #import').val(),
                    connection_id: connection_id,
                    concept:   $('#modal-add-recargo #concept').val()
                };

                var request = $.ajax({
                    url: "/ispbrain/DebitNotes/addRecargos",
                    method: "POST",
                    data: JSON.stringify(payload),
                    dataType: "json"
                });

                request.done(function( response ) {

                    $('.btn-add-recargo-popup').show();

                    if (!response.data.error) {
                        if (table_moves) {
                            generateNoty('success', 'Recargo agregado correctamente');
                            table_moves.draw();
                            $('#modal-add-recargo').modal('hide');
                        }
                    } else {
                        generateNoty('warning', response.data.msg);
                    }
                });

                request.fail(function(jqXHR) {
                    
                    $('.btn-add-recargo-popup').show();

                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error','Error al intentar agregar el recargo');
                    }
                });
            }
        });

        $('.btn-add-credit').click(function() {
            if (customer_selected) {
                if (customer_selected.billing_for_service) {
                    if ($('#connection-id').val() == '') {
                        generateNoty('warning', 'Debe seleccionar un servicio o sino marcar como otras ventas');
                    } else {
                        $('#modal-add-credit').modal('show');
                    }
                } else {
                    $('#modal-add-credit').modal('show');
                }
            } else {
                generateNoty('warning', 'Debe especificar un cliente.');
            }
        });

        $('#form-add-credit').submit(function(e) {

            e.preventDefault();

            if ($('#modal-add-credit #import').val() == 0) {
                generateNoty('warning', 'El importe debe ser mayor a cero.');
            } else {

                $('.btn-add-credit-popup').hide();

                var connection_id = null;

                if (customer_selected.billing_for_service) {
                    connection_id = $('#connection-id').val() != 0 ? $('#connection-id').val() : null;
                }

                var payload = {
                    customer_code: customer_selected.code,
                    type:          $('#modal-add-credit #type').val(),
                    total:         $('#modal-add-credit #import').val(),
                    connection_id: connection_id,
                    concept:   $('#modal-add-credit #concept').val()
                };

                var request = $.ajax({
                    url: "/ispbrain/CreditNotes/addCreditos",
                    method: "POST",
                    data: JSON.stringify(payload),
                    dataType: "json"
                });

                request.done(function( response ) {

                    $('.btn-add-credit-popup').show();

                    if (!response.data.error) {
                        if (table_moves) {
                            generateNoty('success', 'Crédito agregado correctamente');
                            table_moves.draw();
                            $('#modal-add-credit').modal('hide');
                        }
                    } else {
                        generateNoty('warning', response.data.msg);
                    }
                });

                request.fail(function(jqXHR) {
                    
                    $('.btn-add-credit-popup').show();

                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() {
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error','Error al intentar agregar el crédito');
                    }
                });
            }
        });

        $('.btn-observations').click(function() {
            customer_code = customer_selected.code
            $('#modal-add-observation').modal('show');
        });

        $('.btn-accounts').click(function() {
            $('#modal-accounts').modal('show');
        });

        $('#modal-accounts').on('ACCOUNT_SELECTED', function() {
            cleanDataAccountSelection();
            var account_selectedx = account_selected;

            $('#account-id').val(account_selectedx.id);
            addAccountSelected(account_selectedx);
            $('#payment-method-id').val(account_selectedx.payment_getway_id);
            showPaymentMethodData(account_selectedx.payment_getway_id);
            addCleanButton();
        });

        // para mostrar info del cliente al cual recien se le genero un pago
        if (customer_load != null) {
            customer_selected = customer_load;
            afterSelectedCustomer();
        }
    });

    function showPaymentMethodData(id)
    {
        if (id == 2) { //cheque 

	        $('#payment_date-datetimepicker').show();
            $('#bank').show();
            $('#number-transaction').show();
	    } else if (id == 3) { // Tranferencias o Depositos

	        $('#account').show();
	        $('#number-transaction').show();
	    } else if (id == 4) { // Tarjeta de Debito 

	         $('#number-transaction').show();
	    } else if (id == 5) { // Tarjeta de Credito 

	         $('#number-transaction').show();
	    } else if (id == 6) { // Pim 

	         $('#number-transaction').show();
	    } else if (id == 7) { // Payu 

	         $('#number-transaction').show();
	    } else if (id == 99) { // Cobrodigital 

	         $('#number-transaction').show();
        } else if (id == 100) { // TodoPago 

	         $('#number-transaction').show();
	    } else if (id == 101) { // MercadoPago 

	         $('#number-transaction').show();
	    } else if (id == 102) { // Debito automatico chubut 

	        $('#number-transaction').show();
	    } else if (id == 104) { // Debito automatico cobro digital 

	         $('#number-transaction').show();
	    } else if (id == 105) { // Cuenta Digital 

	         $('#number-transaction').show();
	    } else if (id == 106) { // Visa Debito Automatico

	         $('#number-transaction').show();
	    } else if (id == 107) { // MasterCard Debito Automatico

	         $('#number-transaction').show();
	    } else if (id == 108) { // Rapipago

            $('#number-transaction').show();
        }
    }

    function addAccountSelected(account)
    {
        var structure = '';

        var payment_name = "";
        $.each(payment_getway.methods, function( index, pg ) {
            if (pg.id == account.payment_getway_id) {
                payment_name = pg.name;
            }
        });

        if (account.hasOwnProperty('id_comercio')) {

            var credential_name = '';
            var credential_id_comercio = '';

            $.each(credentials, function( index, credential ) {
                if (credential.idComercio == account.id_comercio) {
                    credential_name = credential.name;
                    credential_id_comercio = credential.idComercio;
                }
            });
        }

        switch (account.payment_getway_id) {

            case 7:
                structure =
                    '<div class="col-xl-12 mt-3">' +
                        '<div class="row">' +
                            '<div class="col-xl-12 account-containx">' +
                                '<label class=""><b class="">Cuenta seleccionada: </b>' + payment_name + '</label>' +
                            '</div>' +
                            '<div class="col-xl-12">' +
                                '<label class=""><b class="">Cód. Barra: </b>' + account.barcode + '</label>' +
                            '</div>' +
                        '</div>' +
                    '</div>';
                break;

            case 99:
                structure =
                    '<div class="col-xl-12 mt-3">' +
                        '<div class="row">' +
                            '<div class="col-xl-12 account-containx">' +
                                '<label class=""><b class="">Cuenta seleccionada: </b>' + payment_name + '</label>' +
                            '</div>' +
                            '<div class="col-xl-12">' +
                                '<label class=""><b class="">Cód. Barra: </b>' + account.barcode + '</label>' +
                            '</div>' +
                        '</div>' +
                    '</div>';
                break;

            case 102:
                structure =
                    '<div class="col-xl-12 mt-3">' +
                        '<div class="row">' +
                            '<div class="col-xl-12 account-containx">' +
                                '<label class=""><b class="">Cuenta seleccionada: </b>' + payment_name + '</label>' +
                            '</div>' +
                            '<div class="col-xl-12">' +
                                '<label class=""><b class="">CBU: </b>' + account.cbu + '</label>' +
                            '</div>' +
                        '</div>' +
                    '</div>';
                break;

            case 104:
                structure =
                    '<div class="col-xl-12 mt-3">' +
                        '<div class="row">' +
                            '<div class="col-xl-12 account-containx">' +
                                '<label class=""><b class="">Cuenta seleccionada: </b>' + payment_name + '</label>' +
                            '</div>' +
                            '<div class="col-xl-12">' +
                                '<label class=""><b class="">CBU: </b>' + account.cbu + '</label>' +
                            '</div>' +
                        '</div>' +
                    '</div>';
                break;

            case 105:
                structure =
                    '<div class="col-xl-12 mt-3">' +
                        '<div class="row">' +
                            '<div class="col-xl-12 account-containx">' +
                                '<label class=""><b class="">Cuenta seleccionada: </b>' + payment_name + '</label>' +
                            '</div>' +
                            '<div class="col-xl-12">' +
                                '<label class=""><b class="">Cód. Barra: </b>' + account.barcode + '</label>' +
                            '</div>' +
                        '</div>' +
                    '</div>';
                break;

            case 106:
                structure =
                    '<div class="col-xl-12 mt-3">' +
                        '<div class="row">' +
                            '<div class="col-xl-12 account-containx">' +
                                '<label class=""><b class="">Cuenta seleccionada: </b>' + payment_name + '</label>' +
                            '</div>' +
                            '<div class="col-xl-12">' +
                                '<label class=""><b class="">Nro Tarjeta: </b>' + account.card_number + '</label>' +
                            '</div>' +
                        '</div>' +
                    '</div>';
                break;

            case 107:
                structure =
                    '<div class="col-xl-12 mt-3">' +
                        '<div class="row">' +
                            '<div class="col-xl-12 account-containx">' +
                                '<label class=""><b class="">Cuenta seleccionada: </b>' + payment_name + '</label>' +
                            '</div>' +
                            '<div class="col-xl-12">' +
                                '<label class=""><b class="">Nro Tarjeta: </b>' + account.card_number + '</label>' +
                            '</div>' +
                        '</div>' +
                    '</div>';
                break;
        }
        $('.account-info').append(structure);
    }

    function addCleanButton() {
        $('.account-containx').append("<a href='javascript:void(0)' title='Quitar cuenta' class='btn btn-default btn-clean-account-selected mb-3 ml-2'><span class='glyphicon icon-bin'></span></a>");
        $(".btn-clean-account-selected").on( "click", function() {
            cleanAccountSelection();
        });
    }

    function cleanAccountSelection() {
        var text = "¿Está seguro que desea limpiar la seleccion de cuenta?";

        bootbox.confirm(text, function(result) {

            if (result) {
                cleanDataAccountSelection();
            }
        });
    }

    function cleanDataAccountSelection() {
        $('#account-id').val('');
        $('#payment-method-id').val(1).trigger('change');
        $('.account-info').html("");
    }

    function calculateVuelto() {

        var importe = $('input#import').val();

        var total = $('#total-to-pay').val();
        vuelto = importe - total;

        if (vuelto <= 0) {

            $('input#vuelto').val(0.00);
            $('#is-credit').attr('disabled', true);

        } else {

            $('input#vuelto').val(vuelto.toFixed(2));
            $('#is-credit').attr('disabled', false);
        }
    }

    function clear_form_pay() {

        $('.btn-next-due-dates').removeClass('btn-next-due-dates-selected');

        $('#payment-method-id').val(1).trigger('change');
        $('input#total-to-pay').val(0);
        $('input#import').val(0);
        $('input#vuelto').val(0);
        $('input#concept').val('');
        $('input#concept-custom').val('');
        $('.account-info').html('');
        $('#account-id').val('');
    }

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    $(document).click(function() {
        $('[data-toggle="tooltip"]').tooltip('hide');
    });

</script>
