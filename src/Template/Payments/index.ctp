<style type="text/css">

    .anulated {
        background-color: #dc3545;
        color: white !important;
    }

</style>

<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5">

        <?php

            echo $this->Html->link(
                '<span class="fas fa-sync-alt" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Actualizar la tabla',
                'class' => 'btn btn-default btn-update-table ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-eye" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Ocultar o Mostrar Columnas',
                'class' => 'btn btn-default btn-hide-column ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon fas fa-search" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Buscar por Columnas',
                'class' => 'btn btn-default btn-search-column ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<i class="fas fa-download"></i>',
                'javascript:void(0)',
                [
                'title' => 'Descargar Recibos',
                'class' => 'btn btn-default btn-download ml-1',
                'escape' => false
            ]);
        ?>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-hover" id="table-payments">
            <thead>
                <tr>
                    <th>Fecha</th>                   <!--0-->
                    <th>Comp.</th>                   <!--1-->
                    <th>Pto. Vta.</th>               <!--2-->
                    <th>Número</th>                  <!--3-->
                    <th>Concepto</th>                <!--4-->
                    <th class="importe">Importe</th> <!--5-->
                    <th>Código</th>                  <!--6-->
                    <th>Tipo</th>                    <!--7-->
                    <th>Nro</th>                     <!--8-->
                    <th>Nombre</th>                  <!--9-->
                    <th>Domicilio</th>               <!--10-->
                    <th>Ciudad</th>                  <!--11-->
                    <th>Método de pago</th>          <!--12-->
                    <th>Caja</th>                    <!--13-->
                    <th>Usuario</th>                 <!--14-->
                    <th>Estado</th>                  <!--15-->
                    <th>Nro de Referencia</th>       <!--16-->
                    <th>Empresa Facturación</th>     <!--17-->
                    <th>Nro asiento</th>             <!--18-->
                    <th>Cód. Barra</th>              <!--19-->
                </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
                <tr>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-print-payment',
        'name' =>  'Imprimir',
        'icon' =>  'icon-printer',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-go-customer',
        'name' =>  'Ficha del Cliente',
        'icon' =>  'glyphicon icon-profile',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-edit-administrative-movement',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-success'
    ];

    $buttons[] = [
        'id'   =>  'btn-send-email-payment',
        'name' =>  'Enviar Correo',
        'icon' =>  'fab fa-telegram-plane',
        'type' =>  'btn-success'
    ];

    $buttons[] = [
        'id'   =>  'anulate-payment',
        'name' =>  'Anular Pago',
        'icon' =>  'icon-undo2',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-payments', 'title' => 'Acciones', 'buttons' => $buttons]);
    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-payments']);
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-payments']);
    echo $this->element('modal_preloader');
    echo $this->element('AdministrativeMovement/edit_administrative_movement', ['modal' => 'modal-edit-administrative-movement', 'title' => 'Editar']);
    echo $this->element('Email/send', ['modal'=> 'modal-send-email', 'title' => 'Seleccionar Plantilla', 'mass_emails_templates' => $mass_emails_templates ]);
?>

<div class="modal fade" id="modal-anulate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Anular Pago</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <?= $this->Form->create() ?>

                    <div class="row" id="form-anulate-payment">

                        <div class="col-md-12">
                            <?= $this->Form->input('cash_entity_id', ['label' => __('Caja'), 'options' => $cash_entities_array ]) ?>
                        </div>

                    </div>

                <?= $this->Form->end() ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn-anulated-payment" data-dismiss="modal">Anular</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var cash_entities = <?= json_encode($cash_entities) ?>;

    var table_payments = null;
    var payment_selected = null;

    var customer_selected = null;
    var move_selected = null;
    var table_selected = null;
    var mass_emails_templates = null;
    var users = null;
    var paymentMethods = null;

    $(document).ready(function() {
        
        var options_business = [];
        
        $.each(sessionPHP.paraments.invoicing.business, function(i, val) {
            options_business.push({id:val.id, name:val.name + ' (' + val.address + ')'});
        });

        mass_emails_templates = <?= json_encode($mass_emails_templates) ?>;
        users = <?= json_encode($users) ?>;
        paymentMethods = <?= json_encode($paymentMethods) ?>;

        $('.btn-hide-column').click(function() {
            $('.modal-hide-columns-payments').modal('show');
        });

        $('.btn-search-column').click(function() {
            $('.modal-search-columns-payments').modal('show');
        });

        $('#btns-tools').hide();

        loadPreferences('payments4-index', [10, 11, 14, 15, 16, 17, 18]);

        var hide_force_payments = [];
        var no_search_payments= [];

        if (!sessionPHP.paraments.accountant.account_enabled) {
            hide_force_payments = [18];
            no_search_payments = [18];
        }

        table_payments = $('#table-payments').DataTable({
		    "order": [[ 0, 'desc' ]],
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "/ispbrain/payments/get_payments.json",
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "drawCallback": function( settings ) {
                var flag = true;
            	switch (settings.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
            	    if (table_payments) {

                        var tableinfo = table_payments.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }
                    }
            	}

            	this.api().columns('.importe').every(function() {
                    var column = this;

                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    var sum = column
                        .data()
                        .reduce(function (a, b) { 

                           return intVal(a) + intVal(b);
                        }, 0);

                    $(column.footer()).html('$' + sum.toFixed(2));
                });
            },
		    "autoWidth": true,
		    "scrollY": '370px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
		    "columns": [
                {
                    "data": "created",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        var created = data.split('T')[0];
                        created = created.split('-');
                        return created[2] + '/' + created[1] + '/' + created[0];
                    }
                },
                {
                    "data": "receipt.tipo_comp",
                    "type": "options",
                    "options": {
                        "XRX": "RX",
                        "FRX": "FRX"
                    },
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.comprobantesLetter[data];
                    }
                },
                {
                    "data": "receipt.pto_vta",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 4);
                    }
                },
                {
                    "data": "receipt.num",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 8);
                    }
                },
                {
                    "data": "concept",
                    "type": "string",
                },
                {
                    "data": "import",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' )
                },
                {
                    "data": "receipt.customer_code",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                {
                    "className": " center",
                    "data": "receipt.customer_doc_type",
                    "type": "options",
                    "options": sessionPHP.afip_codes.doc_types,
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.doc_types[data];
                    }
                },
                {
                    "className": " center",
                    "data": "receipt.customer_ident",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (data) {
                            ident = data;
                        }
                        return ident;
                    }
                },
                {
                    "data": "receipt.customer_name",
                    "type": "string"
                },
                {
                    "data": "receipt.customer_address",
                    "type": "string"
                },
                { 
                    "data": "receipt.customer_city",
                    "type": "string"
                },
                {
                    "data": "payment_method.name",
                    "type": "options",
                    "options": paymentMethods,
                },
                {
                    "data": "cash_entity",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data.name;
                        }
                        return '';
                    }
                },
                {
                    "className": "",
                    "data": "user.name",
                    "type": "options",
                    "options": users,
                    "render": function ( data, type, row ) {
                        return users[data];
                    }
                },
                {
                    "data": "anulated",
                    "type": "options",
                    "options": {
                        "0":"Anulado",
                        "1":"No Anulado",
                    },
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                        return '';
                    }
                },
                {
                    "class": "left",
                    "data": "number_transaction",
                    "type": "string"
                },
                { 
                    "data": "receipt.business_id",
                    "type": "options_obj",
                    "options": options_business,
                    "render": function ( data, type, row ) {
                        var business_name = '';
                        $.each(options_business, function (i, b) {
                            if (b.id == data) {
                                business_name = b.name;
                            }
                        });
                        return business_name;
                    }
                },
                {
                    "data": "seating_number",
                    "type": "string"
                },
                {
                    "class": "left",
                    "data": "receipt.barcode_alt",
                    "type": "string"
                },
            ],
		    "columnDefs": [
		        { 
                    "class": "left", targets: [4, 10]
                },
                { 
                    "visible": false, targets: hide_columns
                },
                { 
                    "visible": false, targets: hide_force_payments
                },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.receipt_id;
                if (data.anulated) {
                    $(row).addClass('anulated');
                }
            },
		    "language": dataTable_lenguage,
		    "pagingType": "numbers",
        	"lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
        	"dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
		});

        $('#table-payments_wrapper .tools').append($('#btns-tools').contents());

        $('#table-payments').on( 'init.dt', function () {
            createModalHideColumn(table_payments, '.modal-hide-columns-payments', hide_force_payments);
            createModalSearchColumn(table_payments, '.modal-search-columns-payments', no_search_payments);
        });

        $('.btn-update-table').click(function() {
             if (table_payments) {
                table_payments.ajax.reload();
            }
        });

        $('#btns-tools').show();

		$('#table-payments tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                table_payments.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
                $('#btn-edit-administrative-movement').addClass('my-hidden');

                payment_selected = table_payments.row( this ).data();
                if (payment_selected.customer.billing_for_service) {
                    move_selected = {
                        'id': payment_selected.receipt_id,
                        'model': 'Receipts'
                    };
                    customer_selected = {
                        code: payment_selected.receipt.customer_code
                    }
                    $('#btn-edit-administrative-movement').removeClass('my-hidden');
                }

                if (payment_selected.anulated) {
                    $('#anulate-payment').hide();
                } else {
                    if (payment_selected.cd_id_transaccion) {
                        $('#anulate-payment').hide();
                    } else {
                        $('#anulate-payment').show();
                    }
                }

                $('.modal-payments').modal('show');
            }
        });

        $('#btn-print-payment').click(function() {
            var url = '/ispbrain/payments/printReceipt/' + payment_selected.receipt_id;
            window.open(url, 'Imprimir Resumen', "width=600, height=500");
        });

        $('#btn-go-customer').click(function() {
            var action = '/ispbrain/customers/view/' + payment_selected.receipt.customer_code;
            window.open(action, '_self');
        });

        $('.btn-download').click(function() {

            var ids = [];

            table_payments.rows({search: 'applied'}).every( function ( rowIdx, tableLoop, rowLoop ) {
                ids.push( this.data().receipt_id );
            });

            if (ids.length > 0) {

                $('#replacer').remove();

                $('body')
                    .append( $('<form/>').attr({'action': '/ispbrain/download/receipts/', 'method': 'post', 'id': 'replacer'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'ids', 'value': ids}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer').submit();
            } else {
                generateNoty('information', 'No hay recibos para descargar');
            }
        });

        $('#anulate-payment').click(function() {
            $('.modal-payments').modal('hide');
            $('#modal-anulate').modal('show');
        });

        $('#btn-edit-administrative-movement').click(function() {
            $('.modal-payments').modal('hide');
            table_selected = table_payments;
            $('.modal-edit-administrative-movement').modal('show');
        });

        $('#modal-anulate').on('shown.bs.modal', function (e) {

            $('#form-anulate-payment').addClass('d-none');

            if (sessionPHP.paraments.invoicing.use_cash_for_payment_anulate) {
                $('#form-anulate-payment').removeClass('d-none');
            }
        });

        $('#btn-anulated-payment').click(function() {

            var cash = false;
            var flag = true;

            if (sessionPHP.paraments.invoicing.use_cash_for_payment_anulate) {
                flag = false;
                cash = true;
            }

            var cash_entity_id = $('#cash-entity-id').val();

            if (cash) {

                if (!cash_entity_id) { 
                    generateNoty('warning', 'Debe seleccionar una caja');
                }

                var flag = false;

                $.each(cash_entities, function(i, cash_entity) {

                    if (cash_entity.id == cash_entity_id) {

                        if (payment_selected.payment_method_id == 1) {

                            if (cash_entity.contado >= payment_selected.import) {
                                flag = true;
                            } else {
                                generateNoty('warning', 'El importe del recibo es mayor al saldo de caja, en la la categoría Efectivo.');
                            }
                        } else {
                           if (cash_entity.cash_other >= payment_selected.import) {
                                flag = true;
                           } else {
                                generateNoty('warning', 'El importe del recibo es mayor al saldo de caja, en la la categoría Otros medios de pago.');
                           }  
                        }
                   }
                });
            }

            if (flag) {

                bootbox.confirm('Confirmar Anulación', function(result) {

                    if (result) {

                        openModalPreloader("Anulando Pago ...");

                        $.ajax({
                			type: 'POST',
                			dataType: "json",
                			url: '/ispbrain/payments/anulate',
                			data:  JSON.stringify({ id: payment_selected.id, cash_entity_id: cash_entity_id, cash: cash }),
                			success: function(data) {

                				$('.modal-payments').modal('hide');

                		    	closeModalPreloader();

                				if (data.response.error) {
                					generateNoty(data.response.typeMsg, data.response.msg);
                				} else {
                					generateNoty(data.response.typeMsg, data.response.msg);
                					table_payments.ajax.reload();
                				}
                			},
                			error: function(jqXHR) {
                			     if (jqXHR.status == 403) {

                                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                                	setTimeout(function() { 
                                        window.location.href = "/ispbrain";
                                	}, 3000);

                                } else {
                                	closeModalPreloader();
                			        generateNoty('error', 'Error al intentar anular el pago.');
                                }
                			}
                		});
                    }
              });
            }
        });

        $(".btn-export").click(function() {

            bootbox.confirm('Se descargará un archivo Excel', function(result) {
                if (result) {
                    $('#table-payments').tableExport({tableName: 'Recibos', type:'excel', escape:'false', columnNumber: [5, 6]});
                }
            }).find("div.modal-content").addClass("confirm-width-md");
        });

        $('#btn-send-email-payment').click(function() {
            if (payment_selected.customer.email == "" || payment_selected.customer.email == null) {
                generateNoty('warning', 'Debe agregar el Correo Electrónico del Cliente al cual pertenece el Recibo.');
            } else {
                var count = 0;
                $.each(mass_emails_templates, function( index, value ) {
                    count++;
                });
                if (count > 1) {
                    model = 'Receipts';
                    model_id = payment_selected.receipt.id;
                    $('.modal-payments').modal('hide');
                    $('.modal-send-email').modal('show');
                } else {
                    generateNoty('warning', 'No cuenta con plantillas de Correos.');
                }
            }
        });

        $('a[id="payments-tab"]').on('shown.bs.tab', function (e) {
            table_payments.draw();
        });
    });

</script>
