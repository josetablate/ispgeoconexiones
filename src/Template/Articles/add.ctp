<div class="row">
     <div class="col-md-3">
        <div class="text-right">
           <a class="btn btn-default" title="Ir a la lista de artículos." href="<?=$this->Url->build(["action" => "index"])?>" role="button">
                <span class="glyphicon icon-arrow-left"  aria-hidden="true"></span>
            </a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
         <?= $this->Form->create($article,  ['class' => 'form-load']) ?>
        <fieldset>
            <?php
                echo $this->Form->input('code', ['label' => 'Código *']);
                echo $this->Form->input('name', ['label' => 'Nombre *']);
                // echo $this->Form->input('enabled', ['label' => 'Habilitar/Deshabilitar', 'checked' => true, 'class' => 'input-checkbox' ]);
            ?>
        </fieldset>
        <?= $this->Html->link(__('Cancelar'),["controller" => "Articles", "action" => "index"], ['class' => 'btn btn-default']) ?>
            <?= $this->Form->button(__('Agregar'), ['class' => 'btn-submit btn-success']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
