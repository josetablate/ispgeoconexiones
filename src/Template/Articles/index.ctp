<style type="text/css">

    .intput-enabled {
        width: 100%;
    }

</style>

<div id="btns-tools">

    <div class="text-right btns-tools margin-bottom-5">

        <?php

            echo $this->Html->link(
                '<span class="fa fa-plus" aria-hidden="true"></span>',
                ['controller' => 'Articles', 'action' => 'add'],
                [
                'title' => 'Agregar nuevo artículo',
                'class' => 'btn btn-default ',
                'escape' => false
                ]);

             echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export',
                'escape' => false
                ]);
        ?>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-hover table-bordered" id="table-articles">
            <thead>
                <tr>
                    <th>Activo</th>
                    <th>Código</th>
                    <th>Nombre</th>
                    <th>Disponible</th>
                    <th>Histórico</th>
                    <th>Creado</th>
                    <th>Modificado</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-edit',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-default'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-articles', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Editar Artículo</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-md-12">

                        <form method="post" accept-charset="utf-8" role="form" action="<?= $this->Url->build(["controller" => "Articles", "action" => "edit"]) ?>">
                            <div style="display:none;">
                                <input type="hidden" name="_method" value="POST">
                            </div>
                            <?php
                                echo $this->Form->input('code', ['label' => 'Código *']);
                                echo $this->Form->input('name', ['label' => 'Nombre *']);
                            ?>

                            <input type="hidden" name="id" id="id"/>
                            <?php echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]); ?>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-success" id="btn-confirm-edit">Guardar</button> 

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var table_articles = null;
    var article_selected = null;

    $(document).ready(function () {
 
        $('#table-articles').removeClass('display');

    	table_articles = $('#table-articles').DataTable({
    	    "order": [[ 0, 'desc' ]],
    	    "autoWidth": true,
    	    "scrollY": '350px',
    	    "scrollCollapse": true,
    	    "scrollX": true,
    	    "sWrapper":  "dataTables_wrapper dt-bootstrap4",
            "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/articles/index.json",
                "dataSrc": "articles",
                "error": function(c) {
                    switch (c.status) {
                        case 400:
                            flag = false;
                            generateNoty('warning', 'Ha ocurrido un error.');
                            break;
                        case 401:
                            flag = false;
                            generateNoty('warning', 'Error, no posee una autorización.');
                            break;
                        case 403:
                            flag = false;
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                        	setTimeout(function() { 
                        	  window.location.href ="/ispbrain";
                        	}, 3000);
                            break;
                    }
                }
            },
            "columns": [
                { 
                    "data": "enabled",
                    "render": function ( data, type, row ) {
                        var enabled = '<i class="fa fa-times red" aria-hidden="true"></i>';
                        if (data) {
                            enabled = '<i class="fa fa-check green" aria-hidden="true"></i>';
                        }
                        return enabled;
                    }
                },
                { 
                    "data": "code",
                    "render": function ( data, type, row ) {
                        return data.toUpperCase();
                    }
                },
                { "data": "name" },
                { "data": "current_amount" },
                { "data": "total_amount" },
                { 
                    "data": "created",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                { 
                    "data": "modified",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
            ],
            "columnDefs": [
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
    	    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            "dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
    	});

		$('#table-articles_wrapper .tools').append($('#btns-tools').contents());

    });

	$('#table-articles tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_articles.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            article_selected = table_articles.row( this ).data();

            $('.modal-articles').modal('show');
        }
    });

    $('#btn-edit').click(function() {

        $('.modal-actions').modal('hide');

        $('#modal-edit #id').val(article_selected.id);
        $('#modal-edit #code').val(article_selected.code);
        $('#modal-edit #name').val(article_selected.name);

        if (article_selected.enabled == '1') {
          $('#modal-edit #enabled').closest('label').addClass('ui-state-active');
          $('#modal-edit #enabled').closest('label').addClass('ui-checkboxradio-checked');
          $('#modal-edit #enabled').attr('checked', true);
        } else {
          $('#modal-edit #enabled').attr('checked', false);
          $('#modal-edit #enabled').closest('label').removeClass('ui-state-active');
          $('#modal-edit #enabled').closest('label').removeClass('ui-checkboxradio-checked');
        }

        $('#modal-edit #enabled').val(article_selected.enabled);
        $('#modal-edit').modal('show');
    });

    $('#modal-edit #enabled').click(function() {

        if ($('#modal-edit #enabled').val() == '1') {
            $('#modal-edit #enabled').val(0);
        } else {
            $('#modal-edit #enabled').val(1);
        }
    });

    $('#btn-delete').click(function() {

        var text = "¿Está Seguro que desea eliminar el artículo?";
        var id = article_selected.id;

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                    .append( $('<form/>').attr({'action': '/ispbrain/articles/delete/', 'method': 'post', 'id': 'replacer'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer').submit();
            }
        });
    });

    $(".btn-export").click(function() {

        bootbox.confirm('Se descargará un archivo Excel', function(result) {
            if (result) {
                $('#table-articles').tableExport({tableName: 'Articulos', type:'excel', escape:'false'});
            }
        });
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
