<style type="text/css">

    #map {
		height: 350px;
	}

    .w-226 {
        width: 226px;
    }

    .text {
        font-size: 21px;
    }

</style>


<!--contadores-->
<div class="row">
    
    

    <div class="col-sm-6 col-md-6 col-lg-6 col-xl-3 mb-2 pr-1" id="Home-counter_customers">
        <div class="card border-success">
            <h5 class="card-header">Clientes</h5>
            <div class="card-body text-success pl-2 pr-2">
               <div class="input-group mb-2">
                   <span class="input-group-btn w-50">
                    <button class="btn btn-success w-100" type="button">Total</button>
                   </span>
                    <input type="text" class="form-control text-right" id="" placeholder="" value="<?= $data['total_customers'] ?>" readonly>
                </div>
                <div class="input-group mb-2">
                   <span class="input-group-btn w-50">
                    <button class="btn btn-success w-100" type="button">Nuevos (mes)</button>
                   </span>
                    <input type="text" class="form-control text-right" id="" placeholder="" value="<?= $data['customers_new'] ?>" readonly>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-md-6 col-lg-6 col-xl-3 mb-2 pr-1 pl-1" id="Home-counter_connections">
        <div class="card border-info">
            <h5 class="card-header">Conexiones</h5>
            <div class="card-body text-info  pl-2 pr-2">
               <div class="input-group mb-2">
                   <span class="input-group-btn w-50">
                    <button class="btn btn-info w-100" type="button">Total</button>
                   </span>
                    <input type="text" class="form-control text-right" id="" placeholder="" value="<?= $data['total_connections'] ?>" readonly>
                </div>
                <div class="input-group mb-2">
                   <span class="input-group-btn w-50">
                    <button class="btn btn-info w-100" type="button">Bloqueadas</button>
                   </span>
                    <input type="text" class="form-control text-right" id="" placeholder="" value="<?= $data['total_connections_lock'] ?>" readonly>
                </div>
                <div class="input-group mb-2">
                   <span class="input-group-btn w-50">
                    <button class="btn btn-info w-100" type="button">Nuevas (mes)</button>
                   </span>
                    <input type="text" class="form-control text-right" id="" placeholder="" value="<?= $data['connections_new'] ?>" readonly>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-md-6 col-lg-6 col-xl-3 mb-2 pr-1 pl-1" id="Home-counter_tickets">
        <div class="card border-secondary">
            <h5 class="card-header">Tickets</h5>
            <div class="card-body text-secondary  pl-2 pr-2">
               <div class="input-group mb-2">
                   <span class="input-group-btn w-50">
                    <button class="btn btn-secondary w-100" type="button">Abiertos</button>
                   </span>
                    <input type="text" class="form-control text-right" id="" placeholder="" value="<?= $data['tickets_opened'] ?>" readonly>
                </div>
                <div class="input-group mb-2">
                   <span class="input-group-btn w-50">
                    <button class="btn btn-secondary w-100" type="button">Asignados</button>
                   </span>
                    <input type="text" class="form-control text-right" id="" placeholder="" value="<?= $data['tickets_opened_asign'] ?>" readonly>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-md-6 col-lg-6 col-xl-3 mb-2 pl-1" id="Home-counter_payments">
        <div class="card border-warning">
            <h5 class="card-header">Cobranza</h5>
            <div class="card-body text-warning  pl-2 pr-2">
                <div class="input-group mb-2">
                   <span class="input-group-btn w-50">
                    <button data-toggle='tooltip' class="btn btn-warning w-100" title="Última generación de deudas masivas" type="button">Deuda generada (mes)</button>
                   </span>
                    <input type="text" class="form-control text-right" id="" placeholder="" value="$<?= number_format($data['collect_month_service'], 2, ',', '.') ?> " readonly>
                </div>
                <div class="input-group mb-2">
                   <span class="input-group-btn w-50">
                    <button class="btn btn-warning w-100" type="button">Pendiente Acumulado</button>
                   </span>
                    <input type="text" class="form-control text-right" id="" placeholder="" value="$<?= number_format($data['collect_pending'], 2, ',', '.') ?> " readonly>
                </div>
                <div class="input-group mb-2">
                   <span class="input-group-btn w-50">
                    <button class="btn btn-warning w-100" type="button">Cobrado</button>
                   </span>
                    <input type="text" class="form-control text-right" id="" placeholder="" value="$<?= number_format($data['collected_month'], 2, ',', '.') ?>" readonly>
                </div>
            </div>
        </div>
    </div>

</div>

<!--accesos directos-->
<div class="row justify-content-around">

    <div class="col-md-6  col-lg-4 col-xl-4 text-center" id="Home-direct_access_add_customers">
        <button type="button" class="btn btn-outline-secondary w-226 mt-1 mb-1">
            <i class="fas fa-user-plus  fa-3x"></i>
            <br>
            <span class="text">Cargar Cliente</span>
        </button>
    </div>

    <div class="col-md-6  col-lg-4 col-xl-4 text-center" id="Home-direct_access_add_pay">
          <button type="button" class="btn btn-outline-secondary w-226 mt-1 mb-1">
            <i class="far fa-money-bill-alt  fa-3x"></i>
            <br>
            <span class="text">Cargar Pagos</span>
        </button>
    </div>

    <div class="col-md-6  col-lg-4 col-lg-4 col-xl-4 text-center" id="Home-direct_access_add_comprobante">
          <button type="button" class="btn btn-outline-secondary w-226 mt-1 mb-1">
            <i class="far fa-edit fa-3x"></i>
            <br>
            <span class="text">Facturador</span>
        </button>
    </div>

    <div class="col-md-6  col-lg-4 col-xl-4 text-center" id="Home-direct_access_connections">
          <button type="button" class="btn btn-outline-secondary w-226 mt-1 mb-1">
            <i class="fas fa-link  fa-3x"></i>
            <br>
            <span class="text">Conexiones</span>
        </button>
    </div>

    <div class="col-md-6  col-lg-4 col-xl-4 text-center" id="Home-direct_access_add_ticket">
          <button type="button" class="btn btn-outline-secondary w-226 mt-1 mb-1">
            <i class="fas fa-ticket-alt fa-3x"></i>
            <br>
            <span class="text">Cargar Ticket</span>
        </button>
    </div>

    <div class="col-md-6  col-lg-4 col-xl-4 text-center" id="Home-direct_access_my_cash">
          <button type="button" class="btn btn-outline-secondary w-226 mt-1 mb-1">
            <i class="fas fa-inbox  fa-3x"></i>
            <br>
            <span class="text">Mi caja</span>
        </button>
    </div>

    <div class="col-md-6 col-lg-4 col-xl-4 text-center" id="Home-direct_access_presale">
          <button type="button" class="btn btn-outline-secondary w-226 mt-1 mb-1">
            <i class="fas fa-shopping-basket fa-3x"></i>
            <br>
            <span class="text">Venta</span>
        </button>
    </div>
</div>

<!--maps-->
<div class="row">
    <div class="col-xl-12" id="Home-map_connections">
        <div id="map"></div>
    </div>
</div>

<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2K3zoK46JybWPzQbhfQQqIIbdZ_3WcQs">
</script>

<script>

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    $(document).click(function() {
         $('[data-toggle="tooltip"]').tooltip('hide');
    });

    var map = null;
    var geocoder = null;
    var marker = null;
    var count = 0;
    var zoom = <?= json_encode($zoom) ?>;
    var markers = [];
    var markers_clusters = [];
    var infoWindows = [];
    var ctrl = "";
    var markerclusterer;

    var markerGroups = {
        "connections": [],
        // "customers": [],
        "controllers": []
    };

	function initMap() {

		geocoder = new google.maps.Geocoder;

		var myLatlng = {lat: sessionPHP.paraments.system.map.lat, lng: sessionPHP.paraments.system.map.lng};

		var mapOptions = {
			zoom: zoom,
			center: myLatlng,
			mapTypeId: 'hybrid'   // roadmap, satellite, hybrid, terrain 
		};

		map = new google.maps.Map(document.getElementById('map'), mapOptions);

		var tempConnectionsArray = <?php echo json_encode($connections_coords); ?>;

		$.each(tempConnectionsArray, function(i, val) {
		    if (val.lat != null && val.lng != null) {
		        var id = val.id;
		        var ip = val.ip;
		        var init = { lat: val.lat, lng: val.lng };
                var latLng = new google.maps.LatLng(init.lat, init.lng);
                var address = val.address;
                var customer = val.customer;
                var service = val.service;
                var controller = val.controller;
                addMarker(id, ip, latLng, address, customer, service, controller);
		    }
		});

// 		var tempCustomersArray = <?php echo json_encode($customers_coords); ?>;

// 		$.each(tempCustomersArray, function(i, val) {
// 		    if (val.lat != null && val.lng != null) {
// 		        var customer = {
// 		            code: val.code,
// 		            name: val.name,
// 		            doc_type: val.doc_type,
// 		            ident: val.ident
// 		        }

// 		        var init = { lat: val.lat, lng: val.lng };
//                 var latLng = new google.maps.LatLng(init.lat, init.lng);
//                 addCustomerMarker(latLng, customer);
// 		    }
// 		});

		var tempControllersArray = <?php echo json_encode($controllers_coords); ?>;

		$.each(tempControllersArray, function(i, val) {
		    if (val.lat != null && val.lng != null) {
		        var controller = {
		            template: val.template,
		            name: val.name,
		            ip: val.ip,
		            port: val.port,
		            radius: val.radius,
		            color: val.color
		        }

		        var init = { lat: val.lat, lng: val.lng };
                var latLng = new google.maps.LatLng(init.lat, init.lng);
                addControllerMarker(latLng, controller);
		    }
		});

		// a  div where we will place the buttons
        ctrl = $('<div/>').css({background:'#fff',
            border:'1px solid #000',
            padding:'4px',
            margin:'2px',
            textAlign:'center'
        });

        // ctrl.append(
        //     '<ul style="text-align: left; position: relative; display: block; padding-left: 5px; padding-right: 5px; padding-top: 8px;">' +
        //         '<div class="form-check">' +
        //             '<input onclick="toggleGroup(\'connections\')" type="checkbox" class="form-check-input" id="exampleCheck1" checked="checked">' +
        //             '<label style="padding-left: 26px; padding-top: 4px; color: #0000ff !important" class="form-check-label" for="exampleCheck1"> Conexiones</label>' +
        //         '</div>' +
        //         '<div class="form-check">' +
        //             '<input onclick="toggleGroup(\'customers\')" type="checkbox" class="form-check-input" id="exampleCheck2" checked="checked">' +
        //             '<label style="padding-left: 26px; padding-top: 4px; color: #008000 !important" class="form-check-label" for="exampleCheck2"> Clientes</label>' +
        //         '</div>' +
        //         '<div class="form-check">' +
        //             '<input onclick="toggleGroup(\'controllers\')" type="checkbox" class="form-check-input" id="exampleCheck2" checked="checked">' +
        //             '<label style="padding-left: 26px; padding-top: 4px; color: #000000 !important" class="form-check-label" for="exampleCheck2"> Controladores</label>' +
        //         '</div>' +
        //     '</ul>'
        // );
        
         ctrl.append(
            '<ul style="text-align: left; position: relative; display: block; padding-left: 5px; padding-right: 5px; padding-top: 8px;">' +
                '<div class="form-check">' +
                    '<input onclick="toggleGroup(\'connections\')" type="checkbox" class="form-check-input" id="exampleCheck1" checked="checked">' +
                    '<label style="padding-left: 26px; padding-top: 4px; color: #0000ff !important" class="form-check-label" for="exampleCheck1"> Conexiones</label>' +
                '</div>' +
                '<div class="form-check">' +
                    '<input onclick="toggleGroup(\'controllers\')" type="checkbox" class="form-check-input" id="exampleCheck2" checked="checked">' +
                    '<label style="padding-left: 26px; padding-top: 4px; color: #000000 !important" class="form-check-label" for="exampleCheck2"> Controladores</label>' +
                '</div>' +
            '</ul>'
        );

        //use the buttons-div as map-control 
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(ctrl[0]);

        markerclusterer = new MarkerClusterer(map, markers_clusters, {
            imagePath: '/ispbrain/js/googlemap/'
        });

        google.maps.event.addListener(markerclusterer, 'clusterclick', function(cluster) {
            closeAllInfoWindows();
        });
	}

	function addMarker(id, ip, location, address, customer, service, controller) {

        var ident = "";
        if (customer.ident) {
            ident = customer.ident;
        }

	    var contentString = 
	    '<div id="content">' +
            '<div id="siteNotice">' +
            '</div>' +
            '<h1 id="firstHeading" class="firstHeading"><a target="_blank" href="/ispbrain/connections/view/' + id +' ">Conexión - IP: ' + ip + '</a></h1>'+
            '<div id="bodyContent">' +
                '<p>' +
                    '<a target="_blank" href="/ispbrain/customers/view/' + customer.code + '"><b>Cliente: </b>' + customer.name + ' - <b>' + sessionPHP.afip_codes.doc_types[customer.doc_type] + ':</b> ' + ident + ' - <b>Código:</b> ' + customer.code + '</a><br>' +
                    '<b>Domicilio: </b>' + address + '<br>' +
                    '<b>Controlador: </b>' + controller.name + '<br>' +
                    '<b>Servicio: </b>' + service.name + '<br>' +
                '</p>' +
            '</div>' +
        '</div>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });

        infoWindows.push(infowindow);

		marker = new google.maps.Marker({
			position: location,
			map: map,
		    animation: google.maps.Animation.DROP,
			title: '',
			icon: pinSymbol('blue'),
			type: 'connections',
			getPosition: function() { 
		        return location;
			}
		});
		marker.setMap(map);
		markers.push(marker);
		markers_clusters.push(marker);

        if (!markerGroups['connections']) markerGroups['connections'] = [];
        markerGroups['connections'].push(marker);

        google.maps.event.addListener(marker,'click', (function(marker, contentString, infowindow) { 
            return function() {
                removeAnimationAllMarkers(marker);
                closeAllInfoWindows();
                infowindow.setContent(contentString);
                infowindow.open(map, marker);
            };
        })(marker, contentString, infowindow));

        google.maps.event.addListener(infowindow,'closeclick',function() {
            removeAnimationAllMarkers(null);
        });
	}

// 	function addCustomerMarker(location, customer) {

// 	    var contentString = 
// 	    '<div id="content">' +
//             '<div id="siteNotice">' +
//             '</div>' +
//             '<h1 id="firstHeading" class="firstHeading"><a target="_blank" href="/ispbrain/customers/view/' +  customer.code + ' ">Cliente</a></h1>' +
//             '<div id="bodyContent">' +
//                 '<p>' +
//                     '<a target="_blank" href="/ispbrain/customers/view/' + customer.code + '"><b>Cliente: </b>' + customer.name + ' - <b>' + sessionPHP.afip_codes.doc_types[customer.doc_type] + ':</b> ' + customer.ident + ' - <b>Código:</b> ' + customer.code + '</a><br>' +
//                     '<b>Domicilio: </b>' + customer.address + '<br>' +
//                 '</p>' +
//             '</div>' +
//         '</div>';

//         var infowindow = new google.maps.InfoWindow({
//             content: contentString
//         });

//         infoWindows.push(infowindow);

// 		marker = new google.maps.Marker({
// 			position: location,
// 			map: map,
// 		    animation: google.maps.Animation.DROP,
// 			title: '',
// 			icon: pinSymbol('green'),
// 			type: 'customers',
// 			getPosition: function() { 
// 		        return location;
// 			}
// 		});
// 		marker.setMap(map);
// 		markers.push(marker);
// 		markers_clusters.push(marker);

//         if (!markerGroups['customers']) markerGroups['customers'] = [];
//         markerGroups['customers'].push(marker);

//         google.maps.event.addListener(marker,'click', (function(marker, contentString, infowindow) { 
//             return function() {
//                 removeAnimationAllMarkers(marker);
//                 closeAllInfoWindows();
//                 infowindow.setContent(contentString);
//                 infowindow.open(map, marker);
//             };
//         })(marker, contentString, infowindow)); 

//         google.maps.event.addListener(infowindow,'closeclick',function() {
//             removeAnimationAllMarkers(null);
//         });

// 	}

	function addControllerMarker(location, controller) {

	    var contentString = 
	    '<div id="content">' +
            '<div id="siteNotice">' +
            '</div>' +
            '<h1 id="firstHeading" class="firstHeading"><a target="_blank" href="/ispbrain/' +  controller.template + '/' + controller.id +' ">' + controller.name + '</a></h1>' +
            '<div id="bodyContent">' +
                '<p>' +
                    '<b>Controlador: </b>' + controller.name + ' - <b>IP: </b>' + controller.ip + ' - <b>Puerto: </b>' + controller.port + ' - <b>Template: </b>' + controller.template + '<br>' +
                '</p>' +
            '</div>' +
        '</div>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });

        infoWindows.push(infowindow);

        marker = new google.maps.Circle({
            strokeColor: controller.color,
            strokeOpacity: 0.5,
            strokeWeight: 2,
            fillColor: controller.color,
            fillOpacity: 0.15,
            map: map,
            center: location,
            radius: controller.radius,
            type: 'controller',
            getPosition: function() { 
		        return location;
			}
        });
        marker.setMap(map);
		markers.push(marker);

        if (!markerGroups['controllers']) markerGroups['controllers'] = [];
        markerGroups['controllers'].push(marker);

        google.maps.event.addListener(marker,'click', (function(marker, contentString, infowindow) { 
            return function() {
                removeAnimationAllMarkers(marker);
                closeAllInfoWindows();
                infowindow.setContent(contentString);
                infowindow.setPosition(marker.getCenter());
                infowindow.open(map);
            };
        })(marker, contentString, infowindow));

        google.maps.event.addListener(infowindow,'closeclick',function() {
            removeAnimationAllMarkers(null);
        });
	}

	function toggleGroup(type) {
        for (var i = 0; i < markerGroups[type].length; i++) {
            var marker = markerGroups[type][i];
            if (!marker.getVisible()) {
                marker.setVisible(true);
                if (type != 'controllers') {

                    markerclusterer.addMarker(marker, true);
                    markerclusterer.repaint();
                }
            } else {
                marker.setVisible(false);
                if (type != 'controllers') {
                    markerclusterer.removeMarker(marker, true);
                    markerclusterer.repaint();
                }
            }
        }
    }

	function toggleBounce(marker) {
        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }

    function pinSymbol(color) {
        return {
            path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z',
            fillColor: color,
            fillOpacity: 1,
            strokeColor: '#000',
            strokeWeight: 2,
            scale: 1
        };
    }

    function closeAllInfoWindows() {
        for (var i = 0; i < infoWindows.length; i++) {
            infoWindows[i].close();
        }
    }

    function removeAnimationAllMarkers(marker) {
        for (var i = 0; i < markers.length; i++) {
            if (markers[i].type != 'controller') {
                 markers[i].setAnimation(null);
            }
        }
        if (marker != null && marker.type != 'controller') {
            toggleBounce(marker);
        }
    }

	$(document).ready(function() {

        $('#Home-direct_access_add_customers').click(function() {
            var action = '/ispbrain/customers/add/';
            window.open(action, '_self');
        });

        $('#Home-direct_access_add_pay').click(function() {
            var action = '/ispbrain/payments/add/';
            window.open(action, '_self');
        });

        $('#Home-direct_access_add_comprobante').click(function() {
            var action = '/ispbrain/Comprobante/add';
            window.open(action, '_self');
        });

        $('#Home-direct_access_connections').click(function() {
            var action = '/ispbrain/connections';
            window.open(action, '_self');
        });

        $('#Home-direct_access_add_ticket').click(function() {
            var action = '/ispbrain/Tickets/add';
            window.open(action, '_self');
        });

        $('#Home-direct_access_my_cash').click(function() {
            var action = '/ispbrain/CashEntities/pointSaleView';
            window.open(action, '_self');
        });

        $('#Home-direct_access_presale').click(function() {
            var action = '/ispbrain/Presales/firstAdd';
            window.open(action, '_self');
        });

        initMap();

	});

</script>
