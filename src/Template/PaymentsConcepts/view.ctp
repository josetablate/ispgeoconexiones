<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PaymentsConcept $paymentsConcept
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Payments Concept'), ['action' => 'edit', $paymentsConcept->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Payments Concept'), ['action' => 'delete', $paymentsConcept->id], ['confirm' => __('Are you sure you want to delete # {0}?', $paymentsConcept->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Payments Concepts'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Payments Concept'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="paymentsConcepts view large-9 medium-8 columns content">
    <h3><?= h($paymentsConcept->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($paymentsConcept->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Text') ?></h4>
        <?= $this->Text->autoParagraph(h($paymentsConcept->text)); ?>
    </div>
</div>
