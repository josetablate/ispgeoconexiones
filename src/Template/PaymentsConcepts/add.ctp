<?php
/**
 * @var \App\View\AppView $this
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Payments Concepts'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="paymentsConcepts form large-9 medium-8 columns content">
    <?= $this->Form->create($paymentsConcept) ?>
    <fieldset>
        <legend><?= __('Add Payments Concept') ?></legend>
        <?php
            echo $this->Form->control('text');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
