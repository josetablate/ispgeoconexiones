<?php

    $rowsCount = count($comprobantes_lala);

    $this->PhpExcel->getActiveSheet()->setTitle('Alícuota Venta');

    $this->PhpExcel->getActiveSheet()->fromArray($comprobantes_lala, NULL, 'A1');

    $this->PhpExcel->getActiveSheet()->getStyle('A1:Q1')->getFont()->setBold(true);

    $rowCount = 1;
    $customTitle = [
        'Fecha',        //0
        'Tipos',        //1
        'Pto Vta',      //2
        'Nro',          //3
        'Cód.',         //4
        'Nombre',       //5
        'SubTotal',     //6
        'IVA 0',        //7
        'IVA 2',        //8
        'IVA 5',        //9
        'IVA 10',       //10
        'IVA 21',       //11
        'IVA 27',       //12
        'Total',        //13
        'Cond. Fiscal', //14
        'Ciudad',       //15
        'Provincia'     //16
    ];

    $this->PhpExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $customTitle[0]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $customTitle[1]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $customTitle[2]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $customTitle[3]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $customTitle[4]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $customTitle[5]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $customTitle[6]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $customTitle[7]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $customTitle[8]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $customTitle[9]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $customTitle[10]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $customTitle[11]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $customTitle[12]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $customTitle[13]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $customTitle[14]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $customTitle[15]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $customTitle[16]);

    $colsAttr = [
        'A' => [
            'setAutoSize' => true,
        ],
        'B' => [
            'setAutoSize' => true,
        ],
        'C' => [
            'setAutoSize' => true,
        ],
        'D' => [
            'setAutoSize' => true,
        ],
        'E' => [
            'setAutoSize' => true,
        ],
        'F' => [
            'setAutoSize' => true,
        ],
        'G' => [
            'setAutoSize' => true,
        ],
        'H' => [
            'setAutoSize' => true,
        ],
        'I' => [
            'setAutoSize' => true,
        ],
        'J' => [
            'setAutoSize' => true,
        ],
        'K' => [
            'setAutoSize' => true,
        ],
        'L' => [
            'setAutoSize' => true,
        ],
        'M' => [
            'setAutoSize' => true,
        ],
        'N' => [
            'setAutoSize' => true,
        ],
        'O' => [
            'setAutoSize' => true,
        ],
        'P' => [
            'setAutoSize' => true,
        ],
        'Q' => [
            'setAutoSize' => true,
        ]
    ];

    foreach ($colsAttr as $col => $attr) {

        $activeSheet = $this->PhpExcel->getActiveSheet();

        //align center head columns
        $activeSheet->getStyle("$col"."1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $column = $activeSheet->getColumnDimension($col);

        $column->setAutoSize($attr['setAutoSize']);

        if (array_key_exists('setWidth', $attr)) {
            $column->setWidth($attr['setWidth']);
        }

        $alignment = $activeSheet->getStyle("$col" . "2:$col$rowsCount")->getAlignment();

        if (array_key_exists('horizontal', $attr)) {
            if ($attr['horizontal'] == 'right') {
                $alignment->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            } else if ($attr['horizontal'] == 'left') {
                $alignment->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            } else {
                $alignment->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }
        }
    }

?>
