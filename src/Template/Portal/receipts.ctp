<style type="text/css">

    .table {
        font-size: 15px;
    }

    .table th {
        background-color: #ffffff;
    }

    .title-card-general {
        color: #f05f40;
        font-size: 22px;
        text-align: left !important;
        display: block;
        margin-bottom: 7px;
        position: relative;
    }

    .table > tbody > tr > td {
        text-align: center;
    }

    .card-general {
        box-shadow: 2px 2px 4px #b7b4b4;
        margin-top: 60px;
    }

    .card-details {
        box-shadow: 2px 2px 4px #b7b4b4;
        margin-top: 60px;
        margin-left: 50px;
    }

    .td-print-recipt {
        text-align: center !important;
    }

</style>

<div class="row">

    <div class="card card-default card-general mx-auto">
        <div class="card-body">
            <span class="title-card-general"><?= __('Recibos') ?></span>

            <table class="table table-responsive">
              <thead>
                <tr>
                  <th><?= __('Fecha') ?></th>
                  <th><?= __('Concepto') ?></th>
                  <th><?= __('Importe') ?></th>
                  <th><?= __('Imprimir') ?></th>
                </tr>
              </thead>
              <tbody>
              <?php if ($customer->payments->count() > 0): ?>
                  <?php foreach ($customer->payments as $payment): ?>
                  <tr>
                      <td><?= date('d/m/Y', strtotime($payment->created)) ?> </td>
                      <td><?= $payment->concept ?></td>
                      <td>$<?= number_format($payment->import, 2, ', ', '.') ?></td>
                      <td class="td-print-recipt"><a data-id="<?= $payment->receipt_id ?>" class="btn-print-recipt" href="javascript:void(0)"><span class="fa fa-print fa-2x"></span></a></td>
                  </tr>
                  <?php endforeach; ?>
              <?php else: ?>
                  <tr>
                      <td><?= __('No ha realizado ningun pago por el momento') ?></td>
                      <td></td>
                      <td></td>
                      <td></td>
                  </tr>
              <?php endif; ?>
              </tbody>
            </table>

        </div>
    </div>

</div>

<script type="text/javascript">

    var customer;

    $(document).ready(function () {

        customer = <?= json_encode($customer) ?>;

        $('.btn-print-recipt').click(function() {
            var id = $(this).data('id');
            console.log(id);
            var url = "/ispbrain/portal/printreceipt/" + id;
            window.open(url, '_blank');
        });
        
    });

</script>
