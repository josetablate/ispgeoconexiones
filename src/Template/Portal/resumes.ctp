<style type="text/css">

    .table {
        font-size: 15px;
    }

    .table th {
        background-color: #ffffff;
    }

    .title-card-general {
        color: #f05f40;
        font-size: 22px;
        text-align: left !important;
        display: block;
        margin-bottom: 7px;
        position: relative;
    }

    .table > tbody > tr > td {
        text-align: right;
    }

    .card-general {
        box-shadow: 2px 2px 4px #b7b4b4;
        margin-top: 60px;
    }

    .card-details {
        box-shadow: 2px 2px 4px #b7b4b4;
        margin-top: 60px;
        margin-left: 50px;
    }

    .btn-print-resume {
        font-size: 16px;
        color: #f05f40;
        text-shadow: 1px 1px 1px rgba(224, 216, 216, 0
    }

</style>

<div class="row">

    <div class="card card-default card-general col-md-12 col-lg-12 col-xl-5">
        <div class="card-body">

            <table class="table table-responsive">
              <thead>
                <tr>
                  <span class="title-card-general"><?= __('Resumen de Cuenta') ?></span>
                </tr>
              </thead>
              <tbody>
                <tr>
                    <th><?= __('FECHA') ?></th>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td><b><?= $general->date ?></b></td>
                </tr>
                <tr>
                    <th><?= __('TOTAL A PAGAR') ?></th>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td><b>$<?= number_format($general->saldo ,2,',','.') ?></b></td>
                </tr>
                <tr>
                    <th><?= __('VENCIMIENTO') ?></th>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td><b><?= $general->duedate ?></b></td>
                </tr>
                <tr>
                    <th><?= __('CLIENTE') ?></th>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td><b><?= $general->customer_name ?></b></td>
                </tr>
                <tr>
                    <th><?= __('DOC') ?></th>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td><b><?= $general->customer_doc_type ?></b></td>
                </tr>
                 <tr>
                    <th><?= __('CÓDIGO DE CLIENTE') ?></th>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td><b><?= sprintf("%'.05d", $general->customer_code ) ?></b></td>
                </tr>
                <tr>
                    <th><?= __('PERIODO') ?></th>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td><b><?= $general->periode ?></b></td>
                </tr>
              </tbody>
            </table>

            <a class="btn-print-resume" href="javascript:void(0)"><span class="fa fa-print"></span> Imprimir Resumen de Cuenta</a>
        </div>
    </div>

    <div class="card card-default card-details col-md-12 col-lg-12 col-xl-6 mx-auto">
        <div class="card-body">
            <span class="title-card-general"><?= __('Detalle') ?></span>

            <table class="table table-responsive">
              <thead>
                <tr>
                  <th><?= __('Fecha') ?></th>
                  <th><?= __('Descripción') ?></th>
                  <th><?= __('Cantidad') ?></th>
                  <th><?= __('Precio Unit.') ?></th>
                  <th><?= __('Subtotal') ?></th>
                  <th><?= __('Saldo') ?></th>
                </tr>
              </thead>
              <tbody>
              <?php foreach ($resumes->list as $item): ?>
              <?php $resumes->saldo += $item['subtotal']; ?>
              <tr>
                 <td><b><?= $item['fecha']->format('d/m/Y'); ?></b></td>
                 <td><b><?= $item['description'] ?></b></td>
                 <td><b><?= $item['cant'] ?></b></td>
                 <td><b><?= $item['precio'] ?></b></td>
                 <td><b><?= $item['subtotal'] ?></b></td>
                 <td><b><?= $resumes->saldo ?></b></td>
              </tr>
              <?php endforeach; ?>
              </tbody>
            </table>

        </div>
    </div>

</div>

<script type="text/javascript">

    var customer;

    $(document).ready(function () {

        customer = <?= json_encode($customer) ?>;

        $('.btn-print-resume').click(function() {
            var url = "/ispbrain/portal/printresume/" + customer.code;
            window.open(url, '_blank');
        });
    });

</script>
