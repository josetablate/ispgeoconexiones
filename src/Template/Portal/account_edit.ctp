<style type="text/css">

    .title-card-general {
        color: #f05f40;
        font-size: 22px;
        text-align: left !important;
        display: block;
        margin-bottom: 7px;
        position: relative;
    }

    .table > tbody > tr > td {
        text-align: right;
    }

    .card-general {
        box-shadow: 2px 2px 4px #b7b4b4;
        margin-top: 60px;
    }

    .btn-editar {
        text-align: right;
        vertical-align: middle;
    }

</style>

<div class="row">
    <div class="card card-default card-general col-md-6 col-md-offset-3">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <td class="td-title title-card-general"><?= __('Datos de su Cuenta') ?></td>
                      <td class="td-title"></td>
                      <td class="td-title"></td>
                      <td class="td-title btn-editar"><a href="/ispbrain/portal/account-edit">Editar</a></td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                        <th><?= __('Nombre') ?></th>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td><b><?= h($customer->name) ?></b></td>
                    </tr>
                    <tr>
                        <th><?= __('Documento') ?></th>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td><b><?= $doc_types[$customer->doc_type] . ' ' . ($customer->ident ? $customer->ident : '') ?></b></td>
                    </tr>
                    <tr>
                        <th><?= __('Código de Cliente') ?></th>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td><b><?= sprintf("%'.05d", $customer->code ) ?></b></td>
                    </tr>
                    <tr>
                        <th><?= __('Correo') ?></th>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td><b><?= $customer->email ?></b></td>
                    </tr>
                    <tr>
                        <th><?= __('Clave Portal') ?></th>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td><b><?= $customer->clave_portal ?></b></td>
                    </tr>
                  </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

        $('.btn-print-recipt').click(function() {
            var id = $(this).data('id');
            console.log(id);
            var url = "/ispbrain/portal/printreceipt/" + id;
            window.open(url, '_blank');
        });
    });

</script>
