<style type="text/css">

    .table th {
        background-color: #ffffff;
    }

    .td-title {
        border-top: 0px !important;
    }

    .table th {
        background-color: #ffffff;
    }

    .title-card-general {
        color: #f05f40;
        font-size: 22px;
        text-align: left !important;
        display: block;
        margin-bottom: 7px;
        position: relative;
    }

    .table > tbody > tr > td {
        text-align: right;
    }

    .card-general {
        box-shadow: 2px 2px 4px #b7b4b4;
        margin-top: 60px;
    }

    .btn-editar {
        text-align: right;
        vertical-align: middle;
    }

</style>

<div class="row">
    <div class="card card-default card-general mx-auto">
        <div class="card-body">

            <table class="table table-responsive">
              <thead>
                <tr>
                  <td class="td-title title-card-general"><?= __('Datos de su Cuenta') ?></td>
                  <td class="td-title"></td>
                  <td class="td-title"></td>
                  <td class="td-title"></td>
                </tr>
              </thead>
              <tbody>
                <tr>
                    <th><?= __('Nombre') ?></th>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td><b><?= h($customer->name) ?></b></td>
                </tr>
                <tr>
                    <th><?= __('Documento') ?></th>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td><b><?= $doc_types[$customer->doc_type] . ' ' . ($customer->ident ? $customer->ident : '') ?></b></td>
                </tr>
                <tr>
                    <th><?= __('Código de Cliente') ?></th>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td><b><?= sprintf("%'.05d", $customer->code ) ?></b></td>
                </tr>
                <tr>
                    <th><?= __('Correo') ?></th>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td><b><?= $customer->email ?></b></td>
                </tr>
                <tr>
                    <th><?= __('Clave Portal') ?></th>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td><b><?= $customer->clave_portal ?></b></td>
                </tr>
              </tbody>
            </table>

        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

        $('.btn-print-recipt').click(function() {
            var id = $(this).data('id');
            console.log(id);
            var url = "/ispbrain/portal/printreceipt/" + id;
            window.open(url, '_blank');
        });

        $('#btn-print-payment').click(function(){
          var action = '/ispbrain/payments/printReceipt/' + table_payments.$('tr.selected').attr('id');
          window.open(action, '_blank');
        });

    });

</script>
