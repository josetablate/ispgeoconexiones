<style type="text/css">

    hr {
        margin-top: 8px;
        margin-bottom: 20px;
        border: 0;
        border-top: 2px solid #eee;
    }

    .table th {
        background-color: #ffffff;
    }

    .title-card-general {
        color: #f05f40;
        font-size: 22px;
        text-align: left !important;
        display: block;
        margin-bottom: 7px;
        position: relative;
    }

    .table > tbody > tr > td {
        text-align: right;
    }

    .card-general {
        box-shadow: 2px 2px 4px #b7b4b4;
        margin-top: 60px;
    }

    .card-informes {
        box-shadow: 2px 2px 4px #b7b4b4;
        margin-top: 60px;
        margin-left: 50px;
    }

    .menu-informes {
        list-style-type: none;
        padding: 0;
    }

    .item-informes {
        padding: 5px;
    }

</style>

<?php
    $status = [
        'CP' => 'Conexión Pendiente',
        'CC' => 'Conexión Creada',
        'I' => 'Instalación Registrada',
    ];
?>

<div class="row">
    <div class="card card-default card-general mx-auto">
        <div class="card-body">

            <table class="table table-responsive">
              <thead>
                <tr>
                  <span class="title-card-general"><?= __('Información General de su Cuenta') ?></span>
                </tr>
              </thead>
              <tbody>
                <tr>
                    <th><?= __('Nombre') ?></th>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td><b><?= h($customer->name) ?></b></td>
                </tr>
                <tr>
                    <th><?= __('Documento') ?></th>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td><b><?= $doc_types[$customer->doc_type] . ' ' . ($customer->ident ? $customer->ident : '') ?></b></td>
                </tr>
                <tr>
                    <th><?= __('Código de Cliente') ?></th>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td><b><?= sprintf("%'.05d", $customer->code ) ?></b></td>
                </tr>
                <tr>
                    <th><?= __('Saldo Total') ?></th>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td><b>$<?= number_format($customer->debt_total ,2,',','.') ?></b></td>
                </tr>
                <tr>
                    <th><?= __('Deuda del Mes') ?></th>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td><b>$<?= number_format($customer->debt_month ,2,',','.') ?></b></td>
                </tr>
                 <tr>
                    <th><?= __('Día de vencimiento') ?></th>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td><b><?= $customer->daydue ?></b></td>
                </tr>
                <tr>
                    <th><?= __('Estado') ?></th>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td><b><?= h($status[$customer->status]) ?></b></td>
                </tr>
              </tbody>
            </table>

        </div>
    </div>

    <?php if ($paraments->portal->show_receipt || $paraments->portal->show_resume): ?>
    <div class="card card-default card-informes mx-auto">
        <div class="card-body">
            <span class="title-card-general"><?= __('Informes') ?></span>
            <hr>
            <ul class="menu-informes">
                <?php if ($paraments->portal->show_resume): ?>
                <li class="item-informes"><a href="/ispbrain/portal/resumes"><?= __('Ver Resumen de Cuenta') ?></a></li>
                <?php endif; ?>
                <?php if ($paraments->portal->show_receipt): ?>
                <li class="item-informes"><a href="/ispbrain/portal/receipts"><?= __('Ver Recibos') ?></a></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
    <?php endif; ?>
</div>
