<style type="text/css">

    #question-forgot-password {
        cursor: pointer;
    }

</style>

<br>
<br>
<section id="services">
    <div class="">

            <div class="mx-auto" style="width: 240px;">
                <center>
                    <?php echo $this->Html->image('logo-portal.png', ['alt' => 'ISPGEO', 'width' => 240]);?>
                </center>
            </div>

        
            <div class="mx-auto" style="width: 200px;">
                <br>
                <?= $this->Form->create() ?>
                <?= $this->Form->input('num', ['label' => __('DNI'), 'autofocus' => true, 'required' => true]) ?>
                <?= $this->Form->input('psw', ['label' => __('Clave'), 'type' => 'password', 'required' => true]) ?>
                <?= $this->Form->button(__('Entrar'), ['class' => 'btn btn-primary']) ?>
                <?= $this->Form->end() ?>
            </div>
            <div class="mx-auto" style="width: 200px;">
                <i id="question-forgot-password" class="fa fa-question-circle"></i> <a id="btn-forgot-password" href="javascript:void(0)"> <?= __('¿Has olvidado tu Clave?') ?></a>
            </div>
        
    </div>
</seccion>

<div id="forget-password-popup" class="modal fade modal-actions" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Obtener una nueva Clave</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">

              <div class="register forget">
                <p>Ingrese su Correo, le enviaremos una nueva Clave</p>
				        <?= $this->Form->create(null, ['url' => ['controller' => 'Portal', 'action' => 'forgotPassword']]) ?>
                  <?= $this->Form->input('', ['label' => __(''), 'required' => true, 'type' => 'email', 'name' => 'email', 'id' => 'email', 'placeholder' => 'Correo...']) ?>
                  <?= $this->Form->hidden('controller', ['value' => $current_controller]); ?>
                  <?= $this->Form->hidden('action', ['value' => $current_action]); ?>
                  <div class="sign-up">
                    <?= $this->Form->input(__('Enviar'), ['id' => 'btn-forget', 'type' => 'submit']) ?>
                  </div>
                <?= $this->Form->end() ?>
			        </div>

            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="information-forgot-password-popup" class="modal fade modal-actions" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Información para obtener una nueva Clave</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">

        <div class="row">
            <div class="col-md-12">

              <p>En caso que haya olvidado su Clave para acceder al Portal</p> 
              <p><b>Pasos:</b></p>
              <p>1- Haga click en <b>¿Has olvidado tu Clave?</b>. Al hacer click se abrirá una ventana</p>
              <p>2- En la ventana introduzca su correo, el mismo que dio en la registración del servicio.</p>
              <p>3- Presionar click en <b>"Enviar"</b></p> 
              <p>Luego de un instante revise su correo, será enviado su nueva Clave para acceder al Portal, muchas gracias.</p>

            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

        $('#question-forgot-password').click(function() {
           $('#information-forgot-password-popup').modal('show');
        });

        $('#btn-forgot-password').click(function() {
           $('#forget-password-popup').modal('show');
        });
    });

</script>
