<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5">

        <?php 

            echo $this->Html->link(
                '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Agregar nueva cuenta',
                'class' => 'btn btn-default btn-add mr-2',
                'escape' => false
                ]);

             echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export mr-2',
                'escape' => false
                ]);
        ?>

    </div>
</div>

<div class="row">
    <div class="col-xl-12">

        <div id="parents-container">
            <label class="m-2">Cuentas Padres</label>
            <?php echo $this->Form->select('parent', [] , ['id' => 'parent']); ?>
        </div>

        <table class="table table-bordered table-hover" id="table-accounts">
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Saldo</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>

    </div>
</div>

<?php 
    $buttons = [];

    $buttons[] = [
        'id' =>  'btn-edit-account',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
        ];

    echo $this->element('actions', ['modal'=> 'modal-accounts', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Editar Cuenta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            </div>
            <div class="modal-body">
                
                

               <form method="post" accept-charset="utf-8" role="form" action="<?=$this->Url->build(["controller" => "Accounts", "action" => "edit"])?>">
                    <div style="display:none;">
                        <input type="hidden" name="_method" value="POST">
                    </div>

                    <div class="row">
                        <div class="col-xl-4">
                            <?php echo $this->Form->input('code', ['label' => 'Código' , 'type' => 'number', 'max' => 999999999 ,  'readonly' => true]); ?>
                        </div>
                        <div class="col-xl-8">
                            <?php echo $this->Form->input('name', ['label' => 'Nombre' , 'required' => true]); ?>
                        </div>
                        <div class="col-xl-8">
                            <?php echo $this->Form->input('description', ['label' => 'Descripción' , 'required' => false]); ?>
                        </div>
    
                        <div class="col-xl-4">
                            <?php echo $this->Form->input('title', ['type' => 'checkbox', 'label' => 'Cuenta Padre' , 'required' => false]); ?>
                        </div>
                        
                        <?php echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]); ?>

                    </div>

                    <div class="row">
                        <div class="col-xl-12">
                             <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" id="btn-submit" class="btn btn-success float-right">Guardar</button> 
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="label-modal-add">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Agregar Cuenta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

            </div>
            <div class="modal-body">

               <form method="post" accept-charset="utf-8" role="form" action="<?=$this->Url->build(["controller" => "Accounts", "action" => "add"])?>">
                    <div style="display:none;">
                        <input type="hidden" name="_method" value="POST">
                    </div>

                    <div class="row">
                        <div class="col-xl-4">
                            <?php echo $this->Form->input('code', ['label' => 'Código' , 'type' => 'number', 'max' => 999999999 , 'required' => true]); ?>
                        </div>
                        <div class="col-xl-8">
                            <?php echo $this->Form->input('name', ['label' => 'Nombre' , 'required' => true]); ?>
                        </div>
                        <div class="col-xl-8">
                            <?php echo $this->Form->input('description', ['label' => 'Descripción' , 'required' => false]); ?>
                        </div>

                        <div class="col-xl-4">
                            <?php echo $this->Form->input('title', ['type' => 'checkbox', 'label' => 'Cuenta Padre' , 'required' => false]); ?>
                        </div>
                        <?php echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]); ?>

                    </div>

                    <div class="row">
                        <div class="col-xl-12">
                             <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="button" id="btn-submit" class="btn btn-success float-right">Agregar</button> 
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var table_accounts = null;
    var account_selected = null;

    function clear_card_accounts_seeker() {

        $('select#parent').val('');
        $('select#parent').change();
        account_selected = null;
        table_accounts.$('tr.selected').removeClass('selected');
    }

    function getParents() {

         $.ajax({
            url: "<?=$this->Url->build(['controller' => 'Accounts', 'action' => 'getAllParents'])?>",
            type: 'POST',
            dataType: "json",
            success: function(data) {

                if (data != false) {

                    $('select#parent').append('<option value="">Todas las Cuentas (S/Clientes)</option>');
                    $.each(data.parents, function(i, val){
                       $('select#parent').append('<option value="'+val.code+'">'+val.name+'</option>');
                    });

                }
            },
            error: function (jqXHR) {

                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() { 
                	  window.location.href ="/ispbrain";
                	}, 3000);

                } else {
                	generateNoty('error', 'Error al obtener las cuentas padres.');
                }

      
      
            }
        });
    }

    $(document).ready(function() {

        getParents();

        $('#table-accountss').removeClass('display');

        $('#btns-tools').hide();

	    table_accounts = $('#table-accounts').DataTable({
		    "order": [[ 0, 'asc' ]],
            "deferRender": true,
            "info": false,
		    "ajax": {
                "url": "/ispbrain/accounts/get_by_parent.json",
                "dataSrc": "accounts",
                "data": function ( d ) {
                  return $.extend( {}, d, {
                    "parent": $('select#parent').val(),
                  } );
                },
                "error": function(c) {
                    switch (c.status) {
                        case 400:
                            flag = false;
                            generateNoty('warning', 'Ha ocurrido un error.');
                            break;
                        case 401:
                            flag = false;
                            generateNoty('warning', 'Error, no posee una autorización.');
                            break;
                        case 403:
                            flag = false;
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                        	setTimeout(function() { 
                                window.location.href = "/ispbrain";
                        	}, 3000);
                            break;
                    }
                }
            },
		    "autoWidth": true,
		    "scrollY": '440px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": false,
		    "columns": [
                { 
                    "data": "code",
                },
                { 
                    "data": "name"
                },
                { 
                    "data": "description",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                { 
                    "data": "saldo",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' ),
                },
            ],
		    "columnDefs": [
                { 
                    "class": "left", targets: [1, 2]
                },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.code;
                if (data.title == '1') {
                    $(row).addClass('font-weight-bold title');
                }
            },
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            "dom":
    	    	"<'row'<'col-xl-6 parents input-group'><'col-xl-6'<'row'<'col-xl-9'f><'col-xl-3 tools'>>>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
		});

		$('select#parent').change(function() {
            table_accounts.ajax.reload();
        });

		$('#table-accounts_wrapper .parents').append($('#parents-container').contents());

		$('#table-accounts_wrapper .tools').append($('#btns-tools').contents());

		$('#btns-tools').show();

        $('#table-accounts tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                table_accounts.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');

                account_selected = table_accounts.row( this ).data();

                $('.modal-accounts').modal('show');
            }
        });

        $('.btn-add').click(function() {

            $('.modal-accounts').modal('hide');
            $('#modal-add').modal('show');

        });

        $('#btn-edit-account').click(function(event) {

            $('#modal-edit #code').val(account_selected.code);
            $('#modal-edit #name').val(account_selected.name.replace(/&nbsp;/g, '')); 
            $('#modal-edit #description').val(account_selected.description);

            if (account_selected.title == 1) {
                $('#modal-edit #title').prop("checked", true);
            } else {
                $('#modal-edit #title').prop("checked", false);
            }

            $('#modal-edit').modal('show');

        });

        $('#modal-add #btn-submit').click(function(event) {

            var code = $('#modal-add #code').val();

            if (code.length != 9 ) {
                generateNoty('warning','El largo de código de cuenta es incorrecto');
                return false;
            }

            if ($('#modal-add #name').val() == 0) {
                generateNoty('warning','Debe especificar un nombre de cuenta');
                return false;
            }

            var request = $.ajax({
                url: "/ispbrain/accounts/validateFormAjax",
                method: "POST",
                data: JSON.stringify({ code : code }),
                dataType: "json"
            });

            request.done(function( response ) {

                if (!response.data.error) {
                    $('#modal-add form').submit();
                } else {
                     generateNoty('warning', response.data.msgError);
                }
            });

            request.fail(function(jqXHR) {

                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() { 
                	  window.location.href ="/ispbrain";
                	}, 3000);

                } else {
                	generateNoty('error','Error al intentar agregar la cuenta');
                }

     
      
            });

        });

        $(".btn-export").click(function() {

            bootbox.confirm('Se descargará un archivo Excel', function(result) {
                if (result) {
                    $('#table-accounts').tableExport({tableName: 'Plan de cuentas', type:'excel', escape:'false',  columnNumber: [3]});
                }
            });

        });

    });

</script>
