<style type="text/css">

    tr.locked > td.actions {
        background-color: white;
    }

</style>
 
 
  <input type="hidden" name="" id="message_template_id" value="<?=$messageTemplate->id?>"/>
    
    <div class="row">
        <div class="col-md-8">
            <legend class="sub-title">Template Seleccionado: <span class="badge badge-info"><?=$messageTemplate->name?></span></legend>
        </div>
        <div class="col-md-4">
            <div class="text-right">
               <a href="javascript:void(0)" class="btn btn-default btn-selected-all" >
                  <span class="far fa-check-square"  aria-hidden="true" ></span>
                </a>
                <a class="btn btn-default btn-send" href="javascript:void(0)" role="button">
                    <i class="fas fa-bullhorn" aria-hidden="true"></i>
                </a>
            </div>
        </div>
    </div>


<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5" >

        <?php 
                
            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export ml-1',
                'escape' => false
                ]);
            
        ?>

    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        
        <div id="controllers-container">
          
            <?php echo $this->Form->select('controller_id', $controllersArray , ['id' => 'controller-id', 'class' => 'mr-2']); ?>
        </div>
        
        <div id="filter-container">
           
            <?php echo $this->Form->select('filter', ['-1' => 'Todos', 0 => 'Bloquedos', 1 => 'Habilitados' ] , ['id' => 'filter']); ?>
        </div>

        <table id="table-connections" class="table table-bordered">
            <thead>
                <tr>
                    <th>Creado</th>
                    <th>Código</th>
                    <th>Nombre</th>
                    <th>Documento</th>
                    <th>Controlador</th>
                    <th>Servicio</th>
                    <th>IP</th>
                    <th>Domicilio</th>
                    <th>Área</th>
                    <th>Teléfono</th>
                    <th>Dirección MAC</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<?php
    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-connections']);
    echo $this->element('modal_preloader');

?>

<script type="text/javascript">

    var table_connections = null;

    $(document).ready(function () {
        
        $('.btn-hide-column').click(function(){
           
           $('.modal-hide-columns-connections').modal('show');
            
        });

        $('#table-connections').removeClass( 'display' );
		
		$('#btns-tools').hide();

		table_connections = $('#table-connections').DataTable({
		    "order": [[ 0, 'desc' ]],
            "sWrapper":  "dataTables_wrapper dt-bootstrap4",
            "deferRender": true,
            "paging": false,
		    "ajax": {
                "url": "/ispbrain/connections/get_connections_by_controller.json",
                "dataSrc": "connections",
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "controller_id": $('select#controller-id').val(),
                        "filter": $('select#filter').val(),
                    });
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
        				        window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "columns": [
                { 
                    "data": "created",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2]+'/'+created[1]+'/'+created[0];
                        }
                    }
                },
                { 
                    "data": "customer.code",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                { "data": "customer.name" },
                { 
                    "data": "customer.ident",
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (data) {
                            ident = data;
                        }
                        return sessionPHP.afip_codes.doc_types[row.customer.doc_type] + ' ' + ident;
                    }
                },
                { "data": "controller.name" },
                { "data": "service.name" },
                { "data": "ip" },
                { "data": "address" },
                { 
                    "data": "area",
                    "render": function ( data, type, row ) {
                        return data ? data.name : '';
                    }
                },
                { "data": "customer.phone" },
                { "data": "mac" },
            ],
		    "columnDefs": [
		        { "type": 'date-custom', targets: 0 },
		        { "width": '8%', targets: [1] },
		        { "width": '8%', targets: [1] },
		        { "width": '14%', targets: [8] },
		        { "type": 'ip-address', targets: [8] },
                { 
                    "class": "left", targets: [2,7]
                },
                { 
                    "width": "15%", targets: [2]
                },
                { 
                    "width": "20%", targets: [7]
                },
                { 
                    "visible": false, targets: [0,10]
                },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
                if (!data.enabled) {
                    $(row).addClass('locked');
                }
            },
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            "dom":
    	    	"<'row'<'col-xl-6 controllers filter input-group'><'col-xl-6'<'row'<'col-xl-10'f><'col-xl-2 tools'>>>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
		});
			
		$('#table-connections').on( 'init.dt', function () {
          
            createModalHideColumn(table_connections, '.modal-hide-columns-connections');
        });
        
        $('#table-connections').on( 'draw.dt', function () {
           closeModalPreloader();
        });
        
        $('#table-connections_wrapper .controllers').append($('#controllers-container').contents());
        
        $('#table-connections_wrapper .filter').append($('#filter-container').contents());

		$('#table-connections_wrapper .tools').append($('#btns-tools').contents());
        
        $('#btns-tools').show();
        
        $('select#controller-id').change(function(){
            
            openModalPreloader('Actualizando tabla ... ');
            
            table_connections.ajax.reload();
        });
        
        $('select#filter').change(function(){
            
            openModalPreloader('Actualizando tabla ... ');
            
            table_connections.ajax.reload();
        });
        
        loadPreferences(table_connections, 'connections-lockeds');

    });
   

    $(".btn-export").click(function() {

        bootbox.confirm('Se descargará un archivo Excel', function(result) {
            if (result) {
                $('#table-connections').tableExport({tableName: 'Conexiones', type:'excel', escape:'false'});
            }
        }).find("div.modal-content").addClass("confirm-width-md");

    });

    $('#table-connections tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            // table_connections.$('tr.selected').removeClass('selected');
            
            if($(this).hasClass('selected')){
                $(this).removeClass('selected');
            }else{
                $(this).addClass('selected');
            }

        }
    });
    
    $('.btn-selected-all').click(function(){
            
           if($(this).find('span').hasClass('active')){
                $('#table-connections tbody tr').each(function(){
                    if($(this).hasClass('selected')){
                        $(this).removeClass('selected');
                    }
                });
                $('.btn-selected-all span').removeClass('active');
                $('.btn-selected-all span').attr('title', 'Seleccionar todos.')
           }else{
                $('#table-connections tbody tr').each(function(){
                    if(!$(this).hasClass('selected')){
                        $(this).addClass('selected');
                    }
                });
                $('.btn-selected-all span').addClass('active');
                $('.btn-selected-all span').attr('title', 'Limpiar selección.')
           }
        });
        
      
        $('.btn-send').click(function(){
          
        var controller  = 'MessageTempletes';
        var text = '¿Está Seguro que desea enviar el aviso a las conexiones seleccionadas?';

        bootbox.confirm(text, function(result) {
            
            if(result){
                
                var connections_selected = '';
                  var i = 0;
                $('#table-connections tbody tr').each(function(){
                  
                    if($(this).hasClass('selected')){
                        if(i == 0){
                            i++;
                             connections_selected += $(this).attr('id');
                        }else{
                             connections_selected += ',' + $(this).attr('id');
                        }
                    }
                });
                
                var message_template_id = $('#message_template_id').val();
                
                $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/MessageTemplates/selectConnections/'+message_template_id, 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'message_template_id', 'value': message_template_id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'connections_selected', 'value': connections_selected}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#replacer').submit();
            }
        });
          
      });



</script>
