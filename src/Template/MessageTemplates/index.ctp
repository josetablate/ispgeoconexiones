<style type="text/css">

    #table-message-templates {
        width: 100% !important;
    }

    #table-message-templates td {
        margin: 0px 0px 0px 0px !important;
        padding: 0px 5px 0px 5px !important;
        vertical-align: middle;
        max-width: 150px !important;
        word-wrap: break-word !important;
    }

    .modal-actions a.btn {
        width: 100%;
        margin-bottom: 5px;
    }

    tr.selected {
        background-color: #8eea99;
        color: #333 !important;
    }

    .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
        background-color: #d2d2d2;
    }

    .vertical-table {
        width: 100%;
    }

    .vertical-table td {
        max-width: 250px !important;
        word-wrap: break-word !important;
    }

</style>

<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5" >
        <a class="btn btn-default" title="<?= ('Nuevo Template') ?>" href="<?=$this->Url->build(["action" => "add"])?>" role="button">
            <span class="glyphicon icon-plus"  aria-hidden="true"></span>
        </a>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-hover" id="table-message-templates" >
            <thead>
                <tr>
                    <th><?= ('#') ?></th>
                    <th><?= ('Nombre') ?></th>
                    <th><?= ('Descripción') ?></th>
                    <th><?= ('Tipo') ?></th>
                    <th><?= ('Prioridad') ?></th>
                    <th><?= ('Creado') ?></th>
                    
                </tr>
            </thead>
            <tbody>
                <?php foreach ($messageTemplates as $messageTemplate): ?>
                <tr data-id="<?= $messageTemplate->id ?>" data-name="<?= $messageTemplate->name ?>">

                    <td><?= $messageTemplate->id ?></td>
                    <td><?= h($messageTemplate->name) ?></td>
                    <td><?= h($messageTemplate->description) ?></td>
                    <td><?= $messageTemplate->type ?></td>
                    <td><?= $messageTemplate->priority ?></td>
                    <td><?= date('d/m/Y', strtotime($messageTemplate->created)); ?></td>
                    
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>


<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-select-customers',
        'name' =>  'Seleccionar Clientes',
        'icon' =>  'fas fa-users',
        'type' =>  'btn-secondary'
    ];
    
    $buttons[] = [
        'id'   =>  'btn-select-connections',
        'name' =>  'Seleccionar Conexiones',
        'icon' =>  'fas fa-link',
        'type' =>  'btn-secondary'
    ];
    
    $buttons[] = [
        'id'   =>  'btn-edit',
        'name' =>  'Editar',
        'icon' =>  'glyphicon icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete2',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-templates', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>


<script type="text/javascript">

    var messageTemplates = <?= json_encode($messageTemplates) ?>

    var table_message_templates = null;
    var hidden_columns = [];

    $(document).ready(function () {

        $('#table-message-templates').removeClass('display');

		table_message_templates = $('#table-message-templates').DataTable({
		    "order": [[ 1, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "width": "100%",
		    "initComplete": function(settings, json) {
		        
		        setTimeout(function(){table_message_templates.draw(); }, 100);
		    },
		    "columnDefs": [
		        { 
		            "targets": hidden_columns, 
		            "visible": false
		        },
            ],

		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            /*"dom": '<"toolbar">frtip'*/
		});

		table_message_templates.columns.adjust().draw();
		$('#table-message-templates_filter').closest('div').before($('#btns-tools').contents());
    });

    $('#table-message-templates tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_message_templates.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            
            var template = table_message_templates.row( this ).data();
            
            if(template[3] == 'Aviso'){
                
                $('#btn-select-customers').show();
                $('#btn-select-connections').show();
            }else{
                $('#btn-select-customers').hide();
                $('#btn-select-connections').hide();
            }

            $('.modal-templates').modal('show');
        }
    });

    $('a#btn-select-customers').click(function(){
       var action = '/ispbrain/message-templates/select-customers/' + table_message_templates.$('tr.selected').data('id');
       window.open(action, '_self');
    });
    
    $('a#btn-select-connections').click(function(){
       var action = '/ispbrain/message-templates/select-connections/' + table_message_templates.$('tr.selected').data('id');
       window.open(action, '_self');
    });


    $('a#btn-edit').click(function(){
       var action = '/ispbrain/message-templates/edit/' + table_message_templates.$('tr.selected').data('id');
       window.open(action, '_self');
    });

    $(document).on("click", "#btn-delete2", function(e) {

        var controller  = 'MessageTemplates';
        var id  = table_message_templates.$('tr.selected').data('id');
        var name  = table_message_templates.$('tr.selected').data('name');
        var text = 'Esta Seguro que desea eliminar el Template: ' + name + '?';

        bootbox.confirm(text, function(result) {
            if(result){
                $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/'+controller+'/delete/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#replacer').submit();
            }
        });
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
