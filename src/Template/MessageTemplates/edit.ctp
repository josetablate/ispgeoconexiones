<style type="text/css">

    .title {
        margin-left: 0px;
        margin-right: 0px;
        margin-bottom: 10px;
    }

    .title label.label-control {
        margin: 3px 0 3px 0;
    }

    .title label.label-control {
        margin: 3px 0 3px 0;
    }

    textarea.form-control {
        height: 80px !important;
    }

    span.red {
        color: red;
    }

</style>

    <div class="row">

        <div class="col-md-10">
            <?= $this->Form->create($messageTemplate, ['id' => 'message_template_form']) ?>
            <fieldset>

                <div class="row">
                    <div class="col-md-2">
                        <label for="name" class="label-control"><?= __('Nombre') ?></label>
                        <?php 
                            echo $this->Form->text('name', ['required' => true, 'type' => 'textarea'] );
                            echo $this->Form->input('type', ['options' => ['Aviso' => 'Aviso', 'Bloqueo' => 'Bloqueo'],  'label' => __('Tipo')]);
                            echo $this->Form->input('priority', ['type' => 'number', 'min' => 0,  'label' => __('Prioridad'), 'required' => true]);
                            echo $this->Form->input('description', ['type' => 'textarea', 'rows' => 2, 'label' => __('Descripción')]);
                        ?>
                        <button type="submit" class="btn btn-success"><?= __('Guardar') ?></button>
                    </div>
                    <div class="col-md-2">
                        <label for="name" class="label-control"><?= __('Lista de Tags ') ?> <a title="Los Tags serán reemplazados por sus valores. El más importante es el tag: [boton_continuar] es obligatorio ponerlo en las plantillas de avisos, para que el cliente al visualizar el aviso, luego pueda presionar el botón de continuar y siga navegando." data-toggle="tooltip" class="btn-help-test float-right" href="javascript:void(0)"> <i class="fas fa-question-circle"></i></a></label>
                        <span>[cliente_nombre]</span>
                        <span>[cliente_code]</span>
                        <span>[cliente_usuario_portal]</span>
                        <span>[cliente_clave_portal]</span>
                        <span>[cliente_saldo_mes]</span>
                        <span>[boton_continuar]</span>
                    </div>
                    <div class="col-md-8">
                        <textarea name="html" id="editor1" rows="10" cols="80">
                            <?= $messageTemplate->html ?>
                        </textarea>
                    </div>
                </div>

            </fieldset>

            <?= $this->Form->end() ?>
        </div>

        <div class="col-md-2">

            <?= $this->element('MessageTemplates/container_images_resources') ?>

        </div>
    </div>

<script>

    var editor  = null;

    $(document).ready(function() {

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

	    editor = CKEDITOR.replace( 'editor1' );

        CKEDITOR.config.height = 350;
        CKEDITOR.config.width = 'auto';

    });

</script>
