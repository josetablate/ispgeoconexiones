

<style type="text/css">
    
    .status {
        border-radius: 4px;
        display: inline-block;
        font-weight: bold;
        color: white;
        line-height: 1em;
        font-size: 10.5px;
        padding: 3px 0;
        margin: 0px 0px 0px 0px;
        width: 100%;
    }
    
    .cp {
        border: 1px solid #f38a73;
        background-color: #f38a73;
    }
    
    .cc {
         border: 1px solid gray;
         background-color: gray;    
    }
    
    .i {
         border: 1px solid purple;
         background-color: purple;
    }
    
    .plus1M {
         border: 1px solid #f05f40;
         background-color: #f05f40;
             width: 80%;
    }
    
    .plus2M {
          border: 1px solid #f05f40;
         background-color: #f05f40;
             width: 80%;
    }

    .plus3M {
          border: 1px solid #f05f40;
         background-color: #f05f40;
          width: 80%;    
    }
    
    #table-customers {
        width: 100% !important; 
    }
    
        
    #table-customers td {
        margin: 0px 0px 0px 0px !important;
        padding: 0px 5px 0px 5px !important;
        vertical-align: middle;
    }
    
    tr.selected {
        background-color: #8eea99;
    }
    
    .modal-actions a.btn {
        width: 100%;
        margin-bottom: 5px;
    }
    
    .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
        background-color: #d2d2d2;
    }
    
     tr {
         font-stretch: condensed;
    } 
    
</style>

    <input type="hidden" name="" id="message_template_id" value="<?=$messageTemplate->id?>"/>

    <div id="btns-tools">
        <div class="text-right btns-tools margin-bottom-5">
            
            
            
            <a href="javascript:void(0)" class="btn btn-default btn-selected-all" >
              Selección <span class="far fa-check-square"  aria-hidden="true" ></span>
            </a>
            <a class="btn btn-info btn-send" href="javascript:void(0)" role="button">
                Enviar <i class="fas fa-bullhorn" aria-hidden="true"></i>
            </a>
            
            <?php 
            
                echo $this->Html->link(
                    '<span class="glyphicon icon-eye" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Ocultar o Mostrar Columnas',
                    'class' => 'btn btn-default btn-hide-column',
                    'escape' => false
                ]);
            ?>
           
            <a href="javascript:void(0)" class="btn btn-default btn-export" title="Exportar tabla" >
              <span class="glyphicon icon-file-excel"  aria-hidden="true" ></span>
            </a>
            
            
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-8">
            <legend class="sub-title">Template Seleccionado: <span class="badge badge-info"><?=$messageTemplate->name?></span> </legend>
        </div>
        <div class="col-md-4">
            <div class="text-right">
                
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-hover" id="table-customers">
                <thead>
                    <tr>
                            <th >Creado</th>            <!--0-->
                        <th >Código</th>            <!--1-->
                        <th >F.I.</th>              <!--2-->
                        <th >Saldo Mes</th>         <!--3-->
                        <th >Nombre</th>            <!--4-->
                            <th >Tipo doc</th>          <!--5-->
                        <th >Documento</th>         <!--6-->
                        <th >Domicilio</th>         <!--7-->
                        <th >Ciudad</th>            <!--8-->
                            <th >Área</th>              <!--9-->
                            <th >Teléfono</th>          <!--10-->
                            <th >Vendedor</th>          <!--11-->
                            <th >Resp. Iva</th>         <!--12-->
                        <th >Factura</th>           <!--13-->
                            <th >Deno.</th>             <!--14-->
                            <th >Correo</th>            <!--15-->
                            <th >Comentarios</th>       <!--16-->
                            <th >Día Venc.</th>         <!--17-->
                            <th >Clave Portal</th>      <!--18-->
                    </tr>
                </thead>
                <tbody>
                  
                </tbody>
            </table>
        </div>
    </div>

<?php

    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-customers-http']);

?>


<script type="text/javascript">

    var doc_types = <?php echo json_encode($doc_types); ?>;
    
    var table_customers = null;
    
     $(document).ready(function () {
         
         $('.btn-hide-column').click(function(){
           
           $('.modal-hide-columns-customers-http').modal('show');
            
        });
       
        $('#table-customers').removeClass('display');
		
		table_customers = $('#table-customers').DataTable({
		    "order": [[ 1, 'desc' ]],
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
		    "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/MessageTemplates/get_customers.json",
                "dataSrc": "customers",
                "error": function(c) {
        		switch (c.status) {
        			case 400:
        				flag = false;
        				generateNoty('warning', 'Ha ocurrido un error.');
        				break;
        			case 401:
        				flag = false;
        				generateNoty('warning', 'Error, no posee una autorización.');
        				break;
        			case 403:
        				flag = false;
        				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

        				setTimeout(function() { 
        				  window.location.href ="/ispbrain";
        				}, 3000);
        				break;
        		}
        	}
            },
            "columns": [
                { 
                    "data": "created",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                { 
                    "data": "code",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                { "data": "invoices_no_paid" },
                { 
                    "data": "debt_month",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' ),
                },
                { "data": "name" },
                { 
                    "data": "doc_type",
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.doc_types[row.doc_type];
                    }
                },
                { "data": "ident"   },
                { "data": "address" },
                { 
                    "data": "city",
                    "render": function ( data, type, row ) {
                        return data ? data.name : '';
                    }
                },
                { 
                    "data": "area",
                    "render": function ( data, type, row ) {
                        return data ? data.name : '';
                    }
                },
                { "data": "phone" },
                { "data": "seller" },
                
                { 
                    "data": "responsible",
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.responsibles[data];
                    }
                },
                { 
                    "data": "is_presupuesto",
                    "render": function ( data, type, row ) {
                        if(data){
                            return 'NO';
                        }
                        return 'SI';
                    }
                },
                { 
                    "data": "denomination",
                    "render": function ( data, type, row ) {
                        if(data){
                            return 'SI';
                        }
                        return 'NO';
                    }
                },
                { 
                    "data": "email",
                    "render": function ( data, type, row ) {
                        if(data){
                            return data;
                        }
                        return '';
                    }
                },
                { 
                    "data": "comments",
                    "render": function ( data, type, row ) {
                        if(data){
                            return data;
                        }
                        return '';
                    }
                },
                { 
                    "data": "daydue"
                },
                { 
                    "data": "clave_portal",
                    "render": function ( data, type, row ) {
                        if(data){
                            return data;
                        }
                        return '';
                    }
                },
            ],
            "columnDefs": [
                { "type": 'date-custom', targets: 0 },
		        { "type": 'numeric-comma', targets: [2,3] },
		      //  {
        //             "targets": [1],
        //             "width": '5%'
        //         },
        //                       {
        //         "targets": [3],
        //             "width": '5%'
        //         },
                // {
                //     "targets": [0,2,4,6,9,10],
                //     "width": '8%'
                // },
                // {
                //     "targets": [7],
                //     "width": '13%'
                // },
                // {
                //     "targets": [5,8],
                //     "width": '12%'
                // },
                // { 
                //     "class": "left", targets: [5,7]
                // },
                { 
                    "visible": false, targets: [0,5,9,10,11,12,14,15,16,17,18]
                },
             ],
             "createdRow" : function( row, data, index ) {
                row.id = data.code;
            },
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 200, 300, 400, 500, 1000, -1], [100, 200, 300, 400, 500, 1000, "Todas"]],
            "dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-2'f><'col-xl-4 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
		});
		
		
        // $('#table-customers_filter').closest('div').before($('#btns-tools').contents());
        
        $('#table-customers_wrapper .tools').append($('#btns-tools').contents());
        
        $('#table-customers').on( 'init.dt', function () {
          
            createModalHideColumn(table_customers, '.modal-hide-columns-customers-http');
        });

        $('#btns-tools').show();


 
        $('#table-customers tbody').on( 'click', 'tr', function (e) {
            
            if(!$(this).find('.dataTables_empty').length){
                
                if( $(this).hasClass('selected')){
                     $(this).removeClass('selected');
                }else{
                     $(this).addClass('selected');
                }
            
                // table_customers.$('tr.selected').removeClass('selected');
               
            }
        } );
        
         $('.btn-selected-all').click(function(){
            
           if($(this).find('span').hasClass('active')){
                $('#table-customers tbody tr').each(function(){
                    if($(this).hasClass('selected')){
                        $(this).removeClass('selected');
                    }
                });
                $('.btn-selected-all span').removeClass('active');
                $('.btn-selected-all span').attr('title', 'Seleccionar todos.')
           }else{
                $('#table-customers tbody tr').each(function(){
                    if(!$(this).hasClass('selected')){
                        $(this).addClass('selected');
                    }
                });
                $('.btn-selected-all span').addClass('active');
                $('.btn-selected-all span').attr('title', 'Limpiar selección.')
           }
        });
        
      
      $('.btn-send').click(function(){
          
        var controller  = 'MessageTempletes';
        var text = '¿Está Seguro que desea enviar el aviso a los clientes seleccionados?';

        bootbox.confirm(text, function(result) {
            
            if (result) {
                
                var customers_selected = '';
                var i = 0;
                $('#table-customers tbody tr').each(function() {
                  
                    if ($(this).hasClass('selected')) {
                        if (i == 0) {
                            i++;
                             customers_selected += $(this).attr('id');
                        } else {
                             customers_selected += ',' + $(this).attr('id');
                        }
                    }
                });

                var message_template_id = $('#message_template_id').val();

                $('body')
                    .append( $('<form/>').attr({'action': '/ispbrain/MessageTemplates/selectCustomers/' + message_template_id, 'method': 'post', 'id': 'replacer'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'message_template_id', 'value': message_template_id}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'customers_selected', 'value': customers_selected}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer').submit();
            }
        });
          
      });
       
        
         
        loadPreferences(table_customers, 'customers-http');
      
	
    });
    
    
    $(".btn-export").click(function(){
        
        bootbox.confirm('Se descargará un archivo Excel', function(result) {
            if(result){
                $('#table-customers').tableExport({tableName: 'Avisos: Seleccion de Clientes', type:'excel', escape:'false'});
            }
        }).find("div.modal-content").addClass("confirm-width-md");
        
    });
    
      // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

    
    
</script>
