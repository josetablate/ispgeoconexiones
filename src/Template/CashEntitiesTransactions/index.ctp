<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CashEntitiesTransaction[]|\Cake\Collection\CollectionInterface $cashEntitiesTransactions
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Cash Entities Transaction'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="cashEntitiesTransactions index large-9 medium-8 columns content">
    <h3><?= __('Cash Entities Transactions') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_origin_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_destination_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('value') ?></th>
                <th scope="col"><?= $this->Paginator->sort('type') ?></th>
                <th scope="col"><?= $this->Paginator->sort('concept') ?></th>
                <th scope="col"><?= $this->Paginator->sort('acepted') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($cashEntitiesTransactions as $cashEntitiesTransaction): ?>
            <tr>
                <td><?= $this->Number->format($cashEntitiesTransaction->id) ?></td>
                <td><?= $this->Number->format($cashEntitiesTransaction->user_origin_id) ?></td>
                <td><?= $cashEntitiesTransaction->has('destination') ? $this->Html->link($cashEntitiesTransaction->destination->username, ['controller' => 'Users', 'action' => 'view', $cashEntitiesTransaction->destination->id]) : '' ?></td>
                <td><?= $this->Number->format($cashEntitiesTransaction->value) ?></td>
                <td><?= h($cashEntitiesTransaction->type) ?></td>
                <td><?= h($cashEntitiesTransaction->concept) ?></td>
                <td><?= h($cashEntitiesTransaction->acepted) ?></td>
                <td><?= h($cashEntitiesTransaction->created) ?></td>
                <td><?= h($cashEntitiesTransaction->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $cashEntitiesTransaction->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $cashEntitiesTransaction->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $cashEntitiesTransaction->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cashEntitiesTransaction->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
