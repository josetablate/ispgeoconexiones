<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CashEntitiesTransaction $cashEntitiesTransaction
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Cash Entities Transaction'), ['action' => 'edit', $cashEntitiesTransaction->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Cash Entities Transaction'), ['action' => 'delete', $cashEntitiesTransaction->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cashEntitiesTransaction->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Cash Entities Transactions'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cash Entities Transaction'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="cashEntitiesTransactions view large-9 medium-8 columns content">
    <h3><?= h($cashEntitiesTransaction->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Destination') ?></th>
            <td><?= $cashEntitiesTransaction->has('destination') ? $this->Html->link($cashEntitiesTransaction->destination->username, ['controller' => 'Users', 'action' => 'view', $cashEntitiesTransaction->destination->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Type') ?></th>
            <td><?= h($cashEntitiesTransaction->type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Concept') ?></th>
            <td><?= h($cashEntitiesTransaction->concept) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($cashEntitiesTransaction->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Origin Id') ?></th>
            <td><?= $this->Number->format($cashEntitiesTransaction->user_origin_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Value') ?></th>
            <td><?= $this->Number->format($cashEntitiesTransaction->value) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Acepted') ?></th>
            <td><?= h($cashEntitiesTransaction->acepted) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($cashEntitiesTransaction->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($cashEntitiesTransaction->modified) ?></td>
        </tr>
    </table>
</div>
