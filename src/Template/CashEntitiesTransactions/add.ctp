<?php
/**
 * @var \App\View\AppView $this
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Cash Entities Transactions'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="cashEntitiesTransactions form large-9 medium-8 columns content">
    <?= $this->Form->create($cashEntitiesTransaction) ?>
    <fieldset>
        <legend><?= __('Add Cash Entities Transaction') ?></legend>
        <?php
            echo $this->Form->control('user_origin_id');
            echo $this->Form->control('user_destination_id', ['options' => $users]);
            echo $this->Form->control('value');
            echo $this->Form->control('type');
            echo $this->Form->control('concept');
            echo $this->Form->control('acepted', ['empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
