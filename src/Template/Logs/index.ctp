

<style type="text/css">
    
    #table-logs{
        width: 100% !important
    }
        
    #table-logs td{
        margin: 0px 0px 0px 0px !important;
        padding: 0px 0px 0px 0px !important;
        vertical-align: middle;
    }
    
    .modal-actions a.btn{
        width: 100%;
        margin-bottom: 5px;
    }
    
    tr.selected{
        background-color: #8eea99;
        color: #333 !important;
    }
    
    .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
        background-color: #d2d2d2;
    }
    
  
    
</style>


    <div id="btns-tools">
        <div class="text-right btns-tools margin-bottom-5">
            <button type="button" class="btn btn-default btn-export" title="Exportar en formato excel el contenido de la tabla" >
              <span class="glyphicon icon-file-excel"  aria-hidden="true" ></span>
            </button>
        </div>
    </div>
   

    
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-hover" id="table-logs">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Fecha</th>
                        <th>Acción</th>
                        <th>Menu</th>
                        <th>Usuario</th>
                        <th>Cliente</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($logs as $log): ?>
                    <tr>
                        <td><?= $log->id ?></td>
                        <td><?= date('d/m/Y H:i', strtotime($log->created))?></td>
                        <td><?= h($log['action']) ?></td>
                        <td><?= h($log['menu']) ?></td>
                        <td><?= $log['user'] ? $this->Html->link($log->user->username, ['controller' => 'Users', 'action' => 'view', $log['user']['id']]) : '' ?></td>
                        <td><?= $log['customer_code'] ? $this->Html->link($log['customer_code'], ['controller' => 'customers', 'action' => 'view', $log['customer_code']]) : '' ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>



<script type="text/javascript">
    
    $(document).ready(function () {
       
        $('#table-logs').removeClass('display');
		
		$('#table-logs').DataTable({
		    "order": [[ 0, 'desc' ]],
		    "autoWidth": true,
		    "scrollY": '350px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "columnDefs": [
                 {
                    "targets": [0],
                    "visible": true,
                    "searchable": false
                },
                {
                    "type": "datetime-custom",
                    "targets": [1]
                },
            ],
		    "language": {
                "decimal":        "",
                "emptyTable":     "Sin resultados.",
                "info":           "_START_ al _END_ de _TOTAL_",
                "infoEmpty":      "0 al 0 de 0",
                "infoFiltered":   "",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ filas",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar registro:",
                "zeroRecords":    "Sin resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                    "sortDescending": ": Activar para ordenar la columna descendente"
                }
            },
            "lengthMenu": [[50, 100, -1], [50, 100, "Todas"]],
            /*"dom": '<"toolbar">frtip'*/
		});
		
		$('#table-logs_filter').closest('div').before($('#btns-tools').contents());
	
    });
    
    $(".btn-export").click(function(){
        
        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if(result){
                $('#table-logs').tableExport({type:'excel', escape:'false'});
            }
        });
        
    });
    
   
    
    
</script>

