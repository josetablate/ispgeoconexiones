<?php $this->extend('/CashEntities/views/other_payment'); ?>

<?php $this->start('efectivo'); ?>

<style type="text/css">

    .nav-link {
        cursor: pointer;
    }

    .nav-tabs {
        border-bottom: 1px solid #f05f40;
    }

    .btn-next-due-dates-selected {
        color: #f05f40;
        background-color: #e6e6e6;
        border-color: #f05f40;
    }

</style>

<input type="hidden" name="" id="number-part" value="<?= $lastApertureNumber ?>">

<div class="col-xl-12">

    <div class="row">
        <div class="col-md-12">
            <legend class="sub-title-sm"><?= __('Parte N° {0}',  $lastApertureNumber ) ?></legend>
        </div>
    </div>

    <div id="btns-tools">
        <div class="text-right btns-tools margin-bottom-5">

            <?php

                 echo $this->Html->link(

                    '<span class="fas fa-sync-alt" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Actualizar la tabla',
                    'class' => 'btn btn-default btn-update-table-contado ml-1 ',
                    'escape' => false
                ]);

                echo $this->Html->link(
                    '<span class="glyphicon icon-filter"  aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Filtar por Columnas',
                    'class' => 'btn btn-default btn-filter-column-contado ml-1',
                    'data-toggle' => 'modal', 'data-target' => '#modal-filter',
                    'escape' => false
                ]);

                echo $this->Html->link(
                    '<span class="glyphicon icon-file-pdf" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Exportar en formato PDF',
                    'class' => 'btn btn-default btn-export-pdf ml-1',
                    'escape' => false
                ]);

                echo $this->Html->link(
                    '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Exportar tabla',
                    'class' => 'btn btn-default btn-export-efectivo ml-1',
                    'escape' => false
                ]);
            ?>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered" id="table-inoutcash">
                <thead>
                    <tr>
                        <th>Fecha</th>
                        <th>Tipo</th>
                        <th>Cód.</th>
                        <th>Cliente</th>
                        <th>Concepto</th>
                        <th>Ingreso</th>
                        <th>Egreso</th>
                        <th>Saldo</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php

    $types = [
        ''               => 'Seleccione Tipo',
        'TRANSFERENCIA'  => 'TRANSFERENCIA',
        'RENDICIÓN'      => 'RENDICIÓN',
        'COMPRA'         => 'COMPRA',
        'CORRECCIÓN'     => 'CORRECCIÓN',
        'PAGO A TERCERO' => 'PAGO A TERCERO',
        'CAJA CHICA'     => 'CAJA CHICA',
        'SUELDOS'        => 'SUELDOS',
        'OTRO'           => 'OTRO'
    ];

    $types_movements = [
        ''               => 'Seleccione Tipo',
        'TRANSFERENCIA'  => 'TRANSFERENCIA',
        'RENDICIÓN'      => 'RENDICIÓN',
        'COMPRA'         => 'COMPRA',
        'CORRECCIÓN'     => 'CORRECCIÓN',
        'PAGO A TERCERO' => 'PAGO A TERCERO',
        'APERTURA'       => 'APERTURA',
        'CAJA CHICA'     => 'CAJA CHICA',
        'SUELDOS'        => 'SUELDOS',
        'OTRO'           => 'OTRO'
    ];

    $concepts = [
        ''                => 'Editar el Concepto',
        'Instalación'     => 'Instalación',
        'Soporte técnico' => 'Soporte técnico',
        'Almuerzo'        => 'Almuerzo',
    ];
?>

<div class="modal fade" id="modal-filter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Filtrar por columnas (efectivo)</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <?= $this->Form->create() ?>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="created">Fecha</label>
                                <div class='input-group date' id='created-datetimepicker'>
                                    <span class="input-group-addon mr-0">Fecha</span>
                                    <input name="input-created" id="input-created" type='text' class="column_search form-control datetimepicker" data-column="0" />
                                    <span class="input-group-addon mr-0">
                                        <span class="glyphicon icon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <?= $this->Form->input('type_search', ['label' => __('Tipo'), 'class'=> 'column_search', 'data-column' => 1, 'options' => $types_movements]) ?>
                            <?= $this->Form->input('data', ['label' => __('Cód.'), 'class'=> 'column_search', 'data-column' => 2]) ?>
                            <?= $this->Form->input('customer_name', ['label' => __('Cliente'), 'class'=> 'column_search', 'data-column' => 3]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $this->Form->input('concept', ['label' => __('Concepto'), 'class'=> 'column_search', 'data-column' => 4]) ?>
                            <?= $this->Form->input('in_value', ['label' => __('Ingreso'), 'class'=> 'column_search', 'data-column' => 5]) ?>
                            <?= $this->Form->input('out_value', ['label' => __('Egreso'), 'class'=> 'column_search', 'data-column' => 6]) ?>
                            <?= $this->Form->input('saldo', ['label' => __('Saldo'), 'class'=> 'column_search', 'data-column' => 7]) ?>
                        </div>

                  </div>

                <?= $this->Form->end() ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-clear-filter" >Limpiar</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript"> 

    $(document).ready(function () {

        //formating tabla de cuenta corriente
        $('#table-inoutcash').removeClass('display');

		var table_inoutcash = $('#table-inoutcash').DataTable({
		    "order": [[ 0, 'desc' ]],
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/IntOutCashsEntities/get_all_by_cash_entity_contado.json",
                "dataSrc": "movements",
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "cash_entity_id": cash_entity.id,
                    });
                },
                "error": function(c) {
                    switch (c.status) {
                        case 400:
                            flag = false;
                            generateNoty('warning', 'Ha ocurrido un error.');
                            break;
                        case 401:
                            flag = false;
                            generateNoty('warning', 'Error, no posee una autorización.');
                            break;
                        case 403:
                            flag = false;
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                        	setTimeout(function() { 
                                window.location.href = "/ispbrain";
                        	}, 3000);
                            break;
                    }
                }
		    },
            "columns": [
                { 
                    "data": "created",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            var time = data.split('T')[1].split('-')[0].split(':');
                            return created[2] + '/' + created[1] + '/' + created[0] + ' ' + time[0] + ':' + time[1] + ' hs';
                        }
                    }
                },
                { 
                    "data": "type"
                },
                { 
                    "data": "data",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return pad(data, 5);
                        }
                        return '';
                    }
                },
                { 
                    "data": "customer_name"
                },
                { 
                    "data": "concept"
                },
                { 
                    "data": "in_value",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' ),
                },
                { 
                    "data": "out_value",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' ),
                },
                { 
                    "data": "saldo_contado",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' ),
                }
            ],
		    "columnDefs": [
                 { "type": "date-custom", "targets": [0] },
                 { "width": "10%", "targets": [0, 2, 5, 6, 7] },
                 { "type": "numeric-comma", "targets": [5, 6, 7] }
             ],
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            "dom":
    	    	"<'row'<'col-xl-5'l><'col-xl-3'f><'col-xl-4 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
		});

		$('#table-inoutcash_wrapper .tools').append($('#btns-tools').contents());

        $('#btns-tools').show();

        $('#modal-filter .column_search').on( 'keyup', function () {

            table_inoutcash
                .columns( $(this).data('column') )
                .search( this.value )
                .draw();

            checkStatusFilter();
        });

        $('#modal-filter select[name="type_search"]').change(function() {

            table_inoutcash
                .columns( 1 )
                .search( $(this).val() )
                .draw();

            checkStatusFilter();
        });

        $('#modal-filter #created-datetimepicker').datetimepicker({
            locale: 'es',
            format: 'DD/MM/YYYY'
        }).on('dp.change', function (ev) {

            table_inoutcash
                .columns( 0 )
                .search( $('#modal-filter #input-created').val() )
                .draw();

            checkStatusFilter();
        });

        $('#modal-filter .btn-clear-filter').click(function() {

            $('#modal-filter .column_search').val('');
            table_inoutcash.search( '' ).columns().search( '' ).draw();
            checkStatusFilter();
        });

        $(".btn-export-pdf").click(function() {
            var action = '/ispbrain/IntOutCashsEntities/printpart/' + $('#number-part').val();
            window.open(action, '_blank');
        });

        $('.btn-update-table-contado').click(function() {

             if (table_inoutcash) {
                table_inoutcash.ajax.reload();
                updateTotales();
            }
        });

        updateTotales();

        $('a[id="efectivo-tab"]').on('shown.bs.tab', function (e) {
            table_inoutcash.draw();
            table_inoutcash.ajax.reload();
        });

        $(".btn-export-efectivo").click(function() {

            bootbox.confirm('Se descargará un archivo Excel', function(result) {
                if (result) {

                   $('#table-inoutcash').tableExport({
                       tableName: 'Parte ' + last_aperture_number + '- Resp. ' + username,
                       fileName: 'Caja efectivo',
                       type:'excel',
                       escape:'false',
                       columnNumber: [5] 
                   });
                } 
            });
        });

    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>

<?php $this->end(); ?>
