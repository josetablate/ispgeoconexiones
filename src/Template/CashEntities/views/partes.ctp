<?php $this->extend('/CashEntities/views/tabs'); ?>

<?php $this->start('partes'); ?>

<style type="text/css">

    .nav-link {
        cursor: pointer;
    }

    .nav-tabs {
        border-bottom: 1px solid #f05f40;
    }

    .btn-next-due-dates-selected {
        color: #f05f40;
        background-color: #e6e6e6;
        border-color: #f05f40;
    }

</style>

<div class="col-xl-12">
   
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-hover" id="table-apertures">
                <thead>
                    <tr>
                        <th>Fecha</th>
                        <th>Número</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php 

    $buttons = [];

    // $buttons[] = [
    //     'id' =>  'btn-export-pdf',
    //     'name' =>  'Exportar PDF',
    //     'icon' =>  'icon-file-text2',
    //     'type' =>  'btn-secondary'
    // ];

    $buttons[] = [
        'id' =>  'btn-info',
        'name' =>  'Ver Detalles',
        'icon' =>  'icon-eye',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-parts', 'title' => 'Acciones', 'buttons' => $buttons ]);

?>

<script type="text/javascript"> 

    var table_apertures = null;

    $(document).ready(function () {

        //partes anteriores

        $('a[id="parts-tab"]').on('shown.bs.tab', function (e) {
            table_apertures.draw();
        });

        $('#table-apertures').removeClass('display');

		table_apertures = $('#table-apertures').DataTable({
		    "order": [[ 1, 'desc' ]],
		    "scrollY": '350px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "ajax": {
                "url": "/ispbrain/IntOutCashsEntities/get_all_parts.json",
                "dataSrc": "apertures",
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "cash_entity_id": cash_entity.id
                    });
                },
                "error": function(c) {
                    switch (c.status) {
                        case 400:
                            flag = false;
                            generateNoty('warning', 'Ha ocurrido un error.');
                            break;
                        case 401:
                            flag = false;
                            generateNoty('warning', 'Error, no posee una autorización.');
                            break;
                        case 403:
                            flag = false;
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                        	setTimeout(function() { 
                                window.location.href = "/ispbrain";
                        	}, 3000);
                            break;
                    }
                }
            },
            "columns": [
                { 
                    "data": "created",
                    "render": function ( data, type, row ) {
                        if(data){
                            var created = data.split('T')[0];
                            var time = data.split('T')[1];
                            time = time.split(':');
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0] + ' ' + time[0] + ':' + time[1];
                        }
                    }
                },
                { 
                    "data": "number_part"
                }
            ],
		    "columnDefs": [
             { "type": "datetime-custom", "targets": [0] },
             ],
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
		});

		$('#table-apertures tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                table_apertures.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');

                part_selected = table_apertures.row( this ).data();

                $('.modal-parts').modal('show');
            }
        });

        $('a#btn-info').click(function(){
            var action = '/ispbrain/IntOutCashsEntities/viewParte/' + part_selected.cash_entity_id + '/' + part_selected.number_part;
            $('.modal-actions').modal('hide');
            window.open(action, '_blank');
        }); 

        $('a[id="partes-tab"]').on('shown.bs.tab', function (e) {
            table_apertures.draw();
            table_apertures.ajax.reload();
        });

    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>

<?php $this->end(); ?>
