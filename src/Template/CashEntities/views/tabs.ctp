<style type="text/css">

    .vertical-table {
        width: 100%;
        color: #696868;
    }

    table > tbody > tr {
        border-bottom: 1px solid #e5e5e5;
    }

    .my-hidden {
        display: none;
    }

    .total-cash-row {
        font-size: 22px;
    }

</style>

<div class="row">
    <div class="col-xl-8">
        <legend class="sub-title">Caja: <span class="bold-color"><?= $cash_entity->name ?></span></legend>
    </div>

    <div class="col-xl-4">
        <div class="float-right">

            <?php

               echo $this->Html->link(
                    '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                    ['controller' => 'CashEntities', 'action' => 'add'],
                    [
                    'title' => 'Agregar una nueva caja',
                    'class' => 'btn btn-default ml-1 ',
                    'escape' => false
                ]);

                echo $this->Html->link(
                    '<span class="glyphicon icon-pencil2" aria-hidden="true"></span>',
                    ['controller' => 'CashEntities', 'action' => 'edit', $cash_entity->id],
                    [
                    'id' => "",
                    'title' => '',
                    'class' => 'btn btn-default ml-1',
                    'escape' => false
                ]);

                echo $this->Html->link(
                    '<span class="glyphicon icon-bin" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Eliminar Caja',
                    'id' =>  'btn-delete',
                    'class' => 'btn btn-default ml-1',
                    'data-id' => $cash_entity->id,
                    'data-name' => $cash_entity->name,
                    'escape' => false
                ]);
               
            ?>

         </div>
    </div>

    <div class="col-xl-3">
        <legend class="sub-title-sm"><?= __('Información') ?></legend>
        <table class="vertical-table">
            <tr>
                <th><?= __('Número') ?></th>
                <td class="float-right"><?= sprintf("%'.03d", $cash_entity->id) ?></td>
            </tr>
            <tr>
                <th><?= __('Nombre') ?></th>
                <td class="float-right"><?= h($cash_entity->name) ?></td>
            </tr>
            <tr>
                <th><?= __('Estado') ?></th>
                <td class="float-right">
                <?php
                    if ($cash_entity->open) {
                        echo "<span class='font-weight-bold text-success'>Caja Abierta</span>";
                    } else {
                         echo "<span class='font-weight-bold text-danger'>Caja Cerrada</span>";
                    }
                ?>
                </td>
            </tr>
            <tr>
                <th><?= __('Responsable') ?></th>
                <td class="float-right"><?= $cash_entity->user->username ?></td>
            </tr>
            <tr>
                <th class="text-secondary"><?= __('Total Efectivo') ?></th>
                <td class="float-right font-weight-bold text-secondary" id="cash_contado"></td>
            </tr>
            <tr>
                <th class="text-secondary"><?= __('Total Otros') ?></th>
                <td class="float-right font-weight-bold text-secondary" id="cash_other"></td>
            </tr>
            <tr class="total-cash-row">
                <th class="text-secondary"><?= __('Total') ?></th>
                <td class="float-right font-weight-bold text-secondary" id="cash"></td>
            </tr>
         
            <tr>
                <th><?= __('Creado') ?></th>
                <td class="float-right"><?= date('d/m/Y H:i', strtotime($cash_entity->created))?></td>
            </tr>
            <tr>
                <th><?= __('Modificado') ?></th>
                <td class="float-right"><?= date('d/m/Y H:i', strtotime($cash_entity->modified))?></td>
            </tr>
            <tr>
                <th><?= __('Comentario') ?></th>
                <td class="float-right"><?= $cash_entity->comments ?></td>
            </tr>
        </table>
    </div>

    <div class="col-xl-9">
        <ul class="nav nav-tabs" id="myTab" role="tablist">

            <li class="nav-item">
                <a class="nav-link active" id="efectivo-tab" data-target="#efectivo" role="tab" aria-controls="efectivo" aria-expanded="true">Efectivo</a>
            </li>

            <li class="nav-item">
                <a class="nav-link " id="other_payment-tab" data-target="#other_payment" role="tab" aria-controls="other_payment" aria-expanded="false">Otros medios de pagos</a>
            </li>

            <li class="nav-item">
                <a class="nav-link " id="partes-tab" data-target="#partes" role="tab" aria-controls="partes" aria-expanded="false">Partes anteriores</a>
            </li>

        </ul>

        <div class="tab-content" id="myTabContent">

            <div class="tab-pane fade show active" id="efectivo" role="tabpanel" aria-labelledby="efectivo-tab">
                <?= $this->fetch('efectivo') ?>
            </div>

            <div class="tab-pane fade" id="other_payment" role="tabpanel" aria-labelledby="other_payment-tab">
                <?= $this->fetch('other_payment') ?>
            </div>
            
            <div class="tab-pane fade" id="partes" role="tabpanel" aria-labelledby="partes-tab">
                <?= $this->fetch('partes') ?>
            </div>

        </div>
    </div>

</div>

<script type="text/javascript">

    var cash_entity = <?= json_encode($cash_entity) ?>;
    var current_aperture_number = '<?= $lastApertureNumber ?>';
    var username = '<?= $cash_entity->user->username ?>';
    var last_aperture_number = <?= json_encode($lastApertureNumber) ?>;

    function updateTotales() {
            var request = $.ajax({
                url: "/ispbrain/CashEntities/getCashEntityToatales.json",
                method: "POST",
                data: JSON.stringify({
                    cash_entity_id: cash_entity.id,
                }),
                dataType: "json"
            });

            request.done(function(response) {

                $('#cash_contado').html( number_format(response.cash_entity.contado, true));
                $('#cash_other').html( number_format(response.cash_entity.cash_other, true));
                $('#cash').html( number_format(response.cash_entity.contado + response.cash_entity.cash_other, true));
          
            });

            request.fail(function(jqXHR) {

                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() { 
                        window.location.href = "/ispbrain";
                	}, 3000);

                } else {
                	generateNoty('error', 'Error al intentar obtener los totales');
                }
            });
        }

    $(document).ready(function() {

         $('#btn-delete').click(function() {

            var text = '¿Está Seguro que desea eliminar la Caja?';
            var controller = 'CashEntities';
            var id  = $(this).data('id');

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/delete/', 'method': 'post', 'id': 'replacer'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer').submit();
                }
            });

        });

        $('#myTab a').click(function (e) {

            e.preventDefault();

            var target = $(this).data('target');
            $('#myTab a.active').removeClass('active');

            $('#myTabContent .active').fadeOut(100, function() {
                $('#myTabContent .active').removeClass('active');

                $(target).fadeIn(100, function() {
                    $(target).addClass('active show');
                    $(target + '-tab').addClass('active');
                    $(target + '-tab').trigger('shown.bs.tab');
                });
            });
        });

    });

</script>
