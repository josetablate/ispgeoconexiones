<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5" >

        <?php 

            echo $this->Html->link(
                '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                ['controller' => 'CashEntities', 'action' => 'add'],
                [
                'title' => 'Agregar una nueva caja',
                'class' => 'btn btn-default btn-add-connection',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export',
                'escape' => false
            ]);
        ?>

    </div>
</div>

<div class="row">
    <div class="col-xl-12">

        <table class="table table-bordered table-hover" id="table-cash-entities">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Monto Actual</th>
                    <th>Responsable</th>
                    <th>Cuenta</th>
                    <th>Creado</th>
                    <th>Modificado</th>
                    <th>Estado</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>

    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-info',
        'name' =>  'Infomación',
        'icon' =>  'icon-eye',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-edit',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-cash-entities', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<script type="text/javascript">

    var table_cash_entities = null;

    $(document).ready(function () {

        $('#table-cash-entities').removeClass( 'display' );

		$('#btns-tools').hide();
		
		var hide_column = [];

        if (!sessionPHP.paraments.accountant.account_enabled) {
            hide_column = [3];
        }

		table_cash_entities = $('#table-cash-entities').DataTable({
		    "order": [[ 0, 'desc' ]],
            "sWrapper":  "dataTables_wrapper dt-bootstrap4",
            "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/cash-entities/get_all.json",
                "dataSrc": "cashentities",
                "error": function(c) {
                    switch (c.status) {
                        case 400:
                            flag = false;
                            generateNoty('warning', 'Ha ocurrido un error.');
                            break;
                        case 401:
                            flag = false;
                            generateNoty('warning', 'Error, no posee una autorización.');
                            break;
                        case 403:
                            flag = false;
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                        	setTimeout(function() { 
                    	        window.location.href = "/ispbrain";
                        	}, 3000);
                            break;
                    }
                }
            },
		    "autoWidth": true,
		    "scrollY": '550px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "columns": [
		        
		        { 
		            "data": "name"
		        },
		        { 
		            "data": "contado",
		            "render": function ( data, type, row ) {
		                
		                var total = data + row.cash_other;
		                return number_format(data + row.cash_other, true);
                    }
		        },
		        { 
		            "data": "user.username"
		        },
		        {
		            "data": "account_code"
		        },
                { 
                    "data": "created",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] +  '/' + created[0];
                        }
                    }
                },
                { 
                    "data": "modified",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var modified = data.split('T')[0];
                            modified = modified.split('-');
                            return modified[2] + '/' + modified[1] + '/' + modified[0];
                        }
                    }
                },
                { 
                    "data": "open",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return 'Abierta';
                        }
                        return 'Cerrada';
                    }
                }
            ],
		    "columnDefs": [
		        { "type": 'date-custom', targets: [4, 5] },
		        { "type": 'numeric-comma', targets: [4] },
                { 
                    "class": "left", targets: [0]
                },
                { 
                    "visible": false, targets: hide_column
                 },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            "dom":
    	    	"<'row'<'col-md-6'l><'col-md-4'f><'col-md-2 tools'>>" +
        		"<'row'<'col-md-12'tr>>" +
        		"<'row'<'col-md-5'i><'col-md-7'p>>",
		});

		$('#table-cash-entities_wrapper .tools').append($('#btns-tools').contents());

        $('#btns-tools').show();

        $('#table-cash-entities tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                table_cash_entities.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
                $('.modal-cash-entities').modal('show');
            }
        });

        $('#btn-info').click(function() {
            var action = '/ispbrain/cash-entities/view/' + table_cash_entities.$('tr.selected').attr('id');
            window.open(action, '_self');
        });

        $('#btn-edit').click(function() {
            var action = '/ispbrain/cash-entities/edit/' + table_cash_entities.$('tr.selected').attr('id');
            window.open(action, '_self');
        });

        $('#btn-delete').click(function() {

            var text = '¿Está Seguro que desea eliminar la caja?';
            var controller  = 'cash-entities';
            var id = table_cash_entities.$('tr.selected').attr('id');

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/delete/', 'method': 'post', 'id': 'replacer'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer').submit();
                }
            });
        });
    });

</script>
