<style type="text/css">

    textarea.form-control {
        height: 55px !important;
    }

</style>

<div class="row">

    <div class="col-md-3">
        <div class="text-right">

            <?php

                echo $this->Html->link(
                    '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                    ['controller' => 'CashEntities', 'action' => 'add'],
                    [
                    'title' => 'Agregar una nueva caja',
                    'class' => 'btn btn-default ml-1 ',
                    'escape' => false
                ]);

                echo $this->Html->link(
                    '<span class="glyphicon icon-bin" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Eliminar Caja',
                    'id' =>  'btn-delete',
                    'class' => 'btn btn-default ml-1',
                    'data-id' => $cashEntity->id,
                    'data-name' => $cashEntity->name,
                    'escape' => false
                ]);

            ?>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3">

        <?= $this->Form->create($cashEntity,  ['class' => 'form-load']) ?>
            <fieldset>
                <?php
                    echo $this->Form->input('name', ['label' => 'Nombre'] );
                    echo $this->Form->input('comments', ['label' => 'Comentario']);
                    echo $this->Form->input('user_id', ['label' => 'Responsable *', 'options' => $users]);
                    echo $this->Form->input('enabled', ['label' => 'Habilitar/Deshabilitar', 'class' => 'input-checkbox' ]);
                ?>
            </fieldset>
            <br>

            <?= $this->Html->link(__('Cancelar'), ["controller" => "CashEntities", "action" => "index"], ['class' => 'btn btn-default ']) ?>
            <?= $this->Form->button(__('Guardar'), ['id' => 'btn-confirm', 'class' => 'btn-success' ]) ?>
        <?= $this->Form->end() ?>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function functionName() {
         $('#btn-delete').click(function() {

            var text = '¿Está Seguro que desea eliminar la caja?';
            var controller  = 'CashEntities';
            var id  = $(this).data('id');

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/delete/', 'method': 'post', 'id': 'replacer'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer').submit();
                }
            });

        });
    });

</script>
