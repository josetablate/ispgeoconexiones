<style type="text/css">
    
    textarea.form-control{
        height: 55px !important;
    }
    
</style>

<div class="row">
    <div class="col-xl-3">
                
        <?= $this->Form->create($cashEntity, ['class' => 'form-load']) ?>
            <fieldset>
                <?php
                    echo $this->Form->input('name', ['label' => 'Nombre'] );
                    echo $this->Form->input('comments', ['label' => 'Comentario']);
                    echo $this->Form->input('user_id', ['label' => 'Responsable *', 'options' => $users]);
                    echo $this->Form->input('enabled', ['label' => 'Habilitar/Deshabilitar', 'class' => 'input-checkbox' , 'checked' => true]);

                ?>
            </fieldset>
            <br>
            <?= $this->Html->link(__('Cancelar'),["controller" => "CashEntities", "action" => "index"], ['class' => 'btn btn-default ']) ?>
            <?= $this->Form->button(__('Agregar'), ['id' => 'btn-confirm', 'class' => 'btn-success' ]) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
