<style type="text/css">

    .vertical-table {
        width: 100%;
        color: #696868;
    }

    table > tbody > tr {
        border-bottom: 1px solid #e5e5e5;
    }

    .my-hidden {
        display: none;
    }

    .total-cash-row {
        font-size: 22px;
    }

</style>

<div class="row">
    <div class="col-xl-8">
        <legend class="sub-title">Caja: <span class="bold-color"><?= $cash_entity->name ?></span></legend>
    </div>

    <div class="col-xl-4">
        <div class="float-right">

            <?php

                if ($cash_entity->open) {

                    echo $this->Html->link(
                        '<span class="glyphicon icon-switch" aria-hidden="true"></span> Cerrar Caja',
                        'javascript:void(0)',
                        [
                        'id' => 'btn-closeCash',
                        'title' => 'Cerrar Caja',
                        'class' => 'btn btn-default ml-1',
                        'escape' => false
                    ]);

                } else {

                    echo $this->Html->link( 
                        '<span class="glyphicon icon-switch" aria-hidden="true"></span> Abrir Caja',
                        'javascript:void(0)',
                        [
                        'id' => 'btn-openCash',
                        'title' => 'Abrir Caja',
                        'class' => 'btn btn-default ml-1',
                        'escape' => false
                    ]);
                }

            ?>

         </div>
    </div>

    <div class="col-xl-3">
        <legend class="sub-title-sm"><?= __('Información') ?></legend>
        <table class="vertical-table">
            <tr>
                <th><?= __('Número') ?></th>
                <td class="float-right"><?= sprintf("%'.03d", $cash_entity->id) ?></td>
            </tr>
            <tr>
                <th><?= __('Nombre') ?></th>
                <td class="float-right"><?= h($cash_entity->name) ?></td>
            </tr>
            <tr>
                <th><?= __('Estado') ?></th>
                <td class="float-right">
                <?php
                    if ($cash_entity->open) {
                        echo "<span class='font-weight-bold text-success'>Caja Abierta</span>";
                    } else {
                         echo "<span class='font-weight-bold text-danger'>Caja Cerrada</span>";
                    }
                ?>
                </td>
            </tr>
            <tr>
                <th><?= __('Responsable') ?></th>
                <td class="float-right"><?= $cash_entity->user->username ?></td>
            </tr>
            <tr>
                <th class="text-secondary"><?= __('Total Efectivo') ?></th>
                <td class="float-right font-weight-bold text-secondary" id="cash_contado"></td>
            </tr>
            <tr>
                <th class="text-secondary"><?= __('Total Otros') ?></th>
                <td class="float-right font-weight-bold text-secondary" id="cash_other"></td>
            </tr>
            <tr class="total-cash-row">
                <th class="text-secondary"><?= __('Total') ?></th>
                <td class="float-right font-weight-bold text-secondary" id="cash"></td>
            </tr>
         
            <tr>
                <th><?= __('Creado') ?></th>
                <td class="float-right"><?= date('d/m/Y H:i', strtotime($cash_entity->created))?></td>
            </tr>
            <tr>
                <th><?= __('Modificado') ?></th>
                <td class="float-right"><?= date('d/m/Y H:i', strtotime($cash_entity->modified))?></td>
            </tr>
            <tr>
                <th><?= __('Comentario') ?></th>
                <td class="float-right"><?= $cash_entity->comments ?></td>
            </tr>
        </table>
    </div>

    <div class="col-xl-9">
        <ul class="nav nav-tabs" id="myTab" role="tablist">

            <li class="nav-item">
                <a class="nav-link active" id="efectivo-tab" data-target="#efectivo" role="tab" aria-controls="efectivo" aria-expanded="true">Efectivo</a>
            </li>

            <li class="nav-item">
                <a class="nav-link " id="other_payment-tab" data-target="#other_payment" role="tab" aria-controls="other_payment" aria-expanded="false">Otros medios de pagos</a>
            </li>

        </ul>

        <div class="tab-content" id="myTabContent">

            <div class="tab-pane fade show active" id="efectivo" role="tabpanel" aria-labelledby="efectivo-tab">
                <?= $this->fetch('efectivo') ?>
            </div>

            <div class="tab-pane fade" id="other_payment" role="tabpanel" aria-labelledby="other_payment-tab">
                <?= $this->fetch('other_payment') ?>
            </div>

        </div>
    </div>

</div>

<script type="text/javascript">

    var cash_entity = <?= json_encode($cash_entity) ?>;

    function updateTotales() {

        var request = $.ajax({
            url: "/ispbrain/CashEntities/getCashEntityToatales.json",
            method: "POST",
            data: JSON.stringify({
                cash_entity_id: cash_entity.id,
            }),
            dataType: "json"
        });

        request.done(function(response) {
            $('#cash_contado').html( number_format(response.cash_entity.contado, true));
            $('#cash_other').html( number_format(response.cash_entity.cash_other, true));
            $('#cash').html( number_format(response.cash_entity.contado + response.cash_entity.cash_other, true));
        });

        request.fail(function(jqXHR) {

            if (jqXHR.status == 403) {

            	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            	setTimeout(function() { 
                    window.location.href = "/ispbrain";
            	}, 3000);

            } else {
            	generateNoty('error', 'Error al intentar obtener los totales');
            }
        });
    }

    $(document).ready(function() {

        $('#myTab a').click(function (e) {

            e.preventDefault();

            var target = $(this).data('target');
            $('#myTab a.active').removeClass('active');

            $('#myTabContent .active').fadeOut(100, function() {
                $('#myTabContent .active').removeClass('active');

                $(target).fadeIn(100, function() {
                    $(target).addClass('active show');
                    $(target + '-tab').addClass('active');
                    $(target + '-tab').trigger('shown.bs.tab');
                });
            });
        });

    });

</script>

