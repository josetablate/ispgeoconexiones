<style type="text/css">

    .text {
        font-size: 20px;
        color: #696868;
        font-weight: bold;
    }

    td > svg.green {
        color: green !important;
    }

    td > svg.red {
        color: red !important;
    }

</style>

<div class="row justify-content-center">
    <div class="col-xl-8">
        <table class="table table-hover table-bordered" id="table-method-payments">
            <thead>
                <tr>
                    <th></th>
                    <th>Nombre</th>
                    <th>Habilitado</th>
                    <!--<th>Automático</th>-->
                    <th>Ver Cobranza</th>
                    <th>Ver Portal</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($payment_getway->config as $key => $pg): ?>
                <tr data-name="<?= $key ?>" data-enabled="<?= ($pg->enabled == 0) ? '0' : '1' ?>">
                    <td>
                        <?php echo $this->Html->image($pg->logo, ['alt' => $pg->name, 'height' => '30px']); ?>
                    </td>
                    <td class="text"> <?= h($pg->name) ?></td>
                    <?php if ($pg->enabled): ?>
                        <td class="text">
                            <span class="fa fa-check green" aria-hidden="true"></span>
                            <span style="display:none;" >1</span>
                        </td>
                    <?php else:?>
                        <td class="text">
                            <span class="fa fa-times red" aria-hidden="true"></span>
                            <span style="display:none;" >0</span>
                        </td>
                    <?php endif; ?>
                    <!--<?php if ($pg->automatic): ?>-->
                    <!--    <td class="text">-->
                    <!--        <span class="fa fa-check green" aria-hidden="true"></span>-->
                    <!--        <span style="display:none;" >1</span>-->
                    <!--    </td>-->
                    <!--<?php else:?>-->
                    <!--    <td class="text">-->
                    <!--        <span class="fa fa-times red" aria-hidden="true"></span>-->
                    <!--        <span style="display:none;" >0</span>-->
                    <!--    </td>-->
                    <!--<?php endif; ?>-->
                    <?php if ($pg->cash): ?>
                        <td class="text">
                            <span class="fa fa-check green" aria-hidden="true"></span>
                            <span style="display:none;" >1</span>
                        </td>
                    <?php else:?>
                        <td class="text">
                            <span class="fa fa-times red" aria-hidden="true"></span>
                            <span style="display:none;" >0</span>
                        </td>
                    <?php endif; ?>
                    <?php if ($pg->portal): ?>
                        <td class="text">
                            <span class="fa fa-check green" aria-hidden="true"></span>
                            <span style="display:none;" >1</span>
                        </td>
                    <?php else:?>
                        <td class="text">
                            <span class="fa fa-times red" aria-hidden="true"></span>
                            <span style="display:none;" >0</span>
                        </td>
                    <?php endif; ?>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<?php

    foreach ($actions as $key => $action) {

        $buttons = [];

        foreach ($action as $k => $a) {

            $buttons[] = json_decode(json_encode($a), true);

        }
        echo $this->element('actions', ['modal'=> 'modal-' . $key . '-actions', 'title' => 'Acciones', 'buttons' => $buttons ]);
    }
?>

<script type="text/javascript">

    var table_method_payments = null;

    $(document).ready(function () {

        $('#table-method-payments').removeClass('display');

		table_method_payments = $('#table-method-payments').DataTable({
		    "order": [[ 0, 'desc' ]],
		    "autoWidth": true,
		    "scrollY": '400px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "paging":false,
		    "info": false,
		     "columnDefs": [
             { "type": "datetime-custom", "targets": [] }
             ],
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
		});

    });

    $(".btn-export").click(function() {

        bootbox.confirm('Se descargará un archivo Excel', function(result) {
            if (result) {
                $('#table-method-payments').tableExport({tableName: 'Metodos de Pagos', type:'excel', escape:'false'});
            }
        });
    });

    $('#table-method-payments tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_method_payments.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');

            var name = table_method_payments.$('tr.selected').data('name').toLowerCase();
            name = name.replace(/\s/g,''); 

            $('.modal-' + name + '-actions').modal('show');
        }
    });

    $('a#btn-edit').click(function() {

        $('.modal-actions').modal('hide');
        var id = table_method_payments.$('tr.selected').data('id');

        $('#modal-edit #id').val(id);
        $('#modal-edit #name').val(table_method_payments.$('tr.selected').data('name'));
        if (table_method_payments.$('tr.selected').data('enabled') == '1') {
            $('#modal-edit #enabled').closest('label').addClass('ui-state-active');
            $('#modal-edit #enabled').closest('label').addClass('ui-checkboxradio-checked');
            $('#modal-edit #enabled').attr('checked', true);
        } else {
            $('#modal-edit #enabled').attr('checked', false);
            $('#modal-edit #enabled').closest('label').removeClass('ui-state-active');
            $('#modal-edit #enabled').closest('label').removeClass('ui-checkboxradio-checked');
        }

        $('#modal-edit #enabled').val(table_method_payments.$('tr.selected').data('enabled'));
        $('#modal-edit').modal('show');

    });

    $('#modal-edit #enabled').click(function() {

        if ($('#modal-edit #enabled').val() == '1') {
            $('#modal-edit #enabled').val(0);
        } else {
            $('#modal-edit #enabled').val(1);
        }
    });

	$('a.btn-delete2').click(function() {

        var text = "¿Está Seguro que desea eliminar el método de pago?";
        var controller = "PaymentMethods";
        var id = table_method_payments.$('tr.selected').data('id');

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                    .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/delete/', 'method': 'post', 'id': 'replacer'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer').submit();
            }
        });
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
