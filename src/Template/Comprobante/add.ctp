<style type="text/css">

    #card-customer-selector .form-group {
        margin-bottom: 0;
    }

    .btn-link {
        color: #FF5722;
    }

    .btn-link:hover {
        color: #FF5722;
    }

</style>

<?php if ($paraments->customer->search_list_customers): ?>

<div class="modal fade" id="modal-customer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                
                <div class="row">
                    <div class="col-12">
                        <button type="button" class="close" aria-label="Close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        <?= $this->element('customer_list_seeker') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php else: ?>

<div class="modal fade" id="modal-customer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                
                <div class="row">
                    <div class="col-12">
                        <button type="button" class="close" aria-label="Close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        <?= $this->element('customer_seeker') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php endif; ?>

<div class="row">
    <div class="col-xl-12">
        <div class="card border-secondary p-1 mb-2" id="card-customer-selector">
            <div class="card-body p-2">
                
                <div class="row">
                    <div class="col-md-4 col-lg-1 col-xl-1">
                        <button type="button" class="btn btn-default " data-toggle="modal" data-target="#modal-customer">
                          Buscar
                        </button>
                    </div>
                    <div class="col-md-4 col-lg-4 col-xl-4">
                        <?php echo $this->Form->input('customer_code_info', ['type' => 'text', 'label' => false, 'placeholder' => 'Cliente seleccionado', 'readonly' => true, 'id' => 'customer_code_info']); ?>
                    </div>

                    <?php if ($paraments->gral_config->billing_for_service): ?>
                    <div class="col-md-12 col-lg-6 col-xl-7">
                         <?php echo $this->Form->input('connection_id', ['options' => [], 'label' => false]); ?>
                        <small id="emailHelp" class="form-text text-muted">Seleccione un 'Servicio' o 'Otras ventas' para generar un comprobante relacionado.</small>
                    </div>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-xl-12 mb-5">

        <div class="card border-secondary mb-0" >

            <div class="card-body text-secondary pb-0">

                <form id="form-add-comprobante">

                    <div class="row">

                        <div class="col-md-6 col-lg-4 col-xl-4">
                            <?php
                                echo $this->Form->input('business_id', ['options' => $business,  'label' =>  'Empresa', 'value' => '']); 
                            ?>
                        </div>

                        <div class="col-md-6 col-lg-4 col-xl-3">
                            <?php 
                                echo $this->Form->input('type', ['options' => [],  'label' =>  'Tipo', 'required' => true, 'class' => 'is-valid']);
                            ?>
                        </div>

                        <div class="col-md-6 col-lg-4 col-xl-2">
                            <?php
                                echo $this->Form->input('concept_type', ['options' => $concept_types,  'label' =>  'Conceptos', 'value' => 2]); 
                            ?>
                        </div>

                        <div class="col-md-6 col-lg-4 col-xl-3">
                            <?php
                                echo $this->Form->input('cond_venta', ['options' => $cond_venta,  'label' =>  'Cond. Venta', 'value' => 96]); 
                            ?>
                        </div>

                        <div class="col-md-6 col-lg-4 col-xl-2">
                            <label for="">Fecha</label>
                            <div class='input-group date' id='date-datetimepicker'>
                                <input name='date' id="date" type='text' class="form-control" />
                                <span class="input-group-addon input-group-text calendar">
                                    <span class="glyphicon icon-calendar"></span>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-4 col-xl-2">
                            <label for="">Desde</label>
                            <div class='input-group date' id='date_start-datetimepicker'>
                            <input name='date_start'id="date-start" type='text' class="form-control" />
                                <span class="input-group-addon input-group-text calendar">
                                    <span class="glyphicon icon-calendar"></span>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-4 col-xl-2">
                            <label for="">Hasta</label>
                            <div class='input-group date' id='date_end-datetimepicker'>
                            <input name='date_end'id="date-end" type='text' class="form-control" />
                                <span class="input-group-addon input-group-text calendar">
                                    <span class="glyphicon icon-calendar"></span>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-4 col-xl-2">
                            <label for="">Vencimiento</label>
                            <div class='input-group date' id='duedate-datetimepicker'>
                            <input name='duedate'id="duedate" type='text' class="form-control" />
                                <span class="input-group-addon input-group-text calendar">
                                    <span class="glyphicon icon-calendar"></span>
                                </span>
                            </div>
                        </div>
                     
                        <div class="col-md-6 col-lg-12 col-xl-12">
                            <?= $this->Form->input('comments', ['label' =>  'Comentario', 'value' => '']) ?>
                        </div>

                    </div>

                    <div class="row mb-1">

                        <div class="col-xl-12">

                            <label for="">Conceptos</label>

                            <table class="table table-bordered table-hover" id="table-concepts">
                                <thead>
                                    <tr style="font-size: small;">
                                        <th>Tipo</th>
                                        <th>Código</th>
                                        <th>Descripción</th>
                                        <th>Cant.</th>
                                        <th>U. Medida</th>
                                        <th>Precio U.</th>
                                        <th>Subtotal</th>
                                        <th>Alícuota IVA</th>
                                        <th>Subtotal c/IVA</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody> 
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row ">
                        <div class="col-md-6 col-lg-4  col-xl-3">

                            <div class="card border-secondary p-1 mb-2">
                                <div class="card-body p-2">

                                    <div class="input-group">
                                        <span class="input-group-text w-50" id="basic-addon1">Subtotal</span>
                                        <input type="text" class="form-control text-right" id="footer_subtotal" value="00.00" >
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-text w-50" id="basic-addon1">Subtotal IVA</span>
                                        <input type="text" class="form-control text-right" id="footer_subtotal_iva" value="00.00" >
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-text w-50" id="basic-addon1">TOTAL</span>
                                        <input type="text" class="form-control text-right" id="footer_total" value="00.00" >
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-6 col-lg-8 col-xl-9">
                            <a class="btn btn-default btn-add-concept float-right" href="#" role="button">
                                Agregar Concepto
                            </a>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-xl-12">
                            <?= $this->Form->input('auto_print', ['label' => 'Mostrar comprobante luego de su generación', 'class' => '', 'type' => 'checkbox', 'checked' => $paraments->invoicing->show_comprobante_from_facturador_manual_after_generation]) ?>
                        </div>
                        <div class="col-xl-12 mt-1">
                            <?= $this->Form->button(__('Confimar'), ['class' => 'btn-submit btn-success float-right']) ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

<div id="modal-add-concept" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Nuevo Concepto</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <form id="form-add-concept">

                    <div class="row">
                        <div class="col-xl-3">
                            <?= $this->Form->input('code_concept', ['label' => 'Código', 'maxlength' => 4, 'value' => 99, 'required' => true]) ?>
                        </div>

                        <div class="col-xl-3">
                            <?= $this->Form->input('type_concept', ['label' => 'Tipo', 'options' => ['S' => 'Servicio', 'P' => 'Producto', 'I' => 'Paquete']]) ?>
                        </div>

                        <div class="col-xl-6">
                            <?= $this->Form->input('description', ['label' => 'Descripción', 'required' => true]) ?>
                        </div>

                        <div class="col-xl-2">
                            <?= $this->Form->input('quantity', ['type' => 'number', 'label' => 'Cantidad', 'value' => 1 , 'required' => true]) ?>
                        </div>
                        <div class="col-xl-3">
                            <?= $this->Form->input('unit', ['label' => 'Unidad', 'options' => $units_types, 'value' => 7, 'required' => true ]) ?>
                        </div>

                        <div class="col-xl-4">
                            <?= $this->Form->input('tax', ['label' => 'Alícuota', 'options' => $alicuotas_types , 'required' => true]) ?>
                        </div>
                        <div class="col-xl-3">
                            <?= $this->Form->input('total', ['type' => 'number', 'label' => 'Total', 'step' => 0.01,  'readonly' => false, 'required' => true ]) ?>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-xl-12">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-success float-right">Agregar</button>
                        </div>
                    </div>

                </form>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<?php
    echo $this->element('modal_preloader');
?>

<script type="text/javascript">

    $(document).ready(function() {

        var business_selected = null;

        $('#business-id').change(function() {

           var selected = $(this).val() ;

           business_selected = null;

           $('#type').empty();

           $.each(sessionPHP.paraments.invoicing.business, function(i, business) {

                if (selected == business.id) {

                    business_selected = business;
                    
                    $('#type').append('<option value="">Seleccione Tipo</option>');
                    $('#type').append('<optgroup label="Presupuesto"><option value="000">PRESUPUESTO</option></optgroup>');

                    if (business.responsible == 1) { //ri

                        if (business.webservice_afip.crt != '') {
                            $('#type').append('<optgroup label="Facturas"><option value="XXX">PRESU X</option><option value="001">FACTURA A</option><option value="006">FACTURA B</option></optgroup>');
                            $('#type').append('<optgroup label="Nota Débito"><option value="NDX">NOTA DE DÉBITO X</option><option value="002">NOTA DE DÉBITO A</option><option value="007">NOTA DE DÉBITO B</option></optgroup>');
                            $('#type').append('<optgroup label="Nota Crédito"><option value="NCX">NOTA DE CRÉDITO X</option><option value="003">NOTA DE CRÉDITO A</option><option value="008">NOTA DE CRÉDITO B</option></optgroup>');
                        } else {
                            $('#type').append('<optgroup label="Facturas"><option value="XXX">PRESU X</option></optgroup>');
                            $('#type').append('<optgroup label="Nota Débito"><option value="NDX">NOTA DE DÉBITO X</option>/optgroup>');
                            $('#type').append('<optgroup label="Nota Crédito"><option value="NCX">NOTA DE CRÉDITO X</option></optgroup>');
                        }
                    } else if (business.responsible == 6) { //monotributerou
                       
                        if (business.webservice_afip.crt != '') {
                            $('#type').append('<optgroup label="Facturas"><option value="XXX">PRESU X</option><option value="011">FACTURA C</option></optgroup>');
                            $('#type').append('<optgroup label="Nota Débito"><option value="NDX">NOTA DE DÉBITO X</option><option value="012">NOTA DE DÉBITO C</option></optgroup>');
                            $('#type').append('<optgroup label="Nota Crédito"><option value="NCX">NOTA DE CRÉDITO X</option><option value="013">NOTA DE CRÉDITO C</option></optgroup>');
                        } else {
                            $('#type').append('<optgroup label="Facturas"><option value="XXX">PRESU X</option></optgroup>');
                            $('#type').append('<optgroup label="Nota Débito"><option value="NDX">NOTA DE DÉBITO X</option></optgroup>');
                            $('#type').append('<optgroup label="Nota Crédito"><option value="NCX">NOTA DE CRÉDITO X</option></optgroup>');
                        }
                   }
               }
           });
        });

        $('#business-id').change();

        var table_concepts = null;
        var table_other_comprobantes = null;
        var other_comprobante_selected =null;

        //concepts

        $('#table-concepts').removeClass('display');

        table_concepts = $('#table-concepts').DataTable({
            "deferRender": true,
    	    "autoWidth": true,
    	    "scrollX": true,
    	    "searching" : false,
            "ordering": false,
    	    "paging": false,
    	    "info": false,
    	    "columns": [
    	        { 
                    "data": "type",
                },
                { 
                    "data": "code",
                },
                { 
                    "data": "description",
                },
                { 
                    "data": "quantity",
                },
                { 
                    "data": "unit",
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.units_types[data];
                    }
                },
                { 
                    "data": "price",
                },
                { 
                    "data": "sum_price",
                },
                { 
                    "data": "tax",
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.alicuotas_types[data];
                    }
                },
                { 
                    "data": "total",
                },
                {
                    "className":      'delete-control',
                    "orderable":      false,
                    "data":           null,
                    "render": function ( data, type, full, meta ) {
                        return "<a href='javascript:void(0)' class='grey'><span class='icon-bin' aria-hidden='true'></span></a>";
                    },
                    "width": '3%'
                }
            ],
    	    "columnDefs": [
    	        {
    	            target: 0,
    	            'visible': false,
    	        },
    	        {
    	            "targe": [1, 3],
    	            'width': '38%',
    	        }
            ],
    	    "language": {
                "emptyTable": "Sin resultados.",
            },
    
    	});

        $('#table-concepts tbody').on( 'click', 'td', function () {

           if (!$(this).closest('tr').find('.dataTables_empty').length) {

                var column = table_concepts.cell( this ).index().columnVisible;

                if (column == 9) {

                    row = table_concepts.row( $(this).closest('tr') );

                    var data = row.data();

                    var footer_subtotal = parseFloat($('#footer_subtotal').val());
                    footer_subtotal -= parseFloat(data.sum_price);
                    $('#footer_subtotal').val(parseFloat(footer_subtotal).toFixed(2));

                    var footer_subtotal_iva = parseFloat($('#footer_subtotal_iva').val());
                    footer_subtotal_iva -= parseFloat(data.sum_tax);
                    $('#footer_subtotal_iva').val(parseFloat(footer_subtotal_iva).toFixed(2));

                    var footer_total = parseFloat($('#footer_total').val());
                    footer_total -= parseFloat(data.total);
                    $('#footer_total').val(parseFloat(footer_total).toFixed(2));

                    row.remove().draw();
                }
           }

        });

        //end concepts

        $('form#form-add-concept').submit(function() {

            event.preventDefault();

            var total = parseFloat($('#total').val());

            var tax_ = parseFloat($('#tax').val());

            if (table_concepts.rows().count() == 10) {
                generateNoty('warning', 'La creación de comprobantes permite hasta 10 conceptos');
            } else {

                var inputs = $(this).serializeArray();

                var concept = {};

                $.each(inputs, function( i, input) {
                    concept[input.name] = input.value;
                });

                var total = parseFloat(concept['total']);

                var tax_percentage = sessionPHP.afip_codes.alicuotas_percectage[concept['tax']];

                var quantity = parseFloat(concept['quantity']);

                var row = calulateIvaAndNeto(total, tax_percentage, quantity);

                table_concepts.rows.add([{
                    'type': concept['type_concept'],
                    'code': concept['code_concept'],
                    'description': concept['description'],
                    'quantity': quantity,
                    'unit': concept['unit'],
                    'price': row.price,
                    'discount': 0,
                    'sum_price': row.sum_price,
                    'tax': concept['tax'],
                    'sum_tax': row.sum_tax,
                    'total': row.total
                }]).draw();

                var footer_subtotal = parseFloat($('#footer_subtotal').val());
                footer_subtotal += row.sum_price;
                $('#footer_subtotal').val(parseFloat(footer_subtotal).toFixed(2));

                var footer_subtotal_iva = parseFloat($('#footer_subtotal_iva').val());
                footer_subtotal_iva += row.sum_tax;
                $('#footer_subtotal_iva').val(footer_subtotal_iva.toFixed(2));

                var footer_total = parseFloat($('#footer_total').val());
                footer_total += row.total;
                $('#footer_total').val(parseFloat(footer_total).toFixed(2));

                $('#modal-add-concept').modal('hide');
            }
        });

        $('.btn-add-concept').click(function(){
    	    $("#modal-add-concept").modal('show');
    	});

    	$('#modal-add-concept').on('shown.bs.modal', function (e) {
            $('#description').focus();
            clearFormConcept();
        });

        $('#card-customer-seeker').on('CUSTOMER_SELECTED', function() {

            var doc_type_name = sessionPHP.afip_codes.doc_types[customer_selected.doc_type];

            var ident = "";
            if (customer_selected.ident) {
                ident = customer_selected.ident;
            }

            $('#card-customer-selector #customer_code_info').val(pad(customer_selected.code,5) + ' ' + doc_type_name + ' ' + ident + ' ' + customer_selected.name.toUpperCase());

            $('#form-add-comprobante #business-id').val(customer_selected.business_billing);
            $('#form-add-comprobante #business-id').change()


            $('#modal-customer').modal('hide');

            $.ajax({

                url: "<?= $this->Url->build(['controller' => 'Connections', 'action' => 'getConnectionsSelectByCustomer']) ?>",
                type: 'POST',
                dataType: "json",
                data: JSON.stringify({customer_code: customer_selected.code }),
                success: function(data) {

                    $('#connection-id').find('option').remove();
                    $('#connection-id').prop('required', false);
                    $('#connection-id').prop('disabled', true);

                    if (customer_selected.billing_for_service) {

                        $('#connection-id').prop('required', true);
                        $('#connection-id').prop('disabled', false);
                        $('#connection-id').append('<option value="">Seleccione servicio</option>');

                        $.each(data.connectionsArray, function(i, val) {
                            $('#connection-id').append('<option value="' + i + '">' + val + '</option>');
                        });

                        $('#connection-id').change();
                    }
                }, 
                error: function (jqXHR) {
                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                    	  window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al intentar obtener las conexión del cliente.');
                    }
                }
            });
            
        });
        
        $('#modal-customer').on('shown.bs.modal', function (e) {

            if (sessionPHP.paraments.customer.search_list_customers) {
                
                enableLoad = 1;
                table_customers.draw();
            } else {

                $('#card-customer-seeker input#customer-ident').focus();
            }
        });

        $('#type').trigger('change');

        $('#date-datetimepicker').datetimepicker({
            locale: 'es',
            defaultDate: new Date(),
            format: 'DD/MM/YYYY'
        });

        $('#date_start-datetimepicker').datetimepicker({
            locale: 'es',
            defaultDate: new Date(),
            format: 'DD/MM/YYYY'
        });

        $('#date_end-datetimepicker').datetimepicker({
            locale: 'es',
            defaultDate: new Date(),
            format: 'DD/MM/YYYY'
        });

        $('#duedate-datetimepicker').datetimepicker({
            locale: 'es',
            defaultDate: new Date(),
            format: 'DD/MM/YYYY'
        });
        
        $('#quantity').keyup(function() {
            updateTotal();
        });

        function updateTotal() {

            var total = parseFloat($('#total').val());
            var quantity =  $('#quantity').val();

            total = total * quantity;

            $('#total').val(total.toFixed(2));
        }

        function clearFormConcept() {

             $('#type-concept').val('S');
             $('#code-concept').val(99);
             $('#description').val('');
             $('#quantity').val(1);
             $('#unit').val(7);
             $('#price').val('');
             $('#sum-price').val('');

             $('#tax').val(1);

             if (business_selected && business_selected.responsible == 1) {
                 $('#tax').val(5);
             }

             $('#total').val('');
        }

        function clear_selected_customer() {

            customer_selected = null;

            $('#card-customer-selector #customer_code_info').val('');
            $('#connection-id').find('option').remove();
        }

        function clearAll() {

            clear_selected_customer();

            var now = new Date();

            $('#date-datetimepicker').data("DateTimePicker").date(now);
            $('#date_start-datetimepicker').data("DateTimePicker").date(now);
            $('#date_end-datetimepicker').data("DateTimePicker").date(now);
            $('#duedate-datetimepicker').data("DateTimePicker").date(now);

            $('#comments').val('');
            $('#comments').val('');

            table_concepts.clear().draw();

            //concept

            $('#type-concept').val('S');
            $('#code-concept').val(99);
            $('#description').val('');
            $('#quantity').val(1);
            $('#unit').val(7);
            $('#price').val('');
            $('#sum-price').val('');
            $('#tax').val(1);
            $('#total').val('');

            $('.btn-submit').show(); 

            $('#footer_subtotal').val(0.00);
            $('#footer_subtotal_iva').val(0.00);
            $('#footer_total').val(0.00);
        }

        $('#form-add-comprobante').submit(function(e) {

            var flag = true;

            e.preventDefault();

            var send_data = {};

            var table_concepts_rows = table_concepts.rows().data();

            if (!customer_selected) {

                generateNoty('warning', 'Debe especificar un cliente.');
                flag = false;
            }

            if (customer_selected && customer_selected.billing_for_service) {
                if ($('#connection-id').val() == '') {
                    generateNoty('warning', 'Debe especificar un servicio o seleccione otras ventas.');
                    flag = false;
                }
            }

            if (table_concepts_rows.length == 0) {

                generateNoty('warning', 'Debe agregar al menos 1 concepto.');

                flag = false;
            }

            if ($('#footer_total').val() <= 0) {

                generateNoty('warning', 'El total del comprobante no puede ser menor o igual a 0.');

                flag = false;
            }

            if (flag) {

                bootbox.confirm('Confirmar', function(result) {
                    
                    if (result) {

                        send_data.customer_code = customer_selected.code;

                        send_data.connection_id = null;

                        if (customer_selected.billing_for_service) {
                            send_data.connection_id = $('#connection-id').val() != 0 ? $('#connection-id').val() : null;
                        }

                        send_data.concepts = [];

                        $.each(table_concepts_rows, function(i, val) {
                            send_data.concepts.push(val);
                        });

                        var form_data = $('#form-add-comprobante').serializeArray();

                        $.each(form_data, function(i, val) {

                            if (val.name == 'date' || val.name == 'date_start' || val.name == 'date_end' || val.name == 'duedate') {
                                val.value = val.value.split("/");
                                var d = new Date();
                                val.value = val.value[2] + '-' + val.value[1] + '-' + val.value[0] + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
                            }
                            
                            if (val.name == 'period') {
                                val.value = val.value.split("/");
                                val.value = val.value[0] + '/' + val.value[1];
                            }

                            send_data[val.name] = val.value;
                        });

                        openModalPreloader("Generando Comprobante. Espere Por favor ...");

                        $('.btn-submit').hide(); 

                        $.ajax({
                            type: 'POST',
                            dataType: "json",
                            url: '/ispbrain/comprobante/add',
                            data:  JSON.stringify(send_data),
                            success: function(data) {

                                if (data.response) {

                                    switch (data.response.tipo_comp) {
                                        
                                        
                                        case '000':

                                            clearAll();

                                            generateNoty('success', 'Comprobante generado correctamente.');

                                            if ($('#auto-print').is(":checked")) {
                                                var url = "/ispbrain/Presupuestos/showprint/" + data.response.comp_id;
                                                window.open(url, 'Imprimir Comprobante', "width=600, height=500");
                                            }

                                            break;
                                        
                                        case 'XXX':
                                        case '001':
                                        case '006':
                                        case '011':

                                            clearAll();

                                            generateNoty('success', 'Comprobante generado correctamente.');

                                            if ($('#auto-print').is(":checked")) {
                                                var url = "/ispbrain/Invoices/showprint/" + data.response.comp_id;
                                                window.open(url, 'Imprimir Comprobante', "width=600, height=500");
                                            }

                                            break;

                                        case 'NDX':
                                        case '002':
                                        case '007':
                                        case '012':

                                            clearAll();

                                            generateNoty('success', 'Comprobante generado correctamente.');

                                            if ($('#auto-print').is(":checked")) {
                                                var url = "/ispbrain/DebitNotes/showprint/" + data.response.comp_id;
                                                window.open(url, 'Imprimir Comprobante', "width=600, height=500");
                                            }

                                            break;
    
                                        case 'NCX':
                                        case '003':
                                        case '008':
                                        case '013':

                                            clearAll();

                                            generateNoty('success', 'Comprobante generado correctamente.');

                                            if ($('#auto-print').is(":checked")) {
                                                var url = "/ispbrain/CreditNotes/showprint/" + data.response.comp_id;
                                                window.open(url, 'Imprimir Comprobante', "width=600, height=500");
                                            }

                                            break;
                                    }

                                    $('.btn-submit').show(); 

                                } else {
                                    generateNoty('error', 'No se pudo generar el comprobante.');
                                    $('.btn-submit').show(); 
                                }
                                
                                updateCountersTitle();

                                closeModalPreloader();

                            },
                            error: function(jqXHR) {

                                if (jqXHR.status == 403) {

                                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                                	setTimeout(function() { 
                                        window.location.href = "/ispbrain";
                                	}, 3000);

                                } else {
                                	generateNoty('error', 'No se pudo generar el comprobante.');
    
                                    closeModalPreloader();

                                    $('.btn-submit').show();
                                }
                            }
                        });

                    }
                });
            }
        });

    });

</script>
