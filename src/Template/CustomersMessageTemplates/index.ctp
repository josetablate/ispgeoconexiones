<style type="text/css">

    #table-message-templates {
        width: 100% !important;
    }

    #table-message-templates td {
        margin: 0px 0px 0px 0px !important;
        padding: 0px 5px 0px 5px !important;
        vertical-align: middle;
        max-width: 150px !important;
        word-wrap: break-word !important;
    }

    .modal-actions a.btn {
        width: 100%;
        margin-bottom: 5px;
    }

    tr.selected {
        background-color: #8eea99;
        color: #333 !important;
    }

    .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
        background-color: #d2d2d2;
    }

    .vertical-table {
        width: 100%;
    }

    .vertical-table td {
        max-width: 250px !important;
        word-wrap: break-word !important;
    }

    .filter-apply > span.glyphicon {
       color: white !important;
    }

    .filter-apply {
        background-color: #f05f40 !important;
    }

</style>

<div class="alert alert-info alert-dismissible fade show" role="alert">
    <i class="fas fa-info-circle text-info"></i> Se ve un aviso por vez, el aviso con prioridad más alta y activo
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5" >
        <a class="btn btn-default" title="<?= ('Nuevo Template') ?>" href="<?= $this->Url->build(["action" => "add"]) ?>" role="button">
            <span class="glyphicon icon-plus" aria-hidden="true"></span>
        </a>

        <a class="btn btn-default btn-filter-column" title="Filtrar por Columnas" href="javascript:void(0)" role="button" data-toggle='modal' data-target='#modal-filter'>
            <span class="glyphicon icon-filter" aria-hidden="true"></span>
        </a>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-hover" id="table-message-templates" >
            <thead>
                <tr>
                    <th><?= ('#') ?></th>
                    <th><?= ('Nombre') ?></th>
                    <th><?= ('Descripción') ?></th>
                    <th><?= ('Categoría') ?></th>
                    <th><?= ('Prioridad') ?></th>
                    <th><?= ('Creado') ?></th>
                    <th><?= ('Activo') ?></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="modal-filter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Filtrar por columnas</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <?= $this->Form->create() ?>

                    <div class="row">
                        <div class="col-md-4">
                            <?= $this->Form->input('name', ['label' => __('Nombre'), 'class'=> 'column_search', 'data-column' => 1]) ?>
                            <?= $this->Form->input('description', ['label' => __('Descripción'), 'class'=> 'column_search', 'data-column' => 2]) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $this->Form->input('category', ['label' => __('Categoría'), 'class'=> 'column_search', 'data-column' => 3]) ?>
                            <?= $this->Form->input('priority', ['label' => __('Prioridad'), 'class'=> 'column_search', 'data-column' => 4]) ?>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label" for="created">Creado</label>
                                <div class='input-group date' id='created-datetimepicker'>
                                    <span class="input-group-addon mr-0">Fecha</span>
                                    <input name="input-created" id="input-created" type='text' class="column_search form-control datetimepicker" data-column="5" />
                                    <span class="input-group-addon mr-0">
                                        <span class="glyphicon icon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <?= $this->Form->input('active', ['label' => __('Activo'), 'class'=> 'column_search', 'data-column' => 6, 'checked' => false]) ?>
                            <?= $this->Form->input('portal_sync', ['label' => __('Portal'), 'class'=> 'column_search', 'data-column' => 7, 'checked' => false]) ?>
                        </div>
                    </div>

                <?= $this->Form->end() ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-clear-filter" >Limpiar</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-edit',
        'name' =>  'Editar',
        'icon' =>  'glyphicon icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete2',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-templates', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<script type="text/javascript">

    var messageTemplates = <?= json_encode($customersMessageTemplates) ?>;
    var customer_message_template_selected;

    var table_message_templates = null;
    var hidden_columns = [];

    $(document).ready(function () {

        $('#table-message-templates').removeClass('display');

		table_message_templates = $('#table-message-templates').DataTable({
		    "order": [[ 1, 'asc' ]],
		    "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/CustomersMessageTemplates/index.json",
                "dataSrc": "customersMessageTemplates",
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
            				  window.location.href ="/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "width": "100%",
		    "initComplete": function(settings, json) {
		        setTimeout(function() { table_message_templates.draw(); }, 100);
		    },
		    "columns": [
		        { "data": "id" },
		        { "data": "name" },
		        { "data": "description" },
		        { "data": "category" },
		        { "data": "priority" },
                {
                    "data": "created",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                { 
                    "data": 'active',
                    "render": function(data, type, full, meta) {

                        if (type === 'display') {
                            var enabled = '<i class="fa fa-times red" aria-hidden="true"></i>';
                            if (data) {
                                enabled = '<i class="fa fa-check green" aria-hidden="true"></i>';
                            }
                            data = enabled;
                        }

                        return data;
                    }
                }
            ],
		    "columnDefs": [
		        { 
		            "targets": hidden_columns, 
		            "visible": false
		        },
            ],
            "createdRow" : function( row, data, index ) {
               row.id = data.id;
            },
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
		});

		table_message_templates.columns.adjust().draw();
		$('#table-message-templates_filter').closest('div').before($('#btns-tools').contents());
		
		$('.column_search').on( 'keyup', function () {
    
            table_message_templates
                .columns( $(this).data('column') )
                .search( this.value )
                .draw();
    
            checkStatusFilter();
        });
    
        $('#created-datetimepicker').datetimepicker({
            locale: 'es',
            format: 'DD/MM/YYYY'
        }).on('dp.change', function (ev) {
    
            table_message_templates
                .columns( 5 )
                .search( $('#input-created').val() )
                .draw();
    
            checkStatusFilter();
        });
    
        $('input:checkbox.column_search').change(function() {
    
            table_message_templates
                .columns( $(this).data('column') )
                .search( $(this).is(":checked") )
                .draw();
    
            checkStatusFilter();
        });
    
        $('.btn-clear-filter').click(function() {
            $('.column_search').val('');
            table_message_templates.search( '' ).columns().search( '' ).draw();
            $('.column_search').prop('checked', false);
            checkStatusFilter();
        });

        $('.column_search').val('');
        table_message_templates.search( '' ).columns().search( '' ).draw();
        $('.column_search').prop('checked', false);
        checkStatusFilter();
    });

    $('#table-message-templates tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_message_templates.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');

            customer_message_template_selected = table_message_templates.row( this ).data();

            $('.modal-templates').modal('show');
        }
    });

    $('a#btn-edit').click(function() {
       var action = '/ispbrain/customers-message-templates/edit/' + customer_message_template_selected.id;
       window.open(action, '_self');
    });

    $(document).on("click", "#btn-delete2", function(e) {

        var controller = 'CustomersMessageTemplates';
        var id = customer_message_template_selected.id;
        var name = customer_message_template_selected.name;
        var text = '¿Está Seguro que desea eliminar el Template: ' + name + '?';

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                    .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/delete/', 'method': 'post', 'id': 'replacer'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer').submit();
            }
        });
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
