<style type="text/css">

    .title {
        margin-left: 0px;
        margin-right: 0px;
        margin-bottom: 10px;
    }

    .title label.label-control {
        margin: 3px 0 3px 0;
    }

    .title label.label-control {
        margin: 3px 0 3px 0;
    }

    textarea.form-control {
        height: 80px !important;
    }

    span.red {
        color: red;
    }

</style>

<div class="row">
    <div class="col-md-12">
        <?= $this->Form->create($customerMessageTemplate, ['id' => 'message_template_form']) ?>
        <fieldset>
    
            <div class="row">
                <div class="col-md-3">
                    <label for="name" class="label-control"><?= __('Nombre') ?></label>
                    <?php 
                        echo $this->Form->text('name', ['required' => true, 'type' => 'textarea'] );
                        echo $this->Form->input('category', [ 'label' => __('Categoría'), 'type' => 'text']);
                    ?>
                    <div class="form-group number required">
                        <label class="control-label" for="priority">Prioridad</label>
                        <?= $this->Form->text('priority', ['class' => 'form-control', 'type' => 'number', 'min' => 0]); ?>
                        <small class="form-text text-muted m-0 ">El valor más alto representa la prioridad más alta</small>
                    </div>
                    <?php
                        echo $this->Form->input('description', ['type' => 'textarea', 'rows' => 2, 'label' => __('Descripción')]);
                        echo $this->Form->input('active', ['label' => 'Activo', 'type' => 'checkbox']);
                    ?>

                    <button type="submit" class="btn btn-success"><?= __('Agregar') ?></button>
                </div>
                <div class="col-md-9">
                    <textarea name="html" id="editor1" rows="10" cols="80">
                        Mensaje...
                    </textarea>
                </div>
            </div>
    
        </fieldset>
    
        <?= $this->Form->end() ?>
     </div>

</div>

<script>

    var editor  = null;

    $(document).ready(function() {

	    editor = CKEDITOR.replace( 'editor1' );

        CKEDITOR.config.height = 350;
        CKEDITOR.config.width = 'auto';

    });

</script>
