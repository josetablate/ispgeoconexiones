<?php $this->extend('/Settings/views/download_upload'); ?>

<?php $this->start('emails_accounts'); ?>

    <style type="text/css">

        legend {
            display: block;
            width: 100%;
            padding: 0;
            margin-bottom: 20px;
            font-size: 21px;
            line-height: inherit;
            color: #333;
            border: 0;
            border-bottom: 1px solid #e5e5e5;
        }

        .accounts-container {
            background: whitesmoke;
            border-radius: 4px;
            padding: 25px;
            border: 1px solid #FF5722;
        }

    </style>

    <br>
    <div class="row">

        <div class="col-md-12">
            <legend>Configuración de Proveedores de Correos</legend>

            <div class="row">
                <div class="col-md-6">

                    <?= $this->Form->create($paraments, ['class' => 'form-load form-inline', 'id' => 'form-emails_accounts'] ) ?>

                    <fieldset>

                        <?= $this->Form->hidden('form', ['value' => 'emails_accounts']); ?>
                        <?= $this->Form->hidden('sub_form', ['value' => 'config']); ?>

                        <div class="row">
                            <div class="col-md-6">
                                <label for="provider">Proveedor</label>
                                <?php echo $this->Form->select('provider', $providers, ['value' => $paraments->emails_accounts->provider]); ?>

                            </div>
                        </div>
                    </fieldset>
                    <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success mt-4' ]) ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>

        </div>

        <div class="col-md-4">

            <div class="accounts-container">

                <?= $this->Form->create($paraments, ['class' => 'form-load', 'id' => 'form-emails_accounts'] ) ?>

                <fieldset>

                    <legend>Sendgrid</legend>

                    <?= $this->Form->hidden('form', ['value' => 'emails_accounts']); ?>
                    <?= $this->Form->hidden('sub_form', ['value' => 'sendgrid']); ?>

                    <div class="row">
                        <div class="col-md-12">
                            <?php

                                $api_key = "";
                                $enabled = FALSE;

                                $api_key = $this->request->getSession()->read('paraments')->emails_accounts->providers->sendgrid->api_key;
                                $enabled = $this->request->getSession()->read('paraments')->emails_accounts->providers->sendgrid->enabled;

                                echo $this->Form->input('api_key', ['label' => 'API KEY', 'type' => 'text', 'value' => $api_key, 'required' => TRUE, 'data-toggle' => 'tooltip','data-placement' => 'top','title' => $api_key]);
                                echo $this->Form->input('enabled', ['label' => 'Habilitado', 'type' => 'checkbox', 'checked' => $enabled]);
                            ?>
                        </div>
                    </div>
                </fieldset>
                <?= $this->Html->link(__('Cancelar'),["action" => "index"], ['class' => 'btn btn-default ']) ?>
                <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success' ]) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>

    </div>
<?php $this->end(); ?>
