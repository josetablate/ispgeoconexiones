<?php $this->extend('/Settings/views/contable'); ?>

<?php $this->start('duedates'); ?>

    <style type="text/css">

        .card-title {
            margin-bottom: 1px;
        }

        legend {
            margin-bottom: 10px !important;
        }

    </style>

    <?= $this->Form->create($paraments,  ['class' => ['form-load', 'mt-1']]) ?>
    <fieldset>

        <?= $this->Form->hidden('form', ['value' => 'duedates']); ?>

        <div class="row">

            <div class="col-md-5">

                <div class="card bg-light mt-2" >
                    <div class="card-body">
                        <h5 class="card-title">Bloqueo Automático de Conexiones</h5>
                        <h7 class="card-subtitle mb-2 text-muted">Configure los parámetros por defecto que aplicará a los clientes.</h7>

                        <div class="row">

                            <div class="col-xl-6">
                                <?= $this->Form->input('automatic_connection_blocking.enabled', ['type' => 'checkbox', 'label' => 'Habilitar', 'checked' => $paraments->automatic_connection_blocking->enabled]); ?>
                            </div>

                            <div class="col-xl-6">
                                <div class="row">
                                    <div class="col-xl-12">
                                        <legend>Configuración a nivel</legend>
                                    </div>
                                    <div class="col-xl-12">
                                        <input type="hidden" name="automatic_connection_blocking[force]" value="">
                                        <label for="automatic-connection-blocking-force-1" style='width: 100%'>
                                        <input type="radio" name="automatic_connection_blocking[force]" value="1" id="automatic-connection-blocking-force-1" <?= $paraments->automatic_connection_blocking->force ? 'checked="checked"' : '' ?> <?= $paraments->automatic_connection_blocking->enabled ? '' : 'disabled="disabled"' ?>> General</label>       
                                        <label for="automatic-connection-blocking-force-0" style='width: 100%'>
                                        <input type="radio" name="automatic_connection_blocking[force]" value="0" id="automatic-connection-blocking-force-0" <?= $paraments->automatic_connection_blocking->force ? '' : 'checked="checked"' ?> <?= $paraments->automatic_connection_blocking->enabled ? '' : 'disabled="disabled"' ?>> Individual por cliente</label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-12">
                                <legend>Control</legend>
                            </div>

                            <div class="col-xl-6">
                                <?= $this->Form->input('automatic_connection_blocking.day', ['label' => 'Día', 'value' => $paraments->automatic_connection_blocking->day, 'min' => 1, 'max' => 28, 'disabled' => $paraments->automatic_connection_blocking->enabled ? '' : 'disabled']); ?>
                            </div> 

                            <div class="col-xl-6">

                                <div class="form-group required">
                                    <label class="control-label" for="automatic_connection_blocking-hours-execution">Horario de Ejecución</label>
                                    <div class='input-group date' id='automatic_connection_blocking-hours-execution-datetimepicker'>
                                        <input  name="automatic_connection_blocking[hours_execution]" required="required" id="automatic_connection_blocking-hours-execution" type='text' class="form-control" <?= $paraments->automatic_connection_blocking->enabled ? '' : 'disabled="disabled"' ?> />
                                        <span class="input-group-addon input-group-text calendar mr-0">
                                            <span class="glyphicon icon-calendar"></span>
                                        </span>
                                    </div>
                                </div>

                            </div>

                            <div class="col-xl-6">
                                <?= $this->Form->input('automatic_connection_blocking.due_debt', ['label' => 'Deuda vencida', 'value' => $paraments->automatic_connection_blocking->due_debt, 'step' => 0.01, 'disabled' => $paraments->automatic_connection_blocking->enabled ? '' : 'disabled']); ?>
                            </div> 

                            <div class="col-xl-6">
                                <?= $this->Form->input('automatic_connection_blocking.invoice_no_paid', ['label' => 'Facturas impagas', 'value' => $paraments->automatic_connection_blocking->invoice_no_paid, 'step' => 0.01, 'disabled' => $paraments->automatic_connection_blocking->enabled ? '' : 'disabled']); ?>
                            </div>

                            <div class="col-xl-12">
                                <legend class="mb-2 text-muted text-naranja">Recargo por reconexión automático</legend>
                            </div>

                            <div class="col-xl-12">
                                <?= $this->Form->input('automatic_connection_blocking.surcharge_enabled', ['type' => 'checkbox', 'label' => 'Aplicar regargo','checked' => $paraments->automatic_connection_blocking->surcharge_enabled, 'disabled' => $paraments->automatic_connection_blocking->enabled ? '' : 'disabled']); ?>
                            </div> 

                            <div class="col-xl-6">
                                <?= $this->Form->input('automatic_connection_blocking.surcharge_value', ['label' => 'Importe', 'value' => $paraments->automatic_connection_blocking->surcharge_value, 'step' => 0.01, 'disabled' => ($paraments->automatic_connection_blocking->enabled && $paraments->automatic_connection_blocking->surcharge_enabled) ? '' : 'disabled']); ?>
                            </div> 

                            <div class="col-xl-6">
                                <?= $this->Form->input('automatic_connection_blocking.surcharge_concept', ['label' => 'Concepto', 'value' => $paraments->automatic_connection_blocking->surcharge_concept, 'disabled' => ($paraments->automatic_connection_blocking->enabled && $paraments->automatic_connection_blocking->surcharge_enabled) ? '' : 'disabled']); ?>
                            </div> 

                            <div class="col-xl-6">
                                <?= $this->Form->input('automatic_connection_blocking.surcharge_type', ['label' => 'Tipo', 'options' => ['next_month' => 'Deuda c/venc. mes próximo', 'this_month' => 'Nota de Débito p/mes en curso'],  'value' => $paraments->automatic_connection_blocking->surcharge_type, 'disabled' => ($paraments->automatic_connection_blocking->enabled && $paraments->automatic_connection_blocking->surcharge_enabled) ? '' : 'disabled']); ?>
                            </div>

                            <div class="col-xl-6">
                                <?= $this->Form->input('automatic_connection_blocking.surcharge_presupuesto', ['type' => 'checkbox', 'label' => 'Forzar presupuesto', 'checked' => $paraments->automatic_connection_blocking->surcharge_presupuesto, 'disabled' => ($paraments->automatic_connection_blocking->enabled && $paraments->automatic_connection_blocking->surcharge_enabled) ? '' : 'disabled']); ?>
                            </div>

                            <div class="col-xl-12">
                                <button type="button" id="btn-force-execution-automatic-blocking" class="btn btn-dark">Forzar Ejecución</button>
                            </div>

                        </div>

                    </div>
                </div>

            </div>

        </div>
    </fieldset>
    <?= $this->Html->link(__('Cancelar'), ["action" => "index"], ['class' => 'btn btn-default mt-3']) ?>
    <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success mt-3' ]) ?>
    <?= $this->Form->end() ?>

    <script type="text/javascript">

        $(document).ready(function () {

            var parament = <?= json_encode($paraments) ?>;

            var defaultDate = "2017-10-01T" + parament.automatic_connection_blocking.hours_execution;
            $('#automatic_connection_blocking-hours-execution-datetimepicker').datetimepicker({
                locale: 'es',
                defaultDate: defaultDate,
                format: 'HH:mm',
            });

            $('#automatic-connection-blocking-enabled').change(function() {

                if ($(this).is(":checked")) {

                    // config general
                    $("#automatic-connection-blocking-force-1").attr("disabled", false);
                    $("#automatic-connection-blocking-force-0").attr("disabled", false);

                    // control
                    if (parament.gral_config.billing_for_service) {
                        $("#automatic-connection-blocking-type").attr("disabled", false);
                    }

                    $("#automatic-connection-blocking-day").attr("disabled", false);
                    $("#automatic_connection_blocking-hours-execution").attr("disabled", false);
                    $("#automatic-connection-blocking-due-debt").attr("disabled", false);
                    $("#automatic-connection-blocking-invoice-no-paid").attr("disabled", false);

                    //Recargo por reconexión automático
                    $("#automatic-connection-blocking-surcharge-enabled").attr("disabled", false);
                    $("#automatic-connection-blocking-surcharge-value").attr("disabled", false);
                    $("#automatic-connection-blocking-surcharge-type").attr("disabled", false);
                    $("#automatic-connection-blocking-surcharge-presupuesto").attr("disabled", false);
                    $("#automatic-connection-blocking-surcharge-concept").attr("disabled", false);
                } else {

                    // config general
                    if (parament.gral_config.billing_for_service) {
                        $("#automatic-connection-blocking-force-1").attr("disabled", true);
                    }
                    $("#automatic-connection-blocking-force-0").attr("disabled", true);

                    // control
                    $("#automatic-connection-blocking-type").attr("disabled", true);
                    $("#automatic-connection-blocking-day").attr("disabled", true);
                    $("#automatic_connection_blocking-hours-execution").attr("disabled", true);
                    $("#automatic-connection-blocking-due-debt").attr("disabled", true);
                    $("#automatic-connection-blocking-invoice-no-paid").attr("disabled", true);

                    //Recargo por reconexión automático
                    $("#automatic-connection-blocking-surcharge-enabled").attr("disabled", true);
                    $("#automatic-connection-blocking-surcharge-value").attr("disabled", true);
                    $("#automatic-connection-blocking-surcharge-type").attr("disabled", true);
                    $("#automatic-connection-blocking-surcharge-presupuesto").attr("disabled", true);
                    $("#automatic-connection-blocking-surcharge-concept").attr("disabled", true);
                }
            });

            $('#automatic-connection-blocking-surcharge-enabled').change(function() {

                if ($(this).is(":checked")) {

                    //Recargo por reconexión automático
                    $("#automatic-connection-blocking-surcharge-value").attr("disabled", false);
                    $("#automatic-connection-blocking-surcharge-type").attr("disabled", false);
                    $("#automatic-connection-blocking-surcharge-presupuesto").attr("disabled", false);
                    $("#automatic-connection-blocking-surcharge-concept").attr("disabled", false);
                } else {

                    //Recargo por reconexión automático
                    $("#automatic-connection-blocking-surcharge-value").attr("disabled", true);
                    $("#automatic-connection-blocking-surcharge-type").attr("disabled", true);
                    $("#automatic-connection-blocking-surcharge-presupuesto").attr("disabled", true);
                    $("#automatic-connection-blocking-surcharge-concept").attr("disabled", true);
                }
            });

            $('#btn-force-execution-automatic-blocking').click(function() {

                var text = "¿Está Seguro que desea ejecutar?";

                bootbox.confirm(text, function(result) {
                    if (result) {
                        $('body')
                            .append( $('<form/>').attr({'action': '/ispbrain/Connections/forceExecution/', 'method': 'post', 'id': 'replacer-force-automtic-blocking'})
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                            .find('#replacer-force-automtic-blocking').submit();
                    }
                });
            });

        });

    </script>

<?php $this->end(); ?>
