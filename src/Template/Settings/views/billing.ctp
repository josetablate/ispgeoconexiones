<?php $this->extend('/Settings/views/tabs'); ?>

<?php $this->start('billing'); ?>
    <br>
    <?= $this->Form->create($paraments, ['class' => 'form-load', 'id' => 'form-billing'] ) ?>

    <fieldset>

        <?= $this->Form->hidden('form', ['value' => 'billing']); ?>

        <div class="row">
            <div class="col-xl-6">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <legend class="sub-title-sm">Datos:</legend>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <?php
                            $responsibles_billing = [];
                            $responsibles_billing[1] = $this->request->getSession()->read('afip_codes')['responsibles'][1];
                            $responsibles_billing[4] = $this->request->getSession()->read('afip_codes')['responsibles'][4];
                            $responsibles_billing[6] = $this->request->getSession()->read('afip_codes')['responsibles'][6];
                            echo $this->Form->input('billing.responsible', ['label' => 'Cond. frente al IVA', 'options' => $responsibles_billing, 'value' => $this->request->getSession()->read('paraments')->billing->responsible]);
                        ?>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <?php  echo $this->Form->input('billing.ident', ['label' => "CUIT", 'value' => $this->request->getSession()->read('paraments')->billing->ident ]); ?>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <?php
                            echo $this->Form->input('billing.razon_social', ['label' => 'Razón social', 'value' => $this->request->getSession()->read('paraments')->billing->razon_social]);
                        ?>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <?php
                            echo $this->Form->input('billing.province_billing', ['label' => 'Provincia', 'options' => $provinces_billing_setting, 'title' => 'Seleccionar Provinvia', 'class' => 'selectpicker', 'data-live-search' => "true", 'value' => $this->request->getSession()->read('paraments')->billing->province]);
                        ?>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <?php
                            echo $this->Form->input('billing.localidad_billing', ['id' => 'form-billing-localidad', 'label' => 'Localidad', 'value' => $this->request->getSession()->read('paraments')->billing->localidad]);
                        ?>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <?php
                            echo $this->Form->input('billing.address', ['label' => 'Domicilio', 'value' => $this->request->getSession()->read('paraments')->billing->address]);
                        ?>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <?php
                            echo $this->Form->input('billing.email', ['label' => 'Correo Electrónico', 'value' => $this->request->getSession()->read('paraments')->billing->email]);
                        ?>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <?php
                            echo $this->Form->input('billing.confirm', ['label' => 'Confirmado', 'type' => 'checkbox', 'checked' => $this->request->getSession()->read('paraments')->billing->confirm]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <?= $this->Html->link(__('Cancelar'), ["action" => "index"], ['class' => 'btn btn-default ']) ?>
    <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success' ]) ?>
    <?= $this->Form->end() ?>

<?php $this->end(); ?>
