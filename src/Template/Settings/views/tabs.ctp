<style type="text/css">

    legend {
        display: block;
        width: 100%;
        padding: 0;
        margin-bottom: 20px;
        font-size: 21px;
        line-height: inherit;
        color: #333;
        border: 0;
        border-bottom: 1px solid #e5e5e5;
    }

    .nav-link {
        cursor: pointer;
    }

    .table > tbody > tr > td.left {
        text-align: left;
    }

    tr.title{
        color: #696868 !important;
        font-weight: bold;
    }

    .btn-search {
        padding: 2px;
        margin: 5px;
    }

    textarea.title {
         height: 25px !important;
    }

    textarea.des {
         height: 100px !important;
    }

    #invoicing_color_hex_add {
        height: 35px;
    }

    #invoicing_color_hex_edit {
        height: 35px;
    }

</style>

<?php

    $accountsArray = [];
    foreach ($accounts as $account) {
        $accountsArray[$account->code] = $account->code . ' ' . $account->name;
    }

?>

<div class="row">
    <div class="col-md-12">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" id="myTab" role="tablist">

            <li class="nav-item">
                <a class="nav-link <?= $tab == 'bills' ? 'active' : '' ?>" id="bills-tab" data-target="#bills" role="tab" aria-controls="bills" aria-expanded="true">Facturación</a>
            </li>
            
            <li class="nav-item">
                <a class="nav-link <?= $tab == 'empresas' ? 'active' : '' ?>" id="empresas-tab" data-target="#empresas" role="tab" aria-controls="empresas" aria-expanded="true">Empresas</a>
            </li>

            <li class="nav-item">
                <a class="nav-link <?= $tab == 'duedates' ? 'active' : '' ?>" id="duedates-tab" data-target="#duedates" role="tab" aria-controls="duedates" aria-expanded="false">Vencimientos</a>
            </li>

            <?php if ($user_id == 100): ?>

            <li class="nav-item">
                <a class="nav-link <?= $tab == 'contable' ? 'active' : '' ?>" id="contable-tab" data-target="#contable" role="tab" aria-controls="contable" aria-expanded="false">Contable</a>
            </li>

            <?php endif; ?>

            <li class="nav-item">
                <a class="nav-link <?= $tab == 'system' ? 'active' : '' ?>" id="system-tab" data-target="#system" role="tab" aria-controls="system" aria-expanded="false">Sistema</a>
            </li>

            <?php if ($user_id == 100): ?>

            <li class="nav-item">
                <a class="nav-link <?= $tab == 'emails_accounts' ? 'active' : '' ?>" id="emails_accounts-tab" data-target="#emails_accounts" role="tab" aria-controls="emails_accounts" aria-expanded="false">Proveedores de Correos</a>
            </li>

            <li class="nav-item">
                <a class="nav-link <?= $tab == 'download_upload' ? 'active' : '' ?>" id="download_upload-tab" data-target="#download_upload" role="tab" aria-controls="download_upload" aria-expanded="false">Descargar/Subir</a>
            </li>

            <li class="nav-item">
                <a class="nav-link <?= $tab == 'payment_commitment' ? 'active' : '' ?>" id="payment_commitment-tab" data-target="#payment_commitment" role="tab" aria-controls="payment_commitment" aria-expanded="false">Compromiso de Pago</a>
            </li>

            <li class="nav-item">
                <a class="nav-link <?= $tab == 'gral_config' ? 'active' : '' ?>" id="gral_config-tab" data-target="#gral_config" role="tab" aria-controls="gral_config" aria-expanded="false">Configuración General</a>
            </li>

            <li class="nav-item">
                <a class="nav-link <?= $tab == 'telegram' ? 'active' : '' ?>" id="telegram-tab" data-target="#telegram" role="tab" aria-controls="telegram" aria-expanded="false">Telegram</a>
            </li>

            <li class="nav-item">
                <a class="nav-link <?= $tab == 'billing' ? 'active' : '' ?>" id="billing-tab" data-target="#billing" role="tab" aria-controls="billing" aria-expanded="false">Datos Facturación</a>
            </li>

            <?php endif; ?>

        </ul>

        <!-- Tab panes -->
        <div class="tab-content" id="myTabContent">

            <div class="tab-pane fade <?= $tab == 'bills' ? 'show active' : '' ?>" id="bills" role="tabpanel" aria-labelledby="bills-tab">
                <?= $this->fetch('bills') ?>
            </div>

            <div class="tab-pane fade <?= $tab == 'empresas' ? 'show active' : '' ?>" id="empresas" role="tabpanel" aria-labelledby="empresas-tab">
                <?= $this->fetch('empresas') ?>
            </div>

            <div class="tab-pane fade <?= $tab == 'duedates' ? 'show active' : '' ?>" id="duedates" role="tabpanel" aria-labelledby="duedates-tab">
                <?= $this->fetch('duedates') ?>
            </div>

            <?php if ($user_id == 100): ?>

            <div class="tab-pane fade <?= $tab == 'contable' ? 'show active' : '' ?>" id="contable" role="tabpanel" aria-labelledby="contable-tab">
                <?= $this->fetch('contable') ?>
            </div>

            <?php endif; ?>

            <div class="tab-pane fade <?= $tab == 'system' ? 'show active' : '' ?>" id="system" role="tabpanel" aria-labelledby="system-tab">
                <?= $this->fetch('system') ?>
            </div>

            <?php if ($user_id == 100): ?>

            <div class="tab-pane fade <?= $tab == 'emails_accounts' ? 'show active' : '' ?>" id="emails_accounts" role="tabpanel" aria-labelledby="emails_accounts-tab">
                <?= $this->fetch('emails_accounts') ?>
            </div>

            <div class="tab-pane fade <?= $tab == 'download_upload' ? 'show active' : '' ?>" id="download_upload" role="tabpanel" aria-labelledby="download_upload-tab">
                <?= $this->fetch('download_upload') ?>
            </div>

            <div class="tab-pane fade <?= $tab == 'payment_commitment' ? 'show active' : '' ?>" id="payment_commitment" role="tabpanel" aria-labelledby="payment_commitment-tab">
                <?= $this->fetch('payment_commitment') ?>
            </div>

            <div class="tab-pane fade <?= $tab == 'gral_config' ? 'show active' : '' ?>" id="gral_config" role="tabpanel" aria-labelledby="gral_config-tab">
                <?= $this->fetch('gral_config') ?>
            </div>

            <div class="tab-pane fade <?= $tab == 'telegram' ? 'show active' : '' ?>" id="telegram" role="tabpanel" aria-labelledby="telegram-tab">
                <?= $this->fetch('telegram') ?>
            </div>

            <div class="tab-pane fade <?= $tab == 'billing' ? 'show active' : '' ?>" id="billing" role="tabpanel" aria-labelledby="billing-tab">
                <?= $this->fetch('billing') ?>
            </div>

            <?php endif; ?>

        </div>

    </div>
</div>

<div class="modal fade" id="modal-accounts" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel">Seleccionar Cuenta</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">

                    <table class="table table-hover" id="table-accounts">
                        <thead>
                            <tr>
                                <th>Cód.</th>
                                <th>Nombre</th>
                                <th>Descripción</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            foreach ($accounts as $acc): ?>
                            <tr class="<?= ($acc->title) ? 'title' : '' ?> <?= (!$acc->status) ? ' font-through' : '' ?>"
                                data-code="<?= $acc->code ?>"
                                data-name="<?= $acc->name ?>"
                                >
                                <td class="left"><?= $acc->code ?></td>
                                <td class="left"><?php

                                    $hrchy = substr(strval($acc->code), 0, 4);
                                    $hrchy = str_split($hrchy);
                                    foreach ($hrchy as $h) {
                                        if ($h != '0') {
                                            echo '&nbsp;&nbsp;&nbsp;'; 
                                        }
                                    }
                                    if (!$acc->title) {
                                        echo '&nbsp;&nbsp;&nbsp;'; 
                                    }
                                    echo $acc->name; 
                                ?>
                                </td>
                                <td class="left"><?= $acc->description ?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <br>

                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" id="btn-account-selected" class="btn btn-success">Seleccionar</button> 
                </div>
            </div>
        </div>
    </div>
  </div>
</div>

<script type="text/javascript">

    var parament;

    function hexToRgb(hex) {
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }

    var table_acccounts = null;

    $(document).ready(function () {

        parament = <?= json_encode($paraments) ?>;

        $('#table-accounts').removeClass('display');

		table_acccounts = $('#table-accounts').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '480px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
		});

		$('#table-accounts_filter').closest('div').before($('#btns-tools').contents());   

		var input_select = null;

		$('.btn-search-account').click(function() {

		    input_select = $(this).data('input');
		    $('#modal-accounts').modal('show');
		});

		$('.btn-bin-account').click(function(){

		    var input_select_temp = $(this).data('input');
		    $('#' + input_select_temp).val('');
		    $('#' + input_select_temp + '-show').val('');
		});

		$('#btn-account-selected').click(function() {

		    var code = table_acccounts.$('tr.selected').data('code');
		    var name = table_acccounts.$('tr.selected').data('name');

		    $('#' + input_select).val(code);
		    $('#' + input_select + '-show').val(code + ' ' + name);

		    $('#modal-accounts').modal('hide');
		});

		$('#modal-accounts').on('shown.bs.modal', function (e) {
		    table_acccounts.draw();
        });

        $('#table-accounts tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                table_acccounts.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });

        $('#invoicing_color_hex_add').change(function() {
            var hex = $(this).val();
            var rgb = hexToRgb(hex);
            if (!rgb) {
                generateNoty('warning', 'No se pudo generar el RGB del color seleccionado.');
            }
            $('#invoicing_color_rgb_add').val(rgb.r + ',' + rgb.g + ',' + rgb.b);
        });

        $('#invoicing_color_hex_edit').change(function() {
            var hex = $(this).val();
            var rgb = hexToRgb(hex);
            if (!rgb) {
                generateNoty('warning', 'No se pudo generar el RGB del color seleccionado.');
            }
            $('#invoicing_color_rgb_edit').val(rgb.r + ',' + rgb.g + ',' + rgb.b);
        });

        $('#myTab a').click(function (e) {

            e.preventDefault();

            var target = $(this).data('target');
            $('#myTab a.active').removeClass('active');

            $('#myTabContent .active').fadeOut(100, function() {
            $('#myTabContent .active').removeClass('active');

                $(target).fadeIn(100, function() {
                   $(target).addClass('active show');
                   $(target + '-tab').addClass('active');
                   $(target + '-tab').trigger('shown.bs.tab');
                });
            });
        });

        var defaultDate = "2017-10-01T" + parament.backup.hours_execution;
        $('#backup-hours-execution-datetimepicker').datetimepicker({
            locale: 'es',
            defaultDate: defaultDate,
            format: 'HH:mm',
        });
    });

    document.getElementById("image-logo-add").onchange = function () {
        var reader = new FileReader();

        reader.onload = function (e) {
            // get loaded data and render thumbnail.
            document.getElementById("image-preview-add").src = e.target.result;
        };

        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    };

     document.getElementById("image-logo-edit").onchange = function () {
        var reader = new FileReader();

        reader.onload = function (e) {
            // get loaded data and render thumbnail.
            document.getElementById("image-preview-edit").src = e.target.result;
        };

        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    };

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
