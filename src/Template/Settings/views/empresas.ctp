<?php $this->extend('/Settings/views/duedates'); ?>

<?php $this->start('empresas'); ?>

<style type="text/css">

    /* .td-presupuesto, .td-denomination {
        padding-bottom: 8px;
        width: 200px;
    }

    .td-deuda {
        width: 200px;
    }

    #accountant-surcharges-enabled {
        margin-top: 10px;
    }

    #invoicing-is-presupuesto {
        margin-top: 20px;
    }

    #accountant-service-debt-proporcional {
        margin-top: 20px;
    }

    #accountant-service-debt-change {
        margin-top: 10px;
    }

    #customer-doc-validate {
        margin-top: 15px;
    }

    #customer-searching-code {
        margin-top: 15px;
    }

    #system-integration {
        margin-top: 15px;
    }

    #connection-credencials {
        margin-top: 15px;
    }

    #connection-accounting-traffic-enabled {
        margin-top: 15px;
    } */
/* 
    .sub-title-sm {
        margin-top: 15px;
    }

    .sub-title {
       margin-top: 15px;
    } */
/* 
    #portal-show-resume {
        margin-top: 15px;
    }

    #portal-show-receipt {
        margin-top: 15px;
        margin-bottom: 15px;
    } */

    /* #upload_parament {

        margin-bottom: 15px;
    }

    #table-business {
        width: 100% !important;
    } */

    /* .sub-titlex {
        color: #696868;
        font-size: 17px;
        font-weight: bold;
        margin-bottom: 10px;
    } */

</style>

<?php
    $responsiblesCustom = [ 1 => "IVA Responsable Inscripto", 6 => "Responsable Monotributo" ];
?>

<div id="btns-tools-business">
    <div class="text-right btns-tools margin-bottom-5" >

        <?php

            echo $this->Html->link(
                '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Agregar nueva empresa',
                'class' => 'btn btn-default btn-add-business',
                'escape' => false
            ]);
        ?>

    </div>
</div>

<div class="modal fade" id="modal-add-business" tabindex="-1" role="dialog" aria-labelledby="label-modal-add">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Agregar Empresa</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <form id="add-enterprise-form" method="post" accept-charset="utf-8" enctype="multipart/form-data" role="form" action="<?= $this->Url->build(["controller" => "Settings", "action" => "addBusiness"]) ?>">
                    <div style="display:none;">
                        <input type="hidden" name="_method" value="POST">
                    </div>

                    <div class="row">
                        <div class="col-xl-4">
                            <?php
                                echo $this->Form->input('name', ['label' => 'Razón social *', 'type' => 'text', 'required' => true, 'maxlength' => '120' ]);
                                echo $this->Form->input('fantasy_name', ['label' => 'Nombre Fantasía', 'type' => 'text' ]);
                                echo $this->Form->input('phone', ['label' => 'Teléfono', 'maxlength' => '45']);
                                echo $this->Form->input('pto_vta', ['label' => 'Punto de venta *', 'type' => 'number', 'required' => true ]);
                                echo $this->Form->input('cuit', ['label' => 'CUIT *', 'type' => 'text', 'required' => true, 'maxlength' => '45' ]);
                                echo $this->Form->input('ing_bruto', ['label' => 'Ing. Bruto *', 'type' => 'text', 'required' => true, 'maxlength' => '45' ]);
                                echo $this->Form->input('email', ['label' => 'Correo Electrónico', 'type' => 'email', 'maxlength' => '255' ]);
                            ?>
                        </div>

                        <div class="col-xl-4">
                            <?php
                                echo $this->Form->input('responsible', ['label' => 'Tipo *', 'options' => $responsiblesCustom, 'required' => true ]);
                                echo $this->Form->input('address', ['label' => 'Domicilio *', 'type' => 'text', 'required' => true, 'maxlength' => '200' ]);
                                echo $this->Form->input('city', ['label' => 'Ciudad *', 'type' => 'text', 'required' => true, 'maxlength' => '145' ]);
                                echo $this->Form->input('cp', ['label' => 'CP *', 'type' => 'text', 'required' => true, 'maxlength' => '5' ]);
                                echo $this->Form->input('init_act', ['label' => 'Inicio de Act. *', 'type' => 'text', 'required' => true, 'maxlength' => '45' ]);
                                echo $this->Form->input('web', ['label' => 'Sitio web', 'type' => 'text' ]);
                            ?>
                        </div>

                        <div class="col-xl-4">
                            <h5 class="w-100 border-bottom">Estilo de PDF</h5>
                            <?php
                                echo $this->Form->input('direccion_comercio_interior', ['label' => 'Dirección de Comercio Interior', 'value' => '"DIRECCIÓN DE COMERCIO INTERIOR 0800-444-3346"', 'title' => '"DIRECCIÓN DE COMERCIO INTERIOR 0800-444-3346"', 'data-toggle' => 'tooltip', 'type' => 'text', 'required' => FALSE ]);
                                echo $this->Form->input('copias', ['options' => [1 => 'Solo Original', 2 => 'Duplicado', 3 => 'Triplicado'], 'class' => 'mb-1']);
                                echo $this->Form->input('hex', ['type' => 'color', 'id' => 'invoicing_color_hex_add', 'label' => 'Color', 'class' => 'mb-1']);
                                echo $this->Form->hidden('rgb', ['type' => 'color', 'id' => 'invoicing_color_rgb_add', 'class' => 'mb-1'])
                            ?>
                            <label  title="Click para cambiar de imagen." style="padding-left: 10px; margin-top: 3px; ">
                                <img src="" id="image-preview-add" style="max-width: 220px;"></img>
                                <br> Buscar Imagen <input name="logo" type="file" style="display: none;" id="image-logo-add">
                            </label>
                        </div>

                        <?php echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]); ?>

                        <div class="col-xl-12 mt-1">
                            <input type="hidden" name="id" id="id"/>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-success" id="btn-confirm-edit">Agregar</button> 
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-edit-business" tabindex="-1" role="dialog" aria-labelledby="label-modal-add">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Editar Empresa</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <form id="edit-enterprise-form" method="post" accept-charset="utf-8" enctype="multipart/form-data"  role="form" action="<?= $this->Url->build(["controller" => "Settings", "action" => "editBusiness"]) ?>">
                    <div style="display:none;">
                        <input type="hidden" name="_method" value="POST">
                    </div>

                    <div class="row">
                        <div class="col-xl-4">
                            <?php
                                echo $this->Form->input('name', ['label' => 'Razón social *', 'type' => 'text', 'required' => true, 'maxlength' => '120' ]);
                                echo $this->Form->input('fantasy_name', ['label' => 'Nombre Fantasía', 'type' => 'text' ]);
                                echo $this->Form->input('phone', ['label' => 'Teléfono', 'maxlength' => '45']);
                                echo $this->Form->input('pto_vta', ['label' => 'Punto de venta *', 'type' => 'number', "max" => "999999999", 'required' => true ]);
                                echo $this->Form->input('cuit', ['label' => 'CUIT *', 'type' => 'text', 'required' => true, 'maxlength' => '45' ]);
                                echo $this->Form->input('ing_bruto', ['label' => 'Ing. Bruto *', 'type' => 'text', 'required' => true, 'maxlength' => '45' ]);
                                echo $this->Form->input('init_act', ['label' => 'Inicio de Act. *', 'type' => 'text', 'required' => true, 'maxlength' => '45']);
                                echo $this->Form->input('email', ['label' => 'Correo Electrónico', 'type' => 'email', 'maxlength' => '255']);
                                echo $this->Form->input('web', ['label' => 'Sitio web', 'type' => 'text']);
                            ?>
                        </div>

                        <div class="col-xl-4">
                            <?php
                                echo $this->Form->input('responsible', ['label' => 'Tipo *', 'options' => $responsiblesCustom, 'required' => true ]);
                                echo $this->Form->input('enable', ['label' => 'Habilitado', 'type' => 'checkbox', 'id' => 'enable' ]);
                                echo $this->Form->input('address', ['label' => 'Domicilio *', 'type' => 'text', 'required' => true, 'maxlength' => '200' ]);
                                echo $this->Form->input('city', ['label' => 'Ciudad *', 'type' => 'text', 'required' => true, 'maxlength' => '145' ]);
                                echo $this->Form->input('cp', ['label' => 'CP *', 'type' => 'text', 'required' => true, 'maxlength' => '5' ]);
                            ?>
                            <h5 class="w-100 border-bottom">Estilo de PDF</h5>
                            <?php
                                echo $this->Form->input('direccion_comercio_interior', ['label' => 'Dirección de Comercio Interior', 'data-toggle' => 'tooltip', 'type' => 'text', 'required' => FALSE ]);
                                echo $this->Form->input('copias', ['options' => [1 => 'Solo Original', 2 => 'Duplicado', 3 => 'Triplicado'], 'class' => 'mb-1']);
                                echo $this->Form->input('hex', ['type' => 'color', 'id' => 'invoicing_color_hex_edit', 'label' => 'color', 'class' => 'mb-1']);
                                echo $this->Form->hidden('rgb', ['type' => 'Color', 'id' => 'invoicing_color_rgb_edit', 'class' => 'mb-1'])
                            ?>
                            <label  title="Click para cambiar de imagen." style="padding-left: 10px; margin-top: 3px; ">
                                <img src="" id="image-preview-edit" style="max-width: 220px;"></img>
                                <br> Buscar Imagen <input name="logo" type="file" style="display: none;" id="image-logo-edit">
                            </label>

                        </div>

                        <div class="col-xl-4">

                            <label for="">Integración AFIP</label>

                            <div class="card">
                                <div class="card-body">
                                    <?php
                                        echo $this->Form->input('testing', ['label' => 'Testing', 'type' => 'checkbox', 'id' => 'testing' ]);
                                        echo $this->Form->input('log', ['label' => 'Log AFIP', 'type' => 'checkbox' , 'id' => 'log' ]);
                                        echo $this->Form->input('alias', ['label' => 'Alias', 'type' => 'text', 'required' => false, 'id' => 'alias' ]);
                                        echo $this->Form->input('id',['type' => 'hidden']);
                                    ?>

                                    <a href="/ispbrain/settings/generateCSR" class="button" target="_self" id="link-generate-csr">Generar y descargar CSR</a>

                                    <br>
                                    <br>
                                    <label  title="Click para subir certificado.">
                                        <i class="fas fa-upload fa-2x mr-2"></i>Subir CRT
                                        <input name="crt" type="file" style="display: none;"> 
                                    </label>

                                    <br>

                                    <?php echo $this->Html->link('<i class="fas fa-download fa-2x"></i> Descargar CRT','/ispbrain/settings/downloadCRT', ['class' => 'button', 'target' => '_self', 'escape' => false, 'id' => 'link-downlod-crt']); ?>

                                    <br>

                                    <br>
                                    <?php
                                         echo $this->Html->link(
                                            'Probar login AFIP',
                                            '#',
                                            [
                                                'id' => 'btn-testLoginAFIP',
                                            'title' => '',
                                            'class' => 'btn btn-default ',
                                            'escape' => false
                                        ]);

                                    ?>

                                </div>
                            </div>

                        </div>

                        <?php echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]); ?>

                        <div class="col-xl-12 mt-1">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-success" id="btn-confirm-edit">Guardar</button> 
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered w-100" id="table-business">
            <thead>
                <tr>
                    <th></th>
                    <th>ID</th>
                    <th>Tipo</th>
                    <th>Razon social</th>
                    <th>Nombre Fantasia</th>
                    <th>Domicilio</th>
                    <th>Pto. Vta</th>
                    <th>CUIT</th>
                    <th>CRT</th>
                    <th>Habilitado</th>
                </tr>
            <thead>
            <tbody>
            </tbody>
        </table>             
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-edit-business',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2 mr-2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-enable-business',
        'name' =>  'Habilitar',
        'icon' =>  'far fa-check-square mr-2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-disable-business',
        'name' =>  'Desabilitar',
        'icon' =>  'far fa-square mr-2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete-business',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin mr-2',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-business', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<script type="text/javascript">

    var table_business = null;
    var business_selected = null;

    $('[data-toggle="tooltip"]').tooltip();

    $(document).ready(function() {

        $('#btns-tools').hide();

        table_business = $('#table-business').DataTable({
		    "order": [[ 1, 'desc' ]],
		    "autoWidth": true,
		    "scrollY": '250px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "sWrapper":  "dataTables_wrapper dt-bootstrap4",
            "deferRender": true,
            "searching": false,
            "paging": false,
            "info": false,
            "ordering": false,
		    "ajax": {
                "url": "/ispbrain/settings/get_business.json",
                "dataSrc": "business",
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href  = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "columns": [
                { 
                    "data": "logo",
                    "width":"36px",
                    "render": function ( data, type, row ) {

                        if (data && type === 'display') {
                            return "<img src='/ispbrain/img/"+data+"' style='max-width: 50px;'></img>";
                        } 
                        return '';
                    }
                },
                { 
                    "data": "id",
                },
                { 
                    "data": "responsible",
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.responsibles[data];
                    }
                },
                { 
                    "data": "name",                    
                },
                { 
                    "data": "fantasy_name",                    
                },
                { 
                    "data": "address"                    
                },
                { 
                    "data": "pto_vta"                    
                },
                { 
                    "data": "cuit",
                },
                { 
                    "data": "webservice_afip.crt",
                    "render": function ( data, type, row ) {
                        return data != '' ? 'SI' : 'NO'
                    }
                },
                { 
                    "data": "enable",
                    "render": function ( data, type, row ) {
                        return data  ? 'SI' : 'NO'
                    }
                },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
		    "language": dataTable_lenguage,
            "lengthMenu": [[50, 100, -1], [50, 100, "Todas"]],
            "dom":
    	    	"<'row'<'col-xl-6 title'><'col-xl-6 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
		});

		$('#table-business_wrapper .title').append('<h5>Empresas</h5>');
		$('#table-business_wrapper .tools').append($('#btns-tools-business').contents());

        $('#btn-enable-business').click(function() {       

            $.ajax({
                url: "<?= $this->Url->build(['controller' => 'Settings', 'action' => 'enableBusiness']) ?>",
                type: 'POST',
                dataType: "json",    
                data: JSON.stringify({busines_id: business_selected.id }),
                success: function(data) {
                    $('.modal-business').modal('hide');
                    table_business.ajax.reload();
                    console.log('enableBusiness reload')
                },
                error: function (jqXHR) {

                    $('.modal-business').modal('hide');

                    if (jqXHR.status == 403) {

                        generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                        setTimeout(function() { 
                            window.location.href = "/ispbrain";
                        }, 3000);

                    } else {
                        generateNoty('error', 'Error al intentar obtener las Deudas sin Facturar del Cliente.');
                    }
                }
            });

        })

        $('#btn-disable-business').click(function() {       

            $.ajax({
                url: "<?= $this->Url->build(['controller' => 'Settings', 'action' => 'disableBusiness']) ?>",
                type: 'POST',
                dataType: "json",  
                data: JSON.stringify({busines_id: business_selected.id }),  
                success: function(data) {
                    $('.modal-business').modal('hide');
                    table_business.ajax.reload();
                    console.log('disableBusiness reload')
                },
                error: function (jqXHR) {

                    $('.modal-business').modal('hide');

                    if (jqXHR.status == 403) {

                        generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                        setTimeout(function() { 
                            window.location.href = "/ispbrain";
                        }, 3000);

                    } else {
                        generateNoty('error', 'Error al intentar obtener las Deudas sin Facturar del Cliente.');
                    }
                }
            });

        })

    });

    $('.btn-add-business').click(function() {
        $('#modal-add-business').modal('show');
    });

    var row_id = null;

    $('#btn-edit-business').click(function() {

        $('#modal-edit-business #id').val(business_selected.id);
        $('#modal-edit-business #responsible').val(business_selected.responsible);
        $('#modal-edit-business #enable').attr('checked', business_selected.enable);
        $('#modal-edit-business #pto-vta').val(business_selected.pto_vta);
        $('#modal-edit-business #cuit').val(business_selected.cuit);
        $('#modal-edit-business #init-act').val(business_selected.init_act);
        $('#modal-edit-business #ing-bruto').val(business_selected.ing_bruto);
        $('#modal-edit-business #name').val(business_selected.name);
        $('#modal-edit-business #fantasy-name').val(business_selected.fantasy_name);
        $('#modal-edit-business #address').val(business_selected.address);
        $('#modal-edit-business #city').val(business_selected.city);
        $('#modal-edit-business #cp').val(business_selected.cp);
        $('#modal-edit-business #email').val(business_selected.email);
        $('#modal-edit-business #web').val(business_selected.web);

        $('#modal-edit-business #phone').val(business_selected.phone);
        $('#modal-edit-business #direccion-comercio-interior').val(business_selected.direccion_comercio_interior);
        $('#modal-edit-business #direccion-comercio-interior').attr('title', business_selected.direccion_comercio_interior);
        $('#modal-edit-business #invoicing_color_hex_edit').val(business_selected.color.hex);
        $('#modal-edit-business #invoicing_color_rgb_edit').val(business_selected.color.rgb);
        $('#modal-edit-business #copias').val(business_selected.copias);

        if (business_selected.logo) {
            $('#image-preview-edit').attr('src', '/ispbrain/img/' + business_selected.logo);
        }

        $('#modal-edit-business #integration-afip').attr('checked', business_selected.integration_afip);
        $('#modal-edit-business #testing').attr('checked', business_selected.webservice_afip.testing);
        $('#modal-edit-business #log').attr('checked', business_selected.webservice_afip.log);
        $('#modal-edit-business #alias').val(business_selected.webservice_afip.alias);

        $('#link-generate-csr').attr('href', '/ispbrain/settings/generateCSR/' + business_selected.id);
        $('#link-downlod-crt').attr('href', '/ispbrain/settings/downloadCRT/' + business_selected.id);

        $('.modal-business').modal('hide');
        $('#modal-edit-business').modal('show');
    });

    $('#table-business tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_business.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            business_selected = table_business.row( this ).data();

            if(business_selected.enable){
                $('#btn-enable-business').hide();
                $('#btn-disable-business').show();
            }else{
                $('#btn-enable-business').show();
                $('#btn-disable-business').hide();
            }

            row_id = $(this).attr('id');

            $('.modal-business').modal('show');
        }
    });
    
    $('#btn-testLoginAFIP').click(function() {

        if (business_selected) {

            $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/settings/testLoginAFIP/', 'method': 'post', 'id': 'replacer1'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': business_selected.id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#replacer1').submit();
           
        }
    });

    $('#btn-delete-business').click(function() {

        if (business_selected) {

            var text = '¿Está Seguro que desea eliminar la empresa ' + business_selected.name + '?';
            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/settings/deleteBusiness/', 'method': 'post', 'id': 'replacer'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': business_selected.id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer').submit();
                }
            });
        }
    });

    $('#add-enterprise-form').submit(function (e) {
        if ($('#image-preview-add').attr('src') == "") {
            e.preventDefault();
            generateNoty('warning', 'Debe agregar una imagen.');
        }
    });

    $('#edit-enterprise-form').submit(function (e) {
        if ($('#image-preview-edit').attr('src') == "") {
            e.preventDefault();
            generateNoty('warning', 'Debe agregar una imagen.');
        }
    });

    

    
   

    $('a[id="empresas-tab"]').on('shown.bs.tab', function (e) {
        table_business.draw();
    });


</script>

<?php $this->end(); ?>
