<?php 

    if ($user_id == 100) {
        $this->extend('/Settings/views/emails_accounts'); 
    } else {
        $this->extend('/Settings/views/tabs'); 
    }

?>

<?php $this->start('system'); ?>
    <br>
    <?= $this->Form->create($paraments,  ['class' => 'form-load']) ?>
    <fieldset>

        <?= $this->Form->hidden('form', ['value' => 'system']); ?>

        <div class="row">

            <div class="col-md-2">

                <legend class="sub-title-sm">Mapas</legend>
                <?= $this->Form->input('system.map.lat', ['label' => 'Latitud', 'type' => 'number', 'step' => 'any', 'value' =>$paraments->system->map->lat]) ?>
                <?= $this->Form->input('system.map.lng', ['label' => 'Longitud', 'type' => 'number', 'step' => 'any', 'value' =>$paraments->system->map->lng]) ?>
                <?= $this->Form->input('system.map.zoom', ['label' => 'Zoom', 'type' => 'number', 'step' => 'any', 'value' =>$paraments->system->map->zoom]) ?>

                <legend class="sub-title-sm">Servidor</legend>
                <?= $this->Form->input('system.server.ip', ['label' => 'Server IP', 'value' => $paraments->system->server->ip]) ?>
                <?= $this->Form->input('system.uri_http', ['label' => 'uri http avisos', 'value' => $paraments->system->uri_http]); ?>
                <?= $this->Form->input('session_timeout', ['label' => 'Tiempo de Sesión (minutos)', 'type' => 'number', 'min'=> '1', 'step' => '1', 'value' => $session_timeout]); ?>

            </div>

            <div class="col-md-3">

                <legend class="sub-title-sm">Clientes</legend>
                <?= $this->Form->input('customer.doc_validate', ['label' => 'Validación por DNI', 'type' => 'checkbox',  'checked' => $paraments->customer->doc_validate]) ?>
                <?= $this->Form->input('customer.search_list_customers', ['label' => 'Búsqueda de cliente (tabla)', 'type' => 'checkbox',  'checked' => $paraments->customer->search_list_customers]) ?>
                <?= $this->Form->input('customer.email_validate', ['label' => 'Validar Correo Electónico', 'type' => 'checkbox',  'checked' => $paraments->customer->email_validate]) ?>
                <?php
                    echo $this->Form->input('customer.country_default', ['label' => 'País (por defecto)', 'options' => [], 'title' => 'Seleccionar País', 'class' => 'selectpicker', 'data-live-search' => "true"]);
                    echo $this->Form->input('customer.province_default', ['label' => 'Provincia (por defecto)', 'options' => [], 'title' => 'Seleccionar Provinvia', 'class' => 'selectpicker', 'data-live-search' => "true"]);
                    echo $this->Form->input('customer.city_default', ['label' => 'Ciudad (por defecto)', 'options' => [], 'title' => 'Seleccionar Ciudad', 'class' => 'selectpicker', 'data-live-search' => "true"]);
                ?>

                <legend class="sub-title">Conexiones</legend>

                <legend class="sub-title-sm">Credenciales PPPoE Automático  </legend>
                <?= $this->Form->input('connection.pppoe_credential', ['label' => 'Habilitado', 'type' => 'checkbox', 'checked' => $paraments->connection->pppoe_credential]) ?>

                <?php if ($user_id == 100): ?>
                <legend class="sub-title-sm">Relación de redes con servicios </legend>
                <?= $this->Form->input('connection.pool_plan', ['label' => 'Habilitado', 'type' => 'checkbox', 'checked' => $paraments->connection->pool_plan]) ?>
                <?php endif; ?>

            </div>

            <div class="col-xl-3">

                <legend class="sub-title-sm">Mostrar contador de errores</legend>
                <?= $this->Form->input('system.show_error_log', ['label' => 'Habilitado', 'type' => 'checkbox', 'checked' => $paraments->system->show_error_log]) ?>

            </div>

            <div class="col-xl-3">
                <legend class="sub-title-sm">Acciones</legend>
                <button type="button" id="force-debt-month-control" class="btn btn-primary mt-3">Forzar recalculo de saldo</button>
            </div>

        </div>
    </fieldset>
    <?= $this->Html->link(__('Cancelar'),[ "action" => "index"], ['class' => 'btn btn-default ']) ?>
    <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success' ]) ?>
    <?= $this->Form->end() ?>

    <script type="text/javascript">

        var paraments = null;
        var countries = null;

        var country_selected = null;
        var province_selected = null;
        var city_selected = null;

        $(document).ready(function() {

            paraments = <?= json_encode($paraments) ?>;

            //selects de pais, provincia, ciudad

            countries = <?= json_encode($countries) ?>;

            $.each(countries, function(i, country) {
                $('#customer-country-default').append("<option value=" + country.id + ">" + country.name + "</option>");
            });

            $('#customer-country-default').change(function() {

                $('#customer-province-default').empty();
                $('#customer-city-default').empty();

                var pre_selected = null;

                var country_id = $(this).val();

                $.each(countries, function(i, country) {

                    if (country.id == country_id) {

                        country_selected = country;

                        $.each(country_selected.provinces, function(i, province) {

                            $('#customer-province-default').append("<option value=" + province.id + ">" + province.name+"</option>");

                            if (province.id == paraments.customer.province_default) {
                                pre_selected = province.id;
                            }
                        });
                    }
                });

                $('#customer-province-default').selectpicker('refresh');
                $('#customer-city-default').selectpicker('refresh');

                if (pre_selected) {
                    $('#customer-province-default').selectpicker('val', pre_selected);
                    $('#customer-province-default').change();
                }

                $('#customer-province-default').change();

            });

            $('#customer-province-default').change(function() {

                $('#customer-city-default').empty();

                var pre_selected = null;

                var province_id = $(this).val();

                $.each(country_selected.provinces, function(i, province) {

                    if (province.id == province_id) {

                        province_selected = province;

                        $.each(province_selected.cities, function(i, city) {
                            $('#customer-city-default').append("<option value=" + city.id + ">" + city.name + "</option>");

                            if (city.id == paraments.customer.city_default) {
                                pre_selected = city.id;
                            }
                        });
                    }
                });

                $('#customer-city-default').selectpicker('refresh');

                if (pre_selected) {
                    $('#customer-city-default').selectpicker('val', pre_selected);
                    $('#customer-city-default').change();
                }
            });

            $('#customer-country-default').selectpicker('val', paraments.customer.country_default);
            $('#customer-country-default').selectpicker('refresh');
            $('#customer-country-default').change();

            $('#force-debt-month-control').click(function() {

                var text = '¿Está Seguro que desea forzar el recalculo de saldo?';
                var controller = 'Settings';

                bootbox.confirm(text, function(result) {
                    if (result) {
                        $('body')
                            .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/forceDebtMonthControl/', 'method': 'post', 'id': 'replacer-force-debt-month-control'})
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                            .find('#replacer-force-debt-month-control').submit();
                    }
                });
            });
        });

    </script>
<?php $this->end(); ?>
