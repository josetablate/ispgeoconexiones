<?php $this->extend('/Settings/views/gral_config'); ?>

<?php $this->start('payment_commitment'); ?>
    <br>
    <?= $this->Form->create($paraments, ['class' => 'form-load', 'id' => 'form-payment_commitment'] ) ?>

    <fieldset>

        <?= $this->Form->hidden('form', ['value' => 'payment_commitment']); ?>

        <div class="row">
            <div class="col-xl-3">
                <legend class="sub-title-sm">Configuración</legend>
                <div class="form-group required">
                    <label class="control-label" for="payment_commitment">Ejecución para hacer Control de Pagos</label>
                    <div class='input-group date' id='payment_commitment-datetimepicker'>
                        <span class="input-group-addon input-group-text mr-0">Horario</span>
                        <input  name="customer[payment_commitment]" required="required" id="customer.payment_commitment"  type='text' class="form-control" />
                        <span class="input-group-addon input-group-text mr-0">
                            <span class="glyphicon icon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-xl-3">
                <legend class="sub-title-sm">Forzar Ejecución</legend>
                <div class='input-group date' id='date-execution-datetimepicker'>
                    <span class="input-group-addon input-group-text mr-0">Fecha</span>
                    <input  name="date_execution" required="required" id="date-execution"  type='text' class="form-control" />
                    <span class="input-group-addon input-group-text mr-0">
                        <span class="glyphicon icon-calendar"></span>
                    </span>
                </div>
                <button type="button" id="force-execution" class="btn btn-primary mt-3">Forzar</button>
            </div>
        </div>
    </fieldset>
    <?= $this->Html->link(__('Cancelar'),["action" => "index"], ['class' => 'btn btn-default ']) ?>
    <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success' ]) ?>
    <?= $this->Form->end() ?>

<script type="text/javascript">

    var parament;

    $(document).ready(function () {

        paraments = <?= json_encode($paraments) ?>;
        var year = new Date().getFullYear(); 
        var defaultDate = year + "-10-01T" + paraments.customer.payment_commitment;
        $('#payment_commitment-datetimepicker').datetimepicker({
            locale: 'es',
            defaultDate: defaultDate,
            format: 'HH:mm',
        });

        $('#date-execution-datetimepicker').datetimepicker({
            locale: 'es',
            defaultDate: new Date(),
            format: 'DD/MM/YYYY'
        });

        $('#force-execution').click(function() {

            var text = "¿Está Seguro que desea ejecutar?";
            var date =  $("#date-execution-datetimepicker").find("input").val();

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/PaymentCommitment/forcePaymentCommitmentControl/', 'method': 'post', 'id': 'replacer'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'date', 'value': date}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer').submit();
                }
            });
        });
    });

</script>

<?php $this->end(); ?>
