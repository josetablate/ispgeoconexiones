<?php $this->extend('/Settings/views/payment_commitment'); ?>

<?php $this->start('download_upload'); ?>
    <br>
    <div class="row">

        <div class="col-md-4">

            <legend>Paraments General</legend>

            <?= $this->Form->create($paraments, ['enctype' => "multipart/form-data",  'class' => 'form-load', 'id' => 'form-download_upload'] ) ?>

            <fieldset>

                <?= $this->Form->hidden('form', ['value' => 'download_upload']); ?>

                <div class="row">
                    <div class="col-md-4">

                        <legend class="sub-title-sm">Descargar Archivo de Configuración</legend>
                        <?=$this->Html->link('Descargar', '/Settings/download', ['class' => 'btn btn-default', 'target' => '_self']) ?> 
                        <br>
                        <br>

                        <legend class="sub-title-sm">Subir Archivo de Configuración</legend>
                        <input name="upload_parament" type="file" id="upload_parament">
                        <br>

                    </div>
                </div>
            </fieldset>
            <?= $this->Html->link(__('Cancelar'),["action" => "index"], ['class' => 'btn btn-default ']) ?>
            <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success' ]) ?>
            <?= $this->Form->end() ?>
        </div>

        <div class="col-md-4">

            <legend>Pasarela de Pago</legend>

            <?= $this->Form->create($payment_getway, ['enctype' => "multipart/form-data",  'class' => 'form-load', 'id' => 'form-download_upload_payment_getway'] ) ?>
        
            <fieldset>
        
                <?= $this->Form->hidden('form', ['value' => 'download_upload']); ?>

                <div class="row">
                    <div class="col-md-4">

                        <legend class="sub-title-sm">Descargar Archivo de Configuración</legend>
                        <?=$this->Html->link('Descargar', '/Settings/download_payment_getway', ['class' => 'btn btn-default', 'target' => '_self']) ?> 
                        <br>
                        <br>

                        <legend class="sub-title-sm">Subir Archivo de Configuración</legend>
                        <input name="upload_payment_getway" type="file" id="upload_payment_getway">
                        <br>

                    </div>
                </div>
            </fieldset>
            <?= $this->Html->link(__('Cancelar'),["action" => "index"], ['class' => 'btn btn-default mt-3']) ?>
            <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success mt-3' ]) ?>
            <?= $this->Form->end() ?>
        </div>

      
    </div>
<?php $this->end(); ?>
