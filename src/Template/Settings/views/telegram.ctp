<?php $this->extend('/Settings/views/billing'); ?>

<?php $this->start('telegram'); ?>
    <br>
    <?= $this->Form->create($paraments, ['class' => 'form-load', 'id' => 'form-telegram'] ) ?>

    <fieldset>

        <?= $this->Form->hidden('form', ['value' => 'telegram']); ?>

        <div class="row">
            <div class="col-xl-3">
                <legend class="sub-title-sm">Configuración</legend>
                <?php
                    echo $this->Form->input('telegram.token', ['label' => 'TOKEN', 'type' => 'text', 'value' => $this->request->getSession()->read('paraments')->telegram->token]);
                    echo $this->Form->input('telegram.chat_id', ['label' => 'CHAT ID', 'type' => 'text', 'value' => $this->request->getSession()->read('paraments')->telegram->chat_id]);
                    echo $this->Form->input('telegram.url', ['label' => 'URL', 'type' => 'text', 'value' => $this->request->getSession()->read('paraments')->telegram->url]);
                ?>
            </div>
        </div>
    </fieldset>
    <?= $this->Html->link(__('Cancelar'), ["action" => "index"], ['class' => 'btn btn-default ']) ?>
    <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success' ]) ?>
    <?= $this->Form->end() ?>

<?php $this->end(); ?>
