<?php $this->extend('/Settings/views/telegram'); ?>

<?php $this->start('gral_config'); ?>

    <?php

        $buttons = [];

        $buttons[] = [
            'id'   => 'btn-enable',
            'name' => 'Habilitar',
            'icon' => 'fas fa-check',
            'type' => 'btn-success'
        ];

        $buttons[] = [
            'id'   => 'btn-disable',
            'name' => 'Deshabilitar',
            'icon' => 'fas fa-times',
            'type' => 'btn-danger'
        ];

        $buttons[] = [
            'id'   => 'btn-edit',
            'name' => 'Editar',
            'icon' => 'icon-pencil2',
            'type' => 'btn-dark'
        ];

        echo $this->element('actions', ['modal'=> 'modal-crons', 'title' => 'Acciones', 'buttons' => $buttons ]);
    ?>

    <div class="modal fade" id="modal-job-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edición Tarea: <span id="job-title"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <?= $this->Form->create(null, [
                    'url' => [
                        'controller' => 'Settings',
                        'action' => 'editJob'
                    ]
                ]); ?>
                <div class="modal-body">

                        <div class="row">

                            <div class="col-md-12">
                                <?= $this->Form->input('job', ['label' => __('Tarea'), 'value' => '']) ?>
                                <?= $this->Form->hidden('job_old', ['id' => 'job-old', 'value' => '']); ?>
                            </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-secondary btn-edit-job" >Guardar</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                </div>

                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>

    <br>
    <?= $this->Form->create($paraments, ['class' => 'form-load', 'id' => 'form-gral_config'] ) ?>

    <fieldset>

        <?= $this->Form->hidden('form', ['value' => 'gral_config']); ?>

        <div class="row">
            <div class="col-xl-3">
                <legend class="sub-title-sm">Clientes</legend>
                <?php
                    echo $this->Form->input('gral_config.billing_for_service', ['label' => 'Facturación por Servicio', 'type' => 'checkbox', 'checked' => $this->request->getSession()->read('paraments')->gral_config->billing_for_service]);
                    echo $this->Form->input('gral_config.value_service_customer', ['label' => 'Valor de servicio por cliente', 'type' => 'checkbox', 'checked' => $this->request->getSession()->read('paraments')->gral_config->value_service_customer]);
                ?>
            </div>
            <div class="col-xl-3">
                <legend class="sub-title-sm">Privilegios</legend>
                <?php
                    echo $this->Form->input('gral_config.using_privilegios', ['label' => 'Usar', 'type' => 'checkbox', 'checked' => $this->request->getSession()->read('paraments')->gral_config->using_privilegios]);
                ?>
            </div>
            <div class="col-xl-3">
                <legend class="sub-title-sm">Avisos</legend>
                <?php
                    echo $this->Form->input('gral_config.using_avisos', ['label' => 'Usar', 'type' => 'checkbox', 'checked' => $this->request->getSession()->read('paraments')->gral_config->using_avisos]);
                ?>
            </div>
            <div class="col-xl-3">
                <legend class="sub-title-sm">Empresa</legend>
                <?php
                    echo $this->Form->input('gral_config.enterprise_name', ['label' => 'Nombre', 'type' => 'text', 'value' => $this->request->getSession()->read('paraments')->gral_config->enterprise_name]);
                ?>
            </div>
            <div class="col-xl-3">
                <legend class="sub-title-sm">Chat</legend>
                <?php
                    echo $this->Form->input('gral_config.using_chat', ['label' => 'Usar', 'type' => 'checkbox', 'checked' => $this->request->getSession()->read('paraments')->gral_config->using_chat]);
                    echo $this->Form->input('gral_config.telegram_group_link', ['label' => 'Link Grupo Telegram', 'type' => 'text', 'value' => $this->request->getSession()->read('paraments')->gral_config->telegram_group_link]);
                ?>
            </div>
            <div class="col-xl-4">
                <legend class="sub-title-sm">Cronograma de tareas</legend>
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Tareas</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (sizeof($cron_posta) > 0): ?>
                        <?php foreach ($cron_posta as $job): ?>
                        <tr class="table-row" data-id='<?= $job->id ?>' data-name='<?= $job->name ?>' data-enabled='<?= $job->enabled ?>' data-job='<?= $job->job ?>'>
                            <td style="text-align: left !important;" scope="row" id="<?= $job->id ?>">
                                <b><?= $job->name ?>
                                <?php if ($job->enabled): ?>
                                <i class="fa fa-check green" style="color: #4CAF50;" aria-hidden="true"></i>
                                <?php else: ?>
                                <i class="fa fa-times red" style="color: #F44336;" aria-hidden="true"></i>
                                <?php endif; ?></b>
                                <p><?= $job->description ?></p>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                        <?php else: ?>
                        <tr>
                            <td>
                                No está configurado el cronograma de tareas.
                            </td>
                        </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <div class="col-xl-3">
                <legend class="sub-title-sm">Debug</legend>
                <?php
                    echo $this->Form->input('debug_mode', ['label' => 'Habilitado', 'type' => 'checkbox', 'checked' => $debug_mode]);
                ?>
            </div>
        </div>
    </fieldset>
    <?= $this->Html->link(__('Cancelar'), ["action" => "index"], ['class' => 'btn btn-default ']) ?>
    <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success' ]) ?>
    <?= $this->Form->end() ?>

<script type="text/javascript">

    var parament;
    var job_selected = null;

    $(document).ready(function () {

        parament = <?= json_encode($paraments) ?>;

        $(".table-row").click(function() {
            job_selected = {
                id: $(this).data('id'),
                name: $(this).data('name'),
                job: $(this).data('job'),
                enabled: $(this).data('enabled')
            };

            if (job_selected.enabled) {
                $('#btn-enable').addClass('d-none');
                $('#btn-disable').removeClass('d-none');
            } else {
                $('#btn-enable').removeClass('d-none');
                $('#btn-disable').addClass('d-none');
            }

            $('.modal-crons').modal('show');

            $('#modal-job-edit').on('shown.bs.modal', function (e) {
                $('#job-title').text(job_selected.name);
                $('#job').val(job_selected.job);
                $('#job-old').val(job_selected.job);
            });
        });

        $('#btn-enable').click(function() {

            var text = '¿Está Seguro que desea habilitar la tarea ' + job_selected.name + '?';
            var controller  = 'Settings';
            var id  = job_selected.id;
            var job  = job_selected.job;

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/enableJob/', 'method': 'post', 'id': 'replacer-enable-job'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'job', 'value': job}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer-enable-job').submit();
                }
            });
        });

        $('#btn-disable').click(function() {

            var text = '¿Está Seguro que desea deshabilitar la tarea ' + job_selected.name + '?';
            var controller  = 'Settings';
            var id  = job_selected.id;
            var job  = job_selected.job;

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/disableJob/', 'method': 'post', 'id': 'replacer-disable-job'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'job', 'value': job}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer-disable-job').submit();
                }
            });
        });

        $('#btn-edit').click(function() {
            $('#modal-job-edit').modal('show');
        });

    });

</script>

<?php $this->end(); ?>
