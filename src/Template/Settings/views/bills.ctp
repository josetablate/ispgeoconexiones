<?php $this->extend('/Settings/views/empresas'); ?>

<?php $this->start('bills'); ?>

<style type="text/css">

    .td-presupuesto, .td-denomination {
        padding-bottom: 8px;
        width: 200px;
    }

    .td-deuda {
        width: 200px;
    }

    #accountant-surcharges-enabled {
        margin-top: 10px;
    }

    #invoicing-is-presupuesto {
        margin-top: 20px;
    }

    #accountant-service-debt-proporcional {
        margin-top: 20px;
    }

    #accountant-service-debt-change {
        margin-top: 10px;
    }

    #customer-doc-validate {
        margin-top: 15px;
    }

    #customer-searching-code {
        margin-top: 15px;
    }

    #system-integration {
        margin-top: 15px;
    }

    #connection-credencials {
        margin-top: 15px;
    }

    #connection-accounting-traffic-enabled {
        margin-top: 15px;
    }

    .sub-title-sm {
        margin-top: 15px;
    }

    .sub-title {
       margin-top: 15px;
    }

    #portal-show-resume {
        margin-top: 15px;
    }

    #portal-show-receipt {
        margin-top: 15px;
        margin-bottom: 15px;
    }

    #upload_parament {
        margin-bottom: 15px;
    }

    /* #table-business {
        width: 100% !important;
    } */

    .sub-titlex {
        color: #696868;
        font-size: 17px;
        font-weight: bold;
        margin-bottom: 10px;
    }

</style>

<?php
    $responsiblesCustom = [ 1 => "IVA Responsable Inscripto", 6 => "Responsable Monotributo" ];
?>



<?= $this->Form->create($paraments, ['enctype' => "multipart/form-data",  'class' => 'form-load', 'id' => 'form-bills']) ?>
    <fieldset>

        <?= $this->Form->hidden('form', ['value' => 'bills']); ?>

        <div class="row">

            <div class="col-md-6">
               
                <div class="row">
                    <div class="col-md-12">
                        <legend class="sub-title">Comprobantes</legend>
                    </div>
                    <div class="col-md-12">

                        <?php if (sizeof($paraments->mass_emails->emails) > 0): ?>
                        <div class="row">

                            <div class="col-md-12">
                                <legend class="sub-titlex">Enviar por Correo luego de la facturación masiva</legend>
                            </div>
                            <div class="col-md-12">
                                <?php 
                                    echo $this->Form->input('invoicing.email_emplate_invoice', ['options' => $emails_emplates, 'label' =>  'Seleccionar plantilla', 'default' => $paraments->invoicing->email_emplate_invoice]); 
                                ?>
                            </div>
                        </div>
                        <?php endif; ?>

                        <div class="row">
                            <div class="col-md-12">
                                <legend class="sub-titlex">Resumen de Cuenta</legend>
                            </div>
                            <div class="col-md-12">
                                <?= $this->Form->input('invoicing.account_summary_complete', ['label' => 'Mostrar resumen de cuenta completo', 'class' => 'mt-3 ml-1', 'type' => 'checkbox', 'checked' => $paraments->invoicing->account_summary_complete]) ?>
                            </div>
                            <?php if ($payment_getway->config->cobrodigital->enabled): ?>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                <?php 
                                    echo $this->Form->input('invoicing.add_cobrodigital_account_summary', ['label' => 'Agregar cód. barra Cobro Digital (PDF)', 'class' =>  ['mt-2 ml-1'], 'type' => 'checkbox', 'checked' => $paraments->invoicing->add_cobrodigital_account_summary, 'title' => 'Tildar para agregar el código de barra de Cobro Digital en el PDF del Resumen de Cuenta', 'data-toggle'=>'tooltip']);
                                ?>
                            </div>
                            <?php endif; ?>
                            <?php if ($payment_getway->config->payu->enabled): ?>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                <?php 
                                    echo $this->Form->input('invoicing.add_payu_account_summary', ['label' => 'Agregar cód. barra PayU (PDF)', 'class' =>  ['mt-2 ml-1'], 'type' => 'checkbox', 'checked' => $paraments->invoicing->add_payu_account_summary, 'title' => 'Tildar para agregar el código de barra de Payu en el PDF del Resumen de Cuenta', 'data-toggle'=>'tooltip']);
                                ?>
                            </div>
                            <?php endif; ?>
                            <?php if ($payment_getway->config->cuentadigital->enabled): ?>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                <?php 
                                    echo $this->Form->input('invoicing.add_cuentadigital_account_summary', ['label' => 'Agregar cód. barra Cuenta Digital (PDF)', 'class' =>  ['mt-2 ml-1'], 'type' => 'checkbox', 'checked' => $paraments->invoicing->add_cuentadigital_account_summary, 'title' => 'Tildar para agregar el código de barra de Cuenta Digital en el PDF del Resumen de Cuenta', 'data-toggle'=>'tooltip']);
                                ?>
                            </div>
                            <?php endif; ?>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <legend class="sub-titlex">Recibo</legend>
                            </div>
                            <div class="col-md-12">
                                <?= $this->Form->input('invoicing.print_duplicate_receipt', ['label' => 'Recibo por duplicado', 'class' => 'mt-3 ml-1', 'type' => 'checkbox', 'checked' => $paraments->invoicing->print_duplicate_receipt]) ?>
                                <?= $this->Form->input('invoicing.auto_print', ['label' => 'Auto imprimir recibo', 'class' => 'mt-3 ml-1', 'type' => 'checkbox', 'checked' => $paraments->invoicing->auto_print]) ?>
                                <?= $this->Form->input('invoicing.resume_receipt', ['label' => 'Mostrar resumen de cuenta en recibo', 'class' => 'mt-3 ml-1', 'type' => 'checkbox',  'checked' => $paraments->invoicing->resume_receipt]) ?>
                                <?= $this->Form->input('invoicing.use_cash_for_payment_anulate', ['label' => 'Anular recibo con caja', 'class' => 'mt-3 ml-1', 'type' => 'checkbox', 'checked' => $paraments->invoicing->use_cash_for_payment_anulate]) ?>
                                <?= $this->Form->input('invoicing.observations_receipt', ['label' => 'Observación en Recibo', 'class' => 'mt-3 ml-1', 'type' => 'textarea', 'value' => $paraments->invoicing->observations_receipt]) ?>
                                <?php 
                                    if (sizeof($paraments->mass_emails->emails) > 0) {
                                        echo $this->Form->input('invoicing.email_emplate_receipt', ['options' => $emails_emplates, 'label' =>  'Enviar recibo (cobranza)', 'default' => $paraments->invoicing->email_emplate_receipt]); 
                                    }
                                ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <legend class="sub-title">Secciones</legend>
                            </div>
                        </div>

                        <?php if ($paraments->gral_config->billing_for_service): ?>
                        <div class="row">
                            <div class="col-md-12">
                                <legend class="sub-titlex">Cobranza</legend>
                            </div>
                            <div class="col-md-12">
                                <?= $this->Form->input('invoicing.connection_auto_selected_cobranza', ['label' => 'Auto selección de conexión', 'class' => 'mt-3 ml-1', 'type' => 'checkbox', 'checked' => $paraments->invoicing->connection_auto_selected_cobranza]) ?>
                            </div>
                        </div>
                        <?php endif; ?>

                        <div class="row">
                            <div class="col-md-12">
                                <legend class="sub-titlex">Facturador manual</legend>
                            </div>
                            <div class="col-md-12">
                                <?= $this->Form->input('invoicing.show_comprobante_from_facturador_manual_after_generation', ['label' => 'Mostrar comprobante luego de su generación', 'class' => 'mt-3 ml-1', 'type' => 'checkbox', 'checked' => $paraments->invoicing->show_comprobante_from_facturador_manual_after_generation]) ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <legend class="sub-titlex">Agregar cliente</legend>
                            </div>
                            <div class="col-md-12">
                                <?= $this->Form->input('invoicing.generate_ticket', ['label' => 'Generar ticket', 'class' => 'mt-3 ml-1', 'type' => 'checkbox', 'checked' => $paraments->invoicing->generate_ticket]) ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-4">

                <table>
                    <tr>
                        <td colspan="2"><legend class="sub-title-sm">Parámetros</legend></td>
                    </tr>
                    <tr>
                        <td>Tipo de documento por defecto &nbsp;</td>
                        <td style="width:200px"><?= $this->Form->select('customer.doc_type', $doc_types, ['value' => $paraments->customer->doc_type]) ?></td>
                    </tr>
                    <tr>
                        <td>Tipo de cliente por defecto &nbsp;</td>
                        <td style="width:200px"><?= $this->Form->select('customer.responsible', $responsibles,  ['value' => $paraments->customer->responsible]) ?></td>
                    </tr>
                     <tr>
                        <td>Denominación &nbsp;</td>
                        <td class="td-denomination"><?= $this->Form->input('customer.denomination', ['label' => 'Habilitado', 'checked' => $paraments->customer->denomination]) ?></td>
                    </tr>
                     <tr>
                        <td>Condición de venta por defecto &nbsp;</td>
                        <td style="width:200px"><?= $this->Form->select('customer.cond_venta', $cond_venta,  ['value' => $paraments->customer->cond_venta]) ?></td>
                    </tr>
                    <tr>
                        <td>Presupuesto &nbsp;</td>
                        <td class="td-presupuesto"><?= $this->Form->input('invoicing.is_presupuesto', ['label' => 'Habilitado', 'checked' => $paraments->invoicing->is_presupuesto]) ?></td>
                    </tr>
                    <tr>
                        <td>Tipo de vencimiento &nbsp;</td>
                        <td style="width:200px"><?= $this->Form->select('accountant.type', $accountant_types, ['value' => $paraments->accountant->type]) ?></td>
                    </tr>
                    <tr>
                        <td>Día de vencimiento &nbsp; </td>
                        <td style="width:80px"><?= $this->Form->number('accountant.daydue', ['value' => $paraments->accountant->daydue]) ?></td>
                    </tr>
                    <tr class="d-none">
                        <td>Deuda por servicio nuevo &nbsp;</td>
                        <td style="width:200px"><?= $this->Form->input('accountant.service_debt', ['label' => 'Proporcional', 'type' => 'checkbox', 'checked' => $paraments->accountant->service_debt])?></td>
                    </tr>
                  
                    <tr>
                        <td style="padding-bottom: 5px;">Facturar con &nbsp; <small class="form-text text-muted mb-2">
                            Empresa p/facturar por defecto a los clientes
                        </small></td>
                        <td style="width:200px"><?= $this->Form->select('invoicing.business_billing_default', $business, ['value' => $paraments->invoicing->business_billing_default ]) ?></td>
                    </tr>
                </table>

            </div>

            <div class="col-md-2">
                <table>
                    <tr>
                        <td colspan="2"><legend class="sub-title-sm">Interés Cuotas</legend></td>
                    </tr>
                    <?php foreach($paraments->accountant->dues as $num => $due): ?>

                    <tr>
                        <td class="w-50">En <?= $num + 1 ?>  &nbsp; </td>
                        <td class="w-50"><?= $this->Form->number('accountant.dues[]', ['value' => $due, 'step' => 0.01]) ?></td>
                    </tr>

                    <?php endforeach;?>
                </table>
            </div>

        </div>
   
    </fieldset>
    <br>
    <?= $this->Html->link(__('Cancelar'),["action" => "indx"], ['class' => 'btn btn-default']) ?>
    <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success' ]) ?>
<?= $this->Form->end() ?>



<script type="text/javascript">

    $('[data-toggle="tooltip"]').tooltip();

</script>

<?php $this->end(); ?>
