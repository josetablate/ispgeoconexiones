
<div class="row">
    <div class="col-xl-12">
        <?= $this->Form->create(null, ['class' => 'form-load']) ?>
        <fieldset>

            <div class="row mb-2">
                <div class="col-xl-4">

                    <div class="row">
                        <div class="col-xl-6">
                            <?php
                                echo $this->Form->input('enabled', ['label' => 'Habilitado', 'type' => 'checkbox', 'checked' => $paraments->backup->enabled]);
                           ?>
                        </div>
                        <!-- <div class="col-xl-6">
                            <?php
                                //echo $this->Form->input('enabled_comprobantes', ['label' => 'Comprobantes', 'type' => 'checkbox', 'checked' => $paraments->backup->enabled_comprobantes]);
                            ?>
                        </div> -->
                    </div>

                    <div class="row">
                        <div class="col-xl-12">
                             <?php
                                echo $this->Form->input('folder_name', ['label' => 'Nombre de Carpeta', 'required' => true, 'value' => $paraments->backup->folder_name]);
                           ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xl-6">
                            <?php
                               echo $this->Form->input('amount_files', ['label' => 'Número archivos', 'type' => 'number', 'min' => 1, 'step' => 1, 'required' => true, 'value' => $paraments->backup->amount_files]);
                            ?>
                        </div>
                        <div class="col-xl-6">
                          
                            <div class="form-group required">
                                <label class="control-label" for="backup-hours-execution">Horario de Ejecución</label>
                                <div class='input-group date' id='backup-hours-execution-datetimepicker'>
                                    <input  name="hours_execution" required="required"  id="backup-hours-execution"  type='text' class="form-control" />
                                    <span class="input-group-addon input-group-text calendar mr-0">
                                        <span class="glyphicon icon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xl-12">
                            
                            <div class="card">
                              <div class="card-body">  
                                <h6 class="card-subtitle mb-2 text-muted">Credenciales Dropbox</h6>
                                <?php

                                    echo $this->Form->input('app_key', ['label' => 'app_key', 'required' => true, 'value' => $paraments->backup->app_key]);
                                    echo $this->Form->input('app_secret', ['label' => 'app_secret', 'required' => true, 'value' => $paraments->backup->app_secret]);
                                    echo $this->Form->input('token', ['label' => 'token', 'required' => true, 'value' => $paraments->backup->token]);

                                ?>
                              </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </fieldset>

        <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success' ]) ?>
        <?= $this->Form->end() ?>
    </div>
</div>


<script type="text/javascript">

    $(document).ready(function () {

        var parament = <?= json_encode($paraments) ?>;

        var defaultDate = "2017-10-01T" + parament.backup.hours_execution;
        $('#backup-hours-execution-datetimepicker').datetimepicker({
            locale: 'es',
            defaultDate: defaultDate,
            format: 'HH:mm',
        });

    });

</script>
