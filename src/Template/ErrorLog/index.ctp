<style type="text/css">

    textarea {
      border: 1px solid #999999;
      width: 100%;
      margin: 5px 0;
      padding: 3px;
    }

    tr.new {
        background-color: #a7d6ec;
    }

</style>

<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5" >

        <?php 

            echo $this->Html->link(
                '<span class="glyphicon icon-eye" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                    'id' => 'btn-view-all',
                    'title' => '',
                    'class' => 'btn btn-default',
                    'escape' => false
                ]);

            echo $this->Html->link(
                    '<span class="glyphicon fas fa-search"  aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Buscar por columnas',
                    'class' => 'btn btn-default btn-search-column ml-1',
                    'escape' => false
                ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-bin" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                    'id' => 'btn-delete-all',
                    'title' => '',
                    'class' => 'btn btn-default ml-2',
                    'escape' => false
                ]);
        ?>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-hover table-bordered" id="table-errors">
            <thead>
                <tr>
                    <th>Id</th>          <!--0-->
                    <th>Fecha</th>       <!--1-->
                    <th>Event</th>       <!--2-->
                    <th>Descripción</th> <!--3-->
                    <th>Usuario</th>     <!--4-->
                    <th>Data</th>        <!--5-->
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-view',
        'name' =>  'Detalle',
        'icon' =>  'icon-eye',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-mark-view',
        'name' =>  'Marcar Como Visto',
        'icon' =>  'icon-eye',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-error-log']);
    echo $this->element('actions', ['modal'=> 'modal-errors', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<div class="modal fade" id="modal-view" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Detalle</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-md-12">

                         <textarea name="" id="data" rows="10" readonly>

                         </textarea>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var table_errors = null;
    var error_selected = null;
    var curr_pos_scroll = null;

    $(document).ready(function () {

        $('.btn-search-column').click(function() {
            $('.modal-search-columns-error-log').modal('show');
        });

        $('#table-errors').removeClass('display');

        $('#btns-tools').hide();

    	table_errors = $('#table-errors').DataTable({
    	    "order": [[ 0, 'desc' ]],
    	    "deferRender": true,
            "processing": true,
            "serverSide": true,
		    "ajax": {
                "url": "/ispbrain/ErrorLog/get_error_log.json",
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
            				  window.location.href ="/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "drawCallback": function( c ) {
                var flag = true;
            	switch (c.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
            	    if (table_errors) {

                        var tableinfo = table_errors.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }

                        if (curr_pos_scroll) {

                            $(table_errors.settings()[0].nScrollBody).scrollTop( curr_pos_scroll.top );
                            $(table_errors.settings()[0].nScrollBody).scrollLeft( curr_pos_scroll.left );
                        }
                    }
            	}
            },
            "scrollY": true,
		    "scrollY": '430px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
            "columns": [
                { 
                    "data": "id",
                },
                {
                    "data": "created",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                {
                    "data": "event",
                    "type": "string"
                },
                {
                    "data": "msg",
                    "type": "string"
                },
                { "data": "user.username" },
                {
                    "data": "data",
                },
               
            ],
            "columnDefs": [
                { "type": "datetime-custom", "targets": [1] },
                { "visible": false, "targets": [5] }
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
                if (!data.view) {
                    $(row).addClass('new');
                }
            },
    	    "language": dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Todas"]],
             "dom":
    	    	"<'row'<'col-md-6'l><'col-md-4'f><'col-md-2 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    	});

    	$('#table-errors_wrapper .tools').append($('#btns-tools').contents());

    	$('#btns-tools').show();
    });

    $('#table-errors').on( 'init.dt', function () {
        createModalSearchColumn(table_errors, '.modal-search-columns-error-log');
    });

	$('#table-errors tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_errors.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            error_selected = table_errors.row( this ).data();

            $('.modal-errors').modal('show');
        }
    });

    $('#btn-view').click(function() {
        $('#modal-view #data').val(error_selected.data);
        $('#modal-view').modal('show');
    });

    $('#btn-view-all').click(function() {
        var action = '/ispbrain/ErrorLog/viewAll/';
        window.open(action, '_self');
    });

    $('#btn-mark-view').click(function() {
        var action = '/ispbrain/ErrorLog/markView/' + error_selected.id;
        window.open(action, '_self');
    });

    $('#btn-delete-all').click(function() {
        var text = "¿Está Seguro que desea eliminar todo el log de errores?";

        bootbox.confirm(text, function(result) {
            if (result) {
                var action = '/ispbrain/ErrorLog/deleteAll/';
                window.open(action, '_self');
            }
        });
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
