<style type="text/css">

    .logo {
        color: #FF5722;
        margin-top: 50px;
        margin-bottom: 50px;
    }

    .panel-cards-import {
        background-color: #8BC34A !important;
        color: white !important;
        padding: 10px;
    }

</style>

<div class="col-md-8" style="margin-top: 50px;">
    <p class="lead"><?= __('Cosas a tener') ?> <span class="text-success"><?= __('EN CUENTA') ?></span></p>
    <ul class="list-unstyled" style="line-height: 2">
        <li><span class="fa fa-check text-success"></span> <?= __('Col 1: Fecha de pago') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Col 2: ID Orden') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Col 3: Valor') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Col 4: País') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Col 5: Cuenta') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Col 6: Número de pago') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Col 7: ID Cupón') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Col 8: Referencia de venta') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Col 9: Estado') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Col 10: Medio de pago') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Col 11: Tipo de creación') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Col 12: Múltiples pagos') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Col 13: Frecuencia recordatorio') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Col 14: Fecha de recordatorio') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Col 15: Nombre cliente') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Col 16: Concepto') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Col 17: Fecha de expiración') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Col 18: Email del pagador') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Únicamente archivo con extensión') ?> <strong><?= __('CSV') ?></strong></li>
    </ul>
</div>

<div class="col-md-4" style="margin-top: 50px;">

    <div class="cards cards-default">
        <div class="cards-heading panel-cards-import"><?= __('Importar Pagos') ?></div>
        <div class="cards-body">
            <?= $this->Form->create(null, ['url' => ['controller' => 'Payu' , 'action' => 'loadPayment'], 'name' => 'cards-form', 'type' => 'file','role'=>'form']) ?>

                <div class="form-group">
                    <?= $this->Form->hidden('hash', ['value' => time()] ); ?>
                    <label class="sr-only" for="csv"><?= __('CSV') ?></label>
                    <?php echo $this->Form->input('csv', ['type'=>'file', 'accept' => '.csv', 'class' => 'form-control', 'label' => false, 'placeholder' => __('csv upload'), 'required' => true]); ?>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <button type="submit" class="btn btn-default btn-import"><?= __('Importar') ?></button>
                </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
