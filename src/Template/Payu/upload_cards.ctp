<style type="text/css">

    .logo {
        color: #FF5722;
        margin-top: 50px;
        margin-bottom: 50px;
    }

    .panel-cards-import {
        background-color: #8BC34A !important;
        color: white !important;
        padding: 10px;
    }

</style>

<div class="col-md-8" style="margin-top: 50px;">
    <p class="lead"><?= __('Cosas a tener') ?> <span class="text-success"><?= __('EN CUENTA') ?></span></p>
    <ul class="list-unstyled" style="line-height: 2">
        <li><span class="fa fa-check text-success"></span> <?= __('La 1er columna, debe ser el código de barra') ?></li>
        <li><span class="fa fa-check text-success"></span> <?= __('Únicamente archivo con extensión') ?> <strong><?= __('CSV') ?></strong></li>
    </ul>
</div>

<div class="col-md-4" style="margin-top: 50px;">

    <div class="cards cards-default">
        <div class="cards-heading panel-cards-import"><?= __('Importar Tarjetas') ?></div>
        <div class="cards-body">
            <?= $this->Form->create(null, ['url' => ['controller' => 'Payu' , 'action' => 'uploadCards'], 'name' => 'cards-form', 'type' => 'file','role'=>'form']) ?>

                <div class="form-group">
                    <label class="sr-only" for="csv"><?= __('CSV') ?></label>
                    <?= $this->Form->hidden('hash', ['value' => time()] ); ?>
                    <?php echo $this->Form->input('csv', ['type'=>'file', 'accept' => '.csv', 'class' => 'form-control', 'label' => false, 'placeholder' => __('csv upload'), 'required' => TRUE]); ?>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <button type="submit" class="btn btn-default btn-import"><?= __('Importar') ?></button>
                </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
