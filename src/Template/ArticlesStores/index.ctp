<div id="btns-tools">

    <div class="text-right btns-tools margin-bottom-5" >

        <?php

            echo $this->Html->link(
                '<span class="fa fa-plus" aria-hidden="true"></span>',
                ['controller' => 'ArticlesStores', 'action' => 'add'],
                [
                'title' => 'Actualizar Stock',
                'class' => 'btn btn-default ',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="fa fa-random" aria-hidden="true"></span>',
                ['controller' => 'ArticlesStores', 'action' => 'move'],
                [
                'title' => 'Mover Stock',
                'class' => 'btn btn-default ',
                'escape' => false
            ]);

             echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export',
                'escape' => false
            ]);
        ?>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-hover table-bordered" id="table-articles-stores">
            <thead>
                <tr>
                    <th title="Ordenar">Agregado</th>
                    <th title="Ordenar">Nombre del artículo</th>
                    <th title="Ordenar">Depósito</th>
                    <th title="Ordenar">Disponible</th>
                    <th title="Ordenar">Modificado</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-edit',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-default'
    ];

    echo $this->element('actions', ['modal'=> 'modal-articles-stores', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<div class="modal fade modal-edit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel">Editar Cantidad Disponible</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form method="post" accept-charset="utf-8" role="form" action="<?= $this->Url->build(["controller" => "ArticlesStores", "action" => "edit"]) ?>">
                            <div style="display:none;">
                                <input type="hidden" name="_method" value="POST">
                            </div>
                            <fieldset>

                                <label for="">Código del articulo</label>
                                <input class="form-control" id="cod-article" readonly>

                                <label for="">Depósito</label>
                                <input class="form-control" id="store-name" readonly>

                                <input type="hidden" id="article-store-id" name="article-store-id" class="form-control" >
                                <?php
                                    echo $this->Form->input('current_amount', ['label' => 'Cantidad Actual', 'id' => 'current-amount']);
                                    echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]); 
                                ?>
                            </fieldset>

                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <?= $this->Form->button(__('Guardar'), ['class' => 'btn-submit btn-success']) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var table_tock_stores = null;
    var stock_stores_selected = null;

    $(document).ready(function () {

        $('#table-articles-stores').removeClass('display');

		table_tock_stores = $('#table-articles-stores').DataTable({
		    "order": [[ 0, 'desc' ]],
		    "autoWidth": true,
		    "scrollY": '350px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "sWrapper":  "dataTables_wrapper dt-bootstrap4",
            "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/ArticlesStores/index.json",
                "dataSrc": "articlesStores",
                "error": function(c) {
                    switch (c.status) {
                        case 400:
                            flag = false;
                            generateNoty('warning', 'Ha ocurrido un error.');
                            break;
                        case 401:
                            flag = false;
                            generateNoty('warning', 'Error, no posee una autorización.');
                            break;
                        case 403:
                            flag = false;
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                        	setTimeout(function() { 
                	            window.location.href ="/ispbrain";
                        	}, 3000);
                            break;
                    }
                }
            },
            "columns": [
                { 
                    "data": "created",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                { "data": "article.name" },
                { "data": "store.name" },
                { "data": "current_amount" },
                { 
                    "data": "modified",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
            ],
            "columnDefs": [
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            "dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
		});

		$('#table-articles-stores_wrapper .tools').append($('#btns-tools').contents());

        $(".btn-export").click(function() {

            bootbox.confirm('Se descargará un archivo Excel', function(result) {
                if (result) {
                    $('#table-articles-stores').tableExport({tableName: 'Stock Depósitos', type:'excel', escape:'false'});
                }
            });
        });

        $('#table-articles-stores tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                table_tock_stores.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
                stock_stores_selected = table_tock_stores.row( this ).data();

                $('.modal-articles-stores').modal('show');
            }
        });

        $('a#btn-edit').click(function() {

            $('.modal-actions').modal('hide');

            $('.modal-edit #cod-article').val(stock_stores_selected.article.code);
            $('.modal-edit #store-name').val(stock_stores_selected.store.name);
            $('.modal-edit #current-amount').val(stock_stores_selected.current_amount);
            $('.modal-edit #article-store-id').val(stock_stores_selected.id);

            $('.modal-edit').modal('show');
        });
    });
  
</script>
