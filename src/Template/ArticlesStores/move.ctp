<style type="text/css">

    .ui-autocomplete {
		max-height: 400px;
		overflow-y : auto;
		overflow-x : hidden;
	}

</style>

<div class="row">
     <div class="col-md-4">
        <div class="text-right">
            <a class="btn btn-default" title="Ir al stock de depósitos." href="<?=$this->Url->build(["action" => "index"])?>" role="button">
                <span class="glyphicon icon-arrow-left"  aria-hidden="true"></span>
            </a>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-md-4">

        <?= $this->Form->create($articleStore,  ['class' => 'form-load']) ?>
            <fieldset>

                <?php
                    $stores_options = [];
                    $stores_options[''] = 'Seleccionar Depósito';
                    foreach ($stores as $key => $store) {
                       $stores_options[$key] =   $store;
                    }
                    echo $this->Form->input('store_id_from', ['label' => 'Depósito Origen *', 'options' => $stores_options, 'requiered' => true]); ?>

                <div class="form-group text required">
                    <label class="control-label" for="code">Código del artículo * <span class='fa fa-question-circle' title='Ingrese el código o nombre del stock. Con * desplega la lista completa. Debe seleccionar el Depósito de Origen' aria-hidden='true' ></span></label></label>
                    <input type="text" name="code" required="required" maxlength="45" id="autocomplete" class="form-control">
                    <input type="hidden" name="article_store_id" id="article_store_id" class="form-control">
                </div>
                <div class="form-group text required">
                    <label class="control-label" for="name">Nombre del artículo</label>
                    <input type="text" name="name" required="required" maxlength="99" id="name" class="form-control" readonly>
                </div>

                <div class="form-group text required">
                    <label class="control-label" for="name">Cantidad Disponible</label>
                    <input type="text" name="current_amount" id="current_amount" class="form-control" readonly >
                </div>
                <?php
                    echo $this->Form->input('store_id_to', ['label' => 'Depósito Destino *', 'options' => $stores_options, 'required' => true]);
                    echo $this->Form->input('amount', ['label' => 'Cantidad', 'type' => 'number', 'min' => 0]);
                ?>
            </fieldset>
            <?= $this->Html->link(__('Cancelar'),["controller" => "ArticlesStores", "action" => "index"], ['class' => 'btn btn-default']) ?>
            <?= $this->Form->button(__('Mover'), ['class' => 'btn-submit btn-success']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function() {

        var articles = [];

        $('#store-id-from').change(function() {

            $('#autocomplete').val('');

            var store_id_from = $(this).val();

            articles = [];

            $.ajax({
                url: "<?= $this->Url->build(['controller' => 'ArticlesStores', 'action' => 'getStockByStoreAjax']) ?>",
                type: 'POST',
                dataType: "json",
                data: JSON.stringify({ store_id_from: store_id_from}),
                success: function(data) {

                    $.each(data.articles, function(i, val) {

                        var ui = null;
                        if (val.article.snid_stores != null && val.article.snid_stores.length > 0) {

                             $.each(val.article.snid_stores, function(e, sval){

                                 if (sval.store_id == val.store_id) {
                                    ui = {exist_snid: true, id: val.id, label:sval.snid + ' - ' + val.article.code + '*', value:sval.snid, name:val.article.name, amount: 1};
                                    articles.push(ui);
                                 }
                             });

                        } else {
                            ui = {exist_snid: false, id: val.id, label:val.article.code + ' - ' + val.article.name + '**', value:val.article.code, name:val.article.name, amount: val.current_amount};
                            articles.push(ui);
                        }
                    });

                    $( "#autocomplete" ).autocomplete({
                    	source: articles,
                        select: function( event, ui ) {
                            $('#name').val(ui.item.name);
                            $('#article_store_id').val(ui.item.id);
                            $('#current_amount').val(ui.item.amount);

                            if (ui.item.exist_snid) {
                                 $('#amount').attr('disabled', true);
                            } else {
                                $('#amount').attr('disabled', false);
                            }
                        }
                    });
                },
                error: function (jqXHR) {
                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                    	  window.location.href ="/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al obtener los códigos.');
                    }
                }
            });
        });
    });

</script>
