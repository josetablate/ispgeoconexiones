<style type="text/css">

    textarea.form-control {
        height: 50px !important;
    }

</style>

<div class="row">
     <div class="col-md-3">
        <div class="text-right">
            <a class="btn btn-default" title="Ir al article de depósitos." href="<?= $this->Url->build(["action" => "index"]) ?>" role="button">
                <span class="glyphicon icon-arrow-left" aria-hidden="true"></span>
            </a>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-xl-5">
        <?= $this->Form->create($articleStore,  ['class' => 'form-load']) ?>
            <fieldset>

                <div class="form-group text required">
                    <label class="control-label" for="code">Código del artículo * <span class='fa fa-question-circle' title='Ingrese el código o nombre del article. Con * desplega la lista completa.'  aria-hidden='true' ></span></label>
                    <input type="text" name="code" required="required" maxlength="45" id="autocomplete" class="form-control" >
                    <input type="hidden" name="article_id" id="article_id" class="form-control" >
                </div>

                <div class="form-group text required">
                    <label class="control-label" for="name">Nombre del artículo</label>
                    <input type="text" name="name" maxlength="99" id="name" class="form-control"  >
                </div>

                <?php
                    echo $this->Form->input('store_id', ['label' => 'Depósito *','options' => $stores, 'required' => true ]);
                    echo $this->Form->input('amount', ['type' => 'number', 'min' => '0',  'label' => 'Cantidad *', 'value' => 0] );
                ?>

            </fieldset>
            <?= $this->Html->link(__('Cancelar'),["controller" => "ArticlesStores", "action" => "index"], ['class' => 'btn btn-default']) ?>
            <?= $this->Form->button(__('Agregar'), ['class' => 'btn-submit btn-success']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function() {

        var codes = [];

         $.ajax({
            url: "<?= $this->Url->build(['controller' => 'articles', 'action' => 'getAllCodeAjax']) ?>",
            type: 'POST',
            dataType: "json",
            success: function(data) {

                $.each(data.codes, function(i, val) {
                    var ui =  {id: val.id, label:val.code.toUpperCase() + ' - ' + val.name + '*', value:val.code.toUpperCase(), name:val.name};
                    codes.push(ui);
                });
            },
            error: function (jqXHR) {

                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() { 
                	  window.location.href ="/ispbrain";
                	}, 3000);

                } else {
                	generateNoty('error', 'Error al obtener los códigos.');
                }

                closeModalPreloader();
                $('#btn-apply-config-avisos').show();
            }
        });

        var selected = false;

        $( "#autocomplete" ).autocomplete({
        	source: codes,
            select: function( event, ui ) {
                $('#name').val(ui.item.name);
                $('#article_id').val(ui.item.id);
                $('#name').attr('desabled', true);

                $('#amount').val('');
                $('#amount').attr('disabled', false);
                $('#amount').attr('required', 'required');
            }
        });
    });

</script>
