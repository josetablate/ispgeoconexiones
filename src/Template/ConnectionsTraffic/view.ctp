<style type="text/css">

    .vertical-table {
        width: 100%;
        color: #696868;
    }
    
    table > tbody > tr {
        border-bottom: 1px solid #e5e5e5;
    }
    
</style>


    <div class="row">
        <div class="col-12">
            <legend class="sub-title"><?= __("Conexión del Cliente: {0} ", $connection->customer->name) ?></legend>
        </div>
    </div>

    <div class="row">
        
        <div class="col-xl-4">
        
            <legend class="sub-title-sm"><?=  __('Información') ?></legend>
            <table class="vertical-table">
                <tr>
                    <th><?= __('Creado por el Usuario') ?></th>
                    <td class="text-right"><?= h($connection->user->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Controlador') ?></th>
                    <td class="text-right"><?= h($connection->controller->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Servicio') ?></th>
                    <td class="text-right"><?= h($connection->service->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('IP') ?></th>
                    <td class="text-right"><?= $connection->ip ?></td>
                </tr>
                <tr>
                    <th><?= __('Dirección MAC') ?></th>
                    <td class="text-right"><?= h(strtoupper($connection->mac)) ?></td>
                </tr>
                <tr>
                    <th><?= __('Área') ?></th>
                    <td class="text-right"><?= $connection->area->name ?></td>
                </tr>
            
                <tr>
                    <th><?= __('Código del Cliente') ?></th>
                    <td class="text-right"><?= sprintf("%'.05d", $connection->customer_code)  ?></td>
                </tr>
              
                <tr>
                    <th><?= __('Creado') ?></th>
                    <td class="text-right"><?= date('d-m-Y H:i', strtotime($connection->created))?></td>
                </tr>
                <tr>
                    <th><?= __('Modificado') ?></th>
                    <td class="text-right"><?= date('d-m-Y H:i', strtotime($connection->modified))?></td>
                </tr>
                <tr>
                    <th><?= __('Habilitado') ?></th>
                    <?php if($connection->enabled):?>
                        <td class="text-right green"><?=  __('Si') ?></td>
                    <?php else:?>
                        <td class="text-right red"><?=  __('No') ?></td>
                    <?php endif;?>
                </tr>
            </table>
        </div>

        <div class="col-xl-8">
            <div id="container-bytes"></div>
            <div id="container-packets"></div>
        </div>
        
    </div>

</div>    

<script type="text/javascript"> 
    

    $(document).ready(function () {
        
        var connection =  <?=json_encode($connection)?>
		
		//grafica de accounting traffic
		
		$.ajax({
            url: "<?=$this->Url->build(['controller' => 'ConnectionsTraffic', 'action' => 'getAccountingTrafficHistorial'])?>",
            type: 'POST',
            dataType: "json",
            data: JSON.stringify({ ip: connection.ip }),
            success: function(data){
                
                var bytes_down = [];
                var bytes_up = [];
                
                var packets_dowm = [];
                var packets_up = [];

                $.each(data.connectionsTraffic, function(id, snapshot) {

                     var d = new Date(snapshot.time);

                     var utc = Date.UTC(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMinutes(), d.getSeconds());

                     bytes_down.push([utc, Math.round((snapshot.bytes_down / 1024) / 1024)]);
                     bytes_up.push([utc, Math.round((snapshot.bytes_up / 1024) / 1024)]);
                     
                     packets_dowm.push([utc, snapshot.packets_down]);
                     packets_up.push([utc, snapshot.packets_up]);
                    
                    
                });
               
                Highcharts.setOptions({
                    lang: {
                        printChart: "Imprimir Gráfica",
                        downloadJPEG: "Descargar JPEG",
                        downloadPDF: "Descargar PDF",
                        downloadPNG: "Descargar PNG",
                        downloadSVG: "Descargar SVG" ,
                        months: [ "Enero" , "Febrero" , "Marzo" , "Abril" , "Mayo" , "Junio" , "Julio" , "Agosto" , "Septiembre" , "Octubre" , "Noviembre" , "Diciembre"],
                        shortMonths: [ "Ene" , "Feb" , "Mar" , "Abr" , "May" , "Jun" , "Jul" , "Ago" , "Sep" , "Oct" , "Nov" , "Dic"],
                        weekdays: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        loading: "Cargando...",
                        rangeSelectorFrom: "Desde",
                        rangeSelectorTo: "Hasta",
                        rangeSelectorZoom: "Zoom",
                    }
                });
                
                // Create the chart
                Highcharts.stockChart('container-bytes', {
                    
                    credits:{
                        enabled: false,
                    },
                    title: {
                        text: 'Bytes Traffic Accounting'
                    },
                    legend:{
                        enabled: true,
                    },
                    rangeSelector:{
                        enabled: true,
                        buttons: [{
                        	type: 'month',
                        	count: 1,
                        	text: '1m'
                        }, {
                        	type: 'month',
                        	count: 3,
                        	text: '3m'
                        }, {
                        	type: 'month',
                        	count: 6,
                        	text: '6m'
                        }, {
                        	type: 'ytd',
                        	text: 'YTD'
                        }, {
                        	type: 'year',
                        	count: 1,
                        	text: '1y'
                        }, {
                        	type: 'all',
                        	text: 'All'
                        }],
                        
                    },
                    yAxis: {
                        title: {
                            text: 'Traffic',
                            align: "middle",
                        },
                        labels: {
                            align: "left",
                            format: "{value} MB",
                        },
                    },
                    tooltip: {
                        valueSuffix: ' MB',
                        dateTimeLabelFormats: {
                            millisecond:"%A, %e %B, %H:%M:%S.%L",
                            second:"%A, %e %B, %H:%M:%S",
                            minute:"%A, %e %B, %H:%M",
                            hour:"%A, %e %B, %H:%M",
                            day:"%A, %e %B, %Y",
                            week:"Semana Desde %A, %e %B, %Y",
                            month:"%B %Y",
                            year:"%Y"
                        },
                    },
                    series: [
                    {
                        type: "line",
                        name: 'Down',
                        color: "#00ff00",
                        data: bytes_down,
                        shadow: true,
                        marker: {
                            enabled: true,
                            symbol: 'triangle-down',
                            width: 16
                        },
                    },
                    {
                        type: "line",
                        name: 'Up',
                        color: "#ff0000",
                        data: bytes_up,
                        shadow: true,
                        marker: {
                            enabled: true,
                            symbol: 'triangle',
                            width: 16
                        },
                    }
                    ],
                    
                },
                function (chart) {
            
                    // apply the date pickers
                    setTimeout(function () {
                        $('input.highcharts-range-selector', $(chart.container).parent())
                            .datepicker();
                    }, 0);
                });
                
                Highcharts.stockChart('container-packets', {
                    
                    credits:{
                        enabled: false,
                    },
                    title: {
                        text: 'Packets Traffic Accounting'
                    },
                    legend:{
                        enabled: true,
                    },
                    rangeSelector:{
                        enabled: true,
                        buttons: [{
                        	type: 'month',
                        	count: 1,
                        	text: '1m'
                        }, {
                        	type: 'month',
                        	count: 3,
                        	text: '3m'
                        }, {
                        	type: 'month',
                        	count: 6,
                        	text: '6m'
                        }, {
                        	type: 'ytd',
                        	text: 'YTD'
                        }, {
                        	type: 'year',
                        	count: 1,
                        	text: '1y'
                        }, {
                        	type: 'all',
                        	text: 'All'
                        }],
                        
                    },
                    yAxis: {
                        title: {
                            text: 'Traffic',
                            align: "middle",
                        },
                        labels: {
                            align: "left",
                            format: "{value}",
                        },
                    },
                    tooltip: {
                        // valueSuffix: ' KB',
                        dateTimeLabelFormats: {
                            millisecond:"%A, %e %B, %H:%M:%S.%L",
                            second:"%A, %e %B, %H:%M:%S",
                            minute:"%A, %e %B, %H:%M",
                            hour:"%A, %e %B, %H:%M",
                            day:"%A, %e %B, %Y",
                            week:"Semana Desde %A, %e %B, %Y",
                            month:"%B %Y",
                            year:"%Y"
                        },
                    },
                    series: [{
                        type: "column",
                        name: 'Down',
                        color: "#00ff00",
                        data: packets_dowm,
                        shadow: true,
                        marker: {
                            enabled: true,
                            symbol: 'square',
                        },
                    },
                    {
                        type: "column",
                        name: 'Up',
                        color: "#ff0000",
                        data: packets_up,
                        shadow: true,
                        marker: {
                            enabled: true,
                            symbol: 'square',
                        },
                    }],
                    
                },
                function (chart) {
            
                    // apply the date pickers
                    setTimeout(function () {
                        $('input.highcharts-range-selector', $(chart.container).parent())
                            .datepicker();
                    }, 0);
                });
                
            },
            error: function (jqXHR) {

                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() { 
                	  window.location.href ="/ispbrain";
                	}, 3000);

                } else {
                	generateNoty('error', 'Error accounting Traffic.');
                }
            }
        });

    });
	
	// Set the datepicker's date format
    $.datepicker.setDefaults({
        dateFormat: 'yy-mm-dd',
        onSelect: function () {
            this.onchange();
            this.onblur();
        }
    });

</script>
