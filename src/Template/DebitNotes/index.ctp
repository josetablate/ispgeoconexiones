<style type="text/css">

    .my-hidden {
        display: none;
    }

</style>

    <div id="btns-tools">
        <div class="text-right btns-tools margin-bottom-5">

            <?php

                echo $this->Html->link(
                    '<span class="fas fa-sync-alt" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Actualizar la tabla',
                    'class' => 'btn btn-default btn-update-table ml-1',
                    'escape' => false
                ]);

                echo $this->Html->link(
                    '<span class="glyphicon icon-eye" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Ocultar o Mostrar Columnas',
                    'class' => 'btn btn-default btn-hide-column ml-1',
                    'escape' => false
                ]);

                echo $this->Html->link(
                    '<span class="glyphicon fas fa-search" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Buscar por Columnas',
                    'class' => 'btn btn-default btn-search-column ml-1',
                    'escape' => false
                ]);

                echo $this->Html->link(
                    '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Exportar tabla',
                    'class' => 'btn btn-default btn-export ml-1',
                    'escape' => false
                ]);

                echo $this->Html->link(
                    '<i class="fas fa-download"></i>',
                    'javascript:void(0)',
                    [
                    'title' => 'Descargar notas de débito',
                    'class' => 'btn btn-default btn-download ml-1',
                    'escape' => false
                ]);
                    
            ?>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
             <table class="table table-bordered table-hover" id="table-debit_notes">
                <thead>
                    <tr>
                        <th>Fecha</th>                      <!--0-->
                        <th>Comprobante</th>                <!--1-->
                        <th>Pto. Vta.</th>                  <!--2-->
                        <th>Número</th>                     <!--3-->
                        <th>Desde</th>                      <!--4-->
                        <th>Hasta</th>                      <!--5-->
                        <th>Comentario</th>                 <!--6-->
                        <th>Vencimiento</th>                <!--7-->
                        <th class="subtotal">Subtotal</th>  <!--8-->
                        <th class="impuesto">Impuestos</th> <!--9-->
                        <th class="total">Total</th>        <!--10-->
                        <th>CAE</th>                        <!--11-->
                        <th>VTO</th>                        <!--12-->
                        <th>Usuario</th>                    <!--13-->
                        <th>Código</th>                     <!--14-->
                        <th>Tipo</th>                       <!--15-->
                        <th>Nro</th>                        <!--16-->
                        <th>Nombre</th>                     <!--17-->
                        <th>Domicilio</th>                  <!--18-->
                        <th>Ciudad</th>                     <!--19-->
                        <th>Empresa Facturación</th>        <!--20-->
                        <th>Nro Asiento</th>                <!--21-->
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

<?php 

    $buttons = [];

    $buttons[] = [
        'id' =>  'btn-print-debit_note',
        'name' =>  'Imprimir',
        'icon' =>  'icon-printer',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-go-customer',
        'name' =>  'Ficha del Cliente',
        'icon' =>  'glyphicon icon-profile',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-edit-administrative-movement',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-success'
    ];

    $buttons[] = [
        'id'   =>  'btn-send-email-debit-note',
        'name' =>  'Enviar Correo',
        'icon' =>  'fab fa-telegram-plane',
        'type' =>  'btn-success'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete',
        'name' =>  'Eliminar',
        'icon' =>  'fa fa-times',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-debit_notes', 'title' => 'Acciones', 'buttons' => $buttons ]);
    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-debit_notes']);
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-debit_notes']);
    echo $this->element('AdministrativeMovement/edit_administrative_movement', ['modal' => 'modal-edit-administrative-movement', 'title' => 'Editar']);
    echo $this->element('Email/send', ['modal'=> 'modal-send-email', 'title' => 'Seleccionar Plantilla', 'mass_emails_templates' => $mass_emails_templates ]);
?>

<script type="text/javascript">

    var table_debit_notes = null;
    var debit_note_selected  = null;

    var customer_selected = null;
    var move_selected = null;
    var table_selected = null;

    $(document).ready(function () {
        
         var options_business = [];
        
        $.each(sessionPHP.paraments.invoicing.business, function(i, val) {
            if(val.enable){
                options_business.push({id:val.id, name:val.name + ' (' + val.address + ')'});
            }
            
        });

        $('.btn-hide-column').click(function() {
           $('.modal-hide-columns-debit_notes').modal('show');
        });

        $('.btn-search-column').click(function() {
            $('.modal-search-columns-debit_notes').modal('show');
        });

        $('#table-debit_notes').removeClass('display');

        $('#btns-tools').hide();

        var hide_force_debitnotes = [];
        var no_search_debitnotes  = [];

        if (!sessionPHP.paraments.accountant.account_enabled) {
            hide_force_debitnotes = [21];
            no_search_debitnotes  = [21];
        }

        loadPreferences('debit_notes5-index', [4, 5, 8, 9, 11, 12, 13, 17, 18, 21]);

		table_debit_notes = $('#table-debit_notes').DataTable({
		    "order": [[ 0, 'desc' ]],
            "deferRender": true,
            "processing": true,
            "serverSide": true,
             "ajax": {
                "url": "/ispbrain/DebitNotes/get_debit_notes.json",
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "drawCallback": function( settings ) {
                var flag = true;
            	switch (settings.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
            	    if (table_debit_notes) {

                        var tableinfo = table_debit_notes.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }
                    }
            	}

            	this.api().columns('.total').every(function() {
                    var column = this;
                    
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    var sum = column
                        .data()
                        .reduce(function (a, b) { 

                           return intVal(a) + intVal(b);
                        }, 0);

                    $(column.footer()).html('$' + sum.toFixed(2));
                });

                this.api().columns('.subtotal').every(function() {
                    var column = this;

                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    var sum = column
                        .data()
                        .reduce(function (a, b) { 

                           return intVal(a) + intVal(b);
                        }, 0);

                    $(column.footer()).html('$' + sum.toFixed(2));
                });

                this.api().columns('.impuesto').every(function() {
                    var column = this;

                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    var sum = column
                        .data()
                        .reduce(function (a, b) { 

                           return intVal(a) + intVal(b);
                        }, 0);

                    $(column.footer()).html('$' + sum.toFixed(2));
                });
            },
		    "scrollY": true,
		    "scrollY": '370px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
		    "columns": [
                {
                    "className": "",
                    "data": "date",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        var date = data.split('T')[0];
                        date = date.split('-');
                        return date[2] + '/' + date[1] + '/' + date[0];
                    }
                },
                { 
                    "className": "",
                    "data": "tipo_comp",
                    "type": "options",
                    "options": {
                        "NDX": "NOTA DE DÉBITO X",
                        "002": "NOTA DE DÉBITO A",
                        "007": "NOTA DE DÉBITO B",
                        "012": "NOTA DE DÉBITO C"
                    },
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.comprobantes[data];
                    }
                },
                { 
                    "className": "",
                    "data": "pto_vta",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return  pad(data, 4);
                    }
                },
                { 
                    "className": "",
                    "data": "num",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return pad(data, 8);
                        }
                        return "";
                    }
                },
                { 
                    "className": "",
                    "data": "date_start",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        var date_start = data.split('T')[0];
                        date_start = date_start.split('-');
                        return date_start[2] + '/' + date_start[1] + '/' + date_start[0];
                    }
                },
                { 
                    "className": "",
                    "data": "date_end",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        var date_end = data.split('T')[0];
                        date_end = date_end.split('-');
                        return date_end[2] + '/' + date_end[1] + '/' + date_end[0];
                    }
                },
                { 
                    "className": " left",
                    "data": "comments",
                    "type": "string"
                },
                { 
                    "className": " text-info",
                    "data": "duedate",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        var duedate = data.split('T')[0];
                        duedate = duedate.split('-');
                        return duedate[2] + '/' + duedate[1] + '/' + duedate[0];
                    }
                },
                { 
                    "className": " right",
                    "data": "subtotal",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' )
                },
                { 
                    "className": " right",
                    "data": "sum_tax",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' )
                },
                { 
                    "className": " right text-info",
                    "data": "total",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' )
                },
                { 
                    "className": " left",
                    "data": "cae",
                    "type": "integer",
                },
                { 
                    "className": "",
                    "data": "vto"
                },
                { 
                    "className": " left",
                    "data": "user.username",
                    "type": "string"
                },
                { 
                    "className": "",
                    "data": "customer_code",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                {
                    "className": " center",
                    "data": "customer_doc_type",
                    "type": "options",
                    "options": sessionPHP.afip_codes.doc_types,
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.doc_types[data];
                    }
                },
                {
                    "className": " center",
                    "data": "customer_ident",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (data) {
                            ident = data;
                        }
                        return ident;
                    }
                },
                { 
                    "className": " left",
                    "data": "customer_name",
                    "type": "string"
                },
                { 
                    "className": " left",
                    "data": "customer_address",
                    "type": "string"
                },
                { 
                    "className": " left",
                    "data": "customer_city",
                    "type": "string"
                },
                { 
                    "data": "business_id",
                    "type": "options_obj",
                    "options": options_business,
                    "render": function ( data, type, row ) {
                        var business_name = '';
                        $.each(options_business, function (i, b) {
                            if (b.id == data) {
                                business_name = b.name;
                            }
                        });
                        return business_name;
                    }
                },
                {
                    "data": "seating_number",
                    "type": "string"
                },
            ],
		    "columnDefs": [
                { 
                    "visible": false, targets: hide_columns
                },
                { 
                    "visible": false, targets: hide_force_debitnotes
                },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
            "initComplete": function(settings, json) {},
		    "language": dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
        	"dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
		});

		$('.btn-update-table').click(function() {
             if (table_debit_notes) {
                table_debit_notes.ajax.reload();
            }
        });
 
        $('#table-debit_notes_wrapper .tools').append($('#btns-tools').contents());

        $('#table-debit_notes').on( 'init.dt', function () {
            createModalHideColumn(table_debit_notes, '.modal-hide-columns-debit_notes', hide_force_debitnotes);
            createModalSearchColumn(table_debit_notes, '.modal-search-columns-debit_notes', no_search_debitnotes);
        });

        $('#btns-tools').show();

        $('#table-debit_notes tbody').on( 'click', 'tr', function () {

            if (!$(this).closest('tr').find('.dataTables_empty').length) {

                table_debit_notes.$('tr.selected').removeClass('selected');
                $(this).closest('tr').addClass('selected');
                $('#btn-edit-administrative-movement').addClass('my-hidden');

                debit_note_selected = table_debit_notes.row( this ).data();
                if (debit_note_selected.customer.billing_for_service) {
                    move_selected = {
                        'id': debit_note_selected.id,
                        'model': 'DebitNotes'
                    };
                    customer_selected = {
                        code: debit_note_selected.customer_code
                    }
                    
                    if (debit_note_selected.tipo_comp == 'NDX') {
                        $('#btn-delete').show();
                    } else {
                        $('#btn-delete').hide();
                    }
                    
                    $('#btn-edit-administrative-movement').removeClass('my-hidden');
                }

                $('.modal-debit_notes').modal('show');
            }
        });
    });

    $('.btn-download').click(function() {

        var ids = [];

        table_debit_notes.rows({search: 'applied'}).every( function ( rowIdx, tableLoop, rowLoop ) {
            ids.push( this.data().id );
        });

        if (ids.length > 0) {

            $('#replacer').remove();

            $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/download/debit_notes/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'ids', 'value': ids}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#replacer').submit();
        } else {
            generateNoty('information', 'No hay notas de débito para descargar');
        }
    });

    $('#btn-go-customer').click(function() {
        var action = '/ispbrain/customers/view/' + debit_note_selected.customer_code;
        window.open(action, '_self');
    });

    $('#btn-print-debit_note').click(function() {
        var url = '/ispbrain/DebitNotes/showprint/' + table_debit_notes.$('tr.selected').attr('id');
        window.open(url, 'Imprimir Comprobante', "width=600, height=500");
    });

    $(".btn-export").click(function() {

        bootbox.confirm('Se descargará un archivo Excel', function(result) {
            if (result) {
                $('#table-debit_notes').tableExport({tableName: 'Notas de débito', type:'excel', escape:'false', columnNumber: [9, 10, 11, 12]});
            }
        }).find("div.modal-content").addClass("confirm-width-md");

    });

    $('#btn-edit-administrative-movement').click(function() {
        $('.modal-debit_notes').modal('hide');
        table_selected = table_debit_notes;
        $('.modal-edit-administrative-movement').modal('show');
    });

    $('#btn-send-email-debit-note').click(function() {
        if (debit_note_selected.customer.email == "" || debit_note_selected.customer.email == null) {
            generateNoty('warning', 'Debe agregar el Correo Eectrónico del Cliente al cual pertenece la Nota de Débito.');
        } else {
            var count = 0;
            $.each(mass_emails_templates, function( index, value ) {
                count++;
            });
            if (count > 1) {
                model = 'DebitNotes';
                model_id = debit_note_selected.id;
                $('.modal-debit_notes').modal('hide');
                $('.modal-send-email').modal('show');
            } else {
                generateNoty('warning', 'No cuenta con plantillas de Correos.');
            }
        }
    });

    $('#btn-delete').click(function() {

        $('.modal-debit_notes').modal('hide');

        bootbox.confirm("¿Está Seguro que desea eliminar la nota de débito?", function(result) {
        
            if (result) {
                    
                var request = $.ajax({
                    url: "<?= $this->Url->build(['controller' => 'DebitNotes', 'action' => 'delete']) ?>",  
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify(debit_note_selected),
                });
                
                request.done(function( data ) {
                  table_debit_notes.draw();
                });
                 
                request.fail(function( jqXHR, textStatus ) {
                  generateNoty('error', 'Error al intenetar eliminar la nota de débito');
                });
            }
        });
    });

</script>
