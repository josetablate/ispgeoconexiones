<div class="modal fade" id="modal-preloader" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body text-center">
                <h5 class="card-header" id="text-container"></h5>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function openModalPreloader(tex) {
        $('#modal-preloader #text-container').html(tex);
        $('#modal-preloader').modal('show');
    }

    function closeModalPreloader() {
        setTimeout(function() { 
            $('#modal-preloader').modal('hide');
        }, 900);
    }

    $(document).ready(function() {

        $('#modal-preloader').modal({
            keyboard: false,
            backdrop: 'static',
            show: false
        });
    })

</script>
