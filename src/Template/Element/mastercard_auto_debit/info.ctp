<tr>
    <th><?= __('Estado') ?></th>
    <td class="pull-right font-weight-bold <?= $account->deleted ? 'text-danger' : 'text-success' ?>"><?= $account->deleted ? ' <i class="fas fa-minus-circle"></i> Deshabilitada' : '<i class="far fa-check-circle"></i> Habilitada' ?></td>
</tr>
<tr>
    <th><?= __('Nro Tarjeta') ?></th>
    <td class="pull-right">
        <?= $account->card_number ?>
    </td>
</tr>
