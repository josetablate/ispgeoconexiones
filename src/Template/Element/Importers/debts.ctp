<style type="text/css">

    #table-debts td {
        margin: 0px 0px 0px 0px !important;
        padding: 0px 0px 0px 0px !important;
        vertical-align: middle;
    }

    #table-debts {
        width: 100% !important;
    }

    .modal-actions a.btn {
        width: 100%;
        margin-bottom: 5px;
    }

    tr.selected {
        background-color: #8eea99;
        color: #333 !important;
    }

    .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
        background-color: #d2d2d2;
    }

    #table-debts-without td {
        margin: 0px 0px 0px 0px !important;
        padding: 0px 0px 0px 0px !important;
        vertical-align: middle;
    }

    #table-debts-without {
        width: 100% !important;
    }

    .txt-letter {
        text-transform: uppercase;
    }

</style>

<div class="row">
    <div class="col-md-4" style="margin-top: 50px;">

        <div class="panel panel-default">
            <div class="panel-heading panel-debts-import"><?= __('3. Importar Deudas') ?></div>
            <div class="panel-body">
                <?= $this->Form->create(null, ['url' => ['controller' => 'importers' , 'action' => 'debtsPreview'], 'name' => 'debts-form', 'type' => 'file','role'=>'form']) ?>
                    <div class="form-group">
                        <label class="sr-only" for="csv"><?= __('CSV') ?></label>
                        <?php echo $this->Form->input('csv', ['type'=>'file', 'accept' => '.csv', 'class' => 'form-control', 'label' => false, 'placeholder' => __('csv upload'), 'required' => true]); ?>
                    </div>
                    <legend><?= __('Letra de Columna') ?></legend>
                    <?php $count = 0; ?>
                    <div class="row">
                        <?php foreach ($debtImport as $key => $item): ?>
                            <?php $placeholder = 'letra'; ?>
                            <?php if ($key == 'additional'): ?>
                                <?php $placeholder = 'letra, letra'; ?>
                            <?php endif; ?>
                            <?php if ($key != 'csv' && $key != 'comprobantes'): ?>
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <?php echo $this->Form->input($key, ['label' => __($key), 'placeholder' => __($placeholder), 'type' => 'text', 'class' => 'txt-letter', 'value' => $debtImport[$key]]); ?>
                                    <?php $count++; ?>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                    <legend><?= __('Valores por defecto') ?></legend>
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <?php echo $this->Form->input('comprobantes', ['options' => $comprobantes, 'label' => __('comprobantes'), 'value' => 'XXX']); ?>
                        </div>
    
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <button type="submit" class="btn btn-default btn-import"><?= __('Vista Previa') ?></button>
                        </div>
                    </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>

    <?= $this->element('Importers/debt_information') ?>
</div>

<div class="col-md-12" style="margin-top: 30px;">

    <legend><?= __('Tabla Deudas con Datos Completos') ?></legend>
    <div id="btns-tools-debts">
        <div class="text-right btns-tools margin-bottom-5" >
            <button id="btn-export-debts" type="button" class="btn btn-default btn-export" title="<?= __('Exportar en formato excel el contenido de la tabla') ?>" >
                <span class="glyphicon icon-file-excel" aria-hidden="true" ></span>
            </button>
        </div>
        <div class="text-right btns-tools margin-bottom-5" >
            <button id="btn-import-debts" type="button" class="btn btn-default btn-import" title="<?= __('Importar datos de la Tabla') ?>" >
                <span class="fas fa-upload" aria-hidden="true" ></span>
            </button>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-hover" id="table-debts" >
                <thead>
                    <tr>
                        <th><?= __('code') ?></th>        <!-- 0 -->
                        <th><?= __('ident') ?></th>       <!-- 1 -->
                        <th><?= __('name') ?></th>        <!-- 2 -->
                        <th><?= __('total') ?></th>       <!-- 3 -->
                        <th><?= __('concept') ?></th>     <!-- 4 -->
                        <th><?= __('created') ?></th>     <!-- 5 -->
                        <th><?= __('date_start') ?></th>  <!-- 6 -->
                        <th><?= __('date_end') ?></th>    <!-- 7 -->
                        <th><?= __('duedate') ?></th>     <!-- 8 -->
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

</div>

<div class="col-md-12" style="margin-top: 30px; margin-bottom: 50px;">

    <legend><?= __('Tabla Deudas con Datos Incompletos') ?></legend>
    <div id="btns-tools-debts-without">
        <div class="text-right btns-tools margin-bottom-5" >
            <button id="btn-export-debts-without" type="button" class="btn btn-default btn-export" title="<?= __('Exportar en formato excel el contenido de la tabla') ?>" >
                <span class="glyphicon icon-file-excel"  aria-hidden="true" ></span>
            </button>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-hover" id="table-debts-without" >
                <thead>
                    <tr>
                        <th><?= __('code') ?></th>        <!-- 0 -->
                        <th><?= __('ident') ?></th>       <!-- 1 -->
                        <th><?= __('name') ?></th>        <!-- 2 -->
                        <th><?= __('total') ?></th>       <!-- 3 -->
                        <th><?= __('concept') ?></th>     <!-- 4 -->
                        <th><?= __('created') ?></th>     <!-- 5 -->
                        <th><?= __('date_start') ?></th>  <!-- 6 -->
                        <th><?= __('date_end') ?></th>    <!-- 7 -->
                        <th><?= __('duedate') ?></th>     <!-- 8 -->
                        <th><?= __('Error') ?></th>       <!-- 9 -->
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

</div>

<script type="text/javascript">

   var table_debts = null;
   var table_debts_without = null;

   $(document).ready(function () {

    $('#table-debts').removeClass('display');
    $('#table-debts-without').removeClass('display');

		table_debts = $('#table-debts').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "ajax": "/ispbrain/Importers/debt_data.json",
		    "columns": [
                { 
                    "data": "code",
                },
                { 
                    "data": "ident",
                },
                { 
                    "data": "name",
                },
                { 
                    "data": "total",
                },
                { 
                    "data": "concept",
                },
                { 
                    "data": "date",
                },
                { 
                    "data": "date_start",
                },
                { 
                    "data": "date_end",
                },
                { 
                    "data": "duedate",
                },
            ],
		    "language": {
                "decimal":        "",
                "emptyTable":     "Sin resultados.",
                "info":           "_START_ al _END_ de _TOTAL_",
                "infoEmpty":      "0 al 0 de 0",
                "infoFiltered":   "",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ filas",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "Sin resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                    "sortDescending": ": Activar para ordenar la columna descendente"
                }
            },
            "lengthMenu": [[50, 75, 100, -1], [50, 75, 1000, "Todas"]],
            /*"dom": '<"toolbar">frtip'*/
		});

		$('#table-debts_filter').closest('div').before($('#btns-tools-debts').contents());

    $('#table-debts-without').removeClass('display');

		table_debts_without = $('#table-debts-without').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "ajax": "/ispbrain/Importers/debt_without_data.json",
		    "columns": [
                { 
                    "data": "code",
                },
                { 
                    "data": "ident",
                },
                { 
                    "data": "name",
                },
                { 
                    "data": "total",
                },
                { 
                    "data": "concept",
                },
                { 
                    "data": "date",
                },
                { 
                    "data": "date_start",
                },
                { 
                    "data": "date_end",
                },
                { 
                    "data": "duedate",
                },
                { 
                    "data": "error",
                },
            ],
		    "language": {
                "decimal":        "",
                "emptyTable":     "Sin resultados.",
                "info":           "_START_ al _END_ de _TOTAL_",
                "infoEmpty":      "0 al 0 de 0",
                "infoFiltered":   "",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ filas",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "Sin resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                    "sortDescending": ": Activar para ordenar la columna descendente"
                }
            },
            "lengthMenu": [[50, 75, 100, -1], [50, 75, 1000, "Todas"]],
            /*"dom": '<"toolbar">frtip'*/
		});

		$('#table-debts-without_filter').closest('div').before($('#btns-tools-debts-without').contents());
    });

    $('#table-debts-without tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_debts_without.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');

            $('.modal-actions').modal('show');
        }
    });

    $("#btn-export-debts").click(function() {
        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if (result) {
                $('#table-debts').tableExport({tableName: 'Deudas Datos Completos', type:'excel', escape:'false'});
            }
        });
    });

    $("#btn-export-debts-without").click(function() {
        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if (result) {
                $('#table-debts-without').tableExport({tableName: 'Deudas Datos Incompletos', type:'excel', escape:'false'});
            }
        });
    });

    $("#btn-import-debts").click(function() {
        var text = '¿Está Seguro que desea importar la Tabla de Deudas?';
        var controller = 'Importers';
        var id = table_debts.$('tr.selected').data('id');

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/debtsImport/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#replacer').submit();
            }
        });
    });

</script>
