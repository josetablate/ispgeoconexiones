<div class="col-md-8" style="margin-top: 50px;">
  <p class="lead"><?= __('Cosas a tener') ?> <span class="text-success"><?= __('EN CUENTA') ?></span></p>
  <ul class="list-unstyled" style="line-height: 2">
      <li><span class="fa fa-check text-success"></span> <?= __('Cuenta con una Vista Previa, la misma no genera nada en la base de datos') ?></li>
      <li><span class="fa fa-check text-success"></span> <?= __('Se mostrarán 2 tablas: una con los clientes(en caso de estar en la pestaña o sección de importación de clientes) con sus datos completos y otra con los clientes que falten datos necesarios para su carga, en caso de gestionar de otra manera permite exportar a CSV los datos de cualqueira de las 2 tablas, para su mejor manejo.') ?></li>
      <li><span class="fa fa-check text-success"></span> <?= __('Si no se carga la letra de la columna, toma un valor random los campos son: ip, excepto lat = null, lng = null y mac(null)') ?></li>
      <li><span class="fa fa-check text-success"></span> <?= __('Dar una letra a cada uno de los campos, sino dejar vacio y que tome el valor por defecto seleccionado.') ?></li>
      <li><span class="fa fa-check text-success"></span> <?= __('En caso que se quiera dar valor por defecto a ip(random), lat(null), lng(null), mac(null), dejar vacio el campo.') ?></li>
      <li><span class="fa fa-check text-success"></span> <?= __('Información adicional agregar en el campo additional letras separadas con coma, en caso de tener información adicional dejar vacio.') ?></li>
      <li><span class="fa fa-check text-success"></span> <?= __('Únicamente archivo con extensión') ?> <strong><?= __('CSV') ?></strong></li>
      <li><span class="fa fa-check text-success"></span> <?= __('EXPORTAR se realiza primero generando la VISTA PREVIA con los datos del archivo CSV, luego en la tabla de los datos completos en la parte superior tendrá 2 botones uno para exportar a CSV el contenido de la tabla y el otro para generar la IMPORTACIÓN de los datos contenidos en dicha tabla.') ?></li>
      <li><span class="fa fa-check text-success"></span> <?= __('RESULTADO: se genera en la base de datos los clientes cuyos datos estén completos. A parte se crean su respectiva Cuenta, así también a partir del nombre de la ciudad se matchea con la tabla Areas con el campo name si es el mismo le asigna el zone_id sino lo deja en NULL y posteriormente hay que cargar la ciudad en la base de datos.') ?></li>
      <li><span class="fa fa-check text-success"></span><?= __('GOOD LUCK!!!') ?></li>
  </ul>
</div>
