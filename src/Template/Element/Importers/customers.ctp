<style type="text/css">

    #table-customers td {
        margin: 0px 0px 0px 0px !important;
        padding: 0px 0px 0px 0px !important;
        vertical-align: middle;
    }

    #table-customers {
        width: 100% !important;
    }

    .modal-actions a.btn {
        width: 100%;
        margin-bottom: 5px;
    }

    tr.selected {
        background-color: #8eea99;
        color: #333 !important;
    }

    .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
        background-color: #d2d2d2;
    }

    #table-customers-without td {
        margin: 0px 0px 0px 0px !important;
        padding: 0px 0px 0px 0px !important;
        vertical-align: middle;
    }

    #table-customers-without {
        width: 100% !important;
    }

    .status {
        border-radius: 4px;
        display: inline-block;
        font-weight: bold;
        color: white;
        line-height: 1em;
        font-size: 10.5px;
        padding: 4px;
        margin: 0px 0px 0px 0px;
    }

    .cp {
        border: 1px solid #f38a73;
        background-color: #f38a73;
    }

    .cc {
        border: 1px solid gray;
        background-color: gray;    
    }

    .i {
        border: 1px solid purple;
        background-color: purple;
    }

    .plus1M {
        border: 1px solid #f05f40;
        background-color: #f05f40;
        width: 80%;
    }

    .plus2M {
        border: 1px solid #f05f40;
        background-color: #f05f40;
        width: 80%;
    }

    .plus3M {
        border: 1px solid #f05f40;
        background-color: #f05f40;
        width: 80%;
    }

    .txt-letter {
        text-transform: uppercase;
    }

</style>

<?php
    $status = [
        'plus1M' => ['class' => 'plus1M', 'text' => '+1 Mes'],
        'plus2M' => ['class' => 'plus2M', 'text' => '+2 Meses'],
        'plus3M' => ['class' => 'plus3M', 'text' => '+3 Meses'],
        'CP' => ['class' => 'cp', 'text' => 'Conexión Pendiente'],
        'CC' => ['class' => 'cc', 'text' => 'Conexión Creada'],
        'I' => ['class' => 'i', 'text' => 'Instalación Registrada']
    ];
?>
<div class="row">
    <div class="col-md-4" style="margin-top: 50px;">
    
        <div class="panel panel-default">
            <div class="panel-heading panel-customers-import"><?= __('1. Importar Clientes') ?></div>
            <div class="panel-body">
                <?= $this->Form->create(null, ['url' => ['controller' => 'importers' , 'action' => 'customersPreview'], 'name' => 'customers-form', 'type' => 'file','role'=>'form']) ?>
                    <div class="form-group">
                        <label class="sr-only" for="csv"><?= __('CSV') ?></label>
                        <?php echo $this->Form->input('csv', ['type'=>'file', 'accept' => '.csv', 'class' => 'form-control', 'label' => false, 'placeholder' => __('csv upload'), 'required' => true]); ?>
                    </div>
                    <legend><?= __('Letra de Columna') ?></legend>
                    <?php $count = 0; ?>
                    <div class="row">
                        <?php foreach ($customerImport as $key => $item): ?>
                            <?php $placeholder = 'letra'; ?>
                            <?php if ($key == 'additional'): ?>
                                <?php $placeholder = 'letra, letra'; ?>
                            <?php endif; ?>
                            <?php if ($key != 'csv' && $key != 'doc_types' && $key != 'responsible' && $key != 'cond_venta' && $key != 'is_presupuesto'): ?>
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <?php echo $this->Form->input($key, ['label' => __($key), 'placeholder' => __($placeholder), 'type' => 'text', 'class' => 'txt-letter', 'value' => $customerImport[$key]]); ?>
                                    <?php $count++; ?>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                    <legend><?= __('Valores por defecto') ?></legend>
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <?php echo $this->Form->input('doc_types', ['options' => $doc_types, 'label' => "doc_type", 'value' => $customerImport['doc_types']]); ?>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <?php echo $this->Form->input('responsible', ['label' => 'responsible', 'options' => $responsibles, 'value' => $customerImport['responsible']]); ?>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <?php echo $this->Form->input('cond_venta', ['label' => 'cond_venta', 'options' => $cond_venta, 'value' => $customerImport['cond_venta']]); ?>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <label><?= __('is_presupuesto') ?></label>
                            <?php echo $this->Form->checkbox('is_presupuesto', [
                                'value' => $customerImport['is_presupuesto'],
                                'checked' => $customerImport['is_presupuesto'],
                            ])?>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <button type="submit" class="btn btn-default btn-import"><?= __('Vista Previa') ?></button>
                        </div>
                    </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
    
    <?= $this->element('Importers/customer_information') ?>
</div>

<div class="col-md-12" style="margin-top: 30px;">

    <legend><?= __('Tabla Clientes con Datos Completos') ?></legend>
    <div id="btns-tools-customers">
        <div class="text-right btns-tools margin-bottom-5" >
            <button id="btn-clean-customers" type="button" class="btn btn-default btn-clean-customers" title="<?= __('Limpiar datos de la Tabla') ?>" >
                <span class="fa fa-eraser" aria-hidden="true" ></span>
            </button>
        </div>
        <div class="text-right btns-tools margin-bottom-5" >
            <button id="btn-export-customers" type="button" class="btn btn-default btn-export" title="<?= __('Exportar en formato excel el contenido de la tabla') ?>" >
                <span class="glyphicon icon-file-excel" aria-hidden="true" ></span>
            </button>
        </div>
        <div class="text-right btns-tools margin-bottom-5" >
            <button id="btn-import-customers" type="button" class="btn btn-default btn-import" title="<?= __('Importar datos de la Tabla') ?>" >
                <span class="fas fa-upload" aria-hidden="true" ></span>
            </button>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-hover" id="table-customers" >
                <thead>
                    <tr>
                        <th><?= __('code') ?></th>
                        <th><?= __('name') ?></th>
                        <th><?= __('doc_type') ?></th>
                        <th><?= __('ident') ?></th>
                        <th><?= __('plan_ask') ?></th>
                        <th><?= __('address') ?></th>
                        <th><?= __('zone_id') ?></th>
                        <th><?= __('phone') ?></th>
                        <th><?= __('email') ?></th>
                        <th><?= __('cond_venta') ?></th>
                        <th><?= __('responsible') ?></th>
                        <th><?= __('status') ?></th>
                        <th><?= __('comments') ?></th>
                        <th><?= __('is_presupuesto') ?></th>
                        <th><?= __('clave_portal') ?></th>
                        <th><?= __('created') ?></th>
                        <th><?= __('seller') ?></th>
                        <th><?= __('error') ?></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

</div>


<div class="col-md-12" style="margin-top: 30px; margin-bottom: 50px;">

    <legend><?= __('Tabla Clientes con Datos Incompletos') ?></legend>
    <div id="btns-tools-customers-without">
        <div class="text-right btns-tools margin-bottom-5" >
            <button id="btn-clean-customers-without" type="button" class="btn btn-default btn-clean-customers-without" title="<?= __('Limpiar datos de la Tabla') ?>" >
                <span class="fa fa-eraser" aria-hidden="true" ></span>
            </button>
        </div>
        <div class="text-right btns-tools margin-bottom-5" >
            <button id="btn-export-customers-without" type="button" class="btn btn-default btn-export" title="<?= __('Exportar en formato excel el contenido de la tabla') ?>" >
                <span class="glyphicon icon-file-excel"  aria-hidden="true" ></span>
            </button>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-hover" id="table-customers-without" >
                <thead>
                    <tr>
                        <th><?= __('code') ?></th>
                        <th><?= __('name') ?></th>
                        <th><?= __('doc_type') ?></th>
                        <th><?= __('ident') ?></th>
                        <th><?= __('plan_ask') ?></th>
                        <th><?= __('address') ?></th>
                        <th><?= __('zone_id') ?></th>
                        <th><?= __('phone') ?></th>
                        <th><?= __('email') ?></th>
                        <th><?= __('cond_venta') ?></th>
                        <th><?= __('responsible') ?></th>
                        <th><?= __('status') ?></th>
                        <th><?= __('comments') ?></th>
                        <th><?= __('is_presupuesto') ?></th>
                        <th><?= __('clave_portal') ?></th>
                        <th><?= __('created') ?></th>
                        <th><?= __('seller') ?></th>
                        <th><?= __('error') ?></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

</div>

<script type="text/javascript">

   var table_customers = null;
   var table_customers_without = null;

   $(document).ready(function () {

        $('#table-customers').removeClass('display');
        $('#table-customers-without').removeClass('display');

		table_customers = $('#table-customers').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "deferRender": true,
		    "ajax": "/ispbrain/Importers/customer_data.json",
		    "columns": [
                { 
                    "data": "code",
                },
                { 
                    "data": "name",
                },
                { 
                    "data": "doc_type",
                },
                { 
                    "data": "ident",
                },
                { 
                    "data": "plan_ask",
                },
                { 
                    "data": "address",
                },
                { 
                    "data": "zone_id",
                },
                { 
                    "data": "phone",
                },
                { 
                    "data": "email",
                },
                { 
                    "data": "cond_venta",
                },
                { 
                    "data": "responsible",
                },
                { 
                    "data": "status",
                },
                { 
                    "data": "comments",
                },
                { 
                    "data": "is_presupuesto",
                },
                { 
                    "data": "clave_portal",
                },
                { 
                    "data": "created",
                },
                { 
                    "data": "seller",
                },
                { 
                    "data": "error",
                },
            ],
		    "language": {
                "decimal":        "",
                "emptyTable":     "Sin resultados.",
                "info":           "_START_ al _END_ de _TOTAL_",
                "infoEmpty":      "0 al 0 de 0",
                "infoFiltered":   "",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ filas",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "Sin resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                    "sortDescending": ": Activar para ordenar la columna descendente"
                }
            },
            "lengthMenu": [[50, 75, 100, -1], [50, 75, 1000, "Todas"]],
            /*"dom": '<"toolbar">frtip'*/
		});

		$('#table-customers_filter').closest('div').before($('#btns-tools-customers').contents());

        $('#table-customers-without').removeClass('display');

		table_customers_without = $('#table-customers-without').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "ajax": "/ispbrain/Importers/customer_without_data.json",
		    "columns": [
                { 
                    "data": "code",
                },
                { 
                    "data": "name",
                },
                { 
                    "data": "doc_type",
                },
                { 
                    "data": "ident",
                },
                { 
                    "data": "plan_ask",
                },
                { 
                    "data": "address",
                },
                { 
                    "data": "zone_id",
                },
                { 
                    "data": "phone",
                },
                { 
                    "data": "email",
                },
                { 
                    "data": "cond_venta",
                },
                { 
                    "data": "responsible",
                },
                { 
                    "data": "status",
                },
                { 
                    "data": "comments",
                },
                { 
                    "data": "is_presupuesto",
                },
                { 
                    "data": "clave_portal",
                },
                { 
                    "data": "created",
                },
                { 
                    "data": "seller",
                },
                { 
                    "data": "error",
                }
            ],
		    "language": {
                "decimal":        "",
                "emptyTable":     "Sin resultados.",
                "info":           "_START_ al _END_ de _TOTAL_",
                "infoEmpty":      "0 al 0 de 0",
                "infoFiltered":   "",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ filas",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "Sin resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                    "sortDescending": ": Activar para ordenar la columna descendente"
                }
            },
            "lengthMenu": [[50, 75, 100, -1], [50, 75, 1000, "Todas"]],
            /*"dom": '<"toolbar">frtip'*/
		});

		$('#table-customers-without_filter').closest('div').before($('#btns-tools-customers-without').contents());
    });

    $('#table-customers-without tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_customers_without.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');

            $('.modal-actions').modal('show');
        }
    });

    $("#btn-export-customers").click(function() {
        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if (result) {
                $('#table-customers').tableExport({tableName: 'Clientes Datos Completos', type:'excel', escape:'false'});
            }
        });
    });

    $("#btn-clean-customers").click(function() {
        var text = '¿Desea limpiar la tabla de Clientes?';
        var controller = 'Importers';
        var file_name = 'customer_data';

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/cleanDatatableData/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'file_name', 'value': file_name}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#replacer').submit();
            }
        });
    });

    $("#btn-export-customers-without").click(function() {
        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if (result) {
                $('#table-customers-without').tableExport({tableName: 'Clientes Datos Incompletos', type:'excel', escape:'false'});
            }
        });
    });

    $("#btn-clean-customers-without").click(function() {
        var text = '¿Desea limpiar la tabla de Clientes con datos faltantes?';
        var controller = 'Importers';
        var file_name = 'customer_without_data';

        bootbox.confirm('¿Desea limpiar la tabla de Clientes con datos faltantes?', function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/cleanDatatableData/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'file_name', 'value': file_name}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#replacer').submit();
            }
        });
    });

    $("#btn-import-customers").click(function() {
        var text = '¿Está Seguro que desea importar la Tabla de Clientes?';
        var controller = 'Importers';
        var id = table_customers.$('tr.selected').data('id');

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/customersImport/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"})))
                .find('#replacer').submit();
            }
        });
    });

</script>
