<style type="text/css">

    #table-connections td {
        margin: 0px 0px 0px 0px !important;
        padding: 0px 0px 0px 0px !important;
        vertical-align: middle;
    }

    #table-connections {
        width: 100% !important;
    }

    .modal-actions a.btn {
        width: 100%;
        margin-bottom: 5px;
    }

    tr.selected {
        background-color: #8eea99;
        color: #333 !important;
    }

    .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
        background-color: #d2d2d2;
    }

    #table-connections-without td {
        margin: 0px 0px 0px 0px !important;
        padding: 0px 0px 0px 0px !important;
        vertical-align: middle;
    }

    #table-connections-without {
        width: 100% !important;
    }

</style>

<div class="row">
    <div class="col-md-4" style="margin-top: 50px;">
    
        <div class="panel card-default">
            <div class="panel-heading panel-connections-import"><?= __('2. Importar Conexiones') ?></div>
            <div class="panel-body">
                <?= $this->Form->create(null, ['url' => ['controller' => 'importers' , 'action' => 'connectionsPreview'], 'name' => 'customers-form', 'type' => 'file','role'=>'form']) ?>
                    <div class="form-group">
                        <label class="sr-only" for="csv"><?= __('CSV') ?></label>
                        <?php echo $this->Form->input('csv', ['type'=>'file', 'accept' => '.csv', 'class' => 'form-control', 'label' => false, 'placeholder' => __('csv upload'), 'required' => true]); ?>
                    </div>
                    <legend><?= __('Letra de Columna') ?></legend>
                    <?php $count = 0; ?>
                    <div class="row">
                    <?php foreach ($connectionImport as $key => $item): ?>
                        <?php $placeholder = 'letra'; ?>
                        <?php if ($key == 'additional'): ?>
                            <?php $placeholder = 'letra, letra'; ?>
                        <?php endif; ?>
                        <?php if ($key != 'csv'): ?>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <?php echo $this->Form->input($key, ['label' => __($key), 'placeholder' => __($placeholder), 'type' => 'text', 'class' => 'txt-letter', 'value' => $connectionImport[$key]]); ?>
                                <?php $count++; ?>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <button type="submit" class="btn btn-default btn-import"><?= __('Vista Previa') ?></button>
                    </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
    
    <?= $this->element('Importers/connection_information') ?>
</div>

<div class="col-md-12" style="margin-top: 30px;">

    <legend><?= __('Tabla Conexiones con Datos Completos') ?></legend>
    <div id="btns-tools-connections">
        <div class="text-right btns-tools margin-bottom-5" >
            <button id="btn-clean-connections" type="button" class="btn btn-default btn-clean-connections" title="<?= __('Limpiar datos de la Tabla') ?>" >
                <span class="fa fa-eraser" aria-hidden="true" ></span>
            </button>
        </div>
        <div class="text-right btns-tools margin-bottom-5" >
            <button id="btn-export-connections" type="button" class="btn btn-default btn-export" title="<?= __('Exportar en formato excel el contenido de la tabla') ?>" >
                <span class="glyphicon icon-file-excel" aria-hidden="true" ></span>
            </button>
        </div>
        <div class="text-right btns-tools margin-bottom-5" >
            <button id="btn-import-connections" type="button" class="btn btn-default btn-import" title="<?= __('Importar datos de la Tabla') ?>" >
                <span class="fas fa-upload" aria-hidden="true" ></span>
            </button>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-hover" id="table-connections" >
                <thead>
                    <tr>
                        <th><?= __('#') ?></th>
                        <th><?= __('address') ?></th>
                        <th><?= __('lat') ?></th>
                        <th><?= __('lng') ?></th>
                        <th><?= __('customer_code') ?></th>
                        <th><?= __('customer_name') ?></th>
                        <th><?= __('area_id') ?></th>
                        <th><?= __('controller_id') ?></th>
                        <th><?= __('service_id') ?></th>
                        <th><?= __('ip') ?></th>
                        <th><?= __('mac') ?></th>
                        <th><?= __('date') ?></th>
                        <th><?= __('comments') ?></th>
                        <th><?= __('error') ?></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

</div>

<div class="col-md-12" style="margin-top: 30px; margin-bottom: 50px;">

    <legend><?= __('Tabla Conexiones con Datos Incompletos') ?></legend>
    <div id="btns-tools-connections-without">
        <div class="text-right btns-tools margin-bottom-5" >
            <button id="btn-clean-connections-without" type="button" class="btn btn-default btn-clean-connections-without" title="<?= __('Limpiar datos de la Tabla') ?>" >
                <span class="fa fa-eraser" aria-hidden="true" ></span>
            </button>
        </div>
        <div class="text-right btns-tools margin-bottom-5" >
            <button id="btn-export-connections-without" type="button" class="btn btn-default btn-export" title="<?= __('Exportar en formato excel el contenido de la tabla') ?>" >
                <span class="glyphicon icon-file-excel" aria-hidden="true" ></span>
            </button>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-hover" id="table-connections-without" >
                <thead>
                    <tr>
                        <th><?= __('#') ?></th>
                        <th><?= __('address') ?></th>
                        <th><?= __('lat') ?></th>
                        <th><?= __('lng') ?></th>
                        <th><?= __('customer_code') ?></th>
                        <th><?= __('customer_name') ?></th>
                        <th><?= __('area_id') ?></th>
                        <th><?= __('controller_id') ?></th>
                        <th><?= __('service_id') ?></th>
                        <th><?= __('ip') ?></th>
                        <th><?= __('mac') ?></th>
                        <th><?= __('date') ?></th>
                        <th><?= __('comments') ?></th>
                        <th><?= __('error') ?></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

</div>

<script type="text/javascript">

   var table_connections = null;
   var table_connections_without = null;

   $(document).ready(function () {

        $('#table-connections').removeClass('display');
        $('#table-connections-without').removeClass('display');

		table_connections = $('#table-connections').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "ajax": "/ispbrain/Importers/connection_data.json",
		    "columns": [
                { 
                    "data": "number_row",
                },
                { 
                    "data": "address",
                },
                { 
                    "data": "lat",
                },
                { 
                    "data": "lng",
                },
                { 
                    "data": "customer_code",
                },
                { 
                    "data": "customer_name",
                },
                { 
                    "data": "area_id",
                },
                { 
                    "data": "controller_id",
                },
                { 
                    "data": "plan_id",
                },
                { 
                    "data": "ip",
                },
                { 
                    "data": "mac",
                },
                { 
                    "data": "date",
                },
                { 
                    "data": "comments",
                },
                { 
                    "data": "error",
                },
            ],
		    "language": {
                "decimal":        "",
                "emptyTable":     "Sin resultados.",
                "info":           "_START_ al _END_ de _TOTAL_",
                "infoEmpty":      "0 al 0 de 0",
                "infoFiltered":   "",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ filas",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "Sin resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Último",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                    "sortDescending": ": Activar para ordenar la columna descendente"
                }
            },
            "lengthMenu": [[50, 75, 100, -1], [50, 75, 1000, "Todas"]],
		});

		$('#table-connections_filter').closest('div').before($('#btns-tools-connections').contents());
		$('#table-connections-without').removeClass('display');

		table_connections_without = $('#table-connections-without').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "ajax": "/ispbrain/Importers/connection_without_data.json",
		    "columns": [
                { 
                    "data": "number_row",
                },
                { 
                    "data": "address",
                },
                { 
                    "data": "lat",
                },
                { 
                    "data": "lng",
                },
                { 
                    "data": "customer_code",
                },
                { 
                    "data": "customer_name",
                },
                { 
                    "data": "area_id",
                },
                { 
                    "data": "controller_id",
                },
                { 
                    "data": "plan_id",
                },
                { 
                    "data": "ip",
                },
                { 
                    "data": "mac",
                },
                { 
                    "data": "date",
                },
                { 
                    "data": "comments",
                },
                { 
                    "data": "error",
                },
            ],
		    "language": {
                "decimal":        "",
                "emptyTable":     "Sin resultados.",
                "info":           "_START_ al _END_ de _TOTAL_",
                "infoEmpty":      "0 al 0 de 0",
                "infoFiltered":   "",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ filas",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "Sin resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Último",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna ascendente",
                    "sortDescending": ": Activar para ordenar la columna descendente"
                }
            },
            "lengthMenu": [[50, 75, 100, -1], [50, 75, 1000, "Todas"]],
		});

		$('#table-connections-without_filter').closest('div').before($('#btns-tools-connections-without').contents());

    });

    $('#table-connections-without tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_connections_without.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');

            $('.modal-actions').modal('show');
        }
    });

    $("#btn-export-connections").click(function() {
        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if (result) {
                $('#table-connections').tableExport({tableName: 'Conexiones Datos Completos', type:'excel', escape:'false'});
            }
        });
    });

    $("#btn-clean-conections").click(function() {
        var text = '¿Desea limpiar la tabla de Conexiones?';
        var controller = 'Importers';
        var file_name = 'connection_data';

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/cleanDatatableData/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'file_name', 'value': file_name}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#replacer').submit();
            }
        });
    });

    $("#btn-export-connections-without").click(function() {
        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if (result) {
                $('#table-connections-without').tableExport({tableName: 'Conexiones Datos Incompletos', type:'excel', escape:'false'});
            }
        });
    });

    $("#btn-clean-connections-without").click(function() {
        var text = '¿Desea limpiar la tabla de Conexiones con datos faltantes?';
        var controller = 'Importers';
        var file_name = 'connection_without_data';

        bootbox.confirm('¿Desea limpiar la tabla de Conexiones con datos faltantes?', function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/cleanDatatableData/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'file_name', 'value': file_name}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#replacer').submit();
            }
        });
    });

    $("#btn-import-connections").click(function() {
        var text = '¿Está Seguro que desea importar la Tabla de Conexiones?';
        var controller = 'Importers';
        var id = table_connections.$('tr.selected').data('id');

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/connectionsImport/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"})))
                .find('#replacer').submit();
            }
        });
    });

</script>
