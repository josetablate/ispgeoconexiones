<style type="text/css">

    .sub-title {
        border-bottom: 1px solid #e3e2e2;
        width: 100%;
    }

    .my-hidden {
        display: none;
    }

    .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:active {
        color: #FF5722;
    }

    .modal a.btn {
        width: 100%;
        margin-bottom: 5px;
        text-align: left;
    }

    .disabled {
        cursor: not-allowed;
    }

    .textarea {
        margin-bottom: 0px;
    }

    #textarea_feedback {
        font-family: monospace;
        font-weight: bold;
        color: #696868;
        font-size: 15px;
    }

    #payment-commitment-comment {
        height: 90px !important;
    }

</style>

<div class="modal fade <?= $modal ?>" id="modal-edit-payment-commitment" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel"><?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <div class="row justify-content-md-center">
                            <?= $this->Form->create(NULL,  ['class' => 'form-load', 'id' => 'form-edit-paymemnt-commmitment']) ?>
                                <fieldset>
                                    <div class="row">
                                        <div class="col-5 ml-3">
                                            <label for="" class="label-control mt-3"><span class="red">*</span><?= __('Vencimiento') ?></label>
                                            <div class='input-group date' id='duedate-datetimepicker'>
                                                <input name='duedate' id="duedate-edit" type='text' class="form-control" required/>
                                                <span class="input-group-addon input-group-text calendar">
                                                    <span class="glyphicon icon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="col-6 ">
                                            <div class="form-group number">
                                                <label class="control-label mt-3" for="import">Importe</label>
                                                <input type="number" name="import" class="form-control" min="0.01" step="0.01" id="payment-commitment-edit-import">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xl-12">
                                        <?php
                                            echo $this->Form->input('comment', ['id' => 'payment-commitment-edit-comment', 'label' => 'Comentario', 'type' => 'textarea', 'rows' => 10, 'cols' => 80, 'maxlength' => 200, 'required' => TRUE]);
                                        ?>
                                    </div>

                                    <div class="col-xl-12">
                                        <div class="row">
                                            <div class="col-xl-4">
                                                <button type="button" class="btn btn-danger" id="btn-delete-payment-commitment">Eliminar</button>
                                            </div>
                                            <div class="col-xl-4">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                            </div>
                                            <div class="col-xl-4">
                                                <button type="submit" class="btn btn-primary" id="btn-edit-payment-commitment">Guardar</button>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var table_payment_commitment = null;
    var customer_selected = null;
    var table = null;
    var payment_commitment_selected = null;

    $(document).ready(function() {

        var defaultDate = new Date();

        // add a day
        defaultDate.setDate(defaultDate.getDate() + 1);

        $('#modal-edit-payment-commitment #duedate-datetimepicker').datetimepicker({
            defaultDate: defaultDate,
            format: 'DD/MM/YYYY',
            minDate: defaultDate,
            locale: 'es'
        });

        $('#form-edit-paymemnt-commmitment').submit(function(e) {

            e.preventDefault();

            var id = 0;
            var url = "/ispbrain/PaymentCommitment/edit.json";

            var data = {
                id: payment_commitment_selected.id,
                customer_code: customer_selected.code,
                duedate: $('#duedate-edit').val(),
                comment: $('#payment-commitment-edit-comment').val(),
                import: $('#payment-commitment-edit-import').val()
            };

            var request = $.ajax({
                url: url,
                method: "POST",
                data: JSON.stringify(data),
                dataType: "json"
            });

            request.done(function(response) {

                generateNoty(response.data.type, response.data.msg);
                table.draw();
                $('#modal-edit-payment-commitment').modal('hide');
            });

            request.fail(function(jqXHR) {

                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() { 
                        window.location.href = "/ispbrain";
                	}, 3000);

                } else {
                	generateNoty('error', 'Error al procesar el Compromiso de Pago');
                }
            });

        });
    });

    $('#modal-edit-payment-commitment').on('shown.bs.modal', function () {

        table = window.table_selected;

        var duedate = payment_commitment_selected.duedate.split('T')[0];
        duedate = duedate.split('-');
        duedate = duedate[2] + '/' + duedate[1] + '/' + duedate[0];
        $('#duedate-edit').val(duedate);
        $('#payment-commitment-edit-comment').val(payment_commitment_selected.detail);
        $('#payment-commitment-edit-import').val(payment_commitment_selected.import);
    });

    $("#btn-delete-payment-commitment").click(function(e) {

        e.stopImmediatePropagation();

        var text = '¿Está Seguro que desea eliminar el Compromiso de Pago?';
        var url = "/ispbrain/PaymentCommitment/delete.json";

    	bootbox.confirm(text, function(result) {

    		if (result) {

    			var data = {
                    id: payment_commitment_selected.id,
                    customer_code: customer_selected.code
                };

                var request = $.ajax({
                    url: url,
                    method: "POST",
                    data: JSON.stringify(data),
                    dataType: "json"
                });

                request.done(function(response) {

                    generateNoty(response.data.type, response.data.msg);
                    table_payment_commitment.draw();

                    $('.btn-add-payment_commitment').removeClass('d-none');
                    $('#modal-edit-payment-commitment').modal('hide');
                });

                request.fail(function(jqXHR) {

                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {

                        $('#modal-edit-payment-commitment').modal('hide');
                    	generateNoty('error', 'Error al intentar eliminar el Compromiso de Pago');
                    }
                });
    		}
    	});
    });

</script>
