<style type="text/css">

    .sub-title {
        border-bottom: 1px solid #e3e2e2;
        width: 100%;
    }

    .my-hidden {
        display: none;
    }

    .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:active {
        color: #FF5722;
    }

    .modal a.btn {
        width: 100%;
        margin-bottom: 5px;
        text-align: left;
    }

    .disabled {
        cursor: not-allowed;
    }

    .textarea {
        margin-bottom: 0px;
    }

    #textarea_feedback {
        font-family: monospace;
        font-weight: bold;
        color: #696868;
        font-size: 15px;
    }

    #payment-commitment-comment {
        height: 90px !important;
    }

</style>

<div class="modal fade <?= $modal ?>" id="modal-payment-commitment" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel"><?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <div class="row justify-content-md-center">
                            <?= $this->Form->create(NULL,  ['class' => 'form-load', 'id' => 'form-paymemnt-commmitment']) ?>
                                <fieldset>
                                    <div class="row">
                                        <div class="col-5 ml-3">
                                            <label for="" class="label-control mt-3"><span class="red">*</span><?= __('Vencimiento') ?></label>
                                            <div class='input-group date' id='duedate-datetimepicker'>
                                                <input name='duedate' id="duedate" type='text' class="form-control" required/>
                                                <span class="input-group-addon input-group-text calendar">
                                                    <span class="glyphicon icon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="col-6 ">
                                            <div class="form-group number">
                                                <label class="control-label mt-3" for="import">Importe</label>
                                                <input type="number" name="import" class="form-control" min="0.01" step="0.01" id="payment-commitment-import">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xl-12">
                                        <?php
                                            echo $this->Form->input('comment', ['id' => 'payment-commitment-comment', 'label' => 'Comentario', 'type' => 'textarea', 'rows' => 10, 'cols' => 80, 'maxlength' => 200, 'required' => TRUE]);
                                        ?>
                                    </div>

                                    <div class="col-xl-12">
                                        <div class="row">
                                            <div class="col-xl-4">
                                                <button type="button" class="d-none btn btn-danger btn-delete-payment-commitment-form">Eliminar</button>
                                            </div>
                                            <div class="col-xl-4">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                            </div>
                                            <div class="col-xl-4">
                                                <button type="submit" class="btn btn-primary" id="btn-add-payment-commitment">Guardar</button>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>

                    <?php if ($records): ?>

                        <div class="col-xl-12">

                            <div id="btns-tools-payment-commitment" class="mt-3 d-none">

                                <div class="text-right btns-tools margin-bottom-5">

                                    <?php

                                        echo $this->Html->link(
                                            '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                                            'javascript:void(0)',
                                            [
                                            'title' => 'Exportar tabla',
                                            'class' => 'btn btn-default btn-export-payment-commitment ml-1 mt-1',
                                            'escape' => false
                                        ]);

                                    ?>

                                </div>
                            </div>

                            <table class="table table-bordered table-hover" id="table-payment-commitment">
                                <thead>
                                    <tr>
                                        <th></th>            <!--0-->
                                        <th>Fecha</th>       <!--1-->
                                        <th>Detalle</th>     <!--2-->
                                        <th>Vencimiento</th> <!--3-->
                                        <th>Importe</th>     <!--4-->
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-delete-payment-commitment-popup',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-action-payment-commitment', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<script type="text/javascript">

    var records = null;
    var customer_selected = null;
    var table_payment_commitment = null;
    var table = null;
    var payment_commitment_selected = null;

    $(document).ready(function() {

        records = <?= json_encode($records) ?>;

        var defaultDate = new Date();

        // add a day
        defaultDate.setDate(defaultDate.getDate() + 1);

        $('#modal-payment-commitment #duedate-datetimepicker').datetimepicker({
            defaultDate: defaultDate,
            format: 'DD/MM/YYYY',
            minDate: defaultDate,
            locale: 'es'
        });

        if (records) {
            //crear funcion que busque historial

            $('#table-payment-commitment').removeClass('display');

            $('#btns-tools-table-payment-commitment').hide();

            table_payment_commitment = $('#table-payment-commitment').DataTable({
                "order": [[ 1, 'asc' ]],
                "autoWidth": false,
        	    "scrollY": '450px',
        	    "scrollCollapse": true,
        	    "scrollX": true,
                "processing": true,
                "serverSide": true,
    		    "paging": true,
    		    "ordering": true,
    		    "destroy": true,
                "ajax": {
                    "url": "/ispbrain/PaymentCommitment/getMovements.json",
                    "data": function ( d ) {
                        return $.extend( {}, d, {
                            "where": {
                                "customer_code": customer_selected ? customer_selected.code : 0,
                            }
                        });
                    },
                    "dataFilter": function( data ) {
                        var json = $.parseJSON( data );
                        return JSON.stringify( json.response );
                    },
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                				setTimeout(function() { 
                                    window.location.href = "/ispbrain";
                				}, 3000);
                				break;
                		}
                	},
                },
                "drawCallback": function( settings ) {
                    var flag = true;
                	switch (settings.jqXHR.status) {
                		case 400:
                			flag = false;
                			break;
                		case 401:
                			flag = false;
                			break;
                		case 403:
                			flag = false;
                			break;
                	}
                },
    		    "columns": [
    		        { 
                        "data": "created",
                        "render": function ( data, type, row ) {
                            var enabled = '<i class="far fa-money-bill-alt fa-2x" aria-hidden="true"></i>';
                            if (row.duedate) {
                                enabled = '<i class="far fa-handshake fa-2x" aria-hidden="true"></i>';
                            }
                            return enabled;
                        }
                    },
    		        { 
                        "data": "created",
                        "type": "date",
                        "render": function ( data, type, row ) {
                            if (data) {
                                var created = data.split('T')[0];
                                created = created.split('-');
                                return created[2] + '/' + created[1] + '/' + created[0];
                            }
                        }
                    },
                    {
                       "className": " left",
                        "data": "detail",
                        "type": "string"
                    },
                    {
                        "data": "duedate",
                        "type": "date",
                        "render": function ( data, type, row ) {
                            var created = "";
                            if (data) {
                                created = data.split('T')[0];
                                created = created.split('-');
                                created = created[2] + '/' + created[1] + '/' + created[0];
                            }
                            return created;
                        }
                    },
                    { 
                        "class": "right",
                        "data": "import",
                        "type": "decimal",
                        "render": function ( data, type, row ) {
                            if (data) {
                                return number_format(data, true);
                            }
                            return "";
                        }
                    },
                ],
    		    "columnDefs": [
    		        { "width": "5%", "targets": 0 },
    		        { "width": "5%", "targets": 1 },
    		        { "width": "70%", "targets": 2 },
    		        { "width": "5%", "targets": 3 },
    		        { "width": "5%", "targets": 4 },
                ],
                "createdRow" : function( row, data, index ) {
                    row.id = data.id;
                },
    		    "language": dataTable_lenguage,
    		    "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            	"dom":
        	    	"<'row'<'col-xl-2 title'><'col-xl-3'l><'col-xl-4'f><'col-xl-3 tools'>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});

    		$('#table-payment-commitment_wrapper .title').append('<h5>Historial</h5>');

            $('#table-payment-commitment_wrapper .tools').append($('#btns-tools-payment-commitment').contents().clone());

            $('#btns-tools-table-payment-commitment').show();

            $('#table-payment-commitment tbody').on( 'click', 'tr', function () {

                if (!$(this).find('.dataTables_empty').length) {

                    table_payment_commitment.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');

                    payment_commitment_selected = table_payment_commitment.row( this ).data();

                    if (payment_commitment_selected.duedate != null) {

                        if (new Date(payment_commitment_selected.duedate) > new Date()) {

                            var duedate = payment_commitment_selected.duedate.split('T')[0];
                            duedate = duedate.split('-');
                            duedate = duedate[2] + '/' + duedate[1] + '/' + duedate[0];
                            $('.btn-delete-payment-commitment-form').removeClass('d-none');
                            $('#duedate').val(duedate);
                            $('#payment-commitment-comment').val(payment_commitment_selected.detail);
                            $('#payment-commitment-import').val(payment_commitment_selected.import);
                            $('#form-paymemnt-commmitment').removeClass('d-none');
                        } else {

                            $('#form-paymemnt-commmitment').addClass('d-none');
                            $('.btn-delete-payment-commitment-form').addClass('d-none');
                            $('.modal-action-payment-commitment').modal('show');
                        }
                    } else {

                        $('.btn-delete-payment-commitment-form').addClass('d-none');
                        $('#form-paymemnt-commmitment').addClass('d-none');
                    }
                }
            });

            $(".btn-export-payment-commitment").click(function() {

                bootbox.confirm('Se descargará un archivo Excel', function(result) {
                    if (result) {
                        $('#table-payment-commitment').tableExport({tableName: 'Compromiso de Pagos', type:'excel', escape:'false'});
                    }
                }).find("div.modal-content").addClass("confirm-width-md");
            });

            function deletePyamentCommitment(e) {

                e.stopImmediatePropagation();

                var text = '¿Está Seguro que desea eliminar el Compromiso de Pago?';
                var url = "/ispbrain/PaymentCommitment/delete.json";

            	bootbox.confirm(text, function(result) {

            		if (result) {

            			var data = {
                            id: payment_commitment_selected.id,
                            customer_code: customer_selected.code
                        };

                        var request = $.ajax({
                            url: url,
                            method: "POST",
                            data: JSON.stringify(data),
                            dataType: "json"
                        });

                        request.done(function(response) {

                            generateNoty(response.data.type, response.data.msg);
                            payment_commitment_selected = null;
                            table_payment_commitment.draw();
                            table.draw();

                            var defaultDate = new Date();

                            // add a day
                            defaultDate.setDate(defaultDate.getDate() + 1);

                            $('#duedate').val(moment(defaultDate).format('DD/MM/YYYY'));
                            $('#payment-commitment-comment').val("");
                            $('#payment-commitment-import').val("");
                            $('.btn-delete-payment-commitment-form').addClass('d-none');
                            $('.modal-action-payment-commitment').modal('hide');
                        });

                        request.fail(function(jqXHR) {

                            if (jqXHR.status == 403) {

                            	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                            	setTimeout(function() { 
                                    window.location.href = "/ispbrain";
                            	}, 3000);

                            } else {
                                $('.modal-action-payment-commitment').modal('hide');
                            	generateNoty('error', 'Error al intentar eliminar el Compromiso de Pago');
                            }
                        });
            		}
            	});
            };

            $("#btn-delete-payment-commitment-popup").click(function(e) {
                deletePyamentCommitment(e);
            });

            $(".btn-delete-payment-commitment-form").click(function(e) {
                deletePyamentCommitment(e);
            });
        }

        $('#form-paymemnt-commmitment').submit(function(e) {

            $('#btn-add-payment-commitment').addClass('d-none');

            e.preventDefault();

            var id = 0;
            var url = "/ispbrain/PaymentCommitment/add.json";

            var data = {
                customer_code: customer_selected.code,
                duedate: $('#duedate').val(),
                comment: $('#payment-commitment-comment').val(),
                import: $('#payment-commitment-import').val()
            };

            if (payment_commitment_selected != null) {
                id = payment_commitment_selected.id;
                url = "/ispbrain/PaymentCommitment/edit.json";
                data.id = id;
            }

            var request = $.ajax({
                url: url,
                method: "POST",
                data: JSON.stringify(data),
                dataType: "json"
            });

            request.done(function(response) {

                generateNoty(response.data.type, response.data.msg);
                table.draw();
                $('#btn-add-payment-commitment').removeClass('d-none');
                $('#modal-payment-commitment').modal('hide');
            });

            request.fail(function(jqXHR) {

                $('#btn-add-payment-commitment').removeClass('d-none');

                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() { 
                        window.location.href = "/ispbrain";
                	}, 3000);

                } else {
                	generateNoty('error', 'Error al procesar el Compromiso de Pago');
                }
            });

        });
    });

    $('#modal-payment-commitment').on('shown.bs.modal', function () {

        customer_selected = window.customer_selected;
        payment_commitment_selected = null;
        table = window.table_selected;

        var duedate = new Date();
        duedate.setDate(duedate.getDate() + 1);
        var month = duedate.getMonth() + 1;

        duedate = duedate.getDate() + '/' + month + '/' + duedate.getFullYear();
        $('#duedate').val(duedate);
        $('#payment-commitment-comment').val("");
        $('#payment-commitment-import').val("");

        if (customer_selected.payment_commitment == null) {
            $('.btn-delete-payment-commitment-form').addClass('d-none');
            $('#form-paymemnt-commmitment').removeClass('d-none');
        } else {
            $('#form-paymemnt-commmitment').addClass('d-none');
        }

        if (records) {
            table_payment_commitment.ajax.reload();
        }
    });

</script>
