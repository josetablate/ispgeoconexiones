<style type="text/css">

    .sub-title {
        border-bottom: 1px solid #e3e2e2;
        width: 100%;
    }

    .my-hidden {
        display: none;
    }

    .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:active {
        color: #FF5722;
    }

    .modal a.btn {
        width: 100%;
        margin-bottom: 5px;
        text-align: left;
    }

    .disabled {
        cursor: not-allowed;
    }

    .textarea {
        margin-bottom: 0px;
    }

    #textarea_feedback {
        font-family: monospace;
        font-weight: bold;
        color: #696868;
        font-size: 15px;
    }

    #payment-commitment-comment {
        height: 90px !important;
    }

</style>

<div class="modal fade <?= $modal ?>" id="modal-add-payment-commitment" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel"><?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <div class="row justify-content-md-center">
                            <?= $this->Form->create(NULL,  ['class' => 'form-load', 'id' => 'form-paymemnt-commmitment']) ?>
                                <fieldset>
                                    <div class="row">
                                        <div class="col-5 ml-3">
                                            <label for="" class="label-control mt-3"><span class="red">*</span><?= __('Vencimiento') ?></label>
                                            <div class='input-group date' id='duedate-datetimepicker'>
                                                <input name='duedate' id="duedate-add" type='text' class="form-control" required/>
                                                <span class="input-group-addon input-group-text calendar">
                                                    <span class="glyphicon icon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="col-6 ">
                                            <div class="form-group number">
                                                <label class="control-label mt-3" for="import">Importe</label>
                                                <input type="number" name="import" class="form-control" min="0.01" step="0.01" id="payment-commitment-import">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xl-12">
                                        <?php
                                            echo $this->Form->input('comment', ['id' => 'payment-commitment-comment', 'label' => 'Comentario', 'type' => 'textarea', 'rows' => 10, 'cols' => 80, 'maxlength' => 200, 'required' => TRUE]);
                                        ?>
                                    </div>

                                    <div class="col-xl-12">
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                            </div>
                                            <div class="col-xl-6">
                                                <button type="submit" class="btn btn-primary" id="btn-add-payment-commitment">Guardar</button>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var table_payment_commitment = null;
    var customer_selected = null;
    var table = null;
    var payment_commitment_selected = null;

    $(document).ready(function() {

        var defaultDate = new Date();

        // add a day
        defaultDate.setDate(defaultDate.getDate() + 1);

        $('#modal-add-payment-commitment #duedate-datetimepicker').datetimepicker({
            defaultDate: defaultDate,
            format: 'DD/MM/YYYY',
            minDate: defaultDate,
            locale: 'es'
        });

        $('#form-paymemnt-commmitment').submit(function(e) {

            $('#btn-add-payment-commitment').addClass('d-none');

            e.preventDefault();

            //var id = 0;
            var url = "/ispbrain/PaymentCommitment/add.json";

            var data = {
                customer_code: customer_selected.code,
                duedate: $('#duedate-add').val(),
                comment: $('#payment-commitment-comment').val(),
                import: $('#payment-commitment-import').val()
            };

            // if (payment_commitment_selected != null) {
            //     id = payment_commitment_selected.id;
            //     url = "/ispbrain/PaymentCommitment/edit.json";
            //     data.id = id;
            // }

            var request = $.ajax({
                url: url,
                method: "POST",
                data: JSON.stringify(data),
                dataType: "json"
            });

            request.done(function(response) {

                generateNoty(response.data.type, response.data.msg);
                table.draw();
                $('.btn-add-payment_commitment').addClass('d-none');
                $('#modal-add-payment-commitment').modal('hide');
            });

            request.fail(function(jqXHR) {

                $('#btn-add-payment-commitment').removeClass('d-none');

                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() { 
                        window.location.href = "/ispbrain";
                	}, 3000);

                } else {
                	generateNoty('error', 'Error al procesar el Compromiso de Pago');
                }
            });

        });
    });

    $('#modal-add-payment-commitment').on('shown.bs.modal', function () {

        customer_selected = window.customer_selected;
        table = window.table_selected;

        var duedate = new Date();
        duedate.setDate(duedate.getDate() + 1);
        var month = duedate.getMonth() + 1;

        duedate = duedate.getDate() + '/' + month + '/' + duedate.getFullYear();
        $('#duedate-add').val(duedate);
        $('#payment-commitment-comment').val("");
        $('#payment-commitment-import').val("");
        $('#btn-add-payment-commitment').removeClass('d-none');
    });

</script>
