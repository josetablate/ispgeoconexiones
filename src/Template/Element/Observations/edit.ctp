<style type="text/css">

    .sub-title {
        border-bottom: 1px solid #e3e2e2;
        width: 100%;
    }

    .my-hidden {
        display: none;
    }

    .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:active {
        color: #FF5722;
    }

    .modal a.btn {
        width: 100%;
        margin-bottom: 5px;
        text-align: left;
    }

    .disabled {
        cursor: not-allowed;
    }

    .textarea {
        margin-bottom: 0px;
    }

    #textarea_feedback {
        font-family: monospace;
        font-weight: bold;
        color: #696868;
        font-size: 15px;
    }

    #observation-comment {
        height: 149px !important;
    }

</style>

<div class="modal fade <?= $modal ?>" id="modal-edit-observation" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel"><?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-xl-12">
                        <?php
                            echo $this->Form->input('comment', ['id' => 'observation-comment-edit', 'label' => 'Comentario', 'type' => 'textarea', 'rows' => 10, 'cols' => 80, 'maxlength' => 200, 'required' => 'required']);
                        ?>
                    </div>

                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger mr-auto" id="btn-delete-observation">Eliminar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btn-edit-observation-modal">Guardar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var observation = null;
    var table = null;
    var editor_edit  = null;

    $(document).ready(function() {

        editor_edit = CKEDITOR.replace( 'observation-comment-edit' );

        CKEDITOR.config.height = 350;
        CKEDITOR.config.width = 'auto';
        CKEDITOR.config.title = false;
        CKEDITOR.config.toolbar = [
        	{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo' ] },
        	{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ] },
        	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent' ] },
        	{ name: 'styles', items: [ 'Format' ] },
        ];

        $('#btn-edit-observation-modal').click(function() {
            if (editor_edit.getData().length == 0) {
                generateNoty('warning', 'Debe ingresar un comentario.');
            } else {
                var request = $.ajax({
                    url: "/ispbrain/Observations/edit.json",
                    method: "POST",
                    data: JSON.stringify({
                        id: observation.id,
                        comment: editor_edit.getData()
                    }),
                    dataType: "json"
                });

                request.done(function(response) {

                    generateNoty(response.data.type, response.data.msg);
                    table.draw();
                    editor.setData('');
                    $('#modal-edit-observation').modal('hide');
                });

                request.fail(function(jqXHR) {

                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al traer los servicios');
                    }
                });
            }
        });
    });

    $('#btn-delete-observation').click(function() {

        var text = "¿Está Seguro que desea eliminar la observación?";
        var id  = observation.id;

        bootbox.confirm(text, function(result) {

            if (result) {

                var request = $.ajax({
                    url: "/ispbrain/Observations/delete.json",
                    method: "POST",
                    data: JSON.stringify({
                        id: id,
                    }),
                    dataType: "json"
                });

                request.done(function(response) {

                    generateNoty(response.data.type, response.data.msg);
                    table.draw();
                    $('#modal-edit-observation').modal('hide');
                });

                request.fail(function(jqXHR) {

                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al traer los servicios');
                    }
                });
            }
        });
    });

    $('#modal-edit-observation').on('shown.bs.modal', function () {
        observation = window.observation_selected;
        table = window.table_selected;
        editor_edit.setData(observation.comment);
    });

</script>
