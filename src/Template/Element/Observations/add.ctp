<style type="text/css">

    .sub-title {
        border-bottom: 1px solid #e3e2e2;
        width: 100%;
    }

    .my-hidden {
        display: none;
    }

    .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:active {
        color: #FF5722;
    }

    .modal a.btn {
        width: 100%;
        margin-bottom: 5px;
        text-align: left;
    }

    .disabled {
        cursor: not-allowed;
    }

    .textarea {
        margin-bottom: 0px;
    }

    #textarea_feedback {
        font-family: monospace;
        font-weight: bold;
        color: #696868;
        font-size: 15px;
    }

    #observation-comment {
        height: 90px !important;
    }

    .btn-export-observations {
        width: 25% !important;
    }

    #table-observations_wrapper .title {
        padding-top: 8px;
    }

</style>

<div class="modal fade <?= $modal ?>" id="modal-add-observation" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel"><?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-xl-12">
                        <?php
                            echo $this->Form->input('comment', ['id' => 'observation-comment', 'label' => 'Comentario', 'type' => 'textarea', 'rows' => 10, 'cols' => 80, 'maxlength' => 200, 'required' => 'required']);
                        ?>
                    </div>

                    <div class="col-xl-12">
                        <div class="row">
                            <div class="col-xl-6">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            </div>
                            <div class="col-xl-6">
                                <button type="button" class="btn btn-primary" id="btn-add-observation">Agregar</button>
                            </div>
                        </div>
                    </div>

                    <?php if ($records): ?>

                        <div class="col-xl-12">

                            <div id="btns-tools-observations" class="mt-3 d-none">

                                <div class="text-right btns-tools margin-bottom-5">

                                    <?php

                                        echo $this->Html->link(
                                            '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                                            'javascript:void(0)',
                                            [
                                            'title' => 'Exportar tabla',
                                            'class' => 'btn btn-default btn-export-observations ml-1 mt-1',
                                            'escape' => false
                                        ]);

                                    ?>

                                </div>
                            </div>

                            <table class="table table-bordered table-hover" id="table-observations">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>      <!--0-->
                                        <th>Comentario</th> <!--1-->
                                        <th>Usuario</th>    <!--2-->
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var records = null;
    var customer_code = null;
    var table = null;
    var editor  = null;
    var table_observations = null;

    $(document).ready(function() {

        records = <?= json_encode($records) ?>;

        editor = CKEDITOR.replace( 'observation-comment', {
             height: 180
        });

        CKEDITOR.config.height = '100px';
        CKEDITOR.config.width = 'auto';
        CKEDITOR.config.title = false;
        CKEDITOR.config.toolbar = [
        	{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo' ] },
        	{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ] },
        	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent' ] },
        	{ name: 'styles', items: [ 'Format' ] },
        ];

        $('#btn-add-observation').click(function() {
            if (editor.getData().length == 0) {
                generateNoty('warning', 'Debe ingresar un comentario.');
            } else {
                var request = $.ajax({
                    url: "/ispbrain/Observations/add.json",
                    method: "POST",
                    data: JSON.stringify({
                        customer_code: customer_code,
                        comment: editor.getData()
                    }),
                    dataType: "json"
                });

                request.done(function(response) {

                    generateNoty(response.data.type, response.data.msg);
                    editor.setData('');

                    if (records) {
                        table_observations.draw();
                    } else {
                        table.draw();
                        $('#modal-add-observation').modal('hide');
                    }
                });

                request.fail(function(jqXHR) {

                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al traer los servicios');
                    }
                });
            }
        });
    });

    $('#modal-add-observation').on('shown.bs.modal', function () {

        customer_code = window.customer_code;
        table = window.table_selected;

        if (records) {
            //crear funcion que busque historial
            users = <?= json_encode($users) ?>;

            $('#table-observations').removeClass('display');

            $('#btns-tools-observations').hide();

            table_observations = $('#table-observations').DataTable({
                "order": [[ 1, 'desc' ]],
                "autoWidth": false,
        	    "scrollY": '450px',
        	    "scrollCollapse": true,
        	    "scrollX": true,
                "processing": true,
                "serverSide": true,
    		    "paging": true,
    		    "ordering": true,
    		    "destroy": true,
                "ajax": {
                    "url": "/ispbrain/Observations/index.json",
                    "data": function ( d ) {
                        return $.extend( {}, d, {
                            "customer_code": customer_code,
                            "main": 0
                        });
                    },
                    "dataFilter": function( data ) {
                        var json = $.parseJSON( data );
                        return JSON.stringify( json.response );
                    },
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                				setTimeout(function() { 
                                    window.location.href = "/ispbrain";
                				}, 3000);
                				break;
                		}
                	},
                },
                "drawCallback": function( settings ) {
                    var flag = true;
                	switch (settings.jqXHR.status) {
                		case 400:
                			flag = false;
                			break;
                		case 401:
                			flag = false;
                			break;
                		case 403:
                			flag = false;
                			break;
                	}
                },
    		    "columns": [
    		        {
                        "data": "created",
                        "type": "date",
                        "render": function ( data, type, row ) {
                            if (data) {
                                var date = data.split('T')[0];
                                date = date.split('-');
                                var other = data.split('T')[1];
                                other = other.split('-')[0];
                                other = other.split(':');
                                return date[2] + '/' + date[1] + '/' + date[0] + ' ' + other[0] + ':' + other[1];
                            }
                        }
                    },
                    {
                       "className": " left",
                        "data": "comment",
                        "type": "string"
                    },
                    {
                        "data": "user_id",
                        "options": users,
                        "render": function ( data, type, row ) {
                            var u = "";
                            if (typeof data !== "undefined") {
                                $.each(users, function( index, value ) {
                                    if (data == index) {
                                        u = value;
                                    }
                                });
                            }
                            return u;
                        }
                    },
                ],
    		    "columnDefs": [
    		        { "width": "10%", "targets": 0 },
    		        { "width": "10%", "targets": 2 }
                ],
                "createdRow" : function( row, data, index ) {
                    row.id = data.id;
                },
    		    "language": dataTable_lenguage,
    		    "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            	"dom":
        	    	"<'row'<'col-xl-2 title'><'col-xl-3'l><'col-xl-4'f><'col-xl-3 tools'>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});

    		$('#table-observations_wrapper .title').append('<h5>Historial</h5>');

            $('#table-observations_wrapper .tools').append($('#btns-tools-observations').contents().clone());

            $('#btns-tools-observations').show();

            $('#table-observations tbody').on( 'click', 'tr', function () {

                if (!$(this).find('.dataTables_empty').length) {

                    table_observations.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');

                    observation_selected = table_observations.row( this ).data();

                    table_selected = table_observations;
                    if (records) {
                        $('#modal-add-observation').modal('hide');
                    }
                    $('#modal-edit-observation').modal('show');
                }
            });

            $(".btn-export-observations").click(function() {

                bootbox.confirm('Se descargará un archivo Excel', function(result) {
                    if (result) {
                        $('#table-observations').tableExport({tableName: 'Observaciones', type:'excel', escape:'false'});
                    }
                }).find("div.modal-content").addClass("confirm-width-md");
            });

            $('.btn-add-observation').click(function() {
                table_selected = table_observations;
                $('#modal-add-observation').modal('show');
            });
        }
    });

</script>
