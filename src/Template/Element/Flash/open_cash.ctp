<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal body -->
            <div class="modal-body">
                <?= $message ?>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <a href="/ispbrain/cash-entities/open-cash" title="Abrir Caja" class="btn btn-success"><span class="glyphicon icon-switch  text-white" aria-hidden="true"></span></a>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">

    $('#myModal').modal('show');
    
</script>
