<style type="text/css">

    textarea {
        border: 1px solid #999999;
        width: 100%;
        margin: 5px 0;
        padding: 3px;
    }

    tr.new {
        background-color: #a7d6ec;
    }

    table {
        width: 100% !important;
    }

</style>

<div class="row">
    <div class="col-md-12">
        <table class="table table-hover table-bordered" id="table-errors-quick-view">
            <thead>
                <tr>
                    <th>Fecha</th>       <!--1-->
                    <th>Event</th>       <!--2-->
                    <th>Descripción</th> <!--3-->
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<div class="row justify-content-end">
    <div class="col-3">
        <?php

            echo $this->Html->link(
                '<span class="glyphicon icon-plus" aria-hidden="true"></span> Más detalles',
                'javascript:void(0)',
                [
                'title' => 'Ver mas detalles',
                'class' => 'btn btn-default btn-more-detail mt-2',
                'escape' => false
            ]);

        ?>
    </div>
</div>

<script type="text/javascript">

    var table_errors_quick_view = null;
    var error_selected = null;
    var curr_pos_scroll = null;

    $(document).ready(function () {

        $('#table-errors-quick-view').removeClass('display');

    	table_errors_quick_view = $('#table-errors-quick-view').DataTable({
    	    "order": [[ 0, 'desc' ]],
    	    "deferRender": true,
            "processing": true,
            "serverSide": true,
            "searching": false,
            "ordering": false,
            "info": false,
		    "paging": false,
		    "ajax": {
                "url": "/ispbrain/ErrorLog/get_error_log_quick_view.json",
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "drawCallback": function( c ) {
              
            },
            "scrollY": true,
		    "scrollY": '330px',
		    "scrollX": true,
		    "scrollCollapse": true,
            "columns": [
                {
                    "data": "created",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            var time = data.split('T')[1];
                            time = time.split(':');
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0] +' '+time[0] +':'+time[1];
                        }
                    }
                },
                {
                    "class": "left",
                    "data": "event",
                },
                {
                    "class": "font-weight-bold left",
                    "data": "msg",
                },
               
            ],
            "columnDefs": [
                { "type": "datetime-custom", "targets": [0] },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
                if (!data.view) {
                    $(row).addClass('new');
                }
            },
    	    "language": dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[-1], ["Todas"]],
             "dom":
    	    	"<'row'<'col-md-6'l><'col-md-4'f><'col-md-2 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    	});

        $('.btn-more-detail').click(function() {
            
             var action = '/ispbrain/ErrorLog/index/';
             window.open(action, '_blank');
        });
    });

</script>
