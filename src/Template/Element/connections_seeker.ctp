<div class="card">
    <div class="card-header card-connections-seeker">
        <h5 class="<?= isset($class_title) ? $class_title : '' ?>" > <?= isset($title) ? $title : 'Seleccionar Conexión del Cliente' ?></h5>
    </div>
    <div class="card-block text-secondary">
        <table class="table table-bordered table-hover" id="table-connections">
            <thead>
                <tr>
                    <th><?= __('Código') ?></th>
                    <th><?= __('Nombre') ?></th>
                    <th><?= __('Documento') ?></th>
                    <th><?= __('Controlador') ?></th>
                    <th><?= __('Servicio') ?></th>
                    <th><?= __('IP') ?></th>
                    <th><?= __('Domicilio') ?></th>
                    <th><?= __('Área') ?></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">

    var connection_selected = null;
    var table_connections = null;

    function clear_card_connecion_seeker() {
        connection_selected = null;
        table_connections.clear().draw();;
    }

    $(document).ready(function() {

        $('#table-connections').removeClass('display');

        table_connections = $('#table-connections').DataTable({
		    "order": [[ 0, 'desc' ]],
            "deferRender": true,
            "bFilter": false,
	        "ajax": {
                "url": "/ispbrain/connections/get_connection_customer.json",
                "dataSrc": "connections",
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "customer_code":  (customer_selected) ? customer_selected.code : '',
                        "deleted": 0,
                    });
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": false,
		    "columns": [
		        {
                    "data": "customer.code",
                    "render": function ( data, type, row ) {
                        var customer_code = "";
                        if (data) {
                            customer_code = pad(data, 5);
                        }
                        return customer_code;
                    }
                },
                { 
                    "data": "customer.name",
                },
                {
                    "data": "customer.ident",
                    "render": function ( data, type, row ) {
                        var customer_ident = "";
                        if (data) {
                            customer_ident = sessionPHP.afip_codes.doc_types[row.customer.doc_type] + ' ' + data;
                        }
                        return customer_ident;
                    }
                },
                { 
                    "data": "controller.name",
                },
                { "data": "service.name" },
                { 
                    "data": "ip",
                },
                { "data": "address" },
                { "data": "area.name" },
            ],
		    "columnDefs": [
                { 
                    "class": "left", targets: [1, 3]
                },
            ],
             "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
		    "language": dataTable_lenguage,
            "lengthMenu": [[50, 100, -1], [50, 100, "Todas"]],
		});

        $('#table-connections tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                if ($(this).hasClass('selected')) {

                     table_connections.$('tr.selected').removeClass('selected');
                     $(document).trigger('CONNECTION_UNSELECTED');
                     connection_selected = null;

                } else {
                    table_connections.$('tr.selected').removeClass('selected');
                    connection_selected = table_connections.row( this ).data();
                    $(this).addClass('selected');
                    $(document).trigger('CONNECTION_SELECTED');
                }
            }
        });
    });
</script>
