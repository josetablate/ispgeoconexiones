<tr>
    <th><?= __('ID Comercio') ?></th>
    <td class="pull-right">
    <?php foreach ($credentials as $credential): ?>
        <?php if ($credential->idComercio == $connection->connections_cobrodigital->id_comercio): ?>
            <td class="pull-right"><?= $credential->name . ' (' . $credential->idComercio  . ')' ?></td>
        <?php endif; ?>
    <?php endforeach; ?>
    </td>
</tr>