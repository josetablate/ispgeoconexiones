<style type="text/css">

    .sub-title {
        border-bottom: 1px solid #e3e2e2;
        width: 100%;
    }

    .my-hidden {
        display: none;
    }

    .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:active {
        color: #FF5722;
    }

    .modal a.btn {
        width: 100%;
        margin-bottom: 5px;
        text-align: left;
    }

    .checkbox label {
        border-bottom: 1px solid #e3e2e2;
        font-size: 20px;
        width: 100%;
    }

    .disabled {
        cursor: not-allowed;
    }

</style>

<div class="modal fade <?= $modal ?>" id="modal-add-boleta-cobrodigital" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel"><?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-xl-12 mt-3">

                        <div class="row">

                            <div class="col-12">
                                <?php
                                    $id_comercios_posta = ['' => __('Seleccione')];
                                    foreach ($id_comercios as $key => $id_comercio) {
                                        foreach ($credentials as $credential) {
                                            if ($credential->idComercio == $key && $credential->boleta && $credential->enabled) {
                                                $id_comercios_posta[$credential->idComercio] = '(' . $credential->idComercio . ') ' . $credential->name;
                                            }
                                        }
                                    }
                                    echo $this->Form->input('id_comercio', ['label' => 'ID Comercio', 'options' => $id_comercios_posta, 'class' => 'selectpicker add-boleta-cobrodigital', 'data-live-search' => "true"]);
                                ?>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btn-selected-boleta-cobrodigital">Seleccionar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function clearCdModal()
    {
        $('select[name=id_comercio]').val('');
        $('#id-comercio').selectpicker('refresh');
    }

    var cobrodigital_boleta_selected = null;

    $(document).ready(function() {

        $('#btn-selected-boleta-cobrodigital').click(function() {

            var msgError = "";
            var flag = false;
            if (cobrodigital_boleta_selected == null) {
                msgError += "* Debe seleccionar un ID Comercio";
                flag = true;
            }

            if (flag) {
                generateNoty('warning', msgError);
            } else {
                $('#btn-selected-boleta-cobrodigital').addClass('my-hidden');
                $('.modal-add-boleta-cobrodigital').modal('hide');
                $('#modal-add-boleta-cobrodigital').trigger('COBRODIGITAL_BOLETA_SELECTED');
            }

        });

        $('#modal-add-boleta-cobrodigital').on('shown.bs.modal', function () {
            clearCdModal();
            cobrodigital_boleta_selected = null;
            $('#btn-selected-boleta-cobrodigital').removeClass('my-hidden');
        });

        $("#modal-add-boleta-cobrodigital").on("hidden.bs.modal", function () {
            $('#modal-add-boleta-cobrodigital').trigger('COBRODIGITAL_BOLETA_SELECTED_CLOSED');
        });

        $('.selectpicker.add-boleta-cobrodigital').change(function () {
            cobrodigital_boleta_selected = {
                id_comercio: $(this).find("option:selected").val()
            };
        });
    });

</script>
