<style type="text/css">

    .sub-title {
        border-bottom: 1px solid #e3e2e2;
        width: 100%;
    }

    .my-hidden {
        display: none;
    }

    .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:active {
        color: #FF5722;
    }

    .modal a.btn {
        width: 100%;
        margin-bottom: 5px;
        text-align: left;
    }

    .checkbox label {
        border-bottom: 1px solid #e3e2e2;
        font-size: 20px;
        width: 100%;
    }

    .disabled {
        cursor: not-allowed;
    }

</style>

<div class="modal fade <?= $modal ?>" id="modal-edit-boleta-cobrodigital" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel"><?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-xl-12 mt-3">

                        <div class="row">

                            <div class="col-12">
                                <?php
                                    $id_comercios_posta = ['' => __('Seleccione')];
                                    foreach ($id_comercios as $key => $id_comercio) {
                                        foreach ($credentials as $credential) {
                                            if ($credential->idComercio == $key && $credential->barcode_in_invoice) {
                                                $id_comercios_posta[$credential->idComercio] = '(' . $credential->idComercio . ') ' . $credential->name;
                                            }
                                        }
                                    }
                                    echo $this->Form->input('id_comercio-edit', ['label' => 'ID Comercio', 'options' => $id_comercios_posta, 'class' => 'selectpicker', 'data-live-search' => "true", 'value' => $connection->payment_method->id_comercio]);
                                ?>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btn-selected-boleta-cobrodigital-edit">Seleccionar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function clearCdModal()
    {
        $('select[name=id_comercio-edit]').val('');
        $('#id-comercio-edit').selectpicker('refresh');
    }

    var cobrodigital_boleta_selected = null;
    var connection = null;
    var id_comercios = null;

    $(document).ready(function() {

        connection = <?= json_encode($connection) ?>;
        id_comercios = <?= json_encode($id_comercios) ?>;

        $('#btn-selected-boleta-cobrodigital-edit').click(function() {

            var msgError = "";
            var flag = false;
            if (cobrodigital_auto_debit_selected == null) {
                msgError += "* Debe seleccionar un ID Comercio";
                flag = true;
            }

            if (flag) {
                generateNoty('warning', msgError);
            } else {
                cobrodigital_auto_debit_selected = {
                    auto_debit_id: connection.payment_method.auto_debit_id,
                    id_comercio: $('select[name=id_comercio-edit]').val()
                };

                $('.modal-edit-boleta-cobrodigital').modal('hide');
                $('#modal-edit-boleta-cobrodigital').trigger('COBRODIGITAL_EDIT_BOLETA_SELECTED');
            }

        });

        $('#modal-add-boleta-cobrodigital').on('shown.bs.modal', function () {
            // if (!cobrodigital_selected) {
            //     clearCdModal();
            // }
        });

        $('.selectpicker').change(function () {
            cobrodigital_boleta_selected = {
                id_comercio: $(this).find("option:selected").val()
            };
        });

    });

</script>
