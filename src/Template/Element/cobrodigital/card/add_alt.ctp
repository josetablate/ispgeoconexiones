<style type="text/css">

    .sub-title {
        border-bottom: 1px solid #e3e2e2;
        width: 100%;
    }

    .my-hidden {
        display: none;
    }

    .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:active {
        color: #FF5722;
    }

    .modal a.btn {
        width: 100%;
        margin-bottom: 5px;
        text-align: left;
    }

    .checkbox label {
        border-bottom: 1px solid #e3e2e2;
        font-size: 20px;
        width: 100%;
    }

    .disabled {
        cursor: not-allowed;
    }

</style>

<div class="modal fade <?= $modal ?>" id="modal-add-card-cobrodigital" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel"><?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-xl-12 mt-3">

                        <div class="row">

                            <div class="col-xl-6">
                                <?php
                                    echo $this->Form->input('electronic_code', ['label' => 'Código Electrónico', 'options' => [0 => "Seleccionar Código Electrónico"], 'class' => 'selectpicker', 'data-live-search' => "true"]);
                                ?>
                            </div>

                            <div class="col-xl-6">
                                <?php
                                    echo $this->Form->input('bar_code', ['label' => 'Código de Barras', 'options' => [0 => "Seleccionar Código de Barras"], 'class' => 'selectpicker', 'data-live-search' => "true"]);
                                ?>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btn-selected-card-cobrodigital">Seleccionar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function clearCdModal()
    {
        $('select[name=bar_code]').val('');
        $('#bar-code').selectpicker('refresh');

        $('select[name=electronic_code]').val('');
        $('#electronic-code').selectpicker('refresh');
    }

    var cobrodigital_card_selected = null;

    $(document).ready(function() {

        $( "#electronic-code" ).change(function() {
            $('select[name=bar_code]').val($(this).val());
            $('#bar-code').selectpicker('refresh');
        });

        $( "#bar-code" ).change(function() {
            $('select[name=electronic_code]').val($(this).val());
            $('#electronic-code').selectpicker('refresh');
        });

        $('#btn-selected-card-cobrodigital').click(function() {

            if ($("#bar-code").val() != "") {

                $.ajax({
                    url: "<?= $this->Url->build(['controller' => 'CobroDigital', 'action' => 'markUsedCard']) ?>",
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify({ card_id: $("#bar-code").val() }),
                    success: function(data) {

                        if (data.data.status == 200) {
                            
                        } else {

                        }
                    },
                    error: function (jqXHR) {
                        if (jqXHR.status == 403) {

                        	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                        	setTimeout(function() { 
                        	  window.location.href ="/ispbrain";
                        	}, 3000);

                        } else {
                        	generateNoty('error', 'Error al selecconar la tarjeta.');
                        }
                    }
                });

                cobrodigital_card_selected = {
                    id: $("#bar-code").val(),
                    barcode: $('#bar-code option:selected').text(),
                    electronic_code: $('#electronic-code option:selected').text()
                };

                $('.modal-add-card-cobrodigital').modal('hide');
                $('#modal-add-card-cobrodigital').trigger('COBRODIGITAL_CARD_SELECTED');

            } else {
                generateNoty('warning', 'Debe seleccionar, una tarjeta por código de barra o código electónico.');
            }
        });

        $('#modal-add-card-cobrodigital').on('shown.bs.modal', function () {
            // if (!cobrodigital_card_selected) {
            //     clearCdModal();
            // }

            $.ajax({
                url: "<?= $this->Url->build(['controller' => 'CobroDigital', 'action' => 'getCards']) ?>",
                type: 'GET',
                dataType: "json",
                success: function(data) {

                    if (data.data.status == 200) {

                        $("#electronic-code").empty();
                        $("#bar-code").empty();

                        $("#electronic-code").append("<option value=''>Seleccionar Código Electrónico</option>");
                        $("#bar-code").append("<option value=''>Seleccionar Código de Barras</option>");

                        $.each(data.data.cards, function( index, value ) {
                            $("#electronic-code").append("<option value='" + value.id + "'>" + value.electronic_code + "</option>");
                            $("#bar-code").append("<option value='" + value.id + "'>" + value.barcode + "</option>");
                        });

                        $('#bar-code').selectpicker('refresh');
                        $('#electronic-code').selectpicker('refresh');
                    } else {
                        $('.modal-add-card-cobrodigital').modal('hide');
                        generateNoty('warning', data.data.message);
                    }
                },
                error: function (jqXHR) {
                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                    	  window.location.href ="/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al selecconar la tarjeta.');
                    }
                }
            });
        });

    });

</script>
