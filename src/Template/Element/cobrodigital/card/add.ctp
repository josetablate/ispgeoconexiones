<style type="text/css">

    .sub-title {
        border-bottom: 1px solid #e3e2e2;
        width: 100%;
    }

    .my-hidden {
        display: none;
    }

    .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:active {
        color: #FF5722;
    }

    .modal a.btn {
        width: 100%;
        margin-bottom: 5px;
        text-align: left;
    }

    .checkbox label {
        border-bottom: 1px solid #e3e2e2;
        font-size: 20px;
        width: 100%;
    }

    .disabled {
        cursor: not-allowed;
    }

    #form-create-card-cobrodigital {
        width: 100%;
    }

</style>

<div class="modal fade <?= $modal ?>" id="modal-add-card-cobrodigital" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel"><?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <div class="modal-body">
                <div class="row">

                    <?= $this->Form->create(null, [
                                'url' => ['controller' => 'CobroDigital', 'action' => 'markUsedCard'], 
                                'id'  => "form-add-card-cobrodigital"]) ?>

                    <div class="col-xl-12 mt-3">

                        <div class="row">

                            <?php
                                echo $this->Form->hidden('customer_code', ['id' => 'customer-code-add-card-cobrodigital', 'value' => $customer->code]);
                                echo $this->Form->hidden('tab', ['value' => $tab]);
                            ?>

                            <div class="col-xl-6 loading">
                                <label>Cargando...</label>
                            </div>
                            
                            <div class="col-xl-12 card-data">
                                <legend class="sub-title">Asignar Tarjeta</legend>
                            </div>

                            <div class="col-xl-6 card-data">
                                <?php
                                    echo $this->Form->input('electronic_code', ['label' => 'Código Electrónico', 'options' => [0 => "Seleccionar Código Electrónico"], 'class' => 'selectpicker', 'data-live-search' => "true"]);
                                ?>
                            </div>

                            <div class="col-xl-6 card-data">
                                <?php
                                    echo $this->Form->input('barcode', ['label' => 'Código de Barras', 'options' => [0 => "Seleccionar Código de Barras"], 'class' => 'selectpicker', 'data-live-search' => "true"]);
                                ?>
                            </div>

                            <div class="col-xl-6 card-data  mb-3">
                                <button type="button" class="btn btn-primary" id="btn-selected-card-cobrodigital">Asignar</button>
                            </div>

                        </div>
                    </div>

                    <?= $this->Form->end() ?>

                    <?= $this->Form->create(null, [
                            'url' => ['controller' => 'CobroDigital', 'action' => 'createCard'], 
                            'id'  => "form-create-card-cobrodigital"]) ?>

                    <div class="col-xl-12 mt-3">

                        <div class="row">

                            <?php
                                echo $this->Form->hidden('customer_code', ['id' => 'customer-code-create-card-cobrodigital', 'value' => $customer->code]);
                                echo $this->Form->hidden('tab', ['value' => $tab]);
                            ?>

                            <div class="col-xl-12">
                                <legend class="sub-title">Crear Tarjeta</legend>
                            </div>

                            <div class="col-xl-6">
                                <?php
                                    echo $this->Form->input('barcode', ['label' => 'Código Barra', 'type' => 'text']);
                                ?>
                                <?php
                                    echo $this->Form->input('electronic_code', ['label' => 'Código Electrónico', 'type' => 'text']);
                                ?>
                            </div>

                            <div class="col-xl-12">
                                <button type="button" class="btn btn-primary" id="btn-create-card-cobrodigital">Crear</button>
                            </div>

                        </div>
                    </div>
                    <?= $this->Form->end() ?>

                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function clearCdModal() {
        $('select[name=barcode]').val('');
        $('#barcode').selectpicker('refresh');

        $('select[name=electronic_code]').val('');
        $('#electronic-code').selectpicker('refresh');
    }

    var cobrodigital_card_selected = null;
    var cards = null;

    $(document).ready(function() {

        $( "#electronic-code" ).change(function() {
            $('select[name=barcode]').val($(this).val());
            $('#barcode').selectpicker('refresh');
        });

        $( "#barcode" ).change(function() {
            $('select[name=electronic_code]').val($(this).val());
            $('#electronic-code').selectpicker('refresh');
        });

        $('#btn-selected-card-cobrodigital').click(function() {

            var id_comercio = null;

            $.each(cards, function( index, value ) {
                if (value.electronic_code == $('#electronic-code option:selected').text()) {
                    id_comercio = value.id_comercio;
                }
            });

            if ($("#barcode").val() != "") {
                $("#form-add-card-cobrodigital").submit();
            } else {
                generateNoty('warning', 'Debe seleccionar, una tarjeta por código de barra o código electónico.');
            }
        });

        $('#btn-create-card-cobrodigital').click(function() {

            if ($("#form-create-card-cobrodigital  #barcode").val() != "") {
                $("#form-create-card-cobrodigital").submit();
            } else {
                generateNoty('warning', 'Debe ingresar el código de barra de la tarjeta.');
            }
        });

        $('#modal-add-card-cobrodigital').on('shown.bs.modal', function () {

            $('.loading').removeClass('d-none');
            $('.card-data').addClass('d-none');

            $.ajax({
                url: "<?= $this->Url->build(['controller' => 'CobroDigital', 'action' => 'getCards']) ?>",
                type: 'GET',
                dataType: "json",
                success: function(data) {

                    if (data.data.status == 200) {

                        $('.loading').addClass('d-none');
                        $('.card-data').removeClass('d-none');

                        cards = data.data.cards;

                        $("#electronic-code").empty();
                        $("#barcode").empty();

                        $("#electronic-code").append("<option value=''>Seleccionar Código Electrónico</option>");
                        $("#barcode").append("<option value=''>Seleccionar Código de Barras</option>");

                        $.each(data.data.cards, function( index, value ) {
                            $("#electronic-code").append("<option value='" + value.id + "'>" + value.electronic_code + "</option>");
                            $("#barcode").append("<option value='" + value.id + "'>" + value.barcode + "</option>");
                        });

                        $('#barcode').selectpicker('refresh');
                        $('#electronic-code').selectpicker('refresh');
                    } else {
                        $('.modal-add-card-cobrodigital').modal('hide');
                        generateNoty('warning', data.data.message);
                    }
                },
                error: function (e) {

                }
            });
        });

        $("#modal-add-card-cobrodigital").on("hidden.bs.modal", function () {
            $('#modal-add-card-cobrodigital').trigger('COBRODIGITAL_CARD_SELECTED_CLOSED');
        });

    });

</script>
