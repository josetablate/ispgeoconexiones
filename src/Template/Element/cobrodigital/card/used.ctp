<style type="text/css">

    .sub-title {
        border-bottom: 1px solid #e3e2e2;
        width: 100%;
    }

    .my-hidden {
        display: none;
    }

    .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:active {
        color: #FF5722;
    }

    .modal a.btn {
        width: 100%;
        margin-bottom: 5px;
        text-align: left;
    }

    .checkbox label {
        border-bottom: 1px solid #e3e2e2;
        font-size: 20px;
        width: 100%;
    }

    .disabled {
        cursor: not-allowed;
    }

</style>

<div class="modal fade <?= $modal ?>" id="modal-used-card-cobrodigital" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel"><?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-xl-12 mt-3">

                        <div class="row">

                            <div class="col-xl-12">
                                <select class="form-control" id="select-card-cd-other-connection"></select>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btn-selected-card-cobrodigital-to-connection">Seleccionar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function clearCdModal() {
        $('#select-card-cd-other-connection').val('');
    }

    var cobrodigital_card_selected = null;
    var connections = null;

    $(document).ready(function() {

        connections = <?= json_encode($connections) ?>;

        $('#btn-selected-card-cobrodigital-to-connection').click(function() {

            if ($("#select-card-cd-other-connection").val() != "") {
                $.each(connections, function( index, con ) {
                    if (con.id == $("#select-card-cd-other-connection").val()) {
                        cobrodigital_card_selected = {
                            id: con.payment_method.card_id,
                            service_name: con.service.name,
                            barcode: con.payment_method.barcode,
                            electronic_code: con.payment_method.electronic_code
                        };
                        return false;
                    }
                });

                $('.modal-used-card-cobrodigital').modal('hide');
                $('#modal-used-card-cobrodigital').trigger('COBRODIGITAL_CARD_USED_TO_CONNECTION');
            } else {
                generateNoty('warning', 'Debe seleccionar uan tarjeta.');
            }
        });

        $('#modal-used-card-cobrodigital').on('shown.bs.modal', function () {

            $('#select-card-cd-other-connection').html("");
            $('#select-card-cd-other-connection').append('<option value="">Seleccionar tarjeta</option>');
            $.each(connections, function( index, con ) {
                if (con.payment_method.id == 99) {
                    var name = 'Cód. Barra: ' + con.payment_method.barcode + ' - Cód. electr.: ' + con.payment_method.electronic_code + ' - Servicio: ' + con.service.name;
                    $('#select-card-cd-other-connection').append('<option tooltip="' + name + '" title="' + name + '" value="' + con.id + '">' + name + '</option>');
                }
            });
        });

    });

</script>
