<tr>
    <th><?= __('Estado') ?></th>
    <td class="pull-right font-weight-bold <?= $account->deleted ? 'text-danger' : 'text-success' ?>"><?= $account->deleted ? ' <i class="fas fa-minus-circle"></i> Deshabilitada' : '<i class="far fa-check-circle"></i> Habilitada' ?></td>
</tr>
<tr>
    <th><?= __('ID Comercio') ?></th>
    <?php foreach ($credentials as $credential): ?>
        <?php if ($credential->idComercio == $account->id_comercio): ?>
            <td class="pull-right"><?= $credential->name . ' (' . $credential->idComercio  . ')' ?></td>
        <?php endif; ?>
    <?php endforeach; ?>
</tr>
<tr>
    <th><?= __('Código de Barra') ?></th>
    <td class="pull-right">
        <?= $account->barcode ?>
    </td>
</tr>
<tr>
    <th><?= __('Código Electrónico') ?></th>
    <td class="pull-right">
        <?= $account->electronic_code ?>
    </td>
</tr>
