<style type="text/css">

    .sub-title {
        border-bottom: 1px solid #e3e2e2;
        width: 100%;
    }

    .my-hidden {
        display: none;
    }

    .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:active {
        color: #FF5722;
    }

    .modal a.btn {
        width: 100%;
        margin-bottom: 5px;
        text-align: left;
    }

    .checkbox label {
        border-bottom: 1px solid #e3e2e2;
        font-size: 20px;
        width: 100%;
    }

    .disabled {
        cursor: not-allowed;
    }

</style>

<div class="modal fade <?= $modal ?>" id="modal-edit-auto-debit-cobrodigital" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel"><?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <?= $this->Form->create(null, [
                'url' => ['controller' => 'CustomersAccounts', 'action' => 'edit'], 
                'id'  => "form-edit-auto-debit-cobrodigital"]) ?>

            <div class="modal-body">
                <div class="row">

                    <div class="col-xl-12 mt-3">

                        <div class="row">
                            <?php
                                echo $this->Form->hidden('customer_code', ['id' => 'customer-code-edit-auto-debit-cobrodigital']);
                                echo $this->Form->hidden('account_id', ['id' => 'account-id-edit-auto-debit-cobrodigital']);
                                echo $this->Form->hidden('payment_getway_id', ['id' => 'payment-getway-id-edit-auto-debit-cobrodigital']);
                                echo $this->Form->hidden('tab', ['value' => $tab]);
                            ?>

                            <div class="col-4">
                                <?php
                                    $id_comercios_posta = ['' => __('Seleccione')];
                                    foreach ($id_comercios as $key => $id_comercio) {
                                        foreach ($credentials as $credential) {
                                            if ($credential->idComercio == $key
                                                && $credential->auto_debit) {

                                                $id_comercios_posta[$credential->idComercio] = '(' . $credential->idComercio . ') ' . $credential->name;
                                            }
                                        }
                                    }
                                    echo $this->Form->input('id_comercio', ['id' => 'id-comercio-edit-auto-debit-cobrodigital', 'label' => 'ID Comercio', 'options' => $id_comercios_posta, 'class' => 'selectpicker', 'data-live-search' => "true", 'value' => '']);
                                ?>
                            </div>
                            <div class="col-xl-4">
                                <?php
                                    echo $this->Form->input('firstname', ['id' => 'firstname-edit-auto-debit-cobrodigital', 'label' => 'Nombre', 'type' => 'text', 'value' => '']);
                                ?>
                            </div>
                            <div class="col-xl-4">
                                <?php
                                    echo $this->Form->input('lastname', ['id' => 'lastname-edit-auto-debit-cobrodigital', 'label' => 'Apellido', 'type' => 'text', 'value' => '']);
                                ?>
                            </div>
                            <div class="col-xl-4">
                                <?php
                                    echo $this->Form->input('email', ['id' => 'email-edit-auto-debit-cobrodigital', 'label' => 'Correo', 'type' => 'email', 'value' => '']);
                                ?>
                            </div>
                            <div class="col-xl-4">
                                <?php
                                    echo $this->Form->input('cuit', ['id' => 'cuit-edit-auto-debit-cobrodigital', 'label' => 'CUIT', 'type' => 'number', 'value' => '']);
                                ?>
                            </div>
                            <div class="col-xl-4">
                                <?php
                                    echo $this->Form->input('cbu', ['id' => 'cbu-edit-auto-debit-cobrodigital', 'label' => 'CBU', 'type' => 'number', 'value' => '']);
                                ?>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btn-selected-auto-debit-cobrodigital-edit">Guardar</button>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<script type="text/javascript">

    function validateCBU(cbu) {

        if (cbu.length < 22) {
            return false;
        }

        var ponderador;
        ponderador = '97139713971397139713971397139713';

        var i;
        var nDigito;
        var nPond;
        var bloque1;
        var bloque2;

        var nTotal;
        nTotal = 0;

        bloque1 = '0' + cbu.substring(0, 7);

        for (i = 0; i <= 7; i++) {
            nDigito = bloque1.charAt(i);
            nPond = ponderador.charAt(i);
            nTotal = nTotal + (nPond * nDigito) - ((Math.floor(nPond * nDigito / 10)) * 10);
        }

        i = 0;

        while ( ((Math.floor((nTotal + i) / 10)) * 10) != (nTotal + i) ) {
            i = i + 1;
        }

        // i = digito verificador

        if (cbu.substring(7, 8) != i) {
            return false;
        }

        nTotal = 0;

        bloque2 = '000' + cbu.substring(8, 21);

        for (i = 0; i <= 15; i++) {
            nDigito = bloque2.charAt(i);
            nPond = ponderador.charAt(i);
            nTotal = nTotal + (nPond * nDigito) - ((Math.floor(nPond * nDigito / 10)) * 10);
        }

        i = 0;

        while ( ((Math.floor((nTotal + i) / 10)) * 10) != (nTotal + i) ) {
            i = i + 1;
        }

        // i = digito verificador

        if (cbu.substring(21, 22) != i) {
            return false;
        } 

        return true;
    }

	function validateCUIT(sCUIT) {     
        var aMult = '5432765432'; 
        var aMult = aMult.split(''); 

        if (sCUIT && sCUIT.length == 11) 
        { 
            aCUIT = sCUIT.split(''); 
            var iResult = 0; 
            for (i = 0; i <= 9; i++) 
            { 
                iResult += aCUIT[i] * aMult[i]; 
            } 
            iResult = (iResult % 11); 
            iResult = 11 - iResult; 

            if (iResult == 11) iResult = 0; 
            if (iResult == 10) iResult = 9; 

            if (iResult == aCUIT[10]) 
            { 
                return true; 
            } 
        }     
        return false; 
    } 

    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( $email );
    }

    function clearCdModal() {
        $('#id-comercio-edit-auto-debit-cobrodigital').val('');
        $('#id-comercio-edit-auto-debit-cobrodigital').selectpicker('refresh');
        $('firstname-edit-auto-debit-cobrodigital').val('');
        $('#lastname-edit-auto-debit-cobrodigital').val('');
        $('#email-edit-auto-debit-cobrodigital').val('');
        $('#cuit-edit-auto-debit-cobrodigital').val('');
        $('#cbu-edit-auto-debit-cobrodigital').val('');
    }

    var cobrodigital_auto_debit_selected = null;
    var cbu = null;

    $(document).ready(function() {

        $('#btn-selected-auto-debit-cobrodigital-edit').click(function() {

            var msgError = "";
            var flag = false;
            if ($( "#id-comercio-edit-auto-debit-cobrodigital option:selected" ).val() == "") {
                msgError += "* Debe seleccionar un ID Comercio";
                flag = true;
            }
            if ($("#firstname-edit-auto-debit-cobrodigital").val() == "") {
                if (flag) {
                    msgError += "<br/>";
                }
                msgError += "* Debe ingresar Nombre";
                flag = true;
            }
            if ($("#lastname-edit-auto-debit-cobrodigital").val() == "") {
                if (flag) {
                    msgError += "<br/>";
                }
                msgError += "* Debe ingresar Apellido";
                flag = true;
            }
            if ($("#email-edit-auto-debit-cobrodigital").val() == "") {
                if (flag) {
                    msgError += "<br/>";
                }
                msgError += "* Debe ingresar Correo";
                flag = true;
            }
            if (!validateEmail($("#email-edit-auto-debit-cobrodigital").val())) {
                msgError += "* Correo inválido";
                flag = true;
            }
            if (!validateCUIT($("#cuit-edit-auto-debit-cobrodigital").val())) {
                if (flag) {
                    msgError += "<br/>";
                }
                msgError += "* CUIT inválido";
                flag = true;
            }
            if (!validateCBU($("#cbu-edit-auto-debit-cobrodigital").val())) {
                if (flag) {
                    msgError += "<br/>";
                }
                msgError += "* CBU inválido";
                flag = true;
            } 

            if (flag) {
                generateNoty('warning', msgError);
            } else {

                $('#btn-selected-auto-debit-cobrodigital-edit').addClass('my-hidden');

                if (cbu != $("#cbu-edit-auto-debit-cobrodigital").val()) {
                    validateExistCBU($("#cbu-edit-auto-debit-cobrodigital").val(), form_trigger_auto_debit_edit_cobrodigital);
                } else {
                    form_trigger_auto_debit_edit_cobrodigital(true);
                }
            }
        });

        $('#modal-edit-auto-debit-cobrodigital').on('shown.bs.modal', function () {
            cbu = $("#cbu-edit-auto-debit-cobrodigital").val();
        });

        var form_trigger_auto_debit_edit_cobrodigital = function(result) {
            if (result) {
                $("#form-edit-auto-debit-cobrodigital").submit();
            } else {
                $('#btn-selected-auto-debit-cobrodigital-edit').removeClass('my-hidden');
            }
        }

    });

</script>
