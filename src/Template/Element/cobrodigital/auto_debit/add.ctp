<style type="text/css">

    .sub-title {
        border-bottom: 1px solid #e3e2e2;
        width: 100%;
    }

    .my-hidden {
        display: none;
    }

    .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:active {
        color: #FF5722;
    }

    .modal a.btn {
        width: 100%;
        margin-bottom: 5px;
        text-align: left;
    }

    .checkbox label {
        border-bottom: 1px solid #e3e2e2;
        font-size: 20px;
        width: 100%;
    }

    .disabled {
        cursor: not-allowed;
    }

</style>

<div class="modal fade <?= $modal ?>" id="modal-add-auto-debit-cobrodigital" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel"><?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <?= $this->Form->create(null, [
                'url' => ['controller' => 'CobroDigital', 'action' => 'createAutoDebitAccount'], 
                'id'  => "form-add-auto-debit-cobrodigital"]) ?>

            <div class="modal-body">
                <div class="row">

                    <div class="col-xl-12 mt-3">

                        <div class="row">

                            <?php
                                echo $this->Form->hidden('customer_code', ['id' => 'customer-code-add-auto-debit-cobrodigital', 'value' => $customer->code]);
                                echo $this->Form->hidden('tab', ['value' => $tab]);
                            ?>

                            <div class="col-4">
                                <?php
                                    $id_comercios_posta = ['' => __('Seleccione')];
                                    foreach ($id_comercios as $key => $id_comercio) {
                                        foreach ($credentials as $credential) {
                                            if ($credential->idComercio == $key && $credential->enabled && $credential->auto_debit) {
                                                $id_comercios_posta[$credential->idComercio] = '(' . $credential->idComercio . ') ' . $credential->name;
                                            }
                                        }
                                    }
                                    echo $this->Form->input('id_comerciox', ['label' => 'ID Comercio', 'options' => $id_comercios_posta, 'class' => 'selectpicker', 'data-live-search' => "true"]);
                                ?>
                            </div>
                            <div class="col-xl-4">
                                <?php
                                    echo $this->Form->input('cd-firstname', ['label' => 'Nombre', 'type' => 'text']);
                                ?>
                            </div>
                            <div class="col-xl-4">
                                <?php
                                    echo $this->Form->input('cd-lastname', ['label' => 'Apellido', 'type' => 'text']);
                                ?>
                            </div>
                            <div class="col-xl-4">
                                <?php
                                    echo $this->Form->input('cd-email', ['label' => 'Correo', 'type' => 'email']);
                                ?>
                            </div>
                            <div class="col-xl-4">
                                <?php
                                    echo $this->Form->input('cd-cuit', ['label' => 'CUIT', 'type' => 'number']);
                                ?>
                            </div>
                            <div class="col-xl-4">
                                <?php
                                    echo $this->Form->input('cd-cbu', ['label' => 'CBU', 'type' => 'number']);
                                ?>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btn-selected-auto-debit-cobrodigital">Crear</button>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<script type="text/javascript">

    function validateCBU(cbu) {

        if (cbu.length < 22) {
            return false;
        }

        var ponderador;
        ponderador = '97139713971397139713971397139713';

        var i;
        var nDigito;
        var nPond;
        var bloque1;
        var bloque2;

        var nTotal;
        nTotal = 0;

        bloque1 = '0' + cbu.substring(0, 7);

        for (i = 0; i <= 7; i++) {
            nDigito = bloque1.charAt(i);
            nPond = ponderador.charAt(i);
            nTotal = nTotal + (nPond * nDigito) - ((Math.floor(nPond * nDigito / 10)) * 10);
        }

        i = 0;

        while ( ((Math.floor((nTotal + i) / 10)) * 10) != (nTotal + i) ) {
            i = i + 1;
        }

        // i = digito verificador

        if (cbu.substring(7, 8) != i) {
            return false;
        }

        nTotal = 0;

        bloque2 = '000' + cbu.substring(8, 21);

        for (i = 0; i <= 15; i++) {
            nDigito = bloque2.charAt(i);
            nPond = ponderador.charAt(i);
            nTotal = nTotal + (nPond * nDigito) - ((Math.floor(nPond * nDigito / 10)) * 10);
        }

        i = 0;

        while ( ((Math.floor((nTotal + i) / 10)) * 10) != (nTotal + i) ) {
            i = i + 1;
        }

        // i = digito verificador

        if (cbu.substring(21, 22) != i) {
            return false;
        } 

        return true;
    }

	function validateCUIT(sCUIT) {
        var aMult = '5432765432';
        var aMult = aMult.split('');

        if (sCUIT && sCUIT.length == 11) {
            aCUIT = sCUIT.split('');
            var iResult = 0;
            for (i = 0; i <= 9; i++)
            {
                iResult += aCUIT[i] * aMult[i];
            }
            iResult = (iResult % 11);
            iResult = 11 - iResult;

            if (iResult == 11) iResult = 0;
            if (iResult == 10) iResult = 9;

            if (iResult == aCUIT[10])
            {
                return true;
            }
        }
        return false;
    } 

    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( $email );
    }

    function clearCdModal() {
        $('#btn-selected-auto-debit-cobrodigital').removeClass('my-hidden');
        $('#customer-code-add-auto-debit-cobrodigital').val(customer.code);
        $('select[name=id_comerciox]').val('');
        $('#id-comercio').selectpicker('refresh');
        $('#cd-firstname').val('');
        $('#cd-lastname').val('');
        $('#cd-email').val('');
        $('#cd-cuit').val('');
        $('#cd-cbu').val('');
    }

    var cobrodigital_auto_debit_selected = null;

    $(document).ready(function() {

        $('#btn-selected-auto-debit-cobrodigital').click(function() {
            var msgError = "";
            var flag = false;
            if ($( "#id-comerciox option:selected" ).val() == "") {
                msgError += "* Debe seleccionar un ID Comercio";
                flag = true;
            }
            if ($("#cd-firstname").val() == "") {
                if (flag) {
                    msgError += "<br/>";
                }
                msgError += "* Debe ingresar Nombre";
                flag = true;
            }
            if ($("#cd-lastname").val() == "") {
                if (flag) {
                    msgError += "<br/>";
                }
                msgError += "* Debe ingresar Apellido";
                flag = true;
            }
            if ($("#cd-email").val() == "") {
                if (flag) {
                    msgError += "<br/>";
                }
                msgError += "* Debe ingresar Correo";
                flag = true;
            }
            if (!validateEmail($("#cd-email").val())) {
                msgError += "* Correo inválido";
                flag = true;
            }
            if (!validateCUIT($("#cd-cuit").val())) {
                if (flag) {
                    msgError += "<br/>";
                }
                msgError += "* CUIT inválido";
                flag = true;
            }
            if (!validateCBU($("#cd-cbu").val())) {
                if (flag) {
                    msgError += "<br/>";
                }
                msgError += "* CBU inválido";
                flag = true;
            } 

            if (flag) {
                generateNoty('warning', msgError);
            } else {
                $('#btn-selected-auto-debit-cobrodigital').addClass('my-hidden');
                var cbu = $("#cd-cbu").val();
                validateExistCBU(cbu, form_trigger_auto_debuit_cobrodigital);
            }

        });

        $('#modal-add-auto-debit-cobrodigital').on('shown.bs.modal', function () {
            clearCdModal();
        });

        $("#modal-add-auto-debit-cobrodigital").on("hidden.bs.modal", function () {
            $('#modal-add-auto-debit-cobrodigital').trigger('COBRODIGITAL_AUTO_DEBIT_SELECTED_CLOSED');
        });

    });

    var form_trigger_auto_debuit_cobrodigital = function(result) {
        if (result) {
            $("#form-add-auto-debit-cobrodigital").submit();
        } else {
            $('#btn-selected-auto-debit-cobrodigital').removeClass('my-hidden');
        }
    }

</script>
