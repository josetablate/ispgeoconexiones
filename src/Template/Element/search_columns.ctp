<div class="modal fade  <?= $modal ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header p-2">

                <div class="row w-100">
                    <div class="col-xl-11 ">
                          <h5 class="modal-title text-center" id="exampleModalLabel">Buscar por Columnas</h5>
                    </div>
                    <div class="col-xl-1">
                         <button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>

            </div>
            <div class="modal-body p-3">
                <form id="search-column-form">
                    <div class="row input-container">
                    </div>
                </form>
            </div>

            <div class="modal-footer p-2">
                <div class="col-xl-3">
                     <?php

                         echo $this->Html->link(
                            '<span class="glyphicon icon-floppy-disk" aria-hidden="true"></span> Guardar',
                            'javascript:void(0)',
                            [
                            'id' => 'btn-save-search-column-selected',
                            'title' => '',
                            'class' => 'btn btn-default float-right',
                            'escape' => false
                        ]);

                    ?>
                </div>
                <button class="btn-seconsary btn" id="btn-clear" type="button">Limpiar</button>
                <button class="btn-success btn" id="btn-search" type="button">Buscar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var index_serach_selected = '';

    $(document).ready(function() {

        // Jquery draggable modals
        $('.modal-dialog').draggable({
            handle: ".modal-header"
        });

        $('#btn-save-search-column-selected').click(function() {
            saveSearchPreferences();
        });
    });

    /** parameters: 
     * table: objeto tabla de datatable
     * index: indice para ubicar lo que se guarda, como ayuda se puede usar la seccion
     */
    function loadSearchPreferences(index) {

        index_serach_selected = index;

        if (typeof(Storage) !== "undefined") {

            var preferences = JSON.parse(localStorage.getItem(index_serach_selected + '-' + sessionPHP.Auth.User.id));

            if (preferences !== null) {

                // then refresh the page and run this to restore your form values:
                [].forEach.call(document.querySelector('#search-column-form').elements, function(el) {
                    el.value = preferences[el.id];
                    if (el.type == 'select-one') {
                        $('#' + el.id).val(el.value);
                    } else if (el.type == 'select-multiple') {
                        $('#' + el.id).val(preferences[el.id]);
                    }
                });
            }

        } else {
             generateNoty('information', 'El navegador no soporta almacenamiento web.');
        }
    }

    /** parameters: 
     * table: objeto tabla de datatable
     * index: indice para ubicar lo que se guarda, como ayuda se puede usar la seccion
     */
    function saveSearchPreferences() {

        if (typeof(Storage) !== "undefined") {
            // Code for localStorage/sessionStorage.

            var preferences = {};

            [].forEach.call(document.querySelector('#search-column-form').elements, function(el) {
                preferences[el.id] = el.value;
                if (el.type == 'select-multiple') {
                    preferences[el.id] = $('#' + el.id).val();
                }
            });

            if (Object.keys(preferences).length > 0) {

                var item = index_serach_selected + '-' + sessionPHP.Auth.User.id;
                localStorage.setItem(item, JSON.stringify(preferences));
            }

            generateNoty('information', 'Selección guardada.');

        } else {
            generateNoty('information', 'El navegador no soporta almacenamiento web.');
        }
    }

    function createModalSearchColumn(table, modal, no_seach = []) {

        var table_selected_search_columns = table;
        var modal_selected_search_columns = modal;

        var date_ids = [];
        var periode_ids = [];

        var html = "";

        $.each(table_selected_search_columns.columns()[0], function(i, val) {

            var sColumn = table.settings()[0].aoColumns[i];

            var column = table_selected_search_columns.column(i);

            if (typeof(sColumn.type) !== "undefined") {
                
                if ($.inArray(sColumn.idx, no_seach) != -1) {
                    return; 
                }

                switch (sColumn.type) {

                    case 'string':
                        var name = $(column.header()).html();
                        html += "<div class='col-xl-4'>";
                        html += "   <div class='form-group text'>";
                        html += "       <input type='text' data-column='" + i + "' id='" + sColumn.data + "' class='column_search form-control' placeHolder='" + name + "'>";
                        html += "   </div>";
                        html += "</div>";
                        break;

                    case 'integer':
                        var name = $(column.header()).html();
                        html += "<div class='col-xl-4'>";
                        html += "   <div class='form-group'>";
                        html += "       <input type='number' step='1'  data-column='" + i + "' id='" + sColumn.data + "' class='column_search form-control' placeHolder='" + name + "'>";
                        html += "   </div>";
                        html += "</div>";
                        break;

                    case 'decimal':
                        var name = $(column.header()).html();
                        html += "<div class='col-xl-4 mb-3'>";
                        html += "   <div class='input-group column_search decimal_between'>";
                        html += "       <div class='input-group-prepend'>";
                        html += "           <span class='input-group-text m-0'>" + name + "</span>";
                        html += "       </div>";
                        html += "       <input type='text' class='form-control min' data-column='" + i + "' id='" + sColumn.data + "_min' placeholder='Min'>";
                        html += "       <input type='text' class='form-control max' data-column='" + i + "' id='" + sColumn.data + "_max' placeholder='Max'>";
                        html += "   </div>";
                        html += "</div>";
                        break;

                    case 'bool':
                        var name = $(column.header()).html();
                        html += "<div class='col-xl-4'>";
                        html += "   <label class='form-check-label'>";
                        html += "       <input type='checkbox' class='column_search form-check-input' id='" + sColumn.data + "' data-column='" + i + "' > " + name;     
                        html += "   </label>";
                        html += "</div>";
                        break;

                    case 'date':
                        var name = $(column.header()).html();

                        if (sColumn.hasOwnProperty('display_search_text') ) {
                            name = sColumn.display_search_text;
                        }

                        html += "<div class='col-xl-8 column_search date_between'>";
                        html += "   <div class='row'>";
                        html += "       <div class='col-xl-6'>";
                        html += "           <div class='input-group date' id='" + sColumn.data.replace(".", "_") + "_min-datetimepicker'>";
                        html += "               <div class='input-group-prepend'>";
                        html += "                   <span class='input-group-text m-0'>" + name + "</span>";
                        html += "               </div>";
                        html += "               <input id='" + sColumn.data.replace(".", "_") + "_min' type='text' class='form-control min' data-column='" + i + "' title='" + $(column.header()).html() + "' data-toggle='tooltip' />";
                        html += "               <div class='input-group-prepend'>";
                        html += "                   <span class='input-group-addon input-group-text calendar m-0'>";
                        html += "                       <span class='glyphicon icon-calendar m-0'></span>";
                        html += "                   </span>";
                        html += "               </div>";
                        html += "           </div>";
                        html += "           <small  class='form-text text-muted mt-0 mb-1'>Fecha Mínima</small>";
                        html += "       </div>";
                        html += "       <div class='col-xl-6'>";
                        html += "           <div class='input-group date' id='" + sColumn.data.replace(".", "_") + "_max-datetimepicker'>";
                        html += "               <div class='input-group-prepend'>";
                        html += "                   <span class='input-group-text m-0'>" + name + "</span>";
                        html += "               </div>";
                        html += "               <input id='" + sColumn.data.replace(".", "_") + "_max' type='text' class='form-control max' data-column='" + i + "' title='" + $(column.header()).html() + "' data-toggle='tooltip' />";
                        html += "               <div class='input-group-prepend'>";
                        html += "                   <span class='input-group-addon input-group-text calendar m-0'>";
                        html += "                       <span class='glyphicon icon-calendar m-0'></span>";
                        html += "                   </span>";
                        html += "               </div>";
                        html += "               </div>";
                        html += "               <small  class='form-text text-muted mt-0 mb-1'>Fecha Máxima</small>";
                        html += "           </div>";
                        html += "       </div>";
                        html += "</div>";

                        date_ids.push(sColumn.data.replace(".", "_"));
                        break;

                    case 'periode':
                        var name = $(column.header()).html();

                        html += "<div class='col-xl-8 column_search periode_between'>";
                        html += "   <div class='row'>";
                        html += "       <div class='col-xl-6'>";
                        html += "           <div class='input-group date' id='" + sColumn.data.replace(".", "_") + "_min-datetimepicker'>";
                        html += "               <div class='input-group-prepend'>";
                        html += "                   <span class='input-group-text m-0'>" + name + "</span>";
                        html += "               </div>";
                        html += "               <input id='" + sColumn.data.replace(".", "_") + "_min' type='text' class='form-control min' data-column='" + i + "' />";
                        html += "               <div class='input-group-prepend'>";
                        html += "                   <span class='input-group-addon input-group-text calendar m-0'>";
                        html += "                       <span class='glyphicon icon-calendar m-0'></span>";
                        html += "                   </span>";
                        html += "               </div>";
                        html += "           </div>";
                        html += "           <small  class='form-text text-muted mt-0 mb-1'>Período Mínimo</small>";
                        html += "       </div>";
                        html += "       <div class='col-xl-6'>";
                        html += "           <div class='input-group date' id='" + sColumn.data.replace(".", "_") + "_max-datetimepicker'>";
                        html += "               <div class='input-group-prepend'>";
                        html += "                   <span class='input-group-text m-0'>" + name + "</span>";
                        html += "               </div>";
                        html += "               <input id='" + sColumn.data.replace(".", "_") + "_max' type='text' class='form-control max' data-column='" + i + "' />";
                        html += "               <div class='input-group-prepend'>";
                        html += "                   <span class='input-group-addon input-group-text calendar m-0'>";
                        html += "                       <span class='glyphicon icon-calendar m-0'></span>";
                        html += "                   </span>";
                        html += "               </div>";
                        html += "               </div>";
                        html += "               <small  class='form-text text-muted mt-0 mb-1'>Período Máximo</small>";
                        html += "           </div>";
                        html += "       </div>";
                        html += "</div>";

                        periode_ids.push(sColumn.data.replace(".", "_"));
                        break;

                    case 'options':
                        var name = $(column.header()).html();
                        html += "<div class='col-xl-4'>";
                        html += "   <div class='form-group'>";
                        html += "       <select id='" + sColumn.data + "' class='input-group-text column_search form-control' data-column='" + i + "'>";
                        html += "           <option value=''>" + name + "</option>";
                        $.each(sColumn.options, function(i, option) {
                            html += "           <option value='" + i + "'>" + option + "</option>";
                        });
                        html += "       </select>";
                        html += "   </div>";
                        html += "</div>";
                        break;

                    case 'options_obj':
                        var name = $(column.header()).html();
                        html += "<div class='col-xl-4'>";
                        html += "   <div class='form-group'>";
                        html += "       <select id='" + sColumn.data + "' class='input-group-text column_search form-control' data-column='" + i + "'>";
                        html += "           <option value=''>" + name + "</option>";
                        $.each(sColumn.options, function(i, option) {
                            html += "           <option value='" + option.id + "'>" + option.name + "</option>";
                        });
                        html += "       </select>";
                        html += "   </div>";
                        html += "</div>";
                        break;

                    case 'options_colors':
                        var name = $(column.header()).html();
                        html += "<div class='col-xl-4'>";
                        html += "   <div class='form-group'>";
                        html += "       <select multiple id='" + sColumn.data + "' class='input-group-text column_search multiple form-control' data-column='" + i + "'>";
                        html += "           <option value=''>" + name + "</option>";
                        $.each(sColumn.options, function(i, option) {
                            html += "           <option value='" + option.id + "' style='color: " + option.color_back + "; '>" + option.text + "</option>";
                        });
                        html += "       </select>";
                        html += "   </div>";
                        html += "</div>";
                        break;
                }
            }
        });

        $(modal_selected_search_columns + ' .input-container').append(html);

        $.each(date_ids, function(i, val) {

            $(modal + ' #' + val + '_min-datetimepicker').datetimepicker({
                locale: 'es',
                format: 'DD/MM/YYYY'
            });

            $(modal + ' #' + val + '_max-datetimepicker').datetimepicker({
                locale: 'es',
                format: 'DD/MM/YYYY'
            });

        });

        $.each(periode_ids, function(i, val) {

            $(modal + ' #' + val + '_min-datetimepicker').datetimepicker({
                locale: 'es',
                format: 'MM/YYYY'
            });

            $(modal + ' #' + val + '_max-datetimepicker').datetimepicker({
                locale: 'es',
                format: 'MM/YYYY'
            });

        });

        $(modal_selected_search_columns).on( "click", "#btn-search", function() {

            $(modal_selected_search_columns + ' .column_search').each(function() {

                var value  = this.value;
                var column_index = $(this).data('column');

                if ($(this).hasClass('decimal_between')) {

                    if ($(this).find('.min').val() != '' ||  $(this).find('.max').val() != '') {
                        value = $(this).find('.min').val() + '<>' + $(this).find('.max').val();
                    } else {
                        value = '';
                    }
                    column_index = $(this).find('.min').data('column');
                }

                if ($(this).hasClass('form-check-input')) {
                    value = $(this).is(":checked") ? 1 : 0;
                }

                if ($(this).hasClass('date_between')) {

                    if ($(this).find('.min').val() != '' ||  $(this).find('.max').val() != '') {
                        value = $(this).find('.min').val() + '<>' + $(this).find('.max').val();
                    } else {
                        value = '';
                    }
                    column_index = $(this).find('.min').data('column');
                }

                if ($(this).hasClass('periode_between')) {

                    if ($(this).find('.min').val() != '' ||  $(this).find('.max').val() != '') {
                        value = $(this).find('.min').val() + '<>' + $(this).find('.max').val();
                    } else {
                        value = '';
                    }
                    column_index = $(this).find('.min').data('column');
                }

                if ($(this).hasClass('multiple')) {

                    value = '';
                    var c = 0;

                    $(this).find(":selected").map(function(i, el) {

                        if (c == 0) {
                            value += $(el).val();
                            c++;
                        } else {
                            value += "," + $(el).val();
                        }
                    })
                }

                table_selected_search_columns.columns( column_index ).search( value );

            });

            table_selected_search_columns.draw();

            $(modal_selected_search_columns).modal('hide');

        });

        $(modal_selected_search_columns).on( "click", "#btn-clear", function() {

            $(modal_selected_search_columns + ' .column_search').each(function() {

                if ($(this).hasClass('date_between')) {

                     $(this).find('.min').val('');
                     $(this).find('.max').val('');
                }

                if ($(this).hasClass('periode_between')) {

                     $(this).find('.min').val('');
                     $(this).find('.max').val('');
                }

                if ($(this).hasClass('decimal_between')) {

                     $(this).find('.min').val('');
                     $(this).find('.max').val('');
                }

                $(this).val('');
            });

            var item = index_serach_selected + '-' + sessionPHP.Auth.User.id;
            localStorage.removeItem(item);

            $(modal_selected_search_columns + " #btn-search").click();

        });

        $(modal_selected_search_columns + " .column_search").keyup(function(e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) {
                $("#btn-search").click();
            }
        });
    }

</script>
