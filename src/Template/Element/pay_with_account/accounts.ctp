<style type="text/css">

    .sub-title {
        border-bottom: 1px solid #e3e2e2;
        width: 100%;
    }

    .my-hidden {
        display: none;
    }

    .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:active {
        color: #FF5722;
    }

    .modal a.btn {
        width: 100%;
        margin-bottom: 5px;
        text-align: left;
    }

    .checkbox label {
        border-bottom: 1px solid #e3e2e2;
        font-size: 20px;
        width: 100%;
    }

    .disabled {
        cursor: not-allowed;
    }

    .lbl-title {
        font-weight: bold !important;
        font-size: 18px;
    }

    .lbl-connection {
        color: #f05f40;
        font-size: 22px;
        font-family: sans-serif;
    }

    .lbl-payment-method {
        color: #5e5f61;
        font-size: 22px;
        font-family: sans-serif;
    }

    .lbl-no-account {
        color: #67635e;
        font-family: sans-serif;
        font-weight: bold;
    }

    .account-contain {
        border: 1px solid #f05f40;
        border-radius: 5px;
        background: #f3f3f3;
        padding: 15px;
    }

    .lbl-connection-account {
        color: #696868 !important
    }

</style>

<div class="modal fade <?= $modal ?>" id="modal-accounts" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel"><?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row accounts-container">
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var accounts = null;
    var account_selected = null;
    var credentials = null;
    var customer = null;
    var payment_getway = null;

    $(document).ready(function() {

        credentials = <?= json_encode($credentials) ?>;
        payment_getway = <?= json_encode($payment_getway) ?>;

        $(document).on('click', '.btn-accounts-selected', function() {

            account_selected = null;
            var account_id = $(this).data('account-id');

            $.each(accounts, function( index, account ) {
                if (account.id == account_id) {
                    account_selected = account;
                    return false;
                }
            });

            $('#modal-accounts').modal('hide');
            $('#modal-accounts').trigger('ACCOUNT_SELECTED');
        });

        $('#modal-accounts').on('shown.bs.modal', function () {
            customer = window.customer_selected;
            $('.accounts-container').html('');
            var request = $.ajax({
                url: "/ispbrain/CustomersAccounts/getAccounts.json",
                method: "POST",
                data: JSON.stringify({
                    customer_code: customer.code,
                }),
                dataType: "json"
            });

            request.done(function(response) {

                if (response.response.accounts) {

                    accounts = response.response.accounts;

                    $.each(response.response.accounts, function( index, account ) {
                        addAccount(account);
                    });

                } else {
                    generateNoty('warning', 'No posee ninguna cuenta el cliente.');
                    $('#modal-accounts').modal('hide');
                }
            });

            request.fail(function(jqXHR) {

                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() { 
                        window.location.href = "/ispbrain";
                	}, 3000);

                } else {
                	generateNoty('error', 'Error al obtener las cuentas');
                }
            });
        });

        function addAccount(account)
        {
            var structure = '';

            var payment_name = "";
            $.each(payment_getway.methods, function( index, pg ) {
                if (pg.id == account.payment_getway_id) {
                    payment_name = pg.name;
                }
            });

            if (account.hasOwnProperty('id_comercio')) {

                var credential_name = '';
                var credential_id_comercio = '';

                $.each(credentials, function( index, credential ) {
                    if (credential.idComercio == account.id_comercio) {
                        credential_name = credential.name;
                        credential_id_comercio = credential.idComercio;
                    }
                });
            }

            switch (account.payment_getway_id) {

                case 7:
                    structure =
                        '<div class="col-xl-12 mt-3">' +
                            '<div class="account-contain">' +
                                '<div class="row">' +
                                    '<div class="col-xl-12 ">' +
                                        '<label class="lbl-title"><b class="lbl-connection">Cuenta: </b>' + payment_name + '</label>' +
                                    '</div>' +
                                    '<div class="col-xl-12">' +
                                        '<table class="vertical-table">' +
                                            '<tr>' +
                                                '<th><?= __('ID Comercio') ?></th>' +
                                                 '<td class="pull-right"> ' + account.id_comercio + '</td>' +
                                            '</tr>' +
                                            '<tr>' +
                                                '<th><?= __('Código de Barra') ?></th>' +
                                                '<td class="pull-right">' +
                                                    account.barcode +
                                                '</td>' +
                                            '</tr>' +
                                        '</table>' +
                                    '</div>';

                                    if (account.hasOwnProperty('connections') && Object.keys(account.connections).length > 0) {
                                        structure += '<div class="col-xl-12">' +
                                        '<label class="lbl-title mt-3"><b class="lbl-connection">Utilizan las conexiones:</b></label>';
                                            $.each(account.connections, function( index, connection ) {
                                                structure += '<p class="font-weight-bold lbl-connection-account">' + connection + '</p>';
                                            });
                                        structure += '</div>';
                                    }
                                    structure += '<div class="col-xl-12">' +
                                        '<button type="button" data-account-id="' + account.id + '" data-payment-getway-id="' + account.payment_getway_id + '" class="btn btn-primary btn-accounts-selected float-right">Seleccionar</button>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>';
                    break;

                case 99:
                    structure =
                        '<div class="col-xl-12 mt-3">' +
                            '<div class="account-contain">' +
                                '<div class="row">' +
                                    '<div class="col-xl-12 ">' +
                                        '<label class="lbl-title"><b class="lbl-connection">Cuenta: </b>' + payment_name + '</label>' +
                                    '</div>' +
                                    '<div class="col-xl-12">' +
                                        '<table class="vertical-table">' +
                                            '<tr>' +
                                                '<th><?= __('ID Comercio') ?></th>' +
                                                 '<td class="pull-right"> ' + credential_name + ' (' + credential_id_comercio + ')' + '</td>' +
                                            '</tr>' +
                                            '<tr>' +
                                                '<th><?= __('Código de Barra') ?></th>' +
                                                '<td class="pull-right">' +
                                                    account.barcode +
                                                '</td>' +
                                            '</tr>' +
                                            '<tr>' +
                                                '<th><?= __('Código Electrónico') ?></th>' +
                                                '<td class="pull-right">' +
                                                    account.electronic_code +
                                                '</td>' +
                                            '</tr>' +
                                        '</table>' +
                                    '</div>';
                                    if (account.hasOwnProperty('connections') && Object.keys(account.connections).length > 0) {
                                        structure += '<div class="col-xl-12">' +
                                        '<label class="lbl-title mt-3"><b class="lbl-connection">Utilizan las conexiones:</b></label>';
                                            $.each(account.connections, function( index, connection ) {
                                                structure += '<p class="font-weight-bold lbl-connection-account">' + connection + '</p>';
                                            });
                                        structure += '</div>';
                                    }
                                    structure += '<div class="col-xl-12">' +
                                        '<button type="button" data-account-id="' + account.id + '" data-payment-getway-id="' + account.payment_getway_id + '" class="btn btn-primary btn-accounts-selected float-right">Seleccionar</button>' +
                                '</div>' +
                            '</div>' +
                        '</div>';
                    break;

                case 102:
                    structure =
                        '<div class="col-xl-12 mt-3">' +
                            '<div class="account-contain">' +
                                '<div class="row">' +
                                    '<div class="col-xl-12 ">' +
                                        '<label class="lbl-title"><b class="lbl-connection">Cuenta seleccionada: </b>' + payment_name + '</label>' +
                                    '</div>' +
                                    '<div class="col-xl-12">' +
                                        '<table class="vertical-table">' +
                                            '<tr>' +
                                                '<th><?= __('Nombre') ?></th>' +
                                                '<td class="pull-right">' + account.firstname + '</td>' +
                                            '</tr>' +
                                            '<tr>' +
                                                '<th><?= __('Apellido') ?></th>' +
                                                '<td class="pull-right">' + account.lastname + '</td>' +
                                            '</tr>' +
                                            '<tr>' +
                                                '<th><?= __('CUIT') ?></th>' +
                                                '<td class="pull-right">' + account.cuit + '</td>' +
                                            '</tr>' +
                                            '<tr>' +
                                                '<th><?= __('CBU') ?></th>' +
                                                '<td class="pull-right">' + account.cbu + '</td>' +
                                            '</tr>' +
                                        '</table>' +
                                    '</div>';
                                    if (account.hasOwnProperty('connections') && Object.keys(account.connections).length > 0) {
                                        structure += '<div class="col-xl-12">' +
                                        '<label class="lbl-title mt-3"><b class="lbl-connection">Utilizan las conexiones:</b></label>';
                                            $.each(account.connections, function( index, connection ) {
                                                structure += '<p class="font-weight-bold lbl-connection-account">' + connection + '</p>';
                                            });
                                        structure += '</div>';
                                    }
                                    structure += '<div class="col-xl-12">' +
                                        '<button type="button" data-account-id="' + account.id + '" data-payment-getway-id="' + account.payment_getway_id + '" class="btn btn-primary btn-accounts-selected float-right">Seleccionar</button>' +
                                '</div>' +
                            '</div>' +
                        '</div>';
                    break;

                case 104:
                    structure =
                        '<div class="col-xl-12 mt-3">' +
                            '<div class="account-contain">' +
                                '<div class="row">' +
                                    '<div class="col-xl-12 ">' +
                                        '<label class="lbl-title"><b class="lbl-connection">Cuenta: </b>' + payment_name + '</label>' +
                                    '</div>' +
                                    '<div class="col-xl-12">' +
                                        '<table class="vertical-table">' +
                                            '<tr>' +
                                                '<th><?= __('ID Comercio') ?></th>' +
                                                '<td class="pull-right"> ' + credential_name + ' (' + credential_id_comercio + ')' + '</td>' +
                                            '</tr>' +
                                            '<tr>' +
                                                '<th><?= __('Nombre') ?></th>' +
                                                '<td class="pull-right">' + account.firstname + '</td>' +
                                            '</tr>' +
                                            '<tr>' +
                                                '<th><?= __('Apellido') ?></th>' +
                                                '<td class="pull-right">' + account.lastname + '</td>' +
                                            '</tr>' +
                                            '<tr>' +
                                                '<th><?= __('Correo') ?></th>' +
                                                '<td class="pull-right">' + account.email + '</td>' +
                                            '</tr>' +
                                            '<tr>' +
                                                '<th><?= __('CUIT') ?></th>' +
                                                '<td class="pull-right">' + account.cuit + '</td>' +
                                            '</tr>' +
                                            '<tr>' +
                                                '<th><?= __('CBU') ?></th>' +
                                                '<td class="pull-right">' + account.cbu + '</td>' +
                                            '</tr>' +
                                        '</table>' +
                                    '</div>';
                                    if (account.hasOwnProperty('connections') && Object.keys(account.connections).length > 0) {
                                        structure +='<div class="col-xl-12">' +
                                        '<label class="lbl-title mt-3"><b class="lbl-connection">Utilizan las conexiones:</b></label>';
                                            $.each(account.connections, function( index, connection ) {
                                                structure += '<p class="font-weight-bold lbl-connection-account">' + connection + '</p>';
                                            });
                                        structure += '</div>';
                                    }
                                    structure += '<div class="col-xl-12">' +
                                        '<button type="button" data-account-id="' + account.id + '" data-payment-getway-id="' + account.payment_getway_id + '" class="btn btn-primary btn-accounts-selected float-right">Seleccionar</button>' +
                                '</div>' +
                            '</div>' +
                        '</div>';
                    break;

                case 105:
                    structure =
                        '<div class="col-xl-12 mt-3">' +
                            '<div class="account-contain">' +
                                '<div class="row">' +
                                    '<div class="col-xl-12 ">' +
                                        '<label class="lbl-title"><b class="lbl-connection">Cuenta: </b>' + payment_name + '</label>' +
                                    '</div>' +
                                    '<div class="col-xl-12">' +
                                        '<table class="vertical-table">' +
                                            '<tr>' +
                                                '<th><?= __('Código de Barra') ?></th>' +
                                                '<td class="pull-right">' +
                                                    account.barcode +
                                                '</td>' +
                                            '</tr>' +
                                        '</table>' +
                                    '</div>';
                                    if (account.hasOwnProperty('connections') && Object.keys(account.connections).length > 0) {
                                        structure += '<div class="col-xl-12">' +
                                        '<label class="lbl-title mt-3"><b class="lbl-connection">Utilizan las conexiones:</b></label>';
                                            $.each(account.connections, function( index, connection ) {
                                                structure += '<p class="font-weight-bold lbl-connection-account">' + connection + '</p>';
                                            });
                                        structure += '</div>';
                                    }
                                    structure += '<div class="col-xl-12">' +
                                        '<button type="button" data-account-id="' + account.id + '" data-payment-getway-id="' + account.payment_getway_id + '" class="btn btn-primary btn-accounts-selected float-right">Seleccionar</button>' +
                                '</div>' +
                            '</div>' +
                        '</div>';
                    break;

                case 106:
                    structure =
                        '<div class="col-xl-12 mt-3">' +
                            '<div class="account-contain">' +
                                '<div class="row">' +
                                    '<div class="col-xl-12 ">' +
                                        '<label class="lbl-title"><b class="lbl-connection">Cuenta: </b>' + payment_name + '</label>' +
                                    '</div>' +
                                    '<div class="col-xl-12">' +
                                        '<table class="vertical-table">' +
                                            '<tr>' +
                                                '<th><?= __('Nro de Tarjeta') ?></th>' +
                                                '<td class="pull-right">' +
                                                    account.card_number +
                                                '</td>' +
                                            '</tr>' +
                                        '</table>' +
                                    '</div>';
                                    if (account.hasOwnProperty('connections') && Object.keys(account.connections).length > 0) {
                                        structure += '<div class="col-xl-12">' +
                                        '<label class="lbl-title mt-3"><b class="lbl-connection">Utilizan las conexiones:</b></label>';
                                            $.each(account.connections, function( index, connection ) {
                                                structure += '<p class="font-weight-bold lbl-connection-account">' + connection + '</p>';
                                            });
                                        structure += '</div>';
                                    }
                                    structure += '<div class="col-xl-12">' +
                                        '<button type="button" data-account-id="' + account.id + '" data-payment-getway-id="' + account.payment_getway_id + '" class="btn btn-primary btn-accounts-selected float-right">Seleccionar</button>' +
                                '</div>' +
                            '</div>' +
                        '</div>';
                    break;

                case 107:
                    structure =
                        '<div class="col-xl-12 mt-3">' +
                            '<div class="account-contain">' +
                                '<div class="row">' +
                                    '<div class="col-xl-12 ">' +
                                        '<label class="lbl-title"><b class="lbl-connection">Cuenta: </b>' + payment_name + '</label>' +
                                    '</div>' +
                                    '<div class="col-xl-12">' +
                                        '<table class="vertical-table">' +
                                            '<tr>' +
                                                '<th><?= __('Nro de Tarjeta') ?></th>' +
                                                '<td class="pull-right">' +
                                                    account.card_number +
                                                '</td>' +
                                            '</tr>' +
                                        '</table>' +
                                    '</div>';
                                    if (account.hasOwnProperty('connections') && Object.keys(account.connections).length > 0) {
                                        structure += '<div class="col-xl-12">' +
                                        '<label class="lbl-title mt-3"><b class="lbl-connection">Utilizan las conexiones:</b></label>';
                                            $.each(account.connections, function( index, connection ) {
                                                structure += '<p class="font-weight-bold lbl-connection-account">' + connection + '</p>';
                                            });
                                        structure += '</div>';
                                    }
                                    structure += '<div class="col-xl-12">' +
                                        '<button type="button" data-account-id="' + account.id + '" data-payment-getway-id="' + account.payment_getway_id + '" class="btn btn-primary btn-accounts-selected float-right">Seleccionar</button>' +
                                '</div>' +
                            '</div>' +
                        '</div>';
                    break;
            }
            $('.accounts-container').append(structure);
        }
    });

</script>
