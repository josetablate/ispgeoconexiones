<style type="text/css">

    #table-customers_wrapper .title {
        color: #696868;
        padding-top: 10px;
    }

</style>

<div class="card border-secondary mb-3 mx-auto" id="card-customer-seeker" style="">

    <!--titlo del buscado-->
    <div class="card-header ">
         <div class="row">
            <div class="col-8">
                <h5>1. Buscar Cliente</h5>
            </div>
            <div class="col-auto">
                <?php

                 echo $this->Html->link(
                        '<span class="glyphicon icon-bin" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Limpiar fomulario',
                        'class' => 'btn btn-default clear-from-customer',
                        'escape' => false
                    ]);
                
                ?>
            </div>
        </div>
    </div>

    <div class="card-body text-secondary">

        <div class="row">

            <div class="col-12">
                 <?php echo $this->Form->input('customer_ident', ['label' => 'Documento',  'autofocus' => true,  'autocomplete' => 'off', 'required' => false]); ?>
            </div>
            <div class="col-12">
                <?php echo $this->Form->input('customer_code', ['label' => 'Código', 'autocomplete' => 'off', 'required' => false]); ?>
            </div>
       </div>

    </div>

    <ul class="list-group list-group-flush">

        <!--boton para abrir la lista de clientes-->
        <li class="list-group-item">
            <button type="button" class="btn btn-success float-right btn-search">Buscar</button>
        </li>
    </ul>

</div>

<script type="text/javascript">

    var customer_selected = null;
    var enableLoad = false;

    function clear_card_customer_seeker() {

        customer_selected = null;
        $('#card-customer-seeker #customer-code').val('');
        $('#card-customer-seeker #customer-ident').val('');
    }

    $(document).ready(function() {

        $('#customer-code').keypress(function(e) {

            if (e.which == 13) {
                $('#card-customer-seeker .btn-search').click();
            }
        });

        $('#customer-ident').keypress(function(e) {
            if (e.which == 13) {
                $('#card-customer-seeker .btn-search').click();
            }
        });

        $('#card-customer-seeker .btn-search').click(function() {

            var customer = {
                code: '',
                ident: ''
            };

            if ($('#customer-code').val().length > 0 || $('#customer-ident').val().length > 0) {

                customer.code = $('#customer-code').val().trim();
                customer.ident = $('#customer-ident').val().replace("-", "").replace("-", "").trim();

                $.ajax({
                    url: "<?= $this->Url->build(['controller' => 'Customers', 'action' => 'geCustomerAjax']) ?>",
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify(customer),
                    success: function(data) {
                        var doc_type_name = sessionPHP.afip_codes.doc_types[data.customer.doc_type];

                        if (!data.customer) {
                            generateNoty('warning', 'No se pudo encontrar el cliente. Verifique los datos de búsqueda.');
                        } else {

                            customer_selected = data.customer;
                            $('#card-customer-seeker').trigger('CUSTOMER_SELECTED');
                        }
                    },
                    error: function (jqXHR) {

                        if (jqXHR.status == 403) {

                        	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                        	setTimeout(function() { 
                                window.location.href = "/ispbrain";
                        	}, 3000);

                        } else {
                        	generateNoty('error', 'Error al buscar el cliente.');
                        }
                    }
                });
            } else {
                generateNoty('warning', 'Debe ingresar el código o documento del cliente.');
            }
        });

        $('.clear-from-customer').click(function() {
            clear_card_customer_seeker();
            $('#card-customer-seeker').trigger('CUSTOMER_CLEAR');
        });
    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    $(document).click(function() {
        $('[data-toggle="tooltip"]').tooltip('hide');
    });

</script>
