<style type="text/css">

    .sub-title {
        border-bottom: 1px solid #e3e2e2;
        width: 100%;
    }

    .my-hidden {
        display: none;
    }

    .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:active {
        color: #FF5722;
    }

    .modal a.btn {
        width: 100%;
        margin-bottom: 5px;
        text-align: left;
    }

    .checkbox label {
        border-bottom: 1px solid #e3e2e2;
        font-size: 20px;
        width: 100%;
    }

    .disabled {
        cursor: not-allowed;
    }

    #form-create-visa-auto-debit {
        width: 100%;
    }

</style>

<div class="modal fade <?= $modal ?>" id="modal-add-visa-auto-debit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel"><?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

             <?= $this->Form->create(null, [
                'url' => ['controller' => 'VisaAutoDebit', 'action' => 'createAccount'], 
                'id'  => "form-create-visa-auto-debit"]) ?>

            <div class="modal-body">

                <div class="row">

                    <?php
                        echo $this->Form->hidden('customer_code', ['id' => 'customer-code-create-visa-auto-debit', 'value' => $customer->code]);
                        echo $this->Form->hidden('tab', ['value' => $tab]);
                    ?>

                    <div class="col-xl-6">
                        <?php
                            echo $this->Form->input('card_number', ['label' => 'Nro Tarjeta', 'type' => 'text']);
                        ?>
                    </div>

                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn-create-visa-auto-debit">Crear</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<script type="text/javascript">

    function clearCdModal() {
        $('#card-number').val('');
    }

    var visa_auto_debit_selected = null;
    var visa_auto_debit_account = null;

    $(document).ready(function() {

        $('#btn-create-visa-auto-debit').click(function() {

            if ($("#form-create-visa-auto-debit  #card-number").val() != "") {
                $("#form-create-visa-auto-debit").submit();
            } else {
                generateNoty('warning', 'Debe ingresar el nro de la tarjeta.');
            }
        });

        $('#modal-add-visa-auto-debit').on('shown.bs.modal', function () {

            clearCdModal();
        });

        $("#modal-add-visa-auto-debit").on("hidden.bs.modal", function () {
            $('#modal-add-visa-auto-debit').trigger('VISA_AUTO_DEBIT_SELECTED_CLOSED');
        });

    });

</script>
