<style type="text/css">

    .modal button.btn {
        width: 100%;
        margin-bottom: 5px;
        text-align: left;
        font-size: 20px;
    }

    .modal span {
       margin-right: 15px;
    }

</style>

<div class="modal modal-labels fade <?=$modal?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel">Etiquetas</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <center>
                            <?php
                                foreach ($buttons as $button) {
                                    if ($button['id'] != 0) {
                                         echo "<button type='button' id='label-" . $button['id'] . "' class='btn default label-btn' data-labelid='" . $button['id'] . "' style='background-color: " . $button['color_back'] . "; color: " . $button['color_text'] . "' ><span class='fas fa-tag' aria-hidden='true'></span><span>" . $button['text'] . "</span><span class='fas fa-check float-right check' style='font-size: 30px;' aria-hidden='true'></span></button>";
                                    }
                               }
                           ?>
                        </center>
                    </div>
                </div>
                <?php if (isset($save) && $save == true): ?>
                    <button type="button" class="btn btn-success mt-2 float-right" id="label-save" style="width: 100px;">Guardar</button>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function() {

        $('.label-btn').click(function() {
            if ($(this).find('.check').hasClass('d-none')) {
                $(this).find('.check').removeClass('d-none');
            } else {
                $(this).find('.check').addClass('d-none');
            }
        });
    });

</script>
