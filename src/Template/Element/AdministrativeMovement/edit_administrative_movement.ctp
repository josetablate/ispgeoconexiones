<style type="text/css">

    .sub-title {
        border-bottom: 1px solid #e3e2e2;
        width: 100%;
    }

    .my-hidden {
        display: none;
    }

    .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:active {
        color: #FF5722;
    }

    .modal a.btn {
        width: 100%;
        margin-bottom: 5px;
        text-align: left;
    }

    .checkbox label {
        border-bottom: 1px solid #e3e2e2;
        font-size: 20px;
        width: 100%;
    }

    .disabled {
        cursor: not-allowed;
    }

</style>

<div class="modal fade <?= $modal ?>" id="modal-edit-administrative-movement" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel"><?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-xl-12">
                        <select name="source" required="required" id="source" class="form-control">
                        </select>
                    </div>

                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btn-select-source">Seleccionar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var customer;
    var move;
    var table_selected;

    function clearModal()
    {
        $('#source').find('option').remove();
    }

    function getSourceAdministrativeMovement() {

        var request = $.ajax({
            url: "/ispbrain/Customers/getSourceAdministrativeMovementFromAjax.json",
            method: "POST",
            data: JSON.stringify({
                customer_code: customer.code,
            }),
            dataType: "json"
        });

        request.done(function(response) {

            var source = response.data;
            $('#source').find('option').remove();

            $('#source').append('<option value="">Seleccione Servicio</option>');

            $.each(source, function(i, val) {

                if (i != 0 && !(val.indexOf('Eliminado') != -1)) {
                    $('#source').append('<option value="' + i + '">' + val + '</option>');
                }
            });

            $.each(source, function(i, val) {

                if (i != 0 && val.indexOf('Eliminado') != -1) {
                    $('#source').append('<option value="' + i + '">' + val + '</option>');
                }
            });

            $('#source').append('<option value="0">Otras ventas</option>');
            $('#source').change();
        });

        request.fail(function(jqXHR) {

            if (jqXHR.status == 403) {

            	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            	setTimeout(function() { 
                    window.location.href = "/ispbrain";
            	}, 3000);

            } else {
            	generateNoty('error', 'Error al traer los servicios');
            }
        });
    }

    $(document).ready(function() {

        $('#btn-select-source').click(function() {
            //selecciono fuente
            var msgError = "";
            var flag = false;
            if ($( "#source option:selected" ).val() == "") {
                msgError += "* Debe seleccionar un Servicio";
                flag = true;
            }

            if (flag) {
                generateNoty('warning', msgError);
            } else {

                var connection_id = $( "#source option:selected" ).val();

                var request = $.ajax({
                    url: "/ispbrain/Customers/editAdministrativeMovement.json",
                    method: "POST",
                    data: JSON.stringify({
                        customer_code: customer.code,
                        connection_id: connection_id,
                        model: move_selected.model,
                        model_id: move_selected.id
                    }),
                    dataType: "json"
                });

                request.done(function(response) {

                    generateNoty(response.data.type, response.data.msg);
                    table_selected.draw();
                    $('#modal-edit-administrative-movement').trigger('CHANGED_EDIT_ADMINISTRATIVE__MOVEMENT_SUCCESSFUL');
                    $('#modal-edit-administrative-movement').modal('hide');
                });

                request.fail(function(jqXHR) {

                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al editar');
                    }
                });
            }

        });

        $('#modal-edit-administrative-movement').on('shown.bs.modal', function () {

            clearModal();

            customer = window.customer_selected;
            move = window.move_selected;
            table_selected = window.table_selected;

            getSourceAdministrativeMovement();
        });

    });

</script>
