<style type="text/css">

    #table-customers_wrapper .title {
        color: #696868;
        padding-top: 10px;
    }

    .h-card {
        height: 500px;
    }

</style>

<div class="modal fade" id="modal-customer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" aria-label="Close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="card border-secondary mb-3 mx-auto" id="card-customer-seeker" style=" <?=(isset($height)) ? 'height: ' . $height . 'px' : ''?>;">

                    <!--titlo del buscado-->
                    <div class="card-header w-100">
                         <div class="row">
                            <div class="col-9">
                                <h5 class="<?= isset($class_title) ? $class_title : '' ?>"> <?= isset($title) ? $title : '1. Buscar Cliente' ?></h5>
                            </div>
                        </div>
                    </div>

                    <!--tabla completa de clientes-->
                    <div id="table-seeker">

                        <div class="col-12 mt-2">

                            <table class="table table-bordered table-hover" id="table-customers">
                                <thead>
                                    <tr>
                                        <th >Código</th>    <!--0-->
                                        <th >Nombre</th>    <!--1-->
                                        <th >Documento</th> <!--2-->
                                        <th >Domicilio</th> <!--3-->
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-12 mt-2 mb-3">
                            <button type="button" class="btn btn-primary float-right" id="btn-selected">Asignar Pago</button>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var table_customers = null;
    var customer_selected = null;

    $(document).ready(function() {

        $('#btn-selected').click(function() {

            $('#btn-selected').addClass('d-none');

            if (customer_selected) {

                $('#card-customer-seeker').trigger('CUSTOMER_SELECTED');

            } else {

                $('#btn-selected').removeClass('d-none');
                generateNoty('warning', 'Debe seleccionar un cliente.');
            }
        });

        $('.btn-print-resume').click(function() {

             if (customer_selected) {
                 var url = "/ispbrain/customers/printresume/" + customer_selected.code;
                window.open(url, 'Resumen', "width=500, height=300");
             } else {
                generateNoty('warning', 'Debe seleccionar un cliente.');
             }
        });

        $('.btn-go-customer').click(function() {

            if (customer_selected) {
                var url = "/ispbrain/customers/view/" + customer_selected.code;
                 window.open(url, '_blank');
            } else {
                generateNoty('warning', 'Debe seleccionar un cliente.');
            }
        });

        // Jquery draggable modals
        $('.modal-dialog').draggable({
            handle: ".modal-header"
        });
    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    $(document).click(function() {
        $('[data-toggle="tooltip"]').tooltip('hide');
    });

    $('#modal-customer').on('shown.bs.modal', function () {

        if ($('#btn-selected').hasClass('d-none')) {
            $('#btn-selected').removeClass('d-none');
        }

        if (table_customers) {
            table_customers.draw();
        } else {
            $('#table-customers').removeClass('display');

            var loading = false;
            var enableLoad = 1;

            table_customers = $('#table-customers')
                .on( 'processing.dt', function ( e, settings, processing ) {
                    loading = processing;
                })
                .DataTable({
    		    "order": [[ 1, 'desc' ]],
                "processing": true,
                "serverSide": true,
    		    "ajax": {
                    "url": "/ispbrain/customers/get_customers_simple.json",
                    "dataFilter": function( data ) {
                        var json = $.parseJSON( data );
                        return JSON.stringify( json.response );
                    },
                    "data": function ( d ) {
                        return $.extend( {}, d, {
                            "enableLoad": enableLoad
                        });
                    },
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                				setTimeout(function() {
                				  window.location.href = "/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "scrollY": true,
    		    "scrollY": '330px',
    		    "scrollX": true,
    		    "scrollCollapse": true,
    		    "paging": true,
    		    "columns": [
                    { 
                        "data": "code",
                        "type": "integer",
                        "render": function ( data, type, row ) {
                            return pad(data, 5);
                        }
                    },
                    { 
                        "data": "name",
                        "type": "string",
                    },
                    { 
                        "data": "ident",
                        "type": "integer",
                        "render": function ( data, type, row ) {
                            var ident = "";
                            if (data) {
                                ident = data;
                            }
                            return sessionPHP.afip_codes.doc_types[row.doc_type] + ' ' + ident;
                        }
                    },
                    {
                        "data": "address",
                        "type": "string",
                    },
                ],
    		    "columnDefs": [
    		        {
                        "targets": [0],
                        "width": '5%'
                    },
                    {
                        "targets": [2],
                        "width": '8%'
                    },
                    {
                        "targets": [3],
                        "width": '18%'
                    },
                    {
                        "targets": [1],
                        "width": '12%'
                    },
                    {
                        "class": "left", targets: [2, 3]
                    },
                    {
                        "visible": false, targets:  []
                    },
                ],
                "createdRow" : function( row, data, index ) {
                   row.id = data.code;
                },
    		    "language": dataTable_lenguage,
    		    "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            	"dom":
        	    	"<'row'<'col-xl-6'l><'col-xl-6'f>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
    		});

    		$('#table-customers').on('processing.dt', function (e, settings, processing) {
                if (processing) {
                    selectEnable = false;
                } else {
                    selectEnable = true;
                }
            });

    		$('#table-customers_wrapper .title').append('<h5>Lista de Clientes</h5>');

            $('#table-customers tbody').on( 'click', 'tr', function (e) {

                if (!loading && !$(this).find('.dataTables_empty').length) {

                    table_customers.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');

                    customer_selected = table_customers.row( this ).data();
                }
            });
        }
    });

</script>
