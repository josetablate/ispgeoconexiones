<tr>
    <th><?= __('Estado') ?></th>
    <td class="pull-right font-weight-bold <?= $account->deleted ? 'text-danger' : 'text-success' ?>"><?= $account->deleted ? ' <i class="fas fa-minus-circle"></i> Deshabilitada' : '<i class="far fa-check-circle"></i> Habilitada' ?></td>
</tr>
<tr>
    <th><?= __('ID Comercio') ?></th>
    <td class="pull-right">
        <?= $account->merchant_id == NULL ? "" : $account->merchant_id ?>
    </td>
</tr>
<tr>
    <th><?= __('Código de Barra') ?></th>
    <td class="pull-right">
        <?= $account->barcode ?>
    </td>
</tr>
