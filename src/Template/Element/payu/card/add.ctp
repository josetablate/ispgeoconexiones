<style type="text/css">

    .sub-title {
        border-bottom: 1px solid #e3e2e2;
        width: 100%;
    }

    .my-hidden {
        display: none;
    }

    .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:active {
        color: #FF5722;
    }

    .modal a.btn {
        width: 100%;
        margin-bottom: 5px;
        text-align: left;
    }

    .checkbox label {
        border-bottom: 1px solid #e3e2e2;
        font-size: 20px;
        width: 100%;
    }

    .disabled {
        cursor: not-allowed;
    }

</style>

<div class="modal fade <?= $modal ?>" id="modal-add-card-payu" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel"><?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <?= $this->Form->create(null, [
                'url' => ['controller' => 'Payu', 'action' => 'markUsedCard'], 
                'id'  => "form-add-card-payu"]) ?>

            <div class="modal-body">
                <div class="row">

                    <div class="col-xl-12 mt-3">

                        <div class="row">

                            <?php
                                echo $this->Form->hidden('customer_code', ['id' => 'customer-code-add-card-payu', 'value' => $customer->code]);
                                echo $this->Form->hidden('tab', ['value' => $tab]);
                            ?>

                            <div class="col-xl-6 loading">
                                <label>Cargando...</label>
                            </div>

                            <div class="col-xl-12 card-data">
                                <?php
                                    echo $this->Form->input('payu_barcode', ['label' => 'Código de Barras', 'options' => [0 => "Seleccionar Código de Barras"], 'class' => 'selectpicker', 'data-live-search' => "true"]);
                                ?>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn-selected-card-payu">Asignar</button>
            </div>
            <?= $this->Form->end() ?>

            <?= $this->Form->create(null, [
                            'url' => ['controller' => 'Payu', 'action' => 'createCard'], 
                            'id'  => "form-create-card-payu"]) ?>

                    <div class="col-xl-12 mt-3">

                        <div class="row">

                            <?php
                                echo $this->Form->hidden('customer_code', ['id' => 'customer-code-create-card-payu', 'value' => $customer->code]);
                                echo $this->Form->hidden('tab', ['value' => $tab]);
                            ?>

                            <div class="col-xl-12">
                                <legend class="sub-title">Crear Tarjeta</legend>
                            </div>

                            <div class="col-xl-6">
                                <?php
                                    echo $this->Form->input('barcode', ['label' => 'Código Barra', 'type' => 'text']);
                                ?>
                            </div>

                            <div class="col-xl-12">
                                <button type="button" class="btn btn-primary" id="btn-create-card-payu">Crear</button>
                            </div>

                        </div>
                    </div>
                    <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<script type="text/javascript">

    function clearCdModal() {
        $('select[name=barcode]').val('');
        $('#payu-barcode').selectpicker('refresh');
    }

    var payu_card_selected = null;
    var cards = null;

    $(document).ready(function() {

        $('#btn-selected-card-payu').click(function() {

            var id_comercio = null;

            if ($("#payu-barcode").val() != "") {
                $("#form-add-card-payu").submit();
            } else {
                generateNoty('warning', 'Debe cargar elcódigo de barras.');
            }

            var id_comercio = null;

            $.each(cards, function( index, value ) {
                if (value.barcode == $('#payu-barcode option:selected').text()) {
                    id_comercio = value.id_comercio;
                }
            });

            if ($("#payu-barcode").val() != "") {
                $("#form-add-card-payu").submit();
            } else {
                generateNoty('warning', 'Debe seleccionar, una tarjeta por código de barra o nro de tarjeta.');
            }
        });

        $('#btn-create-card-payu').click(function() {

            if ($("#form-create-card-payu  #barcode").val() != "") {
                $("#form-create-card-payu").submit();
            } else {
                generateNoty('warning', 'Debe ingresar el código de barra de la tarjeta.');
            }
        });

        $('#modal-add-card-payu').on('shown.bs.modal', function () {

            $('.loading').removeClass('d-none');
            $('.card-data').addClass('d-none');

            $.ajax({
                url: "<?= $this->Url->build(['controller' => 'Payu', 'action' => 'getCards']) ?>",
                type: 'GET',
                dataType: "json",
                success: function(data) {

                    if (data.data.status == 200) {

                        $('.loading').addClass('d-none');
                        $('.card-data').removeClass('d-none');

                        cards = data.data.cards;

                        $("#payu-barcode").empty();

                        $("#payu-barcode").append("<option value=''>Seleccionar Código de Barras</option>");

                        $.each(data.data.cards, function( index, value ) {
                            $("#payu-barcode").append("<option value='" + value.id + "'>" + value.barcode + "</option>");
                        });

                        $('#payu-barcode').selectpicker('refresh');
                    } else {
                        $('.modal-add-card-payu').modal('hide');
                        generateNoty('warning', data.data.message);
                    }
                },
                error: function (e) {

                }
            });
        });

        $("#modal-add-card-payu").on("hidden.bs.modal", function () {
            $('#modal-add-card-payu').trigger('PAYU_CARD_SELECTED_CLOSED');
        });

    });

</script>
