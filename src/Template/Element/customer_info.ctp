<style type="text/css">

    .vertical-table {
        width: 100%;
    }

    table.vertical-table tbody tr {
        border-bottom: 1px solid #d8d8d8;
    }

</style>

<div class="card border-secondary mb-3" id="card-customer-info">
    <div class="card-header">
        1. Info Cliente
    </div>
    <div class="card-block card-data-block p-4">
        <table class="vertical-table">
            <tr>
                <th><?= __('Nombre') ?></th>
                <?php if (isset($presale->customer)): ?>
                    <td class="text-right text-right"><?= h($presale->customer->name) ?></td>
                <?php else: ?>
                     <td class="text-right text-right"></td>
                <?php endif; ?>
            </tr>
            <tr>
                <th><?= __('Código') ?></th>
                <?php if (isset($presale->customer)): ?>
                    <td class="text-right text-right"><?= str_pad($presale->customer->code, 5, '0', STR_PAD_LEFT) ?></td>
                <?php else: ?>
                     <td class="text-right text-right"></td>
                <?php endif; ?>
            </tr>
            <tr>
                <th><?= __('Documento') ?></th>
                <?php if (isset($presale->customer)): ?>
                    <td class="text-right text-right"><?= $doc_types[$presale->customer->doc_type] . ' ' . ($presale->customer->ident ? $presale->customer->ident : '') ?></td>
                <?php else: ?>
                     <td class="text-right text-right"></td>
                <?php endif; ?>
            </tr>
            <tr>
                <th><?= __('Domicilio') ?></th>
                <?php if (isset($presale->customer)): ?>
                    <td class="text-right text-right"><?= h($presale->customer->address) ?></td>
                <?php else: ?>
                     <td class="text-right text-right"></td>
                <?php endif; ?>
            </tr>
            <tr>
                <th><?= __('Teléfono') ?></th>
                <?php if (isset($presale->customer)): ?>
                    <td class="text-right text-right"><?= h($presale->customer->phone) ?></td>
                <?php else: ?>
                     <td class="text-right text-right"></td>
                <?php endif; ?>
            </tr>
            <tr>
                <th><?= __('Correo electrónico') ?></th>
                <?php if (isset($presale->customer)): ?>
                    <td class="text-right text-right"><?= h($presale->customer->email) ?></td>
                <?php else: ?>
                     <td class="text-right text-right"></td>
                <?php endif; ?>
            </tr>

        </table>

        <table class="vertical-table">
            <tr>
                <th><?= __('Etiquetas') ?></th>
                <?php if (isset($presale->customer)): ?>
                    <td class="text-right info-customer-labels">
                    <?php
                        foreach ($presale->customer->labels as $label) {
                            if ($label->id != 0) {
                                 echo "<span class='badge m-1' style='background-color: " . $label->color_back . "; color: " . $label->color_text . "' >" . $label->text . "</span>";
                            }
                        }
                    ?>
                    </td>
                <?php else: ?>
                     <td class="text-right info-customer-labels"></td>
                <?php endif; ?>
            </tr>
        </table>
    </div>
</div>
