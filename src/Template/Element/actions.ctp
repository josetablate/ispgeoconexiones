<style type="text/css">

    .modal a.btn {
        width: 100%;
        margin-bottom: 5px;
        text-align: left;
    }

</style>

<div class="modal fade <?= $modal ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-<?= isset($size) ? $size : 'sm' ?>" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel"><?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <center>
                            <?php
                                foreach ($buttons as $button) {

                                    $link = array_key_exists('link', $button) ? $button['link'] : 'javascript:void(0)';
                                    $id = array_key_exists('id', $button) ? $button['id'] : '';
                                    $target = array_key_exists('target', $button) ? $button['target'] : '_self';

                                    echo $this->Html->link(
                                        '<span class="glyphicon ' . $button['icon'] . ' text-white mr-2" aria-hidden="true"></span>' . $button['name'] . '',
                                        $link,
                                        ['id' => $id, 'target' => $target, 'class' => 'btn ' . $button['type']  , 'escape' => false]
                                    );
                               }
                           ?>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
