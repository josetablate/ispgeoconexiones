<style type="text/css">

    .modal a.btn {
        width: 100%;
        margin-bottom: 5px;
        text-align: left;
    }

    .lbl-title-text {
        text-align: left; font-size: 21px;
    }

    .lbl-description-text {
        text-align: left; font-size: 21px;
    }

    .lbl-description-value {
        text-align: left; font-size: 20px; height: 350px; overflow-y: auto;
    }

</style>

<div class="modal fade <?= $modal ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel"><?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <center>
                            <div class="row">
                                <div class="col-md-4" style="border: 1px solid #b5b4b4; border-radius: 5px;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <legend>Acciones</legend>
                                        </div>
                                        <div class="col-md-12">
                                            <?php
                                                foreach ($buttons as $button) {
        
                                                    $link = array_key_exists('link', $button) ? $button['link'] : 'javascript:void(0)';
                                                    $id = array_key_exists('id', $button) ? $button['id'] : '';
                                                    $target = array_key_exists('target', $button) ? $button['target'] : '_self';
        
                                                    echo $this->Html->link(
                                                        '<span class="glyphicon ' . $button['icon'] . ' text-white mr-2" aria-hidden="true"></span>' . $button['name'] . '',
                                                        $link,
                                                        ['id' => $id, 'target' => $target, 'class' => 'btn ' . $button['type']  , 'escape' => false]
                                                    );
                                               }
                                            ?>
                                         </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <legend class="lbl-title-text"><b>Titulo</b>: <span class="lbl-ticket-title-active"></span></legend>
                                        </div>
                                        <div class="col-md-12">
                                            <legend class="lbl-description-text"><b>Descripción</b>:</legend>
                                            <div class="lbl-description-value lbl-ticket-description-active"></div>
                                        </div>
                                    </div>
                                </div>
                           </div>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
