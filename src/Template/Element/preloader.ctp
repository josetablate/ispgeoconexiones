
<?php
  $bg = [
      'preloader-03.gif',
      'preloader-04.gif',
      'preloader-08.gif',
      'preloader-10.gif',
      'preloader-11.gif',
      'preloader-12.gif',
      ]; // array of filenames

  $i = rand(0, count($bg)-1); // generate random number size of the array
  $selectedBg = "$bg[$i]"; // set variable equal to which random filename was chosen
?>


<style type="text/css">
<!--
    .se-pre-con{
        background: url(/ispbrain/img/preloaders/<?php echo $selectedBg; ?>)  center no-repeat #ffffff;
    }
-->
</style>

<style type="text/css">

    .no-js #loader {
        display: none;
    }

    .js #loader {
        display: block;
        position: absolute;
        left: 100px;
        top: 0;
    }

    .se-pre-con {
    	position: fixed;
    	left: 0px;
    	top: 0px;
    	width: 100%;
    	height: 100%;
    	z-index: 9999;
    }

</style>

<div class="se-pre-con"></div>

<script type="text/javascript">

    window.onload = function(e) {

      $(".se-pre-con").fadeOut(800);

    }

</script>
