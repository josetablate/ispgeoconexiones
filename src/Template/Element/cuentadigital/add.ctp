<style type="text/css">

    .sub-title {
        border-bottom: 1px solid #e3e2e2;
        width: 100%;
    }

    .my-hidden {
        display: none;
    }

    .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:active {
        color: #FF5722;
    }

    .modal a.btn {
        width: 100%;
        margin-bottom: 5px;
        text-align: left;
    }

    .checkbox label {
        border-bottom: 1px solid #e3e2e2;
        font-size: 20px;
        width: 100%;
    }

    .disabled {
        cursor: not-allowed;
    }

    #form-generate-barcode-cuentadigital {
        width: 100%;
    }

</style>

<div class="modal fade <?= $modal ?>" id="modal-generate-barcode-cuentadigital" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel"><?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <div class="modal-body">
                <div class="row">

                    <?= $this->Form->create(NULL, [
                        'url' => [
                            'controller' => 'Cuentadigital',
                            'action'     => 'generateBarcode'
                        ],
                        'id' => "form-generate-barcode-cuentadigital"
                    ]) ?>

                    <div class="col-xl-12 mt-3">

                        <div class="row">

                            <div class="col-4">
                                <?php
                                    $credentials_posta = ['' => __('Seleccione')];
                                    foreach ($this->request->getSession()->read('payment_getway')->config->cuentadigital->credentials as $credential) {

                                        if ($credential->enabled) {
                                            $credentials_posta[$credential->id] = $credential->name . ' (' . $credential->account_number . ')';
                                        }
                                    }
                                    echo $this->Form->input('credential_id', ['id' => 'credential_id', 'label' => 'Cuenta', 'options' => $credentials_posta, 'required' => TRUE, 'default' => '']);
                                ?>
                            </div>

                            <?php
                                echo $this->Form->hidden('customer_code', ['id' => 'customer-code-generate-barcode-cuentadigital', 'value' => $customer->code]);
                                echo $this->Form->hidden('tab', ['value' => $tab]);
                            ?>

                        </div>
                        <button type="button" class="btn btn-primary" id="btn-generate-barcode-cuentadigital">Generar código de barra</button>
                    </div>

                    <?= $this->Form->end() ?>

                    <?= $this->Form->create(NULL, [
                        'url' => [
                            'controller' => 'Cuentadigital',
                            'action'     => 'createCard'
                        ],
                        'id' => "form-create-card-cuentadigital"
                    ]) ?>

                    <div class="col-xl-12 mt-3">

                        <div class="row">

                            <?php
                                echo $this->Form->hidden('customer_code', ['id' => 'customer-code-create-card-cuentadigital', 'value' => $customer->code]);
                                echo $this->Form->hidden('tab', ['value' => $tab]);
                            ?>

                            <div class="col-xl-12">
                                <legend class="sub-title">Cargar Código de Barra</legend>
                            </div>

                            <div class="col-4">
                                <?php
                                    echo $this->Form->input('credential_id', ['id' => 'credential_id', 'label' => 'Cuenta', 'options' => $credentials_posta, 'default' => '']);
                                ?>
                            </div>

                            <div class="col-xl-12">
                                <div class="form-group text mb-0">
                                    <label class="control-label" for="barcode">Código de Barra</label>
                                    <input type="text" name="barcode" id="barcode" class="form-control">
                                </div>
                                <small class="form-text text-muted mb-3">
                                    Ejemplos de códigos de barras: "0190<span style="background: #ffd659;">71199142</span>00" ó "71199142". Puede ingresar cualquiera de los 2. En caso que tenga un nro de 14 digitos similar al 1er nro del ejemplo, solamente ingresar los 8 digitos resaltados en amarillo, pero sin espacios en blanco y sin las comillas solamente el nro.
                                </small>
                            </div>

                        </div>
                        
                        <button type="button" class="btn btn-primary" id="btn-create-card-cuentadigital">Cargar</button>

                    </div>

                    <?= $this->Form->end() ?>

                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function() {

        $('#btn-generate-barcode-cuentadigital').click(function() {

            if ($('#credential_id option:selected').val() != '') {
                $("#form-generate-barcode-cuentadigital").submit();
            } else {
                generateNoty('warning', 'Debe seleccionar una cuenta.');
            }
        });

        $('#btn-create-card-cuentadigital').click(function() {

            if ($('#form-create-card-cuentadigital #credential_id option:selected').val() != ''
                || $('#form-create-card-cuentadigital #barcode').val() != '') {

                $("#form-create-card-cuentadigital").submit();
            } else {
                generateNoty('warning', 'Debe seleccionar al menos una cuenta.');
            }
        });

        $("#modal-generate-barcode-cuentadigital").on("hidden.bs.modal", function () {
            $('#modal-generate-barcode-cuentadigital').trigger('CUENTADIGITAL_GENERATE_BARCODE_CLOSED');
        });

        $('#modal-generate-barcode-cuentadigital').on('shown.bs.modal', function () {
            $('#credential_id').val('');
        });

    });

</script>
