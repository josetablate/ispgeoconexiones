<tr>
    <th><?= __('Estado') ?></th>
    <td class="pull-right font-weight-bold <?= $account->deleted ? 'text-danger' : 'text-success' ?>"><?= $account->deleted ? ' <i class="fas fa-minus-circle"></i> Deshabilitada' : '<i class="far fa-check-circle"></i> Habilitada' ?></td>
</tr>
<tr>
    <th><?= __('Nro de Cuenta') ?></th>
    <td class="pull-right">
        <?= $account->account_number ? $account->account_number : '' ?>
    </td>
</tr>
<tr>
    <th><?= __('Código de Barra') ?></th>
    <td class="pull-right">
        <?= $account->barcode ?>
    </td>
</tr>
