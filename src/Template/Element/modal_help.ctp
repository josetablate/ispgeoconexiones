<div class="modal fade" id="modal-help" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-<?=$size?>" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabel">Ayuda</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <p id="text-container"></p>
                    	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function openModalHelp(tex) {
        $('#modal-help #text-container').html(tex);
        $('#modal-help').modal('show');
    }

    function closedModalHelp() {
        $('#modal-help').modal('hide');
    }

</script>
