



<div class="row">
    <div class="col-xl-12">
        
        <div class="card border-secondary mb-3" id="card-accounts-seeker">
            
            <div class="card-body text-secondary ">
                
                <div id="parents-container">
                    <label class="m-2">Buscar Cuenta</label>
                    <?php echo $this->Form->select('parent', [] , ['id' => 'parent']); ?>
                </div>
                        
                <table class="table table-bordered table-hover" id="table-accounts">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Nombre</th>
                            <th>Description</th>
                            <th>Saldo</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div> 
        </div>
    </div>
</div>

 <div class="row">
    <div class="col-xl-12">
        <?= $this->Form->button(__('Cancelar'), ['type' => 'button', 'id' => 'btn-account-cancel', 'class' => 'btn-default float-left']) ?>
        <?= $this->Form->button(__('Seleccionar'), ['type' => 'button', 'id' => "btn-account-selected", 'class' => 'btn-success float-right']) ?>
    </div>
</div>





<script type="text/javascript">



    var table_accounts = null;
    var account_selected = null;
    
    function clear_card_accounts_seeker(){
        
        $('select#parent').val('');
        $('select#parent').change();
        account_selected = null;
        table_accounts.$('tr.selected').removeClass('selected');
    }
    
    function getParents(){
        
         $.ajax({
            url: "<?=$this->Url->build(['controller' => 'Accounts', 'action' => 'getAllParents'])?>" ,
            type: 'POST',
            dataType: "json",
            success: function(data){
                
                if(data != false){
                    
                    $('select#parent').append('<option value="">Todas las Cuentas (S/clientes) </option>');
                    $.each(data.parents, function(i, val){
                       $('select#parent').append('<option value="'+val.code+'">'+val.name+'</option>');
                    });
                  
                }
            },
            error: function (jqXHR) {
                if (jqXHR.status == 403) {
                
                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                
                	setTimeout(function() { 
                	  window.location.href ="/ispbrain";
                	}, 3000);
                
                } else {
                	generateNoty('error', 'Error al obtener las cuentas padres.');
                }
            }
        });
    }

    $(document).ready(function() {
        
        getParents();
       
        $('#table-accountss').removeClass('display');
		
	    table_accounts = $('#table-accounts').DataTable({
		    "order": [[ 0, 'asc' ]],
            "deferRender": true,
            "info": false,
		    "ajax": {
                "url": "/ispbrain/accounts/get_by_parent.json",
                "dataSrc": "accounts",
                "data": function ( d ) {
                  return $.extend( {}, d, {
                    "parent": $('select#parent').val(),
                  } );
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
		    "autoWidth": true,
		    "scrollY": '300px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": false,
		    "columns": [
                { 
                    "data": "code",
                },
                { 
                    "data": "name"
                },
                { 
                    "data": "description",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                { 
                    "data": "saldo",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' ),
                },
            ],
		    "columnDefs": [
                { 
                    "class": "left", targets: [1, 2]
                },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.code;
                if (data.title == '1') {
                    $(row).addClass('font-weight-bold title');
                }
            },
		    "language": dataTable_lenguage,
            "dom":
    	    	"<'row'<'col-xl-8 parents input-group'><'col-xl-4'f>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
		});
		
		$('select#parent').change(function() {
            table_accounts.ajax.reload();
        });
		
		$('#table-accounts_wrapper .parents').append($('#parents-container').contents());
	
        $('#table-accounts tbody').on( 'click', 'tr', function (e) {
            
            if(!$(this).find('.dataTables_empty').length){
                
                if(!$(this).hasClass('title')){
                    
                    table_accounts.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    
                    account_selected =  table_accounts.row( this ).data() ;
                    
                }
            }
        });
        
        $('#table-accounts tbody').on( 'dblclick', 'tr', function (e) {
            
            if(!$(this).find('.dataTables_empty').length){
                
                if(!$(this).hasClass('title')){
                    
                    table_accounts.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    
                    account_selected =  table_accounts.row( this ).data() ;
                    
                    $('#card-accounts-seeker').trigger('ACCOUNT_SELECTED');
                }
            }
        });
        
        $('#btn-account-selected').click(function() {
        
            if(account_selected){
                 
                $('#card-accounts-seeker').trigger('ACCOUNT_SELECTED');
                
            }else{
                generateNoty('warning', 'Debe seleccionar una cuenta.');
            }
        });
        
        $('#btn-account-cancel').click(function(){
     
            clear_card_accounts_seeker();
            $('#card-accounts-seeker').trigger('ACCOUNT_CANCEL');
        });

    });

</script>
