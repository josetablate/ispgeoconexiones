<style type="text/css">

    .modal {
	    overflow-x: hidden;
	    overflow-y: auto;
	}

    .description {
        height: 145px;
        width: 100%;
        margin-bottom: 20px;
    }

    .my-hidden {
        display: none;
    }

    .actions-tickets {
        border: 1px solid #cccccc59;
        padding: 18px;
        font-family: sans-serif;
    }

    .view-tickets {
        border: 1px solid #cccccc59;
        padding: 18px;
        font-family: sans-serif;
    }

    .card-header {
        font-size: 12px;
    }

    .badge {
        font-size: 54%;
    }

    .btn-default.active {
        color: #f05f40;
        background-color: #e6e6e6;
        border-color: #f05f40;
    }

    .table-tickets-records th {
        background-color: #607D8B !important;
        color: white !important;
    }

</style>

<div class="modal fade <?= $modal ?>" id="modal-add-ticket" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel"><?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-xl-12">
                        <?= $this->Form->create(NULL,  ['id' => 'form-add-ticket']) ?>
                            <fieldset>

                                <div class="row">

                                    <div class="col-md-12">
                                        <div class='input-group date' id='start-task-datetimepicker'>
                                            <label class="input-group-text" for="start-task">Inicio</label>
                                            <input name='start_task' id="start-task" type='text' class="form-control" required/>
                                            <span class="input-group-addon input-group-text calendar">
                                                <span class="glyphicon icon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <div class="input-group select required mt-2">
                                            <label class="input-group-text w-auto" for="category"><?= __('Categoría') ?></label>
                                            <select name="category" required id="category" class="form-control is-valid">
                                                <option value=""><?= __('Seleccionar') ?></option>
                                                <?php foreach ($categorias as $key => $categoria): ?>
                                                    <option value="<?= $key ?>"><i class="fas fa-square-full"></i> <?= $categoria ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="input-group select mt-2">
                                            <label class="input-group-text w-auto" for="area_id"><?= __('Área') ?></label>
                                            <select name="area_id" id="area_id" class="form-control is-valid">
                                                <option value=""><?= __('Seleccionar') ?></option>
                                                <?php foreach ($areas as $key => $area): ?>
                                                    <option value="<?= $key ?>"><i class="fas fa-square-full"></i> <?= $area ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>

                                        <hr class="mt-2 mb-2">

                                        <div class="form-group text required" style="padding: 14px;">
                                            <label class="control-label" for="title">Título</label>
                                            <input type="text" name="title" required="required" id="titcket-title" class="form-control" maxlength="100">
                                        </div>

                                        <hr class="mt-2 mb-2">

                                        <div class="form-group text">
                                            <textarea name="description" id="editor-ticket" required>
                                            </textarea>
                                        </div>

                                        <?php if ($customer->billing_for_service): ?>
                                            <?php echo $this->Form->input('connection_id_debt', ['options' => [], 'label' => 'Servicios y productos']); ?>
                                        <?php endif; ?>

                                    </div>
                                </div>

                            </fieldset>
                        <?= $this->Form->button(__('Agregar'), ['class' => 'btn-primary btn-add-ticket mt-1 ml-3 btn' ]) ?>
                        <?= $this->Form->end() ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var editor_ticket  = null;
    var customer = null;
    var connectionsArray = null;
    var table = null;
    var categorias = null;

    $(document).ready(function() {
        
         customer = <?php echo json_encode($customer); ?>;
         categorias = <?php echo json_encode($categorias); ?>;

         if (customer.billing_for_service) {

            connectionsArray = <?php echo json_encode($connectionsArray); ?>;

            $('select#connection-id-debt').html("");
            $('select#connection-id-debt').append('<option value="">Seleccione</option>');

            $.each(connectionsArray, function( index, con ) {
                $('select#connection-id-debt').append('<option value="' + index + '">' + con + '</option>');
            });
        }

        $('#start-task-datetimepicker').datetimepicker({
            defaultDate: new Date(),
            locale: 'es',
            format: 'DD/MM/YYYY HH:mm'
        });

        editor_ticket = CKEDITOR.replace( 'editor-ticket', {
             height: 180
        });

        CKEDITOR.config.title = false;
        CKEDITOR.config.toolbar = [
        	{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo' ] },
        	{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ] },
        	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent' ] },
        	{ name: 'styles', items: [ 'Format' ] },
        ];

        $('#form-add-ticket').submit(function(e) {

            e.preventDefault();

            if (editor_ticket.getData().length == 0) {
                generateNoty('warning', 'Debe ingresar un comentario.');
            } else {

                var data = {
                    category: $('select#category').val(),
                    title: $('#titcket-title').val(),
                    description: editor_ticket.getData(),
                    customer_code: customer.code,
                    start_task: $("#start-task").val(),
                    area_id: $("select[name=area_id]").val()
                };

                if (customer.billing_for_service) {
                    data.connection_id = $('select#connection-id-debt').val();
                }

                var request = $.ajax({
                    url: "/ispbrain/Tickets/addFromCustomer.json",
                    method: "POST",
                    data: JSON.stringify(data),
                    dataType: "json"
                });

                request.done(function(response) {

                    generateNoty(response.response.type, response.response.msg);

                    if (!response.response.error) {

                        $('#start-task-datetimepicker').data("DateTimePicker").date(new Date());
                        editor_ticket.setData('');
                        table = window.table_tickets;
                        if (customer.billing_for_service) {
                            $('select#connection-id-debt').val('');
                        }
                        $('select#category').val('');

                        table.draw();
                        $('#modal-add-ticket').modal('hide');
                    }
                });

                request.fail(function(jqXHR) {

                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al crear el ticket');
                    }
                });
            }
        });
    });

    $('#modal-add-ticket').on('shown.bs.modal', function () {
        table = window.table_tickets;
    });

</script>
