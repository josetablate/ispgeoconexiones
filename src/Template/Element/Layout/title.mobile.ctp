<style type="text/css">

    .header-title {
        text-align: center;
        color: #696868; 
        font-size: 24px;
        background-color: #bfbfbf; 
        margin: 0 0 10px 0; 
        border-radius: 0 0 5px 5px;
        font-weight: bold;
    }

    .my-hidden {
        display: none;
    }

    #btn-notification {
        cursor: pointer;
    }

    .lbl-notication-title:hover {
        color: #f05f40;
    }

    .animation-alert {
      animation: shake 1s cubic-bezier(.36,.07,.19,.97) both;
      transform: translate3d(0, 0, 0);
      backface-visibility: hidden;
      perspective: 1000px;
    }

    .modal-license .control-label {
        font-size: 13px;
    }

    .modal-license .table td, .table th {
        padding: 0.35rem;
    }

    .modal-license .table th {
        background-color: #ffffff !important;
    }

    @keyframes shake {
      10%, 90% {
        transform: translate3d(-1px, 0, 0);
      }

      20%, 80% {
        transform: translate3d(2px, 0, 0);
      }

      30%, 50%, 70% {
        transform: translate3d(-4px, 0, 0);
      }

      40%, 60% {
        transform: translate3d(4px, 0, 0);
      }
    }

</style>

<div class="row row justify-content-between header-title">

    <div class="col-sm-12">
        <?=$this->request->getSession()->read('titles')[$this->request->getParam('controller')][$this->request->getParam('action')] ?>
    </div>

    <div class="col-xs-12 col-sm-12">
        <div class="row justify-content-center">

            <!--datos de facturación-->
            <?php if ($this->request->getSession()->read('Auth.User')['billing'] && !$_SESSION['paraments']->billing->confirm): ?>
            <div class="col-auto pr-1 pl-1 pt-1 pb-1">

                <a id="btn-license" class ="btn btn-primary btn-sm mt-1" href="javascript:void(0)">Adquirir ISPBrain</a>

            </div>
            <?php endif; ?>

            <!--chat-->
            <?php if ($_SESSION['paraments']->gral_config->using_chat && $this->request->getSession()->read('Auth.User')['chat'] && $_SESSION['paraments']->gral_config->telegram_group_link != ''): ?>
            <div class="col-auto pr-1 pl-1 pt-1 pb-1 mr-2">

                <span id="btn-chat" class="fa-layers fa-fw" title="Chat">
                    <a href="javascript:void(0)"><i style="font-size: 38px; margin-top: 1px;" class="fas fa-comment"></i></a>
                    <label style="position: absolute; font-size: 9px; color: white !important; top: 12px; left: 2px;">Soporte</label>
                </span>

            </div>
            <?php endif; ?>

            <!--contador de errores o advertancia-->
            <div class="col-auto pr-1 pl-1 pt-1 pb-1">

                <span id="btn-errors" class="fa-layers fa-fw" style="font-size: 38px;" title="Registro de Errores o adventencias">
                    <i class="fas fa-window-close"></i>
                    <span id="counter_error" class="fa-layers-counter d-none" style="background:Tomato; font-size: 40px;">0</span>
                </span>

            </div>

            <!--contador de diferencias en los controadores-->
            <div class="col-auto pr-1 pl-1 pt-1 pb-1">
                <span id="btn-unsync" class="fa-layers fa-fw" style="font-size: 38px;" title="Diferencias en los controladores">
                    <i class="fas fa-exchange-alt" data-fa-transform="shrink-6" data-fa-mask="fas fa-square"></i>
                    <span id="counter_unsync" class="fa-layers-counter d-none" style="background:Tomato; font-size: 40px;">0</span>
                </span>
            </div>

            <!--contador de transferencia de caja -->
            <div class="col-auto pr-1 pl-1 pt-1 pb-1">
                <span id="btn-chash-transactions" class="fa-layers fa-fw" style="font-size: 35px;" title="Transferencia de caja pendientes ">
                    <i class="fas fa-donate"></i>
                    <span id="counter_chash_trans" class="fa-layers-counter d-none" style="background:Tomato; font-size: 40px;">0</span>
                </span>
            </div>

            <!--contador de recordatorios-->
            <div class="col-auto pr-1 pl-1 pt-1 pb-1">
                <span id="btn-notifications" class="fa-layers fa-fw" style="font-size: 35px;" title="Recordatorios">
                    <i class="fas fa-calendar-check"></i>
                    <span id="counter_notification" class="fa-layers-counter d-none" style="background:Tomato; font-size: 40px;">0</span>
                </span>
            </div>

            <div class="col-auto pr-1 pl-1 pt-1 pb-1">
                <span id="btn-wiki" class="fa-layers fa-fw" style="font-size: 35px;" title="Ir a la wiki">
                    <i class="fas fa-question-circle"></i>
                </span>
            </div>
            <div class="col-auto pr-1 pl-1 pt-1 pb-1">
                <span class="text-truncate float-right" style="width: 8rem;font-weight:  normal;font-size: 24px;">
                    <?=$username?> 
                </span>
            </div>
        </div>
    </div>

</div>

<!--diferencias en los controladores-->

<div id="template-row-unsync-controller-status">
    
    <div id="controller-${id}" style="padding: 12px; border-radius: 4px; background-color: #ffff6f;" class="form-check mb-2 d-none border border-secondary"> 
        <div class="row">  
            <div class="col-xl-2">
                <div class="row">
                    <div class="col-xl-12">
                        <label class="form-check-label text-secondary"> 
                            <span>${last_control_diff}</span>  
                        </label>
                    </div>
                </div>
            </div>        
            <div class="col-xl-10">
                <div class="row">
                    <div class="col-xl-12">
                        <label class="form-check-label text-secondary"> 
                            No se pudo establecer una conexión con el controlador  <span style="font-size: 20px;color: #FF5722;" >${name}</span> 
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="template-row-unsync-controller">

    <div id="controller-${id}" style="padding: 12px; border-radius: 4px;" class="form-check mb-2 d-none border border-secondary"> 
        <div class="row">
            <div class="col-xl-10">
                <div class="row">
                    <div class="col-xl-12">
                        <label class="form-check-label text-secondary"> 
                            Existen <span style="font-size: 20px;color: #FF5722;" >${diff}</span> diferencia(s) en el controlador <span style="font-size: 20px;color: #FF5722;" >${name}</span> 
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-xl-2">
                <button type="button" data-id="${controller_id}" class="btn btn-success float-right btn-go-sync text-center" title="Ir al sincronizador"><i class="fas fa-wrench"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-unsync-controllers" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="col-xl-11">
                      <div class="row">
                           <div class="col-12">
                                <h4 class="modal-title" id="gridSystemModalLabel">
                                    <i class="fas fa-exchange-alt" data-fa-transform="shrink-6" data-fa-mask="fas fa-square"></i> 
                                    Diferencias en controladores
                                </h4>
                           </div>
                           <div class="col-12">
                                 <small id="emailHelp" class="form-text text-muted">Actualización cada 5 minutos.</small>
                           </div>
                      </div>
                 </div>
                 <div class="col-xl-1 text-right">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-12">
                        <div id="container">
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>


<!--recordatorios-->

<div class="modal fade modal-notifications" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel"><a class="lbl-notication-title" href="/ispbrain/Notifications/index"><i class="fas fa-tasks" aria-hidden="true"></i> Recordatorios</a></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-12">
                        <div id="container">
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>

<!--transferencia de caja-->

<div id="template-row-transaccion">

    <div id="transaction-${id}" style="background: #8bc34a; padding: 12px; border-radius: 4px;" class="form-check mb-2 d-none"> 
        <div class="row">
            <div class="col-xl-10">
                <div class="row">
                    <div class="col-xl-4">
                        <label class="form-check-label text-white"> 
                        ${date}
                        </label>
                    </div>
                    <div class="col-xl-4 text">
                        <label class="form-check-label text-white"> 
                            ${user_origin}
                        </label>
                    </div>
                    <div class="col-xl-4">
                        <label class="form-check-label text-white"> 
                            ${value}
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12">
                        <label class="form-check-label text-secondary"> 
                            ${concept}
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-xl-2">
                <button type="button" data-id="${transaccion_id}" class="btn btn-success float-right btn-ok-transaccion">Ok</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-chash-transactions" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel"><i class="fas fa-donate"></i> Transferencias pendientes</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-12">
                        <div id="container">
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>

<!--errores-->

<div class="modal fade modal-error-log-quick-view" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel">Log de Error - Vista Rápida</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-12">
                        <?php echo $this->element('error_log_quick-view') ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<!--billing-->

<div class="modal fade modal-license" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <?php echo $this->Html->image('logo.png', ['alt' => 'ISPGEO', 'width' => 240]); ?>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <div class="modal-body">

               <form method="post" accept-charset="utf-8" role="form" action="<?=$this->Url->build(["controller" => "Settings", "action" => "billing"])?>">
                    <div style="display:none;">
                        <input type="hidden" name="_method" value="POST">
                    </div>

                    <div class="row">

                        <div class="col-12">

                            <p style="font-family: sans-serif;">Completa los datos y un agente administrativo te contactará para finalizar la compra.
                            </p>

                        </div>
                        <!-- <div class="col-12">
                            <label>Licencias de ISPBrain</label>
                        </div>
                        <div class="col-12">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Licencia (hasta)</th>
                                        <th>Abono mensual</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>50</td>
                                        <td>$800,00</td>
                                    </tr>
                                    <tr>
                                        <td>100</td>
                                        <td>$1.100,00</td>
                                    </tr>
                                    <tr>
                                        <td>200</td>
                                        <td>$1.500,00</td>
                                    </tr>
                                    <tr>
                                        <td>300</td>
                                        <td>$2.000,00</td>
                                    </tr>
                                    <tr>
                                        <td>500</td>
                                        <td>$2.300,00</td>
                                    </tr>
                                    <tr>
                                        <td>800</td>
                                        <td>$2.500,00</td>
                                    </tr>
                                    <tr>
                                        <td>1000</td>
                                        <td>$3.000,00</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div> -->

                        <div class="col-12">
                            <div class="row">

                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <legend style="border-bottom: 1px solid #cec6c6; font-size: 20px; font-family: sans-serif;">Datos de facturación</legend>
                                </div>

                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <?php
                                        $responsibles_billing = [];
                                        $responsibles_billing[1] = $this->request->getSession()->read('afip_codes')['responsibles'][1];
                                        $responsibles_billing[4] = $this->request->getSession()->read('afip_codes')['responsibles'][4];
                                        $responsibles_billing[6] = $this->request->getSession()->read('afip_codes')['responsibles'][6];
                                        echo $this->Form->input('responsible', ['id' => 'form-billing-responsible', 'label' => 'Condición frente al IVA', 'options' => $responsibles_billing, 'value' => $this->request->getSession()->read('paraments')->billing->responsible, 'required' => true]);
                                    ?>
                                </div>

                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <?php  echo $this->Form->input('ident', ['id' => 'form-billing-ident', 'label' => "CUIT", 'required' => true, 'value' => $this->request->getSession()->read('paraments')->billing->ident ]); ?>
                                </div>

                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <?php
                                        echo $this->Form->input('razon_social', ['id' => 'form-billing-razon_social', 'label' => 'Razón social', 'required' => true, 'value' => $this->request->getSession()->read('paraments')->billing->razon_social]);
                                    ?>
                                </div>

                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <?php
                                        echo $this->Form->input('province_billing', ['label' => 'Provincia', 'options' => $provinces_billing, 'title' => 'Seleccionar Provincia', 'class' => 'selectpicker', 'data-live-search' => "true", 'value' => $this->request->getSession()->read('paraments')->billing->province, 'required' => TRUE]);
                                    ?>
                                </div>

                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <?php
                                        echo $this->Form->input('localidad_billing', ['id' => 'form-billing-localidad', 'label' => 'Localidad', 'value' => $this->request->getSession()->read('paraments')->billing->localidad, 'required' => TRUE]);
                                    ?>
                                </div>

                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <?php
                                        echo $this->Form->input('address', ['id' => 'form-billing-address', 'label' => 'Domicilio', 'required' => true, 'value' => $this->request->getSession()->read('paraments')->billing->address]);
                                    ?>
                                </div>

                                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <?php
                                        echo $this->Form->input('email', ['id' => 'form-billing-email', 'label' => 'Correo Electrónico', 'required' => true, 'value' => $this->request->getSession()->read('paraments')->billing->email]);
                                    ?>
                                </div>

                                <?php echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]); ?>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xl-12">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" id="btn-submit" class="btn btn-success float-right"><?= ($this->request->getSession()->read('paraments')->billing->confirm) ? 'Editar'  : 'Confirmar' ?></button> 
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var csrfToken = <?= json_encode($this->request->getParam('_csrfToken')); ?>

    $.ajaxSetup({
        beforeSend: function (xhr) {
            xhr.setRequestHeader('X-CSRF-Token', csrfToken);
        }
    });

    var token = <?= !empty($token) ? json_encode($token) : "[]" ?>;
    if (token == "[]") {
        token = false;
    }

    var version = <?=json_encode($version)?>;
    var controller = <?=json_encode($this->request->getParam('controller'))?>;
    var action = <?=json_encode($this->request->getParam('action'))?>;

    function updateCountersTitle() {

        getErrors();

        getUnsyncControllers();

        getChashTransactions();

        getNotifications();

    }

    function getErrors() {

        if (sessionPHP.paraments.system.show_error_log) {

            $.ajax({
                url: "<?= $this->Url->build(['controller' => 'ErrorLog', 'action' => 'getCounterNewError']) ?>",
                type: 'POST',
                dataType: "json",
                success: function(data) {

                    var counter_error_current =  parseInt($('#counter_error').html());

                    if (data.error_new_count > 0) {

                        $('#counter_error').removeClass('d-none');

                        if (counter_error_current < data.error_new_count) {

                            $('#counter_error').html(data.error_new_count);
                            
                            $('#btn-errors').addClass('animation-alert');

                            setTimeout(function() { 
                                $('#btn-errors').removeClass('animation-alert');
                        	}, 3000);
                        }
                    } else {
                        $('#counter_error').addClass('d-none');
                    }

                },
                error: function (jqXHR) {

                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales. 3');

                    	setTimeout(function() { 
                            window.location.href ="/ispbrain";
                    	}, 3000);
                    
                    } else {

                    }

                }
            });
        }
    }

    function getChashTransactions() {

        $.ajax({
            url: "<?= $this->Url->build(['controller' => 'CashEntitiesTransactions', 'action' => 'verify']) ?>",

            type: 'POST',
            dataType: "json",
            success: function(data) {

                $( ".modal-chash-transactions #container" ).empty();

                if (data.transactions.length > 0) {

                    $('#counter_chash_trans').removeClass('d-none');

                    $.each(data.transactions, function(i, transaction) {

                        var template_row_transaccion = $('#template-row-transaccion').clone().html();

                        template_row_transaccion = template_row_transaccion.replace('d-none', '');
                        template_row_transaccion = template_row_transaccion.split('${id}').join( transaction.id );
                        template_row_transaccion = template_row_transaccion.split('${transaccion_id}').join( transaction.id );

                        var created = transaction.created.split('T')[0];
                        created = created.split('-');
                        created = created[2] + '/' + created[1] + '/' + created[0];
                        template_row_transaccion = template_row_transaccion.replace('${date}', created);
                        template_row_transaccion = template_row_transaccion.split('${user_origin}').join( transaction.origin.username );
                        template_row_transaccion = template_row_transaccion.split('${value}').join( number_format(transaction.value, true));
                        template_row_transaccion = template_row_transaccion.split('${concept}').join( transaction.concept);

                        $(".modal-chash-transactions #container").append( template_row_transaccion );

                    });

                } else {
                    $(".modal-chash-transactions #container").append( "<p>No existen transferencias.</p>" );
                }

                $('#counter_chash_trans').html(data.transactions.length);
            },
            error: function (jqXHR) {

                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales. 1');

                	setTimeout(function() { 
                	  window.location.href ="/ispbrain";
                	}, 3000);
                
                } else {

                }
            }
        });
    }

    function getNotifications() {

        $.ajax({
            url: "<?= $this->Url->build(['controller' => 'Notifications', 'action' => 'verify']) ?>",

            type: 'POST',
            dataType: "json",
            success: function(data) {

                $( ".modal-notifications #container" ).empty();

                if (data.notifications.length > 0) {

                    $('#counter_notification').removeClass('d-none');

                    var date = new Date();
                    var current_day = date.getDate();
                    var danger = false;

                    $.each(data.notifications, function( index, value ) {

                        var background = "#17a2b8";

                        if (current_day >= value.dueday) {
                            danger = true;
                            background = "#dc3545";
                        }

                        var value = '<div style="background: ' + background + '; padding: 12px; border-radius: 4px;" class="form-check mb-2"><input id="exampleCheck' + index + '" type="checkbox" class="notifications-check" data-id="' + value.id + '" data-notificator-id="' + value.notificator_id + '" name="' + value.name + '"> <label class="form-check-label text-white" for="exampleCheck' + index + '">' + value.message + '</label> </div>';
                        $( ".modal-notifications #container" ).append( value );

                    });

                    if (danger) {
                        $('#btn-notification').addClass('btn-danger');
                    } else {
                        $('#btn-notification').addClass('btn-info');
                    }

                    $('#btn-notification').removeClass('btn-secondary');

                } else {

                    $('#btn-notification').addClass('btn-secondary');
                    $('#btn-notification').removeClass('btn-info');

                    $( ".modal-notifications #container" ).append( "<p>No existen recordatorios.</p>" );

                }

                $('#counter_notification').html(data.notifications.length);

            },
            error: function (jqXHR) {

                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() { 
                        window.location.href = "/ispbrain";
                	}, 3000);

                } else {

                }
            }
        });
    }

    function getUnsyncControllers() {

        $.ajax({
            url: "<?= $this->Url->build(['controller' => 'Connections', 'action' => 'getCounterUnsync']) ?>",

            type: 'POST',
            dataType: "json",
            success: function(data) {

                 $( ".modal-unsync-controllers #container" ).empty();

                 if (data.controllers_unsync == -1) {
                     $(".modal-unsync-controllers #container").append( "<p>La conexión con los controladores esta desactivada.</p>");
                     $('#counter_unsync').html(0);
                     $('#counter_unsync').addClass('d-none');
                 } else {

                     if (data.controllers_unsync.length > 0) {

                        $('#counter_chash_trans').removeClass('d-none');

                        var count = 0;
                        
                        $.each(data.controllers_unsync, function(i, controller){
                             
                            if (controller.last_status && controller.counter_unsync > 0) {
                                 
                                 var template_row_unsync_controller = $('#template-row-unsync-controller').clone().html();
                             
                                 template_row_unsync_controller = template_row_unsync_controller.replace('d-none', '');
                                 template_row_unsync_controller = template_row_unsync_controller.split('${id}').join( controller.id );
                                 template_row_unsync_controller = template_row_unsync_controller.split('${controller_id}').join( controller.template + '#' + controller.id );                    

                                 var last_control_diff =  controller.last_control_diff.split('T');
                                 var date = last_control_diff[0].split('-');
                                 date = date[2] + '/' + date[1] + '/' + date[0];                             
                                 var hour = last_control_diff[1].split('-')[0].split(':');
                                 hour = hour[0] + ':' + hour[1] + ':' + hour[2];

                                 template_row_unsync_controller = template_row_unsync_controller.split('${last_control_diff}').join(date +' '+ hour);
                                 template_row_unsync_controller = template_row_unsync_controller.split('${diff}').join( controller.counter_unsync);
                                 template_row_unsync_controller = template_row_unsync_controller.split('${name}').join( controller.name);
                                 
                                 $(".modal-unsync-controllers #container").append( template_row_unsync_controller );
                                 
                                 count+=1;

                           } else if (!controller.last_status) {
                                
                                 var template_row_unsync_controller = $('#template-row-unsync-controller-status').clone().html();
                             
                                 template_row_unsync_controller = template_row_unsync_controller.replace('d-none', '');
                                 template_row_unsync_controller = template_row_unsync_controller.split('${id}').join( controller.id );
                                 template_row_unsync_controller = template_row_unsync_controller.split('${controller_id}').join( controller.template + '#' + controller.id );                    

                                 var last_control_diff =  controller.last_control_diff.split('T');
                                 var date = last_control_diff[0].split('-');
                                 date = date[2] + '/' + date[1] + '/' + date[0];                             
                                 var hour = last_control_diff[1].split('-')[0].split(':');
                                 hour = hour[0]+  ':' + hour[1] + ':' + hour[2];

                                 template_row_unsync_controller = template_row_unsync_controller.split('${last_control_diff}').join(date + ' ' + hour);                         
                                 template_row_unsync_controller = template_row_unsync_controller.split('${name}').join( controller.name);
                                 
                                 $(".modal-unsync-controllers #container").append( template_row_unsync_controller );
                                 
                                 count+=1;
                           }
                     
                        });
                        
                        if(count > 0){
                            $('#counter_unsync').html(count);
                            $('#counter_unsync').removeClass('d-none');
                        }
                        
                    } else {
                        $(".modal-unsync-controllers #container").append( "<p>No existen diferencias en los controladores.</p>" );
                        $('#counter_unsync').html(0);
                        $('#counter_unsync').addClass('d-none');
                    }
                }
            },
            error: function (jqXHR) {

                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() { 
        	            window.location.href = "/ispbrain";
                	}, 3000);

                } else {

                }
            }
        });

    }

    $(document).ready(function () {

        updateCountersTitle();

        //errores

        if (!sessionPHP.paraments.system.show_error_log) {
            $('#btn-errors').hide();
        }

        $('#btn-chat').click(function() {
            var url = sessionPHP.paraments.gral_config.telegram_group_link;
            if (url != "") {
                window.open(url,'_blank');
            }
        });

        $('#btn-license').click(function() {
            $('.modal-license').modal('show');
        });

        $('#btn-errors').click(function() {
            $('.modal-error-log-quick-view').modal('show');
        });

        $('.modal-error-log-quick-view').on('shown.bs.modal', function () {
            if (table_errors_quick_view) {
                table_errors_quick_view.ajax.reload();
            }
        });

        //recordatorios
        $('#btn-notifications').click(function() {
            $('.modal-notifications').modal('show');
        });

        $('.modal-notifications #container').on( 'click', '.notifications-check', function (e) {

            var arr = [];
            $('.notifications-check:checked').each(function () {
                var val = {
                    'id': $(this).data('id'),
                    'notificator_id': $(this).data('notificator-id')
                };
                arr.push(val);
            });

            if (arr.length > 0) {
                $('body')
                    .append( $('<form/>').attr({'action': '/ispbrain/Notifications/check', 'method': 'post', 'id': 'form-block'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'data', 'value': JSON.stringify(arr)}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#form-block').submit();
            } else {
                generateNoty('warning', "No ha marcado ninguna notificación como vista");
            }

        });

        //trasferencias de caja
        $('#btn-chash-transactions').click(function() {
            $('.modal-chash-transactions').modal('show');
        });

        $('.modal-chash-transactions #container').on( 'click', '.btn-ok-transaccion', function (e) {

            var transaction_id = $(this).data('id');
            var send_data = {};  
            send_data.transaction_id = $(this).data('id');

            $(this).hide();

            $.ajax({
                url: "<?= $this->Url->build(['controller' => 'CashEntities', 'action' => 'confirmTransaction']) ?>",
                type: 'POST',
                dataType: "json",
                data: JSON.stringify(send_data),
                success: function(data){

                    if (!data.response.error) {
                       generateNoty('success',data.response.msg);
                       $('#transaction-'+transaction_id).remove();
                    } else {
                       generateNoty('error', data.response.msg);
                    }
                    
                    $(this).show();
                    getChashTransactions();
                },
                error: function (jqXHR) {

                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales. 2');

                    	setTimeout(function() { 
        	                window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al intentar confirmar la transferencia.');

                    	$(this).show();
                    	getChashTransactions();
                    }
                }
            });
    
        });

        //direrencias en los controladores

        $('#btn-unsync').click(function() {
            $('.modal-unsync-controllers').modal('show');
        });

        $('.modal-unsync-controllers #container').on( 'click', '.btn-go-sync', function (e) {

            var data = $(this).data('id').split('#');

            var template = data[0];
            var id = data[1];
            var action = '/ispbrain/IspController'+template+'/sync/' + id;
            window.open(action, '_blank');

        });

        // wiki
        $('#btn-wiki').click(function() {
           var action = 'https://docs.ispbrain.io/';
           window.open(action, '_blank');
        });

        var text = sessionPHP.titles[controller][action];
        var desc = 'ISBRAIN: Software de Gestion para ISP | ' + version;
        document.title = text  + ' | ' + desc;

        $('.modal-dialog').draggable({
            handle: ".modal-header"
        });

    });

</script>
