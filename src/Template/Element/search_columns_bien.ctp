

<div class="modal fade  <?=$modal?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header p-2">
                
                <div class="row w-100">
                    <div class="col-xl-11 ">
                          <h5 class="modal-title text-center" id="exampleModalLabel">Buscar por columnas</h5>
                    </div>
                    <div class="col-xl-1">
                         <button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
               
            </div>
            <div class="modal-body p-3">
              
                <div class="row input-container">
               
                </div>
            </div>
            
            <div class="modal-footer p-2">
                <button class="btn-seconsary btn" id="btn-clear" type="button">Limpiar</button>
                <button class="btn-success btn" id="btn-search" type="button">Buscar</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">


    var table_selected_search_columns = null;
    var modal_selected_search_columns = null;
  
    $(document).ready(function(){
        
       // Jquery draggable modals
        $('.modal-dialog').draggable({
            handle: ".modal-header"
        });
    });

    
    function createModalSearchColumn(table, modal){
        
        table_selected_search_columns = table;
        modal_selected_search_columns = modal;
        
        var date_ids = [];
        
        var html = "";
        
        $.each(table_selected_search_columns.columns()[0], function(i, val){
            
            var sColumn = table.settings()[0].aoColumns[i];
            
            var column = table_selected_search_columns.column(i);
            
            if(typeof(sColumn.type) !== "undefined"){
                
                switch (sColumn.type) {
                    case 'string':
                        var name = $(column.header()).html();
                        html += "<div class='col-xl-4'>";
                        html += "   <div class='form-group text'>";
                        html += "       <input type='text' data-column='"+i+"' id='"+sColumn.data+"' class='column_search form-control' placeHolder='"+name+"'>";
                        html += "   </div>";
                        html += "</div>";
                        break;

                    case 'integer':
                        var name = $(column.header()).html();
                        html += "<div class='col-xl-4'>";
                        html += "   <div class='form-group'>";
                        html += "       <input type='number' step='1'  data-column='"+i+"' id='"+sColumn.data+"' class='column_search form-control' placeHolder='"+name+"'>";
                        html += "   </div>";
                        html += "</div>";
                        break;

                    case 'decimal':
                        var name = $(column.header()).html();
                        html += "<div class='col-xl-4 mb-3'>";
                        html += "   <div class='input-group column_search decimal_between'>";
                        html += "       <span class='input-group-text m-0'>"+name+"</span>";
                        html += "       <input type='text' class='form-control min' data-column='"+i+"' id='"+sColumn.data+"_min' placeholder='Min'>";
                        html += "       <input type='text' class='form-control max' data-column='"+i+"' id='"+sColumn.data+"_max' placeholder='Max'>";
                        html += "   </div>";
                        html += "</div>";
                        break;
                        
                    case 'bool':
                        var name = $(column.header()).html();
                        html += "<div class='col-xl-4'>";
                        html += "   <label class='form-check-label'>";
                        html += "       <input type='checkbox' class='column_search form-check-input' id='"+sColumn.data+"' data-column='"+i+"' > "+name;     
                        html += "   </label>";
                        html += "</div>";
                        break;

                    case 'date':
                        
                        var name = $(column.header()).html();
                        
                        html += "<div class='col-xl-8 column_search date_between'>";
                        html += "   <div class='row'>";
                        html += "       <div class='col-xl-6'>";
                        html += "           <div class='input-group date' id='"+sColumn.data+"_min-datetimepicker'>";
                        html += "               <span class='input-group-text m-0'>"+name+"</span>";
                        html += "               <input id='"+sColumn.data+"_min' type='text' class='form-control min' data-column='"+i+"' />";
                        html += "               <span class='input-group-addon input-group-text calendar m-0'>";
                        html += "                   <span class='glyphicon icon-calendar m-0'></span>";
                        html += "               </span>";
                        html += "           </div>";
                        html += "           <small  class='form-text text-muted mt-0 mb-1'>Desde</small>";
                        html += "       </div>";
                        html += "       <div class='col-xl-6'>";
                        html += "           <div class='input-group date' id='"+sColumn.data+"_max-datetimepicker'>";
                        html += "               <span class='input-group-text m-0'>"+name+"</span>";
                        html += "               <input id='"+sColumn.data+"_max' type='text' class='form-control max' data-column='"+i+"' />";
                        html += "               <span class='input-group-addon input-group-text calendar m-0'>";
                        html += "                   <span class='glyphicon icon-calendar m-0'></span>";
                        html += "               </span>";
                        html += "               </div>";
                        html += "               <small  class='form-text text-muted mt-0 mb-1'>Hasta</small>";
                        html += "           </div>";
                        html += "       </div>";
                        html += "</div>";
                        
                        date_ids.push(sColumn.data);
                        break;

                    case 'options':
                        var name = $(column.header()).html();
                        html += "<div class='col-xl-4'>";
                        html += "   <div class='form-group'>";
                        html += "       <select id='"+sColumn.data+"' class='input-group-text column_search form-control' data-column='"+i+"'>";
                        html += "           <option value=''>"+name+"</option>";
                        $.each(sColumn.options, function(i, option){
                            html += "           <option value='"+i+"'>"+option+"</option>";
                        });
                        html += "       </select>";
                        html += "   </div>";
                        html += "</div>";
                        break;
                }
            }
        });
        
        $(modal_selected_search_columns +' .input-container').append(html);
        
        $.each(date_ids, function(i, val){
            
            $('#'+val+'_min-datetimepicker').datetimepicker({
                locale: 'es',
                format: 'DD/MM/YYYY'
            });
            
            $('#'+val+'_max-datetimepicker').datetimepicker({
                locale: 'es',
                format: 'DD/MM/YYYY'
            });
                
        });
        
        $(modal_selected_search_columns).on( "click", "#btn-search", function() {
            
            $(modal_selected_search_columns + ' .column_search').each(function(){
                
                var value  = this.value;
                var column_index = $(this).data('column');
                
                if($(this).hasClass('decimal_between')){
                    
                    if($(this).find('.min').val() != '' ||  $(this).find('.max').val() != ''){
                        value = $(this).find('.min').val() +'<>'+ $(this).find('.max').val();
                    }else{
                        value = '';
                    }
                    column_index =  $(this).find('.min').data('column');
                }
                
                if($(this).hasClass('form-check-input')){
                    value =  $(this).is(":checked") ? 1 : 0;
                }
                
                if($(this).hasClass('date_between')){
                    
                    if($(this).find('.min').val() != '' ||  $(this).find('.max').val() != ''){
                        value = $(this).find('.min').val() +'<>'+ $(this).find('.max').val();
                    } else{
                        value = '';
                    }
                    column_index =  $(this).find('.min').data('column');
                }
             
                table_selected_search_columns.columns( column_index ).search( value );
           
            });
  
            table_selected_search_columns.draw();
            
            $(modal_selected_search_columns).modal('hide');
           
        });
        
        $(modal_selected_search_columns).on( "click", "#btn-clear", function() {
            
            $(modal_selected_search_columns + ' .column_search').each(function(){
                
                if($(this).hasClass('date_between')){
                    
                     $(this).find('.min').val('');
                     $(this).find('.max').val('');
                }
                
                if($(this).hasClass('decimal_between')){
                    
                     $(this).find('.min').val('');
                     $(this).find('.max').val('');
                }
               
                $(this).val('');
            });
            
            $("#btn-search").click();
           
        });
        
        $(".column_search").keyup(function(e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if(code == 13){
                $("#btn-search").click();
            }
        });
                
    }
    
       
   
   
  
    
    
</script>