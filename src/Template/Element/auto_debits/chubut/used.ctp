<style type="text/css">

    .sub-title {
        border-bottom: 1px solid #e3e2e2;
        width: 100%;
    }

    .my-hidden {
        display: none;
    }

    .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:active {
        color: #FF5722;
    }

    .modal a.btn {
        width: 100%;
        margin-bottom: 5px;
        text-align: left;
    }

    .checkbox label {
        border-bottom: 1px solid #e3e2e2;
        font-size: 20px;
        width: 100%;
    }

    .disabled {
        cursor: not-allowed;
    }

</style>

<div class="modal fade <?= $modal ?>" id="modal-used-auto-debit-chubut" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel"><?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
               <div class="row">
                    <div class="col-xl-12">
                        <select class="form-control" id="select-auto-debit-chubut"></select>
                    </div>

                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btn-selected-auto-debit-used-chubut">Seleccionar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function validateCBU(cbu) {

        if (cbu.length < 22) {
            return false;
        }

        var ponderador;
        ponderador = '97139713971397139713971397139713';

        var i;
        var nDigito;
        var nPond;
        var bloque1;
        var bloque2;

        var nTotal;
        nTotal = 0;

        bloque1 = '0' + cbu.substring(0, 7);

        for (i = 0; i <= 7; i++) {
            nDigito = bloque1.charAt(i);
            nPond = ponderador.charAt(i);
            nTotal = nTotal + (nPond * nDigito) - ((Math.floor(nPond * nDigito / 10)) * 10);
        }

        i = 0;

        while ( ((Math.floor((nTotal + i) / 10)) * 10) != (nTotal + i) ) {
            i = i + 1;
        }

        // i = digito verificador

        if (cbu.substring(7,8) != i) {
            return false;
        }

        nTotal = 0;

        bloque2 = '000' + cbu.substring(8, 21);

        for (i = 0; i <= 15; i++) {
            nDigito = bloque2.charAt(i);
            nPond = ponderador.charAt(i);
            nTotal = nTotal + (nPond * nDigito) - ((Math.floor(nPond * nDigito / 10)) * 10);
        }

        i = 0;

        while ( ((Math.floor((nTotal + i) / 10)) * 10) != (nTotal + i) ) {
            i = i + 1;
        }

        // i = digito verificador

        if (cbu.substring(21, 22) != i) {
            return false;
        } 

        return true;
    }

	function validateCUIT(sCUIT) {     
        var aMult = '5432765432'; 
        var aMult = aMult.split(''); 

        if (sCUIT && sCUIT.length == 11) 
        { 
            aCUIT = sCUIT.split(''); 
            var iResult = 0; 
            for (i = 0; i <= 9; i++) 
            { 
                iResult += aCUIT[i] * aMult[i]; 
            } 
            iResult = (iResult % 11); 
            iResult = 11 - iResult; 

            if (iResult == 11) iResult = 0; 
            if (iResult == 10) iResult = 9; 

            if (iResult == aCUIT[10]) 
            { 
                return true; 
            } 
        }     
        return false; 
    } 

    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( $email );
    }

    function clearCdModal()
    {
        $('#select-auto-debit-chubut').val('');
    }

    var auto_debit_chubut_selected = null;
    var connection = null;
    var connections = null;

    $(document).ready(function() {

        connection = <?= json_encode($connection) ?>;
        connections = <?= json_encode($connections) ?>;

        $('#btn-selected-auto-debit-used-chubut').click(function() {

            if ($("#select-auto-debit-chubut option:selected").val() != "") {

                $.each(connections, function( index, con ) {
                    if (con.id == $("#select-auto-debit-chubut option:selected").val()) {
                        auto_debit_chubut_selected = {
                            firstname: con.payment_method.firstname,
                            lastname: con.payment_method.lastname,
                            email: con.payment_method.email,
                            cuit: con.payment_method.cuit,
                            cbu: con.payment_method.cbu,
                            auto_debit_id: con.payment_method.auto_debit_id
                        };
                        return false;
                    }
                });

                $('.modal-used-auto-debit-chubut').modal('hide');
                $('#modal-used-auto-debit-chubut').trigger('AUTO_DEBIT_USED_CHUBUT_SELECTED');
            } else {
                generateNoty('warning', 'Debe seleccionar una tarjeta.');
            }
        });

        $('#modal-used-auto-debit-chubut').on('shown.bs.modal', function () {
            // if (!cobrodigital_selected) {
            //     clearCdModal();
            // }
            $('#select-auto-debit-chubut').html("");
            $('#select-auto-debit-chubut').append('<option value="">Seleccionar débito automático Chubut</option>');
            $.each(connections, function( index, con ) {
                if (con.payment_method.id == 102) {
                    var name = 'Nombre: ' + con.payment_method.firstname + ' - Apellido: ' + con.payment_method.lastname + ' - CUIT: ' + con.payment_method.cuit + ' - CBU: ' + con.payment_method.cbu + ' - Servicio: ' + con.service.name;
                    $('#select-auto-debit-chubut').append('<option value="' + con.id + '">' + name + '</option>');
                }
            });
        });

    });

</script>
