<style type="text/css">

    .sub-title {
        border-bottom: 1px solid #e3e2e2;
        width: 100%;
    }

    .my-hidden {
        display: none;
    }

    .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:active {
        color: #FF5722;
    }

    .modal a.btn {
        width: 100%;
        margin-bottom: 5px;
        text-align: left;
    }

    .checkbox label {
        border-bottom: 1px solid #e3e2e2;
        font-size: 20px;
        width: 100%;
    }

    .disabled {
        cursor: not-allowed;
    }

</style>

<script type="text/javascript" src="/ispbrain/js/common.js"></script>

<div class="modal fade <?= $modal ?>" id="modal-add-auto-debit-chubut" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel"><?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <?= $this->Form->create(null, [
                'url' => ['controller' => 'DebitAutoChubut', 'action' => 'createAutoDebitAccount'], 
                'id'  => "form-add-auto-debit-chubut"]) ?>

            <div class="modal-body">
                <div class="row">

                    <div class="col-xl-12 mt-3">

                        <div class="row">
                            <?php
                                echo $this->Form->hidden('customer_code', ['id' => 'customer-code-add-auto-debit-chubut', 'value' => $customer->code]);
                                echo $this->Form->hidden('tab', ['value' => $tab]);
                            ?>

                            <div class="col-xl-4">
                                <?php
                                    echo $this->Form->input('firstname', ['label' => 'Nombre', 'type' => 'text']);
                                ?>
                            </div>
                            <div class="col-xl-4">
                                <?php
                                    echo $this->Form->input('lastname', ['label' => 'Apellido', 'type' => 'text']);
                                ?>
                            </div>
                            <div class="col-xl-4">
                                <?php
                                    echo $this->Form->input('cuit', ['label' => 'CUIT', 'type' => 'number']);
                                ?>
                            </div>
                            <div class="col-xl-4">
                                <?php
                                    echo $this->Form->input('cbu', ['label' => 'CBU', 'type' => 'number']);
                                ?>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btn-select-auto-debit-chubut">Crear</button>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<script type="text/javascript">

    function validateCBU(cbu) {

        if (cbu.length < 22) {
            return false;
        }

        var ponderador;
        ponderador = '97139713971397139713971397139713';

        var i;
        var nDigito;
        var nPond;
        var bloque1;
        var bloque2;

        var nTotal;
        nTotal = 0;

        bloque1 = '0' + cbu.substring(0, 7);

        for (i = 0; i <= 7; i++) {
            nDigito = bloque1.charAt(i);
            nPond = ponderador.charAt(i);
            nTotal = nTotal + (nPond * nDigito) - ((Math.floor(nPond * nDigito / 10)) * 10);
        }

        i = 0;

        while ( ((Math.floor((nTotal + i) / 10)) * 10) != (nTotal + i) ) {
            i = i + 1;
        }

        // i = digito verificador

        if (cbu.substring(7, 8) != i) {
            return false;
        }

        nTotal = 0;

        bloque2 = '000' + cbu.substring(8, 21);

        for (i = 0; i <= 15; i++) {
            nDigito = bloque2.charAt(i);
            nPond = ponderador.charAt(i);
            nTotal = nTotal + (nPond * nDigito) - ((Math.floor(nPond * nDigito / 10)) * 10);
        }

        i = 0;

        while ( ((Math.floor((nTotal + i) / 10)) * 10) != (nTotal + i) ) {
            i = i + 1;
        }

        // i = digito verificador

        if (cbu.substring(21, 22) != i) {
            return false;
        } 

        return true;
    }

	function validateCUIT(sCUIT) {     
        var aMult = '5432765432'; 
        var aMult = aMult.split(''); 

        if (sCUIT && sCUIT.length == 11) { 
            aCUIT = sCUIT.split(''); 
            var iResult = 0; 
            for (i = 0; i <= 9; i++) { 
                iResult += aCUIT[i] * aMult[i]; 
            } 
            iResult = (iResult % 11); 
            iResult = 11 - iResult; 

            if (iResult == 11) iResult = 0; 
            if (iResult == 10) iResult = 9; 

            if (iResult == aCUIT[10]) { 
                return true; 
            } 
        }     
        return false; 
    } 

    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( $email );
    }

    function clearAutoDebitModal() {
        $('#btn-select-auto-debit-chubut').removeClass('my-hidden');
        $('#customer-code-add-auto-debit-chubut').val(customer.code);
        $('#firstname').val('');
        $('#lastname').val('');
        $('#cuit').val('');
        $('#cbu').val('');
    }

    var auto_debit_chubut_selected = null;

    $(document).ready(function() {

        $('#btn-select-auto-debit-chubut').click(function() {
            //selecciono debito automatico chubut
            var msgError = "";
            var flag = false;
            if ($("#firstname").val() == "") {
                if (flag) {
                    msgError += "<br/>";
                }
                msgError += "* Debe ingresar Nombre";
                flag = true;
            }
            if ($("#lastname").val() == "") {
                if (flag) {
                    msgError += "<br/>";
                }
                msgError += "* Debe ingresar Apellido";
                flag = true;
            }
            if (!validateCUIT($("#cuit").val())) {
                if (flag) {
                    msgError += "<br/>";
                }
                msgError += "* CUIT inválido";
                flag = true;
            }
            if (!validateCBU($("#cbu").val())) {
                if (flag) {
                    msgError += "<br/>";
                }
                msgError += "* CBU inválido";
                flag = true;
            }

            if (flag) {
                generateNoty('warning', msgError);
            } else {
                $('#btn-select-auto-debit-chubut').addClass('my-hidden');
                var cbu = $("#cbu").val();
                validateExistCBU(cbu, form_trigger_auto_debit_chubut);
            }
        });

        $('#modal-add-auto-debit-chubut').on('shown.bs.modal', function () {
            clearAutoDebitModal();
        });

        $("#modal-add-auto-debit-chubut").on("hidden.bs.modal", function () {
            $('#modal-add-auto-debit-chubut').trigger('AUTO_DEBIT_CHUBUT_SELECTED_CLOSED');
        });
    });

    var form_trigger_auto_debit_chubut = function(result) {
        if (result) {
            $("#form-add-auto-debit-chubut").submit();
        } else {
            $('#btn-select-auto-debit-chubut').removeClass('my-hidden');
        }
    }

</script>
