<div class="modal fade edit-debt-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabel">Editar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <form id="from-edit-debt">

                    <fieldset>

                        <div class="row">

                            <div class="col-xl-12">
                                <div class='form-group' >
                                    <label class="control-label" for="">Vencimiento</label>
                                    <div class='input-group date' id='duedate-datetimepicker'>
                                        <input name="duedate" id="duedate"  type='text' class="form-control is-valid" required />
                                        <span class="input-group-addon input-group-text calendar mr-0">
                                            <span class="glyphicon icon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-12">

                                <?php 
                                    echo $this->Form->input('debt_id', [
                                        'label' => false,
                                        'type' => 'hidden'
                                    ]);

                                    echo $this->Form->input('tipo_comp', [
                                        'label' => 'Destino',
                                        'class' => 'is-valid',
                                        'options' => []
                                    ]);
                                ?>
                            </div>

                        </div>

                    </fieldset>

                	<button type="button" class="btn btn-default mt-2" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-success mt-2 float-right">Guardar</button>
                </form>     

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var debt_selected = null;
    var table_view_customer = false; 

    $(document).ready(function() {

        $('.edit-debt-modal #duedate-datetimepicker').datetimepicker({
            locale: 'es',
            format: 'DD/MM/YYYY'
        });

        $('#btn-edit-debt').click(function() {

           if (!table_debts) {
               generateNoty('error', 'Error no existe la tabla de deudas');
           } else {

                if (debt_selected) {

                    $('.edit-debt-modal #tipo-comp').empty();


                    $.each(sessionPHP.paraments.invoicing.business, function(i, business) {
                        
                      

                        if (debt_selected.customer.business_billing == business.id) {
                            
                          

                            if (business.responsible == 1) { //ri

                                if (business.webservice_afip.crt != '') {
                                    $('.edit-debt-modal #tipo-comp').append('<option value="XXX">PRESU X</option><option value="001">FACTURA A</option><option value="006">FACTURA B</option>');
                                } else {
                                    $('.edit-debt-modal #tipo-comp').append('<option value="XXX">PRESU X</option>');
                                }

                            } else if (business.responsible == 6) { //mo

                                if (business.webservice_afip.crt != '') {
                                    $('.edit-debt-modal #tipo-comp').append('<option value="XXX">PRESU X</option><option value="011">FACTURA C</option>');
                                } else {
                                    $('.edit-debt-modal #tipo-comp').append('<option value="XXX">PRESU X</option>');
                                }

                            }
                        }
                    });

                    $('.edit-debt-modal #debt-id').val(debt_selected.id);

                    var duedate = debt_selected.duedate.split('T')[0].split('-');
                    duedate = duedate[2] + '/' + duedate[1] + '/' + duedate[0];

                    $('.edit-debt-modal #duedate').val(duedate);
                    $('.edit-debt-modal #tipo-comp').val(debt_selected.tipo_comp);
                }

                $('.modal').modal('hide');
                $('.edit-debt-modal').modal('show');
           }
        });

        $('#from-edit-debt').submit(function(event) {

            event.preventDefault();

            var send_data = {};  

            var form_data = $(this).serializeArray();
            $.each(form_data, function(i, val){
                send_data[val.name] = val.value;
            });

            $.ajax({
                url: "<?= $this->Url->build(['controller' => 'debts', 'action' => 'editAjax']) ?>",
                type: 'POST',
                dataType: "json",
                data: JSON.stringify(send_data),
                success: function(data) {

                    if (data.debt) {


                        debt_selected.duedate = data.debt.duedate;
                        debt_selected.tipo_comp = data.debt.tipo_comp;

                        
                        // if(table_view_customer){
                        //     table_debts.row($('#table-debts tr#' + data.debt.id)).data(debt_selected).draw();
                        // }else{
                            table_debts.row($('#table-debts tr#debt_' + data.debt.id)).data(debt_selected).draw();
                        // }                      

                        
                        $('.modal').modal('hide');

                        $('.edit-debt-modal').trigger('EDIT_DEBT_COMPLETED');
                    }
                },
                error: function (jqXHR) {

                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                    	  window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al intentar editar la deuda');
                    }
                }
            });
        });

        $('.edit-debt-modal #from-edit-debt').keypress(function(event) {
            if ( event.which == 13 ) {
                event.preventDefault();
                return false;
            }
        });
    });

</script>
