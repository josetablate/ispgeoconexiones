<style type="text/css">

.row-card:hover {
    background-color: #cbffd2 !important;
}

</style>

<div class="col-12">

    <div class="card border-secondary mb-1 m p-0 w-100 row-card" id="<?= $id ?>" >

        <div class="card-body p-1">

            <div class="row">
                <div class="col-12">
                    <h5 class="border-bottom"><?= $service->name ?></h5>
                </div>
            </div>

            <div class="row">

                <div class="col-6">
                    <div class="row">
                        <div class="col-xl-6">
                            <label class="font-weight-bold m-0">Instalación</label>
                        </div>
                        <div class="col-xl-6 text-xl-right">
                            <span class=""><?= date('d/m/Y', strtotime($service->installation_date->format('Y-m-d'))) ?></span>
                        </div>
                    </div>
                </div>

                <div class="col-6">
                    <div class="row">
                        <div class="col-xl-6">
                            <label class="font-weight-bold m-0">Precio</label>
                        </div>
                        <div class="col-xl-6 text-xl-right">
                            <span class="">$<?= number_format($service->service_total, 2, ',', '.') ?></span>
                        </div>
                    </div>
                </div>

                <div class="col-12">

                    <div class="row">
                        <div class="col-xl-4">
                            <label class="font-weight-bold m-0">Descuento</label>
                        </div>
                        <div class="col-xl-8 text-xl-right">
                            <span class=""><?= $service->discount_name ? h($service->discount_name) : ''?></span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">

                     <div class="row">
                        <div class="col-xl-4">
                            <label class="font-weight-bold m-0">Domicilio</label>
                        </div>
                        <div class="col-xl-8 text-xl-right">
                            <span class=""><?= h($service->address) ?></span>
                        </div>
                    </div>
                </div>

                <div class="col-6">

                     <div class="row">
                        <div class="col-xl-6">
                            <label class="font-weight-bold m-0">Disponibilidad</label>
                        </div>
                        <div class="col-xl-6 text-xl-right">
                            <span class=""><?= h($service->availability) ?></span>
                        </div>
                    </div>
                </div>

                <div class="col-6">
                    <div class="row">
                        <div class="col-xl-6">
                            <label class="font-weight-bold m-0">Total</label>
                        </div>
                        <div class="col-xl-6 text-xl-right">
                            <span class="">
                                <?php
                                    if ($service->discount_total != null) {
                                        $total = $service->service_total * (1 - $service->discount_total);
                                    } else {
                                        $total = $service->service_total;
                                    }
                                    echo '$' . number_format($total, 2, ',', '.');
                                ?>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="col-6">

                     <div class="row">
                        <div class="col-xl-6">
                            <label class="font-weight-bold m-0">Generar Ticket</label>
                        </div>
                        <div class="col-xl-6 text-xl-right">
                            <span class=""><?= $service->generate_ticket ? 'Si' : 'No' ?></span>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

</div>
