<style type="text/css">

    .row-card:hover {
        background-color: #cbffd2 !important;
    }

</style>

<div class="col-12">

    <div class="card border-secondary mb-1 p-0 w-100 row-card" id="<?= $id ?>" >

        <div class="card-body p-1">

            <div class="row">
                <div class="col-12">
                    <h5 class="border-bottom"><?= $package->name ?></h5>
                </div>
            </div>

            <div class="row">
                
                <div class="col-6">

                     <div class="row">
                        <div class="col-xl-6">
                            <label class="font-weight-bold m-0">Cuotas</label>
                        </div>
                        <div class="col-xl-6 text-xl-right">
                            <span class=""><?= $package->dues ?></span>
                        </div>
                    </div>
                </div>

                <div class="col-6">
                    <div class="row">
                        <div class="col-xl-6">
                            <label class="font-weight-bold m-0">Valor cuota</label>
                        </div>
                        <div class="col-xl-6 text-xl-right">
                            <span class="">$<?= number_format($package->package_total_due, 2, ',', '.') ?></span>
                        </div>
                    </div>
                </div>

                <div class="col-6">

                     <div class="row">
                        <div class="col-12">
                            <label class="font-weight-bold m-0">Descuento</label>
                        </div>
                        <div class="col-12">
                            <span class=""><?= $package->discount_name ? h($package->discount_name) : '' ?></span>
                        </div>
                    </div>
                </div>

                <div class="col-6">
                    <div class="row">
                        <div class="col-xl-6">
                            <label class="font-weight-bold m-0">Total</label>
                        </div>
                        <div class="col-xl-6 text-xl-right">
                            <span class="">$<?= number_format($package->package_total, 2, ',', '.') ?></span>
                        </div>
                    </div>
                </div>

                <?php if ($package->connection_id != NULL): ?>
                    <div class="col-12">

                         <div class="row">
                            <div class="col-12">
                                <label class="font-weight-bold m-0">Pago relacionado con el Servicio</label>
                            </div>
                            <div class="col-12">
                                <span class=""><?= $package->pay_with_connection ? h($package->pay_with_connection) : '' ?></span>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

            </div>

        </div>
    </div>

</div>
