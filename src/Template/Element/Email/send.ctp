<style type="text/css">

    .modal {
	    overflow-x: hidden;
	    overflow-y: auto;
	}

    .description {
        height: 145px;
        width: 100%;
        margin-bottom: 20px;
    }

    .my-hidden {
        display: none;
    }

    .actions-tickets {
        border: 1px solid #cccccc59;
        padding: 18px;
        font-family: sans-serif;
    }

    .view-tickets {
        border: 1px solid #cccccc59;
        padding: 18px;
        font-family: sans-serif;
    }

    .card-header {
        font-size: 12px;
    }

    .badge {
        font-size: 54%;
    }

    .btn-default.active {
        color: #f05f40;
        background-color: #e6e6e6;
        border-color: #f05f40;
    }

    .table-tickets-records th {
        background-color: #607D8B !important;
        color: white !important;
    }

</style>

<div class="modal fade <?= $modal ?>" id="modal-send-email" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel"><?= $title ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-xl-12">
                        <?= $this->Form->create(NULL, ['id' => 'form-send-email']) ?>
                            <fieldset>

                                <div class="row">
                                    <div class="col-12">
                                        <label class="control-label mt-1" for="mass-emails-template-id" style="margin-bottom: 0;">Plantilla</label>
                                        <?php 
                                            echo $this->Form->input('template_id', ['class' => '', 'options' => $mass_emails_templates,  'label' =>  '', 'default' => '' , 'required' => TRUE]);
                                        ?>
                                    </div>
                                </div>

                            </fieldset>
                        <?= $this->Form->button(__('Agregar'), ['class' => 'btn-primary mt-1 ml-3 btn' ]) ?>
                        <?= $this->Form->end() ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var mass_emails_templates  = null;
    var model = null;
    var model_id = null;

    $(document).ready(function() {

        mass_emails_templates = <?php echo json_encode($mass_emails_templates); ?>;

        $('#form-send-email').submit(function(e) {

            e.preventDefault();

            var data = {
                model: model,
                model_id: model_id,
                template_id: $( "#template-id option:selected" ).val()
            };

            var request = $.ajax({
                url: "/ispbrain/MassEmails/sendEmail",
                method: "POST",
                data: JSON.stringify(data),
                dataType: "json"
            });

            request.done(function(response) {

                generateNoty(response.response.type, response.response.message);

                if (!response.response.error) {
                    $('#modal-send-email').modal('hide');
                }
            });

            request.fail(function(jqXHR) {

                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() { 
                        window.location.href = "/ispbrain";
                	}, 3000);

                } else {
                	generateNoty('error', 'Error al enviar el Correo');
                }
            });
        });
    });

    $('#modal-send-email').on('shown.bs.modal', function () {
        model = window.model;
        model_id = window.model_id;
    });

</script>
