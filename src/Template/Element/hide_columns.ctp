<div class="modal fade  <?= $modal ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header pb-0">

                <div class="row w-100">
                    <div class="col-xl-8">
                          <h5 class="modal-title float-left" id="exampleModalLabel">Columnas</h5>
                    </div>
                    <div class="col-xl-3">
                         <?php

                         echo $this->Html->link(
                            '<span class="glyphicon icon-floppy-disk" aria-hidden="true"></span> Guardar',
                            'javascript:void(0)',
                            [
                                'id' => 'btn-save-hidden-column-selected',
                                'title' => '',
                                'class' => 'btn btn-default float-right',
                                'escape' => false
                            ]);

                        ?>
                    </div>
                    <div class="col-xl-1">
                         <button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>

            </div>
            <div class="modal-body pt-4 pl-5 checkbox-container">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var table_selected_hide_columns = null;
    var index_selected = '';
    var hide_columns = [];
    var modal_selected_hide_columns = null;

    $(document).ready(function() {

       // Jquery draggable modals
        $('.modal-dialog').draggable({
            handle: ".modal-header"
        });
    });

    $('#btn-save-hidden-column-selected').click(function() {
        savePreferences();
    });

    /* parameters: 
     * table: objeto tabla de datatable
     * index: indice para ubicar lo que se guarda, como ayuda se puede usar la seccion
     */
    function loadPreferences(index, default_hide_columns = null) {

        index_selected = index;

        if (typeof(Storage) !== "undefined") {

            var preferences = JSON.parse(localStorage.getItem(index_selected + '-' + sessionPHP.Auth.User.id));

            if (preferences !== null) {

                $.each(preferences, function(i, val) {
                    if (!val) {
                         hide_columns.push(parseInt(i));
                    }
                });
            } else {
                hide_columns = default_hide_columns;
            }

        } else {
             generateNoty('information', 'El navegador no soporta almacenamiento web.');
        }
    }

    /* parameters: 
     * table: objeto tabla de datatable
     * index: indice para ubicar lo que se guarda, como ayuda se puede usar la seccion
     */
    function savePreferences() {

        if (typeof(Storage) !== "undefined") {
            // Code for localStorage/sessionStorage.

            var preferences = {};

            $.each(table_selected_hide_columns.columns()[0], function(i, val) {

                var column = table_selected_hide_columns.column(i);
                preferences[i] = column.visible();
            });

            if (Object.keys(preferences).length > 0) {

                var item = index_selected + '-' + sessionPHP.Auth.User.id;

                localStorage.setItem(item, JSON.stringify(preferences));
            }

            generateNoty('information', 'Selección guardada.');

        } else {
            generateNoty('information', 'El navegador no soporta almacenamiento web.');
        }

        $(modal_selected_hide_columns).modal('hide');
    }

    function createModalHideColumn(table, modal, no_check = []) {

        table_selected_hide_columns = table;
        modal_selected_hide_columns = modal;

        var checkbox = "";
        checkbox += "<div class='row' style='margin-right: 0; margin-left: 0;'>";

        $.each(table_selected_hide_columns.columns()[0], function(i, val) {

            var column = table_selected_hide_columns.column(i);
            var ckecked = column.visible() ? 'checked' : '';
            var name = $(column.header()).html();

            if (jQuery.inArray(i, no_check) == -1) {

                checkbox += "   <div class='col-sm-6 col-md-6 col-lg-4 col-xl-3'>";
                checkbox += "       <label class='form-check-label'>";
                checkbox += "           <input type='checkbox' class='form-check-input toggle-hide-column' " + ckecked + " data-column='" + i + "' > " + name;            
                checkbox += "       </label>";
                checkbox += "   </div>";
            }

        });

        $(modal_selected_hide_columns + ' .checkbox-container').append(checkbox);

        checkbox += "</div>";

        $('.toggle-hide-column').on( 'click', function (e) {

            var column = table_selected_hide_columns.column( $(this).data('column'));
            column.visible(  $(this).is(":checked") );
            table_selected_hide_columns.columns.adjust().draw();

        });
    }

</script>
