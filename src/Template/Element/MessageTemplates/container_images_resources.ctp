<style type="text/css">
    
    img.imagen_resource{
        
        width: 72px; height: auto;
    }
    
    div.container_resources{
        overflow: auto;
        max-height: 500px;
    }

</style>

<a class="btn btn-default btn-add-recurso" title="Agregar recurso" role="button">
    Agregar Imagen
</a>

<br>

<div class="container_resources">
    <?php foreach ($templateResources as $templateResource): ?>
    
        <img class="imagen_resource"  data-id="<?=$templateResource->id?>" data-templateid="<?= $messageTemplate->id ?>" data-action="<?= $current_action ?>" src="<?= $this->Url->build('../../avisos/resources', true) .'/'. $templateResource->url ?>" alt=""/>
             
    <?php endforeach; ?>
</div>



<div id="assets-popup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel"><?= __('Añadir Imagen') ?></h4>
      </div>
      <div class="modal-body">
            <?= $this->Form->create(null, ['url' => ['controller' => 'TemplateResources' , 'action' => 'add'], 'enctype' => "multipart/form-data" ]) ?>
            <fieldset>
                <?php
                    echo $this->Form->file('files[]', ['required' => true, 'multiple'=>'multiple']);
                ?>
            </fieldset>
            <br>
            <input type="hidden" name="action" value="<?= $current_action ?>">
            <input type="hidden" name="message_template_id" value="<?= $messageTemplate->id ?>">
            <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Cancelar') ?></button>
            <?= $this->Form->button(__('Agregar')) ?>
            <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</div>

<script>


    $(document).ready(function() {

        $('a.btn-add-recurso').click(function() {
            $('#assets-popup').modal('show');
        });

        // Jquery draggable modals
        $('.modal-dialog').draggable({
            handle: ".modal-header"
        });

        $('.imagen_resource').click(function() {

            var text = 'Esta Seguro que desea eliminar el recurso. Este puede estar siendo ocupado por otro template ?'; 
            var id = $(this).data('id');
            var current_action = $(this).data('action');
            var message_template_id = $(this).data('templateid');

            bootbox.confirm(text, function(result) {
                if (result) {
                    
                    // alert('asdfas');
                    
                    $('body')
                    .append( $('<form/>').attr({'action': '/ispbrain/TemplateResources/delete/', 'method': 'post', 'id': 'replacer'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'message_template_id', 'value': message_template_id}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'action', 'value': current_action}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer').submit();
                }
            });
        });
    });

</script>
