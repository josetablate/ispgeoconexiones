<div class="alert alert-<?= $status ?>" role="alert">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <p><?= $message ?></p>
    <hr>
    <p class="mb-0"><span class="font-weight-bold" >ISPBrain</span>.</p>
</div>
