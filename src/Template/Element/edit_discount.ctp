<div class="modal fade edit-discount-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabel">Editar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <form id="from-edit-discount">

                    <fieldset>

                        <div class="row">

                            <div class="col-xl-12">

                                <?php 
                                    echo $this->Form->input('discount_id', [
                                        'label' => false,
                                        'type' => 'hidden'
                                    ]);

                                    echo $this->Form->input('tipo_comp', [
                                        'label' => 'Destino',
                                        'class' => 'is-valid',
                                        'options' => []
                                    ]);
                                ?>
                            </div>

                        </div>

                    </fieldset>

                	<button type="button" class="btn btn-default mt-2" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-success mt-2 float-right">Guardar</button>
                </form>     

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var discount_selected = null;
    
    var table_view_customer = false;

    $(document).ready(function() {

        $('#btn-edit-discount').click(function() {

            if (!table_debts) {
                generateNoty('error', 'Error no existe la tabla de deudas');
            } else{

                if (discount_selected) {

                    $('.edit-discount-modal #tipo-comp').empty();

                    $.each(sessionPHP.paraments.invoicing.business, function(i, business) {

                        if (discount_selected.customer.business_billing == business.id) {
                            
                            if (business.responsible == 1) { //ri

                                if (business.webservice_afip.crt != '') {
                                    $('.edit-discount-modal #tipo-comp').append('<option value="XXX">PRESU X</option><option value="001">FACTURA A</option><option value="006">FACTURA B</option>');
                                } else {
                                    $('.edit-discount-modal #tipo-comp').append('<option value="XXX">PRESU X</option>');
                                }

                            } else if (business.responsible == 6) { //mo

                                if (business.webservice_afip.crt != '') {
                                    $('.edit-discount-modal #tipo-comp').append('<option value="XXX">PRESU X</option><option value="011">FACTURA C</option>');
                                } else {
                                    $('.edit-discount-modal #tipo-comp').append('<option value="XXX">PRESU X</option>');
                                }

                            }
                        }
                    });

                    $('.edit-discount-modal #discount-id').val(discount_selected.id);
                    $('.edit-discount-modal #tipo-comp').val(discount_selected.tipo_comp);
                }

                $('.modal').modal('hide');
                $('.edit-discount-modal').modal('show');
           }
        });

        $('#from-edit-discount').submit(function(event) {

            event.preventDefault();

            var send_data = {};  

            var form_data = $(this).serializeArray();
            $.each(form_data, function(i, val) {
                send_data[val.name] = val.value;
            });

            $.ajax({
                url: "<?= $this->Url->build(['controller' => 'CustomersHasDiscounts', 'action' => 'editAjax']) ?>",
                type: 'POST',
                dataType: "json",
                data: JSON.stringify(send_data),
                success: function(data) {

                    if (data.discount) {

                        discount_selected.tipo_comp = data.discount.tipo_comp;
                        
                        // if(table_view_customer){
                        //     table_debts.row($('#table-debts tr#' + data.discount.id)).data(discount_selected).draw();
                        // }else{
                            table_debts.row($('#table-debts tr#discount_' + data.discount.id)).data(discount_selected).draw();
                        // }
                        
                        $('.modal').modal('hide');

                        $('.edit-discount-modal').trigger('EDIT_DEBT_COMPLETED');
                    }
                },
                error: function (jqXHR) {

                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                    	  window.location.href ="/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al intentar editar el descuento');
                    }
                }
            });
        });

        $('.edit-discount-modal #from-discount-debt').keypress(function( event ) {
            if ( event.which == 13 ) {
                event.preventDefault();
                return false;
            }
        });
    });

</script>
