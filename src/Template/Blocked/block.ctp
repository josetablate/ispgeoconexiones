

    <?= $this->Html->css([
      '/vendor/Bootstrap/css/bootstrap.min',
      ]) ?>
      

    <?= $this->fetch('css') ?>


<div class="container">
  
    <div class="row justify-content-center mt-5">
        <div class="col-sm-10 col-md-8 col-lg-6 col-xl-6">
            <div class="alert alert-danger" role="alert">
              <h4 class="alert-heading">Sistema Deshabilitado</h4>
              <p>Lo invitamos a regularizar su situación. Una vez abonada la deuda por favor comuníquese con la administración para la pronta habilitación del sistema.</p>
              <hr>
              <p class="mb-0">Administración <span class="font-weight-bold" >ISPBrain</span>.</p>
            </div>
        </div>
    </div>
    
</div>

