

    <?= $this->Html->css([
      '/vendor/Bootstrap/css/bootstrap.min',
      ]) ?>
      

    <?= $this->fetch('css') ?>


<div class="container">

    <div class="row justify-content-center mt-5">
        <div class="col-sm-10 col-md-8 col-lg-6 col-xl-6">
            <div class="alert alert-info" role="alert">
              <h4 class="alert-heading">Sistema Deshabilitado</h4>
              <p>El Sistema estuvo sin conexión durante 5 días. Por favor comunicarse con el Servicio Técnico.</p>
              <hr>
              <p class="mb-0">Servicio Técnico <span class="font-weight-bold" >ISPBrain</span>.</p>
            </div>
        
        </div>
    </div>
    
</div>

