

<style type="text/css">
    
    /*#table-customers_wrapper{*/
    /*    padding: 0 15px 0 15px;*/
    /*}*/
    
    #table-chmts{
        width: 100% !important; 
    }
    
        
    #table-chmts td{
        margin: 0px 0px 0px 0px !important;
        padding: 0px 5px 0px 5px !important;
        vertical-align: middle;
    }
    
    tr.selected{
        background-color: #8eea99;
    }
    
    .modal-actions a.btn{
        width: 100%;
        margin-bottom: 5px;
    }
    
     .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
        background-color: #d2d2d2;
    }
    
     tr{
         font-stretch: condensed;
    } 
    
</style>


    <div id="btns-tools">
        <div class="text-right btns-tools margin-bottom-5">
            
            <a href="javascript:void(0)" class="btn btn-default btn-selected-all" >
                Seleccionar todos
              <span class="far fa-check-square"  aria-hidden="true" ></span>
            </a>
        
            
             <a  class="btn btn-default btn-delete-btn-chmt" title="Eliminar los avisos seleccionados" >
              <span class="glyphicon icon-bin"  aria-hidden="true" ></span>
            </a>
           
            <a  class="btn btn-default btn-export" title="Exportar tabla" >
              <span class="glyphicon icon-file-excel"  aria-hidden="true" ></span>
            </a>
        </div>
    </div>

    
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-hover" id="table-chmts">
                <thead>
                    <tr>
                        <th >Código</th>
                        <th >Nombre</th>
                        <th >Documento</th>
                        <th >Domicilio</th>
                        <th >Área</th>
                        <th >Template</th>
                        <th >Prioridad</th>
                        <th >Fecha Envio</th>
                        <th >IP</th>
                    </tr>
                </thead>
                <tbody>
                  
                </tbody>
            </table>
        </div>
    </div>

</div>

<script type="text/javascript">

    var doc_types = <?php echo json_encode($doc_types); ?>;

    var table_chmts = null;

     $(document).ready(function () {

        $('#table-chmts').removeClass('display');

        $('#btns-tools').hide();

		table_chmts = $('#table-chmts').DataTable({
		    "order": [[ 0, 'desc' ]],
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
		    "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/ConnectionsHasMessageTemplates/index.json",
                "dataSrc": "chmts",
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
            				  window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "columns": [
                { "data": "connection.customer.code" },
                { "data": "connection.customer.name" },
                { 
                    "data": "connection.customer",
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (data.ident) {
                            ident = data.ident;
                        }
                        return doc_types[data.doc_type] + ' ' + ident;
                    }
                },
                { "data": "connection.address" },
                { 
                    "data": "connection.area",
                    "render": function ( data, type, row ) {
                        return data ? data.name : '';
                    }
                },
                { "data": "message_template.name" },
                { "data": "message_template.priority" },
                { 
                    "data": "created",
                    "render": function ( data, type, row ) {
                        var created = data.split('T')[0];
                        created = created.split('-');
                        return created[2] + '/' + created[1] + '/' + created[0];
                    }
                },
                { "data": "connection.ip" },
            ],
            "columnDefs": [
                 { 
                    "width": "100px", targets: [0, 2, 6, 7, 8]
                 },
                 { 
                    "class": "left", targets: [1, 3, 5]
                 },
             ],
             "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
		});
		
		
        $('#table-chmts_filter').closest('div').before($('#btns-tools').contents());

 
        $('#table-chmts tbody').on( 'click', 'tr', function (e) {
            
            if (!$(this).find('.dataTables_empty').length) {
                
                if ( $(this).hasClass('selected') ){
                     $(this).removeClass('selected');
                } else {
                     $(this).addClass('selected');
                }
            }
        });
        
         $('.btn-selected-all').click(function(){
            
           if($(this).find('span').hasClass('active')){
                $('#table-chmts tbody tr').each(function(){
                    if($(this).hasClass('selected')){
                        $(this).removeClass('selected');
                    }
                });
                $('.btn-selected-all span').removeClass('active');
                $('.btn-selected-all span').attr('title', 'Seleccionar todos.')
           }else{
                $('#table-chmts tbody tr').each(function(){
                    if(!$(this).hasClass('selected')){
                        $(this).addClass('selected');
                    }
                });
                $('.btn-selected-all span').addClass('active');
                $('.btn-selected-all span').attr('title', 'Limpiar selección.')
           }
        });
        
      
      $('.btn-delete-btn-chmt').click(function(){
          
   
        var text = '¿Está Seguro que desea eliminar los avisos seleccionados?';

        bootbox.confirm(text, function(result) {
            
            if(result){
                
                var chmts_selected = '';
                var i = 0;
                $('#table-chmts tbody tr').each(function(){
                  
                    if($(this).hasClass('selected')){
                        if(i == 0){
                            i++;
                             chmts_selected += $(this).attr('id');
                        }else{
                             chmts_selected += ',' + $(this).attr('id');
                        }
                    }
                });
                
                $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/ConnectionsHasMessageTemplates/delete/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'lala', 'value': '' }))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'chmts_selected', 'value': chmts_selected}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#replacer').submit();
            }
        });
          
      });

    });

    $(".btn-export").click(function(){
        
        bootbox.confirm('Se descargará un archivo Excel', function(result) {
            if(result){
                $('#table-chmts').tableExport({tableName: 'Aviso Enviados', type:'excel', escape:'false'});
            }
        }).find("div.modal-content").addClass("confirm-width-md");
        
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

    
    
</script>
