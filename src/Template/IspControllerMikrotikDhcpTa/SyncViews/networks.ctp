<?php $this->extend('/IspControllerMikrotikDhcpTa/SyncViews/gateways'); ?>

<?php $this->start('networks'); ?>
 
<style type="text/css">
    
     #table-networksA_wrapper .title{
        color: #696868;
        padding-top: 10px;
    }
    
     #table-networksB_wrapper .title{
        color: #696868;
        padding-top: 10px;
    }
    
</style>


    <div class="row">
        <div class="col-xl-5">
            
            <div class="card border-secondary mb-1 mt-2">
                <div class="card-body p-1">
                    <div class="text-secondary p-0">Controlador Seleccionado: <span class="font-weight-bold"><?=$controller->name?></span></div>
                </div>
            </div>
        </div>
        <div class="col-xl-1">
            <?php
              echo $this->Html->link(
                '<span class="glyphicon icon-loop2" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                    'id' => 'btn-refresh-networks',
                    'title' => 'Click para realizar el proceso control de diferencias',
                    'class' => 'btn btn-default mt-2',
                    'data-toggle' => 'tooltip',
                    'escape' => false
                ]);
             
             ?>
        </div>
        <div class="col-xl-2">
            
             <?php
              echo $this->Html->link(
                'Sincronizar todo',
                'javascript:void(0)',
                [
                    'id' => 'btn-sync-networks',
                    'title' => '',
                    'class' => 'btn btn-primary mt-2',
                ]);
             
             ?>

        </div>
        
        <div class="col-xl-4 text-right">
            <?= $this->Form->input('clear_networks', [
            'label' => 'Limpiar (networks) del controlador',
            'checked' => false,
            'class' => 'm-0 mr-3 mt-3',
            'title' => 'Eliminar los registros agenos a este controlador. Los marcados en azul.',
            'data-toggle' => 'tooltip',
            ])?>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-6">
            <table class="table table-bordered table-hover" id="table-networksA">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Network</th>
                        <th>Gateway</th>
                        <th>DNS Server</th>
                    </tr>
                </thead>
                <tbody>
                  
                </tbody>
            </table>
        </div>
        <div class="col-xl-6">
            <table class="table table-bordered table-hover" id="table-networksB">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Network</th>
                        <th>Gateway</th>
                        <th>DNS Server</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div>
    
    
    
<?php

    $buttonsA = [];
    
    $buttonsA[] = [
        'id'   =>  'btn-edit-network',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];
    
    $buttonsA[] = [
        'id'   =>  'btn-sync-network-indi',
        'name' =>  'Sincronizar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-networksA', 'title' => 'Acciones', 'buttons' => $buttonsA ]);
    
    $buttonsB = [];

    $buttonsB[] = [
        'id'   =>  'btn-delete-network',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-networksB', 'title' => 'Acciones', 'buttons' => $buttonsB ]);
  
?>


    <script type="text/javascript">
        
    
    
        var table_networksA = null;
        var table_networksB = null;
        var networkA_selected = null;

        $(document).ready(function () {
            
            //networksA

            $('#table-networksA').removeClass('display');

    		table_networksA = $('#table-networksA').DataTable({
    		    "order": [[ 0, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ordering" : false,
    		    "paging": false,
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/sync/mikrotik_dhcp_ta/networksA.json",
                    "dataSrc": "data",
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                
                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "api_id",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "comment",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "address",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "gateway",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "dns_server",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    }
                ],
    		    "columnDefs": [
    		      //  { "type": 'ip-address', targets: [2] },
                   
                ],
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-networksA_wrapper .title').append('<h5>Sistema</h5>');
    		
    		$('#table-networksA tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {
                    
                    table_networksA.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    networkA_selected = table_networksA.row( this ).data();
                    $('.modal-networksA').modal('show');
                }
            });
            
            $('#btn-edit-network').click(function() {
                var action = '/ispbrain/IspControllerMikrotikDhcpTa/config/' + networkA_selected.controller_id;
                window.open(action, '_black');
            });
          
            $('#btn-sync-networks').click(function() {
                
                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
                
                if(table_networksA.rows('tr').count() == 0 && table_networksB.rows('tr').count() == 0){
            
                    generateNoty('warning', 'No existen elementos para sincronizar.');
                    
                }else{
                    
                    var networks_ids = [];
                
                    $.each(table_networksA.rows().data(), function(i, network){
                        networks_ids.push(network.id);
                    });
              
                    var text = '¿Está seguro que desea sincronizar los networks?';
        
                    bootbox.confirm(text, function(result) {
                        if (result) {
                        
                            openModalPreloader("Sincronizando ...");
                            
                             $.ajax({
                                url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikDhcpTa', 'action' => 'syncnetworks'])?>" ,
                                type: 'POST',
                                dataType: "json",
                                data: JSON.stringify({controller_id: controller_id, networks_ids: networks_ids, delete_networks_side_b: $('#clear-networks').is(':checked')}),
                                success: function(data){
                                    
                                    console.log(data);
                                    
                                    if(data.response){
                                        generateNoty('success', 'Sincronización de (networks) completa');
                                    }else{
                                        generateNoty('error', 'Error en la Sincronización');
                                    }
                                    
                                    closeModalPreloader();
                                   
                                    table_networksA.ajax.reload();
                                    table_networksB.ajax.reload();
                                    
                                    updateCountersTitle();
                                   
                                },
                                error: function (jqXHR ) {
                                                        
                                    if(jqXHR.status == 403){
                                        
                                        generateNoty('warning', 'La sesión ha expirado. Por favor ingrese sus credenciales.');
                                        
                                        setTimeout(function(){ 
                                          window.location.href ="/ispbrain";
                                        }, 3000);
                                        
                                    }else{
                                        generateNoty('error', 'Error al intentar sincronizar los (networks).');
                                        closeModalPreloader();
                                        
                                        updateCountersTitle();
                                    }
                                    
                                    
                                }
                                
                            });
                        }
                    });
                    
                    
                }
                
                
                
            });
            
            $('#btn-sync-network-indi').click(function() {
                
                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }

                var networks_ids = [];
                
                networks_ids.push(networkA_selected.id);
          
                var text = '¿Está seguro que desea sincronizar este (network)?';
    
                bootbox.confirm(text, function(result) {
                    if (result) {
                         
                         $('.modal-networksA').modal('hide');
                    
                        openModalPreloader("Sincronizando ...");
                        
                         $.ajax({
                            url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikDhcpTa', 'action' => 'syncnetworks'])?>" ,
                            type: 'POST',
                            dataType: "json",
                            data: JSON.stringify({controller_id: controller_id, networks_ids: networks_ids, delete_networks_side_b: $('#clear-networks').is(':checked')}),
                            success: function(data){
                                
                                console.log(data);
                                
                                if(data.response){
                                    generateNoty('success', 'Sincronización de (networks) completa');
                                }else{
                                    generateNoty('error', 'Error en la Sincronización');
                                }
                                
                                closeModalPreloader();
                               
                                table_networksA.ajax.reload();
                                table_networksB.ajax.reload();
                                
                                updateCountersTitle();
                            
                               
                            },
                            error: function (jqXHR ) {
                                                    
                                if(jqXHR.status == 403){
                                    
                                    generateNoty('warning', 'La sesión ha expirado. Por favor ingrese sus credenciales.');
                                    
                                    setTimeout(function(){ 
                                      window.location.href ="/ispbrain";
                                    }, 3000);
                                    
                                }else{
                                    generateNoty('error', 'Error al intentar sincronizar los (networks).');
                                    closeModalPreloader();
                                }
                                
                                updateCountersTitle();
                             
                            }
                            
                        });
                    }
                });
                
            });
            
            $('#btn-refresh-networks').click(function() {
                
                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
            
                openModalPreloader("Actualizando ...");
                
                 $.ajax({
                    url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikDhcpTa', 'action' => 'refreshNetworks'])?>" ,
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify({controller_id: controller_id}),
                    success: function(data){
                        
                        closeModalPreloader();
    
                        if (!data.data) {
                             generateNoty('error', 'No se pudo establecer una conexión con el controlador.');
                        }else{
                            table_networksA.ajax.reload();
                            table_networksB.ajax.reload();
                        }
                        
                        updateCountersTitle();
                    },
                    error: function (jqXHR ) {
                                                    
                        if(jqXHR.status == 403){
                            
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese sus credenciales.');
                            
                            setTimeout(function(){ 
                              window.location.href ="/ispbrain";
                            }, 3000);
                            
                        }else{
                            generateNoty('error', 'Error al intentar sincronizar actualizar.');
                            closeModalPreloader();
                        }
                    }
                });
                
            });
            

            //end networksA

            //networksB

            $('#table-networksB').removeClass('display');

    		table_networksB = $('#table-networksB').DataTable({
    		    "order": [[ 0, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ordering" : false,
    		    "paging": false,
    		    
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/sync/mikrotik_dhcp_ta/networksB.json",
                    "dataSrc": "data",
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                
                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "api_id",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "comment",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "address",
                        "render": function ( data, type, row ) {
                             if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "gateway",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "dns_server",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    }
                ],
    		    "columnDefs": [
    		        { "type": 'ip-address', targets: [3,4] },
                ],
                
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-networksB_wrapper .title').append('<h5>Controlador</h5>');
    		
    		$('#table-networksB tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {
                    
                    table_networksB.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    networkB_selected = table_networksB.row( this ).data();
                    $('.modal-networksB').modal('show');
                }
            });
            
            $('#btn-delete-network').click(function() {
                
                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
                
                openModalPreloader("Eliminado (network) del Controlador ...");
                
                $('.modal-networksB').modal('hide');
                
                 $.ajax({
                    url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikDhcpTa', 'action' => 'deleteNetworksInController'])?>" ,
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify({controller_id: controller_id, network_api_id: networkB_selected.api_id}),
                    success: function(data){
                        
                        closeModalPreloader();
    
                        if (!data.data) {
                             generateNoty('error', 'Error al intentar eliminar.');
                        }else{
                            table_networksA.ajax.reload();
                            table_networksB.ajax.reload();
                        }
                        
                        updateCountersTitle();
                    },
                    error: function (jqXHR ) {
                                            
                        if(jqXHR.status == 403){
                            
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese sus credenciales.');
                            
                            setTimeout(function(){ 
                              window.location.href ="/ispbrain";
                            }, 3000);
                            
                        }else{
                            generateNoty('error', 'Error al intentar eliminar.');
                            closeModalPreloader();
                            updateCountersTitle();
                        }
                        
                        
                    }
                });
            });
    		
    		 //end networksB

    		$('a[id="networks-tab"]').on('shown.bs.tab', function (e) {
    		    
                table_networksA.draw();
                table_networksB.draw();
               
            });
            
       
           
        });
    </script>
<?php $this->end(); ?>
