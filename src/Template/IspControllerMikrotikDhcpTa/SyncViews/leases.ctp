<?php $this->extend('/IspControllerMikrotikDhcpTa/SyncViews/address_lists'); ?>

<?php $this->start('leases'); ?>


<style type="text/css">
    
     #table-leasesA_wrapper .title{
        color: #696868;
        padding-top: 10px;
    }
    
     #table-leasesB_wrapper .title{
        color: #696868;
        padding-top: 10px;
    }
    
    /*.other{*/
    /*    color: blue;*/
    /*}*/
    
</style>


    <div class="row">
        <div class="col-xl-5">
            
            <div class="card border-secondary mb-1 mt-2">
                <div class="card-body p-1">
                    <div class="text-secondary p-0">Controlador Seleccionado: <span class="font-weight-bold"><?=$controller->name?></span></div>
                </div>
            </div>
        </div>
        <div class="col-xl-1">
            <?php
              echo $this->Html->link(
                '<span class="glyphicon icon-loop2" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                    'id' => 'btn-refresh-leases',
                    'title' => 'Click para realizar el proceso control de diferencias',
                    'class' => 'btn btn-default mt-2',
                    'data-toggle' => 'tooltip',
                    'escape' => false
                ]);
             
             ?>
        </div>
        <div class="col-xl-3 ">
            
             <?php
              echo $this->Html->link(
                'Sincronizar todo',
                'javascript:void(0)',
                [
                    'id' => 'btn-sync-leases',
                    'title' => '',
                    'class' => 'btn btn-primary mt-2',
                ]);
             
             ?>

        </div>
        
        <div class="col-xl-3 text-right">
            <?= $this->Form->input('clear_leases', [
            'label' => 'Limpiar (leases) del controlador',
            'checked' => false,
            'class' => 'm-0 mr-3 mt-3',
            'title' => 'Eliminar los registros agenos a este controlador. Los marcados en azul.',
            'data-toggle' => 'tooltip',
            ])?>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-6">
            <table class="table table-bordered table-hover" id="table-leasesA">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Address</th>
                        <th>MAC address</th>
                        <th>Server</th>
                        <th>Comment</th>
                        <th>Address lists</th>
                        <th>Lease time</th>
                    </tr>
                </thead>
                <tbody>
                  
                </tbody>
            </table>
        </div>
        <div class="col-xl-6">
            <table class="table table-bordered table-hover" id="table-leasesB">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Address</th>
                        <th>MAC address</th>
                        <th>Server</th>
                        <th>Comment</th>
                        <th>Address lists</th>
                        <th>Lease time</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div>
    
    
    
<?php

    $buttonsA = [];

    $buttonsA[] = [
        'id'   =>  'btn-edit-lease',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];
    
    $buttonsA[] = [
        'id'   =>  'btn-sync-lease-indi',
        'name' =>  'Sincronizar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-leasesA', 'title' => 'Acciones', 'buttons' => $buttonsA ]);
    
    $buttonsB = [];

    $buttonsB[] = [
        'id'   =>  'btn-delete-lease',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-leasesB', 'title' => 'Acciones', 'buttons' => $buttonsB ]);
  
?>


    <script type="text/javascript">

        var table_leasesA = null;
        var table_leasesB = null;
        var leaseA_selected = null;
        var leaseB_selected = null;

        $(document).ready(function () {
            //leasesA

            $('#table-leasesA').removeClass('display');

    		table_leasesA = $('#table-leasesA').DataTable({
    		    "order": [[ 0, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ordering" : false,
    		    "paging": false,
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/sync/mikrotik_dhcp_ta/leasesA.json",
                    "dataSrc": "data",
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                
                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "api_id",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "address",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "mac_address",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "server",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "comment",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "address_lists",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "lease_time",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    }
                ],
    		    "columnDefs": [
    		        { "type": 'ip-address', targets: [1] },
                   
                ],
                "createdRow" : function( row, data, index ) {
                    row.id = data.connection_id;
                },
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-leasesA_wrapper .title').append('<h5>Sistema</h5>');
    		
    		$('#table-leasesA tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {
                    
                    table_leasesA.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    leaseA_selected = table_leasesA.row( this ).data();
                    
                    $('.modal-leasesA').modal('show');
                }
            });
            
            $('#btn-edit-lease').click(function() {
                var action = '/ispbrain/connections/edit/' + leaseA_selected.connection_id;
                window.open(action, '_black');
            });
            
            $('#btn-sync-leases').click(function() {
                
                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
                
                if(table_leasesA.rows('tr').count() == 0 && table_leasesB.rows('tr').count() == 0){
            
                    generateNoty('warning', 'No existen elementos para sincronizar.');
                    
                }else{
                    
                    var connections_ids = [];
                    
                    $.each(table_leasesA.$('tr'), function(i, lease){
                        connections_ids.push(lease.id);
                    });
                    
                    // console.log(connections_ids);
              
                    var text = '¿Está seguro que desea sincronizar los (leases)?';
        
                    bootbox.confirm(text, function(result) {
                        if (result) {
                        
                            openModalPreloader("Sincronizando ...");
                            
                             $.ajax({
                                url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikDhcpTa', 'action' => 'sync-leases'])?>" ,
                                type: 'POST',
                                dataType: "json",
                                data: JSON.stringify({controller_id: controller_id, connections_ids: connections_ids, delete_leases_side_b: $('#clear-leases').is(':checked')}),
                                success: function(data){
                                    
                                    console.log(data);
                                    
                                    if(data.data){
                                        generateNoty('success', 'Sincronización de (leases) completa');
                                    }else{
                                        generateNoty('error', 'Error en la Sincronización');
                                    }
                                    
                                    closeModalPreloader();
                                   
                                    updateCountersTitle();
                                   
                                    table_leasesA.ajax.reload();
                                    table_leasesB.ajax.reload();
                                    
                                    updateCountersTitle();
                              
                                   
                                },
                                error: function (jqXHR ) {
                                                            
                                    if(jqXHR.status == 403){
                                        
                                        generateNoty('warning', 'La sesión ha expirado. Por favor ingrese sus credenciales.');
                                        
                                        setTimeout(function(){ 
                                          window.location.href ="/ispbrain";
                                        }, 3000);
                                        
                                    }else{
                                        generateNoty('error', 'Error al intentar sincronizar los (leases).');
                                        closeModalPreloader();
                                        updateCountersTitle();
                                    }
                                }
                                
                            });
                        }
                    });
                }
                
            });
            
            $('#btn-sync-lease-indi').click(function() {
                
                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }               
              
                var connections_ids = [];
                connections_ids.push(leaseA_selected.connection_id);
                
                var text = '¿Está seguro que desea sincronizar este (leases)?';
    
                bootbox.confirm(text, function(result) {
                    if (result) {
                        
                        $('.modal-leasesA').modal('hide');
                    
                        openModalPreloader("Sincronizando ...");
                        
                         $.ajax({
                            url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikDhcpTa', 'action' => 'sync-leases'])?>" ,
                            type: 'POST',
                            dataType: "json",
                            data: JSON.stringify({controller_id: controller_id, connections_ids: connections_ids, delete_leases_side_b: $('#clear-leases').is(':checked')}),
                            success: function(data){
                                
                                console.log(data);
                                
                                if(data.data){
                                    generateNoty('success', 'Sincronización de (leases) completa');
                                }else{
                                    generateNoty('error', 'Error en la Sincronización');
                                }
                                
                                closeModalPreloader();
                               
                                table_leasesA.ajax.reload();
                                table_leasesB.ajax.reload();
                                
                                updateCountersTitle();
                                
                               
                            },
                            error: function (jqXHR ) {
                                                        
                                if(jqXHR.status == 403){
                                    
                                    generateNoty('warning', 'La sesión ha expirado. Por favor ingrese sus credenciales.');
                                    
                                    setTimeout(function(){ 
                                      window.location.href ="/ispbrain";
                                    }, 3000);
                                    
                                }else{
                                     generateNoty('error', 'Error al intentar sincronizar los (leases).');
                                    closeModalPreloader();
                                    updateCountersTitle();
                                }
                            }
                            
                        });
                    }
                });
            });
            
            $('#btn-refresh-leases').click(function() {
                
                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
            
                openModalPreloader("Actualizando ...");
                
                 $.ajax({
                    url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikDhcpTa', 'action' => 'refreshLeases'])?>" ,
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify({controller_id: controller_id}),
                    success: function(data){
                        
                        closeModalPreloader();
    
                        if (!data.data) {
                             generateNoty('error', 'No se pudo establecer una conexión con el controlador.');
                        }else{
                            table_leasesA.ajax.reload();
                            table_leasesB.ajax.reload();
                        }
                        
                        updateCountersTitle();
                    },
                    error: function (jqXHR ) {
                                                    
                        if(jqXHR.status == 403){
                            
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese sus credenciales.');
                            
                            setTimeout(function(){ 
                              window.location.href ="/ispbrain";
                            }, 3000);
                            
                        }else{
                            generateNoty('error', 'Error al intentar actualizar.');
                            closeModalPreloader();
                        }
                    }
                });
                
            });
            

            //end leasesA

            //leasesB

            $('#table-leasesB').removeClass('display');

    		table_leasesB = $('#table-leasesB').DataTable({
    		    "order": [[ 0, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ordering" : false,
    		    "paging": false,
    		    
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/sync/mikrotik_dhcp_ta/leasesB.json",
                    "dataSrc": "data",
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                
                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "api_id",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "address",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "mac_address",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "server",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "comment",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "address_lists",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "lease_time",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    }
                ],
    		    "columnDefs": [
    		        { "type": 'ip-address', targets: [1] },
                ],
                "createdRow" : function( row, data, index ) {
                    
                    row.id = data.connection_id;
                    
                    console.log(data);
                    
                    // if(data.other){
                    //      $(row).addClass('other');
                    // }
                },
                
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-leasesB_wrapper .title').append('<h5>Controlador</h5>');
    		
    		$('#table-leasesB tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {
                    
                    table_leasesB.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    leaseB_selected = table_leasesB.row( this ).data();
                    $('.modal-leasesB').modal('show');
                }
            });
            
            $('#btn-delete-lease').click(function() {
                
                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
                
                openModalPreloader("Eliminado (lease) del Controlador ...");
                
                $('.modal-leasesB').modal('hide');
                
                 $.ajax({
                    url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikDhcpTa', 'action' => 'deleteLeaseInController'])?>" ,
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify({controller_id: controller_id, lease_api_id: leaseB_selected.api_id}),
                    success: function(data){
                        
                        closeModalPreloader();
    
                        if (!data.data) {
                             generateNoty('error', 'Error al intentar eliminar.');
                        }else{
                            generateNoty('success', '(lease) eliminado.');
                        }
                        
                        table_leasesA.ajax.reload();
                        table_leasesB.ajax.reload();
                        
                        updateCountersTitle();
                      
                        
                    },
                    error: function (jqXHR ) {
                                                    
                        if(jqXHR.status == 403){
                            
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                            
                            setTimeout(function(){ 
                              window.location.href ="/ispbrain";
                            }, 3000);
                            
                        }else{
                            generateNoty('error', 'Error al intentar eliminar.');
                            closeModalPreloader();
                            updateCountersTitle();
                          
                        }
                    }
                    
                });
            });
            
    		 //end leasesB

    		$('a[id="leases-tab"]').on('shown.bs.tab', function (e) {
                table_leasesA.draw();
                table_leasesB.draw();
            });

           
        });
    </script>
 
<?php $this->end(); ?>
