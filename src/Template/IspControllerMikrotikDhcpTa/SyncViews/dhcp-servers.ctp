<?php $this->extend('/IspControllerMikrotikDhcpTa/SyncViews/pools'); ?>

<?php $this->start('dhcp-servers'); ?>

<style type="text/css">
    
    #table-dhcpserversA_wrapper .title{
        color: #696868;
        padding-top: 10px;
    }
    
    #table-dhcpserversB_wrapper .title{
        color: #696868;
        padding-top: 10px;
    }
    
</style>


    <div class="row">
        <div class="col-xl-5">
            
            <div class="card border-secondary mb-1 mt-2">
                <div class="card-body p-1">
                    <div class="text-secondary p-0">Controlador Seleccionado: <span class="font-weight-bold"><?=$controller->name?></span></div>
                </div>
            </div>
        </div>
        <div class="col-xl-1">
           
            <?php
              echo $this->Html->link(
                '<span class="glyphicon icon-loop2" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                    'id' => 'btn-refresh-dhcpservers',
                    'title' => 'Click para realizar el proceso control de diferencias',
                    'class' => 'btn btn-default mt-2',
                    'data-toggle' => 'tooltip',
                    'escape' => false
                ]);
             ?>
          
        </div>
        <div class="col-xl-2">
            
             <?php
              echo $this->Html->link(
                'Sincronizar todo',
                'javascript:void(0)',
                [
                    'id' => 'btn-sync-dhcpservers',
                    'title' => '',
                    'class' => 'btn btn-primary mt-2',
                ]);
             
             ?>

        </div>
        
        <div class="col-xl-4 text-right">
            <?= $this->Form->input('clear_dhcpservers', [
            'label' => 'Limpiar (dhcp-server) del controlador',
            'checked' => false,
            'class' => 'm-0 mr-3 mt-3',
            'title' => 'Eliminar los registros agenos a este controlador. Los marcados en azul.',
            'data-toggle' => 'tooltip',
            ])?>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-6">
            <table class="table table-bordered table-hover" id="table-dhcpserversA">
                <thead>
                    <tr>
                       <th>Id</th>
                       <th>Service Name</th>
                       <th>Interface</th>
                       <th>Address Pool</th>
                       <th>Leases Time</th>
                       <th>disabled</th>
                    </tr>
                </thead>
                <tbody>
                  
                </tbody>
            </table>
        </div>
        <div class="col-xl-6">
            <table class="table table-bordered table-hover" id="table-dhcpserversB">
                <thead>
                    <tr>
                       <th>Id</th>
                       <th>Service Name</th>
                       <th>Interface</th>
                       <th>Address Pool</th>
                       <th>Leases Time</th>
                       <th>disabled</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div>
    
    
    
<?php

    $buttonsA = [];

    $buttonsA[] = [
        'id'   =>  'btn-edit-dhcpserver',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];
    
    $buttonsA[] = [
        'id'   =>  'btn-sync-dhcpserver-indi',
        'name' =>  'Sincronizar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-dhcpserverA', 'title' => 'Acciones', 'buttons' => $buttonsA ]);
    
    
    $buttonsB = [];

    $buttonsB[] = [
        'id'   =>  'btn-delete-dhcpserver',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-dhcpserverB', 'title' => 'Acciones', 'buttons' => $buttonsB ]);
  
?>


    <script type="text/javascript">


        var table_dhcpserversA = null;
        var table_dhcpserversB = null;
        var dhcpserverA_selected = null;

        $(document).ready(function () {
            
            
            //dhcpserversA

            $('#table-dhcpserversA').removeClass('display');

    		table_dhcpserversA = $('#table-dhcpserversA').DataTable({
    		    "order": [[ 1, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ordering" : false,
    		    "paging": false,
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/sync/mikrotik_dhcp_ta/dhcpserversA.json",
                    "dataSrc": "data",
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                
                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                                },
                "columns": [
                    { 
                        "data": "api_id",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "name",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "interface",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "address_pool",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "leases_time",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "disabled",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    }
                ],
    		    "columnDefs": [
    		       
                ],
                "createdRow" : function( row, data, index ) {
                    row.id = data.id;
                },
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-dhcpserversA_wrapper .title').append('<h5>Sistema</h5>');
    		
    		$('#table-dhcpserversA tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {
                    
                    table_dhcpserversA.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    dhcpserverA_selected = table_dhcpserversA.row( this ).data();
                    $('.modal-dhcpserverA').modal('show');
                }
            });
            
            $('#btn-edit-dhcpserver').click(function() {
                var action = '/ispbrain/IspControllerMikrotikDhcpTa/config/' + dhcpserverA_selected.controller_id;
                window.open(action, '_black');
            });
            
            $('#btn-sync-dhcpservers').click(function() {
                
                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
                
                
                if(table_dhcpserversA.rows('tr').count() == 0 && table_dhcpserversB.rows('tr').count() == 0){
            
                    generateNoty('warning', 'No existen elementos para sincronizar.');
                    
                }else{
                    
                    var ids = [];
                
                    $.each(table_dhcpserversA.$('tr'), function(i, dhcpserver){
                        ids.push(dhcpserver.id);
                    });
              
                    var text = '¿Está seguro que desea sincronizar los (dhcp-servers)?';
        
                    bootbox.confirm(text, function(result) {
                        if (result) {
                        
                            openModalPreloader('Sincronizando ... ');
                            
                             $.ajax({
                                url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikDhcpTa', 'action' => 'syncdhcpservers'])?>" ,
                                type: 'POST',
                                dataType: "json",
                                data: JSON.stringify({controller_id: controller_id, ids: ids, delete_dhcpserver_side_b: $('#clear-dhcpservers').is(':checked')}),
                                success: function(data){
                                    
                                    console.log(data);
                                    
                                    closeModalPreloader();
                
                                    if (!data.data) {
                                         generateNoty('error', 'Error al intentar sincronizar los (dhcp servers).');
                                    }else{
                                        table_dhcpserversA.ajax.reload();
                                        table_dhcpserversB.ajax.reload();
                                        
                                        generateNoty('success', 'sincronización de los (dhcp servers) completo.');
                                    }
                                    
                                    updateCountersTitle();
                                },
                                error: function (jqXHR) {
                                    
                                    if(jqXHR.status == 403){
                                        
                                        generateNoty('warning', 'La sesión ha expirado. Por favor ingrese sus credenciales.');
                                        
                                        setTimeout(function(){ 
                                          window.location.href ="/ispbrain";
                                        }, 3000);
                                        
                                    }else{
                                        generateNoty('error', 'Error al intentar sincronizar los (dhcp servers).');
                                        closeModalPreloader();
                                        
                                        updateCountersTitle();
                                    }
                                }
                                
                            });
                        }
                    });
                    
                }
                
                
                
                
            });
            
            $('#btn-sync-dhcpserver-indi').click(function() {
                
                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
       
                var ids = [];
                ids.push(dhcpserverA_selected.id);
             
                var text = '¿Está seguro que desea sincronizar este (dhcp-server)?';
    
                bootbox.confirm(text, function(result) {
                    if (result) {
                        
                        $('.modal-dhcpserverA').modal('hide');
                    
                        openModalPreloader('Sincronizando ... ');
                        
                         $.ajax({
                            url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikDhcpTa', 'action' => 'syncdhcpservers'])?>" ,
                            type: 'POST',
                            dataType: "json",
                            data: JSON.stringify({controller_id: controller_id, ids: ids, delete_dhcpserver_side_b: $('#clear-dhcpservers').is(':checked')}),
                            success: function(data){
                                
                                console.log(data);
                                
                                closeModalPreloader();
            
                                if (!data.data) {
                                     generateNoty('error', 'Error al intentar sincronizar los (dhcp servers).');
                                }else{
                                    table_dhcpserversA.ajax.reload();
                                    table_dhcpserversB.ajax.reload();
                                    
                                    generateNoty('success', 'sincronización de los (dhcp servers) completo.');
                                }
                                
                                updateCountersTitle();
                            },
                            error: function (jqXHR) {
                                
                                if(jqXHR.status == 403){
                                    
                                    generateNoty('warning', 'La sesión ha expirado. Por favor ingrese sus credenciales.');
                                    
                                    setTimeout(function(){ 
                                      window.location.href ="/ispbrain";
                                    }, 3000);
                                    
                                }else{
                                    generateNoty('error', 'Error al intentar sincronizar los (dhcp servers).');
                                    closeModalPreloader();
                                    
                                    updateCountersTitle();
                                }
                            }
                            
                        });
                    }
                });
            });
            
            $('#btn-refresh-dhcpservers').click(function() {
                
                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
               
                openModalPreloader('Actualizando ... ');
                
                 $.ajax({
                    url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikDhcpTa', 'action' => 'refreshDHCPServers'])?>" ,
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify({controller_id: controller_id}),
                    success: function(data){
                        
                        console.log(data);
                        
                         closeModalPreloader();
    
                        if (!data.data) {
                             generateNoty('error', 'No se pudo establecer una conexión con el controlador.');
                        }else{
                            table_dhcpserversA.ajax.reload();
                            table_dhcpserversB.ajax.reload();
                        }
                        
                        updateCountersTitle();
                    },
                    error: function (jqXHR ) {
                        
                        if(jqXHR.status == 403){
                            
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese sus credenciales.');
                            
                            setTimeout(function(){ 
                              window.location.href ="/ispbrain";
                            }, 3000);
                            
                        }else{
                            generateNoty('error', 'Error al intentar actualizar.');
                            closeModalPreloader();
                        }
                    }
                });
                
            });

            //end dhcpserversA

            //dhcpserversB

            $('#table-dhcpserversB').removeClass('display');

    		table_dhcpserversB = $('#table-dhcpserversB').DataTable({
    		    "order": [[ 1, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ordering" : false,
    		    "paging": false,
    		    
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/sync/mikrotik_dhcp_ta/dhcpserversB.json",
                    "dataSrc": "data",
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                
                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "api_id",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "name",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "interface",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "address_pool",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                     { 
                        "data": "leases_time",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "disabled",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    }
                    
                ],
    		    "columnDefs": [
    		        
                ],
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-dhcpserversB_wrapper .title').append('<h5>Controlador</h5>');
    		
    		$('#table-dhcpserversB tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {
                    
                    table_dhcpserversB.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    dhcpserverB_selected = table_dhcpserversB.row( this ).data();
                    $('.modal-dhcpserverB').modal('show');
                }
            });
            
            $('#btn-delete-dhcpserver').click(function() {
                
                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
                
                $('.modal-dhcpserverB').modal('hide');
               
                openModalPreloader('Eliminado DHCPServer del Controlador ... ');
                
                 $.ajax({
                    url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikDhcpTa', 'action' => 'deleteDHCPServerInController'])?>" ,
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify({controller_id: controller_id, dhcpserver_api_id: dhcpserverB_selected.api_id}),
                    success: function(data){
                        
                        closeModalPreloader();
    
                        if (!data.data) {
                             generateNoty('error', 'Error al intentar eliminar.');
                        }else{
                            table_dhcpserversA.ajax.reload();
                            table_dhcpserversB.ajax.reload();
                        }
                        
                         updateCountersTitle();
                    },
                    error: function (jqXHR) {
                        
                        if(jqXHR.status == 403){
                            
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese sus credenciales.');
                            
                            setTimeout(function(){ 
                              window.location.href ="/ispbrain";
                            }, 3000);
                            
                        }else{
                            generateNoty('error', 'Error al intentar eliminar.');
                            closeModalPreloader();
                             updateCountersTitle();
                        }
                    }
                });
            });
    		
    		 //end dhcpserversB

    		$('a[id="dhcp-servers-tab"]').on('shown.bs.tab', function (e) {
    		    
                table_dhcpserversA.ajax.reload();
                table_dhcpserversB.ajax.reload();
           
            });
            
           
           
        });
        
  
        
        
    </script>
<?php $this->end(); ?>
