
<div class="row">

    <div class="col-md-12" id="myTabs">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" id="myTab" role="tablist">

            <li class="nav-item">
                <a class="nav-link active" id="queues-tab" data-target="#queues" role="tab" aria-controls="queues" aria-expanded="true">Queues</a>
            </li>
            
            <li class="nav-item">
                <a class="nav-link " id="leases-tab" data-target="#leases" role="tab" aria-controls="leases" aria-expanded="false">Leases</a>
            </li>

            <li class="nav-item">
                <a class="nav-link " id="address-lists-tab" data-target="#address-lists" role="tab" aria-controls="address-lists" aria-expanded="false">Address Lists</a>
            </li>
            
            <li class="nav-item">
                <a class="nav-link " id="networks-tab" data-target="#networks" role="tab" aria-controls="networks" aria-expanded="false">Networks</a>
            </li>
            
            <li class="nav-item">
                <a class="nav-link " id="gateways-tab" data-target="#gateways" role="tab" aria-controls="gateways" aria-expanded="false">Gateways</a>
            </li>
            
            
            
            <li class="nav-item">
                <a class="nav-link " id="dhcp-servers-tab" data-target="#dhcp-servers" role="tab" aria-controls="dhcp-servers" aria-expanded="false">DHCP Servers</a>
            </li>
            
            <li class="nav-item">
                <a class="nav-link " id="pools-tab" data-target="#pools" role="tab" aria-controls="pools" aria-expanded="false">Pools</a>
            </li>
         
        </ul>

        <!-- Tab panes -->
        <div class="tab-content" id="myTabContent">

            <div class="tab-pane fade show active" id="queues" role="tabpanel" aria-labelledby="queues-tab">
                <?= $this->fetch('queues') ?>
            </div>

            <div class="tab-pane fade" id="leases" role="tabpanel" aria-labelledby="leases-tab">
                <?= $this->fetch('leases') ?>
            </div>
            
            <div class="tab-pane fade" id="address-lists" role="tabpanel" aria-labelledby="address-lists-tab">
                <?= $this->fetch('address-lists') ?>
            </div>
            
            <div class="tab-pane fade" id="networks" role="tabpanel" aria-labelledby="networks-tab">
                <?= $this->fetch('networks') ?>
            </div>
            
            <div class="tab-pane fade" id="gateways" role="tabpanel" aria-labelledby="gateways-tab">
                <?= $this->fetch('gateways') ?>
            </div>
            
           
            <div class="tab-pane fade" id="dhcp-servers" role="tabpanel" aria-labelledby="dhcp-servers-tab">
                <?= $this->fetch('dhcp-servers') ?>
            </div>
            
             <div class="tab-pane fade" id="pools" role="tabpanel" aria-labelledby="pools-tab">
                <?= $this->fetch('pools') ?>
            </div>
            

        </div>
    </div>

</div>

<?php
echo $this->element('modal_preloader');
?>
<script type="text/javascript">

    var controller_id = <?php echo json_encode($controller->id); ?>;
    var integrationEnabled = <?php echo json_encode($controller->integration); ?>;

    $(document).ready(function () {

        $('#myTab a').click(function (e) {

            e.preventDefault();

            var target = $(this).data('target');
            $('#myTab a.active').removeClass('active');

            $('#myTabContent .active').fadeOut(100, function() {
            $('#myTabContent .active').removeClass('active');

                $(target).fadeIn(100, function() {
                    $(target).addClass('active show');
                    $(target+'-tab').addClass('active');
                    $(target+'-tab').trigger('shown.bs.tab');
                });
            });
        });
        
         $(function () {
          $('[data-toggle="tooltip"]').tooltip()
        });
        
         $(document).click(function() {
             $('[data-toggle="tooltip"]').tooltip('hide');
        });


    });

</script>
