<?php $this->extend('/IspControllerMikrotikDhcpTa/SyncViews/tabs'); ?>

<?php $this->start('pools'); ?>
 
<style type="text/css">
    
     #table-poolsA_wrapper .title{
        color: #696868;
        padding-top: 10px;
    }
    
     #table-poolsB_wrapper .title{
        color: #696868;
        padding-top: 10px;
    }
    
</style>


    <div class="row">
        <div class="col-xl-5">
            
            <div class="card border-secondary mb-1 mt-2">
                <div class="card-body p-1">
                    <div class="text-secondary p-0">Controlador Seleccionado: <span class="font-weight-bold"><?=$controller->name?></span></div>
                </div>
            </div>
        </div>
        <div class="col-xl-1">
            <?php
              echo $this->Html->link(
                '<span class="glyphicon icon-loop2" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                    'id' => 'btn-refresh-pools',
                    'title' => 'Click para realizar el proceso control de diferencias',
                    'class' => 'btn btn-default mt-2',
                    'data-toggle' => 'tooltip',
                    'escape' => false
                ]);
             
             ?>
        </div>
        <div class="col-xl-2">
            
             <?php
              echo $this->Html->link(
                'Sincronizar todo',
                'javascript:void(0)',
                [
                    'id' => 'btn-sync-pools',
                    'title' => '',
                    'class' => 'btn btn-primary mt-2',
                ]);
             
             ?>

        </div>
        
        <div class="col-xl-4 text-right">
            <?= $this->Form->input('clear_pools', [
            'label' => 'Limpiar (pool) del controlador',
            'checked' => false,
            'class' => 'm-0 mr-3 mt-3',
            'title' => 'Eliminar los registros agenos a este controlador. Los marcados en azul.',
            'data-toggle' => 'tooltip',
            ])?>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-6">
            <table class="table table-bordered table-hover" id="table-poolsA">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Ranges</th>
                    </tr>
                </thead>
                <tbody>
                  
                </tbody>
            </table>
        </div>
        <div class="col-xl-6">
            <table class="table table-bordered table-hover" id="table-poolsB">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Ranges</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div>
    
    
    
<?php


    $buttonsA = [];
    
    $buttonsA[] = [
        'id'   =>  'btn-edit-pool',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];
    
    $buttonsA[] = [
        'id'   =>  'btn-sync-pool-indi',
        'name' =>  'Sincronizar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-poolsA', 'title' => 'Acciones', 'buttons' => $buttonsA ]);
    
    $buttonsB = [];

    $buttonsB[] = [
        'id'   =>  'btn-delete-pool',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-poolsB', 'title' => 'Acciones', 'buttons' => $buttonsB ]);
  
?>


    <script type="text/javascript">
        
    
    
        var table_poolsA = null;
        var table_poolsB = null;
        var poolA_selected = null;

        $(document).ready(function () {
            
            //poolsA

            $('#table-poolsA').removeClass('display');

    		table_poolsA = $('#table-poolsA').DataTable({
    		    "order": [[ 0, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ordering" : false,
    		    "paging": false,
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/sync/mikrotik_dhcp_ta/poolsA.json",
                    "dataSrc": "data",
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                
                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "api_id",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "name",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "ranges",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    }
                ],
    		    "columnDefs": [
                   
                ],
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-poolsA_wrapper .title').append('<h5>Sistema</h5>');
    		
    		$('#table-poolsA tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {
                    
                    table_poolsA.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    poolA_selected = table_poolsA.row( this ).data();
                    $('.modal-poolsA').modal('show');
                }
            });
            
            $('#btn-edit-pool').click(function() {
                var action = '/ispbrain/IspControllerMikrotikDhcpTa/config/' + poolA_selected.controller_id;
                window.open(action, '_black');
            });
          
            $('#btn-sync-pools').click(function() {
                
                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
                
                if(table_poolsA.rows('tr').count() == 0 && table_poolsB.rows('tr').count() == 0){
            
                    generateNoty('warning', 'No existen elementos para sincronizar.');
                    
                }else{
                    
                    var pools_ids = [];
                
                    $.each(table_poolsA.rows().data(), function(i, pool){
                        pools_ids.push(pool.id);
                    });
                  
                    var text = '¿Está seguro que desea sincronizar los pools?';
        
                    bootbox.confirm(text, function(result) {
                        if (result) {
                        
                            openModalPreloader("Sincronizando ...");
                            
                             $.ajax({
                                url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikDhcpTa', 'action' => 'syncpools'])?>" ,
                                type: 'POST',
                                dataType: "json",
                                data: JSON.stringify({controller_id: controller_id, pools_ids: pools_ids, delete_pools_side_b: $('#clear-pools').is(':checked')}),
                                success: function(data){
                                    
                                    console.log(data);
                                    
                                    if(data.response){
                                        generateNoty('success', 'Sincronización de (pools) completa');
                                    }else{
                                        generateNoty('error', 'Error en la Sincronización');
                                    }
                                    
                                    closeModalPreloader();
                                   
                                    table_poolsA.ajax.reload();
                                    table_poolsB.ajax.reload();
                                    
                                    updateCountersTitle();
                                   
                                },
                                error: function (jqXHR ) {
                                                        
                                    if(jqXHR.status == 403){
                                        
                                        generateNoty('warning', 'La sesión ha expirado. Por favor ingrese sus credenciales.');
                                        
                                        setTimeout(function(){ 
                                          window.location.href ="/ispbrain";
                                        }, 3000);
                                        
                                    }else{
                                        generateNoty('error', 'Error al intentar sincronizar los (pools).');
                                        closeModalPreloader();
                                        updateCountersTitle();
                                    }
                                }
                                
                            });
                        }
                    });
                    
                    
                }
                
                
                
            });
            
            $('#btn-sync-pool-indi').click(function() {
                
                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
                
                var pools_ids = [];
                
                pools_ids.push(poolA_selected.id);
              
                var text = '¿Está seguro que desea sincronizar este (pool)?';
    
                bootbox.confirm(text, function(result) {
                    
                    if (result) {
                        
                        $('.modal-poolsA').modal('hide');
                    
                        openModalPreloader("Sincronizando ...");
                        
                         $.ajax({
                            url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikDhcpTa', 'action' => 'syncpools'])?>" ,
                            type: 'POST',
                            dataType: "json",
                            data: JSON.stringify({controller_id: controller_id, pools_ids: pools_ids, delete_pools_side_b: $('#clear-pools').is(':checked')}),
                            success: function(data){
                                if(data.response){
                                    generateNoty('success', 'Sincronización de (pool) completa');
                                }else{
                                    generateNoty('error', 'Error en la Sincronización');
                                }
                                
                                closeModalPreloader();
                               
                                table_poolsA.ajax.reload();
                                table_poolsB.ajax.reload();
                                
                                updateCountersTitle();
                               
                            },
                            error: function (jqXHR ) {
                                                    
                                if(jqXHR.status == 403){
                                    
                                    generateNoty('warning', 'La sesión ha expirado. Por favor ingrese sus credenciales.');
                                    
                                    setTimeout(function(){ 
                                      window.location.href ="/ispbrain";
                                    }, 3000);
                                    
                                }else{
                                    generateNoty('error', 'Error al intentar sincronizar los (pools).');
                                    closeModalPreloader();
                                    updateCountersTitle();
                                }
                            }
                            
                        });
                    }
                });
            });
            
            $('#btn-refresh-pools').click(function() {
                
                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
            
                openModalPreloader("Actualizando ...");
                
                 $.ajax({
                    url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikDhcpTa', 'action' => 'refreshPools'])?>" ,
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify({controller_id: controller_id}),
                    success: function(data){
                        
                        closeModalPreloader();
    
                        if (!data.data) {
                             generateNoty('error', 'No se pudo establecer una conexión con el controlador.');
                        }else{
                            table_poolsA.ajax.reload();
                            table_poolsB.ajax.reload();
                        }
                        
                        updateCountersTitle();
                    },
                    error: function (jqXHR ) {
                                                    
                        if(jqXHR.status == 403){
                            
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese sus credenciales.');
                            
                            setTimeout(function(){ 
                              window.location.href ="/ispbrain";
                            }, 3000);
                            
                        }else{
                            generateNoty('error', 'Error al intentar sincronizar actualizar.');
                            closeModalPreloader();
                        }
                    }
                });
                
            });
            

            //end poolsA

            //poolsB

            $('#table-poolsB').removeClass('display');

    		table_poolsB = $('#table-poolsB').DataTable({
    		    "order": [[ 0, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ordering" : false,
    		    "paging": false,
    		    
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/sync/mikrotik_dhcp_ta/poolsB.json",
                    "dataSrc": "data",
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                
                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "api_id",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "name",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "ranges",
                        "render": function ( data, type, row ) {
                             if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    }
                ],
    		    "columnDefs": [
    		       
                ],
                
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-poolsB_wrapper .title').append('<h5>Controlador</h5>');
    		
    		$('#table-poolsB tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {
                    
                    table_poolsB.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    poolB_selected = table_poolsB.row( this ).data();
                    $('.modal-poolsB').modal('show');
                }
            });
            
            $('#btn-delete-pool').click(function() {
                
                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
                
                $('.modal-poolsB').modal('hide');
                
                openModalPreloader("Eliminado (pool) del Controlador ...");
                
                 $.ajax({
                    url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikDhcpTa', 'action' => 'deletePoolsInController'])?>" ,
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify({controller_id: controller_id, pool_api_id: poolB_selected.api_id}),
                    success: function(data){
                        
                        closeModalPreloader();
    
                        if (!data.data) {
                             generateNoty('error', 'Error al intentar eliminar.');
                        }else{
                            table_poolsA.ajax.reload();
                            table_poolsB.ajax.reload();
                        }
                        
                        updateCountersTitle();
                    },
                    error: function (jqXHR ) {
                                            
                        if(jqXHR.status == 403){
                            
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese sus credenciales.');
                            
                            setTimeout(function(){ 
                              window.location.href ="/ispbrain";
                            }, 3000);
                            
                        }else{
                            generateNoty('error', 'Error al intentar eliminar.');
                            closeModalPreloader();
                            updateCountersTitle();
                        }
                    }
                });
            });
    		
    		 //end poolsB

    		$('a[id="pools-tab"]').on('shown.bs.tab', function (e) {
    		    
                table_poolsA.draw();
                table_poolsB.draw();
               
            });
            
       
           
        });
    </script>
<?php $this->end(); ?>
