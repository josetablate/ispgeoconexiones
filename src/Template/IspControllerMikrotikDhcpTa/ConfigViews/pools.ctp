<?php $this->extend('/IspControllerMikrotikDhcpTa/ConfigViews/dhcp-server'); ?>

<?php $this->start('pools'); ?>

   <div id="btns-tools-tpools">
        <div class="text-right btns-tools margin-bottom-5">

            <?php 
                echo $this->Html->link(
                    '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Agregar',
                    'class' => 'btn btn-default btn-add-tpool',
                    'escape' => false
                    ]);
            ?>

        </div>
    </div>
    
    <div class="row justify-content-center mt-2">
        <div class="col-auto">
            <div class="card border-naranja">
              <div class="card-body text-secondary p-2">
                <p class="card-text text-naranja">Para que los cambios surtan efecto en el controlador, debe <a href="/ispbrain/IspControllerMikrotikDhcpTa/sync/<?=$controller->id?>"  class="font-weight-bold"  >sincronizar</a>.</p>
              </div>
            </div>
        </div>
    </div>

   <div class="row">
        <div class="col-7 mx-auto">
            
            <div class="card border-secondary mt-2">
                <div class="card-body p-1">
                    
                     <table class="table table-bordered table-hover" id="table-tpools">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Rango</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                    </table>

                    
                </div>
            </div>
        </div>
    </div>

   
    <!-- modal actions tpools -->
    <?php
        $buttons = [];

         $buttons[] = [
            'id'   =>  'btn-edit-tpool',
            'name' =>  'Editar',
            'icon' =>  'icon-pencil2',
            'type' =>  'btn-default'
        ];

        $buttons[] = [
            'id'   =>  'btn-delete-tpool',
            'name' =>  'Eliminar',
            'icon' =>  'icon-bin',
            'type' =>  'btn-danger'
        ];

        echo $this->element('actions', ['modal'=> 'modal-actions-tpools', 'title' => 'Acciones', 'buttons' => $buttons ]);
    ?>

    <!-- modal agregar tpools -->
    <div class="modal fade modal-add-tpool" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="gridSystemModalLabel">Agregar Pool</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-12">

                            <?= $this->Form->create(null, ['id' => 'form-add-pool']) ?>
                                <fieldset>
                                    
                                    
                                    <div class="row">
                                        <div class="col-xl-12">
                                            <?php
                                            echo $this->Form->input('name', ['label' => 'Nombre', 'value' => '', 'required' => true, 'autocomplete' => 'off']);
                                            ?>
                                        </div>
                                          <div class="col-xl-12">
                                            <?php
                                            echo $this->Form->input('ranges', ['label' => 'Rango', 'required' => true]);                    
                                            ?>
                                        </div>
                                        
                                       
                                         <?php
                                            echo $this->Form->input('controller_id', ['value' => $controller->id, 'type' => 'hidden']);
                                            ?>
                                        
                                    </div>

                                </fieldset>
                                <?= $this->Form->button(__('Agregar', ['id' => 'btn-submit-add-pool'])) ?>
                            <?= $this->Form->end() ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal edit tpools -->
    <div class="modal fade modal-edit-tpool" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="gridSystemModalLabel">Editar Pool</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">

                            <?= $this->Form->create(null, ['id' => 'form-edit-pool']) ?>
                                
                                <fieldset>
                                    
                                    <div class="row">
                                        <div class="col-xl-12">
                                            <?php
                                            echo $this->Form->input('name', ['label' => 'Nombre', 'value' => '', 'required' => true, 'autocomplete' => 'off']);
                                            ?>
                                        </div>
                                          <div class="col-xl-12">
                                            <?php
                                            echo $this->Form->input('ranges', ['label' => 'Rango', 'required' => true]);
                                            echo '<p class="text-muted">Formato 10.0.0.0/24</p>';
                                            ?>
                                        </div>
                                        
                                            <?php
                                            echo $this->Form->input('controller_id', ['value' => $controller->id, 'type' => 'hidden']);
                                            echo $this->Form->input('id', ['type' => 'hidden']);
                                            ?>
                                       
                                    </div>

                                </fieldset>
                                <?= $this->Form->button(__('Guardar'), ['id' => 'btn-submit-edit-pool']) ?>
                            <?= $this->Form->end() ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        //tpools
        
        var pool_selected = null;
        var table_tpools = null;

        $(document).ready(function () {

            $('#table-tpools').removeClass('display');

    		table_tpools = $('#table-tpools').on( 'draw.dt', function () {
    		    
    		    updateSelectPools();
                    
                }).DataTable({
                    
    		    "order": [[ 1, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ajax": {
                    "url": "/ispbrain/IspControllerMikrotikDhcpTa/get_pools_by_controller.json",
                    "dataSrc": "pools",
                    "data": function ( d ) {
                        return $.extend( {}, d, {
                            "controller_id": controller_id
                        });
                    },
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                
                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "name"
                    },
    		        { 
                        "data": "ranges"
                    },
                  
                ],
    		    "columnDefs": [
    		        
                ],
                "createdRow" : function( row, data, index ) {
                    
                    row.id = data.id;
                    if(!data.api_id){
                        $(row).addClass('no-sync');
                    }
                },
              
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
            

            $('#table-tpools_wrapper .tools').append($('#btns-tools-tpools').contents());
    		
    		$('#btns-tpools-tpools').show();

            $(".btn-export-tpools").click(function(){

                bootbox.confirm('Se descargará un archivo Excel', function(result) {
                    if (result) {
                        $('#table-tpools').tableExport({tableName: 'Pools', type:'excel', escape:'false'});
                    }
                });
            });

            $('#table-tpools tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {

                    table_tpools.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    pool_selected = table_tpools.row( this ).data();

                    $('.modal-actions-tpools').modal('show');
                }
            });

            $('.btn-add-tpool').click(function() {
                $('.modal-add-tpool').modal('show');
            });
            
            $('#form-add-pool').submit(function(){
                
                 event.preventDefault();
                 
                var send_data = {};
                 
                var form_data = $(this).serializeArray();
                
                $.each(form_data, function(i, val){
                    send_data[val.name] = val.value;
                });
                
                $('#btn-submit-add-pool').hide(); 
              
                openModalPreloader("Agregando Pool ...");
                
                $.ajax({
                        type: 'POST',
                        dataType: "json",
                        url: '/ispbrain/IspControllerMikrotikDhcpTa/addPool',
                        data:  JSON.stringify(send_data),
                        success: function(data){
                            
                            $('#btn-submit-add-pool').show(); 
                            closeModalPreloader();
                            
                            if(data.response){
                                generateNoty('success', 'El Pool se agrego correctamente');
                            }else{
                                generateNoty('error', 'Error al intentar agregar el Pool');
                            }
                            
                            table_tpools.ajax.reload();
                            updateSelectPools();
                            $('.modal-add-tpool').modal('hide');
                            
                            updateCountersTitle();
                            
                        },
                        error: function(jqXHR) {
                            if (jqXHR.status == 403) {
                            
                            	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                            
                            	setTimeout(function() { 
                            	  window.location.href ="/ispbrain";
                            	}, 3000);
                            
                            } else {
                            	generateNoty('error', 'Error al agregar el Pool.');
                            }
                        }
                    });
                
            });

            $('#btn-delete-tpool').click(function() {
               
                var text = '¿Está seguro que desea eliminar el Pool : ' + pool_selected.name + '?';
                
                bootbox.confirm(text, function(result) {
                    if (result) {
                     
                        openModalPreloader("Eliminado Pool ...");
                        
                        $.ajax({
                            type: 'POST',
                            dataType: "json",
                            url: '/ispbrain/IspControllerMikrotikDhcpTa/deletePool',
                            data:  JSON.stringify({id: pool_selected.id}),
                            success: function(data){
                              
                                closeModalPreloader();
                              
                                if(data.response){
                                    generateNoty('success', 'El Pool se elimino correctamente');
                                }else{
                                    generateNoty('error', 'Error al intentar eliminar el Pool');
                                }
                        
                                table_tpools.ajax.reload();
                                updateSelectPools();
                                
                                $('.modal-actions-tpools').modal('hide');
                                
                                updateCountersTitle();
                                
                            },
                            error: function(jqXHR) {
                                if (jqXHR.status == 403) {
                                
                                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                                
                                	setTimeout(function() { 
                                	  window.location.href ="/ispbrain";
                                	}, 3000);
                                
                                } else {
                                	generateNoty('error', 'Error al eliminar el Pool.');
                                }
                            }
                        });
                    }
                });
                
            });

            $('#btn-edit-tpool').click(function(){
                
                $('.modal-actions-tpools').modal('hide');
             
                $('.modal-edit-tpool input#id').val(pool_selected.id);
                $('.modal-edit-tpool input#name').val(pool_selected.name);
                $('.modal-edit-tpool input#ranges').val(pool_selected.ranges);
                
                $('.modal-edit-tpool').modal('show');
                
            });
            
            $('#form-edit-pool').submit(function(){
                
                 event.preventDefault();
                 
                var send_data = {};
                 
                var form_data = $(this).serializeArray();
                
                $.each(form_data, function(i, val){
                    send_data[val.name] = val.value;
                });
                
                $('#btn-submit-edit-pool').hide(); 
               
                openModalPreloader("Editando Pool ...");
                
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '/ispbrain/IspControllerMikrotikDhcpTa/editPool',
                    data:  JSON.stringify(send_data),
                    success: function(data){
                        
                        closeModalPreloader();
                        
                        $('#btn-submit-edit-pool').show(); 
                        
                        if(data.response){
                            generateNoty('success', 'El Pool se edito correctamente');
                        }else{
                            generateNoty('error', 'Error al intentar editar el Pool');
                        }
                 
                        table_tpools.ajax.reload();
                        updateSelectPools();
                        $('.modal-edit-tpool').modal('hide');
                        
                        updateCountersTitle();
                   
                    },
                    error: function(jqXHR) {
                        if (jqXHR.status == 403) {
                        
                        	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                        
                        	setTimeout(function() { 
                        	  window.location.href ="/ispbrain";
                        	}, 3000);
                        
                        } else {
                        	generateNoty('error', 'Error al editar el Pool.');
                        }
                    }
                });
                
            });


            $('a[id="pools-tab"]').on('shown.bs.tab', function (e) {
                table_tpools.draw();
            });
        });

        function updateSelectPools(){
            
            if(table_tpools){
                
                 $('.modal-add-dhcpServer #address-pool').html('');
                 $('.modal-edit-dhcpServer #address-pool').html('');
                 
                 $('.modal-add-dhcpServer #address-pool').append($('<option>', { 
                        value: '',
                        text : '' 
                 }));
                 
                 $('.modal-edit-dhcpServer #address-pool').append($('<option>', { 
                        value: '',
                        text : '' 
                 }));
               
                
                $.each(table_tpools.rows().data(), function (i, pool) {
                    
                    $('.modal-add-dhcpServer #address-pool').append($('<option>', { 
                        value: pool.id,
                        text : pool.name 
                    }));
                    
                    $('.modal-edit-dhcpServer #address-pool').append($('<option>', { 
                        value: pool.id,
                        text : pool.name 
                    }));
                
                });
                
            }
            
        }

        // Jquery draggable modals
        $('.modal-dialog').draggable({
            handle: ".modal-header"
        });

        //end tpools
    </script>

<?php $this->end(); ?>
