<?php $this->extend('/IspControllerMikrotikDhcpTa/ConfigViews/profiles'); ?>

<?php $this->start('gateways'); ?>

   <div id="btns-tools-tgateways">
        <div class="text-right btns-tools margin-bottom-5">

            <?php 
                echo $this->Html->link(
                    '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Agregar',
                    'class' => 'btn btn-default btn-add-tgateway',
                    'escape' => false
                    ]);
            ?>

        </div>
    </div>
    
    <div class="row justify-content-center mt-2">
        <div class="col-auto">
            <div class="card border-naranja">
              <div class="card-body text-secondary p-2">
                <p class="card-text text-naranja">Para que los cambios surtan efecto en el controlador, debe <a href="/ispbrain/IspControllerMikrotikDhcpTa/sync/<?=$controller->id?>"  class="font-weight-bold"  >sincronizar</a>.</p>
              </div>
            </div>
        </div>
    </div>

   <div class="row">
        <div class="col-7 mx-auto">
            
            <div class="card border-secondary mt-2">
                <div class="card-body p-1">
                    
                     <table class="table table-bordered table-hover" id="table-tgateways">
                        <thead>
                            <tr>
                                <th>Address</th>
                                <th>Interface</th>
                                <th>Comentario</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                    </table>

                    
                </div>
            </div>
        </div>
    </div>

   
    <!-- modal actions tgateways -->
    <?php
        $buttons = [];

         $buttons[] = [
            'id'   =>  'btn-edit-tgateway',
            'name' =>  'Editar',
            'icon' =>  'icon-pencil2',
            'type' =>  'btn-default'
        ];

        $buttons[] = [
            'id'   =>  'btn-delete-tgateway',
            'name' =>  'Eliminar',
            'icon' =>  'icon-bin',
            'type' =>  'btn-danger'
        ];

        echo $this->element('actions', ['modal'=> 'modal-actions-tgateways', 'title' => 'Acciones', 'buttons' => $buttons ]);
    ?>

    <!-- modal agregar tgateways -->
    <div class="modal fade modal-add-tgateway" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="gridSystemModalLabel">Agregar Gateway</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-12">

                            <?= $this->Form->create(null, ['id' => 'form-add-gateway']) ?>
                                <fieldset>
                                    
                                    
                                    <div class="row">
                                        <div class="col-xl-12">
                                            <?php
                                            echo $this->Form->input('network_id', ['label' => 'Address', 'options' => [], 'required' => true, 'autocomplete' => 'off']);
                                            ?>
                                        </div>
                                        <div class="col-xl-12">
                                            <?php
                                            echo $this->Form->input('interface', ['label' => 'Interface',  'options' => $controller->interfaces, 'required' => true]);
                                            ?>
                                        </div>
                                        <div class="col-xl-12">
                                            <?php
                                            echo $this->Form->input('comment', ['label' => 'Comentario', 'required' => true]);
                                            ?>
                                        </div>
                                       
                                         <?php
                                            echo $this->Form->input('controller_id', ['value' => $controller->id, 'type' => 'hidden']);
                                            ?>
                                        
                                    </div>

                                </fieldset>
                                <?= $this->Form->button(__('Agregar', ['id' => 'btn-submit-add-gateway'])) ?>
                            <?= $this->Form->end() ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal edit tgateways -->
    <div class="modal fade modal-edit-tgateway" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="gridSystemModalLabel">Editar Gateway</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">

                            <?= $this->Form->create(null, ['id' => 'form-edit-gateway']) ?>
                                
                                <fieldset>
                                    
                                    <div class="row">
                                        <div class="col-xl-12">
                                            <?php
                                            echo $this->Form->input('network_id', ['label' => 'Address', 'options' => [], 'required' => true, 'autocomplete' => 'off']);
                                            ?>
                                        </div>
                                        <div class="col-xl-12">
                                            <?php
                                            echo $this->Form->input('interface', ['label' => 'Interface',  'options' => $controller->interfaces, 'required' => true]);
                                            ?>
                                        </div>
                                        <div class="col-xl-12">
                                            <?php
                                            echo $this->Form->input('comment', ['label' => 'Comentario', 'required' => true]);
                                            ?>
                                        </div>
                                        
                                        <?php
                                            echo $this->Form->input('controller_id', ['value' => $controller->id, 'type' => 'hidden']);
                                            echo $this->Form->input('id', ['type' => 'hidden']);
                                        ?>
                                       
                                    </div>

                                </fieldset>
                                <?= $this->Form->button(__('Guardar'), ['id' => 'btn-submit-edit-gateway']) ?>
                            <?= $this->Form->end() ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        //tgateways
        
        var gateway_selected = null;
        var table_tgateways = null;

        $(document).ready(function () {

            $('#table-tgateways').removeClass('display');

    		table_tgateways = $('#table-tgateways').on( 'draw.dt', function () {
                    
                }).DataTable({
                    
    		    "order": [[ 1, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ajax": {
                    "url": "/ispbrain/IspControllerMikrotikDhcpTa/get_gateways_by_controller.json",
                    "dataSrc": "gateways",
                    "data": function ( d ) {
                        return $.extend( {}, d, {
                            "controller_id": controller_id
                        });
                    },
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                
                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "address"
                    },
    		        { 
                        "data": "interface"
                    },
                     { 
                        "data": "comment"
                    },
                  
                ],
    		    "columnDefs": [
    		        
                ],
                "createdRow" : function( row, data, index ) {
                    
                    row.id = data.id;
                    if(!data.api_id){
                        $(row).addClass('no-sync');
                    }
                },
              
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
            

            $('#table-tgateways_wrapper .tools').append($('#btns-tools-tgateways').contents());
    		
    		$('#btns-tgateways-tgateways').show();

            $(".btn-export-tgateways").click(function(){

                bootbox.confirm('Se descargará un archivo Excel', function(result) {
                    if (result) {
                        $('#table-tgateways').tableExport({tableName: 'Gateways', type:'excel', escape:'false'});
                    }
                });
            });

            $('#table-tgateways tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {

                    table_tgateways.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    gateway_selected = table_tgateways.row( this ).data();

                    $('.modal-actions-tgateways').modal('show');
                }
            });

            $('.btn-add-tgateway').click(function() {
                $('.modal-add-tgateway').modal('show');
            });
            
            $('#form-add-gateway').submit(function(){
                
                 event.preventDefault();
                 
                var send_data = {};
                 
                var form_data = $(this).serializeArray();
                
                $.each(form_data, function(i, val){
                    send_data[val.name] = val.value;
                });
                
                $('#btn-submit-add-gateway').hide(); 
              
                openModalPreloader("Agregando Gateway ...");
                
                $.ajax({
                        type: 'POST',
                        dataType: "json",
                        url: '/ispbrain/IspControllerMikrotikDhcpTa/addGateway',
                        data:  JSON.stringify(send_data),
                        success: function(data){
                            
                            $('#btn-submit-add-gateway').show(); 
                            closeModalPreloader();
                            
                            if(data.response){
                                generateNoty('success', 'El Gateway se agrego correctamente');
                            }else{
                                generateNoty('error', 'Error al intentar agregar el Gateway');
                            }
                            
                            table_tgateways.ajax.reload();
                         
                            $('.modal-add-tgateway').modal('hide');
                            
                            updateCountersTitle();
                            
                        },
                        error: function(jqXHR) {
                            if (jqXHR.status == 403) {
                            
                            	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                            
                            	setTimeout(function() { 
                            	  window.location.href ="/ispbrain";
                            	}, 3000);
                            
                            } else {
                            	generateNoty('error', 'Error al agregar el Gateway.');
                            }
                        }
                    });
                
            });

            $('#btn-delete-tgateway').click(function() {
               
                var text = '¿Está seguro que desea eliminar el Gateway : ' + gateway_selected.address + '?';
                
                bootbox.confirm(text, function(result) {
                    if (result) {
                     
                        openModalPreloader("Eliminado Gateway ...");
                        
                        $.ajax({
                            type: 'POST',
                            dataType: "json",
                            url: '/ispbrain/IspControllerMikrotikDhcpTa/deleteGateway',
                            data:  JSON.stringify({id: gateway_selected.id}),
                            success: function(data){
                              
                                closeModalPreloader();
                              
                                if(data.response){
                                    generateNoty('success', 'El Gateway se elimino correctamente');
                                }else{
                                    generateNoty('error', 'Error al intentar eliminar el Gateway');
                                }
                        
                                table_tgateways.ajax.reload();
                             
                                
                                $('.modal-actions-tgateways').modal('hide');
                                
                                updateCountersTitle();
                                
                            },
                            error: function(jqXHR) {
                                if (jqXHR.status == 403) {
                                
                                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                                
                                	setTimeout(function() { 
                                	  window.location.href ="/ispbrain";
                                	}, 3000);
                                
                                } else {
                                	generateNoty('error', 'Error al eliminar el Gateway.');
                                }
                            }
                        });
                    }
                });
                
            });

            $('#btn-edit-tgateway').click(function(){
                
                $('.modal-actions-tgateways').modal('hide');
                
                console.log(gateway_selected);
             
                $('.modal-edit-tgateway input#id').val(gateway_selected.id);
                $('.modal-edit-tgateway select#network-id').val(gateway_selected.network_id);
                $('.modal-edit-tgateway select#interface').val(gateway_selected.interface);
                $('.modal-edit-tgateway input#comment').val(gateway_selected.comment);
                
                $('.modal-edit-tgateway').modal('show');
                
            });
            
            $('#form-edit-gateway').submit(function(){
                
                 event.preventDefault();
                 
                var send_data = {};
                 
                var form_data = $(this).serializeArray();
                
                $.each(form_data, function(i, val){
                    send_data[val.name] = val.value;
                });
                
                $('#btn-submit-edit-gateway').hide(); 
               
                openModalPreloader("Editando Gateway ...");
                
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '/ispbrain/IspControllerMikrotikDhcpTa/editGateway',
                    data:  JSON.stringify(send_data),
                    success: function(data){
                        
                        closeModalPreloader();
                        
                        $('#btn-submit-edit-gateway').show(); 
                        
                        if(data.response){
                            generateNoty('success', 'El Gateway se edito correctamente');
                        }else{
                            generateNoty('error', 'Error al intentar editar el Gateway');
                        }
                 
                        table_tgateways.ajax.reload();
                       
                        $('.modal-edit-tgateway').modal('hide');
                        
                        updateCountersTitle();
                   
                    },
                    error: function(jqXHR) {
                        if (jqXHR.status == 403) {
                        
                        	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                        
                        	setTimeout(function() { 
                        	  window.location.href ="/ispbrain";
                        	}, 3000);
                        
                        } else {
                        	generateNoty('error', 'Error al editar el Gateway.');
                        }
                    }
                });
                
            });


            $('a[id="gateways-tab"]').on('shown.bs.tab', function (e) {
                table_tgateways.ajax.reload();
            });
        });

        // Jquery draggable modals
        $('.modal-dialog').draggable({
            handle: ".modal-header"
        });

        //end tgateways
    </script>

<?php $this->end(); ?>
