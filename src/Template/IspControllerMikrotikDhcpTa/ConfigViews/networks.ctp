<?php $this->extend('/IspControllerMikrotikDhcpTa/ConfigViews/gateways'); ?>

<?php $this->start('networks'); ?>

   <div id="btns-tools-tnetworks">
        <div class="text-right btns-tools margin-bottom-5">

            <?php 
                echo $this->Html->link(
                    '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Agregar',
                    'class' => 'btn btn-default btn-add-tnetwork',
                    'escape' => false
                    ]);

                 echo $this->Html->link(
                    '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Exportar tabla',
                    'class' => 'btn btn-default btn-export-tnetworks ml-1',
                    'escape' => false
                    ]);
            ?>

        </div>
    </div>
    
    <div class="row justify-content-center mt-2">
        <div class="col-auto">
            <div class="card border-naranja">
              <div class="card-body text-secondary p-2">
                <p class="card-text text-naranja">Para que los cambios surtan efecto en el controlador, debe <a href="/ispbrain/IspControllerMikrotikDhcpTa/sync/<?=$controller->id?>"  class="font-weight-bold"  >sincronizar</a>.</p>
              </div>
            </div>
        </div>
    </div>

   <div class="row">
        <div class="col-9 mx-auto">
            
            <div class="card border-secondary mt-2">
                <div class="card-body p-1">
                    
                     <table class="table table-bordered table-hover" id="table-tnetworks">
                        <thead>
                            <tr>
                                <th>Conexiones</th>
                                <th>Comentario</th>
                                <th>Addresses</th>
                                <th>Gateway</th>
                                <th>DNS Server</th>
                                <th>Next Network</th>
                                <th>Nuevo cliente</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                    </table>

                    
                </div>
            </div>
        </div>
    </div>

   
    <!-- modal actions tnetworks -->
    <?php
        $buttons = [];

         $buttons[] = [
            'id'   =>  'btn-edit-tnetwork',
            'name' =>  'Editar',
            'icon' =>  'icon-pencil2',
            'type' =>  'btn-default'
        ];
        
        $buttons[] = [
            'id'   =>  'btn-move-tnetwork',
            'name' =>  'Cambiar Network',
            'icon' =>  'icon-pencil2',
            'type' =>  'btn-default'
        ];

        $buttons[] = [
            'id'   =>  'btn-delete-tnetwork',
            'name' =>  'Eliminar',
            'icon' =>  'icon-bin',
            'type' =>  'btn-danger'
        ];

        
        echo $this->element('actions', ['modal'=> 'modal-actions-tnetworks', 'title' => 'Acciones', 'buttons' => $buttons ]);
    ?>
    
    

    <!-- modal agregar tnetworks -->
    <div class="modal fade modal-add-tnetwork" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="gridSystemModalLabel">Agregar Network</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-12">

                            <?= $this->Form->create(null, ['id' => 'form-add-network']) ?>
                                <fieldset>
                                    
                                    
                                    <div class="row">
                                        <div class="col-xl-4">
                                            <?php
                                            echo $this->Form->input('comment', ['label' => 'Nombre', 'value' => '', 'required' => true, 'autocomplete' => 'off']);
                                            ?>
                                        </div>
                                          <div class="col-xl-4">
                                            <?php
                                            echo $this->Form->input('gateway', ['label' => 'Gateway', 'required' => true]);
                                            ?>
                                        </div>
                                         <div class="col-xl-4">
                                            <?php
                                            echo $this->Form->input('dns_server', ['label' => 'DNS Server', 'required' => false]);
                                            ?>
                                        </div>
                                         <div class="col-xl-4">
                                            <?php
                                            echo $this->Form->input('address', ['label' => 'Address', 'required' => true, 'autocomplete' => 'off']);
                                            echo '<p class="text-muted">Formato 10.0.0.0/24</p>';
                                            ?>
                                        </div>
                                         <div class="col-xl-4">
                                            <?php
                                            echo $this->Form->input('min_host', ['label' => 'Min Host', 'readonly' => true]);
                                            ?>
                                        </div>
                                         <div class="col-xl-4">
                                            <?php
                                            echo $this->Form->input('max_host', ['label' => 'Max Host', 'readonly' => true]);
                                            ?>
                                        </div>
                                        
                                        
                                         <div class="col-xl-4">
                                            <?php
                                            echo $this->Form->input('next_network_id', ['label' => 'Next Network', 'required' => false]);
                                            echo $this->Form->input('controller_id', ['value' => $controller->id, 'type' => 'hidden']);
                                            ?>
                                        </div>
                                        
                                         <div class="col-xl-4">
                                            <?php
                                            echo $this->Form->input('access_new_customer', ['type' => 'checkbox', 'label' => 'Para Nuevos Clientes', 'checked' => false]);
                                            ?>
                                        </div>
                                    </div>

                                </fieldset>
                                <?= $this->Form->button(__('Agregar', ['id' => 'btn-submit-add-network'])) ?>
                            <?= $this->Form->end() ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal edit tnetworks -->
    <div class="modal fade modal-edit-tnetwork" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="gridSystemModalLabel">Editar Network</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">

                            <?= $this->Form->create(null, ['id' => 'form-edit-network']) ?>
                                
                                <fieldset>
                                    
                                    <div class="row">
                                        <div class="col-xl-4">
                                            <?php
                                            echo $this->Form->input('comment', ['label' => 'Nombre', 'value' => '', 'required' => true, 'autocomplete' => 'off']);
                                            ?>
                                        </div>
                                          <div class="col-xl-4">
                                            <?php
                                            echo $this->Form->input('gateway', ['label' => 'Gateway', 'required' => true]);
                                            ?>
                                        </div>
                                         <div class="col-xl-4">
                                            <?php
                                            echo $this->Form->input('dns_server', ['label' => 'DNS Server', 'required' => false]);
                                            ?>
                                        </div>
                                         <div class="col-xl-4">
                                            <?php
                                            echo $this->Form->input('address', ['label' => 'Address', 'required' => true, 'autocomplete' => 'off']);
                                            echo '<p class="text-muted">Formato 10.0.0.0/24</p>';
                                            ?>
                                        </div>
                                         <div class="col-xl-4">
                                            <?php
                                            echo $this->Form->input('min_host', ['label' => 'Min Host', 'readonly' => true]);
                                            ?>
                                        </div>
                                         <div class="col-xl-4">
                                            <?php
                                            echo $this->Form->input('max_host', ['label' => 'Max Host', 'readonly' => true]);
                                            ?>
                                        </div>
                                      
                                         <div class="col-xl-4">
                                            <?php
                                            echo $this->Form->input('next_network_id', ['label' => 'Next Network', 'required' => false]);
                                            echo $this->Form->input('controller_id', ['value' => $controller->id, 'type' => 'hidden']);
                                            echo $this->Form->input('id', ['type' => 'hidden']);
                                            ?>
                                        </div>
                                        
                                        <div class="col-xl-4">
                                            <?php
                                            echo $this->Form->input('access_new_customer', ['type' => 'checkbox',  'label' => 'Para nuevos clientes']);
                                            ?>
                                        </div>
                                    </div>

                                </fieldset>
                                <?= $this->Form->button(__('Guardar'), ['id' => 'btn-submit-edit-network']) ?>
                            <?= $this->Form->end() ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- modal move tnetworks -->
    <div class="modal fade modal-move-tnetwork" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="gridSystemModalLabel">Cambiar Network</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    
                     <div class="row justify-content-center m-3">
                        <div class="col-12">
                            <div class="card border-naranja">
                              <div class="card-body text-secondary p-3">
                                <p class="card-text text-naranja">
                                    Esta acción permite mover las conexiones de una red a otra.
                                    Si la red destino no tiene las ips libres suficientes,
                                    las conexiones que no pudieron pasar, quedarán con la red original.
                                </p>
                              </div>
                            </div>
                        </div>
                    </div>
                                    
                    
                    
                    <div class="row">
                        <div class="col-md-12">

                            <?= $this->Form->create(null, ['id' => 'form-move-network']) ?>
                                
                                <fieldset>
                                    
                                    <div class="row">
                                        <div class="col-xl-12">
                                            <?php
                                            echo $this->Form->input('network_from_name', ['label' => 'Network (origen)', 'value' => '', 'disabled' => true]);
                                            echo $this->Form->input('network_from_id', ['type' => 'hidden']);
                                            ?>
                                        </div>
                                          <div class="col-xl-12">
                                            <?php
                                            echo $this->Form->input('network_to_id', ['label' => 'Network (destino)', 'options' => []]);
                                            echo $this->Form->input('controller_id', ['value' => $controller->id, 'type' => 'hidden']);
                                            ?>
                                        </div>
                                    </div>

                                </fieldset>
                                <?= $this->Form->button(__('Confimar'), ['id' => 'btn-submit-move-network', 'class' => 'btn-danger']) ?>
                            <?= $this->Form->end() ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    

    <script type="text/javascript">
        //tnetworks
        
        var network_selected = null;
        var table_tnetworks = null;

        $(document).ready(function () {

            $('#table-tnetworks').removeClass('display');

    		table_tnetworks = $('#table-tnetworks').on( 'draw.dt', function () {
    		    
    		    updateSelectNetworks();
                    
                }).DataTable({
                    
    		    "order": [[ 1, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ajax": {
                    "url": "/ispbrain/IspControllerMikrotikDhcpTa/get_networks_by_controller.json",
                    "dataSrc": "networks",
                    "data": function ( d ) {
                        return $.extend( {}, d, {
                            "controller_id": controller_id
                        });
                    },
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                
                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "connections_amount"
                    },
    		        { 
                        "data": "comment"
                    },
                    { 
                        "data": "address"
                    },
                    { 
                        "data": "gateway"
                    },
                    { 
                        "data": "dns_server"
                    },
                    { 
                        "data": "next_network",
                        "render": function ( data, type, row ) {
                            return data ? data.comment : '';
                        }
                    },
                    { 
                        "data": "access_new_customer",
                        "render": function ( data, type, row ) {
                            if(data){
                                return '<i class="fa fa-check text-success" aria-hidden="true"></i>'
                            }
                            return '';
                        }
                    },
                ],
    		    "columnDefs": [
    		        { "type": 'ip-address', targets: [3] },
                ],
                "createdRow" : function( row, data, index ) {
                    
                  
                    row.id = data.id;
                    if(!data.api_id){
                        $(row).addClass('no-sync');
                    }
                },
              
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
            

            $('#table-tnetworks_wrapper .tools').append($('#btns-tools-tnetworks').contents());
    		
    		$('#btns-tnetworks-tnetworks').show();

            $(".btn-export-tnetworks").click(function(){

                bootbox.confirm('Se descargará un archivo Excel', function(result) {
                    if (result) {
                        $('#table-tnetworks').tableExport({tableName: 'Networks', type:'excel', escape:'false'});
                    }
                });
            });

            $('#table-tnetworks tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {

                    table_tnetworks.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    network_selected = table_tnetworks.row( this ).data();

                    $('.modal-actions-tnetworks').modal('show');
                }
            });

            $('.btn-add-tnetwork').click(function() {
                $('.modal-add-tnetwork').modal('show');
            });
            
            $('#form-add-network').submit(function(){
                
                 event.preventDefault();
                 
                var send_data = {};
                 
                var form_data = $(this).serializeArray();
                
                $.each(form_data, function(i, val){
                    send_data[val.name] = val.value;
                });
                
                $('#btn-submit-add-network').hide(); 
              
                openModalPreloader("Agregando Network ...");
                
                $.ajax({
                        type: 'POST',
                        dataType: "json",
                        url: '/ispbrain/IspControllerMikrotikDhcpTa/addNetwork',
                        data:  JSON.stringify(send_data),
                        success: function(data){
                            
                            $('#btn-submit-add-network').show(); 
                            closeModalPreloader();
                            
                            if(data.response){
                                generateNoty('success', 'El Network se agrego correctamente');
                            }else{
                                generateNoty('error', 'Error al intentar agregar el Network');
                                updateCountersTitle();
                            }
                            
                            table_tnetworks.ajax.reload();
                            updateSelectNetworks();
                            
                            $('.modal-add-tnetwork').modal('hide');
                            
                        },
                        error: function(jqXHR) {
                            if (jqXHR.status == 403) {
                            
                            	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                            
                            	setTimeout(function() { 
                            	  window.location.href ="/ispbrain";
                            	}, 3000);
                            
                            } else {
                            	generateNoty('error', 'Error al agregar el Network.');
                            }
                        }
                    });
                
            });

            $('#btn-delete-tnetwork').click(function() {
               
                var text = '¿Está seguro que desea eliminar el Network : ' + network_selected.comment + '?';
                
                bootbox.confirm(text, function(result) {
                    if (result) {
                     
                        openModalPreloader("Eliminado Network ...");
                        
                        $.ajax({
                            type: 'POST',
                            dataType: "json",
                            url: '/ispbrain/IspControllerMikrotikDhcpTa/deleteNetwork',
                            data:  JSON.stringify({id: network_selected.id}),
                            success: function(data){
                              
                                closeModalPreloader();
                              
                                if(data.response){
                                    generateNoty('success', 'El Network se elimino correctamente');
                                }else{
                                    generateNoty('error', 'Error al intentar eliminar el Network');
                                    updateCountersTitle();
                                }
                        
                                table_tnetworks.ajax.reload();
                                updateSelectNetworks();
                                
                                $('.modal-actions-tnetworks').modal('hide');
                                
                                
                                
                            },
                            error: function(jqXHR) {
                                if (jqXHR.status == 403) {
                                
                                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                                
                                	setTimeout(function() { 
                                	  window.location.href ="/ispbrain";
                                	}, 3000);
                                
                                } else {
                                	generateNoty('error', 'Error al eliminar el Network.');
                                }
                            }
                        });
                    }
                });
                
            });

            $('#btn-edit-tnetwork').click(function(){
                
                $('.modal-actions-tnetworks').modal('hide');
             
                $('.modal-edit-tnetwork input#id').val(network_selected.id);
                $('.modal-edit-tnetwork input#comment').val(network_selected.comment);
                $('.modal-edit-tnetwork input#gateway').val(network_selected.gateway);
                $('.modal-edit-tnetwork input#dns-server').val(network_selected.dns_server);
                $('.modal-edit-tnetwork input#address').val(network_selected.address);
                $('.modal-edit-tnetwork select#next-network-id').val(network_selected.next_network ? network_selected.next_network.id : '');
                $('.modal-edit-tnetwork input#address').focusout();
                // $('.modal-edit-tnetwork input#access-new-customer').val(network_selected.access_new_customer);
                $('.modal-edit-tnetwork input#access-new-customer').attr('checked', network_selected.access_new_customer);
                
                
                $('.modal-edit-tnetwork').modal('show');
                
            });
            
            $('#form-edit-network').submit(function(){
                
                 event.preventDefault();
                 
                var send_data = {};
                 
                var form_data = $(this).serializeArray();
                
                $.each(form_data, function(i, val){
                    send_data[val.name] = val.value;
                });
                
                $('#btn-submit-edit-network').hide(); 
               
                openModalPreloader("Editando Network ...");
                
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '/ispbrain/IspControllerMikrotikDhcpTa/editNetwork',
                    data:  JSON.stringify(send_data),
                    success: function(data){
                        
                        closeModalPreloader();
                        
                        $('#btn-submit-edit-network').show(); 
                        
                        if(data.response){
                            generateNoty('success', 'El Network se edito correctamente');
                        }else{
                            generateNoty('error', 'Error al intentar editar el Network');
                            updateCountersTitle();
                        }
                 
                        table_tnetworks.ajax.reload();
                        updateSelectNetworks();
                        $('.modal-edit-tnetwork').modal('hide');
                        
                        updateCountersTitle();
                   
                    },
                    error: function(jqXHR) {
                        if (jqXHR.status == 403) {
                        
                        	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                        
                        	setTimeout(function() { 
                        	  window.location.href ="/ispbrain";
                        	}, 3000);
                        
                        } else {
                        	generateNoty('error', 'Error al editar el Network.');
                        }
                    }
                });
                
            });
            
            $('#btn-move-tnetwork').click(function(){
                
                $('.modal-actions-tnetworks').modal('hide');
             
                $('.modal-move-tnetwork input#network-from-name').val(network_selected.comment + ' (' + network_selected.connections_amount + ')');
                $('.modal-move-tnetwork input#network-from-id').val(network_selected.id);
                
                $('.modal-move-tnetwork select#network_to_id').val('');
                
                $('.modal-move-tnetwork').modal('show');
                
            });
            
            $('#form-move-network').submit(function(){

               var send_data = {};

               var form_data = $(this).serializeArray();

               $.each(form_data, function(i, val){
                    send_data[val.name] = val.value;
               });
                 
                 
                 event.preventDefault();
                 
                var text = '¿Está seguro que desea cambiar el Network : ' + network_selected.comment + '?';
                
                bootbox.confirm(text, function(result) {
                    
                    if (result) {
                        
                        
                     
                        
                        
                        $('#btn-submit-move-network').hide(); 
                       
                        openModalPreloader("Cambiando Network ...");
                        
                        $.ajax({
                            type: 'POST',
                            dataType: "json",
                            url: '/ispbrain/IspControllerMikrotikDhcpTa/moveNetwork',
                            data:  JSON.stringify(send_data),
                            success: function(data){
                                
                                closeModalPreloader();
                                
                                $('#btn-submit-move-network').show(); 
                                
                                if(data.response){
                                    generateNoty('success', 'El Network se cambio correctamente');
                                }else{
                                    generateNoty('error', 'Error al intentar cambiar el Network');
                                    updateCountersTitle();
                                }
                         
                                table_tnetworks.ajax.reload();
                                updateSelectNetworks();
                                
                                $('.modal-move-tnetwork').modal('hide');
                                
                                updateCountersTitle();
                           
                            },
                            error: function(jqXHR) {
                                
                                if (jqXHR.status == 403) {
                                
                                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                                
                                	setTimeout(function() { 
                                	  window.location.href ="/ispbrain";
                                	}, 3000);
                                
                                } else {
                                	generateNoty('error', 'Error al cambiar el Network.');
                                }
                            }
                        });
                    }
                });
            });

            $('.modal-edit-tnetwork input#address').focusout(function() {

                var address = $(this).val();

                if (address != '') {

                    $.ajax({
                        url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikDhcpTa', 'action' => 'getRangeNetwork'])?>" ,
                        type: 'POST',
                        dataType: "json",
                        data: JSON.stringify({ address: address}),
                        success: function(data){

                            $('.modal-edit-tnetwork input#min-host').val(data.range.minHost);
                            $('.modal-edit-tnetwork input#max-host').val(data.range.maxHost);
                        },
                        error: function (jqXHR) {
                            
                            if (jqXHR.status == 403) {
                            
                            	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                            
                            	setTimeout(function() { 
                            	  window.location.href ="/ispbrain";
                            	}, 3000);
                            
                            } else {
                            	generateNoty('error', 'Error al intentar calcular el rango del network. Verifique el formato.');
                            }
                        }
                    });
                }
            });

            $('.modal-add-tnetwork input#address').focusout(function() {

                var address = $(this).val();

                if (address != '') {
                     $.ajax({
                        url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikDhcpTa', 'action' => 'getRangeNetwork'])?>" ,
                        type: 'POST',
                        dataType: "json",
                        data: JSON.stringify({ address: address}),
                        success: function(data){

                            $('.modal-add-tnetwork input#min-host').val(data.range.minHost);
                            $('.modal-add-tnetwork input#max-host').val(data.range.maxHost);
                        },
                        error: function (jqXHR) {
                            
                            if (jqXHR.status == 403) {
                            
                            	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                            
                            	setTimeout(function() { 
                            	  window.location.href ="/ispbrain";
                            	}, 3000);
                            
                            } else {
                                generateNoty('error', 'Error al intentar calcular el rango del network. Verifique el formato.');
                            }
                        }
                    });
                }
            });

            $('a[id="networks-tab"]').on('shown.bs.tab', function (e) {
                table_tnetworks.draw();
            });
        
        });

        function updateSelectNetworks(){
            
            if(table_tnetworks){
                
                 $('.modal-add-tnetwork #next-network-id').html('');
                 $('.modal-edit-tnetwork #next-network-id').html('');
                 
                 $('.modal-add-tplan #network-id').html('');
                 $('.modal-edit-tplan #network-id').html('');
                 $('.modal-move-tnetwork #network-to-id').html('');
                 
                //  $('.modal-add-ipfree #network-id').html('');
                 
                 $('.modal-add-tgateway #network-id').html('');
                 $('.modal-edit-tgateway #network-id').html('');
                 
                 
                 $('.modal-add-tnetwork #next-network-id').append($('<option>', { 
                        value: '',
                        text : '' 
                 }));
                 
                 $('.modal-edit-tnetwork #next-network-id').append($('<option>', { 
                        value: '',
                        text : '' 
                 }));
                 
                 $('.modal-add-tplan #network-id').append($('<option>', { 
                        value: '',
                        text : '' 
                 }));
                 
                  $('.modal-edit-tplan #network-id').append($('<option>', { 
                        value: '',
                        text : '' 
                 }));
                 
                 $('.modal-add-ipfree #network-id').append($('<option>', { 
                        value: '',
                        text : '' 
                 }));
                 
                 $('.modal-add-tgateway #network-id').append($('<option>', { 
                        value: '',
                        text : '' 
                 }))
                 
                 $('.modal-edit-tgateway #network-id').append($('<option>', { 
                        value: '',
                        text : '' 
                 }))
                 
                 $('.modal-move-tnetwork #network-to-id').append($('<option>', { 
                        value: '',
                        text : '' 
                 }))
                 
                 var networks_available = [];
                 
                $.each(table_tnetworks.rows().data(), function (i, networka) {
                    
                    var flag = false;
                    
                    $.each(table_tnetworks.rows().data(), function (i, networkb) {
                    
                       if(networka.id == networkb.next_pool_id){
                           flag = true;
                       }
                    });
                    
                    if(!flag){
                        networks_available.push(networka);
                    }
               
                });
                 
                 
                
                $.each(networks_available, function (i, network) {
                    
                    $('.modal-add-tnetwork #next-network-id').append($('<option>', { 
                        value: network.id,
                        text : network.comment 
                    }));
                    
                    $('.modal-edit-tnetwork #next-network-id').append($('<option>', { 
                        value: network.id,
                        text : network.comment 
                    }));
                    
                    $('.modal-add-tplan #network-id').append($('<option>', { 
                        value: network.id,
                        text : network.comment 
                    }));
                    
                    $('.modal-edit-tplan #network-id').append($('<option>', { 
                        value: network.id,
                        text : network.comment 
                    }));
                
                    
                    $('.modal-add-tgateway #network-id').append($('<option>', { 
                        value: network.id,
                        text : network.comment 
                    }));
                    
                    $('.modal-edit-tgateway #network-id').append($('<option>', { 
                        value: network.id,
                        text : network.comment 
                    }));
                    
                    $('.modal-move-tnetwork #network-to-id').append($('<option>', { 
                        value: network.id,
                        text : network.comment + ' (' + network.connections_amount  + ')'
                    }));
                
                
                });
                
            }
            
        }

        // Jquery draggable modals
        $('.modal-dialog').draggable({
            handle: ".modal-header"
        });

        //end tnetworks
    </script>

<?php $this->end(); ?>
