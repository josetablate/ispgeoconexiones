<?php $this->extend('/IspControllerMikrotikDhcpTa/ConfigViews/ip-excluded'); ?>

<?php $this->start('dhcp-server'); ?>
    
 <div id="btns-tools-dhcpServers">
        <div class="text-right btns-tools margin-bottom-5">
    
            <?php 
                echo $this->Html->link(
                    '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Agregar',
                    'class' => 'btn btn-default btn-add-dhcpServer',
                    'escape' => false
                    ]);
    
                 echo $this->Html->link(
                    '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Exportar tabla',
                    'class' => 'btn btn-default btn-export-dhcpServers ml-1',
                    'escape' => false
                    ]);
            ?>
    
        </div>
    </div>
    
    
    <div class="row justify-content-center mt-2">
        <div class="col-auto">
            <div class="card border-naranja">
              <div class="card-body text-secondary p-2">
                <p class="card-text text-naranja">Para que los cambios surtan efecto en el controlador, debe <a href="/ispbrain/IspControllerMikrotikDhcpTa/sync/<?=$controller->id?>"  class="font-weight-bold"  >sincronizar</a>.</p>
              </div>
            </div>
        </div>
    </div>
        
    
    <div class="row">
        <div class="col-8 mx-auto">
            
            <div class="card border-secondary mt-2">
                <div class="card-body p-1">
                    
                    <table class="table table-bordered table-hover" id="table-dhcpServers">
                        <thead>
                            <tr>
                                <th>Service Name</th>
                                <th>Interface</th>
                                <th>Address Pool</th>
                                <th>Disabled</th>
                                <th>Leases Time (DHCP)</th>
                                <th>Leases time</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="modal fade modal-add-dhcpServer" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="gridSystemModalLabel">Agregar DHCP Server</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
    
                            <?= $this->Form->create(null, ['id' => 'form-add-dhcpServer']) ?>
                                <fieldset>
                                    
                                    
                                    <div class="row">
                                        <div class="col-6">
                                            
                                              <?php 
                                             
                                                echo $this->Form->input('name', ['value' => 'DHCPServer',  'readonly' => false, 'required' => true]);
                                                echo $this->Form->input('interface', ['options' => $controller->interfaces, 'required' => true]);
                                                echo $this->Form->input('disabled', ['options' => [  'no' => 'No', 'yes' => 'Yes']]);
                                                
                                            ?>
                                                    
                                        </div>
                                        
                                         <div class="col-6">
                                            
                                              <?php 
                                              
                                                echo $this->Form->input('address_pool', ['options' => []]);
                                                
                                                echo $this->Form->input('leases_time', ['label' => 'Lease time (DHCP)', 'value' => '10m', 'required' => true]);
                                                echo '<p class="text-muted">Ejemplo: 1d4h3m2s</p>';
                                                echo $this->Form->input('leases_time_gral',  ['label' => 'Lease time', 'value' => '10m', 'required' => true]);
                                                echo '<p class="text-muted">Ejemplo: 1d4h3m2s</p>';
                                                
                                                echo $this->Form->input('controller_id', ['value' => $controller->id, 'type' => 'hidden']);
                                            ?>
                                                     
                                        </div>
                                    </div>
                                  
                                </fieldset>
                                
                                <?= $this->Form->button(__('Agregar', ['id' => 'btn-submit-add-dhcpServer', 'type' => 'button'])) ?>
                                
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
     <div class="modal fade modal-edit-dhcpServer" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="gridSystemModalLabel">Editar DHCP Server</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
    
                            <?= $this->Form->create(null, ['id' => 'form-edit-dhcpServer']) ?>
                                <fieldset>
                                    
                                     
                                    <div class="row">
                                        <div class="col-6">
                                            
                                             <?php 
                                     
                                                echo $this->Form->input('name', ['readonly' => false, 'required' => true]);
                                                echo $this->Form->input('interface', ['options' => $controller->interfaces, 'required' => true]);
                                                echo $this->Form->input('disabled', ['options' => [  'no' => 'No', 'yes' => 'Yes']]);
                                             
                                            ?>
                                            
                                        </div>
                                        
                                        <div class="col-6">
                                            
                                             <?php 
                                           
                                                echo $this->Form->input('address_pool', ['options' => []]);
                                                
                                                echo $this->Form->input('leases_time', ['label' => 'Lease time (DHCP)', 'value' => '10m', 'required' => true]);
                                                echo '<p class="text-muted">Ejemplo: 1d4h3m2s</p>';
                                                echo $this->Form->input('leases_time_gral' , ['label' => 'Lease time', 'value' => '10m', 'required' => true]);
                                                echo '<p class="text-muted">Ejemplo: 1d4h3m2s</p>';
                                                
                                                echo $this->Form->input('controller_id', ['type' => 'hidden']);
                                                echo $this->Form->input('id', ['type' => 'hidden']);
                                            ?>
                                            
                                        </div>
                                    </div>
                                            
                                    
                                    
                                   

                                </fieldset>
                                
                                <?= $this->Form->button(__('Guardar', ['id' => 'btn-submit-edit-dhcpServer', 'type' => 'button'])) ?>
                                
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    

    <!-- modal actions dhcpServers -->
    <?php
        $buttons = [];
        
        $buttons[] = [
            'id'   =>  'btn-edit-dhcpServer',
            'name' =>  'Editar',
            'icon' =>  'icon-pencil',
            'type' =>  'btn-default'
        ];

        $buttons[] = [
            'id'   =>  'btn-delete-dhcpServer',
            'name' =>  'Eliminar',
            'icon' =>  'icon-bin',
            'type' =>  'btn-danger'
        ];
        

        echo $this->element('actions', ['modal'=> 'modal-actions-dhcpServers', 'title' => 'Acciones', 'buttons' => $buttons ]);
    ?>


    <script type="text/javascript">
    
       var dhcpServer_selected = null;
       var table_dhcpServers = null; 

        $(document).ready(function () {
           
            $('#table-dhcpServers').removeClass('display');
            
            $('#btns-tools-dhcpServers').hide();

    		table_dhcpServers = $('#table-dhcpServers').DataTable({
    		    "order": [[ 0, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/IspControllerMikrotikDhcpTa/get_dhcp_servers_by_controller.json",
                    "dataSrc": "dhcpServers",
                    "data": function ( d ) {
                        return $.extend( {}, d, {
                            "controller_id": controller_id
                        });
                    },
                    "error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
    		        { 
                        "data": "name"
                    },
                    { 
                        "data": "interface"
                    },
                    { 
                        "data": "address_pool_name"
                    },
                    { 
                        "data": "disabled"
                    },
                    { 
                        "data": "leases_time"
                    },
                    { 
                        "data": "leases_time_gral"
                    },
                ],
    		    "columnDefs": [
    		          
                ],
                "createdRow" : function( row, data, index ) {
                    row.id = data.id;
                    if(!data.api_id){
                        $(row).addClass('no-sync');
                    }
                    
                    if(table_dhcpServers && table_dhcpServers.rows().count() > 0){
                        $('.btn-add-dhcpServer').hide();
                    }else{
                        $('.btn-add-dhcpServer').show();
                    }
                },
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-dhcpServers_wrapper .tools').append($('#btns-tools-dhcpServers').contents());
    		
    		$('#btns-tools-dhcpServers').show();

            $(".btn-export-dhcpServers").click(function(){

                bootbox.confirm('Se descargará un archivo Excel', function(result) {
                    if (result) {
                        $('#table-dhcpServers').tableExport({tableName: 'DHCP Servers', type:'excel', escape:'false'});
                    }
                });
            });

            $('#table-dhcpServers tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length ){

                    table_dhcpServers.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    
                    dhcpServer_selected = table_dhcpServers.row( this ).data();

                    $('.modal-actions-dhcpServers').modal('show');
                }
            });

            $('.btn-add-dhcpServer').click(function() {
                
                clearFormAddDHCPServer();
                
                $('.modal-add-dhcpServer').modal('show');
            });
            
            $('#form-add-dhcpServer').submit(function(event){
               
                event.preventDefault();
                 
                var send_data = {};
                 
                var form_data = $(this).serializeArray();
                
                $.each(form_data, function(i, val){
                    send_data[val.name] = val.value;
                });
                
                $('#btn-submit-add-dhcpServer').hide(); 
           
                openModalPreloader("Agregando (dhcp Server) ...");
                
                $('.modal-actions-dhcpServers').modal('hide');
                
                $.ajax({
                        type: 'POST',
                        dataType: "json",
                        url: '/ispbrain/IspControllerMikrotikDhcpTa/addDhcpServer',
                        data:  JSON.stringify(send_data),
                        success: function(data){
                            
                           closeModalPreloader();
                            
                            $('#btn-submit-add-dhcpServer').show(); 
                            
                            if(data.response){
                                generateNoty('success', 'El (dhcp Server) se agrego correctamente');
                            }else{
                                generateNoty('error', 'Error al intentar agregar el (dhcp Server)');
                            }
                            
                            table_dhcpServers.ajax.reload();
                            $('.modal-add-dhcpServer').modal('hide');
                            
                            updateCountersTitle();
                        },
                        error: function(jqXHR) {
                            if (jqXHR.status == 403) {
                            
                            	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                            
                            	setTimeout(function() { 
                            	  window.location.href ="/ispbrain";
                            	}, 3000);
                            
                            } else {
                            	generateNoty('error', 'Error al agregar el (dhcp Server)');
                            }
                        }
                    });
                
            });

            $('#btn-edit-dhcpServer').click(function(){
                
                $('.modal-actions-dhcpServers').modal('hide');
             
                $('.modal-edit-dhcpServer input#id').val(dhcpServer_selected.id);
                $('.modal-edit-dhcpServer input#controller-id').val(dhcpServer_selected.controller_id);
                $('.modal-edit-dhcpServer input#name').val(dhcpServer_selected.name);
                $('.modal-edit-dhcpServer input#interface').val(dhcpServer_selected.interface);
                $('.modal-edit-dhcpServer input#disabled').val(dhcpServer_selected.disabled);
                $('.modal-edit-dhcpServer select#address-pool').val(dhcpServer_selected.address_pool);
                
                $('.modal-edit-dhcpServer input#leases-time').val(dhcpServer_selected.leases_time);
                $('.modal-edit-dhcpServer input#leases-time-gral').val(dhcpServer_selected.leases_time_gral);
                
                $('.modal-edit-dhcpServer').modal('show');
                
            });
            
             $('#form-edit-dhcpServer').submit(function(){
                
                 event.preventDefault();
                 
                var send_data = {};
                 
                var form_data = $(this).serializeArray();
                
                $.each(form_data, function(i, val){
                    send_data[val.name] = val.value;
                });
                
                $('#btn-submit-edit-dhcpServer').hide(); 
                
                $('.modal-edit-dhcpServer').modal('hide');
               
                openModalPreloader("Editando (dhcp Server) ...");
                
                $('.modal-actions-dhcpServers').modal('hide');
                
                $.ajax({
                        type: 'POST',
                        dataType: "json",
                        url: '/ispbrain/IspControllerMikrotikDhcpTa/editDhcpServer',
                        data:  JSON.stringify(send_data),
                        success: function(data){
                            
                            closeModalPreloader();
                            
                            $('#btn-submit-edit-network').show(); 
                            
                            if(data.response){
                                generateNoty('success', 'El (dhcp Server) se edito correctamente');
                            }else{
                                generateNoty('error', 'Error al intentar editar el (dhcp Server)');
                            }
                     
                            table_dhcpServers.ajax.reload();
                            
                            $('#btn-submit-edit-dhcpServer').show(); 
                            
                            updateCountersTitle();
                       
                        },
                        error: function(jqXHR) {
                            if (jqXHR.status == 403) {
                            
                            	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                            
                            	setTimeout(function() { 
                            	  window.location.href ="/ispbrain";
                            	}, 3000);
                            
                            } else {
                            	generateNoty('error', 'Error al editar el (dhcp Server).');
                            }
                        }
                    });
                
            });

            $('#btn-delete-dhcpServer').click(function() {

                var text = '¿Está seguro que desea eliminar el (dhcp Server): ' + dhcpServer_selected.name + '?';

                bootbox.confirm(text, function(result) {
                    if (result) {
                    
                         openModalPreloader("Eliminado (dhcp Server) ...");
                         
                         $('.modal-actions-dhcpServers').modal('hide');
                        
                        $.ajax({
                            type: 'POST',
                            dataType: "json",
                            url: '/ispbrain/IspControllerMikrotikDhcpTa/deleteDhcpServer',
                            data:  JSON.stringify({id: dhcpServer_selected.id}),
                            success: function(data){
                                
                                closeModalPreloader();
                                
                                if(data.response){
                                    generateNoty('success', 'El (dhcp Server)se elimino correctamente');
                                    
                                    $('.btn-add-dhcpServer').show();
                                }else{
                                    generateNoty('error', 'Error al intentar eliminar el (dhcp Server)');
                                }
                                
                                table_dhcpServers.ajax.reload();
                                table_dhcpServers.draw();
                                updateCountersTitle();
                            },
                            error: function(jqXHR) {
                                if (jqXHR.status == 403) {
                                
                                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                                
                                	setTimeout(function() { 
                                	  window.location.href ="/ispbrain";
                                	}, 3000);
                                
                                } else {
                                	generateNoty('error', 'Error al eliminar (dhcp Server).');
                                }
                            }
                        });
                    }
                });
            });

            $('a[id="dhcp-server-tab"]').on('shown.bs.tab', function (e) {
                table_dhcpServers.ajax.reload();
     
            });
            
            setTimeout(function(){
                table_dhcpServers.ajax.reload();
            },100)
        });
     
        
        function clearFormAddDHCPServer(){

            $('#form-add-dhcpServer #service-name').val('ISPBrain-DHCPServer');
        }
        
        
        // Jquery draggable modals
        $('.modal-dialog').draggable({
            handle: ".modal-header"
        });

        //end dhcpServers
    </script>
   
<?php $this->end(); ?>
