<?php $this->extend('/IspControllerMikrotikDhcpTa/ConfigViews/pools'); ?>

<?php $this->start('profiles'); ?>


<style type="text/css">
    
    .w-60{
        width: 60%;
    }
    
</style>


   <div id="btns-tools-tprofiles">
        <div class="text-right btns-tools margin-bottom-5">

            <?php 
                echo $this->Html->link(
                    '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Agregar',
                    'class' => 'btn btn-default btn-add-tprofile',
                    'escape' => false
                    ]);

                 echo $this->Html->link(
                    '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Exportar tabla',
                    'class' => 'btn btn-default btn-export-tprofiles ml-1',
                    'escape' => false
                    ]);
            ?>

        </div>
    </div>
    
    <div class="row justify-content-center mt-2">
        <div class="col-auto">
            <div class="card border-naranja">
              <div class="card-body text-secondary p-2">
                <p class="card-text text-naranja">Para que los cambios surtan efecto en el controlador, debe <a href="/ispbrain/IspControllerMikrotikDhcpTa/sync/<?=$controller->id?>"  class="font-weight-bold"  >sincronizar</a>.</p>
              </div>
            </div>
        </div>
    </div>

   <div class="row">
        <div class="col-12">
            
            <div class="card border-secondary mt-2">
                <div class="card-body p-1">
                    
                     <table class="table table-bordered table-hover" id="table-tprofiles">
                        <thead>
                            <tr>
                                <th>Conexiones</th>
                                <th>Nombre</th>
                                <th>Max Limit</th>
                                <th>Limit At</th>
                                <th>Burst Limit</th>
                                <th>Burst Threshold</th>
                                <th>Burst Time</th>
                                <th>Priority</th>
                                <th>Queue Type (down)</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                    </table>

                    
                </div>
            </div>
        </div>
    </div>

   
    <!-- modal actions tprofiles -->
    <?php
        $buttons = [];

         $buttons[] = [
            'id'   =>  'btn-edit-tprofile',
            'name' =>  'Editar',
            'icon' =>  'icon-pencil2',
            'type' =>  'btn-default'
        ];

        $buttons[] = [
            'id'   =>  'btn-delete-tprofile',
            'name' =>  'Eliminar',
            'icon' =>  'icon-bin',
            'type' =>  'btn-danger'
        ];

        echo $this->element('actions', ['modal'=> 'modal-actions-tprofiles', 'title' => 'Acciones', 'buttons' => $buttons ]);
    ?>

    <!-- modal agregar tprofiles -->
    <div class="modal fade modal-add-tprofile" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header pb-1">
                    <h4 class="modal-title" id="gridSystemModalLabel">Agregar Perfil</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body mt-1">
                    <div class="row">
                        <div class="col-xl-12">

                            <?= $this->Form->create(null, ['id' => 'form-add-profile']) ?>
                                <fieldset>
                                    
                                    <div class="row">
                                        
                                        <div class="col-xl-4 pl-2 pr-1">
                                            
                                            
                                            <div class="row">
                                                <div class="col-xl-12">
                                                    <?php
                                                    echo $this->Form->input('name', ['label' => 'Nombre', 'value' => 'profile1', 'required' => true]);
                                                    ?>
                                                </div>
                                                <div class="col-xl-12">
                                                    <?php
                                                     echo $this->Form->input('up_queue_type', ['label' => 'Queue Type (up)', 'options' => $queue_types_array,  'value' => $controller->queue_default, 'required' => true]);
                                                    ?>
                                                </div>
                                                <div class="col-xl-12">
                                                    <?php
                                                     echo $this->Form->input('down_queue_type', ['label' => 'Queue Type (down)', 'options' => $queue_types_array,  'value' => $controller->queue_default, 'required' => true]);
                                                    ?>
                                                </div>
                                                <div class="col-xl-12">
                                                    <?php
                                                    //  echo $this->Form->input('rate_limit_string', ['label' => 'Rate limit', 'readonly' => true]);
                                                     echo $this->Form->input('controller_id', ['type' => 'hidden', 'value' => $controller->id]);
                                                    ?>
                                                </div>
                                                
                                            </div>
                                            
                                        </div>
                                        
                                         <div class="col-xl-8 pl-0 pr-2">
                                             
                                            <label class="control-label mr-2">
                                                Limites (Kbytes/s)
                                                <a href="javascript:void(0)" onClick="openModalHelp('Valores numericos. Los unidad en Kbytes/s')"><i class="fas fa-question-circle"></i></a>
                                            </label>
                                             
                                             
                              
                                            <div class="input-group number required">
                                                <label class="input-group-text w-60 text-success" for="up">max limit up</label>
                                                <input type="number" name="up" min="0" step="1" class="field-value form-control" required="required" id="up" value="">
                                            </div>
                                            <div class="input-group number required">
                                                <label class="input-group-text w-60 text-success" for="down">max limit down</label>
                                                <input type="number" name="down" min="0" step="1" class="field-value form-control" required="required" id="down" value="">
                                            </div>
                                            
                                            <div class="input-group number">
                                                <label class="input-group-text w-60 text-warning" for="up-at-limit">limit at up </label>
                                                <input type="number" name="up_at_limit" min="0" step="1" class="field-value form-control" id="up-at-limit" value="">
                                            </div>
                                            <div class="input-group number">
                                                <label class="input-group-text w-60 text-warning" for="down-at-limit">limit at down</label>
                                                <input type="number" name="down_at_limit" min="0" step="1" class="field-value form-control" id="down-at-limit" value="">
                                            </div>
                                            <div class="input-group number">
                                                <label class="input-group-text w-60 text-primary" for="up-burst">burst up</label>
                                                <input type="number" name="up_burst" min="0" step="1" class="field-value form-control" id="up-burst" value="">
                                            </div>
                                            <div class="input-group number">
                                                <label class="input-group-text w-60 text-primary" for="down-burst">burst down</label>
                                                <input type="number" name="down_burst" min="0" step="1" class="field-value form-control" id="down-burst" value="">
                                            </div>
                                            <div class="input-group number">
                                                <label class="input-group-text w-60 text-primary" for="up-threshold">burst threshold up</label>
                                                <input type="number" name="up_threshold" min="0" step="1" class="field-value form-control" id="up-threshold" value="">
                                            </div>
                                            <div class="input-group number">
                                                <label class="input-group-text w-60 text-primary" for="down-threshold">burst threshold down</label>
                                                <input type="number" name="down_threshold" min="0" step="1" class="field-value form-control" id="down-threshold" value="">
                                            </div>
                                            <div class="input-group text">
                                                <label class="input-group-text w-60 text-primary" for="up-time">burst time up</label>
                                                <input type="text" name="up_time" min="0" step="1" class="field-value form-control" id="up-time" value="">
                                            </div>
                                            <div class="input-group text">
                                                <label class="input-group-text w-60 text-primary" for="down-time">burst time down</label>
                                                <input type="text" name="down_time" min="0" step="1" class="field-value form-control" id="down-time" value="">
                                            </div>
                                            <div class="input-group number">
                                                <label class="input-group-text w-60 text-secondary" for="priority">priority</label>
                                                <input type="number" name="priority" min="0" step="1" class="field-value form-control" id="priority" value="">
                                            </div>
                                           
                                        </div>
                                    </div>

                                </fieldset>
                                <?= $this->Form->button(__('Agregar', ['id' => 'btn-submit-add-profile'])) ?>
                            <?= $this->Form->end() ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal edit tprofiles -->
    <div class="modal fade modal-edit-tprofile" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="gridSystemModalLabel">Editar Perfil</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">

                            <?= $this->Form->create(null, ['id' => 'form-edit-profile']) ?>
                                
                                <fieldset>
                                    
                                     <div class="row">
                                        
                                        <div class="col-xl-4 pl-2 pr-1">
                                            
                                            
                                            <div class="row">
                                                <div class="col-xl-12">
                                                    <?php
                                                    echo $this->Form->input('name', ['label' => 'Nombre', 'value' => 'profile1', 'required' => true]);
                                                    ?>
                                                </div>
                                                <div class="col-xl-12">
                                                    <?php
                                                     echo $this->Form->input('up_queue_type', ['label' => 'Queue Type (up)', 'options' => $queue_types_array, 'required' => true]);
                                                    ?>
                                                </div>
                                                <div class="col-xl-12">
                                                    <?php
                                                     echo $this->Form->input('down_queue_type', ['label' => 'Queue Type (down)', 'options' => $queue_types_array, 'required' => true]);
                                                    ?>
                                                </div>
                                                <div class="col-xl-12">
                                                    <?php
                                                    //  echo $this->Form->input('rate_limit_string', ['label' => 'Rate limit', 'readonly' => true]);
                                                     echo $this->Form->input('controller_id', ['type' => 'hidden', 'value' => $controller->id]);
                                                     echo $this->Form->input('id', ['type' => 'hidden']);
                                                    ?>
                                                </div>
                                                
                                            </div>
                                            
                                        </div>
                                        
                                         <div class="col-xl-8 pl-0 pr-2">
                                             
                                            <label class="control-label mr-2">
                                                Limites (Kbytes/s)
                                                <a href="javascript:void(0)" onClick="openModalHelp('Valores numericos. Los unidad en Kbytes/s')"><i class="fas fa-question-circle"></i></a>
                                            </label>
                                             
                              
                                           <div class="input-group number required">
                                                <label class="input-group-text w-60 text-success" for="up">max limit up</label>
                                                <input type="number" name="up" min="0" step="1" class="field-value form-control" required="required" id="up" value="">
                                            </div>
                                            <div class="input-group number required">
                                                <label class="input-group-text w-60 text-success" for="down">max limit down</label>
                                                <input type="number" name="down" min="0" step="1" class="field-value form-control" required="required" id="down" value="">
                                            </div>
                                            
                                            <div class="input-group tenumberxt">
                                                <label class="input-group-text w-60 text-warning" for="up-at-limit">limit at up </label>
                                                <input type="number" name="up_at_limit" min="0" step="1" class="field-value form-control" id="up-at-limit" value="">
                                            </div>
                                            <div class="input-group number">
                                                <label class="input-group-text w-60 text-warning" for="down-at-limit">limit at down</label>
                                                <input type="number" name="down_at_limit" min="0" step="1" class="field-value form-control" id="down-at-limit" value="">
                                            </div>
                                            <div class="input-group number">
                                                <label class="input-group-text w-60 text-primary" for="up-burst">burst up</label>
                                                <input type="number" name="up_burst" min="0" step="1" class="field-value form-control" id="up-burst" value="">
                                            </div>
                                            <div class="input-group number">
                                                <label class="input-group-text w-60 text-primary" for="down-burst">burst down</label>
                                                <input type="number" name="down_burst" min="0" step="1" class="field-value form-control" id="down-burst" value="">
                                            </div>
                                            <div class="input-group number">
                                                <label class="input-group-text w-60 text-primary" for="up-threshold">burst threshold up</label>
                                                <input type="number" name="up_threshold" min="0" step="1" class="field-value form-control" id="up-threshold" value="">
                                            </div>
                                            <div class="input-group number">
                                                <label class="input-group-text w-60 text-primary" for="down-threshold">burst threshold down</label>
                                                <input type="number" name="down_threshold" min="0" step="1" class="field-value form-control" id="down-threshold" value="">
                                            </div>
                                            <div class="input-group text">
                                                <label class="input-group-text w-60 text-primary" for="up-time">burst time up</label>
                                                <input type="text" name="up_time" min="0" step="1" class="field-value form-control" id="up-time" value="">
                                            </div>
                                            <div class="input-group text">
                                                <label class="input-group-text w-60 text-primary" for="down-time">burst time down</label>
                                                <input type="text" name="down_time" min="0" step="1" class="field-value form-control" id="down-time" value="">
                                            </div>
                                            <div class="input-group number">
                                                <label class="input-group-text w-60 text-secondary" for="priority">priority</label>
                                                <input type="number" name="priority" min="0" step="1" class="field-value form-control" id="priority" value="">
                                            </div>
                                           
                                        </div>
                                    </div>
                                  
                                </fieldset>
                                <?= $this->Form->button(__('Guardar'), ['id' => 'btn-submit-edit-profile']) ?>
                            <?= $this->Form->end() ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        //tprofiles
        
        var profile_selected = null;
        var table_tprofiles = null;

        $(document).ready(function () {
            

            $('#table-tprofiles').removeClass('display');

    		table_tprofiles = $('#table-tprofiles').on( 'draw.dt', function () {
    		    
    		    updateSelectProfiles();
                    
                }).DataTable({
                    
    		    "order": [[ 1, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ajax": {
                    "url": "/ispbrain/IspControllerMikrotikDhcpTa/get_profiles_by_controller.json",
                    "dataSrc": "profiles",
                    "data": function ( d ) {
                        return $.extend( {}, d, {
                            "controller_id": controller_id
                        });
                    },
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                
                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "connections_amount"
                    },
    		        { 
                        "data": "name"
                    },
                    { 
                        "data": "up",
                        "render": function ( data, type, row ) {
                            if(data){
                                return data+'/'+row.down;
                            }
                            return '';
                        }
                    },
                    { 
                        "data": "up_at_limit",
                        "render": function ( data, type, row ) {
                            if(data){
                                return data+'/'+row.down_at_limit;
                            }
                            return '';
                        }
                    },
                    { 
                        "data": "up_burst",
                        "render": function ( data, type, row ) {
                            if(data){
                                return data+'/'+row.down_burst;
                            }
                            return '';
                        }
                    },
                    { 
                        "data": "up_threshold",
                        "render": function ( data, type, row ) {
                            if(data){
                                return data+'/'+row.down_threshold;
                            }
                            return '';
                        }
                    },
                    { 
                        "data": "up_time",
                        "render": function ( data, type, row ) {
                            if(data){
                                return data+'/'+row.down_time;
                            }
                            return '';
                        }
                    },
                    { 
                        "data": "priority",
                        "render": function ( data, type, row ) {
                            if(data){
                                return data+'/'+data;
                            }
                            return '';
                        }
                    },
                    { 
                        "data": "up_queue_type",
                        "render": function ( data, type, row ) {
                            if(data){
                                return data+'/'+row.down_queue_type;
                            }
                            return '';
                        }
                    },
                ],
    		    "columnDefs": [
    		          
                ],
                "createdRow" : function( row, data, index ) {
                    row.id = data.id;
                },
              
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
            

            $('#table-tprofiles_wrapper .tools').append($('#btns-tools-tprofiles').contents());
    		
    		$('#btns-tprofiles-tprofiles').show();

            $(".btn-export-tprofiles").click(function(){

                bootbox.confirm('Se descargará un archivo Excel', function(result) {
                    if (result) {
                        $('#table-tprofiles').tableExport({tableName: 'Profiles', type:'excel', escape:'false'});
                    }
                });
            });

            $('#table-tprofiles tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {

                    table_tprofiles.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    profile_selected = table_tprofiles.row( this ).data();

                    $('.modal-actions-tprofiles').modal('show');
                }
            });

            $('.btn-add-tprofile').click(function() {
                
                clearFormProfile('.modal-add-tprofile');
                
                $('.modal-add-tprofile').modal('show');
            });
            
            $('#form-add-profile').submit(function(){
                
                 event.preventDefault();
                 
                var send_data = {};
                 
                var form_data = $(this).serializeArray();
                
                $.each(form_data, function(i, val){
                    send_data[val.name] = val.value;
                });

                if(validate(send_data)){
                    
                     $('#btn-submit-add-profile').hide(); 
          
                    openModalPreloader("Agregando Profile ...");
                    
                    $.ajax({
                        type: 'POST',
                        dataType: "json",
                        url: '/ispbrain/IspControllerMikrotikDhcpTa/addProfile',
                        data:  JSON.stringify(send_data),
                        success: function(data){
                            
                            $('#btn-submit-add-profile').show(); 
                            closeModalPreloader();
                            
                            
                            if(data.response){
                                generateNoty('success', 'Perfil agregado Correctamente');
                            }else{
                                generateNoty('error', 'Error al intentar agregar el Perfil');
                            }
                          
                            table_tprofiles.ajax.reload();
                            updateSelectProfiles();
                            $('.modal-add-tprofile').modal('hide');
                            
                        },
                        error: function(jqXHR) {
                            if (jqXHR.status == 403) {
                            
                            	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                            
                            	setTimeout(function() { 
                            	  window.location.href ="/ispbrain";
                            	}, 3000);
                            
                            } else {
                            	generateNoty('error', 'Error al intentar agregar el Perfil.');
                            }
                        }
                    });
                        
                }
                
            });

            $('#btn-delete-tprofile').click(function() {
          
                var text = '¿Está seguro que desea eliminar el Profile : ' + profile_selected.name + '?';
                
                bootbox.confirm(text, function(result) {
                    if (result) {
          
                        openModalPreloader("Eliminado Profile ...");
                        
                        $.ajax({
                            type: 'POST',
                            dataType: "json",
                            url: '/ispbrain/IspControllerMikrotikDhcpTa/deleteProfile',
                            data:  JSON.stringify({id: profile_selected.id}),
                            success: function(data){
                                
                                closeModalPreloader();
                                
                                
                                if(data.response){
                                    generateNoty('success', 'Perfil eliminado Correctamente');
                                }else{
                                    generateNoty('error', 'Error al intentar eliminar el Perfil');
                                }
                                
                                 table_tprofiles.ajax.reload();
                                 updateSelectPools();
                              
                                $('.modal-actions-tprofiles').modal('hide');
                            },
                            error: function(jqXHR) {
                                if (jqXHR.status == 403) {
                                
                                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                                
                                	setTimeout(function() { 
                                	  window.location.href ="/ispbrain";
                                	}, 3000);
                                
                                } else {
                                	generateNoty('error', 'Error al eliminar el Perfil.');
                                }
                            }
                        });
                    }
                });
                
            });

            $('#btn-edit-tprofile').click(function(){
                
                $('.modal-actions-tprofiles').modal('hide');
                
                $('.modal-edit-tprofile input#id').val(profile_selected.id);
                $('.modal-edit-tprofile input#name').val(profile_selected.name);
                
                $('.modal-edit-tprofile select#up-queue-type').val(profile_selected.up_queue_type);
                $('.modal-edit-tprofile select#down-queue-type').val(profile_selected.down_queue_type);
                
                $('.modal-edit-tprofile input#down').val(profile_selected.down);
                $('.modal-edit-tprofile input#up').val(profile_selected.up);
                $('.modal-edit-tprofile input#down-burst').val(profile_selected.down_burst);
                $('.modal-edit-tprofile input#up-burst').val(profile_selected.up_burst);
                $('.modal-edit-tprofile input#down-threshold').val(profile_selected.down_threshold);
                $('.modal-edit-tprofile input#up-threshold').val(profile_selected.up_threshold);
                $('.modal-edit-tprofile input#down-time').val(profile_selected.down_time);
                $('.modal-edit-tprofile input#up-time').val(profile_selected.up_time);
                $('.modal-edit-tprofile input#priority').val(profile_selected.priority);
                $('.modal-edit-tprofile input#down-at-limit').val(profile_selected.down_at_limit);
                $('.modal-edit-tprofile input#up-at-limit').val(profile_selected.up_at_limit);
                
                $('.modal-edit-tprofile').modal('show');
                
            });
            
            $('#form-edit-profile').submit(function(){
                
                 event.preventDefault();
                 
                var send_data = {};
                 
                var form_data = $(this).serializeArray();
                
                $.each(form_data, function(i, val){
                    send_data[val.name] = val.value;
                });
                
                if(validate(send_data)){
                    
                     $('#btn-submit-edit-profile').hide(); 
             
                    openModalPreloader("Editando Profile ...");
                    
                    $.ajax({
                        type: 'POST',
                        dataType: "json",
                        url: '/ispbrain/IspControllerMikrotikDhcpTa/editProfile',
                        data:  JSON.stringify(send_data),
                        success: function(data){
                            
                            $('#btn-submit-edit-profile').show(); 
                            closeModalPreloader();
                            
                            if(data.response){
                                generateNoty('success', 'Perfil Editado Correctamente');
                            }else{
                                generateNoty('error', 'Error al intentar Editar Perfil');
                            }
                            
                            table_tprofiles.ajax.reload();
                            updateSelectPools();
                            $('.modal-edit-tprofile').modal('hide');
                        },
                        error: function(jqXHR) {
                            if (jqXHR.status == 403) {
                            
                            	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                            
                            	setTimeout(function() { 
                            	  window.location.href ="/ispbrain";
                            	}, 3000);
                            
                            } else {
                            	generateNoty('error', 'Error al editar el Perfil.');
                            }
                        }
                    });
                        
                }
               
                
            });

            $('a[id="profiles-tab"]').on('shown.bs.tab', function (e) {
                table_tprofiles.ajax.reload();
                table_tprofiles.draw();
            });
          
        });
        
        function validate(profile){
            
            var max_limit_up = parseInt(profile.up);
            var max_limit_down = parseInt(profile.down);
            
            if(parseInt(profile.up_at_limit) > max_limit_up){
                generateNoty('warning', 'Limit at up > limit up');
                return false;
            }
            
            if(parseInt(profile.down_at_limit) > max_limit_down){
                generateNoty('warning', 'Limit at down > max limit down');
                return false;
            }
            
            
            if(parseInt(profile.up_burst) > 0 && parseInt(profile.up_burst) < max_limit_up){
                generateNoty('warning', 'Burst limit up < Max limit up');
                return false;
            }
            
            if(parseInt(profile.down_burst) > 0 && parseInt(profile.down_burst) < max_limit_down){
                generateNoty('warning', 'Burst limit down < Max limit down');
                return false;
            }
            
            
            if(parseInt(profile.up_threshold) >= max_limit_up){
                generateNoty('warning', 'Burst threshold up >= Max limit up');
                return false;
            }
            
            if(parseInt(profile.down_threshold) >= max_limit_down){
                generateNoty('warning', 'Burst threshold down >= Max limit down');
                return false;
            }
            
            
            return true;
        }
        
     
        
        function updateSelectProfiles(){
            
            if(table_tprofiles){
             
                 $('.modal-add-tplan #profile-id').html('');
             
                 $('.modal-add-tplan #profile-id').append($('<option>', { 
                        value: '',
                        text : '' 
                 }));
                
                $.each(table_tprofiles.rows().data(), function (i, profile) {
                    
                    $('.modal-add-tplan #profile-id').append($('<option>', { 
                            value: profile.id,
                            text : profile.name 
                     }));
                
                });
                
                
                $('.modal-edit-tplan #profile-id').html('');
             
                $('.modal-edit-tplan #profile-id').append($('<option>', { 
                        value: '',
                        text : '' 
                }));
                
                $.each(table_tprofiles.rows().data(), function (i, profile) {
                    
                    $('.modal-edit-tplan #profile-id').append($('<option>', { 
                            value: profile.id,
                            text : profile.name 
                     }));
                
                });
                
            }
            
        }
        
        function clearFormProfile(modal){
            
            // console.log('clearFormProfile');
            
            $(modal + ' input#name').val('profile1');
            $(modal + ' input#rate-limit-string').val('');
            $(modal + ' .field-value').val('');
        }
        
        
        // Jquery draggable modals
        $('.modal-dialog').draggable({
            handle: ".modal-header"
        });

        //end tprofiles
    </script>
  
<?php $this->end(); ?>
