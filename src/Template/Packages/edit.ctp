

 <div class="row">
    <div class="col-xl-3">
        <div class="float-right">
            
            <?=
                $this->Html->link(
                    '<span class="glyphicon icon-bin" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'class' => 'btn btn-default', 
                    'title' => 'Eliminar Paquete',
                    'role' => 'button',
                    'id' =>  'btn-delete',
                    'data-id' => $package->id,
                    'data-text' =>  __('¿Está Seguro que desea eliminar el Paquete <strong>{0}</strong>?', $package->name),
                    'escape' => false
                ]);
            ?>
        </div>
    </div>    
</div>

<div class="row">
    <div class="col-xl-3">

        <?= $this->Form->create($package,  ['class' => 'form-load']) ?>
        <fieldset>

            <?php
                echo $this->Form->input('name', ['label' => 'Nombre']);
                echo $this->Form->input('description',  ['label' => 'Descripción']);
                echo $this->Form->input('price',  ['label' => 'Precio Final']);
                echo $this->Form->input('aliquot', ['label' => 'Alícuota', 'options' => $alicuotas_types]);
                echo $this->Form->input('account_code',  ['label' => 'Cuenta', 'required' => false]);
                echo $this->Form->input('enabled', ['label' => 'Habilitar/Deshabilitar' ]);
            ?>


        </fieldset>
         <?= $this->Html->link(__('Cancelar'),["controller" => "Packages", "action" => "index"], ['class' => 'btn btn-default']) ?>
        <?= $this->Form->button(__('Guardar')) ?>
        <?= $this->Form->end() ?>
    </div>
</div>

<script type="text/javascript">


      if(!sessionPHP.paraments.accountant.account_enabled){
            $('#account-code').closest('div').hide();
        }

    $('#btn-delete').click(function() {

        var text = $(this).data('text');
        var id  = $(this).data('id');

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/packages/delete/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#replacer').submit();
            }
        });

    });
 

</script>
