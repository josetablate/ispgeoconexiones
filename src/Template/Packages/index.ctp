 <div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5" >

        <?php

            echo $this->Html->link(
                '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                ['controller' => 'packages', 'action' => 'add'],
                [
                'title' => 'Agregar una nueva paquete',
                'class' => 'btn btn-default btn-add',
                'escape' => false
                ]);

             echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export ml-1',
                'escape' => false
                ]);
        ?>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-hover" id="table-packages">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Precio ($)</th>
                    <th>Alícuota</th>
                    <th>Cuenta</th>
                    <th>Creado</th>
                    <th>Modificado</th>
                    <th>Activo</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-info',
        'name' =>  'Infomación',
        'icon' =>  'icon-eye',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-edit',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-packages', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<script type="text/javascript">

    var table_packages = null;
    var package_selected = null;

    $(document).ready(function () {

        $('#table-packages').removeClass('display');
        
         $('#btns-tools').hide();

        var hide_column = [];
        
        if(!sessionPHP.paraments.accountant.account_enabled){
            hide_column = [4];
        }

		table_packages = $('#table-packages').DataTable({
		    "order": [[ 4, 'desc' ]],
		    "autoWidth": true,
		    "scrollY": '350px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "sWrapper":  "dataTables_wrapper dt-bootstrap4",
            "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/packages/index.json",
                "dataSrc": "packages",
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
            				  window.location.href ="/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "columns": [
                { "data": "name" },
                { "data": "description" },
                {
                    "data": "price",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' )
                },
                { 
                    "data": "aliquot",
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.alicuotas_types[data];
                    }
                
                },
                {
                    "data": "account",
                    "render": function ( data, type, row ) {
                        if(data){
                            return data.code + ' ' + data.name;;
                        }
                        return '';
                    }
                },
                {
                    "data": "created",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/'+ created[1] + '/' + created[0];
                        }
                    }
                },
                {
                    "data": "modified",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var modified = data.split('T')[0];
                            modified = modified.split('-');
                            return modified[2] + '/'+ modified[1] + '/' + modified[0];
                        }
                    }
                },
                {
                    "data": "enabled",
                    "render": function ( data, type, row ) {
                        var enabled = '<i class="fa fa-times red" aria-hidden="true"></i>';
                        if (data) {
                            enabled = '<i class="fa fa-check green" aria-hidden="true"></i>';
                        }
                        return enabled;
                    }
                },
            ],
            "columnDefs": [
                { "type": "numeric-comma", "targets": 2 },
                { 
                    "visible": false, targets: hide_column
                 },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            "dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-4'f><'col-xl-2 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
		});

	    $('#table-packages_wrapper .tools').append($('#btns-tools').contents());

	    $('#btns-tools').show();
    });

    $(".btn-export").click(function(){

        bootbox.confirm('Se descargará un archivo Excel', function(result) {
            if (result) {
                $('#table-packages').tableExport({tableName: 'Paquetes', type:'excel', escape:'false', columnNumber: [2]});
            }
        });
    });

    $('#table-packages tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_packages.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            package_selected = table_packages.row( this ).data();

            $('.modal-packages').modal('show');
        }
    });

    $('#btn-delete').click(function() {

        var text = "¿Está Seguro que desea eliminar el Paquete?";
        var id  = package_selected.id;

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/packages/delete/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#replacer').submit();
            }
        });
    });

    $('a#btn-info').click(function(){
      var action = '/ispbrain/packages/view/' + package_selected.id;
      window.open(action, '_self');
    });

    $('a#btn-edit').click(function(){
       var action = '/ispbrain/packages/edit/' + package_selected.id;
       window.open(action, '_self');
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
