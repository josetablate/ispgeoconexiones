

<div class="row">

    <div class="col-xl-3">

        <?= $this->Form->create($package,  ['class' => 'form-load']) ?>
        <fieldset>

            <?php
                echo $this->Form->input('name', ['label' => 'Nombre']);
                echo $this->Form->input('description',  ['label' => 'Descripción']);
                echo $this->Form->input('price',  ['label' => 'Precio Final']);
                echo $this->Form->input('aliquot', ['label' => 'Alícuota', 'options' => $alicuotas_types, 'value' => 5]);
                echo $this->Form->input('enabled', ['label' => 'Habilitar/Deshabilitar', 'checked' => true, 'class' => 'input-checkbox' ]);
            ?>
   

        </fieldset>
        <?= $this->Form->button(__('Agregar')) ?>
        <?= $this->Form->end() ?>
    </div>
</div>

