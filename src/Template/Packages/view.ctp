<style type="text/css">

    .vertical-table {
        width: 100%;
        color: #696868;
    }

    table > tbody > tr {
        border-bottom: 1px solid #e5e5e5;
    }

</style>

 <div class="row">
    <div class="col-md-2">
        <legend class="sub-title"><?= __('Paquete') ?></legend>
    </div>
    <div class="col-md-2">
        <div class="text-right">
          
            <a class="btn btn-default" title="Agregar un nuevo Paquete" href="<?=$this->Url->build(["action" => "add"])?>" role="button">
                <span class="glyphicon icon-plus" aria-hidden="true"></span>
            </a>
            <a class="btn btn-default" title="Editar el Paquete" href="<?=$this->Url->build(["action" => "edit", $package->id])?>" role="button">
                <span class="glyphicon icon-pencil2" aria-hidden="true"></span>
            </a>

            <?= 
                $this->Html->link(
                    '<span class="glyphicon icon-bin" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'class' => 'btn btn-default', 
                    'title' => 'Eliminar Paquete',
                    'role' => 'button',
                    'id' =>  'btn-delete',
                    'data-id' => $package->id,
                    'data-text' =>  __('¿Está Seguro que desea eliminar el Paquete <strong>{0}</strong>?', $package->name),
                    'escape' => false
                ]);
            ?> 
        </div>
    </div>    
</div>

<div class="row">

    <div class="col-xl-4">
        <legend class="sub-title-sm"><?= $package->name ?></legend>
        <table class="vertical-table">
            <tr>
                <th><?= __('Nombre') ?></th>
                <td class="text-right"><?= h($package->name) ?></td>
            </tr>
            <tr>
                <th><?= __('Descripción') ?></th>
                <td class="text-right"><?= h($package->description) ?></td>
            </tr>
              <tr>
                <th><?= __('Precio Final') ?></th>
                <td class="text-right">$<?= $package->price ?></td>
            </tr>
             <tr>
                <th><?= __('Alícuota') ?></th>
                <td class="text-right"><?= $alicuotas_types[$package->aliquot] ?></td>
            </tr>

            <tr>
                <th><?= __('Id') ?></th>
                <td class="text-right"><?= $package->id ?></td>
            </tr>

            <tr>
                <th><?= __('Creado') ?></th>
                <td class="text-right"><?= date('d-m-Y H:i', strtotime($package->created))?></td>
            </tr>
            <tr>
                <th><?= __('Modificado') ?></th>
                <td class="text-right"><?= date('d-m-Y H:i', strtotime($package->modified))?></td>
            </tr>

            <tr>
                <th><?= __('Habilitado') ?></th>
                <?php if($package->enabled):?>
                    <td class="text-right font-weight-bold"><?=  __('Si') ?></td>
                <?php else:?>
                    <td class="text-right font-weight-bold"><?=  __('No') ?></td>
                <?php endif;?>
            </tr>
        </table>
    </div>

</div>

<script type="text/javascript">

    $('#btn-delete').click(function() {

        var text = $(this).data('text');
        var id  = $(this).data('id');

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/packages/delete/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#replacer').submit();
            }
        });

    });


</script>

