<style type="text/css">

    .table-hover tbody tr:hover {
        background-color: white !important;
    }

    .table-hover tbody tr.selectable:hover {
        background-color: rgba(105, 104, 104, 0.28) !important;
    }

    .row-control  {
        font-weight: 600;
    }

</style>

<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5">
        
        <?php 
        
            echo $this->Html->link(
                '<span class="fas fa-sync-alt" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Actualizar la tabla',
                'class' => 'btn btn-default btn-update-table ml-1',
                'escape' => false
            ]);
                
            echo $this->Html->link(
                '<span class="glyphicon icon-eye" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Ocultar o Mostrar Columnas',
                'class' => 'btn btn-default btn-hide-column ml-1',
                'escape' => false
            ]);
            
            echo  $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar en formato excel el contenido de la tabla',
                'class' => 'btn btn-default btn-export ml-1',
                'escape' => false
            ]);
        
        ?>
     
    </div>
</div>

<div class="row">
    <div class="col-xl-8">
        
        <div class="card border-secondary mb-2">
        
            <div class="card-body p-2">
                
                <div class="row">
                    <div class="col-xl-3 pr-0">
                        <div class="text-secondary p-2">Perido de facturación</div>
                    </div>
                   
                   <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                        <div class='input-group date' id='period-datetimepicker'>
                            <input name='period' id="period" type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar">
                                <span class="glyphicon icon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
   
    </div>
      
    <div class="col-xl-4 ">
        
        <div class="card border-secondary p-1 mb-2">
        
            <div class="card-body p-2 d-flex justify-content-between">

                <?php

                   
                
                     echo $this->Html->link(
                        '<span class="glyphicon fas fa-search"  aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Buscar por Columnas',
                        'class' => 'btn btn-default btn-search-column ml-1',
                        'escape' => false
                    ]);
      
                    echo  $this->Html->link(
                        '<span class="glyphicon icon-checkbox-checked" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Seleccionar todos',
                        'class' => 'btn btn-default btn-selected-all ml-2',
                        'escape' => false
                    ]);
                        
                    echo  $this->Html->link(
                        '<span class="glyphicon icon-checkbox-unchecked" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Limpiar Selección',
                        'class' => 'btn btn-default btn-clear-selected-all ml-2',
                        'escape' => false
                    ]);
                        
                    echo  $this->Html->link(
                        '</span><span class="glyphicon icon-file-text2 text-white" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Generar facturas',
                        'class' => 'btn btn-danger btn-confirm ml-2 text-white',
                        'escape' => false
                    ]);

                  ?>
                
            </div>
            
        </div>
            
    </div>
</div>

<div class="row">
    
    <div class="col-12">
       <table class="table table-bordered table-hover" id="table-presux">
            <thead>
                <tr>
                    <th></th>                    <!--0-->
                    <th>Fecha</th>               <!--1-->
                    <th>Pto. Vta.</th>           <!--2-->
                    <th>Número</th>              <!--3-->
                        <th>Concepto</th>            <!--4-->
                        <th>Desde</th>               <!--5-->
                        <th>Hasta</th>               <!--6-->
                    <th>Comentario</th>          <!--7-->
                    <th>Vencimiento</th>         <!--8-->
                        <th>Subtotal</th>            <!--9-->
                        <th>Impuestos</th>           <!--10-->
                    <th>Total</th>               <!--11-->
                    <th>Pagado</th>              <!--12-->
                        <th>Usuario</th>             <!--13-->
                    <th>Código</th>              <!--14-->
                        <th>Tipo</th>                <!--15-->
                    <th>Nro</th>                 <!--16-->
                    <th>Nombre</th>              <!--17-->
                        <th>Domicilio</th>           <!--18-->
                        <th>Ciudad</th>              <!--19-->
                        <th>Empresa Facturación</th> <!--20-->
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    
</div>

<div class="modal fade modal-generate-invoice-from-presux" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabel">Facturar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                
                <div class="row">
                    
                    <div class="col-6">
                        <?php
                         echo $this->Form->input('business_id', ['options' => $business,  'label' =>  'Empresa', 'value' => '']); 
                        ?>
                    </div>
                    <div class="col-6">
                         <?php
                         echo $this->Form->input('type', ['options' => [],  'label' =>  'Tipo comprobante', 'required' => true]);
                        ?>
                    </div>
                    
                 </div>  
                 
                <div class="row">
                    
                    <div class="col-6">
                        <label class="control-label" for="">Fecha de emisión</label>
                        <div class='input-group date' id='created-datetimepicker'>
                            <input name="created" id="created"  type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar mr-0">
                                <span class="glyphicon icon-calendar mr-0"></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-6">
                         <?php
                         echo $this->Form->input('concept_type', ['label' => 'Tipo concepto', 'options' => $concept_types, 'value' => 2, 'id' => 'concept_type'])
                        ?>
                    </div>
                    
                 </div>
                 
                <div class="row">
                     <div class="col-12">
                         <legend class="sub-title-sm">Período</legend>
                     </div>
                 </div>
                 
                <div class="row">
                     <div class="col-6">
                        <label class="control-label" for="">Desde</label>
                        <div class='input-group date' id='date_start-datetimepicker'>
                            <input name="date_start" id="date_start"  type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar mr-0">
                                <span class="glyphicon icon-calendar mr-0"></span>
                            </span>
                        </div>
                     </div>
                     <div class="col-6">
                        <label class="control-label" for="">Hasta</label>
                        <div class='input-group date' id='date_end-datetimepicker'>
                            <input name="date_end" id="date_end"  type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar mr-0">
                                <span class="glyphicon icon-calendar mr-0"></span>
                            </span>
                        </div>
                     </div>
                 </div>
                 
                <div class="row mt-2">
                     <div class="col-12">
                        <label class="control-label" for="">Venc. de Pago</label>
                        <div class='input-group date' id='duedate-datetimepicker'>
                            <input name="duedate" id="duedate"  type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar mr-0">
                                <span class="glyphicon icon-calendar mr-0"></span>
                            </span>
                        </div>
                     </div>
                </div>
                 
                <div class="row mt-4">
                     <div class="col-12">
                         <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-danger btn-generate float-right">Confirmar</button>
                     </div>
                </div>
       
            </div>
        </div>
    </div>
</div>

<?php 
    
    $buttons = [];
 
    $buttons[] = [
        'id'   =>  'btn-go-customer',
        'name' =>  'Ficha del Cliente',
        'icon' =>  'glyphicon icon-profile',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-actions-presux', 'title' => 'Acciones', 'buttons' => $buttons ]);
    
    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-presux']);
    
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-presux']);
    
    echo $this->element('modal_preloader');

?>

<script type="text/javascript">

    $('#period-datetimepicker').datetimepicker({
            locale: 'es',
            defaultDate: new Date(),
            format: 'YYYY-MM',
            icons: {
                time: "glyphicon icon-alarm",
                date: "glyphicon icon-calendar",
                up: "glyphicon icon-arrow-right",
                down: "glyphicon icon-arrow-left"
            }
        });
 
    $('.modal-generate-invoice-from-presux #created-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'DD/MM/YYYY'
    });

    $('.modal-generate-invoice-from-presux #date_start-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'DD/MM/YYYY'
    });

    $('.modal-generate-invoice-from-presux #date_end-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'DD/MM/YYYY'
    });
     
    $('.modal-generate-invoice-from-presux #duedate-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'DD/MM/YYYY'
    });

    var table_presux = null;
    
    var rows_selected_ids = [];
     
    var total_selected_value = 0;

    $(document).ready(function() {
        
         $('.modal-generate-invoice-from-presux #business-id').change(function() {

           var selected = $(this).val() ;

           business_selected = null;

           $('.modal-generate-invoice-from-presux #type').empty();

           $.each(sessionPHP.paraments.invoicing.business, function(i, business) {

                if (selected == business.id) {

                    business_selected = business;
                    
                    $('.modal-generate-invoice-from-presux #type').append('<option value="">Seleccione Tipo</option>');

                    if (business.responsible == 1) { //ri

                        if (business.webservice_afip.crt != '') {
                            $('.modal-generate-invoice-from-presux #type').append('<optgroup label="Facturas"><option value="001">FACTURA A</option><option value="006">FACTURA B</option></optgroup>');
                        }
                    } else if (business.responsible == 6) { //monotributerou
                       
                        if (business.webservice_afip.crt != '') {
                            $('.modal-generate-invoice-from-presux #type').append('<optgroup label="Facturas"><option value="011">FACTURA C</option></optgroup>');
                        } 
                   }
               }
           });
        });

        $('.modal-generate-invoice-from-presux #business-id').change();
        
        
        $('.btn-hide-column').click(function() {
           
           $('.modal-hide-columns-presux').modal('show');
            
        });
        
        $('.btn-search-column').click(function() {
           
           $('.modal-search-columns-presux').modal('show');
           
        });
        
        loadPreferences('presux-index2', [4,5,6,9,10,13,15,18,19,20]);
        
        $('#btns-tools').hide();
        
        table_presux = $('#table-presux').DataTable({
		    "order": [[ 1, 'asc' ], [3, 'desc' ]],
            "deferRender": true,
            "processing": true,
            "serverSide": true,
             "ajax": {
                "url": "/ispbrain/Invoices/get_presu_x.json",
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "where": {
                            "Invoices.date_end like": "%" + $('#period').val() +"%",
                        },
                        // "period": $('#period').val(),
                    });
                },
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "drawCallback": function( settings ) {
                var flag = true;
            	switch (settings.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
            	    if (table_presux) {

                        var tableinfo = table_presux.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }
                    }
            	}
            },
		    "scrollY": true,
		    "scrollY": '380px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
		    "columns": [
		         {
                    "className":      'checkbox-control',
                    "orderable":      false,
                    "searchable":     false,
                    "data":           'id',
                    "render": function ( data, type, full, meta ) {
                        return '<label class="custom-control custom-checkbox pl-3 pr-3 m-0 align-middle"><span class="custom-control-input checkbox-select p-0 m-0 glyphicon icon-checkbox-unchecked" id="row-checkbox-' + data + '" aria-hidden="true"></span><span class="custom-control-indicator w-100 align-middle"></span></label>';
                    },
                    "width": '3%'
                },
                { 
                    "className": "row-control",
                    "data": "date",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        var date = data.split('T')[0];
                        date = date.split('-');
                        return date[2] + '/' + date[1] + '/' + date[0];
                    }
                },
                { 
                    "className": "row-control",
                    "data": "pto_vta",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return  pad(data, 4);
                    }
                },
                { 
                    "className": "row-control",
                    "data": "num",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 8);
                    }
                },
                { 
                    "className": "row-control",
                    "data": "concept_type",
                    "type": "options",
                    "options": {
                        "1": "Productos",
                        "2": "Servicios",
                        "3": "Productos y Servicios"
                    },
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.concept_types[data];
                    }
                },
                { 
                    "className": "row-control",
                    "data": "date_start",
                    "render": function ( data, type, row ) {
                        var date_start = data.split('T')[0];
                        date_start = date_start.split('-');
                        return date_start[2] + '/' + date_start[1] + '/' + date_start[0];
                    }
                },
                { 
                    "className": "row-control",
                    "data": "date_end",
                    "render": function ( data, type, row ) {
                        var date_end = data.split('T')[0];
                        date_end = date_end.split('-');
                        return date_end[2] + '/' + date_end[1] + '/' + date_end[0];
                    }
                },
                { 
                    "className": "row-control left",
                    "data": "comments",
                    "type": "string"
                },
                { 
                    "className": " text-info",
                    "data": "duedate",
                    "type": "date",
                    "render": function ( data, type, row ) {

                        var duedate = data.split('T')[0];
                        duedate = duedate.split('-');

                        if (type == 'display') {

                            if (row.paid == null) {

                                var date = new Date(duedate[1] + '/' + duedate[2] + '/' + duedate[0]);
                                var todayDate = new Date();

                                duedate =  duedate[2] + '/' + duedate[1] + '/' + duedate[0];

                                if (date < todayDate) {
                                    return "<span class='text-danger'>" + duedate + "</span>";
                                } else {
                                    return "<span class='text-info'>" + duedate + "</span>";
                                }
                            }
                        }

                        duedate =  duedate[2] + '/' + duedate[1] + '/' + duedate[0];
                        return duedate;
                    }
                },
                { 
                    "className": "row-control right",
                    "data": "subtotal",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' )
                },
                { 
                    "className": "row-control right",
                    "data": "sum_tax",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' )
                },
                { 
                    "className": "row-control right text-info",
                    "data": "total",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' )
                },
                { 
                    
                    "className": "row-control text-success",
                    "data": "paid",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var date = data.split('T')[0];
                            date = date.split('-');
                            return date[2] + '/' + date[1] + '/' + date[0];
                        }
                        return '';
                    }
                },
                { 
                    "className": "row-control left",
                    "data": "user.name",
                    "type": "string"
                },
                { 
                    "className": "row-control",
                    "data": "customer_code",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                {
                    "className": "row-control center",
                    "data": "customer_doc_type",
                    "type": "options",
                    "options": sessionPHP.afip_codes.doc_types,
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.doc_types[data];
                    }
                },
                {
                    "className": "row-control center",
                    "data": "customer_ident",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (data) {
                            ident = data;
                        }
                        return ident;
                    }
                },
                { 
                    "className": "row-control left",
                    "data": "customer_name",
                    "type": "string"
                },
                { 
                    "className": "row-control left",
                    "data": "customer_address",
                    "type": "string"
                },
                {
                    "className": "row-control left",
                    "data": "customer_city",
                    "type": "string"
                },
                { 
                    "className": "row-control",
                    "data": "business_id",
                    "type": "options_obj",
                    "options": sessionPHP.paraments.invoicing.business,
                    "render": function ( data, type, row ) {
                        var business_name = '';
                        $.each(sessionPHP.paraments.invoicing.business, function (i, b) {
                            if (b.id == data) {
                                business_name = b.name;
                            }
                        });
                        return business_name;
                    }
                },
            ],
		    "columnDefs": [
		        { "type": 'date-custom', targets: [1,5,6,8,12] },
		        { "type": 'numeric-comma', targets: [9,10,11] },
		        { 
                    "class": "left", targets: [7,17,18,19]
                },
                { 
                    "visible": false, targets: hide_columns
                },
            ],
            
             "createdRow" : function( row, data, index ) {
                 
                $(row).addClass('selectable');
            },
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 200, 300, 400, 500], [100, 200, 300, 400, 500]],
        	"dom":
    	    	"<'row'<'col-xl-3'l><'col-xl-3 mt-2  total_selected'><'col-xl-4'f><'col-xl-2 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
   
		});
		
		$('.btn-update-table').click(function() {

             if (table_presux) {
                table_presux.ajax.reload();
            }
        });

        $('#table-presux_wrapper .tools').append($('#btns-tools').contents());
        
        $('#table-presux_wrapper .total_selected').html('<div><label for="">Seleccionado:</label><span id="total_selected_value" class="ml-2 " style="color: #f05f40;">$0,00</span></div>');

        $('#btns-tools').show();
        
        var select_all_activate = false;
        
        var clear_all_activate = false;
		
		$('#table-presux').on( 'init.dt', function () {
		    
		    createModalHideColumn(table_presux, '.modal-hide-columns-presux');
            
            createModalSearchColumn(table_presux, '.modal-search-columns-presux');
		        
		     $( "#table-presux tbody" ).on( "click",  ".checkbox-select", function() {
		        
		        var data = table_presux.row( $(this).closest('tr') ).data();
		        
		        presux_selected = table_presux.row( $(this).closest('tr') ).data();
		        
                if (!$(this).closest('tr').hasClass('selected')) {
                    
                    if (!clear_all_activate) {
                        
                        $(this).closest('tr').addClass('selected');
                    
                        total_selected_value += data.total;
                     
                        rows_selected_ids.push(presux_selected.id); 
                        
                        $(this).removeClass('icon-checkbox-unchecked');
                        $(this).addClass('icon-checkbox-checked');
                    }
                    
                } else {
                    
                    if (!select_all_activate) {
                        
                        $(this).closest('tr').removeClass('selected');
                    
                        total_selected_value -= data.total;
                        
                        rows_selected_ids = rows_selected_ids.filter(function(item) { 
                            return item !== presux_selected.id 
                        });
                        
                        $(this).removeClass('icon-checkbox-checked');
                        $(this).addClass('icon-checkbox-unchecked');
                       
                    }
               
                }
                
                $('#total_selected_value').html(number_format(total_selected_value, true));
            });
            
            $( "#table-presux tbody" ).on( "click",  ".row-control", function() {
                
                presux_selected = table_presux.row( $(this).closest('tr') ).data();
                $('.modal-actions-presux').modal('show');
            });

        });
        
    
        $("#period-datetimepicker").on("dp.change", function (e) {
            table_presux.draw();
        });
     

        $('.btn-confirm').click(function() {
            $('.modal-generate-invoice-from-presux').modal('show');
        });
        
        $('.btn-selected-all').click(function() {
            select_all_activate = true;
            $('.checkbox-select').click();
            select_all_activate = false;
        });
        
        $('.btn-clear-selected-all').click(function() {
            clear_all_activate = true;
            $('.checkbox-select').click();
            clear_all_activate = false;
        });
        
        $('.btn-filter-period').click(function() {
             $('#modal-filter-period').modal('show');
        });
        
        $('#btn-go-customer').click(function() {
            var action = '/ispbrain/customers/view/' + presux_selected.customer_code;
            window.open(action, '_self');
        });
        
        $(".btn-export").click(function() {
            
            bootbox.confirm('Se descargara un archivo Excel', function(result) {
                if (result) {
                   
                   $('#table-presux').tableExport({
                       tableName: 'Debts',
                       type:'excel',
                       escape:'false',
                       columnNumber: [6, 7, 9, 10, 11]
                   });
                } 
            });
            
        });

        $('#concept_type').change(function() {
    
            if ($(this).val() == 1) {
                $('#table-period').hide();
            } else {
                $('#table-period').show();
            }
        });
             
        $(document).on("click", ".btn-generate", function(e) {
                
            if (rows_selected_ids.length > 0) {
                
                var d = new Date(); 
                var created = $('.modal-generate-invoice-from-presux #created').val().split('/');
                
                var h = addZero(d.getHours());
                var m = addZero(d.getMinutes());
                var s = addZero(d.getSeconds());
                
                created = created[2] + '-' + created[1] + '-' + created[0] + ' ' + h + ':' + m + ':' + s;
                
                var duedate = $('.modal-generate-invoice-from-presux #duedate').val().split('/');
                duedate = duedate[2] + '-' + duedate[1] + '-' + duedate[0] + ' ' + h + ':'+ m + ':' + s;
                
                var date_start = $('.modal-generate-invoice-from-presux #date_start').val().split('/');
                date_start = date_start[2] + '-' + date_start[1] + '-' + date_start[0] + ' ' + h + ':' + m + ':' + s;
                
                var date_end = $('.modal-generate-invoice-from-presux #date_end').val().split('/');
                date_end = date_end[2] + '-' + date_end[1] + '-' + date_end[0] + ' ' + h + ':' + m + ':' + s;
  
                var data = {
                    ids: rows_selected_ids,
                    date: created,
                    duedate: duedate,
                    concept_type: parseInt($('.modal-generate-invoice-from-presux #concept_type').val()),
                    date_start: date_start,
                    date_end: date_end,
                    type: $('.modal-generate-invoice-from-presux #type').val(),
                    business_id: $('.modal-generate-invoice-from-presux #business-id').val(),
                };
                
                created = new Date(data.date);
                duedate = new Date(data.duedate);
                date_start = new Date(data.date_start);
                date_end = new Date(data.date_end);
                
                var now = new Date();
                now.setHours(0);
                now.setMinutes(0);
                now.setSeconds(0);
              
                created.setHours(0);
                created.setMinutes(0);
                created.setSeconds(0);
                
                if (created > now) {
                    generateNoty('warning', 'La fecha de la factura no puede ser mayor a la fecha actual.');
                    return false;
                }
                
                if (duedate < created) {
                    generateNoty('warning', 'La fecha de vencimiento no puede ser menor a la fecha actual.');
                    return false;
                }
                
                if (data.concept_type != 1 && date_end < date_start) {
                    generateNoty('warning', 'Las fechas del período no son correctas.');
                    return false;
                }

                bootbox.confirm('Se generarán las facturas de los conceptos seleccionados. Cada factura de cada cliente tendrá la lista de conceptos en el orden que fueron realizados. ¿Desea continuar?', function(result) {
                    if (result) {
                
                        $('body')
                            .append( $('<form/>').attr({'action': '/ispbrain/invoices/generateMasiveFromPresuX/', 'method': 'post', 'id': 'form-block'})
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'data', 'value': JSON.stringify(data)}))
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                            .find('#form-block').submit();
                        
                        openModalPreloader("Generando Facturas. Espere Por favor ...");
                        
                    }
                });
                
            } else {
                bootbox.alert('Debe seleccionar al menos una deuda para continuar.');
            }
           
        });

    });

    function addZero(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }
    
</script>
