<style type="text/css">

    .my-hidden {
        display: none;
    }

    tr.anulated {
        background-color: #aa1220;
        color: white;
    }

</style>

<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5">

        <?php 

            echo $this->Html->link(
                '<span class="fas fa-sync-alt" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Actualizar la tabla',
                'class' => 'btn btn-default btn-update-table ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-eye" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Ocultar o Mostrar Columnas',
                'class' => 'btn btn-default btn-hide-column ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon fas fa-search" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Buscar por Columnas',
                'class' => 'btn btn-default btn-search-column ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla Excell',
                'class' => 'btn btn-default btn-export ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<i class="fas fa-download"></i>',
                'javascript:void(0)',
                [
                'title' => 'Descargar facturas',
                'class' => 'btn btn-default btn-download ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="fas fa-file-invoice aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Seleccionar y eliminar/anular',
                'class' => 'btn btn-default btn-remove-anulate ml-1 mt-1',
                'escape' => false
            ]);

        ?>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
         <table class="table table-bordered table-hover" id="table-invoices">
            <thead>
                <tr>
                    <th>Fecha</th>                      <!--0-->
                    <th>Comprobante</th>                <!--1-->
                    <th>Pto. Vta.</th>                  <!--2-->
                    <th>Número</th>                     <!--3-->
                    <th>Concepto</th>                   <!--4-->
                    <th>Desde</th>                      <!--5-->
                    <th>Hasta</th>                      <!--6-->
                    <th>Comentario</th>                 <!--7-->
                    <th>Vencimiento</th>                <!--8-->
                    <th class="subtotal">Subtotal</th>  <!--9-->
                    <th class="impuesto">Impuestos</th> <!--10-->
                    <th class="total">Total</th>        <!--11-->
                    <th>Pagado</th>                     <!--12-->
                    <th>CAE</th>                        <!--13-->
                    <th>VTO</th>                        <!--14-->
                    <th>Usuario</th>                    <!--15-->
                    <th>Código</th>                     <!--16-->
                    <th>Tipo</th>                       <!--17-->
                    <th>Nro</th>                        <!--18-->
                    <th>Nombre</th>                     <!--19-->
                    <th>Domicilio</th>                  <!--20-->
                    <th>Ciudad</th>                     <!--21-->
                    <th>Empresa Facturación</th>        <!--22-->
                    <th>Nro Asiento</th>                <!--23-->
                    <th>Anulado</th>                    <!--24-->
                </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
                <tr>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

<div class="modal fade modal-generate-invoice-from-move" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabel">Facturar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <div class="row">

                    <div class="col-6">
                        <?php
                         echo $this->Form->input('business_id', ['options' => $business,  'label' =>  'Empresa', 'value' => '']); 
                        ?>
                    </div>
                    <div class="col-6">
                         <?php
                         echo $this->Form->input('type', ['options' => [],  'label' =>  'Tipo comprobante', 'required' => true]);
                        ?>
                    </div>

                </div>  

                <div class="row">

                    <div class="col-6">
                        <label class="control-label" for="">Fecha de emisión</label>
                        <div class='input-group date' id='created-datetimepicker'>
                            <input name="created" id="created"  type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar mr-0">
                                <span class="glyphicon icon-calendar mr-0"></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-6">
                         <?php
                         echo $this->Form->input('concept_type', ['label' => 'Tipo concepto', 'options' => $concept_types, 'value' => 2, 'id' => 'concept_type'])
                        ?>
                    </div>

                </div>

                <div class="row">
                     <div class="col-12">
                         <legend class="sub-title-sm">Período</legend>
                     </div>
                </div>

                <div class="row">
                     <div class="col-6">
                        <label class="control-label" for="">Desde</label>
                        <div class='input-group date' id='date_start-datetimepicker'>
                            <input name="date_start" id="date_start"  type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar mr-0">
                                <span class="glyphicon icon-calendar mr-0"></span>
                            </span>
                        </div>
                     </div>
                     <div class="col-6">
                        <label class="control-label" for="">Hasta</label>
                        <div class='input-group date' id='date_end-datetimepicker'>
                            <input name="date_end" id="date_end"  type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar mr-0">
                                <span class="glyphicon icon-calendar mr-0"></span>
                            </span>
                        </div>
                     </div>
                </div>

                <div class="row mt-2">
                     <div class="col-12">
                        <label class="control-label" for="">Venc. de Pago</label>
                        <div class='input-group date' id='duedate-datetimepicker'>
                            <input name="duedate" id="duedate"  type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar mr-0">
                                <span class="glyphicon icon-calendar mr-0"></span>
                            </span>
                        </div>
                     </div>
                </div>

                <div class="row mt-4">
                     <div class="col-12">
                         <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-danger btn-generate float-right">Confirmar</button>
                     </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-remove-anulate" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabel">Confirmar Anulación/Eliminación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <div class="row">

                    <div class="col-xl-12">
                        <label>Selección:</label>
                    </div>

                    <div class="col-xl-12">
                        <ul style="list-style-type: none;" id="comprobantes-selected">
                        </ul>
                    </div>

                    <div class="col-xl-12">
                        <label>Total comprobanes seleccionados: <span id="count-comprobantes-selected"></span></label>
                    </div>

                    <div class="col-xl-12">
                        <label>Resultado:</label>
                        
                    </div>

                    <div class="col-xl-12">
                        <ul style="list-style-type: none;" id="comprobantes-generated">
                        </ul>
                    </div>

                    <div class="col-xl-12">
                        <label>Tener encuenta:</label>
                    </div>

                    <div class="col-xl-12">
                        <span style="font-size: 12px;" class="text-muted">* Para NCA, NCB, NCC, se generarán con el concepto: Anulación de factura #nro comprobante.</span>
                    </div>

                    <div class="col-xl-12">
                        <span style="font-size: 12px;" class="text-muted">* FX serán eliminados.</span>
                    </div>

                </div>

                <div class="row mt-4">
                     <div class="col-12">
                         <button type="button" class="btn btn-default btn-cancel" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-danger btn-confirm float-right">Confirmar</button>
                     </div>
                </div>

            </div>
        </div>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   => 'btn-print-invoice',
        'name' => 'Imprimir',
        'icon' => 'icon-printer',
        'type' => 'btn-secondary'
    ];

    $buttons[] = [
        'id'   => 'btn-go-customer',
        'name' => 'Ficha del Cliente',
        'icon' => 'glyphicon icon-profile',
        'type' => 'btn-secondary'
    ];

    $buttons[] = [
        'id'   => 'btn-edit-administrative-movement',
        'name' => 'Editar',
        'icon' => 'icon-pencil2',
        'type' => 'btn-success'
    ];

    $buttons[] = [
        'id'   => 'btn-generate-invoice-from-move',
        'name' => 'Convertir',
        'icon' => 'icon-pencil2',
        'type' => 'btn-info'
    ];

    $buttons[] = [
        'id'   => 'btn-send-email-invoice',
        'name' => 'Enviar Correo',
        'icon' => 'fab fa-telegram-plane',
        'type' => 'btn-success'
    ];

    $buttons[] = [
        'id'   => 'btn-delete',
        'name' => 'Eliminar',
        'icon' => 'fa fa-times',
        'type' => 'btn-danger'
    ];

    echo $this->element('modal_preloader');
    echo $this->element('actions', ['modal'=> 'modal-invoices', 'title' => 'Acciones', 'buttons' => $buttons ]);
    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-invoices']);
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-invoices']);
    echo $this->element('AdministrativeMovement/edit_administrative_movement', ['modal' => 'modal-edit-administrative-movement', 'title' => 'Editar']);
    echo $this->element('Email/send', ['modal'=> 'modal-send-email', 'title' => 'Seleccionar Plantilla', 'mass_emails_templates' => $mass_emails_templates ]);
?>

<script type="text/javascript">

    function addZero(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

    var table_invoices = null;
    var invoice_selected  = null;

    var customer_selected = null;
    var move_selected = null;
    var table_selected = null;

    var model = null;
    var model_id = null;
    var mass_emails_templates = null;

    var table_moves_mode = null;
    var move_selected_list = [];
    var invoices_removes = {
        fa: {
            count: 0,
            name: 'FA'
        },
        fb: {
            count: 0,
            name: 'FB'
        },
        fc: {
            count: 0,
            name: 'FC'
        },
        fx: {
            count: 0,
            name: 'FX'
        },
        total: 0
    };

    $('.modal-generate-invoice-from-move #created-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'DD/MM/YYYY'
    });

    $('.modal-generate-invoice-from-move #date_start-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'DD/MM/YYYY'
    });

    $('.modal-generate-invoice-from-move #date_end-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'DD/MM/YYYY'
    });

    var users = null;

    $('.modal-generate-invoice-from-move #duedate-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'DD/MM/YYYY'
    });
    var hash = null;

    $(document).ready(function () {

        var options_business = [];
        hash = <?= time() ?>;

        $.each(sessionPHP.paraments.invoicing.business, function(i, val) {
            if(val.enable){
                options_business.push({id:val.id, name:val.name + ' (' + val.address + ')'});
            }            
        });

        $('.modal-generate-invoice-from-move #business-id').change(function() {

            var selected = $(this).val() ;

            business_selected = null;

            $('.modal-generate-invoice-from-move #type').empty();

            $.each(sessionPHP.paraments.invoicing.business, function(i, business) {

                if (selected == business.id) {

                    business_selected = business;

                    $('.modal-generate-invoice-from-move #type').append('<option value="">Seleccione Tipo</option>');

                    if (business.responsible == 1) { //ri

                        if (business.webservice_afip.crt != '') {
                            $('.modal-generate-invoice-from-move #type').append('<optgroup label="Facturas"><option value="001">FACTURA A</option><option value="006">FACTURA B</option></optgroup>');
                        }
                    } else if (business.responsible == 6) { //monotributerou

                        if (business.webservice_afip.crt != '') {
                            $('.modal-generate-invoice-from-move #type').append('<optgroup label="Facturas"><option value="011">FACTURA C</option></optgroup>');
                        } 
                   }
               }
           });
        });

        $('.modal-generate-invoice-from-move #business-id').change();

        mass_emails_templates = <?= json_encode($mass_emails_templates) ?>;

        $('.btn-hide-column').click(function() {
            $('.modal-hide-columns-invoices').modal('show');
        });

        $('.btn-search-column').click(function() {
            $('.modal-search-columns-invoices').modal('show');
        });

        $('#table-invoices').removeClass('display');

        $('#btns-tools').hide();

        var hide_force_invoices = [];
        var no_search_invoices = [];

        if (!sessionPHP.paraments.accountant.account_enabled) {
            hide_force_invoices = [23];
            no_search_invoices = [23];
        }

        loadPreferences('invoices5-index', [4, 5, 6, 9, 10, 13, 14, 15, 17, 20, 21, 22]);

		table_invoices = $('#table-invoices').DataTable({
		    "order": [[ 0, 'desc' ]],
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "/ispbrain/Invoices/get_invoices.json",
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "drawCallback": function( settings ) {
                var flag = true;
            	switch (settings.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
            	    if (table_invoices) {

                        var tableinfo = table_invoices.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }
                    }
            	}

            	this.api().columns('.total').every(function() {
                    var column = this;
                    //$(column.footer()).html('$0.00');
                    //if (!$(column.row().node()).hasClass('anulated')) {
                        var intVal = function ( i ) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '')*1 :
                                typeof i === 'number' ?
                                    i : 0;
                        };

                        var sum = column
                            .data()
                            .reduce(function (a, b) { 

                            return intVal(a) + intVal(b);
                            }, 0);

                        $(column.footer()).html('$' + sum.toFixed(2));
                    //}
                });

                this.api().columns('.subtotal').every(function() {
                    var column = this;
                    //$(column.footer()).html('$0.00');
                    //if (!$(column.row().node()).hasClass('anulated')) {

                        var intVal = function ( i ) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '')*1 :
                                typeof i === 'number' ?
                                    i : 0;
                        };

                        var sum = column
                            .data()
                            .reduce(function (a, b) { 

                            return intVal(a) + intVal(b);
                            }, 0);

                        $(column.footer()).html('$' + sum.toFixed(2));
                    //}
                });

                this.api().columns('.impuesto').every(function() {
                    var column = this;
                    $(column.footer()).html('$0.00');
                    if (!$(column.row().node()).hasClass('anulated')) {
                        var intVal = function ( i ) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '')*1 :
                                typeof i === 'number' ?
                                    i : 0;
                        };

                        var sum = column
                            .data()
                            .reduce(function (a, b) { 

                            return intVal(a) + intVal(b);
                            }, 0);

                        $(column.footer()).html('$' + sum.toFixed(2));
                    }
                });
            },
		    "scrollY": true,
		    "scrollY": '370px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
		    "columns": [
                { 
                    "className": "",
                    "data": "date",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        var date = data.split('T')[0];
                        date = date.split('-');
                        return date[2] + '/' + date[1] + '/' + date[0];
                    }
                },
                { 
                    "className": "",
                    "data": "tipo_comp",
                    "type": "options",
                    "options": {
                        "XXX": "PRESU X",
                        "001": "FACTURA A",
                        "006": "FACTURA B",
                        "011": "FACTURA C"
                    },
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.comprobantes[data];
                    }
                },
                { 
                    "className": "",
                    "data": "pto_vta",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return  pad(data, 4);
                        }
                        return "";
                    }
                },
                { 
                    "className": "",
                    "data": "num",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return pad(data, 8);
                        }
                        return "";
                    }
                },
                { 
                    "className": "",
                    "data": "concept_type",
                    "type": "options",
                    "options": {
                        "1": "Productos",
                        "2": "Servicios",
                        "3": "Productos y Servicios"
                    },
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.concept_types[data];
                    }
                },
                { 
                    "className": "",
                    "data": "date_start",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        var date_start = data.split('T')[0];
                        date_start = date_start.split('-');
                        return date_start[2] + '/' + date_start[1] + '/' + date_start[0];
                    }
                },
                { 
                    "className": "",
                    "data": "date_end",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        var date_end = data.split('T')[0];
                        date_end = date_end.split('-');
                        return date_end[2] + '/' + date_end[1] + '/' + date_end[0];
                    }
                },
                { 
                    "className": " left",
                    "data": "comments",
                    "type": "string"
                },
                { 
                    "className": " text-info",
                    "data": "duedate",
                    "type": "date",
                    "render": function ( data, type, row ) {

                        var duedate = data.split('T')[0];
                        duedate = duedate.split('-');

                        if (type == 'display') {

                            if (row.paid == null) {

                                var date = new Date(duedate[1] + '/' + duedate[2] + '/' + duedate[0]);
                                var todayDate = new Date();

                                duedate =  duedate[2] + '/' + duedate[1] + '/' + duedate[0];

                                if (date < todayDate) {
                                    return "<span class='text-danger'>" + duedate + "</span>";
                                } else {
                                    return "<span class='text-info'>" + duedate + "</span>";
                                }
                            }
                        }

                        duedate =  duedate[2] + '/' + duedate[1] + '/' + duedate[0];
                        return duedate;
                    }
                },
                { 
                    "className": " right",
                    "data": "subtotal",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' )
                },
                { 
                    "className": " right",
                    "data": "sum_tax",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' )
                },
                { 
                    "className": " right text-info",
                    "data": "total",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' )
                },
                { 
                    
                    "className": " text-success",
                    "data": "paid",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var date = data.split('T')[0];
                            date = date.split('-');
                            return date[2] + '/' + date[1] + '/' + date[0];
                        }
                        return '';
                    }
                },
                { 
                    "className": " left",
                    "data": "cae",
                    "type": "integer",
                },
                { 
                    "className": "",
                    "data": "vto"
                },
                { 
                    "className": " left",
                    "data": "user.name",
                    "type": "string"
                },
                { 
                    "className": "",
                    "data": "customer_code",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                {
                    "className": " center",
                    "data": "customer_doc_type",
                    "type": "options",
                    "options": sessionPHP.afip_codes.doc_types,
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.doc_types[data];
                    }
                },
                {
                    "className": " center",
                    "data": "customer_ident",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return data;
                    }
                },
                { 
                    "className": " left",
                    "data": "customer_name",
                    "type": "string"
                },
                { 
                    "className": " left",
                    "data": "customer_address",
                    "type": "string"
                },
                {
                    "className": " left",
                    "data": "customer_city",
                    "type": "string"
                },
                { 
                    "data": "business_id",
                    "type": "options_obj",
                    "options": options_business,
                    "render": function ( data, type, row ) {
                        var business_name = '';
                        $.each(options_business, function (i, b) {
                            if (b.id == data) {
                                business_name = b.name;
                            }
                        });
                        return business_name;
                    }
                },
                {
                    "data": "seating_number",
                    "type": "string"
                },
                { 
                    "className": "",
                    "data": "anulated",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var date = data.split('T')[0];
                            date = date.split('-');
                            return date[2] + '/' + date[1] + '/' + date[0];
                        }
                        return '';
                    }
                },
            ],
		    "columnDefs": [
                { 
                    "visible": false, targets: hide_columns
                },
                { 
                    "visible": false, targets: hide_force_invoices
                },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
                if (data.anulated) {
                    $(row).addClass('anulated');
                }
            },
            "initComplete": function(settings, json) {
            },
		    "language": dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
        	"dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
		});

		$('.btn-update-table').click(function() {
             if (table_invoices) {
                table_invoices.ajax.reload();
            }
        });
 
        $('#table-invoices_wrapper .tools').append($('#btns-tools').contents());

        $('#table-invoices').on( 'init.dt', function () {
            createModalHideColumn(table_invoices, '.modal-hide-columns-invoices', hide_force_invoices);
            createModalSearchColumn(table_invoices, '.modal-search-columns-invoices', no_search_invoices);
        });

        $('#btns-tools').show();

        $('#table-invoices tbody').on( 'click', 'tr', function () {

            if (!$(this).closest('tr').find('.dataTables_empty').length) {

                if (table_moves_mode == null) {

                    table_invoices.$('tr.selected').removeClass('selected');
                    $(this).closest('tr').addClass('selected');
                    $('#btn-edit-administrative-movement').addClass('my-hidden');

                    invoice_selected = table_invoices.row( this ).data();
                   
                    move_selected = {
                        'id': invoice_selected.id,
                        'model': 'Invoices'
                    };
                    customer_selected = {
                        code: invoice_selected.customer_code
                    }

                    if (invoice_selected.customer.billing_for_service) {                       
                        $('#btn-edit-administrative-movement').removeClass('my-hidden');
                    }

                    if (invoice_selected.tipo_comp == 'XXX') {
                        $('#btn-generate-invoice-from-move').show();
                        $('#btn-delete').show();
                    } else {
                        $('#btn-generate-invoice-from-move').hide();
                        $('#btn-delete').hide();
                    }

                    $('.modal-invoices').modal('show');
                } else if (table_moves_mode == 'remove-anulate') {

                    if ($.inArray($(this).closest('tr').attr('id'), move_selected_list) == -1) {

                        move_selected_list.push($(this).closest('tr').attr('id'));
                        $(this).closest('tr').addClass('selected');

                    } else {

                        var attr_id = $(this).closest('tr').attr('id');
                        move_selected_list = move_selected_list.filter(function(item) { 
                            return item !== attr_id 
                        });

                        $(this).closest('tr').removeClass('selected');
                    }
                }
            }
        });

        $('.btn-remove-anulate').click(function() {

            if (table_moves_mode == null) {

                table_moves_mode = 'remove-anulate';

                move_selected_list = [];

                $('#table-invoices_wrapper').addClass('border border-primary rounded p-2');
                $('.btn-remove-anulate').addClass('border border-primary text-primary');

            } else if (table_moves_mode == 'remove-anulate') {

                if (move_selected_list.length > 0) {
                    $('.modal-remove-anulate').modal('show');
                } else {
                    generateNoty('warning', 'Debe seleccionar al menos 1 factura.');
                }
            }
        });

        $('.modal-remove-anulate').on('shown.bs.modal', function (e) {

            if ($('.modal-remove-anulate .btn-confirm').hasClass('d-none')) {
                $('.modal-remove-anulate .btn-confirm').removeClass('d-none');
            }

            let invoices = table_invoices.rows().data();

            invoices_removes.fa.count = 0;
            invoices_removes.fb.count = 0;
            invoices_removes.fc.count = 0;
            invoices_removes.fx.count = 0;
            invoices_removes.total = 0;

            $.each(invoices, function( index, invoice ) {

                $.each(move_selected_list, function( index, id ) {

                    if (id == invoice.id) {

                        //FA
                        if (invoice.tipo_comp == '001') {
                            invoices_removes.fa.count++;
                        }

                        //FB
                        if (invoice.tipo_comp == '006') {
                            invoices_removes.fb.count++;
                        }

                        //FC
                        if (invoice.tipo_comp == '011') {
                            invoices_removes.fc.count++;
                        }

                        //FX
                        if (invoice.tipo_comp == 'XXX') {
                            invoices_removes.fx.count++;
                        }

                        invoices_removes.total++;
                    }
                });
            });

            $('#comprobantes-selected').html('');
            $('#comprobantes-generated').html('');
            $('#count-comprobantes-selected').html('');

            if (invoices_removes.fa.count > 0) {
                $('#comprobantes-selected').append('<li>FA: ' + invoices_removes.fa.count + '</li>');
                $('#comprobantes-generated').append('<li>NCA a crear: ' + invoices_removes.fa.count + '</li>');
            }

            if (invoices_removes.fb.count > 0) {
                $('#comprobantes-selected').append('<li>FB: ' + invoices_removes.fb.count + '</li>');
                $('#comprobantes-generated').append('<li>NCB a crear: ' + invoices_removes.fb.count + '</li>');
            }

            if (invoices_removes.fc.count > 0) {
                $('#comprobantes-selected').append('<li>FC: ' + invoices_removes.fc.count + '</li>');
                $('#comprobantes-generated').append('<li>NCC a crear: ' + invoices_removes.fc.count + '</li>');
            }

            if (invoices_removes.fx.count > 0) {
                $('#comprobantes-selected').append('<li>FX: ' + invoices_removes.fx.count + '</li>');
                $('#comprobantes-generated').append('<li>FX a eliminar: ' + invoices_removes.fx.count + '</li>');
            }

            $('#count-comprobantes-selected').append(invoices_removes.total);
        });

        $(document).on("click", ".modal-remove-anulate .btn-cancel", function(e) {
            table_moves_mode =  null;
            $('#table-invoices_wrapper').removeClass('border border-primary rounded p-2');
            $('.btn-remove-anulate').removeClass('border border-primary text-primary');
            table_invoices.draw();
        });

        $('#btn-generate-invoice-from-move').click(function() {
             $('.modal-invoices').modal('hide');
             $('.modal-generate-invoice-from-move').modal('show');
        });

        $(document).on("click", ".modal-remove-anulate .btn-confirm", function(e) {

            let data = {
                invoices_removes: invoices_removes,
                ids: move_selected_list
            };

            $('.modal-remove-anulate .btn-confirm').addClass('d-none');

            $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/invoices/remove/', 'method': 'post', 'id': 'form-remove-anulate'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'data', 'value': JSON.stringify(data)}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'hash', 'value': hash}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#form-remove-anulate').submit();
        });

        $(document).on("click", ".modal-generate-invoice-from-move .btn-generate", function(e) {

             var rows_selected_ids = [];
             rows_selected_ids.push(move_selected.id);

             if (rows_selected_ids) {

                var d = new Date(); 
                var created = $('.modal-generate-invoice-from-move #created').val().split('/');

                var h = addZero(d.getHours());
                var m = addZero(d.getMinutes());
                var s = addZero(d.getSeconds());

                created = created[2] + '-' + created[1] + '-' + created[0] + ' ' + h + ':' + m + ':' + s;

                var duedate = $('.modal-generate-invoice-from-move #duedate').val().split('/');
                duedate = duedate[2] + '-' + duedate[1] + '-' + duedate[0] + ' ' + h + ':'+ m + ':' + s;

                var date_start = $('.modal-generate-invoice-from-move #date_start').val().split('/');
                date_start = date_start[2] + '-' + date_start[1] + '-' + date_start[0] + ' ' + h + ':' + m + ':' + s;

                var date_end = $('.modal-generate-invoice-from-move #date_end').val().split('/');
                date_end = date_end[2] + '-' + date_end[1] + '-' + date_end[0] + ' ' + h + ':' + m + ':' + s;

                var data = {
                    ids: rows_selected_ids,
                    date: created,
                    duedate: duedate,
                    concept_type: parseInt($('.modal-generate-invoice-from-move #concept_type').val()),
                    date_start: date_start,
                    date_end: date_end,
                    type: $('.modal-generate-invoice-from-move #type').val(),
                    business_id: $('.modal-generate-invoice-from-move #business-id').val(),
                };

                created = new Date(data.date);
                duedate = new Date(data.duedate);
                date_start = new Date(data.date_start);
                date_end = new Date(data.date_end);

                var now = new Date();
                now.setHours(0);
                now.setMinutes(0);
                now.setSeconds(0);

                created.setHours(0);
                created.setMinutes(0);
                created.setSeconds(0);

                if (created > now) {
                    generateNoty('warning', 'La fecha de la factura no puede ser mayor a la fecha actual.');
                    return false;
                }

                if (duedate < created) {
                    generateNoty('warning', 'La fecha de vencimiento no puede ser menor a la fecha actual.');
                    return false;
                }

                if (data.concept_type != 1 && date_end < date_start) {
                    generateNoty('warning', 'Las fechas del período no son correctas.');
                    return false;
                }

                bootbox.confirm('Se generará la factura. ¿Desea continuar?', function(result) {
                    if (result) {

                        $('body')
                            .append( $('<form/>').attr({'action': '/ispbrain/invoices/generateIndiFromPresuX/', 'method': 'post', 'id': 'form-block'})
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'data', 'value': JSON.stringify(data)}))
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                            .find('#form-block').submit();

                        openModalPreloader("Generando Factura. Espere Por favor ...");
                        
                    }
                });

            } else {
                bootbox.alert('Debe seleccionar al menos una deuda para continuar.');
            }
        });
    });

    $('.btn-download').click(function() {

        var ids = [];

        table_invoices.rows({search: 'applied'}).every( function ( rowIdx, tableLoop, rowLoop ) {
            ids.push( this.data().id );
        });

        if (ids.length > 0) {

            $('#replacer').remove();

            $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/download/invoices/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'ids', 'value': ids}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#replacer').submit();
        } else {
            generateNoty('information', 'No hay facturas para descargar');
        }
    });

    $('#btn-go-customer').click(function() {
        var action = '/ispbrain/customers/view/' + invoice_selected.customer_code;
        window.open(action, '_self');
    });

    $('#btn-print-invoice').click(function() {
        var url = '/ispbrain/Invoices/showprint/' + table_invoices.$('tr.selected').attr('id');
        window.open(url, 'Imprimir Comprobante', "width=600, height=500");
    });

    $('#btn-edit-administrative-movement').click(function() {
        $('.modal-invoices').modal('hide');
        table_selected = table_invoices;
        $('.modal-edit-administrative-movement').modal('show');
    });

    $('#btn-send-email-invoice').click(function() {

        if (invoice_selected.customer.email == "" || invoice_selected.customer.email == null) {
            generateNoty('warning', 'Debe agregar el Correo Electrónico del Cliente al cual pertenece la Factura.');
        } else {
            var count = 0;
            $.each(mass_emails_templates, function( index, value ) {
                count++;
            });
            if (count > 1) {
                model = 'Invoices';
                model_id = invoice_selected.id;
                $('.modal-invoices').modal('hide');
                $('.modal-send-email').modal('show');
            } else {
                generateNoty('warning', 'No cuenta con plantillas de Correos.');
            }
        }
    });

    $('#btn-delete').click(function() {
        
        $('.modal-invoices').modal('hide');
        
        bootbox.confirm("¿Está Seguro que desea eliminar el PRESU X?", function(result) {
        
        if (result) {
                
            var request = $.ajax({
                    url: "<?= $this->Url->build(['controller' => 'Invoices', 'action' => 'delete']) ?>",  
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify(invoice_selected),
                });
                
                request.done(function( data ) {
                    table_invoices.draw();
                });
                 
                request.fail(function( jqXHR, textStatus ) {
                    generateNoty('error', 'Error al intenetar eliminar el PRESU X');
                });
            }
        });

    });

    $(".btn-export").click(function() {

        bootbox.confirm('Se descargará un archivo Excel', function(result) {
            if (result) {
                $('#table-invoices').tableExport({tableName: 'Facturas', type:'excel', escape:'false', columnNumber: [10, 11, 12, 13]});
            }
        }).find("div.modal-content").addClass("confirm-width-md");
    });

</script>
