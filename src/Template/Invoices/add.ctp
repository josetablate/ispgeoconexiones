<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Invoices'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Debts'), ['controller' => 'Debts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Debt'), ['controller' => 'Debts', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="invoices form large-9 medium-8 columns content">
    <?= $this->Form->create($invoice) ?>
    <fieldset>
        <legend><?= __('Add Invoice') ?></legend>
        <?php
            echo $this->Form->input('date');
            echo $this->Form->input('date_start', ['empty' => true]);
            echo $this->Form->input('date_end', ['empty' => true]);
            echo $this->Form->input('duedate');
            echo $this->Form->input('tipo_comp');
            echo $this->Form->input('pto_vta');
            echo $this->Form->input('num');
            echo $this->Form->input('tax');
            echo $this->Form->input('subtotal');
            echo $this->Form->input('sum_tax');
            echo $this->Form->input('discount');
            echo $this->Form->input('total');
            echo $this->Form->input('cae');
            echo $this->Form->input('vto', ['empty' => true]);
            echo $this->Form->input('company_name');
            echo $this->Form->input('company_address');
            echo $this->Form->input('company_cp');
            echo $this->Form->input('company_city');
            echo $this->Form->input('company_phone');
            echo $this->Form->input('company_fax');
            echo $this->Form->input('company_ident');
            echo $this->Form->input('company_email');
            echo $this->Form->input('company_web');
            echo $this->Form->input('company_responsible');
            echo $this->Form->input('concept_receipt');
            echo $this->Form->input('customer_code');
            echo $this->Form->input('customer_name');
            echo $this->Form->input('customer_address');
            echo $this->Form->input('customer_cp');
            echo $this->Form->input('customer_city');
            echo $this->Form->input('customer_country');
            echo $this->Form->input('customer_ident');
            echo $this->Form->input('customer_doc_type');
            echo $this->Form->input('customer_responsible');
            echo $this->Form->input('comments');
            echo $this->Form->input('user_id', ['options' => $users]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
