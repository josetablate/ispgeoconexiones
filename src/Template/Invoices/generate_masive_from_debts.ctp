<style type="text/css">

    .table-hover tbody tr:hover {
        background-color: white !important;
    }

    .table-hover tbody tr.selectable:hover {
        background-color: rgba(105, 104, 104, 0.28) !important;
    }

    .row-control {
        font-weight: 600;
    }

</style>

<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5">

        <?php 

            echo $this->Html->link(
                '<span class="fas fa-sync-alt" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Actualizar la tabla',
                'class' => 'btn btn-default btn-update-table ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-eye" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Ocultar o Mostrar Columnas',
                'class' => 'btn btn-default btn-hide-column ml-1',
                'escape' => false
            ]);

            echo  $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar en formato excel el contenido de la tabla',
                'class' => 'btn btn-default btn-export ml-1',
                'escape' => false
            ]);

        ?>

    </div>
</div>

<div class="row">
    <div class="col-xl-8">

        <div class="card border-secondary mb-2">

            <div class="card-body p-2">

                <div class="row">
                    <div class="col-xl-3 pr-0">
                        <div class="text-secondary p-2">Período de facturación</div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                        <div class='input-group date' id='period-datetimepicker'>
                            <input name='period' id="period" type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar">
                                <span class="glyphicon icon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
 
    </div>

    <div class="col-xl-4 ">

        <div class="card border-secondary p-1 mb-2">

            <div class="card-body p-2 d-flex justify-content-between">

                <?php

                    echo $this->Html->link(
                        '<span class="glyphicon fas fa-search"  aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Buscar por Columnas',
                        'class' => 'btn btn-default btn-search-column ml-1',
                        'escape' => false
                    ]);

                    echo  $this->Html->link(
                        '<span class="glyphicon icon-checkbox-checked" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Seleccionar todos',
                        'class' => 'btn btn-default btn-selected-all ml-2',
                        'escape' => false
                    ]);

                    echo  $this->Html->link(
                        '<span class="glyphicon icon-checkbox-unchecked" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Limpiar Selección',
                        'class' => 'btn btn-default btn-clear-selected-all ml-2',
                        'escape' => false
                    ]);

                    echo  $this->Html->link(
                        '<span class="glyphicon fa fa-play-circle" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Acciones',
                        'class' => 'btn btn-default btn-action ml-2',
                        'escape' => false
                    ]);

                    echo  $this->Html->link(
                        '</span><span class="glyphicon icon-file-text2 text-white" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Generar facturas',
                        'class' => 'btn btn-danger btn-confirm ml-2 text-white',
                        'escape' => false
                    ]);

                  ?>

            </div>

        </div>

    </div>
</div>

<div class="row">

    <div class="col-12">
       <table class="table table-bordered table-hover" id="table-debts">
            <thead>
                <tr>
                    <th></th>                   <!--0-->
                    <th>Fecha</th>              <!--1-->
                    <th>Venc.</th>              <!--2-->
                    <th>Código</th>             <!--3-->
                    <th>Descripción</th>        <!--4-->
                    <th>Cant.</th>              <!--5-->
                    <th>Unidad</th>             <!--6-->
                    <th>Precio</th>             <!--7-->
                    <th>Subtotal</th>           <!--8-->
                    <th>Alícuota</th>           <!--9-->
                    <th>Impuesto</th>           <!--10-->
                    <th>Descuento</th>          <!--11-->
                    <th>Total</th>              <!--12-->
                    <th>Destino</th>            <!--13-->
                    <th>Código</th>             <!--14-->
                    <th>Cuenta</th>             <!--15-->
                    <th>Documento</th>          <!--16-->
                    <th>Nombre</th>             <!--17-->
                    <th>Domicilio</th>          <!--18-->
                    <th>Usuario</th>            <!--19-->
                    <th>Asiento #</th>          <!--20-->
                    <th>Empresa Facturación</th><!--21-->
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

</div>

<div class="modal fade confirm-invoice-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabel">Confirmar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">

                        <table style="width:100%">

                            <tr>
                                <td style="width:40%"><label class="control-label" for="">Fecha</label></td>
                                <td style="width:60%">
                                    
                                    <div class='input-group date' id='created-datetimepicker'>
                                        <input name="created" id="created"  type='text' class="form-control" required />
                                        <span class="input-group-addon input-group-text calendar mr-0">
                                            <span class="glyphicon icon-calendar mr-0"></span>
                                        </span>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td style="width:40%"><label class="control-label" for="">Tipo</label></td>
                                <td style="width:60%">
                                <?php echo $this->Form->input('concept_type', ['label' => false, 'options' => $concept_types, 'value' => 2, 'id' => 'concept_type'])?>
                                </td>
                            </tr>
                        </table>
                        <table id="table-period" style="width:100%">
                            <tr>
                                <td colspan="2" style="width:100%">
                                    <br>
                                    <legend class="sub-title-sm">Período</legend>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:40%"><label class="control-label" for="">Desde</label></td>
                                <td style="width:60%">

                                    <div class='input-group date' id='date_start-datetimepicker'>
                                        <input name="date_start" id="date_start" type='text' class="form-control" required />
                                        <span class="input-group-addon input-group-text calendar mr-0">
                                            <span class="glyphicon icon-calendar mr-0"></span>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:40%"><label class="control-label" for="">Hasta</label></td>
                                <td style="width:60%">

                                    <div class='input-group date' id='date_end-datetimepicker'>
                                        <input name="date_end" id="date_end"  type='text' class="form-control" required />
                                        <span class="input-group-addon input-group-text calendar mr-0">
                                            <span class="glyphicon icon-calendar mr-0"></span>
                                        </span>
                                    </div>
                               </td>
                            </tr>
                              <tr>
                                <td style="width:40%"><label class="control-label" for="">Venc. de Pago</label></td>
                                <td style="width:60%">

                                    <div class='input-group date' id='duedate-datetimepicker'>
                                        <input name="duedate" id="duedate"  type='text' class="form-control" required />
                                        <span class="input-group-addon input-group-text calendar mr-0">
                                            <span class="glyphicon icon-calendar mr-0"></span>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <br>

                    	<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-danger btn-generate float-right">Confirmar</button>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php 

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-edit-debt',
        'name' =>  'Editar deuda',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-edit-discount',
        'name' =>  'Editar descuento',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete',
        'name' =>  'Eliminar',
        'icon' =>  'fa fa-times',
        'type' =>  'btn-danger'
    ];

    $buttons[] = [
        'id'   =>  'btn-go-customer',
        'name' =>  'Ficha del Cliente',
        'icon' =>  'glyphicon icon-profile',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-actions-debts', 'title' => 'Acciones', 'buttons' => $buttons ]);

    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-debts']);

    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-debts']);

    echo $this->element('edit_debt_complete');

    echo $this->element('edit_discount');

    echo $this->element('modal_preloader');

    $buttons_action = [];

    $buttons_action[] = [
        'id'   =>  'btn-delete-masive',
        'name' =>  'Eliminar',
        'icon' =>  'fa fa-times',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-actions-masive', 'title' => 'Acciones', 'buttons' => $buttons_action ]);
?>

<script type="text/javascript">

    $('#period-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'MM/YYYY',
        icons: {
            time: "glyphicon icon-alarm",
            date: "glyphicon icon-calendar",
            up: "glyphicon icon-arrow-right",
            down: "glyphicon icon-arrow-left"
        }
    });
 
    $('.confirm-invoice-modal #created-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'DD/MM/YYYY'
    });

    $('.confirm-invoice-modal #date_start-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'DD/MM/YYYY'
    });

    $('.confirm-invoice-modal #date_end-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'DD/MM/YYYY'
    });

    $('.confirm-invoice-modal #duedate-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'DD/MM/YYYY'
    });

    var table_debts = null;

    var move_selected = null;

    var rows_selected_ids = [];

    var total_selected_value = 0;

    var hash = null;

    $(document).ready(function() {

        var options_business = [];
        hash = <?= time() ?>;

        $.each(sessionPHP.paraments.invoicing.business, function(i, val) {
            options_business.push({id:val.id, name:val.name + ' (' + val.address + ')'});
        });

        $('.btn-hide-column').click(function() {
           $('.modal-hide-columns-debts').modal('show');
        });

        $('.btn-search-column').click(function() {
           $('.modal-search-columns-debts').modal('show');
        });

        loadPreferences('debts-index2', [3, 5, 6, 7, 8, 9, 10, 11, 15, 18, 19, 20]);

        $('#btns-tools').hide();

        table_debts = $('#table-debts').DataTable({
		    "order": [[ 1, 'asc' ], [3, 'desc' ]],
            "deferRender": true,
            "processing": true,
            "serverSide": true,
             "ajax": {
                "url": "/ispbrain/debts/get_without_invoice.json",
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "period": $('#period').val(),
                    });
                },
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "drawCallback": function( settings ) {
                var flag = true;
            	switch (settings.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
            	    if (table_debts) {

                        var tableinfo = table_debts.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }
                    }
            	}
            },
		    "scrollY": true,
		    "scrollY": '380px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
		    "columns": [
		        {
                    "className":      'checkbox-control',
                    "orderable":      false,
                    "searchable":     false,
                    "data":           'id',
                    "render": function ( data, type, full, meta ) {
                        return '<label class="custom-control custom-checkbox pl-3 pr-3 m-0 align-middle"><span class="custom-control-input checkbox-select p-0 m-0 glyphicon icon-checkbox-unchecked" id="row-checkbox-' + data + '" aria-hidden="true"></span><span class="custom-control-indicator w-100 align-middle"></span></label>';
                    },
                    "width": '3%'
                },
                { 
                    "className": 'row-control ',
                    "data": "created",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                {
                    "className": 'row-control ',
                    "data": "duedate",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var duedate = data.split('T')[0];
                            duedate = duedate.split('-');
                            return duedate[2] + '/' + duedate[1] + '/' + duedate[0];
                        }
                        return '';
                    }
                },
                { 
                    "className": 'row-control ',
                    "data": "code"
                },
                { 
                    "className": 'row-control left ',
                    "data": "description",
                    "type": "string"
                },
                { 
                    "className": 'row-control ',
                    "data": "quantity",
                },
                { 
                    "className": 'row-control ',
                    "data": "unit",
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.units_types[data];
                    }
                },
                { 
                    "className": 'row-control ',
                    "data": "price",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' )
                },
                { 
                    "className": 'row-control ',
                    "data": "sum_price",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' )
                },
                { 
                    "className": 'row-control ',
                    "data": "tax",
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.alicuotas_types[data];
                    }
                },
                { 
                    "className": 'row-control ',
                    "data": "sum_tax",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' )
                },
                { 
                    "className": 'row-control ',
                    "data": "discount",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' )
                },
                { 
                    "className": 'row-control ',
                    "data": "total",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' )
                },
                { 
                    "className": 'row-control ',
                    "data": "tipo_comp",
                    "type": "options",
                    "options": {
                        "XXX": "PRESU X",
                        "001": "FACTURA A",
                        "006": "FACTURA B",
                        "011": "FACTURA C",
                    },
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.comprobantes[data];
                    }
                },
                { 
                    "className": 'row-control ',
                    "data": "customer.code",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                { 
                    "className": 'row-control ',
                    "data": "customer.account_code",
                },
                { 
                    "className": 'row-control ',
                    "data": "customer.ident",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (data) {
                            ident = data;
                        }
                        return sessionPHP.afip_codes.doc_types[row.customer.doc_type] + ' ' + ident;
                    }
                },
                {   
                    "className": 'row-control ',
                    "data": "customer.name",
                    "type": "string"
                },
                {   
                    "className": 'row-control ',
                    "data": "connection",
                    "render": function ( data, type, row ) {
                        if (data) {
                             return data.address;
                        }
                       return row.customer.address;
                    }
                },
                { 
                    "className": 'row-control ',
                    "data": "user.name",
                    "type": "options",
                    "options": users,
                    "render": function ( data, type, row ) {
                       return data;
                    }
                },
                { 
                    "className": 'row-control ',
                    "data": "seating_number",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return pad(data, 5);
                        }
                        return '';
                    }
                },
                { 
                    "className": 'row-control ',
                    "data": "customer.business_billing",
                    "type": "options_obj",
                    "options": options_business,
                    "render": function ( data, type, row ) {
                        var business_name = '';
                        $.each(options_business, function (i, b) {
                            if (b.id == data) {
                                business_name = b.name;
                            }
                        });
                        return business_name;
                    }
                },
            ],
		    "columnDefs": [
		        { "type": 'date-custom', targets: [1, 2] },
		        { "type": 'numeric-comma', targets: [6, 7, 9, 10, 11] },
		        { 
                    "class": "left", targets: [4, 17]
                },
                { 
                    "visible": false, targets: hide_columns
                },
            ],
            "createdRow" : function( row, data, index ) {

                $(row).addClass('selectable');

                if (data.type_move == 'debt') {

                    row.id = 'debt_' +  data.id;

                    if ($.inArray('debt_' +  data.id , rows_selected_ids) != -1) {

                        $(row).addClass('selected');
                        $(row).find('.checkbox-select').removeClass('icon-checkbox-unchecked');
                        $(row).find('.checkbox-select').addClass('icon-checkbox-checked');
                    }

                    if (data.customers_has_discount) {

                        var row = table_debts.row($(row));
                        row.child( format( data ) ).show();
                    }

                } else {

                    row.id = 'discount_' +  data.id;

                    if ($.inArray(  'discount_' +  data.id , rows_selected_ids) != -1) {

                        $(row).addClass('selected');
                        $(row).find('.checkbox-select').removeClass('icon-checkbox-unchecked');
                        $(row).find('.checkbox-select').addClass('icon-checkbox-checked');
                    }
                }
            },
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 200, 300, 400, 500], [100, 200, 300, 400, 500]],
        	"dom":
    	    	"<'row'<'col-xl-3'l><'col-xl-3 mt-2  total_selected'><'col-xl-4'f><'col-xl-2 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
		});

		function format ( d ) {

		    html = '<div class="row row p-0 mt-0 mr-0 mb-0 ml-1">';
                html += '<div class="col-xl-10 text-left " style="">';

                    html += '<span class="glyphicon icon-level-down"  aria-hidden="true"></span> &nbsp;&nbsp;';
                    html += d.customers_has_discount.description;
                    html += '&nbsp;&nbsp;&nbsp;&nbsp;';
                    html += number_format(-d.customers_has_discount.total, true);
                    
                html += '</div>';
            html += '</div>';

            return html;
        }

		var users = <?= json_encode($users) ?>;

		$('.btn-update-table').click(function() {

             if (table_debts) {
                table_debts.ajax.reload();
            }
        });

        $('#table-debts_wrapper .tools').append($('#btns-tools').contents());

        $('#table-debts_wrapper .total_selected').html('<div><label for="">Seleccionado:</label><span id="total_selected_value" class="ml-2 " style="color: #f05f40;">$0,00</span></div>');

        $('#btns-tools').show();

        var select_all_activate = false;

        var clear_all_activate = false;

		$('#table-debts').on( 'init.dt', function () {

    	    createModalHideColumn(table_debts, '.modal-hide-columns-debts');
            createModalSearchColumn(table_debts, '.modal-search-columns-debts');
            loadSearchPreferences('invoices-generate-masive-from-debt-search');

	        $( "#table-debts tbody" ).on( "click",  ".checkbox-select", function() {

		        var data = table_debts.row( $(this).closest('tr') ).data();

		        if (data.type_move == 'debt') {
		            debt_selected = table_debts.row( $(this).closest('tr') ).data();
		        } else {
		            discount_selected = table_debts.row( $(this).closest('tr') ).data();
		        }

                if (!$(this).closest('tr').hasClass('selected')) {

                    if (!clear_all_activate) {

                        $(this).closest('tr').addClass('selected');

                        total_selected_value += data.total;

                        if (data.customers_has_discount) {
                            total_selected_value -= data.customers_has_discount.total;
                        }

                        rows_selected_ids.push($(this).closest('tr').attr('id')); 

                        $(this).removeClass('icon-checkbox-unchecked');
                        $(this).addClass('icon-checkbox-checked');
                    }

                } else {

                    if (!select_all_activate) {

                        $(this).closest('tr').removeClass('selected');

                        total_selected_value -= data.total;

                        if (data.customers_has_discount) {
                            total_selected_value += data.customers_has_discount.total;
                        }

                        var attr_id = $(this).closest('tr').attr('id');

                        rows_selected_ids = rows_selected_ids.filter(function(item) { 
                            return item !== attr_id 
                        });

                        $(this).removeClass('icon-checkbox-checked');
                        $(this).addClass('icon-checkbox-unchecked');
                    }
                }

                $('#total_selected_value').html(number_format(total_selected_value, true));
            });

            $( "#table-debts tbody" ).on( "click",  ".row-control", function() {

                move_selected = table_debts.row( $(this).closest('tr') ).data();

                if (move_selected.type_move == 'debt') {

		            debt_selected = move_selected;

		            $('.modal-actions-debts #btn-edit-discount').hide();
		            $('.modal-actions-debts #btn-edit-debt').show();

		        } else {

		            discount_selected = move_selected

		            $('.modal-actions-debts #btn-edit-discount').show();
		            $('.modal-actions-debts #btn-edit-debt').hide();
		        }

                $('.modal-actions-debts').modal('show');
            });
        });

        $('#btn-delete').click(function() {

            $('.modal-actions-debts').modal('hide');

            switch (move_selected.type_move) {
                case 'debt':

                    bootbox.confirm("¿Está Seguro que desea eliminar la deuda?", function(result) {

                        if (result) {

                            var request = $.ajax({
                                url: "<?= $this->Url->build(['controller' => 'Debts', 'action' => 'delete']) ?>",  
                                type: 'POST',
                                dataType: "json",
                                data: JSON.stringify(move_selected),
                            });

                            request.done(function( data ) {
                              table_debts.draw();
                            });

                            request.fail(function( jqXHR, textStatus ) {
                              generateNoty('error', 'Error al intenetar eliminar la deuda');
                            });
                        }
                    });

                    break;

                case 'customer_has_discount':

                    bootbox.confirm("¿Está Seguro que desea eliminar el descuento?", function(result) {

                        if (result) {

                            var request = $.ajax({
                                url: "<?= $this->Url->build(['controller' => 'CustomersHasDiscounts', 'action' => 'delete']) ?>",  
                                type: 'POST',
                                dataType: "json",
                                data: JSON.stringify(move_selected),
                            });

                            request.done(function( data ) {
                              table_debts.draw();
                            });

                            request.fail(function( jqXHR, textStatus ) {
                              generateNoty('error', 'Error al intenetar eliminar el descuento');
                            });
                        }
                    });

                    break;
            }
        });

        $('#btn-delete-masive').click(function() {

            $('.modal-actions-masive').modal('hide');

            bootbox.confirm("¿Está Seguro que desea eliminar todo lo seleccionado?", function(result) {

                if (result) {

                    var ids_debts = [];
                    var ids_discounts = [];

                    $.each(rows_selected_ids, function( index, value ) {
                        if (value.indexOf("discount_") >= 0) {
                            value = value.replace('discount_', '');
                            ids_discounts.push(value);
                        }
                        if (value.indexOf("debt_") >= 0) {
                            value = value.replace('debt_', '');
                            ids_debts.push(value);
                        }
                    });

                    if (ids_debts.length > 0) {

                        var request = $.ajax({
                            url: "<?= $this->Url->build(['controller' => 'Debts', 'action' => 'deleteMasive']) ?>",  
                            type: 'POST',
                            dataType: "json",
                            data: JSON.stringify(ids_debts),
                        });

                        request.done(function( data ) {
                            generateNoty('success', 'Se han elimnado correctamente las deudas seleccionadas');
                            table_debts.draw();
                        });

                        request.fail(function( jqXHR, textStatus ) {
                            generateNoty('error', 'Error al intenetar eliminar las deudas');
                        });
                    }

                    if (ids_discounts.length > 0) {

                        var request = $.ajax({
                            url: "<?= $this->Url->build(['controller' => 'CustomersHasDiscounts', 'action' => 'deleteMasive']) ?>",  
                            type: 'POST',
                            dataType: "json",
                            data: JSON.stringify(ids_discounts),
                        });

                        request.done(function( data ) {
                            generateNoty('success', 'Se han elimnado correctamente los descuentos seleccionados');
                            table_debts.draw();
                        });

                        request.fail(function( jqXHR, textStatus ) {
                            generateNoty('error', 'Error al intenetar eliminar los descuentos');
                        });
                    }

                    rows_selected_ids = [];
                }
            });
        });

        $("#period-datetimepicker").on("dp.change", function (e) {
            table_debts.ajax.reload();
        });

        $('.btn-confirm').click(function() {
            $('.confirm-invoice-modal').modal('show');
        });

        $('.btn-selected-all').click(function() {
            select_all_activate = true;
            $('.checkbox-select').click();
            select_all_activate = false;
        });

        $('.btn-clear-selected-all').click(function() {
            clear_all_activate = true;
            $('.checkbox-select').click();
            clear_all_activate = false;
        });

        $('.btn-filter-period').click(function() {
             $('#modal-filter-period').modal('show');
        });

        $('#btn-go-customer').click(function() {
            var move = discount_selected;
            if (move_selected.type_move == 'debt') {
                move = debt_selected;
            }
            var action = '/ispbrain/customers/view/' + move.customer.code;
            window.open(action, '_self');
        });

        $('.btn-action').click(function() {
            if (rows_selected_ids.length > 0) {
                $('.modal-actions-masive').modal('show');
            } else {
                bootbox.alert('Debe seleccionar al menos una deuda para continuar.');
            }
        });

        $(".btn-export").click(function() {

            bootbox.confirm('Se descargara un archivo Excel', function(result) {
                if (result) {

                   $('#table-debts').tableExport({
                       tableName: 'Debts',
                       type:'excel',
                       escape:'false',
                       columnNumber: [6, 7, 9, 10, 11]
                   });
                } 
            });
        });

        $('#concept_type').change(function() {

            if ($(this).val() == 1) {
                $('#table-period').hide();
            } else {
                $('#table-period').show();
            }
        });

        $(document).on("click", ".btn-generate", function(e) {

            if (rows_selected_ids.length > 0) {

                var d = new Date(); 
                var created = $('.confirm-invoice-modal #created').val().split('/');

                var h = addZero(d.getHours());
                var m = addZero(d.getMinutes());
                var s = addZero(d.getSeconds());

                created = created[2] + '-' + created[1] + '-' + created[0] + ' ' + h + ':' + m + ':' + s;

                var duedate = $('.confirm-invoice-modal #duedate').val().split('/');
                duedate = duedate[2] + '-' + duedate[1] + '-' + duedate[0] + ' ' + h + ':'+ m + ':' + s;

                var date_start = $('.confirm-invoice-modal #date_start').val().split('/');
                date_start = date_start[2] + '-' + date_start[1] + '-' + date_start[0] + ' ' + h + ':' + m + ':' + s;

                var date_end = $('.confirm-invoice-modal #date_end').val().split('/');
                date_end = date_end[2] + '-' + date_end[1] + '-' + date_end[0] + ' ' + h + ':' + m + ':' + s;

                var data = {
                    ids: rows_selected_ids,
                    date: created,
                    duedate: duedate,
                    concept_type: parseInt($('.confirm-invoice-modal #concept_type').val()),
                    date_start: date_start,
                    date_end: date_end
                };

                created = new Date(data.date);
                duedate = new Date(data.duedate);
                date_start = new Date(data.date_start);
                date_end = new Date(data.date_end);

                var now = new Date();
                now.setHours(0);
                now.setMinutes(0);
                now.setSeconds(0);

                created.setHours(0);
                created.setMinutes(0);
                created.setSeconds(0);

                /*if (created > now) {
                    generateNoty('warning', 'La fecha de la factura no puede ser mayor a la fecha actual.');
                    return false;
                }*/

                if (duedate < created) {
                    generateNoty('warning', 'La fecha de vencimiento no puede ser menor a la fecha actual.');
                    return false;
                }

                if (data.concept_type != 1 && date_end < date_start) {
                    generateNoty('warning', 'Las fechas del período no son correctas.');
                    return false;
                }

                bootbox.confirm('Se generarán las facturas de los conceptos seleccionados. Cada factura de cada cliente tendrá la lista de conceptos en el orden que fueron realizados. ¿Desea continuar?', function(result) {
                    if (result) {

                        $('body')
                            .append( $('<form/>').attr({'action': '/ispbrain/invoices/generateMasiveFromDebts/', 'method': 'post', 'id': 'form-block'})
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'data', 'value': JSON.stringify(data)}))
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'hash', 'value': hash}))
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                            .find('#form-block').submit();

                        openModalPreloader("Generando Facturas. Espere Por favor ...");
                    }
                });

            } else {
                bootbox.alert('Debe seleccionar al menos una deuda para continuar.');
            }
        });
    });

    function addZero(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

</script>
