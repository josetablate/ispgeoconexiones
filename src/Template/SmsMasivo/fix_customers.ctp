<style type="text/css">

    .status{
        border-radius: 4px;
        display: inline-block;
        font-weight: bold;
        color: white;
        line-height: 1em;
        font-size: 10.5px;
        padding: 3px 0;
        margin: 0px 0px 0px 0px;
        width: 100%;
         
    }

    .cp {
        border: 1px solid #f38a73;
        background-color: #f38a73;
    }
    
    .cc {
        border: 1px solid gray;
        background-color: gray;    
    }

    .i {
        border: 1px solid purple;
        background-color: purple;
    }

    .plus1M {
        border: 1px solid #f05f40;
        background-color: #f05f40;
        width: 80%;
    }

    .plus2M {
        border: 1px solid #f05f40;
        background-color: #f05f40;
        width: 80%;
    }

    .plus3M {
        border: 1px solid #f05f40;
        background-color: #f05f40;
        width: 80%;    
    }

    #modal-filter .form-group {
        margin-bottom: 4px;
    }

    .my-hidden {
        display: none;
    }

</style>

<?php

$status = [
    'plus1M' => ['class' => 'plus1M', 'text' => '+1 Mes'],
    'plus2M' => ['class' => 'plus2M', 'text' => '+2 Meses'],
    'plus3M' => ['class' => 'plus3M', 'text' => '+3 Meses'],
    'CP' => ['class' => 'cp', 'text' => 'Conexión Pendiente'],
    'CC' => ['class' => 'cc', 'text' => 'Conexión Creada'],
    'I' => ['class' => 'i', 'text' => 'Instalación Registrada']
];

?>

<div class="modal fade help-phone-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabel">Información</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="list-unstyled" style="line-height: 2">
                            <li><span class="fa fa-check text-success"></span> <?= __('Formato del teléfono: 10 dígitos y numérico') ?></li>
                            <li><span class="fa fa-check text-success"></span> <?= __('La útima columna teléfono, se debe ingresar el teléfono en el formato correcto y presionar el botón verde con la tilde para modificar los teléfonos.') ?></li>
                        </ul>
                    	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5">
        <?php
         

            echo $this->Html->link(
                '<span class="glyphicon icon-eye" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Ocultar o Mostrar Columnas',
                'class' => 'btn btn-default btn-hide-column ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-filter" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Filtrar por Columnas',
                'class' => 'btn btn-default btn-filter-column ml-1',
                'data-toggle' => 'modal', 'data-target' => '#modal-filter',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel"  aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="far fa-save" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Guardar Cambios',
                'class' => 'btn btn-outline-info btn-edit ml-1',
                'escape' => false
            ]);
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-hover" id="table-customers">
            <thead>
                <tr>
                    <th >Código</th>            <!--0-->
                    <th >Documento</th>         <!--1-->
                    <th >Nombre</th>            <!--2-->
                    <th >Domicilio</th>         <!--3-->
                    <th >Ciudad o Área</th>     <!--4-->
                    <th >Teléfono</th>          <!--5-->
                    <th title="Formato de teléfono: sin el CERO inicial, sin el 15 y sin espacios. En total 10 dígitos. Únicamente números sin: '-' o '+'" data-toggle="tooltip">Modificar <i class="far fa-question-circle"  ></i></th> <!--6-->
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="modal-filter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Filtrar por columnas</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <?= $this->Form->create() ?>

                <div class="row">

                    <div class="col-md-12">
                        <?= $this->Form->input(null, ['label' => __('Código'), 'class'=> 'column_search', 'data-column' => 0]) ?>
                        <?= $this->Form->input(null, ['label' => __('Nombre'), 'class'=> 'column_search', 'data-column' => 2]) ?>
                        <?= $this->Form->input(null, ['label' => __('Documento'), 'class'=> 'column_search', 'data-column' => 1]) ?>
                        <?= $this->Form->input(null, ['label' => __('Teléfono'), 'class'=> 'column_search', 'data-column' => 5]) ?>
                    </div>

                </div>

                <?= $this->Form->end() ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-clear-filter" >Limpiar</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<?php 

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-go-customer',
        'name' =>  'Ficha del Cliente',
        'icon' =>  'glyphicon icon-profile',
        'type' =>  'btn-info'
    ];

    echo $this->element('actions', ['modal'=> 'modal-customers', 'title' => 'Acciones', 'buttons' => $buttons ]);
    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-customers']);
?>

<script type="text/javascript">

    var table_customers = null;
    var customer_selected = null;

    $(document).ready(function () {

        $('[data-toggle="tooltip"]').tooltip();

        $('.btn-hide-column').click(function() {

           $('.modal-hide-columns-customers').modal('show');

        });

        $('#table-customers').removeClass('display');

        $('#btns-tools').hide();

		table_customers = $('#table-customers').DataTable({
		    "order": [[ 2, 'desc' ]],
            "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/SmsMasivo/fixCustomers.json",
                "dataSrc": "customers",
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
            				  window.location.href ="/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
		    "scrollY": true,
		    "scrollY": '450px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
		    "columns": [
                { 
                    "data": "code",
                    "className": 'row-control',
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                { 
                    "data": "ident",
                    "className": 'row-control',
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (data) {
                            ident = data;
                        }
                        return sessionPHP.afip_codes.doc_types[row.doc_type] + ' ' + ident;
                    }
                },
                { 
                    "data": "name",
                    "className": 'row-control'
                },
                { 
                    "data": "address",
                    "className": 'row-control left'
                },
                { 
                    "data": "area",
                    "className": 'row-control',
                    "render": function ( data, type, row ) {
                        return data.name;
                    }
                },
                { 
                    "data": "phone",
                    "className": 'row-control'
                },
                {
                    "data": "",
                    "className": 'phone',
                    "render": function ( data, type, row ) {
                        return "<input style='width: auto;' class='form-control' placeholder='Ingrese el Teléfono...' type='number' max='10' maxlength='10'/>";
                    }
                }
            ],
		    "columnDefs": [
		        {
		            "targets": [0, 6],
                    "width": '5%'
                },
                { 
                    "class": "left", targets: [2, 3, 4]
                },
            ],
             "createdRow" : function( row, data, index ) {
                row.id = data.code;
            },

		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],

        	"dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
   
		});

        $('#table-customers_wrapper .tools').append($('#btns-tools').contents());

        $('#table-customers').on( 'init.dt', function () {
            createModalHideColumn(table_customers, '.modal-hide-columns-customers');
        });

        $('#btns-tools').show();

        $('#table-customers tbody').on( 'click', '.row-control', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                table_customers.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');

                customer_selected = table_customers.row( this ).data();

                $('.modal-customers').modal('show');
            }
        });

        $('.column_search').on( 'keyup', function () {

            table_customers
                .columns( $(this).data('column') )
                .search( this.value )
                .draw();

            checkStatusFilter();

        });

        $('input:checkbox.column_search').change(function() {

            table_customers
                .columns( $(this).data('column') )
                .search( $(this).is(":checked") )
                .draw();

            checkStatusFilter();
        });

        $('.btn-clear-filter').click(function(){
            $('.column_search').val('');
            table_customers.search( '' ).columns().search( '' ).draw();

            checkStatusFilter();
        });
	
    });

    $('#btn-go-customer').click(function() {
            var action = '/ispbrain/customers/view/' + customer_selected.code;
            window.open(action, '_blank');
        });

    $(".btn-export").click(function() {

        bootbox.confirm('Se descargará un archivo Excel', function(result) {
            if (result) {
                $('#table-customers').tableExport({tableName: 'Clientes', type:'excel', escape:'false', columnNumber: []});
            }
        }).find("div.modal-content").addClass("confirm-width-md");

    });

    $('.btn-phone-help').click(function() {
        $('.help-phone-modal').modal('show');
    });

    $(".btn-edit").click(function() {

        var count = 0;
        var count_format_invalid = 0;
        var customers = [];
        var no_customers = [];

        $('#table-customers tbody tr').each(function(index, value) {

            var phone = $(this).find('td.phone').find('input').val();

            if (phone != '') {
                var id = value.id;
    
                if (phone.length == 10) {
                    var customer = {
                        id: id,
                        phone: phone
                    }
                    customers.push(customer);
                } else {
                    count_format_invalid++;
                    var customer = {
                        id: id,
                        phone: phone
                    }
                    no_customers.push(customer);
                }
            }

        });

        if (customers.length > 0) {

            var text = "¿Está Seguro que desea modificar los Teléfonos?";

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/SmsMasivo/fixCustomers/', 'method': 'post', 'id': 'replacer'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'no_customers', 'value': JSON.stringify(no_customers)}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'customers', 'value': JSON.stringify(customers)}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer').submit();
                }
            });
        } else {
            if (count_format_invalid > 0) {
                generateNoty('warning','Cantidad de Teléfonos con formato inválido: ' + count_format_invalid);
            } else {
                generateNoty('warning','Sin modificación de Teléfonos');
            }
        }

    });

</script>
