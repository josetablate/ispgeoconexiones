<style type="text/css">

    .my-hidden {
        display: none;
    }

</style>

<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5">

        <?php

            echo $this->Html->link(
                '<span class="glyphicon fas fa-search" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Buscar por Columnas',
                'class' => 'btn btn-default btn-search-column ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-eye" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Ocultar o Mostrar Columnas',
                'class' => 'btn btn-default btn-hide-column ml-2',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar en formato excel el contenido de la Tabla',
                'class' => 'btn btn-default btn-export ml-2',
                'escape' => false
            ]);
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-hover" id="table-sms" >
            <thead>
                <tr>
                    <th><?= ('Plantilla') ?></th>       <!--0-->
                    <th><?= ('Bloque') ?></th>          <!--1-->
                    <th><?= ('Mensaje') ?></th>         <!--2-->
                    <th><?= ('Estado') ?></th>          <!--3-->
                    <th><?= ('Envio') ?></th>           <!--4-->
                    <th><?= ('Cliente') ?></th>         <!--5-->
                    <th><?= ('Documento') ?></th>       <!--6-->
                    <th><?= ('Código') ?></th>          <!--7-->
                    <th><?= ('Teléfono') ?></th>        <!--8-->
                    <th><?= ('Empresa') ?></th>         <!--9-->
                    <th><?= ('Usuario') ?></th>         <!--10-->
                    <th><?= ('Plataforma') ?></th>      <!--11-->
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade confirm-send-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabel">Confirmar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label>Se archivarán los correos filtrados. ¿Desea continuar?</label>

                        <div class="row">
                            <div class="col-md-4">
                        	    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        	</div>
                        	<div class="col-md-4">
                                <button type="button" class="btn btn-danger btn-confirm-send float-right" value="messages">Confirmar y Continuar</button>
                            </div>
                            <div class="col-md-4">
                                <button type="button" class="btn btn-info btn-confirm-send float-right" value="archived_messages">Confirmar y Salir</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-go-customer',
        'name' =>  'Ficha del Cliente',
        'icon' =>  'glyphicon icon-profile',
        'type' =>  'btn-primary'
    ];

    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-sms']);
    echo $this->element('actions', ['modal'=> 'modal-sms', 'title' => 'Acciones', 'buttons' => $buttons ]);
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-sms']);
?>

<script type="text/javascript">

    var table_sms = null;
    var sms_selected = null;
    var sms_selected_ids = null;
    var users = null;
    var platforms = null;
    var templates = null;

    $(document).ready(function () {

        users = <?= json_encode($users) ?>;
        platforms = <?= json_encode($platforms) ?>;
        templates = <?= json_encode($templates) ?>;

        $('.btn-hide-column').click(function() {
            $('.modal-hide-columns-sms').modal('show');
        });

        $('.btn-search-column').click(function() {
           $('.modal-search-columns-sms').modal('show');
        });

        $('#table-sms').removeClass('display');
        
        $('#btns-tools').hide();

        loadPreferences('sms-archived-message', [0, 1, 3, 4, 5, 6 , 7, 8, 9, 10]);

		table_sms = $('#table-sms').DataTable({
		    "order": [[ 1, 'desc' ]],
            "deferRender": true,
            "processing": true,
            "serverSide": true,
		    "ajax": {
                "url": "/ispbrain/SmsMasivo/getMessages.json",
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "archived": true
                    });
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "drawCallback": function( settings ) {
                var flag = true;
            	switch (settings.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
            	    if (table_sms) {

                        var tableinfo = table_sms.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }
                    }
            	}
            },
		    "scrollY": true,
		    "scrollY": '450px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
		    "columns": [
                { 
                    "className": "",
                    "data": "sms_bloque.template_name",
                    "type": "options",
                    "options": templates,
                    "render": function ( data, type, row ) {
                        return  data;
                    }
                },
                { 
                    "className": "",
                    "data": "sms_bloque.id",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return  data;
                    }
                },
                { 
                    "className": "",
                    "data": "message",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        return  data;
                    }
                },
                { 
                    "className": "",
                    "data": "status",
                    "type": "options",
                    "options": {
                        'OK': 'OK',
                        'PENDIENTE': 'PENDIENTE',
                        'ENCOLADO': 'ENCOLADO',
                        'ERROR': 'ERROR'
                    },
                    "render": function ( data, type, row ) {
                        return  data;
                    }
                },
                {
                    "className": " text-info",
                    "data": "sms_bloque.created",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            var hours = data.split('T')[1].split('-')[0].split(':');
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0] + ' ' + hours[0] + ':' + hours[1];
                        }
                    }
                },
                { 
                    "className": "",
                    "data": "customer.name",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        return  data;
                    }
                },
                { 
                    "className": " left",
                    "data": "customer.ident",
                    "type": "ident",
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (data) {
                            ident = data;
                        }
                        return sessionPHP.afip_codes.doc_types[row.customer.doc_type] + ' ' + ident;
                    }
                },
                { 
                    "className": "",
                    "data": "customer.code",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                { 
                    "className": "",
                    "data": "phone",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        return data;
                    }
                },
                {
                    "className": "",
                    "data": "business_billing",
                    "type": "options_obj",
                    "options": sessionPHP.paraments.invoicing.business,
                    "render": function ( data, type, row ) {
                        var business_name = '';
                        $.each(sessionPHP.paraments.invoicing.business, function (i, b) {
                            if (b.id == data) {
                                business_name = b.name;
                            }
                        });
                        return business_name;
                    }
                },
                { 
                    "className": "",
                    "data": "sms_bloque.user_id",
                    "type": "options",
                    "options": users,
                    "render": function ( data, type, row ) {
                        return users[data];
                    }
                },
                {
                    "className": "",
                    "data": "sms_bloque.platform",
                    "type": "options",
                    "options": platforms,
                    "render": function ( data, type, row ) {
                        $platform_name = "";
                        if (data) {
                            $platform_name = sessionPHP.paraments.sms.platform[data].name;
                        }
                        return $platform_name;
                    }
                }
            ],
		    "columnDefs": [
                { 
                    "visible": false, targets: hide_columns
                },
            ],
             "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
	
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[50, 100, 500, -1], [50, 100, 500, "Todas"]],
            
        	"dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
   
		});

		$('#table-sms_wrapper .tools').append($('#btns-tools').contents());

		$('#btns-tools').show();

        $('#table-sms').on( 'init.dt', function () {

            createModalHideColumn(table_sms, '.modal-hide-columns-sms');
		    createModalSearchColumn(table_sms, '.modal-search-columns-sms');

            $('#table-sms tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {

                    table_sms.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    sms_selected = table_sms.row( this ).data();

                    $('.modal-sms').modal('show');
                }
            });
        });

        $('#btn-go-customer').click(function() {
            var action = '/ispbrain/customers/view/' + sms_selected.customer.code;
            window.open(action, '_blank');
        });

        $(".btn-export").click(function() {
            bootbox.confirm('Se descargará un archivo Excel', function(result) {
                if (result) {
                    $('#table-sms').tableExport({tableName: 'Mensajes', type:'excel', escape:'false', columnNumber: [4]});
                }
            }).find("div.modal-content").addClass("confirm-width-md");
        });

    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
