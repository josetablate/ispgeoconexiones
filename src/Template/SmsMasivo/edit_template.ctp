<style type="text/css">

    legend {
        display: block;
        width: 100%;
        padding: 0;
        margin-bottom: 20px;
        font-size: 21px;
        line-height: inherit;
        color: #333;
        border: 0;
        border-bottom: 1px solid #e5e5e5;
    }

    .title {
        margin-left: 0px;
        margin-right: 0px;
        margin-bottom: 10px;
    }

    .title label.label-control {
        margin: 3px 0 3px 0;
    }

    .title label.label-control {
        margin: 3px 0 3px 0;
    }

    textarea.form-control {
        height: 80px !important;
    }

    span.red {
        color: red;
    }
    
    .lbl-check-tag {
        margin-left: 8px;
    }

    .textarea {
        margin-bottom: 0px;
    }

    #textarea_feedback {
        font-family: monospace;
        font-weight: bold;
        color: #696868;
        font-size: 15px;
    }

    #message-template {
        height: 149px !important;
    }

</style>

<div class="alert alert-info alert-dismissible fade show" role="alert">
    <i class="fas fa-info-circle item-accordion-icon text-info"></i> Las telefónicas están detectando como spam las siguientes palabras, sus derivados o palabras que contengan parte del contenido mencionado a continuación: financiar, sorteo, plan, 0 km, Crédito, autoahorro, autoplan, marca de vehículos (todas), concesionario, promoción, código de validación, preaprobado, directv, oportunidad. Tener en cuenta la platnilla debe ser texto plano, es decir texto sin formato ni salto de lineas.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<div class="row">
   
    <div class="col-md-8">
        <?= $this->Form->create($template, ['class' => 'form-load', 'id' => 'form-sms-masivo-edit-template']) ?>
        <fieldset>

            <div class="row">
                <div class="col-md-4">
                    <label for="name" class="label-control"><span class="red">*</span><?= __('Nombre') ?></label>
                    <?php 
                        echo $this->Form->text('name', ['required' => true] );
                        echo $this->Form->input('description', ['type' => 'textarea', 'rows' => 2, 'label' => __('Descripción')]);
                    ?>
                    <label for="name" class="label-control mt-3"><span class="red">*</span><?= __('Empresa') ?></label>
                    <?php 
                        echo $this->Form->input('business_billing', ['options' => $business,  'label' =>  '', 'required' => true, 'default' => $template->business_billing]);
                    ?>
                    <button type="submit" class="btn btn-success mt-3"><?= __('Modificar') ?></button>
                </div>
                <div class="col-md-8">
                    <?php
                        echo $this->Form->input('message', ['id' => 'message-template', 'label' => 'Mensaje', 'type' => 'textarea', 'rows' => 10, 'cols' => 80, 'maxlength' => 160, 'required' => 'required']);
                    ?>
                    <div class="float-right" id="textarea_feedback"></div>
                </div>
            </div>

        </fieldset>

        <?= $this->Form->end() ?>
    </div>
    <div class="col-md-4">
        <legend class="lead sub-title"><?= __('Marcas para poner los') ?> <span class="text-info"><?= __('Valores') ?></span> <a class="btn-help-sms" href="javascript:void(0)"><i class="fas fa-question-circle"></i></a></legend>
        <ul class="list-unstyled" style="line-height: 2">
            <li>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="customer-name">
                    <label class="form-check-label lbl-check-tag" for="customer-name">%cliente_nombre%</label>
                </div>
            </li>
            <li>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="customer-code">
                    <label class="form-check-label lbl-check-tag" for="customer-code">%cliente_codigo%</label>
                </div>
            </li>
            <li>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="customer-doc">
                    <label class="form-check-label lbl-check-tag" for="customer-doc">%cliente_documento%</label>
                </div>
            </li>
            <li>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="customer-portal-user">
                    <label class="form-check-label lbl-check-tag" for="customer-portal-user">%cliente_usuario_portal%</label>
                </div>
            </li>
            <li>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="customer-portal-pass">
                    <label class="form-check-label lbl-check-tag" for="customer-portal-pass">%cliente_clave_portal%</label>
                </div>
            </li>
            <li>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="customer-balance">
                    <label class="form-check-label lbl-check-tag" for="customer-balance">%cliente_saldo_total%</label>
                </div>
            </li>
            <li>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="customer-month-balance">
                    <label class="form-check-label lbl-check-tag" for="customer-month-balance">%cliente_saldo_mes%</label>
                </div>
            </li>
            <li>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="enterprise-name">
                    <label class="form-check-label lbl-check-tag" for="enterprise-name">%empresa_nombre%</label>
                </div>
            </li>
            <li>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="enterprise-address">
                    <label class="form-check-label lbl-check-tag" for="enterprise-address">%empresa_domicilio%</label>
                </div>
            </li>
            <li>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="enterprise-web">
                    <label class="form-check-label lbl-check-tag" for="enterprise-web">%empresa_web%</label>
                </div>
            </li>
        </ul>
    </div>
</div>

<div class="modal fade help-sms-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabel">Consejos</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="list-unstyled" style="line-height: 2">
                            <li><span class="fa fa-check text-success"></span> <?= __('Para que en el mensaje aparezca el nombre del cliente se debe tildar: <strong>%cliente_nombre%</strong> dicha marca luego sera reemplazada por el nombre del cliente y así con cualquiera de las marcas que desee que aparezcan en el mensaje.') ?></li>
                            <li><span class="fa fa-check text-success"></span> <?= __('Se tiene un limite de 160 caracteres por mensaje') ?></li>
                            <li><span class="fa fa-check text-success"></span> <?= __('Los caracteres permitidos son: ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!?#$%()*+, -./:;=@ y el espacio') ?></li>
                            <li><span class="fa fa-check text-success"></span> <?= __('NOTA: Recuerde que, predeterminadamente, todos los usuarios de SMS Masivos cuentan con una franja horaria habilitada para realizar los envíos (desde las 08:00hs hasta las 22:00hs). Si precisa enviar mensajes las 24hs, por favor, póngase en contacto con el departamento de soporte.') ?></li>
                        </ul>
                    	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    var editor = null;
    var text_remaining = <?= json_encode($num_characters) ?>;

    $(document).ready(function() {

        var message = $('#message-template').val();
        var text_max = parseInt($('#message-template').attr('maxlength'));

        if (message.indexOf("%cliente_nombre%") >= 0) {
            $('#customer-name').attr('checked','checked');
            text_max = parseInt($('#message-template').attr('maxlength')) - 4;
            $("#message-template").attr('maxlength', text_max);
        }

        if (message.indexOf("%cliente_codigo%") >= 0) {
            $('#customer-code').attr('checked','checked');
            text_max = parseInt($('#message-template').attr('maxlength')) + 5;
            $("#message-template").attr('maxlength', text_max);
        }

        if (message.indexOf("%cliente_documento%") >= 0) {
            $('#customer-doc').attr('checked','checked');
            text_max = parseInt($('#message-template').attr('maxlength')) + 3;
            $("#message-template").attr('maxlength', text_max);
        }

        if (message.indexOf("%cliente_usuario_portal%") >= 0) {
            $('#customer-portal-user').attr('checked','checked');
            text_max = parseInt($('#message-template').attr('maxlength')) + 13;
            $("#message-template").attr('maxlength', text_max);
        }

        if (message.indexOf("%cliente_clave_portal%") >= 0) {
            $('#customer-portal-pass').attr('checked','checked');
            text_max = parseInt($('#message-template').attr('maxlength')) + 12;
            $("#message-template").attr('maxlength', text_max);
        }

        if (message.indexOf("%cliente_saldo_total%") >= 0) {
            $('#customer-balance').attr('checked','checked');
            text_max = parseInt($('#message-template').attr('maxlength')) + 11;
            $("#message-template").attr('maxlength', text_max);
        }

        if (message.indexOf("%cliente_saldo_mes%") >= 0) {
            $('#customer-month-balance').attr('checked','checked');
            text_max = parseInt($('#message-template').attr('maxlength')) + 9;
            $("#message-template").attr('maxlength', text_max);
        }

        if (message.indexOf("%empresa_nombre%") >= 0) {
            $('#enterprise-name').attr('checked','checked');
            text_max = parseInt($('#message-template').attr('maxlength')) - 4;
            $("#message-template").attr('maxlength', text_max);
        }

        if (message.indexOf("%empresa_domicilio%") >= 0) {
            $('#enterprise-address').attr('checked','checked');
            text_max = parseInt($('#message-template').attr('maxlength')) - 1;
            $("#message-template").attr('maxlength', text_max);
        }

        if (message.indexOf("%empresa_web%") >= 0) {
            $('#enterprise-web').attr('checked','checked');
            text_max = parseInt($('#message-template').attr('maxlength')) - 7;
            $("#message-template").attr('maxlength', text_max);
        }

        text_remaining = parseInt($('#message-template').attr('maxlength')) - $('#message-template').val().length;

        if (text_remaining <= 10) {
            $('#textarea_feedback').addClass('text-danger');
        } else {
            $('#textarea_feedback').removeClass('text-danger');
        }

        $('#textarea_feedback').html(text_remaining + ' caracteres restantes');

        $('#message-template').keyup(function() {

            var texto = $('#message-template').val();

            if (texto.indexOf("%cliente_nombre%") > -1) {
                if (!$('#customer-name').is(':checked')) {
                    text_max = parseInt($('#message-template').attr('maxlength')) - 4;
                    $("#message-template").attr('maxlength', text_max);
                    $('#customer-name').prop('checked', true);
                }
            }

            if ($('#customer-name').is(':checked') && texto.indexOf("%cliente_nombre%") == -1) {
                text_max = parseInt($('#message-template').attr('maxlength')) + 4;
                $("#message-template").attr('maxlength', text_max);
                $('#customer-name').prop('checked', false);
            }

            if (texto.indexOf("%cliente_codigo%") > -1) {
                if (!$('#customer-code').is(':checked')) {
                    text_max = parseInt($('#message-template').attr('maxlength')) + 5;
                    $("#message-template").attr('maxlength', text_max);
                    $('#customer-code').prop('checked', true);
                }
            }

            if ($('#customer-code').is(':checked') && texto.indexOf("%cliente_codigo%") == -1) {
                text_max = parseInt($('#message-template').attr('maxlength')) - 5;
                $("#message-template").attr('maxlength', text_max);
                $('#customer-code').prop('checked', false);
            }

            if (texto.indexOf("%cliente_documento%") > -1) {
                if (!$('#customer-doc').is(':checked')) {
                    text_max = parseInt($('#message-template').attr('maxlength')) + 3;
                    $("#message-template").attr('maxlength', text_max);
                    $('#customer-doc').prop('checked', true);
                }
            }

            if ($('#customer-doc').is(':checked') && texto.indexOf("%cliente_documento%") == -1) {
                text_max = parseInt($('#message-template').attr('maxlength')) - 3;
                $("#message-template").attr('maxlength', text_max);
                $('#customer-doc').prop('checked', false);
            }

            if (texto.indexOf("%cliente_usuario_portal%") > -1) {
                if (!$('#customer-portal-user').is(':checked')) {
                    text_max = parseInt($('#message-template').attr('maxlength')) + 13;
                    $("#message-template").attr('maxlength', text_max);
                    $('#customer-portal-user').prop('checked', true);
                }
            }

            if ($('#customer-portal-user').is(':checked') && texto.indexOf("%cliente_usuario_portal%") == -1) {
                text_max = parseInt($('#message-template').attr('maxlength')) - 13;
                $("#message-template").attr('maxlength', text_max);
                $('#customer-portal-user').prop('checked', false);
            }

            if (texto.indexOf("%cliente_clave_portal%") > -1) {
                if (!$('#customer-portal-pass').is(':checked')) {
                    text_max = parseInt($('#message-template').attr('maxlength')) + 12;
                    $("#message-template").attr('maxlength', text_max);
                    $('#customer-portal-pass').prop('checked', true);
                }
            }

            if ($('#customer-portal-pass').is(':checked') && texto.indexOf("%cliente_clave_portal%") == -1) {
                text_max = parseInt($('#message-template').attr('maxlength')) - 12;
                $("#message-template").attr('maxlength', text_max);
                $('#customer-portal-pass').prop('checked', false);
            }

            if (texto.indexOf("%cliente_saldo_total%") > -1) {
                if (!$('#customer-balance').is(':checked')) {
                    text_max = parseInt($('#message-template').attr('maxlength')) + 11;
                    $("#message-template").attr('maxlength', text_max);
                    $('#customer-balance').prop('checked', true);
                }
            }

            if ($('#customer-balance').is(':checked') && texto.indexOf("%cliente_saldo_total%") == -1) {
                text_max = parseInt($('#message-template').attr('maxlength')) - 11;
                $("#message-template").attr('maxlength', text_max);
                $('#customer-balance').prop('checked', false);
            }

            if (texto.indexOf("%cliente_saldo_mes%") > -1) {
                if (!$('#customer-month-balance').is(':checked')) {
                    text_max = parseInt($('#message-template').attr('maxlength')) + 9;
                    $("#message-template").attr('maxlength', text_max);
                    $('#customer-month-balance').prop('checked', true);
                }
            }

            if ($('#customer-month-balance').is(':checked') && texto.indexOf("%cliente_saldo_mes%") == -1) {
                text_max = parseInt($('#message-template').attr('maxlength')) - 9;
                $("#message-template").attr('maxlength', text_max);
                $('#customer-month-balance').prop('checked', false);
            }

            if (texto.indexOf("%empresa_nombre%") > -1) {
                if (!$('#enterprise-name').is(':checked')) {
                    text_max = parseInt($('#message-template').attr('maxlength')) - 4;
                    $("#message-template").attr('maxlength', text_max);
                    $('#enterprise-name').prop('checked', true);
                }
            }

            if ($('#enterprise-name').is(':checked') && texto.indexOf("%empresa_nombre%") == -1) {
                text_max = parseInt($('#message-template').attr('maxlength')) + 4;
                $("#message-template").attr('maxlength', text_max);
                $('#enterprise-name').prop('checked', false);
            }

            if (texto.indexOf("%empresa_domicilio%") > -1) {
                if (!$('#enterprise-address').is(':checked')) {
                    text_max = parseInt($('#message-template').attr('maxlength')) - 1;
                    $("#message-template").attr('maxlength', text_max);
                    $('#enterprise-address').prop('checked', true);
                }
            }

            if ($('#enterprise-address').is(':checked') && texto.indexOf("%empresa_domicilio%") == -1) {
                text_max = parseInt($('#message-template').attr('maxlength')) + 1;
                $("#message-template").attr('maxlength', text_max);
                $('#enterprise-address').prop('checked', false);
            }

            if (texto.indexOf("%empresa_web%") > -1) {
                if (!$('#enterprise-web').is(':checked')) {
                    text_max = parseInt($('#message-template').attr('maxlength')) - 7;
                    $("#message-template").attr('maxlength', text_max);
                    $('#enterprise-web').prop('checked', true);
                }
            }

            if ($('#enterprise-web').is(':checked') && texto.indexOf("%empresa_web%") == -1) {
                text_max = parseInt($('#message-template').attr('maxlength')) + 7;
                $("#message-template").attr('maxlength', text_max);
                $('#enterprise-web').prop('checked', false);
            }            

            var text_length = $('#message-template').val().length;

            text_remaining = text_max - text_length;

            if (text_remaining <= 10) {
                $('#textarea_feedback').addClass('text-danger');
            } else {
                $('#textarea_feedback').removeClass('text-danger');
            }

            $('#textarea_feedback').html(text_remaining + ' caracteres restantes');
        });

        $('.form-check-input').click(function() {

            var id = $(this).attr('id');

            switch (id) {
                case 'customer-name':
                    if ($(this).is(':checked')) {
                        if ((text_remaining - 20) >= 0) {
                            var text = $('#message-template').val() + $(this).next('label').text();
                            $('#message-template').val(text);
                            text_max = parseInt($('#message-template').attr('maxlength')) - 4;
                            $("#message-template").attr('maxlength', text_max);
                        } else {
                            $(this).prop('checked', false);
                            generateNoty('warning', 'No se puede agregar la marca debido a que superaría los 160 caracteres');
                        }
                    } else {
                        var text = $('#message-template').val().replace($(this).next('label').text(), '');
                        $('#message-template').val(text);
                        text_max = parseInt($('#message-template').attr('maxlength')) + 4;
                        $("#message-template").attr('maxlength', text_max);
                    }
                    break;

                case 'customer-code':
                    if ($(this).is(':checked')) {
                        if ((text_remaining - 11) >= 0) {
                            var text = $('#message-template').val() + $(this).next('label').text();
                            $('#message-template').val(text);
                            text_max = parseInt($('#message-template').attr('maxlength')) + 5;
                            $("#message-template").attr('maxlength', text_max);
                        } else {
                            $(this).prop('checked', false);
                            generateNoty('warning', 'No se puede agregar la marca debido a que superaría los 160 caracteres');
                        }
                    } else {
                        var text = $('#message-template').val().replace($(this).next('label').text(), '');
                        $('#message-template').val(text);
                        text_max = parseInt($('#message-template').attr('maxlength')) - 5;
                        $("#message-template").attr('maxlength', text_max);
                    }
                    break;

                case 'customer-doc':
                    if ($(this).is(':checked')) {
                        if ((text_remaining - 16) >= 0) {
                            var text = $('#message-template').val() + $(this).next('label').text();
                            $('#message-template').val(text);
                            text_max = parseInt($('#message-template').attr('maxlength')) + 3;
                            $("#message-template").attr('maxlength', text_max);
                        } else {
                            $(this).prop('checked', false);
                            generateNoty('warning', 'No se puede agregar la marca debido a que superaría los 160 caracteres');
                        }
                    } else {
                        var text = $('#message-template').val().replace($(this).next('label').text(), '');
                        $('#message-template').val(text);
                        text_max = parseInt($('#message-template').attr('maxlength')) - 3
                        $("#message-template").attr('maxlength', text_max);
                    }
                    break;

                case 'customer-portal-user':
                    if ($(this).is(':checked')) {
                        if ((text_remaining - 11) >= 0) {
                            var text = $('#message-template').val() + $(this).next('label').text();
                            $('#message-template').val(text);
                            text_max = parseInt($('#message-template').attr('maxlength')) + 13;
                            $("#message-template").attr('maxlength', text_max);
                        } else {
                            $(this).prop('checked', false);
                            generateNoty('warning', 'No se puede agregar la marca debido a que superaría los 160 caracteres');
                        }
                    } else {
                        var text = $('#message-template').val().replace($(this).next('label').text(), '');
                        $('#message-template').val(text);
                        text_max = parseInt($('#message-template').attr('maxlength')) - 13;
                        $("#message-template").attr('maxlength', text_max);
                    }
                    break;

                case 'customer-portal-pass':
                    if ($(this).is(':checked')) {
                        if ((text_remaining - 10) >= 0) {
                            var text = $('#message-template').val() + $(this).next('label').text();
                            $('#message-template').val(text);
                            text_max = parseInt($('#message-template').attr('maxlength')) + 12;
                            $("#message-template").attr('maxlength', text_max);
                        } else {
                            $(this).prop('checked', false);
                            generateNoty('warning', 'No se puede agregar la marca debido a que superaría los 160 caracteres');
                        }
                    } else {
                        var text = $('#message-template').val().replace($(this).next('label').text(), '');
                        $('#message-template').val(text);
                        text_max = parseInt($('#message-template').attr('maxlength')) - 12;
                        $("#message-template").attr('maxlength', text_max);
                    }
                    break;

                case 'customer-balance':
                    if ($(this).is(':checked')) {
                        if ((text_remaining - 9) >= 0) {
                            var text = $('#message-template').val() + $(this).next('label').text();
                            $('#message-template').val(text);
                            text_max = parseInt($('#message-template').attr('maxlength')) + 11;
                            $("#message-template").attr('maxlength', text_max);
                        } else {
                            $(this).prop('checked', false);
                            generateNoty('warning', 'No se puede agregar la marca debido a que superaría los 160 caracteres');
                        }
                    } else {
                        var text = $('#message-template').val().replace($(this).next('label').text(), '');
                        $('#message-template').val(text);
                        text_max = parseInt($('#message-template').attr('maxlength')) - 11;
                        $("#message-template").attr('maxlength', text_max);
                    }
                    break;

                case 'customer-month-balance':
                    if ($(this).is(':checked')) {
                        if ((text_remaining - 9) >= 0) {
                            var text = $('#message-template').val() + $(this).next('label').text();
                            $('#message-template').val(text);
                            text_max = parseInt($('#message-template').attr('maxlength')) + 9;
                            $("#message-template").attr('maxlength', text_max);
                        } else {
                            $(this).prop('checked', false);
                            generateNoty('warning', 'No se puede agregar la marca debido a que superaría los 160 caracteres');
                        }
                    } else {
                        var text = $('#message-template').val().replace($(this).next('label').text(), '');
                        $('#message-template').val(text);
                        text_max = parseInt($('#message-template').attr('maxlength')) - 9;
                        $("#message-template").attr('maxlength', text_max);
                    }
                    break;

                case 'enterprise-name':
                    if ($(this).is(':checked')) {
                        if ((text_remaining - 20) >= 0) {
                            var text = $('#message-template').val() + $(this).next('label').text();
                            $('#message-template').val(text);
                            text_max = parseInt($('#message-template').attr('maxlength')) - 4;
                            $("#message-template").attr('maxlength', text_max);
                        } else {
                            $(this).prop('checked', false);
                            generateNoty('warning', 'No se puede agregar la marca debido a que superaría los 160 caracteres');
                        }
                    } else {
                        var text = $('#message-template').val().replace($(this).next('label').text(), '');
                        $('#message-template').val(text);
                        text_max = parseInt($('#message-template').attr('maxlength')) + 4;
                        $("#message-template").attr('maxlength', text_max);
                    }
                    break;

                case 'enterprise-address':
                    if ($(this).is(':checked')) {
                        if ((text_remaining - 20) >= 0) {
                            var text = $('#message-template').val() + $(this).next('label').text();
                            $('#message-template').val(text);
                            text_max = parseInt($('#message-template').attr('maxlength')) - 1;
                            $("#message-template").attr('maxlength', text_max);
                        } else {
                            $(this).prop('checked', false);
                            generateNoty('warning', 'No se puede agregar la marca debido a que superaría los 160 caracteres');
                        }
                    } else {
                        var text = $('#message-template').val().replace($(this).next('label').text(), '');
                        $('#message-template').val(text);
                        text_max = parseInt($('#message-template').attr('maxlength')) + 1;
                        $("#message-template").attr('maxlength', text_max);
                    }
                    break;

                case 'enterprise-web':
                    if ($(this).is(':checked')) {

                        if ((text_remaining - 20) >= 0) {
                            var text = $('#message-template').val() + $(this).next('label').text();
                            $('#message-template').val(text);
                            text_max = parseInt($('#message-template').attr('maxlength')) - 7;
                            $("#message-template").attr('maxlength', text_max);
                        } else {
                            $(this).prop('checked', false);
                            generateNoty('warning', 'No se puede agregar la marca debido a que superaría los 160 caracteres');
                        }
                    } else {
                        var text = $('#message-template').val().replace($(this).next('label').text(), '');
                        $('#message-template').val(text);
                        text_max = parseInt($('#message-template').attr('maxlength')) + 7;
                        $("#message-template").attr('maxlength', text_max);
                    }
                    break;

            }

            var text_length = $('#message-template').val().length;

            text_remaining = text_max - text_length;

            if (text_remaining <= 10) {
                $('#textarea_feedback').addClass('text-danger');
            } else {
                $('#textarea_feedback').removeClass('text-danger');
            }

            $('#textarea_feedback').html(text_remaining + ' caracteres restantes');
        });

        $('.btn-help-sms').click(function() {
            $('.help-sms-modal').modal('show');
        });

        $('#form-sms-masivo-edit-template').submit(function(e) {
        	var currentForm = this;
        	e.preventDefault();

        	var count = $("#message-template").val().length;

        	if ($("#customer-name").is(':checked')) {
        	    count += 4;
        	}

        	if ($("#customer-code").is(':checked')) {
        	    count -= 13;
        	}

        	if ($("#customer-doc").is(':checked')) {
        	    count -= 3;
        	}

        	if ($("#customer-portal-user").is(':checked')) {
        	    count -= 13;
        	}

        	if ($("#customer-portal-pass").is(':checked')) {
        	    count -= 12;
        	}

        	if ($("#customer-balance").is(':checked')) {
        	    count -= 11;
        	}

        	if ($("#customer-month-balance").is(':checked')) {
        	    count -= 9;
        	}

        	if ($("#enterprise-name").is(':checked')) {
        	    count += 4;
        	}

        	if ($("#enterprise-address").is(':checked')) {
        	    count += 1;
        	}

        	if ($("#enterprise-web").is(':checked')) {
        	    count += 7;
        	}

        	if (count > 160) {
        	    generateNoty('warning', 'El mensaje supera 160 caracteres, debe ser menor o igual a 160 caracteres');
        	} else {
        	    currentForm.submit();
        	}

        });

    });

</script>
