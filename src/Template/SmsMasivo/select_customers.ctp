<style type="text/css">

    .status {
        border-radius: 4px;
        display: inline-block;
        font-weight: bold;
        color: white;
        line-height: 1em;
        font-size: 10.5px;
        padding: 3px 0;
        margin: 0px 0px 0px 0px;
        width: 100%;
    }

    .cp {
        border: 1px solid #f38a73;
        background-color: #f38a73;
    }

    .cc {
        border: 1px solid gray;
        background-color: gray;
    }

    .i {
        border: 1px solid purple;
        background-color: purple;
    }

    .plus1M {
        border: 1px solid #f05f40;
        background-color: #f05f40;
        width: 80%;
    }

    .plus2M {
        border: 1px solid #f05f40;
        background-color: #f05f40;
        width: 80%;
    }

    .plus3M {
        border: 1px solid #f05f40;
        background-color: #f05f40;
        width: 80%;
    }

    .fz-13px {
        font-size: 13px;
    }

    .fz-16px {
        font-size: 16px;
    }

    .fz-26px {
        font-size: 26px;
    }

    .font-size-13px {
        font-size: 13px;
    }

    .badge-info {
        font-size: 100%;
        border: 1px solid #ccc;
    }

</style>

    <input type="hidden" name="" id="template_id" value="<?= !empty($template) ? $template->id : '' ?>"/>

     <div id="btns-tools">
        <div class="text-right btns-tools margin-bottom-5">

            <?php 

                echo  $this->Html->link(
                    '<span class="glyphicon icon-checkbox-checked" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Seleccionar todos',
                    'class' => 'btn btn-default btn-selected-all ml-2',
                    'escape' => false
                ]);

                echo  $this->Html->link(
                    '<span class="glyphicon icon-checkbox-unchecked" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Limpiar Selección',
                    'class' => 'btn btn-default btn-clear-selected-all ml-2',
                    'escape' => false
                ]);

                echo $this->Html->link(
                    '<span class="glyphicon icon-eye" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Ocultar o Mostrar Columnas',
                    'class' => 'btn btn-default btn-hide-column ml-2',
                    'escape' => false
                ]);

                echo $this->Html->link(
                    '<span class="glyphicon fas fa-search"  aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Buscar por columnas',
                    'class' => 'btn btn-default btn-search-column ml-1',
                    'escape' => false
                ]);

                echo $this->Html->link(
                    '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Exportar tabla',
                    'class' => 'btn btn-default btn-export ml-1',
                    'escape' => false
                ]);
            ?>

        </div>
    </div>

    <div id="btn-send">
         <?php

            echo  $this->Html->link(
                '<span class="fa fa-bullhorn" aria-hidden="true"></span> Enviar',
                'javascript:void(0)',
                [

                'class' => 'btn btn-default btn-send margin-bottom-5',
                'escape' => false
            ]);
        ?>
    </div>

    <div class="row">

        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">

            <div class="card border-secondary mb-3" style="height:200px;">
                <div class="card-header p-2">Plataform SMS</div>
                <div class="card-body text-secondary p-2">
                    <div class="row">

                        <div class="col-12">
                            <?php
                                echo $this->Form->input('platform', ['options' => $platform,  'label' =>  '', 'default' => $default_platform, 'disabled' => true]);
                            ?>
                        </div>

                        <div class="info-sms-getway col-12 my-hidden">
                            <?php
                                echo $this->Form->input('sms_getway_device_id', ['options' => $sms_getway_devices,  'label' =>  'Dispositivo', 'default' => $default_device]);
                            ?>
                        </div>

                        <div class="info-smsmasivo col-12 my-hidden">
                            <div class="card-body text-secondary p-2">

                                <div class="row justify-content-around">
                                    <div class="col-auto">
                                        <span class="badge badge-secondary fz-13px">
                                            <i class="far fa-envelope mr-1" aria-hidden="true" title="Mensajes disponibles" ></i>
                                            <?=  $disponibles ? $disponibles : '-'?>
                                        </span>
                                    </div>
                                    <div class="col-auto">
                                        <span class="badge badge-secondary fz-13px">
                                            <i class="far fa-calendar-times mr-1" aria-hidden="true" title="Vencimiento del Paquete" ></i>
                                            <?= $vencimiento_paquete ? $vencimiento_paquete : '-'?>
                                        </span>
                                    </div>
                                    <div class="col-auto">
                                        <span class="badge badge-secondary fz-13px">
                                            <i class="far fa-clock mr-1" aria-hidden="true" title="Hora del servidor" ></i>
                                            <?= $fecha_servidor ? $fecha_servidor : '-'?>
                                        </span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12">

            <div class="row">
                <div class="col-12">
                    <div class="card border-secondary mb-3" style="height:98px;">
                        <div class="card-header p-2">
                            Bloque Nº <span class="badge badge-info fz-14px"><?= $block_next_id ?></span>
                            <a title="En el caso de SMS Masivo se crea un bloque cada 50 SMS." data-toggle="tooltip" class="btn-help-test float-right" href="javascript:void(0)"><i class="fas fa-question-circle"></i></a>
                        </div>
                        <div class="card-body text-secondary p-2 text-center block-lists">

                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="card border-secondary mb-3" style="height:87px;">
                        <div class="card-header p-2 text-center">Mensajes</div>
                        <div class="card-body text-secondary p-2 text-center">
                            <span class="badge badge-secondary fz-16px">
                                  <i class="fas fa-check mr-1" aria-hidden="true" title="Contador de seleccionados" ></i>
                                  <span class="customer-count"></span>
                             </span>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">

            <div class="card border-secondary mb-3" style="height:200px;">
                <div class="card-header p-2">Configuración</div>
                <div class="card-body text-secondary p-2">
                    <div class="row">
                        <div class="col-12">
                            <?php 
                                echo $this->Form->input('business_billing', ['options' => $business,  'label' =>  '', 'default' => $template->business_billing]);
                            ?>
                        </div>
                        <div class="col-12">
                            <?php
                                echo $this->Form->input('template_id', ['options' => $templates,  'label' =>  '', 'default' => $template->id]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-hover" id="table-customers">
                <thead>
                    <tr>
                        <th></th>                     <!--0-->
                        <th >Creado</th>              <!--1-->
                        <th >Código</th>              <!--2-->
                        <th >Cuenta</th>              <!--3-->
                        <th >Etiquetas</th>           <!--4-->
                        <th >Impagas</th>             <!--5-->
                        <th >Saldo mes</th>           <!--6-->
                        <th >Nombre</th>              <!--7-->
                        <th >Documento</th>           <!--8-->
                        <th >Estado Servicios</th>    <!--9-->
                        <th >Domicilio</th>           <!--10-->
                        <th >Área</th>                <!--11-->
                        <th >Teléfono</th>            <!--12-->
                        <th >Vendedor</th>            <!--13-->
                        <th >Resp. Iva</th>           <!--14-->
                        <th >Comp.</th>               <!--15-->
                        <th >Deno.</th>               <!--16-->
                        <th >Correo electrónico</th>  <!--17-->
                        <th >Comentarios</th>         <!--18-->
                        <th >Día Venc.</th>           <!--19-->
                        <th >Clave Portal</th>        <!--20-->
                        <th >Empresa Facturación</th> <!--21-->
                        <th >Controladores</th>       <!--22-->
                        <th >Servicios</th>           <!--23-->
                        <th >Cuentas</th>             <!--24-->
                        <th >Env. Correo</th>         <!--25-->
                        <th >Env. SMS</th>            <!--26-->
                        <th >Compromiso de Pago</th>  <!--27-->
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade confirm-send-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabel">Confirmar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label>Se enviará el aviso a los clientes seleccionados. ¿Desea continuar?</label>

                        <div class="row">
                            <div class="col-md-4">
                        	    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        	</div>
                        	<div class="col-md-4">
                                <button type="button" class="btn btn-danger btn-confirm-send float-right" value="select_customers">Confirmar y Continuar</button>
                            </div>
                            <div class="col-md-4">
                                <button type="button" class="btn btn-info btn-confirm-send float-right" value="messages">Confirmar y Salir</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php 

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-go-customer',
        'name' =>  'Ficha del Cliente',
        'icon' =>  'glyphicon icon-profile',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-customers-smsmasivo']);
    echo $this->element('actions', ['modal'=> 'modal-actions-customers', 'title' => 'Acciones', 'buttons' => $buttons]);
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-customers']);

    echo $this->element('modal_preloader');
?>

<script type="text/javascript">

    var template = <?php echo json_encode($template); ?>;
    var templates = <?php echo json_encode($templates_model); ?>;
    var labels = <?= json_encode($labels) ?>;
    var paraments = <?= json_encode($paraments) ?>;
    var available_send;
    var block_next_id = <?= json_encode($block_next_id) ?>;
    var default_device = '';

    var table_customers = null;
    var customer_selected = null;
    var customer_count;
    var curr_pos_scroll = null;
    var ids_selected = [];
    var platform_selected = ''
    var areas;
    var controllers;
    var services;
    var accounts = null;
    var hash = null;

    $(document).ready(function () {

        platform_selected = <?= json_encode($default_platform) ?>;
        default_device = <?= json_encode($default_device) ?>;
        controllers = <?php echo json_encode($controllers); ?>;
        services = <?php echo json_encode($services); ?>;
        accounts = <?= json_encode($accounts) ?>;
        hash = <?= time() ?>;

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

        available_send = <?= json_encode($available_send) ?>;
        areas = <?php echo json_encode($areas); ?>;

        $('.btn-hide-column').click(function() {
            $('.modal-hide-columns-customers-smsmasivo').modal('show');
        });

        $('.btn-search-column').click(function() {
            $('.modal-search-columns-customers').modal('show');
        });

        customer_count = 0;
        $('.customer-count').text(customer_count);

		$('#table-customers').removeClass('display');

        $('#btns-tools').hide();

        loadPreferences('customers-index-sms-masivo', [3, 11, 13, 14, 15, 16, 17, 18, 19, 20, 21]);

		table_customers = $('#table-customers').DataTable({
		    "order": [[ 1, 'desc' ]],
            "deferRender": true,
            "processing": true,
            "serverSide": true,
		    "ajax": {
                "url": "/ispbrain/SmsMasivo/get_customers.json",
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "type": platform_selected
                    });
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "drawCallback": function( c ) {
                var flag = true;

            	switch (c.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
            	    if (table_customers) {

            	        $('#platform').prop("disabled", false);

            	        switch (platform_selected) {

                            case 'sms_getway':
                                $('#sms-getway-device-id').val(default_device);
                                $('.info-sms-getway').removeClass('my-hidden');
                                break;

                            case 'smsmasivo':
                                $('.info-smsmasivo').removeClass('my-hidden');
                                break;

                            default:
                                $('.info-sms-getway').addClass('my-hidden');
                                $('.info-smsmasivo').addClass('my-hidden');
            	        }

                        var tableinfo = table_customers.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }

                        if (curr_pos_scroll) {

                            $(table_customers.settings()[0].nScrollBody).scrollTop( curr_pos_scroll.top );
                            $(table_customers.settings()[0].nScrollBody).scrollLeft( curr_pos_scroll.left );
                        }
                    }
            	}
            },
            "scrollY": true,
		    "scrollY": '430px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
		    "columns": [
		        {
                    "className":      'checkbox-control',
                    "orderable":      false,
                    "data":           'id',
                    "render": function ( data, type, full, meta ) {
                        return '<label class="custom-control custom-checkbox pl-3 pr-3 m-0 align-middle"><input type="checkbox" class="custom-control-input checkbox-select p-0 m-0 " id="row-checkbox-' + data + '"><span class="custom-control-indicator w-100 align-middle"></span></label>';
                    },
                    "width": '3%'
                },
                {
                    "className":"row-control",
                    "data": "created",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                {
                    "className":"row-control",
                    "data": "code",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                {
                    "className":"row-control",
                    "data": "account_code",
                    "type": "string"
                },
                {
                    "className":"row-control",
                    "data": "status",
                    "type": "options_colors",
                    "options": labels,
                    "render": function ( data, type, row ) {
                        var spans = "";
                        $.each(row.labels, function(i, val) {
                            spans += "<span class='status' style='background-color: " + val.color_back + "' > </span>"; 
                        });
                        return spans;
                    }
                },
                {
                    "className":"row-control",
                    "data": "invoices_no_paid",
                    "type": "decimal",
                    "render": function ( data, type, row ) {
                       if (row.invoices_no_paid > 0) {
                           return "<span class='badge badge-danger font-size-13px' >" + row.invoices_no_paid + "</span>";
                       }
                       return "<span class='badge badge-success font-size-13px'>" + row.invoices_no_paid + "</span>";
                    }
                    
                },
                {
                    "className":"row-control",
                    "data": "debt_month",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' ),
                },
                {
                    "className":"row-control",
                    "data": "name",
                    "type": "string",
                },
                {
                    "className":"row-control",
                    "data": "ident",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (data) {
                            ident = data;
                        }
                        return sessionPHP.afip_codes.doc_types[row.doc_type] + ' ' + ident;
                    }
                },
                {
                    "className":"row-control",
                    "data": "services_active",
                    "type": "options",
                    "options": {  
                        "0":"Bloqueados",
                        "1":"Habilitados",
                    },
                    "render": function ( data, type, row ) {
                       return "<span class='font-size-13px'>" + row.services_active + '/' + row.services_total + "</span>";
                    }
                },
                {
                    "className":"row-control",
                    "data": "address",
                    "type": "string",
                },
                {
                    "className":"row-control",
                    "data": "area_id",
                    "type": "options",
                    "options": areas,
                    "render": function ( data, type, row ) {
                        if (data) {
                            return areas[data];
                        }
                        return "";
                    }
                },
                {
                    "className":"row-control",
                    "data": "phone",
                    "type": "integer",
                },
                { 
                    "data": "seller",
                    "type": "string",
                },
                {
                    "className":"row-control",
                    "data": "responsible",
                    "type": "options",
                    "options": sessionPHP.afip_codes.responsibles,
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.responsibles[data];
                    }
                },
                {
                    "className":"row-control",
                    "data": "is_presupuesto",
                    "type": "options",
                    "options": {  
                        "0":"Factura",
                        "1":"Presupuesto",
                    },
                    "render": function ( data, type, row ) {
                        if (data) {
                            return 'PRESU';
                        }
                        return 'FACTURA';
                    }
                },
                {
                    "className":"row-control",
                    "data": "denomination",
                    "type": "options",
                    "options": {  
                        "0":"No Denominado",
                        "1":"Denominado",
                    },
                    "render": function ( data, type, row ) {
                        if (data) {
                            return 'SI';
                        }
                        return 'NO';
                    }
                },
                {
                    "className":"row-control",
                    "data": "email",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                {
                    "className":"row-control",
                    "data": "comments",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                {
                    "className":"row-control",
                    "data": "daydue",
                    "type": "integer",
                },
                {
                    "className":"row-control",
                    "data": "clave_portal",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                {
                    "className":"row-control",
                    "data": "business_billing",
                    "type": "options_obj",
                    "options": sessionPHP.paraments.invoicing.business,
                    "render": function ( data, type, row ) {
                        var business_name = '';
                        $.each(sessionPHP.paraments.invoicing.business, function (i, b) {
                            if (b.id == data) {
                                business_name = b.name;
                            }
                        });
                        return business_name;
                    }
                },
                {
                    "className":"row-control",
                    "data": "controller",
                    "type": "string",
                    "type": "options",
                    "options": controllers,
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                {
                    "className":"row-control",
                    "data": "service",
                    "type": "string",
                    "type": "options",
                    "options": services,
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                {
                    "data": "customers_accounts",
                    "type": "string",
                    "type": "options",
                    "options": accounts,
                    "render": function ( data, type, row ) {
                        if (row.customers_accounts.length > 0) {
                            var accounts_name = "";
                            $.each(row.customers_accounts, function( index, value ) {
                                accounts_name += accounts[value.payment_getway_id] + ' - ';
                            });
                            return accounts_name;
                        }
                        return '';
                    }
                },
                {
                    "className": "row-control",
                    "data": "last_emails",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                        return "";
                    }
                },
                {
                    "className": "row-control",
                    "data": "last_sms",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                        return "";
                    }
                },
                {
                    "data": "payment_commitment",
                    "type": "date",
                    "display_search_text": "C. Pago",
                    "render": function ( data, type, row ) {
                        var created = '';
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                        return created;
                    }
                },
            ],
		    "columnDefs": [
                {
                "targets": [0, 1, 2, 3, 4, 5, 6, 8, 9, 11, 12, 13, 14, 15, 16, 17, 19, 20],
                    "width": '5%'
                },
                {
                    "targets": [21],
                    "width": '8%'
                },
                {
                    "targets": [10, 18],
                    "width": '13%'
                },
                {
                    "targets": [7],
                    "width": '12%'
                },
                { 
                    "class": "left", targets: [7, 10]
                },
                { 
                    "visible": false, targets: hide_columns
                },
                { "orderable": false, "targets": [4, 9] }
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.code;
                $.each(ids_selected, function( index, value ) {
                    if (row.id == value) {
                        $(row).addClass('selected');
                        $(row).find('input:checkbox').attr('checked', 'checked')
                    }
                });
            },
		    "language": dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[50, 100, 500, -1], [50, 100, 500, "Todas"]],
        	"dom":
    	    	"<'row'<'col-xl-2'l><'col-xl-2 selected'><'col-xl-2 btn-send-container'><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
		});

	    $('#table-customers_wrapper .tools').append($('#btns-tools').contents());
	    $('#table-customers_wrapper .btn-send-container').append($('#btn-send').contents());

	    $('#btns-tools').show();

		$('#table-customers').on( 'init.dt', function () {

		    createModalHideColumn(table_customers, '.modal-hide-columns-customers-smsmasivo');
		    createModalSearchColumn(table_customers, '.modal-search-columns-customers');
		    loadSearchPreferences('customers-index-sms-masivo-search');

		    $( "#table-customers tbody" ).on( "click",  ".checkbox-select", function() {

                if ($(this).is(':checked')) {
                    $(this).closest('tr').addClass('selected');
                    ids_selected.push(table_customers.row( $(this).closest('tr') ).data().code);
                    customer_count++;
                    if (platform_selected == 'smsmasivo') {
                        addBlockBySMS();
                    }
                } else {
                    $(this).closest('tr').removeClass('selected');
                    ids_selected.splice($.inArray(table_customers.row( $(this).closest('tr') ).data().code, ids_selected), 1);
                    customer_count--;
                    if (platform_selected == 'smsmasivo') {
                        addBlockBySMS();
                    }
                }
                $('.customer-count').text(customer_count);
            });

            $("#table-customers tbody").on( "click",  ".row-control", function() {
                customer_selected = table_customers.row( $(this).closest('tr') ).data();
                $('.modal-actions-customers').modal('show');
            });
        });

        $('.btn-selected-all').click(function() {
            $('.checkbox-select:checkbox').prop('checked', false);
            $('.checkbox-select').click();
            customer_count = 0;
            ids_selected = [];
            table_customers.rows().eq(0).each( function ( idx ) {
                var row = table_customers.row( idx );
                if ($(row.node()).hasClass('selected')) {
                    ids_selected.push(table_customers.row( idx ).data().code);
                    customer_count++;
                }
            });
            $('.customer-count').text(customer_count);

            if (platform_selected == 'smsmasivo') {
                addBlockBySMS();
            }
        });

        function addBlockBySMS() {
            $('.block-lists').html('');

            var limit = array_chunk(ids_selected, 50);
            limit = limit.length - 1;

            if (limit > 0) {
                $('.block-lists').append('<span class="badge badge-info fz-14px">' + block_next_id + '</span>');
                var next = block_next_id + limit;
                $('.block-lists').append('<span>...</span>');
                $('.block-lists').append('<span class="badge badge-info fz-14px">' + next + '</span>');
            }
        }

        $('.btn-clear-selected-all').click(function() {
            $('.checkbox-select:checkbox').prop('checked', true);
            $('.checkbox-select').click();
            customer_count = 0;
            ids_selected = [];
            table_customers.rows().eq(0).each( function ( idx ) {
                var row = table_customers.row( idx );
                if ($(row.node()).hasClass('selected')) {
                    customer_count++;
                }
            });
            $('.customer-count').text(customer_count);
            $('.block-lists').html('');
        });

        $('#btn-go-customer').click(function() {
            var action = '/ispbrain/customers/view/' + customer_selected.code;
            window.open(action, '_blank');
        });

        $(".btn-export").click(function() {
            bootbox.confirm('Se descargará un archivo Excel', function(result) {
                if (result) {
                    $('#table-customers').tableExport({tableName: 'Avisos: Selección de Clientes', type:'excel', escape:'false'});
                }
            }).find("div.modal-content").addClass("confirm-width-md");
        });

        $(document).on("click", ".btn-send", function(e) {
            if (available_send) {
                if (ids_selected.length > 0) {

                    $('.confirm-send-modal').modal('show');

                } else {
                    bootbox.alert('Debe seleccionar al menos 1 cliente para continuar.');
                }
            } else {
                bootbox.confirm('No se pudo establecer comunicación con el servidor de SMS Masivo (posible motivo las credenciales de la Cuenta). Presione OK para ir a configuración de cuentas SMS.', function(result) {
                    if (result) {
                        var action = '/ispbrain/SmsMasivo/config';
                        window.open(action, '_blank');
                    }
                });
            }
        });

        $('.btn-confirm-send').click(function() {

            var device_id = null;

            switch (platform_selected) {
                case 'sms_getway':
                    device_id = $('#sms-getway-device-id').val();
                    break;
                case 'smsmasivo':
                    device_id = null;
                    break;
            }

            var data = {
                business_billing: $('#business-billing :selected').val(),
                platform: $('#platform').val(),
                device_id: device_id,
                template_id: $('#template-id').val(),
                ids: ids_selected,
                redirect: $(this).attr("value")
            };

            $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/SmsMasivo/selectCustomers/' + template.id, 'method': 'post', 'id': 'form-block'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'data', 'value': JSON.stringify(data)}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'hash', 'value': hash}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#form-block').submit();

            table_customers.ajax.reload();
            $('.confirm-send-modal').modal('hide');
        });

        $('#platform').change(function() {

            available_send = false;
            platform_selected = $(this).val();

            $('.info-smsmasivo').addClass('my-hidden');
            $('.info-sms-getway').addClass('my-hidden');
            $('#platform').prop("disabled", true);

            switch (platform_selected) {

                case 'sms_getway':
                    $('.block-lists').html('');
                    var request = $.ajax({
                        url: "/ispbrain/SmsMasivo/ajaxValidateAccount.json",
                        method: "POST",
                        data: JSON.stringify({platform: platform_selected}),
                        dataType: "json"
                    });

                    request.done(function(response) {

                        if (response.data.error) {
                            generateNoty(response.data.type, response.data.msg);
                        } else {
                            available_send = true;
                        }
                        table_customers.ajax.reload();
                    });

                    request.fail(function(jqXHR) {

                        if (jqXHR.status == 403) {

                        	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                        	setTimeout(function() {
                                window.location.href = "/ispbrain";
                        	}, 3000);

                        } else {
                        	generateNoty('error', 'Error al editar la cuenta');
                        }
                    });
                    break;

                case 'smsmasivo':

                    addBlockBySMS();
                    var dt = new Date();
                    var hour = dt.getHours();
                     if (paraments.sms.platform.smsmasivo.fulltime 
                        || hour < 22 
                        && hour > 8) {

                        var request = $.ajax({
                            url: "/ispbrain/SmsMasivo/ajaxValidateAccount.json",
                            method: "POST",
                            data: JSON.stringify({platform: platform_selected}),
                            dataType: "json"
                        });

                        request.done(function(response) {

                            if (response.data.error) {
                                generateNoty(response.data.type, response.data.msg);
                            } else {
                                available_send = true;
                            }
                            table_customers.ajax.reload();
                        });

                        request.fail(function(jqXHR) {

                            if (jqXHR.status == 403) {

                            	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                            	setTimeout(function() { 
                                    window.location.href = "/ispbrain";
                            	}, 3000);

                            } else {
                            	generateNoty('error', 'Error al editar la cuenta');
                            }
                        });
                    } else {
                        generateNoty('warning', 'NOTA: Recuerde que, predeterminadamente, todos los usuarios de SMS Masivos cuentan con una franja horaria habilitada para realizar los envíos (desde las 08:00hs hasta las 22:00hs). Si precisa enviar mensajes las 24hs, por favor, póngase en contacto con el departamento de soporte.');
                    }
                    break;

                default:
                    table_customers.ajax.reload();
            }
        });

        $('#template-id').change(function() {

            var template_id = $(this).val();

            $.each(templates, function( index, value ) {
                if (template_id == value.id) {
                    $('#business-billing').val(value.business_billing);
                }
            });
        });
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

    /**
     * Returns an array with arrays of the given size.
     *
     * @param myArray {Array} array to split
     * @param chunk_size {Integer} Size of every group
     */
    function array_chunk(myArray, chunk_size){
        var index = 0;
        var arrayLength = myArray.length;
        var tempArray = [];

        for (index = 0; index < arrayLength; index += chunk_size) {
            myChunk = myArray.slice(index, index+chunk_size);
            // Do something if you want with the group
            tempArray.push(myChunk);
        }

        return tempArray;
    }

</script>
