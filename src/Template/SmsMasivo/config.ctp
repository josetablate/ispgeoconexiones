<style type="text/css">

    legend {
        display: block;
        width: 100%;
        padding: 0;
        margin-bottom: 20px;
        font-size: 21px;
        line-height: inherit;
        color: #333;
        border: 0;
        border-bottom: 1px solid #e5e5e5;
    }

    .contain-block {
        background-color: #ececec;
        border: 1px solid #c3c2c2;
        border-radius: 5px;
        margin-left: 15px;
        padding-top: 5px;
        margin-top: 15px;
    }

    .btn-get-devices-sms-getway {
        margin-top: 35;
    }

</style>


    <div class="row">

        <div class="col-md-12">
            <legend class="sub-title">Configuración</legend>
        </div>
        
        <div class="col-md-4 contain-block">
            <?= $this->Form->create($paraments,  ['class' => 'form-load']) ?>
                <fieldset>
                    <legend class="sub-title">General</legend>
                    <?= $this->Form->hidden('form', ['value' => 'sms']); ?>
                    <?php 
                        echo $this->Form->input('sms.default_platform', ['options' => $platform,  'label' =>  'Seleccionar Plataforma SMS por defecto', 'default' => $paraments->sms->default_platform]);
                    ?>
                </fieldset>
            <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success' ]) ?>
            <?= $this->Form->end() ?>
        </div>

        <div class="col-md-4 contain-block">
            <?= $this->Form->create($paraments,  ['class' => 'form-load']) ?>
                <fieldset>
                    <legend class="sub-title"><?= $paraments->sms->platform->smsmasivo->name ?></legend>
                    <?= $this->Form->hidden('form', ['value' => 'sms_masivo']); ?>
                    <?= $this->Form->input('sms_masivo.smsusuario', ['label' => 'Usuario', 'type' => 'text', 'value' => $paraments->sms->platform->smsmasivo->smsusuario]) ?>
                    <?= $this->Form->input('sms_masivo.smsclave', ['label' => 'Clave', 'type' => 'text', 'value' => $paraments->sms->platform->smsmasivo->smsclave]) ?>
                    <?= $this->Form->input('sms_masivo.enabled', ['label' => 'Habilitar', 'type' => 'checkbox', 'checked' => $paraments->sms->platform->smsmasivo->enabled]) ?>
                    <?= $this->Form->input('sms_masivo.fulltime', ['label' => '24hs', 'type' => 'checkbox', 'checked' => $paraments->sms->platform->smsmasivo->fulltime]) ?>
                </fieldset>
            <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success' ]) ?>
            <?= $this->Form->end() ?>
        </div>

        <div class="col-md-4 contain-block">
            <?= $this->Form->create($paraments,  ['class' => 'form-load']) ?>
                <fieldset>
                    <legend class="sub-title"><?= $paraments->sms->platform->sms_getway->name ?></legend>
                    <?= $this->Form->hidden('form', ['value' => 'sms_getway']); ?>
                    <?= $this->Form->input('sms_getway.token', ['label' => 'API Token', 'type' => 'text', 'value' => $paraments->sms->platform->sms_getway->token]) ?>
                    <div class="row">
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                            <?= $this->Form->input('sms_getway.device_id', ['options' => $sms_getway_devices,  'label' =>  'Dispositivos', 'default' => $paraments->sms->platform->sms_getway->device_id]); ?>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2">
                            <button type="button" class="btn btn-default btn-get-devices-sms-getway" data-toggle='tooltip' data-placement='top' title='Presionar para traer los dispositivos asociados a su cuenta'>Buscar <i class="fas fa-sync"></i></button>
                        </div>
                    </div>
                    <?= $this->Form->input('sms_getway.enabled', ['label' => 'Habilitar', 'type' => 'checkbox', 'checked' => $paraments->sms->platform->sms_getway->enabled]) ?>
                </fieldset>
            <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success' ]) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>

<script type="text/javascript">

    var paraments;

    $(document).ready(function () {

        paraments = <?= json_encode($paraments) ?>;

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

        $('.btn-get-devices-sms-getway').click(function() {

            if ($('#sms-getway-token').val() != '') {
 
                var request = $.ajax({
                    url: "/ispbrain/SmsMasivo/getDevices.json",
                    method: "POST",
                    data: JSON.stringify({
                        platform: 'sms_getway',
                        token: $('#sms-getway-token').val()
                    }),
                    dataType: "json"
                });

                request.done(function(response) {

                    if (response.data.error) {

                        generateNoty(response.data.type, response.data.msg);
                    } else {

                        $('#sms-getway-device-id option').remove();
                        $('#sms-getway-device-id');

                        $('#sms-getway-device-id').append($('<option>', {
                            value: '',
                            text: 'Seleccione dispositivo predeterminado'
                        }));

                        $.each(response.data.devices, function( index, device ) {
                            $('#sms-getway-device-id').append($('<option>', {
                                value: device.id,
                                text: device.name
                            }));
                        });

                        generateNoty('success', 'Actualizada la lista de dispositivos con éxito. Debe seleccionar el dispositivo.');
                    }
                });

                request.fail(function(jqXHR) {

                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al editar la cuenta');
                    }
                });
            } else {
                generateNoty('warning', 'Debe agregar el API Token, para poder buscar los dispositivos relacionados');
            }
        });

        $('#sms-getway-token').keyup(function () {
            $('#sms-getway-device-id option').remove();
            $('#sms-getway-device-id');

            $('#sms-getway-device-id').append($('<option>', {
                value: '',
                text: 'Seleccione dispositivo predeterminado'
            }));
        });

    });

</script>