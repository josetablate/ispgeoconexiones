<?php
/**
 * @var \App\View\AppView $this
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Connections Has Discounts'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="connectionsHasDiscounts form large-9 medium-8 columns content">
    <?= $this->Form->create($connectionsHasDiscount) ?>
    <fieldset>
        <legend><?= __('Add Connections Has Discount') ?></legend>
        <?php
            echo $this->Form->control('code');
            echo $this->Form->control('concept');
            echo $this->Form->control('alicuot');
            echo $this->Form->control('percent');
            echo $this->Form->control('comments');
            echo $this->Form->control('always');
            echo $this->Form->control('month');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
