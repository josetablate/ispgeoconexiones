<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ConnectionsHasDiscount[]|\Cake\Collection\CollectionInterface $connectionsHasDiscounts
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Connections Has Discount'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="connectionsHasDiscounts index large-9 medium-8 columns content">
    <h3><?= __('Connections Has Discounts') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('code') ?></th>
                <th scope="col"><?= $this->Paginator->sort('concept') ?></th>
                <th scope="col"><?= $this->Paginator->sort('alicuot') ?></th>
                <th scope="col"><?= $this->Paginator->sort('percent') ?></th>
                <th scope="col"><?= $this->Paginator->sort('comments') ?></th>
                <th scope="col"><?= $this->Paginator->sort('always') ?></th>
                <th scope="col"><?= $this->Paginator->sort('month') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($connectionsHasDiscounts as $connectionsHasDiscount): ?>
            <tr>
                <td><?= $this->Number->format($connectionsHasDiscount->id) ?></td>
                <td><?= h($connectionsHasDiscount->created) ?></td>
                <td><?= $this->Number->format($connectionsHasDiscount->code) ?></td>
                <td><?= h($connectionsHasDiscount->concept) ?></td>
                <td><?= $this->Number->format($connectionsHasDiscount->alicuot) ?></td>
                <td><?= $this->Number->format($connectionsHasDiscount->percent) ?></td>
                <td><?= h($connectionsHasDiscount->comments) ?></td>
                <td><?= h($connectionsHasDiscount->always) ?></td>
                <td><?= $this->Number->format($connectionsHasDiscount->month) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $connectionsHasDiscount->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $connectionsHasDiscount->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $connectionsHasDiscount->id], ['confirm' => __('Are you sure you want to delete # {0}?', $connectionsHasDiscount->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
