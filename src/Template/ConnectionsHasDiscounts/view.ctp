<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ConnectionsHasDiscount $connectionsHasDiscount
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Connections Has Discount'), ['action' => 'edit', $connectionsHasDiscount->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Connections Has Discount'), ['action' => 'delete', $connectionsHasDiscount->id], ['confirm' => __('Are you sure you want to delete # {0}?', $connectionsHasDiscount->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Connections Has Discounts'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Connections Has Discount'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="connectionsHasDiscounts view large-9 medium-8 columns content">
    <h3><?= h($connectionsHasDiscount->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Concept') ?></th>
            <td><?= h($connectionsHasDiscount->concept) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Comments') ?></th>
            <td><?= h($connectionsHasDiscount->comments) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($connectionsHasDiscount->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Code') ?></th>
            <td><?= $this->Number->format($connectionsHasDiscount->code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Alicuot') ?></th>
            <td><?= $this->Number->format($connectionsHasDiscount->alicuot) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Percent') ?></th>
            <td><?= $this->Number->format($connectionsHasDiscount->percent) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Month') ?></th>
            <td><?= $this->Number->format($connectionsHasDiscount->month) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($connectionsHasDiscount->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Always') ?></th>
            <td><?= $connectionsHasDiscount->always ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
