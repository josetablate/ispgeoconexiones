 
 
<div class="row">
    <div class="col-xl-12">
        <div class="card border-secondary p-1 mb-2" id="card-customer-selector">
            <div class="card-body p-2">
                
                <div class="row">
                    <div class="col-xl-2 ">
                        <div class="input-group">
                            <span class="input-group-text" >Código</span>
                            <input type="text" class="form-control text-right" value="<?= sprintf("%'.05d", $connection->customer->code) ?>" readonly="true" value="">
                        </div>
                    </div>
                    <div class="col-xl-2">
                        <div class="input-group">
                            <span class="input-group-text">Doc.</span>
                            <input type="text" class="form-control text-right" value="<?=$connection->customer->ident?>" readonly="true" value="" >
                        </div>
                    </div>
                    <div class="col-xl-3">
                        <div class="input-group">
                            <span class="input-group-text">Nombre</span>
                            <input type="text" class="form-control text-right text-truncate" value="<?=$connection->customer->name?>" readonly="true" value="" >
                        </div>
                    </div>
                    
                    <div class="col-xl-2">
                        <div class="input-group">
                            <span class="input-group-text">IP</span>
                            <input type="text" class="form-control text-right" value="<?=$connection->ip?>" readonly="true" value="" >
                        </div>
                    </div>
                    
                    <div class="col-xl-3">
                        <div class="input-group">
                            <span class="input-group-text">Servicio</span>
                            <input type="text" class="form-control text-right" value="<?=$connection->service->name?>" readonly="true" value="" >
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
  <div class="col-12">
    
    <div id="chart_real_time_speed"></div>
    
  </div>
</div>
 
 
 <!--Div that will hold the pie chart-->
    



<script>
  
  var connection =  <?=json_encode($connection)?>
    
  var scaleH = 50;
    
  var TIMER = 3000;
    
  var rates_data = [];

  var profile = null;
  var profile_up_name = '';
  var profile_down_name = '';
  var profile_up_value = null;
  var profile_down_value = null
  
  var x = scaleH - 1;


  function initArrayData(){
  
      rates_data = [];
      rates_data.push(['Tiempo', 'up', 'down', profile_up_name, profile_down_name]);
      for(var i=1; i < scaleH; i++){
        rates_data.push([i-1, null, null, profile_up_value, profile_down_value]);
      }
      
      // console.log(rates_data);
      
    }
    
    
    
  
  function setValesArayData(rate){
   
      rates_data[x] = [x-1, rate.up, rate.down, profile_up_value, profile_down_value];
      
      x--;
      
      // rates_data.reverse();
      
    }
  
  function drawChart() {
          
      var data = google.visualization.arrayToDataTable(rates_data);

      var options = {
        title: 'Gráfica de velocidad ',
        hAxis: {
          textPosition: 'none',
          // maxAlternation: 3,
          minValue: 0,
          title: '(tiempo 3s)'
        },
        // hAxis: null,
        vAxis: { 
          // title: 'Tiempo',
          // titleTextStyle: {
          //   color: '#333'
          // },
          minValue: 0,
          format: 'decimal',
          title: '(Mb/s)'
        },
        areaOpacity : 0.3,
        backgroundColor: 'white',
        backgroundColor: {
          stroke: '#666',
          strokeWidth: 0,
          fill: 'white'
        },
        // chartArea:{left:20,top:0,width:'50%',height:'75%', backgroundColor: 'red'}
        chartArea: null,
        // colors:['red','#004411'],
        // explorer: { axis: 'vertical' }
        // explorer: { axis: 'horizontal' }
        // explorer: { actions: ['dragToZoom', 'rightClickToReset']},
        // explorer: { keepInBounds: true }
        // focusTarget: 'datum',
        height: 300,
        // width: 400,
        series: {
          0: { //up
             areaOpacity : 0,
             color: '#007bff'
          },
          1: { //down
            areaOpacity : 0.3,
            color: '#28a745',
          },
          2: { //down
            areaOpacity : 0,
            color: '#DF7401',
          },
          3: { //down
            areaOpacity : 0,
            color: 'red',
          },
        },
      };

      var chart = new google.visualization.AreaChart(document.getElementById('chart_real_time_speed'));
      chart.draw(data, options);
    }
    
  function restart(){
    
    x = scaleH - 1;
    
    // setTimeout(update, TIMER);
    
    update();
    
      
  }
    
  function update() {
     
    console.log('update ...');
    
      // drawChart();
      
      // return false;
  		
  		$.ajax({
              url: "<?=$this->Url->build(['controller' => 'ConnectionsGraphic', 'action' => 'UpdateRealTimeGraph'])?>",
              type: 'POST',
              dataType: "json",
              data: JSON.stringify({ connection_id: connection.id }),
              success: function(data){

                  
                  if(data.rate){
                    
                    if(x > 0){
                      
                      setValesArayData(data.rate);
                    
                      drawChart();
                      
                      // setTimeout(update, TIMER);
                      
                      update();
                        
                    }else{
                      
                       bootbox.confirm('¿Continuar?', function(result) {
                          
                          if (result) {
                            restart();
                          }
                      });
                      
                    }
                    
                  }else{
                    
                     generateNoty('warning', 'rate vacio .');
                  }
            
              },
              error: function (jqXHR) {
  
                // console.log(jqXHR);
  
                  if (jqXHR.status == 403) {
  
                  	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
  
                  	setTimeout(function() { 
                  	  window.location.href ="/ispbrain";
                  	}, 3000);
  
                  } else {
                  	generateNoty('error', 'Error .');
                  }
              }
          });
   }

  $(document).ready(function(){
    
    profile = connection.queue.profile;
    profile_up_name = Math.ceil(profile.up / 1024) + 'Mb/s (subida)';
    profile_down_name = Math.ceil(profile.down / 1024) + 'Mb/s (descarga)';
    profile_up_value = parseFloat(profile.up/1024);
    profile_down_value = parseFloat(profile.down/1024);
    
    
    initArrayData();
    
    // setValesArayData({up:56,down:123});
    
    // setValesArayData({up:56,down:123});
    
    // setValesArayData({up:56,down:123});
    
    // setValesArayData({up:56,down:123});
    
    google.charts.load('current', {'packages':['corechart']});
    
    google.charts.setOnLoadCallback(update);
  
  
  });
  

 
    
    
</script>