


<style type="text/css">
    iframe{
        
        width: 100%;
        height: 100%;
        
    }
</style>




<div class="row">
    <div class="col-xl-12">
        <div class="card border-secondary p-1 mb-2" id="card-customer-selector">
            <div class="card-body p-2">
                
                <div class="row">
                    <div class="col-xl-2 ">
                        <div class="input-group">
                            <span class="input-group-text" >Código</span>
                            <input type="text" class="form-control text-right" value="<?= sprintf("%'.05d", $connection->customer->code) ?>" readonly="true" value="">
                        </div>
                    </div>
                    <div class="col-xl-2">
                        <div class="input-group">
                            <span class="input-group-text">Doc.</span>
                            <input type="text" class="form-control text-right" value="<?=$connection->customer->ident?>" readonly="true" value="" >
                        </div>
                    </div>
                    <div class="col-xl-3">
                        <div class="input-group">
                            <span class="input-group-text">Nombre</span>
                            <input type="text" class="form-control text-right text-truncate" value="<?=$connection->customer->name?>" readonly="true" value="" >
                        </div>
                    </div>
                    
                    <div class="col-xl-2">
                        <div class="input-group">
                            <span class="input-group-text">IP</span>
                            <input type="text" class="form-control text-right" value="<?=$connection->ip?>" readonly="true" value="" >
                        </div>
                    </div>
                    
                    <div class="col-xl-3">
                        <div class="input-group">
                            <span class="input-group-text">Servicio</span>
                            <input type="text" class="form-control text-right" value="<?=$connection->service->name?>" readonly="true" value="" >
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    
    
    <div class="col-xl-6">
        
        <div class="card  mb-2">
            <div class="card-body p-3">
                <div class="row justify-content-between">
                    <div class="col-auto">
                        <h5 class="card-title text-secondary">Gráfico Diario</h5>
                    </div>
                    <div class="col-auto">
                        <small id="passwordHelpBlock" class="form-text text-muted">
                            Promedio de 5 minutos
                        </small>
                    </div>
                </div>
                <img class="rounded mx-auto d-block img-fluid" src="/ispbrain/queues_graphs/daily-<?=$connection->id?>.gif" alt="Gráfico Diario">
                <p class="card-text text-muted mt-2"><?=$caption['daily']?></p>
            </div>
        </div>
    </div>
    
    <div class="col-xl-6">
        
        <div class="card  mb-2">
            <div class="card-body p-3">
                <div class="row justify-content-between">
                    <div class="col-auto">
                        <h5 class="card-title text-secondary">Gráfico Semanal</h5>
                    </div>
                    <div class="col-auto">
                        <small id="passwordHelpBlock" class="form-text text-muted">
                            Promedio de 30 minutos
                        </small>
                    </div>
                </div>
                <img  class="rounded mx-auto d-block img-fluid" src="/ispbrain/queues_graphs/weekly-<?=$connection->id?>.gif" alt="Gráfico Semanal">
                <p class="card-text text-muted mt-2"><?=$caption['weekly']?></p>
            </div>
        </div>
    </div>
    
    <div class="col-xl-6">
        
        <div class="card  mb-2">
            <div class="card-body p-3">
                <div class="row justify-content-between">
                    <div class="col-auto">
                        <h5 class="card-title text-secondary">Gráfico Mensual</h5>
                    </div>
                    <div class="col-auto">
                        <small id="passwordHelpBlock" class="form-text text-muted">
                            Promedio de 2 horas
                        </small>
                    </div>
                </div>
                <img  class="rounded mx-auto d-block img-fluid" src="/ispbrain/queues_graphs/monthly-<?=$connection->id?>.gif" alt="Gráfico Mensual">
                <p class="card-text text-muted mt-2"><?=$caption['monthly']?></p>
            </div>
        </div>
    </div>
    
     <div class="col-xl-6">
        
        <div class="card  mb-2">
            <div class="card-body p-3">
                <div class="row justify-content-between">
                    <div class="col-auto">
                        <h5 class="card-title text-secondary">Gráfico Anual</h5>
                    </div>
                    <div class="col-auto">
                        <small id="passwordHelpBlock" class="form-text text-muted">
                            Promedio de 1 día
                        </small>
                    </div>
                </div>
                <img  class="rounded mx-auto d-block img-fluid" src="/ispbrain/queues_graphs/yearly-<?=$connection->id?>.gif" alt="Gráfico Anual">
                <p class="card-text text-muted mt-2"><?=$caption['yearly']?></p>
            </div>
        </div>
    </div>
    
    
</div>


<script type="text/javascript">


    
    
    
</script>





