 
 
<div class="row">
    <div class="col-xl-12">
        <div class="card border-secondary p-1 mb-2" id="card-customer-selector">
            <div class="card-body p-2">
                
                <div class="row">
                    <div class="col-xl-2 ">
                        <div class="input-group">
                            <span class="input-group-text" >Código</span>
                            <input type="text" class="form-control text-right" value="<?= sprintf("%'.05d", $connection->customer->code) ?>" readonly="true" value="">
                        </div>
                    </div>
                    <div class="col-xl-2">
                        <div class="input-group">
                            <span class="input-group-text">Doc.</span>
                            <input type="text" class="form-control text-right" value="<?=$connection->customer->ident?>" readonly="true" value="" >
                        </div>
                    </div>
                    <div class="col-xl-3">
                        <div class="input-group">
                            <span class="input-group-text">Nombre</span>
                            <input type="text" class="form-control text-right text-truncate" value="<?=$connection->customer->name?>" readonly="true" value="" >
                        </div>
                    </div>
                    
                    <div class="col-xl-2">
                        <div class="input-group">
                            <span class="input-group-text">IP</span>
                            <input type="text" class="form-control text-right" value="<?=$connection->ip?>" readonly="true" value="" >
                        </div>
                    </div>
                    
                    <div class="col-xl-3">
                        <div class="input-group">
                            <span class="input-group-text">Servicio</span>
                            <input type="text" class="form-control text-right" value="<?=$connection->service->name?>" readonly="true" value="" >
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
  <div class="col-12">
    
    <div id="chart_real_time_speed"></div>
    
  </div>
</div>
 
 
 <!--Div that will hold the pie chart-->
    



<script>
  
  var connection =  <?=json_encode($connection)?>
    
  var scaleH = 50;
    
  var TIMER = 3000;
    
  var rates_data = [];

  var profile = null;
  var profile_up_name = '';
  var profile_down_name = '';
  var profile_up_value = null;
  var profile_down_value = null


  function initArrayData(){
  
      rates_data = [];
      rates_data.push(['Tiempo', 'up', 'down', profile_up_name, profile_down_name]);
      for(var i=1; i < scaleH; i++){
        rates_data.push([i-1, null, null, profile_up_value, profile_down_value]);
      }
    }
    
  function setValesArayData(rates){
      
      rates_data = [];

      var i = 0;

      for(i = 0; i < rates.length; i++){
        rates_data.push( [(scaleH - i) , rates[i].up, rates[i].down, profile_up_value, profile_down_value] );
      }
      
      for(var j = i; j < scaleH - 1; j++){
        rates_data.push([(scaleH - j), null, null, profile_up_value, profile_down_value]);
      }
      
      rates_data.push(['Tiempo', 'subida', 'descarga', profile_up_name, profile_down_name]);
      rates_data.reverse();
      
    }
  
  function drawChart() {
   
      var data = google.visualization.arrayToDataTable(rates_data);

      var options = {
        title: 'Gráfica de velocidad ',
        hAxis: {
          textPosition: 'none',
          // maxAlternation: 3,
          minValue: 0,
          title: '(tiempo 3s)'
        },
        // hAxis: null,
        vAxis: { 
          // title: 'Tiempo',
          // titleTextStyle: {
          //   color: '#333'
          // },
          minValue: 0,
          format: 'decimal',
          title: '(Mb/s)'
        },
        areaOpacity : 0.3,
        backgroundColor: 'white',
        backgroundColor: {
          stroke: '#666',
          strokeWidth: 0,
          fill: 'white'
        },
        // chartArea:{left:20,top:0,width:'50%',height:'75%', backgroundColor: 'red'}
        chartArea: null,
        // colors:['red','#004411'],
        // explorer: { axis: 'vertical' }
        // explorer: { axis: 'horizontal' }
        // explorer: { actions: ['dragToZoom', 'rightClickToReset']},
        // explorer: { keepInBounds: true }
        // focusTarget: 'datum',
        height: 300,
        // width: 400,
        series: {
          0: { //up
             areaOpacity : 0,
             color: '#007bff'
          },
          1: { //down
            areaOpacity : 0.3,
            color: '#28a745',
          },
          2: { //down
            areaOpacity : 0,
            color: '#DF7401',
          },
          3: { //down
            areaOpacity : 0,
            color: 'red',
          },
        },
      };

      var chart = new google.visualization.AreaChart(document.getElementById('chart_real_time_speed'));
      chart.draw(data, options);
    }
    
  function restart(){
      
      $.ajax({
              url: "<?=$this->Url->build(['controller' => 'ConnectionsGraphic', 'action' => 'restartRealTimeGraph'])?>",
              type: 'POST',
              dataType: "json",
              data: JSON.stringify({ connection_id: connection.id }),
              success: function(data){
                  
                  if(data.response){
                    
                    setTimeout(update, TIMER);;
                    
                  }
            
              },
              error: function (jqXHR) {
  
                  if (jqXHR.status == 403) {
  
                  	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
  
                  	setTimeout(function() { 
                  	  window.location.href ="/ispbrain";
                  	}, 3000);
  
                  } else {
                  	generateNoty('error', 'Error .');
                  }
              }
          });
      
      
    }
    
  function update() {
     
    // console.log('update ...');
  		
  		$.ajax({
              url: "<?=$this->Url->build(['controller' => 'ConnectionsGraphic', 'action' => 'UpdateRealTimeGraph'])?>",
              type: 'POST',
              dataType: "json",
              data: JSON.stringify({ connection_id: connection.id }),
              success: function(data){
                  
                  if(data.rates && data.rates.length > 0){
                    
                    setValesArayData(data.rates);
                    drawChart();
                    setTimeout(update, TIMER);
                    
                  }else{
                    
                      bootbox.confirm('La gráfica esta pausada ó no existen valores para graficar. ¿Desea Reiniciar? ', function(result) {
                          
                          if (result) {
                            restart();
                          }
                      });
                  }
            
              },
              error: function (jqXHR) {
  
                  if (jqXHR.status == 403) {
  
                  	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
  
                  	setTimeout(function() { 
                  	  window.location.href ="/ispbrain";
                  	}, 3000);
  
                  } else {
                  	generateNoty('error', 'Error .');
                  }
              }
          });
   }

  $(document).ready(function(){
    
    profile = connection.queue.profile;
    profile_up_name = Math.ceil(profile.up / 1024) + 'Mb/s (subida)';
    profile_down_name = Math.ceil(profile.down / 1024) + 'Mb/s (descarga)';
    profile_up_value = parseFloat(profile.up/1024);
    profile_down_value = parseFloat(profile.down/1024);
    
    
    initArrayData();
    
    google.charts.load('current', {'packages':['corechart']});
    
    google.charts.setOnLoadCallback(update);
  
  
  });
  

 
    
    
</script>