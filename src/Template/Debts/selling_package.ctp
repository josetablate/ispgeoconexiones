<div class="row">

    <div class="col-sm-12 col-md-6 col-lg-5 col-xl-4">
        <?php if ($presale): ?>
            <?= $this->element('customer_info', ['presale' => $presale]) ?>
        <?php else: ?>
            <?= $this->element('customer_seeker', ['showbtn' => false, 'listCustomers' => true]) ?>
        <?php endif; ?>
    </div>

    <div class="col-sm-12 col-md-6 col-lg-7 col-xl-8">

        <div class="card border-secondary mb-3" >
            <h5 class="card-header"> 2. Vender Paquete </h5>
            <div class="card-body text-secondary pb-0">

                <?= $this->Form->create($debt_form,  ['id' => 'form-sell-package', 'class' => 'mb-0']) ?>

                <fieldset>

                <div class="row">
                    <div class="col-xl-12">
                         <div class="form-group text required">
                            <label class="control-label" for="code">Paquete <span class='fa fa-question-circle' title='Ingrese el código o nombre del Paquete. &nbsp;&nbsp;Con * despliega la lista completa de Paquetes.'  aria-hidden='true' ></span></label>
                            <input type="text" required="required" maxlength="45" id="autocompletePackages" disabled placeholder="Cargando Paquetes..." class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-12 col-lg-6 col-xl-3">
                        <label class="control-label" for="code">Precio</label>
                        <input type="text" name="package_total" class="form-control" id="package-total" readonly/>
                    </div>

                    <div class="col-md-12 col-lg-6 col-xl-3">
                        <div class="form-group date required">
                        	<label  class="control-label">Cuotas</label> 
                        	<select name="dues" id="dues" class="form-control">
                        	    <?php foreach ($dues_interest as $num => $di): ?>
                        	    <option data-interest="<?php echo  $di; ?>" value="<?php echo $num + 1; ?>"><?php echo $num + 1 . ' cuotas (' . $di * 100  . '%)'; ?></option>
                        	    <?php endforeach; ?>
                        	</select>
                    	</div>
                    </div>

                    <div class="col-md-12 col-lg-6 col-xl-3">
                        <label class="control-label" for="code">Cuota</label>
                        <input type="text" name="package_total_due" class="form-control" id="package-total-due" readonly/>
                    </div>

                     <div class="col-md-12 col-lg-6 col-xl-3">
                        <label  class="control-label">Vencimiento</label> 
                        <div class='input-group date' id='duedate-datetimepicker'>
                            <input name='duedate' id="duedate" type='text' class="form-control" required />
                            <span class="input-group-addon  input-group-text calendar">
                                <span class="glyphicon icon-calendar"></span>
                            </span>
                        </div>
                    </div>

                    <div class="col-12">
                         <div class="form-group text">
                            <?= $this->Form->input('discount_id', [
                                'label'    => 'Descuento (opcional)', 
                                'type'     => 'select', 
                                'empty'    => 'Seleccionar descuento', 
                                'options'  => $discounts_list
                            ]); ?>
                        </div>
                    </div>

                    <?php if ($presale->customer->billing_for_service): ?>
                        <?php if ($connections): ?>
                            <div class="col-12">
                                 <div class="form-group text">
                                    <?= $this->Form->input('connection_id', [
                                        'label'    => 'Pagar con el Servicio', 
                                        'type'     => 'select', 
                                        'empty'    => 'Seleccionar Servicio', 
                                        'options'  => $connections
                                    ]); ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>

                </div>

                <div class="row">
                    <?php if (!$presale): ?>
                    <div class="col-xl-12">
                        <?= $this->Form->input('concept', ['label' => 'Concepto', 'rows' => 2]); ?>
                    </div>
                    <?php endif; ?>
                </div>

                </fieldset>

                <?= $this->Form->end() ?> 
 
            </div>

            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <button name="btn-sell" value="" type="button" class="btn btn-success float-right" id="btn-sell"><?= $presale ? 'Cargar' : 'Vender' ?></button>
                    <button name="btn-sell-exit" value="exit" type="button" class="btn btn-primary float-right mr-3" id="btn-sell-exit"><?= $presale ? 'Cargar y Salir' : 'Vender y Salir' ?></button>
                    <?php if ($presale): ?>
                        <?= $this->Html->link(__('Salir'), ["controller" => "presales", "action" => "add"], ['title' => 'Ir a la Venta', 'class' => 'btn btn-default ml-2']) ?>
                    <?php endif; ?>
                </li>
            </ul>

        </div>
    </div>

</div>

<script type="text/javascript">

    $('#duedate-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: <?= json_encode($debt_form->duedate) ?>,
        format: 'DD/MM/YYYY',
        icons: {
            time: "glyphicon icon-alarm",
            date: "glyphicon icon-calendar",
            up: "glyphicon icon-arrow-right",
            down: "glyphicon icon-arrow-left"
        }
    });

    var package_select = null; 
    var amount = 1;
    var interest = 1;
    var dues = 1;
    var presale = null;
    var discount_select = null;
    var discounts = null;
    var redirect = "";

    $(document).ready(function() {

        redirect = "";

        presale = <?= json_encode($presale) ?>;
        if (presale) {
            customer_selected = presale.customer;
        }
        discounts = <?= json_encode($discounts) ?>;

        $('#card-customer-seeker').on('CUSTOMER_SELECTED', function() {
            $('#autocompletePackages').focus();
        });

        $('#btn-sell').click(function() {
            redirect = "";
           $('#form-sell-package').submit();
        });

        $('#btn-sell-exit').click(function() {
            redirect = "exit";
           $('#form-sell-package').submit();
        });

        $('#form-sell-package').submit(function(e) {

            e.preventDefault();

            if (presale) {
                if (!customer_selected) {
                    generateNoty('warning', 'Debe especificar un cliente.');
                } else if (!package_select) {
                    generateNoty('warning', 'Debe especificar un paquete.');
                } else {
                    bootbox.confirm('Confirmar', function(result) {
                        if (result) {

                            var send_data = {};

                            send_data.package_id = package_select.id;
                            send_data.from = "presale";
                            send_data.redirect = redirect;

                            send_data.discount_id = null;
                            send_data.discount_name = null;
                            send_data.discount_total = null;

                            if (discount_select) {
                                send_data.discount_id = discount_select.id;
                                send_data.discount_name = discount_select.concept;
                                send_data.discount_total = discount_select.value;
                            }

                            var form_data = $('#form-sell-package').serializeArray();
                            $.each(form_data, function(i, val){
                                send_data[val.name] = val.value;
                            });

                            $('body')
                                .append( $('<form/>').attr({'action': '/ispbrain/debts/selling_package/', 'method': 'post', 'id': 'form-block'})
                                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'data', 'value': JSON.stringify(send_data)}))
                                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                                .find('#form-block').submit();

                            $('#btn-sell').hide();
                            $('#btn-sell-exit').hide();
                        }
                    });
                }

            } else {
                if (!customer_selected) {
                    generateNoty('warning', 'Debe especificar un cliente.');
                } else if (!package_select) {
                    generateNoty('warning', 'Debe especificar un paquete.');
                } else {

                    bootbox.confirm('Confirmar', function(result) {
                        if (result) {

                            var send_data = {};
                            send_data.from = "";
                            send_data.customer_code = customer_selected.code;
                            send_data.customer_account_code = customer_selected.account_code;

                            send_data.package_id = package_select.id;

                            var form_data = $('#form-sell-package').serializeArray();
                            $.each(form_data, function(i, val){
                                send_data[val.name] = val.value;
                            });

                            $('body')
                                .append( $('<form/>').attr({'action': '/ispbrain/debts/selling_package/', 'method': 'post', 'id': 'form-block'})
                                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'data', 'value': JSON.stringify(send_data)}))
                                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                                .find('#form-block').submit();

                            $('#btn-sell').hide(); 
                        }
                    });
                }
            }

        });

        $("select#dues").change(function () {

            if (package_select) {

                $( "select#dues option:selected" ).each(function() {

                    interest = $(this).data('interest');

                    dues = $(this).val();

                    var package_price = package_select.price

                    if (discount_select) {
                        package_price = package_price * (1 - discount_select.value);
                    }

                    var sum_price = package_price * amount;
                    var interest_ = parseFloat(interest) + 1;
                    sum_price = sum_price * interest_;

                    var total = sum_price.toFixed(2);

                    $('input#package-total').val(total);
                    var total_due = total / dues;
                    total_due = total_due.toFixed(2);

                    $('input#package-total-due').val(total_due);
                });
            }
        });

        $('#discount-id').change(function() {
            if (package_select) {
                var value = $(this).val();
                var flag = false;
                $.each(discounts, function( index, discount ) {
                    if (value == discount.id) {
                        flag = true;
                        discount_select = discount;
                    }
                });
                if (!flag) {
                    discount_select = null;
                }

                var due_selected = $('select#dues').find(":selected");

                interest = due_selected.data('interest');

                dues = due_selected.val();

                var package_price = package_select.price

                if (discount_select) {
                    package_price = package_price * (1 - discount_select.value);
                }

                var sum_price = package_price * amount;
                var interest_ = parseFloat(interest) + 1;
                sum_price = sum_price * interest_;

                var total = sum_price.toFixed(2);

                $('input#package-total').val(total);
                var total_due = total / dues;
                total_due = total_due.toFixed(2);

                $('input#package-total-due').val(total_due);
            } else {
                $("#discount-id").val("");
                generateNoty('warning', 'Debe seleccionar primero un paquete para poder aplicar el descuento');
            }
        });

        $( "#autocompletePackages" ).val('');
 
        var packages = [];

        $.ajax({
            url: "<?= $this->Url->build(['controller' => 'Packages', 'action' => 'getAllAjax']) ?>",
            type: 'POST',
            dataType: "json",
            success: function(data) {

                if (data.packages != false) {

                    $.each(data.packages, function(i, val) {

                        var ui = {
                            id: val.id,
                            label: val.name + ' ($' + val.price + ')' + ' *',
                            value: val.name,
                            aliquot:val.aliquot,
                            price:val.price
                        };
                        packages.push(ui);
                    });

                    //carga el campo de autocompletado con los paquetes obtenidoss
                    $( "#autocompletePackages" ).autocomplete({
                    	source: packages,
                    	create: function( event, ui ) {
                           $( "#autocompletePackages" ).attr('disabled', false);
                           $( "#autocompletePackages" ).attr('placeholder', 'Seleccione el paquete.');
                        },
                        select: function( event, ui ) {

                            package_select = ui.item;

                            interest = $( "select#dues option:selected" ).data('interest');

                            dues = $( "select#dues option:selected" ).val();
                            amount = 1;

                            var sum_price = package_select.price * amount;
                            var interest_ = parseFloat(interest) + 1;

                            sum_price = sum_price * interest_;

                            var total = sum_price.toFixed(2);
                            $('input#package-total').val(total);
                            var total_due = total / dues;
                            total_due = total_due.toFixed(2);
                            $('input#package-total-due').val(total_due);

                            $('#concept').html(package_select.value);
                        }
                    }); 
                } else { //no hay paquetes

                    $( "#autocompletePackages" ).addClass('placeholder-error');
                    $( "#autocompletePackages" ).attr('placeholder', 'No existen Paquetes.');
                }
            },
            error: function (jqXHR) {
                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() { 
                        window.location.href = "/ispbrain";
                	}, 3000);

                } else {
                	generateNoty('error', 'Error al obtener los Paquetes.');
                }
            }
        });
     });

</script>
