


<div class="col-md-8">
    <legend class="sub-title">Importar CSV Deudas</legend>
        <?= $this->Form->create($debt, ['type' => 'file','class'=>'form-inline','role'=>'form']) ?>
        <div class="form-group">
            <label class="sr-only" for="csv"> CSV </label>
            <?php echo $this->Form->input('csv', ['type'=>'file', 'accept' => '.csv', 'class' => 'form-control', 'label' => false, 'placeholder' => 'Subir CSV',]); ?>
        </div>
        <button type="submit" class="btn btn-default"> Subir </button>
    <?= $this->Form->end() ?>
</div>