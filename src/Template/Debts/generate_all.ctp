<style type="text/css">

    input[type=checkbox] {
        transform: scale(2.0);
        margin-top: 20px;
        margin-right: 10px;
        margin-left: 5px;
    }

    .no_force_debt {
        background-color: black;
        color: white !important;
    }

    .table-hover tbody tr:hover {
        background-color: white !important;
    }

    .table-hover tbody tr.selectable:hover {
        background-color: rgba(105, 104, 104, 0.28) !important;
    }

</style>

<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5">

        <?php
            echo  $this->Html->link(
                '<span class="glyphicon icon-checkbox-checked" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Seleccionar todos',
                'class' => 'btn btn-default btn-selected-all ml-2',
                'escape' => false
            ]);

            echo  $this->Html->link(
                '<span class="glyphicon icon-checkbox-unchecked" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Limpiar Selección',
                'class' => 'btn btn-default btn-clear-selected-all ml-2',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="fas fa-sync-alt"  aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Actualizar la tabla',
                'class' => 'btn btn-default btn-update-table ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon fas fa-search"  aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Buscar por Columnas',
                'class' => 'btn btn-default btn-search-column ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel"  aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export ml-1',
                'escape' => false
            ]);
        ?>

    </div>
</div>

<div id="moda-paraments" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel">Parámetros</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-12">

                        <small id="passwordHelpBlock" class="form-text text-muted mb-2">
                              <b>Aclaración: </b> al aplicar la configuración si realizó alguna selección de deudas, las mismas se borraran.
                        </small>

                        <?= $this->Form->create($debt_form, ['id' => 'form-generate']) ?>
                            <fieldset>

                                <div class="row">
                                    <div class="col-md-6 col-xl-6">

                                        <label class="control-label" for="">Período</label>
                                        <div class='input-group date' id='debt_month-datetimepicker'>
                                            <input name='debt_month'id="debt-month" type='text' class="form-control" />
                                            <span class="input-group-addon input-group-text calendar">
                                                <span class="glyphicon icon-calendar"></span>
                                            </span>
                                        </div>

                                    </div>
                                    <div class="col-md-6 col-xl-6">

                                        <?= $this->Form->input('customer_duedate_check', ['label' => 'Vencimiento individual ', 'type' => 'checkbox', 'checked' => true]) ?>

                                        <div id="custom_duedate_table">

                                            <label class="control-label" for="">Vencimiento</label>

                                            <div class='input-group date' id='custom_duedate-datetimepicker'>
                                                <input name='duedate' id="duedate" type='text' class="form-control" />
                                                <span class="input-group-addon input-group-text calendar">
                                                    <span class="glyphicon icon-calendar"></span>
                                                </span>
                                            </div>

                                        </div>

                                        <div id="customer_duedate_table">

                                            <label class="control-label" for="">Vencimiento</label>

                                            <div class='input-group date' id='customer_duedate-datetimepicker'>
                                                <input name='duedate_month' id="duedate-month" type='text' class="form-control" />
                                                <span class="input-group-addon input-group-text calendar">
                                                    <span class="glyphicon icon-calendar"></span>
                                                </span>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="col-md-12 col-xl-12">
                                        <?= $this->Form->input('custom_concept_check', ['label' => 'Especificar Concepto', 'type' => 'checkbox', 'checked' => false]) ?>
                                        <?= $this->Form->input('concept', ['label' => 'Concepto', 'type' => 'textarea', 'rows' => "2" ]) ?>
                                        <?= $this->Form->input('generate_debt_blocking_connection', ['label' => 'Generar Deudas Conexiones Bloqueadas', 'type' => 'checkbox', 'checked' => TRUE]) ?>
                                        <?= $this->Form->input('force_presupuesto', ['label' => 'Forzar Presupuesto', 'type' => 'checkbox', 'checked' => FALSE]) ?>
                                    </div>
                                </div>
                                <div class="row justify-content-end">

                                    <div class="col-sm-5  col-xl-6">

                                        <?php
                                            echo $this->Html->link(__('Aplicar'), 
                                                'javascript:void(0)', [
                                                'title' => 'Aplicar configuración', 
                                                'class' => 'btn btn-preview btn-secondary w-100 mb-2'
                                            ]);
                                        ?>

                                    </div>
                                </div>

                            </fieldset>

                        <?= $this->Form->end() ?>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div id="moda-edit-customer" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel">Editar Cliente</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-xl-12">

                        <?= $this->Form->create(null,  ['id' => 'form-edit-customer']) ?>
                            <fieldset>

                                <div class="row">
                                    <div class="col-md-12 col-xl-12">

                                        <?php 
                                            echo $this->Form->input('business_billing', ['label' => 'Empresa de Facturación', 'options' => $business, 'required' => true]);
                                            echo $this->Form->input('responsible', ['label' => 'Resp. ante el IVA', 'options' => $this->request->getSession()->read('afip_codes')['responsibles'], 'required' => true]);
                                            echo $this->Form->input('is_presupuesto', ['label' => 'Prespuesto', 'type' => 'checkbox']);
                                            
                                            if($this->request->getSession()->read('paraments')->gral_config->value_service_customer){
                                                echo $this->Form->input('value_service', ['label' => 'Valor de servicio', 'required' => false, 'type' => 'number']);
                                            }
                                                        
                                            
                                            echo $this->Form->input('daydue', ['label' => 'Día de Vencimiento',  'min' => 1, 'max' => 28, 'required' => true]);
                                            echo $this->Form->input('code', ['type' => 'hidden']);
                                        ?>

                                    </div>
                                </div>
                                <div class="row justify-content-end">

                                    <div class="col-sm-5  col-xl-6">

                                        <?php
                                            echo $this->Html->link(__('Guardar'), 
                                                'javascript:void(0)',[
                                                'title' => '', 
                                                'class' => 'btn btn-edit-customer btn-secondary w-100'
                                            ]);
                                        ?>

                                    </div>
                                </div>

                            </fieldset>

                        <?= $this->Form->end() ?>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div id="modal-confirm-generation" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel">Confirmar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-md-6">

                        <div class="card border-secondary mb-3">
                            <h5 class="card-header">Se generará:</h5>
                            <div class="card-body text-secondary container-total p-2">
                            </div>
                        </div>

                    </div>

                    <div class="col-xl-6">
                        <h5 class="card-header">Parámetros:</h5>
                        <div class="row">
                            <div class="col-xl-12">
                                <label><i class="fa fa-angle-double-right fa-w-14"></i> Período: <span id="confirm-period"></span></label>
                            </div>
                            <div class="col-xl-12">
                                <label><i class="fa fa-angle-double-right fa-w-14"></i> Vencimiento individual: <span id="confirm-customer-duedate-check"></span></label>
                            </div>
                            <div class="col-xl-12">
                                <label><i class="fa fa-angle-double-right fa-w-14"></i> Vencimiento: <span id="confirm-duedate"></span></label>
                            </div>
                            <div class="col-xl-12">
                                <label><i class="fa fa-angle-double-right fa-w-14"></i> Concepto: <span id="confirm-concept"></span></label>
                                <label><i class="fa fa-angle-double-right fa-w-14"></i> Generar Deudas Conexiones Bloqueadas: <span id="confirm-generate-debt-blocking-connection"></span></label>
                                <label><i class="fa fa-angle-double-right fa-w-14"></i> Forzar Presupuesto: <span id="confirm-force-presupuesto"></span></label>
                            </div>
                            <div class="col-xl-12">
                                <label style="font-size: 20px;"><i class="fa fa-angle-double-right fa-w-14"></i> Total deudas a generar: <span id="total-debts"></span> de <span id="super-total-debts"></label>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button id="btn-recontra-confirm" type="button" class="btn btn-primary">Confirmar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-md-12 col-lg-2 col-xl-2 pr-1">

        <div class="card border-secondary mb-3">
            <div class="card-header">
                <h5>Período: <span id="periode-selected"></span></h5>
                <h5>Totales</h5>
            </div>
            <div class="card-body text-secondary container-total p-2">
            </div>
        </div>

    </div>

    <div class="col-md-12 col-lg-10 col-xl-10 pl-0">

        <div class="card border-secondary mb-3">

            <div class="row justify-content-between card-header pt-2 pb-1">
                <div class="col-2">
                    <h5 class="">Vista Previa</h5>
                </div>
                <div class="col-10">

                    <?php

                        echo $this->Html->link(__('Ir al Facturador'), 
                            [
                                "controller" => "invoices",
                                "action" => "generateMasiveFromDebts"
                            ],
                            [
                                'title' => '', 
                                'class' => 'btn btn-default float-right ml-2'
                        ]);

                        echo $this->Html->link(__('Generar Deudas'), 
                            'javascript:void(0)', [
                            'id' => 'btn-confirm',
                            'title' => 'Ir a la lista de clientes', 
                            'class' => 'btn btn-danger float-right ml-2',
                        ]);

                        echo $this->Html->link(__('Configuración General'), 
                            'javascript:void(0)', [
                            'title' => 'Configurar parámetros de generación de deudas', 
                            'class' => 'btn btn-default float-right ml-2',
                            'id' => 'btn-paraments'
                        ]);
                    ?>

                </div>
            </div>

            <div class="card-body text-secondary">

                <table class="table table-bordered table-hover" id="table-debts">
                    <thead>
                        <tr>
                            <th></th>                                <!--0-->
                            <th>Código Cliente</th>                  <!--1-->
                            <th>Doc. Cliente</th>                    <!--2-->
                            <th>Nombre de Cliente</th>               <!--3-->
                            <th>Cliente</th>                         <!--4-->
                            <th title="Facturas Impagas" >F. I.</th> <!--5-->
                            <th>Concepto</th>                        <!--6-->
                            <th>Venc.</th>                           <!--7-->
                            <th>Total</th>                           <!--8-->
                            <th>Empresa</th>                         <!--9-->
                            <th>Destino</th>                         <!--10-->
                            <th>Estado de Conexión</th>              <!--11-->
                            <th>Área</th>                            <!--12-->
                            <th>Descuento</th>                       <!--13-->
                        </tr> 
                    </thead>
                    <tbody>
                    </tbody>
                </table>

            </div>
        </div>

    </div>

</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-edit-debt',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-force_debt',
        'name' =>  'Forzar Deuda',
        'icon' =>  'far fa-check-circle',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-no-force_debt',
        'name' =>  'No generar deuda',
        'icon' =>  'far fa-times-circle',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-go-customer',
        'name' =>  'Ficha del Cliente',
        'icon' =>  'glyphicon icon-profile',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-go-connection',
        'name' =>  'Ir a la Conexión',
        'icon' =>  'glyphicon icon-profile',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal' => 'modal-actions-debts', 'title' => 'Acciones', 'buttons' => $buttons]);
    echo $this->element('search_columns', ['modal' => 'modal-search-columns-debts']);
    echo $this->element('modal_preloader');
?>

<script type="text/javascript">

    var init_date = <?= json_encode($debt_form->initdate) ?>;
    var init_duedate = <?= json_encode($debt_form->duedate) ?>;
    var table_debts = null;

    var get_preview = false;
    var areas = null;
    var ids_selected = {};
    var businessTotales;

    function array_key_exists (key, search) {
        // Checks if the given key or index exists in the array
        // 
        // version: 1109.2015
        // discuss at: http://phpjs.org/functions/array_key_exists    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // +   improved by: Felix Geisendoerfer (http://www.debuggable.com/felix)
        // *     example 1: array_key_exists('kevin', {'kevin': 'van Zonneveld'});
        // *     returns 1: true
        // input sanitation
        if (!search || (search.constructor !== Array && search.constructor !== Object)) {
            return false;
        }

        return key in search;
    }

    function genrateTotales() {

        businessTotales = {};

        $.each(sessionPHP.paraments.invoicing.business, function(index, business) {

            if (business.responsible == 1) {

                businessTotales[business.id] = {
                    'XXX': 0,
                    '001': 0,
                    '006': 0,
                    'total': 0,
                    'address': business.address,
                    'name': business.name
                };

            } else if (business.responsible == 6) {

                businessTotales[business.id] = {
                    'XXX': 0,
                    '011': 0,
                    'total': 0,
                    'address': business.address,
                    'name': business.name
                };
            }
        });

        businessTotales['total'] = {'total': 0};

        $.each(ids_selected, function(index, debt) {

            if (debt.force_debt) {

                debt.tipo_comp = $('#moda-paraments #force-presupuesto').is(":checked") ? 'XXX' : debt.tipo_comp;

                businessTotales[debt.business.id][debt.tipo_comp] += debt.total;
                businessTotales[debt.business.id]['total'] += debt.total;
                businessTotales['total']['total'] += debt.total;

                if (debt.customers_has_discount) {
                    businessTotales[debt.business.id][debt.tipo_comp] -= debt.customers_has_discount.total;
                    businessTotales[debt.business.id]['total'] -= debt.customers_has_discount.total;
                    businessTotales['total']['total'] -= debt.customers_has_discount.total;
                }
            }
        });

        var html = '';

        $.each(businessTotales, function(id, types_compr_totales) {

            if (id != 'total') {
                html += '<i class="fas fa-angle-right mr-2 mt-2 "></i>' + types_compr_totales.name + '<br> (' + types_compr_totales.address + ')';
            } else {
                html += '<br>';
            }

            $.each(types_compr_totales, function(tipo, subtotal) {

                if (tipo != 'address' && tipo != 'name' ) {

                    if (tipo != 'total') {
                        html += '<table class="w-100">';
                        html += '   <tr>';
                        html += '       <td style="width: 5%; text-align: left;">';
                        html += '           <i class="fas fa-angle-double-right mr-2"></i>';
                        html += '       </td>';
                        html += '       <td style="width: 30%; text-align: left;" >';

                        if (tipo != 'gratis') {
                            html +=             sessionPHP.afip_codes.comprobantesLetter[tipo];
                        } else {
                            html +=             tipo;
                        }

                        html += '       </td>';
                        html += '       <td style="width: 10%; text-align: left;" >';
                        html += '       </td>';
                        html += '       <td style="width: 55%; text-align: right;">';

                        if (name == 'total') {
                            html += '       <span class="mt-2" style="border-top: 1px solid; font-weight: bold;" >';
                            html +=             number_format(subtotal, true);
                            html += '       </span>';
                        } else {
                            html +=         number_format(subtotal, true);
                        }
                        html += '       </td>';
                        html += '   </tr>';
                        html += '</table>';
                    } else {

                        html += '<table class="w-100">';
                        html += '   <tr>';
                        html += '       <td style="width: 5%; text-align: left;">';
                        html += '           <i class="fas fa-angle-double-right mr-2"></i>';
                        html += '       </td>';
                        html += '       <td style="width: 30%; text-align: left;" >';
                        html += '       </td>';
                        html += '       <td style="width: 10%; text-align: left;" >';
                        html += '       </td>';
                        html += '       <td style="width: 55%;text-align: right;border-top: 1px solid #686868; font-weight:  bold;" >';
                        html +=             number_format(subtotal, true);
                        html += '       </td>';
                        html += '   </tr>';
                        html += '</table>';
                    }
                
                }
            });
        });

        $('.container-total').html(html);
    }

    function buildTotales() {

        businessTotales = {};

        $.each(sessionPHP.paraments.invoicing.business, function(index, business) {

            if (business.responsible == 1) {

                businessTotales[business.id] = {
                    'XXX': 0,
                    '001': 0,
                    '006': 0,
                    'total': 0,
                    'address': business.address,
                    'name': business.name
                };

            } else if (business.responsible == 6) {

                businessTotales[business.id] = {
                    'XXX': 0,
                    '011': 0,
                    'total': 0,
                    'address': business.address,
                    'name': business.name
                };
            }
        });

        businessTotales['total'] = {'total': 0};

        $.each(ids_selected, function(index, debt) {

            if (debt.force_debt) {

                debt.tipo_comp = $('#moda-paraments #force-presupuesto').is(":checked") ? 'XXX' : debt.tipo_comp;

                businessTotales[debt.business.id][debt.tipo_comp] += debt.total;
                businessTotales[debt.business.id]['total'] += debt.total;
                businessTotales['total']['total'] += debt.total;

                if (debt.customers_has_discount) {
                    businessTotales[debt.business.id][debt.tipo_comp] -= debt.customers_has_discount.total;
                    businessTotales[debt.business.id]['total'] -= debt.customers_has_discount.total;
                    businessTotales['total']['total'] -= debt.customers_has_discount.total;
                }
            }
        });

        return businessTotales;
    }

    $(document).ready(function() {

        var options_business = [];

        $.each(sessionPHP.paraments.invoicing.business, function(i, val) {
            options_business.push({id:val.id, name:val.name + ' (' + val.address + ')'});
        });

        areas = <?= json_encode($areas) ?>;

        $('#btn-paraments').click(function() {
           $('#moda-paraments').modal('show');
        });

        $('#custom_duedate_table').hide();
        $('#customer_duedate_table').show();

        $('#customer-duedate-check').change(function() {
            if ($(this).is(':checked')) {
                $('#custom_duedate_table').hide();
                $('#customer_duedate_table').show();
            } else {
                $('#customer_duedate_table').hide();
                $('#custom_duedate_table').show();
            }
        });

        $( "#concept" ).prop( "disabled", true );

        $('#custom-concept-check').change(function() {
            if ($(this).is(':checked')) {
                $( "#concept" ).prop( "disabled", false );
            } else {
                $( "#concept" ).prop( "disabled", true );
            }
        });

        $('#debt_month-datetimepicker').datetimepicker({
            locale: 'es',
            defaultDate: init_date,
            viewMode: 'months',
            format: 'MM/YYYY'
        });

        $('#customer_duedate-datetimepicker').datetimepicker({
            defaultDate: init_duedate,
            viewMode: 'months',
            format: 'MM/YYYY'
        });

        $('#custom_duedate-datetimepicker').datetimepicker({
            defaultDate: init_duedate,
            format: 'DD/MM/YYYY'
        });

        $('#periode-selected').text($('#debt-month').val());

        var form_generate_data = {};

        $('.btn-edit-customer').click(function() {

            var dataArray = $("#form-edit-customer").serializeArray();
            var send_data = {};
            $.each(dataArray, function(i, input) {
                send_data[input.name] = input.value;
            });

            openModalPreloader("Editando cliente ...");

            $.ajax({
                url: "<?= $this->Url->build(['controller' => 'Customers', 'action' => 'editCustomerFromDebt']) ?>",
                type: 'POST',
                dataType: "json",
                data: JSON.stringify(send_data),
                success: function(data) {

                    if (data.customer) {
                        if (table_debts) {
                            table_debts.ajax.reload(function () {
                                table_debts.rows().every( function () {
                                    var d = this.data();
                                    if (array_key_exists(d.id, ids_selected) && d.customer.code == debt_selected.customer.code) {
                                        ids_selected[d.id] = d;
                                    }
                                });
                                genrateTotales();
                            });
                            generateNoty('success', 'El cliente se edito correctamente.');
                        }
                    } else {
                        generateNoty('error', 'Error al intentar editar el cliente.');
                    }

                    closeModalPreloader();

                },
                error: function (jqXHR) {
                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al intentar editar el cliente.');
                        closeModalPreloader();
                    }
                }
            });

            $('#moda-edit-customer').modal('hide');
        });

        $('#btn-edit-debt').click(function() {

            $('#moda-edit-customer #code').val(debt_selected.customer.code);
            $('#moda-edit-customer #responsible').val(debt_selected.customer.responsible);
            $('#moda-edit-customer #is-presupuesto').attr('checked', debt_selected.customer.is_presupuesto);
            $('#moda-edit-customer #daydue').val(debt_selected.customer.daydue);
            $('#moda-edit-customer #business-billing').val(debt_selected.customer.business_billing);
            $('#moda-edit-customer #value-service').val(debt_selected.customer.value_service);

            $('.modal-actions-debts').modal('hide');

            $('#moda-edit-customer').modal('show');
        });

        $('.btn-preview').click(function() {

            if (Object.keys(ids_selected).length == 0) {

                var dataArray = $("#form-generate").serializeArray();
                form_generate_data = {};
                $.each(dataArray, function(i, input) {
                    form_generate_data[input.name] = input.value;
                });

                if (table_debts) {
                     table_debts.ajax.reload();
                }

                $('#periode-selected').text($('#debt-month').val());

                $('#moda-paraments').modal('hide');
            } else {

                bootbox.confirm('Se ha seleccionado deudas al aplicar la configuración se borrarán dicha selección, ¿desea continuar?', function(result) {

                    if (result) {

                        ids_selected = {};

                        var dataArray = $("#form-generate").serializeArray();
                        form_generate_data = {};
                        $.each(dataArray, function(i, input) {
                            form_generate_data[input.name] = input.value;
                        });

                        if (table_debts) {
                             table_debts.ajax.reload();
                        }

                        $('#periode-selected').text($('#debt-month').val());

                        $('#moda-paraments').modal('hide');
                    }
                });
            }
        });

        var draw_dt = false;

        $('#btn-confirm').click(function() {
            if (Object.keys(ids_selected).length == 0) {
                generateNoty('warning', 'La tabla está vacia, debe seleccionar otro período.');
            } else {
                var concept = $('#moda-paraments #concept').val();
                if ($('#moda-paraments #custom-concept-check').is(":checked") && concept == "") {
                    generateNoty('warning', 'Se van a generar deudas con conceptos vacios, debe agregar concepto desde la Configuración general o destildar especificar concepto, para que el sistema agregue el concepto.');
                } else {
                    $('#modal-confirm-generation').modal('show');
                }
            }
        });

        $('#btn-recontra-confirm').click(function() {

            $('#btn-recontra-confirm').hide();

            $('#modal-confirm-generation').modal('hide');

            var dataArray = $("#form-generate").serializeArray();
            form_generate_data = {};
            $.each(dataArray, function(i, input) {
                form_generate_data[input.name] = input.value;
            });

            form_generate_data['ids_selected'] = ids_selected;

            let data_confirm = {
                confirm_period: $('#modal-confirm-generation #confirm-period').text(),
                confirm_customer_duedate_check: $('#modal-confirm-generation #confirm-customer-duedate-check').text(),
                confirm_duedate: $('#modal-confirm-generation #confirm-duedate').text(),
                confirm_concept: $('#modal-confirm-generation #confirm-concept').text(),
                confirm_generate_debt_blocking_connection: $('#modal-confirm-generation #confirm-generate-debt-blocking-connection').text(),
                confirm_force_presupuesto: $('#modal-confirm-generation #confirm-force-presupuesto').text(),
                super_total_debts: $('#modal-confirm-generation #super-total-debts').text(),
                build_total: buildTotales()
            };

            openModalPreloader("Generando Deudas. Espere Por favor ...");

            $.ajax({
                url: "<?= $this->Url->build(['controller' => 'Debts', 'action' => 'generate']) ?>",
                type: 'POST',
                dataType: "json",
                data: JSON.stringify({
                    ids: form_generate_data,
                    data_confirm: data_confirm
                }),
                success: function(data) {

                    if (table_debts) {
                        var dataArray = $("#form-generate").serializeArray();
                        form_generate_data = {};
                        $.each(dataArray, function(i, input) {
                            form_generate_data[input.name] = input.value;
                        });
                        ids_selected = {};
                        table_debts.ajax.reload();
                    }

                    closeModalPreloader();
                    generateNoty('success', 'Deudas generadas correctamente.');
                },
                error: function (jqXHR) {

                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                        table_debts.ajax.reload();
                    	closeModalPreloader();
                        generateNoty('error', 'Error al generar las deudas.');
                    }
                }
            });
        });

        $('#modal-confirm-generation').on('shown.bs.modal', function (e) {

            $('#btn-recontra-confirm').show();

            $('#modal-confirm-generation #confirm-period').text($('#moda-paraments #debt-month').val());
            var flag = "No";
            var confirm_duedate = $('#moda-paraments #duedate').val();

            if ($('#moda-paraments #customer-duedate-check').is(":checked")) {
                flag = "Si";
                confirm_duedate = $('#moda-paraments #duedate-month').val();
            }
            $('#modal-confirm-generation #confirm-customer-duedate-check').text(flag);

            $('#modal-confirm-generation #confirm-duedate').text(confirm_duedate);

            var concept = "Genero por el sistema";

            if ($('#moda-paraments #custom-concept-check').is(":checked")) {
                concept = $('#moda-paraments #concept').val();
            }
            $('#modal-confirm-generation #confirm-concept').text(concept);

            var connection_block = "No";

            if ($('#moda-paraments #generate-debt-blocking-connection').is(":checked")) {
                connection_block = "Si";
            }
            $('#modal-confirm-generation #confirm-generate-debt-blocking-connection').text(connection_block);

            var presupuesto = "No";

            if ($('#moda-paraments #force-presupuesto').is(":checked")) {
                presupuesto = "Si";
            }
            $('#modal-confirm-generation #confirm-force-presupuesto').text(presupuesto);

            $('#modal-confirm-generation #total-debts').text(Object.keys(ids_selected).length);
            $('#modal-confirm-generation #super-total-debts').text(table_debts.page.info().recordsTotal);
        });

        $('#form-generate').submit(function(e) {
            e.preventDefault();
        });

        $('#btns-tools').show();

        $('.btn-search-column').click(function() {
            $('.modal-search-columns-debts').modal('show');
        });

        $('.btn-preview').click();

        var businessTotales = [];

        $('#table-debts').removeClass('display');
        table_debts = $('#table-debts').DataTable({
		    "order": [[ 1, 'asc' ]],
            "deferRender": true,
            "processing": true,
            "serverSide": true,
		    "ajax": {
                "url": "/ispbrain/debts/debt_preview.json",
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    genrateTotales();
                    return JSON.stringify( json.response );
                },
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "form_generate_data": form_generate_data
                    });
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "drawCallback": function( settings ) {
                var flag = true;
            	switch (settings.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
            	    if (table_debts) {
                        var tableinfo = table_debts.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }
                    }
            	}
            },
            "scrollY": true,
		    "scrollY": '430px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
            "columns": [
                {
                    "className":      'checkbox-control',
                    "orderable":      false,
                    "data":           'id',
                    "render": function ( data, type, full, meta ) {
                        return '<label class="custom-control custom-checkbox pl-3 pr-3 m-0 align-middle"><input type="checkbox" class="custom-control-input checkbox-select p-0 m-0 " id="row-checkbox-' + data + '"><span class="custom-control-indicator w-100 align-middle"></span></label>';
                    },
                    "width": '3%'
                },
                {
                    "className": "row-control",
                    "data": "customer.code",
                    "type": "integer",
                },
                {
                    "className": "row-control",
                    "data": "customer.ident",
                    "type": "integer",
                },
                {
                    "className": "row-control",
                    "data": "customer.name",
                    "type": "string",
                },
                {
                    "className": "row-control",
                    "data": "customer",
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (data.ident) {
                            ident = data.ident;
                        }
                        return pad(data.code, 5) + ' ' + sessionPHP.afip_codes.doc_types[data.doc_type] + ' ' + ident + ' ' + data.name;
                    }
                },
                {
                    "className": "row-control",
                    "data": "f_impagas",
                    "type": "decimal",
                },
                {
                    "className": "row-control",
                    "data": "concept",
                    "type": "string",
                },
                {
                    "className": "row-control",
                    "data": "duedate",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var duedate = data.split('T')[0];
                            duedate = duedate.split('-');
                            return duedate[2] + '/' + duedate[1] + '/' + duedate[0];
                        }
                    }
                },
                {
                    "className": "row-control",
                    "data": "total",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' ),
                },
                {
                    "className": "row-control",
                    "data": "business",
                    "type": "options_obj",
                    "options": options_business,
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data.name;
                        }
                    }
                    
                },
                {
                    "className": "row-control",
                    "data": "tipo_comp",
                    "type": "options",
                    "options": {
                        "XXX": "PRESU X",
                        "001": "FACTURA A",
                        "006": "FACTURA B",
                        "011": "FACTURA C",
                    },
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.comprobantes[data];
                    }
                },
                {
                    "className": "row-control",
                    "data": "connection_locker",
                    "type": "options",
                    "options": {  
                        "1":"Bloqueados",
                        "0":"Habilitados",
                    },
                },
                {
                    "className": "row-control",
                    "data": "area_id",
                    "type": "options",
                    "options": areas,
                    "render": function ( data, type, row ) {
                        return data ? areas[data] : '';
                    }
                },
                {
                    "className": "row-control",
                    "data": "customers_has_discount",
                    "render": function ( data, type, row ) {
                        return data ? data.description : '';
                    }
                }
            ],
		    "columnDefs": [
                {
                    "type": "date-custom",
                    "targets": [7]
                },
                {
                    "type": "numeric-comma",
                    "targets": [8]
                },
                {
                    "visible": false,
                    "targets": [1, 2, 3, 11]
                },
                {
                    "width": "10%",
                    "targets": [10, 8, 7]
                },
                {
                    "width": "12%",
                    "targets": [5]
                },
                {
                    "width": "29%",
                    "targets": [4]
                },
                {
                    "width": "29%",
                    "targets": [6]
                },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
                $.each(ids_selected, function( index, value ) {
                    if (row.id == index) {
                        $(row).addClass('selected');
                        $(row).find('input:checkbox').attr('checked', 'checked')
                    }
                });

                if (!data.force_debt) {
                    $(row).addClass('no_force_debt');
                } else {
                    if (data.connection_locker) {
                         $(row).addClass('locked');
                    }
                }

                if (data.customers_has_discount) {
                    var row = table_debts.row($(row));
                    row.child( format( data ) ).show();
                }
            },
		    "language": dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
        	"dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
		});

		function format ( d ) {

		    html = '<div class="row row p-0 mt-0 mr-0 mb-0 ml-1">';
                html += '<div class="col-xl-10 text-left " style="">';

                    html += '<span class="glyphicon icon-level-down"  aria-hidden="true"></span> &nbsp;&nbsp;';
                    html += d.customers_has_discount.description;
                    html += '&nbsp;&nbsp;&nbsp;&nbsp;';
                    html += number_format(-d.customers_has_discount.total, true);

                html += '</div>';
            html += '</div>';

            return html ;
        }

		$('#table-debts').on( 'init.dt', function () {
            createModalSearchColumn(table_debts, '.modal-search-columns-debts');
            loadSearchPreferences('debts-generate-all-search');
        });

        $("#table-debts tbody").on( "click", ".checkbox-select", function() {

            var data = table_debts.row( $(this).closest('tr') ).data();

            if ($(this).is(':checked')) {
                $(this).closest('tr').addClass('selected');

                ids_selected[data.id] = data;

            } else {
                $(this).closest('tr').removeClass('selected');
                delete ids_selected[data.id];
            }
            genrateTotales();
        });

        $("#table-debts tbody").on("click", ".row-control", function() {

            if (!$(this).find('.dataTables_empty').length) {

                debt_selected = table_debts.row( this ).data();

                $('#btn-force_debt').hide();
                $('#btn-no-force_debt').hide();

                if (debt_selected.force_debt) {
                    $('#btn-no-force_debt').show();
                } else {
                    $('#btn-force_debt').show();
                }

                $('.modal-actions-debts').modal('show');
            }
        });

		$('#table-debts_wrapper .tools').append($('#btns-tools').contents());

        $('#btns-tools').show();

        $('.btn-selected-all').click(function() {

            $('#table-debts tr').addClass('selected');
            $('#table-debts .checkbox-select').prop('checked', true);

            table_debts.rows().eq(0).each( function ( idx ) {
                var row = table_debts.row( idx );
                if ($(row.node()).hasClass('selected')) {

                    let data = table_debts.row( idx ).data();
                    if (!array_key_exists(data.id, ids_selected)) {
                        ids_selected[data.id] = data;
                    }
                }
            });
            genrateTotales();
        });

        $('.btn-clear-selected-all').click(function() {

            if (Object.keys(ids_selected).length > 0) {
                bootbox.confirm('Se borrará la selección realizada, ¿desea continuar?', function(result) {

                    if (result) {

                        $('#table-debts tr').removeClass('selected');
                        $('#table-debts .checkbox-select').prop('checked', false);
                        ids_selected = {};
                        genrateTotales();
                    }
                });
            }
        });

        $('#btn-force_debt').click(function() {

            var data = {
                id: debt_selected.connection_id,
                force_debt: true
            }

            $.ajax({
                url: "<?= $this->Url->build(['controller' => 'Connections', 'action' => 'editConnectionFromDebt']) ?>",
                type: 'POST',
                dataType: "json",
                data: JSON.stringify(data),
                success: function(data) {

                    if (data.connection) {
                        if (table_debts) {
                            table_debts.ajax.reload();
                            var debt = ids_selected[debt_selected.id];
                            debt.force_debt = true;
                            ids_selected[debt_selected.id] = debt;
                            genrateTotales();
                        }
                    }

                    generateNoty('success', 'Edición completa.');
                },
                error: function (jqXHR) {
                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	closeModalPreloader();
                        generateNoty('error', 'Error al realizar la edición.');
                    }
                }
            });
            $('.modal-actions-debts').modal('hide');
        });

        $('#btn-no-force_debt').click(function() {

           var data = {
                id: debt_selected.connection_id,
                force_debt: false
            };

            $.ajax({
                url: "<?= $this->Url->build(['controller' => 'Connections', 'action' => 'editConnectionFromDebt']) ?>",
                type: 'POST',
                dataType: "json",
                data: JSON.stringify(data),
                success: function(data) {

                    if (data.connection) {
                        if (table_debts) {
                            table_debts.ajax.reload();
                            var debt = ids_selected[debt_selected.id];
                            debt.force_debt = false;
                            ids_selected[debt_selected.id] = debt;
                            genrateTotales();
                        }
                    }

                    generateNoty('success', 'Edición completa.');
                },
                error: function (jqXHR) {

                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                        closeModalPreloader();
                        generateNoty('error', 'Error al realizar la edición.');
                    }
                }
            });

            $('.modal-actions-debts').modal('hide');
        });

        $('#btn-go-customer').click(function() {
            var action = '/ispbrain/customers/view/' + debt_selected.customer.code;
            window.open(action, '_blank');
        });

        $('#btn-go-connection').click(function() {
            var action = '/ispbrain/connections/view/' + debt_selected.connection_id;
            window.open(action, '_blank');
        });

        $('.btn-update-table').click(function() {
             if (table_debts) {
                table_debts.ajax.reload();
            }
        });

        $(".btn-export").click(function() {

            bootbox.confirm('Se descargara un archivo Excel', function(result) {
                if (result) {
                    $('#table-debts').tableExport({tableName: 'Deudas (Vista Previa)', type:'excel', escape:'false', columnNumber: [7]});
                }
            }).find("div.modal-content").addClass("confirm-width-md");
        });

		$('#table-debts').on( 'draw.dt', function () {
		   closeModalPreloader();
        } );

        // Jquery draggable modals
        $('.modal-dialog').draggable({
            handle: ".modal-header"
        });
    });

</script>
