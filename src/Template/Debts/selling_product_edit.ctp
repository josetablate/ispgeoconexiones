<div class="row">

    <div class="col-sm-12 col-md-6 col-lg-5 col-xl-4">

        <?php if ($presale): ?>
            <?= $this->element('customer_info', ['presale' => $presale]) ?>
        <?php else: ?>
            <?=$this->element('customer_seeker', ['showbtn' => false, 'listCustomers' => true])?>
        <?php endif; ?>

    </div>

    <div class="col-sm-12 col-md-6 col-lg-7 col-xl-8">

        <div class="card border-secondary mb-3" >
            <h5 class="card-header"> 2. Vender Producto </h5>
            <div class="card-body text-secondary pb-0">

                <?= $this->Form->create($debt_form,  ['id' => 'form-sell-product', 'class' => 'mb-0']) ?>

                <fieldset>

                <div class="row">
                    <div class="col-xl-12">
                         <div class="form-group text required">
                            <label class="control-label" for="code">Producto <span class='fa fa-question-circle' title='Ingrese el código o nombre del producto. &nbsp;&nbsp;Con * desplega la lista completa.'  aria-hidden='true' ></span></label>
                            <input type="text" required="required" maxlength="45" id="autocompleteProducts" disabled placeholder="Cargando Productos..." class="form-control" value="<?= $presale->products[$id]->name ?>">
                        </div>
                    </div>
                </div>

                <?= $this->Form->input('user_id', ['type' => 'hidden', 'value' => $user_id]); ?>

                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-2 col-xl-2">
                       <?= $this->Form->input('amount', ['label' => 'Cantidad', 'type' => 'number', 'min' => 1, 'value' => $presale->products[$id]->amount]); ?>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-3 col-xl-2">
                        <label class="control-label" for="code">Precio</label>
                        
                        <input type="text" name="product_total" class="form-control" id="product-total" readonly value="<?= $presale->products[$id]->product_total ?>"/>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                        <div class="form-group date required">
                        	<label  class="control-label">Cuotas</label> 
                        	<select name="dues" id="dues" class="form-control">
                        	    <?php foreach($dues_interest as $num => $di): ?>
                        	    <option <?= ($presale->products[$id]->dues == ($num + 1)) ? 'selected' : '' ?> data-interest="<?php echo  $di; ?>" value="<?php echo $num + 1; ?>"><?php echo $num + 1 . ' cuotas (' . $di * 100  . '%)'; ?></option>
                        	    <?php endforeach; ?>
                        	</select>
                    	</div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-2">
                        <label class="control-label" for="code">Cuota</label>
                        <input type="text" name="product_total_due" class="form-control" id="product-total-due" readonly value="<?= $presale->products[$id]->product_total_due ?>"/>
                    </div>
                    
                     <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                        <label  class="control-label">Vencimiento</label> 
                        <div class='input-group date' id='duedate-datetimepicker'>
                            <input name='duedate' id="duedate" type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar">
                                <span class="glyphicon icon-calendar"></span>
                            </span>
                        </div>
                    </div>

                    <div class="col-12">
                         <div class="form-group text">
                            <?= $this->Form->input('discount', [
                                'label'    => 'Descuento (opcional)', 
                                'type'     => 'select', 
                                'empty'    => 'Seleccionar descuento', 
                                'options'  => $discounts_list,
                                'value'    => $presale->products[$id]->discount_id
                            ]); ?>
                        </div>
                    </div>

                    <?php if ($presale->customer->billing_for_service): ?>
                        <?php if ($connections): ?>
                            <div class="col-12">
                                 <div class="form-group text">
                                    <?= $this->Form->input('connection_id', [
                                        'label'    => 'Pagar con el Servicio', 
                                        'type'     => 'select', 
                                        'empty'    => 'Seleccionar Servicio', 
                                        'options'  => $connections,
                                        'value'    => $presale->products[$id]->connection_id
                                    ]); ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>

                </fieldset>

                <?= $this->Form->end() ?> 

            </div>

            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <button type="button" class="btn btn-success float-right" id="btn-sell"><?= $presale ? 'Guardar' : 'Vender' ?></button>
                    <?php if ($presale): ?>
                        <?= $this->Html->link(__('Cancelar'), ["controller" => "presales", "action" => "add"], ['title' => 'Ir a la venta', 'class' => 'btn btn-default ml-2']) ?>
                    <?php endif; ?>
                </li>
            </ul>

        </div>
    </div>

</div>

<script type="text/javascript">

    $('#duedate-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: <?= json_encode($debt_form->duedate) ?>,
        format: 'DD/MM/YYYY',
        icons: {
            time: "glyphicon icon-alarm",
            date: "glyphicon icon-calendar",
            up: "glyphicon icon-arrow-right",
            down: "glyphicon icon-arrow-left"
        }
    });

    var product_select = null; 
    var amount = 1;
    var interest = 1;
    var dues = 1;
    var presale = null;
    var discount_select = null;
    var discounts = null;
    var id = null;

    $(document).ready(function() {

        id = <?= json_encode($id) ?>;
        presale = <?= json_encode($presale) ?>;
        if (presale) {
            customer_selected = presale.customer;
        }
        product_select = presale.products[id];

        discounts = <?= json_encode($discounts) ?>;
        $.each(discounts, function(i, val) {
            if (val.id == product_select.discount_id) {
                discount_select = val;
                return false;
            }
        });

        $('#card-customer-seeker').on('CUSTOMER_SELECTED', function() {
            $('#autocompleteProducts').focus();
        });

        $('#btn-sell').click(function() {
           $('#form-sell-product').submit();
        });

        $('#form-sell-product').submit(function(e) {

            e.preventDefault();

            if (presale) {
                if (!customer_selected) {
                    generateNoty('warning', 'Debe especificar un cliente.');
                } else if (!product_select) {
                    generateNoty('warning', 'Debe especificar un Producto.');
                } else {
                    bootbox.confirm('Confirmar', function(result) {
                        if (result) {

                            var send_data = {};
                            send_data.from = "presale";
                            send_data.product_id = product_select.id;
                            send_data.product_article_store_id = product_select.product_article_store_id;

                            send_data.discount_id = null;
                            send_data.discount_name = null;
                            send_data.discount_total = null;

                            if (discount_select) {
                                send_data.discount_id = discount_select.id;
                                send_data.discount_name = discount_select.concept;
                                send_data.discount_total = discount_select.value;
                            }

                            var form_data = $('#form-sell-product').serializeArray();
                            $.each(form_data, function(i, val){
                                send_data[val.name] = val.value;
                            });

                            $('body')
                                .append( $('<form/>').attr({'action': '/ispbrain/debts/selling_product_edit/' + id, 'method': 'post', 'id': 'form-block'})
                                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'data', 'value': JSON.stringify(send_data)}))
                                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                                .find('#form-block').submit();
    
                            $('#btn-sell').hide(); 
                        }
                    });
                }
            } else {
                if (!customer_selected) {
                    generateNoty('warning', 'Debe especificar un cliente.');
                } else if (!product_select) {
                    generateNoty('warning', 'Debe especificar un Producto.');
                } else {

                    bootbox.confirm('Confirmar', function(result) {
                        if (result) {

                            var send_data = {};
                            send_data.from = "";
                            send_data.customer_code = customer_selected.code;
                            send_data.customer_account_code = customer_selected.account_code;

                            send_data.product_id = product_select.id;
                            send_data.product_article_store_id = product_select.product_article_store_id;

                            var form_data = $('#form-sell-product').serializeArray();
                            $.each(form_data, function(i, val){
                                send_data[val.name] = val.value;
                            });

                            $('body')
                                .append( $('<form/>').attr({'action': '/ispbrain/debts/selling_product/', 'method': 'post', 'id': 'form-block'})
                                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'data', 'value': JSON.stringify(send_data)}))
                                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                                .find('#form-block').submit();

                            $('#btn-sell').hide(); 
                        }
                    });
                }
            }

        });

        $("select#dues").change(function () {

            amount = $('input#amount').val();

            if (product_select != null && amount.length > 0 &&  amount > 0) {

                $( "select#dues option:selected" ).each(function() {

                    interest = $(this).data('interest');

                    dues = $(this).val();

                    var product_price = product_select.unit_price

                    if (discount_select) {
                        product_price = product_price * (1 - discount_select.value);
                    }

                    var sum_price = product_price * amount;
                    var interest_ = parseFloat(interest) + 1;
                    sum_price = sum_price * interest_;
                    var total = sum_price.toFixed(2);
                    $('input#product-total').val(total);
                    var total_due = total / dues;
                    total_due = total_due.toFixed(2);
                    $('input#product-total-due').val(total_due);

                });
            }
        });

        $('input#amount').change(function() {

            amount = $(this).val();

            if (product_select != null && amount.length > 0 &&  amount > 0) {

                $( "select#dues option:selected" ).each(function() {

                    interest = $(this).data('interest');

                    dues = $(this).val();

                    var product_price = product_select.unit_price

                    if (discount_select) {
                        product_price = product_price * (1 - discount_select.value);
                    }

                    var sum_price = product_price * amount;
                    var interest_ = parseFloat(interest) + 1;
                    sum_price = sum_price * interest_;
                    var total = sum_price.toFixed(2);
                    $('input#product-total').val(total);
                    var total_due = total / dues;
                    total_due = total_due.toFixed(2);
                    $('input#product-total-due').val(total_due);
                });
            }
        });

        $('#discount').change(function() {

            amount = $('input#amount').val();

            if (product_select != null && amount.length > 0 &&  amount > 0) {
                var value = $(this).val();
                var flag = false;
                $.each(discounts, function( index, discount ) {
                    if (value == discount.id) {
                        flag = true;
                        discount_select = discount;
                    }
                });
                if (!flag) {
                    discount_select = null;
                }

                var due_selected = $('select#dues').find(":selected");

                interest = due_selected.data('interest');

                dues = due_selected.val();

                var product_price = product_select.unit_price

                if (discount_select) {
                    product_price = product_price * (1 - discount_select.value);
                }

                var sum_price = product_price * amount;
                var interest_ = parseFloat(interest) + 1;
                sum_price = sum_price * interest_;

                var total = sum_price.toFixed(2);

                $('input#product-total').val(total);
                var total_due = total / dues;
                total_due = total_due.toFixed(2);

                $('input#product-total-due').val(total_due);
            } else {
                $("#discount").val("");
                generateNoty('warning', 'Debe seleccionar primero un producto para poder aplicar el descuento');
            }
        });

        //$( "#autocompleteProducts" ).val('');

        var user_id = $('#user-id').val();
        var products = [];

        $.ajax({
            url: "<?= $this->Url->build(['controller' => 'Products', 'action' => 'getAllByUserAjax']) ?>",
            type: 'POST',
            dataType: "json",
            data: JSON.stringify({ user_id: user_id}),
            success: function(data) {

                if (data.products != false) {

                    $.each(data.products, function(i, val) {

                        var mylabel = val.article.code + ' - ' + val.name  + ' ($' + val.unit_price + ')' + ' *';

                        var ui = {
                            store_id: data.store_id,
                            article_id: val.article.id,
                            id: val.id,
                            label:mylabel,
                            value:val.name,
                            unit_price:val.unit_price,
                            aliquot:val.aliquot,
                            product_article_store_id: val.product_article_store_id,
                            amount: val.amount
                        };

                        products.push(ui);
                    });

                    //carga el campo de autocompletado con los productos obtenidoss
                    $( "#autocompleteProducts" ).autocomplete({
                    	source: products,
                    	create: function( event, ui ) {
                           $( "#autocompleteProducts" ).attr('disabled', false);
                           $( "#autocompleteProducts" ).attr('placeholder', 'Seleccione un producto.');
                        },
                        select: function( event, ui ) {

                            product_select = ui.item;

                            amount = $('input#amount').val();

                            if (product_select != null && amount.length > 0 &&  amount > 0) {

                                $( "select#dues option:selected" ).each(function() {

                                    interest = $(this).data('interest');

                                    dues = $(this).val();

                                    var sum_price = product_select.unit_price * amount;

                                    var interest_ = parseFloat(interest) + 1;
                                    sum_price = sum_price * interest_;

                                    var total = sum_price.toFixed(2);
                                    $('input#product-total').val(total);
                                    var total_due = total / dues;
                                    total_due = total_due.toFixed(2);
                                    $('input#product-total-due').val(total_due);
                                });

                                $('#amount').attr('max', product_select.amount);

                                $('#concept').html(product_select.value);
                            }
                        }
                    }); 
                } else { //no hay productos

                    $( "#autocompleteProducts" ).addClass('placeholder-error');
                    $( "#autocompleteProducts" ).attr('placeholder', 'No existen Productos.');
                }
            },
            error: function (jqXHR) {
                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() { 
                        window.location.href = "/ispbrain";
                	}, 3000);

                } else {
                	generateNoty('error', 'Error al obtener los Productos.');
                }
            }
        });
    });

</script>
