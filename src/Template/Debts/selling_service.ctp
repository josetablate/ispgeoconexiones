<style type="text/css">

    legend {
        display: block;
        width: 100%;
        padding: 0;
        margin-bottom: 20px;
        font-size: 21px;
        line-height: inherit;
        color: #333;
        border: 0;
        border-bottom: 1px solid #e5e5e5;
    }

    #map {
		height: 350px;
	}

	#floating-panel {
		top: 10px;
		left: 25%;
		z-index: 5;
		background-color: #fff;
		padding: 5px;
		border: 1px solid #999;
		text-align: center;
		font-family: 'Roboto','sans-serif';
		line-height: 30px;
		padding-left: 10px;
	}

	#submit-map {
        padding: 10px;
        top: 2px;
    }

    #update-map {
        padding: 10px;
        top: 2px;
    }

    #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
    }

    #infowindow-content .title {
        font-weight: bold;
    }

    #infowindow-content {
        display: none;
    }

    #map #infowindow-content {
        display: inline;
    }

    .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
    }

    #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
    }

    .pac-controls {
        display: inline-block;
        padding: 5px 11px;
    }

    .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
    }

    #pac-input:focus {
        border-color: #4d90fe;
    }

    #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
    }

    #target {
        width: 345px;
    }

</style>

<div class="row">

    <div class="col-sm-12 col-md-6 col-lg-5 col-xl-4">

        <?php if ($presale): ?>
            <?= $this->element('customer_info', ['presale' => $presale, 'paymentMethods' => $paymentMethods ]) ?>
        <?php else: ?>
            <?=$this->element('customer_seeker', ['showbtn' => false, 'listCustomers' => true])?>
        <?php endif; ?>

    </div>

    <div class="col-sm-12 col-md-6 col-lg-7 col-xl-8">

        <div class="card border-secondary mb-3" >
            <h5 class="card-header"> 2. Vender Servicio </h5>
            <div class="card-body text-secondary pb-0">

                <?= $this->Form->create($debt_form,  ['id' => 'form-sell-service', 'class' => 'mb-0']) ?>

                <fieldset>

                <?= $this->Form->input('user_id', ['type' => 'hidden', 'value' => $user_id]); ?>

                <div class="row">
                    
                    <div class="col-sm-12 col-md-6 col-lg-8 col-xl-8">
                         <div class="form-group text required">
                            <?= $this->Form->input('service', [
                                'label'    => 'Servicios', 
                                'type'     => 'select', 
                                'empty'    => 'Seleccionar servicio', 
                                'options'  => $services_list, 
                                'required' => true
                            ]); ?>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
                        <label class="control-label" for="code">Precio ($)</label>
                        <input type="text" name="service_total" class="form-control" id="service-total" readonly/>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-8 col-xl-8">
                         <div class="form-group text">
                            <?= $this->Form->input('discount', [
                                'label'    => 'Descuento (opcional)', 
                                'type'     => 'select', 
                                'empty'    => 'Seleccionar descuento', 
                                'options'  => $discounts_list
                            ]); ?>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
                        <label class="control-label" for="code">Total ($)</label>
                        <input type="text" name="service_discount_total" class="form-control" id="service-discount-total" readonly/>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                        <label  class="control-label">Instalación</label> 
                        <div class='input-group date' id='installation-date-datetimepicker'>
                            <input name='installation_date' id="installation-date" type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar">
                                <span class="glyphicon icon-calendar"></span>
                            </span>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                        <div class="form-group select">
                            <label class="control-label" for="range-hours-installation">Disponibilidad</label>
                            <select name="range_hours_installation" id="range-hours-installation" class="form-control">
                                <?php foreach ($range_hours_installation as $item): ?>
                                    <option value="<?= $item ?>"><?= $item ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-6">
                        <label class="control-label" for="address">Domicilio</label>
                        <input type="text" name="address" class="form-control" id="address" required />
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                        <?php echo $this->Form->input('generate_ticket', ['type' => 'checkbox', 'label' => 'Generar Ticket', 'required' => false, 'checked' => TRUE]); ?>
                    </div>

    		        <div class="col-xl-12 my-hidden" id="payment_method_selected">
                    </div>

                    <div class="col-xl-12">

                        <?php
                            echo $this->Form->input('lat', ['type' => 'hidden', 'id' => 'coodmapslat']);
                            echo $this->Form->input('lng', ['type' => 'hidden', 'id' => 'coodmapslng']);
                        ?>

                        <legend class="sub-title-sm"><?=  __('Ubicación') ?></legend>
                    	<div id="floating-panel">
                		    <div class="row">
                		        <div class="col-md-12">
                    		        <div class="input-group">
                    		            <span class="input-group-btn">
                                            <button id="update-map" title="Recargar Mapa" class="btn btn-info" type="button"><span class="text-white glyphicon icon-map2" aria-hidden="true"></span></button>
                                        </span>
                                        <input id="addressmap" type="text" class="form-control margin-top-5" value="" placeholder="Calle #, Ciudad, Provincia">
                                        <span class="input-group-btn">
                                            <button id="submit-map" title="Buscar Dirección" class="btn btn-success" type="button"><span class="text-white glyphicon icon-search" aria-hidden="true"></span></button>
                                        </span>
                                    </div>
                		        </div>
                		    </div>
                		</div>

                		<div id="map" class="mb-3"></div>
                    </div>

                </div>

                </fieldset>

                <?= $this->Form->end() ?> 

            </div>

            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <button type="button" class="btn btn-success float-right" id="btn-sell"><?= $presale ? 'Cargar' : 'Vender' ?></button>
                    <button type="button" class="btn btn-primary float-right mr-3" id="btn-sell-exit"><?= $presale ? 'Cargar y Salir' : 'Vender y Salir' ?></button>
                    <?php if ($presale): ?>
                        <?= $this->Html->link(__('Cancelar'),["controller" => "presales", "action" => "add"], ['title' => 'Ir a la venta', 'class' => 'btn btn-default ml-2']) ?>
                    <?php endif; ?>
                </li>
            </ul>

        </div>
    </div>

</div>

<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2K3zoK46JybWPzQbhfQQqIIbdZ_3WcQs&libraries=places">
</script>

<script type="text/javascript">

    $('#update-map').click(function() {
        initMap();
    });

    $('form input').on('keypress', function(e) {
        return e.which !== 13;
    });

    var map = null;
    var geocoder = null;
    var marker = null;
    var paraments = <?= json_encode($paraments) ?>;
    var banks = null;
    var redirect = "";

    function isEmpty(val) {
        return (val === undefined || val == null || val == 0 || val.length <= 0) ? true : false;
    }

	function initMap() {

		geocoder = new google.maps.Geocoder;

		var myLatlng = {lat: paraments.system.map.lat, lng: paraments.system.map.lng};

		var mapOptions = {
			zoom: 14,
			center: myLatlng,
			mapTypeId: 'hybrid'   // roadmap, satellite, hybrid, terrain 
		};

		map = new google.maps.Map(document.getElementById('map'), mapOptions);

		document.getElementById('submit-map').addEventListener('click', function() {
			geocodeAddress(geocoder, map);
		});

	    var init = myLatlng;

        var haveMarker = false;

        if (service_select) {
            if (!isEmpty(service_select.lat) || !isEmpty(service_select.lng)) {
                haveMarker = true;
                init.lat = service_select.lat;
                init.lng = service_select.lng;
            }
        }

        var latLng = new google.maps.LatLng(init.lat, init.lng);
        if (haveMarker) {
           addMarker(latLng);
        }

        map.setCenter(latLng);

		map.addListener('click', function(event) {
			addMarker(event.latLng);	    
		});
    }

    function refreshMap() {
        google.maps.event.trigger(map, 'resize');
    }

    $('input#addressmap').keypress(function(event) {
        if ( event.which == 13 ) {
            geocodeAddress(geocoder, map);
        }
    });

	function addMarker(location) {

		if (marker != null) {
			clearMarkers();
		}

        if (service_select) {
            marker = new google.maps.Marker({
    			position: location,
    			animation: google.maps.Animation.DROP,
    			map: map
    		});

            var lat = location.lat();
            var lng = location.lng();
            marker.addListener('click', toggleBounce);
		    marker.setMap(map);
            service_select.lat;
            service_select.lng;
            $('#coodmapslat').val(lat);
		    $('#coodmapslng').val(lng);
        } else {
            generateNoty("warning", "Debe seleccionar un servicio primero.");
        }
	}

	function toggleBounce() {
        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }

	function clearMarkers() {
		marker.setMap(null);
	}			

	function geocodeAddress(geocoder, map) {

		var address = document.getElementById('addressmap').value;

		geocoder.geocode({
			'address': address
			}, function(results, status) {

				if (status === google.maps.GeocoderStatus.OK) {	
					addMarker(results[0].geometry.location)  	    
					map.setCenter(results[0].geometry.location);
					map.setZoom(14);

    			} else {
    				if (status == google.maps.GeocoderStatus.ZERO_RESULTS) {
    			        bootbox.alert('No se encontraron Resultados');
    			    } else {
    			        window.alert('Geocode was not successful for the following reason: ' + status);
    			    }
    			}
		});
	}

	function freeUsedCard() {
	    $.ajax({
            url: "<?= $this->Url->build(['controller' => 'CobroDigital', 'action' => 'freeUsedCard']) ?>",
            type: 'POST',
            dataType: "json",
            data: JSON.stringify({ card_id: cobrodigital_card_selectedx.id }),
            success: function(data) {

                if (data.data.status == 200) {
                    cobrodigital_card_selectedx = null;

                } else {

                }
            },
            error: function (jqXHR) {
                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() { 
                        window.location.href = "/ispbrain";
                	}, 3000);
                
                } else {
                	generateNoty('error', 'Error al intentar liberar la tarjeta.');
                }
            }
        });
	}

    $('#installation-date-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'DD/MM/YYYY',
        icons: {
            time: "glyphicon icon-alarm",
            date: "glyphicon icon-calendar",
            up:   "glyphicon icon-arrow-right",
            down: "glyphicon icon-arrow-left"
        }
    });

    var service_select = null;
    var discount_select = null;
    var services = null
    var discounts = null;
    var interest = 1;
    var dues = 1;
    var presale = null;
    var customer_selected = null;

    $(document).ready(function() {

        redirect = "";

        presale = <?= json_encode($presale) ?>;
        services = <?= json_encode($services) ?>;
        discounts = <?= json_encode($discounts) ?>;

        if (presale) {
            customer_selected = presale.customer;
        }

        banks = <?= json_encode($banks) ?>;
        initMap();

        $('#card-customer-seeker').on('CUSTOMER_SELECTED', function() {
            $('#autocompleteProducts').focus();
            $('input#address').val(customer_selected.address);
        });

        $('#btn-sell').click(function() {
            redirect = "";
            $('#form-sell-service').submit();
        });

        $('#btn-sell-exit').click(function() {
            redirect = "exit";
            $('#form-sell-service').submit();
        });

        $('#form-sell-service').submit(function(e) {

            e.preventDefault();

            if (presale) {

                if (!customer_selected) {

                    generateNoty('warning', 'Debe especificar un Cliente.');
                } else if (!service_select) {

                    generateNoty('warning', 'Debe especificar un Servicio.');
                } else if ($('#form-sell-service #address').val() == "") {

                    generateNoty('warning', 'Debe ingresar un domicilio.');
                } else {
                    bootbox.confirm('Confirmar', function(result) {

                        if (result) {

                            var send_data = {};
                            send_data.from = "presale";
                            send_data.service_id = service_select.id;
                            send_data.service_name = service_select.name;
                            send_data.service_total = service_select.price;
                            send_data.address = service_select.address;
                            send_data.redirect = redirect;

                            send_data.discount_id = null;
                            send_data.discount_name = null;
                            send_data.discount_total = null;

                            if (discount_select) {
                                send_data.discount_id = discount_select.id;
                                send_data.discount_name = discount_select.concept;
                                send_data.discount_total = discount_select.value;
                            }

                            var form_data = $('#form-sell-service').serializeArray();
                            $.each (form_data, function(i, val) {
                                send_data[val.name] = val.value;
                            });

                            $('body')
                                .append( $('<form/>').attr({'action': '/ispbrain/debts/selling_service/', 'method': 'post', 'id': 'form-block'})
                                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'data', 'value': JSON.stringify(send_data)}))
                                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                                .find('#form-block').submit();

                            $('#btn-sell').hide();
                            $('#btn-sell-exit').hide();
                        }
                    });
                }
            } else {

                if (!customer_selected) {
                    generateNoty('warning', 'Debe especificar un Cliente.');
                } else if (!service_select) {
                    generateNoty('warning', 'Debe especificar un Servicio.');
                } else if ($('#form-sell-service #address').val() == "") {
                    generateNoty('warning', 'Debe ingresar un domicilio.');
                } else {

                    bootbox.confirm('Confirmar', function(result) {
                        if (result) {

                            var send_data = {};
                            send_data.from = "";
                            send_data.customer_code = customer_selected.code;
                            send_data.customer_account_code = customer_selected.account_code;
                            send_data.service_id = service_select.id;

                            var form_data = $('#form-sell-product').serializeArray();
                            $.each(form_data, function(i, val) {
                                send_data[val.name] = val.value;
                            });

                            $('body')
                                .append( $('<form/>').attr({'action': '/ispbrain/debts/selling_service/', 'method': 'post', 'id': 'form-block'})
                                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'data', 'value': JSON.stringify(send_data)}))
                                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                                .find('#form-block').submit();

                            $('#btn-sell').hide(); 
                        }
                    });
                }
            }

        });

        $('#service').change(function() {

            var value = $(this).val();
            var flag = false;

            $.each(services, function( index, service ) {

                if (value == service.id) {

                    flag = true;
                    service_select = service;

                    if (customer_selected) {

                        var latLng = new google.maps.LatLng(customer_selected.lat, customer_selected.lng);
                        addMarker(latLng);
                    }
                }
            });

            if (!flag) {
                service_select = null;
            }

            if (service_select) {

                if (discount_select) {

                    $('input#service-total').val(service_select.price);
                    $('input#service-discount-total').val(service_select.price * (1 - discount_select.value));
                } else {

                    $('input#service-total').val(service_select.price);
                    $('input#service-discount-total').val(service_select.price);
                }

            } else {

                $("#discount").val("");
                $('input#service-discount-total').val("");
                $('input#service-total').val("");
                discount_select = null;
            }
        });

        $('#discount').change(function() {

            if (service_select) {

                var value = $(this).val();
                var flag = false;

                $.each(discounts, function( index, discount ) {

                    if (value == discount.id) {

                        flag = true;
                        discount_select = discount;
                    }
                });

                if (!flag) {
                    discount_select = null;
                }

                if (discount_select) {

                    $('input#service-discount-total').val(service_select.price * (1 - discount_select.value));
                } else {

                    $('input#service-discount-total').val(service_select.price);
                }

            } else {

                $("#discount").val("");
                generateNoty('warning', 'Debe seleccionar primero un servicio para poder aplicar el descuento');
            }
        });
    });

</script>

<script type='text/javascript'>

    function initialize() {

        var input = document.getElementById('addressmap');

        var autocomplete = new google.maps.places.Autocomplete(input);
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>
