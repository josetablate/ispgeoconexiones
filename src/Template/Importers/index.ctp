<style type="text/css">

    .nav-link {
        cursor: pointer;
    }

    .logo {
      color: #FF5722;
      margin-top: 50px;
      margin-bottom: 50px;
    }

    .panel-customers-import {
      background-color: #8BC34A !important;
      color: white !important;
      padding: 15px;
    }

    .panel-connections-import {
      background-color: #2196F3 !important;
      color: white !important;
      padding: 15px;
    }

    .panel-debts-import {
      background-color: #E91E63 !important;
      color: white !important;
      padding: 15px;
    }

    /* Tabs panel */
    .tabbable-panel {
      padding: 10px;
    }

    /* Default mode */
    .tabbable-line > .nav-tabs {
      border: none;
      margin: 0px;
    }

    .tabbable-line > .nav-tabs > li {
      margin-right: 2px;
      margin-left: 15px;
    }

    .tabbable-line > .nav-tabs > li > a {
      border: 0;
      margin-right: 0;
      color: #737373;
    }

    .tabbable-line > .nav-tabs > li > a > i {
      color: #a6a6a6;
    }

    .tabbable-line > .nav-tabs > li.open, .tabbable-line > .nav-tabs > li:hover {
      border-bottom: 4px solid #F44336;
    }

    .tabbable-line > .nav-tabs > li.open > a, .tabbable-line > .nav-tabs > li:hover > a {
      border: 0;
      background: none !important;
      color: #333333;
    }

    .tabbable-line > .nav-tabs > li.open > a > i, .tabbable-line > .nav-tabs > li:hover > a > i {
      color: #a6a6a6;
    }

    .tabbable-line > .nav-tabs > li.open .dropdown-menu, .tabbable-line > .nav-tabs > li:hover .dropdown-menu {
      margin-top: 0px;
    }

    .tabbable-line > .nav-tabs > li.active {
      border-bottom: 4px solid #f3565d;
      position: relative;
    }

    .tabbable-line > .nav-tabs > li.active > a {
      border: 0;
      color: #333333;
    }

    .tabbable-line > .nav-tabs > li.active > a > i {
      color: #404040;
    }

    .tabbable-line > .tab-content {
      margin-top: -3px;
      background-color: #fff;
      border: 0;
      padding: 15px 0;
    }

    .portlet .tabbable-line > .tab-content {
      padding-bottom: 0;
    }

</style>


<div class="row">

  <div class="col-md-12 text-center logo">
      <i class="fa fa-upload fa-5x" aria-hidden="true"></i>
  </div>

  <div class="tabbable-panel">
		<div class="tabbable-line">
			<ul class="nav nav-tabs ">
				<li class="active">
					<a href="#tab_default_1" data-toggle="tab">
					<?= __('1- Importar Clientes') ?> </a>
				</li>
				<li>
					<a href="#tab_default_2" data-toggle="tab">
					<?= __('2- Importar Conexiones') ?> </a>
				</li>
				<li>
					<a href="#tab_default_3" data-toggle="tab">
					<?= __('3- Importar Deudas') ?> </a>
				</li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="tab_default_1">
			    <?= $this->element('Importers/customers') ?>
				</div>
				<div class="tab-pane" id="tab_default_2">
          <?= $this->element('Importers/connections') ?>
				</div>
				<div class="tab-pane" id="tab_default_3">
			    <?= $this->element('Importers/debts') ?>
				</div>
			</div>
		</div>
	</div>

</div>

<script type="text/javascript">

  $(document).ready(function () {
    $('.nav-tabs a').click(function (e) {
      
    })

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      table_connections.draw();
      table_connections_without.draw();
      table_debts.draw();
      table_debts_without.draw();
      table_customers.draw();
      table_customers_without.draw();
    });
  });

</script>
