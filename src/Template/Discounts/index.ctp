
<div id="btns-tools">
        <div class="text-right btns-tools margin-bottom-5">

        <?php
        
        echo $this->Html->link(
                    '<span class="glyphicon icon-eye" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Ocultar o Mostrar Columnas',
                    'class' => 'btn btn-default btn-hide-column ml-1',
                    'escape' => false
                ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                ['controller' => 'discounts', 'action' => 'add'],
                [
                'title' => 'Agregar nuevo descuento',
                'class' => 'btn btn-default btn-add ml-1',
                'escape' => false
                ]);

             echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar en formato excel el contenido de la tabla',
                'class' => 'btn btn-default btn-export ml-1',
                'escape' => false
                ]);
        ?>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered" id="table-discounts">
            <thead>
                <tr>
                    <th>Fecha</th>          <!--0-->
                    <th>Código</th>         <!--1-->
                    <th>Tipo</th>           <!--2-->
                    <th>Concepto</th>       <!--3-->
                    <th>Valor</th>          <!--4-->
                    <th>Duración</th>       <!--5-->
                        <th>comments</th>       <!--6-->
                        <th>Usuario</th>        <!--7-->
                    <th>Habilitado</th>     <!--8-->
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-edit',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-discounts', 'title' => 'Acciones', 'buttons' => $buttons ]);
    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-discounts']);
?>

<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Editar Descuento</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                

                <form method="post" accept-charset="utf-8" role="form" action="<?=$this->Url->build(["controller" => "Discounts", "action" => "edit"])?>">
                    <div style="display:none;">
                        <input type="hidden" name="_method" value="POST">
                    </div>
                    
                    <div class="row">
                        
                         <div class="col-xl-6">
                            <?php
                                echo $this->Form->input('id', ['label' => 'false', 'type' => 'hidden']);
                                echo $this->Form->input('code', ['label' => 'Código', 'required' => true, 'maxlength' => 45]);
                            ?>
                        </div>
                        
                        <div class="col-xl-6">
                            <?php
                                echo $this->Form->input('concept', ['label' => 'Concepto',  'required' => true]);
                            ?>
                        </div>
                        
                        <div class="col-xl-6">
                            <?php
                                // echo $this->Form->input('alicuot', ['label' => 'Alícuota', 'options' => $alicuotas_types]);
                            ?>
                        </div>
                        
                        <div class="col-xl-6">
                            <?php
                                echo $this->Form->input('value', ['label' => 'Valor', 'required' => true, 'type' => 'number', 'min' => 0.01, 'max' => 1, 'step' => 0.01]);
                            ?>
                        </div>  
                        
                        <div class="col-xl-6">
                            <?php
                            echo $this->Form->input('always', ['label' => 'Siempre', 'class' => 'input-checkbox','type' => 'checkbox']);
                            echo $this->Form->input('duration', ['label' => 'Duración (meses)', 'required' => true, 'type' => 'number',  'min' => 1 ]);
                            ?>
                        </div>
                        
                         <div class="col-xl-6">
                             <?php
                                echo $this->Form->input('type', [
                                    'label' => 'Tipo',
                                    'options' => [
                                        'generic' => 'General',
                                        'service' => 'Servicio',
                                        'package' => 'Paquete',
                                        'product' => 'Producto'
                                        ]
                                    ]);
                            ?>
                        </div>
              
                        
                        <div class="col-xl-6">
                             <?php
                                echo $this->Form->input('enabled', ['label' => 'Habilitado', 'class' => 'input-checkbox', 'type' => 'checkbox']);
                            ?>
                        </div>
                        
                        <div class="col-xl-12">
                             <?php
                                echo $this->Form->input('comments', ['label' => 'Comentarios', 'type'=> 'textarea', 'rows' => 2 , 'required' => false]);
                            ?>
                        </div>
                       
                        <div class="col-xl-12">
                            <input type="hidden" name="id" id="id"/>
                            <?php echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]); ?>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-success float-right" id="btn-confirm-edit">Guardar</button> 
                        </div>
                        
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var table_discounts = null;
    var discount_selected = null;

    var codes = [];

    $(document).ready(function () {
        
        $('.btn-hide-column').click(function(){
           
           $('.modal-hide-columns-discounts').modal('show');
            
        });
        
        var selected = false;
        
        $('#table-discounts').removeClass('display');
        
        $('#btns-tools').hide();
        
        loadPreferences('discounts-index2', [ 6, 7]);

		table_discounts = $('#table-discounts').DataTable({
		    "order": [[ 0, 'desc' ]],
		    "autoWidth": true,
		    "scrollY": '350px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "sWrapper":  "dataTables_wrapper dt-bootstrap4",
            "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/Discounts/index.json",
                "dataSrc": "discounts",
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
            				  window.location.href ="/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "columns": [
                {
                    "data": "created",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/'+ created[1] + '/' + created[0];
                        }
                    }
                },
                { 
                    "data": "code",
                },
                {
                    "data": "type",
                    "render": function ( data, type, row ) {
                        switch(data){
                            case 'generic':
                                return 'General';
                            case 'service':
                                return 'Servicio';
                            case 'package':
                                return 'Paquete';
                            case 'product':
                                return 'Producto';
                        }
                        return '';
                    }
                       
                },
                { 
                    "class": 'left',
                    "data": "concept",
                },
                // {
                //     "data": "alicuot",
                //     "render": function ( data, type, row ) {
                //         return sessionPHP.afip_codes.alicuotas_types[data];
                //     }
                // },
                {
                    "data": "value",
                    "render": function ( data, type, row ) {
                        let v = (data * 100).toString() + '%' ;                       
                        return  parseFloat(v).toFixed(2)
                    }
                },
                { 
                    "data": "duration",
                    "render": function ( data, type, row ) {
                        if(row.always){
                            return '<span class= "text-danger">Siempre</span>';
                        }
                        return data + ' meses';
                    }
                    
                },
                { 
                    "class": 'left',
                    "data": "comments"
                },
                { "data": "user.username"},
                { 
                    "data": "enabled",
                    "render": function ( data, type, row ) {
                        if (data) {
                             return '<i class="fas fa-check-circle green"></i>';
                        }
                         return '<i class="fas fa-times-circle red"></i>';
                    }
                    
                }
            ],
            "columnDefs": [
                { "type": "date-custom", "targets": [0] },
                { "type": "numeric-comma", "targets": 4 },
                { 
                    "visible": false, targets: hide_columns
                },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
		    "language": dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
        	"dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
		});
        
       
		$('#table-discounts_wrapper .tools').append($('#btns-tools').contents());
		
		$('#btns-tools').show();
    });
    
    $('#table-discounts').on( 'init.dt', function () {
          
        createModalHideColumn(table_discounts, '.modal-hide-columns-discounts');
  
    });

    $(".btn-export").click(function(){
        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if (result) {
                $('#table-discounts').tableExport({tableName: 'Descuentos', type:'excel', escape:'false', columnNumber: [5]});
            }
        });
    });

    $('#table-discounts tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_discounts.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            discount_selected = table_discounts.row( this ).data();

            $('.modal-discounts').modal('show');
        }
    });
    
    $('#always').change(function(){
           
       if($(this).is(':checked')){
           
           $('#duration').closest('div').hide();
           
       }else{
           $('#duration').closest('div').show();
       }
   });
  

    $('a#btn-edit').click(function() {

        $('.modal-actions').modal('hide');

        $('#modal-edit #id').val(discount_selected.id);

        $('#modal-edit #code').val(discount_selected.code);
        $('#modal-edit #concept').val(discount_selected.concept);
        $('#modal-edit #alicuot').val(discount_selected.alicuot);
        
        $('#modal-edit #type').val(discount_selected.type);
 
        $('#modal-edit #value').val(discount_selected.value);
        
        // $('#modal-edit #always').val(0);
        // $('#modal-edit #enabled').val(0);
        // $('#modal-edit #always').removeAttr('checked');
        // $('#modal-edit #enabled').removeAttr('checked');
        
        $('#modal-edit #always').attr('checked', discount_selected.always);
        $('#modal-edit #enabled').attr('checked', discount_selected.enabled);
        // $('#modal-edit #always').val(discount_selected.always);
        // $('#modal-edit #enabled').val(discount_selected.enabled);

        $('#modal-edit #duration').val(discount_selected.duration);
        $('#modal-edit #comments').val(discount_selected.comments);
        
        
        $('#always').change();
        
        $('.modal-discounts').modal('hide');
    
        $('#modal-edit').modal('show');

    });

  
	$('a#btn-delete').click(function() {

        var text = "¿Está Seguro que desea eliminar el Descuento?";
        var id  = discount_selected.id;

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                    .append( $('<form/>').attr({'action': '/ispbrain/discounts/delete/', 'method': 'post', 'id': 'replacer'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer').submit();
            }
        });
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
