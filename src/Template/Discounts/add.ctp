
    <div class="row">
        <div class="col-xl-6">
            <?= $this->Form->create($discount,  ['class' => 'form-load']) ?>
                <fieldset>
                    
                    <div class="row">
                        
                        <div class="col-xl-6">
                             <?php
                                echo $this->Form->input('code', ['label' => 'Código', 'required' => true, 'maxlength' => 45]);
                            ?>
                        </div>
                        
                        <div class="col-xl-6">
                             <?php
                                echo $this->Form->input('concept', ['label' => 'Concepto', 'type'=> 'textarea', 'rows' => 2 , 'required' => true]);
                            ?>
                        </div>
                        
                        <div class="col-xl-6">
                             <?php
                                // echo $this->Form->input('alicuot', ['label' => 'Alícuota', 'options' => $alicuotas_types]);
                            ?>
                        </div>
                        
                        <div class="col-xl-6">
                             <?php
                                echo $this->Form->input('value', ['label' => 'Valor', 'required' => true, 'type' => 'number', 'min' => 0.01, 'max' => 1, 'step' => 0.01]);
                            ?>
                        </div>
                        
                        <div class="col-xl-6">
                             <?php
                              echo $this->Form->input('always', ['label' => 'Siempre', 'class' => 'input-checkbox', 'checked' => false, 'type' => 'checkbox']);
                                echo $this->Form->input('duration', ['label' => 'Duración (meses)', 'required' => true, 'type' => 'number',  'min' => 1,  'value' => 1 ]);
                            ?>
                        </div>
                        
                         <div class="col-xl-6">
                             <?php
                                echo $this->Form->input('type', [
                                    'label' => 'Tipo',
                                    'options' => [
                                        'generic' => 'General',
                                        'service' => 'Servicio',
                                        'package' => 'Paquete',
                                        'product' => 'Producto'
                                        ]
                                    ]);
                            ?>
                        </div>
                        
                        <div class="col-xl-6">
                             <?php
                                echo $this->Form->input('enabled', ['label' => 'Habilitado', 'class' => 'input-checkbox', 'checked' => true, 'type' => 'checkbox']);
                            ?>
                        </div>
                  
                        
                        <div class="col-xl-12">
                             <?php
                                echo $this->Form->input('comments', ['label' => 'Comentarios', 'type'=> 'textarea', 'rows' => 2 , 'required' => false]);
                            ?>
                        </div>
                       
                    </div>
                    
                   
                 
                </fieldset>
                <?= $this->Html->link(__('Cancelar'),["controller" => "Discounts", "action" => "index"], ['class' => 'btn btn-default']) ?>
                <?= $this->Form->button(__('Agregar'), ['class' => 'btn-submit btn-success float-right']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
            
            
            
<script type="text/javascript">
    
    
    $(document).ready(function(){
        
       $('#always').change(function(){
           
           if($(this).is(':checked')){
               
               $('#duration').closest('div').hide();
               
           }else{
               $('#duration').closest('div').show();
           }
       });
        
    });
    
    
    
</script>
          