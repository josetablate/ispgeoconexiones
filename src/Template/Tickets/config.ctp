<style type="text/css">

    .modal #color-text {
        height: 35px;
    }

    .modal #color-back {
        height: 35px;
    }

</style>

<div class="row">
    <div class="col-md-6">
        <div id="btns-tools-status">
            <div class="text-right btns-tools margin-bottom-5" >

                <?php
                    echo $this->Html->link(
                        '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Agregar nuevo estado',
                        'class' => 'btn btn-default btn-add-status',
                        'escape' => false
                    ]);

                    echo $this->Html->link(
                        '<span class="glyphicon icon-file-excel"  aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Exportar tabla',
                        'class' => 'btn btn-default btn-export-status',
                        'escape' => false
                    ]);
                ?>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-hover" id="table-status-tickets">
                    <thead>
                        <tr>
                            <th>Orden</th>
                            <th>Nombre</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div id="btns-tools-categories">
            <div class="text-right btns-tools margin-bottom-5" >

                <?php
                    echo $this->Html->link(
                        '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Agregar nueva categoría',
                        'class' => 'btn btn-default btn-add-categories',
                        'escape' => false
                    ]);

                    echo $this->Html->link(
                        '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Exportar tabla',
                        'class' => 'btn btn-default btn-export-categories',
                        'escape' => false
                    ]);
                ?>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-hover" id="table-categories-tickets">
                    <thead>
                        <tr>
                            <th>Orden</th>
                            <th>Nombre</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <?php if ($user_id == 100): ?>
    <div class="col-md-6">
        <?= $this->Form->input('ticket.save_change_start_task', ['label' => 'Guardar Cambio de Inicio de Tarea', 'type' => 'checkbox',  'checked' => $paraments->ticket->save_change_start_task]) ?>
    </div>
    <?php endif; ?>

</div>

<div class="modal fade" id="modal-edit-labels" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Editar Etiqueta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <form method="post" accept-charset="utf-8" role="form" action="<?= $this->Url->build(["controller" => "Labels", "action" => "edit"]) ?>">
                    <div style="display:none;">
                        <input type="hidden" name="_method" value="POST">
                    </div>

                    <div class="row">

                        <div class="col-xl-6">
                             <?php
                                echo $this->Form->input('text', ['label' => 'Texto', 'required' => true]);
                            ?>
                        </div>

                        <div class="col-xl-6">
                            <?php
                                echo $this->Form->control('color_text', ['label' => 'Color texto', 'type' => 'color' ]);
                            ?>
                        </div>

                        <div class="col-xl-6">
                            <?php
                                echo $this->Form->input('entity', ['type' => 'hidden', 'value' => 2]);
                            ?>
                        </div>

                        <div class="col-xl-6">
                            <?php
                                echo $this->Form->control('color_back', ['label' => 'Color fondo', 'type' => 'color']);
                            ?>
                        </div>

                        <div class="col-xl-6">
                            <?php
                                echo $this->Form->control('enabled', ['label' => 'Habilitar/Deshabilitar', 'type' => 'checkbox' ]);
                            ?>
                        </div>

                        <div class="col-xl-12">
                            <input type="hidden" name="id" id="id"/>
                            <?php echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]); ?>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-success float-right" id="btn-confirm-edit">Guardar</button> 
                        </div>

                    </div>

                </form>
           </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-add-labels" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Agregar Etiqueta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <form method="post" accept-charset="utf-8" role="form" action="<?= $this->Url->build(["controller" => "Labels", "action" => "add"]) ?>">
                    <div style="display:none;">
                        <input type="hidden" name="_method" value="POST">
                    </div>

                    <div class="row">

                        <div class="col-xl-6">
                             <?php
                                echo $this->Form->input('text', ['label' => 'Texto', 'required' => true]);
                            ?>
                        </div>

                        <div class="col-xl-6">
                            <?php
                                echo $this->Form->control('color_text', ['label' => 'Color texto', 'type' => 'color', 'value' => '#ffffff' ]);
                            ?>
                        </div>

                        <div class="col-xl-6">
                            <?php
                                echo $this->Form->input('entity', ['type' => 'hidden', 'value' => 2]);
                            ?>
                        </div>

                        <div class="col-xl-6">
                            <?php
                                echo $this->Form->control('color_back', ['label' => 'Color fondo', 'type' => 'color', 'value' => '#2196f3']);
                            ?>
                        </div>

                        <div class="col-xl-6">
                            <?php
                                echo $this->Form->control('enabled', ['label' => 'Habilitar/Deshabilitar', 'checked' => true,  'type' => 'checkbox' ]);
                            ?>
                        </div>
                        
                        <?php echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]); ?>

                        <div class="col-xl-12">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-success float-right" id="btn-confirm-edit">Guardar</button> 
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-add-status" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Agregar Estado</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <form method="post" accept-charset="utf-8" role="form" action="<?= $this->Url->build(["controller" => "Tickets", "action" => "addStatus"]) ?>">
                    <div style="display:none;">
                        <input type="hidden" name="_method" value="POST">
                    </div>

                    <div class="row">

                        <div class="col-xl-6">
                            <?php
                                echo $this->Form->input('name', ['label' => 'Nombre', 'required' => true]);
                            ?>
                        </div>

                        <div class="col-xl-6">
                            <?php
                                echo $this->Form->input('ordering', ['label' => 'Orden', 'type' => 'number', 'min' => "1", 'step' => "1", 'required' => true]);
                            ?>
                        </div>
                        
                        <?php echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]); ?>

                        <div class="col-xl-12">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-success float-right" id="btn-confirm-edit">Guardar</button> 
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-edit-status-ticket" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Editar Estado</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <form method="post" accept-charset="utf-8" role="form" action="<?= $this->Url->build(["controller" => "Tickets", "action" => "editStatus"]) ?>">
                    <div style="display:none;">
                        <input type="hidden" name="_method" value="POST">
                    </div>

                    <div class="row">

                        <div class="col-xl-6">
                             <?php
                                echo $this->Form->input('name', ['label' => 'Nombre', 'required' => true]);
                            ?>
                        </div>

                        <div class="col-xl-6">
                             <?php
                                echo $this->Form->input('ordering', ['label' => 'Orden', 'type' => 'number', 'min' => "1", 'step' => "1", 'required' => true]);
                            ?>
                        </div>

                        <div class="col-xl-12">
                            <input type="hidden" name="id" id="id"/>
                            <?php echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]); ?>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-success float-right" id="btn-confirm-edit">Guardar</button> 
                        </div>

                    </div>

                </form>
           </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-add-categories" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Agregar Categoría</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <form method="post" accept-charset="utf-8" role="form" action="<?= $this->Url->build(["controller" => "Tickets", "action" => "addCategory"]) ?>">
                    <div style="display:none;">
                        <input type="hidden" name="_method" value="POST">
                    </div>

                    <div class="row">

                        <div class="col-xl-6">
                             <?php
                                echo $this->Form->input('name', ['label' => 'Nombre', 'required' => true]);
                            ?>
                        </div>

                        <div class="col-xl-6">
                             <?php
                                echo $this->Form->input('ordering', ['label' => 'Orden', 'type' => 'number', 'min' => "1", 'step' => "1", 'required' => true]);
                            ?>
                        </div>

                        <div class="col-xl-6">
                            <?php
                                echo $this->Form->control('color_text', ['label' => 'Color texto', 'type' => 'color', 'value' => '#ffffff' ]);
                            ?>
                        </div>

                        <div class="col-xl-6">
                            <?php
                                echo $this->Form->control('color_back', ['label' => 'Color fondo', 'type' => 'color', 'value' => '#2196f3']);
                            ?>
                        </div>
                        
                        <?php echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]); ?>

                        <div class="col-xl-12">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-success float-right" id="btn-confirm-edit">Guardar</button> 
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-edit-category-ticket" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Editar Categoría</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <form method="post" accept-charset="utf-8" role="form" action="<?= $this->Url->build(["controller" => "Tickets", "action" => "editCategory"]) ?>">
                    <div style="display:none;">
                        <input type="hidden" name="_method" value="POST">
                    </div>

                    <div class="row">

                        <div class="col-xl-6">
                             <?php
                                echo $this->Form->input('name', ['label' => 'Nombre', 'required' => true]);
                            ?>
                        </div>

                        <div class="col-xl-6">
                             <?php
                                echo $this->Form->input('ordering', ['label' => 'Orden', 'type' => 'number', 'min' => "1", 'step' => "1", 'required' => true]);
                            ?>
                        </div>

                        <div class="col-xl-6">
                            <?php
                                echo $this->Form->control('color_text', ['label' => 'Color texto', 'type' => 'color']);
                            ?>
                        </div>

                        <div class="col-xl-6">
                            <?php
                                echo $this->Form->control('color_back', ['label' => 'Color fondo', 'type' => 'color']);
                            ?>
                        </div>

                        <div class="col-xl-12">
                            <input type="hidden" name="id" id="id"/>
                            <?php echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]); ?>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-success float-right" id="btn-confirm-edit">Guardar</button> 
                        </div>

                    </div>

                </form>
           </div>
        </div>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id' =>  'btn-edit-status-ticket',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete-status-ticket',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-status-tickets', 'title' => 'Acciones', 'buttons' => $buttons ]);

    $buttons = [];

    $buttons[] = [
        'id' =>  'btn-edit-category-ticket',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete-category-ticket',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-categories-tickets', 'title' => 'Acciones', 'buttons' => $buttons ]);

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-edit-labels',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete-labels',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-labels', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<script type="text/javascript">

    var table_status_tickets = null;
    var status_ticket_selected = null;

    var table_categories_tickets = null;
    var category_ticket_selected = null;

    var table_labels = null;
    var label_selected = null;

    var codes = [];
    var selected;

    $(document).ready(function () {

        selected = false;

        $('#table-status-tickets').removeClass('display');

		table_status_tickets = $('#table-status-tickets').DataTable({
		    "order": false,
		    "autoWidth": true,
		    "scrollY": '350px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/tickets/get_status_tickets.json",
                "dataSrc": "status_tickets",
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
            				  window.location.href ="/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "columns": [
                { "data": "ordering" },
                { "data": "name" }
            ],
            "columnDefs": [
                {},
            ],
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            "dom":
    	    	"<'row'<'col-xl-6 title'l><'col-xl-4'f><'col-xl-2 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
		});
		
		$('#table-status-tickets_wrapper .title').append('<h5>Estados</h5>');

        $('#table-status-tickets_wrapper .tools').append($('#btns-tools-status').contents());

        $('#table-categories-tickets').removeClass('display');

		table_categories_tickets = $('#table-categories-tickets').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '350px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/tickets/get_categories_tickets.json",
                "dataSrc": "categories_tickets",
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
            				  window.location.href ="/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "columns": [
                { "data": "ordering" },
                { 
                    "data": "name",
                    "render": function ( data, type, row ) {
                        return "<span class='my_label' style='background-color: " + row.color_back + ";  color: " + row.color_text + ";' > " + data + " </span>";
                    }
                }
            ],
            "columnDefs": [
                {},
            ],
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            "dom":
    	    	"<'row'<'col-xl-6 title'l><'col-xl-4'f><'col-xl-2 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
		});

		$('#table-categories-tickets_wrapper .title').append('<h5>Categorías</h5>');

        $('#table-categories-tickets_wrapper .tools').append($('#btns-tools-categories').contents());

        $('#table-labels').removeClass('display');

        $('#btns-tools').hide();

		table_labels = $('#table-labels').DataTable({
		    "order": [[ 1, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '350px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "sWrapper":  "dataTables_wrapper dt-bootstrap4",
            "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/Labels/tickets.json",
                "dataSrc": "labels",
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
            				  window.location.href ="/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "columns": [
                { "data": "id" },
                { 
                    "data": "text",
                    "render": function ( data, type, row ) {
                        return "<span class='my_label' style='background-color: " + row.color_back + ";  color: " + row.color_text+";' > " + data + " </span>";
                    }
                },
                {
                    "data": "enabled",
                    "render": function ( data, type, row ) {
                        var enabled = '<i class="fa fa-times" aria-hidden="true"></i>';
                        if (data) {
                            enabled = '<i class="fa fa-check" aria-hidden="true"></i>';
                        }
                        return enabled;
                    }
                },
            ],
            "columnDefs": [
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
		    "language": dataTable_lenguage,
            "lengthMenu": [[50, 100, -1], [50, 100, "Todas"]],
        	"dom":
    	    	"<'row'<'col-xl-6 title'l><'col-xl-4'f><'col-xl-2 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
		});

		$('#table-labels_wrapper .title').append('<h5>Etiquetas</h5>');

		$('#table-labels_wrapper .tools').append($('#btns-tools-labels').contents());

		$('#btns-tools-labels').show();
    });

    $(".btn-export-status").click(function() {

        bootbox.confirm('Se descargará un archivo Excel', function(result) {
            if (result) {
                $('#table-status-tickets').tableExport({tableName: 'Estados', type:'excel', escape:'false'});
            }
        });
        
    });

    $(".btn-export-categories").click(function() {

        bootbox.confirm('Se descargará un archivo Excel', function(result) {
            if (result) {
                $('#table-categories-tickets').tableExport({tableName: 'Estados', type:'excel', escape:'false'});
            }
        });
        
    });

    $('#table-status-tickets tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_status_tickets.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            status_ticket_selected = table_status_tickets.row( this ).data();

            if (jQuery.inArray(status_ticket_selected.id, [1, 2]) == -1) {
                $('.modal-status-tickets').modal('show');
            } else {
                generateNoty('warning', 'No se puede editar el estado seleccionado');
            }
        }
    });

    $('#table-categories-tickets tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_categories_tickets.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            category_ticket_selected = table_categories_tickets.row( this ).data();

            if (jQuery.inArray(category_ticket_selected.id, [1, 2, 3, 4]) == -1) {
                $('.modal-categories-tickets').modal('show');
            } else {
                generateNoty('warning', 'No se puede editar la categoría seleccionada');
            }
        }
    });

    $('#table-labels tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_labels.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            label_selected = table_labels.row( this ).data();

            $('.modal-labels').modal('show');
        }
    });

    $(".btn-export-labels").click(function(){

        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if (result) {
                $('#table-labels').tableExport({tableName: 'Etiquetas', type:'excel', escape:'false'});
            }
        });
    });

    $('a#btn-edit-labels').click(function() {

        $('.modal-labels').modal('hide');

        $('#modal-edit-labels #id').val(label_selected.id);
        $('#modal-edit-labels #text').val(label_selected.text);
        $('#modal-edit-labels #entity').val(label_selected.entity);
        $('#modal-edit-labels #color-back').val(label_selected.color_back);
        $('#modal-edit-labels #color-text').val(label_selected.color_text);

        $('#modal-edit-labels #enabled').attr('checked', label_selected.enabled);
        $('#modal-edit-labels').modal('show');

    });

    $('#btn-edit-status-ticket').click(function() {

        $('.modal-status-tickets').modal('hide');

        $('#modal-edit-status-ticket #id').val(status_ticket_selected.id);
        $('#modal-edit-status-ticket #name').val(status_ticket_selected.name);
        $('#modal-edit-status-ticket #ordering').val(status_ticket_selected.ordering);

        $('#modal-edit-status-ticket #enabled').attr('checked', status_ticket_selected.enabled);
        $('#modal-edit-status-ticket').modal('show');

    });

    $('#btn-edit-category-ticket').click(function() {

        $('.modal-categories-tickets').modal('hide');

        $('#modal-edit-category-ticket #id').val(category_ticket_selected.id);
        $('#modal-edit-category-ticket #name').val(category_ticket_selected.name);
        $('#modal-edit-category-ticket #ordering').val(category_ticket_selected.ordering);
        $('#modal-edit-category-ticket #color-back').val(category_ticket_selected.color_back);
        $('#modal-edit-category-ticket #color-text').val(category_ticket_selected.color_text);

        $('#modal-edit-category-ticket #enabled').attr('checked', category_ticket_selected.enabled);
        $('#modal-edit-category-ticket').modal('show');

    });

    $('#modal-edit-labels #enabled').click(function() {
        if ($('#modal-edit-labels #enabled').val() == '1') {
            $('#modal-edit-labels #enabled').val(1);
        } else {
            $('#modal-edit-labels #enabled').val(0);
        }
    });

    $('#modal-edit-status #enabled').click(function() {
        if ($('#modal-edit-status #enabled').val() == '1') {
            $('#modal-edit-status #enabled').val(1);
        } else {
            $('#modal-edit-status #enabled').val(0);
        }
    });

    $('#modal-edit-caetgory #enabled').click(function() {
        if ($('#modal-edit-caetgory #enabled').val() == '1') {
            $('#modal-edit-caetgory #enabled').val(1);
        } else {
            $('#modal-edit-caetgory #enabled').val(0);
        }
    });

    $('.btn-add-labels').click(function() {
       $('#modal-add-labels').modal('show');
    });

    $('.btn-add-status').click(function() {
       $('#modal-add-status').modal('show');
    });

    $('.btn-add-categories').click(function() {
       $('#modal-add-categories').modal('show');
    });

	$('a#btn-delete-labels').click(function() {
        var text = "¿Está Seguro que desea eliminar la etiqueta?";
        var id = label_selected.id;

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                    .append( $('<form/>').attr({'action': '/ispbrain/labels/delete/', 'method': 'post', 'id': 'replacer'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer').submit();
            }
        });
    });

    $('a#btn-delete-status-ticket').click(function() {
        var text = "¿Está Seguro que desea eliminar el estado?";
        var id = status_ticket_selected.id;

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                    .append( $('<form/>').attr({'action': '/ispbrain/tickets/deleteStatus/', 'method': 'post', 'id': 'replacer'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer').submit();
            }
        });
    });

    $('a#btn-delete-category-ticket').click(function() {
        var text = "¿Está Seguro que desea eliminar la categoría?";
        var id = category_ticket_selected.id;

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                    .append( $('<form/>').attr({'action': '/ispbrain/tickets/deleteCategory/', 'method': 'post', 'id': 'replacer'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer').submit();
            }
        });
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

    $(document).on("click", "#ticket-save-change-start-task", function(e) {

        $('body')
            .append( $('<form/>').attr({'action': '/ispbrain/tickets/changeStartTaskFromConfig/', 'method': 'post', 'id': 'replacer'})
            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
            .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
            .find('#replacer').submit();
    });

</script>
