<style type="text/css">

    .comment-text {
        display: block;
        text-overflow: ellipsis;
        word-wrap: break-word;
        overflow: hidden;
        line-height: 1.8em;
        max-height: 115px;
        overflow-y: auto;
    }

    .border-bottom {
        border-bottom: 1px solid #dee2e6!important;
    }

</style>

<div class="row">

    <div class="col-sm-12 col-md-6 col-lg-6 col-xl-5">
        <div class="card">

            <div class="card-header card-tickets">
                <h5><?= __('Datos del Ticket') ?></h5>
            </div>

            <div class="card-block">
                <?= $this->Form->create($ticket, ['id' => 'form-add-ticket']) ?>
                    <fieldset>

                        <div class="row">

                            <div class="col-md-12">
                                <div class='input-group date' id='start-task-datetimepicker'>
                                    <label class="input-group-text" for="start-task">Inicio *</label>
                                    <input name='start_task' id="start-task" type='text' class="form-control" required/>
                                    <span class="input-group-addon input-group-text calendar">
                                        <span class="glyphicon icon-calendar"></span>
                                    </span>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="input-group select required mt-2">
                                    <label class="input-group-text w-auto" for="category"><?= __('Categoría *') ?></label>
                                    <select name="category" required id="category" class="form-control is-valid">
                                        <option value=""><?= __('Seleccionar') ?></option>
                                        <?php foreach($tickets_categories as $key => $ticket_category): ?>
                                            <option value="<?= $ticket_category->id ?>"><i class="fas fa-square-full"></i> <?= $ticket_category->name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="input-group select mt-2">
                                    <label class="input-group-text w-auto" for="area_id"><?= __('Área') ?></label>
                                    <select name="area_id" id="area_id" class="form-control is-valid">
                                        <option value=""><?= __('Seleccionar') ?></option>
                                        <?php foreach($areas as $key => $area): ?>
                                            <option value="<?= $area->id ?>"><i class="fas fa-square-full"></i> <?= $area->name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="input-group select mt-2">
                                    <label class="input-group-text w-auto" for="asigned_user_id"><?= __('Usuario') ?></label>
                                    <select name="asigned_user_id" id="asigned_user_id" class="form-control is-valid">
                                        <option value=""><?= __('Seleccionar') ?></option>
                                        <?php foreach($users_add as $key => $user_add): ?>
                                            <option value="<?= $user_add->id ?>"><i class="fas fa-square-full"></i> <?= $user_add->name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>

                                <hr class="mt-2 mb-2">

                                <div class="form-group text required" style="padding: 14px;">
                                    <label class="control-label" for="title">Título *</label>
                                    <input type="text" name="title" required="required" id="title" class="form-control" maxlength="100">
                                </div>

                                <hr class="mt-2 mb-2">

                                <div class="form-group text">
                                    <textarea name="description" id="editor1">
                                    </textarea>
                                </div>

                                <?php
                                    echo $this->Form->hidden('customer_id', ['id' => 'info_customer_id']);
                                    echo $this->Form->hidden('connection_id', ['id' => 'info_connection_id']);
                                ?>

                            </div>
                        </div>

                    </fieldset>
                <?= $this->Form->button(__('Agregar'), ['class' => 'btn-primary btn-add-ticket mt-1 ml-3 btn' ]) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-6 col-lg-6 col-xl-7">
        
        <div class="card border-secondary mb-3" id="customer-info">

            <div class="card-header">

                 <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <button type="button" class="btn btn-default " data-toggle="modal" data-target="#modal-customer">
                          Buscar Cliente
                        </button>
                    </div>
                    <div class="col-3 d-none">
                        <button type="button" title="Observación" class="btn btn-default btn-observations float-right my-hidden"><span class="fas fa-file-signature" aria-hidden="true"></span></button>
                    </div>
                    <div class="col-3 d-none">
                        <button type="button" title="Editar Cliente" class="btn btn-default btn-edit-customer float-right my-hidden"><span class="glyphicon icon-pencil2" aria-hidden="true"></span></button>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <button type="button" id="btn-open-modal-connections" class="btn btn-warning btn-default text-white">Seleccionar Conexión</button>
                    </div>
                </div>
            </div>
            <div class="card-block card-data-block p-4">

                <legend class="sub-title-sm">Cliente seleccionado</legend>

                <div class="row">

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Nombre') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right" id="name"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Código') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right" id="code"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Documento') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right" id="ident"></span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Teléfono') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right" id="phone"></span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Domicilio') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right" id="address"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Presupuesto') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right" id="is_presupuesto"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Denominación') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right" id="denomination"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Facturar con') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right" id="business_billing"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Etiquetas') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right" id="labels"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Comentario') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right comment-text" id="comments"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 mt-3">
                        <legend class="sub-title-sm">Conexión seleccionada</legend>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Controlador') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-left comment-text" id="connection-node-seleted"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Servicio') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-left comment-text" id="connection-service-seleted"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('IP') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-left comment-text" id="connection-ip-seleted"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Domicilio') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-left comment-text" id="connection-address-seleted"></span>
                            </div>
                        </div>
                    </div>

                    <div id="accordion" class="d-none col-12 mt-3">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">

                                    <button class="btn btn-secondary" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <?= $paraments->gral_config->billing_for_service ? 'Servicios y otras ventas' : 'Servicios' ?>
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <table id="table-services" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th><?= $paraments->gral_config->billing_for_service ? 'Servicios y otras ventas' : 'Servicios' ?></th>
                                                        <th>Deuda mes</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 mt-3">
                        <legend class="sub-title-sm">Historial de Tickets</legend>
                    </div>
                    <div class="col-xl-12">
                        <table class="table table-hover table-bordered" id="table-tickets">
                           <thead>
                               <tr>
                                    <th></th>           <!--0-->
                                    <th>#</th>          <!--1-->
                                    <th>Creado</th>     <!--2-->
                                    <th>Modif.</th>     <!--3-->
                                    <th>Usuario</th>    <!--4-->
                                    <th>Estado</th>     <!--5-->
                                    <th>Categ.</th>     <!--6-->
                                    <th>Cód.</th>       <!--7-->
                                    <th>Cliente</th>    <!--8-->
                                    <th>Dom.</th>       <!--9-->
                                    <th>Inicio</th>     <!--10-->
                                    <th>Asig.</th>      <!--11-->
                                    <th>Título</th>     <!--12-->
                                    <th>Área</th>       <!--13-->
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
            
        </div>

    </div>

</div>

<?php if ($paraments->customer->search_list_customers): ?>

    <div class="modal fade" id="modal-customer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-12">
                            <button type="button" class="close" aria-label="Close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <?= $this->element('customer_list_seeker') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php else: ?>

    <div class="modal fade" id="modal-customer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-12">
                            <button type="button" class="close" aria-label="Close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <?= $this->element('customer_seeker') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php endif; ?>

<div class="modal fade" id="modal-find-connection" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <?= $this->element('connections_seeker', [
                    'title' => 'Seleccionar Conexión',
                    'class_title' => 'text-warning',
                ]) ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ticket-number">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"><?= __('Mensaje') ?></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">

                <label for="">Último Ticket <span class="label label-success">REGISTRADO </span> correctamente </label>
                <br>
                <h1><span class="badge badge-success ">TICKET #<?= $ticket_number ?></span></h1>
            </div>
            <div class="modal-footer">
                <a class="btn btn-success ml-1 float-left" href="javascript:void(0)" data-dismiss="modal"><i class="fas fa-plus"></i> Cargar Ticket</a>
                <a class="btn btn-default ml-1 float-right" href="/ispbrain/tickets/index_table"><i class="fas fa-list"></i> Ir Lista de Tickets</a>
            </div>
        </div>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-info-conn',
        'name' =>  'Información',
        'icon' =>  'icon-eye',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-edit-conn',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-administrative-conn',
        'name' =>  'Administrativa ',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-disable-conn',
        'name' =>  'Deshabilitar',
        'icon' =>  'icon-lock',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-enable-conn',
        'name' =>  'Habilitar',
        'icon' =>  'icon-unlocked',
        'type' =>  'btn-secondary'
    ];
  
    $buttons[] = [
        'id'   =>  'btn-delete-conn',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-service', 'title' => 'Acciones', 'buttons' => $buttons ]);
    echo $this->element('search_columns', ['modal' => 'modal-search-columns-tickets']);
?>

<script type="text/javascript">

    var ticket_number = <?= json_encode($ticket_number); ?>;
    var editor  = null;
    var table_services = null;
    var table_tickets = null;
    var users = null;
    var status_ti = <?php echo json_encode($status_ti); ?>;
    var categories = <?php echo json_encode($categories); ?>;
    var curr_pos_scroll;
    var areas_ticket = null;

    function format(d) {

        var html = '<table class="table table-tickets-records">'
            + '<thead>'
                + '<tr>'
                  + '<th>Fecha</th>'
                  + '<th>Acción</th>'
                  + '<th>Descripción</th>'
                  + '<th>Imagen</th>'
                  + '<th>Usuario</th>'
                + '</tr>'
              + '</thead>'
            + '<tbody>';

        $.each(d.tickets_records, function(i, ticket_record) {
            var created = '';
            if (ticket_record.created) {
                created = ticket_record.created.split('T')[0];
                created = created.split('-');
                created = created[2] + '/' + created[1] + '/' + created[0];
            }
            var short_action = ticket_record.short_description ? ticket_record.short_description : '';

            var image = '';

            if (ticket_record.image) {
                image = '<img id="img-signature" src="/ispbrain/ticket_image/' + ticket_record.image + '" class="img-fluid" alt="Firma">';
            } else {
                image = '';
            }

            html +=
            '<tr>'
              + '<td>' + created + '</td>'
              + '<td>' + short_action + '</td>'
              + '<td>' + ticket_record.description + '</td>'
              + '<td>' + image + '</td>'
              + '<td>' + ticket_record.user.name + '</td>'
            + '</tr>';
        });

        html += '</tbody>'
            + '</table>';

        return html;
    }

    $('#modal-find-connection').on('shown.bs.modal', function (e) {
        table_connections.draw();
    });

    $('#card-customer-seeker').on('CUSTOMER_SELECTED', function() {

        var class_debt = sessionPHP.paraments.gral_config.billing_for_service ? '' : 'd-none';

        clear_connection_selected();

        $('#customer-code-selected-from').val(customer_selected.code); 

        $('.btn-edit-customer').removeClass('my-hidden');
        $('.btn-observations').removeClass('my-hidden');
        var doc_type_name = sessionPHP.afip_codes.doc_types[customer_selected.doc_type];
        $('#customer-info #name').html(customer_selected.name);
        $('#customer-info #code').html(customer_selected.code);
        var ident = "";
        if (customer_selected.ident) {
            ident = customer_selected.ident;
        }
        $('#customer-info #ident').html(doc_type_name + ' ' + ident);
        $('#customer-info #phone').html(customer_selected.phone);
        $('#customer-info #address').html(customer_selected.address);
        var is_presupuesto = customer_selected.is_presupuesto ? 'Si' : 'No';
        $('#customer-info #is_presupuesto').html(is_presupuesto);
        var denomination = customer_selected.denomination ? 'Si' : 'No';
        $('#customer-info #denomination').html(denomination);
        $('#customer-info #comments').html(customer_selected.comments);
        var business_name = '';
        $.each(sessionPHP.paraments.invoicing.business, function (i, b) {
            if (b.id == customer_selected.business_billing) {
                business_name = b.name;
            }
        });
        $('#customer-info #labels').html("");
        $('#customer-info #business_billing').html(business_name);
        $.each(customer_selected.labels, function( index, label ) {
            $('#customer-info #labels').append("<span class='badge m-1' style='background-color: " + label.color_back + "; color: " + label.color_text + "' >" + label.text + "</span>");
        });

        $('#modal-customer').modal('hide');

        $('#info_customer_id').val(customer_selected.code);

        table_tickets.draw();
        table_tickets.ajax.reload();

        table_connections.ajax.reload();

        table_services = $('#table-services').DataTable({
		    "order": [[ 0, 'desc' ]],
            "deferRender": true,
            "searching": false,
            "destroy": true,
            "info": false,
		    "ajax": {
                "url": "/ispbrain/connections/get_accounts_status.json",
                "dataSrc": "connections",
                "data": function ( d ) {
                  return $.extend( {}, d, {
                    "customer_code": customer_selected ? customer_selected.code : 0,
                    "deleted": 0
                  });
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": false,
		    "columns": [
                { 
                    "data": "created",
                    "render": function ( data, type, row ) {
                        var result = "";
                        if (data != "") {

                            var created = data.split('T')[0];
                            created = created.split('-');
                            created =  created[2] + '/' + created[1] + '/' + created[0];
                            result += created + ' - ' + row.service.name + ' - ' + row.address;
                        } else {
                            result += 'Otras ventas';
                        }
                        return result;
                    }
                },
                {
                    "class": "right " + class_debt,
                    "data": "debt_month",
                    "render": function ( data, type, row ) {

                       if (customer_selected.billing_for_service) {
                           return number_format(data, true);
                       }
                       return '-';
                    }
                }
            ],
		    "columnDefs": [
		        { "type": 'date-custom', targets: 0 },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;

                if (!data.enabled) {
                    $(row).addClass('locked');
                }
                if (data.error) {
                    $(row).addClass('error-custom');
                }
            },
		    "language": dataTable_lenguage,
            "lengthMenu": [[ -1], ["Todas"]],
        	"dom":
    	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
   
		});

		$('#table-services tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                table_services.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');

                var move_selected = table_services.row( this ).data() ;

                if (move_selected.created != "") {
                    $('.modal-service').modal('show');
                }
            }
        });

        $('#btn-info-conn').click(function() {
            var action = '/ispbrain/connections/view/' + table_services.$('tr.selected').attr('id');
            window.open(action, '_blank');
        });

        $('#btn-administrative-conn').click(function() {
            var action = '/ispbrain/connections/administrative/' + table_services.$('tr.selected').attr('id');
            window.open(action, '_blank');
        });

        $('#btn-edit-conn').click(function() {
            var action = '/ispbrain/connections/edit/' + table_services.$('tr.selected').attr('id');
            window.open(action, '_blank');
        });

        $('#btn-disable-conn').click(function() {

            if (!table_services.$('tr.selected').hasClass('locked')) {

                var text = '¿Está seguro que desea deshabilitar está conexión?';
                var id  = table_services.$('tr.selected').attr('id');

                bootbox.confirm(text, function(result) {
                    if (result) {
                        $('body')
                            .append( $('<form/>').attr({'action': '/ispbrain/connections/disable/', 'method': 'post', 'id': 'form-block'})
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                            .find('#form-block').submit();
                    }
                });
            } else {
                generateNoty('information', 'La conexión ya se encuentra deshabilitada');
            }
        });

        $('#btn-enable-conn').click(function() {

            if (table_services.$('tr.selected').hasClass('locked')) {

                var text = '¿Está seguro que desea habilitar está conexión?';
                var id  = table_services.$('tr.selected').attr('id');

                bootbox.confirm(text, function(result) {
                    if (result) {
                        $('body')
                            .append( $('<form/>').attr({'action': '/ispbrain/connections/enable/', 'method': 'post', 'id': 'form-block'})
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                            .find('#form-block').submit();
                    }
                });
            } else {
                generateNoty('information', 'La conexión ya se encuentra habilitada');
            }
        });

        $('#btn-delete-conn').click(function() {

            var text = '¿Está Seguro que desea eliminar la conexión ?';
            var id  = table_services.$('tr.selected').attr('id');

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/connections/delete/', 'method': 'post', 'id': 'replacer'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer').submit();
                }
            });
        });

        $('#accordion').removeClass('d-none');
        $('.collapse').collapse('hide');
    });

    $('#card-customer-seeker').on('CUSTOMER_CLEAR', function() {

        $('#info_customer_id').val('');
        $('#info_connection_id').val('');

        $('.btn-edit-customer').addClass('my-hidden');
        $('.btn-observations').addClass('my-hidden');

        $('#customer-info #name').html("");
        $('#customer-info #code').html("");
        $('#customer-info #ident').html("");
        $('#customer-info #phone').html("");
        $('#customer-info #address').html("");
        $('#customer-info #is_presupuesto').html("");
        $('#customer-info #denomination').html("");
        $('#customer-info #comments').html("");
        $('#customer-info #labels').html("");
        $('#customer-info #business_billing').html("");

        $('#connection-node-seleted').text('');
        $('#connection-service-seleted').valtext;
        $('#connection-ip-seleted').text('');
        $('#connection-address-seleted').text('');
        $('#accordion').addClass('d-none');

        clear_card_connecion_seeker();
        clear_connection_selected();

        customer_selected = null;

        table_tickets.draw();
        table_tickets.ajax.reload();
    });

    $("#accordion").on('shown.bs.collapse', function() {
        if (table_services) {
            table_services.draw();
        }
    });

    $(document).on('CONNECTION_SELECTED', function() {
        $('#info_connection_id').val(connection_selected.id);

        $('#connection-node-seleted').text(connection_selected.controller.name);
        $('#connection-service-seleted').text(connection_selected.service.name);
        $('#connection-ip-seleted').text(connection_selected.ip);
        $('#connection-address-seleted').text(connection_selected.address);
    });

    $(document).on('CONNECTION_UNSELECTED', function() {
        clear_connection_selected();
    });

    function clear_connection_selected() {
        $('#info_connection_id').val('');

        $('#connection-node-seleted').text('');
        $('#connection-service-seleted').text('');
        $('#connection-ip-seleted').text('');
        $('#connection-address-seleted').text('');
    };

    $('#modal-customer').on('shown.bs.modal', function (e) {

        if (sessionPHP.paraments.customer.search_list_customers) {

            enableLoad = 1;
            table_customers.draw();
        } else {
            clear_card_customer_seeker();
            $('#card-customer-seeker input#customer-ident').focus();
        }
    });

    $(document).ready(function() {

        users = <?= json_encode($users) ?>;
        areas_ticket = <?= json_encode($areas_ticket) ?>;

        table_tickets = $('#table-tickets').DataTable({
    	    "order": [[ 1, 'desc' ]],
    	    "autoWidth": true,
    	    "scrollY": '350px',
    	    "scrollCollapse": true,
    	    "scrollX": true,
    	    "sWrapper":  "dataTables_wrapper dt-bootstrap4",
            "processing": true,
            "serverSide": true,
		    "ajax": {
                "url": "/ispbrain/tickets/get_tickets_from_add_ticket.json",
                "dataFilter": function(data) {
                    var json = $.parseJSON(data);
                    return JSON.stringify(json.response);
                },
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "customer_code": customer_selected ? customer_selected.code : 0
                    });
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "drawCallback": function(c) {
                var flag = true;
            	switch (c.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
            	    if (table_tickets) {

                        var tableinfo = table_tickets.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }

                        if (curr_pos_scroll) {
                            $(table_tickets.settings()[0].nScrollBody).scrollTop(curr_pos_scroll.top);
                            $(table_tickets.settings()[0].nScrollBody).scrollLeft(curr_pos_scroll.left);
                        }
                    }
            	}
            },
            "columns": [
                {
                    "className": 'details-control ',
                    "orderable": false,
                    "data":      'tickets_records',
                    "render": function ( data, type, full, meta ) {
                        var num = data.length;
                        if (num > 0) {
                            return "<a href='javascript:void(0)' class='grey'>"
                                + "<span class='icon-plus' aria-hidden='true'>"
                                + "</span>"
                                + "</a>";
                        }

                        return '';
                    },
                    "width": '3%'
                },
                { 
                    "data": "id",
                    "type": "integer"
                },
                { 
                    "data": "created",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                { 
                    "data": "modified",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                { 
                    "data": "user_id",
                    "type": "options",
                    "options": users,
                    "render": function ( data, type, row ) {
                        var u = "";
                        if (typeof data !== "undefined") {
                            $.each(users, function( index, value ) {
                                if (data == index) {
                                    u = value;
                                }
                            });
                        }
                        return u;
                    }
                },
                { 
                    "data": "status.name",
                    "type": "options",
                    "options": status_ti,
                },
                {
                    "data": "category.name",
                    "type": "options",
                    "options": categories,
                    "render": function ( data, type, row ) {
                        return '<span class="badge" id="category" style="font-size: 100%; color: ' + row.category.color_text + '; background-color: ' + row.category.color_back + '">' + data + '</span>';
                    }
                },
                { 
                    "data": "customer",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        var customer = "";
                        if (data) {
                            customer = pad(data.code, 5);
                        }
                        return customer;
                    }
                },
                { 
                    "data": "customer",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        var customer = "";
                        if (data) {
                            customer = data.name
                        }
                        return customer;
                    }
                },
                { 
                    "data": "customer",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        var customer = "";
                        if (data) {
                            customer = data.address
                        }
                        return customer;
                    }
                },
                { 
                    "data": "start_task",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            var created_2 = data.split('T')[1].split('-')[0].split(':');

                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0] + ' ' + created_2[0] + ':' + created_2[1];
                        }
                    }
                },
                { 
                    "data": "asigned_user_id",
                    "type": "options",
                    "options": users,
                    "render": function ( data, type, row ) {
                        var u = "";
                        if (typeof data !== "undefined") {
                            $.each(users, function( index, value ) {
                                if (data == index) {
                                    u = value;
                                }
                            });
                        }
                        return u;
                    }
                },
                {
                    "class": "col-description",
                    "data": "title",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        return data;
                    }
                },
                {
                    "data": "area_id",
                    "type": "options",
                    "options": areas_ticket,
                    "render": function ( data, type, row ) {
                        var u = "";
                        if (typeof data !== "undefined" && data != null) {
                            u = areas_ticket[data];
                        }
                        return u;
                    }
                },
            ],
            "columnDefs": [
                { 
                    "visible": false, targets: [7, 8, 9]
                },
                { 
                    "class": "left", 
                    "width": "15%",
                    "targets": [8]
                },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
    	    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
    	});

		$('#table-tickets_filter').closest('div').before($('#btns-tools').contents());

		$('#table-tickets').on( 'init.dt', function () {
            createModalSearchColumn(table_tickets, '.modal-search-columns-tickets');
        });

        $('#btns-tools').show();

		$('#table-tickets tbody').on('click', 'td.details-control', function () {

            if (!$(this).closest('tr').find('.dataTables_empty').length) {
                table_tickets.$('tr.selected').removeClass('selected');
                $(this).closest('tr').addClass('selected');
            }

            var tr = $(this).closest('tr');
            var row = table_tickets.row( tr );

            if ( row.child.isShown() ) {
                row.child.hide();
                tr.removeClass('shown');
                $(this).find('span').removeClass('icon-minus');
                $(this).find('span').addClass('icon-plus');
            } else {
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
                $(this).find('span').removeClass('icon-plus');
                $(this).find('span').addClass('icon-minus');
            }
        });

        $('#btn-open-modal-connections').click(function() {
            $('#modal-find-connection').modal('show');
        });

        $('#start-task-datetimepicker').datetimepicker({
            defaultDate: new Date(),
            locale: 'es',
            format: 'DD/MM/YYYY HH:mm'
        });

        editor = CKEDITOR.replace( 'editor1' );

        CKEDITOR.config.title = false;
        CKEDITOR.config.toolbar = [
        	{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo' ] },
        	{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ] },
        	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent' ] },
        	{ name: 'styles', items: [ 'Format' ] },
        ];

        if (ticket_number) {
            $('#modal-ticket-number').modal('show');
        }

        $('#form-add-ticket').submit(function() {
            $('.btn-add-ticket').hide();
        });

    });
</script>
