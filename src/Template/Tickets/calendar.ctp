<style type="text/css">

    .btn-save-active {
        color: white !important;
        background: #dc3545 !important;
    }

</style>
    
<link href='/ispbrain/vendor/fullcalendar-3.9.0/fullcalendar.min.css' rel='stylesheet' />
<link href='/ispbrain/vendor/fullcalendar-3.9.0/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<script src='/ispbrain/vendor/fullcalendar-3.9.0/lib/moment.min.js'></script>
<script src='/ispbrain/vendor/fullcalendar-3.9.0/fullcalendar.min.js'></script>
<script src='/ispbrain/vendor/fullcalendar-3.9.0/locale-all.js'></script>
<script>

    var events;
    var ticket_selected;
    var users;
    var action;
    var send_datas = [];

    $(document).ready(function() {
        action = true;

        events = <?php echo json_encode($events); ?>;
        users = <?php echo json_encode($users); ?>;

        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listMonth,view_table,save'
            },
            customButtons: {
                save: {
                    text: 'Guardar',
                    click: function() {
                        if (send_datas.length > 0) {
                            $.ajax({
                                type: 'POST',
                                dataType: "json",
                                url: '/ispbrain/tickets/changeStartTaskFromCalendar',
                                data:  JSON.stringify({data: send_datas}),
                                success: function(data) {
                                    generateNoty(data.response.typeMsg, data.response.msg);
                                    send_datas = [];
                                    $('.fc-save-button').removeClass('btn-save-active');
                                },
                                error: function(jqXHR) {

                                    if (jqXHR.status == 403) {

                                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                                    	setTimeout(function() {
                                            window.location.href = "/ispbrain";
                                    	}, 3000);

                                    } else {
                                    	generateNoty('error', 'Error al intentar cambiar la fecha de inicio del ticket.');
                                    }
                                }
                            });
                        }
                    }
                },
                view_table: {
                    text: 'Lista de Tickets',
                    click: function() {
                        if (send_datas.length > 0) {
                            var text = 'Tiene cambios sin guardar, ¿Ir igualmente a la lista de Tickets?';

                            bootbox.confirm(text, function(result) {
                                if (result) {
                                    window.location = "/ispbrain/tickets/index_table";
                                }
                            });
                        } else {
                            window.location = "/ispbrain/tickets/index_table";
                        }
                    }
                }
            },
            defaultDate: new Date(),
            locale: 'es',
            navLinks: true, // can click day/week names to navigate views
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            events: events,
            displayEventTime: false,
            eventRender: function (event, element) {

                element.attr('href', 'javascript:void(0);');
                element.click(function() {
                    if (action) {
                        ticket_selected = event;

                        $("#titleTicket").html(event.title);
                        $("#startTime").html(moment(event.start).format('DD/MM/YYYY HH:mm'));
                        $("#customerInfo").html(event.customer);
                        $("#titleTicketPosta").html(event.ticket_title);
                        $("#eventInfo").html(event.description);
                        $("#statusInfo").html(event.status);
                        $("#userAsigned").html(event.user_asigned);
    
                        $('#eventContent').modal('show');
                    } else {
                        generateNoty('warning', "Actualizando información, al terminar podrá abrir la ventana.");
                    }
                });
            },
            eventDrop: function(event, delta, revertFunc) {

                if (action) {
                    action = false;
                    $('.fc-save-button').addClass('btn-save-active');
                    $('.fc-save-button').tooltip('show');

                    var data = delta._data;
                    var array_date = event.start.format().split('T');
                    var first_part = array_date[0];
                    var array_first_part = first_part.split('-');
                    var year =  parseInt(array_first_part[0]);
                    var month =  parseInt(array_first_part[1]);
                    var day =  parseInt(array_first_part[2]);

                    var second_part = array_date[1];
                    var array_second_part = second_part.split('-')[0].split(':');
                    var hour =  parseInt(array_second_part[0]);
                    var minute =  parseInt(array_second_part[1]);
                    var second =  parseInt(array_second_part[2]);

                    var newDate = year + '-' + month + '-' + day + 'T' + hour + ':' + minute + ':' + second + '-00:00';

                    var send_data = {
                        'ticket_id': event.ticket_id,
                        'change_start_task': newDate
                    }

                    if (send_data) {
                        send_datas[event.ticket_id] = send_data;
                    }
                    action = true;
                    $('.fc-save-button').tooltip('show');
                }
            }
        });

        $('.fc-save-button').prop('title', 'Para asentar los cambios presione Guardar');
        $('.fc-save-button').prop('data-toggle', 'tooltip');
        $('.fc-save-button').prop('data-placement', 'bottom');

        $('#start-task-datetimepicker').datetimepicker({
            locale: 'es',
            format: 'DD/MM/YYYY HH:mm'
        });

        // Jquery draggable modals
        $('.modal-dialog').draggable({
            handle: ".modal-header"
        });

        $('.btn-edit').click(function() {
            $('#eventContent').modal('hide');

            if (ticket_selected.user_asigned != "-") {
                $('.lbl-user-assigned-name').text('Reasignar Usuario');
            } else {
                $('.lbl-user-assigned-name').text('Asignar Usuario');
            }
            $('.modal-actions-tickets').modal('show');
        });

        $('#btn-assigned').click(function() {
            $('.modal-actions-tickets').modal('hide');

		    $('#ticket_id_user_assigment').val(ticket_selected.ticket_id);
		    $('#modal-user-assigned').modal('show');
		});

		$('#form-user-assigned').submit(function() {

		    var user_name = $('#asigned-user-id :selected').text();

		    $('#modal-user-assigned').modal('hide');

		    openModalPreloader("Asignando Usuario ...");

		    event.preventDefault();

            var send_data = {};  
            var form_data = $(this).serializeArray();
            $.each(form_data, function(i, val) {
                send_data[val.name] = val.value;
            });

            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '/ispbrain/tickets/userAssignment',
                data:  JSON.stringify({data: send_data}),
                success: function(data) {

                    closeModalPreloader();
                    generateNoty(data.response.typeMsg, data.response.msg);

                    $.each(events, function( index, event ) {
                        if (event.ticket_id == ticket_selected.ticket_id) {
                            event.user_asigned = user_name;
                        }
                    });

                    refreshTickets();
                },
                error: function(jqXHR) {
                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al asignar usuario al Ticket.');
                    }
                }
            });
		});

		$('#btn-change-status').click(function() {

		    $('.modal-actions-tickets').modal('hide');

		    $('#ticket_id_change_status').val(ticket_selected.ticket_id);

		    $('#modal-change-status').modal('show');
		});

		$('#form-change-status').submit(function() {

		    $('#modal-change-status').modal('hide');

		    openModalPreloader("Cambiando estado...");

		    event.preventDefault();

            var send_data = {};  
            var form_data = $(this).serializeArray();
            $.each(form_data, function(i, val) {
                send_data[val.name] = val.value;
            });

            var status_name = $('#change-status-id :selected').text();
            send_data.status_name = status_name

            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '/ispbrain/tickets/changeStatus',
                data:  JSON.stringify({data: send_data}),
                success: function(data) {

                    closeModalPreloader();
                    generateNoty(data.response.typeMsg, data.response.msg);

                    $.each(events, function( index, event ) {
                        if (event.ticket_id == ticket_selected.ticket_id) {
                            event.status = status_name;
                        }
                    });

                    refreshTickets();
                },
                error: function(jqXHR) {
                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                    	  window.location.href ="/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al cambiar el estado del Ticket.');
                    }
                }
            });
		});

		$('#btn-change-start-task').click(function() {

            $('.modal-actions-tickets').modal('hide');

            $('#ticket_id_change_start_task').val(ticket_selected.ticket_id);

            var start_task = ticket_selected.start_task.split('T')[0];
            start_task = start_task.split('-');

            var start_task_2 = ticket_selected.start_task.split('T')[1];
            start_task_2 = start_task_2.split('-')[0];
            start_task_2 = start_task_2.split(':');

            start_task = start_task[2] + '/' + start_task[1] + '/' + start_task[0] + ' ' + start_task_2[0] + ':' + start_task_2[1];

            $('#start-task-datetimepicker').data('DateTimePicker').date(start_task);

            $('#modal-change-start-task').modal('show');
        });

        $('#form-change-start-task').submit(function() {

		    $('#modal-change-start-task').modal('hide');

		    openModalPreloader("Cambiando fecha de inicio...");

		    event.preventDefault();

            var send_data = [];
            var data_date = {};
            var form_data = $(this).serializeArray();
            $.each(form_data, function(i, val) {
                data_date[val.name] = val.value;
            });

            var start_task_array = data_date.change_start_task.split(' ');
            var start_task_1 = start_task_array[0].split('/');
            var start_task_2 = start_task_array[1].split(':');

            data_date.change_start_task = start_task_1[2] + '-' + start_task_1[1] + '-' + start_task_1[0] + 'T' + start_task_2[0] + ':' + start_task_2[1] + ':00-00:00';

            send_data.push(data_date);

            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '/ispbrain/tickets/changeStartTaskFromCalendar',
                data:  JSON.stringify({data: send_data}),
                success: function(data) {

                    closeModalPreloader();
                    generateNoty(data.response.typeMsg, data.response.msg);

                    $.each(events, function( index, event ) {
                        if (event.ticket_id == ticket_selected.ticket_id) {

                            //year
                            var year = start_task_1[2];

                            //month
                            var month = start_task_1[1];

                            //day
                            var day = start_task_1[0];

                            //hour
                            var hour = start_task_2[0];

                            //minute
                            var minute = start_task_2[1];

                            //second
                            var second = "00";

                            var newDate = year + '-' + month + '-' + day + 'T' + hour + ':' + minute + ':' + second + '-00:00';

                            event.start = newDate;
                            event.start_task = newDate;
                        }
                    });

                    refreshTickets();
                },
                error: function(jqXHR) {

                    closeModalPreloader();

                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al cambiar la fecha del Ticket.');
                    }
                }
            });
		});

    });

    function refreshTickets() {
        $('#calendar').fullCalendar('removeEvents');
        $('#calendar').fullCalendar('addEventSource', events);
        $('#calendar').fullCalendar('refetchEvents');
    }

</script>

<style>

    body {
        margin: 40px 10px;
        padding: 0;
        font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
        font-size: 14px;
    }

    #calendar {
        max-width: 900px;
        margin: 0 auto;
    }

    .fc-day-grid-event > .fc-content {
        text-overflow: ellipsis;
        white-space: nowrap;
        cursor: pointer; /* for showing title */
        max-height: 20px; /* can be adjusted according to your requirement */
    }

</style>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-assigned',
        'name' =>  '<span class="lbl-user-assigned-name">Asignar usuario</span>',
        'icon' =>  'icon-user-tie',
        'type' =>  'btn-danger'
    ];

    $buttons[] = [
        'id'   =>  'btn-change-status',
        'name' =>  'Cambiar Estado',
        'icon' =>  'fas fa-exchange-alt',
        'type' =>  'btn-dark'
    ];

    $buttons[] = [
        'id'   =>  'btn-change-start-task',
        'name' =>  'Cambiar Inicio',
        'icon' =>  'far fa-calendar-alt',
        'type' =>  'btn-dark'
    ];

    echo $this->element('actions', ['modal'=> 'modal-actions-tickets', 'title' => '<span class="lbl-ticket-title"></span>    Acciones', 'buttons' => $buttons ]);
?>

<?php
    echo $this->element('modal_preloader');
?>

<div class="modal fade" id="eventContent" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="titleTicket"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-xl-12">
                        <b>Inicio</b>: <span id="startTime"></span><br>
                        <b>Cliente</b>: <span id="customerInfo"></span><br>
                        <b>Estado</b>: <span id="statusInfo"></span><br>
                        <b>Asignado</b>: <span id="userAsigned"></span><br>
                        <br>
                        <b>Título</b>: <span id="titleTicketPosta"></span><br>
                        <b>Descripción</b>:<br>
                        <p id="eventInfo"></p>
                        <button class="btn btn-success btn-edit"><i class="fas fa-edit"></i>Editar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-user-assigned" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?= __('Asignar Usuario') ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-md-12">
                        <?= $this->Form->create(null,  [ 'url' => false,  'id' => 'form-user-assigned']) ?>
                            <fieldset>
                                <?php
                                    echo $this->Form->hidden('id', ['id' => 'ticket_id_user_assigment']);
                                    echo $this->Form->hidden('status', ['id' => 'ticket_status_user_assigment']);
                                    echo $this->Form->input('asigned_user_id', ['label' => 'Usuarios', 'options' => $users, 'required' => true]);
                                ?>
                            </fieldset>
                            <?= $this->Form->button(__('Asignar'), ['class' => 'primary']) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-change-status" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?= __('Cambiar Estado') ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-xl-12">
                        <?= $this->Form->create(null,  [ 'url' => false,  'id' => 'form-change-status']) ?>
                            <fieldset>
                                <div class="input-group select">
                                    <label class="input-group-text" for="change-status-id">Estados</label>
                                    <select name="change_status_id" id="change-status-id" class="form-control" required>
                                        <option value="" selected>Seleccionar</option>
                                        <?php foreach ($tickets_status as $status): ?>
                                            <option value="<?= $status->id ?>"><?= $status->name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <?php
                                    echo $this->Form->hidden('ticket_id', ['id' => 'ticket_id_change_status']);
                                ?>
                            </fieldset>
                            <?= $this->Form->button(__('Cambiar'), ['class' => 'primary mt-3']) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-change-start-task" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?= __('Cambiar Inicio') ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-xl-12">
                        <?= $this->Form->create(null,  [ 'url' => false,  'id' => 'form-change-start-task']) ?>
                            <fieldset>
                                <div class='input-group date' id='start-task-datetimepicker'>
                                    <label class="input-group-text" for="start-task">Inicio</label>
                                    <input name='change_start_task' id="change-start-task-id" type='text' class="form-control" />
                                    <span class="input-group-addon input-group-text calendar">
                                        <span class="glyphicon icon-calendar"></span>
                                    </span>
                                </div>
                                <?php
                                    echo $this->Form->hidden('ticket_id', ['id' => 'ticket_id_change_start_task']);
                                ?>
                            </fieldset>
                            <?= $this->Form->button(__('Cambiar'), ['class' => 'primary mt-3']) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id='calendar'></div>
