<style type="text/css">

    #container-tickets .card-header:hover {
        background-color: rgba(105, 104, 104, 0.28) !important;
        cursor: pointer;
    }

    .header-number {
        /*width: 12%;*/
    }

    .header-status {
        /*width: 18%;*/
    }

    .header-category {
        /*width: 22%;*/
    }

    .header-customer {
        /*width: 30%;*/
    }

    .header-date {
        /*width: 33%;*/
    }

    .card-ticket {
        min-height: 215px;
    }

    #map {
		height: 350px;
	}

	.modal {
	    overflow-x: hidden;
	    overflow-y: auto;
	}

    @media only screen and (max-device-width : 750px) {

       .header-number {
            /*width: 50%;*/
        }

        .header-status {
            /*width: 50%;*/
        }

        .header-category {
            /*width: 50%;*/
        }

        .header-customer {
            /*width: 50%;*/
        }

        .header-date {
            /*width: 100%;*/
        }
        
        
    }

    .description {
        height: 50px;
        width: 100%;
        margin-bottom: 20px;
    }

    .my-hidden {
        display: none;
    }
    

    .filter-tickets {
        border: 1px solid #cccccc59;
        padding: 18px;
        font-family: sans-serif;
    }

    .actions-tickets {
        border: 1px solid #cccccc59;
        padding: 18px;
        font-family: sans-serif;
    }

    .view-tickets {
        border: 1px solid #cccccc59;
        padding: 18px;
        font-family: sans-serif;
    }

    .card-header {
        font-size: 12px;
    }

    .badge {
        font-size: 54%;
    }

    .btn-default.active {
        color: #f05f40;
        background-color: #e6e6e6;
        border-color: #f05f40;
    }

    .table-tickets-records th {
        background-color: #607D8B !important;
        color: white !important;
    }

    #digital-signature-description {
        height: 30px;
    }

</style>

<div id="template-card-records-ticket">

    <div class="card d-none">

        <div class="card-header" role="tab" id="headingOne">
          <h5 class="mb-0">
            <a data-toggle="collapse" href="#record-${record_id}" aria-expanded="true" aria-controls="collapseOne">

            <div class="row font-weight-bold">
                <div class="col-4 pr-1 pl-1">
                    <span id="date">${date_record}</span>
                </div>
                <div class="col-4 pr-1 pl-1">
                    <span id="short-description">${short_description_record}</span>
                </div>
                <div class="col-4 pr-1 pl-1">
                    <span id="user">${username}</span>
                </div>
            </div>

            </a>
          </h5>
        </div>

        <div id="record-${record_id}" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
          <div class="card-body">
            ${description_record}
            ${digital_signature_record}
          </div>
        </div>

    </div>

</div>

<div id="template-card-ticket">
    <div class="col-xl-6 d-none card-ticket" id="ticket-${ticket_id}">
        <div class="card border-secondary mb-3 ">
            <div class="card-header btn-open-action crystal-header" data-target="${ticket_id}">

                <div class="row font-weight-bold">
                    <div class="col-auto">
                        <div class="pr-1 header-number">
                            <i class="fas fa-hashtag"></i> <span id="number">${number}</span>
                        </div>
                    </div>

                    <div class="col-auto">
                        <div class="pr-1 header-status text-left">
                            <i class="far fa-circle"></i> <span id="status" class="text-info">${status}</span>
                        </div>
                    </div>

                    <div class="col-auto">
                        <div class="pr-1 header-category text-left">
                            <i class="fas fa-tags"></i> <span class="badge" id="categoty" style="font-size: 100%; color: ${category-color}; background-color: ${category-color-back}">${category}</span>
                        </div>
                    </div>

                    <div class="col-auto">
                        <div class="header-customer text-right">
                            ${customer_icon}
                            <span id="customer">${customer_code}</span>
                            <span id="customer">${customer_name}</span>
                        </div>
                    </div>

                    <div class="col-auto">
                        <div class="p-0 header-date text-left">
                            <i class="far fa-calendar-alt"></i> INICIO: <span id="date">${date_ticket}</span> 
                        </div>
                    </div>
                </div>

            </div>

            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <p class="card-text text-success">
                        ${description_ticket}
                    </p>
                </li>
                <li class="list-group-item">

                    <div class="row">
                        <div class="col-xl-6">
                             <button type="button" class="btn btn-default btn-show-history mb-2" data-target="${ticket_id}">
                              Historial <span class="badge badge-secondary" id="records-amount-${ticket_id}">9999</span>
                            </button>
                        </div>
                        <div class="col-xl-6">
                            <h5><span id="" class="badge badge-danger text-white float-right">${user_asigned}</span></h5>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xl-12">
                            <div id="records-container-${ticket_id}" class="d-none" role="tablist">
                            </div>
                        </div>
                    </div>
                </li>
            </ul>

        </div>

    </div>
</div>

<div class="row mb-2">
    <div class="col-sm-12 col-xl-7 filter-tickets">

        <div class="row">
            <div class="col-xl-12">
                <label>Filtros</label>
            </div>

            <div class="col-xl-6">
                <div class="input-group select">
                    <label class="input-group-text" for="status-id">Estados</label>
                    <select name="status_id" id="status-id" class="form-control filter-ticket">
                        <option value="">Seleccionar</option>
                        <?php foreach ($tickets_status as $status): ?>
                            <option value="<?= $status->id ?>" <?= ($status->id == 1) ? 'selected' : '' ?>><?= $status->name ?></option>
                        <?php endforeach; ?>
                        <option value="-1">Todos</option>
                    </select>
                </div>
            </div>

            <div class="col-xl-6">
                <div class="input-group select">
                    <label class="input-group-text" for="category-id">Categorías</label>
                    <select name="category_id" id="category-id" class="form-control filter-ticket">
                        <option value="">Seleccionar</option>
                        <?php foreach ($tickets_categories as $ticket_category): ?>
                            <option value="<?= $ticket_category->id ?>"><?= $ticket_category->name ?></option>
                        <?php endforeach; ?>
                        <option value="-1" selected>Todos</option>
                    </select>
                </div>
            </div>
        </div>

    </div>
    <div class="col-sm-12 col-xl-2 view-tickets">

        <div class="row">
            <div class="col-xl-12">
                <label>Vistas</label>
            </div>
            <?php

                echo $this->Html->link(
                    '<span class="fas fa-th-large fa-2x" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                        'id'     => 'btn-view-card',
                        'title'  => 'Tarjeta',
                        'class'  => 'btn btn-default btn-lg active float-right mx-auto btn-views',
                        'escape' => false,
                        'data-id' => 'card'
                ]);

                echo $this->Html->link(
                    '<span class="far fa-calendar-alt fa-2x" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                        'id' => 'btn-calendar',
                        'title' => 'Ver Calendario',
                        'class' => 'btn btn-default btn-lg float-right mx-auto',
                        'escape' => false
                ]);

            ?>
      </div>

    </div>
    <div class="col-sm-12 col-xl-3 actions-tickets">

        <div class="row">
            <div class="col-xl-12">
                <label>Acciones</label>
            </div>
            <?php

                echo $this->Html->link(
                    '<span class="glyphicon icon-search fa-2x" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                        'id' => 'btn-open-search-ticket',
                        'title' => 'Buscar Ticket',
                        'class' => 'btn btn-default btn-lg float-right mx-auto',
                        'escape' => false
                ]);

                echo $this->Html->link(
                    '<span class="glyphicon icon-plus fa-2x" aria-hidden="true"></span>',
                    ['controller' => 'tickets', 'action' => 'add'],
                    [
                        'title' => 'Agregar Ticket',
                        'class' => 'btn btn-default btn-lg float-right mx-auto',
                        'escape' => false,
                        'target' => '_black'
                ]);

                echo $this->Html->link(
                    '<span class="far fa-file-pdf fa-2x" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                        'title'  => 'Exportar Tickets PDF',
                        'class'  => 'btn btn-default btn-lg float-right mx-auto',
                        'id'     => 'btn-tickets-export-pdf',
                        'escape' => false,
                        'target' => '_black'
                ]);

            ?>
      </div>

    </div>
</div>

<div class="row" id="container-tickets"></div>

<div id="modal-search-ticket" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header pb-1">
          <h4 class="modal-title">Buscar</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body pt-1">

          <form id="form-search-ticket">

            <div class="row">

                <div class="col-xl-6">

                    <div class="card border-secondary mb-3">
                        <h5 class="card-header">Ticket</h5>
                        <div class="card-body text-secondary">
                            <div class="row">

                                 <div class="col-xl-12">
                                    <div class="input-group text">
                                        <label class="input-group-text" for="number-ticket">Número</label>
                                        <input type="number" name="number_ticket" id="number-ticket" autocomplete="off" class="form-control">
                                    </div>           
                                </div>

                                <div class="col-xl-12">
                                    <div class='input-group date' id='date-ticket-datetimepicker'>
                                        <label class="input-group-addon input-group-text" for="date-ticket">Fecha Creación</label>
                                        <input name='date_ticket' id="date-ticket" type='text' class="form-control"  />
                                        <span class="input-group-addon input-group-text calendar">
                                            <span class="glyphicon icon-calendar"></span>
                                        </span>
                                    </div>
                                </div>

                                <div class="col-xl-12">
                                    <div class="input-group select">
                                        <label class="input-group-text" for="category-id">Categoría</label>
                                        <select name="category_id" id="category-id" class="form-control">
                                            <?php foreach($tickets_categories as $ticket_category):?>
                                                <option value="<?= $ticket_category->id ?>"><?= $ticket_category->name ?></option>
                                            <?php endforeach;?>
                                            <option value="" selected>Todos</option>
                                        </select>
                                    </div>       
                                </div>
                                <div class="col-xl-12">
                                    <div class="input-group text">
                                        <label class="input-group-text" for="description-ticket">Descripción</label>
                                        <input type="text" name="description_ticket" id="description-ticket" class="form-control"  autocomplete="off" >
                                    </div>           
                                </div>
                                <div class="col-xl-12">
                                    <div class="input-group select">
                                        <label class="input-group-text" for="user-id-ticket">Creado por</label>
                                        <select name="user_id_ticket" id="user-id-ticket" class="form-control">
                                            <option value="">Seleccione</option>
                                            <?php foreach($users as $key => $user):?>
                                            <option value="<?=$key?>"><?=$user?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>      
                                </div>

                                <div class="col-xl-12">
                                    <div class="input-group select">
                                        <label class="input-group-text" for="user-id-ticket">Asignado a</label>
                                        <select name="asigned_user_id_ticket" id="asigned-user-id-ticket" class="form-control">
                                            <option value="">Seleccione</option>
                                            <?php foreach($users as $key => $user):?>
                                            <option value="<?=$key?>"><?=$user?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>      
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-xl-6">

                    <div class="card border-secondary mb-3">
                        <h5 class="card-header">Cliente</h5>
                        <div class="card-body text-secondary">
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="input-group text">
                                        <label class="input-group-text" for="customer-code">Código</label>
                                        <input type="text" name="customer_code" id="customer-code" class="form-control"  autocomplete="off" >
                                    </div> 
                                </div>
                                <div class="col-xl-12">
                                    <div class="input-group text">
                                        <label class="input-group-text" for="customer-ident">Documento</label>
                                        <input type="text" name="customer_ident" id="customer-ident" class="form-control"  autocomplete="off" >
                                    </div> 
                                </div>
                                <div class="col-xl-12">
                                    <div class="input-group text">
                                        <label class="input-group-text" for="customer-name">Nombre</label>
                                        <input type="text" name="customer_name" id="customer-name" class="form-control"  autocomplete="off" >
                                    </div>         
                                </div>
                            </div>
                        </div>
                    </div>
          
                </div>

                <div class="col-xl-6">

                    <div class="card border-secondary mb-3">
                        <h5 class="card-header">Historial</h5>
                        <div class="card-body text-secondary">
                            <div class="row">
                                <div class="col-xl-12">
                                     <div class='input-group date' id='date-record-datetimepicker'>
                                        <label class="input-group-text" for="date-record">Fecha Creación</label>
                                        <input name='date_record' id="date-record" type='text' class="form-control" />
                                        <span class="input-group-addon input-group-text calendar">
                                            <span class="glyphicon icon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-xl-12">
                                    <div class="input-group text">
                                        <label class="input-group-text" for="description-record">Descripción</label>
                                        <input type="text" name="description_record" id="description-record" class="form-control"  autocomplete="off" >
                                    </div>                 
                                </div>
                                <div class="col-xl-12">
                                    <div class="input-group select">
                                        <label class="input-group-text" for="user-id-record">Usuario</label>
                                        <select name="user_id_record" id="user-id-record" class="form-control">
                                            <option value="">Seleccione</option>
                                            <?php foreach($users as $key => $user):?>
                                            <option value="<?=$key?>"><?=$user?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>       
                                </div>
                            </div>
                        </div>
                    </div>
         
                </div>

                <div class="col-xl-6">

                    <div class="card border-secondary mb-3">
                        <h5 class="card-header">Conexión</h5>
                        <div class="card-body text-secondary">
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="input-group text">
                                        <label class="input-group-text" for="connection-controller">Controlador</label>
                                        <select name="connection_controller" id="connection-controller" class="form-control">
                                            <option value="">Seleccione</option>
                                            <?php foreach($controllers as $key => $controller):?>
                                            <option value="<?=$key?>"><?=$controller?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>         
                                </div>
                                <div class="col-xl-12">
                                    <div class="input-group text">
                                        <label class="input-group-text" for="connection-service">Servicio</label>
                                        
                                         <select name="connection_service" id="connection-service" class="form-control">
                                            <option value="">Seleccione</option>
                                            <?php foreach($services as $key => $service):?>
                                            <option value="<?=$key?>"><?=$service?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>   
                                </div>
                                <div class="col-xl-12">
                                    <div class="input-group text">
                                        <label class="input-group-text" for="connection-ip">IP</label>
                                        <input type="text" name="connection_ip" id="connection-ip" class="form-control"  autocomplete="off" >
                                    </div>                             
                                </div>
                                <div class="col-xl-12">
                                    <div class="input-group text">
                                        <label class="input-group-text" for="connection-address">Domicilio</label>
                                        <input type="text" name="connection_address" id="connection-address" class="form-control"  autocomplete="off" >
                                    </div>   
                                </div>
                            </div>
                        </div>
                    </div>
        
                </div>

             </div>

              <div class="row">
                  <div class="col-xl-12">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                      <button type="submit" class="btn btn-success float-right ">Buscar</button>
                  </div>
              </div>

          </form>

      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-assigned',
        'name' =>  '<span class="lbl-user-assigned-name">Asignar usuario</span>',
        'icon' =>  'icon-user-tie mr-2',
        'type' =>  'btn-danger'
    ];

    $buttons[] = [
        'id'   =>  'btn-change-status',
        'name' =>  'Cambiar Estado',
        'icon' =>  'fas fa-exchange-alt mr-2',
        'type' =>  'btn-dark'
    ];

    $buttons[] = [
        'id'   =>  'btn-change-start-task',
        'name' =>  'Cambiar Inicio',
        'icon' =>  'far fa-calendar-alt mr-2',
        'type' =>  'btn-dark'
    ];

    $buttons[] = [
        'id'   =>  'btn-info-customer',
        'name' =>  'Info. Cliente',
        'icon' =>  'icon-profile mr-2',
        'type' =>  'btn-info'
    ];

    $buttons[] = [
        'id'   =>  'btn-add-comment',
        'name' =>  'Agregar Comentario',
        'icon' =>  'icon-pencil2 mr-2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-photo',
        'name' =>  'Agregar Foto',
        'icon' =>  'fas fa-camera mr-2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-digital-signature',
        'name' =>  'Firma',
        'icon' =>  'fas fa-edit mr-2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-edit-description',
        'name' =>  'Editar Descripción',
        'icon' =>  'icon-pencil2 mr-2',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-actions-tickets', 'title' => '<span class="lbl-ticket-title"></span> Acciones', 'buttons' => $buttons]);
?>

<div class="modal fade" id="customer-info-popup" tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Info Cliente</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-12 col-xl-3">

                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Código</span>
                          </div>
                          <input type="text" class="form-control text-right" id="info_customer_code" readonly>
                        </div>
                    </div>

                    <div class="col-12 col-xl-4">

                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Documento</span>
                          </div>
                          <input type="text" class="form-control text-right" id="info_customer_doc" readonly> 
                        </div>
                    </div>

                    <div class="col-12 col-xl-5">

                         <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Nombre</span>
                          </div>
                          <input type="text" class="form-control text-right" id="info_customer_name" readonly >
                        </div>
                    </div>

                    <div class="col-12 col-xl-4 mt-2">

                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Tel.</span>
                          </div>
                          <input type="text" class="form-control text-right" id="info_phone" readonly>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-12">

                        <legend style="margin-top: 15px;"><?= __('Conexión del Cliente') ?></legend>
                        <table class="table table-bordered table-hover" id="table-connections">
                            <thead>
                                <tr>
                                    <th><?= __('Controlador') ?></th>
                                    <th><?= __('Servicio') ?></th>
                                    <th><?= __('IP') ?></th>
                                    <th><?= __('Domicilio') ?></th>
                                    <th><?= __('Área') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="row mb-3" id="service-pending-block" >
                    <div class="col-xl-12">

                        <legend style="margin-top: 15px;"><?= __('Servicio Pendiente del Cliente') ?></legend>
                        <div class="row">
                            <div class="col-12 col-xl-5">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Servicio</span>
                                    </div>
                                    <input type="text" class="form-control text-right" id="info_service_name_service_pending" readonly>
                                </div>
                            </div>
                            <div class="col-12 col-xl-5">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Domicilio</span>
                                    </div>
                                    <input type="text" class="form-control text-right" id="info_address_service_pending" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row"  style="height: 350px;">
                    <div class="col-xl-12" style="height: 350px;">
                        <div id="map"  style="height: 350px;"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-user-assigned" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?= __('Asignar Usuario') ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-md-12">
                        <?= $this->Form->create(null,  [ 'url' => false,  'id' => 'form-user-assigned']) ?>
                            <fieldset>
                                <?php
                                    echo $this->Form->hidden('id', ['id' => 'ticket_id_user_assigment']);
                                    echo $this->Form->hidden('status', ['id' => 'ticket_status_user_assigment']);
                                    echo $this->Form->input('asigned_user_id', ['label' => 'Usuarios', 'options' => $users, 'required' => true]);
                                ?>
                            </fieldset>
                            <?= $this->Form->button(__('Asignar'), ['class' => 'primary']) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-add-comment" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?= __('Agregar Comentario') ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-xl-12">
                        <?= $this->Form->create(null,  [ 'url' => false,  'id' => 'form-add-comment']) ?>
                            <fieldset>
                                <div class="form-group text">
                                    <label class="control-label" for="description"> <?= __('Comentario') ?> </label>
                                    <textarea name="description" class="description" id="editor1" rows="6" cols="4" required>
                                    </textarea>
                                </div>
                                <?php
                                    echo $this->Form->hidden('ticket_id', ['id' => 'ticket_id_add_comment']);
                                ?>
                            </fieldset>
                            <?= $this->Form->button(__('Agregar'), ['class' => 'primary']) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-digital-signature" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?= __('Firmar') ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-xl-12">
                        <?= $this->Form->create(null,  ['url' => ['controller' => 'Tickets', 'action' => 'addDigitalSignature'], 'id' => 'signature-form', 'name' => 'signature-form']) ?>
                       
                            	<div id="signature"></div>
                       
                            
                            <div id="tools">
                                <input type="button" class="btn btn-dark btn-reset-signature mb-2" value="Limpiar">
                            </div>
                            <label class="control-label" for="description"> <?= __('Aclaración') ?> </label>
                            <textarea id="digital-signature-description" class="description" name="description" rows="4" cols="4" required>
                            </textarea>
                            <input type="hidden" name="hdnSignature" id="hdnSignature" />
                            <?php
                                echo $this->Form->hidden('ticket_id', ['id' => 'ticket-id-add-digital-signature']);
                                echo $this->Form->hidden('view', ['value' => 'card']);
                            ?>
                            <input type="button" class="btn btn-success btn-ok-signature" value="OK">
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-change-status" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?= __('Cambiar Estado') ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-xl-12">
                        <?= $this->Form->create(null,  [ 'url' => false,  'id' => 'form-change-status']) ?>
                            <fieldset>
                                <div class="input-group select">
                                    <label class="input-group-text" for="change-status-id">Estados</label>
                                    <select name="change_status_id" id="change-status-id" class="form-control" required>
                                        <option value="" selected>Seleccionar</option>
                                        <?php foreach ($tickets_status as $status): ?>
                                            <option value="<?= $status->id ?>"><?= $status->name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <?php
                                    echo $this->Form->hidden('ticket_id', ['id' => 'ticket_id_change_status']);
                                ?>
                            </fieldset>
                            <?= $this->Form->button(__('Cambiar'), ['class' => 'primary mt-3']) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-change-start-task" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?= __('Cambiar Inicio') ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-xl-12">
                        <?= $this->Form->create(null,  [ 'url' => false,  'id' => 'form-change-start-task']) ?>
                            <fieldset>
                                <div class='input-group date' id='start-task-datetimepicker'>
                                    <label class="input-group-text" for="start-task">Inicio</label>
                                    <input name='change_start_task' id="change-start-task-id" type='text' class="form-control" />
                                    <span class="input-group-addon input-group-text calendar">
                                        <span class="glyphicon icon-calendar"></span>
                                    </span>
                                </div>
                                <?php
                                    echo $this->Form->hidden('ticket_id', ['id' => 'ticket_id_change_start_task']);
                                ?>
                            </fieldset>
                            <?= $this->Form->button(__('Cambiar'), ['class' => 'primary mt-3']) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-photo" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?= __('Agregar foto') ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-xl-12">
                        <?= $this->Form->create(null, ['url' => ['controller' => 'Tickets' , 'action' => 'addPhoto'], 'enctype' => "multipart/form-data"]) ?>
                        <fieldset>
                            <?php
                                echo $this->Form->hidden('ticket_id', ['id' => 'ticket_id_photo']);
                                echo $this->Form->hidden('view', ['value' => 'card']);
                                echo $this->Form->file('file', ['required' => true, 'type'=>'file']);
                            ?>
                            <div class="input-group textarea mt-2">
                                <label class="control-label" for="description"> <?= __('Descripción') ?> </label>
                                <textarea class="description" name="description" rows="4" cols="4" required>
                                </textarea>
                            </div>
                        </fieldset>
                        <br>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Cancelar') ?></button>
                        <?= $this->Form->button(__('Agregar')) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-edit-description" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?= __('Editar Descripción') ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-xl-12">
                        <?= $this->Form->create(null,  [ 'url' => false,  'id' => 'form-edit-description']) ?>
                            <fieldset>
                                <div class="form-group text">
                                    <label class="control-label" for="description"> <?= __('Descripción') ?> </label>
                                    <textarea name="description" class="ticket_description" id="editor2" rows="6" cols="4" required>
                                    </textarea>
                                </div>
                                <?php
                                    echo $this->Form->hidden('ticket_id', ['id' => 'ticket_id_edit_description']);
                                ?>
                            </fieldset>
                            <?= $this->Form->button(__('Editar'), ['class' => 'primary']) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    echo $this->element('modal_preloader');
?>

<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2K3zoK46JybWPzQbhfQQqIIbdZ_3WcQs">
</script>

<!--[if lt IE 9]>
<script type="text/javascript" src="/ispbrain/vendor/jSignature/flashcanvas.js"></script>
<![endif]-->
<script src="/ispbrain/vendor/jSignature/src/jSignature.js"></script>
<script src="/ispbrain/vendor/jSignature/src/plugins/jSignature.CompressorBase30.js"></script>
<script src="/ispbrain/vendor/jSignature/src/plugins/jSignature.CompressorSVG.js"></script>
<script src="/ispbrain/vendor/jSignature/src/plugins/jSignature.UndoButton.js"></script>
<script src="/ispbrain/vendor/jSignature/src/plugins/signhere/jSignature.SignHere.js"></script>

<script type="text/javascript">

    var map = null;
    var geocoder = null;
    var marker = null;
    var markers = [];
    var infoWindows = [];

    var markerGroups = {
        "connections": [],
        "customers": [],
        "services_pending": []
    };

	function initMap(connections_coords, customers_coords, services_pending_coords) {

		geocoder = new google.maps.Geocoder;

		var myLatlng = {lat: sessionPHP.paraments.system.map.lat, lng: sessionPHP.paraments.system.map.lng};

		var mapOptions = {
			zoom: 12,
			center: myLatlng,
			mapTypeId: 'hybrid'   // roadmap, satellite, hybrid, terrain 
		};

		map = new google.maps.Map(document.getElementById('map'), mapOptions);

		$.each(connections_coords, function(i, val) {
		    if (val.lat != null && val.lng != null) {
		        var id = val.id;
		        var ip = val.ip;
		        var init = { lat: val.lat, lng: val.lng };
                var latLng = new google.maps.LatLng(init.lat, init.lng);
                var address = val.address;
                addMarker(id, ip, latLng, address);
		    }
		});

		$.each(customers_coords, function(i, val) {

		    if (val.lat != null && val.lng != null) {
		        var ident = "";
		        if (val.ident) {
		            ident = val.ident;
		        }
		        var customer = {
		            code: val.code,
		            name: val.name,
		            doc_type: val.doc_type,
		            ident: ident,
		            address: val.address
		        }

		        var init = { lat: val.lat, lng: val.lng };
                var latLng = new google.maps.LatLng(init.lat, init.lng);
                addCustomerMarker(latLng, customer);
		    }
		});

		$.each(services_pending_coords, function(i, val) {
		    if (val.lat != null && val.lng != null) {
		        var id = val.id;
		        var init = { lat: val.lat, lng: val.lng };
                var latLng = new google.maps.LatLng(init.lat, init.lng);
                var address = val.address;
                var service_name = val.service_name;
                addServicePendingMarker(id, latLng, address, service_name);
		    }
		});

		// a  div where we will place the buttons
        ctrl = $('<div/>').css({background:'#fff',
            border:'1px solid #000',
            padding:'4px',
            margin:'2px',
            textAlign:'center'
        });

        ctrl.append(
            '<ul style="text-align: left; position: relative; display: block; padding-left: 5px; padding-right: 5px; padding-top: 8px;">' +
                '<div class="form-check">' +
                    '<input onclick="toggleGroup(\'connections\')" type="checkbox" class="form-check-input" id="exampleCheck1" checked="checked">' +
                    '<label style="padding-left: 26px; padding-top: 4px; color: #0000ff !important" class="form-check-label" for="exampleCheck1"> Conexiones</label>' +
                '</div>' +
                '<div class="form-check">' +
                    '<input onclick="toggleGroup(\'customers\')" type="checkbox" class="form-check-input" id="exampleCheck2" checked="checked">' +
                    '<label style="padding-left: 26px; padding-top: 4px; color: #008000 !important" class="form-check-label" for="exampleCheck2"> Clientes</label>' +
                '</div>' +
                '<div class="form-check">' +
                    '<input onclick="toggleGroup(\'services_pending\')" type="checkbox" class="form-check-input" id="exampleCheck3" checked="checked">' +
                    '<label style="padding-left: 26px; padding-top: 4px; color: #ff0000 !important" class="form-check-label" for="exampleCheck3"> Servicio Pendiente</label>' +
                '</div>' +
            '</ul>'
        );

        //use the buttons-div as map-control 
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(ctrl[0]);
	}

	function addMarker(id, ip, location, address) {

	    var contentString = 
	    '<div id="content">' +
            '<div id="siteNotice">' +
            '</div>' +
            '<h1 id="firstHeading" class="firstHeading"><a target="_blank" href="/ispbrain/connections/view/' + id +' ">Conexión - IP: ' + ip + '</a></h1>'+
            '<div id="bodyContent">' +
                '<p>' +
                    '<b>Domicilio: </b>' + address + '<br>' +
                '</p>' +
            '</div>' +
        '</div>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });

        infoWindows.push(infowindow);

		marker = new google.maps.Marker({
			position: location,
			map: map,
		    animation: google.maps.Animation.DROP,
			title: '',
			icon: pinSymbol('blue'),
			type: 'connections'
		});
		marker.setMap(map);	
		markers.push(marker);

        if (!markerGroups['connections']) markerGroups['connections'] = [];
        markerGroups['connections'].push(marker);

        google.maps.event.addListener(marker,'click', (function(marker, contentString, infowindow) { 
            return function() {
                removeAnimationAllMarkers(marker);
                closeAllInfoWindows();
                infowindow.setContent(contentString);
                infowindow.open(map, marker);
            };
        })(marker, contentString, infowindow));

        google.maps.event.addListener(infowindow,'closeclick',function() {
            removeAnimationAllMarkers(null);
        });
	}

	function addCustomerMarker(location, customer) {

	    var contentString = 
	    '<div id="content">' +
            '<div id="siteNotice">' +
            '</div>' +
            '<h1 id="firstHeading" class="firstHeading"><a target="_blank" href="/ispbrain/customers/view/' +  customer.code + ' ">Cliente</a></h1>' +
            '<div id="bodyContent">' +
                '<p>' +
                    '<a target="_blank" href="/ispbrain/customers/view/' + customer.code + '"><b>Cliente: </b>' + customer.name + ' - <b>' + sessionPHP.afip_codes.doc_types[customer.doc_type] + ':</b> ' + customer.ident + ' - <b>Código:</b> ' + customer.code + '</a><br>' +
                    '<b>Domicilio: </b>' + customer.address + '<br>' +
                '</p>' +
            '</div>' +
        '</div>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });

        infoWindows.push(infowindow);

		marker = new google.maps.Marker({
			position: location,
			map: map,
		    animation: google.maps.Animation.DROP,
			title: '',
			icon: pinSymbol('green'),
			type: 'customers'
		});
		marker.setMap(map);
		markers.push(marker);

        if (!markerGroups['customers']) markerGroups['customers'] = [];
        markerGroups['customers'].push(marker);

        google.maps.event.addListener(marker,'click', (function(marker, contentString, infowindow) { 
            return function() {
                removeAnimationAllMarkers(marker);
                closeAllInfoWindows();
                infowindow.setContent(contentString);
                infowindow.open(map, marker);
            };
        })(marker, contentString, infowindow)); 

        google.maps.event.addListener(infowindow,'closeclick',function() {
            removeAnimationAllMarkers(null);
        });
	}

	function addServicePendingMarker(id, location, address, service_name) {

	    var contentString = 
	    '<div id="content">' +
            '<div id="siteNotice">' +
            '</div>' +
            '<h1 id="firstHeading" class="firstHeading"><a href="javascript:void(0)">Servicio Pendiente</a></h1>'+
            '<div id="bodyContent">' +
                '<p>' +
                    '<b>Servicio: </b>' + service_name + '<br>' +
                '</p>' +
                '<p>' +
                    '<b>Domicilio: </b>' + address + '<br>' +
                '</p>' +
            '</div>' +
        '</div>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });

        infoWindows.push(infowindow);

		marker = new google.maps.Marker({
			position: location,
			map: map,
		    animation: google.maps.Animation.DROP,
			title: '',
			icon: pinSymbol('red'),
			type: 'services_pending'
		});
		marker.setMap(map);	
		markers.push(marker);
		
		map.setCenter(location);

        if (!markerGroups['services_pending']) markerGroups['services_pending'] = [];
        markerGroups['services_pending'].push(marker);

        google.maps.event.addListener(marker,'click', (function(marker, contentString, infowindow) { 
            return function() {
                removeAnimationAllMarkers(marker);
                closeAllInfoWindows();
                infowindow.setContent(contentString);
                infowindow.open(map, marker);
            };
        })(marker, contentString, infowindow));

        google.maps.event.addListener(infowindow,'closeclick',function() {
            removeAnimationAllMarkers(null);
        });
	}

	function toggleGroup(type) {
        for (var i = 0; i < markerGroups[type].length; i++) {
            var marker = markerGroups[type][i];
            if (!marker.getVisible()) {
                marker.setVisible(true);
            } else {
                marker.setVisible(false);
            }
        }
    }

	function toggleBounce(marker) {
        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }

    function pinSymbol(color) {
        return {
            path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z',
            fillColor: color,
            fillOpacity: 1,
            strokeColor: '#000',
            strokeWeight: 2,
            scale: 1
        };
    }

    function closeAllInfoWindows() {
        for (var i = 0; i < infoWindows.length; i++) {
            infoWindows[i].close();
        }
    }

    function removeAnimationAllMarkers(marker) {
        for (var i = 0; i < markers.length; i++) {
            if (markers[i].type != 'controller') {
                 markers[i].setAnimation(null);
            }
        }
        if (marker != null && marker.type != 'controller') {
            toggleBounce(marker);
        }
    }

    $('#date-ticket-datetimepicker').datetimepicker({
        defaultDate: '',
        locale: 'es',
        format: 'DD/MM/YYYY'
    });

    $('#date-record-datetimepicker').datetimepicker({
        defaultDate: '',
        locale: 'es',
        format: 'DD/MM/YYYY'
    });

    var tickets_categories = <?php echo json_encode($tickets_categories); ?>;
    var tickets_status = <?php echo json_encode($tickets_status); ?>;;

    var available_status = 1;
    var assigned_status;
    var finished_status;

    var tickets = [];
    var ticket_selected = null;

    var status_selected = 0;
    var category_selected = -1;

    var editor = null;

    var table_connections = null;
    var table_tickets = null;

    function addCardTicket(ticket) {

        var container_tickets = $('#container-tickets').html();

        var template_card_ticket = $('#template-card-ticket').clone().html();

        template_card_ticket = template_card_ticket.replace('d-none', '');
        template_card_ticket = template_card_ticket.split('${ticket_id}').join( ticket.id );

        template_card_ticket = template_card_ticket.replace('${number}', ticket.id);

        template_card_ticket = template_card_ticket.replace('${status}', ticket.status.name.toUpperCase());

        template_card_ticket = template_card_ticket.replace('${user_asigned}', ticket.user_asigned ? ticket.user_asigned.username.toUpperCase() : '');

        template_card_ticket = template_card_ticket.replace('${category}', ticket.category.name.toUpperCase());

        template_card_ticket = template_card_ticket.replace('${category-color}', ticket.category.color_text.toUpperCase());
        template_card_ticket = template_card_ticket.replace('${category-color-back}', ticket.category.color_back.toUpperCase());

        template_card_ticket = template_card_ticket.replace('${customer_icon}', ticket.customer ? '<i class="fas fa-user"></i>' : '');
        template_card_ticket = template_card_ticket.replace('${customer_name}', ticket.customer ? ticket.customer.name : '');
        template_card_ticket = template_card_ticket.replace('${customer_code}', ticket.customer ? pad(ticket.customer.code, 5) : '');

        var start_task = ticket.start_task.split('T')[0];
        start_task = start_task.split('-');

        var start_task_2 = ticket.start_task.split('T')[1];
        start_task_2 = start_task_2.split('-')[0];
        start_task_2 = start_task_2.split(':');

        start_task = start_task[2] + '/' + start_task[1] + '/' + start_task[0] + ' ' + start_task_2['0'] + ':' + start_task_2[1];

        template_card_ticket = template_card_ticket.replace('${date_ticket}', start_task);
        template_card_ticket = template_card_ticket.replace('${description_ticket}', ticket.description);

        container_tickets += template_card_ticket;

        $('#container-tickets').html(container_tickets);

        $('#records-amount-' + ticket.id).html(ticket.tickets_records.length);

        addCardRecordsTicket(ticket);
    }

    function addCardRecordsTicket(ticket) {

        var container_records_tickets = '';

        $.each(ticket.tickets_records, function(i, record) {

            var template_card_records_ticket = $('#template-card-records-ticket').clone().html();

            template_card_records_ticket = template_card_records_ticket.replace('d-none', '');
            template_card_records_ticket = template_card_records_ticket.split('${record_id}').join( record.id );

            var created = record.created.split('T')[0];
            created = created.split('-');
            created = created[2] + '/' + created[1] + '/' + created[0];

            template_card_records_ticket = template_card_records_ticket.replace('${date_record}', created);

            var short_description = record.short_description ? record.short_description : '---';

            template_card_records_ticket = template_card_records_ticket.replace('${short_description_record}', short_description);

            template_card_records_ticket = template_card_records_ticket.replace('${username}', record.user.name);
            template_card_records_ticket = template_card_records_ticket.replace('${description_record}', record.description);
            if (record.image) {
                template_card_records_ticket = template_card_records_ticket.replace('my-hidden', '');
                
                var img = '';
                img += '<figure class="figure">';
                // img += '    <figcaption class="figure-caption">record.description</figcaption>';
                img += '    <img src="/ispbrain/ticket_image/' + record.image + '" class="figure-img img-fluid rounded" alt="A generic square placeholder image with rounded corners in a figure.">';
                img += '</figure>';
                
                
                
                // var img = '<img id="img-signature" src="/ispbrain/ticket_image/' + record.image + '" class="img-fluid" alt="Imagen">';
                template_card_records_ticket = template_card_records_ticket.replace('${digital_signature_record}', img);
            } else {
                template_card_records_ticket = template_card_records_ticket.replace('${digital_signature_record}', '');
            }

            container_records_tickets += template_card_records_ticket;
        });

        $('#container-tickets #ticket-' + ticket.id + ' #records-container-' + ticket.id).html(container_records_tickets);
    }

    function loadTickets(status, category) {

        status_selected = status;
        category_selected = category ? category : -1;

        $('#container-tickets').html('');

        tickets = [];

        // openModalPreloader("Actualizando lista ...");

        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '/ispbrain/tickets/getTickets',
            data: JSON.stringify({status: status, category: category_selected}),
            success: function(data) {

                if (data.tickets.length > 0) {
                    tickets = data.tickets;

                    $.each(tickets, function(i, ticket) {
                        addCardTicket(ticket);
                    });

                } else {
                    $('#container-tickets').append( "<p style='margin-left: 20px; font-size: 25px; text-shadow: 1px 1px 1px #b4b0b0; font-family: sans-serif;'>No se han encontrado tickets</p>" );
                }
            },
            error: function(jqXHR) {
                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() { 
                	  window.location.href ="/ispbrain";
                	}, 3000);

                } else {
                	generateNoty('error', 'Error al obtener los tickets.');
                }
            }
        });
    }

    function setTicketSelect(id) {
        $.each(tickets, function(i, ticket) {
            if (ticket.id == id) {
                ticket_selected = ticket;
            }
        });
    }

    $('.btn-views').click(function() {
        $('#btn-view-card').addClass('active');
        loadTickets(status_selected, category_selected);
    });

    $(document).ready(function() {

        editor = CKEDITOR.replace( 'editor1' );
        editor2 = CKEDITOR.replace( 'editor2' );

        CKEDITOR.config.title = false;
        CKEDITOR.config.toolbar = [
        	{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo' ] },
        	{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ] },
        	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent' ] },
        	{ name: 'styles', items: [ 'Format' ] },
        ];

        $('#start-task-datetimepicker').datetimepicker({
            locale: 'es',
            format: 'DD/MM/YYYY HH:mm'
        });

        loadTickets(available_status);

        $(document).on('click', '.btn-show-history', function() {

            var selector = '#records-container-' + $(this).data('target');

            if ($(selector).hasClass('d-none')) {
                $(selector).removeClass('d-none');
            } else {
                $(selector).addClass('d-none');
            }
        });

        $(document).on('click', '.card-ticket .btn-open-action',  function() {

            setTicketSelect($(this).data('target'));

            $('#btn-finished').show();
            $('#btn-opened').hide();

            $('.lbl-ticket-title').html('#' + ticket_selected.id);

            if (ticket_selected.asigned_user_id != null) {
                $('.lbl-user-assigned-name').text('Reasignar Usuario');
            } else {
                $('.lbl-user-assigned-name').text('Asignar Usuario');
            }

    		if (ticket_selected.customer) {
    		    $('#btn-info-customer').show();
    		} else {
    		    $('#btn-info-customer').hide();
    		}

            $('.modal-actions-tickets').modal('show');
        });

        $('#table-connections').removeClass('display');

	    table_connections = $('#table-connections').DataTable({
		    "order": [[ 0, 'desc' ]],
            "deferRender": true,
            "bFilter": false,
	        "ajax": {
                "url": "/ispbrain/connections/get_connection_customer.json",
                "dataSrc": "connections",
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "customer_code": ticket_selected && ticket_selected.customer ? ticket_selected.customer.code : 0,
                        "deleted": 1,
                    });
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
            				  window.location.href ="/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
		    "autoWidth": true,
		    "scrollX": true,
		    "paging": false,
		    "columns": [
                {
                    "data": "controller.name",
                },
                {
                    "data": "service.name"
                },
                {
                    "data": "ip",
                },
                {
                    "data": "address"
                },
                {
                    "data": "area.name"
                },
            ],
		    "columnDefs": [
                { 
                    "class": "left", targets: [1, 3]
                },
            ],
             "createdRow" : function( row, data, index ) {
                row.id = data.id;
                if (data.id == ticket_selected.connection_id) {
                    $(row).addClass('selected');
                }
            },
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
		});

        $('.btn-filter').click(function() {

            $('.btn-filter').addClass('disabled');

            var target = $(this).data('target');

            $(this).removeClass('disabled');

            loadTickets(target);
        })

        $('#btn-open-search-ticket').click(function() {
            $('#modal-search-ticket').modal('show');
        });

        // $('#btn-info-customer').click(function() {

        //     if (ticket_selected.customer) {

        //         var doc_type_name = sessionPHP.afip_codes.doc_types[ticket_selected.customer.doc_type];

        //         $('#customer-info-popup #info_customer_code').val(pad(ticket_selected.customer.code, 5))
        //         $('#customer-info-popup #info_customer_name').val(ticket_selected.customer.name.toUpperCase());
        //         $('#customer-info-popup #info_customer_doc').val(doc_type_name + ' '+ ticket_selected.customer.ident);
        //         $('#customer-info-popup #info_phone').val(ticket_selected.customer.phone);


        //         //setear valores en caso que exista servicio pendiente del cliente
        //         if (ticket_selected.service_pending) {
        //             $('#service-pending-block').removeClass('my-hidden');
        //             $('#customer-info-popup #info_address_service_pending').val(ticket_selected.service_pending.address);
        //             $('#customer-info-popup #info_service_name_service_pending').val(ticket_selected.service_pending.service_name);
        //         } else {
        //             $('#customer-info-popup #info_address_service_pending').val("");
        //             $('#customer-info-popup #info_service_name_service_pending').val("");
        //             $('#service-pending-block').addClass('my-hidden');
        //         }

        //         $('.modal-actions-tickets').modal('hide');
        //         var customers_coords = [];
        //         var customer_coords = {
        //             code: ticket_selected.customer.code,
		      //      name: ticket_selected.customer.name,
		      //      doc_type: ticket_selected.customer.doc_type,
		      //      ident: ticket_selected.customer.ident,
		      //      lat: ticket_selected.customer.lat,
		      //      lng: ticket_selected.customer.lng,
		      //      address: ticket_selected.customer.address
        //         };
        //         customers_coords.push(customer_coords);
        //         var connections_coords = [];
        //         $.each(ticket_selected.customer.connections, function(i, connection) {
        //             var connection_coords = {
        //                 id: connection.id,
    		  //          lat: connection.lat,
    		  //          lng: connection.lng,
    		  //          ip: connection.ip,
    		  //          address: connection.address
        //             };
        //             connections_coords.push(connection_coords);
        //         });

        //         //coordenadas servicio pendiente

        //         var services_pending_coords = [];
        //         if (ticket_selected.service_pending_id) {

        //             var service_pending_coords = {
    		  //          lat: ticket_selected.service_pending.lat,
    		  //          lng: ticket_selected.service_pending.lng,
    		  //          address: ticket_selected.service_pending.address,
    		  //          service_name: ticket_selected.service_pending.service_name
        //             };
        //             services_pending_coords.push(service_pending_coords);
        //         }

        //         initMap(connections_coords, customers_coords, services_pending_coords);
        //         $('#customer-info-popup').modal('show');
        //     }

        // });
        
        $('#btn-info-customer').click(function() {

            if (ticket_selected.customer) {

                var customers_coords = [];
                var doc_type_name = sessionPHP.afip_codes.doc_types[ticket_selected.customer.doc_type];
                $('#customer-info-popup #info_customer_code').val(pad(ticket_selected.customer.code, 5))
                $('#customer-info-popup #info_customer_name').val(ticket_selected.customer.name.toUpperCase());
                var ident = "";
                if (ticket_selected.customer.ident) {
                    ident = ticket_selected.customer.ident;
                }
                $('#customer-info-popup #info_customer_doc').val(doc_type_name + ' ' + ident);
                $('#customer-info-popup #info_phone').val(ticket_selected.customer.phone);

                var customer_coords = {
                    code: ticket_selected.customer.code,
		            name: ticket_selected.customer.name,
		            doc_type: ticket_selected.customer.doc_type,
		            ident: ident,
		            lat: ticket_selected.customer.lat,
		            lng: ticket_selected.customer.lng,
		            address: ticket_selected.customer.address
                };
                customers_coords.push(customer_coords);

                var connections_coords = [];
                if (ticket_selected.connection) {
                    
                    var connection_coords = {
                        id: ticket_selected.connection.id,
    		            lat: ticket_selected.connection.lat,
    		            lng: ticket_selected.connection.lng,
    		            ip: ticket_selected.connection.ip,
    		            address: ticket_selected.connection.address
                    };
                    connections_coords.push(connection_coords);
                }

                var services_pending_coords = [];
                if (ticket_selected.service_pending) {
                    
                    $('#service-pending-block').removeClass('my-hidden');
                    $('#customer-info-popup #info_address_service_pending').val(ticket_selected.service_pending.address);
                    $('#customer-info-popup #info_service_name_service_pending').val(ticket_selected.service_pending.service_name);

                    var service_pending_coords = {
    		            lat: ticket_selected.service_pending.lat,
    		            lng: ticket_selected.service_pending.lng,
    		            address: ticket_selected.service_pending.address,
    		            service_name: ticket_selected.service_pending.service_name
                    };
                    services_pending_coords.push(service_pending_coords);
                } else{
                    $('#customer-info-popup #info_address_service_pending').val("");
                    $('#customer-info-popup #info_service_name_service_pending').val("");
                    $('#service-pending-block').addClass('my-hidden');
                }

                initMap(connections_coords, customers_coords, services_pending_coords);

                $('.modal-actions-tickets').modal('hide');
                $('#customer-info-popup').modal('show');
            }

        });

        $('#customer-info-popup').on('shown.bs.modal', function (e) {
          table_connections.draw();
        });

        $('#form-search-ticket').submit(function() {

            $('#modal-search-ticket').modal('hide');

            event.preventDefault();

            var filter_data = {};  
            var form_data = $(this).serializeArray();
            $.each(form_data, function(i, val) {
                filter_data[val.name] = val.value;
            });

            $.each(tickets, function(i, ticket) {

                $('#ticket-' + ticket.id).removeClass('d-none');

                var flag = true;

                //find in ticket

                if (filter_data.number_ticket.length > 0 && ticket.id != filter_data.number_ticket) {
                    flag = false;
                }

                if (filter_data.date_ticket.length > 0) {

                    var created = ticket.created.split('T')[0];
                    created = created.split('-');
                    created = created[2] + '/' + created[1] + '/' + created[0];

                    if (created != filter_data.date_ticket) {
                        flag = false;
                    }
                }

                if (filter_data.category_id.length > 0 && ticket.category.id != filter_data.category_id) {
                    flag = false;
                }

                if (filter_data.description_ticket.length > 0 && !ticket.description.toUpperCase().includes(filter_data.description_ticket.toUpperCase())) {
                    flag = false;
                }

                if (filter_data.user_id_ticket.length > 0 && ticket.user_id != filter_data.user_id_ticket) {
                    flag = false;
                }

                if (ticket.user_asigned) {
                    if (filter_data.asigned_user_id_ticket.length > 0 && (ticket.user_asigned.id != filter_data.asigned_user_id_ticket)) {
                        flag = false;
                    }
                }

                if (filter_data.asigned_user_id_ticket.length > 0 && ticket.user_asigned == null && filter_data.asigned_user_id_ticket != 0) {
                    flag = false;
                }

                //find in customer

                if (filter_data.customer_code.length > 0) {

                    if (ticket.customer) {

                         if (ticket.customer.code != filter_data.customer_code) {
                            flag = false;
                        }
                    } else {
                        flag = false;
                    }
                }

                if (filter_data.customer_ident.length > 0) {

                    if (ticket.customer) {

                        if (ticket.customer.ident != filter_data.customer_ident) {
                            flag = false;
                        }
                    } else {
                        flag = false;
                    }
                }

                if (filter_data.customer_name.length > 0) {

                    if (ticket.customer) {

                        if (!ticket.customer.name.toUpperCase().includes(filter_data.customer_name.toUpperCase())) {
                            flag = false;
                        }
                    } else {
                        flag = false;
                    }
                }

                //find in records

                if (filter_data.date_record.length > 0) {

                    if (ticket.tickets_records.length > 0) {

                        flag = false;

                        $.each(ticket.tickets_records, function(i, record) {

                            var created = record.created.split('T')[0];
                            created = created.split('-');
                            created = created[2] + '/' + created[1] + '/' + created[0];

                            if (created == filter_data.date_record) {
                                flag = true;
                            }
                        });

                    } else {
                        flag = false;
                    }
                }

                if (filter_data.description_record.length > 0) {

                    if (ticket.tickets_records.length > 0) {

                        flag = false;

                        $.each(ticket.tickets_records, function(i, record) {

                            if (record.description.toUpperCase().includes(filter_data.description_record.toUpperCase())) {
                                flag = true;
                            }
                        });
                    } else {
                        flag = false;
                    }
                }

                if (filter_data.user_id_record.length > 0) {

                    if (ticket.tickets_records.length > 0) {

                        flag = false;

                        $.each(ticket.tickets_records, function(i, record) {

                            if (record.user_id == filter_data.user_id_record) {
                                flag = true;
                            }
                        });
                    } else {
                        flag = false;
                    }
                }

                //find in connections 

                if (filter_data.connection_controller.length > 0) {

                    if (ticket.connection) {

                        if (ticket.connection.controller_id != filter_data.connection_controller) {
                            flag = false;
                        }
                    } else {
                        flag = false;
                    }
                }

                if (filter_data.connection_service.length > 0) {

                    if (ticket.connection) {

                        if (ticket.connection.service_id != filter_data.connection_service) {
                            flag = false;
                        }
                    } else {
                        flag = false;
                    }
                }

                if (filter_data.connection_ip.length > 0) {

                    if (ticket.connection) {

                        if (ticket.connection.ip != filter_data.connection_ip) {
                            flag = false;
                        }
                    } else {
                         flag = false;
                    }
                }

                if (filter_data.connection_address.length > 0) {

                    if (ticket.connection) {

                        if (!ticket.connection.address.toUpperCase().includes(filter_data.connection_address.toUpperCase())) {
                            flag = false;
                        }
                    } else {
                        flag = false;
                    }
                }

                if (!flag) {
                    $('#ticket-' + ticket.id).addClass('d-none');
                }
            });
        });

        $('#btn-assigned').click(function() {

            $('.modal-actions-tickets').modal('hide');

		    $('#ticket_id_user_assigment').val(ticket_selected.id);

		    $('#modal-user-assigned').modal('show');
		});

		$('#form-user-assigned').submit(function() {

		    $('#modal-user-assigned').modal('hide');

		    event.preventDefault();

            var send_data = {};  
            var form_data = $(this).serializeArray();
            $.each(form_data, function(i, val) {
                send_data[val.name] = val.value;
            });

            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '/ispbrain/tickets/userAssignment',
                data:  JSON.stringify({data: send_data}),
                success: function(data) {

                    generateNoty(data.response.typeMsg, data.response.msg);

                    loadTickets(status_selected);
                },
                error: function(jqXHR) {
                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                    	  window.location.href ="/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al asignar usuario al Ticket.');
                    }
                }
            });
		});

		$('#btn-add-comment').click(function() {

		    $('.modal-actions-tickets').modal('hide');

		    $('#ticket_id_add_comment').val(ticket_selected.id);

		    editor.setData('');

		    $('#modal-add-comment').modal('show');
		});

		$('#form-add-comment').submit(function() {

		    $('#modal-add-comment').modal('hide');

		    event.preventDefault();

            var send_data = {};  
            var form_data = $(this).serializeArray();
            $.each(form_data, function(i, val) {
                send_data[val.name] = val.value;
            });

            send_data.description = editor.getData();

            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '/ispbrain/tickets/addComment',
                data:  JSON.stringify({data: send_data}),
                success: function(data) {

                    generateNoty(data.response.typeMsg, data.response.msg);

                    loadTickets(status_selected);
                },
                error: function(jqXHR) {
                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                    	  window.location.href ="/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al agregar comentario al Ticket.');
                    }
                }
            });
		});

		$('#btn-edit-description').click(function() {

		    $('.modal-actions-tickets').modal('hide');

            editor2.setData(ticket_selected.description);
		    $('#ticket_id_edit_description').val(ticket_selected.id);

		    $('#modal-edit-description').modal('show');
		});

		$('#form-edit-description').submit(function() {

		    $('#modal-edit-description').modal('hide');

		    openModalPreloader("Editando descripción ...");

		    event.preventDefault();

            var send_data = {};  
            var form_data = $(this).serializeArray();
            $.each(form_data, function(i, val) {
                send_data[val.name] = val.value;
            });

            send_data.description = editor2.getData();

            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '/ispbrain/tickets/editDescription',
                data:  JSON.stringify({data: send_data}),
                success: function(data) {

                    closeModalPreloader();
                    generateNoty(data.response.typeMsg, data.response.msg);

                    loadTickets(status_selected);
                },
                error: function(jqXHR) {
                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                    	  window.location.href ="/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al editar la descripción del Ticket.');
                    }
                }
            });
		});

		$('#btn-change-status').click(function() {

		    $('.modal-actions-tickets').modal('hide');

		    $('#ticket_id_change_status').val(ticket_selected.id);

		    editor.setData('');

		    $('#modal-change-status').modal('show');
		});

		$('#form-change-status').submit(function() {

		    $('#modal-change-status').modal('hide');

		    event.preventDefault();

            var send_data = {};  
            var form_data = $(this).serializeArray();
            $.each(form_data, function(i, val) {
                send_data[val.name] = val.value;
            });

            send_data.description = editor.getData();
            send_data.status_name = $('#change-status-id :selected').text();

            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '/ispbrain/tickets/changeStatus',
                data:  JSON.stringify({data: send_data}),
                success: function(data) {

                    generateNoty(data.response.typeMsg, data.response.msg);

                    loadTickets(status_selected);
                },
                error: function(jqXHR) {
                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                    	  window.location.href ="/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al acmbiar el estado del Ticket.');
                    }
                }
            });
		});

		$('#btn-opened').click(function() {

		    $('.modal-actions-tickets').modal('hide');

		    updateStatusTicket(available_status);
		});

		$('#btn-finished').click(function() {

		    $('.modal-actions-tickets').modal('hide');

		    updateStatusTicket(finished_status);
		});

		$('#btn-tickets-export-pdf').click(function() {
		    if (tickets.length > 0) {
		        var action = "/ispbrain/Tickets/ticketExportPDF/";
                $('body')
                  .append( $('<form/>').attr({'action': action, 'target': '_blank', 'method': 'post', 'id': 'replacer_pdf'})
                  .append( $('<input/>').attr( {'type': 'hidden', 'name': 'tickets', 'value': JSON.stringify(tickets)}))
                  .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                  .find('#replacer_pdf').submit();
		    } else {
		        generateNoty('warning', 'Debe generar una búsqueda de Tickets para poder exportar los mismos');
		    }
		});

		$('#btn-digital-signature').click(function() {
		    $('#modal-digital-signature').modal('show');
		});

		$('#modal-digital-signature').on('show.bs.modal', function (e) {
            // This is the part where jSignature is initialized.
        	var $sigdiv = $("#signature").jSignature({'UndoButton':false, height: 200, width: '100%', })

        	// All the code below is just code driving the demo. 
        	, $tools = $('#tools');
            $('.btn-reset-signature').click(function() {
                $sigdiv.jSignature('reset');
            });

            $('#ticket-id-add-digital-signature').val(ticket_selected.id);
            $('#digital-signature-description').val("");

            $('.btn-ok-signature').click(function() {

                var text = 'Confirmar para guardar firma';

                bootbox.confirm(text, function(result) {
                    if (result) {
                        var datapair = $sigdiv.jSignature("getData", "image");
                        $('#hdnSignature').val(datapair[1]);
                        //now submit form 
                        $('#signature-form').submit();
                    }
                });
            });

            $('.btn-ok-signature').hide();
            $('.btn-reset-signature').hide();

            $("#signature").bind(
                "change", function(event) {

                	$('#signature').find('img').hide();
                	$('#signature img').hide();

                    // 'event.target' will refer to DOM element with id "#signature"
                    var d = $(event.target).jSignature("getData", "native")
                    // if there are more than 2 strokes in the signature
                    // or if there is just one stroke, but it has more than 20 points
                    if ( d.length > 0 || ( d.length === 1) ) {
                        // we show "Submit" button
                        // $(event.target).unbind('change')
                        $(".btn-ok-signature").show();
                        $(".btn-reset-signature").show();
                    } else {
                        $(".btn-reset-signature").hide();
                        $(".btn-ok-signature").hide();
                    }
                }
            );

            $("#signature").resize();
        });

        $(".filter-ticket").change(function() {

            status_selected = $('#status-id :selected').val();
            category_selected = $('#category-id :selected').val();

            loadTickets(status_selected, category_selected);
        });

        $('#btn-calendar').click(function() {
            if (tickets.length > 0) {
		        var action = "/ispbrain/Tickets/calendar/";
                $('body')
                  .append( $('<form/>').attr({'action': action, 'method': 'post', 'id': 'replacer_calendar'})
                  .append( $('<input/>').attr( {'type': 'hidden', 'name': 'tickets', 'value': JSON.stringify(tickets)}))
                  .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                  .find('#replacer_calendar').submit();
		    } else {
		        generateNoty('warning', 'Debe generar una búsqueda de Tickets para poder visualizar los mismos en el calendario.');
		    }
        });

        $('#btn-change-start-task').click(function() {

            $('.modal-actions-tickets').modal('hide');

            $('#ticket_id_change_start_task').val(ticket_selected.id);

            var start_task = ticket_selected.start_task.split('T')[0];
            start_task = start_task.split('-');

            var start_task_2 = ticket_selected.start_task.split('T')[1];
            start_task_2 = start_task_2.split('-')[0];
            start_task_2 = start_task_2.split(':');

            start_task = start_task[2] + '/' + start_task[1] + '/' + start_task[0] + ' ' + start_task_2[0] + ':' + start_task_2[1];

            $('#start-task-datetimepicker').data('DateTimePicker').date(start_task);

            $('#modal-change-start-task').modal('show');
        });

        $('#form-change-start-task').submit(function() {

		    $('#modal-change-start-task').modal('hide');

		    event.preventDefault();

            var send_data = {};  
            var form_data = $(this).serializeArray();
            $.each(form_data, function(i, val) {
                send_data[val.name] = val.value;
            });

            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '/ispbrain/tickets/changeStartTask',
                data:  JSON.stringify({data: send_data}),
                success: function(data) {

                    generateNoty(data.response.typeMsg, data.response.msg);

                    loadTickets(status_selected);
                },
                error: function(jqXHR) {
                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                    	  window.location.href ="/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al cambaiar la fecha del Ticket.');
                    }
                }
            });
		});

		$('#btn-photo').click(function() {
            $('.modal-actions-tickets').modal('hide');
            $('#ticket_id_photo').val(ticket_selected.id);
            $('#modal-photo').modal('show');
        });
    });

    function updateStatusTicket(status) {

	    event.preventDefault();

        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '/ispbrain/tickets/updateStatusTicket',
            data:  JSON.stringify({id: ticket_selected.id, status: status}),
            success: function(data) {

                generateNoty(data.response.typeMsg, data.response.msg);

                loadTickets(status_selected);
            },
            error: function(jqXHR) {
                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() { 
                	  window.location.href ="/ispbrain";
                	}, 3000);

                } else {
                	generateNoty('error', 'Error al al actualizar el estado del Ticket.');
                }
            }
        });
	}

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
