<?php $this->extend('/Tickets/table/closed'); ?>

<?php $this->start('actives'); ?>

<script type="text/javascript">

    function opemModalImage(path) {
        $('#modal-photo-view #foto-view-img').attr('src', path );
        $('#modal-photo-view #foto-view-source').attr('srcset', path );
        $('#modal-photo-view').modal('show');
    }

</script>

<div class="row" id="container-tickets">
    <div class="col-xl-12 col-md-12 col-sm-12">
        <table class="table table-hover table-bordered" id="table-tickets-actives">
           <thead>
               <tr>
                    <th></th>              <!--0-->
                    <th>#</th>             <!--1-->
                    <th>Creado</th>        <!--2-->
                    <th>Modificado</th>    <!--3-->
                    <th>Usuario</th>       <!--4-->
                    <th>Estado</th>        <!--5-->
                    <th>Categoría</th>     <!--6-->
                    <th>Cód.</th>          <!--7-->
                    <th>Cliente</th>       <!--8-->
                    <th>Dom. cliente</th>  <!--9-->
                    <th>Inicio</th>        <!--10-->
                    <th>Asignado</th>      <!--11-->
                    <th>Título</th>        <!--12-->
                    <th>Área</th>          <!--13-->
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<div id="btns-tools-actives">
    <div class="text-right btns-tools margin-bottom-5">

        <?php
            echo $this->Html->link(
                '<span class="fas fa-sync-alt" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Actualizar tabla',
                'class' => 'btn btn-default btn-update-table ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon fas fa-search" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Buscar por columnas',
                'class' => 'btn btn-default btn-search-column ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                ['controller' => 'Tickets', 'action' => 'add'],
                [
                'title' => 'Agregar nuevo ticket',
                'class' => 'btn btn-default btn-add ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="far fa-calendar-alt" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                    'title' => 'Ver Calendario',
                    'class' => 'btn btn-default btn-calendar ml-1',
                    'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="fas fa-file-invoice aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Seleccionar y cambiar estado',
                'class' => 'btn btn-default btn-change-status-from-active ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export-actives ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-pdf" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar PDF',
                'class' => 'btn btn-default btn-export-pdf ml-1',
                'escape' => false
            ]);
        ?>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-add-customer',
        'name' =>  'Agregar Cliente',
        'icon' =>  'fas fa-user',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-assigned',
        'name' =>  '<span class="lbl-user-assigned-name">Asignar usuario</span>',
        'icon' =>  'icon-user-tie  mr-2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-change-category-actives',
        'name' =>  'Cambiar Categoría',
        'icon' =>  'fas fa-exchange-alt mr-2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-change-status-actives',
        'name' =>  'Cambiar Estado',
        'icon' =>  'fas fa-exchange-alt mr-2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-change-areas-actives',
        'name' =>  'Cambiar Área',
        'icon' =>  'fas fa-exchange-alt mr-2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-change-start-task',
        'name' =>  'Cambiar Inicio',
        'icon' =>  'far fa-calendar-alt mr-2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-add-comment',
        'name' =>  'Agregar Comentario',
        'icon' =>  'icon-pencil2 mr-2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-photo',
        'name' =>  'Agregar Foto',
        'icon' =>  'fas fa-camera mr-2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-digital-signature',
        'name' =>  'Firma',
        'icon' =>  'fas fa-edit mr-2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-edit-title',
        'name' =>  'Editar Título',
        'icon' =>  'icon-pencil2 mr-2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-edit-description',
        'name' =>  'Editar Descripción',
        'icon' =>  'icon-pencil2 mr-2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-go-ip',
        'name' =>  'Abrir Antena',
        'icon' =>  'fas fa-broadcast-tower',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-view-realTimeGraph',
        'name' =>  'Gráfica en tiempo real',
        'icon' =>  'fas fa-chart-area',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-view-speed',
        'name' =>  'Gráfica historial',
        'icon' =>  'glyphicon icon-plus',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-print-pdf',
        'name' =>  'Imprimir PDF',
        'icon' =>  'icon-file-pdf mr-2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-info-customer-actives',
        'name' =>  'Info Cliente',
        'icon' =>  'icon-profile mr-2',
        'type' =>  'btn-info'
    ];

    $buttons[] = [
        'id'   =>  'btn-archive-actives',
        'name' =>  'Archivar',
        'icon' =>  'fa fa-archive',
        'type' =>  'btn-warning'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete-actives',
        'name' =>  'Eliminar',
        'icon' =>  'fa fa-times mr-2',
        'type' =>  'btn-danger'
    ];

    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-tickets-actives']);
    echo $this->element('actions_tickets', ['modal'=> 'modal-actions-tickets-actives', 'title' => '<span class="lbl-ticket-title"></span>', 'buttons' => $buttons]);
?>

<div class="modal fade" id="modal-customer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-12">
                            <button type="button" class="close" aria-label="Close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <?= $this->element('customer_list_seeker') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="modal-user-assigned" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?= __('Asignar Usuario') ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-md-12">
                        <?= $this->Form->create(null,  [ 'url' => false,  'id' => 'form-user-assigned']) ?>
                            <fieldset>
                                <?php
                                    echo $this->Form->hidden('id', ['id' => 'ticket_id_user_assigment']);
                                    echo $this->Form->hidden('status', ['id' => 'ticket_status_user_assigment']);
                                    echo $this->Form->input('asigned_user_id', ['label' => 'Usuarios', 'options' => $users, 'required' => true]);
                                ?>
                            </fieldset>
                            <?= $this->Form->button(__('Asignar'), ['class' => 'primary']) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-add-comment" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?= __('Agregar Comentario') ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-xl-12">
                        <?= $this->Form->create(null,  [ 'url' => false,  'id' => 'form-add-comment']) ?>
                            <fieldset>
                                <div class="form-group text">
                                    <label class="control-label" for="description"> <?= __('Comentario') ?> </label>
                                    <textarea name="description" class="description" id="editor1" rows="6" cols="4" required>
                                    </textarea>
                                </div>
                                <?php
                                    echo $this->Form->hidden('ticket_id', ['id' => 'ticket_id_add_comment']);
                                ?>
                            </fieldset>
                            <?= $this->Form->button(__('Agregar'), ['class' => 'primary']) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-digital-signature" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?= __('Firmar') ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-xl-12">
                        <?= $this->Form->create(null,  ['url' => ['controller' => 'Tickets', 'action' => 'addDigitalSignature'], 'id' => 'signature-form', 'name' => 'signature-form']) ?>
                            <div id="signature"></div>
                            <div id="tools">
                                <input type="button" class="btn btn-dark btn-reset-signature mb-2" value="Limpiar">
                            </div>
                            <label class="control-label" for="description"> <?= __('Aclaración') ?> </label>
                            <textarea id="digital-signature-description" class="description" name="description" rows="4" cols="4" required>
                            </textarea>
                            <input type="hidden" name="hdnSignature" id="hdnSignature" />
                            <?php
                                echo $this->Form->hidden('ticket_id', ['id' => 'ticket-id-add-digital-signature']);
                                echo $this->Form->hidden('view', ['value' => 'table']);
                            ?>
                            <input type="button" class="btn btn-success btn-ok-signature" value="OK">
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-change-category-actives" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?= __('Cambiar Categoría') ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-xl-12">
                        <?= $this->Form->create(null,  [ 'url' => false,  'id' => 'form-change-category-actives']) ?>
                            <fieldset>
                                <div class="input-group select">
                                    <label class="input-group-text" for="change-category-id-actives">Estados</label>
                                    <select name="change_category_id" id="change-category-id-actives" class="form-control" required>
                                        <option value="" selected>Seleccionar</option>
                                        <?php foreach ($tickets_categories as $category): ?>
                                            <option value="<?= $category->id ?>"><?= $category->name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <?php
                                    echo $this->Form->hidden('ticket_id', ['id' => 'ticket_id_change_category_actives']);
                                ?>
                            </fieldset>
                            <?= $this->Form->button(__('Cambiar'), ['class' => 'primary mt-3']) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-change-status-actives" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?= __('Cambiar Estado') ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-xl-12">
                        <?= $this->Form->create(null,  [ 'url' => false,  'id' => 'form-change-status-actives']) ?>
                            <fieldset>
                                <div class="input-group select">
                                    <label class="input-group-text" for="change-status-id-actives">Estados</label>
                                    <select name="change_status_id" id="change-status-id-actives" class="form-control" required>
                                        <option value="" selected>Seleccionar</option>
                                        <?php foreach ($tickets_status as $status): ?>
                                            <option value="<?= $status->id ?>"><?= $status->name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <?php
                                    echo $this->Form->hidden('ticket_id', ['id' => 'ticket_id_change_status_actives']);
                                ?>
                            </fieldset>
                            <?= $this->Form->button(__('Cambiar'), ['class' => 'primary mt-3']) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-change-start-task" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?= __('Cambiar Inicio') ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-xl-12">
                        <?= $this->Form->create(null,  [ 'url' => false,  'id' => 'form-change-start-task']) ?>
                            <fieldset>
                                <div class='input-group date' id='start-task-datetimepicker'>
                                    <label class="input-group-text" for="start-task">Inicio</label>
                                    <input name='change_start_task' id="change-start-task-id" type='text' class="form-control" />
                                    <span class="input-group-addon input-group-text calendar">
                                        <span class="glyphicon icon-calendar"></span>
                                    </span>
                                </div>
                                <?php
                                    echo $this->Form->hidden('ticket_id', ['id' => 'ticket_id_change_start_task']);
                                ?>
                            </fieldset>
                            <?= $this->Form->button(__('Cambiar'), ['class' => 'primary mt-3']) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-photo" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?= __('Agregar foto') ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-xl-12">
                        <?= $this->Form->create(NULL, ['url' => ['controller' => 'Tickets' , 'action' => 'addPhoto'], 'enctype' => "multipart/form-data"]) ?>
                        <fieldset>
                            <?php
                                echo $this->Form->hidden('ticket_id', ['id' => 'ticket_id_photo']);
                                echo $this->Form->hidden('view', ['value' => 'table']);
                                echo $this->Form->file('files[]', ['required' => TRUE, 'multiple' => 'multiple']);
                            ?>
                            <div class="input-group textarea mt-2">
                                <label class="control-label" for="description"> <?= __('Descripción') ?> </label>
                                <textarea class="description" name="description" rows="4" cols="4" id="description_photo" required>
                                </textarea>
                            </div>
                        </fieldset>
                        <br>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Cancelar') ?></button>
                        <?= $this->Form->button(__('Agregar')) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-photo-view" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?= __('Foto') ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-xl-12">
                        <picture>
                            <source srcset="" id="foto-view-source" type="image/svg+xml">
                            <img src="" class="rounded mx-auto d-block img-fluid img-thumbnail foto-view-source" alt="...">
                        </picture>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-edit-title" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?= __('Editar Título') ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-xl-12">
                        <?= $this->Form->create(null,  [ 'url' => false,  'id' => 'form-edit-title']) ?>
                            <fieldset>
                                <div class="form-group text required">
                                    <label class="control-label" for="title"> <?= __('Título') ?> </label>
                                    <input name="title" type="text" class="form-control" id="edit-title-value" required="required" maxlength="100"/>
                                </div>
                                <?php
                                    echo $this->Form->hidden('ticket_id', ['id' => 'ticket_id_edit_title']);
                                ?>
                            </fieldset>
                            <?= $this->Form->button(__('Editar'), ['class' => 'primary']) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-edit-description" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?= __('Editar Descripción') ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-xl-12">
                        <?= $this->Form->create(null,  [ 'url' => false,  'id' => 'form-edit-description']) ?>
                            <fieldset>
                                <div class="form-group text">
                                    <label class="control-label" for="description"> <?= __('Descripción') ?> </label>
                                    <textarea name="description" class="ticket_description" id="editor2" rows="6" cols="4" required>
                                    </textarea>
                                </div>
                                <?php
                                    echo $this->Form->hidden('ticket_id', ['id' => 'ticket_id_edit_description']);
                                ?>
                            </fieldset>
                            <?= $this->Form->button(__('Editar'), ['class' => 'primary']) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-change-areas-actives" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?= __('Cambiar Área') ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-xl-12">
                        <?= $this->Form->create(null,  [ 'url' => false,  'id' => 'form-change-areas-actives']) ?>
                            <fieldset>
                                <div class="input-group select">
                                    <label class="input-group-text" for="change-area-id-actives">Áreas</label>
                                    <select name="change_area_id" id="change-area-id-actives" class="form-control" required>
                                        <option value="" selected>Seleccionar</option>
                                        <?php foreach ($areas as $key => $area): ?>
                                            <option value="<?= $key ?>"><?= $area ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <?php
                                    echo $this->Form->hidden('ticket_id', ['id' => 'ticket_id_change_area_actives']);
                                ?>
                            </fieldset>
                            <?= $this->Form->button(__('Cambiar'), ['class' => 'primary mt-3']) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-change-status-from-active" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabel">Cambiar Estados</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-xl-12">

                        <div class="input-group select">
                            <label class="input-group-text" for="change-status-id-actives">Estados</label>
                            <select name="change_status_id" id="change-status-id-actives" class="form-control" required>
                                <option value="" selected>Seleccionar</option>
                                <?php foreach ($tickets_status as $status): ?>
                                    <option value="<?= $status->id ?>"><?= $status->name ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                    </div>
                </div>

                <div class="row mt-4">
                     <div class="col-12">
                         <button type="button" class="btn btn-default btn-cancel" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-danger btn-generate float-right">Confirmar</button>
                     </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $('#date-ticket-datetimepicker-actives').datetimepicker({
        locale: 'es',
        defaultDate: '',
        format: 'DD/MM/YYYY'
    });

    $('#date-record-datetimepicker-actives').datetimepicker({
        locale: 'es',
        defaultDate: '',
        format: 'DD/MM/YYYY'
    });

    var tickets_categories = <?php echo json_encode($tickets_categories); ?>;
    var tickets_status = <?php echo json_encode($tickets_status); ?>;
    var status_ti = <?php echo json_encode($status_ti); ?>;
    var categories = <?php echo json_encode($categories); ?>;

    var available_status = 1;
    var assigned_status;
    var finished_status;

    var tickets = [];
    var ticket_selected_actives = null;

    var status_selected = 0;
    var category_selected = -1;

    var editor = null;

    var table_connections_actives = null;
    var table_tickets_actives = null;
    var users;
    var areas = null;

    var table_tickets_mode = null;
    var tickets_selected_list = [];

    function format(d) {

        //ordena historial del ticket por fecha created de mayor a menor
        d.tickets_records.sort(function(a,b) {
            // Turn your strings into dates, and then subtract them
            // to get a value that is either negative, positive, or zero.
            return new Date(b.created) - new Date(a.created);
        });

        var html = '<br><table class="table table-bordered table-sm table-tickets-records mr-5">'
            + '<thead>'
                + '<tr>'
                  + '<th>Fecha</th>'
                  + '<th>Acción</th>'
                  + '<th>Descripción</th>'
                  + '<th>Imagen</th>'
                  + '<th>Usuario</th>'
                + '</tr>'
              + '</thead>'
            + '<tbody>';

        $.each(d.tickets_records, function(i, ticket_record) {
            var created = '';
            if (ticket_record.created) {
                created = ticket_record.created.split('T')[0];
                created = created.split('-');
                created = created[2] + '/' + created[1] + '/' + created[0];
            }
            var short_action = ticket_record.short_description ? ticket_record.short_description : '';

            var image = '';

            if (ticket_record.image) {

                image = '<picture onclick="opemModalImage(\'/ispbrain/ticket_image/' + ticket_record.image + '\')" ><source srcset="" type="image/svg+xml"><img src="/ispbrain/ticket_image/' + ticket_record.image +'" class="img-fluid img-thumbnail" alt="..."></picture>';

            } else {
                image = '';
            }

            html +=
            '<tr>'
              + '<td>' + created + '</td>'
              + '<td>' + short_action + '</td>'
              + '<td>' + ticket_record.description + '</td>'
              + '<td style="width: 36px; height:  20px;"' + image + '</td>'
              + '<td>' + ticket_record.user.name + '</td>'
            + '</tr>';
        });

        html += '</tbody>'
            + '</table><br>';

        return html;
    }

    function setTicketSelect(id) {
        $.each(tickets, function(i, ticket) {
            if (ticket.id == id) {
                ticket_selected_actives = ticket;
            }
        });
    }

    $('#modal-customer').on('shown.bs.modal', function (e) {

        enableLoad = 1;
        table_customers.draw();
    });

    $(document).ready(function() {

        editor = CKEDITOR.replace( 'editor1' );
        editor2 = CKEDITOR.replace( 'editor2' );
        areas = <?= json_encode($areas) ?>;

        CKEDITOR.config.title = false;
        CKEDITOR.config.toolbar = [
        	{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo' ] },
        	{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ] },
        	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent' ] },
        	{ name: 'styles', items: [ 'Format' ] },
        ];

        $('#start-task-datetimepicker').datetimepicker({
            locale: 'es',
            format: 'DD/MM/YYYY HH:mm'
        });

        users = <?= json_encode($users) ?>;

        table_tickets_actives = $('#table-tickets-actives').DataTable({
    	    "order": [[ 1, 'desc' ]],
    	    "autoWidth": true,
    	    "scrollY": '365px',
    	    "scrollCollapse": true,
    	    "scrollX": true,
    	    "sWrapper":  "dataTables_wrapper dt-bootstrap4",
            "processing": true,
            "serverSide": true,
            "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/tickets/get_tickets_from_table.json",
                "dataFilter": function(data) {
                    var json = $.parseJSON(data);
                    return JSON.stringify(json.response);
                },
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "status": 1,
                        "archived": 0
                    });
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "drawCallback": function(c) {
                var flag = true;
            	switch (c.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
            	    if (table_tickets_actives) {

                        var tableinfo = table_tickets_actives.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }

                        if (curr_pos_scroll) {
                            $(table_tickets_actives.settings()[0].nScrollBody).scrollTop(curr_pos_scroll.top);
                            $(table_tickets_actives.settings()[0].nScrollBody).scrollLeft(curr_pos_scroll.left);
                        }
                    }
            	}
            },
            "columns": [
                {
                    "className": 'details-control ',
                    "orderable": false,
                    "data":      'tickets_records',
                    "render": function ( data, type, full, meta ) {
                        var num = data.length;
                        if (num > 0) {
                            return "<a href='javascript:void(0)' class='grey'>"
                                + "<span class='icon-plus' aria-hidden='true'>"
                                + "</span>"
                                + "</a>";
                        }

                        return '';
                    },
                    "width": '3%'
                },
                { 
                    "data": "id",
                    "type": "integer"
                },
                { 
                    "data": "created",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                { 
                    "data": "modified",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                { 
                    "data": "user_id",
                    "type": "options",
                    "options": users,
                    "render": function ( data, type, row ) {
                        var u = "";
                        if (typeof data !== "undefined") {
                            $.each(users, function( index, value ) {
                                if (data == index) {
                                    u = value;
                                }
                            });
                        }
                        return u;
                    }
                },
                { 
                    "data": "status.name",
                    "type": "options",
                    "options": status_ti,
                },
                {
                    "data": "category.name",
                    "type": "options",
                    "options": categories,
                    "render": function ( data, type, row ) {
                        return '<span class="badge" id="category" style="font-size: 100%; color: ' + row.category.color_text + '; background-color: ' + row.category.color_back + '">' + data + '</span>';
                    }
                },
                { 
                    "data": "customer",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        var customer = "";
                        if (data) {
                            customer = pad(data.code, 5);
                        }
                        return customer;
                    }
                },
                { 
                    "data": "customer",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        var customer = "";
                        if (data) {
                            customer = data.name
                        }
                        return customer;
                    }
                },
                { 
                    "data": "customer",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        var customer = "";
                        if (data) {
                            customer = data.address
                        }
                        return customer;
                    }
                },
                { 
                    "data": "start_task",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            var created_2 = data.split('T')[1].split('-')[0].split(':');

                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0] + ' ' + created_2[0] + ':' + created_2[1] + ' hs';
                        }
                    }
                },
                { 
                    "data": "asigned_user_id",
                    "type": "options",
                    "options": users,
                    "render": function ( data, type, row ) {
                        var u = "";
                        if (typeof data !== "undefined") {
                            $.each(users, function( index, value ) {
                                if (data == index) {
                                    u = value;
                                }
                            });
                        }
                        return u;
                    }
                },
                {
                    "class": "col-description",
                    "data": "title",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        return data;
                    }
                },
                {
                    "data": "area_id",
                    "type": "options",
                    "options": areas,
                    "render": function ( data, type, row ) {
                        var u = "";
                        if (typeof data !== "undefined" && data != null) {
                            u = areas[data];
                        }
                        return u;
                    }
                },
            ],
            "columnDefs": [
                { 
                    "visible": false,
                    "targets":[3]
                },
                { 
                    "class": "left", 
                    "width": "15%",
                    "targets": [8]
                },
            ],
            "createdRow" : function( row, data, index ) {

                $(row).addClass('selectable');
                row.id = data.id;
            },
    	    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            "dom":
    	    	"<'row'<'col-xl-4'l><'col-xl-4'f><'col-xl-4 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
    	});

		$('#table-tickets-actives_wrapper .tools').append($('#btns-tools-actives').contents());

		$('#table-tickets-actives').on( 'init.dt', function () {
            createModalSearchColumn(table_tickets_actives, '.modal-search-columns-tickets-actives');
        });

        $('#btns-tools-actives').show();

		$('#table-tickets-actives tbody').on('click', 'td.details-control', function () {

            if (!$(this).closest('tr').find('.dataTables_empty').length) {
                table_tickets_actives.$('tr.selected').removeClass('selected');
                $(this).closest('tr').addClass('selected');
            }

            var tr = $(this).closest('tr');
            var row = table_tickets_actives.row( tr );

            if ( row.child.isShown() ) {
                row.child.hide();
                tr.removeClass('shown');
                $(this).find('span').removeClass('icon-minus');
                $(this).find('span').addClass('icon-plus');
            }
            else {
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
                $(this).find('span').removeClass('icon-plus');
                $(this).find('span').addClass('icon-minus');
            }
        });

    	$('#table-tickets-actives tbody').on( 'click', 'td', function (e) {

            if (!$(this).closest('tr').find('.dataTables_empty').length) {

                if (!$(this).hasClass('details-control')) {

                    if (typeof ticket_selected_actives !== "undefined") {

                        if (table_tickets_mode == null) {

                            table_tickets_actives.$('tr.selected').removeClass('selected');
                            $(this).addClass('selected');
                            ticket_selected_actives = table_tickets_actives.row( this ).data();

        		            $('.lbl-ticket-title').html('Ticket: #' + ticket_selected_actives.id);

                            if (ticket_selected_actives.asigned_user_id != null) {
                                $('.lbl-user-assigned-name').text('Reasignar Usuario');
                            } else {
                                $('.lbl-user-assigned-name').text('Asignar Usuario');
                            }

                    		if (ticket_selected_actives.customer) {
                    		    $('#btn-info-customer-actives').show();
                    		} else {
                    		    $('#btn-info-customer-actives').hide();
                    		}

                            $('.lbl-ticket-title-active').html("");
                    		$('.lbl-ticket-description-active').html("");

                    		$('.lbl-ticket-title-active').html(ticket_selected_actives.title);
                    		$('.lbl-ticket-description-active').html(ticket_selected_actives.description);

                    		if (ticket_selected_actives.connection) {
                    		    $('#btn-go-ip').removeClass("d-none");
                    		    $('#btn-view-realTimeGraph').removeClass("d-none");
                    		    $('#btn-view-speed').removeClass("d-none");
                    		} else {
                    		    $('#btn-go-ip').addClass("d-none");
                    		    $('#btn-view-realTimeGraph').addClass("d-none");
                    		    $('#btn-view-speed').addClass("d-none");
                    		}

                            $('.modal-actions-tickets-actives').modal('show');

        		        } else if (table_tickets_mode == 'change-status') {

        	               if ($.inArray($(this).closest('tr').attr('id'), tickets_selected_list) == -1) {

        	                   tickets_selected_list.push($(this).closest('tr').attr('id'));
        	                   $(this).closest('tr').addClass('selected');

        	               } else {

                                var attr_id = $(this).closest('tr').attr('id');
                                tickets_selected_list = tickets_selected_list.filter(function(item) { 
                                    return item !== attr_id 
                                });

        	                   $(this).closest('tr').removeClass('selected');
        	               }
        		        }
                    }
                }
            }
        });

        $('.btn-change-status-from-active').click(function() {

            if (table_tickets_mode == null) {

                table_tickets_mode = 'change-status';

                tickets_selected_list = [];

                $('#table-tickets-actives_wrapper').addClass('border border-primary rounded p-2');
                $('.btn-change-status-from-active').addClass('border border-primary text-primary');

            } else if (table_tickets_mode == 'change-status') {

                 $('.modal-change-status-from-active').modal('show');
            }
        });

        $(document).on("click", ".modal-change-status-from-active .btn-cancel", function(e) {
            table_tickets_mode =  null;
            $('#table-tickets-actives_wrapper').removeClass('border border-primary rounded p-2');
            $('.btn-change-status-from-active').removeClass('border border-primary text-primary');
            table_tickets_actives.draw();
        });

        $(document).on("click", ".modal-change-status-from-active .btn-generate", function(e) {

            if (tickets_selected_list.length > 0) {

                var status_selected = $('.modal-change-status-from-active #change-status-id-actives :selected').val();
                var status_name = $('.modal-change-status-from-active #change-status-id-actives :selected').text();

                if (status_selected == "") {
                    generateNoty('warning', 'Debe seleccionar un estado.');
                    return false;
                }
 
                var data = {
                    ids: tickets_selected_list,
                    status: status_selected,
                    status_name: status_name
                };

                bootbox.confirm('Se cambiará el estado de Ticket/s. ¿Desea continuar?', function(result) {

                    if (result) {

                        $('.modal-change-status-from-active').modal('hide');

                        openModalPreloader("Cambiando Estado. Espere Por favor ...");
                        data.tab = "active";

                        $.ajax({
                            type: 'POST',
                            dataType: "json",
                            url: '/ispbrain/tickets/changeMasiveStatus',
                            data:  JSON.stringify({data: data}),
                            success: function(data) {

                                closeModalPreloader();
                                generateNoty(data.response.typeMsg, data.response.msg);

                                table_tickets_mode =  null;
                                $('#table-tickets-actives_wrapper').removeClass('border border-primary rounded p-2');
                                $('.btn-change-status-from-active').removeClass('border border-primary text-primary');
                                table_tickets_actives.ajax.reload();
                            },
                            error: function(jqXHR) {

                                closeModalPreloader();

                                if (jqXHR.status == 403) {

                                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                                	setTimeout(function() { 
                                        window.location.href = "/ispbrain";
                                	}, 3000);

                                } else {

                                	generateNoty('error', 'Error al cambiar estado.');

                                	table_tickets_mode =  null;
                                    $('#table-tickets-actives_wrapper').removeClass('border border-primary rounded p-2');
                                    $('.btn-change-status-from-active').removeClass('border border-primary text-primary');
                                    table_tickets_actives.ajax.reload();
                                }
                            }
                        });

                    }
                });

            } else {
                bootbox.alert('Debe seleccionar al menos un Ticket para continuar.');
            }
        });

        $('.modal-change-status-from-active').on('shown.bs.modal', function (e) {
            $('.modal-change-status-from-active #change-status-id-actives').val("");
        });

        $('#btn-info-customer-actives').click(function() {

            if (ticket_selected_actives.customer) {

                ticket_selected = ticket_selected_actives;

                var customers_coords = [];
                var doc_type_name = sessionPHP.afip_codes.doc_types[ticket_selected_actives.customer.doc_type];
                $('#customer-info-popup #info_customer_code').val(pad(ticket_selected_actives.customer.code, 5))
                $('#customer-info-popup #info_customer_name').val(ticket_selected_actives.customer.name.toUpperCase());
                var ident = ticket_selected_actives.customer.ident ? ticket_selected_actives.customer.ident : "";
                $('#customer-info-popup #info_customer_doc').val(doc_type_name + ' ' + ident);
                $('#customer-info-popup #info_phone').val(ticket_selected_actives.customer.phone);

                var lat = ticket_selected_actives.customer.lat ? ticket_selected_actives.customer.lat : '';
                var lng = ticket_selected_actives.customer.lng ? ticket_selected_actives.customer.lng : '';
                if (ticket_selected_actives.connection) {
                    lat = ticket_selected_actives.connection.lat ? ticket_selected_actives.connection.lat : lat;
                    lng = ticket_selected_actives.connection.lng ? ticket_selected_actives.connection.lng : lng;
                }
                $('#customer-info-popup #info_lat').val(lat);
                $('#customer-info-popup #info_lng').val(lng);

                var customer_coords = {
                    code: ticket_selected_actives.customer.code,
		            name: ticket_selected_actives.customer.name,
		            doc_type: ticket_selected_actives.customer.doc_type,
		            ident: ticket_selected_actives.customer.ident ? ticket_selected_actives.customer.ident : "",
		            lat: ticket_selected_actives.customer.lat,
		            lng: ticket_selected_actives.customer.lng,
		            address: ticket_selected_actives.customer.address
                };
                customers_coords.push(customer_coords);

                var connections_coords = [];
                if (ticket_selected_actives.connection) {

                    var connection_coords = {
                        id: ticket_selected_actives.connection.id,
    		            lat: ticket_selected_actives.connection.lat,
    		            lng: ticket_selected_actives.connection.lng,
    		            ip: ticket_selected_actives.connection.ip,
    		            address: ticket_selected_actives.connection.address
                    };
                    connections_coords.push(connection_coords);
                }

                var services_pending_coords = [];
                if (ticket_selected_actives.service_pending) {

                    $('#service-pending-block').removeClass('my-hidden');
                    $('#customer-info-popup #info_address_service_pending').val(ticket_selected_actives.service_pending.address);
                    $('#customer-info-popup #info_service_name_service_pending').val(ticket_selected_actives.service_pending.service_name);

                    var service_pending_coords = {
    		            lat: ticket_selected_actives.service_pending.lat,
    		            lng: ticket_selected_actives.service_pending.lng,
    		            address: ticket_selected_actives.service_pending.address,
    		            service_name: ticket_selected_actives.service_pending.service_name
                    };
                    services_pending_coords.push(service_pending_coords);
                } else {
                    $('#customer-info-popup #info_address_service_pending').val("");
                    $('#customer-info-popup #info_service_name_service').val("");
                    $('#service-pending-block').addClass('my-hidden');
                }

                initMap(connections_coords, customers_coords, services_pending_coords);

                $('.modal-actions-tickets-actives').modal('hide');
                $('#customer-info-popup').modal('show');
            }

        });

        $('#btn-add-customer').click(function() {

            $('.modal-actions-tickets-actives').modal('hide');

		    $('#modal-customer').modal('show');
		});

        $('#btn-assigned').click(function() {

            $('.modal-actions-tickets-actives').modal('hide');

		    $('#ticket_id_user_assigment').val(ticket_selected_actives.id);
		    $('#modal-user-assigned').modal('show');
		});

		$('#form-user-assigned').submit(function() {

		    $('#modal-user-assigned').modal('hide');

		    openModalPreloader("Asignando Usuario ...");

		    event.preventDefault();

            var send_data = {};  
            var form_data = $(this).serializeArray();
            $.each(form_data, function(i, val) {
                send_data[val.name] = val.value;
            });

            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '/ispbrain/tickets/userAssignment',
                data:  JSON.stringify({data: send_data}),
                success: function(data) {

                    closeModalPreloader();
                    generateNoty(data.response.typeMsg, data.response.msg);

                    table_tickets_actives.ajax.reload();
                },
                error: function(jqXHR) {
                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al asignar usuario al Ticket.');
                    }
                }
            });
		});

		$('#btn-add-comment').click(function() {

		    $('.modal-actions-tickets-actives').modal('hide');

		    $('#ticket_id_add_comment').val(ticket_selected_actives.id);

		    editor.setData('');

		    $('#modal-add-comment').modal('show');
		});

		$('#form-add-comment').submit(function() {

		    $('#modal-add-comment').modal('hide');

		    openModalPreloader("Agregando comentario ...");

		    event.preventDefault();

            var send_data = {};  
            var form_data = $(this).serializeArray();
            $.each(form_data, function(i, val) {
                send_data[val.name] = val.value;
            });

            send_data.description = editor.getData();

            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '/ispbrain/tickets/addComment',
                data:  JSON.stringify({data: send_data}),
                success: function(data) {

                    closeModalPreloader();
                    generateNoty(data.response.typeMsg, data.response.msg);

                    table_tickets_actives.ajax.reload();
                },
                error: function(jqXHR) {
                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al agreagar el comentario al Ticket.');
                    }
                }
            });
		});

		$('#btn-change-status-actives').click(function() {

	        $('.modal-actions-tickets-actives').modal('hide');

		    $('#ticket_id_change_status_actives').val(ticket_selected_actives.id);

		    editor.setData('');

		    $('#modal-change-status-actives').modal('show');
		});

		$('#form-change-status-actives').submit(function() {

		    $('#modal-change-status-actives').modal('hide');

		    openModalPreloader("Cambiando estado...");

		    event.preventDefault();

            var send_data = {};  
            var form_data = $(this).serializeArray();
            $.each(form_data, function(i, val) {
                send_data[val.name] = val.value;
            });

            send_data.description = editor.getData();
            send_data.status_name = $('#change-status-id-actives :selected').text();

            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '/ispbrain/tickets/changeStatus',
                data:  JSON.stringify({data: send_data}),
                success: function(data) {

                    closeModalPreloader();
                    generateNoty(data.response.typeMsg, data.response.msg);

                    table_tickets_actives.ajax.reload();
                },
                error: function(jqXHR) {
                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al cambiar el estado del Ticket.');
                    }
                }
            });
		});

		$('#btn-change-category-actives').click(function() {

	        $('.modal-actions-tickets-actives').modal('hide');

		    $('#ticket_id_change_category_actives').val(ticket_selected_actives.id);

		    editor.setData('');

		    $('#modal-change-category-actives').modal('show');
		});

		$('#form-change-category-actives').submit(function() {

		    $('#modal-change-category-actives').modal('hide');

		    openModalPreloader("Cambiando categoría...");

		    event.preventDefault();

            var send_data = {};  
            var form_data = $(this).serializeArray();
            $.each(form_data, function(i, val) {
                send_data[val.name] = val.value;
            });

            send_data.description = editor.getData();
            send_data.category_name = $('#change-category-id-actives :selected').text();

            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '/ispbrain/tickets/changeCategory',
                data:  JSON.stringify({data: send_data}),
                success: function(data) {

                    closeModalPreloader();
                    generateNoty(data.response.typeMsg, data.response.msg);

                    table_tickets_actives.ajax.reload();
                },
                error: function(jqXHR) {
                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al cambiar la categoría del Ticket.');
                    }
                }
            });
		});

		$('#btn-change-areas-actives').click(function() {

	        $('.modal-actions-tickets-actives').modal('hide');

		    $('#ticket_id_change_area_actives').val(ticket_selected_actives.id);

		    editor.setData('');

		    $('#modal-change-areas-actives').modal('show');
		});

		$('#form-change-areas-actives').submit(function() {

		    $('#modal-change-areas-actives').modal('hide');

		    openModalPreloader("Cambiando área...");

		    event.preventDefault();

            var send_data = {};  
            var form_data = $(this).serializeArray();
            $.each(form_data, function(i, val) {
                send_data[val.name] = val.value;
            });

            send_data.description = editor.getData();
            send_data.area_name = $('#change-area-id-actives :selected').text();

            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '/ispbrain/tickets/changeAreas',
                data:  JSON.stringify({data: send_data}),
                success: function(data) {

                    closeModalPreloader();
                    generateNoty(data.response.typeMsg, data.response.msg);

                    table_tickets_actives.ajax.reload();
                },
                error: function(jqXHR) {
                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                        closeModalPreloader();
                    	generateNoty('error', 'Error al cambiar el área del Ticket.');
                    }
                }
            });
		});

		$('#btn-digital-signature').click(function() {
		    $('#modal-digital-signature').modal('show');
		});

		$('#modal-digital-signature').on('show.bs.modal', function (e) {
            // This is the part where jSignature is initialized.
        	var $sigdiv = $("#signature").jSignature({
        	    'UndoButton': true,
        	    'lineWidth': 4,
        	    'signatureLine': true,
        	    'height': 300,
        	    'background-color': 'transparent',
        	    'decor-color': 'transparent',
        	    width: 700
        	    
        	})

        	// All the code below is just code driving the demo. 
        	, $tools = $('#tools');

            $('.btn-reset-signature').click(function() {
                $sigdiv.jSignature('reset');
            });

            $('#ticket-id-add-digital-signature').val(ticket_selected_actives.id);
            $('#digital-signature-description').val("");

            $('.btn-ok-signature').click(function() {

                var text = 'Confirmar para guardar firma';

                bootbox.confirm(text, function(result) {
                    if (result) {
                        var datapair = $sigdiv.jSignature("getData", "image");
                        $('#hdnSignature').val(datapair[1]);
                        //now submit form 
                        $('#signature-form').submit();
                    }
                });
            });

            $('.btn-ok-signature').hide();
            $('.btn-reset-signature').hide();

            $("#signature").bind(
                "change"
                , function(event) {
                    // 'event.target' will refer to DOM element with id "#signature"
                    var d = $(event.target).jSignature("getData", "native")
                    // if there are more than 2 strokes in the signature
                    // or if there is just one stroke, but it has more than 20 points
                    if ( d.length > 0 || ( d.length === 1) ) {
                        // we show "Submit" button
                        // $(event.target).unbind('change')
                        $(".btn-ok-signature").show();
                        $(".btn-reset-signature").show();
                    } else {
                        $(".btn-reset-signature").hide();
                        $(".btn-ok-signature").hide();
                    }
                }
            );

            $("#signature").resize();
        });

        $('.btn-calendar').click(function() {
            var tickets = table_tickets_actives.data().toArray();
            if (tickets.length > 0) {
		        var action = "/ispbrain/Tickets/calendar/";
                $('body')
                    .append( $('<form/>').attr({'action': action, 'method': 'post', 'id': 'replacer_calendar'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'tickets', 'value': JSON.stringify(tickets)}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer_calendar').submit();
		    } else {
		        generateNoty('warning', 'Debe generar una búsqueda de Tickets para poder visualizar los mismos en el calendario.');
		    }
        });

        $('#btn-change-start-task').click(function() {

            $('.modal-actions-tickets-actives').modal('hide');

            $('#ticket_id_change_start_task').val(ticket_selected_actives.id);

            var start_task = ticket_selected_actives.start_task.split('T')[0];
            var start_task_2 = ticket_selected_actives.start_task.split('T')[1].split('-')[0].split(':');
            start_task = start_task.split('-');
            start_task = start_task[2] + '/' + start_task[1] + '/' + start_task[0] + ' ' + start_task_2[0] + ':' + start_task_2[1];

            $('#start-task-datetimepicker').data('DateTimePicker').date(start_task);

            $('#modal-change-start-task').modal('show');
        });

        $('#btn-edit-title').click(function() {

		    $('.modal-actions-tickets-actives').modal('hide');

            $('#edit-title-value').val(ticket_selected_actives.title);
		    $('#ticket_id_edit_title').val(ticket_selected_actives.id);

		    $('#modal-edit-title').modal('show');
		});

        $('#btn-edit-description').click(function() {

		    $('.modal-actions-tickets-actives').modal('hide');

            editor2.setData(ticket_selected_actives.description);
		    $('#ticket_id_edit_description').val(ticket_selected_actives.id);

		    $('#modal-edit-description').modal('show');
		});

        $('#form-edit-title').submit(function() {

		    $('#modal-edit-title').modal('hide');

		    openModalPreloader("Editando título ...");

		    event.preventDefault();

            var send_data = {};  
            var form_data = $(this).serializeArray();
            $.each(form_data, function(i, val) {
                send_data[val.name] = val.value;
            });

            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '/ispbrain/tickets/editTitle',
                data:  JSON.stringify({data: send_data}),
                success: function(data) {

                    closeModalPreloader();
                    generateNoty(data.response.typeMsg, data.response.msg);

                    table_tickets_actives.ajax.reload();
                },
                error: function(jqXHR) {
                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al editar el título del Ticket.');
                    }
                }
            });
		});

		$('#form-edit-description').submit(function() {

		    $('#modal-edit-description').modal('hide');

		    openModalPreloader("Editando descripción ...");

		    event.preventDefault();

            var send_data = {};  
            var form_data = $(this).serializeArray();
            $.each(form_data, function(i, val) {
                send_data[val.name] = val.value;
            });

            send_data.description = editor2.getData();

            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '/ispbrain/tickets/editDescription',
                data:  JSON.stringify({data: send_data}),
                success: function(data) {

                    closeModalPreloader();
                    generateNoty(data.response.typeMsg, data.response.msg);

                    table_tickets_actives.ajax.reload();
                },
                error: function(jqXHR) {
                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al editar la descripción del Ticket.');
                    }
                }
            });
		});

        $('#form-change-start-task').submit(function() {

		    $('#modal-change-start-task').modal('hide');

		    openModalPreloader("Cambiando fecha de inicio...");

		    event.preventDefault();

            var send_data = {};  
            var form_data = $(this).serializeArray();
            $.each(form_data, function(i, val) {
                send_data[val.name] = val.value;
            });

            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '/ispbrain/tickets/changeStartTask',
                data:  JSON.stringify({data: send_data}),
                success: function(data) {

                    closeModalPreloader();
                    generateNoty(data.response.typeMsg, data.response.msg);

                    table_tickets_actives.ajax.reload();
                },
                error: function(jqXHR) {
                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al cambiar la fecha del Ticket.');
                    }
                }
            });
		});

		$('#btn-photo').click(function() {
            $('.modal-actions-tickets-actives').modal('hide');
            $('#ticket_id_photo').val(ticket_selected_actives.id);
            $('#description_photo').val('');
            
            $('#modal-photo').modal('show');
        });

        $('.btn-update-table').click(function() {
             if (table_tickets_actives) {
                table_tickets_actives.ajax.reload();
            }
        });

        $('.btn-search-column').click(function() {
            $('.modal-search-columns-tickets-actives').modal('show');
        });

        $(".btn-export-actives").click(function() {
            bootbox.confirm('Se descargará un archivo Excel', function(result) {
                if (result) {
                    $('#table-tickets-actives').tableExport({tableName: 'tickets', type:'excel', escape:'false'});
                }
            }).find("div.modal-content").addClass("confirm-width-md");
        });

        $(".btn-export-pdf").click(function() {

            var tickets = table_tickets_actives.data().toArray();

            if (tickets.length > 0) {
		        var action = "/ispbrain/Tickets/ticketExportPDF/";
                $('body')
                    .append( $('<form/>').attr({'action': action, 'method': 'post', 'target': '_blank', 'id': 'replacer_pdf'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'tickets', 'value': JSON.stringify(tickets)}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer_pdf').submit();

                $('#replacer_pdf').remove();

		    } else {
		        generateNoty('warning', 'Debe generar una búsqueda de Tickets para poder exportar los mismos');
		    }
        });

        $('#btn-go-ip').click(function() {
            var ip = ticket_selected_actives.connection.ip;
            window.open("http://" + ip, '_blank');
        });

        $('#btn-view-speed').click(function() {
            var action = '/ispbrain/ConnectionsGraphic/queueHistoryGraph/' + ticket_selected_actives.connection.id;
            window.open(action, '_blank');
        });

        $('#btn-view-realTimeGraph').click(function() {
            var action = '/ispbrain/ConnectionsGraphic/queueRealTimeGraph/' + ticket_selected_actives.connection.id;
            window.open(action, '_blank');
        });

        $("#btn-print-pdf").click(function() {

            var id = ticket_selected_actives.id;

            $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/tickets/individualTicketExportPDF/' + id, 'method': 'post', 'id': 'replacer_archive_actives_individual_export_ticket', "target": "_blank"})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#replacer_archive_actives_individual_export_ticket').submit();
        });

        $('a[id="actives-tab"]').on('shown.bs.tab', function (e) {
            table_tickets_actives.draw();
        });

        $('#btn-archive-actives').click(function() {

            var text = "¿Está Seguro que desea archivar el Ticket?";
            var id = ticket_selected_actives.id;

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/tickets/archive/' + id, 'method': 'post', 'id': 'replacer_archive_actives'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer_archive_actives').submit();
                }
            });
        });

        $('#btn-delete-actives').click(function() {

            var text = "¿Está Seguro que desea eliminar el Ticket?";
            var id = ticket_selected_actives.id;

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/tickets/delete/' + id, 'method': 'post', 'id': 'replacer_delete_actives'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer_delete_actives').submit();
                }
            });
        });

        $('#card-customer-seeker').on('CUSTOMER_SELECTED', function() {

            var send_data = {};
            send_data.customer_code = customer_selected.code;
            send_data.ticket_id = ticket_selected_actives.id;

            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '/ispbrain/tickets/addCustomer',
                data:  JSON.stringify({data: send_data}),
                success: function(data) {

                    closeModalPreloader();
                    generateNoty(data.response.typeMsg, data.response.msg);

                    table_tickets_actives.ajax.reload();
                    $('#modal-customer').modal('hide');
                },
                error: function(jqXHR) {
                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al agregar al cliente al Ticket.');
                    }
                    $('#modal-customer').modal('hide');
                }
            });
        });

        $('#card-customer-seeker').on('CUSTOMER_CLEAR', function() {

            var send_data = {};
            send_data.ticket_id = ticket_selected_actives.id;

            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '/ispbrain/tickets/clearCustomer',
                data:  JSON.stringify({data: send_data}),
                success: function(data) {

                    closeModalPreloader();
                    generateNoty(data.response.typeMsg, data.response.msg);

                    table_tickets_actives.ajax.reload();
                    $('#modal-customer').modal('hide');
                },
                error: function(jqXHR) {
                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al agregar al cliente al Ticket.');
                    }
                    $('#modal-customer').modal('hide');
                }
            });
        });
        
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>

<?php $this->end(); ?>
