<?php $this->extend('/Tickets/table/tabs'); ?>

<?php $this->start('closed'); ?>

<script type="text/javascript">

    function opemModalImageClosed(path) {
        $('#modal-photo-view-close #foto-view-img-close').attr('src', path );
        $('#modal-photo-view-close #foto-view-source-close').attr('srcset', path );
        $('#modal-photo-view-close').modal('show');
    }

</script>

<div class="row" id="container-tickets">
    <div class="col-xl-12 col-md-12 col-sm-12">
        <table class="table table-hover table-bordered" id="table-tickets">
           <thead>
               <tr>
                    <th></th>
                    <th>#</th>
                    <th>Creado</th>
                    <th>Cierre</th>
                    <th>Usuario</th>
                    <th>Estado</th>
                    <th>Categoría</th>
                    <th>Cód.</th>
                    <th>Cliente</th>
                    <th>Dom. cliente</th>
                    <th>Inicio</th>
                    <th>Asignado</th>
                    <th>Título</th>
                    <th>Área</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5">

        <?php
            echo $this->Html->link(
                '<span class="fas fa-sync-alt" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Actualizar tabla',
                'class' => 'btn btn-default btn-update-table-closed ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon fas fa-search" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Buscar por columnas',
                'class' => 'btn btn-default btn-search-column-closed ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="far fa-calendar-alt" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                    'title' => 'Ver Calendario',
                    'class' => 'btn btn-default btn-calendar-closed ml-1',
                    'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export-closed ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-pdf" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar PDF',
                'class' => 'btn btn-default btn-export-pdf-closed ml-1',
                'escape' => false
            ]);
        ?>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-change-status',
        'name' =>  'Cambiar Estado',
        'icon' =>  'fas fa-exchange-alt mr-2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-info-customer',
        'name' =>  'Ficha de Cliente',
        'icon' =>  'icon-profile mr-2',
        'type' =>  'btn-info'
    ];

    $buttons[] = [
        'id'   =>  'btn-archive',
        'name' =>  'Archivar',
        'icon' =>  'fa fa-archive',
        'type' =>  'btn-warning'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete',
        'name' =>  'Eliminar',
        'icon' =>  'fa fa-times mr-2',
        'type' =>  'btn-danger'
    ];

    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-tickets']);
    echo $this->element('actions_tickets', ['modal'=> 'modal-actions-tickets', 'title' => '<span class="lbl-ticket-title"></span>', 'buttons' => $buttons]);
?>

<div class="modal fade" id="modal-change-status" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?= __('Cambiar Estado') ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-xl-12">
                        <?= $this->Form->create(null,  [ 'url' => false,  'id' => 'form-change-status']) ?>
                            <fieldset>
                                <div class="input-group select">
                                    <label class="input-group-text" for="change-status-id">Estados</label>
                                    <select name="change_status_id" id="change-status-id" class="form-control" required>
                                        <option value="" selected>Seleccionar</option>
                                        <?php foreach ($tickets_status as $status): ?>
                                            <option value="<?= $status->id ?>"><?= $status->name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <?php
                                    echo $this->Form->hidden('ticket_id', ['id' => 'ticket_id_change_status']);
                                ?>
                            </fieldset>
                            <?= $this->Form->button(__('Cambiar'), ['class' => 'primary mt-3']) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-photo-view-close" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?= __('Foto') ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-xl-12">
                        <picture>
                            <source srcset="" id="foto-view-source-close" type="image/svg+xml">
                            <img src="" class="rounded mx-auto d-block img-fluid img-thumbnail foto-view-source-close" alt="...">
                        </picture>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $('#date-ticket-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: '',
        format: 'DD/MM/YYYY'
    });

    $('#date-record-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: '',
        format: 'DD/MM/YYYY'
    });

    var tickets_categories = <?php echo json_encode($tickets_categories); ?>;
    var tickets_status = <?php echo json_encode($tickets_status); ?>;
    var status_ti = <?php echo json_encode($status_ti); ?>;
    var categories = <?php echo json_encode($categories); ?>;

    var available_status = 1;
    var assigned_status;
    var finished_status;

    var tickets = [];
    var ticket_selected_closed = null;

    var status_selected = 0;
    var category_selected = -1;

    var editor = null;

    var table_connections = null;
    var table_tickets = null;
    var users;
    var areas;

    function formatClosed(d) {

        //ordena historial del ticket por fecha created de mayor a menor
        d.tickets_records.sort(function(a,b) {
            // Turn your strings into dates, and then subtract them
            // to get a value that is either negative, positive, or zero.
            return new Date(b.created) - new Date(a.created);
        });

        var html = '<br><table class="table table-bordered table-sm table-tickets-records mr-5">'
            + '<thead>'
                + '<tr>'
                  + '<th>Fecha</th>'
                  + '<th>Acción</th>'
                  + '<th>Descripción</th>'
                  + '<th>Imagen</th>'
                  + '<th>Usuario</th>'
                + '</tr>'
              + '</thead>'
            + '<tbody>';

        $.each(d.tickets_records, function(i, ticket_record) {
            var created = '';
            if (ticket_record.created) {
                created = ticket_record.created.split('T')[0];
                created = created.split('-');
                created = created[2] + '/' + created[1] + '/' + created[0];
            }
            var short_action = ticket_record.short_description ? ticket_record.short_description : '';

            var image = '';

            if (ticket_record.image) {

                image = '<picture onclick="opemModalImageClosed(\'/ispbrain/ticket_image/' + ticket_record.image + '\')" ><source srcset="" type="image/svg+xml"><img src="/ispbrain/ticket_image/' + ticket_record.image +'" class="img-fluid img-thumbnail" alt="..."></picture>';

            } else {
                image = '';
            }

            html +=
            '<tr>'
              + '<td>' + created + '</td>'
              + '<td>' + short_action + '</td>'
              + '<td>' + ticket_record.description + '</td>'
              + '<td style="width: 36px; height:  20px;"' + image + '</td>'
              + '<td>' + ticket_record.user.name + '</td>'
            + '</tr>';
        });

        html += '</tbody>'
            + '</table><br>';

        return html;
    }

    function setTicketSelect(id) {
        $.each(tickets, function(i, ticket) {
            if (ticket.id == id) {
                ticket_selected_closed = ticket;
            }
        });
    }

    $(document).ready(function() {

        users = <?= json_encode($users) ?>;
        areas = <?= json_encode($areas) ?>;

        table_tickets = $('#table-tickets').DataTable({
    	    "order": [[ 1, 'desc' ]],
    	    "autoWidth": true,
    	    "scrollY": '365px',
    	    "scrollCollapse": true,
    	    "scrollX": true,
    	    "sWrapper":  "dataTables_wrapper dt-bootstrap4",
            "processing": true,
            "serverSide": true,
		    "ajax": {
                "url": "/ispbrain/tickets/get_tickets_from_table.json",
                "dataFilter": function(data) {
                    var json = $.parseJSON(data);
                    return JSON.stringify(json.response);
                },
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "status": 2,
                        "archived": 0
                    });
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "drawCallback": function(c) {
                var flag = true;
            	switch (c.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
            	    if (table_tickets) {

                        var tableinfo = table_tickets.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column-closed').addClass('search-apply');
                        } else {
                            $('.btn-search-column-closed').removeClass('search-apply');
                        }

                        if (curr_pos_scroll) {
                            $(table_tickets.settings()[0].nScrollBody).scrollTop(curr_pos_scroll.top);
                            $(table_tickets.settings()[0].nScrollBody).scrollLeft(curr_pos_scroll.left);
                        }
                    }
            	}
            },
            "columns": [
                {
                    "className": 'details-control ',
                    "orderable": false,
                    "data":      'tickets_records',
                    "render": function ( data, type, full, meta ) {
                        var num = data.length;
                        if (num > 0) {
                            return "<a href='javascript:void(0)' class='grey'>"
                                + "<span class='icon-plus' aria-hidden='true'>"
                                + "</span>"
                                + "</a>";
                        }

                        return '';
                    },
                    "width": '3%'
                },
                { 
                    "data": "id",
                    "type": "integer"
                },
                { 
                    "data": "created",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                { 
                    "data": "modified",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                { 
                    "data": "user_id",
                    "type": "options",
                    "options": users,
                    "render": function ( data, type, row ) {
                        var u = "";
                        if (typeof data !== "undefined") {
                            $.each(users, function( index, value ) {
                                if (data == index) {
                                    u = value;
                                }
                            });
                        }
                        return u;
                    }
                },
                { 
                    "data": "status.name",
                    "type": "options",
                    "options": status_ti,
                },
                {
                    "data": "category.name",
                    "type": "options",
                    "options": categories,
                    "render": function ( data, type, row ) {
                        return '<span class="badge" id="category" style="font-size: 100%; color: ' + row.category.color_text + '; background-color: ' + row.category.color_back + '">' + data + '</span>';
                    }
                },
                { 
                    "data": "customer",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        var customer = "";
                        if (data) {
                            customer = pad(data.code, 5);
                        }
                        return customer;
                    }
                },
                { 
                    "data": "customer",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        var customer = "";
                        if (data) {
                            customer = data.name
                        }
                        return customer;
                    }
                },
                { 
                    "data": "customer",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        var customer = "";
                        if (data) {
                            customer = data.address
                        }
                        return customer;
                    }
                },
                { 
                    "data": "start_task",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            var created_2 = data.split('T')[1].split('-')[0].split(':');

                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0] + ' ' + created_2[0] + ':' + created_2[1] + ' hs';
                        }
                    }
                },
                { 
                    "data": "asigned_user_id",
                    "type": "options",
                    "options": users,
                    "render": function ( data, type, row ) {
                        var u = "";
                        if (typeof data !== "undefined") {
                            $.each(users, function( index, value ) {
                                if (data == index) {
                                    u = value;
                                }
                            });
                        }
                        return u;
                    }
                },
                {
                    "class": "col-description",
                    "data": "title",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        return data.replace('&nbsp;', '');
                    }
                },
                {
                    "data": "area_id",
                    "type": "options",
                    "options": areas,
                    "render": function ( data, type, row ) {
                        var u = "";
                        if (typeof data !== "undefined" && data != null) {
                            u = areas[data];
                        }
                        return u;
                    }
                },
            ],
            "columnDefs": [
                { 
                    "visible": true,
                    "targets":[3]
                },
                { 
                    "class": "left", 
                    "width": "15%",
                    "targets": [8]
                },
            ],
            "createdRow" : function( row, data, index ) {
                
                $(row).addClass('selectable');
                
                row.id = data.id;
            },
    	    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            "dom":
    	    	"<'row'<'col-xl-4'l><'col-xl-4'f><'col-xl-4 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
    	});

// 		$('#table-tickets_filter').closest('div').before($('#btns-tools').contents());
		
		$('#table-tickets_wrapper .tools').append($('#btns-tools').contents());

		$('#table-tickets').on( 'init.dt', function () {
            createModalSearchColumn(table_tickets, '.modal-search-columns-tickets');
        });

        $('#btns-tools').show();

		$('#table-tickets tbody').on('click', 'td.details-control', function () {

            if (!$(this).closest('tr').find('.dataTables_empty').length) {
                table_tickets.$('tr.selected').removeClass('selected');
                $(this).closest('tr').addClass('selected');
            }

            var tr = $(this).closest('tr');
            var row = table_tickets.row( tr );

            if ( row.child.isShown() ) {
                row.child.hide();
                tr.removeClass('shown');
                $(this).find('span').removeClass('icon-minus');
                $(this).find('span').addClass('icon-plus');
            }
            else {
                row.child( formatClosed(row.data()) ).show();
                tr.addClass('shown');
                $(this).find('span').removeClass('icon-plus');
                $(this).find('span').addClass('icon-minus');
            }
        });

    	$('#table-tickets tbody').on( 'click', 'td', function (e) {

            if (!$(this).closest('tr').find('.dataTables_empty').length) {

                if (!$(this).hasClass('details-control')) {

                    table_tickets.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    ticket_selected_closed = table_tickets.row( this ).data();

                    if (typeof ticket_selected_closed !== "undefined") {
                        
                        $('.lbl-ticket-title').html('Ticket: #' + ticket_selected_closed.id);
    
                        if (ticket_selected_closed.asigned_user_id != null) {
                            $('.lbl-user-assigned-name').text('Reasignar Usuario');
                        } else {
                            $('.lbl-user-assigned-name').text('Asignar Usuario');
                        }

                		if (ticket_selected_closed.customer) {
                		    $('#btn-info-customer').show();
                		} else {
                		    $('#btn-info-customer').hide();
                		}

                		$('.lbl-ticket-title-active').html("");
                		$('.lbl-ticket-description-active').html("");

                		$('.lbl-ticket-title-active').html(ticket_selected_closed.title);
                		$('.lbl-ticket-description-active').html(ticket_selected_closed.description);

                        $('.modal-actions-tickets').modal('show');
                    }
                }
            }
        });

        $('#btn-info-customer').click(function() {

            if (ticket_selected_closed.customer) {

                ticket_selected = ticket_selected_closed;

                var customers_coords = [];
                var doc_type_name = sessionPHP.afip_codes.doc_types[ticket_selected_closed.customer.doc_type];
                $('#customer-info-popup #info_customer_code').val(pad(ticket_selected_closed.customer.code, 5))
                $('#customer-info-popup #info_customer_name').val(ticket_selected_closed.customer.name.toUpperCase());
                var ident = "";
                if (ticket_selected_closed.customer.ident) {
                    ident = ticket_selected_closed.customer.ident;
                }
                $('#customer-info-popup #info_customer_doc').val(doc_type_name + ' ' + ident);
                $('#customer-info-popup #info_phone').val(ticket_selected_closed.customer.phone);

                var customer_coords = {
                    code: ticket_selected_closed.customer.code,
		            name: ticket_selected_closed.customer.name,
		            doc_type: ticket_selected_closed.customer.doc_type,
		            ident: ident,
		            lat: ticket_selected_closed.customer.lat,
		            lng: ticket_selected_closed.customer.lng,
		            address: ticket_selected_closed.customer.address
                };
                customers_coords.push(customer_coords);

                var connections_coords = [];
                if (ticket_selected_closed.connection) {

                    var connection_coords = {
                        id: ticket_selected_closed.connection.id,
    		            lat: ticket_selected_closed.connection.lat,
    		            lng: ticket_selected_closed.connection.lng,
    		            ip: ticket_selected_closed.connection.ip,
    		            address: ticket_selected_closed.connection.address
                    };
                    connections_coords.push(connection_coords);
                }

                var services_pending_coords = [];
                if (ticket_selected_closed.service_pending) {

                    $('#service-pending-block').removeClass('my-hidden');
                    $('#customer-info-popup #info_address_service_pending').val(ticket_selected_closed.service_pending.address);
                    $('#customer-info-popup #info_service_name_service_pending').val(ticket_selected_closed.service_pending.service_name);

                    var service_pending_coords = {
    		            lat: ticket_selected_closed.service_pending.lat,
    		            lng: ticket_selected_closed.service_pending.lng,
    		            address: ticket_selected_closed.service_pending.address,
    		            service_name: ticket_selected_closed.service_pending.service_name
                    };
                    services_pending_coords.push(service_pending_coords);
                } else {
                    $('#customer-info-popup #info_address_service_pending').val("");
                    $('#customer-info-popup #info_service_name_service_pending').val("");
                    $('#service-pending-block').addClass('my-hidden');
                }

                initMap(connections_coords, customers_coords, services_pending_coords, 'map');
                
                $('.modal-actions-tickets').modal('hide');
                $('#customer-info-popup').modal('show');
            }

        });

		$('#btn-change-status').click(function() {

	        $('.modal-actions-tickets').modal('hide');

		    $('#ticket_id_change_status').val(ticket_selected_closed.id);

		    editor.setData('');

		    $('#modal-change-status').modal('show');
		});

		$('#form-change-status').submit(function() {

		    $('#modal-change-status').modal('hide');

		    openModalPreloader("Cambiando estado...");

		    event.preventDefault();

            var send_data = {};  
            var form_data = $(this).serializeArray();
            $.each(form_data, function(i, val) {
                send_data[val.name] = val.value;
            });

            send_data.description = editor.getData();
            send_data.status_name = $('#change-status-id :selected').text();

            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '/ispbrain/tickets/changeStatus',
                data:  JSON.stringify({data: send_data}),
                success: function(data) {

                    closeModalPreloader();
                    generateNoty(data.response.typeMsg, data.response.msg);

                    table_tickets.ajax.reload();
                },
                error: function(jqXHR) {
                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al cambiar el estado del Ticket.');
                    }
                }
            });
		});

        $('.btn-update-table-closed').click(function() {
             if (table_tickets) {
                table_tickets.ajax.reload();
            }
        });

        $('.btn-search-column-closed').click(function() {
            $('.modal-search-columns-tickets').modal('show');
        });

        $(".btn-export-closed").click(function() {
            bootbox.confirm('Se descargará un archivo Excel', function(result) {
                if (result) {
                    $('#table-tickets').tableExport({tableName: 'tickets', type:'excel', escape:'false'});
                }
            }).find("div.modal-content").addClass("confirm-width-md");
        });

        $(".btn-export-pdf-closed").click(function() {

            var tickets = table_tickets.data().toArray();

            if (tickets.length > 0) {
		        var action = "/ispbrain/Tickets/ticketExportPDF/";
                $('body')
                    .append( $('<form/>').attr({'action': action, 'method': 'post', 'target': '_blank', 'id': 'replacer_pdf1'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'tickets', 'value': JSON.stringify(tickets)}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer_pdf1').submit();

                  $('#replacer_pdf').remove();

		    } else {
		        generateNoty('warning', 'Debe generar una búsqueda de Tickets para poder exportar los mismos');
		    }
        });

        $('a[id="closed-tab"]').on('shown.bs.tab', function (e) {
            table_tickets.draw();
        });

        $('#btn-archive').click(function() {

            var text = "¿Está Seguro que desea archivar el Ticket?";
            var id = ticket_selected_closed.id;

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/tickets/archive/' + id, 'method': 'post', 'id': 'replacer_archive'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer_archive').submit();
                }
            });
        });

        $('#btn-delete').click(function() {

            var text = "¿Está Seguro que desea eliminar el Ticket?";
            var id = ticket_selected_closed.id;

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/tickets/delete/' + id, 'method': 'post', 'id': 'replacer_delete'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer_delete').submit();
                }
            });
        });

        $('.btn-calendar-closed').click(function() {
            var tickets = table_tickets.data().toArray();
            if (tickets.length > 0) {
		        var action = "/ispbrain/Tickets/calendar/";
                $('body')
                    .append( $('<form/>').attr({'action': action, 'method': 'post', 'id': 'replacer_calendar'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'tickets', 'value': JSON.stringify(tickets)}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer_calendar').submit();
		    } else {
		        generateNoty('warning', 'Debe generar una búsqueda de Tickets para poder visualizar los mismos en el calendario.');
		    }
        });
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>

<?php $this->end(); ?>