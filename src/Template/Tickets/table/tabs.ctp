<style type="text/css">

    .nav-link {
        cursor: pointer;
    }

    .nav-tabs {
        border-bottom: 1px solid #f05f40;
    }

    #map {
		height: 350px;
	}

	.modal {
	    overflow-x: hidden;
	    overflow-y: auto;
	}

    .description {
        height: 145px;
        width: 100%;
        margin-bottom: 20px;
    }

    .my-hidden {
        display: none;
    }

    .actions-tickets {
        border: 1px solid #cccccc59;
        padding: 18px;
        font-family: sans-serif;
    }

    .view-tickets {
        border: 1px solid #cccccc59;
        padding: 18px;
        font-family: sans-serif;
    }

    .card-header {
        font-size: 12px;
    }

    .badge {
        font-size: 54%;
    }

    .btn-default.active {
        color: #f05f40;
        background-color: #e6e6e6;
        border-color: #f05f40;
    }

    .table-tickets-records th {
        background-color: #607D8B !important;
        color: white !important;
    }

    #digital-signature-description {
        height: 30px;
    }

    .table-hover tbody tr:hover {
        background-color: white !important;
    }

    .table-hover tbody tr.selectable:hover {
        background-color: rgba(105, 104, 104, 0.28) !important;
    }

    .col-description p {
        margin: 0 !important;
    }

    table .img-thumbnail {
        max-width: 50px !important;
    }

    .ticket_description {
        width: 100%;        
    }
    
    #signature > div > img {
        display: none !important;
    }

</style>

<ul class="nav nav-tabs" id="myTab" role="tablist">

    <li class="nav-item">
        <a class="nav-link active" id="actives-tab" data-target="#actives" role="tab" aria-controls="actives" aria-expanded="true">Activos</a>
    </li>

    <li class="nav-item">
        <a class="nav-link " id="closed-tab" data-target="#closed" role="tab" aria-controls="closed" aria-expanded="false">Cerrados</a>
    </li>

</ul>

<div class="tab-content" id="myTabContent">

    <div class="tab-pane fade show active" id="actives" role="tabpanel" aria-labelledby="actives-tab">
        <?= $this->fetch('actives') ?>
    </div>

    <div class="tab-pane fade" id="closed" role="tabpanel" aria-labelledby="closed-tab">
        <?= $this->fetch('closed') ?>
    </div>

</div>

<div class="modal fade" id="customer-info-popup" tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Info Cliente</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <div class="row">

                    <div class="col-12 col-xl-3">
                        <div class="input-group m-1">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Código</span>
                            </div>
                            <input type="text" class="form-control text-right" id="info_customer_code" readonly>
                        </div>
                    </div>

                    <div class="col-12 col-xl-4">

                        <div class="input-group m-1">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Documento</span>
                            </div>
                            <input type="text" class="form-control text-right" id="info_customer_doc" readonly> 
                        </div>
                    </div>

                    <div class="col-12 col-xl-5">

                         <div class="input-group m-1">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Nombre</span>
                            </div>
                            <input type="text" class="form-control text-right" id="info_customer_name" readonly>
                        </div>
                    </div>

                    <div class="col-12 col-xl-4 ">

                        <div class="input-group m-1">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Tel.</span>
                            </div>
                            <input type="text" class="form-control text-right" id="info_phone" readonly>
                        </div>
                    </div>

                    <div class="col-12 col-xl-4 ">

                        <div class="input-group m-1">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Lat.</span>
                            </div>
                            <input type="text" class="form-control text-right" id="info_lat" readonly>
                        </div>
                    </div>

                    <div class="col-12 col-xl-4 ">

                        <div class="input-group m-1">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Lng.</span>
                            </div>
                            <input type="text" class="form-control text-right" id="info_lng" readonly>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-xl-12">

                        <legend style="margin-top: 15px;"><?= __('Conexión del Cliente') ?></legend>
                        <table class="table table-bordered table-hover" id="table-connections">
                            <thead>
                                <tr>
                                    <th><?= __('Controlador') ?></th>
                                    <th><?= __('Servicio') ?></th>
                                    <th><?= __('IP') ?></th>
                                    <th><?= __('Domicilio') ?></th>
                                    <th><?= __('Área') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="row mb-3" id="service-pending-block" >
                    <div class="col-xl-12">

                        <legend style="margin-top: 15px;"><?= __('Servicio Pendiente del Cliente') ?></legend>
                        <div class="row">
                            <div class="col-12 col-xl-5">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Servicio</span>
                                    </div>
                                    <input type="text" class="form-control text-right" id="info_service_name_service_pending" readonly>
                                </div>
                            </div>
                            <div class="col-12 col-xl-5">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Domicilio</span>
                                    </div>
                                    <input type="text" class="form-control text-right" id="info_address_service_pending" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" style="height: 350px;">
                    <div class="col-xl-12" style="height: 350px;">
                        <div id="map" style="height: 350px;"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<?php
    echo $this->element('modal_preloader');
?>

<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2K3zoK46JybWPzQbhfQQqIIbdZ_3WcQs">
</script>

<!--[if lt IE 9]>
<script type="text/javascript" src="/ispbrain/vendor/jSignature/flashcanvas.js"></script>
<![endif]-->
<script src="/ispbrain/vendor/jSignature/src/jSignature.js"></script>
<script src="/ispbrain/vendor/jSignature/src/plugins/jSignature.CompressorBase30.js"></script>
<script src="/ispbrain/vendor/jSignature/src/plugins/jSignature.CompressorSVG.js"></script>
<script src="/ispbrain/vendor/jSignature/src/plugins/jSignature.UndoButton.js"></script>
<script src="/ispbrain/vendor/jSignature/src/plugins/signhere/jSignature.SignHere.js"></script>

<script type="text/javascript">

    var map = null;
    var geocoder = null;
    var marker = null;
    var markers = [];
    var infoWindows = [];
    var curr_pos_scroll = null;
    var ticket_selected = null;

    var markerGroups = {
        "connections": [],
        "customers": [],
        "services_pending": []
    };

	function initMap(connections_coords, customers_coords, services_pending_coords) {

		geocoder = new google.maps.Geocoder;

		var myLatlng = {lat: sessionPHP.paraments.system.map.lat, lng: sessionPHP.paraments.system.map.lng};

		var mapOptions = {
			zoom: 15,
			center: myLatlng,
			mapTypeId: 'hybrid'   // roadmap, satellite, hybrid, terrain 
		};

		map = new google.maps.Map(document.getElementById('map'), mapOptions);

		$.each(connections_coords, function(i, val) {
		    if (val.lat != null && val.lng != null) {
		        var id = val.id;
		        var ip = val.ip;
		        var init = { lat: val.lat, lng: val.lng };
                var latLng = new google.maps.LatLng(init.lat, init.lng);
                var address = val.address;
                addMarker(id, ip, latLng, address, val.lat, val.lng);
		    }
		});

		$.each(customers_coords, function(i, val) {

		    if (val.lat != null && val.lng != null) {
		        var customer = {
		            code: val.code,
		            name: val.name,
		            doc_type: val.doc_type,
		            ident: val.ident,
		            address: val.address
		        }

		        var init = { lat: val.lat, lng: val.lng };
                var latLng = new google.maps.LatLng(init.lat, init.lng);
                addCustomerMarker(latLng, customer, val.lat, val.lng);
		    }
		});

		$.each(services_pending_coords, function(i, val) {
		    if (val.lat != null && val.lng != null) {
		        var id = val.id;
		        var init = { lat: val.lat, lng: val.lng };
                var latLng = new google.maps.LatLng(init.lat, init.lng);
                var address = val.address;
                var service_name = val.service_name;
                addServicePendingMarker(id, latLng, address, service_name, val.lat, val.lng);
		    }
		});

		// a  div where we will place the buttons
        ctrl = $('<div/>').css({background:'#fff',
            border:'1px solid #000',
            padding:'4px',
            margin:'2px',
            textAlign:'center'
        });

        ctrl.append(
            '<ul style="text-align: left; position: relative; display: block; padding-left: 5px; padding-right: 5px; padding-top: 8px;">' +
                '<div class="form-check">' +
                    '<input onclick="toggleGroup(\'connections\')" type="checkbox" class="form-check-input" id="exampleCheck1" checked="checked">' +
                    '<label style="padding-left: 26px; padding-top: 4px; color: #0000ff !important" class="form-check-label" for="exampleCheck1"> Conexiones</label>' +
                '</div>' +
                '<div class="form-check">' +
                    '<input onclick="toggleGroup(\'customers\')" type="checkbox" class="form-check-input" id="exampleCheck2" checked="checked">' +
                    '<label style="padding-left: 26px; padding-top: 4px; color: #008000 !important" class="form-check-label" for="exampleCheck2"> Clientes</label>' +
                '</div>' +
                '<div class="form-check">' +
                    '<input onclick="toggleGroup(\'services_pending\')" type="checkbox" class="form-check-input" id="exampleCheck3" checked="checked">' +
                    '<label style="padding-left: 26px; padding-top: 4px; color: #ff0000 !important" class="form-check-label" for="exampleCheck3"> Servicio Pendiente</label>' +
                '</div>' +
            '</ul>'
        );

        //use the buttons-div as map-control 
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(ctrl[0]);
	}

	function addMarker(id, ip, location, address, lat, lng) {

	    var contentString = 
	    '<div id="content">' +
            '<div id="siteNotice">' +
            '</div>' +
            '<h1 id="firstHeading" class="firstHeading"><a target="_blank" href="/ispbrain/connections/view/' + id +' ">Conexión - IP: ' + ip + '</a></h1>'+
            '<div id="bodyContent">' +
                '<p>' +
                    '<b>Lat: </b>' + lat + ' - <b>Lng: </b>' + lng + '<br>' +
                '</p>' +
                '<p>' +
                    '<b>Domicilio: </b>' + address + '<br>' +
                '</p>' +
            '</div>' +
        '</div>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });

        infoWindows.push(infowindow);

		marker = new google.maps.Marker({
			position: location,
			map: map,
		    animation: google.maps.Animation.DROP,
			title: '',
			icon: pinSymbol('blue'),
			type: 'connections'
		});
		marker.setMap(map);	
		markers.push(marker);

        if (!markerGroups['connections']) markerGroups['connections'] = [];
        markerGroups['connections'].push(marker);

        google.maps.event.addListener(marker,'click', (function(marker, contentString, infowindow) { 
            return function() {
                removeAnimationAllMarkers(marker);
                closeAllInfoWindows();
                infowindow.setContent(contentString);
                infowindow.open(map, marker);
            };
        })(marker, contentString, infowindow));

        google.maps.event.addListener(infowindow,'closeclick',function() {
            removeAnimationAllMarkers(null);
        });
	}

	function addCustomerMarker(location, customer, lat ,lng) {

	    var contentString = 
	    '<div id="content">' +
            '<div id="siteNotice">' +
            '</div>' +
            '<h1 id="firstHeading" class="firstHeading"><a target="_blank" href="/ispbrain/customers/view/' +  customer.code + ' ">Cliente</a></h1>' +
            '<div id="bodyContent">' +
                '<p>' +
                    '<b>Lat: </b>' + lat + ' - <b>Lng: </b>' + lng + '<br>' +
                '</p>' +
                '<p>' +
                    '<a target="_blank" href="/ispbrain/customers/view/' + customer.code + '"><b>Cliente: </b>' + customer.name + ' - <b>' + sessionPHP.afip_codes.doc_types[customer.doc_type] + ':</b> ' + customer.ident + ' - <b>Código:</b> ' + customer.code + '</a><br>' +
                    '<b>Domicilio: </b>' + customer.address + '<br>' +
                '</p>' +
            '</div>' +
        '</div>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });

        infoWindows.push(infowindow);

		marker = new google.maps.Marker({
			position: location,
			map: map,
		    animation: google.maps.Animation.DROP,
			title: '',
			icon: pinSymbol('green'),
			type: 'customers'
		});
		marker.setMap(map);
		markers.push(marker);

        if (!markerGroups['customers']) markerGroups['customers'] = [];
        markerGroups['customers'].push(marker);

        google.maps.event.addListener(marker,'click', (function(marker, contentString, infowindow) { 
            return function() {
                removeAnimationAllMarkers(marker);
                closeAllInfoWindows();
                infowindow.setContent(contentString);
                infowindow.open(map, marker);
            };
        })(marker, contentString, infowindow)); 

        google.maps.event.addListener(infowindow,'closeclick',function() {
            removeAnimationAllMarkers(null);
        });
	}

	function addServicePendingMarker(id, location, address, service_name, lat, lng) {

	    var contentString = 
	    '<div id="content">' +
            '<div id="siteNotice">' +
            '</div>' +
            '<h1 id="firstHeading" class="firstHeading"><a href="javascript:void(0)">Servicio Pendiente</a></h1>'+
            '<div id="bodyContent">' +
                '<p>' +
                    '<b>Lat: </b>' + lat + ' - <b>Lng: </b>' + lng + '<br>' +
                '</p>' +
                '<p>' +
                    '<b>Servicio: </b>' + service_name + '<br>' +
                '</p>' +
                '<p>' +
                    '<b>Domicilio: </b>' + address + '<br>' +
                '</p>' +
            '</div>' +
        '</div>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });

        infoWindows.push(infowindow);

		marker = new google.maps.Marker({
			position: location,
			map: map,
		    animation: google.maps.Animation.DROP,
			title: '',
			icon: pinSymbol('red'),
			type: 'services_pending'
		});
		marker.setMap(map);	
		markers.push(marker);
		
		 map.setCenter(location);

        if (!markerGroups['services_pending']) markerGroups['services_pending'] = [];
        markerGroups['services_pending'].push(marker);

        google.maps.event.addListener(marker,'click', (function(marker, contentString, infowindow) { 
            return function() {
                removeAnimationAllMarkers(marker);
                closeAllInfoWindows();
                infowindow.setContent(contentString);
                infowindow.open(map, marker);
            };
        })(marker, contentString, infowindow));

        google.maps.event.addListener(infowindow,'closeclick',function() {
            removeAnimationAllMarkers(null);
        });
	}

	function toggleGroup(type) {
        for (var i = 0; i < markerGroups[type].length; i++) {
            var marker = markerGroups[type][i];
            if (!marker.getVisible()) {
                marker.setVisible(true);
            } else {
                marker.setVisible(false);
            }
        }
    }

	function toggleBounce(marker) {
        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }

    function pinSymbol(color) {
        return {
            path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z',
            fillColor: color,
            fillOpacity: 1,
            strokeColor: '#000',
            strokeWeight: 2,
            scale: 1
        };
    }

    function closeAllInfoWindows() {
        for (var i = 0; i < infoWindows.length; i++) {
            infoWindows[i].close();
        }
    }

    function removeAnimationAllMarkers(marker) {
        for (var i = 0; i < markers.length; i++) {
            if (markers[i].type != 'controller') {
                 markers[i].setAnimation(null);
            }
        }
        if (marker != null && marker.type != 'controller') {
            toggleBounce(marker);
        }
    }

    $(document).ready(function() {

        $('#table-connections').removeClass('display');

	    table_connections = $('#table-connections').DataTable({
		    "order": [[ 0, 'desc' ]],
            "deferRender": true,
            "bFilter": false,
	        "ajax": {
                "url": "/ispbrain/connections/get_connection_customer_from_ticket.json",
                "dataSrc": "connections",
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "customer_code": ticket_selected && ticket_selected.customer ? ticket_selected.customer.code : 0
                    });
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
		    "autoWidth": true,
		    "scrollX": true,
		    "paging": false,
		    "columns": [
                {
                    "data": "controller.name",
                },
                {
                    "data": "service.name"
                },
                {
                    "data": "ip",
                },
                {
                    "data": "address"
                },
                {
                    "data": "area.name"
                },
            ],
		    "columnDefs": [
                { 
                    "class": "left", targets: [1, 3]
                },
            ],
             "createdRow" : function( row, data, index ) {
                row.id = data.id;
                if (ticket_selected.connection && data.id == ticket_selected.connection.id) {
                    $(row).addClass('selected');
                }
            },
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
		});

		$('#customer-info-popup').on('shown.bs.modal', function (e) {
            table_connections.ajax.reload();
        });

        $('#myTab a').click(function (e) {

            e.preventDefault();

            var target = $(this).data('target');
            $('#myTab a.active').removeClass('active');

            $('#myTabContent .active').fadeOut(100, function() {
                $('#myTabContent .active').removeClass('active');

                $(target).fadeIn(100, function() {
                    $(target).addClass('active show');
                    $(target + '-tab').addClass('active');
                    $(target + '-tab').trigger('shown.bs.tab');
                });
            });
        });
    });

</script>
