<style type="text/css">

    #map {
		height: 350px;
	}

	.modal {
	    overflow-x: hidden;
	    overflow-y: auto;
	}

    .description {
        height: 145px;
        width: 100%;
        margin-bottom: 20px;
    }

    .my-hidden {
        display: none;
    }

    .actions-tickets {
        border: 1px solid #cccccc59;
        padding: 18px;
        font-family: sans-serif;
    }

    .view-tickets {
        border: 1px solid #cccccc59;
        padding: 18px;
        font-family: sans-serif;
    }

    .card-header {
        font-size: 12px;
    }

    .badge {
        font-size: 54%;
    }

    .btn-default.active {
        color: #f05f40;
        background-color: #e6e6e6;
        border-color: #f05f40;
    }

    .table-tickets-records th {
        background-color: #607D8B !important;
        color: white !important;
    }

    #digital-signature-description {
        height: 30px;
    }

    .table-hover tbody tr:hover {
        background-color: white !important;
    }

    .table-hover tbody tr.selectable:hover {
        background-color: rgba(105, 104, 104, 0.28) !important;
    }

    .col-description p {
        margin: 0 !important;
    }

    table .img-thumbnail {
        max-width: 50px !important;
    }

    .ticket_description {
        width: 100%;        
    }
    
    #signature > div > img {
        display: none !important;
    }

</style>

<script type="text/javascript">

    function opemModalImage(path) {
        $('#modal-photo-view #foto-view-img').attr('src', path );
        $('#modal-photo-view #foto-view-source').attr('srcset', path );
        $('#modal-photo-view').modal('show');
    }

</script>

<div class="row" id="container-tickets">
    <div class="col-xl-12 col-md-12 col-sm-12">
        <table class="table table-hover table-bordered" id="table-tickets">
           <thead>
               <tr>
                    <th></th>
                    <th>#</th>
                    <th>Creado</th>
                    <th>Modificado</th>
                    <th>Usuario</th>
                    <th>Estado</th>
                    <th>Categoría</th>
                    <th>Cód.</th>
                    <th>Cliente</th>
                    <th>Dom. cliente</th>
                    <th>Inicio</th>
                    <th>Asignado</th>
                    <th>Título</th>
                    <th>Área</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5">

        <?php
            echo $this->Html->link(
                '<span class="fas fa-sync-alt" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Actualizar tabla',
                'class' => 'btn btn-default btn-update-table ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon fas fa-search" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Buscar por columnas',
                'class' => 'btn btn-default btn-search-column ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-pdf" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar PDF',
                'class' => 'btn btn-default btn-export-pdf ml-1',
                'escape' => false
            ]);
        ?>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-info-customer',
        'name' =>  'Ficha de Cliente',
        'icon' =>  'icon-profile mr-2',
        'type' =>  'btn-info'
    ];

    $buttons[] = [
        'id'   =>  'btn-restore',
        'name' =>  'Restaurar',
        'icon' =>  'icon-undo2 mr-2',
        'type' =>  'btn-success'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete',
        'name' =>  'Eliminar',
        'icon' =>  'fa fa-times mr-2',
        'type' =>  'btn-danger'
    ];

    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-tickets']);
    echo $this->element('actions_tickets', ['modal'=> 'modal-actions-tickets', 'title' => '<span class="lbl-ticket-title"></span>', 'buttons' => $buttons]);
?>

<div class="modal fade" id="customer-info-popup" tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Info Cliente</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <div class="row">

                    <div class="col-12 col-xl-3">
                        <div class="input-group m-1">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Código</span>
                            </div>
                            <input type="text" class="form-control text-right" id="info_customer_code" readonly>
                        </div>
                    </div>

                    <div class="col-12 col-xl-4">

                        <div class="input-group m-1">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Documento</span>
                            </div>
                            <input type="text" class="form-control text-right" id="info_customer_doc" readonly> 
                        </div>
                    </div>

                    <div class="col-12 col-xl-5">

                         <div class="input-group m-1">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Nombre</span>
                            </div>
                            <input type="text" class="form-control text-right" id="info_customer_name" readonly>
                        </div>
                    </div>

                    <div class="col-12 col-xl-4 ">

                        <div class="input-group m-1">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Tel.</span>
                            </div>
                            <input type="text" class="form-control text-right" id="info_phone" readonly>
                        </div>
                    </div>

                    <div class="col-12 col-xl-4 ">

                        <div class="input-group m-1">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Lat.</span>
                            </div>
                            <input type="text" class="form-control text-right" id="info_lat" readonly>
                        </div>
                    </div>

                    <div class="col-12 col-xl-4 ">

                        <div class="input-group m-1">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Long.</span>
                            </div>
                            <input type="text" class="form-control text-right" id="info_lng" readonly>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-12">

                        <legend style="margin-top: 15px;"><?= __('Conexión del Cliente') ?></legend>
                        <table class="table table-bordered table-hover" id="table-connections">
                            <thead>
                                <tr>
                                    <th><?= __('Controlador') ?></th>
                                    <th><?= __('Servicio') ?></th>
                                    <th><?= __('IP') ?></th>
                                    <th><?= __('Domicilio') ?></th>
                                    <th><?= __('Área') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="row mb-3" id="service-pending-block" >
                    <div class="col-xl-12">

                        <legend style="margin-top: 15px;"><?= __('Servicio Pendiente del Cliente') ?></legend>
                        <div class="row">
                            <div class="col-12 col-xl-5">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Servicio</span>
                                    </div>
                                    <input type="text" class="form-control text-right" id="info_service_name_service_pending" readonly>
                                </div>
                            </div>
                            <div class="col-12 col-xl-5">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Domicilio</span>
                                    </div>
                                    <input type="text" class="form-control text-right" id="info_address_service_pending" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row"  style="height: 350px;">
                    <div class="col-xl-12" style="height: 350px;">
                        <div id="map"  style="height: 350px;"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<?php
    echo $this->element('modal_preloader');
?>

<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2K3zoK46JybWPzQbhfQQqIIbdZ_3WcQs">
</script>

<script type="text/javascript">

    var map = null;
    var geocoder = null;
    var marker = null;
    var markers = [];
    var infoWindows = [];
    var curr_pos_scroll = null;
    var areas = null;

    var markerGroups = {
        "connections": [],
        "customers": [],
        "services_pending": []
    };

	function initMap(connections_coords, customers_coords, services_pending_coords) {

		geocoder = new google.maps.Geocoder;

		var myLatlng = {lat: sessionPHP.paraments.system.map.lat, lng: sessionPHP.paraments.system.map.lng};

		var mapOptions = {
			zoom: 15,
			center: myLatlng,
			mapTypeId: 'hybrid'   // roadmap, satellite, hybrid, terrain 
		};

		map = new google.maps.Map(document.getElementById('map'), mapOptions);

		$.each(connections_coords, function(i, val) {
		    if (val.lat != null && val.lng != null) {
		        var id = val.id;
		        var ip = val.ip;
		        var init = { lat: val.lat, lng: val.lng };
                var latLng = new google.maps.LatLng(init.lat, init.lng);
                var address = val.address;
                addMarker(id, ip, latLng, address, val.lat, val.lng);
		    }
		});

		$.each(customers_coords, function(i, val) {

		    if (val.lat != null && val.lng != null) {
		        var customer = {
		            code: val.code,
		            name: val.name,
		            doc_type: val.doc_type,
		            ident: val.ident,
		            address: val.address
		        }

		        var init = { lat: val.lat, lng: val.lng };
                var latLng = new google.maps.LatLng(init.lat, init.lng);
                addCustomerMarker(latLng, customer, val.lat, val.lng);
		    }
		});

		$.each(services_pending_coords, function(i, val) {
		    if (val.lat != null && val.lng != null) {
		        var id = val.id;
		        var init = { lat: val.lat, lng: val.lng };
                var latLng = new google.maps.LatLng(init.lat, init.lng);
                var address = val.address;
                var service_name = val.service_name;
                addServicePendingMarker(id, latLng, address, service_name, val.lat, val.lng);
		    }
		});

		// a  div where we will place the buttons
        ctrl = $('<div/>').css({background:'#fff',
            border:'1px solid #000',
            padding:'4px',
            margin:'2px',
            textAlign:'center'
        });

        ctrl.append(
            '<ul style="text-align: left; position: relative; display: block; padding-left: 5px; padding-right: 5px; padding-top: 8px;">' +
                '<div class="form-check">' +
                    '<input onclick="toggleGroup(\'connections\')" type="checkbox" class="form-check-input" id="exampleCheck1" checked="checked">' +
                    '<label style="padding-left: 26px; padding-top: 4px; color: #0000ff !important" class="form-check-label" for="exampleCheck1"> Conexiones</label>' +
                '</div>' +
                '<div class="form-check">' +
                    '<input onclick="toggleGroup(\'customers\')" type="checkbox" class="form-check-input" id="exampleCheck2" checked="checked">' +
                    '<label style="padding-left: 26px; padding-top: 4px; color: #008000 !important" class="form-check-label" for="exampleCheck2"> Clientes</label>' +
                '</div>' +
                '<div class="form-check">' +
                    '<input onclick="toggleGroup(\'services_pending\')" type="checkbox" class="form-check-input" id="exampleCheck3" checked="checked">' +
                    '<label style="padding-left: 26px; padding-top: 4px; color: #ff0000 !important" class="form-check-label" for="exampleCheck3"> Servicio Pendiente</label>' +
                '</div>' +
            '</ul>'
        );

        //use the buttons-div as map-control 
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(ctrl[0]);
	}

	function addMarker(id, ip, location, address, lat, lng) {

	    var contentString = 
	    '<div id="content">' +
            '<div id="siteNotice">' +
            '</div>' +
            '<h1 id="firstHeading" class="firstHeading"><a target="_blank" href="/ispbrain/connections/view/' + id +' ">Conexión - IP: ' + ip + '</a></h1>'+
            '<div id="bodyContent">' +
                '<p>' +
                    '<b>Lat: </b>' + lat + ' - <b>Lng: </b>' + lng + '<br>' +
                '</p>' +
                '<p>' +
                    '<b>Domicilio: </b>' + address + '<br>' +
                '</p>' +
            '</div>' +
        '</div>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });

        infoWindows.push(infowindow);

		marker = new google.maps.Marker({
			position: location,
			map: map,
		    animation: google.maps.Animation.DROP,
			title: '',
			icon: pinSymbol('blue'),
			type: 'connections'
		});
		marker.setMap(map);	
		markers.push(marker);

        if (!markerGroups['connections']) markerGroups['connections'] = [];
        markerGroups['connections'].push(marker);

        google.maps.event.addListener(marker,'click', (function(marker, contentString, infowindow) { 
            return function() {
                removeAnimationAllMarkers(marker);
                closeAllInfoWindows();
                infowindow.setContent(contentString);
                infowindow.open(map, marker);
            };
        })(marker, contentString, infowindow));

        google.maps.event.addListener(infowindow,'closeclick',function() {
            removeAnimationAllMarkers(null);
        });
	}

	function addCustomerMarker(location, customer, lat, lng) {

	    var contentString = 
	    '<div id="content">' +
            '<div id="siteNotice">' +
            '</div>' +
            '<h1 id="firstHeading" class="firstHeading"><a target="_blank" href="/ispbrain/customers/view/' +  customer.code + ' ">Cliente</a></h1>' +
            '<div id="bodyContent">' +
                '<p>' +
                    '<b>Lat: </b>' + lat + ' - <b>Lng: </b>' + lng + '<br>' +
                '</p>' +
                '<p>' +
                    '<a target="_blank" href="/ispbrain/customers/view/' + customer.code + '"><b>Cliente: </b>' + customer.name + ' - <b>' + sessionPHP.afip_codes.doc_types[customer.doc_type] + ':</b> ' + customer.ident + ' - <b>Código:</b> ' + customer.code + '</a><br>' +
                    '<b>Domicilio: </b>' + customer.address + '<br>' +
                '</p>' +
            '</div>' +
        '</div>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });

        infoWindows.push(infowindow);

		marker = new google.maps.Marker({
			position: location,
			map: map,
		    animation: google.maps.Animation.DROP,
			title: '',
			icon: pinSymbol('green'),
			type: 'customers'
		});
		marker.setMap(map);
		markers.push(marker);

        if (!markerGroups['customers']) markerGroups['customers'] = [];
        markerGroups['customers'].push(marker);

        google.maps.event.addListener(marker,'click', (function(marker, contentString, infowindow) { 
            return function() {
                removeAnimationAllMarkers(marker);
                closeAllInfoWindows();
                infowindow.setContent(contentString);
                infowindow.open(map, marker);
            };
        })(marker, contentString, infowindow)); 

        google.maps.event.addListener(infowindow,'closeclick',function() {
            removeAnimationAllMarkers(null);
        });
	}

	function addServicePendingMarker(id, location, address, service_name, lat, lng) {

	    var contentString = 
	    '<div id="content">' +
            '<div id="siteNotice">' +
            '</div>' +
            '<h1 id="firstHeading" class="firstHeading"><a href="javascript:void(0)">Servicio Pendiente</a></h1>'+
            '<div id="bodyContent">' +
                '<p>' +
                    '<b>Lat: </b>' + lat + ' - <b>Lng: </b>' + lng + '<br>' +
                '</p>' +
                '<p>' +
                    '<b>Servicio: </b>' + service_name + '<br>' +
                '</p>' +
                '<p>' +
                    '<b>Domicilio: </b>' + address + '<br>' +
                '</p>' +
            '</div>' +
        '</div>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });

        infoWindows.push(infowindow);

		marker = new google.maps.Marker({
			position: location,
			map: map,
		    animation: google.maps.Animation.DROP,
			title: '',
			icon: pinSymbol('red'),
			type: 'services_pending'
		});
		marker.setMap(map);	
		markers.push(marker);
		
		 map.setCenter(location);

        if (!markerGroups['services_pending']) markerGroups['services_pending'] = [];
        markerGroups['services_pending'].push(marker);

        google.maps.event.addListener(marker,'click', (function(marker, contentString, infowindow) { 
            return function() {
                removeAnimationAllMarkers(marker);
                closeAllInfoWindows();
                infowindow.setContent(contentString);
                infowindow.open(map, marker);
            };
        })(marker, contentString, infowindow));

        google.maps.event.addListener(infowindow,'closeclick',function() {
            removeAnimationAllMarkers(null);
        });
	}

	function toggleGroup(type) {
        for (var i = 0; i < markerGroups[type].length; i++) {
            var marker = markerGroups[type][i];
            if (!marker.getVisible()) {
                marker.setVisible(true);
            } else {
                marker.setVisible(false);
            }
        }
    }

	function toggleBounce(marker) {
        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }

    function pinSymbol(color) {
        return {
            path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z',
            fillColor: color,
            fillOpacity: 1,
            strokeColor: '#000',
            strokeWeight: 2,
            scale: 1
        };
    }

    function closeAllInfoWindows() {
        for (var i = 0; i < infoWindows.length; i++) {
            infoWindows[i].close();
        }
    }

    function removeAnimationAllMarkers(marker) {
        for (var i = 0; i < markers.length; i++) {
            if (markers[i].type != 'controller') {
                 markers[i].setAnimation(null);
            }
        }
        if (marker != null && marker.type != 'controller') {
            toggleBounce(marker);
        }
    }

    $('#date-ticket-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: '',
        format: 'DD/MM/YYYY'
    });

    $('#date-record-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: '',
        format: 'DD/MM/YYYY'
    });

    var tickets_categories = <?php echo json_encode($tickets_categories); ?>;
    var tickets_status = <?php echo json_encode($tickets_status); ?>;
    var status_ti = <?php echo json_encode($status_ti); ?>;
    var categories = <?php echo json_encode($categories); ?>;

    var available_status = 1;
    var assigned_status;
    var finished_status;

    var tickets = [];
    var ticket_selected = null;

    var status_selected = 0;
    var category_selected = -1;

    var editor = null;

    var table_connections = null;
    var table_tickets = null;
    var users;

    function format(d) {

        //ordena historial del ticket por fecha created de mayor a menor
        d.tickets_records.sort(function(a,b) {
            // Turn your strings into dates, and then subtract them
            // to get a value that is either negative, positive, or zero.
            return new Date(b.created) - new Date(a.created);
        });

        var html = '<br><table class="table table-bordered table-sm table-tickets-records mr-5">'
            + '<thead>'
                + '<tr>'
                  + '<th>Fecha</th>'
                  + '<th>Acción</th>'
                  + '<th>Descripción</th>'
                  + '<th>Imagen</th>'
                  + '<th>Usuario</th>'
                + '</tr>'
              + '</thead>'
            + '<tbody>';

        $.each(d.tickets_records, function(i, ticket_record) {
            var created = '';
            if (ticket_record.created) {
                created = ticket_record.created.split('T')[0];
                created = created.split('-');
                created = created[2] + '/' + created[1] + '/' + created[0];
            }
            var short_action = ticket_record.short_description ? ticket_record.short_description : '';

            var image = '';

            if (ticket_record.image) {

                image = '<picture onclick="opemModalImage(\'/ispbrain/ticket_image/' + ticket_record.image + '\')" ><source srcset="" type="image/svg+xml"><img src="/ispbrain/ticket_image/' + ticket_record.image +'" class="img-fluid img-thumbnail" alt="..."></picture>';

            } else {
                image = '';
            }

            html +=
            '<tr>'
              + '<td>' + created + '</td>'
              + '<td>' + short_action + '</td>'
              + '<td>' + ticket_record.description + '</td>'
              + '<td style="width: 36px; height:  20px;"' + image + '</td>'
              + '<td>' + ticket_record.user.name + '</td>'
            + '</tr>';
        });

        html += '</tbody>'
            + '</table><br>';

        return html;
    }

    function setTicketSelect(id) {
        $.each(tickets, function(i, ticket) {
            if (ticket.id == id) {
                ticket_selected = ticket;
            }
        });
    }

    $(document).ready(function() {

        $('#start-task-datetimepicker').datetimepicker({
            locale: 'es',
            format: 'DD/MM/YYYY HH:mm'
        });

        users = <?= json_encode($users) ?>;
        areas = <?= json_encode($areas) ?>; 

        table_tickets = $('#table-tickets').DataTable({
    	    "order": [[ 1, 'desc' ]],
    	    "autoWidth": true,
    	    "scrollY": '450px',
    	    "scrollCollapse": true,
    	    "scrollX": true,
    	    "sWrapper":  "dataTables_wrapper dt-bootstrap4",
            "processing": true,
            "serverSide": true,
		    "ajax": {
                "url": "/ispbrain/tickets/get_tickets_from_table.json",
                "dataFilter": function(data) {
                    var json = $.parseJSON(data);
                    return JSON.stringify(json.response);
                },
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "status": 0,
                        "archived": 1
                    });
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "drawCallback": function(c) {
                var flag = true;
            	switch (c.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
            	    if (table_tickets) {

                        var tableinfo = table_tickets.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }

                        if (curr_pos_scroll) {
                            $(table_tickets.settings()[0].nScrollBody).scrollTop(curr_pos_scroll.top);
                            $(table_tickets.settings()[0].nScrollBody).scrollLeft(curr_pos_scroll.left);
                        }
                    }
            	}
            },
            "columns": [
                {
                    "className": 'details-control ',
                    "orderable": false,
                    "data":      'tickets_records',
                    "render": function ( data, type, full, meta ) {
                        var num = data.length;
                        if (num > 0) {
                            return "<a href='javascript:void(0)' class='grey'>"
                                + "<span class='icon-plus' aria-hidden='true'>"
                                + "</span>"
                                + "</a>";
                        }

                        return '';
                    },
                    "width": '3%'
                },
                { 
                    "data": "id",
                    "type": "integer"
                },
                { 
                    "data": "created",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                { 
                    "data": "modified",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                { 
                    "data": "user_id",
                    "type": "options",
                    "options": users,
                    "render": function ( data, type, row ) {
                        var u = "";
                        if (typeof data !== "undefined") {
                            $.each(users, function( index, value ) {
                                if (data == index) {
                                    u = value;
                                }
                            });
                        }
                        return u;
                    }
                },
                { 
                    "data": "status.name",
                    "type": "options",
                    "options": status_ti,
                },
                {
                    "data": "category.name",
                    "type": "options",
                    "options": categories,
                    "render": function ( data, type, row ) {
                        return '<span class="badge" id="category" style="font-size: 100%; color: ' + row.category.color_text + '; background-color: ' + row.category.color_back + '">' + data + '</span>';
                    }
                },
                { 
                    "data": "customer",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        var customer = "";
                        if (data) {
                            customer = pad(data.code, 5);
                        }
                        return customer;
                    }
                },
                { 
                    "data": "customer",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        var customer = "";
                        if (data) {
                            customer = data.name
                        }
                        return customer;
                    }
                },
                { 
                    "data": "customer",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        var customer = "";
                        if (data) {
                            customer = data.address
                        }
                        return customer;
                    }
                },
                { 
                    "data": "start_task",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            var created_2 = data.split('T')[1].split('-')[0].split(':');

                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0] + ' ' + created_2[0] + ':' + created_2[1] + ' hs';
                        }
                    }
                },
                { 
                    "data": "asigned_user_id",
                    "type": "options",
                    "options": users,
                    "render": function ( data, type, row ) {
                        var u = "";
                        if (typeof data !== "undefined") {
                            $.each(users, function( index, value ) {
                                if (data == index) {
                                    u = value;
                                }
                            });
                        }
                        return u;
                    }
                },
                {
                    "class": "col-description",
                    "data": "title",
                    "type": "string",
                    "render": function ( data, type, row ) {
                     
                        return data.replace('&nbsp;', '');
                    }
                },
                {
                    "data": "area_id",
                    "type": "options",
                    "options": areas,
                    "render": function ( data, type, row ) {
                        var u = "";
                        if (typeof data !== "undefined" && data != null) {
                            u = areas[data];
                        }
                        return u;
                    }
                },
            ],
            "columnDefs": [
                { 
                    "visible": true
                },
                { 
                    "class": "left", 
                    "width": "15%",
                    "targets": [8]
                },
            ],
            "createdRow" : function( row, data, index ) {
                
                $(row).addClass('selectable');
                
                row.id = data.id;
            },
    	    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
    	});

		$('#table-tickets_filter').closest('div').before($('#btns-tools').contents());

		$('#table-tickets').on( 'init.dt', function () {
            createModalSearchColumn(table_tickets, '.modal-search-columns-tickets');
        });

        $('#btns-tools').show();

		$('#table-tickets tbody').on('click', 'td.details-control', function () {

            if (!$(this).closest('tr').find('.dataTables_empty').length) {
                table_tickets.$('tr.selected').removeClass('selected');
                $(this).closest('tr').addClass('selected');
            }

            var tr = $(this).closest('tr');
            var row = table_tickets.row( tr );

            if ( row.child.isShown() ) {
                row.child.hide();
                tr.removeClass('shown');
                $(this).find('span').removeClass('icon-minus');
                $(this).find('span').addClass('icon-plus');
            }
            else {
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
                $(this).find('span').removeClass('icon-plus');
                $(this).find('span').addClass('icon-minus');
            }
        });

    	$('#table-tickets tbody').on( 'click', 'td', function (e) {

            if (!$(this).closest('tr').find('.dataTables_empty').length) {

                if (!$(this).hasClass('details-control')) {

                    table_tickets.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    ticket_selected = table_tickets.row( this ).data();

                    if (typeof ticket_selected !== "undefined") {
                        
                        $('.lbl-ticket-title').html('Ticket: #' + ticket_selected.id);
    
                        if (ticket_selected.asigned_user_id != null) {
                            $('.lbl-user-assigned-name').text('Reasignar Usuario');
                        } else {
                            $('.lbl-user-assigned-name').text('Asignar Usuario');
                        }

                		if (ticket_selected.customer) {
                		    $('#btn-info-customer').show();
                		} else {
                		    $('#btn-info-customer').hide();
                		}

                		$('.lbl-ticket-title-active').html("");
                		$('.lbl-ticket-description-active').html("");

                		$('.lbl-ticket-title-active').html(ticket_selected.title);
                		$('.lbl-ticket-description-active').html(ticket_selected.description);

                        $('.modal-actions-tickets').modal('show');
                    }
                }
            }
        });

        $('#table-connections').removeClass('display');

	    table_connections = $('#table-connections').DataTable({
		    "order": [[ 0, 'desc' ]],
            "deferRender": true,
            "bFilter": false,
	        "ajax": {
                "url": "/ispbrain/connections/get_connection_customer.json",
                "dataSrc": "connections",
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "customer_code": ticket_selected && ticket_selected.customer ? ticket_selected.customer.code : 0,
                        "deleted": 0
                    });
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
		    "autoWidth": true,
		    "scrollX": true,
		    "paging": false,
		    "columns": [
                {
                    "data": "controller.name",
                },
                {
                    "data": "service.name"
                },
                {
                    "data": "ip",
                },
                {
                    "data": "address"
                },
                {
                    "data": "area.name"
                },
            ],
		    "columnDefs": [
                { 
                    "class": "left", targets: [1, 3]
                },
            ],
             "createdRow" : function( row, data, index ) {
                row.id = data.id;
                if (ticket_selected.connection && data.id == ticket_selected.connection.id) {
                    $(row).addClass('selected');
                }
            },
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
		});

        $('#btn-info-customer').click(function() {

            if (ticket_selected.customer) {

                var customers_coords = [];
                var doc_type_name = sessionPHP.afip_codes.doc_types[ticket_selected.customer.doc_type];
                $('#customer-info-popup #info_customer_code').val(pad(ticket_selected.customer.code, 5))
                $('#customer-info-popup #info_customer_name').val(ticket_selected.customer.name.toUpperCase());
                $('#customer-info-popup #info_customer_doc').val(doc_type_name + ' ' + ticket_selected.customer.ident);
                $('#customer-info-popup #info_phone').val(ticket_selected.customer.phone);

                var lat = ticket_selected.customer.lat ? ticket_selected.customer.lat : '';
                var lng = ticket_selected.customer.lng ? ticket_selected.customer.lng : '';
                if (ticket_selected.connection) {
                    lat = ticket_selected.connection.lat ? ticket_selected.connection.lat : lat;
                    lng = ticket_selected.connection.lng ? ticket_selected.connection.lng : lng;
                }
                $('#customer-info-popup #info_lat').val(lat);
                $('#customer-info-popup #info_lng').val(lng);

                var customer_coords = {
                    code: ticket_selected.customer.code,
		            name: ticket_selected.customer.name,
		            doc_type: ticket_selected.customer.doc_type,
		            ident: ticket_selected.customer.ident,
		            lat: ticket_selected.customer.lat,
		            lng: ticket_selected.customer.lng,
		            address: ticket_selected.customer.address
                };
                customers_coords.push(customer_coords);

                var connections_coords = [];
                if (ticket_selected.connection) {

                    var connection_coords = {
                        id: ticket_selected.connection.id,
    		            lat: ticket_selected.connection.lat,
    		            lng: ticket_selected.connection.lng,
    		            ip: ticket_selected.connection.ip,
    		            address: ticket_selected.connection.address
                    };
                    connections_coords.push(connection_coords);
                }

                var services_pending_coords = [];
                if (ticket_selected.service_pending) {

                    $('#service-pending-block').removeClass('my-hidden');
                    $('#customer-info-popup #info_address_service_pending').val(ticket_selected.service_pending.address);
                    $('#customer-info-popup #info_service_name_service_pending').val(ticket_selected.service_pending.service_name);

                    var service_pending_coords = {
    		            lat: ticket_selected.service_pending.lat,
    		            lng: ticket_selected.service_pending.lng,
    		            address: ticket_selected.service_pending.address,
    		            service_name: ticket_selected.service_pending.service_name
                    };
                    services_pending_coords.push(service_pending_coords);
                } else {
                    $('#customer-info-popup #info_address_service_pending').val("");
                    $('#customer-info-popup #info_service_name_service_pending').val("");
                    $('#service-pending-block').addClass('my-hidden');
                }

                initMap(connections_coords, customers_coords, services_pending_coords);

                $('.modal-actions-tickets').modal('hide');
                $('#customer-info-popup').modal('show');
            }

        });

        $('#customer-info-popup').on('shown.bs.modal', function (e) {
            table_connections.ajax.reload();
        });

		$('#btn-photo').click(function() {
            $('.modal-actions-tickets').modal('hide');
            $('#ticket_id_photo').val(ticket_selected.id);
            $('#description_photo').val('');
            
            $('#modal-photo').modal('show');
        });

        $('.btn-update-table').click(function() {
             if (table_tickets) {
                table_tickets.ajax.reload();
            }
        });

        $('.btn-search-column').click(function() {
            $('.modal-search-columns-tickets').modal('show');
        });

        $(".btn-export").click(function() {
            bootbox.confirm('Se descargará un archivo Excel', function(result) {
                if (result) {
                    $('#table-tickets').tableExport({tableName: 'tickets', type:'excel', escape:'false'});
                }
            }).find("div.modal-content").addClass("confirm-width-md");
        });

        $(".btn-export-pdf").click(function() {

            var tickets = table_tickets.data().toArray();

            if (tickets.length > 0) {
		        var action = "/ispbrain/Tickets/ticketExportPDF/";
                $('body')
                    .append( $('<form/>').attr({'action': action, 'method': 'post', 'target': '_blank', 'id': 'replacer_pdf'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'tickets', 'value': JSON.stringify(tickets)}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer_pdf').submit();

                  $('#replacer_pdf').remove();

		    } else {
		        generateNoty('warning', 'Debe generar una búsqueda de Tickets para poder exportar los mismos');
		    }
        });

        $('#btn-restore').click(function() {
            var text = "¿Está Seguro que desea retaurar el Ticket?";
            var id = ticket_selected.id;

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/tickets/restore/' + id, 'method': 'post', 'id': 'replacer_restore'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer_restore').submit();
                }
            });
        });

        $('#btn-delete').click(function() {

            var text = "¿Está Seguro que desea eliminar el Ticket?";
            var id = ticket_selected.id;

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/tickets/delete/' + id, 'method': 'post', 'id': 'replacer_delete'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer_delete').submit();
                }
            });
        });
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
