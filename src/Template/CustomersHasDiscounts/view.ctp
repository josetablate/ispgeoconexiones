<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CustomersHasDiscount $customersHasDiscount
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Customers Has Discount'), ['action' => 'edit', $customersHasDiscount->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Customers Has Discount'), ['action' => 'delete', $customersHasDiscount->id], ['confirm' => __('Are you sure you want to delete # {0}?', $customersHasDiscount->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Customers Has Discounts'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Customers Has Discount'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Debts'), ['controller' => 'Debts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Debt'), ['controller' => 'Debts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Invoices'), ['controller' => 'Invoices', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Invoice'), ['controller' => 'Invoices', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Connections'), ['controller' => 'Connections', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Connection'), ['controller' => 'Connections', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="customersHasDiscounts view large-9 medium-8 columns content">
    <h3><?= h($customersHasDiscount->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Code') ?></th>
            <td><?= h($customersHasDiscount->code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Description') ?></th>
            <td><?= h($customersHasDiscount->description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Debt') ?></th>
            <td><?= $customersHasDiscount->has('debt') ? $this->Html->link($customersHasDiscount->debt->id, ['controller' => 'Debts', 'action' => 'view', $customersHasDiscount->debt->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $customersHasDiscount->has('user') ? $this->Html->link($customersHasDiscount->user->username, ['controller' => 'Users', 'action' => 'view', $customersHasDiscount->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Invoice') ?></th>
            <td><?= $customersHasDiscount->has('invoice') ? $this->Html->link($customersHasDiscount->invoice->id, ['controller' => 'Invoices', 'action' => 'view', $customersHasDiscount->invoice->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Connection') ?></th>
            <td><?= $customersHasDiscount->has('connection') ? $this->Html->link($customersHasDiscount->connection->id, ['controller' => 'Connections', 'action' => 'view', $customersHasDiscount->connection->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Period') ?></th>
            <td><?= h($customersHasDiscount->period) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($customersHasDiscount->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Price') ?></th>
            <td><?= $this->Number->format($customersHasDiscount->price) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sum Price') ?></th>
            <td><?= $this->Number->format($customersHasDiscount->sum_price) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sum Tax') ?></th>
            <td><?= $this->Number->format($customersHasDiscount->sum_tax) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Total') ?></th>
            <td><?= $this->Number->format($customersHasDiscount->total) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Customer Code') ?></th>
            <td><?= $this->Number->format($customersHasDiscount->customer_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Seating Number') ?></th>
            <td><?= $this->Number->format($customersHasDiscount->seating_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tax') ?></th>
            <td><?= $this->Number->format($customersHasDiscount->tax) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($customersHasDiscount->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($customersHasDiscount->modified) ?></td>
        </tr>
    </table>
</div>
