<div class="row">

    <div class="col-sm-12 col-md-6 col-lg-5 col-xl-4">
    <?php if ($presale): ?>
            <?= $this->element('customer_info', ['presale' => $presale, 'paymentMethods' => $paymentMethods ]) ?>
        <?php else: ?>
            <?= $this->element('customer_seeker', ['showbtn' => true, 'listCustomers' => true]) ?>
        <?php endif; ?>
    </div>

    <div class="col-sm-12 col-md-6 col-lg-7 col-xl-8">

        <div class="card border-secondary mb-3" >
            <h5 class="card-header"> 2. Aplicar Descuento </h5>
            <div class="card-body text-secondary pb-0">

                <?= $this->Form->create($debt_form,  ['id' => 'form-apply-discount', 'class' => 'mb-0']) ?>

                <fieldset>

                <div class="row">
                    <div class="col-xl-12">
                         <div class="form-group text required">
                            <label class="control-label" for="code">Descuento <span class='fa fa-question-circle' title='Ingrese el código o nombre del descuento. &nbsp;&nbsp;Con * despliega la lista completa de Descuentos.'  aria-hidden='true' ></span></label>
                            <input type="text" required="required" maxlength="45" id="autocompleteDiscounts" disabled placeholder="Cargando Descuentos..." class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                   
                    <div class="col-md-12 col-lg-6 col-xl-3">
                        <label class="control-label" for="code">Valor</label>
                        <input type="text" name="discount_total" class="form-control" id="discount-total" readonly/>
                    </div>
                </div>
    
                <div class="row">
                    <div class="col-xl-12">
                        <?= $this->Form->input('concept', ['label' => 'Descripción', 'rows' => 2]); ?>
                    </div>
                </div>

                </fieldset>

                <?= $this->Form->end() ?> 
 
            </div>

            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <button name="btn-apply" value="" type="button" class="btn btn-success float-right" id="btn-apply"><?= $presale ? 'Cargar' : 'Aplicar' ?></button>
                    <?php if ($presale): ?>
                        <?= $this->Html->link(__('Cancelar'),["controller" => "presales", "action" => "add"], ['title' => 'Ir a la Preventa', 'class' => 'btn btn-default ml-2']) ?>
                    <?php endif; ?>
                </li>
            </ul>

        </div>
    </div>

</div>

<script type="text/javascript">

    var discount_select = null; 

    var presale = null;

    $(document).ready(function() {

        presale = <?= json_encode($presale) ?>;

        $('#card-customer-seeker').on('CUSTOMER_SELECTED', function() {
            $('#autocompleteDiscounts').focus();
        });

        $('#btn-apply').click(function() {
           $('#form-apply-discount').submit();
        });

        $('#form-apply-discount').submit(function(e) {

            e.preventDefault();

            if (presale) {
                bootbox.confirm('Confirmar', function(result) {
                    if (result) {

                        var send_data = {};  

                        send_data.discount_id = discount_select.id;
                        send_data.from = "presale";

                        var form_data = $('#form-apply-discount').serializeArray();
                        $.each(form_data, function(i, val){
                            send_data[val.name] = val.value;
                        });

                        $('body')
                            .append( $('<form/>').attr({'action': '/ispbrain/CustomersHasDiscounts/add/', 'method': 'post', 'id': 'form-block'})
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'data', 'value': JSON.stringify(send_data)}))
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                            .find('#form-block').submit();

                         $('#btn-sell').hide(); 
                    }
                });
            } else {
                if (!customer_selected) {
                    generateNoty('warning', 'Debe especificar un cliente.');
                } else if (!discount_select) {
                    generateNoty('warning', 'Debe especificar un descuento.');
                } else {
    
                    bootbox.confirm('Confirmar', function(result) {
                        if (result) {
    
                            var send_data = {};
                            send_data.from = "";
                            send_data.customer_code = customer_selected.code;
                            send_data.customer_account_code = customer_selected.account_code;
    
                            send_data.discount_id = discount_select.id;
    
                            var form_data = $('#form-apply-discount').serializeArray();
                            $.each(form_data, function(i, val) {
                                send_data[val.name] = val.value;
                            });
    
                             $('body')
                                .append( $('<form/>').attr({'action': '/ispbrain/CustomersHasDiscounts/add/', 'method': 'post', 'id': 'form-block'})
                                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'data', 'value': JSON.stringify(send_data)}))
                                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                                .find('#form-block').submit();

                             $('#btn-sell').hide(); 
                        }
                    });
                }
            }

        });

        $( "#autocompleteDiscounts" ).val('');
 
        var discounts = [];

        $.ajax({
            url: "<?= $this->Url->build(['controller' => 'Discounts', 'action' => 'getGenericAllAjax']) ?>",
            type: 'POST',
            dataType: "json",
            success: function(data) {

                if (data.discounts != false) {

                    $.each(data.discounts, function(i, val) {

                        var ui = {
                            id: val.id,
                            label: val.name +'(' + val.code + ')' + val.concept + ' *',
                            value: val.id,
                            type_value: val.type_value,
                            d_value: val.value
                        };
                        discounts.push(ui);
                    });

                    //carga el campo de autocompletado con los descuentos obtenidoss
                    $( "#autocompleteDiscounts" ).autocomplete({
                    	source: discounts,
                    	create: function( event, ui ) {
                           $( "#autocompleteDiscounts" ).attr('disabled', false);
                           $( "#autocompleteDiscounts" ).attr('placeholder', 'Seleccione el descuento.');
                        },
                        select: function( event, ui ) {

                            // discount_select = ui.item;

                            // var sum_price = discount_select.price * amount;
                            // var interest_ = parseFloat(interest) + 1;

                            // sum_price = sum_price * interest_;
                            // // var sum_tax = sum_price * aliquot;
                            // // var total = sum_price + sum_tax;
                            // var total = sum_price.toFixed(2);
                            // $('input#discount-total').val(total);
                            // var total_due = total / dues;
                            // total_due = total_due.toFixed(2);
                            // $('input#discount-total-due').val(total_due);

                            // $('#concept').html(discount_select.value);
                        }
                    }); 
                } else { //no hay descuentos

                    $( "#autocompleteDiscounts" ).addClass('placeholder-error');
                    $( "#autocompleteDiscounts" ).attr('placeholder', 'No existen Descuentos.');
                }
            },
            error: function (jqXHR) {
                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() { 
                	  window.location.href ="/ispbrain";
                	}, 3000);

                } else {
                	generateNoty('error', 'Error al obtener los Descuentos.');
                }
            }
        });
     });

</script>
