<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CustomersHasDiscount[]|\Cake\Collection\CollectionInterface $customersHasDiscounts
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Customers Has Discount'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Debts'), ['controller' => 'Debts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Debt'), ['controller' => 'Debts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Invoices'), ['controller' => 'Invoices', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Invoice'), ['controller' => 'Invoices', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Connections'), ['controller' => 'Connections', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Connection'), ['controller' => 'Connections', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="customersHasDiscounts index large-9 medium-8 columns content">
    <h3><?= __('Customers Has Discounts') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col"><?= $this->Paginator->sort('code') ?></th>
                <th scope="col"><?= $this->Paginator->sort('description') ?></th>
                <th scope="col"><?= $this->Paginator->sort('price') ?></th>
                <th scope="col"><?= $this->Paginator->sort('sum_price') ?></th>
                <th scope="col"><?= $this->Paginator->sort('sum_tax') ?></th>
                <th scope="col"><?= $this->Paginator->sort('total') ?></th>
                <th scope="col"><?= $this->Paginator->sort('debt_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('customer_code') ?></th>
                <th scope="col"><?= $this->Paginator->sort('seating_number') ?></th>
                <th scope="col"><?= $this->Paginator->sort('invoice_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('connection_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('period') ?></th>
                <th scope="col"><?= $this->Paginator->sort('tax') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($customersHasDiscounts as $customersHasDiscount): ?>
            <tr>
                <td><?= $this->Number->format($customersHasDiscount->id) ?></td>
                <td><?= h($customersHasDiscount->created) ?></td>
                <td><?= h($customersHasDiscount->modified) ?></td>
                <td><?= h($customersHasDiscount->code) ?></td>
                <td><?= h($customersHasDiscount->description) ?></td>
                <td><?= $this->Number->format($customersHasDiscount->price) ?></td>
                <td><?= $this->Number->format($customersHasDiscount->sum_price) ?></td>
                <td><?= $this->Number->format($customersHasDiscount->sum_tax) ?></td>
                <td><?= $this->Number->format($customersHasDiscount->total) ?></td>
                <td><?= $customersHasDiscount->has('debt') ? $this->Html->link($customersHasDiscount->debt->id, ['controller' => 'Debts', 'action' => 'view', $customersHasDiscount->debt->id]) : '' ?></td>
                <td><?= $customersHasDiscount->has('user') ? $this->Html->link($customersHasDiscount->user->username, ['controller' => 'Users', 'action' => 'view', $customersHasDiscount->user->id]) : '' ?></td>
                <td><?= $this->Number->format($customersHasDiscount->customer_code) ?></td>
                <td><?= $this->Number->format($customersHasDiscount->seating_number) ?></td>
                <td><?= $customersHasDiscount->has('invoice') ? $this->Html->link($customersHasDiscount->invoice->id, ['controller' => 'Invoices', 'action' => 'view', $customersHasDiscount->invoice->id]) : '' ?></td>
                <td><?= $customersHasDiscount->has('connection') ? $this->Html->link($customersHasDiscount->connection->id, ['controller' => 'Connections', 'action' => 'view', $customersHasDiscount->connection->id]) : '' ?></td>
                <td><?= h($customersHasDiscount->period) ?></td>
                <td><?= $this->Number->format($customersHasDiscount->tax) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $customersHasDiscount->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $customersHasDiscount->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $customersHasDiscount->id], ['confirm' => __('Are you sure you want to delete # {0}?', $customersHasDiscount->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
