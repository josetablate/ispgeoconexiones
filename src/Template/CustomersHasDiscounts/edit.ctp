<?php
/**
 * @var \App\View\AppView $this
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $customersHasDiscount->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $customersHasDiscount->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Customers Has Discounts'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Debts'), ['controller' => 'Debts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Debt'), ['controller' => 'Debts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Invoices'), ['controller' => 'Invoices', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Invoice'), ['controller' => 'Invoices', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Connections'), ['controller' => 'Connections', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Connection'), ['controller' => 'Connections', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="customersHasDiscounts form large-9 medium-8 columns content">
    <?= $this->Form->create($customersHasDiscount) ?>
    <fieldset>
        <legend><?= __('Edit Customers Has Discount') ?></legend>
        <?php
            echo $this->Form->control('code');
            echo $this->Form->control('description');
            echo $this->Form->control('price');
            echo $this->Form->control('sum_price');
            echo $this->Form->control('sum_tax');
            echo $this->Form->control('total');
            echo $this->Form->control('debt_id', ['options' => $debts, 'empty' => true]);
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('customer_code');
            echo $this->Form->control('seating_number');
            echo $this->Form->control('invoice_id', ['options' => $invoices, 'empty' => true]);
            echo $this->Form->control('connection_id', ['options' => $connections, 'empty' => true]);
            echo $this->Form->control('period');
            echo $this->Form->control('tax');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
