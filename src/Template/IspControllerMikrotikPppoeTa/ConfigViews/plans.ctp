<?php $this->extend('/IspControllerMikrotikPppoeTa/ConfigViews/pools'); ?>

<?php $this->start('plans'); ?>

    <div id="btns-tools-tplans">
        <div class="text-right btns-tools margin-bottom-5">
    
            <?php 
                echo $this->Html->link(
                    '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Agregar',
                    'class' => 'btn btn-default btn-add-tplan',
                    'escape' => false
                    ]);
    
                 echo $this->Html->link(
                    '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Exportar tabla',
                    'class' => 'btn btn-default btn-export-tplans ml-1',
                    'escape' => false
                    ]);
            ?>
    
        </div>
    </div>
    
     <div class="row justify-content-center mt-2">
        <div class="col-auto">
            <div class="card border-naranja">
              <div class="card-body text-secondary p-2">
                <p class="card-text text-naranja">Para que los cambios surtan efecto en el controlador, debe <a href="/ispbrain/IspControllerMikrotikPppoeTa/sync/<?=$controller->id?>"  class="font-weight-bold"  >sincronizar</a>.</p>
              </div>
            </div>
        </div>
    </div>
    
    
    <div class="row">
        <div class="col-9 mx-auto">
            
            <div class="card border-secondary mt-2">
                <div class="card-body p-1">
                    
                    <table class="table table-bordered table-hover" id="table-tplans">
                        <thead>
                            <tr>
                                <th>Conexiones</th>
                                <th>Servicio</th>
                                <th>Perfil</th>
                                <th>Pool</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="modal fade modal-add-tplan" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="gridSystemModalLabel">Agregar Plan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
    
                            <?= $this->Form->create(null, ['id' => 'form-add-plan']) ?>
                                <fieldset>
                                    <?php 
                                    
                                    echo $this->Form->input('service_id', [
                                        'options' => $servicesArray,
                                        'label' => 'Servicios', 
                                        'class' => 'selectpicker', 
                                        'data-live-search' => "true",
                                        'required' => true]);
                                        
                                    echo $this->Form->input('pool_id',['label' => 'Pool', 'required' => true]);
                                    echo $this->Form->input('profile_id',['label' => 'Perfil', 'required' => true]);
                                    echo $this->Form->input('controller_id', ['value' => $controller->id, 'type' => 'hidden']);
                                    
                                    ?>

                                </fieldset>
                                
                                <?= $this->Form->button(__('Agregar', ['id' => 'btn-submit-add-plan', 'type' => 'button'])) ?>
                                
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade modal-edit-tplan" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="gridSystemModalLabel">Ediat Plan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
    
                            <?= $this->Form->create(null, ['id' => 'form-edit-plan']) ?>
                                <fieldset>
                                    <?php 
                                    
                                    echo $this->Form->input('service_id', ['options' => $servicesArray, 'label' => 'Servicios', 'readonly' => true, 'disabled' => true ]);
                                    echo $this->Form->input('profile_id',['label' => 'Perfil', 'required' => true]);
                                    echo $this->Form->input('pool_id',['label' => 'Pool', 'required' => false]);
                                    echo $this->Form->input('controller_id', ['type' => 'hidden']);
                                    echo $this->Form->input('id', ['type' => 'hidden']);
                                    
                                    ?>

                                </fieldset>
                                
                                <?= $this->Form->button(__('Editar', ['id' => 'btn-submit-edit-plan', 'type' => 'button'])) ?>
                                
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade modal-move-tplan" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="gridSystemModalLabel">Mover Plan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
    
                            <?= $this->Form->create(null, ['id' => 'form-move-plan']) ?>
                                <fieldset>
                                    <?php 
                                    
                                    echo $this->Form->input('service_id', ['options' => $servicesArray, 'label' => 'Servicios', 'readonly' => true, 'disabled' => true ]);
                                    
                                    echo $this->Form->input('service_id_destino', ['options' => $servicesArray, 'label' => 'Servicio destino']);
                                    
                                    echo $this->Form->input('controller_id', ['type' => 'hidden']);
                                    echo $this->Form->input('id', ['type' => 'hidden']);
                                    
                                    ?>

                                </fieldset>
                                
                                <?= $this->Form->button(__('Editar', ['id' => 'btn-submit-move-plan', 'type' => 'button'])) ?>
                                
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    
    

    <!-- modal actions tplans -->
    <?php
        $buttons = [];
                             
        $buttons[] = [
            'id'   =>  'btn-edit-tplan',
            'name' =>  'Editar',
            'icon' =>  'icon-pencil2',
            'type' =>  'btn-default'
        ];
        
         $buttons[] = [
            'id'   =>  'btn-move-tplan',
            'name' =>  'Mover',
            'icon' =>  'icon-pencil2',
            'type' =>  'btn-info'
        ];

        $buttons[] = [
            'id'   =>  'btn-delete-tplan',
            'name' =>  'Eliminar',
            'icon' =>  'fas fa-exchange-alt',
            'type' =>  'btn-danger'
        ];

        echo $this->element('actions', ['modal'=> 'modal-actions-tplans', 'title' => 'Acciones', 'buttons' => $buttons ]);
    ?>


    <script type="text/javascript">
    
       var plan_selected = null;
       var table_tplans = null; 
     

        $(document).ready(function () {
            
            // $('.modal-add-tplan #service-id').html('');

            $('.modal-add-tplan #service-id').selectpicker({
                noneSelectedText: 'Seleccionar servicio'
            });
            
            // $('.modal-edit-tplan #service-id').html('');

            $('.modal-edit-tplan #service-id').selectpicker({
                noneSelectedText: 'Seleccionar servicio'
            });
            

            $('#table-tplans').removeClass('display');
            
            $('#btns-tools-tplans').hide();

    		table_tplans = $('#table-tplans').DataTable({
    		    "order": [[ 1, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/IspControllerMikrotikPppoeTa/get_plans_by_controller.json",
                    "dataSrc": "plans",
                    "data": function ( d ) {
                        return $.extend( {}, d, {
                            "controller_id": controller_id
                        });
                    },
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "connections_amount"
                    },
    		        { 
                        "data": "service.name"
                    },
                    { 
                        "data": "profile.name"
                    },
                    { 
                        "data": "pool",
                        "render": function ( data, type, row ) {
                          
                            return data ? data.name : '';
                        }
                    },
                ],
    		    "columnDefs": [
    		          
                ],
                "createdRow" : function( row, data, index ) {
                    row.id = data.id;
                },
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-tplans_wrapper .tools').append($('#btns-tools-tplans').contents());
    		
    		$('#btns-tools-tplans').show();

            $(".btn-export-tplans").click(function(){

                bootbox.confirm('Se descargara un archivo Excel', function(result) {
                    if (result) {
                        $('#table-tplans').tableExport({tableName: 'Planes', type:'excel', escape:'false'});
                    }
                });
            });

            $('#table-tplans tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length ){

                    table_tplans.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    
                    plan_selected = table_tplans.row( this ).data();

                    $('.modal-actions-tplans').modal('show');
                }
            });

            $('.btn-add-tplan').click(function() {
                
                clearFormAddPlan();
                
                $('.modal-add-tplan').modal('show');
            });
            
            $('#form-add-plan').submit(function(event){
               
                event.preventDefault();
                 
                var send_data = {};
                 
                var form_data = $(this).serializeArray();
                
                $.each(form_data, function(i, val){
                    send_data[val.name] = val.value;
                });
                
                $('#btn-submit-add-plan').hide(); 
           
                openModalPreloader("Agregando plan ...");
                
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '/ispbrain/IspControllerMikrotikPppoeTa/addPlan',
                    data:  JSON.stringify(send_data),
                    success: function(data) {
                        
                        closeModalPreloader();
                        
                        $('#btn-submit-add-plan').show(); 
                        
                        if (data.response) {
                            generateNoty('success', 'El plan se agrego correctamente');
                        } else {
                            generateNoty('error', 'Error al intentar agregar el plan');
                        }
          
                        table_tplans.ajax.reload();
                        $('.modal-add-tplan').modal('hide');
                        
                        updateCountersTitle();
                    },
                    error: function(jqXHR) {
                        if (jqXHR.status == 403) {
                        
                        	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                        
                        	setTimeout(function() { 
                        	  window.location.href ="/ispbrain";
                        	}, 3000);
                        
                        } else {
                        	generateNoty('error', 'Error al agregar el Plan.');
                        }
                    }
                });
                
            });
             
             
            $('#btn-edit-tplan').click(function(){
                
                $('.modal-actions-tplans').modal('hide');
             
                $('.modal-edit-tplan #id').val(plan_selected.id);
                
                $('.modal-edit-tplan #service-id').val(plan_selected.service_id);
                $('.modal-edit-tplan #service-id').selectpicker('refresh');
                
                $('.modal-edit-tplan #profile-id').val(plan_selected.profile_id);
                $('.modal-edit-tplan #pool-id').val(plan_selected.pool_id);
                $('.modal-edit-tplan #controller-id').val(plan_selected.controller_id);
                
                $('.modal-edit-tplan').modal('show');
                
            });
            
            $('#form-edit-plan').submit(function(){
                
                 event.preventDefault();
                 
                var send_data = {};
                 
                var form_data = $(this).serializeArray();
                
                $.each(form_data, function(i, val){
                    send_data[val.name] = val.value;
                });
                
                $('#btn-submit-edit-plan').hide(); 
               
                openModalPreloader("Editando plan ...");
                
                $.ajax({
                        type: 'POST',
                        dataType: "json",
                        url: '/ispbrain/IspControllerMikrotikPppoeTa/editPlan',
                        data:  JSON.stringify(send_data),
                        success: function(data){
                            
                            closeModalPreloader();
                            
                            $('#btn-submit-edit-plan').show(); 
                            
                            if(data.response){
                                generateNoty('success', 'El plan se edito correctamente');
                            }else{
                                generateNoty('error', 'Error al intentar editar el plan');
                            }
                     
                            table_tplans.ajax.reload();
                            
                            table_tprofiles.ajax.reload();
                            
                            // setTimeout(function(){
                                // table_tplans.draw();
                            // },1000);
                  
                            $('.modal-edit-tplan').modal('hide');
                            
                            updateCountersTitle();
                       
                        }, 
                        error: function(jqXHR) {
                            if (jqXHR.status == 403) {
                            
                            	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                            
                            	setTimeout(function() { 
                            	  window.location.href ="/ispbrain";
                            	}, 3000);
                            
                            } else {
                            	generateNoty('error', 'Error al editar el Plan.');
                            }
                        }
                    });
                
            });
            
            $('#btn-delete-tplan').click(function() {

                var text = '¿Está seguro que desea eliminar el Plan : ' + plan_selected.service.name + '?';

                bootbox.confirm(text, function(result) {
                    if (result) {
                        
                         openModalPreloader("Eliminado plan ...");
                        
                        $.ajax({
                            type: 'POST',
                            dataType: "json",
                            url: '/ispbrain/IspControllerMikrotikPppoeTa/deletePlan',
                            data:  JSON.stringify({id: plan_selected.id}),
                            success: function(data){
                                
                                closeModalPreloader();
                                
                                if(data.response){
                                    generateNoty('success', 'El plan se elimino correctamente');
                                }else{
                                    generateNoty('error', 'Error al intentar eliminar el plan');
                                }
                         
                                table_tplans.ajax.reload();
                                updateCountersTitle();
                               
                                
                                $('.modal-actions-tplans').modal('hide');
                            },
                            error: function(jqXHR) {
                                if (jqXHR.status == 403) {
                                
                                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                                
                                	setTimeout(function() { 
                                	  window.location.href ="/ispbrain";
                                	}, 3000);
                                
                                } else {
                                	generateNoty('error', 'Error al eliminar el Plan.');
                                }
                            }
                        });
                    }
                });
            });
            
            
            /*edit masivo de plan*/
            
            $('#btn-move-tplan').click(function(){
                
                $('.modal-actions-tplans').modal('hide');
             
                $('.modal-move-tplan #id').val(plan_selected.id);
                
                $('.modal-move-tplan #service-id').val(plan_selected.service_id);
                $('.modal-move-tplan #service-id').selectpicker('refresh');
               
                $('.modal-move-tplan #service-id-destino').selectpicker('refresh');
                
                $('.modal-move-tplan #controller-id').val(plan_selected.controller_id);
                
                $('.modal-move-tplan').modal('show');
                
            });
            
            $('#form-move-plan').submit(function(){
                
                 event.preventDefault();
                 
                var send_data = {};
                 
                var form_data = $(this).serializeArray();
                
                $.each(form_data, function(i, val){
                    send_data[val.name] = val.value;
                });
                
                
                if(send_data['id'] == send_data['service-id-destino']){
                    generateNoty('warning', 'El plan destino deber ser ditinto al plan selecionado.');
                    return;
                }
                
                $('#btn-submit-move-plan').hide(); 
               
                openModalPreloader("Moviendo plan ...");
                
                $.ajax({
                        type: 'POST',
                        dataType: "json",
                        url: '/ispbrain/IspControllerMikrotikPppoeTa/movePlan',
                        data:  JSON.stringify(send_data),
                        success: function(data){
                            
                            closeModalPreloader();
                            
                            $('#btn-submit-move-plan').show(); 
                            
                            if(data.response){
                                generateNoty('success', 'El plan se movió correctamente');
                            }else{
                                generateNoty('error', 'Error al intentar mover el plan');
                            }
                     
                            table_tplans.ajax.reload();
                            
                            table_tprofiles.ajax.reload();
                            
                            // setTimeout(function(){
                                // table_tplans.draw();
                            // },1000);
                  
                            $('.modal-move-tplan').modal('hide');
                            
                            updateCountersTitle();
                       
                        }, 
                        error: function(jqXHR) {
                            if (jqXHR.status == 403) {
                            
                            	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                            
                            	setTimeout(function() { 
                            	  window.location.href ="/ispbrain";
                            	}, 3000);
                            
                            } else {
                            	generateNoty('error', 'Error al mover el Plan.');
                            }
                        }
                    });
                
            });
            
            //end move plan
            

            $('a[id="plans-tab"]').on('shown.bs.tab', function (e) {
                table_tplans.draw();
            });
            
            setTimeout(function(){
                table_tplans.draw();
            },100)
        });
        
        function clearFormAddPlan(){
        
            $('#form-add-plan #service-id').val('');
            $('.modal-add-tplan #service-id').selectpicker('refresh');
            $('#form-add-plan #profile-id').val('');
            $('#form-add-plan #pool-id').val('');
        }
        
        
        // Jquery draggable modals
        $('.modal-dialog').draggable({
            handle: ".modal-header"
        });

        //end tplans
    </script>

<?php $this->end(); ?>
