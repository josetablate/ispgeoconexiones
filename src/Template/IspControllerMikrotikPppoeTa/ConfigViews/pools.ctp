<?php $this->extend('/IspControllerMikrotikPppoeTa/ConfigViews/profiles'); ?>

<?php $this->start('pools'); ?>

   <div id="btns-tools-tpools">
        <div class="text-right btns-tools margin-bottom-5">

            <?php 
                echo $this->Html->link(
                    '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Agregar',
                    'class' => 'btn btn-default btn-add-tpool',
                    'escape' => false
                    ]);

                 echo $this->Html->link(
                    '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Exportar tabla',
                    'class' => 'btn btn-default btn-export-tpools ml-1',
                    'escape' => false
                    ]);
            ?>

        </div>
    </div>
    
    <div class="row justify-content-center mt-2">
        <div class="col-auto">
            <div class="card border-naranja">
              <div class="card-body text-secondary p-2">
                <p class="card-text text-naranja">Para que los cambios surtan efecto en el controlador, debe <a href="/ispbrain/IspControllerMikrotikPppoeTa/sync/<?=$controller->id?>"  class="font-weight-bold"  >sincronizar</a>.</p>
              </div>
            </div>
        </div>
    </div>

   <div class="row">
        <div class="col-9 mx-auto">
            
            <div class="card border-secondary mt-2">
                <div class="card-body p-1">
                    
                     <table class="table table-bordered table-hover" id="table-tpools">
                        <thead>
                            <tr>
                                <th>Conexiones</th>
                                <th>Nombre</th>
                                <th>Addresses</th>
                                <th>Min Host</th>
                                <th>Max Host</th>
                                <th>Next Pool</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                    </table>

                    
                </div>
            </div>
        </div>
    </div>

   
    <!-- modal actions tpools -->
    <?php
        $buttons = [];

         $buttons[] = [
            'id'   =>  'btn-edit-tpool',
            'name' =>  'Editar',
            'icon' =>  'icon-pencil2',
            'type' =>  'btn-default'
        ];
        
         $buttons[] = [
            'id'   =>  'btn-move-pool',
            'name' =>  'Cambiar pool',
            'icon' =>  'icon-pencil2',
            'type' =>  'btn-default'
        ];

        $buttons[] = [
            'id'   =>  'btn-delete-tpool',
            'name' =>  'Eliminar',
            'icon' =>  'icon-bin',
            'type' =>  'btn-danger'
        ];

        echo $this->element('actions', ['modal'=> 'modal-actions-tpools', 'title' => 'Acciones', 'buttons' => $buttons ]);
    ?>

    <!-- modal agregar tpools -->
    <div class="modal fade modal-add-tpool" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="gridSystemModalLabel">Agregar Pool</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-12">

                            <?= $this->Form->create(null, ['id' => 'form-add-pool']) ?>
                                <fieldset>
                                    
                                    
                                    <div class="row">
                                        <div class="col-xl-4">
                                            <?php
                                            echo $this->Form->input('name', ['label' => 'Nombre', 'value' => '', 'required' => true, 'autocomplete' => 'off']);
                                            ?>
                                        </div>
                                         <div class="col-xl-4">
                                            <?php
                                            echo $this->Form->input('addresses', ['label' => 'Addresses', 'required' => true, 'autocomplete' => 'off']);
                                            echo '<p class="text-muted">Formato 10.0.0.0/24</p>';
                                            ?>
                                        </div>
                                         <div class="col-xl-4">
                                            <?php
                                            echo $this->Form->input('min_host', ['label' => 'Min Host', 'readonly' => true]);
                                            ?>
                                        </div>
                                         <div class="col-xl-4">
                                            <?php
                                            echo $this->Form->input('max_host', ['label' => 'Max Host', 'readonly' => true]);
                                            ?>
                                        </div>
                                         <div class="col-xl-4">
                                            <?php
                                            echo $this->Form->input('next_pool_id', ['label' => 'Next Pool', 'required' => false]);
                                            echo $this->Form->input('controller_id', ['value' => $controller->id, 'type' => 'hidden']);
                                            ?>
                                        </div>
                                    </div>

                                </fieldset>
                                <?= $this->Form->button(__('Agregar', ['id' => 'btn-submit-add-pool'])) ?>
                            <?= $this->Form->end() ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal edit tpools -->
    <div class="modal fade modal-edit-tpool" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="gridSystemModalLabel">Editar Pool</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">

                            <?= $this->Form->create(null, ['id' => 'form-edit-pool']) ?>
                                
                                <fieldset>
                                    
                                    <div class="row">
                                        <div class="col-xl-4">
                                            <?php
                                            echo $this->Form->input('name', ['label' => 'Nombre', 'value' => '', 'required' => true, 'autocomplete' => 'off']);
                                            ?>
                                        </div>
                                         <div class="col-xl-4">
                                            <?php
                                            echo $this->Form->input('addresses', ['label' => 'Addresses', 'required' => true, 'autocomplete' => 'off']);
                                            echo '<p class="text-muted">Formato 10.0.0.0/24</p>';
                                            ?>
                                        </div>
                                         <div class="col-xl-4">
                                            <?php
                                            echo $this->Form->input('min_host', ['label' => 'Min Host', 'readonly' => true]);
                                            ?>
                                        </div>
                                         <div class="col-xl-4">
                                            <?php
                                            echo $this->Form->input('max_host', ['label' => 'Max Host', 'readonly' => true]);
                                            ?>
                                        </div>
                                        
                                        <div class="col-xl-4">
                                            <?php
                                            echo $this->Form->input('next_pool_id', ['label' => 'Next Pool', 'required' => false]);
                                            echo $this->Form->input('controller_id', ['value' => $controller->id, 'type' => 'hidden']);
                                            echo $this->Form->input('id', ['type' => 'hidden']);
                                            ?>
                                        </div>
                                     
                                    </div>

                                </fieldset>
                                <?= $this->Form->button(__('Guardar'), ['id' => 'btn-submit-edit-pool']) ?>
                            <?= $this->Form->end() ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <!-- modal move pool -->
    <div class="modal fade modal-move-pool" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="gridSystemModalLabel">Cambiar red</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    
                     <div class="row justify-content-center m-3">
                        <div class="col-12">
                            <div class="card border-naranja">
                              <div class="card-body text-secondary p-3">
                                <p class="card-text text-naranja">
                                    Esta acción permite mover las conexiones de una red a otra.
                                    Si la red destino no tiene las ips libres suficientes,
                                    las conexiones que no pudieron pasar, quedarán con la red original.
                                </p>
                              </div>
                            </div>
                        </div>
                    </div>
                                    
                    
                    
                    <div class="row">
                        <div class="col-md-12">

                            <?= $this->Form->create(null, ['id' => 'form-move-pool']) ?>
                                
                                <fieldset>
                                    
                                    <div class="row">
                                        <div class="col-xl-12">
                                            <?php
                                            echo $this->Form->input('pool_from_name', ['label' => 'Pool (origen)', 'value' => '', 'disabled' => true]);
                                            echo $this->Form->input('pool_from_id', ['type' => 'hidden']);
                                            ?>
                                        </div>
                                          <div class="col-xl-12">
                                            <?php
                                            echo $this->Form->input('pool_to_id', ['label' => 'Pool (destino)', 'options' => []]);
                                            echo $this->Form->input('controller_id', ['value' => $controller->id, 'type' => 'hidden']);
                                            ?>
                                        </div>
                                    </div>

                                </fieldset>
                                <?= $this->Form->button(__('Confimar'), ['id' => 'btn-submit-move-pool', 'class' => 'btn-danger']) ?>
                            <?= $this->Form->end() ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    

    <script type="text/javascript">
        //tpools
        
        var pool_selected = null;
        var table_tpools = null;

        $(document).ready(function () {

            $('#table-tpools').removeClass('display');

    		table_tpools = $('#table-tpools').on( 'draw.dt', function () {
    		    
    		    updateSelectPools();
                    
                }).DataTable({
                    
    		    "order": [[ 1, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ajax": {
                    "url": "/ispbrain/IspControllerMikrotikPppoeTa/get_pools_by_controller.json",
                    "dataSrc": "pools",
                    "data": function ( d ) {
                        return $.extend( {}, d, {
                            "controller_id": controller_id
                        });
                    },
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "connections_amount"
                    },
    		        { 
                        "data": "name"
                    },
                    { 
                        "data": "addresses"
                    },
                    { 
                        "data": "min_host"
                    },
                    { 
                        "data": "max_host"
                    },
                    { 
                        "data": "next_pool",
                        "render": function ( data, type, row ) {
                            return data ? data.name : '';
                        }
                    },
                ],
    		    "columnDefs": [
    		          
                ],
                "createdRow" : function( row, data, index ) {
                    row.id = data.id;
                },
              
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
            

            $('#table-tpools_wrapper .tools').append($('#btns-tools-tpools').contents());
    		
    		  $('#btns-tpools-tpools').show();

            $(".btn-export-tpools").click(function(){

                bootbox.confirm('Se descargará un archivo Excel', function(result) {
                    if (result) {
                        $('#table-tpools').tableExport({tableName: 'Pools', type:'excel', escape:'false'});
                    }
                });
            });

            $('#table-tpools tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {

                    table_tpools.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    pool_selected = table_tpools.row( this ).data();

                    $('.modal-actions-tpools').modal('show');
                }
            });

            $('.btn-add-tpool').click(function() {
                $('.modal-add-tpool').modal('show');
            });
            
            $('#form-add-pool').submit(function(){
                
                 event.preventDefault();
                 
                var send_data = {};
                 
                var form_data = $(this).serializeArray();
                
                $.each(form_data, function(i, val){
                    send_data[val.name] = val.value;
                });
                
                $('#btn-submit-add-pool').hide(); 
              
                openModalPreloader("Agregando Pool ...");
                
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '/ispbrain/IspControllerMikrotikPppoeTa/addPool',
                    data:  JSON.stringify(send_data),
                    success: function(data){
                        
                        $('#btn-submit-add-pool').show(); 
                        closeModalPreloader();
                        
                        if(data.response){
                            generateNoty('success', 'El pool se agrego correctamente');
                        }else{
                            generateNoty('error', 'Error al intentar agregar el Pool');
                        }
                        
                        table_tpools.ajax.reload();
                        updateSelectPools();
                        $('.modal-add-tpool').modal('hide');
                        
                        updateCountersTitle();
                    },
                    error: function(jqXHR) {
                        if (jqXHR.status == 403) {
                        
                        	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                        
                        	setTimeout(function() { 
                        	  window.location.href ="/ispbrain";
                        	}, 3000);
                        
                        } else {
                        	generateNoty('error', 'Error al al agregar el Pool.');
                        }
                    }
                });
                
            });

            $('#btn-delete-tpool').click(function() {
               
          
                var text = '¿Está seguro que desea eliminar el Pool : ' + pool_selected.name + '?';
                
                bootbox.confirm(text, function(result) {
                    if (result) {
                     
                        openModalPreloader("Eliminado Pool ...");
                        
                        $.ajax({
                            type: 'POST',
                            dataType: "json",
                            url: '/ispbrain/IspControllerMikrotikPppoeTa/deletePool',
                            data:  JSON.stringify({id: pool_selected.id}),
                            success: function(data){
                              
                              closeModalPreloader();                               
                              
                                if(data.response){
                                    generateNoty('success', 'El pool se elimino correctamente');
                                }else{
                                    generateNoty('error', 'Error al intentar eliminar el Pool');
                                }
                       
                                table_tpools.ajax.reload();
                                updateSelectPools();
                                
                                updateCountersTitle();
                               
                                $('.modal-actions-tpools').modal('hide');
                            },
                            error: function(jqXHR) {
                                if (jqXHR.status == 403) {
                                
                                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                                
                                	setTimeout(function() { 
                                	  window.location.href ="/ispbrain";
                                	}, 3000);
                                
                                } else {
                                	generateNoty('error', 'Error al eliminar el Pool.');
                                }
                            }
                        });
                    }
                });
                
            });

            $('#btn-edit-tpool').click(function(){
                
                $('.modal-actions-tpools').modal('hide');
             
                $('.modal-edit-tpool input#id').val(pool_selected.id);
                $('.modal-edit-tpool input#controladorid').val(pool_selected.controller_id);
                $('.modal-edit-tpool input#name').val(pool_selected.name);
                $('.modal-edit-tpool input#addresses').val(pool_selected.addresses);
                $('.modal-edit-tpool select#next-pool-id').val(pool_selected.next_pool ? pool_selected.next_pool.id : '');
                $('.modal-edit-tpool input#addresses').focusout();
                
                $('.modal-edit-tpool').modal('show');
                
            });
            
            $('#form-edit-pool').submit(function(){
                
                 event.preventDefault();
                 
                var send_data = {};
                 
                var form_data = $(this).serializeArray();
                
                $.each(form_data, function(i, val){
                    send_data[val.name] = val.value;
                });
                
                $('#btn-submit-edit-pool').hide(); 
               
                openModalPreloader("Editando Pool ...");
                
                
                $.ajax({
                        type: 'POST',
                        dataType: "json",
                        url: '/ispbrain/IspControllerMikrotikPppoeTa/editPool',
                        data:  JSON.stringify(send_data),
                        success: function(data){
                            
                            closeModalPreloader();
                            
                            $('#btn-submit-edit-pool').show(); 
                            
                            if(data.response){
                                generateNoty('success', 'El pool se edito correctamente');
                            }else{
                                generateNoty('error', 'Error al intentar editar el Pool');
                            }
                        
                     
                            table_tpools.ajax.reload();
                             
                            updateSelectPools();
                             
                            $('.modal-edit-tpool').modal('hide');
                            
                            updateCountersTitle();
                            
                        }, 
                        error: function(jqXHR) {
                            if (jqXHR.status == 403) {
                            
                            	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                            
                            	setTimeout(function() { 
                            	  window.location.href ="/ispbrain";
                            	}, 3000);
                            
                            } else {
                            	generateNoty('error', 'Error al editar el Pool.');
                            }

                        }
                    });
                
            });
            
            $('#btn-move-pool').click(function(){
                
                $('.modal-actions-tpools').modal('hide');
             
                $('.modal-move-pool input#pool-from-name').val(pool_selected.name + ' (' + pool_selected.connections_amount + ')');
                $('.modal-move-pool input#pool-from-id').val(pool_selected.id);
                
                $('.modal-move-pool select#pool-to-id').val('');
                
                $('.modal-move-pool').modal('show');
                
            });
            
            $('#form-move-pool').submit(function(){
                 
               event.preventDefault();

               var send_data = {};

               var form_data = $(this).serializeArray();
                 
               if($('.modal-move-pool input#pool-from-id').val() == $('.modal-move-pool select#pool-to-id').val()){
                     generateNoty('warning', 'El pool destino debe ser direferencia del pool origen.');
                     return false;
               }
                 
                var text = '¿Está seguro que desea cambiar el (pool) : ' + pool_selected.name + '?';
                
                bootbox.confirm(text, function(result) {
                    
                    if (result) {  
                        
                        $.each(form_data, function(i, val){
                            send_data[val.name] = val.value;
                        });
                        
                        $('#btn-submit-move-pool').hide(); 
                       
                        openModalPreloader("Cambiando pool ...");
                        
                        $.ajax({
                            type: 'POST',
                            dataType: "json",
                            url: '/ispbrain/IspControllerMikrotikPppoeTa/movePool',
                            data:  JSON.stringify(send_data),
                            success: function(data){
                                
                                closeModalPreloader();
                                
                                $('#btn-submit-move-pool').show(); 
                                
                                if(data.response){
                                    generateNoty('success', 'El (pool) se cambio correctamente');
                                }else{
                                    generateNoty('error', 'Error al intentar cambiar el (pool)');
                                    updateCountersTitle();
                                }
                         
                                table_tpools.ajax.reload();
                                updateSelectPools();
                                
                                $('.modal-move-pool').modal('hide');
                                
                                updateCountersTitle();
                           
                            },
                            error: function(jqXHR) {
                                
                                if (jqXHR.status == 403) {
                                
                                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                                
                                	setTimeout(function() { 
                                	  window.location.href ="/ispbrain";
                                	}, 3000);
                                
                                } else {
                                	generateNoty('error', 'Error al cambiar el (pool).');
                                }
                            }
                        });
                    }
                });
            });   

            $('.modal-edit-tpool input#addresses').focusout(function() {

                var addresses = $(this).val();

                if (addresses != '') {

                    $.ajax({
                        url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikPppoeTa', 'action' => 'getRangeNetwork'])?>" ,
                        type: 'POST',
                        dataType: "json",
                        data: JSON.stringify({ addresses: addresses}),
                        success: function(data){

                            $('.modal-edit-tpool input#min-host').val(data.range.minHost);
                            $('.modal-edit-tpool input#max-host').val(data.range.maxHost);
                        },
                        error: function (jqXHR) {
                            if (jqXHR.status == 403) {
                            
                            	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                            
                            	setTimeout(function() { 
                            	  window.location.href ="/ispbrain";
                            	}, 3000);
                            
                            } else {
                            	generateNoty('error', 'Error al intentar calcular el rango del pool. Verifique el formato.');
                            }
                        }
                    });
                }
            });

            $('.modal-add-tpool input#addresses').focusout(function() {

                var addresses = $(this).val();

                if (addresses != '') {
                     $.ajax({
                        url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikPppoeTa', 'action' => 'getRangeNetwork'])?>" ,
                        type: 'POST',
                        dataType: "json",
                        data: JSON.stringify({ addresses: addresses}),
                        success: function(data){

                            $('.modal-add-tpool input#min-host').val(data.range.minHost);
                            $('.modal-add-tpool input#max-host').val(data.range.maxHost);
                        },
                        error: function (jqXHR) {
                            
                            if (jqXHR.status == 403) {
                            
                            	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                            
                            	setTimeout(function() { 
                            	  window.location.href ="/ispbrain";
                            	}, 3000);
                            
                            } else {
                            	generateNoty('error', 'Error al intentar calcular el rango del pool. Verifique el formato.');
                            }
                        }
                    });
                }
            });
            
            
            

            $('a[id="pools-tab"]').on('shown.bs.tab', function (e) {
                table_tpools.draw();
            });
        });
        
      
        
        function updateSelectPools(){
            
            if(table_tpools){
                
                 $('.modal-add-tpool #next-pool-id').html('');
                 $('.modal-edit-tpool #next-pool-id').html('');
                 $('.modal-move-pool #pool-to-id').html('');
                 
                 $('.modal-add-tplan #pool-id').html('');
                 $('.modal-edit-tplan #pool-id').html('');
     
                 
                 
                 $('.modal-add-tpool #next-pool-id').append($('<option>', { 
                        value: '',
                        text : '' 
                 }));
                 
                 $('.modal-edit-tpool #next-pool-id').append($('<option>', { 
                        value: '',
                        text : '' 
                 }));
                 
                 $('.modal-move-pool #pool-to-id').append($('<option>', { 
                        value: '',
                        text : '' 
                 }));
                 
                 $('.modal-add-tplan #pool-id').append($('<option>', { 
                        value: '',
                        text : '' 
                 }));
                
                 $('.modal-edit-tplan #pool-id').append($('<option>', { 
                        value: '',
                        text : '' 
                 }));
               
                 
                var pools_available = [];
                 
                $.each(table_tpools.rows().data(), function (i, poola) {
                    
                    var flag = false;
                    
                    $.each(table_tpools.rows().data(), function (i, poolb) {
                    
                       if(poola.id == poolb.next_pool_id){
                           flag = true;
                       }
                    });
                    
                    if(!flag){
                        pools_available.push(poola);
                    }
               
                });
                
                $.each(pools_available, function (i, pool) {
               
                    
                    $('.modal-add-tpool #next-pool-id').append($('<option>', { 
                        value: pool.id,
                        text : pool.name 
                    }));
                    
                    $('.modal-edit-tpool #next-pool-id').append($('<option>', { 
                        value: pool.id,
                        text : pool.name 
                    }));
                     
                     $('.modal-move-pool #pool-to-id').append($('<option>', { 
                        value: pool.id,
                        text : pool.name 
                    }));
                    
                    $('.modal-add-tplan #pool-id').append($('<option>', { 
                        value: pool.id,
                        text : pool.name 
                    }));
                   
                    $('.modal-edit-tplan #pool-id').append($('<option>', { 
                        value: pool.id,
                        text : pool.name 
                    }));
                
                });
                
            }
            
        }
        
        
        // Jquery draggable modals
        $('.modal-dialog').draggable({
            handle: ".modal-header"
        });

        //end tpools
    </script>

<?php $this->end(); ?>
