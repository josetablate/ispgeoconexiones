<?php $this->extend('/IspControllerMikrotikPppoeTa/SyncViews/profiles'); ?>

<?php $this->start('secrets'); ?>

<style type="text/css">
    
     #table-secretsA_wrapper .title{
        color: #696868;
        padding-top: 10px;
    }
    
     #table-secretsB_wrapper .title{
        color: #696868;
        padding-top: 10px;
    }
    
</style>







    <div class="row">
        <div class="col-xl-5">
            
            <div class="card border-secondary mb-1 mt-2">
                <div class="card-body p-1">
                    <div class="text-secondary p-0">Controlador Seleccionado: <span class="font-weight-bold"><?=$controller->name?></span></div>
                </div>
            </div>
        </div>
        <div class="col-xl-1">
            <?php
              echo $this->Html->link(
                '<span class="glyphicon icon-loop2" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                    'id' => 'btn-refresh-secrets',
                    'title' => 'Click para realizar el proceso control de diferencias',
                    'class' => 'btn btn-default mt-2',
                    'data-toggle' => 'tooltip',
                    'escape' => false
                ]);
             
             ?>
        </div>
        <div class="col-xl-2">
            
             <?php
              echo $this->Html->link(
                'Sincronizar todo',
                'javascript:void(0)',
                [
                    'id' => 'btn-sync-secrets',
                    'title' => '',
                    'class' => 'btn btn-primary mt-2',
                ]);
             
             ?>

        </div>
        
        <div class="col-xl-4 text-right">
            <?= $this->Form->input('clear_secrets', [
                'label' => 'Limpiar (secrets) del controlador', 
                'checked' => false, 
                'class' => 'm-0 mr-3 mt-3',
                'title' => 'Eliminar los registros agenos a este controlador. Los marcados en azul.',
                'data-toggle' => 'tooltip',
                ])?>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-6">
            <table class="table table-bordered table-hover" id="table-secretsA">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Password</th>
                        <th>Service</th>
                        <th>Profile</th>
                        <th>Remote Address</th>
                        <th>Comment</th>
                        <th>Caller Id</th>
                    </tr>
                </thead>
                <tbody>
                  
                </tbody>
            </table>
        </div>
        <div class="col-xl-6">
            <table class="table table-bordered table-hover" id="table-secretsB">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Password</th>
                        <th>Service</th>
                        <th>Profile</th>
                        <th>Remote Address</th>
                        <th>Comment</th>
                        <th>Caller Id</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div>
    
    
    
<?php

    $buttonsA = [];

    $buttonsA[] = [
        'id'   =>  'btn-edit-secret',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];
    
     $buttonsA[] = [
        'id'   =>  'btn-sync-secret-indi',
        'name' =>  'Sincronizar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];


    echo $this->element('actions', ['modal'=> 'modal-secretA', 'title' => 'Acciones', 'buttons' => $buttonsA ]);
    
    
    $buttonsB = [];

    $buttonsB[] = [
        'id'   =>  'btn-delete-secret',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-secretB', 'title' => 'Acciones', 'buttons' => $buttonsB ]);
  
?>


    <script type="text/javascript">

        var table_secretsA = null;
        var table_secretsB = null;
        var secretA_selected = null;

        $(document).ready(function () {
            //secretsA

            $('#table-secretsA').removeClass('display');

    		table_secretsA = $('#table-secretsA').DataTable({
    		    "order": [[ 1, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ordering" : false,
    		    "paging": false,
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/sync/mikrotik_pppoe_ta/secretsA.json",
                    "dataSrc": "data",
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "api_id",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "name",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "password",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "service",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "profile",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "remote_address",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "comment",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "caller_id",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    }
                ],
    		    "columnDefs": [
    		        { "type": 'ip-address', targets: [5] },
                    { 
                        "visible": false,
                        "targets": [3,6]
                    },
                ],
                "createdRow" : function( row, data, index ) {
                    row.id = data.connection_id;
                },
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-secretsA_wrapper .title').append('<h5>Sistema</h5>');
    		
    		$('#table-secretsA tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {
                    
                    table_secretsA.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    secretA_selected = table_secretsA.row( this ).data();
                    $('.modal-secretA').modal('show');
                }
            });
            
            $('#btn-edit-secret').click(function() {
                var action = '/ispbrain/connections/edit/' + secretA_selected.connection_id;
                window.open(action, '_black');
            });
            
            $('#btn-sync-secrets').click(function() {

                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
                
                if(table_secretsA.rows('tr').count() == 0 && table_secretsB.rows('tr').count() == 0){
            
                    generateNoty('warning', 'No existen elementos para sincronizar.');
                    
                    return false;
                    
                }
                
                var connections_ids = [];
                
                $.each(table_secretsA.$('tr'), function(i, secret){
                    connections_ids.push(secret.id);
                });
          
                var text = '¿Está seguro que desea sincronizar (Secrets) ?';
    
                bootbox.confirm(text, function(result) {
                    if (result) {
                    
                        openModalPreloader('Sincronizando ... ');
                        
                         $.ajax({
                            url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikPppoeTa', 'action' => 'sync-secrets'])?>" ,
                            type: 'POST',
                            dataType: "json",
                            data: JSON.stringify({controller_id: controller_id, connections_ids: connections_ids, delete_secret_side_b: $('#clear-secrets').is(':checked')}),
                            success: function(data){

                                closeModalPreloader();
            
                                if (!data.data) {
                                     generateNoty('error', 'Error al intentar sincronizar (Secrets).');
                                }else{
                                    generateNoty('success', 'Sincronización de (Secrets) completo.');
                                }
                                
                                table_secretsA.ajax.reload();
                                table_secretsB.ajax.reload();
                                
                            },
                            error: function (jqXHR) {

                                if (jqXHR.status == 403) {
                                
                                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                                
                                	setTimeout(function() { 
                                	  window.location.href ="/ispbrain";
                                	}, 3000);
                                
                                } else {
                                	closeModalPreloader();
                                    generateNoty('error', 'Error al intentar sincronizar (Secrets).');
                                }
                            }
                        });
                    }
                });
            });
            
            $('#btn-sync-secret-indi').click(function() {

                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
      
                var connections_ids = [];
                
                connections_ids.push(secretA_selected.connection_id);
          
                var text = '¿Está seguro que desea sincronizar este (secret) ?';
    
                bootbox.confirm(text, function(result) {
                    
                    if (result) {
                        
                         $('.modal-secretA').modal('hide');
                    
                        openModalPreloader('Sincronizando ... ');
                        
                         $.ajax({
                            url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikPppoeTa', 'action' => 'sync-secret-indi'])?>" ,
                            type: 'POST',
                            dataType: "json",
                            data: JSON.stringify({controller_id: controller_id, connections_ids: connections_ids}),
                            success: function(data){

                                closeModalPreloader();
            
                                if (!data.data) {
                                     generateNoty('error', 'Error al intentar sincronizar el (Secret).');
                                }else{
                                    generateNoty('success', 'Sincronización del (Secret) completo.');
                                }
                                
                                updateCountersTitle();
                                
                                table_secretsA.ajax.reload();
                                table_secretsB.ajax.reload();
                            },
                            error: function (jqXHR) {

                                if (jqXHR.status == 403) {
                                
                                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                                
                                	setTimeout(function() { 
                                	  window.location.href ="/ispbrain";
                                	}, 3000);
                                
                                } else {
                                	closeModalPreloader();
                                    generateNoty('error', 'Error al intentar sincronizar el (secret).');
                                    updateCountersTitle();
                                }
                            }
                        });
                    }
                });
            });
            
            $('#btn-refresh-secrets').click(function() {

                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
               
                openModalPreloader('Actualizando ... ');
                
                 $.ajax({
                    url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikPppoeTa', 'action' => 'refreshSecrets'])?>" ,
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify({controller_id: controller_id}),
                    success: function(data){
                        
                         closeModalPreloader();
    
                        if (!data.data) {
                            generateNoty('error', 'No se pudo establecer una conexión con el controlador.');
                        }else{
                            generateNoty('success', 'Actualización completa.');
                        }
                        
                        table_secretsA.ajax.reload();
                        table_secretsB.ajax.reload();
                        
                        updateCountersTitle();
                    },
                    error: function (jqXHR) {

                        if (jqXHR.status == 403) {
                        
                        	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                        
                        	setTimeout(function() { 
                        	  window.location.href ="/ispbrain";
                        	}, 3000);
                        
                        } else {
                        	closeModalPreloader();
                            generateNoty('error', 'Error al intentar actualizar.');
                            updateCountersTitle();
                        }
                    }
                });
                
            });
            

            //end secretsA

            //secretsB

            $('#table-secretsB').removeClass('display');

    		table_secretsB = $('#table-secretsB').DataTable({
    		    "order": [[ 1, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ordering" : false,
    		    "paging": false,
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/sync/mikrotik_pppoe_ta/secretsB.json",
                    "dataSrc": "data",
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "api_id",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                             else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "name",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "password",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                             else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "service",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                             else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "profile",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                             else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "remote_address",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                             else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "comment",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                             else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "caller_id",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                             else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    }
                ],
    		    "columnDefs": [
    		        { "type": 'ip-address', targets: [5] },
                    { 
                        "visible": false,
                        "targets": [3,6]
                    },
                ],
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-secretsB_wrapper .title').append('<h5>Controlador</h5>');
    		
    		$('#table-secretsB tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {
                    
                    table_secretsB.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    secretB_selected = table_secretsB.row( this ).data();
                    $('.modal-secretB').modal('show');
                }
            });
            
            $('#btn-delete-secret').click(function() {

                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
               
               $('.modal-secretB').modal('hide');
               
                openModalPreloader('Eliminado Secret del Controlador ... ');
                
                 $.ajax({
                    url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikPppoeTa', 'action' => 'deleteSecretInController'])?>" ,
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify({controller_id: controller_id, secret_api_id: secretB_selected.api_id}),
                    success: function(data){
                        
                        closeModalPreloader();
    
                        if (!data.data) {
                             generateNoty('error', 'Error al intentar eliminar.');
                        }else{
                            generateNoty('success', 'Eliminación completa.');
                        }
                        
                         table_secretsA.ajax.reload();
                         table_secretsB.ajax.reload();
                         
                         updateCountersTitle();
                    },
                    error: function (jqXHR) {

                        if (jqXHR.status == 403) {
                        
                        	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                        
                        	setTimeout(function() { 
                        	  window.location.href ="/ispbrain";
                        	}, 3000);
                        
                        } else {
                        	closeModalPreloader();
                            generateNoty('error', 'Error al intentar eliminar.');
                            updateCountersTitle();
                        }
                    }
                });
            });
    		
    		 //end secretsB

    		$('a[id="secrets-tab"]').on('shown.bs.tab', function (e) {
                 table_secretsA.draw();
                 table_secretsB.draw();
            });

           
        });
       

    </script>
<?php $this->end(); ?>
