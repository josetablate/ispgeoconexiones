<?php $this->extend('/IspControllerMikrotikPppoeTa/SyncViews/address_lists'); ?>

<?php $this->start('profiles'); ?>
 
<style type="text/css">
    
     #table-profilesA_wrapper .title{
        color: #696868;
        padding-top: 10px;
    }
    
     #table-profilesB_wrapper .title{
        color: #696868;
        padding-top: 10px;
    }
    
</style>


    <div class="row">
        <div class="col-xl-5">
            
            <div class="card border-secondary mb-1 mt-2">
                <div class="card-body p-1">
                    <div class="text-secondary p-0">Controlador Seleccionado: <span class="font-weight-bold"><?=$controller->name?></span></div>
                </div>
            </div>
        </div>
        <div class="col-xl-1">
            <?php
              echo $this->Html->link(
                '<span class="glyphicon icon-loop2" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                    'id' => 'btn-refresh-profiles',
                    'title' => 'Click para realizar el proceso control de diferencias',
                    'class' => 'btn btn-default mt-2',
                    'data-toggle' => 'tooltip',
                    'escape' => false
                ]);
             
             ?>
        </div>
        <div class="col-xl-2">
            
             <?php
              echo $this->Html->link(
                'Sincronizar todo',
                'javascript:void(0)',
                [
                    'id' => 'btn-sync-profiles',
                    'title' => '',
                    'class' => 'btn btn-primary mt-2',
                ]);
             
             ?>

        </div>
        
        <div class="col-xl-4 text-right">
            <?= $this->Form->input('clear_profiles', [
                'label' => 'Limpiar (profiles) del controlador', 
                'checked' => false,
                'class' => 'm-0 mr-3 mt-3',
                'title' => 'Eliminar los registros agenos a este controlador. Los marcados en azul.',
                'data-toggle' => 'tooltip',
            ])?>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-6">
            <table class="table table-bordered table-hover" id="table-profilesA">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Local Address</th>
                        <th>DNS Server</th>
                        <th>Rate Limit</th>
                        <th>Queue Type</th>
                    </tr>
                </thead>
                <tbody>
                  
                </tbody>
            </table>
        </div>
        <div class="col-xl-6">
            <table class="table table-bordered table-hover" id="table-profilesB">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Local Address</th>
                        <th>DNS Server</th>
                        <th>Rate Limit</th>
                        <th>Queue Type</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div>
    
    
    
<?php

    $buttonsA = [];

    $buttonsA[] = [
        'id'   =>  'btn-edit-profile',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttonsA[] = [
        'id'   =>  'btn-sync-profile-indi',
        'name' =>  'Sincronizar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-prifileA', 'title' => 'Acciones', 'buttons' => $buttonsA ]);
    
    
    $buttonsB = [];

    $buttonsB[] = [
        'id'   =>  'btn-delete-profile',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-prifileB', 'title' => 'Acciones', 'buttons' => $buttonsB ]);
  
?>


    <script type="text/javascript">

        var table_profilesA = null;
        var table_profilesB = null;
        var profileA_selected = null;

        $(document).ready(function () {
            //profilesA

            $('#table-profilesA').removeClass('display');

    		table_profilesA = $('#table-profilesA').DataTable({
    		    "order": [[ 1, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ordering" : false,
    		    "paging": false,
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/sync/mikrotik_pppoe_ta/profilesA.json",
                    "dataSrc": "data",
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "api_id",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "name",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "local_address",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "dns_server",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "rate_limit_string",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "queue_type",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                ],
    		    "columnDefs": [
    		        { "type": 'ip-address', targets: [2,3] },
                   
                ],
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-profilesA_wrapper .title').append('<h5>Sistema</h5>');
    		
    		$('#table-profilesA tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {
                    
                    table_profilesA.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    profileA_selected = table_profilesA.row( this ).data();
                    $('.modal-prifileA').modal('show');
                }
            });
            
            $('#btn-edit-profile').click(function() {
                var action = '/ispbrain/IspControllerMikrotikPppoeTa/config/' + controller_id;
                window.open(action, '_black');
            });
            
            $('#btn-sync-profiles').click(function() {

                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
                
                if(table_profilesA.rows('tr').count() == 0 && table_profilesB.rows('tr').count() == 0){
            
                    generateNoty('warning', 'No existen elementos para sincronizar.');
                    
                    return false;
                    
                }
                
                var profiles_ids = [];
                
                $.each(table_profilesA.rows().data(), function(i, profile){
                    profiles_ids.push(profile.id);
                });
                
                // console.log(profiles_ids);
          
                var text = '¿Está seguro que desea sincronizar (Profiles)?';
    
                bootbox.confirm(text, function(result) {
                    if (result) {
                    
                        openModalPreloader("Sincronizando ...");
                        
                         $.ajax({
                            url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikPppoeTa', 'action' => 'sync-profiles'])?>" ,
                            type: 'POST',
                            dataType: "json",
                            data: JSON.stringify({controller_id: controller_id, profiles_ids: profiles_ids, delete_profiles_side_b: $('#clear-profiles').is(':checked')}),
                            success: function(data){
                                
                                console.log(data);
                                
                                if(data.response){
                                    generateNoty('success', 'Sincronización de (Profiles) completa');
                                }else{
                                    generateNoty('error', 'Error en la Sincronización');
                                }
                                
                                closeModalPreloader();
                               
                                table_profilesA.ajax.reload();
                                table_profilesB.ajax.reload();
                               
                            },
                            error: function (jqXHR) {

                                if (jqXHR.status == 403) {
                                
                                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                                
                                	setTimeout(function() { 
                                	  window.location.href ="/ispbrain";
                                	}, 3000);
                                
                                } else {
                                	closeModalPreloader();
                                    generateNoty('error', 'Error al intentar sincronizar (Profiles).');
                                }
                            }
                        });
                    }
                });
            });
            
            $('#btn-sync-profile-indi').click(function() {

                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
             
                var profiles_ids = [];
                
                profiles_ids.push(profileA_selected.id);
          
                var text = '¿Está seguro que desea sincronizar es (profile)?';
    
                bootbox.confirm(text, function(result) {
                    if (result) {
                        
                        $('.modal-prifileA').modal('hide');
                    
                        openModalPreloader("Sincronizando ...");
                        
                         $.ajax({
                            url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikPppoeTa', 'action' => 'sync-profile-indi'])?>" ,
                            type: 'POST',
                            dataType: "json",
                            data: JSON.stringify({controller_id: controller_id, profiles_ids: profiles_ids, delete_profiles_side_b: $('#clear-profiles').is(':checked')}),
                            success: function(data){
                                
                                if(data.response){
                                    generateNoty('success', 'Sincronización de (Profile) completa');
                                }else{
                                    generateNoty('error', 'Error en la Sincronización');
                                }
                                
                                closeModalPreloader();
                               
                                table_profilesA.ajax.reload();
                                table_profilesB.ajax.reload();
                                
                                updateCountersTitle();
                               
                            },
                            error: function (jqXHR) {

                                if (jqXHR.status == 403) {
                                
                                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                                
                                	setTimeout(function() { 
                                	  window.location.href ="/ispbrain";
                                	}, 3000);
                                
                                } else {
                                	closeModalPreloader();
                                    generateNoty('error', 'Error al intentar sincronizar (Profiles).');
                                    
                                    updateCountersTitle();
                                }
                            }
                        });
                    }
                });
            });
            
            
            $('#btn-refresh-profiles').click(function() {

                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
            
                openModalPreloader("Actualizando ...");
                
                 $.ajax({
                    url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikPppoeTa', 'action' => 'refreshProfiles'])?>" ,
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify({controller_id: controller_id}),
                    success: function(data){
                        
                        closeModalPreloader();
    
                        if (!data.data) {
                             generateNoty('error', 'No se pudo establecer una conexión con el controlador.');
                        }else{
                            
                            generateNoty('success', 'Actualización completa.');
                            
                            table_profilesA.ajax.reload();
                            table_profilesB.ajax.reload();
                        }
                        
                        updateCountersTitle();
                    },
                    error: function (jqXHR) {

                        if (jqXHR.status == 403) {
                        
                        	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                        
                        	setTimeout(function() { 
                        	  window.location.href ="/ispbrain";
                        	}, 3000);
                        
                        } else {
                        	closeModalPreloader();
                            generateNoty('error', 'Error al intentar actualizar.');
                            updateCountersTitle();
                        }
                    }
                });
                
            });
            

            //end profilesA

            //profilesB

            $('#table-profilesB').removeClass('display');

    		table_profilesB = $('#table-profilesB').DataTable({
    		    "order": [[ 1, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ordering" : false,
    		    "paging": false,
    		    
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/sync/mikrotik_pppoe_ta/profilesB.json",
                    "dataSrc": "data",
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "api_id",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "name",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "local_address",
                        "render": function ( data, type, row ) {
                             if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "dns_server",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "rate_limit_string",
                        "render": function ( data, type, row ) {
                             if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "queue_type",
                        "render": function ( data, type, row ) {
                             if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                ],
    		    "columnDefs": [
    		        { "type": 'ip-address', targets: [2,3] },
                ],
                
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-profilesB_wrapper .title').append('<h5>Controlador</h5>');
    		
    		$('#table-profilesB tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {
                    
                    table_profilesB.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    profileB_selected = table_profilesB.row( this ).data();
                    $('.modal-prifileB').modal('show');
                }
            });
            
            $('#btn-delete-profile').click(function() {

                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
                
                var text = '¿Está seguro que desea eliminar este (profile)?';
    
                bootbox.confirm(text, function(result) {
                    
                    if (result) {
                        
                         openModalPreloader("Eliminado Profile del Controlador ...");
                
                        $('.modal-prifileB').modal('hide');
                        
                         $.ajax({
                            url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikPppoeTa', 'action' => 'deleteProfileInController'])?>" ,
                            type: 'POST',
                            dataType: "json",
                            data: JSON.stringify({controller_id: controller_id, profile_api_id: profileB_selected.api_id}),
                            success: function(data){
                                
                                closeModalPreloader();
            
                                if (!data.data) {
                                     generateNoty('error', 'Error al intentar eliminar.');
                                }else{
                                    
                                    generateNoty('success', 'Eliminación completa.');
                                    
                                    table_profilesA.ajax.reload();
                                    table_profilesB.ajax.reload();
                                }
                                
                                 updateCountersTitle();
                            },
                            error: function (jqXHR) {
        
                                if (jqXHR.status == 403) {
                                
                                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                                
                                	setTimeout(function() { 
                                	  window.location.href ="/ispbrain";
                                	}, 3000);
                                
                                } else {
                                	closeModalPreloader();
                                    generateNoty('error', 'Error al intentar eliminar.');
                                     updateCountersTitle();
                                }
                            }
                        });
                        
                    };
                });
                
            });
    		
    		 //end profilesB

    		$('a[id="profiles-tab"]').on('shown.bs.tab', function (e) {
    		    
    		    table_profilesA.ajax.reload();
    		    table_profilesB.ajax.reload();
    		    
                table_profilesA.draw();
                table_profilesB.draw();
    	
            });

           
        });
        
        
        
    </script>
<?php $this->end(); ?>
