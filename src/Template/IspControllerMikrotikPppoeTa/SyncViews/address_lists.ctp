<?php $this->extend('/IspControllerMikrotikPppoeTa/SyncViews/tabs'); ?>

<?php $this->start('address-lists'); ?>
 
<style type="text/css">
    
     #table-address_listsA_wrapper .title{
        color: #696868;
        padding-top: 10px;
    }
    
     #table-address_listsB_wrapper .title{
        color: #696868;
        padding-top: 10px;
    }
    
</style>


    <div class="row">
        <div class="col-xl-5">
            
            <div class="card border-secondary mb-1 mt-2">
                <div class="card-body p-1">
                    <div class="text-secondary p-0">Controlador Seleccionado: <span class="font-weight-bold"><?=$controller->name?></span></div>
                </div>
            </div>
        </div>
        <div class="col-xl-1">
            <?php
              echo $this->Html->link(
                '<span class="glyphicon icon-loop2" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                    'id' => 'btn-refresh-address_lists',
                    'title' => 'Click para realizar el proceso control de diferencias',
                    'class' => 'btn btn-default mt-2',
                    'data-toggle' => 'tooltip',
                    'escape' => false
                ]);
             
             ?>
        </div>
        <div class="col-xl-2">
            
             <?php
              echo $this->Html->link(
                'Sincronizar todo',
                'javascript:void(0)',
                [
                    'id' => 'btn-sync-address_lists',
                    'title' => '',
                    'class' => 'btn btn-primary mt-2',
                ]);
             
             ?>

        </div>
        
        <div class="col-xl-4 text-right">
            <?= $this->Form->input('clear_address_lists', [
                'label' => 'Limpiar (address-list) del controlador',
                'checked' => false,
                'class' => 'm-0 mr-3 mt-3',
                'title' => 'Eliminar los registros agenos a este controlador. Los marcados en azul.',
                'data-toggle' => 'tooltip',
                ])?>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-6">
            <table class="table table-bordered table-hover" id="table-address_listsA">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>List</th>
                        <th>Address</th>
                        <th>Comment</th>
                    </tr>
                </thead>
                <tbody>
                  
                </tbody>
            </table>
        </div>
        <div class="col-xl-6">
            <table class="table table-bordered table-hover" id="table-address_listsB">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>List</th>
                        <th>Address</th>
                        <th>Comment</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div>
    
    
    
<?php

    $buttonsA = [];

    $buttonsA[] = [
        'id'   =>  'btn-edit-address_list',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];
    
    $buttonsA[] = [
        'id'   =>  'btn-sync-address_list-indi',
        'name' =>  'Sincronizar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-addressListA', 'title' => 'Acciones', 'buttons' => $buttonsA ]);
    
    $buttonsB = [];

    $buttonsB[] = [
        'id'   =>  'btn-delete-address_list',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-addressListB', 'title' => 'Acciones', 'buttons' => $buttonsB ]);
  
?>


    <script type="text/javascript">

        var table_address_listsA = null;
        var table_address_listsB = null;
        var address_listA_selected = null;

        $(document).ready(function () {
            //address_listsA

            $('#table-address_listsA').removeClass('display');

    		table_address_listsA = $('#table-address_listsA').DataTable({
    		    "order": [[ 2, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ordering" : false,
    		    "paging": false,
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/sync/mikrotik_pppoe_ta/addressListsA.json",
                    "dataSrc": "data",
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "api_id",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "list",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "address",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "comment",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    }
                ],
    		    "columnDefs": [
    		        { "type": 'ip-address', targets: [2] },
                   
                ],
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-address_listsA_wrapper .title').append('<h5>Sistema</h5>');
    		
    		$('#table-address_listsA tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {
                    
                    table_address_listsA.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    address_listA_selected = table_address_listsA.row( this ).data();
                    $('.modal-addressListA').modal('show');
                }
            });
            
            $('#btn-edit-address_list').click(function() {
                var action = '/ispbrain/IspControllerMikrotikPppoeTa/config/' + controller_id;
                window.open(action, '_black');
            });
            
            $('#btn-sync-address_lists').click(function() {

                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
                
                if(table_address_listsA.rows('tr').count() == 0 && table_address_listsB.rows('tr').count() == 0){
            
                    generateNoty('warning', 'No existen elementos para sincronizar.');
                    
                    return false;
                }
                
                var address_lists_ids = [];
                
                $.each(table_address_listsA.rows().data(), function(i, address_list){
                    address_lists_ids.push(address_list.id);
                });
                
                console.log(address_lists_ids);
          
                var text = '¿Está seguro que desea sincronizar (Address Lists)?';
    
                bootbox.confirm(text, function(result) {
                    if (result) {
                    
                        openModalPreloader("Sincronizando ...");
                        
                         $.ajax({
                            url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikPppoeTa', 'action' => 'sync-address_lists'])?>" ,
                            type: 'POST',
                            dataType: "json",
                            data: JSON.stringify({controller_id: controller_id, address_lists_ids: address_lists_ids, delete_address_lists_side_b: $('#clear-address-lists').is(':checked')}),
                            success: function(data){
                                
                                if(data.response){
                                    generateNoty('success', 'Sincronización de (Address Lists) completa');
                                }else{
                                    generateNoty('error', 'Error en la Sincronización');
                                }
                                
                                closeModalPreloader();
                               
                                table_address_listsA.ajax.reload();
                                table_address_listsB.ajax.reload();
                                
                                updateCountersTitle();
                               
                            },
                            error: function (jqXHR) {

                                if (jqXHR.status == 403) {
                                
                                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                                
                                	setTimeout(function() { 
                                	  window.location.href ="/ispbrain";
                                	}, 3000);
                                
                                } else {
                                	closeModalPreloader();
                                    generateNoty('error', 'Error al intentar sincronizar los (Address Lists).');
                                    updateCountersTitle();
                                }
                            }
                        });
                    }
                });
            });
            
            $('#btn-sync-address_list-indi').click(function() {
                
                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }

                var address_lists_ids = [];
                address_lists_ids.push(address_listA_selected.id);
          
                var text = '¿Está seguro que desea sincronizar este (address-list)?';
    
                bootbox.confirm(text, function(result) {
                    if (result) {
                        
                         $('.modal-addressListA').modal('hide');
                    
                        openModalPreloader("Sincronizando ...");
                        
                         $.ajax({
                            url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikPppoeTa', 'action' => 'sync-address-list-indi'])?>" ,
                            type: 'POST',
                            dataType: "json",
                            data: JSON.stringify({controller_id: controller_id, address_lists_ids: address_lists_ids}),
                            success: function(data){
                                
                                if(data.response){
                                    generateNoty('success', 'Sincronización de (address-lists) completa');
                                }else{
                                    generateNoty('error', 'Error en la Sincronización');
                                }
                                
                                closeModalPreloader();
                               
                                table_address_listsA.ajax.reload();
                                table_address_listsB.ajax.reload();
                                
                                updateCountersTitle();
                               
                            },
                            error: function (jqXHR ) {
                                                    
                                if(jqXHR.status == 403){
                                    
                                    generateNoty('warning', 'La sesión ha expirado. Por favor ingrese sus credenciales.');
                                    
                                    setTimeout(function(){ 
                                      window.location.href ="/ispbrain";
                                    }, 3000);
                                    
                                }else{
                                    generateNoty('error', 'Error al intentar sincronizar los (address-lists).');
                                    closeModalPreloader();
                                    updateCountersTitle();
                                }
                            }
                            
                        });
                    }
                });
               
                
            });
            
            $('#btn-refresh-address_lists').click(function() {

                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
            
                openModalPreloader("Actualizando ...");
                
                 $.ajax({
                    url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikPppoeTa', 'action' => 'refreshAddressLists'])?>" ,
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify({controller_id: controller_id}),
                    success: function(data){
                        
                        closeModalPreloader();
    
                        if (!data.data) {
                             generateNoty('error', 'No se pudo establecer una conexión con el controlador.');
                        }else{
                            table_address_listsA.ajax.reload();
                            table_address_listsB.ajax.reload();
                            
                            generateNoty('success', 'Actualización completa.');
                        }
                        
                        updateCountersTitle();
                    },
                    error: function (jqXHR) {

                        if (jqXHR.status == 403) {
                                
                        	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                        
                        	setTimeout(function() { 
                        	  window.location.href ="/ispbrain";
                        	}, 3000);
                        
                        } else {
                        	closeModalPreloader();
                            generateNoty('error', 'Error al intentar actualizar.');
                            updateCountersTitle();
                        }
                    }
                });
                
            });
            

            //end address_listsA

            //address_listsB

            $('#table-address_listsB').removeClass('display');

    		table_address_listsB = $('#table-address_listsB').DataTable({
    		    "order": [[ 2, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ordering" : false,
    		    "paging": false,
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/sync/mikrotik_pppoe_ta/addressListsB.json",
                    "dataSrc": "data",
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "api_id",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "list",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "address",
                        "render": function ( data, type, row ) {
                             if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "comment",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    }
                ],
    		    "columnDefs": [
    		        { "type": 'ip-address', targets: [2] },
                ],
                
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-address_listsB_wrapper .title').append('<h5>Controlador</h5>');
    		
    		$('#table-address_listsB tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {
                    
                    table_address_listsB.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    address_listB_selected = table_address_listsB.row( this ).data();
                    $('.modal-addressListB').modal('show');
                }
            });
            
            $('#btn-delete-address_list').click(function() {

                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
                
                openModalPreloader("Eliminado (address-list) del Controlador ...");
                
                $('.modal-addressListB').modal('hide');
                
                 $.ajax({
                    url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikPppoeTa', 'action' => 'deleteAddressListInController'])?>" ,
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify({controller_id: controller_id, address_list_api_id: address_listB_selected.api_id}),
                    success: function(data){
                        
                        closeModalPreloader();
    
                        if (!data.data) {
                             generateNoty('error', 'Error al intentar eliminar.');
                        }else{
                            table_address_listsA.ajax.reload();
                            table_address_listsB.ajax.reload();
                            
                            generateNoty('success', 'Eliminación completa.');
                        }
                        
                        updateCountersTitle();
                    },
                    error: function (jqXHR) {

                        if (jqXHR.status == 403) {
                        
                        	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                        
                        	setTimeout(function() { 
                        	  window.location.href ="/ispbrain";
                        	}, 3000);
                        
                        } else {
                        	closeModalPreloader();
                            generateNoty('error', 'Error al intentar eliminar.');
                            updateCountersTitle();
                        }
                    }
                });
            });
    		
    		 //end address_listsB

    		$('a[id="address-lists-tab"]').on('shown.bs.tab', function (e) {
    		    
                table_address_listsA.ajax.reload();
    		    table_address_listsB.ajax.reload();
    		    
                table_address_listsA.draw();
                table_address_listsB.draw();
            });

           
        });
    </script>
<?php $this->end(); ?>
