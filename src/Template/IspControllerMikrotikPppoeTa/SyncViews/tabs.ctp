<style type="text/css">

    .nav-link {
        cursor: pointer;
    }

</style>

<div class="row">

    <div class="col-md-12" id="myTabs">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            
            <li class="nav-item">
                <a class="nav-link active" id="queues-tab" data-target="#queues" role="tab" aria-controls="queues" aria-expanded="false">Queues</a>
            </li>

            <li class="nav-item">
                <a class="nav-link " id="secrets-tab" data-target="#secrets" role="tab" aria-controls="secrets" aria-expanded="true">Secrets</a>
            </li>
            
            <li class="nav-item d-none">
                <a class="nav-link " id="actives-tab" data-target="#actives" role="tab" aria-controls="actives" aria-expanded="true">Actives</a>
            </li>
           
            <li class="nav-item">
                <a class="nav-link " id="profiles-tab" data-target="#profiles" role="tab" aria-controls="profiles" aria-expanded="false">Profiles</a>
            </li>
            
             <li class="nav-item">
                <a class="nav-link " id="address-lists-tab" data-target="#address-lists" role="tab" aria-controls="address-lists" aria-expanded="false">Address Lists</a>
            </li>
            
             <li class="nav-item d-none">
                <a class="nav-link " id="pppoe-servers-tab" data-target="#pppoe-servers" role="tab" aria-controls="pppoe-servers" aria-expanded="false">PPPoE Servers</a>
            </li>

        </ul>

        <!-- Tab panes -->
        <div class="tab-content" id="myTabContent">
            
            <div class="tab-pane fade show active" id="queues" role="tabpanel" aria-labelledby="queues-tab">
                <?= $this->fetch('queues') ?>
            </div>
            
            <div class="tab-pane fade" id="secrets" role="tabpanel" aria-labelledby="secrets-tab">
                <?= $this->fetch('secrets') ?>
            </div>
            
            <div class="tab-pane fade d-none" id="actives" role="tabpanel" aria-labelledby="actives-tab">
                <?php $this->fetch('actives'); ?>
            </div>
            
             <div class="tab-pane fade" id="profiles" role="tabpanel" aria-labelledby="profiles-tab">
                <?= $this->fetch('profiles') ?>
            </div>

            <div class="tab-pane fade" id="address-lists" role="tabpanel" aria-labelledby="address-lists-tab">
                <?= $this->fetch('address-lists') ?>
            </div>
            
            <div class="tab-pane fade d-none" id="pppoe-servers" role="tabpanel" aria-labelledby="pppoe-servers-tab">
                <?= $this->fetch('pppoe-servers') ?>
            </div>

        </div>
    </div>

</div>

<?php
    echo $this->element('modal_preloader');
?>

<script type="text/javascript">

    var controller_id = <?php echo json_encode($controller->id); ?>;
    var integrationEnabled = <?php echo json_encode($controller->integration); ?>;

    $(document).ready(function () {

        $('#myTab a').click(function (e) {

            e.preventDefault();

            var target = $(this).data('target');
            $('#myTab a.active').removeClass('active');

            $('#myTabContent .active').fadeOut(100, function() {
            $('#myTabContent .active').removeClass('active');

                $(target).fadeIn(100, function() {
                    $(target).addClass('active show');
                    $(target+'-tab').addClass('active');
                    $(target+'-tab').trigger('shown.bs.tab');
                });
            });
        });
        
         $(function () {
          $('[data-toggle="tooltip"]').tooltip()
        });
        
         $(document).click(function() {
             $('[data-toggle="tooltip"]').tooltip('hide');
        });

    });
    
    

</script>
