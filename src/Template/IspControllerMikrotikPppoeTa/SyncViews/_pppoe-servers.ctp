<?php $this->extend('/IspControllerMikrotikPppoeTa/SyncViews/tabs'); ?>

<?php $this->start('pppoe-servers'); ?>

<style type="text/css">
    
     #table-pppoeserversA_wrapper .title{
        color: #696868;
        padding-top: 10px;
    }
    
     #table-pppoeserversB_wrapper .title{
        color: #696868;
        padding-top: 10px;
    }
    
</style>


    <div class="row">
        <div class="col-xl-5">
            
            <div class="card border-secondary mb-1 mt-2">
                <div class="card-body p-1">
                    <div class="text-secondary p-0">Controlador Seleccionado: <span class="font-weight-bold"><?=$controller->name?></span></div>
                </div>
            </div>
        </div>
        <div class="col-xl-1">
           
            <?php
              echo $this->Html->link(
                '<span class="glyphicon icon-loop2" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                    'id' => 'btn-refresh-pppoeservers',
                    'title' => 'Click para realizar el Proceso control de diferencias',
                    'class' => 'btn btn-default mt-2',
                    'data-toggle' => 'tooltip',
                    'escape' => false
                ]);
             ?>
          
        </div>
        <div class="col-xl-2">
            
             <?php
              echo $this->Html->link(
                'Sincronizar todo',
                'javascript:void(0)',
                [
                    'id' => 'btn-sync-pppoeservers',
                    'title' => '',
                    'class' => 'btn btn-primary mt-2',
                ]);
             
             ?>

        </div>
        
        <div class="col-xl-4 text-right">
            <?= $this->Form->input('clear_pppoeservers', [
                'label' => 'Limpiar (pppoe-server) del controlador',
                'checked' => false,
                'title' => 'Eliminar los registros agenos a este controlador. Los marcados en azul.',
                'data-toggle' => 'tooltip',
                'class' => 'm-0 mr-3 mt-3'
                ])?>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-6">
            <table class="table table-bordered table-hover" id="table-pppoeserversA">
                <thead>
                    <tr>
                       <th>Id</th>
                       <th>Service Name</th>
                       <th>Interface</th>
                       <th>disabled</th>
                    </tr>
                </thead>
                <tbody>
                  
                </tbody>
            </table>
        </div>
        <div class="col-xl-6">
            <table class="table table-bordered table-hover" id="table-pppoeserversB">
                <thead>
                    <tr>
                       <th>Id</th>
                       <th>Service Name</th>
                       <th>Interface</th>
                       <th>disabled</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div>
    
    
    
<?php

    $buttonsA = [];

    $buttonsA[] = [
        'id'   =>  'btn-edit-pppoeserver',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];
    
    $buttonsA[] = [
        'id'   =>  'btn-sync-pppoeserver-indi',
        'name' =>  'Sincronizar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-pppoeserverA', 'title' => 'Acciones', 'buttons' => $buttonsA ]);
    
    
    $buttonsB = [];

    $buttonsB[] = [
        'id'   =>  'btn-delete-pppoeserver',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-pppoeserverB', 'title' => 'Acciones', 'buttons' => $buttonsB ]);
  
?>


    <script type="text/javascript">


        var table_pppoeserversA = null;
        var table_pppoeserversB = null;
        var pppoeserverA_selected = null;

        $(document).ready(function () {
            
            
            //pppoeserversA

            $('#table-pppoeserversA').removeClass('display');

    		table_pppoeserversA = $('#table-pppoeserversA').DataTable({
    		    "order": [[ 0, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ordering" : false,
    		    "paging": false,
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/sync/mikrotik_pppoe_ta/pppoeServersA.json",
                    "dataSrc": "data",
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                				setTimeout(function() { 
                				  window.location.href = "/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "api_id",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "service_name",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "interface",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "disabled",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    }
                ],
    		    "columnDefs": [
    		       
                ],
                "createdRow" : function( row, data, index ) {
                    row.id = data.id;
                },
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-pppoeserversA_wrapper .title').append('<h5>Sistema</h5>');
    		
    		$('#table-pppoeserversA tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {
                    
                    table_pppoeserversA.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    pppoeserverA_selected = table_pppoeserversA.row( this ).data();
                    $('.modal-pppoeserverA').modal('show');
                }
            });
            
            $('#btn-edit-pppoeserver').click(function() {
                var action = '/ispbrain/IspControllerMikrotikPppoeTa/config/' + pppoeserverA_selected.controller_id;
                window.open(action, '_black');
            });
            
            $('#btn-sync-pppoeservers').click(function() {
                
                if(table_pppoeserversA.rows('tr').count() == 0 && table_pppoeserversB.rows('tr').count() == 0){
            
                    generateNoty('warning', 'No existen elementos para sincronizar.');
                    
                }else{
                    
                    var ids = [];
                
                    $.each(table_pppoeserversA.$('tr'), function(i, pppoeserver){
                        ids.push(pppoeserver.id);
                    });
              
                    var text = '¿Está seguro que desea sincronizar los (pppoe-servers)?';
        
                    bootbox.confirm(text, function(result) {
                        if (result) {
                        
                            openModalPreloader('Sincronizando ... ');
                            
                             $.ajax({
                                url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikPppoeTa', 'action' => 'syncPppoeServers'])?>" ,
                                type: 'POST',
                                dataType: "json",
                                data: JSON.stringify({controller_id: controller_id, ids: ids, delete_pppoeserver_side_b: $('#clear-pppoeservers').is(':checked')}),
                                success: function(data){
                                
                                    closeModalPreloader();
                
                                    if (!data.response) {
                                         generateNoty('error', 'Error al intentar sincronizar los (pppoe servers).');
                                    }else{
                                        
                                        generateNoty('success', 'Sincronizacion (pppoe servers) completa.');
                                        
                                        table_pppoeserversA.ajax.reload();
                                        table_pppoeserversB.ajax.reload();
                                    }
                                    
                                    updateCountersTitle();
                                },
                                error: function (jqXHR) {
                                    
                                    if(jqXHR.status == 403){
                                        
                                        generateNoty('warning', 'La sesión ha expirado. Por favor ingrese sus credenciales.');
                                        
                                        setTimeout(function(){ 
                                          window.location.href ="/ispbrain";
                                        }, 3000);
                                        
                                    }else{
                                        generateNoty('error', 'Error al intentar sincronizar los (pppoe servers).');
                                        closeModalPreloader();
                                        
                                        updateCountersTitle();
                                    }
                                }
                                
                            });
                        }
                    });
                    
                }
                
                
                
                
            });
            
            $('#btn-sync-pppoeserver-indi').click(function() {
              
                var ids = [];
                ids.push(pppoeserverA_selected.id);
          
                var text = '¿Está seguro que desea sincronizar este (pppoe-server)?';
    
                bootbox.confirm(text, function(result) {
                    
                    if (result) {
                        
                        $('.modal-pppoeserverA').modal('hide');
                    
                        openModalPreloader('Sincronizando ... ');
                        
                         $.ajax({
                            url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikPppoeTa', 'action' => 'sync-pppoe-server-indi'])?>" ,
                            type: 'POST',
                            dataType: "json",
                            data: JSON.stringify({controller_id: controller_id, ids: ids}),
                            success: function(data){
                                
                                console.log(data);
                                
                                closeModalPreloader();
            
                                if (!data.response) {
                                     generateNoty('error', 'Error al intentar sincronizar los (pppoe servers).');
                                }else{
                                    
                                    generateNoty('success', 'Sincronizacion (pppoe server) completa.');
                                    
                                    table_pppoeserversA.ajax.reload();
                                    table_pppoeserversB.ajax.reload();
                                }
                                
                                updateCountersTitle();
                            },
                            error: function (jqXHR) {
                                
                                if(jqXHR.status == 403){
                                    
                                    generateNoty('warning', 'La sesión ha expirado. Por favor ingrese sus credenciales.');
                                    
                                    setTimeout(function(){ 
                                      window.location.href ="/ispbrain";
                                    }, 3000);
                                    
                                }else{
                                    generateNoty('error', 'Error al intentar sincronizar los (pppoe servers).');
                                    closeModalPreloader();
                                    
                                    updateCountersTitle();
                                }
                            }
                            
                        });
                    }
                });
                    
               
                
            });
            
            $('#btn-refresh-pppoeservers').click(function() {
               
                openModalPreloader('Actualizando ... ');
                
                 $.ajax({
                    url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikPppoeTa', 'action' => 'refresh-pppoe-servers'])?>" ,
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify({controller_id: controller_id}),
                    success: function(data){
                        
                         closeModalPreloader();
    
                        if (!data.data) {
                             generateNoty('error', 'No se pudo establecer una conexión con el controlador.');
                        }else{
                            
                            generateNoty('success', 'Actualización completa.');
                            
                            table_pppoeserversA.ajax.reload();
                            table_pppoeserversB.ajax.reload();
                        }
                        
                        updateCountersTitle();
                    },
                    error: function (jqXHR ) {
                        
                        if(jqXHR.status == 403){
                            
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese sus credenciales.');
                            
                            setTimeout(function(){ 
                              window.location.href ="/ispbrain";
                            }, 3000);
                            
                        }else{
                            generateNoty('error', 'Error al intentar actualizar.');
                            closeModalPreloader();
                            updateCountersTitle();
                        }
                    }
                });
                
            });

            //end pppoeserversA

            //pppoeserversB

            $('#table-pppoeserversB').removeClass('display');

    		table_pppoeserversB = $('#table-pppoeserversB').DataTable({
    		    "order": [[ 0, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ordering" : false,
    		    "paging": false,
    		    
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/sync/mikrotik_pppoe_ta/pppoeServersB.json",
                    "dataSrc": "data",
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "api_id",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                             else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "service_name",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            } else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "interface",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            } else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "disabled",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            } else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    }
                    
                ],
    		    "columnDefs": [
    		        
                ],
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-pppoeserversB_wrapper .title').append('<h5>Controlador</h5>');
    		
    		$('#table-pppoeserversB tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {
                    
                    table_pppoeserversB.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    pppoeserverB_selected = table_pppoeserversB.row( this ).data();
                    $('.modal-pppoeserverB').modal('show');
                }
            });
            
            $('#btn-delete-pppoeserver').click(function() {
                
                var text = '¿Está seguro que desea eliminar este (pppoe-server)?';
    
                bootbox.confirm(text, function(result) {
                    
                    if (result) {
                        
                        $('.modal-pppoeserverB').modal('hide');
                        
                         openModalPreloader('Eliminado PPPOEServer del Controlador ... ');
                        
                         $.ajax({
                            url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikPppoeTa', 'action' => 'delete-pppoe-server-in-controller'])?>" ,
                            type: 'POST',
                            dataType: "json",
                            data: JSON.stringify({controller_id: controller_id, pppoeserver_api_id: pppoeserverB_selected.api_id}),
                            success: function(data){
                                
                                closeModalPreloader();
            
                                if (!data.response) {
                                     generateNoty('error', 'Error al intentar eliminar.');
                                }else{
                                    
                                    generateNoty('success', 'Eliminación completa.');
                                    
                                    table_pppoeserversA.ajax.reload();
                                    table_pppoeserversB.ajax.reload();
                                }
                                
                                updateCountersTitle();
                            },
                            error: function (jqXHR) {
                                
                                if(jqXHR.status == 403){
                                    
                                    generateNoty('warning', 'La sesión ha expirado. Por favor ingrese sus credenciales.');
                                    
                                    setTimeout(function(){ 
                                      window.location.href ="/ispbrain";
                                    }, 3000);
                                    
                                }else{
                                    generateNoty('error', 'Error al intentar eliminar.');
                                    closeModalPreloader();
                                    updateCountersTitle();
                                }
                            }
                        });
                                
                        
                    }
                });
               
            });
    		
    		 //end pppoeserversB

    		$('a[id="pppoe-servers-tab"]').on('shown.bs.tab', function (e) {
                
                table_pppoeserversA.ajax.reload();
    		    table_pppoeserversB.ajax.reload();
    		    
                table_pppoeserversA.draw();
                table_pppoeserversB.draw();
           
            });
            
           
           
        });
        
  
        
        
    </script>
<?php $this->end(); ?>
