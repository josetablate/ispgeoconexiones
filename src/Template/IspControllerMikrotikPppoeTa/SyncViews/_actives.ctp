<?php $this->extend('/IspControllerMikrotikPppoeTa/SyncViews/profiles'); ?>

<?php $this->start('actives'); ?>

<style type="text/css">
    
     #table-activesA_wrapper .title{
        color: #696868;
        padding-top: 10px;
    }
    
     #table-activesB_wrapper .title{
        color: #696868;
        padding-top: 10px;
    }
    
</style>


    <div class="row">
        <div class="col-xl-5">
            
            <div class="card border-secondary mb-1 mt-2">
                <div class="card-body p-1">
                    <div class="text-secondary p-0">Controlador Seleccionado: <span class="font-weight-bold"><?=$controller->name?></span></div>
                </div>
            </div>
        </div>
        <div class="col-xl-1">
            <?php
              echo $this->Html->link(
                '<span class="glyphicon icon-loop2" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                    'id' => 'btn-refresh-actives',
                    'title' => 'Click para realizar el proceso control de diferencias',
                    'class' => 'btn btn-default mt-2',
                    'data-toggle' => 'tooltip',
                    'escape' => false
                ]);
             
             ?>
        </div>
        <div class="col-xl-2">
            
             <?php
              echo $this->Html->link(
                'Sincronizar todo',
                'javascript:void(0)',
                [
                    'id' => 'btn-sync-actives',
                    'title' => '',
                    'class' => 'btn btn-primary mt-2',
                ]);
             
             ?>

        </div>
        
        <div class="col-xl-4 text-right">
            <?= $this->Form->input('clear_actives', [
                'label' => 'Limpiar (actives) del controlador', 
                'checked' => false, 
                'class' => 'm-0 mr-3 mt-3',
                'title' => 'Eliminar los registros agenos a este controlador. Los marcados en azul.',
                'data-toggle' => 'tooltip',
                ])?>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-6">
            <table class="table table-bordered table-hover" id="table-activesA">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Remote Address</th>
                        <th>Comment</th>
                    </tr>
                </thead>
                <tbody>
                  
                </tbody>
            </table>
        </div>
        <div class="col-xl-6">
            <table class="table table-bordered table-hover" id="table-activesB">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Comment</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div>
    
    
    
<?php

    $buttonsA = [];

    $buttonsA[] = [
        'id'   =>  'btn-edit-active',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-activeA', 'title' => 'Acciones', 'buttons' => $buttonsA ]);
    
    
    $buttonsB = [];

    $buttonsB[] = [
        'id'   =>  'btn-delete-active',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-activeB', 'title' => 'Acciones', 'buttons' => $buttonsB ]);
  
?>


    <script type="text/javascript">

        var table_activesA = null;
        var table_activesB = null;
        var activeA_selected = null;

        $(document).ready(function () {
            //activesA

            $('#table-activesA').removeClass('display');

    		table_activesA = $('#table-activesA').DataTable({
    		    "order": [[ 1, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ordering" : false,
    		    "paging": false,
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/sync/mikrotik_pppoe_ta/activesA.json",
                    "dataSrc": "data",
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "api_id",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "name",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "remote_address",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "comment",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                ],
    		    "columnDefs": [
    		        { "type": 'ip-address', targets: [2] },
                ],
                "createdRow" : function( row, data, index ) {
                    row.id = data.connection_id;
                },
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-activesA_wrapper .title').append('<h5>Sistema</h5>');
    		
    		$('#table-activesA tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {
                    
                    table_activesA.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    activeA_selected = table_activesA.row( this ).data();
                    $('.modal-activeA').modal('show');
                }
            });
            
            $('#btn-edit-active').click(function() {
                var action = '/ispbrain/connections/edit/' + activeA_selected.connection_id;
                window.open(action, '_black');
            });
            
            $('#btn-sync-actives').click(function() {
                
                if(table_activesA.rows('tr').count() == 0 && table_activesB.rows('tr').count() == 0){
            
                    generateNoty('warning', 'No existen elementos para sincronizar.');
                    
                    return false;
                    
                }
                
                var connections_ids = [];
                
                $.each(table_activesA.$('tr'), function(i, active){
                    connections_ids.push(active.id);
                });
          
                var text = '¿Está seguro que desea sincronizar (Actives) ?';
    
                bootbox.confirm(text, function(result) {
                    if (result) {
                    
                        openModalPreloader('Sincronizando ... ');
                        
                         $.ajax({
                            url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikPppoeTa', 'action' => 'sync-actives'])?>" ,
                            type: 'POST',
                            dataType: "json",
                            data: JSON.stringify({controller_id: controller_id, connections_ids: connections_ids, delete_active_side_b: $('#clear-actives').is(':checked')}),
                            success: function(data){

                                closeModalPreloader();
            
                                if (!data.data) {
                                     generateNoty('error', 'Error al intentar sincronizar (actives).');
                                }else{
                                    generateNoty('success', 'Sincronización de (actives) completo.');
                                }
                                
                                table_activesA.ajax.reload();
                                table_activesB.ajax.reload();
                            },
                            error: function (jqXHR) {

                                if (jqXHR.status == 403) {
                                
                                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                                
                                	setTimeout(function() { 
                                	  window.location.href ="/ispbrain";
                                	}, 3000);
                                
                                } else {
                                	closeModalPreloader();
                                    generateNoty('error', 'Error al intentar sincronizar (actives).');
                                }
                            }
                        });
                    }
                });
            });
            
            $('#btn-refresh-actives').click(function() {
               
                openModalPreloader('Actualizando ... ');
                
                 $.ajax({
                    url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikPppoeTa', 'action' => 'refreshActives'])?>" ,
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify({controller_id: controller_id}),
                    success: function(data){
                        
                         closeModalPreloader();
    
                        if (!data.data) {
                            generateNoty('error', 'Error al intentar actualizar.');
                        }else{
                            generateNoty('success', 'Actualización completa.');
                        }
                        
                        table_activesA.ajax.reload();
                        table_activesB.ajax.reload();
                        
                        updateCountersTitle();
                    },
                    error: function (jqXHR) {

                        if (jqXHR.status == 403) {
                        
                        	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                        
                        	setTimeout(function() { 
                        	  window.location.href ="/ispbrain";
                        	}, 3000);
                        
                        } else {
                        	closeModalPreloader();
                            generateNoty('error', 'Error al intentar actualizar.');
                            updateCountersTitle();
                        }
                    }
                });
                
            });
            

            //end activesA

            //activesB

            $('#table-activesB').removeClass('display');

    		table_activesB = $('#table-activesB').DataTable({
    		    "order": [[ 1, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ordering" : false,
    		    "paging": false,
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/sync/mikrotik_pppoe_ta/activesB.json",
                    "dataSrc": "data",
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "api_id",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                             else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "name",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "address",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                             else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    
                    { 
                        "data": "comment",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                             else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                ],
    		    "columnDefs": [
    		        { "type": 'ip-address', targets: [2] },
                ],
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-activesB_wrapper .title').append('<h5>Controlador</h5>');
    		
    		$('#table-activesB tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {
                    
                    table_activesB.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    activeB_selected = table_activesB.row( this ).data();
                    $('.modal-activeB').modal('show');
                }
            });
            
            $('#btn-delete-active').click(function() {
               
               $('.modal-activeB').modal('hide');
               
                openModalPreloader('Eliminado (active) del Controlador ... ');
                
                 $.ajax({
                    url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikPppoeTa', 'action' => 'deleteactiveInController'])?>" ,
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify({controller_id: controller_id, active_api_id: activeB_selected.api_id}),
                    success: function(data){
                        
                        closeModalPreloader();
    
                        if (!data.data) {
                             generateNoty('error', 'Error al intentar eliminar.');
                        }else{
                            generateNoty('success', 'Eliminación completa.');
                        }
                        
                        table_activesA.ajax.reload();
                        table_activesB.ajax.reload();
                        
                        updateCountersTitle();
                    },
                    error: function (jqXHR) {

                        if (jqXHR.status == 403) {
                        
                        	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                        
                        	setTimeout(function() { 
                        	  window.location.href ="/ispbrain";
                        	}, 3000);
                        
                        } else {
                        	closeModalPreloader();
                            generateNoty('error', 'Error al intentar eliminar.');
                            updateCountersTitle();
                        }
                    }
                });
            });
    		
    		 //end activesB

    		$('a[id="actives-tab"]').on('shown.bs.tab', function (e) {
    		    
    		    table_activesA.draw();
    		    table_activesB.draw();
            });

           
        });
        
    
    </script>
<?php $this->end(); ?>
