<style type="text/css">

    #table-selected {
        max-height: 450px;
        height: 450px;
        display: inline-block;
        width: 100%;
        overflow: auto;
   }

    textarea.form-control {
        height: 80px !important;
    }

    .rol-sytem {
        font-size: 16px;
        font-weight: 500;
    }
    
</style>

<div id="btns-tools">
    <div class="btns-tools margin-bottom-5">

        <?php 

            echo  $this->Html->link(
                '<span class="glyphicon icon-checkbox-checked" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Seleccionar todos',
                'class' => 'btn btn-default btn-selected-all ml-2',
                'escape' => false
            ]);

            echo  $this->Html->link(
                '<span class="glyphicon icon-checkbox-unchecked" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Limpiar Selección',
                'class' => 'btn btn-default btn-clear-selected-all ml-2',
                'escape' => false
            ]);
        ?>

    </div>
</div>

<div class="row">

    <div class="col-md-12">

        <?= $this->Form->create($role,  ['class' => 'form-load']) ?>
            <fieldset>
                <div class="row">
                    <div class="col-md-2">
                        <?php
                            echo $this->Form->input('name', ['label' => 'Nombre', 'type' => 'textarea']);
                            echo $this->Form->input('enabled', ['label' => 'Habilitar', 'class' => 'input-checkbox' , 'checked' => true]);
                        ?>
                    </div>
                    <div class="col-md-5">

                       <table class="table table-hover table-bordered" id="table-actions_system-selected">
                            <thead>
                                <tr>
                                    <th>Acciones del Privilegio</th>
                                </tr>
                            </thead>
                        </table>

                    </div>
                    <div class="col-md-5">
                        
                       <table class="table table-hover table-bordered" id="table-actions_system">
                            <thead>
                                <tr>
                                    <th>Acciones del Sistema</th>
                                </tr>
                            </thead>
                        </table>

                        <?php
                            echo $this->Form->input('actions_system._ids', ['type' => 'hidden', 'value' => '']);
                        ?>
                    </div>
                </div>
            </fieldset>
            <?= $this->Html->link(__('Cancelar'),["controller" => "Roles", "action" => "index"], ['class' => 'btn btn-default']) ?>
        <?= $this->Form->button(__('Guardar'), ['class' => 'btn-submit btn-success']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>

<script type="text/javascript">

    var table_actions_system = null;
    var table_actions_system_selected = null;
    var actions_system_selected = [];
    var actions_system_selected_ids = [];

    $(document).ready(function() {

        var role = <?= json_encode($role) ?>;

        $('#table-actions_system').removeClass('display');

        $('#btns-tools').hide();

		table_actions_system = $('#table-actions_system').DataTable({
		    "ajax": {
                "url": "/ispbrain/roles/get_all_actions_system.json",
                "dataSrc": "actionsSystem",
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
    				            window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "columns": [
                { 
                    "data": 'display_name',
                    "className": 'left',
                    "render": function ( data, type, row ) {
                        return "<span class='font-weight-bold'>" + row.clase.display_name + ": " + data + "</span><br><span class='text-muted pl-3'>" + row.description+"</span>";
                    }
                },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
             "initComplete": function( settings, json ) {

                $.each(role.actions_system, function(i, action) {

                    var tr = $('#table-actions_system tbody tr#' + action.id);

                    var data = table_actions_system.row( tr ).data();

                    actions_system_selected.push(data);
                    actions_system_selected_ids.push(data.id);

                    $('#actions-system-ids').val(actions_system_selected_ids.join());

                    $(tr).addClass('selected');

                    table_actions_system_selected.clear().draw();
                    table_actions_system_selected.rows.add(actions_system_selected).draw();
                });
            },
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "paging": false,
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            "dom":
    	    	"<'row'<'col-xl-7'f><'col-xl-4 tools mr-2'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i>>",
		});

		$('#table-actions_system_wrapper .tools').append($('#btns-tools').contents());

		table_actions_system_selected = $('#table-actions_system-selected').DataTable({
            "columns": [
                { 
                    "data": 'display_name',
                    "className": 'left',
                    "render": function ( data, type, row ) {
                        return "<span class='font-weight-bold'>"+row.clase.display_name + ": " + data + "</span><br><span class='text-muted pl-3'>" + row.description + "</span>";
                    }
                },
            ],
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "paging": false,
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            "dom":
    	    	"<'row'<'col-xl-10'f>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i>>",
		});

        $('#table-actions_system tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                if ($(this).hasClass('selected')) {

                    var data = table_actions_system.row( this ).data();

                    var index =  actions_system_selected_ids.indexOf(data.id);

                    if (index > -1) {
                        actions_system_selected.splice(index, 1);
                        actions_system_selected_ids.splice(index, 1);
                    }

                    $('#actions-system-ids').val(actions_system_selected_ids.join());

                    $(this).removeClass('selected');

                } else {

                    var data = table_actions_system.row( this ).data();

                    actions_system_selected.push(data);
                    actions_system_selected_ids.push(data.id);
                    
                    $('#actions-system-ids').val(actions_system_selected_ids.join());
                    
                    $(this).addClass('selected');
                }

                table_actions_system_selected.clear().draw();
                table_actions_system_selected.rows.add(actions_system_selected).draw();
            }
        });

        $('.btn-selected-all').click(function() {

            actions_system_selected = [];
            actions_system_selected_ids = [];

            $('#table-actions_system tbody tr').each(function() {

                 $(this).addClass('selected');

                 var data = table_actions_system.row(this).data();

                 actions_system_selected.push(data);
                 actions_system_selected_ids.push(data.id);
            });

            table_actions_system_selected.clear().draw();
            table_actions_system_selected.rows.add(actions_system_selected).draw();

            $('#actions-system-ids').val(actions_system_selected_ids.join());

        });

        $('.btn-clear-selected-all').click(function() {

            actions_system_selected = [];
            actions_system_selected_ids = [];

            $('#table-actions_system tbody tr').removeClass('selected');
            table_actions_system_selected.clear().draw();

            $('#actions-system-ids').val(actions_system_selected_ids.join());

        })
    });

</script>
