<style type="text/css">

    select[multiple].actions-system, select[size].actions-system {
        height: 450px;
    }

    select.form-control {
        height: 450px !important;
    }

    .table {
        max-height: 450px;
        height: 450px;
        display: inline-block;
        width: 100%;
        overflow: auto;
   }

    textarea.form-control {
        height: 80px !important;
    }

    .rol-sytem {
        font-size: 16px;
        font-weight: 500;
    }

</style>

<div class="row">
     <div class="col-md-1">
        <div class="">
            <a class="btn btn-default" title="Ir a la lista de privilegios." href="<?= $this->Url->build(["action" => "index"]) ?>" role="button">
                <span class="glyphicon icon-arrow-left" aria-hidden="true"></span>
            </a>
        </div>
    </div>
</div>

<div class="row">
 
    <div class="col-md-12">

        <?= $this->Form->create($role,  ['class' => 'form-load']) ?>
            <fieldset>
                <div class="row">
                    <div class="col-md-2">
                        <?php
                            echo $this->Form->input('name', ['label' => 'Nombre', 'type' => 'textarea']);
                            echo $this->Form->input('enabled', ['label' => 'Habilitar/Deshabilitar', 'class' => 'input-checkbox' , 'checked' => true]);
                        ?>
                    </div>
                    <div class="col-md-5">
                       <label for="">Acciones Seleccionadas</label>
                       <table class="table" id="table-selected">
                       </table>
                    </div>
                    <div class="col-md-5">
                    
                        <?php
                            echo $this->Form->input('actions_system._ids', ['options' => $actionsSystem, 'class' => 'actions-system', 'label' => 'Acciones del sistema']);
                        ?>
                    </div>
                </div>
            </fieldset>
            <?= $this->Html->link(__('Cancelar'),["controller" => "Roles", "action" => "index"], ['class' => 'btn btn-default']) ?>
        <?= $this->Form->button(__('Agregar'), ['class' => 'btn-submit btn-success']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>



<script type="text/javascript">

    $(document).ready(function() {

        $('#actions-system-ids').change(function() {
            var tr = "";
            $( "#actions-system-ids option:selected" ).each(function() {
                tr += '<tr><td class="left rol-sytem">' + $( this ).html() + "</td></tr>";
            });
            $( "#table-selected" ).html( tr );
          }).trigger( "change" );
    });

</script>
