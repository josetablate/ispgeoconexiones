<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5" >

        <?php
            echo $this->Html->link(
                '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                ['controller' => 'roles', 'action' => 'add'],
                [
                'title' => 'Agregar nuevo privilegio',
                'class' => 'btn btn-default btn-add-connection',
                'escape' => false
            ]);

             echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export ml-1',
                'escape' => false
            ]);
        ?>

    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <table class="table table-hover table-bordered" id="table-roles">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Creado</th>
                    <th>Modificado</th>
                    <th>Activo</th>
                </tr>
            </thead>
        </table>
    </div>

    <div class="col-md-6">
        <table class="table table-hover table-bordered" id="table-actions_system-selected">
            <thead>
                <tr>
                    <th>Acciones del Privilegio</th>
                </tr>
            </thead>
        </table>
    </div>

</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-edit',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-default'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-roles', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<script type="text/javascript">

    var table_roles = null;
    var table_actions_system_selected = null;

    $(document).ready(function () {

        $('#table-roles').removeClass('display');

		table_roles = $('#table-roles').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "ajax": {
                "url": "/ispbrain/roles/index.json",
                "dataSrc": "roles",
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
        				        window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "columns": [
                { 
                    "data": 'name',
                    "className": 'left',
                },
                { 
                    "data": "created",
                    "type": "date-custom",
                    "render": function ( data, type, full, meta ) {
                        var date = new Date(data);
                        return  date.getDate() + '/' + (date.getMonth() + 1) + '/' +  date.getFullYear();
                    }
                },
                { 
                    "data": "modified",
                    "type": "date-custom",
                    "render": function ( data, type, full, meta ) {
                        var date = new Date(data);
                        return  date.getDate() + '/' + (date.getMonth() + 1) + '/' +  date.getFullYear();
                    }
                },
                { 
                    "data": 'enabled',
                    "render": function(data, type, full, meta){
                        var enabled = '<i class="fa fa-times" aria-hidden="true"></i>';
                        if (data) {
                            enabled = '<i class="fa fa-check" aria-hidden="true"></i>';
                        }
                        return enabled;
                    },
                    "width": '3%'
                },
            ],
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "paging": false,
		    "columnDefs": [
             ],
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            "dom":
        		"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
		});

		$('#table-roles_wrapper .tools').append($('#btns-tools').contents());

		$('#table-roles tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                table_roles.$('tr.selected').removeClass('selected');
                $(this).closest('tr').addClass('selected');

                var role = table_roles.row( this ).data();

                table_actions_system_selected.clear().draw();

                table_actions_system_selected.rows.add(role.actions_system).draw();

                $('.modal-roles').modal('show');
            }
        });

        $('#btns-tools').show();

        table_actions_system_selected = $('#table-actions_system-selected').DataTable({
            "columns": [
                { 
                    "data": 'display_name',
                    "className": 'left',
                    "render": function ( data, type, row ) {
                        return "<span class='font-weight-bold'>"+row.clase.display_name +": "+data+"</span><br><span class='text-muted pl-3'>"+row.description+"</span>";
                    }
                },
            ],
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "paging": false,
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            "dom":
        		"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
		});

		$('#table-actions_system-selected_wrapper .tools').append($('#btns-tools').contents());

    }); //end document ready

    $(".btn-export").click(function() {

        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if (result) {
                $('#table-roles').tableExport({tableName: 'Privilegios', type:'excel', escape:'false'});
            }
        });
    });

    $('#btn-delete').click(function() {

        var text = "¿Está Seguro que desea eliminar el privilegio?";
        var id = table_roles.row( table_roles.$('tr.selected') ).data().id;

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                    .append( $('<form/>').attr({'action': '/ispbrain/roles/delete/', 'method': 'post', 'id': 'replacer'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer').submit();
            }
        });
    });

    $('a#btn-info').click(function() {
        var row = table_roles.row( table_roles.$('tr.selected') ).data();
        var action = '/ispbrain/roles/view/' + row.id;
        window.open(action, '_self');
    });

    $('a#btn-edit').click(function() {
        var row = table_roles.row( table_roles.$('tr.selected') ).data();
        var action = '/ispbrain/roles/edit/' + row.id;
        window.open(action, '_self');
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
