<?php
/**
 * @var \App\View\AppView $this
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $presale->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $presale->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Presales'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Customers'), ['controller' => 'Customers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Customer'), ['controller' => 'Customers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="presales form large-9 medium-8 columns content">
    <?= $this->Form->create($presale) ?>
    <fieldset>
        <legend><?= __('Edit Presale') ?></legend>
        <?php
            echo $this->Form->control('installations_date');
            echo $this->Form->control('customer_code', ['options' => $customers]);
            echo $this->Form->control('debt_product_id');
            echo $this->Form->control('debt_package_id');
            echo $this->Form->control('availability');
            echo $this->Form->control('comments');
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('lat');
            echo $this->Form->control('lng');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
