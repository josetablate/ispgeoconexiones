<?= $this->Form->create(NULL, ['url' => ['controller' => 'Presales' , 'action' => 'comodato'], 'enctype' => "multipart/form-data",  'class' => 'form-load'] ) ?>

    <fieldset>

        <?= $this->Form->hidden('form', ['value' => 'download_upload']); ?>

        <div class="row">
            <div class="col-md-4">

                <legend class="sub-title-sm"><?= __('Descargar Archivo') ?></legend>
                <?=$this->Html->link('Descargar', '/Presales/downloadComodato', ['class' => 'btn btn-default', 'target' => '_self']) ?> 
                <br>
                <br>

                <legend class="sub-title-sm"><?= __('Subir Archivo') ?></legend>
                <input name="upload_comodato" type="file" id="upload_comodato">
                <br>

            </div>
        </div>
    </fieldset>
    <div class="mt-3">
        <?= $this->Html->link(__('Cancelar'),["action" => "index"], ['class' => 'btn btn-default ']) ?>
        <?= $this->Form->button(__('Subir'), ['class' => 'btn-success' ]) ?>
    </div>
<?= $this->Form->end() ?>
