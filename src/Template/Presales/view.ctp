<style type="text/css">

    .vertical-table {
        width:100%;
    }

    table.vertical-table tbody tr {
        border-bottom: 1px solid #d8d8d8;
    }

    #table-connections_wrapper .title {
        color: #696868;
        padding-top: 10px;
    }

	.tr-title {
	    border-bottom: 0 !important;
        font-size: 20px;
        color: #607D8B;
	}

</style>

<div class="row">
    <div class="col-md-12">
        <legend class="sub-title">Resumen Venta</legend>
    </div>
    <div class="col-md-4">
        <br>
        <table class="vertical-table">
            <tr class="tr-title">
                <th><?= __('Datos de Cliente') ?></th>
            </tr>
            <tr>
                <th><?= __('Código de Cliente') ?></th>
                <td class="text-right"><?= sprintf("%'.05d", $presale->customer->code ) ?></td>
            </tr>
            <tr>
                <th><?= __('Nombre') ?></th>
                <td class="text-right"><?= h($presale->customer->name) ?></td>
            </tr>
            <tr>
                <th><?= __('Documento') ?></th>
                <td class="text-right"><?= $doc_types[$presale->customer->doc_type] .' '. ($presale->customer->ident ? $presale->customer->ident : '') ?></td>
            </tr>
            <tr class="tr-title">
                <th><?= __('Datos de Instalación') ?></th>
            </tr>
            <tr>
                <th><?= __('Vendedor') ?></th>
                <td class="text-right"><?= h($presale->user->name) ?></td>
            </tr>
            <tr>
                <th><?= __('Comentario') ?></th>
                <td class="text-right"><?= h($presale->comments) ?></td>
            </tr>
        </table>
            
    </div>

    <div class="col-md-8">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-hover" id="table-debts" >
                    <thead>
                        <tr>
                            <th><?= __('Descripción') ?></th>
                            <th><?= __('Cantidad') ?></th>
                            <th><?= __('Precio unitario') ?></th>
                            <th><?= __('Cuotas') ?></th>
                            <th><?= __('Vencimiento') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($presale->debts as $debt): ?>
                        <tr data-id="<?= $debt->id ?>">
                            <td><?= h($debt->description) ?></td>
                            <td><?= h($debt->quantity) ?></td>
                            <td>$<?= number_format($debt->price, 2, ',', '.') ?></td>
                            <td><?= $debt->dues ?></td>
                            <td><?= date('d/m/Y', strtotime($debt->duedate->format('Y-m-d H:i:s'))) ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div> 
    </div> 
</div>

<script type="text/javascript">

    var presale = <?=json_encode($presale)?>;

    var table_debts = null;

    $(document).ready(function() {

        table_debts = $('#table-debts').DataTable({
		    "order": [[ 0, 'desc' ]],
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": false,
		    "columnDefs": [
            ],
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            
        	"dom":
    	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
   
		});

		$('#table-debts_wrapper .title').append('<h5>Deudas</h5>');
    });

</script>
