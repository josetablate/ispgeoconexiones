<style type="text/css">

    legend {
        display: block;
        width: 100%;
        padding: 0;
        margin-bottom: 20px;
        font-size: 21px;
        line-height: inherit;
        color: #333;
        border: 0;
        border-bottom: 1px solid #e5e5e5;
    }

    .vertical-table {
        width:100%;
    }

    table.vertical-table tbody tr {
        border-bottom: 1px solid #d8d8d8;
    }

	.card-data-block {
        height: 250px;
	}

	#table-packages {
        width: 100% !important;
    }

    #table-packages td {
        margin: 0px 0px 0px 0px !important;
        padding: 0px 5px 0px 5px !important;
        vertical-align: middle;
    }

    #table-products {
        width: 100% !important;
    }

    #table-products td {
        margin: 0px 0px 0px 0px !important;
        padding: 0px 5px 0px 5px !important;
        vertical-align: middle;
    }

    tr.selected {
        background-color: #8eea99;
        color: #333 !important;
    }

    .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
        background-color: #d2d2d2;
    }

    #table-customers_wrapper .title {
        color: #696868;
        padding-top: 10px;
    }

    .container-services_pending {
        overflow-y: auto;
        max-height: 350px;
        min-height: 200px;
    }

    .container-packages {
        overflow-y: auto;
        max-height: 350px;
        min-height: 200px;
    }

    .container-products {
        overflow-y: auto;
        max-height: 350px;
        min-height: 200px;
    }

</style>

<div class="row">

    <!--customers-->
    <div class="col-xl-6 mb-2">
        <div class="card border-secondary">
            <div class="card-header border-secondary">

                <div class="row justify-content-between">
                    <div class="col-9">
                        <h5>
                            <?= __('Cargar Cliente') ?>
                        </h5>
                    </div>
                    <div class="col-auto">
                        <a href="javascript:void(0)" onClick="openModalHelp('Para cargar el cliente debe presionar en el botón cargar y será dirigido al formulario de carga de clientes una vez que complete los datos del mismo presionará en el botón cargar y volverá a esta misma vista para continuar el proceso de venta.')"><i class="fas fa-question-circle fa-2x"></i></a>
                    </div>
                </div>

            </div>
            <div class="card-body card-data-block p-2">
                <table class="vertical-table">
                    <tr>
                        <th><?= __('Nombre') ?></th>
                        <?php if (isset($presale->customer)): ?>
                            <?php
                                $title_notice = "";
                                if (isset($presale->customer->debts_total) && $presale->customer->debts_total > 0) {
                                    $title_notice = '<span>Deudas sin Facturar. </span><br>';
                                    $title_notice .= '<span>Total: $ ' . $presale->customer->debts_total . '</span>';;
                                }
                            ?>
                            <td class="text-right info-customer-name" title="<?= $title_notice ?>" data-original-title="<?= $title_notice ?>" data-toggle="tooltip" data-html="true"><?= h($presale->customer->name) ?></td>
                        <?php else: ?>
                             <td class="text-right info-customer-name" title="" data-original-title="" data-toggle="tooltip" data-html="true"></td>
                        <?php endif; ?>
                    </tr>
                    <tr>
                    <th><?= __('Código') ?></th>
                        <?php if (isset($presale->customer)): ?>
                            <td class="text-right text-right"><?= str_pad($presale->customer->code, 5, '0', STR_PAD_LEFT) ?></td>
                        <?php else: ?>
                             <td class="text-right text-right"></td>
                        <?php endif; ?>
                    </tr>
                    <tr>
                        <th><?= __('Documento') ?></th>
                        <?php if (isset($presale->customer)): ?>
                            <td class="text-right info-customer-document"><?= $doc_types[$presale->customer->doc_type] . ' ' . $presale->customer->ident ?></td>
                        <?php else: ?>
                             <td class="text-right info-customer-document"></td>
                        <?php endif; ?>
                    </tr>
                    <tr>
                        <th><?= __('Domicilio') ?></th>
                        <?php if (isset($presale->customer)): ?>
                            <td class="text-right info-customer-address"><?= h($presale->customer->address) ?></td>
                        <?php else: ?>
                             <td class="text-right info-customer-address"></td>
                        <?php endif; ?>
                    </tr>
                    <tr>
                        <th><?= __('Teléfono') ?></th>
                        <?php if (isset($presale->customer)): ?>
                            <td class="text-right info-customer-phone"><?= h($presale->customer->phone) ?></td>
                        <?php else: ?>
                             <td class="text-right info-customer-phone"></td>
                        <?php endif; ?>
                    </tr>
                    <tr>
                        <th><?= __('Correo electrónico') ?></th>
                        <?php if (isset($presale->customer)): ?>
                            <td class="text-right info-customer-email"><?= h($presale->customer->email) ?></td>
                        <?php else: ?>
                            <td class="text-right info-customer-email"></td>
                        <?php endif; ?>
                    </tr>

                </table>

                <table class="vertical-table">
                    <tr>
                        <th><?= __('Etiquetas') ?></th>
                        <?php if (isset($presale->customer)): ?>
                            <td class="text-right info-customer-labels">
                            <?php
                                foreach ($presale->customer->labels as $label) {
                                    if ($label->id != 0) {
                                         echo "<span class='badge m-1' style='background-color: " . $label->color_back . "; color: " . $label->color_text . "' >" . $label->text . "</span>";
                                    }
                                }
                            ?>
                            </td>
                        <?php else: ?>
                             <td class="text-right info-customer-labels"></td>
                        <?php endif; ?>
                    </tr>
                </table>

            </div>
            <div class="card-footer border-secondary">
                <a href="/ispbrain/customers/edit/<?= isset($presale->customer) ? $presale->customer->code : '' ?>" class="btn btn-success btn-edit-customer <?= isset($presale->customer) ? '' : 'my-hidden' ?>"><?= __('Editar') ?></a>

                <a href="/ispbrain/customers/add/true" class="btn btn-primary btn-add-customer"><?= __('Cargar') ?></a>

                <a id="btn-search-customer" href="javascript:void(0)" class="btn btn-info float-right"><?= __('Buscar') ?></a>
            </div>
        </div>
    </div>

    <!--servicess-->
    <div class="col-xl-6 mb-2">
        <div class="card border-secondary">
            <div class="card-header border-secondary">

                <div class="row justify-content-between">
                    <div class="col-9">
                        <h5>
                            <?= __('Cargar Servicio') ?>
                        </h5>
                    </div>
                    <div class="col-auto">
                        <a href="javascript:void(0)" onClick="openModalHelp('Para cargar un servicio debe presionar en el botón cargar y será dirigido al formulario de cargar un servicio una vez que complete los datos del mismo presionará en el botón cargar y volverá a esta misma vista para continuar el proceso de venta. Algo a tener en cuenta es que podrá cargar la cantidad de servicios que desee y así también eliminar servicios, esto se realiza presiona sobre la fila de la tabla del servicio que desee eliminar. Es opcional su carga')" ><i class="fas fa-question-circle fa-2x"></i></a>
                    </div>
                </div>

            </div>
            <div class="card-body p-2">

                <div class="row container-services_pending">
                     <?php foreach ($presale->services as $key => $service): ?>
                     <?= $this->element('sales/service_detail', ['service' => $service, 'id' => $key]) ?>
                      <?php endforeach; ?>
                </div>

            </div>
            <div class="card-footer  border-secondary">
                <a id="btn-sell-service" href="javascript:void(0)" class="btn btn-success">Cargar</a>
            </div>
        </div>
    </div>

    <!--packages-->
    <div class="col-xl-6 mb-2">
        <div class="card border-secondary">
            <div class="card-header border-secondary">

                <div class="row justify-content-between">
                    <div class="col-9">
                        <h5>
                            <?= __('Cargar Paquete') ?>
                        </h5>
                    </div>
                    <div class="col-auto">
                        <a  href="javascript:void(0)" onClick="openModalHelp('Para cargar un paquete debe presionar en el botón cargar y será dirigido al formulario de carga de paquetes una vez que complete los datos del mismo presionará en el botón cargar y volverá a esta misma vista para continuar el proceso de venta. Algo a tener en cuenta es que podrá cargar la cantidad de paquetes que desee y así también eliminar paquetes, esto se realiza presiona sobre la fila de la tabla del paquete que desee eliminar. Es opcional su carga')" ><i class="fas fa-question-circle fa-2x"></i></a>
                    </div>
                </div>

            </div>
            <div class="card-body p-2">

                <div class="row container-packages">
                     <?php foreach ($presale->packages as $key => $package): ?>
                     <?= $this->element('sales/package_detail', ['package' => $package, 'id' => $key]) ?>
                     <?php endforeach; ?>
                </div>

            </div>
            <div class="card-footer  border-secondar">
                <a id="btn-sell-package" href="javascript:void(0)" class="btn btn-success">Cargar</a>
            </div>
        </div>
    </div>

    <!--products-->
    <div class="col-xl-6 mb-2">
        <div class="card border-secondary">
            <div class="card-header border-secondary">
                
                <div class="row justify-content-between">
                    <div class="col-9">
                        <h5>
                            <?= __('Cargar Producto') ?>
                        </h5>
                    </div>
                    <div class="col-auto">
                        <a href="javascript:void(0)" onClick="openModalHelp('Para cargar un producto debe presionar en el botón cargar y será dirigido al formulario de carga de productos una vez que complete los datos del mismo presionará en el botón cargar y volverá a esta misma vista para continuar el proceso de venta. Algo a tener en cuenta es que podrá cargar la cantidad de productos que desee y así también eliminar productos, esto se realiza presiona sobre la fila de la tabla del producto que desee eliminar. Es opcional su carga')" ><i class="fas fa-question-circle fa-2x"></i></a>
                    </div>
                </div>

            </div>
            <div class="card-body p-2">

                <div class="row container-products">
                     <?php foreach ($presale->products as $key => $product): ?>
                     <?= $this->element('sales/product_detail', ['product' => $product, 'id' => $key]) ?>
                     <?php endforeach; ?>
                </div>

            </div>
            <div class="card-footer border-secondary">
                <a id="btn-sell-product" href="javascript:void(0)" class="btn btn-success">Cargar</a>
            </div>
        </div>
    </div>

    <div class="col-xl-12 mt-4 mb-4">
        <div class="row justify-content-around">

            <div class="col-xl-auto">
                <?=$this->Html->link('Imprimir Comodato','/Presales/downloadComodato', ['class' => 'btn btn-default', 'target' => '_self']) ?>
            </div>

            <div class="col-xl-auto mt-1 mb-1">
                <a id="btn-add-comment" href="javascript:void(0)" class="btn btn-info"><?= __('Agregar Comentario') ?></a>
            </div>

            <div class="col-xl-auto mt-1 mb-1">
                <a id="btn-confirm-presale" href="javascript:void(0)" class="btn btn-success"><?= __('Confirmar Venta') ?></a>
            </div>

            <div class="col-xl-2 mt-1 mb-1">
                <a id="btn-cancel-presale" href="javascript:void(0)" class="btn btn-danger"><?= __('Cancelar Venta') ?></a>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="modal-customer-seeker" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-12">
                        <button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="col-xl-12">
                        <table class="table table-bordered table-hover" id="table-customers">
                            <thead>
                                <tr>
                                    <th >Código</th>            <!--0-->
                                    <th >Nombre</th>            <!--1-->
                                    <th >Documento</th>         <!--2-->
                                    <th >Domicilio</th>         <!--3-->
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btn-selected">Seleccionar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modal-continue-sale" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header"> 
                <h4 class="modal-title">Continuar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <div class="row justify-content-center mb-2">
                    <div class="col-auto">
                        <h5>Existe una venta en proceso.</h5>
                    </div>
                </div>

                <div class="row justify-content-around">

                    <div class="col-xl-6">
                        <button type="button" id="btn-new-sale" class="btn btn-primary">Comenzar una venta nueva</button>
                    </div>
                    <div class="col-xl-6">
                        <button type="button" class="btn btn-success float-right" data-dismiss="modal">Continuar</button>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-add-comment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header"> 
                <h4 class="modal-title">Agregar Comentario</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <div class="card-block p-2">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="row">
                                <div class="col-xl-12">
                                    <?php echo $this->Form->input('comments', ['id' => 'comments', 'label' => 'Comentario', 'type' => 'textarea', 'value' => ""]); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="btn btn-success" id="btn-confirm-add-comment">Agregar</button>
            </div>
        </div>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-package-edit',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-success'
    ];
    
     $buttons[] = [
        'id'   =>  'btn-package-delete',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-action-packages', 'title' => 'Acciones', 'buttons' => $buttons ]);

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-product-edit',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-success'
    ];
    
    $buttons[] = [
        'id'   =>  'btn-product-delete',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-action-products', 'title' => 'Acciones', 'buttons' => $buttons ]);

    $buttons = [];

   
    
    $buttons[] = [
        'id'   =>  'btn-service-edit',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-success'
    ];
    
     $buttons[] = [
        'id'   =>  'btn-service-delete',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-action-services', 'title' => 'Acciones', 'buttons' => $buttons ]);

    echo $this->element('modal_help', ['size' => 'md']);

?>

<script type="text/javascript">

    var paraments;
    var presale;
    var table_packages = null;
    var table_products = null;

    var presale_id = null;

    var table_customers = null;
    var customer_selected = null;
    var paymentMethods = null;

	$(document).ready(function() {

	    var continue_sale  = <?= json_encode($continue_sale) ?>;
	    paymentMethods = <?= json_encode($paymentMethods) ?>;

	    if (continue_sale) {
	        $('#modal-continue-sale').modal('show');
	    }

	    presale_id = <?= !empty($presale_id) ? json_encode($presale_id) : "'[]'" ?>;
	    if (presale_id == '[]') {
	        presale_id = null;
	    } else {
	        var url = "/ispbrain/presales/showprint/" + presale_id;
            window.open(url, 'Imprimir Resumen', "width=600, height=500");
	    }
	    paraments = <?= json_encode($paraments) ?>;
	    presale = <?= json_encode($presale) ?>;

        $('#table-customers').removeClass('display');

        var selectEnable = false;
        var enableLoad = 0;

        table_customers = $('#table-customers').DataTable({
		    "order": [[ 1, 'desc' ]],
            "processing": true,
            "serverSide": true,
		    "ajax": {
                "url": "/ispbrain/customers/get_customers_simple.json",
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "enableLoad": enableLoad
                    });
                },
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "scrollY": true,
		    "scrollY": '330px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
		    "columns": [
                { 
                    "data": "code",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                { 
                    "data": "name",
                    "type": "string",
                },
                { 
                    "data": "ident",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (data) {
                            ident = data;
                        }
                        return sessionPHP.afip_codes.doc_types[row.doc_type] + ' ' + ident;
                    }
                },
                { 
                    "data": "address",
                    "type": "string",
                },
            ],
		    "columnDefs": [
		       {
                    "targets": [0],
                    "width": '5%'
                },
                {
                    "targets": [2],
                    "width": '8%'
                },
                {
                    "targets": [3],
                    "width": '18%'
                },
                {
                    "targets": [1],
                    "width": '12%'
                },
                { 
                    "class": "left", targets: [2, 3]
                },
                { 
                    "visible": false, targets:  []
                },
            ],
            "createdRow" : function( row, data, index ) {
               row.id = data.code;
            },
		    "language": dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
        	"dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
		});

		$('#table-customers').on('processing.dt', function (e, settings, processing) {
            if (processing) {
                selectEnable = false;
            } else {
                selectEnable = true;
            }
        });

		$('#table-customers_wrapper .title').append('<h5>Lista de Clientes</h5>');

		$('.btn-customer-list').click(function(){
            $("#modal-customer-seeker").modal('show');
        });

		$("#modal-customer-seeker").on('shown.bs.modal', function (e) {
		    enableLoad = 1;
		    table_customers.clear().draw();
        });

        $("#modal-customer-seeker").on('hiden.bs.modal', function (e) {
		    enableLoad = 0;
            table_customers.clear().draw();
        });

        $('#table-customers tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                if (selectEnable) {

                    table_customers.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');

                    customer_selected =  table_customers.row( this ).data();
                }
            }
        });

        $('#table-customers tbody').on( 'dblclick', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                if (selectEnable) {

                    table_customers.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');

                    customer_selected = table_customers.row( this ).data();

                    $('#modal-customer-seeker #btn-selected').click();
                }
            }
        });

        $('#modal-customer-seeker #btn-selected').click(function() {

            if (customer_selected) {

                $.ajax({
                    url: "<?= $this->Url->build(['controller' => 'Presales', 'action' => 'addCustomer']) ?>",
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify({ customer: customer_selected }),
                    success: function(data) {
                        if (data != false) {
                            presale.customer = customer_selected;

                            $('.info-customer-name').html(customer_selected.name);
                            $('.info-customer-code').html(pad(customer_selected.code, 5));
                            $('.info-customer-document').html(sessionPHP.afip_codes.doc_types[customer_selected.doc_type] + ' ' + customer_selected.ident);
                            $('.info-customer-address').html(customer_selected.address);
                            $('.info-customer-phone').html(customer_selected.phone);
                            $('.info-customer-email').html(customer_selected.email);
                            $('.info-customer-labels').html('');
                            $('.btn-edit-customer').removeClass('my-hidden');
                            $("a.btn-edit-customer").attr("href", "/ispbrain/customers/edit/" + customer_selected.code)

                            $.each( customer_selected.labels, function( index, label ) {
                                if (label.id != 0) {
                                     $('.info-customer-labels').append("<span class='badge m-1' style='background-color: " + label.color_back + "; color: " + label.color_text + "' >" + label.text + "</span>");
                                }
                            });

                            generateNoty('success', data['data']['message']);

                            $.ajax({
                                url: "<?= $this->Url->build(['controller' => 'Customers', 'action' => 'getDebtWithInvoice']) ?>",
                                type: 'POST',
                                dataType: "json",
                                data: JSON.stringify({customer_code: customer_selected.code }),
                                success: function(data) {

                                    customer_selected.debts_total = data.debts.total;

                                    if (data.debts.total > 0) {
                                        var value_total = '<span>Deudas sin Facturar. </span><br>';
                                        value_total += '<span>Total: $ ' + data.debts.total + '</span>';
                                        $('.info-customer-name').prop('title', value_total);
                                        $('.info-customer-name').prop('data-original-title', value_total);
                                        $('.info-customer-name').attr('data-original-title', value_total);
                                        $('.info-customer-name').tooltip('show');
                                    } else {
                                        $('.info-customer-name').prop('title', '');
                                        $('.info-customer-name').prop('data-original-title', '');
                                        $('.info-customer-name').attr('data-original-title', '');
                                    }
                                },
                                error: function (jqXHR) {
                                    if (jqXHR.status == 403) {

                                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                                    	setTimeout(function() { 
                                            window.location.href = "/ispbrain";
                                    	}, 3000);

                                    } else {
                                    	generateNoty('error', 'Error al intentar obtener las Deudas sin Facturar del Cliente.');
                                    }
                                }
                            });
                        }
                    },
                    error: function (jqXHR) {
                        if (jqXHR.status == 403) {

                        	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                        	setTimeout(function() { 
                                window.location.href = "/ispbrain";
                        	}, 3000);
                        } else {
                        	generateNoty('error', 'Error al agregar el cliente');
                        }
                    }
                });

                $('#modal-customer-seeker').modal('hide');

            } else {
                generateNoty('warning', 'Debe seleccionar un cliente.');
            }
        });
        
        

        $("#btn-confirm-add-comment").click(function() {
            var comment = $('textarea#comments').val();
            $.ajax({
                url: "<?= $this->Url->build(['controller' => 'Presales', 'action' => 'changeComments']) ?>",
                type: 'POST',
                dataType: "json",
                data: JSON.stringify({ comments: comment}),
                success: function(data) {
                    if (data != false) {
                        // console.log("Mensaje: " + data['data']['message']);
                        presale.comments = comment;
                        generateNoty('success', "Se ha agregado correctamente el comentario.");
                        $("#modal-add-comment").modal('hide');
                    }
                },
                error: function (jqXHR) {
                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                    	  window.location.href ="/ispbrain";
                    	}, 3000);

                    } else {
                        generateNoty('error', 'Error cambiar el comentario.');
                        $("#modal-add-comment").modal('hide');
                    }

                }
            });
        });

        //packages

        $('#btn-sell-product').click(function() {
            if (presale.customer) {
                var action = '/ispbrain/Debts/sellingProduct/true';
                window.open(action, '_self');
            } else {
                generateNoty('warning', 'Debe cargar primero un Cliente');
            }
        });

        var row_product_id = null; 

        $('.container-products').on( 'click', '.card',function (e) {
                
            row_product_id = $(this).attr('id');
            $('.modal-action-products').modal('show');
     
        });

        $('a#btn-product-delete').click(function() {

            var text = '¿Está Seguro que desea eliminar el Producto?';
            var controller  = 'presales';
            var id  = row_product_id;

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/deleteProduct/', 'method': 'post', 'id': 'form-temp-product-delete'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#form-temp-product-delete').submit();
                }
            });
        });

        $('a#btn-product-edit').click(function() {
            var id = row_product_id;
            var action = '/ispbrain/Debts/sellingProductEdit/' + id;
            window.open(action, '_self');
        });

        //end products

        //packages

        $('#btn-sell-package').click(function() {
            if (presale.customer) {
                var action = '/ispbrain/Debts/sellingPackage/true';
                window.open(action, '_self');
            } else {
                generateNoty('warning', 'Debe cargar primero un Cliente');
            }
        });

        var row_package_id = null; 

        $('.container-packages').on( 'click', '.card',function (e) {
            row_package_id = $(this).attr('id');
            $('.modal-action-packages').modal('show');
        });

        $('a#btn-package-edit').click(function() {
            var id  = row_package_id;
            var action = '/ispbrain/Debts/sellingPackageEdit/' + id;
            window.open(action, '_self');
        });

        $('a#btn-package-delete').click(function() {

            var text = '¿Está Seguro que desea eliminar el Paquete?';
            var controller  = 'presales';
            var id  = row_package_id;

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/deletePackage/', 'method': 'post', 'id': 'form-temp-package-delete'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#form-temp-package-delete').submit();
                }
            });
        });

        //end packages

        //services

        $('#btn-sell-service').click(function() {
            if (presale.customer) {
                var action = '/ispbrain/Debts/sellingService/true';
                window.open(action, '_self');
            } else {
                generateNoty('warning', 'Debe cargar primero un Cliente');
            }
        });

        var row_service_id = null; 

        $('.container-services_pending').on( 'click', '.card',function (e) {
            row_service_id = $(this).attr('id');
            $('.modal-action-services').modal('show');
        });

        $('a#btn-service-delete').click(function() {

            var text = '¿Está Seguro que desea eliminar el Servicio?';
            var controller  = 'presales';
            var id  = row_service_id;

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/deleteService/', 'method': 'post', 'id': 'form-temp-service-delete'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#form-temp-service-delete').submit();
                }
            });
        });

        $('a#btn-service-edit').click(function() {
            var id  = row_service_id;
            var action = '/ispbrain/Debts/sellingServiceEdit/' + id;
            window.open(action, '_self');
        });

        //end services

        $('#btn-new-sale').click(function() {
            $('#btn-cancel-presale').click();
        });

        $('#btn-cancel-presale').click(function() {

            var text = '¿Está Seguro que desea cancelar la Venta?, se borrará la carga de datos realizada.';
            var controller  = 'presales';

            bootbox.confirm(text, function(result) {
                if (result) {
                    var action = '/ispbrain/presales/cancelPresale';
                    window.open(action, '_self');
                }
            });
        });

        $('#btn-confirm-presale').click(function() {

            var message = "";

            if (presale.customer == null) {
                message += "*Debe Cargar un Cliente. <br/>";
            }

            if (Object.keys(presale.packages).length > 0 || Object.keys(presale.products).length > 0 || Object.keys(presale.services).length > 0) {
            } else {
                 message += "*Debe Cargar al menos un paquete, producto o un servicio, para poder realizar la venta.";
            }

            if (message != "") {
                generateNoty('warning', message);
            } else {
                var text = 'Presione OK para confirmar la Venta.';
                var controller  = 'presales';

                bootbox.confirm(text, function(result) {
                    if (result) {
                        var action = '/ispbrain/presales/confirmPresale';
                        window.open(action, '_self');
                    }
                });
            }
        });

        $('#btn-add-comment').click(function() {
            $('textarea#comments').val(presale.comments);
            $("#modal-add-comment").modal('show');
        });

        $('#btn-search-customer').click(function() {
            table_customers.clear().draw();
            $("#modal-customer-seeker").modal('show');
        });
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    $(document).click(function() {
        $('[data-toggle="tooltip"]').tooltip('hide');
    });

</script>
