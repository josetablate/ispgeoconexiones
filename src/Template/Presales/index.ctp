<style type="text/css">
</style>

<div class="row">

   <div id="btns-tools">
        <div class="text-right btns-tools margin-bottom-5">

               <?php 
                    echo $this->Html->link(
                        '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                        ["controller" => "presales", "action" => "add"],
                        [
                        'title' => 'Generar Venta',
                        'class' => 'btn btn-default btn-add"',
                        'escape' => false
                    ]);

                    echo $this->Html->link(
                        '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Exportar tabla',
                        'class' => 'btn btn-default btn-export',
                        'escape' => false
                    ]);
                ?>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-hover table-bordered" id="table-presales">
            <thead>
                <tr>
                    <th>Fecha</th>          <!-- 0 -->
                    <th>Código</th>         <!-- 1 -->
                    <th>Cliente</th>        <!-- 2 -->
                    <th>Documento</th>      <!-- 3 -->
                    <th>Vendedor</th>       <!-- 4 -->
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="modal-edit-comments" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit-comments">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Editar Comentario</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <form method="post" accept-charset="utf-8" role="form" action="<?= $this->Url->build(["controller" => "Presales", "action" => "edit"]) ?>">
                    <div style="display:none;">
                        <input type="hidden" name="_method" value="POST">
                    </div>

                    <div class="row">

                        <div class="col-xl-12">
                            <fieldset>
                                <?php
                                    echo $this->Form->input('comments', ['type' => 'textarea', 'label' => 'Comentarios']);
                                ?>
                                <input type="hidden" name="presale_id" id="presale_id">
                                <?php echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]); ?>
                            </fieldset>

                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-success float-right" id="btn-confirm-edit">Guardar</button> 

                        </div>
                    </div>

                </form>
            </div>
            </div>
        </div>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'     =>  'btn-info-presale',
        'name'   =>  'Información',
        'icon'   =>  'icon-eye',
        'type'   =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-edit',
        'name' =>  'Comentarios',
        'icon' =>  'glyphicon icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'     =>  'btn-go-customer',
        'name'   =>  'Ficha del Cliente',
        'icon'   =>  'glyphicon icon-profile',
        'type'   =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-print-presale-resume',
        'name' =>  'Imprimir Resumen',
        'icon' =>  'glyphicon icon-printer',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-send-email-presales',
        'name' =>  'Enviar Correo',
        'icon' =>  'fab fa-telegram-plane',
        'type' =>  'btn-success'
    ];

    echo $this->element('actions', ['modal'=> 'modal-presales', 'title' => 'Acciones', 'buttons' => $buttons ]);
    echo $this->element('Email/send', ['modal'=> 'modal-send-email', 'title' => 'Seleccionar Plantilla', 'mass_emails_templates' => $mass_emails_templates ]);
?>

<script type="text/javascript">

    var table_presales = null;
    var mass_emails_templates = null;
    var presale_selected = null;

    $(document).ready(function () {

        mass_emails_templates = <?= json_encode($mass_emails_templates) ?>;

        $('#table-presales').removeClass('display');

        $('#btns-tools').hide();

		table_presales = $('#table-presales').DataTable({
		    "order": [[ 0, 'desc' ]],
		    "scrollY": '350px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/presales/index.json",
                "dataSrc": "presales",
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "columns": [
                { 
                    "data": "created",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                { 
                    "data": "customer.code",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                { "data": "customer.name" },
                { 
                    "data": "customer.ident",
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (data) {
                            ident = data;
                        }
                        return sessionPHP.afip_codes.doc_types[row.customer.doc_type] + ' ' + ident;
                    }
                },
                { "data": "user.username" },
               
            ],
            "columnDefs": [
		        { "type": 'date-custom', targets: [0] },
		        { "width": '10%', targets: [0, 1] },
		        { "width": '8%', targets: 4 },
            ],
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            "dom":
    	    	"<'row'<'col-md-6'l><'col-md-4'f><'col-md-2 tools'>>" +
        		"<'row'<'col-md-12'tr>>" +
        		"<'row'<'col-md-5'i><'col-md-7'p>>",
		});

		$('#table-presales_wrapper .tools').append($('#btns-tools').contents());

        $('#btns-tools').show();
    });

    $(".btn-export").click(function() {

        bootbox.confirm('Se descargará un archivo Excel', function(result) {
            if (result) {
                $('#table-presales').tableExport({tableName: 'Venta', type:'excel', escape:'false'});
            }
        });

    });

    $('#table-presales tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_presales.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');

            presale_selected = table_presales.row( this ).data();

            $('.modal-presales').modal('show');
        }
    });

    $('#btn-edit').click(function() {

        var presale_id = table_presales.$('tr.selected').attr('id');
        var comment = table_presales.$('tr.selected').data('comment'); 

        $('input#presale_id').val(presale_selected.id);
        $('textarea#comments').html(presale_selected.comments);
        $('.modal-presales').modal('hide');
        $('#modal-edit-comments').modal('show');
    });

    $('#btn-go-customer').click(function() {
        var action = '/ispbrain/customers/view/' + presale_selected.customer_code;
        window.open(action, '_blank');
    });

    $('#btn-print-presale-resume').click(function() {
        var url = "/ispbrain/presales/showprint/" + presale_selected.id;
        window.open(url, 'Imprimir Resumen', "width=600, height=500");
    });

    $('#btn-info-presale').click(function() {
        var action = '/ispbrain/presales/view/' + presale_selected.id;
        window.open(action, '_blank');
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

    $('#btn-send-email-presales').click(function() {
        if (presale_selected.customer.email == "" || presale_selected.customer.email == null) {
            generateNoty('warning', 'Debe agregar el Correo Eectrónico del Cliente al cual pertenece la Venta.');
        } else {
            var count = 0;
            $.each(mass_emails_templates, function( index, value ) {
                count++;
            });
            if (count > 1) {
                model = 'Presales';
                model_id = presale_selected.id;
                $('.modal-presales').modal('hide');
                $('.modal-send-email').modal('show');
            } else {
                generateNoty('warning', 'No cuenta con plantillas de Correos.');
            }
        }
    });

</script>
