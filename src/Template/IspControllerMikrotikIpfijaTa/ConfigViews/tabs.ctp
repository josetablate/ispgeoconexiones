<style type="text/css">

    .nav-link {
        cursor: pointer;
    }

</style>



<div class="row">
     <div class="col-10">
          <div class="card border-secondary mb-1">
                <div class="card-body p-1">
                    <div class="text-secondary p-0">Controlador Seleccionado: <span class="font-weight-bold"><?=$controller->name?></span></div>
                </div>
            </div>
     </div>
      <div class="col-2">
        <?php
            
         echo $this->Html->link(
             
            '<span class="glyphicon icon-bin"  aria-hidden="true"></span>',
            'javascript:void(0)',
            [
                'id' => 'btn-delete-controller',
                'title' => 'Eliminar controlador',
                'class' => 'btn btn-default ml-1 float-right',
                'data-id' => $controller->id,
                'escape' => false
            ]);
            
         echo $this->Html->link(
             
            '<span class="glyphicon icon-plus"  aria-hidden="true"></span>',
            ['controller' => 'IspController', "action" => "add"],
            [
                'title' => 'Agregar nuevo controlador',
                'class' => 'btn btn-default float-right',
                'escape' => false
            ]);
            
         ?>
     </div>
</div>

<div class="row">
    <div class="col-xl-12">
    
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" id="myTab" role="tablist">
    
            <li class="nav-item">
                <a class="nav-link active" id="plans-tab" data-target="#plans" role="tab" aria-controls="plans" aria-expanded="true">Planes</a>
            </li>
    
            <li class="nav-item">
                <a class="nav-link " id="pools-tab" data-target="#pools" role="tab" aria-controls="pools" aria-expanded="false">Pools</a>
            </li>
            
            <li class="nav-item">
                <a class="nav-link " id="gateways-tab" data-target="#gateways" role="tab" aria-controls="gateways" aria-expanded="false">Gateways</a>
            </li>
    
            <li class="nav-item">
                <a class="nav-link " id="profiles-tab" data-target="#profiles" role="tab" aria-controls="profiles" aria-expanded="false">Profiles</a>
            </li>
           
            <li class="nav-item">
                <a class="nav-link " id="ip-excluded-tab" data-target="#ip-excluded" role="tab" aria-controls="ip-excluded" aria-expanded="false">IP Excluidas</a>
            </li>
            
            
            <li class="nav-item">
                <a class="nav-link " id="tools-tab" data-target="#tools" role="tab" aria-controls="tools" aria-expanded="false">Herramientas</a>
            </li>
        </ul>
    
        <!-- Tab panes -->
        <div class="tab-content" id="myTabContent">
    
            <div class="tab-pane fade show active" id="plans" role="tabpanel" aria-labelledby="plans-tab">
                <?= $this->fetch('plans') ?>
            </div>
    
            <div class="tab-pane fade" id="pools" role="tabpanel" aria-labelledby="pools-tab">
                <?= $this->fetch('pools') ?>
            </div>
            
            <div class="tab-pane fade" id="gateways" role="tabpanel" aria-labelledby="gateways-tab">
                <?= $this->fetch('gateways') ?>
            </div>
    
            <div class="tab-pane fade" id="profiles" role="tabpanel" aria-labelledby="profiles-tab">
                <?= $this->fetch('profiles') ?>
            </div>
    
             <div class="tab-pane fade" id="ip-excluded" role="tabpanel" aria-labelledby="ip-excluded-tab">
                <?= $this->fetch('ip-excluded') ?>
            </div>
            
            <div class="tab-pane fade" id="tools" role="tabpanel" aria-labelledby="tools-tab">
                <?= $this->fetch('tools') ?>
            </div>
    
        </div>
    
    </div>
</div>

<?php
    echo $this->element('modal_preloader');
    echo $this->element('modal_help', ['size' => 'md']);
?>

<script type="text/javascript">

    var controller_id = <?php echo json_encode($controller->id); ?>;


    $(document).ready(function() {

        $('#btn-delete-controller').click( function(e) {

            var text = '¿Está seguro que desea eliminar el Controlador?';
            var id = $(this).data('id');

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/IspControllerMikrotikIpfijaTa/delete/', 'method': 'post', 'id': 'replacer'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer').submit();
                }
            });
        });

        $('#myTab a').click(function (e) {

            e.preventDefault();

            var target = $(this).data('target');
            $('#myTab a.active').removeClass('active');

            $('#myTabContent .active').fadeOut(100, function() {
                $('#myTabContent .active').removeClass('active');

                $(target).fadeIn(100, function() {
                    $(target).addClass('active show');
                    $(target+'-tab').addClass('active');
                    $(target+'-tab').trigger('shown.bs.tab');
                });
            });
        });
    });

</script>
