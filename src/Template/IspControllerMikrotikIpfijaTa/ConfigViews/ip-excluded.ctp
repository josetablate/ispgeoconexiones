<?php $this->extend('/IspControllerMikrotikIpfijaTa/ConfigViews/tools'); ?>

<?php $this->start('ip-excluded'); ?>

 <div id="btns-tools-ip-excluded">
        <div class="text-right btns-tools margin-bottom-5">
    
            <?php 
                echo $this->Html->link(
                    '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Agregar',
                    'class' => 'btn btn-default btn-add-ip-excluded',
                    'escape' => false
                    ]);
    
                 echo $this->Html->link(
                    '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Exportar tabla',
                    'class' => 'btn btn-default btn-export-ip-excluded ml-1',
                    'escape' => false
                    ]);
            ?>
    
        </div>
    </div>

    <div class="row">
        <div class="col-8 mx-auto">
            
            <div class="card border-secondary mt-2">
                <div class="card-body p-1">
                    
                    <table class="table table-bordered table-hover" id="table-ip-excluded">
                        <thead>
                            <tr>
                               <th>Id</th>
                               <th>IP Address</th>
                               <th>Comentario</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-add-ip-excluded" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="gridSystemModalLabel">Agregar IP Excluida</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
    
                            <?= $this->Form->create(null, ['id' => 'form-add-ip-excluded']) ?>
                                <fieldset>
                                    <?php 
                                        echo $this->Form->input('ip', ['label' => 'IP',  'required' => true, 'autocomplete' => 'off']);
                                        echo $this->Form->input('comments',['label' => 'Comenatario', 'required' => false]);
                                        echo $this->Form->input('controller_id', ['value' => $controller->id, 'type' => 'hidden']);
                                    ?>

                                </fieldset>
                                
                                <?= $this->Form->button(__('Agregar', ['id' => 'btn-submit-add-ip-excluded', 'type' => 'button'])) ?>
                                
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal actions ip-excluded -->
    <?php
        $buttons = [];

        $buttons[] = [
            'id'   =>  'btn-delete-ip-excluded',
            'name' =>  'Eliminar',
            'icon' =>  'icon-bin',
            'type' =>  'btn-danger'
        ];

        echo $this->element('actions', ['modal'=> 'modal-actions-ip-excluded', 'title' => 'Acciones', 'buttons' => $buttons ]);
    ?>


    <script type="text/javascript">
    
       var ip_excluded_selected = null;
       var table_ip_excluded = null; 

       $(document).ready(function () {

            $('#table-ip-excluded').removeClass('display');
            
            $('#btns-tools-ip-excluded').hide();

    		table_ip_excluded = $('#table-ip-excluded').DataTable({
    		    "order": [[ 0, 'desc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/IspControllerMikrotikIpfijaTa/get_ip_excluded_by_controller.json",
                    "dataSrc": "ip_excluded",
                    "data": function ( d ) {
                        return $.extend( {}, d, {
                            "controller_id": controller_id
                        });
                    },
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
    		       
    		        { 
                        "data": "id"
                    },
    		        { 
                        "data": "ip"
                    },
                    { 
                        "data": "comments"
                    },
                ],
    		    "columnDefs": [
    		        { "type": 'ip-address', targets: [1] },
                ],
                "createdRow" : function( row, data, index ) {
                    row.id = data.id;
                },
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-ip-excluded_wrapper .tools').append($('#btns-tools-ip-excluded').contents());
    		
    		$('#btns-tools-ip-excluded').show();

            $(".btn-export-ip-excluded").click(function(){

                bootbox.confirm('Se descargará un archivo Excel', function(result) {
                    if (result) {
                        $('#table-ip-excluded').tableExport({tableName: 'IP-Excluidas', type:'excel', escape:'false'});
                    }
                });
            });

            $('#table-ip-excluded tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length ){

                    table_ip_excluded.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    
                    ip_excluded_selected = table_ip_excluded.row( this ).data();

                    $('.modal-actions-ip-excluded').modal('show');
                }
            });

            $('.btn-add-ip-excluded').click(function() {
                
                clearFormAddIPFree();
                
                $('.modal-add-ip-excluded').modal('show');
            });
            
            $('#form-add-ip-excluded').submit(function(event){
               
                event.preventDefault();
                 
                var send_data = {};
                 
                var form_data = $(this).serializeArray();
                
                $.each(form_data, function(i, val){
                    send_data[val.name] = val.value;
                });
                
                $('#btn-submit-add-ip-excluded').hide(); 
             
                openModalPreloader("Agregando ip Excluida ...");
                
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '/ispbrain/IspControllerMikrotikIpfijaTa/addIPExcluded',
                    data:  JSON.stringify(send_data),
                    success: function(data){
                        
                        closeModalPreloader();
                        
                        $('#btn-submit-add-ip-excluded').show(); 
                        
                        if(data.response){
                            
                            generateNoty('success', 'IP Excluidas agregado correctamente');
                        }else{
                            generateNoty('error', 'Error al intentar agregar la IP Excluidas');
                        }
                        
                        table_ip_excluded.ajax.reload();
                        $('.modal-add-ip-excluded').modal('hide');
                        
                        updateCountersTitle();
                    },
                    error: function(jqXHR) {
                        if (jqXHR.status == 403) {
                        
                        	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                        
                        	setTimeout(function() { 
                        	  window.location.href ="/ispbrain";
                        	}, 3000);
                        
                        } else {
                        	generateNoty('error', 'Error al agregar la IP Excluida.');
                        }
                    }
                });
                
            });
            
            $('#btn-delete-ip-excluded').click(function() {

                var text = '¿Está seguro que desea eliminar el IP Excluidas : ' + ip_excluded_selected.ip + '?';

                bootbox.confirm(text, function(result) {
                    if (result) {
                     
                         openModalPreloader("Eliminado ip-excluded ...");
                         
                         $('.modal-actions-ip-excluded').modal('hide');
                        
                        $.ajax({
                            type: 'POST',
                            dataType: "json",
                            url: '/ispbrain/IspControllerMikrotikIpfijaTa/deleteIPExcluded',
                            data:  JSON.stringify({id: ip_excluded_selected.id}),
                            success: function(data){
                                
                                closeModalPreloader();
                                
                                if(data.response){
                                    generateNoty('success', 'IP Excluidas eliminado correctamente');
                                }else{
                                    generateNoty('error', 'Error al intentar eliminar la IP Excluidas');
                                }
                                
                                table_ip_excluded.ajax.reload();
                                updateCountersTitle();

                            },
                            error: function(jqXHR) {
                                if (jqXHR.status == 403) {
                                
                                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                                
                                	setTimeout(function() { 
                                	  window.location.href ="/ispbrain";
                                	}, 3000);
                                
                                } else {
                                	generateNoty('error', 'Error al eliminar la IP Excluida.');
                                }
                            }
                        });
                    }
                });
            });

            $('a[id="ip-excluded-tab"]').on('shown.bs.tab', function (e) {
                table_ip_excluded.ajax.reload();
                table_ip_excluded.draw();
            });
            
            setTimeout(function(){
                
                table_ip_excluded.draw();
            },100)
        });
        
        function clearFormAddIPFree(){
        
            $('#form-add-ip-excluded #ip').val('');
            $('#form-add-ip-excluded #comments').val('');
        }

        // Jquery draggable modals
        $('.modal-dialog').draggable({
            handle: ".modal-header"
        });

        //end ip-excluded
    </script>
  
<?php $this->end(); ?>
