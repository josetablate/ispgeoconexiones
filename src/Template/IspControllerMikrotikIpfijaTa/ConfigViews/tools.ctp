<?php $this->extend('/IspControllerMikrotikIpfijaTa/ConfigViews/tabs'); ?>

<?php $this->start('tools'); ?>


<div class="row justify-content-center mt-3">
    <div class="col-xl-8">
        <div class="card border-naranja mb-3">
          <div class="card-header">
              <h5 class="text-naranja">Advertencia</h5> 
          </div>
          <div class="card-body text-secondary">
            <h5 class="card-title">Configuración directa al router</h5>
            <p class="card-text">Lo botones siguientes, realizan configuraciones directamente en el router. 
            Esta configuraciones son necesaria para el correcto funcionamiento del sistema.  </p>
          </div>
        </div>
    </div>
</div>
    
<div class="row justify-content-center mb-5">
    
    <div class="col-xl-5">
        
        <div class="row">
            <div class="col-10">
                
                 <?php
        
                  echo $this->Html->link(
                        '<span class="glyphicon icon-cog text-white " aria-hidden="true"></span> CONFIGURACIÓN AVISOS y BLOQUEO',
                        'javascript:void(0)',
                        [
                            'id' => 'btn-apply-config-avisos',
                            'title' => '',
                            'class' => 'btn btn-primary mt-2 w-100 text-left',
                            'escape' => false
                            ]);
                
                  ?>
                
            </div>
            <div class="col-2">
                <i id="apply_config_avisos" class="fa fa-check text-success fa-2x mt-2 <?=($controller->apply_config_avisos) ? '' : 'd-none' ?>" aria-hidden="true"></i>
            </div>
        </div>
        
        <div class="row">
            <div class="col-10">
                
                 <?php
                    
                     echo $this->Html->link(
                            '<span class="glyphicon icon-cog text-white " aria-hidden="true"></span>CONFIGURACIÓN QUEUES GRAPH',
                            'javascript:void(0)',
                            [
                                'id' => 'btn-apply-config-queues-graph',
                                'title' => '',
                                'class' => 'btn btn-primary mt-2  w-100 text-left',
                                'escape' => false
                                ]);
                    
                    ?>
                
            </div>
            <div class="col-2">
                <i id="apply_config_queue_graph" class="fa fa-check text-success fa-2x mt-2 <?=($controller->apply_config_queue_graph) ? '' : 'd-none' ?>" aria-hidden="true"></i>
            </div>
        </div>
        
        <div class="row">
            <div class="col-10">
                
               <?php
        
                            
                echo $this->Html->link(
                        '<span class="glyphicon icon-cog text-white " aria-hidden="true"></span> GENERAR CERTIFICADO Y APLICAR',
                        'javascript:void(0)',
                        [
                            'id' => 'btn-generate-certificate',
                            'title' => '',
                            'class' => 'btn btn-primary mt-2  w-100 text-left',
                            'escape' => false
                            ]);
                
                ?>
                
            </div>
            <div class="col-2">
           
                <i id="apply_config_https" class="fa fa-check text-success fa-2x mt-2 <?=($controller->apply_config_https) ? '' : 'd-none' ?>" aria-hidden="true"></i>
             
            </div>
        </div>      
        
        <div class="row">
            <div class="col-10">
                
               <?php
                            
                echo $this->Html->link(
                    '<span class="glyphicon icon-cog text-white " aria-hidden="true"></span> CONFIGURAR ACCESO NUEVO CLIENTE',
                    'javascript:void(0)',
                    [
                        'id' => 'btn-apply-config-access-new-customer',
                        'title' => '',
                        'class' => 'btn btn-primary mt-2  w-100 text-left',
                        'escape' => false
                        ]);
                ?>
                
            </div>
            <div class="col-2">
                <i id="apply_config_temp_access" class="fa fa-check text-success fa-2x mt-2 <?=($controller->apply_config_temp_access) ? '' : 'd-none' ?>" aria-hidden="true"></i>
            </div>
        </div>
        
        <div class="row d-none">
            <div class="col-10">
                
               <?php
                            
                echo $this->Html->link(
                    '<span class="glyphicon icon-cog text-white " aria-hidden="true"></span>GENREAR ARP DESDE MIKROTIK',
                    'javascript:void(0)',
                    [
                        'id' => 'btn-generate-arps-from-mikrotik',
                        'title' => '',
                        'class' => 'btn btn-primary mt-2  w-100 text-left',
                        'escape' => false
                        ]);
                ?>
                
            </div>
          
        </div>
        

        
    </div>
   
</div>

<div class="modal fade" id="modal-commands" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Comandos para el router mikrotik</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-md-12">
                         <textarea style="font-family: Consolas, monospace; background-color: #000000; color: #1ac9e5;" name="" id="commands" rows="10" readonly> </textarea>
                    </div>
                </div>                            
            </div>
            <div class="card-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-info float-right" onclick="copiarAlPortapapeles('commands')">Copiar</button>
            </div>
        </div>
    </div>
</div>

 <script type="text/javascript">

    function copiarAlPortapapeles(id_elemento) {
        document.getElementById(id_elemento).select();
        try {
            var status = document.execCommand('copy');
            if(!status){
                console.error("Cannot copy text");
            }else{
                console.log("The text is now on the clipboard");
            }
        } catch (err) {
            console.log('Unable to copy.');
        }

        // document.body.removeChild(aux);
    }
     
    $(document).ready(function () {

        $("#btn-apply-config-avisos").click(function() {

            var request = $.ajax({
                url: "/ispbrain/IspControllerMikrotikIpfijaTa/applyConfigAvisos",
                method: "POST",
                data:  JSON.stringify({ controller_id: controller_id} ),
                dataType: "json"
            });

            request.done(function( response ) {

                if (response.data) {
                    $('#modal-commands #commands').val(response.data);
                    $('#modal-commands').modal('show');
                } else {
                     generateNoty('warning', 'No se pudo obtener los comandos' );
                }
            });

            request.fail(function(jqXHR) {

                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() { 
                	  window.location.href ="/ispbrain";
                	}, 3000);

                } else {
                	generateNoty('error','Error al intentar obtener los comandos');
                }      
            });           
        });     
        
        $('#btn-apply-config-queues-graph').click(function(){

            var request = $.ajax({
                url: "/ispbrain/IspControllerMikrotikIpfijaTa/applyConfigQueuesGraph",
                method: "POST",
                data:  JSON.stringify({ controller_id: controller_id} ),
                dataType: "json"
            });

            request.done(function( response ) {

                if (response.data) {
                    $('#modal-commands #commands').val(response.data);
                    $('#modal-commands').modal('show');
                } else {
                     generateNoty('warning', 'No se pudo obtener los comandos' );
                }
            });

            request.fail(function(jqXHR) {

                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() { 
                	  window.location.href ="/ispbrain";
                	}, 3000);

                } else {
                	generateNoty('error','Error al intentar obtener los comandos');
                }      
            });
            
        });
        
        $('#btn-generate-certificate').click(function(){


            var request = $.ajax({
                url: "/ispbrain/IspControllerMikrotikIpfijaTa/generateCertificateAndApply",
                method: "POST",
                data:  JSON.stringify({ controller_id: controller_id} ),
                dataType: "json"
            });

            request.done(function( response ) {

                if (response.data) {
                    $('#modal-commands #commands').val(response.data);
                    $('#modal-commands').modal('show');
                } else {
                     generateNoty('warning', 'No se pudo obtener los comandos' );
                }
            });

            request.fail(function(jqXHR) {

                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() { 
                	  window.location.href ="/ispbrain";
                	}, 3000);

                } else {
                	generateNoty('error','Error al intentar obtener los comandos');
                }      
            });
        });       
       
        
        $('#btn-apply-config-access-new-customer').click(function(){

            var request = $.ajax({
                url: "/ispbrain/IspControllerMikrotikIpfijaTa/applyConfigAccessNewCustomer",
                method: "POST",
                data:  JSON.stringify({ controller_id: controller_id} ),
                dataType: "json"
            });

            request.done(function( response ) {

                if (response.data) {
                    $('#modal-commands #commands').val(response.data);
                    $('#modal-commands').modal('show');
                } else {
                     generateNoty('warning', 'No se pudo obtener los comandos' );
                }
            });

            request.fail(function(jqXHR) {

                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() { 
                	  window.location.href ="/ispbrain";
                	}, 3000);

                } else {
                	generateNoty('error','Error al intentar obtener los comandos');
                }      
            });
        
        }); 
    
    });
             
 </script>


  
<?php $this->end(); ?>
