<?php $this->extend('/IspControllerMikrotikIpfijaTa/SyncViews/address_lists'); ?>

<?php $this->start('address-lists-client'); ?>


<style type="text/css">
    
     #table-address_lists_clientA_wrapper .title{
        color: #696868;
        padding-top: 10px;
    }
    
     #table-address_lists_clientB_wrapper .title{
        color: #696868;
        padding-top: 10px;
    }
    
</style>


    <div class="row">
        <div class="col-xl-5">
            
            <div class="card border-secondary mb-1 mt-2">
                <div class="card-body p-1">
                    <div class="text-secondary p-0">Controlador Seleccionado: <span class="font-weight-bold"><?=$controller->name?></span></div>
                </div>
            </div>
        </div>
        <div class="col-xl-1">
            <?php
              echo $this->Html->link(
                '<span class="glyphicon icon-loop2" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                    'id' => 'btn-refresh-address_lists_client',
                    'title' => 'Actualizar tablas',
                    'class' => 'btn btn-default mt-2',
                    'escape' => false
                ]);
             
             ?>
        </div>
        <div class="col-xl-2">
            
             <?php
              echo $this->Html->link(
                'Sincronizar todo',
                'javascript:void(0)',
                [
                    'id' => 'btn-sync-address_lists_client',
                    'title' => '',
                    'class' => 'btn btn-primary mt-2',
                ]);
             
             ?>

        </div>
        
        <div class="col-xl-4 text-right">
            <?= $this->Form->input('clear_address_lists_client', [
                'label' => 'Limpiar (address-list-client) del controlador',
                'checked' => false,
                'class' => 'm-0 mr-3 mt-3',
                'title' => 'Eliminar los registros agenos a este controlador. Los marcados en azul.',
                'data-toggle' => 'tooltip',
                ])?>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-6">
            <table class="table table-bordered table-hover" id="table-address_lists_clientA">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>List</th>
                        <th>Address</th>
                        <th>Comment</th>
                    </tr>
                </thead>
                <tbody>
                  
                </tbody>
            </table>
        </div>
        <div class="col-xl-6">
            <table class="table table-bordered table-hover" id="table-address_lists_clientB">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>List</th>
                        <th>Address</th>
                        <th>Comment</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div>
    
    
    
<?php

    $buttonsA = [];

    $buttonsA[] = [
        'id'   =>  'btn-edit-address_list_client',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];
    
    $buttonsA[] = [
        'id'   =>  'btn-sync-address_lists_client-indi',
        'name' =>  'Sincronizar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-addressListClientA', 'title' => 'Acciones', 'buttons' => $buttonsA ]);
    
    $buttonsB = [];

    $buttonsB[] = [
        'id'   =>  'btn-delete-address_list_client',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-addressListClientB', 'title' => 'Acciones', 'buttons' => $buttonsB ]);
  
?>


    <script type="text/javascript">

        var table_address_lists_clientA = null;
        var table_address_lists_clientB = null;
        var address_list_clientA_selected = null;
        var address_list_clientB_selected = null;
        

        $(document).ready(function () {
            //address_lists_clientA

            $('#table-address_lists_clientA').removeClass('display');

    		table_address_lists_clientA = $('#table-address_lists_clientA').DataTable({
    		    "order": [[ 0, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ordering" : false,
    		    "paging": false,
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/sync/mikrotik_ipfija_ta/addressListsClientA.json",
                    "dataSrc": "data",
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "api_id",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "list",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "address",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "comment",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    }
                ],
    		    "columnDefs": [
    		        { "type": 'ip-address', targets: [2] },
                   
                ],
                "createdRow" : function( row, data, index ) {
                    row.id = data.connection_id;
                },
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-address_listsA_wrapper .title').append('<h5>Sistema</h5>');
    		
    		$('#table-address_lists_clientA tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {
                    
                    table_address_lists_clientA.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    address_list_clientA_selected = table_address_lists_clientA.row( this ).data();
                    $('.modal-addressListClientA').modal('show');
                }
            });
            
            $('#btn-edit-address_list-client').click(function() {
                var action = '/ispbrain/connections/edit/' + table_address_lists_clientA.connection_id;
                window.open(action, '_black');
            });
            
            $('#btn-sync-address_lists_client').click(function() {
                
                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
                
                var connection_ids = [];
                
                $.each(table_address_lists_clientA.rows().data(), function(i, address_list_client){
                    connection_ids.push(address_list_client.connection_id);
                });
                
                var text = '¿Está seguro que desea sincronizar los (address lists client) ?';
    
                bootbox.confirm(text, function(result) {
                    if (result) {
                    
                        openModalPreloader("Sincronizando ...");
                        
                         $.ajax({
                            url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikIpfijaTa', 'action' => 'sync-address-lists-client'])?>" ,
                            type: 'POST',
                            dataType: "json",
                            data: JSON.stringify({controller_id: controller_id, connection_ids: connection_ids, delete_address_lists_client_side_b: $('#clear-address-lists-client').is(':checked')}),
                            success: function(data){
                                
                                console.log(data);
                                
                                if(data.response){
                                    generateNoty('success', 'Sincronización de (address lists client) completa');
                                }else{
                                    generateNoty('error', 'Error en la Sincronización');
                                }
                                
                                closeModalPreloader();
                               
                                table_address_lists_clientA.ajax.reload();
                                table_address_lists_clientB.ajax.reload();
                               
                            },
                            error: function (jqXHR) {

                                if (jqXHR.status == 403) {
                                
                                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                                
                                	setTimeout(function() { 
                                	  window.location.href ="/ispbrain";
                                	}, 3000);
                                
                                } else {
                                	closeModalPreloader();
                                    generateNoty('error', 'Error al intentar sincronizar los (address lists client).');
                                }
                            }
                        });
                    }
                });
            });
            
            $('#btn-sync-address_lists_client-indi').click(function() {
                
                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
                
                var connection_ids = [];
                connection_ids.push(address_list_clientA_selected.connection_id);
          
                var text = '¿Está seguro que desea sincronizar los (address lists client) ?';
    
                bootbox.confirm(text, function(result) {
                    if (result) {
                        
                        $('.modal-addressListClientA').modal('hide');
                    
                        openModalPreloader("Sincronizando ...");
                        
                         $.ajax({
                            url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikIpfijaTa', 'action' => 'sync-address-list-client-indi'])?>" ,
                            type: 'POST',
                            dataType: "json",
                            data: JSON.stringify({controller_id: controller_id, connection_ids: connection_ids, delete_address_lists_client_side_b: $('#clear-address-lists-client').is(':checked')}),
                            success: function(data){
                             
                                if(data.response){
                                    generateNoty('success', 'Sincronización de (address list client) completa');
                                }else{
                                    generateNoty('error', 'Error en la Sincronización');
                                }
                                
                                closeModalPreloader();
                               
                                table_address_lists_clientA.ajax.reload();
                                table_address_lists_clientB.ajax.reload();
                                
                                updateCountersTitle();
                               
                            },
                            error: function (jqXHR) {

                                if (jqXHR.status == 403) {
                                
                                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                                
                                	setTimeout(function() { 
                                	  window.location.href ="/ispbrain";
                                	}, 3000);
                                
                                } else {
                                	closeModalPreloader();
                                    generateNoty('error', 'Error al intentar sincronizar los (address list client).');
                                    updateCountersTitle();
                                }
                            }
                        });
                    }
                });
            });
            
            $('#btn-refresh-address_lists_client').click(function() {
                
                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
            
                openModalPreloader("Actualizando ...");
                
                 $.ajax({
                    url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikIpfijaTa', 'action' => 'refreshAddressListsClient'])?>" ,
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify({controller_id: controller_id}),
                    success: function(data){
                        
                        closeModalPreloader();
    
                        if (!data.data) {
                             generateNoty('error', 'Error al intentar actualizar.');
                        }else{
                            table_address_lists_clientA.ajax.reload();
                            table_address_lists_clientB.ajax.reload();
                        }
                        
                        updateCountersTitle();
                    },
                    error: function (jqXHR) {

                        if (jqXHR.status == 403) {
                        
                        	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                        
                        	setTimeout(function() { 
                        	  window.location.href ="/ispbrain";
                        	}, 3000);
                        
                        } else {
                        	closeModalPreloader();
                            generateNoty('error', 'Error al intentar actualizar.');
                            updateCountersTitle();
                        }
                    }
                });
                
            });

            //end address_lists_clientA

            //address_lists_clientB

            $('#table-address_lists_clientB').removeClass('display');

    		table_address_lists_clientB = $('#table-address_lists_clientB').DataTable({
    		    "order": [[ 0, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ordering" : false,
    		    "paging": false,
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/sync/mikrotik_ipfija_ta/addressListsClientB.json",
                    "dataSrc": "data",
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "api_id",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "list",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "address",
                        "render": function ( data, type, row ) {
                             if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "comment",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    }
                ],
    		    "columnDefs": [
    		        { "type": 'ip-address', targets: [2] },
                ],
                
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-address_listsB_wrapper .title').append('<h5>Controlador</h5>');
    		
    		$('#table-address_lists_clientB tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {
                    
                    table_address_lists_clientB.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    address_list_clientB_selected = table_address_lists_clientB.row( this ).data();
                    $('.modal-addressListClientB').modal('show');
                }
            });
            
            $('#btn-delete-address_list_client').click(function() {
            
                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
                
                openModalPreloader("Eliminado Address List del Controlador ...");
                
                $('.modal-addressListClientB').modal('hide');
                
                 $.ajax({
                    url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikIpfijaTa', 'action' => 'deleteAddressListClientInController'])?>" ,
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify({controller_id: controller_id, address_list_client_api_id: address_list_clientB_selected.api_id}),
                    success: function(data){
                        
                        closeModalPreloader();
    
                        if (!data.data) {
                             generateNoty('error', 'Error al intentar eliminar.');
                        }else{
                            generateNoty('success', '(address lists client) eliminado.');
                        }
                        
                        table_address_lists_clientA.ajax.reload();
                        table_address_lists_clientB.ajax.reload();
                        
                        updateCountersTitle();
                        
                    },
                    error: function (jqXHR) {

                        if (jqXHR.status == 403) {
                        
                        	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                        
                        	setTimeout(function() { 
                        	  window.location.href ="/ispbrain";
                        	}, 3000);
                        
                        } else {
                        	closeModalPreloader();
                            generateNoty('error', 'Error al intentar eliminar.');
                            updateCountersTitle();
                        }

                    }
                });
            });
    		
    		 //end address_lists_clientB

    		$('a[id="address-lists-client-tab"]').on('shown.bs.tab', function (e) {
                table_address_lists_clientA.draw();
                table_address_lists_clientB.draw();
            });

           
        });
    </script>
 
<?php $this->end(); ?>
