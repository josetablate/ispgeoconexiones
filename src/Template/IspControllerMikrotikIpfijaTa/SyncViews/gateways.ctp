<?php $this->extend('/IspControllerMikrotikIpfijaTa/SyncViews/tabs'); ?>

<?php $this->start('gateways'); ?>
 
<style type="text/css">
    
     #table-gatewaysA_wrapper .title{
        color: #696868;
        padding-top: 10px;
    }
    
     #table-gatewaysB_wrapper .title{
        color: #696868;
        padding-top: 10px;
    }
    
</style>


    <div class="row">
        <div class="col-xl-5">
            
            <div class="card border-secondary mb-1 mt-2">
                <div class="card-body p-1">
                    <div class="text-secondary p-0">Controlador Seleccionado: <span class="font-weight-bold"><?=$controller->name?></span></div>
                </div>
            </div>
        </div>
        <div class="col-xl-1">
            <?php
              echo $this->Html->link(
                '<span class="glyphicon icon-loop2" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                    'id' => 'btn-refresh-gateways',
                    'title' => 'Click para realizar el proceso control de diferencias',
                    'class' => 'btn btn-default mt-2',
                    'data-toggle' => 'tooltip',
                    'escape' => false
                ]);
             
             ?>
        </div>
        <div class="col-xl-2">
            
             <?php
              echo $this->Html->link(
                'Sincronizar todo',
                'javascript:void(0)',
                [
                    'id' => 'btn-sync-gateways',
                    'title' => '',
                    'class' => 'btn btn-primary mt-2',
                ]);
             
             ?>

        </div>
        
        <div class="col-xl-4 text-right">
            <?= $this->Form->input('clear_gateways', [
            'label' => 'Limpiar (gateway) del controlador',
            'checked' => false,
            'class' => 'm-0 mr-3 mt-3',
            'title' => 'Eliminar los registros agenos a este controlador. Los marcados en azul.',
            'data-toggle' => 'tooltip',
            ])?>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-6">
            <table class="table table-bordered table-hover" id="table-gatewaysA">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Address</th>
                        <th>Interface</th>
                        <th>Comentario</th>
                    </tr>
                </thead>
                <tbody>
                  
                </tbody>
            </table>
        </div>
        <div class="col-xl-6">
            <table class="table table-bordered table-hover" id="table-gatewaysB">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Address</th>
                        <th>Interface</th>
                        <th>Comentario</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div>
    
    
    
<?php

    $buttonsA = [];
    
    $buttonsA[] = [
        'id'   =>  'btn-edit-gateway',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];
    
    $buttonsA[] = [
        'id'   =>  'btn-sync-gateway-indi',
        'name' =>  'Sincronizar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-gatewaysA', 'title' => 'Acciones', 'buttons' => $buttonsA ]);
    
    $buttonsB = [];

    $buttonsB[] = [
        'id'   =>  'btn-delete-gateway',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-gatewaysB', 'title' => 'Acciones', 'buttons' => $buttonsB ]);
  
?>


    <script type="text/javascript">
        
    
    
        var table_gatewaysA = null;
        var table_gatewaysB = null;
        var gatewayA_selected = null;

        $(document).ready(function () {
            
            //gatewaysA

            $('#table-gatewaysA').removeClass('display');

    		table_gatewaysA = $('#table-gatewaysA').DataTable({
    		    "order": [[ 0, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ordering" : false,
    		    "paging": false,
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/sync/mikrotik_ipfija_ta/gatewaysA.json",
                    "dataSrc": "data",
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                
                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "api_id",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "address",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "interface",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "comment",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    }
                ],
    		    "columnDefs": [
                   
                ],
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-gatewaysA_wrapper .title').append('<h5>Sistema</h5>');
    		
    		$('#table-gatewaysA tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {
                    
                    table_gatewaysA.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    gatewayA_selected = table_gatewaysA.row( this ).data();
                    $('.modal-gatewaysA').modal('show');
                }
            });
            
            $('#btn-edit-gateway').click(function() {
                var action = '/ispbrain/IspControllerMikrotikIpfijaTa/config/' + gatewayA_selected.controller_id;
                window.open(action, '_black');
            });
          
            $('#btn-sync-gateways').click(function() {
                
                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
                
                if(table_gatewaysA.rows('tr').count() == 0 && table_gatewaysB.rows('tr').count() == 0){
            
                    generateNoty('warning', 'No existen elementos para sincronizar.');
                    
                }else{
                    
                    var gateways_ids = [];
                
                    $.each(table_gatewaysA.rows().data(), function(i, gateway){
                        gateways_ids.push(gateway.id);
                    });
              
                    var text = '¿Está seguro que desea sincronizar los gateways?';
        
                    bootbox.confirm(text, function(result) {
                        if (result) {
                        
                            openModalPreloader("Sincronizando ...");
                            
                             $.ajax({
                                url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikIpfijaTa', 'action' => 'sync-gateways'])?>" ,
                                type: 'POST',
                                dataType: "json",
                                data: JSON.stringify({controller_id: controller_id, gateways_ids: gateways_ids, delete_gateways_side_b: $('#clear-gateways').is(':checked')}),
                                success: function(data){
                                    
                                    console.log(data);
                                    
                                    if(data.response){
                                        generateNoty('success', 'Sincronización de (gateways) completa');
                                    }else{
                                        generateNoty('error', 'Error en la Sincronización');
                                    }
                                    
                                    closeModalPreloader();
                                   
                                    table_gatewaysA.ajax.reload();
                                    table_gatewaysB.ajax.reload();
                                    
                                    updateCountersTitle();
                                   
                                },
                                error: function (jqXHR ) {
                                                        
                                    if(jqXHR.status == 403){
                                        
                                        generateNoty('warning', 'La sesión ha expirado. Por favor ingrese sus credenciales.');
                                        
                                        setTimeout(function(){ 
                                          window.location.href ="/ispbrain";
                                        }, 3000);
                                        
                                    }else{
                                        generateNoty('error', 'Error al intentar sincronizar los (gateways).');
                                        closeModalPreloader();
                                        updateCountersTitle();
                                    }
                                    
                                    
                                }
                                
                            });
                        }
                    });
                    
                    
                }
                
                
                
            });
            
            $('#btn-sync-gateway-indi').click(function() {
                
                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
                
                var gateways_ids = [];
                gateways_ids.push(gatewayA_selected.id);
          
                var text = '¿Está seguro que desea sincronizar este (gateway)?';
    
                bootbox.confirm(text, function(result) {
                    if (result) {
                        
                        $('.modal-gatewaysA').modal('hide');
                    
                        openModalPreloader("Sincronizando ...");
                        
                         $.ajax({
                            url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikIpfijaTa', 'action' => 'sync-gateway-indi'])?>" ,
                            type: 'POST',
                            dataType: "json",
                            data: JSON.stringify({controller_id: controller_id, gateways_ids: gateways_ids, delete_gateways_side_b: $('#clear-gateways').is(':checked')}),
                            success: function(data){
                                
                                console.log(data);
                                
                                if(data.response){
                                    generateNoty('success', 'Sincronización de (gateways) completa');
                                }else{
                                    generateNoty('error', 'Error en la Sincronización');
                                }
                                
                                closeModalPreloader();
                               
                                table_gatewaysA.ajax.reload();
                                table_gatewaysB.ajax.reload();
                                
                                updateCountersTitle();
                                
                               
                            },
                            error: function (jqXHR ) {
                                                    
                                if(jqXHR.status == 403){
                                    
                                    generateNoty('warning', 'La sesión ha expirado. Por favor ingrese sus credenciales.');
                                    
                                    setTimeout(function(){ 
                                      window.location.href ="/ispbrain";
                                    }, 3000);
                                    
                                }else{
                                    generateNoty('error', 'Error al intentar sincronizar los (gateways).');
                                    closeModalPreloader();
                                    updateCountersTitle();
                                }
                                
                                
                            }
                            
                        });
                    }
                });
                
            });
            
            $('#btn-refresh-gateways').click(function() {

             
                
                if(!sessionPHP.paraments.system.integration){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
            
                openModalPreloader("Actualizando ...");
                
                 $.ajax({
                    url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikIpfijaTa', 'action' => 'refreshGateways'])?>" ,
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify({controller_id: controller_id}),
                    success: function(data){
                        
                        closeModalPreloader();
    
                        if (!data.data) {
                             generateNoty('error', 'No se pudo establecer una conexión con el controlador.');
                        }else{
                            table_gatewaysA.ajax.reload();
                            table_gatewaysB.ajax.reload();
                        }
                        
                        updateCountersTitle();
                    },
                    error: function (jqXHR ) {
                                                    
                        if(jqXHR.status == 403){
                            
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese sus credenciales.');
                            
                            setTimeout(function(){ 
                              window.location.href ="/ispbrain";
                            }, 3000);
                            
                        }else{
                            generateNoty('error', 'Error al intentar sincronizar actualizar.');
                            closeModalPreloader();
                        }
                    }
                });
                
            });
            

            //end gatewaysA

            //gatewaysB

            $('#table-gatewaysB').removeClass('display');

    		table_gatewaysB = $('#table-gatewaysB').DataTable({
    		    "order": [[ 0, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ordering" : false,
    		    "paging": false,
    		    
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/sync/mikrotik_ipfija_ta/gatewaysB.json",
                    "dataSrc": "data",
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                
                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "api_id",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "address",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "interface",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "comment",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    }
                ],
    		    "columnDefs": [
    		       
                ],
                
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-gatewaysB_wrapper .title').append('<h5>Controlador</h5>');
    		
    		$('#table-gatewaysB tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {
                    
                    table_gatewaysB.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    gatewayB_selected = table_gatewaysB.row( this ).data();
                    $('.modal-gatewaysB').modal('show');
                }
            });
            
            $('#btn-delete-gateway').click(function() {
                
                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
                
                $('.modal-gatewaysB').modal('hide');
                
                openModalPreloader("Eliminado (gateway) del Controlador ...");
                
                 $.ajax({
                    url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikIpfijaTa', 'action' => 'deleteGatewayInController'])?>" ,
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify({controller_id: controller_id, gateway_api_id: gatewayB_selected.api_id}),
                    success: function(data){
                        
                        closeModalPreloader();
    
                        if (!data.data) {
                             generateNoty('error', 'Error al intentar eliminar.');
                        }else{
                            table_gatewaysA.ajax.reload();
                            table_gatewaysB.ajax.reload();
                        }
                        
                        updateCountersTitle();
                         
                    },
                    error: function (jqXHR ) {
                                            
                        if(jqXHR.status == 403){
                            
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese sus credenciales.');
                            
                            setTimeout(function(){ 
                              window.location.href ="/ispbrain";
                            }, 3000);
                            
                        }else{
                            generateNoty('error', 'Error al intentar eliminar.');
                            closeModalPreloader();
                            updateCountersTitle();
                        }
                        
                        
                    }
                });
            });
    		
    		 //end gatewaysB

    		$('a[id="gateways-tab"]').on('shown.bs.tab', function (e) {
    		    
                table_gatewaysA.ajax.reload();
                table_gatewaysB.ajax.reload();
               
            });
            
       
           
        });
    </script>
<?php $this->end(); ?>
