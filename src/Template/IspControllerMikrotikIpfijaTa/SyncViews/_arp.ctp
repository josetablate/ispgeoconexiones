<?php $this->extend('/IspControllerMikrotikIpfijaTa/SyncViews/address_lists'); ?>

<?php $this->start('arp'); ?>

<style type="text/css">
    
     #table-arpA_wrapper .title{
        color: #696868;
        padding-top: 10px;
    }
    
     #table-arpB_wrapper .title{
        color: #696868;
        padding-top: 10px;
    }
    
</style>


    <div class="row">
        <div class="col-xl-5">
            
            <div class="card border-secondary mb-1 mt-2">
                <div class="card-body p-1">
                    <div class="text-secondary p-0">Controlador Seleccionado: <span class="font-weight-bold"><?=$controller->name?></span></div>
                </div>
            </div>
        </div>
        <div class="col-xl-1">
            <?php
              echo $this->Html->link(
                '<span class="glyphicon icon-loop2" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                    'id' => 'btn-refresh-arp',
                    'title' => 'Actualizar tablas',
                    'class' => 'btn btn-default mt-2',
                    'escape' => false
                ]);
             
             ?>
        </div>
        <div class="col-xl-2">
            
             <?php
              echo $this->Html->link(
                'Sincronizar todo',
                'javascript:void(0)',
                [
                    'id' => 'btn-sync-arp',
                    'title' => '',
                    'class' => 'btn btn-primary mt-2',
                ]);
             
             ?>

        </div>
        
        <div class="col-xl-4">
            <?= $this->Form->input('clear_arp', [
                'label' => 'Limpiar (arp) del controlador',
                'checked' => false,
                'class' => 'm-0 mr-3 mt-3',
                'title' => 'Eliminar los registros agenos a este controlador. Los marcados en azul.',
                'data-toggle' => 'tooltip',
                ])?>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-6">
            <table class="table table-bordered table-hover" id="table-arpA">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Address</th>
                        <th>Mac Address</th>
                        <th>Interface</th>
                        <th>Comment</th>
                    </tr>
                </thead>
                <tbody>
                  
                </tbody>
            </table>
        </div>
        <div class="col-xl-6">
            <table class="table table-bordered table-hover" id="table-arpB">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Address</th>
                        <th>Mac Address</th>
                        <th>Interface</th>
                        <th>Comment</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div>
    
    
    
<?php

    $buttonsA = [];

    $buttonsA[] = [
        'id'   =>  'btn-edit-arp',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];
    
    $buttonsA[] = [
        'id'   =>  'btn-sync-arp-indi',
        'name' =>  'Sincronizar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-arpA', 'title' => 'Acciones', 'buttons' => $buttonsA ]);
    
    
    $buttonsB = [];

    $buttonsB[] = [
        'id'   =>  'btn-delete-arp',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-arpB', 'title' => 'Acciones', 'buttons' => $buttonsB ]);
  
?>


    <script type="text/javascript">

        var table_arpA = null;
        var table_arpB = null;
        var arpA_selected = null;

        $(document).ready(function () {
            //arpA

            $('#table-arpA').removeClass('display');

    		table_arpA = $('#table-arpA').DataTable({
    		    "order": [[ 0, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ordering" : false,
    		    "paging": false,
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/sync/mikrotik_ipfija_ta/arpA.json",
                    "dataSrc": "data",
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "api_id",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "address",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "mac_address",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "interface",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "comment",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    
                ],
    		    "columnDefs": [
    		     
                ],
                "createdRow" : function( row, data, index ) {
                    row.id = data.connection_id;
                },
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-arpA_wrapper .title').append('<h5>Sistema</h5>');
    		
    		$('#table-arpA tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {
                    
                    table_arpA.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    arpA_selected = table_arpA.row( this ).data();
                    $('.modal-arpA').modal('show');
                }
            });
            
            $('#btn-edit-arp').click(function() {
                var action = '/ispbrain/connections/edit/' + arpA_selected.connection_id;
                window.open(action, '_black');
            });
            
            $('#btn-sync-arp').click(function() {
                
                if(!sessionPHP.paraments.system.integration){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
                
                var connection_ids = [];
                
                $.each(table_arpA.$('tr'), function(i, arp){
                    connection_ids.push(arp.id);
                });
          
                var text = '¿Está seguro que desea sincronizar los (arps)?';
    
                bootbox.confirm(text, function(result) {
                    if (result) {
                    
                        openModalPreloader('Sincronizando ... ');
                        
                         $.ajax({
                            url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikIpfijaTa', 'action' => 'sync-arps'])?>" ,
                            type: 'POST',
                            dataType: "json",
                            data: JSON.stringify({controller_id: controller_id, connection_ids: connection_ids, delete_arp_side_b: $('#clear-arp').is(':checked')}),
                            success: function(data){
                                
                                closeModalPreloader();
            
                                if (!data.data) {
                                     generateNoty('error', 'Error al intentar sincronizar los (arps).');
                                }else{
                                    generateNoty('success', 'sincronización de (arps) completa.');
                                    table_arpA.ajax.reload();
                                    table_arpB.ajax.reload();
                                }
                                
                                updateCountersTitle();
                            },
                            
                            error: function (jqXHR) {

                                if (jqXHR.status == 403) {
                                
                                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                                
                                	setTimeout(function() { 
                                	  window.location.href ="/ispbrain";
                                	}, 3000);
                                
                                } else {
                                	closeModalPreloader();
                                    generateNoty('error', 'Error al intentar sincronizar los (arps).');
                                    updateCountersTitle();
                                }
                            }
                        });
                    }
                });
            });
            
            $('#btn-sync-arp-indi').click(function() {
                
                if(!sessionPHP.paraments.system.integration){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
                
                var connection_ids = [];
                connection_ids.push(arpA_selected.connection_id);
               
          
                var text = '¿Está seguro que desea sincronizar el (arp)?';
    
                bootbox.confirm(text, function(result) {
                    if (result) {
                        
                        $('.modal-arpA').modal('hide');
                    
                        openModalPreloader('Sincronizando ... ');
                        
                         $.ajax({
                            url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikIpfijaTa', 'action' => 'sync-arp-indi'])?>" ,
                            type: 'POST',
                            dataType: "json",
                            data: JSON.stringify({controller_id: controller_id, connection_ids: connection_ids, delete_arp_side_b: $('#clear-arp').is(':checked')}),
                            success: function(data){
                                
                                closeModalPreloader();
            
                                if (!data.data) {
                                     generateNoty('error', 'Error al intentar sincronizar el (arp).');
                                }else{
                                    table_arpA.ajax.reload();
                                    table_arpB.ajax.reload();
                                    generateNoty('success', 'sincronización de (arp) completa.');
                                }
                                
                                updateCountersTitle();
                            },
                            error: function (jqXHR) {

                                if (jqXHR.status == 403) {
                                
                                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                                
                                	setTimeout(function() { 
                                	  window.location.href ="/ispbrain";
                                	}, 3000);
                                
                                } else {
                                	closeModalPreloader();
                                    generateNoty('error', 'Error al intentar sincronizar el (arp).');
                                    updateCountersTitle();
                                }
                            }
                        });
                    }
                });
            });
            
            $('#btn-refresh-arp').click(function() {
                
                if(!sessionPHP.paraments.system.integration){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
               
                openModalPreloader('Actualizando ... ');
                
                 $.ajax({
                    url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikIpfijaTa', 'action' => 'refreshArp'])?>" ,
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify({controller_id: controller_id}),
                    success: function(data){
                        
                         closeModalPreloader();
    
                        if (!data.data) {
                             generateNoty('error', 'Error al intentar actualizar.');
                        }else{
                            table_arpA.ajax.reload();
                            table_arpB.ajax.reload();
                            
                            updateCountersTitle();
                        }
                    },
                    error: function (jqXHR) {

                        if (jqXHR.status == 403) {
                        
                        	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                        
                        	setTimeout(function() { 
                        	  window.location.href ="/ispbrain";
                        	}, 3000);
                        
                        } else {
                        	closeModalPreloader();
                            generateNoty('error', 'Error al intentar actualizar.');
                            updateCountersTitle();
                        }
                    }
                });
                
            });
            

            //end arpA

            //arpB

            $('#table-arpB').removeClass('display');

    		table_arpB = $('#table-arpB').DataTable({
    		    "order": [[ 0, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ordering" : false,
    		    "paging": false,
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/sync/mikrotik_ipfija_ta/arpB.json",
                    "dataSrc": "data",
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "api_id",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "address",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "mac_address",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "interface",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "comment",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                            else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                ],
    		    "columnDefs": [
    		     
                   
                ],
           
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-arpB_wrapper .title').append('<h5>Controlador</h5>');
    		
    		$('#table-arpB tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {
                    
                    table_arpB.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    arpB_selected = table_arpB.row( this ).data();
                    $('.modal-arpB').modal('show');
                }
            });
            
            $('#btn-delete-arp').click(function() {
                
                if(!sessionPHP.paraments.system.integration){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
               
                openModalPreloader('Eliminado Arp del Controlador ... ');
                
                $('.modal-arpB').modal('hide');
                
                 $.ajax({
                    url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikIpfijaTa', 'action' => 'deleteArpInController'])?>" ,
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify({controller_id: controller_id, arp_api_id: arpB_selected.api_id}),
                    success: function(data){
                        
                        table_arpA.ajax.reload();
                        table_arpB.ajax.reload();
                        
                        closeModalPreloader();
    
                        if (!data.data) {
                             generateNoty('error', 'Error al intentar eliminar.');
                        }else{
                            console.log('Eliminado Arp completo');
                        }
                        
                        updateCountersTitle();
                    },
                    error: function (jqXHR) {

                        if (jqXHR.status == 403) {
                        
                        	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                        
                        	setTimeout(function() { 
                        	  window.location.href ="/ispbrain";
                        	}, 3000);
                        
                        } else {
                        	closeModalPreloader();
                            generateNoty('error', 'Error al intentar eliminar.');
                            updateCountersTitle();
                        }
                    }
                });
            });
    		
    		 //end arpB

    		$('a[id="arp-tab"]').on('shown.bs.tab', function (e) {
                table_arpA.draw();
                table_arpB.draw();
            });

        });

    </script>
<?php $this->end(); ?>
