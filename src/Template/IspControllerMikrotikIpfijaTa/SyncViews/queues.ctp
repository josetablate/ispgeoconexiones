<?php $this->extend('/IspControllerMikrotikIpfijaTa/SyncViews/address_lists_client'); ?>

<?php $this->start('queues'); ?>


<style type="text/css">
    
     #table-queuesA_wrapper .title{
        color: #696868;
        padding-top: 10px;
    }
    
     #table-queuesB_wrapper .title{
        color: #696868;
        padding-top: 10px;
    }
    
</style>


    <div class="row">
        <div class="col-xl-5">
            
            <div class="card border-secondary mb-1 mt-2">
                <div class="card-body p-1">
                    <div class="text-secondary p-0">Controlador Seleccionado: <span class="font-weight-bold"><?=$controller->name?></span></div>
                </div>
            </div>
        </div>
        <div class="col-xl-1">
            <?php
              echo $this->Html->link(
                '<span class="glyphicon icon-loop2" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                    'id' => 'btn-refresh-queues',
                    'title' => 'Click para realizar el proceso control de diferencias',
                    'class' => 'btn btn-default mt-2',
                    'data-toggle' => 'tooltip',
                    'escape' => false
                ]);
             
             ?>
        </div>
        <div class="col-xl-2">
            
             <?php
              echo $this->Html->link(
                'Sincronizar todo',
                'javascript:void(0)',
                [
                    'id' => 'btn-sync-queues',
                    'title' => '',
                    'class' => 'btn btn-primary mt-2',
                ]);
             
             ?>

        </div>
        
        <div class="col-xl-4 text-right">
            <?= $this->Form->input('clear_queues', [
                'label' => 'Limpiar (queues) del controlador', 
                'checked' => false,
                'class' => 'm-0 mr-3 mt-3',
                'title' => 'Eliminar los registros agenos a este controlador. Los marcados en azul.',
                'data-toggle' => 'tooltip',
                ])?>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-6">
            <table class="table table-bordered table-hover" id="table-queuesA">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Target</th>
                        <th>max_limit</th>
                        <th>limit_at</th>
                        <th>burst_limit</th>
                        <th>burst_threshold</th>
                        <th>burst_time</th>
                        <th>priority</th>
                        <th>queue</th>
                    </tr>
                </thead>
                <tbody>
                  
                </tbody>
            </table>
        </div>
        <div class="col-xl-6">
            <table class="table table-bordered table-hover" id="table-queuesB">
                <thead>
                    <tr>
                        <th>Id</th>             <!--0-->
                        <th>Name</th>           <!--1-->
                        <th>Target</th>         <!--3-->
                        <th>max_limit</th>      <!--4-->
                        <th>limit_at</th>       <!--5-->
                        <th>burst_limit</th>    <!--6-->
                        <th>burst_threshold</th><!--7-->
                        <th>burst_time</th>     <!--8-->
                        <th>priority</th>       <!--9-->
                        <th>queue</th>          <!--10-->
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div>
    
    
    
<?php

    $buttonsA = [];

    $buttonsA[] = [
        'id'   =>  'btn-edit-queue',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];
    
     $buttonsA[] = [
        'id'   =>  'btn-sync-queue-indi',
        'name' =>  'Sincronizar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-queueA', 'title' => 'Acciones', 'buttons' => $buttonsA ]);
    
    
    $buttonsB = [];

    $buttonsB[] = [
        'id'   =>  'btn-delete-queue',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-queueB', 'title' => 'Acciones', 'buttons' => $buttonsB ]);
  
?>


    <script type="text/javascript">

        var table_queuesA = null;
        var table_queuesB = null;
        var queueA_selected = null;

        $(document).ready(function () {
            //queuesA

            $('#table-queuesA').removeClass('display');

    		table_queuesA = $('#table-queuesA').DataTable({
    		    "order": [[ 1, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ordering" : false,
    		    "paging": false,
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/sync/mikrotik_ipfija_ta/queuesA.json",
                    "dataSrc": "data",
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "api_id",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "name",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "target",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "max_limit",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "limit_at",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "burst_limit",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "burst_threshold",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "burst_time",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "priority",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    { 
                        "data": "queue",
                        "render": function ( data, type, row ) {
                            return data.value;
                        }
                    },
                    
                ],
    		    "columnDefs": [
    		        { "type": 'ip-address', targets: [2] },
                ],
                "createdRow" : function( row, data, index ) {
                    row.id = data.connection_id;
                },
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-queuesA_wrapper .title').append('<h5>Sistema</h5>');
    		
    		$('#table-queuesA tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {
                    
                    table_queuesA.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    queueA_selected = table_queuesA.row( this ).data();
                    $('.modal-queueA').modal('show');
                }
            });
            
            $('#btn-edit-queue').click(function() {
                var action = '/ispbrain/connections/edit/' + queueA_selected.connection_id;
                window.open(action, '_black');
            });
            
            $('#btn-sync-queues').click(function() {

                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
                
                if(table_queuesA.rows('tr').count() == 0 && table_queuesB.rows('tr').count() == 0){
            
                    generateNoty('warning', 'No existen elementos para sincronizar.');
                    
                    return false;
                    
                }
                
                var connections_ids = [];
                
                $.each(table_queuesA.$('tr'), function(i, queue){
                    connections_ids.push(queue.id);
                });
          
                var text = '¿Está seguro que desea sincronizar (Queues)?';
    
                bootbox.confirm(text, function(result) {
                    if (result) {
                    
                        openModalPreloader('Sincronizando ... ');
                        
                         $.ajax({
                            url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikIpfijaTa', 'action' => 'sync-queues'])?>" ,
                            type: 'POST',
                            dataType: "json",
                            data: JSON.stringify({controller_id: controller_id, connections_ids: connections_ids, delete_queue_side_b: $('#clear-queues').is(':checked')}),
                            success: function(data){
                                
                                console.log(data.response);
                                
                                closeModalPreloader();
            
                                if (!data.response) {
                                     generateNoty('error', 'Error al intentar sincronizar (Queues).');
                                }else{
                                    table_queuesA.ajax.reload();
                                    table_queuesB.ajax.reload();
                                    
                                     generateNoty('success', 'Sincronización de (Queues) completa');
                                }
                                
                                updateCountersTitle();
                            },
                            error: function (jqXHR) {

                                if (jqXHR.status == 403) {
                                
                                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                                
                                	setTimeout(function() { 
                                	  window.location.href ="/ispbrain";
                                	}, 3000);
                                
                                } else {
                                	closeModalPreloader();
                                    generateNoty('error', 'Error al intentar sincronizar (Queues).');
                                    updateCountersTitle();
                                }
                            }
                        });
                    }
                });
            });
            
            $('#btn-sync-queue-indi').click(function() {
                
                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
               
                var connections_ids = [];
                connections_ids.push(queueA_selected.connection_id);
             
          
                var text = '¿Está seguro que desea sincronizar este (queue)?';
                
                bootbox.confirm(text, function(result) {
                    if (result) {
                        
                        $('.modal-queueA').modal('hide');
                    
                        openModalPreloader('Sincronizando ... ');
                        
                         $.ajax({
                            url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikIpfijaTa', 'action' => 'sync-queue-indi'])?>" ,
                            type: 'POST',
                            dataType: "json",
                            data: JSON.stringify({controller_id: controller_id, connections_ids: connections_ids}),
                            success: function(data){
                                
                                console.log(data);
                                
                                closeModalPreloader();
            
                                if (!data.response) {
                                     generateNoty('error', 'Error al intentar sincronizar las (queues).');
                                }else{
                                    table_queuesA.ajax.reload();
                                    table_queuesB.ajax.reload();
                                    
                                    generateNoty('success', 'Sincronización de las (queues) completa.');
                                    
                                    updateCountersTitle();
                                   
                                }
                            },
                            error: function (jqXHR ) {
                                                        
                                if(jqXHR.status == 403){
                                    
                                    generateNoty('warning', 'La sesión ha expirado. Por favor ingrese sus credenciales.');
                                    
                                    setTimeout(function(){ 
                                      window.location.href ="/ispbrain";
                                    }, 3000);
                                    
                                }else{
                                    generateNoty('error', 'Error al intentar sincronizar las (queues).');
                                    closeModalPreloader();
                                    updateCountersTitle();
                                }
                            }
                        });
                    }
                });
            });
            
            $('#btn-refresh-queues').click(function() {

                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
               
                openModalPreloader('Actualizando ... ');
                
                 $.ajax({
                    url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikIpfijaTa', 'action' => 'refreshQueues'])?>" ,
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify({controller_id: controller_id}),
                    success: function(data){
                        
                         closeModalPreloader();
    
                        if (!data.data) {
                            generateNoty('error', 'No se pudo establecer una conexión con el controlador.');
                        }else{
                            
                            generateNoty('success', 'Actualización completa.');
                            
                            table_queuesA.ajax.reload();
                            table_queuesB.ajax.reload();
                        }
                        
                        updateCountersTitle();
                    },
                    error: function (jqXHR) {

                        if (jqXHR.status == 403) {
                                
                        	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                        
                        	setTimeout(function() { 
                        	  window.location.href ="/ispbrain";
                        	}, 3000);
                        
                        } else {
                            closeModalPreloader();
                            generateNoty('error', 'Error al intentar actualizar.');
                            updateCountersTitle();
                        }
                    }
                });
                
            });

            //end queuesA

            //queuesB

            $('#table-queuesB').removeClass('display');

    		table_queuesB = $('#table-queuesB').DataTable({
    		    "order": [[ 1, 'asc' ]],
    		    "autoWidth": true,
    		    "scrollY": '350px',
    		    "scrollCollapse": true,
    		    "scrollX": true,
    		    "ordering" : false,
    		    "paging": false,
    		    "deferRender": true,
    		    "ajax": {
                    "url": "/ispbrain/sync/mikrotik_ipfija_ta/queuesB.json",
                    "dataSrc": "data",
                	"error": function(c) {
                		switch (c.status) {
                			case 400:
                				flag = false;
                				generateNoty('warning', 'Ha ocurrido un error.');
                				break;
                			case 401:
                				flag = false;
                				generateNoty('warning', 'Error, no posee una autorización.');
                				break;
                			case 403:
                				flag = false;
                				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                				setTimeout(function() { 
                				  window.location.href ="/ispbrain";
                				}, 3000);
                				break;
                		}
                	}
                },
                "columns": [
                    { 
                        "data": "api_id",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                             else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "name",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                             else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "target",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                             else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "max_limit",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                             else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "limit_at",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                             else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "burst_limit",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                             else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "burst_threshold",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                             else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "burst_time",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                             else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "priority",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                             else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    { 
                        "data": "queue",
                        "render": function ( data, type, row ) {
                            if(data.state){
                                return data.value;
                            }
                             else if(row.other){
                                return "<span class='text-info'>" + data.value + "</span>";
                            }
                            return "<span class='text-danger'>" + data.value + "</span>";
                        }
                    },
                    
                ],
    		    "columnDefs": [
    		        { "type": 'ip-address', targets: [2] },
                    
                ],
    		    "language": dataTable_lenguage,
                "pagingType": "numbers",
                "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
                "dom":
        	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
            		"<'row'<'col-xl-12'tr>>" +
            		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
    		});
    		
    		$('#table-queuesB_wrapper .title').append('<h5>Controlador</h5>');
    		
    		$('#table-queuesB tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {
                    
                    table_queuesB.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    queueB_selected = table_queuesB.row( this ).data();
                    $('.modal-queueB').modal('show');
                }
            });
            
            $('#btn-delete-queue').click(function() {

                if(!integrationEnabled){
                     generateNoty('warning', 'integración desactivada.');
                     return false;
                }
               
                openModalPreloader('Eliminado Queue del Controlador ... ');
                
                $('.modal-queueB').modal('hide');
                
                 $.ajax({
                    url: "<?=$this->Url->build(['controller' => 'IspControllerMikrotikIpfijaTa', 'action' => 'deleteQueueInController'])?>" ,
                    type: 'POST',
                    dataType: "json",
                    data: JSON.stringify({controller_id: controller_id, queue_api_id: queueB_selected.api_id}),
                    success: function(data){
                        
                        closeModalPreloader();
    
                        if (!data.data) {
                             generateNoty('error', 'Error al intentar eliminar.');
                        }else{
                            
                            generateNoty('success', 'Eliminación completa.');
                            
                            table_queuesA.ajax.reload();
                            table_queuesB.ajax.reload();
                        }
                        
                        updateCountersTitle();
                    },
                    error: function (jqXHR) {

                        if (jqXHR.status == 403) {
                        
                        	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');
                        
                        	setTimeout(function() { 
                        	  window.location.href ="/ispbrain";
                        	}, 3000);
                        
                        } else {
                        	closeModalPreloader();
                            generateNoty('error', 'Error al intentar eliminar.');
                            updateCountersTitle();
                        }
                    }
                });
            });
    		
    		//end queuesB

    		$('a[id="queues-tab"]').on('shown.bs.tab', function (e) {
                table_queuesA.draw();
                table_queuesB.draw();
            });

        });

    </script>
<?php $this->end(); ?>