
<div class="row">

    <div class="col-md-12" id="myTabs">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" id="myTab" role="tablist">

            <li class="nav-item">
                <a class="nav-link active" id="queues-tab" data-target="#queues" role="tab" aria-controls="queues" aria-expanded="true">Queues</a>
            </li>
            
            <li class="nav-item">
                <a class="nav-link " id="address-lists-client-tab" data-target="#address-lists-client" role="tab" aria-controls="address-lists-client" aria-expanded="false">Address List Client</a>
            </li>
            
            <li class="nav-item d-none">
                <a class="nav-link" id="arp-tab" data-target="#arp" role="tab" aria-controls="arp" aria-expanded="true">Arp</a>
            </li>

            <li class="nav-item">
                <a class="nav-link " id="address-lists-tab" data-target="#address-lists" role="tab" aria-controls="address-lists" aria-expanded="false">Address Lists</a>
            </li>
         
             <li class="nav-item d-none">
                <a class="nav-link " id="gateways-tab" data-target="#gateways" role="tab" aria-controls="gateways" aria-expanded="false">Gateways</a>
            </li>
         
        </ul>

        <!-- Tab panes -->
        <div class="tab-content" id="myTabContent">

            <div class="tab-pane fade show active" id="queues" role="tabpanel" aria-labelledby="queues-tab">
                <?= $this->fetch('queues') ?>
            </div>

            <div class="tab-pane fade" id="address-lists-client" role="tabpanel" aria-labelledby="address-lists-client-tab">
                <?= $this->fetch('address-lists-client') ?>
            </div>
            
            <div class="tab-pane fade  d-none" id="arp" role="tabpanel" aria-labelledby="arp-tab">
                <?= $this->fetch('arp') ?>
            </div>
            
            <div class="tab-pane fade" id="address-lists" role="tabpanel" aria-labelledby="address-lists-tab">
                <?= $this->fetch('address-lists') ?>
            </div>
            
            <div class="tab-pane fade d-none" id="gateways" role="tabpanel" aria-labelledby="gateways-tab">
                <?= $this->fetch('gateways') ?>
            </div>

        </div>
    </div>

</div>

<?php
echo $this->element('modal_preloader');
?>
<script type="text/javascript">

    var controller_id = <?php echo json_encode($controller->id); ?>;
    var integrationEnabled = <?php echo json_encode($controller->integration); ?>;

    $(document).ready(function () {

        console.log('integrationEnabled', integrationEnabled);

        $('#myTab a').click(function (e) {

            e.preventDefault();

            var target = $(this).data('target');
            $('#myTab a.active').removeClass('active');

            $('#myTabContent .active').fadeOut(100, function() {
            $('#myTabContent .active').removeClass('active');

                $(target).fadeIn(100, function() {
                    $(target).addClass('active show');
                    $(target+'-tab').addClass('active');
                    $(target+'-tab').trigger('shown.bs.tab');
                });
            });
        });

    });

</script>
