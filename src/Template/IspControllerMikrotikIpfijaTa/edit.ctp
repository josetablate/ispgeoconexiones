<style type="text/css">

    #map {
		height: 370px;
	}

	#floating-panel {
		top: 10px;
		left: 25%;
		z-index: 5;
		background-color: #fff;
		padding: 5px;
		border: 1px solid #999;
		text-align: center;
		font-family: 'Roboto','sans-serif';
		line-height: 30px;
		padding-left: 10px;
	}

	#submit-map {
        padding: 10px;
        top: 2px;
    }

    #update-map {
        padding: 10px;
        top: 2px;
    }

    .slidecontainer {
        width: 100%; /* Width of the outside container */
    }

    /* The slider itself */
    .slider {
        -webkit-appearance: none;  /* Override default CSS styles */
        appearance: none;
        width: 100%; /* Full-width */
        height: 5px; /* Specified height */
        background: #d3d3d3; /* Grey background */
        outline: none; /* Remove outline */
        opacity: 0.7; /* Set transparency (for mouse-over effects on hover) */
        -webkit-transition: .2s; /* 0.2 seconds transition on hover */
        transition: opacity .2s;
        margin-bottom:15px;
    }

    /* Mouse-over effects */
    .slider:hover {
        opacity: 1; /* Fully shown on mouse-over */
    }

    /* The slider handle (use -webkit- (Chrome, Opera, Safari, Edge) and -moz- (Firefox) to override default look) */ 
    .slider::-webkit-slider-thumb {
        -webkit-appearance: none; /* Override default look */
        appearance: none;
        width: 25px; /* Set a specific slider handle width */
        height: 25px; /* Slider handle height */
        background: #4CAF50; /* Green background */
        cursor: pointer; /* Cursor on hover */
        border-radius: 30px;
    }

    .slider::-moz-range-thumb {
        width: 25px; /* Set a specific slider handle width */
        height: 25px; /* Slider handle height */
        background: #4CAF50; /* Green background */
        cursor: pointer; /* Cursor on hover */
        border-radius: 30px;
    }

    .my-hidden {
        display: none;
    }

    .radio-color {
        width: 15% !important;
        height: 35px;
    }

</style>

<div class="row">
     <div class="col-xl-3">
        <div class="pull-left">
            <?php
            
             echo $this->Html->link(
                 
                '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                ['controller' => 'controllers', "action" => "add"],
                [
                    'title' => 'Agregar nuevo Controlador',
                    'class' => 'btn btn-default',
                    'escape' => false
                ]);
                
             echo $this->Html->link(
                 
                '<span class="glyphicon icon-bin" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                    'id' => 'btn-delete-controller',
                    'title' => 'Eliminar Controlador',
                    'class' => 'btn btn-default ml-1',
                    'data-id' => $controller->id,
                    'escape' => false
                ]);
                
             ?>
           
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">

        <?= $this->Form->create($controller) ?>
        <fieldset>
            
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <?php 
                    echo $this->Form->input('name', ['label' => 'Nombre', 'required' => true]);
                    ?>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <?php 
                    echo $this->Form->input('description', ['label' => 'Descripción', 'type' => 'textarea', 'rows' => 2]);
                    ?>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <?php 
                    echo $this->Form->input('template', ['value' => $controller->template_name, 'disabled' => true]);
                    ?>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <?php 
                    echo $this->Form->input('trademark', ['value' => $controller->trademark_name,'label' => 'Marca',  'disabled' => true]);
                    ?>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <?php 
                    echo $this->Form->input('arp', ['label' => 'Control de creación de Arps', 'type' => 'checkbox', 'checked' => $controller->arp]);
                    ?>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <?php 
                    echo $this->Form->input('message_method', ['type' => 'hidden', "value" => 'met_a']);
                    ?>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <?php 
                    echo $this->Form->input('connect_to', ['label' => 'Conectar a']);
                    ?>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <?php 
                    echo $this->Form->input('port', ['label' => 'Puerto API']);
                    ?>
                </div>
                
                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <?php 
                    echo $this->Form->input('port_ssl', ['label' => 'Puerto API SSL']);
                    ?>
                </div>
                
                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <?php 
                    echo $this->Form->input('port_grap', ['label' => 'Puerto Gráficas']);
                    ?>
                </div>
                
                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <?php 
                    echo $this->Form->input('username', ['label' => 'Usuario', 'autocomplete' => 'off']);
                    ?>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <?php 
                    echo $this->Form->input('password', ['label' => 'Contraseña', 'autocomplete' => 'off']);
                    ?>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <?php 
                     echo $this->Form->input('local_address', ['label' => 'Local Address']);
                    ?>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <?php 
                    echo $this->Form->input('dns_server', ['label' => 'Server DNS']);
                    ?>
                </div>
                
                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <?php 
                    echo $this->Form->input('queue_default', [ 'options' => $queue_types_array, 'label' => 'Queue Type']);
                    ?>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <?php 
                    echo $this->Form->input('enabled', ['label' => 'Habilitado']);
                    ?>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <?php 
                    echo $this->Form->input('integration', ['label' => 'Sincronización', "type" => 'checkbox']);
                    ?>
                </div>
                <div class="data-map my-hidden col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <div class="slidecontainer">
                        <input type="range" min="500" max="6000" step="100" value="" class="slider" id="myRange">
                        <p>Radio: <span id="demo"></span></p>
                    </div>
                </div>
                <div class="data-map my-hidden col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <div class="form-inline color">
                        <label class="control-label" for="color">Color</label>
                        <?= $this->Form->number('color', ['id' => 'color', 'label' => 'Color', 'type' => 'color', 'value' => '#000000', 'class' => 'ml-2 radio-color'])?>
                    </div>
                </div>
                
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <?php
                        echo $this->Form->input('lat', ['type' => 'hidden', 'id' => 'coodmapslat']);
                        echo $this->Form->input('lng', ['type' => 'hidden', 'id' => 'coodmapslng']);
                        echo $this->Form->input('radius', ['type' => 'hidden', 'id' => 'radius']);
                    ?>
                	<div id="floating-panel">
            		    <div class="row">
            		        <div class="col-md-12">
                		        <div class="input-group">
                		            <span class="input-group-btn">
                                        <button id="update-map" title="Recargar Mapa" class="btn btn-info" type="button"><span class="text-white glyphicon icon-map2" aria-hidden="true"></span></button>
                                    </span>
                                    <input id="addressmap" type="text" class="form-control margin-top-5" value="" placeholder="Calle #, Ciudad, Provincia">
                                    <span class="input-group-btn">
                                        <button id="submit-map" title="Buscar Dirección" class="btn btn-success" type="button"><span class="text-white glyphicon icon-search" aria-hidden="true"></span></button>
                                    </span>
                                </div>
            		        </div>
            		    </div>
            		</div>
            		<div id="map" class="mb-3"></div>
                </div>
               
            </div>

        </fieldset>
        
        <?= $this->Form->button(__('Guardar'), ['class' => 'mt-2']) ?>
        <?= $this->Form->end() ?>

    </div>
</div>

<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2K3zoK46JybWPzQbhfQQqIIbdZ_3WcQs&libraries=places&callback=initAutocomplete">
</script>

<script type="text/javascript">
    var paraments;
    var controller;

    $(document).ready(function() {
        paraments = <?= json_encode($paraments) ?>;
        controller = <?= json_encode($controller) ?>;
        initMap();
    });
   
    $('#btn-delete-controller').click( function(e) {

        var text = 'esta seguro que desea eliminar el Controllador';
        var id = $(this).data('id');

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/IspControllerMikrotikIpfijaTa/delete/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#replacer').submit();
            }
        });
    });
    
    $('#update-map').click(function() {
        initMap();
    });

    $('form input').on('keypress', function(e) {
        return e.which !== 13;
    });

    var map = null;
    var geocoder = null;
    var marker = null;
    var slider = document.getElementById("myRange");
    var output = document.getElementById("demo");
    output.innerHTML = slider.value;

    slider.oninput = function() {
        output.innerHTML = this.value;
        marker.setRadius(parseInt(this.value));
        $('#radius').val(this.value);
    }

    $("#color").on("change",function() {
        marker.setOptions({
            fillColor: $(this).val(),
            strokeColor: $(this).val()
        });
    });

    function isEmpty(val) {
        return (val === undefined || val == null || val == 0 || val.length <= 0) ? true : false;
    }

	function initMap() {

		geocoder = new google.maps.Geocoder;

		var myLatlng = {lat: paraments.system.map.lat, lng: paraments.system.map.lng};

		var mapOptions = {
			zoom: 14,
			center: myLatlng,
			mapTypeId: 'hybrid'   // roadmap, satellite, hybrid, terrain 
		};

		map = new google.maps.Map(document.getElementById('map'), mapOptions);

		document.getElementById('submit-map').addEventListener('click', function() {
			geocodeAddress(geocoder, map);
		});

	    var init = myLatlng;

        var haveMarker = false;
        if (!isEmpty(controller.lat) || !isEmpty(controller.lng)) {
            haveMarker = true;
            init.lat = controller.lat;
            init.lng = controller.lng;
        }

        var latLng = new google.maps.LatLng(init.lat, init.lng);
        if (haveMarker) {
            console.log(controller.color);
            addMarker(latLng, controller.radius, controller.color);
        }

        map.setCenter(latLng);

		map.addListener('click', function(event) {
			addMarker(event.latLng, false, false);	    
		});
    }

    function refreshMap() {
        google.maps.event.trigger(map, 'resize');
    }

    $('input#addressmap').keypress(function(event) {
        if ( event.which == 13 ) {
            geocodeAddress(geocoder, map);
        }
    });

	function addMarker(location, radius, color) {

		if (marker != null) {
			clearMarkers();
		} else {
		    $('.data-map').removeClass('my-hidden');
		}

		if (radius) {
            $('#myRange').val(radius);
    		$('#radius').val(radius);
    		output.innerHTML = radius;
        } else {
            $('#myRange').val(500);
    		$('#radius').val(500);
    		output.innerHTML = 500;
    		radius = 500;
        }

        if (color) {
             $("#color").val(color);
        } else {
            color = '#000000'
        }

		marker = new google.maps.Circle({
            strokeColor: color,
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: color,
            fillOpacity: 0.35,
            map: map,
            center: location,
            radius: radius
        });

        var lat = location.lat();
        var lng = location.lng();

		$('#coodmapslat').val(lat);
		$('#coodmapslng').val(lng);	
	}

	function clearMarkers() {
		marker.setMap(null);
	}			

	function geocodeAddress(geocoder, map) {

		var address = document.getElementById('addressmap').value;

		geocoder.geocode({
			'address': address
			}, function(results, status) {

				if (status === google.maps.GeocoderStatus.OK) {	
					addMarker(results[0].geometry.location)  	    
					map.setCenter(results[0].geometry.location);
					map.setZoom(14);

    			} else {
    				if (status == google.maps.GeocoderStatus.ZERO_RESULTS) {
    			        bootbox.alert('No se encontraron Resultados');
    			    } else {
    			        window.alert('No se encontraron Resultados. Estados: ' + status);
    			    }
    			}
		});
	}

	// Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>

<script type='text/javascript'>

    function initialize() {

        var input = document.getElementById('addressmap');

        var autocomplete = new google.maps.places.Autocomplete(input);
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>
