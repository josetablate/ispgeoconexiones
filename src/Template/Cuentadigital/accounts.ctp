<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5" >

        <?php

            echo $this->Html->link(
                '<span class="glyphicon fas fa-search" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Buscar por Columnas',
                'class' => 'btn btn-default btn-search-column ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export ml-1',
                'escape' => false
            ]);

        ?>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered" id="table-accounts">
            <thead>
                <tr>
                    <th></th>           <!--0-->
                    <th>Creado</th>     <!--1-->
                    <th>Cuenta</th>     <!--2-->
                    <th>Cód. Barra</th> <!--3-->
                    <th>Cliente</th>    <!--4-->
                    <th>Doc.</th>       <!--5-->
                    <th>Cód.</th>       <!--6-->
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-go-customer',
        'name' =>  'Ficha del Cliente',
        'icon' =>  'glyphicon icon-profile',
        'type' =>  'btn-info'
    ];

    $buttons[] = [
        'id'   =>  'btn-disabled',
        'name' =>  'Deshabilitar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-accounts', 'title' => 'Acciones', 'buttons' => $buttons ]);
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-accounts']);
    echo $this->element('modal_preloader');
?>

<script type="text/javascript">

    var table_accounts = null;
    var account_selected = null;
    var curr_pos_scroll = null;

    $(document).ready(function () {

        let cuenta_digital_credentials = <?= json_encode($cuenta_digital_credentials) ?>;

        $('.btn-search-column').click(function() {
            $('.modal-search-columns-accounts').modal('show');
        });

        $('.btn-hide-column').click(function() {
            $('.modal-hide-columns-customers-deleted').modal('show');
        });

        $('#table-accounts').removeClass('display');

        $('#btns-tools').hide();

        $('#table-accounts').removeClass('display');

    	table_accounts = $('#table-accounts').DataTable({
    	    "order": [[ 1, 'desc' ]],
            "deferRender": true,
            "processing": true,
            "serverSide": true,
		    "ajax": {
                "url": "/ispbrain/Cuentadigital/get_accounts.json",
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "deleted": 0,
                        "payment_getway_id": 105
                    });
                },
                "error": function(c) {
                    switch (c.status) {
                        case 400:
                            flag = false;
                            generateNoty('warning', 'Ha ocurrido un error.');
                            break;
                        case 401:
                            flag = false;
                            generateNoty('warning', 'Error, no posee una autorización.');
                            break;
                        case 403:
                            flag = false;
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                        	setTimeout(function() { 
                                window.location.href = "/ispbrain";
                        	}, 3000);
                            break;
                    }
                }
            },
            "drawCallback": function( c ) {
                var flag = true;
            	switch (c.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
	            if (flag) {
                    if (table_accounts) {

                        var tableinfo = table_accounts.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }
                    }
            	}
            },
            "scrollY": true,
		    "scrollY": '430px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
            "columns": [
                { 
                    "data": "id",
                    "orderable": false,
                    "render": function ( data, type, row ) {
                        return "";
                    }
                },
                { 
                    "data": "created",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                {
                    "data": "account_number",
                    "type": "options",
                    "options": cuenta_digital_credentials,
                    "render": function ( data, type, row ) {
                        if (data) {
                            return cuenta_digital_credentials[data];
                        }
                        return "";
                    }
                },
                { 
                    "data": "barcode",
                    "type": "string"
                },
                { 
                    "data": "customer.name",
                    "type": "string"
                },
                { 
                    "data": "customer",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (data.ident) {
                            ident = data.ident;
                        }
                        return sessionPHP.afip_codes.doc_types[data.doc_type] + ' ' + ident;
                    }
                },
                { 
                    "data": "customer.code",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
            ],
    	    "columnDefs": [
    	        { "type": 'date-custom', targets: [1] },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
    	    "language": dataTable_lenguage,
    	    "pagingType": "numbers",
            "lengthMenu": [[ 50, 100, -1], [ 50, 100, "Todas"]],
            "dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
    	});

    	$('#table-accounts_wrapper .tools').append($('#btns-tools').contents());

    	$('#table-accounts').on( 'init.dt', function () {
            createModalSearchColumn(table_accounts, '.modal-search-columns-accounts');
        });

        $('#btns-tools').show();

        $('#table-accounts tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                table_accounts.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
                account_selected = table_accounts.row( this ).data();

                $('.modal-accounts').modal('show');
            }
        });

        $(".btn-export").click(function() {

            bootbox.confirm('Se descargará un archivo Excel', function(result) {
                if (result) {
                    $('#table-accounts').tableExport({tableName: 'Cuentas', type:'excel', escape:'false'});
                }
            });
        });

        $('#btn-go-customer').click(function() {
            var action = '/ispbrain/customers/view/' + account_selected.customer.code;
            window.open(action, '_blank');
        });

        $('#btn-disabled').click(function() {

            var text = "¿Está Seguro que desea deshabilitar la Cuenta?";
            var id  = account_selected.id;

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/Cuentadigital/disabled/' + id, 'method': 'post', 'id': 'replacer'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer').submit();
                }
            });
        });

        // Jquery draggable modals
        $('.modal-dialog').draggable({
            handle: ".modal-header"
        });

    });

</script>
