<style type="text/css">

    tr.title {
        color: #696868 !important;
        font-weight: bold;
    }

    .btn-small {
        padding: 2px;
        margin: 5px;
    }

    .contain-block {
        background-color: #ececec;
        border: 1px solid #c3c2c2;
        border-radius: 5px;
        margin-left: 15px;
        padding-top: 5px;
    }

    .form-contain {
        border: 1px solid #d2cfcf;
        background: #f0f0f1;
        margin-left: 15px;
        padding: 15px;
        border-radius: 5px;
    }

</style>

<?php
    $accountsArray = [];
    $accountsArray[''] = 'Raíz';
    foreach ($accounts as $a) {
        $accountsArray[$a->code] = $a->code . ' ' . $a->name;
    }
?>

<div id="btns-tools">
    <div class="pull-right btns-tools margin-bottom-5" >

        <?php

            echo $this->Html->link(
                '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Agregar Cuenta',
                'class' => 'btn btn-default btn-add-credential ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export ml-1',
                'escape' => false
            ]);
        ?>

    </div>
</div>

<div class="row d-block">

    <div class="col-md-10 ml-3">
        <legend class="sub-title">Cuentas de cuenta digital</legend>
        <table class="table table-hover table-bordered" id="table-credentials">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Control</th>
                    <th>Nro de Cuenta</th>
                    <th>Habilitado</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <div class="col-md-10">
        <legend class="sub-title ml-3">Configuración</legend>
        <?= $this->Form->create($paraments, ['enctype' => 'multipart/form-data', 'class' => ['form-load', 'ml-3', 'form-contain']]) ?>
            <fieldset>

                <br>
                <div class="row">

                    <div class="col-xl-3 contain-block">
                        <label class="control-label">Visualización</label>
                        <?= $this->Form->input('enabled', ['label' => 'Habilitado', 'type' => 'checkbox', 'checked' => $payment_getway->config->cuentadigital->enabled ]); ?>
                        <?= $this->Form->input('cash', ['label' => 'Ver Cobranza', 'type' => 'checkbox', 'checked' => $payment_getway->config->cuentadigital->cash]); ?>
                        <?= $this->Form->input('portal', ['label' => 'Ver Portal', 'type' => 'checkbox', 'checked' => $payment_getway->config->cuentadigital->portal]); ?>
                    </div>

                    <div class="col-md-4 contain-block">
                        <div class="form-group required">
                            <?php if ($user_id == 100): ?>  
                            <label class="control-label" for="hours-execution">Ejecución para Consultar Transacciones</label>
                            <div class='input-group date' id='hours-execution-datetimepicker'>
                                <span class="input-group-addon input-group-text mr-0">Horario</span>
                                <input  name="hours_execution" required="required" id="hours-execution"  type='text' class="form-control" />
                                <span class="input-group-addon input-group-text mr-0">
                                    <span class="glyphicon icon-calendar"></span>
                                </span>
                            </div>
                            <?php endif; ?>
                            <div class="form-group required mt-3">
                                <label class="control-label" for="hours-execution">Ejecutar forazadamente</label>

                                <div class='input-group date' id='date-execution-datetimepicker'>
                                    <span class="input-group-addon input-group-text mr-0">Fecha</span>
                                    <input  name="date_execution" required="required" id="date-execution"  type='text' class="form-control" />
                                    <span class="input-group-addon input-group-text mr-0">
                                        <span class="glyphicon icon-calendar"></span>
                                    </span>
                                </div>

                            </div>
                            <button type="button" id="force-execution" class="btn btn-dark mb-3">Forzar ejecución</button>
                            <button type="button" id="clean-transactions" class="btn btn-danger mb-3">Borrar transacción sin asignación</button>
                        </div>
                        <!--<?= $this->Form->input('automatic', ['label' => 'Automático', 'type' => 'checkbox', 'checked' => $payment_getway->config->cuentadigital->automatic]); ?>-->
                    </div>

                    <?php if ($account_enabled): ?>
                        <div class="col-xl-4 contain-block">

                            <label for="">Cuenta contable</label>
                            <table>
                                <tr>
                                    <td style="width:400px">
                                        <input type="hidden" name="account" id="account_code" value="<?= $payment_getway->config->cuentadigital->account ?>" >
                                        <input type="text" id="account_code-show" class="form-control" readonly value="<?= $accountsArray[$payment_getway->config->cuentadigital->account] ?>" placeholder="Cuenta">
                                    </td>
                                    <td> 
                                        <a class="btn btn-default btn-search-account btn-small" href="#" data-input="account_code" role="button">
                                            <span class="glyphicon icon-search" aria-hidden="true"></span>
                                        </a>
                                    </td>
                                    <td> 
                                        <a class="btn btn-default btn-bin-account btn-small" href="#" data-input="account_code" role="button">
                                            <span class="glyphicon icon-bin" aria-hidden="true"></span>
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    <?php endif; ?>
        
                </div>
            </fieldset>
            <br>
            <?= $this->Html->link(__('Cancelar'),["action" => "index"], ['class' => 'btn btn-default']) ?>
            <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success' ]) ?>
        <?= $this->Form->end() ?>
    </div>

</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-edit',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-default'
    ];

    $buttons[] = [
        'id'   =>  'test-config',
        'name' =>  'Probar configuración',
        'icon' =>  'fas fa-check',
        'type' =>  'btn-dark'
    ];

    echo $this->element('actions', ['modal'=> 'modal-credentials', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<div class="modal fade" id="modal-accounts" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Seleccionar Cuenta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">

                        <table class="table table-hover" id="table-accounts">
                            <thead>
                                <tr>
                                    <th>Cód.</th>
                                    <th>Nombre</th>
                                    <th>Descripción</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                foreach ($accounts as $acc): ?>
                                <tr class="<?= ($acc->title) ? 'title' : '' ?> <?= (!$acc->status) ? ' font-through' : '' ?>"
                                    data-code="<?= $acc->code ?>"
                                    data-name="<?= $acc->name ?>">

                                    <td class="left"><?= $acc->code ?></td>
                                    <td class="left"><?php

                                        $hrchy = substr(strval($acc->code), 0, 4);
                                        $hrchy = str_split($hrchy);
                                        foreach ($hrchy as $h) {
                                            if ($h != '0') {
                                                echo '&nbsp;&nbsp;&nbsp;'; 
                                            }
                                        }
                                        if (!$acc->title) {
                                            echo '&nbsp;&nbsp;&nbsp;'; 
                                        }
                                        echo $acc->name; 
                                    ?>
                                    </td>
                                    <td class="left"><?= $acc->description ?></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <br>

                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" id="btn-account-selected" class="btn btn-success">Seleccionar</button> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="label-modal-add">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Agregar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-md-12">

                        <form method="post" accept-charset="utf-8" role="form" action="<?= $this->Url->build(["controller" => "Cuentadigital", "action" => "addCredential"]) ?>">
                            <div style="display:none;">
                                <input type="hidden" name="_method" value="POST">
                            </div>

                            <?php
                                echo $this->Form->input('name', ['id' => 'input-name-credential-add', 'label' => 'Nombre']);
                                echo $this->Form->input('control_number', ['id' => 'input-control_number-credential-add', 'label' => 'Control']);
                                echo $this->Form->input('account_number', ['id' => 'input-account_number-credential-add', 'label' => 'Nro de Cuenta']);
                                echo $this->Form->input('enabled', ['id' => 'input-enabled-credential-add', 'label' => 'Habilitado', 'type' => 'checkbox', 'checked' => TRUE]);
                                echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]);
                            ?>

                            <input type="hidden" name="id" id="id"/>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-success" id="btn-confirm-edit">Agregar</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Editar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-md-12">

                        <form method="post" accept-charset="utf-8" role="form" action="<?= $this->Url->build(["controller" => "Cuentadigital", "action" => "editCredential"]) ?>">
                            <div style="display:none;">
                                <input type="hidden" name="_method" value="POST">
                            </div>

                            <?php
                                echo $this->Form->hidden('id', ['value' => '']);
                                echo $this->Form->input('name', ['id' => 'input-name-credential-edit', 'label' => 'Nombre']);
                                echo $this->Form->input('control_number', ['id' => 'input-control_number-credential-edit', 'label' => 'Control']);
                                echo $this->Form->input('account_number', ['id' => 'input-account_number-credential-edit', 'label' => 'Nro de Cuenta']);
                                echo $this->Form->input('enabled', ['id' => 'input-enabled-credential-edit', 'label' => 'Habilitado', 'type' => 'checkbox', 'checked' => true]);
                                echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]);
                            ?>

                            <input type="hidden" name="id" id="id"/>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-success" id="btn-confirm-edit">Guardar</button> 

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var table_acccounts = null;
    var paraments;
    var payment_getway;
    var table_credentials = null;
    var credential_selected = null;

    $(document).ready(function () {

        paraments = <?= json_encode($paraments) ?>;
        payment_getway = <?= json_encode($payment_getway) ?>;
        var year = new Date().getFullYear();
        var defaultDateCD = year + "-10-01T" + payment_getway.config.cuentadigital.hours_execution;
        $('#hours-execution-datetimepicker').datetimepicker({
            locale: 'es',
            defaultDate: defaultDateCD,
            format: 'HH:mm',
        });

        $('#date-execution-datetimepicker').datetimepicker({
            locale: 'es',
            defaultDate: new Date(),
            format: 'DD/MM/YYYY'
        });

        $('#table-credentials').removeClass('display');

        $('#btns-tools').hide();

		table_credentials = $('#table-credentials').DataTable({
		    "autoWidth": true,
		    "scrollY": '350px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "sWrapper":  "dataTables_wrapper dt-bootstrap4",
            "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/Cuentadigital/get_credentials.json",
                "dataSrc": "credentials",
                "error": function(c) {
                    switch (c.status) {
                        case 400:
                            flag = false;
                            generateNoty('warning', 'Ha ocurrido un error.');
                            break;
                        case 401:
                            flag = false;
                            generateNoty('warning', 'Error, no posee una autorización.');
                            break;
                        case 403:
                            flag = false;
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                        	setTimeout(function() { 
                                window.location.href = "/ispbrain";
                        	}, 3000);
                            break;
                    }
                }
            },
            "columns": [
                { "data": "name" },
                { "data": "control_number" },
                { "data": "account_number"},
                { 
                    "data": "enabled",
                    "render": function ( data, type, row ) {
                        var enabled = '<i class="fa fa-times red" aria-hidden="true"></i>';
                        if (data) {
                            enabled = '<i class="fa fa-check green" aria-hidden="true"></i>';
                        }
                        return enabled;
                    }
                },
            ],
            "columnDefs": [
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
		    "language":  dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            "dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
		});

		$('#table-credentials_wrapper .tools').append($('#btns-tools').contents());

        $(".btn-export").click(function() {
            bootbox.confirm('Se descargará un archivo Excel', function(result) {
                if (result) {
                    $('#table-credentials').tableExport({tableName: 'Credenciales', type:'excel', escape:'false'});
                }
            });
        });

        $('#btns-tools').show();

        $('#table-credentials tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                table_credentials.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
                credential_selected = table_credentials.row( this ).data();

                $('.modal-credentials').modal('show');
            }
        });

        $('.btn-add-credential').click(function() {
            $('#input-name-credential-add').val("");
            $('#input-control_number-credential-add').val("");
            $('#input-account_number-credential-add').val("");
            $('#input-enabled-credential-add').prop('checked', true);
            $('#modal-add').modal('show');
        });

        $('a#btn-edit').click(function() {

            $('.modal-credentials').modal('hide');

            $('input[name="id"]').val(credential_selected.id);
            $('#input-name-credential-edit').val(credential_selected.name);
            $('#input-control_number-credential-edit').val(credential_selected.control_number);
            $('#input-account_number-credential-edit').val(credential_selected.account_number);
            $('#input-enabled-credential-edit').prop('checked', credential_selected.enabled);

            $('#modal-edit').modal('show');
        });

        $('#table-accounts').removeClass('display');

		table_acccounts = $('#table-accounts').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '480px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
		});

		$('#table-accounts_filter').closest('div').before($('#btns-tools').contents());   

		var input_select = null;

		$('.btn-search-account').click(function() {

		    input_select = $(this).data('input');
		    $('#modal-accounts').modal('show');
		});

		$('.btn-bin-account').click(function(){

		    var input_select_temp = $(this).data('input');
		    $('#' + input_select_temp).val('');
		    $('#' + input_select_temp + '-show').val('');
		});

		$('#btn-account-selected').click(function() {

		    var code = table_acccounts.$('tr.selected').data('code');
		    var name = table_acccounts.$('tr.selected').data('name');

		    $('#' + input_select).val(code);
		    $('#' + input_select + '-show').val(code + ' ' + name);
		    $('#account_name').val(name);

		    $('#modal-accounts').modal('hide');
		});

		$('#modal-accounts').on('shown.bs.modal', function (e) {
		    table_acccounts.draw();
        });

        $('#table-accounts tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                table_acccounts.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });

        $('#test-config').click(function() {

            var request = $.ajax({
                url: "/ispbrain/Cuentadigital/test.json",
                method: "POST",
                data: JSON.stringify({
                    id: credential_selected.id
                }),
                dataType: "json"
            });

            request.done(function(response) {
                generateNoty('success', response.result);
            });

            request.fail(function(jqXHR) {

                if (jqXHR.status == 403) {

                    generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    setTimeout(function() {
                        window.location.href = "/ispbrain";
                    }, 3000);

                } else {
                    generateNoty('error', 'Error al intentar obtener los totales');
                }
            });
        });

        $('#force-execution').click(function() {

            var text = "¿Está Seguro que desea ejecutar?";
            var date =  $("#date-execution-datetimepicker").find("input").val();

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/Cuentadigital/forceExecution/', 'method': 'post', 'id': 'replacer'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'date', 'value': date}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer').submit();
                }
            });
        });

        $('#clean-transactions').click(function() {

            var text = "¿Está Seguro que desea borrar las transacciones con estado: sin asignar?";

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/Cuentadigital/cleanTransactions/', 'method': 'post', 'id': 'replacer'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer').submit();
                }
            });
        });
    });

</script>
