<style type="text/css">

     tr.title {
        color: #696868 !important;
        font-weight: bold;
    }

    .btn-small {
        padding: 2px;
        margin: 5px;
    }

    .contain-block {
        background-color: #ececec;
        border: 1px solid #c3c2c2;
        border-radius: 5px;
        margin-left: 15px;
        padding-top: 5px;
    }

    .form-contain {
        border: 1px solid #d2cfcf;
        background: #f0f0f1;
        margin-left: 15px;
        padding: 15px;
        border-radius: 5px;
    }

</style>

<?php
    $accountsArray = [];
    $accountsArray[''] = 'Raíz';
    foreach ($accounts as $a) {
        $accountsArray[$a->code] = $a->code . ' ' . $a->name;
    }
?>

<div class="row d-block">
    
    <div class="col-xl-12">
        <legend class="sub-title">Configuración</legend>
    </div>

    <?= $this->Form->create($paraments, ['enctype' => 'multipart/form-data',  'class' => ['form-load', 'ml-3', 'form-contain']]) ?>
        <fieldset>

            <br>
            <div class="row">

                <div class="col-xl-3 contain-block">
                    <label class="control-label">Visualización</label>
                    <?= $this->Form->input('enabled', ['label' => 'Habilitado', 'type' => 'checkbox', 'checked' => $payment_getway->config->pim->enabled ]); ?>
                    <?= $this->Form->input('cash', ['label' => 'Ver Cobranza', 'type' => 'checkbox', 'checked' => $payment_getway->config->pim->cash]); ?>
                    <?= $this->Form->input('portal', ['label' => 'Ver Portal', 'type' => 'checkbox', 'checked' => $payment_getway->config->pim->portal]); ?>
                </div>

                <?php if ($account_enabled): ?>
                    <div class="col-xl-4 contain-block">

                        <label for="">Cuenta contable</label>
                        <table>
                             <tr>
                                <td style="width:400px">
                                    <input type="hidden" name="account" id="account_code" value="<?= $payment_getway->config->pim->account ?>" >
                                    <input type="text" id="account_code-show" class="form-control" readonly value="<?= $accountsArray[$payment_getway->config->pim->account] ?>" placeholder="Cuenta">
                                </td>
                                <td> 
                                    <a class="btn btn-default btn-search-account btn-small" href="#" data-input="account_code" role="button">
                                        <span class="glyphicon icon-search" aria-hidden="true"></span>
                                    </a>
                                </td>
                                <td> 
                                    <a class="btn btn-default btn-bin-account btn-small" href="#" data-input="account_code" role="button">
                                        <span class="glyphicon icon-bin" aria-hidden="true"></span>
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                <?php endif; ?>

            </div>
        </fieldset>
        <br>
        <?= $this->Html->link(__('Cancelar'),["action" => "index"], ['class' => 'btn btn-default']) ?>
        <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success' ]) ?>
    <?= $this->Form->end() ?>

</div>

<div class="modal fade" id="modal-accounts" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Seleccionar Cuenta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">

                        <table class="table table-hover" id="table-accounts">
                            <thead>
                                <tr>
                                    <th>Cód.</th>
                                    <th>Nombre</th>
                                    <th>Descripción</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                foreach ($accounts as $acc): ?>
                                <tr class="<?= ($acc->title) ? 'title' : '' ?> <?= (!$acc->status) ? ' font-through' : '' ?>"
                                    data-code="<?= $acc->code ?>"
                                    data-name="<?= $acc->name ?>">

                                    <td class="left"><?= $acc->code ?></td>
                                    <td class="left"><?php

                                        $hrchy = substr(strval($acc->code), 0, 4);
                                        $hrchy = str_split($hrchy);
                                        foreach ($hrchy as $h) {
                                            if ($h != '0') {
                                                echo '&nbsp;&nbsp;&nbsp;'; 
                                            }
                                        }
                                        if (!$acc->title) {
                                            echo '&nbsp;&nbsp;&nbsp;'; 
                                        }
                                        echo $acc->name; 
                                    ?>
                                    </td>
                                    <td class="left"><?= $acc->description ?></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <br>

                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" id="btn-account-selected" class="btn btn-success">Seleccionar</button> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    
    var table_acccounts = null;

    $(document).ready(function () {

        $('#table-accounts').removeClass('display');

		table_acccounts = $('#table-accounts').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '480px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
		});

		$('#table-accounts_filter').closest('div').before($('#btns-tools').contents());   

		var input_select = null;

		$('.btn-search-account').click(function() {

		    input_select = $(this).data('input');
		    $('#modal-accounts').modal('show');
		});

		$('.btn-bin-account').click(function(){

		    var input_select_temp = $(this).data('input');
		    $('#' + input_select_temp).val('');
		    $('#' + input_select_temp + '-show').val('');
		});

		$('#btn-account-selected').click(function() {

		    var code = table_acccounts.$('tr.selected').data('code');
		    var name = table_acccounts.$('tr.selected').data('name');

		    $('#' + input_select).val(code);
		    $('#' + input_select + '-show').val(code + ' ' + name);
		    $('#account_name').val(name);

		    $('#modal-accounts').modal('hide');
		});

		$('#modal-accounts').on('shown.bs.modal', function (e) {
		    table_acccounts.draw();
        });

        $('#table-accounts tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                table_acccounts.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });
    });

</script>
