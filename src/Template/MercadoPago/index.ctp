<style type="text/css">

    .my-hidden {
        display: none;
    }

    tr.title {
        color: #696868 !important;
        font-weight: bold;
    }

    .btn-small {
        padding: 2px;
        margin: 5px;
    }

    .contain-block {
        background-color: #ececec;
        border: 1px solid #c3c2c2;
        border-radius: 5px;
        margin-left: 15px;
        padding-top: 5px;
    }

    .lbl-legend {
        font-size: 20px;
        font-family: sans-serif;
    }

    .form-contain {
        border: 1px solid #d2cfcf;
        background: #f0f0f1;
        padding: 15px;
        border-radius: 5px;
    }

</style>

<?php
    $accountsArray = [];
    $accountsArray[''] = 'Raiz';
    foreach ($accounts as $a) {
        $accountsArray[$a->code] = $a->code . ' ' . $a->name;
    }
?>

<?= $this->Form->create($payment_getway,  ['class' => ['form-load', 'ml-3', 'form-contain']]) ?>
    <fieldset>
        <legend class="sub-title">Configuración</legend>

        <?php
            $sandbox = $payment_getway->config->mercadopago->mode == 'sandbox' ? '' : 'my-hidden';
            $prod = $payment_getway->config->mercadopago->mode == 'prod' ? '' : 'my-hidden';
        ?>

        <div class="row d-block">

            <div class="col-md-3 contain-block float-left mt-3">
                <legend class="lbl-legend">Visualización</legend>
                <?= $this->Form->input('enabled', ['label' => 'Habilitar', 'type' => 'checkbox', 'checked' => $payment_getway->config->mercadopago->enabled]) ?>
                <?= $this->Form->input('cash', ['label' => 'Ver Cobranza', 'type' => 'checkbox', 'checked' => $payment_getway->config->mercadopago->cash]) ?>
                <?= $this->Form->input('portal', ['label' => 'Ver Portal', 'type' => 'checkbox', 'checked' => $payment_getway->config->mercadopago->portal]) ?>
            </div>

            <div class="col-md-3 contain-block float-left mt-3">

                <div class="col-md-12">
                    <legend class="lbl-legend">Cuenta Mercadopago</legend>
                </div>

                <div class="col-md-12">
                    <?= $this->Form->input('payment_getway.mercadopago.mode', ['options' => ['prod' => 'Producción', 'sandbox' => 'Sandbox (prueba)' ], 'value' => $payment_getway->config->mercadopago->mode, 'label' => 'Modo']); ?>
                </div>

                <div class="col-md-12">
                    <?= $this->Form->input('CLIENT_ID', ['label' => 'CLIENT_ID', 'type' => 'text', 'value' => $payment_getway->config->mercadopago->CLIENT_ID]) ?>
                </div>

                <div class="col-md-12">
                    <?= $this->Form->input('CLIENT_SECRET', ['label' => 'CLIENT_SECRET', 'type' => 'text', 'value' => $payment_getway->config->mercadopago->CLIENT_SECRET]) ?>
                </div>

                <div class="col-md-12">
                    <div id="sandbox-public-key-group" class="form-group text <?= $sandbox ?>">
                        <label class="control-label" for="sandbox-public-key">Public Key</label>
                        <input type="text" name="sandbox-public_key" class="form-control" id="sandbox-public-public_key" value="<?= $payment_getway->config->mercadopago->sandbox->public_key ?>">
                    </div>
                    <div id="prod-public-key-group" class="form-group text <?= $prod ?>">
                        <label class="control-label" for="prod-public-key">Public Key</label>
                        <input type="text" name="prod-public_key" class="form-control" id="prod-public-key" value="<?= $payment_getway->config->mercadopago->prod->public_key ?>">
                    </div>
                </div>

                <div class="col-md-12">

                    <div id="sandbox-access-token-group" class="form-group text <?= $sandbox ?>">
                        <label class="control-label" for="sandbox-access-token">Access Token</label>
                        <input type="text" name="sandbox-access_token" class="form-control" id="sandbox-access-token" value="<?= $payment_getway->config->mercadopago->sandbox->access_token ?>">
                    </div>

                    <div id="prod-access-token-group" class="form-group text <?= $prod ?>">
                        <label class="control-label" for="prod-access-token">Access Token</label>
                        <input type="text" name="prod-access_token" class="form-control" id="prod-access-token" value="<?= $payment_getway->config->mercadopago->prod->access_token ?>">
                    </div>

                </div>

                <div class="col-md-12">
                    <button type="button" id="test-config" class="btn btn-dark mb-3">Probar configuración</button>
                </div>

            </div>

            <?php if ($account_enabled): ?>
                <div class="col-md-3 contain-block float-left mt-3">
                    <legend class="lbl-legend">Cuenta contable</legend>
                    <table class="mb-3">
                         <tr>
                            <td style="width:400px">
                                <input type="hidden" name="account" id="account_code" value="<?= $payment_getway->config->mercadopago->account ?>" >
                                <input type="text" id="account_code-show" class="form-control" readonly value="<?= $accountsArray[$payment_getway->config->mercadopago->account] ?>" placeholder="Cuenta">
                            
                            </td>
                            <td> 
                                <a class="btn btn-default btn-search-account btn-small" href="#" data-input="account_code" role="button">
                                    <span class="glyphicon icon-search" aria-hidden="true"></span>
                                </a>
                            </td>
                            <td> 
                                <a class="btn btn-default btn-bin-account btn-small" href="#" data-input="account_code" role="button">
                                    <span class="glyphicon icon-bin" aria-hidden="true"></span>
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
            <?php endif; ?>

            <div class="col-md-3 contain-block float-left mt-3">

                <div class="form-group required">
                    <label class="control-label" for="hours-execution">Ejecución para Consultar Pagos</label>
                    <div class='input-group date' id='hours-execution-datetimepicker'>
                        <span class="input-group-addon input-group-text mr-0">Horario</span>
                        <input  name="hours_execution" required="required" id="hours-execution"  type='text' class="form-control" />
                        <span class="input-group-addon input-group-text mr-0">
                            <span class="glyphicon icon-calendar"></span>
                        </span>
                    </div>
                </div>
                <button type="button" id="force-execution" class="btn btn-dark mb-3">Forzar ejecución</button>
                <button type="button" id="clean-transactions" class="btn btn-danger mb-3">Borrar transacción sin asignación</button>
                <!--<?= $this->Form->input('automatic', ['label' => 'Automático', 'type' => 'checkbox', 'checked' => $payment_getway->config->mercadopago->automatic]); ?>-->

            </div>

            <div class="col-md-6 contain-block float-left mt-3">

                <div class="form-group required">
                    <label class="control-label">Excluir Métodos de Pagos</label>
                    <div class="row">
                        <?php foreach ($payment_getway->config->mercadopago->payment_methods as $key => $payment_method): ?>
                            <div class="col-6">
                                <?php
                                    $checked = in_array($key, $payment_getway->config->mercadopago->excluded_payment_methods) ? 'checked' : '';
                                ?>
                                <?= $this->Form->input('payment_getway.mercadopago.excluded_payment_methods.' . $key, ['label' => $payment_method, 'type' => 'checkbox', 'checked' => $checked]); ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>

            </div>

            <div class="col-md-3 contain-block float-left mt-3">

                <div class="form-group required">
                    <label class="control-label">Excluir Tipos de de Pagos</label>
                    <div class="row">
                        
                        <?php foreach ($payment_getway->config->mercadopago->payment_types as $key => $payment_type): ?>
                            <div class="col-12">
                            <?php
                                $checked = in_array($key, $payment_getway->config->mercadopago->excluded_payment_types) ? 'checked' : '';
                            ?>
                            <?= $this->Form->input('payment_getway.mercadopago.excluded_payment_types.' . $key, ['label' => $payment_type, 'type' => 'checkbox', 'checked' => $checked]); ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>

            </div>

            <div class="col-md-3 contain-block float-left mt-3">

                <div class="form-group required">
                    <label class="control-label">Cantidad de Cuotas permitidas</label>
                    <div class="col-md-12">
                        <?= $this->Form->input('payment_getway.mercadopago.installments', [
                            'options' => [
                                '1' => '1', 
                                '2' => '2',
                                '3' => '3',
                                '4' => '4',
                                '5' => '5',
                                '6' => '6',
                                '7' => '7',
                                '8' => '8',
                                '9' => '9',
                                '10' => '10',
                                '11' => '11',
                                '12' => '12',
                                '13' => '13',
                                '14' => '14',
                                '15' => '15',
                                '16' => '16',
                                '17' => '17',
                                '18' => '18',
                                '19' => '19',
                                '20' => '20',
                                '21' => '21',
                                '22' => '22',
                                '23' => '23',
                                '24' => '24'
                            ], 'value' => $payment_getway->config->mercadopago->installments, 'label' => 'Cuotas'
                            ]); 
                        ?>
                    </div>
                </div>

            </div>

        </div>
    </fieldset>
    <?= $this->Html->link(__('Cancelar'), ["controller" => "PaymentMethods", "action" => "index"], ['class' => 'btn btn-default mt-3']) ?>
    <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success mt-3' ]) ?>
<?= $this->Form->end() ?>

<div class="modal fade" id="modal-accounts" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Seleccionar Cuenta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">

                        <table class="table table-hover" id="table-accounts">
                            <thead>
                                <tr>
                                    <th>Cód.</th>
                                    <th>Nombre</th>
                                    <th>Descripción</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                foreach ($accounts as $acc): ?>
                                <tr class="<?= ($acc->title) ? 'title' : '' ?> <?= (!$acc->status) ? ' font-through' : '' ?>"
                                    data-code="<?= $acc->code ?>"
                                    data-name="<?= $acc->name ?>">

                                    <td class="left"><?= $acc->code ?></td>
                                    <td class="left"><?php
    
                                        $hrchy = substr(strval($acc->code), 0, 4);
                                        $hrchy = str_split($hrchy);
                                        foreach ($hrchy as $h) {
                                            if ($h != '0') {
                                                echo '&nbsp;&nbsp;&nbsp;'; 
                                            }
                                        }
                                        if (!$acc->title) {
                                            echo '&nbsp;&nbsp;&nbsp;'; 
                                        }
                                        echo $acc->name; 
                                    ?>
                                    </td>
                                    <td class="left"><?= $acc->description ?></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <br>

                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" id="btn-account-selected" class="btn btn-success">Seleccionar</button> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var payment_getway;
    var table_acccounts = null;

    $(document).ready(function () {
        payment_getway = <?= json_encode($payment_getway) ?>;
        var year = new Date().getFullYear(); 
        var defaultDateCD = year + "-10-01T" + payment_getway.config.mercadopago.hours_execution;
        $('#hours-execution-datetimepicker').datetimepicker({
            locale: 'es',
            defaultDate: defaultDateCD,
            format: 'HH:mm',
        });

        $( "#payment-getway-mercadopago-mode" ).change(function() {
            var mode = $(this).val();
            if (mode == "sandbox") {
                $('#prod-public-key-group').addClass('my-hidden');
                $('#prod-access-token-group').addClass('my-hidden');
                $('#sandbox-public-key-group').removeClass('my-hidden');
                $('#sandbox-access-token-group').removeClass('my-hidden');
            } else {
                $('#sandbox-public-key-group').addClass('my-hidden');
                $('#sandbox-access-token-group').addClass('my-hidden');
                $('#prod-public-key-group').removeClass('my-hidden');
                $('#prod-access-token-group').removeClass('my-hidden');
            }
        });

        $('#table-accounts').removeClass('display');

		table_acccounts = $('#table-accounts').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '480px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
		});

		$('#table-accounts_filter').closest('div').before($('#btns-tools').contents());   

		var input_select = null;

		$('.btn-search-account').click(function() {

		    input_select = $(this).data('input');
		    $('#modal-accounts').modal('show');
		});

		$('.btn-bin-account').click(function() {

		    var input_select_temp = $(this).data('input');
		    $('#' + input_select_temp).val('');
		    $('#' + input_select_temp + '-show').val('');
		});

		$('#btn-account-selected').click(function() {

		    var code = table_acccounts.$('tr.selected').data('code');
		    var name = table_acccounts.$('tr.selected').data('name');

		    $('#' + input_select).val(code);
		    $('#' + input_select + '-show').val(code + ' ' + name);
		    $('#account_name').val(name);

		    $('#modal-accounts').modal('hide');
		});

		$('#modal-accounts').on('shown.bs.modal', function (e) {
		    table_acccounts.draw();
        });

        $('#table-accounts tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                table_acccounts.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });

        $('#test-config').click(function() {

            var request = $.ajax({
                url: "/ispbrain/MercadoPago/test.json",
                method: "POST",
                data: JSON.stringify({}),
                dataType: "json"
            });

            request.done(function(response) {
                generateNoty('success', response.messagemp);
            });

            request.fail(function(jqXHR) {

                if (jqXHR.status == 403) {

                    generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    setTimeout(function() { 
                        window.location.href = "/ispbrain";
                    }, 3000);

                } else {
                    generateNoty('error', 'Error al intentar obtener los totales');
                }
            });
        });

        $('#force-execution').click(function() {

            var text = "¿Está Seguro que desea ejecutar?";

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/MercadoPago/forceExecution/', 'method': 'post', 'id': 'replacer'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer').submit();
                }
            });
        });

        $('#clean-transactions').click(function() {

            var text = "¿Está Seguro que desea borrar las transacciones con estado: sin asignar?";

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/MercadoPago/cleanTransactions/', 'method': 'post', 'id': 'replacer'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer').submit();
                }
            });
        });
    });

</script>
