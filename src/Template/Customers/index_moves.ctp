<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

<style type="text/css">

    .my-hidden {
        display: none;
    }

    .input-debt-month-red {
        font-size: 23px;
        padding: 0 15px 0 0;
        color: #f05f40;
    }

    .input-debt-month-green {
        font-size: 23px;
        padding: 0 15px 0 0;
        color: #28a745;
    }

    .last-cell-saldo-red {
        color: #f05f40;
        font-weight: bold;
    }

    .last-cell-saldo-green {
        color: #28a745;
        font-weight: bold;
    }

</style>

<link href='https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css' rel='stylesheet' type='text/css'>

<div class="row">
    <div class="col-xl-6">

        <div class="card border-secondary mb-2">

            <div class="card-body p-2">

                <div class="row ">
                    <div class="col-xl-5">
                        <div class="input-group mr-2">
                            <span class="input-group-addon input-group-text">Fecha</span>
                            <input type="text" id="date_from" class="form-control font-weight-bold" readonly>
                            <span class="input-group-addon input-group-text glyphicon icon-calendar btn-filter-period" data-toggle='modal' data-target=#modal-filter title='Filtrar fecha'></span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-xl-6 ">

        <div class="card border-secondary p-1 mb-2">

            <div class="card-body p-2 d-flex justify-content-around">

                <?php

                    echo $this->Html->link(
                        '<span class="fas fa-sync-alt" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Actualizar la tabla',
                        'class' => 'btn btn-default btn-update-table-move ml-1 mt-1',
                        'escape' => false
                    ]);

                    echo $this->Html->link(
                        '<span class="glyphicon icon-eye" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Ocultar o Mostrar Columnas',
                        'class' => 'btn btn-default btn-hide-column ml-1',
                        'escape' => false
                    ]);

                    echo $this->Html->link(
                        '<span class="glyphicon fas fa-search" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Buscar por Columnas',
                        'class' => 'btn btn-default btn-search-column-moves ml-1 mt-1',
                        'escape' => false
                    ]);
        
                    echo $this->Html->link(
                        '<span class="far fa-calendar-plus" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Mostrar próximos vencimientos',
                        'class' => 'btn btn-default btn-next-due-dates-moves ml-1 mt-1',
                        'escape' => false
                    ]);
        
                    echo $this->Html->link(
                        '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Exportar Resumen',
                        'class' => 'btn btn-default btn-export-moves ml-1 mt-1',
                        'escape' => false
                    ]);
        
                ?>

            </div>

        </div>
    </div>
</div>

<div class="modal fade dont-fade bs-example-modal-sm" id="modal-filter" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Filtrar por Fecha</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <?= $this->Form->create() ?>

                    <div class="row">
                        <div class="col-md-12">

                            <div class="input-group">
                                <div class='input-group date' id='date-from-datetimepicker'>
                                    <span class="input-group-addon input-group-text mr-0">Fecha</span>
                                    <input name="from" id="date-from" type='text' class="column_search form-control datetimepicker" data-column="2" required />
                                    <span class="input-group-addon input-group-text mr-0">
                                        <span class="glyphicon icon-calendar"></span>
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div>

                <?= $this->Form->end() ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-clear-filter" >Limpiar</button>
                <button type="button" class="btn btn-primary btn-periode-search">Buscar</button>
            </div>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-xl-12">
        <table class="table table-bordered table-hover" id="table-moves">
            <thead>
                <tr>
                    <th>Fecha</th>                      <!--0-->
                    <th>Cód.</th>                       <!--1-->
                    <th>Cliente</th>                    <!--2-->
                    <th>Tipo Doc.</th>                  <!--3-->
                    <th>Nro</th>                        <!--4-->
                    <th>Usuario</th>                    <!--5-->
                    <th>Caja</th>                       <!--6-->
                    <th>Tipo</th>                       <!--7-->
                    <th>Pto. Vta.</th>                  <!--8-->
                    <th>Número</th>                     <!--9-->   
                    <th>CAE</th>                        <!--10-->   
                    <th>Descripción</th>                <!--11-->
                    <th class="subtotal">Subtotal</th>  <!--12-->
                    <th class="impuesto">Impuestos</th> <!--13-->
                    <th class="total">Total</th>        <!--14-->
                    <th>Pagado</th>                     <!--15-->
                    <th>Venc.</th>                      <!--16-->
                    <th>Nro Asiento</th>                <!--17-->
                </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
                <tr>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

<div class="modal fade modal-generate-invoice-from-move" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabel">Facturar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <div class="row">

                    <div class="col-6">
                        <?php
                            echo $this->Form->input('business_id', ['options' => $business,  'label' =>  'Empresa', 'value' => '']); 
                        ?>
                    </div>
                    <div class="col-6">
                        <?php
                            echo $this->Form->input('type', ['options' => [],  'label' =>  'Tipo comprobante', 'required' => true]);
                        ?>
                    </div>

                 </div>

                <div class="row">

                    <div class="col-6">
                        <label class="control-label" for="">Fecha de emisión</label>
                        <div class='input-group date' id='created-datetimepicker'>
                            <input name="created" id="created"  type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar mr-0">
                                <span class="glyphicon icon-calendar mr-0"></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-6">
                         <?php
                         echo $this->Form->input('concept_type', ['label' => 'Tipo concepto', 'options' => $concept_types, 'value' => 2, 'id' => 'concept_type'])
                        ?>
                    </div>

                </div>

                <div class="row">
                     <div class="col-12">
                         <legend class="sub-title-sm">Período</legend>
                     </div>
                </div>

                <div class="row">
                     <div class="col-6">
                        <label class="control-label" for="">Desde</label>
                        <div class='input-group date' id='date_start-datetimepicker'>
                            <input name="date_start" id="date_start"  type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar mr-0">
                                <span class="glyphicon icon-calendar mr-0"></span>
                            </span>
                        </div>
                     </div>
                     <div class="col-6">
                        <label class="control-label" for="">Hasta</label>
                        <div class='input-group date' id='date_end-datetimepicker'>
                            <input name="date_end" id="date_end"  type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar mr-0">
                                <span class="glyphicon icon-calendar mr-0"></span>
                            </span>
                        </div>
                     </div>
                </div>

                <div class="row mt-2">
                     <div class="col-12">
                        <label class="control-label" for="">Venc. de Pago</label>
                        <div class='input-group date' id='duedate-datetimepicker'>
                            <input name="duedate" id="duedate"  type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar mr-0">
                                <span class="glyphicon icon-calendar mr-0"></span>
                            </span>
                        </div>
                     </div>
                </div>

                <div class="row mt-4">
                     <div class="col-12">
                         <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-danger btn-generate float-right">Confirmar</button>
                     </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-anulate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Anular Pago</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <?= $this->Form->create() ?>

                    <div class="row" id="form-anulate-payment">

                        <div class="col-md-12">
                            <?= $this->Form->input('cash_entity_id', ['label' => __('Caja'), 'options' => $cash_entities_array ]) ?>
                        </div>

                    </div>

                <?= $this->Form->end() ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn-anulated-payment" data-dismiss="modal">Anular</button>
            </div>
        </div>
    </div>
</div>

<?php

    $buttons[] = [
        'id'   => 'btn-print-move',
        'name' => 'Imprimir',
        'icon' => 'icon-printer',
        'type' => 'btn-secondary'
    ];

    $buttons[] = [
        'id'   => 'btn-send-email',
        'name' => 'Enviar Correo',
        'icon' => 'fab fa-telegram-plane',
        'type' => 'btn-secondary'
    ];

    $buttons[] = [
        'id'   => 'btn-edit-administrative-movement',
        'name' => 'Editar',
        'icon' => 'icon-pencil2',
        'type' => 'btn-success'
    ];

    $buttons[] = [
        'id'   => 'btn-generate-invoice-from-move',
        'name' => 'Convertir',
        'icon' => 'icon-pencil2',
        'type' => 'btn-info'
    ];

    $buttons[] = [
        'id'   => 'btn-delete-move',
        'name' => 'Eliminar',
        'icon' => 'fa fa-times',
        'type' => 'btn-danger'
    ];

    $buttons[] = [
        'id'   => 'btn-anulate-move',
        'name' => 'Anulación rápida',
        'icon' => 'fa fa-times',
        'type' => 'btn-danger'
    ];

    $buttons[] = [
        'id'   => 'anulate-payment',
        'name' => 'Anular Pago',
        'icon' => 'icon-undo2',
        'type' => 'btn-danger'
    ];

    echo $this->element('actions', ['modal' => 'modal-actions-move', 'title' => 'Acciones', 'buttons' => $buttons]);
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-moves']);
    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-moves']);
    echo $this->element('Email/send', ['modal'=> 'modal-send-email', 'title' => 'Seleccionar Plantilla', 'mass_emails_templates' => $mass_emails_templates ]);
    echo $this->element('modal_preloader');
?>

<?php

    $buttons_account_summary[] = [
        'id'   => 'btn-export-pdf-moves',
        'name' => 'Imprimir PDF',
        'icon' => 'glyphicon icon-file-pdf',
        'type' => 'btn-secondary'
    ];

    $buttons_account_summary[] = [
        'id'   => 'btn-send-email-account-summary',
        'name' => 'Enviar Correo',
        'icon' => 'fab fa-telegram-plane',
        'type' => 'btn-success'
    ];

    echo $this->element('actions', ['modal' => 'modal-actions-account-summary', 'title' => 'Acciones Resumen de Cuenta', 'buttons' => $buttons_account_summary]);
    echo $this->element('Email/send_account_summary', ['modal' => 'modal-send-email-account-summary', 'title' => 'Seleccionar Plantilla', 'mass_emails_templates' => $mass_emails_templates]);
?>

<script type="text/javascript">

    var move_selected = null;
    var table_selected = null;
    var table_moves = null;
    var model = null;
    var model_id = null;
    var mass_emails_templates = null;
    var cash_entities = <?= json_encode($cash_entities) ?>;

    $('.modal-generate-invoice-from-move #created-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'DD/MM/YYYY'
    });

    $('.modal-generate-invoice-from-move #date_start-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'DD/MM/YYYY'
    });

    $('.modal-generate-invoice-from-move #date_end-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'DD/MM/YYYY'
    });

    var users = null;

    $('.modal-generate-invoice-from-move #duedate-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'DD/MM/YYYY'
    });

    $(document).ready(function() {

        mass_emails_templates = <?= json_encode($mass_emails_templates) ?>;

        $('.modal-generate-invoice-from-move #business-id').change(function() {

           var selected = $(this).val() ;

           business_selected = null;

           $('.modal-generate-invoice-from-move #type').empty();

           $.each(sessionPHP.paraments.invoicing.business, function(i, business) {

                if (selected == business.id) {

                    business_selected = business;

                    $('.modal-generate-invoice-from-move #type').append('<option value="">Seleccione Tipo</option>');

                    if (business.responsible == 1) { //ri

                        if (business.webservice_afip.crt != '') {
                            $('.modal-generate-invoice-from-move #type').append('<optgroup label="Facturas"><option value="001">FACTURA A</option><option value="006">FACTURA B</option></optgroup>');
                        }
                    } else if (business.responsible == 6) { //monotributerou

                        if (business.webservice_afip.crt != '') {
                            $('.modal-generate-invoice-from-move #type').append('<optgroup label="Facturas"><option value="011">FACTURA C</option></optgroup>');
                        } 
                   }
               }
           });
        });

        $('.modal-generate-invoice-from-move #business-id').change();

        $('.btn-hide-column').click(function() {
           $('.modal-hide-columns-moves').modal('show');
        });

        $('.btn-search-column-moves').click(function() {
           $('.modal-search-columns-moves').modal('show');
        });

        $('#table-moves').removeClass('display');

        $('#btns-tools-moves').hide();

        let hide_columns = [8, 10, 12, 13];
        var no_search_moves = [8, 10, 12, 13];

        if (!sessionPHP.paraments.accountant.account_enabled) {
            hide_columns.push(17);
            no_search_moves.push(17);
        }

        loadPreferences('moves-index', hide_columns);

        $('.btn-next-due-dates-moves').click(function() {

            if ($(this).hasClass('btn-next-due-dates-selected')) {

                $(this).removeClass('btn-next-due-dates-selected');

                $('#label-connection-saldo-move').html('Deuda del mes');
            } else {

                $('#label-connection-saldo-move').html('Deuda total');

                $(this).addClass('btn-next-due-dates-selected');
            }

            table_moves.draw();
        });

        var color_debt_month = "";

        var date_now = new Date();
        var day = date_now.getDate();
        var month = date_now.getMonth() + 1;
        var year = date_now.getFullYear();
        date_now = day + '/' + month + '/' + year;

        $('#date-from-datetimepicker').datetimepicker({
            locale: 'es',
            defaultDate: new Date(),
            format: 'DD/MM/YYYY'
        });

        table_moves = $('#table-moves').DataTable({
            "deferRender": false,
            "processing": true,
            "serverSide": true,
             "ajax": {
                "url": "/ispbrain/Customers/get_moves_all.json",
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "duedate": $('.btn-next-due-dates-moves').hasClass('btn-next-due-dates-selected') ? 1 : 0,
                        "date": $('#date-from').val(),
                        "complete": 1
                    });
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	},
                "dataFilter": function(data) {
                    var json = $.parseJSON(data);
                    return JSON.stringify(json.response);
                },
            },
            "drawCallback": function( settings ) {
                var flag = true;
            	switch (settings.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
            	    if (table_moves) {

                        var tableinfo = table_moves.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column-move').addClass('search-apply');
                        } else {
                            $('.btn-search-column-move').removeClass('search-apply');
                        }
                    }
            	}
            	this.api().columns('.total').every(function() {
                    var column = this;

                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    var sum = column
                        .data()
                        .reduce(function (a, b) { 

                           return intVal(a) + intVal(b);
                        }, 0);

                    $(column.footer()).html('$' + sum.toFixed(2));
                });
                this.api().columns('.subtotal').every(function() {
                    var column = this;

                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    var sum = column
                        .data()
                        .reduce(function (a, b) { 

                           return intVal(a) + intVal(b);
                        }, 0);

                    $(column.footer()).html('$' + sum.toFixed(2));
                });
                this.api().columns('.impuesto').every(function() {
                    var column = this;

                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    var sum = column
                        .data()
                        .reduce(function (a, b) { 

                           return intVal(a) + intVal(b);
                        }, 0);

                    $(column.footer()).html('$' + sum.toFixed(2));
                });
            },
		    "scrollY": true,
		    "scrollY": '260px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
		    "ordering": false,
		    "columns": [
		        { 
                    "data": "date",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {

                            var date = data.split('T')[0];
                            var date_2 = data.split('T')[1].split('-')[0].split(':');

                            date = date.split('-');
                            return date[2] + '/' + date[1] + '/' + date[0] + ' ' + date_2[0] + ':' + date_2[1] + ' hs';
                        }
                        return "";
                    }
                },
                {
                    "data": "customer_code",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                { 
                    "data": "customer_name",
                    "type": "string",
                },
                {
                    "data": "customer_doc_type",
                    "type": "options",
                    "options": sessionPHP.afip_codes.doc_types,
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.doc_types[data];
                    }
                },
                {
                    "data": "customer_ident",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (data) {
                            ident = data;
                        }
                        return ident;
                    }
                },
                { 
                    "data": "username",
                    "type": "string"
                },
                { 
                    "data": "cash_entity_name",
                    "type": "string"
                },
                { 
                    "data": "tipo_comp",
                    "type": "options",
                    "options": {
                        'debt': 'Deuda sin facturar',
                    },
                    "render": function ( data, type, row ) {
                        if (row.type_move == 'debt') {
                            return 'Deuda sin facturar';
                        }
                        return sessionPHP.afip_codes.comprobantes[data];
                    }
                },
                { 
                    "data": "pto_vta",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return pad(data, 4);
                        }
                        return '';
                    }
                },
                { 
                    "data": "num",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return pad(data, 8);
                        }
                        return '';
                    }
                },
                { 
                    "data": "cae",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                { 
                    "class": "left",
                    "data": "description",
                    "type": "string"
                },
                { 
                    "class": "right",
                    "data": "subtotal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '$ ' )
                },
                { 
                    "class": "right",
                    "data": "sum_tax",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '$ ' )
                },
                { 
                    "class": "right total",
                    "data": "total",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '$ ' )
                },
                { 
                    "data": "paid",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (row.type_move == 'invoice' && data) {
                            var paid = data.split('T')[0];
                            paid = paid.split('-');
                            return paid[2] + '/' + paid[1] + '/' + paid[0];
                        }
                        return '';
                    }
                },
                { 
                    "data": "duedate",
                    "type": "date",
                    "render": function ( data, type, row ) {

                        if (data) {

                            var duedate = data.split('T')[0];
                            duedate = duedate.split('-');

                            if (type == 'display') {

                                if (row.paid == null) {

                                    var date = new Date(duedate[1] + '/' + duedate[2] + '/' + duedate[0]);
                                    var todayDate = new Date();

                                    duedate =  duedate[2] + '/' + duedate[1] + '/' + duedate[0];

                                    if (date < todayDate) {
                                        return "<span class='text-danger'>" + duedate + "</span>";
                                    } else {
                                        return "<span class='text-info'>" + duedate + "</span>";
                                    }
                                }
                            }

                            duedate =  duedate[2] + '/' + duedate[1] + '/' + duedate[0];
                            return duedate;
                        }

                        return '';
                    }
                },
                {
                    "data": "seating_number",
                    "type": "string",
                },
            ],
		    "columnDefs": [
                { 
                    "visible": false, targets: hide_columns
                },
                {
                    "targets": [14, 16],
                    "width": '10%'
                },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
            "initComplete": function(settings, json) {},
		    "language": dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
        	"dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
		});

		$('#table-moves').closest('.dataTables_scrollBody').css('max-height', '500px');

        $('#table-moves_wrapper .tools').append($('#btns-tools-moves').contents());
        $('.buttons-pdf').html('Resumen de Cuenta <span class="glyphicon icon-file-pdf" data-toggle="tooltip" title="Resumen de Cuenta"/>');

        $('#table-moves').on( 'init.dt', function () {
            createModalHideColumn(table_moves, '.modal-hide-columns-moves', hide_columns);
            createModalSearchColumn(table_moves, '.modal-search-columns-moves', no_search_moves);
        });

        $('#date_from').val($('#date-from').val());

        $("#date-from-datetimepicker").on("dp.change",function (e) {
            $('#date_from').val($('#date-from').val());
        });

        $('.btn-periode-search').click(function() {
            if ($('#date-from').val() != '') {
                table_moves.draw();
                $('#modal-filter').modal('hide');
            } else {
                generateNoty('warning', 'Debe cargar una fecha para poder aplicar el filtro.');
            }
        });

        $('.btn-clear-filter').click(function() {
            table_moves.search( '' ).columns().search( '' ).draw();
            $('#date-from').val('');
            $('#date_from').val($('#date-from').val());
        });

        $('#btns-tools-moves').show();

        $('#connecton-id-move').change(function() {

            $(this).closest('div').addClass('mb-0');
            $('#connection-saldo-month-move').closest('div').addClass('mb-0');
            $('#connection-saldo-total-move').closest('div').addClass('mb-0');

            table_moves.draw();
        });

        $('#btn-generate-invoice-from-move').click(function() {

             $('.modal-actions-move').modal('hide');

             $('.modal-generate-invoice-from-move').modal('show');
        });

        $(document).on("click", ".modal-generate-invoice-from-move .btn-generate", function(e) {

            var rows_selected_ids = [];
            rows_selected_ids.push(move_selected.id);

            if (rows_selected_ids) {

                var d = new Date(); 
                var created = $('.modal-generate-invoice-from-move #created').val().split('/');

                var h = addZero(d.getHours());
                var m = addZero(d.getMinutes());
                var s = addZero(d.getSeconds());

                created = created[2] + '-' + created[1] + '-' + created[0] + ' ' + h + ':' + m + ':' + s;

                var duedate = $('.modal-generate-invoice-from-move #duedate').val().split('/');
                duedate = duedate[2] + '-' + duedate[1] + '-' + duedate[0] + ' ' + h + ':'+ m + ':' + s;

                var date_start = $('.modal-generate-invoice-from-move #date_start').val().split('/');
                date_start = date_start[2] + '-' + date_start[1] + '-' + date_start[0] + ' ' + h + ':' + m + ':' + s;

                var date_end = $('.modal-generate-invoice-from-move #date_end').val().split('/');
                date_end = date_end[2] + '-' + date_end[1] + '-' + date_end[0] + ' ' + h + ':' + m + ':' + s;

                var data = {
                    ids: rows_selected_ids,
                    date: created,
                    duedate: duedate,
                    concept_type: parseInt($('.modal-generate-invoice-from-move #concept_type').val()),
                    date_start: date_start,
                    date_end: date_end,
                    type: $('.modal-generate-invoice-from-move #type').val(),
                    business_id: $('.modal-generate-invoice-from-move #business-id').val(),
                };

                created = new Date(data.date);
                duedate = new Date(data.duedate);
                date_start = new Date(data.date_start);
                date_end = new Date(data.date_end);

                var now = new Date();
                now.setHours(0);
                now.setMinutes(0);
                now.setSeconds(0);

                created.setHours(0);
                created.setMinutes(0);
                created.setSeconds(0);

                if (created > now) {
                    generateNoty('warning', 'La fecha de la factura no puede ser mayor a la fecha actual.');
                    return false;
                }

                if (duedate < created) {
                    generateNoty('warning', 'La fecha de vencimiento no puede ser menor a la fecha actual.');
                    return false;
                }

                if (data.concept_type != 1 && date_end < date_start) {
                    generateNoty('warning', 'Las fechas del período no son correctas.');
                    return false;
                }

                bootbox.confirm('Se generará la factura. ¿Desea continuar?', function(result) {
                    if (result) {

                        $('body')
                            .append( $('<form/>').attr({'action': '/ispbrain/invoices/generateIndiFromPresuX/', 'method': 'post', 'id': 'form-block'})
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'data', 'value': JSON.stringify(data)}))
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                            .find('#form-block').submit();

                        openModalPreloader("Generando Factura. Espere Por favor ...");

                    }
                });

            } else {
                bootbox.alert('Debe seleccionar al menos una deuda para continuar.');
            }
        });

        $('.btn-update-table-move').click(function() {
            if (table_moves) {
                table_moves.draw();
            }
        });

        $('#connecton-id-move').change();

        $('#table-moves tbody').on( 'click', 'tr', function () {

            if (!$(this).find('.dataTables_empty').length) {

                table_moves.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');

                move_selected = table_moves.row( this ).data();
                $('#btn-edit-administrative-movement').addClass('my-hidden');

                if (move_selected.customer.billing_for_service) {
                    $('#btn-edit-administrative-movement').removeClass('my-hidden');
                }

                $('#btn-anulate-move').hide();
                $('#btn-delete-move').hide();
                $('#anulate-payment').hide();

                if ($.inArray(move_selected.tipo_comp, ['XXX', 'NDX', 'NCX']) != -1) {
                    $('#btn-delete-move').show();
                } else {
                    if ($.inArray(move_selected.tipo_comp, ['RX', 'FRX', 'XRX']) != -1) {
                        $('#anulate-payment').show();
                    } else {
                        $('#btn-anulate-move').show();
                    }
                }

                if (move_selected.tipo_comp == 'XXX') {
                    $('#btn-generate-invoice-from-move').show();
                } else {
                    $('#btn-generate-invoice-from-move').hide();
                }

                $('.modal-actions-move').modal('show');
            }
        });

        $('#btn-print-move').click(function() {

            var url = '';

            switch (move_selected.type_move) {

                case 'invoice':
                    url = '/ispbrain/Invoices/showprint/' + move_selected.id;
                    break;
                case 'payment':
                    url = '/ispbrain/payments/printReceipt/' + move_selected.id;
                    break;
                case 'debit':
                    url = '/ispbrain/DebitNotes/showprint/' + move_selected.id;
                    break;
                case 'credit':
                    url = '/ispbrain/CreditNotes/showprint/' + move_selected.id;
                    break;
            }
            window.open(url, 'Imprimir Comprobante', "width=600, height=500");
        });

        $('#btn-edit-administrative-movement').click(function() {
            $('.modal-actions-move').modal('hide');
            table_selected = table_moves;
            $('.modal-edit-administrative-movement').modal('show');
        });

        $('#modal-edit-administrative-movement').on('CHANGED_EDIT_ADMINISTRATIVE__MOVEMENT_SUCCESSFUL', function() {
            table_moves.draw();
        });

        $('#btn-delete-move').click(function() {

            $('.modal-actions-move').modal('hide');

            switch (move_selected.type_move) {

                case 'invoice':

                    bootbox.confirm("¿Está Seguro que desea eliminar el PRESU X?", function(result) {

                        if (result) {

                            var request = $.ajax({
                                url: "<?= $this->Url->build(['controller' => 'Invoices', 'action' => 'delete']) ?>",  
                                type: 'POST',
                                dataType: "json",
                                data: JSON.stringify(move_selected),
                            });

                            request.done(function( data ) {
                                table_moves.draw();
                            });

                            request.fail(function( jqXHR, textStatus ) {
                                generateNoty('error', 'Error al intentar eliminar el PRESU X');
                            });
                        }
                    });
                    break;

                case 'payment':
                    break;

                case 'debit':

                    bootbox.confirm("¿Está Seguro que desea eliminar la nota de débito?", function(result) {

                        if (result) {

                            var request = $.ajax({
                                url: "<?= $this->Url->build(['controller' => 'DebitNotes', 'action' => 'delete']) ?>",  
                                type: 'POST',
                                dataType: "json",
                                data: JSON.stringify(move_selected),
                            });

                            request.done(function( data ) {
                                table_moves.draw();
                            });

                            request.fail(function( jqXHR, textStatus ) {
                                generateNoty('error', 'Error al intentar eliminar la nota de débito');
                            });
                        }
                    });
                    break;

                case 'credit':

                    bootbox.confirm("¿Está Seguro que desea eliminar la nota de crédito?", function(result) {

                        if (result) {

                            var request = $.ajax({
                                url: "<?= $this->Url->build(['controller' => 'CreditNotes', 'action' => 'delete']) ?>",  
                                type: 'POST',
                                dataType: "json",
                                data: JSON.stringify(move_selected),
                            });

                            request.done(function( data ) {
                                table_moves.draw();
                            });

                            request.fail(function( jqXHR, textStatus ) {
                                generateNoty('error', 'Error al intentar eliminar la nota de crédito');
                            });
                        }
                    });
                    break;
            }
        });

        $('#btn-anulate-move').click(function() {

            $('.modal-actions-move').modal('hide');

            switch (move_selected.type_move) {

                case 'invoice':

                    bootbox.confirm("¿Está Seguro que desea anular la factura?", function(result) {

                        if (result) {

                            var request = $.ajax({
                                url: "<?= $this->Url->build(['controller' => 'Invoices', 'action' => 'anulate']) ?>",  
                                type: 'POST',
                                dataType: "json",
                                data: JSON.stringify(move_selected),
                            });

                            request.done(function( data ) {
                                table_moves.draw();
                            });

                            request.fail(function( jqXHR, textStatus ) {
                                generateNoty('error', 'Error al intentar anular la factura');
                            });
                        }
                    });
                    break;
            }
        });

        $(".btn-export-moves").click(function() {

            bootbox.confirm('Se descargará un archivo Excel', function(result) {
                if (result) {
                    $('#table-moves').tableExport({tableName: 'Resumen', type:'excel', escape:'false', columnNumber: [8, 9, 10, 13]});
                }
            });
        });

        $(".btn-export-pdf-moves-action").click(function() {
            $('.modal-actions-account-summary').modal('show');
        });

        $("#btn-export-pdf-moves").click(function() {

            $('.modal-actions-account-summary').modal('hide');

            var action = "/ispbrain/Customers/accountSummaryPDF/" + move_selected.customer_code;

            $('body')
                .append( $('<form/>').attr({'action': action, 'method': 'post', 'target': '_blank', 'id': 'replacer_pdf'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#replacer_pdf').submit();

            $('#replacer_pdf').remove();

        });

        $('#btn-send-email-account-summary').click(function() {

            if (move_selected.customer.email == "" || move_selected.customer.email == null) {
                generateNoty('warning', 'Debe agregar el Correo Electrónico del Cliente al cual pertenece el Comprobante.');
            } else {
                var count = 0;
                $.each(mass_emails_templates, function( index, value ) {
                    count++;
                });
                if (count > 1) {
                    $('.modal-actions-account-summary').modal('hide');
                    $('.modal-send-email-account-summary').modal('show');
                } else {
                    generateNoty('warning', 'No cuenta con plantillas de Correos.');
                }
            }
        });

        $('a[id="moves-tab"]').on('shown.bs.tab', function (e) {
            table_moves.draw();
        });

        $('#btn-send-email').click(function() {

            if (move_selected.customer.email == "" || move_selected.customer.email == null) {
                generateNoty('warning', 'Debe agregar el Correo Electrónico del Cliente al cual pertenece el Comprobante.');
            } else {
                var count = 0;
                $.each(mass_emails_templates, function( index, value ) {
                    count++;
                });
                if (count > 1) {
                    if (move_selected.model == 'Payments') {
                        move_selected.model = 'Receipts';
                    }
                    model = move_selected.model;
                    model_id = move_selected.id;
                    $('.modal-actions-move').modal('hide');
                    $('.modal-send-email').modal('show');
                } else {
                    generateNoty('warning', 'No cuenta con plantillas de Correos.');
                }
            }
        });

        $('#anulate-payment').click(function() {
            $('.modal-actions-move').modal('hide');
            $('#modal-anulate').modal('show');
        });

        $('#modal-anulate').on('shown.bs.modal', function (e) {

            $('#form-anulate-payment').removeClass('d-none');

            var date = new Date(move_selected.date);
            var month_recibo = date.getMonth() + 1;

            var now = new Date();
            var month = now.getMonth() + 1;

            if (now.getFullYear() > date.getFullYear()) {
                $('#form-anulate-payment').addClass('d-none');
            } else if (month > month_recibo) {
                $('#form-anulate-payment').addClass('d-none');
            } else if (now.getDate() > date.getDate()) {
                $('#form-anulate-payment').addClass('d-none');
            }
        });

        $('#btn-anulated-payment').click(function() {

            var flag = false;
            var cash = true;

            var date = new Date(move_selected.date);
            var month_recibo = date.getMonth() + 1;

            var now = new Date();
            var month = now.getMonth() + 1;

            if (now.getFullYear() > date.getFullYear()) {
                cash = false;
                flag = true;
            } else if (month > month_recibo) {
                cash = false;
                flag = true;
            } else if (now.getDate() > date.getDate()) {
                cash = false;
                flag = true;
            }

            var cash_entity_id = $('#cash-entity-id').val();

            if (cash) {

                if (!cash_entity_id) { 
                    generateNoty('warning', 'Debe seleccionar una caja');
                }

                var flag = false;

                $.each(cash_entities, function(i, cash_entity) {

                    if (cash_entity.id == cash_entity_id) {

                        if (move_selected.payment_method_id == 1) {

                            var total = move_selected.total * -1;

                            if (cash_entity.contado >= total) {
                                flag = true;
                            } else {
                                generateNoty('warning', 'El importe del recibo es mayor al saldo de caja, en la la categoría Efectivo.');
                            }
                        } else {
                            if (cash_entity.cash_other >= total) {
                                flag = true;
                            } else {
                                generateNoty('warning', 'El importe del recibo es mayor al saldo de caja, en la la categoría Otros medios de pago.');
                            }  
                        }
                    }
                });
            }

            if (flag) {

                bootbox.confirm('Confirmar Anulación', function(result) {

                    if (result) {

                        openModalPreloader("Anulando Pago  ...");

                        $.ajax({
                			type: 'POST',
                			dataType: "json",
                			url: '/ispbrain/payments/anulate',
                			data:  JSON.stringify({ id: move_selected.payment_id, cash_entity_id: cash_entity_id, cash: cash }),
                			success: function(data) {

                				$('.modal-payments').modal('hide');

                		    	closeModalPreloader();

                				if (data.response.error) {
                					generateNoty(data.response.typeMsg, data.response.msg);
                				} else {
                					generateNoty(data.response.typeMsg, data.response.msg);
                					table_moves.ajax.reload();
                				}
                			},
                			error: function(jqXHR) {
                			     if (jqXHR.status == 403) {

                                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                                	setTimeout(function() { 
                                        window.location.href = "/ispbrain";
                                	}, 3000);

                                } else {
                                	closeModalPreloader();
                			        generateNoty('error', 'Error al intentar anular el pago.');
                                }
                			}
                		});
                    }
              });
            }
        });

    });

</script>
