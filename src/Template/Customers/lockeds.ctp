<style type="text/css">

    .font-size-13px {
        font-size: 13px;
    }

    .table-connections th {
        background-color: #607D8B !important;
        color: white !important;
    }

    .table-hover tbody tr:hover {
        background-color: white !important;
        color: black !important;
    }

    .table-hover tbody tr.selectable:hover {
        background-color: rgba(105, 104, 104, 0.28) !important;
    }

    .info-contain {
        background: #f1f4f7;
        border-bottom: 1px solid #FF5722;
        border-top: 1px solid #FF5722;
        padding-top: 12px;
        margin-bottom: 10px;
    }

</style>

<div class="row">
    <div class="col-xl-12">

        <div class="card border-secondary mb-2">

            <div class="card-body p-2 d-auto justify-content-around">

                <?php

                    echo $this->Html->link(
                        '<span class="glyphicon icon-checkbox-checked text-white" aria-hidden="true"></span> Seleccionar todos',
                        'javascript:void(0)',
                        [
                        'id' => 'btn-select-all',
                        'title' => 'Seleccionar todos',
                        'class' => 'btn btn-info ml-2',
                        'escape' => false
                    ]);

                    echo $this->Html->link(
                        '<span class="glyphicon icon-checkbox-unchecked text-white" aria-hidden="true"></span> Quitar selección',
                        'javascript:void(0)',
                        [
                        'id' => 'btn-unselect-all',
                        'title' => 'Quitar selección',
                        'class' => 'btn btn-info ml-2',
                        'escape' => false
                    ]);

                    echo $this->Html->link(
                        '<span class="far fa-play-circle" aria-hidden="true"></span> Acciones',
                        'javascript:void(0)',
                        [
                        'id' => 'btn-actions',
                        'title' => 'Acciones',
                        'class' => 'btn btn-dark ml-2',
                        'escape' => false
                    ]);

                ?>

            </div>
        </div>
    </div>
</div>

<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5">

        <?php

            echo $this->Html->link(
                '<span class="fas fa-sync-alt" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Actualizar la tabla',
                'class' => 'btn btn-default btn-update-table ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-eye" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Ocultar o mostrar columnas',
                'class' => 'btn btn-default btn-hide-column ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon fas fa-search" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Buscar por columnas',
                'class' => 'btn btn-default btn-search-column ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export ml-1',
                'escape' => false
            ]);

        ?>

    </div>
</div>

<?php
    $buttons = [];

    $buttons[] = [
        'id'   => 'btn-lock',
        'name' => 'Bloquear',
        'icon' => 'fas fa-lock',
        'type' => 'btn-danger'
    ];

    $buttons[] = [
        'id'   => 'btn-unlock',
        'name' => 'Habilitar',
        'icon' => 'fas fa-unlock',
        'type' => 'btn-success'
    ];

    $buttons[] = [
        'id'   => 'btn-apply-debts',
        'name' => 'Aplicar Recargos',
        'icon' => 'far fa-plus-square',
        'type' => 'btn-secondary'
    ];

    $buttons[] = [
        'id'   => 'btn-apply-discounts',
        'name' => 'Aplicar Descuentos',
        'icon' => 'far fa-minus-square',
        'type' => 'btn-secondary'
    ];

    $buttons[] = [
        'id'   => 'btn-massive-edition',
        'name' => 'Edición Masiva',
        'icon' => 'fas fa-edit',
        'type' => 'btn-dark'
    ];

    echo $this->element('actions', ['modal'=> 'modal-actions', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-hover" id="table-customers">
            <thead>
                <tr>
                    <th></th>                             <!--0-->
                    <th >Creado</th>                      <!--1-->
                    <th >Código</th>                      <!--2-->
                    <th >Cuenta</th>                      <!--3-->
                    <th >Etiquetas</th>                   <!--4-->
                    <th >Impagas</th>                     <!--5-->
                    <th class="debt_month">Saldo mes</th> <!--6-->
                    <th >Nombre</th>                      <!--7-->
                    <th >Tipo</th>                        <!--8-->
                    <th >Nro</th>                         <!--9-->
                    <th >Servicios</th>                   <!--10-->
                    <th >Domicilio</th>                   <!--11-->
                    <th >Área</th>                        <!--12-->
                    <th >Teléfono</th>                    <!--13-->
                    <th >Vendedor</th>                    <!--14-->
                    <th >Resp. Iva</th>                   <!--15-->
                    <th >Comp.</th>                       <!--16-->
                    <th >Deno.</th>                       <!--17-->
                    <th >Correo electrónico</th>          <!--18-->
                    <th >Comentarios</th>                 <!--19-->
                    <th >Día Venc.</th>                   <!--20-->
                    <th >Clave Portal</th>                <!--21-->
                    <th >Empresa Facturación</th>         <!--22-->
                    <th >Compromiso de Pago</th>          <!--23-->
                    <th >Cuentas</th>                     <!--24-->
                    <th >Controladores</th>               <!--25-->
                    <th >Servicios</th>                   <!--26-->
                </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
                <tr>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                    <th class="pt-1 pb-1"></th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

<?php
    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-customers']);
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-customers']);
    echo $this->element('modal_preloader');
?>

<div class="tooltip" role="tooltip">
    <div class="arrow">
    </div>
    <div class="tooltip-inner">
    </div>
</div>

<div class="modal fade dont-fade bs-example-modal-sm" id="modal-debts" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Acción: Aplicar Recargos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">

                     <div class="col-xl-12">
                        <div class="row info-contain">
                            <div class="col-12">
                                <legend class="sub-title">Información</legend>
                            </div>
                            <div class="col-auto info-service">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Servicio: </label> <span class="lbl-service-selected"></span>
                                </span>
                            </div>
                            <div class="col-auto info-controller">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Controlador: </label> <span class="lbl-controller-selected"></span>
                                </span>
                            </div>
                            <div class="col-xl-12 info-area">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Área: </label> <span class="lbl-area-selected"></span>
                                </span>
                            </div>
                            <div class="col-auto info-debt">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Saldo: </label> <span class="lbl-debt-selected"></span>
                                </span>
                            </div>
                            <div class="col-auto info-invoice-no-paid">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Impagas: </label> <span class="lbl-invoice-no-paid-selected"></span>
                                </span>
                            </div>
                            <div class="col-12">
                                <label>Cant. Clientes: </label> <span class="lbl-connections-selected"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12">
                        <legend class="sub-title">Recargo</legend>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                        <?= $this->Form->input('surcharge_type', [ 'label' => 'Para', 'options' => ['next_month' => 'Mes próximo', 'this_month' => 'Adelantado']]); ?>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                        <?php echo $this->Form->input('value', ['type' => 'number', 'label' => 'Importe', 'min' => 0.01, 'step' => 0.01]); ?>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                        <label  class="control-label">Período</label> 
                        <div class='input-group date' id='period-datetimepicker'>
                            <input name='period' id="period" type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar">
                                <span class="glyphicon icon-calendar"></span>
                            </span>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                        <label  class="control-label">Vencimiento</label> 
                        <div class='input-group date' id='duedate-datetimepicker'>
                            <input name='duedate' id="duedate" type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar">
                                <span class="glyphicon icon-calendar"></span>
                            </span>
                        </div>
                    </div>

                    <div class="col-xl-6">
                        <?= $this->Form->input('presupuesto', ['type' => 'checkbox', 'label' => 'Forzar presupuesto', 'checked' => TRUE]); ?>
                    </div>

                    <div class="col-xl-12">
                        <?php echo $this->Form->input('concept', ['label' => 'Concepto', "autocomplete" => "off", 'value' => 'Recargo']); ?>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="btn-confirm-debt">Confirmar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade dont-fade bs-example-modal-sm" id="modal-discounts" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Acción: Aplicar Descuentos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">

                     <div class="col-xl-12">
                        <div class="row info-contain">
                            <div class="col-12">
                                <legend class="sub-title">Información</legend>
                            </div>
                            <div class="col-auto info-service">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Servicio: </label> <span class="lbl-service-selected"></span>
                                </span>
                            </div>
                            <div class="col-auto info-controller">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Controlador: </label> <span class="lbl-controller-selected"></span>
                                </span>
                            </div>
                            <div class="col-xl-12 info-area">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Área: </label> <span class="lbl-area-selected"></span>
                                </span>
                            </div>
                            <div class="col-auto info-debt">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Saldo: </label> <span class="lbl-debt-selected"></span>
                                </span>
                            </div>
                            <div class="col-auto info-invoice-no-paid">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Impagas: </label> <span class="lbl-invoice-no-paid-selected"></span>
                                </span>
                            </div>
                            <div class="col-12">
                                <label>Cant. Clientes: </label> <span class="lbl-connections-selected"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12">
                        <legend class="sub-title">Descuento</legend>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                        <?= $this->Form->input('surcharge_type', [ 'label' => 'Para', 'options' => ['next_month' => 'Mes próximo', 'this_month' => 'Adelantado']]); ?>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                        <?php echo $this->Form->input('value', ['type' => 'number', 'label' => 'Importe', 'min' => 0.01, 'step' => 0.01]); ?>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                        <label  class="control-label">Período</label> 
                        <div class='input-group date' id='period-datetimepicker'>
                            <input name='period' id="period" type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar">
                                <span class="glyphicon icon-calendar"></span>
                            </span>
                        </div>
                    </div>

                    <div class="col-xl-6">
                        <?= $this->Form->input('presupuesto', ['type' => 'checkbox', 'label' => 'Forzar presupuesto', 'checked' => TRUE]); ?>
                    </div>

                    <div class="col-xl-12">
                        <?php echo $this->Form->input('concept', ['label' => 'Concepto', "autocomplete" => "off", 'value' => 'Bonificación']); ?>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="btn-confirm-discount">Confirmar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade dont-fade bs-example-modal-sm" id="modal-block" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Acción: Bloquear</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">

                     <div class="col-xl-12">
                        <div class="row info-contain">
                            <div class="col-12">
                                <legend class="sub-title">Información</legend>
                            </div>
                            <div class="col-auto info-service">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Servicio: </label> <span class="lbl-service-selected"></span>
                                </span>
                            </div>
                            <div class="col-auto info-controller">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Controlador: </label> <span class="lbl-controller-selected"></span>
                                </span>
                            </div>
                            <div class="col-xl-12 info-area">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Área: </label> <span class="lbl-area-selected"></span>
                                </span>
                            </div>
                            <div class="col-auto info-debt">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Saldo: </label> <span class="lbl-debt-selected"></span>
                                </span>
                            </div>
                            <div class="col-auto info-invoice-no-paid">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Impagas: </label> <span class="lbl-invoice-no-paid-selected"></span>
                                </span>
                            </div>
                            <div class="col-12">
                                <label>Cant. Clientes: </label> <span class="lbl-connections-selected"></span>
                            </div>
                            <div class="col-12">
                                <?php
                                    echo $this->Form->input('blocking_payemnt_commitment', ['label' => 'Bloquear también conexiones de clientes c/compromiso de pago', 'type' => 'checkbox', 'checked' => FALSE]);
                                ?>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="btn-confirm-block">Confirmar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade dont-fade bs-example-modal-sm" id="modal-unblock" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Acción: Habilitar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">

                     <div class="col-xl-12">
                        <div class="row info-contain">
                            <div class="col-12">
                                <legend class="sub-title">Información</legend>
                            </div>
                            <div class="col-auto info-service">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Servicio: </label> <span class="lbl-service-selected"></span>
                                </span>
                            </div>
                            <div class="col-auto info-controller">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Controlador: </label> <span class="lbl-controller-selected"></span>
                                </span>
                            </div>
                            <div class="col-xl-12 info-area">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Área: </label> <span class="lbl-area-selected"></span>
                                </span>
                            </div>
                            <div class="col-auto info-debt">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Saldo: </label> <span class="lbl-debt-selected"></span>
                                </span>
                            </div>
                            <div class="col-auto info-invoice-no-paid">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Impagas: </label> <span class="lbl-invoice-no-paid-selected"></span>
                                </span>
                            </div>
                            <div class="col-12">
                                <label>Cant. Clientes: </label> <span class="lbl-connections-selected"></span>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="btn-confirm-unblock">Confirmar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade dont-fade bs-example-modal-sm" id="modal-massive-edition" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Acción: Edición Masiva</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">

                     <div class="col-xl-12">
                        <div class="row info-contain">
                            <div class="col-12">
                                <legend class="sub-title">Información</legend>
                            </div>
                            <div class="col-auto info-service">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Servicio: </label> <span class="lbl-service-selected"></span>
                                </span>
                            </div>
                            <div class="col-auto info-controller">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Controlador: </label> <span class="lbl-controller-selected"></span>
                                </span>
                            </div>
                            <div class="col-xl-12 info-area">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Área: </label> <span class="lbl-area-selected"></span>
                                </span>
                            </div>
                            <div class="col-auto info-debt">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Saldo: </label> <span class="lbl-debt-selected"></span>
                                </span>
                            </div>
                            <div class="col-auto info-invoice-no-paid">
                                <span class="badge badge-info">
                                    <label class="text-white"><span class="fas fa-search"></span> Impagas: </label> <span class="lbl-invoice-no-paid-selected"></span>
                                </span>
                            </div>
                            <div class="col-12">
                                <label>Cant. Clientes: </label> <span class="lbl-connections-selected"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12">
                        <legend class="sub-title">Edición</legend>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                        <?php
                            $area_posta[''] = 'Seleccionar';
                            foreach ($areas as $key => $area) {
                                $area_posta[$key] = $area;
                            }
                        ?>
                        <?= $this->Form->input('area', [ 'label' => 'Área', 'options' => $area_posta, 'value' => '']); ?>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                        <?php
                            $doc_types = $this->request->getSession()->read('afip_codes')['doc_types'];
                            $doc_types_posta[''] = 'Seleccionar';
                            foreach ($doc_types as $key => $doc_type) {
                                $doc_types_posta[$key] = $doc_type;
                            }
                        ?>
                        <?php  echo $this->Form->input('doc_type', ['options' => $doc_types_posta, 'label' => "Tipo", 'value' => '', 'required' => true]); ?>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                        <?php
                            $responsibles = $this->request->getSession()->read('afip_codes')['responsibles'];
                            $responsibles_posta[''] = 'Seleccionar';
                            foreach ($responsibles as $key => $responsible) {
                                $responsibles_posta[$key] = $responsible;
                            }
                            echo $this->Form->input('responsible', ['label' => 'Resp. ante el IVA', 'options' => $responsibles_posta, 'value' => '', 'required' => true]);
                        ?>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                        <?php
                            $presupuesto_options = [
                                '' => 'Seleccionar',
                                1  => 'Presupuesto',
                                0  => 'Factura'
                            ];
                            echo $this->Form->input('is_presupuesto', ['label' => 'Comp.', 'options' => $presupuesto_options, 'value' => '']);
                        ?>
                    </div>

                    <div class="col-sm-12 col-md-8 col-lg-6 col-xl-6">
                        <?php
                            $business_posta[''] = 'Seleccionar';
                            foreach ($business as $key => $busi) {
                                $business_posta[$key] = $busi;
                            }
                            echo $this->Form->input('business_billing', ['label' => 'Facturar con', 'options' => $business_posta, 'value' => '', 'required' => true]);
                        ?>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="btn-confirm-massive-edition">Confirmar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var table_customers = null;
    var table_selected = null;
    var customer_selected = null;
    var areas = null;
    var services = null;
    var users = null;
    var accounts = null;
    var controllers = null;

    var curr_pos_scroll = null;

    var labels = <?= json_encode($labels) ?>;
    var hash = null;

    $(document).ready(function () {

        areas = <?= json_encode($areas) ?>;
        services = <?= json_encode($services) ?>;
        users = <?= json_encode($users) ?>;
        accounts = <?= json_encode($accounts) ?>;
        controllers = <?php echo json_encode($controllers); ?>;
        hash = <?= time() ?>;

        $('.btn-hide-column').click(function() {
            $('.modal-hide-columns-customers').modal('show');
        });

        $('.btn-search-column').click(function() {
            $('.modal-search-columns-customers').modal('show');
        });

        $('#table-customers').removeClass('display');

        $('#btns-tools').hide();

        var hide_force = [];
        var no_seach = [];

        if (!sessionPHP.paraments.accountant.account_enabled) {
            hide_force = [3];
            no_seach = [3];
        }

        loadPreferences('customers-index5', [3, 11, 13, 14, 15, 16, 17, 18, 19, 20, 21]);

		table_customers = $('#table-customers').DataTable({
		    "order": [[ 1, 'desc' ]],
            "deferRender": true,
            "processing": true,
            "serverSide": true,
		    "ajax": {
                "url": "/ispbrain/customers/get_customers.json",
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() {
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "drawCallback": function( c ) {
                var flag = true;
            	switch (c.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
	            if (flag) {
	                if (table_customers) {

                        var tableinfo = table_customers.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }

                        if (curr_pos_scroll) {
                            $(table_customers.settings()[0].nScrollBody).scrollTop( curr_pos_scroll.top );
                            $(table_customers.settings()[0].nScrollBody).scrollLeft( curr_pos_scroll.left );
                        }
                    }
	            }

	            this.api().columns('.debt_month').every(function() {
                    var column = this;

                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    var sum = column
                        .data()
                        .reduce(function (a, b) {

                           return intVal(a) + intVal(b);
                        }, 0);

                    $(column.footer()).html('$' + sum.toFixed(2));
                });
            },
            "scrollY": true,
		    "scrollY": '450px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": false,
		    "columns": [
		        {
                    "className": 'details-control ',
                    "orderable": false,
                    "data":      'connections',
                    "render": function ( data, type, full, meta ) {
                        var num = data.length;
                        if (num > 0) {
                            return "<a href='javascript:void(0)' class='grey' title='Servicios' data-toggle='tooltip_services'>"
                                + "<span class='icon-plus' aria-hidden='true'>"
                                + "</span>"
                                + "</a>";
                        }

                        return '';
                    },
                    "width": '3%'
                },
                {
                    "data": "created",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                {
                    "data": "code",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                {
                    "data": "account_code",
                    "type": "string"
                },
                {
                    "data": "status",
                    "type": "options_colors",
                    "options": labels,
                    "render": function ( data, type, row ) {
                        if (row.labels.length > 0) {
                            var spans = "";
                            var names = "";
                            $.each(row.labels, function(i, val) {

                                names += "<span>" + val.text + "</span><br>"; 
                                spans += "<span class='status' style='background-color: " + val.color_back + "' > </span>"; 
                            });
                            return "<div data-toggle='tooltip_etiquetas' title='" + names + "'>" + spans + "</div>";
                        }
                        return '';
                    }
                },
                {
                    "data": "invoices_no_paid",
                    "type": "decimal",
                    "render": function ( data, type, row ) {
                       if (row.invoices_no_paid > 0) {
                           return "<span class='badge badge-danger font-size-13px' >" + row.invoices_no_paid + "</span>";
                       }
                       return "<span class='badge badge-success font-size-13px'>" + row.invoices_no_paid + "</span>";
                    }
                    
                },
                {
                    "data": "debt_month",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' ),
                },
                { 
                    "data": "name",
                    "type": "string",
                },
                {
                    "data": "doc_type",
                    "type": "options",
                    "options": sessionPHP.afip_codes.doc_types,
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.doc_types[data];
                    }
                },
                {
                    "data": "ident",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (data) {
                            ident = data;
                        }
                        return ident;
                    }
                },
                {
                    "data": "services_active",
                    "type": "options",
                    "options": {  
                        "0":"Bloqueado",
                        "1":"Habilitado"
                    },
                    "render": function ( data, type, row ) {
                       return "<span class='font-size-13px'>" + row.services_active + '/' + row.services_total + "</span>";
                    }
                },
                {
                    "data": "address",
                    "type": "string",
                },
                {
                    "data": "area_id",
                    "type": "options",
                    "options": areas,
                    "render": function ( data, type, row ) {
                        return data ? areas[data] : '';
                    }
                },
                {
                    "data": "phone",
                    "type": "integer",
                },
                {
                    "data": "seller",
                    "type": "string",
                },
                {
                    "data": "responsible",
                    "type": "options",
                    "options": sessionPHP.afip_codes.responsibles,
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.responsibles[data];
                    }
                },
                {
                    "data": "is_presupuesto",
                    "type": "options",
                    "options": {
                        "0":"Factura",
                        "1":"Presupuesto",
                    },
                    "render": function ( data, type, row ) {
                        if (data) {
                            return 'PRESU';
                        }
                        return 'FACTURA';
                    }
                },
                {
                    "data": "denomination",
                    "type": "options",
                    "options": {
                        "0":"No Denominado",
                        "1":"Denominado",
                    },
                    "render": function ( data, type, row ) {
                        if (data) {
                            return 'SI';
                        }
                        return 'NO';
                    }
                },
                {
                    "data": "email",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                {
                    "data": "comments",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                {
                    "data": "daydue",
                    "type": "integer",
                },
                {
                    "data": "clave_portal",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                { 
                    "data": "business_billing",
                    "type": "options_obj",
                    "options": sessionPHP.paraments.invoicing.business,
                    "render": function ( data, type, row ) {
                        var business_name = '';
                        $.each(sessionPHP.paraments.invoicing.business, function (i, b) {
                            if (b.id == data) {
                                business_name = b.name;
                            }
                        });
                        return business_name;
                    }
                },
                { 
                    "data": "payment_commitment",
                    "type": "date",
                    "display_search_text": "C. Pago",
                    "render": function ( data, type, row ) {
                        var created = '';
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                        return created;
                    }
                },
                {
                    "data": "customers_accounts",
                    "type": "options",
                    "options": accounts,
                    "render": function ( data, type, row ) {
                        if (row.customers_accounts.length > 0) {
                            var accounts_name = "";
                            $.each(row.customers_accounts, function( index, value ) {
                                accounts_name += accounts[value.payment_getway_id] + ' - ';
                            });
                            return accounts_name;
                        }
                        return '';
                    }
                },
                {
                    "className":"row-control",
                    "data": "controller",
                    "type": "options",
                    "options": controllers,
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                {
                    "className":"row-control",
                    "data": "service",
                    "type": "options",
                    "options": services,
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
            ],
		    "columnDefs": [
                {
                    "targets": [1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 18, 20, 21, 23],
                    "width": '5%'
                },
                {
                    "targets": [22],
                    "width": '8%'
                },
                {
                    "targets": [10, 18],
                    "width": '13%'
                },
                {
                    "targets": [7, 24],
                    "width": '12%'
                },
                { 
                    "class": "left", targets: [7, 11]
                },
                { 
                    "visible": false, targets: hide_columns
                },
                { 
                    "visible": false, targets: hide_force
                },
                { "orderable": false, "targets": [4, 10] }
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.code;
                $(row).addClass('selectable');
            },
		    "language": dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
        	"dom":
    	    	"<'row'<'col-auto mr-auto'f><'col-auto tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
		}).on( 'draw.dt', function () {
            $('[data-toggle="tooltip_etiquetas"]').tooltip({html: true});
            $('[data-toggle="tooltip_services"]').tooltip({html: true});
        });

		$('.btn-update-table').click(function() {
             if (table_customers) {
                table_customers.ajax.reload();
            }
        });

        $('#table-customers_wrapper .tools').append($('#btns-tools').contents());

        $('#table-customers').on( 'init.dt', function () {
            createModalHideColumn(table_customers, '.modal-hide-columns-customers', hide_force);
            createModalSearchColumn(table_customers, '.modal-search-columns-customers', no_seach);
            loadSearchPreferences('customers-index5-search');
        });

        $('#btns-tools').show();

        $('#btn-actions').click(function() {
            var customers_selected = table_customers.$('tr.selected');
            if (customers_selected.length == 0) {
                generateNoty('warning', 'Debe seleccionar al menos un Cliente para realizar alguna acción.');
            } else {
                $('.modal-actions').modal('show');
            }
        });

        $('#table-customers tbody').on( 'click', 'tr td', function (e) {

            if (!$(this).hasClass('details-control')) {

                if ($(this).parent().hasClass('selectable')) {

                    if (!$(this).parent().find('.dataTables_empty').length) {

                        if ($(this).parent().hasClass('selected')) {
                            $(this).parent().removeClass('selected');
                        } else {
                            $(this).parent().addClass('selected');
                        }

                        customer_selected = table_customers.row( this ).data();
                    }
                }
            }
        });

        $('#table-customers tbody').on('click', 'td.details-control', function () {

            if (!$(this).parent().closest('tr').find('.dataTables_empty').length) {
                table_customers.$('tr.selected').removeClass('selected');
                $(this).parent().closest('tr').addClass('selected');
            }

            customer_selected = table_customers.row( this ).data();

            if (customer_selected.services_total > 0) {

                customer_selected = table_customers.row( this ).data();

                var tr = $(this).closest('tr');
                var row = table_customers.row( tr );
                if ( row.child.isShown() ) {
                    row.child.hide();
                    tr.removeClass('shown');
                    $(this).find('span').removeClass('icon-minus');
                    $(this).find('span').addClass('icon-plus');
                } else {
                    row.child( format(row.data()) ).show();
                    tr.addClass('shown');
                    $(this).find('span').removeClass('icon-plus');
                    $(this).find('span').addClass('icon-minus');
                }
            }
        });

        $('#btn-lock').click(function() {
            $('.modal-actions').modal('hide');
            $('#modal-block').modal('show');
        });

        $('#btn-unlock').click(function() {
            $('.modal-actions').modal('hide');
            $('#modal-unblock').modal('show');
        });

        $('#btn-select-all').click(function() {
            var i = 0;
            table_customers.rows().every(function(rowIdx, tableLoop, rowLoop) {
                $(this.node()).addClass('selected');
            });
        });

        $('#btn-unselect-all').click(function() {
            table_customers.rows().every(function(rowIdx, tableLoop, rowLoop) {
                $(this.node()).removeClass('selected');
            });
        });

        $('#btn-apply-debts').click(function() {
            $('.modal-actions').modal('hide');
            $('#modal-debts').modal('show');
        });

        $('#btn-apply-discounts').click(function() {
            $('.modal-actions').modal('hide');
            $('#modal-discounts').modal('show');
        });

        $('#btn-massive-edition').click(function() {
            $('.modal-actions').modal('hide');
            $('#modal-massive-edition').modal('show');
        });

        $('#modal-massive-edition').on('shown.bs.modal', function (e) {

            $("#modal-massive-edition .info-service").addClass("d-none");
            $("#modal-massive-edition .info-controller").addClass("d-none");
            $("#modal-massive-edition .info-area").addClass("d-none");
            $("#modal-massive-edition .info-debt").addClass("d-none");
            $("#modal-massive-edition .info-invoice-no-paid").addClass("d-none");

            $("#modal-massive-edition .lbl-service-selected").text("");
            $("#modal-massive-edition .lbl-controller-selected").text("");
            $("#modal-massive-edition .lbl-area-selected").text("");
            $("#modal-massive-edition .lbl-debt-selected").text("");
            $("#modal-massive-edition .lbl-invoice-no-paid-selected").text("");

            if ($('.btn-search-column').hasClass('search-apply')) {

                if ($('#service_id').val() != "") {
                    var service_filter = services[$('#service_id').val()];
                    $("#modal-massive-edition .lbl-service-selected").text(service_filter);
                    $("#modal-massive-edition .info-service").removeClass("d-none");
                }

                if ($('#controller_id').val() != "") {
                    var controller_filter = controllers[$('#controller_id').val()];
                    $("#modal-massive-edition .lbl-controller-selected").text(controller_filter);
                    $("#modal-massive-edition .info-controller").removeClass("d-none");
                }

                if ($('#area_id').val() != "") {
                    var area_filter = areas[$('#area_id').val()];
                    $("#modal-massive-edition .lbl-area-selected").text(area_filter);
                    $("#modal-massive-edition .info-area").removeClass("d-none");
                }

                if ($('#debt_month_min').val() != "" || $('#debt_month_max').val() != "") {
                    var debt_filter = 'Min: ' + $('#debt_month_min').val() + ' - Max: ' + $('#debt_month_max').val();
                    $("#modal-massive-edition .lbl-debt-selected").text(debt_filter);
                    $("#modal-massive-edition .info-debt").removeClass("d-none");
                }

                if ($('#invoices_no_paid_min').val() != "" || $('#invoices_no_paid_max').val() != "") {
                    var invoice_no_paid_filter = 'Min: ' + $('#invoices_no_paid_min').val() + ' - Max: ' + $('#invoices_no_paid_max').val();
                    $("#modal-massive-edition .lbl-invoice-no-paid-selected").text(invoice_no_paid_filter);
                    $("#modal-massive-edition .info-invoice-no-paid").removeClass("d-none");
                }
            }

            var customers_selected = table_customers.$('tr.selected');
            $("#modal-massive-edition .lbl-connections-selected").text(customers_selected.length);
        });

        $('#btn-confirm-massive-edition').click(function() {

            var area = $('#modal-massive-edition #area').find(":selected").val();
            var doc_type = $('#modal-massive-edition #doc-type').find(":selected").val();
            var responsible = $('#modal-massive-edition #responsible').find(":selected").val();
            var is_presupuesto = $('#modal-massive-edition #is-presupuesto').find(":selected").val();
            var business_billing = $('#modal-massive-edition #business-billing').find(":selected").val();

            if (area == '' && doc_type == '' && is_presupuesto == '' && business_billing == '') {
                generateNoty('warning', 'Debe modificar al menos una de las opciones.');
            } else {

                var text = '¿Está seguro qué desea editar masivamente a los Clientes seleccionados?';

                bootbox.confirm(text, function(result) {

                    if (result) {

                        var selected_ids = [];
                        var customers_selected = table_customers.$('tr.selected');

                        $.each(customers_selected, function(i, customer) {
                            selected_ids.push(customer.id);
                        });

                        var data = {
                            ids: selected_ids,
                            area: area,
                            doc_type: doc_type,
                            responsible: responsible,
                            is_presupuesto: is_presupuesto,
                            business_billing: business_billing,
                        };

                        $('body')
                            .append( $('<form/>').attr({'action': '/ispbrain/customers/massiveEdition/', 'method': 'post', 'id': 'form-block'})
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'data', 'value': JSON.stringify(data)}))
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'hash', 'value': hash}))
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                            .find('#form-block').submit();
                    }
                });
            }
        });

        $('#modal-debts').on('shown.bs.modal', function (e) {

            $('#modal-debts #concept').val("Recargo");
            $('#modal-debts #value').val("");

            $("#modal-debts .info-service").addClass("d-none");
            $("#modal-debts .info-controller").addClass("d-none");
            $("#modal-debts .info-area").addClass("d-none");
            $("#modal-debts .info-debt").addClass("d-none");
            $("#modal-debts .info-invoice-no-paid").addClass("d-none");

            $("#modal-debts .lbl-service-selected").text("");
            $("#modal-debts .lbl-controller-selected").text("");
            $("#modal-debts .lbl-area-selected").text("");
            $("#modal-debts .lbl-debt-selected").text("");
            $("#modal-debts .lbl-invoice-no-paid-selected").text("");

            $('#modal-massive-edition #area').val('');
            $('#modal-massive-edition #doc-type').val('');
            $('#modal-massive-edition #responsible').val('');
            $('#modal-massive-edition #is-presupuesto').val('');
            $('#modal-massive-edition #business-billing').val('');

            if ($('.btn-search-column').hasClass('search-apply')) {

                if ($('#service_id').val() != "") {
                    var service_filter = services[$('#service_id').val()];
                    $("#modal-debts .lbl-service-selected").text(service_filter);
                    $("#modal-debts .info-service").removeClass("d-none");
                }

                if ($('#controller_id').val() != "") {
                    var controller_filter = controllers[$('#controller_id').val()];
                    $("#modal-debts .lbl-controller-selected").text(controller_filter);
                    $("#modal-debts .info-controller").removeClass("d-none");
                }

                if ($('#area_id').val() != "") {
                    var area_filter = areas[$('#area_id').val()];
                    $("#modal-debts .lbl-area-selected").text(area_filter);
                    $("#modal-debts .info-area").removeClass("d-none");
                }

                if ($('#debt_month_min').val() != "" || $('#debt_month_max').val() != "") {
                    var debt_filter = 'Min: ' + $('#debt_month_min').val() + ' - Max: ' + $('#debt_month_max').val();
                    $("#modal-debts .lbl-debt-selected").text(debt_filter);
                    $("#modal-debts .info-debt").removeClass("d-none");
                }

                if ($('#invoices_no_paid_min').val() != "" || $('#invoices_no_paid_max').val() != "") {
                    var invoice_no_paid_filter = 'Min: ' + $('#invoices_no_paid_min').val() + ' - Max: ' + $('#invoices_no_paid_max').val();
                    $("#modal-debts .lbl-invoice-no-paid-selected").text(invoice_no_paid_filter);
                    $("#modal-debts .info-invoice-no-paid").removeClass("d-none");
                }
            }

            $('#modal-debts #period-datetimepicker').datetimepicker({
                locale: 'es',
                defaultDate: new Date(),
                format: 'MM/YYYY',
                icons: {
                    time: "glyphicon icon-alarm",
                    date: "glyphicon icon-calendar",
                    up: "glyphicon icon-arrow-right",
                    down: "glyphicon icon-arrow-left"
                }
            });

            $('#modal-debts #duedate-datetimepicker').datetimepicker({
                locale: 'es',
                defaultDate: new Date(),
                format: 'DD/MM/YYYY',
                icons: {
                    time: "glyphicon icon-alarm",
                    date: "glyphicon icon-calendar",
                    up: "glyphicon icon-arrow-right",
                    down: "glyphicon icon-arrow-left"
                }
            });

            var customers_selected = table_customers.$('tr.selected');
            $("#modal-debts .lbl-connections-selected").text(customers_selected.length);
        });

        $('#btn-confirm-debt').click(function() {

            var importe = $('#modal-debts #value').val();
            var periode = $('#modal-debts #period').val();
            var duedate = $('#modal-debts #duedate').val();
            var concept = $('#modal-debts #concept').val();
            var presupuesto = $('#modal-debts #presupuesto').prop('checked');

            if (importe == "" || importe <= 0  || periode == "" || duedate == "" || concept == "") {

                var message = "";

                if (importe == "" || importe <= 0) {
                    message += "*Debe ingresar un importe mayor a 0 \n";
                    message.replace(/\\n/g,"\n");
                }

                if (periode == "") {
                    message += "*Debe ingresar un período \n";
                    message.replace(/\\n/g,"\n");
                }

                if (duedate == "") {
                    message += "*Debe ingresar una fecha de vencimiento \n";
                    message.replace(/\\n/g,"\n");
                }

                if (concept == "") {
                    message += "*Debe ingresar un concepto \n";
                    message.replace(/\\n/g,"\n");
                }

                generateNoty('warning', message);
            } else {

                var text = '¿Está seguro qué desea aplicar recargos a las Conexiones de los Clientes seleccionados?';

                bootbox.confirm(text, function(result) {

                    if (result) {

                        $('#modal-debts').modal('hide');
                        openModalPreloader('Generando recargos a los Clientes ... ');

                        var selected_ids = [];
                        var customers_selected = table_customers.$('tr.selected');

                        $.each(customers_selected, function(i, customer) {
                            selected_ids.push(customer.id);
                        });

                        var data = {
                            surcharge_type:  $('#modal-debts #surcharge-type').val(),
                            concept: concept,
                            periode: periode,
                            importe: importe,
                            duedate: duedate,
                            ids: selected_ids,
                            presupuesto: presupuesto
                        }

                        var request = $.ajax({
                            url: "<?= $this->Url->build(['controller' => 'Customers', 'action' => 'applyMassiveDebt']) ?>",  
                            type: 'POST',
                            dataType: "json",
                            data: JSON.stringify(data),
                        });

                        request.done(function( data ) {
                            closeModalPreloader();

                            generateNoty(data.response.typeMsg, data.response.msg);

                            if (!data.response.error) {
                                table_customers.ajax.reload();
                            }
                        });

                        request.fail(function( jqXHR, textStatus ) {
                            if (jqXHR.status == 403) {

                            	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                            	setTimeout(function() { 
                                    window.location.href = "/ispbrain";
                            	}, 3000);

                            } else {
                            	closeModalPreloader();
                                generateNoty('error', 'Error al intentar generar recargos a los Clientes.');
                            }
                        });
                    }
                });
            }
        });

        $('#modal-discounts').on('shown.bs.modal', function (e) {

            $('#modal-discounts #concept').val("Bonificación");
            $('#modal-discounts #value').val("");

            $("#modal-discounts .info-service").addClass("d-none");
            $("#modal-discounts .info-controller").addClass("d-none");
            $("#modal-discounts .info-area").addClass("d-none");
            $("#modal-discounts .info-debt").addClass("d-none");
            $("#modal-discounts .info-invoice-no-paid").addClass("d-none");

            $("#modal-discounts .lbl-service-selected").text("");
            $("#modal-discounts .lbl-controller-selected").text("");
            $("#modal-discounts .lbl-area-selected").text("");
            $("#modal-discounts .lbl-debt-selected").text("");
            $("#modal-discounts .lbl-invoice-no-paid-selected").text("");

            if ($('.btn-search-column').hasClass('search-apply')) {

                if ($('#service_id').val() != "") {
                    var service_filter = services[$('#service_id').val()];
                    $("#modal-discounts .lbl-service-selected").text(service_filter);
                    $("#modal-discounts .info-service").removeClass("d-none");
                }

                if ($('#controller_id').val() != "") {
                    var controller_filter = controllers[$('#controller_id').val()];
                    $("#modal-discounts .lbl-controller-selected").text(controller_filter);
                    $("#modal-discounts .info-controller").removeClass("d-none");
                }

                if ($('#area_id').val() != "") {
                    var area_filter = areas[$('#area_id').val()];
                    $("#modal-discounts .lbl-area-selected").text(area_filter);
                    $("#modal-discounts .info-area").removeClass("d-none");
                }

                if ($('#debt_month_min').val() != "" || $('#debt_month_max').val() != "") {
                    var debt_filter = 'Min: ' + $('#debt_month_min').val() + ' - Max: ' + $('#debt_month_max').val();
                    $("#modal-discounts .lbl-debt-selected").text(debt_filter);
                    $("#modal-discounts .info-debt").removeClass("d-none");
                }

                if ($('#invoices_no_paid_min').val() != "" || $('#invoices_no_paid_max').val() != "") {
                    var invoice_no_paid_filter = 'Min: ' + $('#invoices_no_paid_min').val() + ' - Max: ' + $('#invoices_no_paid_max').val();
                    $("#modal-discounts .lbl-invoice-no-paid-selected").text(invoice_no_paid_filter);
                    $("#modal-discounts .info-invoice-no-paid").removeClass("d-none");
                }
            }

            $('#modal-discounts #period-datetimepicker').datetimepicker({
                locale: 'es',
                defaultDate: new Date(),
                format: 'MM/YYYY',
                icons: {
                    time: "glyphicon icon-alarm",
                    date: "glyphicon icon-calendar",
                    up: "glyphicon icon-arrow-right",
                    down: "glyphicon icon-arrow-left"
                }
            });

            var customers_selected = table_customers.$('tr.selected');
            $("#modal-discounts .lbl-connections-selected").text(customers_selected.length);
        });

        $('#btn-confirm-discount').click(function() {

            var importe = $('#modal-discounts #value').val();
            var periode = $('#modal-discounts #period').val();
            var concept = $('#modal-discounts #concept').val();
            var presupuesto = $('#modal-discounts #presupuesto').prop('checked');

            if (importe == "" || importe <= 0  || periode == "" || concept == "") {

                var message = "";

                if (importe == "" || importe <= 0) {
                    message += "*Debe ingresar un importe mayor a 0 \n";
                    message.replace(/\\n/g,"\n");
                }

                if (periode == "") {
                    message += "*Debe ingresar un período \n";
                    message.replace(/\\n/g,"\n");
                }

                if (concept == "") {
                    message += "*Debe ingresar un concepto \n";
                    message.replace(/\\n/g,"\n");
                }

                generateNoty('warning', message);
            } else {

                var text = '¿Está seguro qué desea aplicar descuentos a las Conexiones de los Clientes seleccionados?';

                bootbox.confirm(text, function(result) {

                    if (result) {

                        $('#modal-discounts').modal('hide')
                        openModalPreloader('Generando descuentos a los Clientes ... ');

                        var selected_ids = [];
                        var customers_selected = table_customers.$('tr.selected');

                        $.each(customers_selected, function(i, customer) {
                            selected_ids.push(customer.id);
                        });

                        var data = {
                            surcharge_type: $('#modal-discounts #surcharge-type').val(),
                            concept: concept,
                            periode: periode,
                            importe: importe,
                            ids: selected_ids,
                            presupuesto: presupuesto
                        }

                        var request = $.ajax({
                            url: "<?= $this->Url->build(['controller' => 'Customers', 'action' => 'applyMassiveDiscount']) ?>",  
                            type: 'POST',
                            dataType: "json",
                            data: JSON.stringify(data),
                        });

                        request.done(function( data ) {
                            closeModalPreloader();

                            generateNoty(data.response.typeMsg, data.response.msg);

                            if (!data.response.error) {
                                table_customers.ajax.reload();
                            }
                        });

                        request.fail(function( jqXHR, textStatus ) {
                            if (jqXHR.status == 403) {

                            	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                            	setTimeout(function() { 
                                    window.location.href = "/ispbrain";
                            	}, 3000);

                            } else {
                            	closeModalPreloader();
                                generateNoty('error', 'Error al intentar generar descuentos a los Clientes.');
                            }
                        });
                    }
                });
            }
        });

        $('#modal-block').on('shown.bs.modal', function (e) {

            $("#modal-block .info-service").addClass("d-none");
            $("#modal-block .info-controller").addClass("d-none");
            $("#modal-block .info-area").addClass("d-none");
            $("#modal-block .info-debt").addClass("d-none");
            $("#modal-block .info-invoice-no-paid").addClass("d-none");

            $("#modal-block .lbl-service-selected").text("");
            $("#modal-block .lbl-controller-selected").text("");
            $("#modal-block .lbl-area-selected").text("");
            $("#modal-block .lbl-debt-selected").text("");
            $("#modal-block .lbl-invoice-no-paid-selected").text("");

            if ($('.btn-search-column').hasClass('search-apply')) {

                if ($('#service_id').val() != "") {
                    var service_filter = services[$('#service_id').val()];
                    $("#modal-block .lbl-service-selected").text(service_filter);
                    $("#modal-block .info-service").removeClass("d-none");
                }

                if ($('#controller_id').val() != "") {
                    var controller_filter = controllers[$('#controller_id').val()];
                    $("#modal-block .lbl-controller-selected").text(controller_filter);
                    $("#modal-block .info-controller").removeClass("d-none");
                }

                if ($('#area_id').val() != "") {
                    var area_filter = areas[$('#area_id').val()];
                    $("#modal-block .lbl-area-selected").text(area_filter);
                    $("#modal-block .info-area").removeClass("d-none");
                }

                if ($('#debt_month_min').val() != "" || $('#debt_month_max').val() != "") {
                    var debt_filter = 'Min: ' + $('#debt_month_min').val() + ' - Max: ' + $('#debt_month_max').val();
                    $("#modal-block .lbl-debt-selected").text(debt_filter);
                    $("#modal-block .info-debt").removeClass("d-none");
                }

                if ($('#invoices_no_paid_min').val() != "" || $('#invoices_no_paid_max').val() != "") {
                    var invoice_no_paid_filter = 'Min: ' + $('#invoices_no_paid_min').val() + ' - Max: ' + $('#invoices_no_paid_max').val();
                    $("#modal-block .lbl-invoice-no-paid-selected").text(invoice_no_paid_filter);
                    $("#modal-block .info-invoice-no-paid").removeClass("d-none");
                }
            }

            var customers_selected = table_customers.$('tr.selected');
            $("#modal-block .lbl-connections-selected").text(customers_selected.length);
        });

        $('#btn-confirm-block').click(function() {

            var text = '¿Está seguro qué desea Bloquear las Conexiones de los Clientes seleccionados?';

            bootbox.confirm(text, function(result) {

                if (result) {

                    var selected_ids = [];
                    var customers_selected = table_customers.$('tr.selected');
                    let blocking_payment_commitment = $('#blocking-payemnt-commitment').is(":checked");

                    $.each(customers_selected, function(i, customer) {
                        selected_ids.push(customer.id);
                    });

                    $('#modal-block').modal('hide');

                    openModalPreloader('Bloqueando Conexiones ... ');

                    $.ajax({
                        url: "<?= $this->Url->build(['controller' => 'Connections', 'action' => 'lockedMasiveFromCustomers']) ?>",
                        type: 'POST',
                        dataType: "json",
                        data: JSON.stringify({
                            ids: selected_ids,
                            blocking_payment_commitment: blocking_payment_commitment
                        }),
                        success: function(data) {

                            closeModalPreloader();

                            generateNoty(data.response.typeMsg, data.response.msg);

                            if (!data.response.error) {
                                table_customers.ajax.reload();
                            }
                        },
                        error: function (jqXHR) {

                            if (jqXHR.status == 403) {

                            	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                            	setTimeout(function() { 
                                    window.location.href = "/ispbrain";
                            	}, 3000);

                            } else {
                            	closeModalPreloader();
                                generateNoty('error', 'Error al intentar Bloquear las Conexiones de los Clientes.');
                            }
                        }
                    });
                }
            });
        });

        $('#modal-unblock').on('shown.bs.modal', function (e) {

            $("#modal-unblock .info-service").addClass("d-none");
            $("#modal-unblock .info-controller").addClass("d-none");
            $("#modal-unblock .info-area").addClass("d-none");
            $("#modal-unblock .info-debt").addClass("d-none");
            $("#modal-unblock .info-invoice-no-paid").addClass("d-none");

            $("#modal-unblock .lbl-service-selected").text("");
            $("#modal-unblock .lbl-controller-selected").text("");
            $("#modal-unblock .lbl-area-selected").text("");
            $("#modal-unblock .lbl-debt-selected").text("");
            $("#modal-unblock .lbl-invoice-no-paid-selected").text("");

            if ($('.btn-search-column').hasClass('search-apply')) {

                if ($('#service_id').val() != "") {
                    var service_filter = services[$('#service_id').val()];
                    $("#modal-unblock .lbl-service-selected").text(service_filter);
                    $("#modal-unblock .info-service").removeClass("d-none");
                }

                if ($('#controller_id').val() != "") {
                    var controller_filter = controllers[$('#controller_id').val()];
                    $("#modal-unblock .lbl-controller-selected").text(controller_filter);
                    $("#modal-unblock .info-controller").removeClass("d-none");
                }

                if ($('#area_id').val() != "") {
                    var area_filter = areas[$('#area_id').val()];
                    $("#modal-unblock .lbl-area-selected").text(area_filter);
                    $("#modal-unblock .info-area").removeClass("d-none");
                }

                if ($('#debt_month_min').val() != "" || $('#debt_month_max').val() != "") {
                    var debt_filter = 'Min: ' + $('#debt_month_min').val() + ' - Max: ' + $('#debt_month_max').val();
                    $("#modal-unblock .lbl-debt-selected").text(debt_filter);
                    $("#modal-unblock .info-debt").removeClass("d-none");
                }

                if ($('#invoices_no_paid_min').val() != "" || $('#invoices_no_paid_max').val() != "") {
                    var invoice_no_paid_filter = 'Min: ' + $('#invoices_no_paid_min').val() + ' - Max: ' + $('#invoices_no_paid_max').val();
                    $("#modal-unblock .lbl-invoice-no-paid-selected").text(invoice_no_paid_filter);
                    $("#modal-unblock .info-invoice-no-paid").removeClass("d-none");
                }
            }

            var customers_selected = table_customers.$('tr.selected');
            $("#modal-unblock .lbl-connections-selected").text(customers_selected.length);
        });

        $('#btn-confirm-unblock').click(function() {

            var text = '¿Está seguro qué desea Habilitar las Conexiones de los Clientes seleccionadas?';

            bootbox.confirm(text, function(result) {

                if (result) {

                    var selected_ids = [];
                    var customers_selected = table_customers.$('tr.selected');

                    $.each(customers_selected, function(i, customer) {
                        selected_ids.push(customer.id);
                    }); 

                    $('#modal-unblock').modal('hide');

                    openModalPreloader('Habilitando Conexiones ... ');

                    $.ajax({
                        url: "<?= $this->Url->build(['controller' => 'Connections', 'action' => 'unlockedMasiveFromCustomers']) ?>",
                        type: 'POST',
                        dataType: "json",
                        data: JSON.stringify({ids: selected_ids}),
                        success: function(data) {

                            closeModalPreloader();

                            generateNoty(data.response.typeMsg, data.response.msg);

                            if (!data.response.error) {
                                table_customers.ajax.reload();
                            }
                        },
                        error: function (jqXHR) {

                            if (jqXHR.status == 403) {

                            	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                            	setTimeout(function() { 
                                    window.location.href = "/ispbrain";
                            	}, 3000);

                            } else {
                            	closeModalPreloader();
                                generateNoty('error', 'Error al intentar Habilitar las Conexiones de los Clientes.');
                            }
                        }
                    });
                }
            });
        });

    });

    $(document).click(function() {
         $('[data-toggle="tooltip_etiquetas"]').tooltip('hide');
         $('[data-toggle="tooltip_services"]').tooltip('hide');
    });

    $(".btn-export").click(function() {

        bootbox.confirm('Se descargará un archivo Excel', function(result) {
            if (result) {
                $('#table-customers').tableExport({tableName: 'Clientes', type:'excel', escape:'false', columnNumber: [5]});
            }
        }).find("div.modal-content").addClass("confirm-width-md");
    });

    function format(d) {

        //ordena historial del ticket por fecha created de mayor a menor
        d.connections.sort(function(a, b) {
            // Turn your strings into dates, and then subtract them
            // to get a value that is either negative, positive, or zero.
            return new Date(b.created) - new Date(a.created);
        });

        var html = '<br><table class="table table-bordered table-connections mr-5">'
            + '<thead>'
                + '<tr>'
                  + '<th style="width: 8%">Fecha</th>'
                  + '<th>Servicio</th>'
                  + '<th>Domicilio</th>';
                  if (sessionPHP.paraments.gral_config.billing_for_service) {
                      html += '<th>Saldo mes</th>';
                  }
                  html +=
                + '</tr>'
              + '</thead>'
            + '<tbody>';

        $.each(d.connections, function(i, connection) {

            if (!connection.deleted) {

                var created = '';
                var class_locked = "";
                if (connection.created) {
                    created = connection.created.split('T')[0];
                    created = created.split('-');
                    created = created[2] + '/' + created[1] + '/' + created[0];
                }

                if (connection.enabled == 0) {
                    class_locked = '"bg-danger text-white"';
                }

                html +=
                '<tr class=' + class_locked + '>'
                  + '<td>' + created + '</td>'
                  + '<td>' + services[connection.service_id] + '</td>'
                  + '<td style="text-align: left !important;">' + connection.address + '</td>';
                  if (sessionPHP.paraments.gral_config.billing_for_service) {
                      html += '<td>$' + connection.debt_month.toFixed(2) + '</td>';
                  }
                  html +=
                + '</tr>';
            }
        });

        html += '</tbody>'
            + '</table><br>';

        return html;
    }

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
