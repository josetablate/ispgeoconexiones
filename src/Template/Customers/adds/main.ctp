<?php $this->extend('/Customers/adds/extras'); ?>

<?php $this->start('main'); ?>

    <style type="text/css">

        .sub-title {
            border-bottom: 1px solid #e3e2e2;
        }

        .my-hidden {
            display: none;
        }

        #map {
    		height: 370px;
    	}

    	#floating-panel {
    		top: 10px;
    		left: 25%;
    		z-index: 5;
    		background-color: #fff;
    		padding: 5px;
    		border: 1px solid #999;
    		text-align: center;
    		font-family: 'Roboto','sans-serif';
    		line-height: 30px;
    		padding-left: 10px;
    	}

    	#submit-map {
            padding: 10px;
            top: 2px;
        }

        #update-map {
            padding: 10px;
            top: 2px;
        }

        #description {
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
        }

        #infowindow-content .title {
            font-weight: bold;
        }

        #infowindow-content {
            display: none;
        }

        #map #infowindow-content {
            display: inline;
        }

        .pac-card {
            margin: 10px 10px 0 0;
            border-radius: 2px 0 0 2px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            outline: none;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
            background-color: #fff;
            font-family: Roboto;
        }

        #pac-container {
            padding-bottom: 12px;
            margin-right: 12px;
        }

        .pac-controls {
            display: inline-block;
            padding: 5px 11px;
        }

        .pac-controls label {
            font-family: Roboto;
            font-size: 13px;
            font-weight: 300;
        }

        #pac-input {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 400px;
        }

        #pac-input:focus {
            border-color: #4d90fe;
        }

        #title {
            color: #fff;
            background-color: #4d90fe;
            font-size: 25px;
            font-weight: 500;
            padding: 6px 12px;
        }

        #target {
            width: 345px;
        }

    </style>

    <div class="row mt-3">

        <div class="col-xl-3" id="input-created">
            <label for="">Fecha</label>
            <div class='input-group date' id='created-datetimepicker'>
                <input name='created' id="created" type='text' class="form-control" required />
                <span class="input-group-addon input-group-text calendar">
                    <span class="glyphicon icon-calendar"></span>
                </span>
            </div>
        </div>

        <div class="col-sm-12 col-md-8 col-lg-8 col-xl-8">

            <div class="row">

                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <?php
                        echo $this->Form->input('name', ['label' => 'Nombre Completo', 'autofocus' => true, 'required' => true, 'value' => isset($customer->name) ? $customer->name : '']);
                    ?>
                </div>

                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <?php
                        echo $this->Form->input('address', ['label' => 'Domicilio', 'required' => true, 'value' => isset($customer->address) ? $customer->address : '']);
                    ?>
                </div>

                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">

                    <div class="row">
                        <div class="col-xl-5 pr-1">
                            <?php  echo $this->Form->input('doc_type', ['options' => $this->request->getSession()->read('afip_codes')['doc_types'], 'label' => "Tipo", 'value' => $this->request->getSession()->read('paraments')->customer->doc_type, 'required' => true, 'value' => $customer != null ? $customer->doc_type : '']); ?>
                        </div>
                        <div class="col-xl-7 pl-3-sm pl-1-md pl-1-lg pl-1-xl">
                            <?php  echo $this->Form->input('ident', ['label' => "Número", 'required' => $this->request->getSession()->read('paraments')->customer->doc_validate, 'value' => isset($customer->ident) ? $customer->ident : '']); ?>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <?php
                        echo $this->Form->input('responsible', ['label' => 'Resp. ante el IVA', 'options' => $this->request->getSession()->read('afip_codes')['responsibles'], 'value' => $this->request->getSession()->read('paraments')->customer->responsible, 'required' => true]);
                    ?>
                </div>

                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4" style="padding-top: 36px;">
                    <?php
                        echo $this->Form->input('is_presupuesto', ['label' => 'Prespuesto', 'type' => 'checkbox', 'checked' => $this->request->getSession()->read('paraments')->invoicing->is_presupuesto]);
                    ?>
                </div>

                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <?php
                        echo $this->Form->input('phone', ['label' => 'Teléfono', 'required' => true, 'value' => isset($customer->phone) ? $customer->phone : '']);
                    ?>
                </div>

                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 d-none">
                    <?php
                        echo $this->Form->input('country_id', ['label' => 'País', 'options' => [], 'title' => 'Seleccionar País', 'class' => 'selectpicker', 'data-live-search' => "true", 'value' => isset($customer->country_id) ? $customer->country_id : '']);
                    ?>
                </div>

                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <?php
                        echo $this->Form->input('province_id', ['label' => 'Provincia', 'options' => [], 'title' => 'Seleccionar Provincia', 'class' => 'selectpicker', 'data-live-search' => "true", 'value' => isset($customer->province_id) ? $customer->province_id : '']);
                    ?>
                </div>

                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <?php
                        echo $this->Form->input('city_id', ['label' => 'Ciudad', 'options' => [], 'title' => 'Seleccionar Ciudad', 'required' => true, 'class' => 'selectpicker', 'data-live-search' => "true", 'value' => isset($customer->city_id) ? $customer->city_id : '']);
                    ?>
                </div>

                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <?php
                        echo $this->Form->input('area_id', ['label' => 'Área', 'options' => [], 'required' => true, 'class' => 'selectpicker', 'data-live-search' => "true", 'value' => isset($customer->area_id) ? $customer->area_id : '']);
                    ?>
                </div>

                <?php if ($paraments->invoicing->generate_ticket): ?>
                <div class="col-12 mt-3">
                    <legend class="sub-title-sm">Tickets de instalación</legend>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                    <div class='input-group date' id='start-task-datetimepicker'>
                        <label class="input-group-text" for="start-task">Inicio</label>
                        <input name='start_task' id="start-task" type='text' class="form-control" required/>
                        <span class="input-group-addon input-group-text calendar">
                            <span class="glyphicon icon-calendar"></span>
                        </span>
                    </div>
                </div>
                <div class="col-sm-12 col-md-8 col-lg-8 col-xl-8">
                    <div class="input-group select">
                        <label class="input-group-text w-auto" for="asigned_user_id"><?= __('Usuario') ?></label>
                        <select name="asigned_user_id" id="asigned_user_id" class="form-control is-valid">
                            <option value=""><?= __('Seleccionar') ?></option>
                            <?php foreach($users_add as $key => $user_add): ?>
                                <option value="<?= $user_add->id ?>"><i class="fas fa-square-full"></i> <?= $user_add->name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-3">
                    <?php
                        echo $this->Form->input('ticket_title', ['label' => 'Título Ticket', 'type' => 'text', 'value' => 'Nueva instalación. Ticket generado en la carga de Cliente']);
                    ?>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-3">
                    <?php
                        echo $this->Form->input('ticket_description', ['label' => 'Descripción Ticket', 'type' => 'textarea', 'value' => '']);
                    ?>
                </div>
                <?php endif; ?>

            </div>

        </div>

        <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">

            <div class="col-12">

                <?php
                    echo $this->Form->input('lat', ['type' => 'hidden', 'id' => 'coodmapslat']);
                    echo $this->Form->input('lng', ['type' => 'hidden', 'id' => 'coodmapslng']);
                ?>
            	<div id="floating-panel">
        		    <div class="row">
        		        <div class="col-md-12">
            		        <div class="input-group">
            		            <span class="input-group-btn">
                                    <button id="update-map" title="Recargar Mapa" class="btn btn-info" type="button"><span class="text-white glyphicon icon-map2" aria-hidden="true"></span></button>
                                </span>
                                <input id="addressmap" type="text" class=" form-control margin-top-5" value="" placeholder="Calle #, Ciudad, Provincia">
                                <span class="input-group-btn">
                                    <button id="submit-map" title="Buscar Dirección" class="btn btn-success" type="button"><span class="text-white glyphicon icon-search" aria-hidden="true"></span></button>
                                </span>
                            </div>
        		        </div>
        		    </div>
        		</div>

        		<div id="map" class="mb-3"></div>
            </div>

        </div>

    <br>
    </div>

    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2K3zoK46JybWPzQbhfQQqIIbdZ_3WcQs&libraries=places">
    </script>

    <script type="text/javascript">

        var paraments = null;
        var countries = null;
        var customer = null;

        var country_selected = null;
        var province_selected = null;
        var city_selected = null;
        var area_selected = null;
        var areas = null;
        var business = null;

        $(document).ready(function() {

            $('#start-task-datetimepicker').datetimepicker({
                defaultDate: new Date(),
                locale: 'es',
                format: 'DD/MM/YYYY HH:mm'
            });

            $("#area-id").selectpicker({
                noneSelectedText : 'Seleccionar Área' // by this default 'Nothing selected' -->will change to Please Select
            });

            paraments = <?= json_encode($paraments) ?>;
            customer = <?= json_encode($customer) ?>;
            areas = <?= json_encode($areas) ?>;
            business = <?= json_encode($business) ?>;

            //selects de pais, provincia, ciudad y area

            countries = <?= json_encode($countries) ?>;

            $.each(countries, function(i, country) {
                $('#country-id').append("<option value=" + country.id + ">" + country.name + "</option>");
            });

            $('#country-id').change(function() {

                $('#province-id').empty();
                $('#city-id').empty();
                $('#area-id').empty();

                var pre_selected = null;

                var country_id = $(this).val();

                $.each(countries, function(i, country) {

                    if (country.id == country_id) {

                        country_selected = country;

                        $.each(country_selected.provinces, function(i, province) {

                            $('#province-id').append("<option value=" + province.id + ">" + province.name + "</option>");

                            if (province.id == sessionPHP.paraments.customer.province_default) {
                                pre_selected = province.id;
                            }
                        });
                    }
                });

                $('#province-id').selectpicker('refresh');
                $('#city-id').selectpicker('refresh');
                $('#area-id').selectpicker('refresh');

                if (pre_selected) {
                    $('#province-id').selectpicker('val', pre_selected);
                    $('#province-id').change();
                }

                $('#province-id').change();

                if (sessionPHP.paraments.invoicing.business_billing_default == "") {

                    var area = areas[$('#area-id option:selected').val()];

                    var business_billing_default = null;

                    if (area.hasOwnProperty("business_billing_default")) {
                        business_billing_default = area.business_billing_default;
                    }

                    if (business_billing_default == null) {
                        business_billing_default = Object.keys(business)[0];
                    }

                    $('#business-billing').val(business_billing_default);
                } else {
                    $("#business-billing").val(sessionPHP.paraments.invoicing.business_billing_default);
                }
            });

            $('#province-id').change(function() {

                $('#city-id').empty();
                $('#area-id').empty();

                var pre_selected = null;

                var province_id = $(this).val();

                $.each(country_selected.provinces, function(i, province) {

                    if (province.id == province_id) {

                        province_selected = province;

                        $.each(province_selected.cities, function(i, city) {
                            $('#city-id').append("<option value=" + city.id + ">" + city.name + "</option>");

                            if (city.id == sessionPHP.paraments.customer.city_default) {
                                pre_selected = city.id;
                            }
                        });
                    }
                });

                $('#city-id').selectpicker('refresh');
                $('#area-id').selectpicker('refresh');

                if (pre_selected) {
                    $('#city-id').selectpicker('val', pre_selected);
                    $('#city-id').change();
                }

                if (sessionPHP.paraments.invoicing.business_billing_default == "") {
                    var area = areas[$('#area-id option:selected').val()];
                    if (typeof area !== 'undefined') {

                        var business_billing_default = null;

                        if (area.hasOwnProperty("business_billing_default")) {
                            business_billing_default = area.business_billing_default;
                        }

                        if (business_billing_default == null) {
                            business_billing_default = Object.keys(business)[0];
                        }

                        $('#business-billing').val(business_billing_default);
                    }
                } else {
                    $("#business-billing").val(sessionPHP.paraments.invoicing.business_billing_default);
                }
            });

            $('#city-id').change(function() {

                $('#area-id').empty();

                var city_id = $(this).val();

                $.each(province_selected.cities, function(i, city) {

                    if (city.id == city_id) {

                        city_selected = city;

                        $.each(city_selected.areas, function(i, area) {

                            $('#area-id').append("<option value=" + area.id + ">" + area.name + "</option>");

                            if (sessionPHP.paraments.invoicing.business_billing_default == "") {

                                var area = areas[area.id];

                                var business_billing_default = null;

                                if (area.hasOwnProperty("business_billing_default")) {
                                    business_billing_default = area.business_billing_default;
                                }

                                if (business_billing_default == null) {
                                    business_billing_default = Object.keys(business)[0];
                                }

                                $('#business-billing').val(business_billing_default);
                            } else {
                                $("#business-billing").val(sessionPHP.paraments.invoicing.business_billing_default);
                            }
                        });
                    }
                });

                $('#area-id').selectpicker('refresh');

                if (sessionPHP.paraments.invoicing.business_billing_default == "") {

                    var area = areas[$('#area-id option:selected').val()];
                    var business_billing_default = null;

                    if (area.hasOwnProperty("business_billing_default")) {
                        business_billing_default = area.business_billing_default;
                    }

                    if (business_billing_default == null) {
                        business_billing_default = Object.keys(business)[0];
                    }

                    $('#business-billing').val(business_billing_default);
                } else {
                    $("#business-billing").val(sessionPHP.paraments.invoicing.business_billing_default);
                }
            });

            $('#country-id').selectpicker('val', sessionPHP.paraments.customer.country_default);
            $('#country-id').selectpicker('refresh');
            $('#country-id').change();

            var mode_migration =  sessionPHP.paraments.system.mode_migration;

            if (mode_migration) {
                $('#created-datetimepicker').datetimepicker({
                    defaultDate: new Date(),
                    format: 'DD/MM/YYYY'
                });
            } else {
                $('#input-created').remove();
            }

            initMap();

            $("#area-id").change(function() {

                if (sessionPHP.paraments.invoicing.business_billing_default == "") {
                    var area = areas[$(this).val()];

                    var business_billing_default = null;

                    if (area.hasOwnProperty("business_billing_default")) {
                        business_billing_default = area.business_billing_default;
                    }
                    if (business_billing_default == null) {
                        business_billing_default = Object.keys(business)[0];
                    }
                    $('#business-billing').val(business_billing_default);
                } else {
                    $("#business-billing").val(sessionPHP.paraments.invoicing.business_billing_default);
                }
            });

            if (sessionPHP.paraments.invoicing.business_billing_default != '') {
                $('#business-billing').val(sessionPHP.paraments.invoicing.business_billing_default);
            } else {
                var area = areas[$('#area-id').val()];

                var business_billing_default = null;

                if (area.hasOwnProperty("business_billing_default")) {
                    business_billing_default = area.business_billing_default;
                }

                if (business_billing_default != null) {
                    $('#business-billing').val(business_billing_default);
                } else {
                    $("#business-billing").val(Object.keys(business)[0]);
                }
            }
        });

        $('#update-map').click(function() {
            initMap();
        });

        $('form input').on('keypress', function(e) {
            return e.which !== 13;
        });

        var map = null;
        var geocoder = null;
        var marker = null;

        function isEmpty(val) {
            return (val === undefined || val == null || val == 0 || val.length <= 0) ? true : false;
        }

    	function initMap() {

    		geocoder = new google.maps.Geocoder;

    		var myLatlng = {lat: paraments.system.map.lat, lng: paraments.system.map.lng};

    		var mapOptions = {
    			zoom: 15,
    			center: myLatlng,
    			mapTypeId: 'hybrid'   // roadmap, satellite, hybrid, terrain 
    		};

    		map = new google.maps.Map(document.getElementById('map'), mapOptions);

    		document.getElementById('submit-map').addEventListener('click', function() {
    			geocodeAddress(geocoder, map);
    		});

    		var init = {lat: paraments.system.map.lat, lng: paraments.system.map.lng};

    		var haveMarker = false;
            if (!isEmpty(customer.lat) || !isEmpty(customer.lng)) {
                haveMarker = true;
                init.lat = customer.lat;
                init.lng = customer.lng;
            }

            var latLng = new google.maps.LatLng(init.lat, init.lng);
            if (haveMarker) {
               addMarker(latLng);
            }

            var latLng = new google.maps.LatLng(init.lat, init.lng);

            map.setCenter(latLng);

    		map.addListener('click', function(event) {
    			addMarker(event.latLng);	    
    		});
        }

        function refreshMap() {
            google.maps.event.trigger(map, 'resize');
        }

        $('input#addressmap').keypress(function(event) {

            if ( event.which == 13 ) {
                geocodeAddress(geocoder, map);
            }
        });

    	function addMarker(location) {

    		if (marker != null) {
    			clearMarkers();
    		}

    		marker = new google.maps.Marker({
    			position: location,
    			map: map
    		});
    		marker.setMap(map);	

            var lat = location.lat();

            var lng = location.lng();

    		$('#coodmapslat').val(lat);
    		$('#coodmapslng').val(lng);	
    	}

    	function clearMarkers() {
    		marker.setMap(null);
    	}

    	function geocodeAddress(geocoder, map) {

    		var address = document.getElementById('addressmap').value;

    		geocoder.geocode({
    			'address': address
    			}, function(results, status) {

    				if (status === google.maps.GeocoderStatus.OK) {	
    					addMarker(results[0].geometry.location)  	    
    					map.setCenter(results[0].geometry.location);
    					map.setZoom(14);

        			} else {
        				if (status == google.maps.GeocoderStatus.ZERO_RESULTS) {
        			        bootbox.alert('No se encontraron Resultados');
        			    } else {
        			        window.alert('Geocode was not successful for the following reason: ' + status);
        			    }
        			}
    		});
    	}

    </script>

    <script type='text/javascript'>

        function initialize() {

            var input = document.getElementById('addressmap');

            var autocomplete = new google.maps.places.Autocomplete(input);
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>

<?php $this->end(); ?>
