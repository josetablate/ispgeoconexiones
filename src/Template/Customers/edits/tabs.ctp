<style type="text/css">

    .nav-link {
        cursor: pointer;
    }

    .nav-tabs .nav-link:focus, .nav-tabs .nav-link:hover {
        border-color: #686868 #686868 #686868;
    }

    .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
        border-color: #f05f40 #f05f40 #fff;
    }

    .nav-tabs {
        border-bottom: 1px solid #f05f40;
    }

</style>

<ul class="nav nav-tabs" id="myTab" role="tablist">

    <li class="nav-item">
        <a class="nav-link active" id="main-tab" data-target="#main" role="tab" aria-controls="main" aria-expanded="true">Básicos</a>
    </li>
    <li class="nav-item">
        <a class="nav-link " id="extras-tab" data-target="#extras" role="tab" aria-controls="extras" aria-expanded="true">Extras</a>
    </li>

</ul>

<?= $this->Form->create($customer, ['id' => 'form-customer-edit']) ?>

    <div class="tab-content" id="myTabContent" >

        <div class="tab-pane fade show active" id="main" role="tabpanel" aria-labelledby="main-tab">
            <?= $this->fetch('main') ?>
        </div>
        <div class="tab-pane fade  " id="extras" role="tabpanel" aria-labelledby="extras-tab">
            <?= $this->fetch('extras') ?>
        </div>

    </div>
    <div class="col-xl-12 mt-3">
        <?php if ($presale): ?>
            <?= $this->Form->button(__('Volver'), ['class' => 'btn-default margin-left-15', 'id' => 'btn-back', 'type' => 'button']) ?>
            <?= $this->Form->button(__('Guardar'), ['class' => 'btn-submit btn-success margin-left-15', 'name' => 'btn-edit', 'value' => 'presale' ]) ?>
        <?php else: ?>
            <?= $this->Html->link(__('Cancelar'),["controller" => "customers", "action" => "index"], ['title' => 'Ir a la lista de Clientes', 'class' => 'btn btn-default margin-left-15']) ?>
            <?= $this->Form->button(__('Guardar'), ['class' => 'btn-submit btn-success margin-left-15', 'name' => 'btn-edit', 'value' => 'customer' ]) ?>
        <?php endif; ?>
    </div>

<?= $this->Form->end() ?> 

<?php
    echo $this->element('modal_preloader');
?>

<script type="text/javascript">

    function validateCBU(cbu) {
        
        if (cbu.length < 22) {
            e.preventDefault();
            return false;
        }

        var ponderador;
        ponderador = '97139713971397139713971397139713';

        var i;
        var nDigito;
        var nPond;
        var bloque1;
        var bloque2;

        var nTotal;
        nTotal = 0;

        bloque1 = '0' + cbu.substring(0, 7);

        for (i = 0; i <= 7; i++) {
            nDigito = bloque1.charAt(i);
            nPond = ponderador.charAt(i);
            nTotal = nTotal + (nPond * nDigito) - ((Math.floor(nPond * nDigito / 10)) * 10);
        }

        i = 0;

        while ( ((Math.floor((nTotal + i) / 10)) * 10) != (nTotal + i) ) {
            i = i + 1;
        }

        // i = digito verificador

        if (cbu.substring(7, 8) != i) {
            e.preventDefault();
            return false;
        }

        nTotal = 0;

        bloque2 = '000' + cbu.substring(8, 21);

        for (i = 0; i <= 15; i++) {
            nDigito = bloque2.charAt(i);
            nPond = ponderador.charAt(i);
            nTotal = nTotal + (nPond * nDigito) - ((Math.floor(nPond * nDigito / 10)) * 10);
        }

        i = 0;

        while ( ((Math.floor((nTotal + i) / 10)) * 10) != (nTotal + i) ) {
            i = i + 1;
        }

        // i = digito verificador

        if (cbu.substring(21, 22) != i) {
            e.preventDefault();
            return false;
        } 

        return true;
    }

    $(document).ready(function() {

        $('form input').on('keypress', function(e) {
            return e.which !== 13;
        });

        $('#btn-back').click(function(e) {
            window.history.back();
        })

        $('#myTab a').click(function (e) {

          e.preventDefault();

          var target = $(this).data('target');
          
          $('#myTab a.active').removeClass('active');

          $('#myTabContent .active').fadeOut(100, function() {

              $('#myTabContent .active').removeClass('active');

               $(target).fadeIn(100, function() {
                   $(target).addClass('active show');
                   $(target + '-tab').addClass('active');
                   $(target + '-tab').trigger('shown.bs.tab');
               });
          });
        });

        $('.btn-submit').click(function(e) {

            if (!$("form#form-customer-edit")[0].checkValidity()) {
                var message = "Debe completar los siguientes campos: ";
                $.each($("form#form-customer-edit :invalid"), function( index, value ) {
                    message += '*' + value.labels[0].textContent + '<br>';
                });
                generateNoty('warning', message);
            }
        });

        $('#form-customer-edit').submit(function(e) {

        });
    });

</script>
