<?php $this->extend('/Customers/edits/tabs'); ?>

<?php $this->start('extras'); ?>

    <style type="text/css">

        .sub-title {
            border-bottom: 1px solid #e3e2e2;
        }

        .my-hidden {
            display: none;
        }

        #map {
    		height: 370px;
    	}

    	#floating-panel {
    		top: 10px;
    		left: 25%;
    		z-index: 5;
    		background-color: #fff;
    		padding: 5px;
    		border: 1px solid #999;
    		text-align: center;
    		font-family: 'Roboto','sans-serif';
    		line-height: 30px;
    		padding-left: 10px;
    	}

    	#submit-map {
            padding: 10px;
            top: 2px;
        }

        #update-map {
            padding: 10px;
            top: 2px;
        }

    </style>

    <div class="row mt-3">

        <div class="col-xl-7">

            <div class="card bg-light" >

                <div class="card-body">

                    <div class="row">

                        <div class="col-xl-6">
                            <?php
                                echo $this->Form->input('cond_venta', ['label' => 'Condición de Venta (Facturación) ', 'options' => $this->request->getSession()->read('afip_codes')['cond_venta'] , 'value' => $customer->cond_venta]);
                            ?>
                        </div>

                        <div class="col-xl-6" >
                            <?php
                                echo $this->Form->input('denomination', ['label' => 'Denominación', 'type' => 'checkbox', 'checked' => $customer->denomination]);
                            ?>
                        </div>

                        <div class="col-xl-6">
                            <?php
                                echo $this->Form->input('business_billing', ['label' => 'Facturar con', 'options' => $business, 'required' => true, 'value' => $customer->business_billing ]);
                            ?>
                        </div>

                        <div class="col-xl-3">
                            <?php
                                echo $this->Form->input('phone_alt', ['label' => 'Teléfono Alt.',  'value' => $customer->phone_alt]);
                            ?>
                        </div>

                        <div class="col-xl-3">
                            <?php
                                echo $this->Form->input('daydue', ['label' => 'Día de Vencimiento', 'min' => 1, 'max' => 28, 'required' => true, 'value' => $customer->daydue]);
                            ?>
                        </div>

                        <div class="col-xl-6">
                            <div class="form-group text">
                                <label class="control-label" for="email">Email</label>
                                <input type="text" name="email" id="email" class="form-control" value="<?= $customer->email ?>">
                                <small class="text-muted">
                                      Es posible agregar varios correos separados por coma (sin las comillas) <b>","</b> por ej.: julano@correo.com,mengano@correo.com
                                </small>
                            </div>
                        </div>

                        <div class="col-xl-6">
                            <?php
                                echo $this->Form->input('seller', ['label' => 'Vendedor', 'required' => true, 'value' => $customer->seller]);
                            ?>
                        </div>

                        <div class="col-xl-6">
                            <?php
                                echo $this->Form->input('clave_portal', ['label' => 'Clave portal', 'required' => true, 'value' => $customer->clave_portal]);
                            ?>
                        </div>
                        
                         <div class="col-xl-6">
                            <?php
                            if($this->request->getSession()->read('paraments')->gral_config->value_service_customer){
                                echo $this->Form->input('value_service', ['label' => 'Valor de servicio', 'required' => false, 'type' => 'number', 'value' => $customer->value_service,  'min' => 0.01, 'step' => 0.01]);
                            }
                            ?>
                        </div>

                        <div class="col-xl-12">
                            <?php
                                echo $this->Form->input('comments', ['label' => 'Comentarios', 'type' => 'textarea', 'value' => $customer->comments]);
                            ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-5">

            <div class="card bg-light">

                <div class="card-body">

                    <h5 class="card-title">Bloqueo Automático de Conexiones</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Configuración especifica para este cliente</h6>

                    <?php if ($this->request->getSession()->read('paraments')->automatic_connection_blocking->force): ?>
                    <p class="card-subtitle mb-2 text-muted text-naranja">Estos valores no se tomarán en cuenta por que la configuración general esta forzada.</p>
                    <?php endif;?>

                    <div class="row">

                        <div class="col-xl-6">
                            <?php
                                echo $this->Form->input('acb_enabled', ['label' => 'Habilitado', 'type' => 'checkbox', 'checked' => $customer->acb_enabled]);
                            ?>
                        </div>

                        <div class="col-xl-12">
                            <legend>Control</legend>
                        </div>

                        <?php if ($paraments->gral_config->billing_for_service): ?>
                        <div class="col-xl-6">
                            <?php
                                echo $this->Form->input('acb_type', ['label' => 'Tipo', 'options' => ['connection' => 'Por conexión', 'customers' => 'Por cliente'],  'value' => $customer->acb_type, 'disabled' => $customer->acb_enabled ? '' : 'disabled']);
                            ?>
                        </div>
                        <?php endif; ?>

                        <div class="col-xl-6">
                            <?php
                               echo $this->Form->input('acb_due_debt', [ 'label' => 'Deuda vencida', 'value' => $customer->acb_due_debt, 'step' => 0.01, 'min' => 0, 'disabled' => $customer->acb_enabled ? '' : 'disabled']);
                            ?>
                        </div>

                        <div class="col-xl-6">
                            <?php
                              echo $this->Form->input('acb_invoice_no_paid', ['label' => 'Facturas impagas', 'value' => $customer->acb_invoice_no_paid, 'min' => 0, 'disabled' => $customer->acb_enabled ? '' : 'disabled']);
                            ?>
                        </div>

                        <div class="col-xl-12">
                            <legend class="mb-2 text-muted text-naranja">Recargo por reconexión automático</legend>
                        </div>

                        <div class="col-xl-6">
                           <?= $this->Form->input('acb_surcharge_enabled', ['type' => 'checkbox', 'label' => 'Aplicar recargo','checked' => $this->request->getSession()->read('paraments')->automatic_connection_blocking->surcharge_enabled, 'disabled' => $customer->acb_enabled ? '' : 'disabled']); ?>
                        </div> 

                    </div>

                </div>
            </div>
        </div>

    </div>

    <script type="text/javascript">

        $(document).ready(function () {

            var parament = <?= json_encode($paraments) ?>;

            $('#acb-enabled').change(function() {

                if ($(this).is(":checked")) {

                    // control
                    if (parament.gral_config.billing_for_service) {
                        $("#acb-type").attr("disabled", false);
                    }
                    $("#acb-due-debt").attr("disabled", false);
                    $("#acb-invoice-no-paid").attr("disabled", false);

                    //Recargo por reconexión automático
                    $("#acb-surcharge-enabled").attr("disabled", false);
                } else {

                    // control
                    if (parament.gral_config.billing_for_service) {
                        $("#acb-type").attr("disabled", true);
                    }
                    $("#acb-due-debt").attr("disabled", true);
                    $("#acb-invoice-no-paid").attr("disabled", true);

                    //Recargo por reconexión automático
                    $("#acb-surcharge-enabled").attr("disabled", true);
                }
            });

        });

    </script>

<?php $this->end(); ?>
