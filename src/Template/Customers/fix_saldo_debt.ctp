<style type="text/css">

    .comment-text {
        display: block;
        text-overflow: ellipsis;
        word-wrap: break-word;
        overflow: hidden;
        line-height: 1.8em;
        max-height: 115px;
        overflow-y: auto;
    }

    .border-bottom {
        border-bottom: 1px solid #dee2e6!important;
    }

</style>

<?php if ($paraments->customer->search_list_customers): ?>

<div class="modal fade" id="modal-customer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                
                <div class="row">
                    <div class="col-12">
                        <button type="button" class="close" aria-label="Close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        <?= $this->element('customer_list_seeker') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php else: ?>

<div class="modal fade" id="modal-customer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                
                <div class="row">
                    <div class="col-12">
                        <button type="button" class="close" aria-label="Close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        <?= $this->element('customer_seeker') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php endif; ?>

<div class="row">

   <div class="col-sm-12 col-md-6 col-lg-5 col-xl-4">

        <div class="card border-secondary mb-3" id="customer-info">

            <div class="card-header">

                 <div class="row">
                    <div class="col-6">
                        <button type="button" class="btn btn-default " data-toggle="modal" data-target="#modal-customer">
                          Buscar Cliente
                        </button>
                    </div>
                    <div class="col-3 d-none">
                        <button type="button" title="Observación" class="btn btn-default btn-observations float-right my-hidden"><span class="fas fa-file-signature" aria-hidden="true"></span></button>
                    </div>
                    <div class="col-3 d-none">
                        <button type="button" title="Editar Cliente" class="btn btn-default btn-edit-customer float-right my-hidden"><span class="glyphicon icon-pencil2" aria-hidden="true"></span></button>
                    </div>
                </div>
            </div>
            <div class="card-block card-data-block p-4">

                <legend class="sub-title-sm">Cliente seleccionado</legend>

                <div class="row">

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Nombre') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right" id="name"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Código') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right" id="code"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Documento') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right" id="ident"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Teléfono') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right" id="phone"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Domicilio') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right" id="address"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Presupuesto') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right" id="is_presupuesto"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Denominación') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right" id="denomination"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Facturar con') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right" id="business_billing"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Etiquetas') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right" id="labels"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-6 border-bottom">
                                <span class="font-weight-bold"><?= __('Comentario') ?></span>
                            </div>
                            <div class="col-6 border-bottom">
                                <span class="text-right comment-text" id="comments"></span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-12">
                </div>
            </div>
        </div>

    </div>

    <div class="col-sm-12 col-md-6 col-lg-7 col-xl-8">

        <div class="card border-secondary mb-3" >
            <h5 class="card-header"> 2. Aplicar ajuste </h5>
            <div class="card-body text-secondary pb-0">

                <?= $this->Form->create($customer, ['id' => 'form-fix-saldo', 'class' => 'mb-0']) ?>

                <fieldset>

                    <div class="row">
                        <?php if ($paraments->gral_config->billing_for_service): ?>
                        <div class="col-12 billing-_for-service-elements">
                             <?php echo $this->Form->input('connection_id', ['options' => [], 'label' => 'Servicios y productos']); ?>
                        </div>
                        <?php endif; ?>
                        <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                            <?php echo $this->Form->input('alicuot', ['options' => $alicuotas_types, 'label' =>  'Alícuota', 'required' => true, 'value' => 5]); ?>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                            <?php echo $this->Form->input('value', ['type' => 'number', 'label' => 'Importe', 'min' => 0.01, 'step' => 0.01, 'required' => true]); ?>
                        </div>
                        
                        <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                            <label  class="control-label">Período</label> 
                            <div class='input-group date' id='period-datetimepicker'>
                                <input name='period' id="period" type='text' class="form-control" required />
                                <span class="input-group-addon input-group-text calendar">
                                    <span class="glyphicon icon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        
                        <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
                            <label  class="control-label">Vencimiento</label> 
                            <div class='input-group date' id='duedate-datetimepicker'>
                                <input name='duedate' id="duedate" type='text' class="form-control" required />
                                <span class="input-group-addon input-group-text calendar">
                                    <span class="glyphicon icon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        
                        <div class="col-xl-12">
                            <?php echo $this->Form->input('concept', ['label' => 'Concepto', 'required' => true, "autocomplete" => "off"]); ?>
                            <?php echo $this->Form->input('customer_code_selected_from', ['type' => 'hidden', 'label' => false, 'value' => '', 'required' => true]); ?>
                        </div>
                    </div>

                </fieldset>

                <?= $this->Form->button(__('Aplicar'), ['class' => 'btn-submit btn-success float-right mb-4']) ?>

                <?= $this->Form->end() ?> 
 
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">

    var customer_code = <?= json_encode($customer_code) ?>;
    var customer_load = <?= json_encode($customer_load) ?>;

    $(document).ready(function() {

        $('#period-datetimepicker').datetimepicker({
            locale: 'es',
            defaultDate: new Date(),
            format: 'MM/YYYY',
            icons: {
                time: "glyphicon icon-alarm",
                date: "glyphicon icon-calendar",
                up: "glyphicon icon-arrow-right",
                down: "glyphicon icon-arrow-left"
            }
        });

        $('#duedate-datetimepicker').datetimepicker({
            locale: 'es',
            defaultDate: <?= json_encode($customer->duedate) ?>,
            format: 'DD/MM/YYYY',
            icons: {
                time: "glyphicon icon-alarm",
                date: "glyphicon icon-calendar",
                up: "glyphicon icon-arrow-right",
                down: "glyphicon icon-arrow-left"
            }
        });
        
        function afterSelectedCustomer() {
            $('#customer-code-selected-from').val(customer_selected.code); 
            clear_form();

            $('.btn-edit-customer').removeClass('my-hidden');
            $('.btn-observations').removeClass('my-hidden');
            var doc_type_name = sessionPHP.afip_codes.doc_types[customer_selected.doc_type];
            $('#customer-info #name').html(customer_selected.name);
            $('#customer-info #code').html(customer_selected.code);
            var ident = "";
            if (customer_selected.ident) {
                ident = customer_selected.ident;
            }
            $('#customer-info #ident').html(doc_type_name + ' ' + ident);
            $('#customer-info #phone').html(customer_selected.phone);
            $('#customer-info #address').html(customer_selected.address);
            var is_presupuesto = customer_selected.is_presupuesto ? 'Si' : 'No';
            $('#customer-info #is_presupuesto').html(is_presupuesto);
            var denomination = customer_selected.denomination ? 'Si' : 'No';
            $('#customer-info #denomination').html(denomination);
            $('#customer-info #comments').html(customer_selected.comments);
            var business_name = '';
            $.each(sessionPHP.paraments.invoicing.business, function (i, b) {
                if (b.id == customer_selected.business_billing) {
                    business_name = b.name;
                }
            });
            $('#customer-info #business_billing').html(business_name);
            $.each(customer_selected.labels, function( index, label ) {
                $('#customer-info #labels').append("<span class='badge m-1' style='background-color: " + label.color_back + "; color: " + label.color_text + "' >" + label.text + "</span>");
            });

            business_selected = null;

            $.each(sessionPHP.paraments.invoicing.business, function(i, val) {

                if (customer_selected.business_billing == val.id) {
                    business_selected = val;
                }
            });

            if (business_selected) {
                
                $('#type').empty();

                if (business_selected.responsible == 1) { //ri

                    $('#type').append('<option value="">Seleccione Tipo</option>');

                    if (business_selected.webservice_afip.crt != '') {
                        $('#type').append('<optgroup label="Nota Débito"><option value="NDX">NOTA DE DÉBITO X</option><option value="002">NOTA DE DÉBITO A</option><option value="007">NOTA DE DÉBITO B</option></optgroup>');
                    } else {
                        $('#type').append('<optgroup label="Nota Débito"><option value="NDX">NOTA DE DÉBITO X</option>/optgroup>');
                    }
                } else if (business_selected.responsible == 6) { //monotributerou

                    $('#type').append('<option value="">Seleccione Tipo</option>');

                    if (business_selected.webservice_afip.crt != '') {
                        $('#type').append('<optgroup label="Nota Débito"><option value="NDX">NOTA DE DÉBITO X</option><option value="012">NOTA DE DÉBITO C</option></optgroup>');
                    } else {
                        $('#type').append('<optgroup label="Nota Débito"><option value="NDX">NOTA DE DÉBITO X</option></optgroup>');
                    }
                }
            }

            $.ajax({
                url: "<?= $this->Url->build(['controller' => 'Connections', 'action' => 'getConnectionsSelectByCustomer']) ?>",
                type: 'POST',
                dataType: "json",
                data: JSON.stringify({customer_code: customer_selected.code }),
                success: function(data) {

                     $('#connection-id').prop('required', true);
                     $('#connection-id').find('option').remove();

                     if (customer_selected.billing_for_service) {

                        $('.billing-_for-service-elements').show();

                        $('#connection-id').append('<option value="">Seleccione Servicio</option>');
                        $.each(data.connectionsArray, function(i, val) {

                            if (i != 0 && !(val.indexOf('Eliminado') != -1)) {
                                $('#connection-id').append('<option value="' + i + '">' + val + '</option>');
                            }
                        });

                        $.each(data.connectionsArray, function(i, val) {

                            if (i != 0 && val.indexOf('Eliminado') != -1) {
                                $('#connection-id').append('<option value="' + i + '">' + val + '</option>');
                            }
                        });

                        $('#connection-id').append('<option value="0">Otras ventas</option>');
                        $('#connection-id').change();

                    } else {
                        $('.billing-_for-service-elements').hide();
                        $('#connection-id').append('<option value="0">Otras ventas</option>');
                        $('#connection-id').val(0);
                        $('#connection-id').change();
                    }
                }, 
                error: function (jqXHR) {
                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al intentar obtener las conexiones del cliente.');
                    }
                }
            });

            $('#modal-customer').modal('hide');
            clear_form();
        }

        $('#card-customer-seeker').on('CUSTOMER_SELECTED', function() {
            afterSelectedCustomer();
        });

        $('#form-fix-saldo').submit(function(event ) {
            if ($('#customer-code-selected-from').val()) {
                return true;
            } else {
                event.preventDefault();
                generateNoty('warning', 'Debe seleccionar un cliente');
            }
        });
        
        $('#modal-customer').on('shown.bs.modal', function (e) {


            if (sessionPHP.paraments.customer.search_list_customers) {
                
                enableLoad = 1;
                table_customers.draw();
            } else {
                clear_card_customer_seeker();
                $('#card-customer-seeker input#customer-ident').focus();
            }
        });

        // para mostrar info del cliente al cual recien se le genero un pago
        if (customer_load != null) {
            customer_selected = customer_load;
            afterSelectedCustomer();
        }
     });

     function clear_form() {
         $('#type').val('');
         $('#business-id').val('');
         $('#alicuot').val(5);
         $('#value').val('');
         $('#concept').val('');
     }

</script>
