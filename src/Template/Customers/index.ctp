<style type="text/css">

    .font-size-13px {
        font-size: 13px;
    }

    .table-connections th {
        background-color: #607D8B !important;
        color: white !important;
    }

    .table-hover tbody tr:hover {
        background-color: white !important;
        color: black !important;
    }

    .table-hover tbody tr.selectable:hover {
        background-color: rgba(105, 104, 104, 0.28) !important;
    }

</style>

    <div id="btns-tools">
        <div class="text-right btns-tools margin-bottom-5">

            <?php 

                echo $this->Html->link(
                    '<span class="fas fa-sync-alt" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Actualizar la tabla',
                    'class' => 'btn btn-default btn-update-table ml-1',
                    'escape' => false
                ]);

                echo $this->Html->link(
                    '<span class="glyphicon icon-eye" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Ocultar o mostrar columnas',
                    'class' => 'btn btn-default btn-hide-column ml-1',
                    'escape' => false
                ]);

                echo $this->Html->link(
                    '<span class="glyphicon fas fa-search" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Buscar por columnas',
                    'class' => 'btn btn-default btn-search-column ml-1',
                    'escape' => false
                ]);

                echo $this->Html->link(
                    '<span class="glyphicon icon-user-plus" aria-hidden="true"></span>',
                    ['controller' => 'customers', 'action' => 'add'],
                    [
                    'title' => 'Agregar nuevo cliente',
                    'class' => 'btn btn-default btn-add ml-1',
                    'escape' => false
                ]);

                echo $this->Html->link(
                    '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Exportar tabla',
                    'class' => 'btn btn-default btn-export ml-1',
                    'escape' => false
                ]);

            ?>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-hover" id="table-customers">
                <thead>
                    <tr>
                        <th></th>                             <!--0-->
                        <th >Creado</th>                      <!--1-->
                        <th >Código</th>                      <!--2-->
                        <th >Cuenta</th>                      <!--3-->
                        <th >Etiquetas</th>                   <!--4-->
                        <th >Impagas</th>                     <!--5-->
                        <th class="debt_month">Saldo mes</th> <!--6-->
                        <th >Nombre</th>                      <!--7-->
                        <th >Tipo</th>                        <!--8-->
                        <th >Nro</th>                         <!--9-->
                        <th >Servicios</th>                   <!--10-->
                        <th >Domicilio</th>                   <!--11-->
                        <th >Área</th>                        <!--12-->
                        <th >Teléfono</th>                    <!--13-->
                        <th >Vendedor</th>                    <!--14-->
                        <th >Resp. Iva</th>                   <!--15-->
                        <th >Comp.</th>                       <!--16-->
                        <th >Deno.</th>                       <!--17-->
                        <th >Correo electrónico</th>          <!--18-->
                        <th >Comentarios</th>                 <!--19-->
                        <th >Día Venc.</th>                   <!--20-->
                        <th >Clave Portal</th>                <!--21-->
                        <th >Empresa Facturación</th>         <!--22-->
                        <th >Compromiso de Pago</th>          <!--23-->
                        <th >Cuentas</th>                     <!--24-->
                        <th >Controladores</th>               <!--25-->
                        <th >Servicios</th>                   <!--26-->
                        <th >Valor de servicio</th>           <!--26-->
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                        <th class="pt-1 pb-1"></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   => 'btn-info',
        'name' => 'Ficha del Cliente',
        'icon' => 'icon-eye',
        'type' => 'btn-secondary'
    ];

    $buttons[] = [
        'id'   => 'btn-edit',
        'name' => 'Editar',
        'icon' => 'icon-pencil2',
        'type' => 'btn-secondary'
    ];

    $buttons[] = [
        'id'   => 'btn-label',
        'name' => 'Etiquetas',
        'icon' => 'fas fa-tag',
        'type' => 'btn-secondary'
    ];

    $buttons[] = [
        'id'   => 'btn-delete',
        'name' => 'Archivar',
        'icon' => 'fas fa-archive',
        'type' => 'btn-warning'
    ];

    $buttons[] = [
        'id'   => 'btn-payment-commitment',
        'name' => 'Compromiso de Pago',
        'icon' => 'far fa-handshake',
        'type' => 'btn-info'
    ];

    $buttons[] = [
        'id'   => 'btn-observations',
        'name' => 'Observaciones',
        'icon' => 'far fa-edit',
        'type' => 'btn-dark'
    ];

    echo $this->element('actions', ['modal'=> 'modal-customers', 'title' => 'Acciones', 'buttons' => $buttons ]);
    echo $this->element('labels', ['modal'=> 'modal-labels-customers',  'buttons' => $labels, 'save' => true  ]);
    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-customers']);
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-customers']);
    echo $this->element('modal_preloader');

    echo $this->element('Observations/add', ['modal' => 'modal-add-observation', 'title' => 'Agregar Observación', 'records' => TRUE]);
    echo $this->element('Observations/edit', ['modal' => 'modal-edit-observation', 'title' => 'Observación']);
    echo $this->element('PaymentCommitment/general', ['modal' => 'modal-payment-commitment', 'title' => 'Compromiso de Pago', 'records' => TRUE]);
?>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-info-connection',
        'name' =>  'Información',
        'icon' =>  'icon-eye',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-edit-connection',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-disable-connection',
        'name' =>  'Bloquear',
        'icon' =>  'glyphicon icon-lock',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-enable-connection',
        'name' =>  'Habilitar',
        'icon' =>  'glyphicon icon-unlocked',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-go-ip',
        'name' =>  'Abrir Antena',
        'icon' =>  'fas fa-broadcast-tower',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-view-realTimeGraph',
        'name' =>  'Gráfica en tiempo real',
        'icon' =>  'fas fa-chart-area',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-view-speed',
        'name' =>  'Gráfica historial',
        'icon' =>  'glyphicon icon-plus',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete-connection',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    $buttons[] = [
        'id'   =>  'btn-sync',
        'name' =>  'Sincronizar',
        'icon' =>  'icon-tab',
        'type' =>  'btn-warning'
    ];

    echo $this->element('actions', ['modal'=> 'modal-connections', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<div class="tooltip" role="tooltip">
    <div class="arrow">
    </div>
    <div class="tooltip-inner">
    </div>
</div>

<script type="text/javascript">

    var table_customers = null;
    var table_selected = null;
    var customer_selected = null;
    var areas = null;
    var services = null;
    var users = null;
    var accounts = null;
    var controllers = null;

    var curr_pos_scroll = null;

    var labels = <?= json_encode($labels) ?>;
    var connection_selected = null;

    function format(d) {

        if (d.connections.length > 1) {
            //ordena historial del ticket por fecha created de mayor a menor
            d.connections.sort(function(a, b) {
                // Turn your strings into dates, and then subtract them
                // to get a value that is either negative, positive, or zero.
                return new Date(b.created) - new Date(a.created);
            });
        }

        var html = '<br><table class="table table-bordered table-connections mr-5">'
            + '<thead>'
                + '<tr>'
                  + '<th style="width: 8%">Fecha</th>'
                  + '<th>Servicio</th>'
                  + '<th>Domicilio</th>';
                  if (sessionPHP.paraments.gral_config.billing_for_service) {
                      html += '<th>Saldo mes</th>';
                  }
                  html +=
                + '</tr>'
              + '</thead>'
            + '<tbody>';

        $.each(d.connections, function(i, connection) {

            if (!connection.deleted) {

                var created = '';
                var class_locked = "";
                if (connection.created) {
                    created = connection.created.split('T')[0];
                    created = created.split('-');
                    created = created[2] + '/' + created[1] + '/' + created[0];
                }

                if (connection.enabled == 0) {
                    class_locked = '"bg-danger text-white"';
                }

                html +=
                '<tr data-id=' + connection.id + ' class=' + class_locked + '>'
                  + '<td>' + created + '</td>'
                  + '<td>' + services[connection.service_id] + '</td>'
                  + '<td style="text-align: left !important;">' + connection.address + '</td>';
                  if (sessionPHP.paraments.gral_config.billing_for_service) {
                      html += '<td>$' + connection.debt_month.toFixed(2) + '</td>';
                  }
                  html +=
                + '</tr>';
            }
        });

        html += '</tbody>'
            + '</table><br>';

        return html;
    }

    $(document).ready(function () {

        var options_business = [];

        $.each(sessionPHP.paraments.invoicing.business, function(i, val) {
            if(val.enable){
                options_business.push({id:val.id, name:val.name + ' (' + val.address + ')'});
            }            
        });

        areas = <?= json_encode($areas) ?>;
        services = <?= json_encode($services) ?>;
        users = <?= json_encode($users) ?>;
        accounts = <?= json_encode($accounts) ?>;
        controllers = <?php echo json_encode($controllers); ?>;

        $('.btn-hide-column').click(function() {
            $('.modal-hide-columns-customers').modal('show');
        });

        $('.btn-search-column').click(function() {
            $('.modal-search-columns-customers').modal('show');
        });

        $('#table-customers').removeClass('display');

        $('#btns-tools').hide();

        var hide_force = [];
        var no_seach = [];

        if (!sessionPHP.paraments.accountant.account_enabled) {
            hide_force = [3];
            no_seach = [3];
        }

        loadPreferences('customers-index5', [3, 11, 13, 14, 15, 16, 17, 18, 19, 20, 21, 27]);

		table_customers = $('#table-customers').DataTable({
		    "order": [[ 1, 'desc' ]],
            "deferRender": true,
            "processing": true,
            "serverSide": true,
		    "ajax": {
                "url": "/ispbrain/customers/get_customers.json",
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "drawCallback": function( c ) {
                var flag = true;
            	switch (c.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
	            if (flag) {
	                if (table_customers) {

                        var tableinfo = table_customers.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }

                        if (curr_pos_scroll) {
                            $(table_customers.settings()[0].nScrollBody).scrollTop( curr_pos_scroll.top );
                            $(table_customers.settings()[0].nScrollBody).scrollLeft( curr_pos_scroll.left );
                        }
                    }
	            }

	            this.api().columns('.debt_month').every(function() {
                    var column = this;

                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    var sum = column
                        .data()
                        .reduce(function (a, b) {

                           return intVal(a) + intVal(b);
                        }, 0);

                    $(column.footer()).html('$' + sum.toFixed(2));
                });
            },
            "scrollY": true,
		    "scrollY": '365px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
		    "columns": [
		        {
                    "className": 'details-control ',
                    "orderable": false,
                    "data": 'connections',
                    "render": function ( data, type, row ) {
                        if (row.services_total > 0) {
                            return "<a href='javascript:void(0)' class='grey' title='Servicios' data-toggle='tooltip_services'>"
                                + "<span class='icon-plus' aria-hidden='true'>"
                                + "</span>"
                                + "</a>";
                        }

                        return '';
                    },
                    "width": '3%'
                },
                {
                    "data": "created",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                {
                    "data": "code",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                {
                    "data": "account_code",
                    "type": "string"
                },
                {
                    "data": "status",
                    "type": "options_colors",
                    "options": labels,
                    "render": function ( data, type, row ) {
                        if (row.labels.length > 0) {
                            var spans = "";
                            var names = "";
                            $.each(row.labels, function(i, val) {

                                names += "<span>" + val.text + "</span><br>"; 
                                spans += "<span class='status' style='background-color: " + val.color_back + "' > </span>"; 
                            });
                            return "<div data-toggle='tooltip_etiquetas' title='" + names + "'>" + spans + "</div>";
                        }
                        return '';
                    }
                },
                {
                    "data": "invoices_no_paid",
                    "type": "decimal",
                    "render": function ( data, type, row ) {
                       if (row.invoices_no_paid > 0) {
                           return "<span class='badge badge-danger font-size-13px' >" + row.invoices_no_paid + "</span>";
                       }
                       return "<span class='badge badge-success font-size-13px'>" + row.invoices_no_paid + "</span>";
                    }
                    
                },
                {
                    "data": "debt_month",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' ),
                },
                { 
                    "data": "name",
                    "type": "string",
                },
                {
                    "data": "doc_type",
                    "type": "options",
                    "options": sessionPHP.afip_codes.doc_types,
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.doc_types[data];
                    }
                },
                {
                    "data": "ident",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (data) {
                            ident = data;
                        }
                        return ident;
                    }
                },
                {
                    "data": "services_active",
                    "type": "options",
                    "options": {  
                        "0":"Bloqueado",
                        "1":"Habilitado"
                    },
                    "render": function ( data, type, row ) {
                        return "<span class='font-size-13px'>" + row.services_active + '/' + row.services_total + "</span>";
                    }
                },
                {
                    "data": "address",
                    "type": "string",
                },
                {
                    "data": "area_id",
                    "type": "options",
                    "options": areas,
                    "render": function ( data, type, row ) {
                        return data ? areas[data] : '';
                    }
                },
                {
                    "data": "phone",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        if (row.phone_alt != null && row.phone_alt != "") {
                            data += ' | ' + row.phone_alt;
                        }
                        return data;
                    }
                },
                {
                    "data": "seller",
                    "type": "string",
                },
                {
                    "data": "responsible",
                    "type": "options",
                    "options": sessionPHP.afip_codes.responsibles,
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.responsibles[data];
                    }
                },
                {
                    "data": "is_presupuesto",
                    "type": "options",
                    "options": {  
                        "0":"Factura",
                        "1":"Presupuesto",
                    },
                    "render": function ( data, type, row ) {
                        if (data) {
                            return 'PRESU';
                        }
                        return 'FACTURA';
                    }
                },
                {
                    "data": "denomination",
                    "type": "options",
                    "options": {  
                        "0":"No Denominado",
                        "1":"Denominado",
                    },
                    "render": function ( data, type, row ) {
                        if (data) {
                            return 'SI';
                        }
                        return 'NO';
                    }
                },
                {
                    "data": "email",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                {
                    "data": "comments",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                {
                    "data": "daydue",
                    "type": "integer",
                },
                {
                    "data": "clave_portal",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                { 
                    "data": "business_billing",
                    "type": "options_obj",
                    "options": options_business,
                    "render": function ( data, type, row ) {
                        var business_name = '';
                        $.each(options_business, function (i, b) {
                            if (b.id == data) {
                                business_name = b.name
                            }
                        });
                        return business_name;
                    }
                },
                {
                    "data": "payment_commitment",
                    "type": "date",
                    "display_search_text": "C. Pago",
                    "render": function ( data, type, row ) {
                        var created = '';
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                        return created;
                    }
                },
                {
                    "data": "customers_accounts",
                    "type": "options",
                    "orderable": false,
                    "options": accounts,
                    "render": function ( data, type, row ) {
                        if (row.customers_accounts.length > 0) {
                            var accounts_name = "";
                            $.each(row.customers_accounts, function( index, value ) {
                                accounts_name += accounts[value.payment_getway_id] + ' - ';
                            });
                            return accounts_name;
                        }
                        return '';
                    }
                },
                {
                    "className":"row-control",
                    "data": "controller",
                    "type": "options",
                    "orderable": false,
                    "options": controllers,
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                {
                    "className":"row-control",
                    "data": "service",
                    "orderable": false,
                    "type": "options",
                    "options": services,
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                {
                    "data": "value_service",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' ),
                },
            ],
		    "columnDefs": [
                {
                    "targets": [1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 18, 20, 21, 23],
                    "width": '5%'
                },
                {
                    "targets": [22],
                    "width": '8%'
                },
                {
                    "targets": [10, 18],
                    "width": '13%'
                },
                {
                    "targets": [7, 24],
                    "width": '12%'
                },
                { 
                    "class": "left", targets: [7, 11]
                },
                { 
                    "visible": false, targets: hide_columns
                },
                { 
                    "visible": false, targets: hide_force
                },
                { "orderable": false, "targets": [4, 10] }
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.code;
                $(row).addClass('selectable');
            },
		    "language": dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
        	"dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
		}).on( 'draw.dt', function () {
            $('[data-toggle="tooltip_etiquetas"]').tooltip({html: true});
            $('[data-toggle="tooltip_services"]').tooltip({html: true});
        });

		$('.btn-update-table').click(function() {
             if (table_customers) {
                table_customers.ajax.reload();
            }
        });

        $('#table-customers_wrapper .tools').append($('#btns-tools').contents());

        $('#table-customers').on( 'init.dt', function () {
            createModalHideColumn(table_customers, '.modal-hide-columns-customers', hide_force);
            createModalSearchColumn(table_customers, '.modal-search-columns-customers', no_seach);
            loadSearchPreferences('customers-index5-search');
        });

        $('#btns-tools').show();

        $('#table-customers tbody').on( 'click', 'tr td', function (e) {

            if (!$(this).hasClass('details-control')) {

                if ($(this).parent().hasClass('selectable')) {

                    if (!$(this).parent().find('.dataTables_empty').length) {

                        table_customers.$('tr.selected').removeClass('selected');
                        $(this).parent().addClass('selected');

                        customer_selected = table_customers.row( this ).data();

                        if (customer_selected.services_total > 0) {
                            $('#btn-payment-commitment').removeClass('d-none');
                        } else {
                            $('#btn-payment-commitment').addClass('d-none');
                        }

                        $('.modal-customers').modal('show');
                    }
                }
            }
        });

        $('#table-customers tbody').on('click', 'td.details-control', function () {

            if (!$(this).parent().closest('tr').find('.dataTables_empty').length) {
                table_customers.$('tr.selected').removeClass('selected');
                $(this).parent().closest('tr').addClass('selected');
            }

            customer_selected = table_customers.row( this ).data();

            if (customer_selected.services_total > 0) {

                customer_selected = table_customers.row( this ).data();

                var tr = $(this).closest('tr');
                var row = table_customers.row( tr );
                if ( row.child.isShown() ) {
                    row.child.hide();
                    tr.removeClass('shown');
                    $(this).find('span').removeClass('icon-minus');
                    $(this).find('span').addClass('icon-plus');
                } else {
                    row.child( format(row.data()) ).show();
                    tr.addClass('shown');
                    $(this).find('span').removeClass('icon-plus');
                    $(this).find('span').addClass('icon-minus');
                }
            }
        });

        $('#btn-info').click(function() {
            var action = '/ispbrain/customers/view/' + table_customers.$('tr.selected').attr('id');
            window.open(action, '_blank');
        });

        $('#btn-edit').click(function() {
            var action = '/ispbrain/customers/edit/' + table_customers.$('tr.selected').attr('id');
            window.open(action, '_black');
        });

        $('#btn-print-resume').click(function() {
            var url = "/ispbrain/customers/printresume/" + table_customers.$('tr.selected').attr('id');
            window.open(url, '_blank');
        });

        $('#btn-delete').click(function() {

            var text = '¿Está Seguro que desea eliminar el cliente?';
            var controller  = 'customers';
            var id  = table_customers.$('tr.selected').attr('id');

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/delete/', 'method': 'post', 'id': 'replacer'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer').submit();
                }
            });
        });

        $('#btn-label').click(function() {

            $('.modal-labels-customers .check').addClass('d-none');

            $.each(customer_selected.labels, function(c, label) {

                $('.modal-labels-customers .label-btn').each(function(i, btn) {

                    if (label.id == $(this).data('labelid')) {
                        $(this).find('.check').removeClass('d-none');
                    }
                });
            });

            $('.modal-customers').modal('hide');
            $('.modal-labels-customers').modal('show');
        });

        $('.modal-labels-customers #label-save').click(function() {

            var send_data = {};
            send_data.customer_code = customer_selected.code; 
            send_data.labels = [];

            $('.modal-labels-customers .label-btn').each(function(a, lala) {

                if (!$(this).find('.check').hasClass('d-none')) {
                    send_data.labels.push($(this).data('labelid'));
                }
            });

            curr_pos_scroll = {
                'top': $(table_customers.settings()[0].nScrollBody).scrollTop(),
                'left': $(table_customers.settings()[0].nScrollBody).scrollLeft()
            };

            $.ajax({
                url: "<?= $this->Url->build(['controller' => 'customers', 'action' => 'changeLabels']) ?>",
                type: 'POST',
                dataType: "json",
                data: JSON.stringify(send_data),
                success: function(data) {

                    if (data.customer) {

                        customer_selected = data.customer;

                        table_customers.row( $('#table-customers tr#' + customer_selected.code) ).data( customer_selected ).draw();

                        $('.modal-labels-customers').modal('hide');
                    }
                },
                error: function (jqXHR) {

                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al intentar cambiar las etiquetas');
                    }
                }
            });
        });

        $('#btn-observations').click(function() {
            customer_code = customer_selected.code
            $('.modal-customers').modal('hide');
            $('#modal-add-observation').modal('show');
        });

    });

    $(document).click(function() {
         $('[data-toggle="tooltip_etiquetas"]').tooltip('hide');
         $('[data-toggle="tooltip_services"]').tooltip('hide');
    });

    $(".btn-export").click(function() {

        bootbox.confirm('Se descargará un archivo Excel', function(result) {
            if (result) {
                $('#table-customers').tableExport({tableName: 'Clientes', type:'excel', escape:'false', columnNumber: [5]});
            }
        }).find("div.modal-content").addClass("confirm-width-md");
    });

    $("#btn-payment-commitment").click(function() {
        table_selected = table_customers;
        $('.modal-customers').modal('hide');
        $('.modal-payment-commitment').modal('show');
    });

    $('#table-customers').on("click", '.table-connections tr', function() {

        var connection_id = $(this).data('id');

        if (typeof connection_id !== "undefined") {
            $.each(customer_selected.connections, function(index, value) {

                if (value.id == connection_id) {
                    connection_selected = value;
                }
            });

            var service_name = services[connection_selected.service_id];

            $('.modal-connections').modal('show');
            $('.modal-connections .modal-title').html("");
            $('.modal-connections .modal-title').append('Acciones <br> <span style="font-size: 15px"> <b>Servicio: </b>' + service_name + '</span>');
        }
    });

    $('#btn-info-connection').click(function() {
    	var action = '/ispbrain/connections/view/' + connection_selected.id;
    	window.open(action, '_blank');
    });

    $('#btn-edit-connection').click(function() {
    	var action = '/ispbrain/connections/edit/' + connection_selected.id;
    	window.open(action, '_blank');
    });

    $('#btn-disable-connection').click(function() {

    	var text = '¿Está Seguro que desea bloquear está Conexión?';
    	var controller = 'connections';
    	var id = connection_selected.id;

    	bootbox.confirm(text, function(result) {
    		if (result) {
    			$('body')
    				.append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/disable/', 'method': 'post', 'id': 'replacer'})
    				.append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
    				.append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
    				.find('#replacer').submit();
    		}
    	});
    });

    $('#btn-enable-connection').click(function() {

    	var text = "¿Está Seguro que desea habilitar está Conexión?";
    	var id  = connection_selected.id;

    	bootbox.confirm(text, function(result) {
    		if (result) {
    			$('body')
    				.append( $('<form/>').attr({'action': '/ispbrain/connections/enable/', 'method': 'post', 'id': 'form-block'})
    				.append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
    				.append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
    				.append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
    				.find('#form-block').submit();
    		}
    	});
    });

    $('#btn-go-ip').click(function() {
    	var ip = connection_selected.ip;
    	window.open("http://" + ip, '_blank');
    });

    $('#btn-view-speed').click(function() {
    	var action = '/ispbrain/ConnectionsGraphic/queueHistoryGraph/' + connection_selected.id;
    	window.open(action, '_blank');
    });

    $('#btn-view-realTimeGraph').click(function() {
    	var action = '/ispbrain/ConnectionsGraphic/queueRealTimeGraph/' + connection_selected.id;
    	window.open(action, '_blank');
    });

    $('#btn-delete-connection').click(function() {

    	var text = '¿Está Seguro que desea eliminar la Conexión?';
    	var controller = 'connections';
    	var id = connection_selected.id;

    	bootbox.confirm(text, function(result) {
    		if (result) {
    			$('body')
    				.append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/delete/', 'method': 'post', 'id': 'replacer'})
    				.append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
    				.append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
    				.append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
    				.find('#replacer').submit();
    		}
    	});
    });

    $('#btn-sync').click(function() {
        var action = '/ispbrain/connections/syncConnection/' + connection_selected.id;
        window.open(action, '_self');
    });

</script>
