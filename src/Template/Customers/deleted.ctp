<style type="text/css">

    .status {
        border-radius: 4px;
        display: inline-block;
        font-weight: bold;
        color: white;
        line-height: 1em;
        font-size: 10.5px;
        padding: 3px 0;
        margin: 0px 0px 0px 0px;
        width: 100%;
    }

    .cp {
        border: 1px solid #f38a73;
        background-color: #f38a73;
    }

    .cc {
        border: 1px solid gray;
        background-color: gray;
    }

    .i {
        border: 1px solid purple;
        background-color: purple;
    }

    .plus1M {
        border: 1px solid #f05f40;
        background-color: #f05f40;
        width: 80%;
    }

    .plus2M {
          border: 1px solid #f05f40;
         background-color: #f05f40;
             width: 80%;
    }

    .plus3M {
          border: 1px solid #f05f40;
         background-color: #f05f40;
          width: 80%;    
    }

    #modal-filter .form-group {
        margin-bottom: 4px;
    }

    .my-hidden {
        display: none;
    }

    .dataTables_processing {
        padding: 0px !important;
    }

    .font-size-13px {
        font-size: 13px;
    }

</style>

<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5">

        <?php

            echo $this->Html->link(
                '<span class="glyphicon icon-eye" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Ocultar o Mostrar Columnas',
                'class' => 'btn btn-default btn-hide-column ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon fas fa-search"  aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Buscar por Columnas',
                'class' => 'btn btn-default btn-search-column ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-user-plus"  aria-hidden="true"></span>',
                ['controller' => 'customers', 'action' => 'add'],
                [
                'title' => 'Agregar nuevo cliente',
                'class' => 'btn btn-default btn-add ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel"  aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export ml-1',
                'escape' => false
            ]);

        ?>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-hover" id="table-customers">
            <thead>
                <tr>
                    <th></th>                             <!--0-->
                    <th >Creado</th>                      <!--1-->
                    <th >Cód.</th>                        <!--2-->
                    <th >Cta</th>                         <!--3-->
                    <th >Etiq.</th>                       <!--4-->
                    <th >Impagas</th>                     <!--5-->
                    <th class="debt_month">Saldo mes</th> <!--6-->
                    <th >Nombre</th>                      <!--7-->
                    <th >Tipo</th>                        <!--8-->
                    <th >Nro</th>                         <!--9-->
                    <th >Serv.</th>                       <!--10-->
                    <th >Dom.</th>                        <!--11-->
                    <th >Área</th>                        <!--12-->
                    <th >Tel.</th>                        <!--13-->
                    <th >Vendedor</th>                    <!--14-->
                    <th >Resp. Iva</th>                   <!--15-->
                    <th >Comp.</th>                       <!--16-->
                    <th >Deno.</th>                       <!--17-->
                    <th >Correo electrónico</th>          <!--18-->
                    <th >Comentarios</th>                 <!--19-->
                    <th >Día Venc.</th>                   <!--20-->
                    <th >Clave Portal</th>                <!--21-->
                    <th >Emp. Facturación</th>            <!--22-->
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<?php 

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-restore',
        'name' =>  'Restaurar',
        'icon' =>  'icon-undo2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   => 'btn-info',
        'name' => 'Ficha del Cliente',
        'icon' => 'icon-eye',
        'type' => 'btn-secondary'
    ];

    $buttons[] = [
        'id'   => 'btn-edit',
        'name' => 'Editar',
        'icon' => 'icon-pencil2',
        'type' => 'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-label',
        'name' =>  'Etiquetas',
        'icon' =>  'fas fa-tag',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-super-delete',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    $buttons[] = [
        'id'   => 'btn-observations',
        'name' => 'Observaciones',
        'icon' => 'far fa-edit',
        'type' => 'btn-dark'
    ];

    echo $this->element('actions', ['modal'=> 'modal-customers-deleted', 'title' => 'Acciones', 'buttons' => $buttons ]);
    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-customers-deleted']);
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-customers-deleted']);
    echo $this->element('labels', ['modal'=> 'modal-labels-customers',  'buttons' => $labels, 'save' => true ]);
    echo $this->element('Observations/add', ['modal' => 'modal-add-observation', 'title' => 'Agregar Observación', 'records' => TRUE]);
    echo $this->element('Observations/edit', ['modal' => 'modal-edit-observation', 'title' => 'Observación']);
?>

<script type="text/javascript">

    var table_customers = null;
    var customer_selected = null;
    var areas = null;
    var services = null;
    var users = null;

    var curr_pos_scroll = null;

    var labels = <?= json_encode($labels) ?>;

    $(document).ready(function () {

        var options_business = [];

        $.each(sessionPHP.paraments.invoicing.business, function(i, val) {
            options_business.push({id:val.id, name:val.name + ' (' + val.address + ')'});
        });

        areas = <?= json_encode($areas) ?>;
        services = <?= json_encode($services) ?>;
        users = <?= json_encode($users) ?>;

        $('.btn-hide-column').click(function() {
            $('.modal-hide-columns-customers-deleted').modal('show');
        });

        $('.btn-search-column').click(function() {
            $('.modal-search-columns-customers-deleted').modal('show');
        });

        $('#table-customers').removeClass('display');

        $('#btns-tools').hide();

        var hide_force = [];
        var no_seach = [];

        if (!sessionPHP.paraments.accountant.account_enabled) {
            hide_force = [3];
            no_seach = [3];
        }

        loadPreferences('customers-deleted1', [3, 9, 11, 13, 14, 15, 16, 17, 18, 19, 20, 21]);

		table_customers = $('#table-customers').DataTable({
		    "order": [[ 1, 'desc' ]],
            "deferRender": true,
            "processing": true,
            "serverSide": true,
		    "ajax": {
                "url": "/ispbrain/customers/get_customers_deleted.json",
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "drawCallback": function( settings ) {
                var flag = true;
            	switch (settings.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
                    if (table_customers) {

                        var tableinfo = table_customers.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }
                    }
            	}
            },
            "scrollY": true,
		    "scrollY": '430px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
		    "columns": [
		        {
                    "orderable": false,
                    "data": 'code',
                    "render": function ( data, type, full, meta ) {
                        return '';
                    },
                    "width": '3%'
                },
                { 
                    "data": "created",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                { 
                    "data": "code",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                { 
                    "data": "account_code",
                    "type": "string"
                },
                {
                    "data": "status",
                    "type": "options_colors",
                    "options": labels,
                    "render": function ( data, type, row ) {
                        if (row.labels.length > 0) {
                            var spans = "";
                            var names = "";
                            $.each(row.labels, function(i, val) {

                                names += "<span>" + val.text + "</span><br>"; 
                                spans += "<span class='status' style='background-color: " + val.color_back + "' > </span>"; 
                            });
                            return "<div data-toggle='tooltip_etiquetas' title='" + names + "'>" + spans + "</div>";
                        }
                        return '';
                    }
                },
                { 
                    "data": "invoices_no_paid",
                    "type": "decimal",
                    "render": function ( data, type, row ) {
                       if (row.invoices_no_paid > 0) {
                           return "<span class='badge badge-danger font-size-13px' >" + row.invoices_no_paid + "</span>";
                       }
                       return "<span class='badge badge-success font-size-13px'>" + row.invoices_no_paid + "</span>";
                    }
                    
                },
                { 
                    "data": "debt_month",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' ),
                },
                { 
                    "data": "name",
                    "type": "string",
                },
                {
                    "data": "doc_type",
                    "type": "options",
                    "options": sessionPHP.afip_codes.doc_types,
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.doc_types[data];
                    }
                },
                { 
                    "data": "ident",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (data) {
                            ident = data;
                        }
                        return ident;
                    }
                },
                { 
                    "data": "services_active",
                    "render": function ( data, type, row ) {
                       return "<span class='font-size-13px'>" + row.services_active + '/' + row.services_total + "</span>";
                    }
                    
                },
                { 
                    "data": "address",
                    "type": "string",
                },
                {
                    "data": "area_id",
                    "type": "options",
                    "options": areas,
                    "render": function ( data, type, row ) {
                        return data ? areas[data] : '';
                    }
                },
                {
                    "data": "phone",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        if (row.phone_alt != null && row.phone_alt != "") {
                            data += ' | ' + row.phone_alt;
                        }
                        return data;
                    }
                },
                { 
                    "data": "seller",
                    "type": "string",
                },
                { 
                    "data": "responsible",
                    "type": "options",
                    "options": sessionPHP.afip_codes.responsibles,
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.responsibles[data];
                    }
                },
                { 
                    "data": "is_presupuesto",
                    "type": "options",
                    "options": {  
                        "0":"Factura",
                        "1":"Presupuesto",
                    },
                    "render": function ( data, type, row ) {
                        if (data) {
                            return 'PRESU';
                        }
                        return 'FACTURA';
                    }
                },
                { 
                    "data": "denomination",
                    "type": "options",
                    "options": {  
                        "0":"No Denominado",
                        "1":"Denominado",
                    },
                    "render": function ( data, type, row ) {
                        if (data) {
                            return 'SI';
                        }
                        return 'NO';
                    }
                },
                { 
                    "data": "email",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                { 
                    "data": "comments",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                { 
                    "data": "daydue",
                    "type": "integer",
                },
                { 
                    "data": "clave_portal",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                { 
                    "data": "business_billing",
                    "type": "options_obj",
                    "options": sessionPHP.paraments.invoicing.business,
                    "render": function ( data, type, row ) {
                        var business_name = '';
                        $.each(sessionPHP.paraments.invoicing.business, function (i, b) {
                            if (b.id == data) {
                                business_name = b.name;
                            }
                        });
                        return business_name;
                    }
                },
               
            ],
		    "columnDefs": [
		        {
                "targets": [1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 18, 20],
                    "width": '5%'
                },
                {
                    "targets": [22],
                    "width": '8%'
                },
                {
                    "targets": [10, 18],
                    "width": '13%'
                },
                {
                    "targets": [7],
                    "width": '12%'
                },
                { 
                    "class": "left", targets: [7, 11]
                },
                { 
                    "visible": false, targets: hide_columns
                },
                { 
                    "visible": false, targets: hide_force
                },
                { "orderable": false, "targets": [4, 10] }
            ],
            "createdRow" : function( row, data, index ) {
               row.id = data.code;
            },
		    "language": dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
        	"dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
		});

        $('#table-customers_wrapper .tools').append($('#btns-tools').contents());

        $('#table-customers').on( 'init.dt', function () {
            createModalHideColumn(table_customers, '.modal-hide-columns-customers-deleted', hide_force);
            createModalSearchColumn(table_customers, '.modal-search-columns-customers-deleted', no_seach);
            loadSearchPreferences('customers-deleted1-search');
        });

        $('#btns-tools').show();

        $('#table-customers tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                if (sessionPHP.paraments.accountant.account_enabled) {
                    $('#btn-super-delete').hide();
                }

                table_customers.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');

                customer_selected = table_customers.row( this ).data();

                $('.modal-customers-deleted').modal('show');
            }
        });

        $('#btn-restore').click(function() {
            var action = '/ispbrain/customers/restore/' + table_customers.$('tr.selected').attr('id');
            window.open(action, '_self');
        });

        $('#btn-observations').click(function() {
            customer_code = customer_selected.code
            $('.modal-customers-deleted').modal('hide');
            $('#modal-add-observation').modal('show');
        });
    });

    $('#btn-label').click(function() {

        $('.modal-labels-customers .check').addClass('d-none');

        $.each(customer_selected.labels, function(c, label){

            $('.modal-labels-customers .label-btn').each(function(i, btn) {

                if (label.id == $(this).data('labelid')) {
                    $(this).find('.check').removeClass('d-none');
                }
            });
        });

        $('.modal-customers').modal('hide');
        $('.modal-labels-customers').modal('show');
    });

    $('#btn-info').click(function() {
        var action = '/ispbrain/customers/view/' + table_customers.$('tr.selected').attr('id');
        window.open(action, '_blank');
    });

    $('#btn-edit').click(function() {
        var action = '/ispbrain/customers/edit/' + table_customers.$('tr.selected').attr('id');
        window.open(action, '_black');
    });

    $('.modal-labels-customers #label-save').click(function() {

            var send_data = {};
            send_data.customer_code = customer_selected.code; 
            send_data.labels = [];

            $('.modal-labels-customers .label-btn').each(function(a, lala) {

                if (!$(this).find('.check').hasClass('d-none')) {
                    send_data.labels.push($(this).data('labelid'));
                }
            });

            curr_pos_scroll = {
                'top': $(table_customers.settings()[0].nScrollBody).scrollTop(),
                'left': $(table_customers.settings()[0].nScrollBody).scrollLeft()
            };

            $.ajax({
                url: "<?= $this->Url->build(['controller' => 'customers', 'action' => 'changeLabels']) ?>",
                type: 'POST',
                dataType: "json",
                data: JSON.stringify(send_data),
                success: function(data) {

                    if (data.customer) {

                        customer_selected = data.customer;

                        table_customers.row( $('#table-customers tr#' + customer_selected.code) ).data( customer_selected ).draw();

                        $('.modal-labels-customers').modal('hide');
                    }
                },
                error: function (jqXHR) {

                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('error', 'Error al intentar cambiar las etiquetas');
                    }
                }
            });
        });

    $(".btn-export").click(function() {

        bootbox.confirm('Se descargará un archivo Excel', function(result) {
            if (result) {
                $('#table-customers').tableExport({tableName: 'Clientes Eliminados', type:'excel', escape:'false', columnNumber: [4, 21]});
            }
        }).find("div.modal-content").addClass("confirm-width-md");

    });

    $('#btn-super-delete').click(function() {

            var text = '¿Está Seguro que desea eliminar el Cliente?';
            var controller  = 'customers';
            var id  = table_customers.$('tr.selected').attr('id');

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/superDelete/', 'method': 'post', 'id': 'replacer'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer').submit();
                }
            });

        });

</script>
