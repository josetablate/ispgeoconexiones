<?php $this->extend('/Customers/views/observations'); ?>

<?php $this->start('logs'); ?>

<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5" >

        <?php
            echo $this->Html->link(
                '<i class="fas fa-sync-alt"></i>',
                'javascript:void(0)',
                [
                'title' => 'Actualizar la tabla',
                'class' => 'btn btn-default btn-refresh',
                'escape' => false
            ]);
  
             echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar en formato excel el contenido de la tabla',
                'class' => 'btn btn-default btn-export-logs ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon fas fa-search" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Buscar por columnas',
                'class' => 'btn btn-default btn-search-column-logs ml-1',
                'escape' => false
            ]);
        ?>

    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <table id="table-action-log" class="table table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Fecha</th>
                    <th>Usuario</th>
                    <th>Acción</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-view',
        'name' =>  'Detalle',
        'icon' =>  'icon-eye',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-action_log', 'title' => 'Acciones', 'buttons' => $buttons ]);
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-action-log']);
    echo $this->element('modal_preloader');

?>

<div class="modal fade" id="modal-view" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Detalle</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-xl-12">

                         <textarea name="" id="data" rows="10" readonly class="w-100">
                         </textarea>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var table_action_log = null;
    var action_log_selected = null;
    var curr_pos_scroll = null;
    var users = null;
    var customer = null;
    var customer_code = 0;

    $(document).ready(function () {

        users = <?= json_encode($users_logs) ?>;
        customer = <?= json_encode($customer) ?>;
        customer_code = customer.code;

        $('#table-action-log').removeClass( 'display' );

		table_action_log = $('#table-action-log').DataTable({
		    "order": [[ 0, 'desc' ]],
    	    "autoWidth": true,
    	    "scrollY": '550px',
    	    "scrollCollapse": true,
    	    "scrollX": true,
    	    "sWrapper":  "dataTables_wrapper dt-bootstrap4",
            "processing": true,
            "serverSide": true,
		    "ajax": {
                "url": "/ispbrain/ActionLog/getActionsLogsFromCustomer.json",
                "dataFilter": function(data) {
                    var json = $.parseJSON(data);
                    return JSON.stringify(json.response);
                },
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "customer_code": customer_code
                    });
                },
                "error": function(c) {
                    switch (c.status) {
                        case 400:
                            flag = false;
                            generateNoty('warning', 'Ha ocurrido un error.');
                            break;
                        case 401:
                            flag = false;
                            generateNoty('warning', 'Error, no posee una autorización.');
                            break;
                        case 403:
                            flag = false;
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                        	setTimeout(function() { 
                                window.location.href = "/ispbrain";
                        	}, 3000);
                            break;
                    }
                }
            },
            "drawCallback": function(c) {

                var flag = true;
                switch (c.jqXHR.status) {
                    case 400:
                        flag = false;
                        break;
                    case 401:
                        flag = false;
                        break;
                    case 403:
                        flag = false;
                        break;
                }
                if (flag) {
                    if (table_action_log) {

                        var tableinfo = table_action_log.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column-logs').addClass('search-apply');
                        } else {
                            $('.btn-search-column-logs').removeClass('search-apply');
                        }

                        if (curr_pos_scroll) {
                            $(table_action_log.settings()[0].nScrollBody).scrollTop(curr_pos_scroll.top);
                            $(table_action_log.settings()[0].nScrollBody).scrollLeft(curr_pos_scroll.left);
                        }
                    }
                }
            },
		    "columns": [
		        { 
		            "data": "id",
		            "type": "integer"
		        },
                { 
                    "data": "created",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            var time = data.split('T')[1];
                            time = time.split(':');
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0] + ' ' + time[0] + ':' + time[1];
                        }
                    }
                },
                { 
                    "data": "user_id",
                    "type": "options",
                    "options": users,
                    "render": function ( data, type, row ) {
                        var u = "";
                        if (typeof data !== "undefined") {
                            $.each(users, function( index, value ) {
                                if (data == index) {
                                    u = value;
                                }
                            });
                        }
                        return u;
                    }
                },
                {
                    "data": "action",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        var action = data;
                        if (row.main) {
                            action = '<b>' + action + '</b>';
                        }
                        return action;
                    }
                },
            ],
		    "columnDefs": [
		        { 
		            "type": 'datetime-custom', targets: [1] 
		        },
		        { 
                    "class": "left", targets: [3]
                },
            ],
            "createdRow" : function( row, data, index ) {
                $(row).addClass('selectable');
                row.id = data.id;
            },
		    "language": dataTable_lenguage,
            "lengthMenu": [[100], [100]],
		});

		$('#table-action-log_filter').closest('div').before($('#btns-tools').contents());

		$('#table-action-log').on( 'init.dt', function () {
            createModalSearchColumn(table_action_log, '.modal-search-columns-action-log');
        });

        $('#btns-tools').show();

    });

    $(".btn-export-logs").click(function() {

        bootbox.confirm('Se descargará un archivo Excel', function(result) {
            if (result) {
                $('#table-action-log').tableExport({tableName: 'Registro de Acciones', type:'excel', escape:'false'});
            }
        }).find("div.modal-content").addClass("confirm-width-md");

    });

    $('#table-action-log tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_action_log.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            action_log_selected = table_action_log.row( this ).data();

            if (action_log_selected.detail) {
               $('.modal-action_log').modal('show'); 
            }
        }
    });

    $('#btn-view').click(function() {

         $('.modal-action_log').modal('hide');
         $('#modal-view #data').val(action_log_selected.detail);
         $('#modal-view').modal('show');

    });

    $('.btn-refresh').click(function() {
        table_action_log.ajax.reload();
        table_action_log.draw();
    });

    $('.btn-search-column-logs').click(function() {
        $('.modal-search-columns-action-log').modal('show');
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

    $('a[id="logs-tab"]').on('shown.bs.tab', function (e) {
        table_action_log.ajax.reload();
    });

</script>

<?php $this->end(); ?>