<?php $this->extend('/Customers/views/tickets'); ?>

<?php $this->start('accounts'); ?>

    <style type="text/css">

        .lbl-title {
            font-weight: bold !important;
            font-size: 18px;
        }

        .lbl-connection {
            color: #f05f40;
            font-size: 22px;
            font-family: sans-serif;
        }

        .lbl-payment-method {
            color: #5e5f61;
            font-size: 22px;
            font-family: sans-serif;
        }

        .lbl-no-account {
            color: #67635e;
            font-family: sans-serif;
            font-weight: bold;
        }

        .account-contain {
            border: 1px solid #f05f40;
            border-radius: 5px;
            background: #f3f3f3;
            padding: 15px;
        }

        .lbl-connection-account {
            color: #696868 !important
        }

    </style>

    <div class="row">

        <div class="col-md-6 accounts-container">
            <?php if (sizeof($accounts) > 0): ?>
                <?php foreach ($accounts as $account): ?>
                    <div class="col-xl-12 mt-3">
                        <div class="account-contain">
                            <div class="row">
                                <div class="col-xl-12 ">
                                    <?php
                                        $payment_name = "";
                                        $edit = false;
                                        foreach ($payment_getway->methods as $pg) {
                                            if ($pg->id == $account->payment_getway_id) {
                                                $payment_name = $pg->name;
                                                $edit = $pg->edit;
                                            }
                                        }
                                    ?>
                                    <label class="lbl-title"><b class="lbl-connection">Cuenta:</b> <?= $payment_name ?></label>
                                </div>
                                <div class="col-xl-12">
                                    <table class="vertical-table">
                                        <?php if ($account->payment_getway_id == 7): ?>
                                        	<?php echo $this->element('payu/card/info', ['account' => $account]); ?>
                                        <?php endif; ?>
                                        <?php if ($account->payment_getway_id == 99): ?>
                                            <?php echo $this->element('cobrodigital/card/info', ['account' => $account, 'credentials' => $credentials]); ?>
                                        <?php endif; ?>
                                        <?php if ($account->payment_getway_id == 102): ?>
                                            <?php echo $this->element('auto_debits/chubut/info', ['account' => $account]); ?>
                                        <?php endif; ?>
                                        <?php if ($account->payment_getway_id == 104): ?>
                                            <?php echo $this->element('cobrodigital/auto_debit/info', ['account' => $account, 'credentials' => $credentials]); ?>
                                        <?php endif; ?>
                                        <?php if ($account->payment_getway_id == 105): ?>
                                            <?php echo $this->element('cuentadigital/info', ['account' => $account, 'credentials' => $credentials]); ?>
                                        <?php endif; ?>
                                        <?php if ($account->payment_getway_id == 106): ?>
                                            <?php echo $this->element('visa_auto_debit/info', ['account' => $account, 'credentials' => $credentials]); ?>
                                        <?php endif; ?>
                                        <?php if ($account->payment_getway_id == 107): ?>
                                            <?php echo $this->element('mastercard_auto_debit/info', ['account' => $account, 'credentials' => $credentials]); ?>
                                        <?php endif; ?>
                                    </table>
                                </div>
                                <?php if (isset($account->connections) && sizeof($account->connections) > 0): ?>
                                <div class="col-xl-12">
                                    <label class="lbl-title mt-3"><b class="lbl-connection">Utilizan las Conexiones:</b></label>
                                    <?php foreach ($account->connections as $connection): ?>
                                        <p class="font-weight-bold lbl-connection-account"><?= $connection ?></p>
                                    <?php endforeach; ?>
                                </div>
                                <?php endif; ?>
                                <div class="col-xl-12">
                                    <div class="row mt-3">
                                        <?php if ($edit): ?>
                                            <div class="col-xl-3">
                                                <button type="button" data-account-id="<?= $account->id ?>" data-payment-getway-id="<?= $account->payment_getway_id ?>" class="btn btn-primary btn-accounts-selected">Editar</button>
                                            </div>
                                        <?php endif; ?>
                                        <?php if ($account->payment_getway_id == 7): ?>
                                            <div class="col-xl-3">
                                                <button type="button" data-account-id="<?= $account->id ?>" data-payment-getway-id="<?= $account->payment_getway_id ?>" class="btn btn-info btn-liberar">Liberar</button>
                                            </div>
                                        <?php endif; ?>
                                        <?php if ($account->payment_getway_id == 99): ?>
                                            <div class="col-xl-3">
                                                <button type="button" data-account-id="<?= $account->id ?>" data-payment-getway-id="<?= $account->payment_getway_id ?>" class="btn btn-info btn-liberar">Liberar</button>
                                            </div>
                                        <?php endif; ?>
                                        <?php if ($account->deleted): ?>
                                            <div class="col-xl-3">
                                                <button type="button" data-account-id="<?= $account->id ?>" data-payment-getway-id="<?= $account->payment_getway_id ?>" class="btn btn-success btn-dar-alta">Habilitar</button>
                                            </div>
                                        <?php else: ?>
                                            <div class="col-xl-3">
                                                <button type="button" data-account-id="<?= $account->id ?>" data-payment-getway-id="<?= $account->payment_getway_id ?>" class="btn btn-danger btn-dar-baja">Deshabilitar</button>
                                            </div>
                                        <?php endif; ?>
                                        <div class="col-xl-3">
                                            <button type="button" data-account-id="<?= $account->id ?>" data-payment-getway-id="<?= $account->payment_getway_id ?>" class="btn btn-dark btn-delete">Eliminar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <h5 class="mt-3 ml-3 lbl-no-account"><?= __('Sin Cuenta') ?></h5>
            <?php endif; ?>
        </div>

        <div class="col-md-4 mt-3">
            <div class="card">
                <div class="card-body">

                    <?= $this->Form->input('payment_method_id', [
                        'options'  => $payment_methods,
                        'label'    => 'Agregar Cuenta',
                        'required' => false,
                        'value'    => ''
                    ]); ?>

                </div>
            </div>
        </div>

    </div>

    <?php
        echo $this->element('auto_debits/chubut/add', ['modal' => 'modal-add-auto-debit-chubut', 'title' => 'Crear Cuenta - Débito automático Chubut', 'tab' => 'accounts', 'customer' => $customer]);
        echo $this->element('auto_debits/chubut/edit', ['modal' => 'modal-edit-auto-debit-chubut', 'title' => 'Editar Cuenta - Débito automático Chubut', 'tab' => 'accounts', 'customer' => $customer]);
        echo $this->element('cobrodigital/auto_debit/add', ['modal' => 'modal-add-auto-debit-cobrodigital', 'title' => 'Crear Cuenta - Débito automático Cobro digital', 'credentials' => $credentials, 'id_comercios' => $id_comercios, 'customer' => $customer, 'tab' => 'accounts']);
        echo $this->element('cobrodigital/auto_debit/edit', ['modal' => 'modal-edit-auto-debit-cobrodigital', 'title' => 'Editar Cuenta - Débito automático Cobro digital', 'credentials' => $credentials, 'id_comercios' => $id_comercios, 'tab' => 'accounts', 'customer' => $customer]);
        echo $this->element('cobrodigital/card/add', ['modal' => 'modal-add-card-cobrodigital', 'title' => 'Tarjeta Cobro digital', 'customer' => $customer, 'tab' => 'accounts']);
        echo $this->element('payu/card/add', ['modal' => 'modal-add-card-payu', 'title' => 'Crear Cuenta - Tarjeta PayU', 'customer' => $customer, 'tab' => 'accounts']);
        echo $this->element('visa_auto_debit/add', ['modal' => 'modal-add-visa-auto-debit', 'title' => 'Crear Cuenta - Visa Débito Automático', 'customer' => $customer, 'tab' => 'accounts']);
        echo $this->element('mastercard_auto_debit/add', ['modal' => 'modal-add-mastercard-auto-debit', 'title' => 'Crear Cuenta - MasterCard Débito Automático', 'customer' => $customer, 'tab' => 'accounts']);
        echo $this->element('cuentadigital/add', ['modal' => 'modal-generate-barcode-cuentadigital', 'title' => 'Cuenta Digital', 'customer' => $customer, 'tab' => 'accounts']);
    ?>

    <script type="text/javascript">

        var accounts = null;
        var credentials = null;
        var payment_methods = null;
        var cobrodigital_card_selectedx = null;
        var payu_card_selectedx = null;
        var cobrodigital_auto_debit_selectedx = null;
        var cobrodigital_boleta_selectedx = null;
        var auto_debit_chubut_selectedx = null;
        var visa_auto_debit_selectedx = null;
        var mastercard_auto_debit_selectedx = null;
        var cuentadigital_selectedx = null;
        var id_comercios = null;
        var payment_getway = null;

        $(document).on("click", ".btn-dar-alta", function() {
            var customer_code = customer.code;
            var account_id = $(this).data('account-id');
            var payment_getway_id = $(this).data('payment-getway-id');
            var account_selected = null;
            var tab = 'accounts';

            var controller = 'CustomersAccounts';

            var text = '¿Está Seguro que desea habilitar la cuenta?';

            bootbox.confirm(text, function(result) {
                if (result) {

                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/darAlta/', 'method': 'post', 'id': 'replacerDarAlta'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'account_id', 'value': account_id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'payment_getway_id', 'value': payment_getway_id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'customer_code', 'value': customer_code}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'tab', 'value': tab}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacerDarAlta').submit();
                }
            });
        });

        $(document).on("click", ".btn-delete", function() {

            var customer_code = customer.code;
            var account_id = $(this).data('account-id');
            var payment_getway_id = $(this).data('payment-getway-id');
            var account_selected = null;
            var tab = 'accounts';

            var controller = 'CustomersAccounts';
            var text = '¿Está Seguro que desea eliminar la cuenta?';

            bootbox.confirm(text, function(result) {

                if (result) {

                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/delete/', 'method': 'post', 'id': 'replacerDelete'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'account_id', 'value': account_id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'customer_code', 'value': customer_code}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'payment_getway_id', 'value': payment_getway_id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'tab', 'value': tab}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacerDelete').submit();
                }
            });
        });

        $(document).on("click", ".btn-dar-baja", function() {
            var customer_code = customer.code;
            var account_id = $(this).data('account-id');
            var payment_getway_id = $(this).data('payment-getway-id');
            var account_selected = null;
            var tab = 'accounts';

            var controller = 'CustomersAccounts';

            var text = '¿Está Seguro que desea deshabilitar la cuenta?';

            bootbox.confirm(text, function(result) {
                if (result) {

                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/darBaja/', 'method': 'post', 'id': 'replacerDarBaja'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'account_id', 'value': account_id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'customer_code', 'value': customer_code}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'payment_getway_id', 'value': payment_getway_id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'tab', 'value': tab}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacerDarBaja').submit();
                }
            });
        });

        $(document).on("click", ".btn-liberar", function() {
            var customer_code = customer.code;

            var account_id = $(this).data('account-id');
            var payment_getway_id = $(this).data('payment-getway-id');
            var account_selected = null;
            var tab = 'accounts';

            var controller = 'CustomersAccounts';

            var text = '¿Está Seguro que desea liberar la tarjeta de cobranza?';

            bootbox.confirm(text, function(result) {
                if (result) {

                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/freedCard/', 'method': 'post', 'id': 'replacerDarBaja'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'account_id', 'value': account_id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'customer_code', 'value': customer_code}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'payment_getway_id', 'value': payment_getway_id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'tab', 'value': tab}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacerDarBaja').submit();
                }
            });
        });

        $(document).on("click", ".btn-accounts-selected", function() {

            var account_id = $(this).data('account-id');
            var payment_getway_id = $(this).data('payment-getway-id');
            var account_selected = null;

            $.each(accounts, function( index, value ) {

                if (value.id == account_id 
                    && value.payment_getway_id == payment_getway_id) {

                    account_selected = value;

                    switch (payment_getway_id) {

                        case 102:
                            $('#customer-code-edit-auto-debit-chubut').val(account_selected.customer_code);
                            $('#account-id-edit-auto-debit-chubut').val(account_selected.id);
                            $('#payment-getway-id-edit-auto-debit-chubut').val(account_selected.payment_getway_id);
                            $('#firstname-edit-auto-debit-chubut').val(account_selected.firstname);
                            $('#lastname-edit-auto-debit-chubut').val(account_selected.lastname);
                            $('#cuit-edit-auto-debit-chubut').val(account_selected.cuit);
                            $('#cbu-edit-auto-debit-chubut').val(account_selected.cbu);
                            $('.modal-edit-auto-debit-chubut').modal('show');
                            break;

                        case 104:
                            $('#id-comercio-edit-auto-debit-cobrodigital').val(account_selected.id_comercio);
                            $('#id-comercio-edit-auto-debit-cobrodigital').selectpicker('refresh');
                            $('#customer-code-edit-auto-debit-cobrodigital').val(account_selected.customer_code);
                            $('#account-id-edit-auto-debit-cobrodigital').val(account_selected.id);
                            $('#payment-getway-id-edit-auto-debit-cobrodigital').val(account_selected.payment_getway_id);
                            $('#firstname-edit-auto-debit-cobrodigital').val(account_selected.firstname);
                            $('#lastname-edit-auto-debit-cobrodigital').val(account_selected.lastname);
                            $('#email-edit-auto-debit-cobrodigital').val(account_selected.email);
                            $('#cuit-edit-auto-debit-cobrodigital').val(account_selected.cuit);
                            $('#cbu-edit-auto-debit-cobrodigital').val(account_selected.cbu);
                            $('.modal-edit-auto-debit-cobrodigital').modal('show');
                            break;
                    }
                }
            });
        });

        $(document).ready(function() {

            accounts = <?= json_encode($accounts); ?>;
            payment_methods = <?= json_encode($payment_methods) ?>;
            id_comercios = <?= json_encode($id_comercios) ?>;
            credentials = <?= json_encode($credentials) ?>;
            payment_getway = <?= json_encode($payment_getway) ?>;

            $('#payment-method-id').change(function() {

                switch (parseInt($(this).val())) {

                    case 7:
                        // tarjeta payu
                        $('.modal-add-card-payu').modal('show');
                        break;

                    case 99:
                        // tarjeta cobro digital
                        $('.modal-add-card-cobrodigital').modal('show');
                        break;

                    case 102:
                        // debito automatico chubut
                        $('.modal-add-auto-debit-chubut').modal('show');
                        break;

                    case 103:
                        // boleta cobro digital
                        $('.modal-add-boleta-cobrodigital').modal('show');
                        break;

                    case 104:
                        // debito automatico cobro digital
                        $('.modal-add-auto-debit-cobrodigital').modal('show');
                        break;

                     case 105:
                        // cuenta digital
                        $('.modal-generate-barcode-cuentadigital').modal('show');
                        break;

                    case 106:
                        // visa debito automatico
                        $('.modal-add-visa-auto-debit').modal('show');
                        break;

                    case 107:
                        // visa debito automatico
                        $('.modal-add-mastercard-auto-debit').modal('show');
                        break;
                }
            });

            $('#modal-add-card-payu').on('PAYU_CARD_SELECTED_CLOSED', function() {
                if (!payu_card_selected) {
                    $('#payment-method-id').val('');
                }
            });

            $('#modal-add-card-cobrodigital').on('COBRODIGITAL_CARD_SELECTED_CLOSED', function() {
                if (!cobrodigital_card_selected) {
                    $('#payment-method-id').val('');
                }
            });

            $('#modal-add-auto-debit-cobrodigital').on('COBRODIGITAL_AUTO_DEBIT_SELECTED_CLOSED', function() {
                if (!cobrodigital_auto_debit_selected) {
                    $('#payment-method-id').val('');
                }
            });

            $('#modal-add-auto-debit-chubut').on('AUTO_DEBIT_CHUBUT_SELECTED_CLOSED', function() {
                if (!auto_debit_chubut_selected) {
                    $('#payment-method-id').val('');
                }
            });

            $('#modal-add-visa-auto-debit').on('VISA_AUTO_DEBIT_SELECTED_CLOSED', function() {
                if (!visa_auto_debit_selected) {
                    $('#payment-method-id').val('');
                }
            });

            $('#modal-add-mastercard-auto-debit').on('MASTERCARD_AUTO_DEBIT_SELECTED_CLOSED', function() {
                if (!mastercard_auto_debit_selected) {
                    $('#payment-method-id').val('');
                }
            });

            $('#modal-generate-barcode-cuentadigital').on('GENERATE_BARCODE_CUENTADIGITAL_SELECTED_CLOSED', function() {
                if (!cuentadigital_selected) {
                    $('#payment-method-id').val('');
                }
            });
        });

    </script>
<?php $this->end(); ?>
