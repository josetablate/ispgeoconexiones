<?php $this->extend('/Customers/views/tabs'); ?>

<?php $this->start('transactions'); ?>

<style type="text/css">

    .my-hidden {
        display: none;
    }

    .input-debt-month-red {
        font-size: 23px;
        padding: 0 15px 0 0;
        color: #f05f40;
    }

    .input-debt-month-green {
        font-size: 23px;
        padding: 0 15px 0 0;
        color: #28a745;
    }

    .last-cell-saldo-red {
        color: #f05f40;
        font-weight: bold;
    }

    .last-cell-saldo-green {
        color: #28a745;
        font-weight: bold;
    }

    .table-hover tbody tr:hover {
        background-color: white !important;
    }

    .table-hover tbody tr.selectable:hover {
        background-color: rgba(105, 104, 104, 0.28) !important;
    }

</style>

<div id="btns-tools-transactions">

    <div class="text-right btns-tools margin-bottom-5">

        <?php
            echo $this->Html->link(
                '<span class="fas fa-sync-alt" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Actualizar la tabla',
                'class' => 'btn btn-default btn-update-table-transaction ml-1 mt-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon fas fa-search" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Buscar por Columnas',
                'class' => 'btn btn-default btn-search-column-transactions ml-1 mt-1',
                'escape' => false
            ]);

            // echo $this->Html->link(
            //     '<span class="glyphicon icon-eye" aria-hidden="true"></span>',
            //     'javascript:void(0)',
            //     [
            //     'title' => 'Ocultar o Mostrar Columnas',
            //     'class' => 'btn btn-default btn-hide-column-transactions ml-1 mt-1',
            //     'escape' => false
            // ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export-transactions ml-1 mt-1',
                'escape' => false
            ]);
        ?>

    </div>
</div>

<div class="row">

    <div class="col-xl-12">
        <table class="table table-bordered table-hover" id="table-transactions">
            <thead>
                <tr>
                    <th>Fecha</th>                          <!--0-->
                    <th>Método de pago</th>                 <!--1-->
                    <th>Estado</th>                         <!--2-->
                    <th class="text-primary">Código</th>    <!--3-->
                    <th class="text-primary">Nombre</th>    <!--4-->
                    <th class="text-primary">Documento</th> <!--5-->
                    <th>Nro Transacción</th>                <!--6-->
                    <th>Importe</th>                        <!--7-->
                    <th>Comentario</th>                     <!--8-->
                    <th>Cód. Barra</th>                     <!--9-->
                    <th>Nro Recibo</th>                     <!--10-->
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   => 'btn-assign-customer',
        'name' => 'Asignar Pago a Cliente',
        'icon' => 'fas fa-file-invoice-dollar',
        'type' => 'btn-success'
    ];

    $buttons[] = [
        'id'   => 'btn-print-receipt',
        'name' => 'Imprimir Recibo',
        'icon' => 'icon-printer',
        'type' => 'btn-success'
    ];

    echo $this->element('actions', ['modal'=> 'modal-transactions', 'title' => 'Acciones', 'buttons' => $buttons ]);
    //echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-transactions']);
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-transactions']);
?>

<script type="text/javascript">

    var transaction_selected = null;
    var table_transactions = null;

    $(document).ready(function() {

        $('.btn-hide-column-transactions').click(function() {
            $('.modal-hide-columns-transactions').modal('show');
        });

        $('.btn-search-column-transactions').click(function() {
            $('.modal-search-columns-transactions').modal('show');
        });

        $('#table-transactions').removeClass('display');

        $('#btns-tools').hide();

        //loadPreferences('transactions-view', [0, 1, 2, 3, 4, 5, 6]);

        table_transactions = $('#table-transactions').DataTable({
            "deferRender": false,
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "/ispbrain/PaymentMethods/get_transactions.json",
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "where": {
                            "customer_code": customer_selected.code
                        }
                    });
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() {
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	},
            },
            "drawCallback": function( c ) {
                var flag = true;
            	switch (c.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
	            if (flag) {
	                if (table_transactions) {

                        var tableinfo = table_transactions.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }

                        if (curr_pos_scroll) {
                            $(table_transactions.settings()[0].nScrollBody).scrollTop( curr_pos_scroll.top );
                            $(table_transactions.settings()[0].nScrollBody).scrollLeft( curr_pos_scroll.left );
                        }
                    }
	            }
            },
		    "scrollY": true,
		    "scrollY": '260px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
		    "ordering": false,
		    "columns": [
		        {
		            "class": "font-weight-bold",
                    "data": "date",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var date = data.split('T')[0];
                            date = date.split('-');
                            return date[2] + '/' + date[1] + '/' + date[0];
                        }
                    }
                },
                {
                    "data": "payment_method_id",
                    "type": "options_obj",
                    "options": sessionPHP.paraments.payment_getway,
                    "render": function ( data, type, row ) {
                        var payment_method_name = "";
                        $.each(sessionPHP.paraments.payment_getway, function( index, value ) {
                            if (value.id == data) {
                                payment_method_name = value.name;
                            }
                        });
                        return payment_method_name;
                    }
                },
                {
                    "data": "status",
                    "type": "options",
                    "options": {
                        "0":"Sin Asignar",
                        "1":"Asignado",
                    },
                    "render": function ( data, type, row ) {
                        var enabled = 'Sin Asignar';
                        if (data) {
                            enabled = 'Asignado';
                        }
                        return enabled;
                    }
                },
                {
                    "data": "customer.code",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        var customer_code = "";
                        if (data) {
                            customer_code = pad(data, 5);
                        }
                        return customer_code;
                    }
                },
                {
                    "data": "customer.name",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        var customer_name = "";
                        if (data) {
                            customer_name = data;
                        }
                        return customer_name;
                    }
                },
                {
                    "data": "customer.ident",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (data) {
                            ident = data;
                        }
                        return sessionPHP.afip_codes.doc_types[row.customer.doc_type] + ' ' + ident;
                    }
                },
                {
                    "data": "number_transaction",
                    "type": "string"
                },
                {
                    "class": "font-weight-bold right",
                    "data": "import",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '$ ' )
                },
                {
                    "data": "comment",
                    "type": "string"
                },
                {
                    "data": "barcode",
                    "type": "string"
                },
                {
                    "data": "receipt_number",
                    "type": "string"
                },
            ],
		    "columnDefs": [
                // {
                //     "visible": false, targets: hide_columns
                // },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
            "initComplete": function(settings, json) {
            },
		    "language": dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
        	"dom":
    	    	"<'row'<'col-xl-2'l><'col-xl-4 service'><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
		});

        $('#table-transactions_wrapper .tools').append($('#btns-tools-transactions').contents());

        $('#table-transactions').on( 'init.dt', function () {
            createModalSearchColumn(table_transactions, '.modal-search-columns-transactions');
            //createModalHideColumn(table_transactions, '.modal-hide-columns-transactions');
        });

        $('#btns-tools-transactions').show();

        $('#table-transactions tbody').on( 'click', 'td', function () {

            var column = table_transactions.cell( this ).index().columnVisible;

            if (column != 0) {

                if (!$(this).closest('tr').find('.dataTables_empty').length) {

                    table_transactions.$('tr.selected').removeClass('selected');
                    $(this).closest('tr').addClass('selected');

                    transaction_selected = table_transactions.row( this ).data();

                    if (transaction_selected.status) {

                        if (transaction_selected.receipt_number.indexOf(',') > -1) {

                        } else {
                            $('#btn-assign-customer').addClass('d-none');
                            $('#btn-print-receipt').removeClass('d-none');
                            $('.modal-transactions').modal('show');
                        }
                    } else {
                        $('#btn-print-receipt').addClass('d-none');
                        $('#btn-assign-customer').removeClass('d-none');
                        $('.modal-transactions').modal('show');
                    }
                }
            }
        });

        $('.btn-update-table-transaction').click(function() {
            if (table_transactions) {
                table_transactions.draw();
            }
        });

        $('#btn-print-receipt').click(function() {
            var action = '/ispbrain/PaymentMethods/printReceipt/' + transaction_selected.receipt_id;
            window.open(action, '_blank');
        });


        $(".btn-export-transactions").click(function() {

            bootbox.confirm('Se descargará un archivo Excel', function(result) {
                if (result) {
                    $('#table-transactions').tableExport({tableName: 'Transacciones', type:'excel', escape:'false'});
                }
            }).find("div.modal-content").addClass("confirm-width-md");
        });
        
        $('#btn-assign-customer').click(function() {
            $('.modal-transactions').modal('hide');
            var request = $.ajax({
                url: "/ispbrain/" + transaction_selected.controller + "/assignTransaction.json",
                method: "POST",
                data: JSON.stringify({
                    customer_code: customer_selected.code,
                    transaction_id: transaction_selected.id
                }),
                dataType: "json"
            });

            request.done(function(response) {

                generateNoty(response.response.type, response.response.message);
                if (table_transactions) {
                    table_transactions.draw();
                }
            });

            request.fail(function(jqXHR) {

                if (jqXHR.status == 403) {

                	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                	setTimeout(function() {
                        window.location.href = "/ispbrain";
                	}, 3000);

                } else {
                	generateNoty('error', 'Error al asignar el pago');
                }
            });
        });

        $('a[id="transactions-tab"]').on('shown.bs.tab', function (e) {
            table_transactions.draw();
        });
    });

</script>

<?php $this->end(); ?>
