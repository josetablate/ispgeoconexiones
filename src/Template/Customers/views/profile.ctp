<?php $this->extend('/Customers/views/moves'); ?>

<?php $this->start('profile'); ?>
<style type="text/css">

    .vertical-table {
        width: 100%;
    }

    table.vertical-table tbody tr {
        background-color: 1px solid #d8d8d8;
    }

    #table-connections_wrapper .title {
        color: #696868;
        padding-top: 10px;
    }

    #map {
		height: 400px;
	}

	.crop { 
        overflow: hidden; 
        white-space:n owrap; 
        text-overflow: ellipsis; 
        width: 100px; 
    }

    .table-hover tbody tr:hover {
        background-color: white !important;
        color: black !important;
    }

    .comment-text {
        display: block;
        text-overflow: ellipsis;
        word-wrap: break-word;
        overflow: hidden;
        line-height: 1.8em;
        max-height: 115px;
        overflow-y: auto;
    }

    .blocked-connection-title {
        font-size: 18px;
    }

    .border-bottom {
        border-bottom: 1px solid #dee2e6!important;
    }

</style>

<?php

    $status = [
        'CP' => 'Conexión Pendiente',
        'CC' => 'Conexión Creada',
        'I'  => 'Instalación Registrada',
    ];
?>

<div class="row">
    <div class="col-md-4">

        <div class="row p-3">
            <legend class="sub-title" style="margin-left: 14px;">Saldo mes</legend>
            <div class="col-6 font-weight-bold border-bottom">
                <?= __('Facturados') ?>
            </div>
            <div class="col-6 text-right border-bottom comment-text">
                <b>$<?= number_format($debt_month_facturados, 2, ',', '.')?></b>
            </div>
            <div class="col-6 font-weight-bold border-bottom">
                <?= __('Sin facturar') ?>
            </div>
            <div class="col-6 text-right border-bottom comment-text">
                <b>$<?= number_format($debt_month_sin_facturar, 2, ',', '.')?></b>
            </div>
            <div class="col-6 font-weight-bold <?= ($customer->debt_month > 0) ? 'text-danger' : 'text-success' ?> border-bottom" style="font-size: 23px; background: rgba(0,0,0,.03);">
                <?= __('Total') ?>
            </div>
            <div class="col-6 text-right <?= ($customer->debt_month > 0) ? 'text-danger' : 'text-success' ?> border-bottom comment-text" style="font-size: 23px; background: rgba(0,0,0,.03);">
                <b>$<?= number_format($customer->debt_month, 2, ',', '.')?></b>
            </div>
        </div>
        
        <?php if($this->request->getSession()->read('paraments')->gral_config->value_service_customer): ?>
        
        <div class="row p-3">
            <legend class="sub-title" style="margin-left: 14px;">Valor del Servicio</legend>
            <div class="col-6 text-right border-bottom comment-text">
                $<?= number_format($customer->value_service, 2, ',' , '.') ?>
            </div>
           
        </div>
        
        <?php endif; ?>

        <div class="row p-3">
            <legend class="sub-title" style="margin-left: 14px;">Información adicional</legend>
            <div class="col-6 font-weight-bold border-bottom">
                <?= __('Total (Saldo Mes + próx. vencimientos)') ?>
            </div>
            <div class="col-6 text-right border-bottom comment-text">
                $<?= number_format($customer->debt_total, 2, ',' , '.') ?>
            </div>
            <div class="col-6 font-weight-bold border-bottom">
                <?= __('Comentario') ?>
            </div>
            <div class="col-6 text-right border-bottom comment-text">
                <?= h($customer->comments) ?>
            </div>
        </div>

        <div class="row p-3">
            <div class="col-6 font-weight-bold text-info border-bottom">
                <?= __('Código de Cliente') ?>
            </div>
            <div class="col-6 text-right text-info border-bottom comment-text">
                <b><?= sprintf("%'.05d", $customer->code ) ?></b>
            </div>

            <div class="col-6 font-weight-bold border-bottom">
                <?= __('Documento') ?>
            </div>
            <div class="col-6 text-right border-bottom comment-text">
                <?= $doc_types[$customer->doc_type] . ' ' . ($customer->ident ? $customer->ident : '' ) ?>
            </div>

            <div class="col-6 font-weight-bold border-bottom">
                <?= __('Tipo') ?>
            </div>
            <div class="col-6 text-right border-bottom comment-text">
                <?= $responsibles[$customer->responsible] ?>
            </div>

            <div class="col-6 font-weight-bold border-bottom">
                <?= __('Clave Portal') ?>
            </div>
            <div class="col-6 text-right border-bottom comment-text">
                <?= $customer->clave_portal ?>
            </div>
        </div>

        <div id="accordion">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">

                        <button class="btn btn-secondary" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Más infomación
                        </button>
                    </h5>
                </div>
    
                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">

                        <div class="row">
                            <div class="col-6 font-weight-bold border-bottom">
                                <?= __('Modificado') ?>
                            </div>
                            <div class="col-6 text-right border-bottom comment-text">
                                <?= date('d-m-Y H:i', strtotime($customer->modified)) ?>
                            </div>
                            <div class="col-6 font-weight-bold border-bottom">
                                <?= __('Domicilio') ?>
                            </div>
                            <div class="col-6 text-right comment-text border-bottom comment-text">
                                <?= $customer->address ?>
                            </div>

                            <div class="col-6 font-weight-bold border-bottom">
                                <?= __('Ciudad') ?>
                            </div>
                            <div class="col-6 text-right border-bottom comment-text">
                                <?= $customer->city->name ?>
                            </div>

                            <div class="col-6 font-weight-bold border-bottom">
                                <?= __('Teléfono') ?>
                            </div>
                            <div class="col-6 text-right border-bottom comment-text">
                                <?= $customer->phone ?>
                            </div>

                            <?php if ($customer->phone_alt != ''): ?>
                            <div class="col-6 font-weight-bold border-bottom">
                                <?= __('Teléfono Alt.') ?>
                            </div>
                            <div class="col-6 text-right border-bottom comment-text">
                                <?= $customer->phone_alt ?>
                            </div>
                            <?php endif; ?>

                            <div class="col-6 font-weight-bold border-bottom">
                                <?= __('Correo Electrónico') ?>
                            </div>
                            <div class="col-6 text-right comment-text border-bottom comment-text">
                                <?= $customer->email ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6 font-weight-bold border-bottom">
                                <?= __('Cond. Vta (Facturación)') ?>
                            </div>
                            <div class="col-6 text-right comment-text border-bottom">
                                <?= $cond_venta[$customer->cond_venta] ?>
                            </div>

                            <div class="col-6 font-weight-bold border-bottom">
                                <?= __('Día de vencimiento') ?>
                            </div>
                            <div class="col-6 text-right border-bottom comment-text">
                                <?= $customer->daydue ?>
                            </div>

                            <div class="col-6 font-weight-bold border-bottom">
                                <?= __('Prespuesto') ?>
                            </div>
                            <div class="col-6 text-right border-bottom comment-text">
                                <?= ($customer->is_presupuesto) ? 'SI' : 'NO' ?>
                            </div>

                            <div class="col-6 font-weight-bold border-bottom">
                                <?= __('Denominación') ?>
                            </div>
                            <div class="col-6 text-right border-bottom comment-text">
                                <?= ($customer->denomination) ? 'SI' : 'NO' ?>
                            </div>

                            <div class="col-6 font-weight-bold border-bottom">
                                <?= __('Facturar con') ?>
                            </div>
                            <div class="col-6 text-right border-bottom comment-text">
                                <?= $business[$customer->business_billing] ?>
                            </div>
                        </div>

                        <br>

                        <div class="row">
                            <div class="col-12 font-weight-bold">
                                <span class="blocked-connection-title border-bottom"><?= __('Bloqueo automático de conexión') ?></span>
                                <div class="row mt-2" style="background: #f3f3f3;">
                                    <div class="col-6 font-weight-bold border-bottom">
                                        <?= __('Habilitado') ?>
                                    </div>
                                    <div class="col-6 text-right border-bottom">
                                        <?= ($customer->acb_enabled) ? 'SI' : 'NO' ?>
                                    </div>
        
                                    <div class="col-6 font-weight-bold border-bottom">
                                        <?= __('Deuda vencida') ?>
                                    </div>
                                    <div class="col-6 text-right border-bottom comment-text">
                                        <?= number_format($customer->acb_due_debt , 2, ',', '.') ?>
                                    </div>
        
                                    <div class="col-6 font-weight-bold border-bottom">
                                        <?= __('Facturas impagas') ?>
                                    </div>
                                    <div class="col-6 text-right border-bottom comment-text">
                                        <?= $customer->acb_invoice_no_paid ?>
                                    </div>
        
                                    <div class="col-6 font-weight-bold border-bottom">
                                        <?= __('Tipo de Control') ?>
                                    </div>
                                    <div class="col-6 text-right border-bottom comment-text">
                                        <?= ($customer->acb_type == 'connection') ? 'Por conexión' : 'Por cliente' ?>
                                    </div>
        
                                    <div class="col-6 font-weight-bold border-bottom">
                                        <?= __('Aplicar recargo') ?>
                                    </div>
                                    <div class="col-6 text-right border-bottom">
                                        <?= ($customer->acb_surcharge_enabled) ? 'SI' : 'NO' ?>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <br>

                        <div class="row">
                            <div class="col-6 font-weight-bold border-bottom">
                                <?= __('Vendedor') ?>
                            </div>
                            <div class="col-6 text-right border-bottom comment-text">
                                <?= h($customer->seller) ?>
                            </div>

                            <div class="col-6 font-weight-bold border-bottom">
                                <?= __('Creado') ?>
                            </div>
                            <div class="col-6 text-right border-bottom comment-text">
                                <?= date('d-m-Y H:i', strtotime($customer->created)) ?>
                            </div>

                            <div class="col-6 font-weight-bold border-bottom">
                                <?= __('Modificado') ?>
                            </div>
                            <div class="col-6 text-right border-bottom comment-text">
                                <?= date('d-m-Y H:i', strtotime($customer->modified)) ?>
                            </div>

                            <div class="col-6 font-weight-bold border-bottom">
                                <?= __('Comentario') ?>
                            </div>
                            <div class="col-6 text-right border-bottom comment-text">
                                <?= h($customer->comments) ?>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>

        <br>
        <br>

    </div>

    <div class="col-md-8">
        <div class="row">

            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <table id="table-connections" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Creado</th>
                            <th>Controlador</th>
                            <th>Servicio</th>
                            <th>Domicilio</th>
                            <th>Deuda mes</th>
                            <th>Tarjeta Cobro Digital</th>
                            <th>Débito Automático Cobro Digital</th>
                            <th>Débito Automático Chubut</th>
                            <th>Tarjeta PayU</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

            <?php if ($paraments->gral_config->billing_for_service): ?>
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-3">
                <label style="font-size: 18px;">Otras Ventas - Saldo mes: <span>$<?= $otras_ventas ?></span></label>
            </div>
            <?php endif; ?>

            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <br>

                <?php if ($customer->lat != null && $customer->lng != null): ?>
                    <div style="display: -webkit-inline-box;">
                        <legend class="sub-title-sm"><?=  __('Ubicación') ?></legend>
                        <button id="share" type="button" class="btn btn-primary text-l"><i class="fas fa-share-alt"></i></button>
                    </div>
                    <div id="map"></div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div> 

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-info-conn',
        'name' =>  'Información',
        'icon' =>  'icon-eye',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-edit-conn',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-administrative-conn',
        'name' =>  'Administrativa ',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-disable-conn',
        'name' =>  'Deshabilitar',
        'icon' =>  'icon-lock',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-enable-conn',
        'name' =>  'Habilitar',
        'icon' =>  'icon-unlocked',
        'type' =>  'btn-secondary'
    ];
  
    $buttons[] = [
        'id'   =>  'btn-delete-conn',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-connection', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2K3zoK46JybWPzQbhfQQqIIbdZ_3WcQs">
</script>

<script type="text/javascript">

    var table_connections = null;
    var customer = <?= json_encode($customer) ?>;

    $(document).ready(function() {

        var class_debt = sessionPHP.paraments.gral_config.billing_for_service ? '' : 'd-none';
        var class_cobrodigital = 'd-none';
        var class_chubut_auto_debit = 'd-none';
        var class_payu = 'd-none';

        if (sessionPHP.paraments.gral_config.billing_for_service) {
            class_cobrodigital = sessionPHP.payment_getway.config.cobrodigital.enabled ? '' : 'd-none';
            class_chubut_auto_debit = sessionPHP.payment_getway.config.chubut_auto_debit.enabled ? '' : 'd-none';
            class_payu = sessionPHP.payment_getway.config.payu.enabled ? '' : 'd-none';
        }

        table_connections = $('#table-connections').DataTable({
		    "order": [[ 0, 'desc' ]],
            "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/connections/get_connection_customer.json",
                "dataSrc": "connections",
                "data": function ( d ) {
                  return $.extend( {}, d, {
                    "customer_code": customer.code,
                    "deleted": 0,
                  });
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
		    "autoWidth": true,
		    "scrollY": '450px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": false,
		    "columns": [
                { 
                    "data": "created",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                { 
                    "data": "controller.name",
                },
                { 
                    "data": "service.name",
                },
               
                { 
                    "data": "address",
                },
                { 
                    "class": "right " + class_debt,
                    "data": "debt_month",
                    "render": function ( data, type, row ) {

                       if (customer.billing_for_service) {
                           return number_format(data, true);
                       }
                       return '-';
                       
                    }
                },
                {
                    "class": "center " + class_cobrodigital,
                    "data": "cobrodigital_card",
                    "type": "options",
                    "options": {  
                        "1":"SI",
                        "0":"NO",
                    },
                    "render": function ( data, type, row ) {
                       if (data) {
                           return 'SI';
                       }
                       return 'NO';
                    }
                },
                {
                    "class": "center " + class_cobrodigital,
                    "data": "cobrodigital_auto_debit",
                    "type": "options",
                    "options": {  
                        "1":"SI",
                        "0":"NO",
                    },
                    "render": function ( data, type, row ) {
                       if (data) {
                           return 'SI';
                       }
                       return 'NO';
                    }
                },
                {
                    "class": "center " + class_chubut_auto_debit,
                    "data": "chubut_auto_debit",
                    "type": "options",
                    "options": {  
                        "1":"SI",
                        "0":"NO",
                    },
                    "render": function ( data, type, row ) {
                       if (data) {
                           return 'SI';
                       }
                       return 'NO';
                    }
                },
                {
                    "class": "center " + class_payu,
                    "data": "payu_card",
                    "type": "options",
                    "options": {  
                        "1":"SI",
                        "0":"NO",
                    },
                    "render": function ( data, type, row ) {
                       if (data) {
                           return 'SI';
                       }
                       return 'NO';
                    }
                },
            ],
		    "columnDefs": [
		        { "type": 'date-custom', targets: 0 },
		        { "visible": false, targets: hide_columns },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
                if (!data.enabled) {
                    $(row).addClass('locked');
                }
                if (data.error) {
                    $(row).addClass('error-custom');
                }
            },
		    "language": dataTable_lenguage,
            "lengthMenu": [[ -1], ["Todas"]],
        	"dom":
    	    	"<'row'<'col-xl-6 title'><'col-xl-6'f>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
   
		});

		$('#table-connections_wrapper .title').append('<h5>Conexiones</h5>');

		$('#table-connections tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                table_connections.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');

                $('.modal-connection').modal('show');
            }
        });

        $('#btn-info-conn').click(function() {
            var action = '/ispbrain/connections/view/' + table_connections.$('tr.selected').attr('id');
            window.open(action, '_self');
        });

         $('#btn-administrative-conn').click(function() {
            var action = '/ispbrain/connections/administrative/' + table_connections.$('tr.selected').attr('id');
            window.open(action, '_blank');
        });

        $('#btn-edit-conn').click(function() {
            var action = '/ispbrain/connections/edit/' + table_connections.$('tr.selected').attr('id');
            window.open(action, '_self');
        });

        $('#btn-disable-conn').click(function() {

            if (!table_connections.$('tr.selected').hasClass('locked')) {

                var text = 'Esta seguro que desea dasabilitar esta conexión?';
                var id  = table_connections.$('tr.selected').attr('id');
    
                bootbox.confirm(text, function(result) {
                    if (result) {
                        $('body')
                            .append( $('<form/>').attr({'action': '/ispbrain/connections/disable/', 'method': 'post', 'id': 'form-block'})
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                            .find('#form-block').submit();
                    }
                });
            } else {
                generateNoty('information', 'La conexión ya se encuentra deshabilitada');
            }
        });

        $('#btn-enable-conn').click(function() {

            if (table_connections.$('tr.selected').hasClass('locked')) {

                var text = '¿Está seguro que desea habilitar está conexión?';
                var id  = table_connections.$('tr.selected').attr('id');

                bootbox.confirm(text, function(result) {
                    if (result) {
                        $('body')
                            .append( $('<form/>').attr({'action': '/ispbrain/connections/enable/', 'method': 'post', 'id': 'form-block'})
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                            .find('#form-block').submit();
                    }
                });
            } else {
                generateNoty('information', 'La conexión ya se encuentra habilitada');
            }
        });

        $('#btn-delete-conn').click(function() {

            var text = '¿Está Seguro que desea eliminar la conexión ?';
            var id  = table_connections.$('tr.selected').attr('id');

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/connections/delete/', 'method': 'post', 'id': 'replacer'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer').submit();
                }
            });
        });

        $('a[id="resume-tab"]').on('shown.bs.tab', function (e) {
            table_connections.draw();
            table_connections.ajax.reload();
        });

        if (customer.lat != null && customer.lng != null) {
            initMap();
        }
    });

    var map = null;
    var geocoder = null;
    var marker = null;

	function initMap() {

	    if (!document.getElementById('map')) {
	        return false;
	    }

		geocoder = new google.maps.Geocoder;

		var Resistencia = {lat: -27.451348, lng: -58.986807};

		var mapOptions = {
			zoom: 15,
			center: Resistencia	    
		};

		map = new google.maps.Map(document.getElementById('map'), mapOptions);

		var init = {
            lat: '<?php echo $customer->lat; ?>',
            lng: '<?php echo $customer->lng; ?>'
        };

        var latLng = new google.maps.LatLng(init.lat, init.lng);

        addMarker(latLng);
        map.setCenter(latLng);
	}

	function addMarker(location) {

		marker = new google.maps.Marker({
			position: location,
			map: map
		});
		marker.setMap(map);	
        var lat = location.lat();  
        var lng = location.lng();
	}	

</script>

<script>

    'use strict';

    function sleep(delay) {
        return new Promise(resolve => {
            setTimeout(resolve, delay);
        });
    }

    function logText(message, isError) {
        if (isError)
            console.error(message);
        else
            console.log(message);
            const p = document.createElement('p');
        if (isError) {
            generateNoty('warning', message);
        } else {
            generateNoty('success', message);
        }
    }

    function logError(message) {
        logText(message, true);
    }

    function checkboxChanged(e) {
        const checkbox = e.target;
        const textfield = document.querySelector('#' + checkbox.id.split('_')[0]);
        textfield.disabled = !checkbox.checked;
        if (!checkbox.checked)
            textfield.value = '';
    }

    async function testWebShare() {
        if (navigator.share === undefined) {
            logError('Error: característica no soportada');
            return;
        }

        var title = "Compartir ubicación";
        var customer_name = customer.name + ' - Cód.: ' + customer.code + ' - Dom.: ' + customer.address;
        var text = "Cliente: " + customer_name;
        var lat = customer.lat;
        var lng = customer.lng;
        var url = "https://www.google.com/maps/search/?api=1&query=" + lat + "," + lng;

        try {
            await navigator.share({title, text, url});
        } catch (error) {
            logError('Error al compartir: ' + error);
            return;
        }
        logText('Compartido correctamente');
    }

    async function testWebShareDelay() {
        await sleep(2000);
        testWebShare();
    }

    function onLoad() {
        if (document.querySelector('#share')) {
            document.querySelector('#share').addEventListener('click', testWebShare);
        }
    }

    window.addEventListener('load', onLoad);

  </script>

<?php $this->end(); ?>
