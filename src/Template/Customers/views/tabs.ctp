<style type="text/css">

    .nav-link {
        cursor: pointer;
    }

    .nav-tabs {
        border-bottom: 1px solid #f05f40;
    }

    .btn-next-due-dates-selected {
        color: #f05f40;
        background-color: #e6e6e6;
        border-color: #f05f40;
    }

</style>

<div class="row">
    <div class="col-md-6">
        <legend class="sub-title">Cliente: <span class="bold-color"><?= $customer->name ?></span></legend>
    </div>
    <div class="col-md-6">

        <div class="text-right">

            <?php

                echo $this->Html->link(
                    '<span class="fas fa-sync-alt" aria-hidden="true"></span>',
                    ['action' => 'updateDebt', $customer->code],
                    [
                    'title' => 'recalcular saldos',
                    'class' => 'btn btn-default mr-1',
                    'escape' => false
                ]);

                echo $this->Html->link(
                    '<span class="glyphicon icon-pencil2" aria-hidden="true"></span>',
                    ['action' => 'edit', $customer->code],
                    [
                    'title' => 'Editar Cliente',
                    'class' => 'btn btn-default mr-1',
                    'escape' => false
                ]);

                echo $this->Html->link(
                    '<span class="fas fa-archive" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Archivar Cliente',
                    'id' =>  'btn-delete',
                    'class' => 'btn btn-default',
                    'data-code' => $customer->code,
                    'data-name' => $customer->name,
                    'escape' => false
                ]);
            ?>

        </div>
    </div>
</div>

<ul class="nav nav-tabs" id="myTab" role="tablist">

    <li class="nav-item">
        <a class="nav-link <?= $tab == 'profile' ? 'active' : '' ?>" id="profile-tab" data-target="#profile" role="tab" aria-controls="profile" aria-expanded="true">Perfil</a>
    </li>

    <li class="nav-item">
        <a class="nav-link <?= $tab == 'moves' ? 'active' : '' ?>" id="moves-tab" data-target="#moves" role="tab" aria-controls="moves" aria-expanded="false">Facturados</a>
    </li>

    <li class="nav-item">
        <a class="nav-link <?= $tab == 'debts' ? 'active' : '' ?>" id="debts-tab" data-target="#debts" role="tab" aria-controls="debts" aria-expanded="false">Sin facturar</a>
    </li>

    <li class="nav-item">
        <a class="nav-link p-2 <?= $tab == 'presupuestos' ? 'active' : '' ?>" id="presupuestos-tab" data-target="#presupuestos" role="tab" aria-controls="presupuestos" aria-expanded="false">Presupuestos</a>
    </li>

    <li class="nav-item">
        <a class="nav-link <?= $tab == 'accounts' ? 'active' : '' ?>" id="accounts-tab" data-target="#accounts" role="tab" aria-controls="accounts" aria-expanded="false">Cuentas</a>
    </li>

    <li class="nav-item">
        <a class="nav-link <?= $tab == 'tickets' ? 'active' : '' ?>" id="tickets-tab" data-target="#tickets" role="tab" aria-controls="tickets" aria-expanded="false">Tickets</a>
    </li>

    <li class="nav-item">
        <a class="nav-link <?= $tab == 'logs' ? 'active' : '' ?>" id="logs-tab" data-target="#logs" role="tab" aria-controls="logs" aria-expanded="false">Logs</a>
    </li>

    <li class="nav-item">
        <a class="nav-link <?= $tab == 'observations' ? 'active' : '' ?>" id="observations-tab" data-target="#observations" role="tab" aria-controls="observations" aria-expanded="false">Observaciones</a>
    </li>

    <li class="nav-item">
        <a class="nav-link <?= $tab == 'payment_commitment' ? 'active' : '' ?>" id="payment_commitment-tab" data-target="#payment_commitment" role="tab" aria-controls="payment_commitment" aria-expanded="false">Compromiso de Pago</a>
    </li>

    <li class="nav-item">
        <a class="nav-link <?= $tab == 'transactions' ? 'active' : '' ?>" id="transactions-tab" data-target="#transactions" role="tab" aria-controls="transactions" aria-expanded="false">Transacciones</a>
    </li>

</ul>

<div class="tab-content" id="myTabContent">

    <div class="tab-pane fade <?= $tab == 'profile' ? 'show active' : '' ?>" id="profile" role="tabpanel" aria-labelledby="profile-tab">
        <?= $this->fetch('profile') ?>
    </div>

    <div class="tab-pane fade <?= $tab == 'moves' ? 'show active' : '' ?>" id="moves" role="tabpanel" aria-labelledby="moves-tab">
        <?= $this->fetch('moves') ?>
    </div>

    <div class="tab-pane fade <?= $tab == 'debts' ? 'show active' : '' ?>" id="debts" role="tabpanel" aria-labelledby="debts-tab">
        <?= $this->fetch('debts') ?>
    </div>

    <div class="tab-pane fade <?= $tab == 'presupuestos' ? 'show active' : '' ?>" id="presupuestos" role="tabpanel" aria-labelledby="presupuestos-tab">
        <?= $this->fetch('presupuestos') ?>
    </div>

    <div class="tab-pane fade <?= $tab == 'accounts' ? 'show active' : '' ?>" id="accounts" role="tabpanel" aria-labelledby="accounts-tab">
        <?= $this->fetch('accounts') ?>
    </div>

    <div class="tab-pane fade <?= $tab == 'tickets' ? 'show active' : '' ?>" id="tickets" role="tabpanel" aria-labelledby="tickets-tab">
        <?= $this->fetch('tickets') ?>
    </div>

    <div class="tab-pane fade <?= $tab == 'logs' ? 'show active' : '' ?>" id="logs" role="tabpanel" aria-labelledby="logs-tab">
        <?= $this->fetch('logs') ?>
    </div>

    <div class="tab-pane fade <?= $tab == 'observations' ? 'show active' : '' ?>" id="observations" role="tabpanel" aria-labelledby="observations-tab">
        <?= $this->fetch('observations') ?>
    </div>

    <div class="tab-pane fade <?= $tab == 'payment_commitment' ? 'show active' : '' ?>" id="payment_commitment" role="tabpanel" aria-labelledby="payment_commitment-tab">
        <?= $this->fetch('payment_commitment') ?>
    </div>

    <div class="tab-pane fade <?= $tab == 'transactions' ? 'show active' : '' ?>" id="transactions" role="tabpanel" aria-labelledby="transactions-tab">
        <?= $this->fetch('transactions') ?>
    </div>

</div>

<div class="modal fade modal-resume" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel">Resumen de Cuenta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form>
                            <table>
                                <tr>
                                    <td style="width:20%"><label class="control-label" for="">Desde</label> &nbsp;</td>
                                    <td style="width:80%">
                                        <div class="form-group date">
                                        	<input type="date"  class="form-control" name="from" id="date-from" value="<?= date('Y-m-d', strtotime($customer->from)) ?>" required>
                                    	</div>
                                	</td>
                                </tr>
                                 <tr>
                                    <td style="width:20%"><label class="control-label" for="">Hasta</label> &nbsp;</td>
                                    <td style="width:80%">
                                        <div class="form-group date">
                                        	<input type="date"  class="form-control" name="to" id="date-to" value="<?= date('Y-m-d', strtotime($customer->to)) ?>" required>
                                    	</div>
                                	</td>
                                </tr>
                            </table>  
                            <br>
                            <center>
                                 <a class="btn btn-default btn-print-resume" data-id="<?= $customer->code ?>" href="" role="button">
                                <span class="glyphicon icon-printer" aria-hidden="true"></span>
                                Imprimir
                            </a>
                            </center>

                         </form>   
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    echo $this->element('modal_preloader');
    echo $this->element('AdministrativeMovement/edit_administrative_movement', ['modal' => 'modal-edit-administrative-movement', 'title' => 'Editar']);
?>

<script type="text/javascript">

    function addZero(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

    $('.btn-print-resume').click(function() {
        var url = "/ispbrain/customers/printresume/" + $(this).data('id') + '/' + $('#date-from').val() + '/' + $('#date-to').val();
        window.open(url, '_blank');
    });

    $('#btn-delete').click(function() {

        var text = '¿Está Seguro que desea eliminar el cliente?';
        var controller = 'customers';
        var id  = $(this).data('code');

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                    .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/delete/', 'method': 'post', 'id': 'replacer'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer').submit();
            }
        });
    });

    var customer = <?= json_encode($customer) ?>;;
    var customer_selected = <?= json_encode($customer) ?>;

    $(document).ready(function() {

        $('#myTab a').click(function (e) {

            e.preventDefault();

            var target = $(this).data('target');
            $('#myTab a.active').removeClass('active');

            $('#myTabContent .active').fadeOut(100, function() {
                $('#myTabContent .active').removeClass('active');

                $(target).fadeIn(100, function() {
                    $(target).addClass('active show');
                    $(target + '-tab').addClass('active');
                    $(target + '-tab').trigger('shown.bs.tab');
                });
            });
        });
    });

</script>
