<?php $this->extend('/Customers/views/accounts'); ?>

<?php $this->start('presupuestos'); ?>

<style type="text/css">

    .my-hidden {
        display: none;
    }

    .input-debt-month-red {
        font-size: 23px;
        padding: 0 15px 0 0;
        color: #f05f40;
    }

    .input-debt-month-green {
        font-size: 23px;
        padding: 0 15px 0 0;
        color: #28a745;
    }

    .last-cell-saldo-red {
        color: #f05f40;
        font-weight: bold;
    }

    .last-cell-saldo-green {
        color: #28a745;
        font-weight: bold;
    }

</style>

<div id="btns-tools-presupuestos">

    <div class="text-right btns-tools margin-bottom-5">

        <?php 

            echo $this->Html->link(
                '<span class="fas fa-sync-alt" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Actualizar la tabla',
                'class' => 'btn btn-default btn-update-table-presupuesto ml-1 mt-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon fas fa-search" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Buscar por Columnas',
                'class' => 'btn btn-default btn-search-column-presupuestos ml-1 mt-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="far fa-calendar-plus" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Mostrar próximos vencimientos',
                'class' => 'btn btn-default btn-next-due-dates-presupuestos ml-1 mt-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar Resumen',
                'class' => 'btn btn-default btn-export ml-1 mt-1',
                'escape' => false
            ]);

        ?>

    </div>
</div>

<?php if ($customer->billing_for_service): ?>
    <div class="row">
        <div class="col-12">
            <div class="card border-secondary p-1 mb-2 mt-1 " id="card-customer-selector">
                <div class="card-body p-1">

                    <div class="row">
                        <div class="col-sm-12 col-md-9 col-lg-9 col-xl-9">
                            <?php echo $this->Form->input('connecton_id_presupuesto', ['options' => $connectionsArray, 'label' => 'Servicios y productos', 'required' => true]); ?>
                        </div>
                        <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">

                            <div class="form-group text mb-0">
                                <label class="control-label" for="connection-saldo-month-presupuesto" id="label-connection-saldo-presupuesto" >Deuda del mes</label>
                                <input type="text" name="connection_saldo_month_presupuesto" class="text-right form-control input-debt-month-green" readonly="readonly" id="connection-saldo-month-presupuesto">
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<div class="row">

    <div class="col-xl-12">
        <table class="table table-bordered table-hover" id="table-presupuestos">
            <thead>
                <tr>
                    <th>Fecha</th>               <!--0-->
                    <th>Usuario</th>             <!--1-->
                        <th>Empresa Facturación</th> <!--2-->
                    <th>Pto. Vta.</th>           <!--3-->
                    <th>Número</th>              <!--4-->
                    <th>Comentario</th>          <!--5-->
                        <th>Domicilio</th>           <!--6-->
                        <th>Ciudad</th>              <!--7-->
                    <th>Vencimiento</th>         <!--8-->
                        <th>Subtotal</th>            <!--9-->
                        <th>Impuestos</th>           <!--10-->
                    <th>Total</th>               <!--11-->
                    <th>Saldo</th>               <!--12-->
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade modal-generate-invoice-from-presupuesto" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabel">Facturar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                
                <div class="row">
                    
                    <div class="col-6">
                        <?php
                         echo $this->Form->input('business_id', ['options' => $business,  'label' =>  'Empresa', 'value' => '']); 
                        ?>
                    </div>
                    <div class="col-6">
                         <?php
                         echo $this->Form->input('type', ['options' => [],  'label' =>  'Tipo comprobante', 'required' => true]);
                        ?>
                    </div>
                    
                 </div>  
                 
                <div class="row">
                    
                    <div class="col-6">
                        <label class="control-label" for="">Fecha de emisión</label>
                        <div class='input-group date' id='created-datetimepicker'>
                            <input name="created" id="created"  type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar mr-0">
                                <span class="glyphicon icon-calendar mr-0"></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-6">
                         <?php
                         echo $this->Form->input('concept_type', ['label' => 'Tipo concepto', 'options' => $concept_types, 'value' => 2, 'id' => 'concept_type'])
                        ?>
                    </div>
                    
                 </div>
                 
                <div class="row">
                     <div class="col-12">
                         <legend class="sub-title-sm">Período</legend>
                     </div>
                 </div>
                 
                <div class="row">
                     <div class="col-6">
                        <label class="control-label" for="">Desde</label>
                        <div class='input-group date' id='date_start-datetimepicker'>
                            <input name="date_start" id="date_start"  type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar mr-0">
                                <span class="glyphicon icon-calendar mr-0"></span>
                            </span>
                        </div>
                     </div>
                     <div class="col-6">
                        <label class="control-label" for="">Hasta</label>
                        <div class='input-group date' id='date_end-datetimepicker'>
                            <input name="date_end" id="date_end"  type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar mr-0">
                                <span class="glyphicon icon-calendar mr-0"></span>
                            </span>
                        </div>
                     </div>
                 </div>
                 
                <div class="row mt-2">
                     <div class="col-12">
                        <label class="control-label" for="">Venc. de Pago</label>
                        <div class='input-group date' id='duedate-datetimepicker'>
                            <input name="duedate" id="duedate"  type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar mr-0">
                                <span class="glyphicon icon-calendar mr-0"></span>
                            </span>
                        </div>
                     </div>
                </div>
                 
                <div class="row mt-4">
                     <div class="col-12">
                         <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-danger btn-generate float-right">Confirmar</button>
                     </div>
                </div>
       
            </div>
        </div>
    </div>
</div>

<?php

    $buttons[] = [
        'id'   =>  'btn-print-presupuesto',
        'name' =>  'Imprimir',
        'icon' =>  'icon-printer',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-generate-invoice-from-presupuesto',
        'name' =>  'Convertir',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-info'
    ];
    
     $buttons[] = [
        'id'   =>  'btn-edit-administrative-movement',
        'name' =>  'Editar conexión',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-success'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete-presupuesto',
        'name' =>  'Eliminar',
        'icon' =>  'fa fa-times',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-actions-presupuesto', 'title' => 'Acciones', 'buttons' => $buttons]);
    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-presupuestos']);
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-presupuestos']);
?>

<script type="text/javascript">

    var presupuesto_selected = null;
    var table_selected = null;
    var table_presupuestos = null;
    
    $('.modal-generate-invoice-from-presupuesto #created-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'DD/MM/YYYY'
    });

    $('.modal-generate-invoice-from-presupuesto #date_start-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'DD/MM/YYYY'
    });

    $('.modal-generate-invoice-from-presupuesto #date_end-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'DD/MM/YYYY'
    });

    var users = null;
     
    $('.modal-generate-invoice-from-presupuesto #duedate-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'DD/MM/YYYY'
    });

    $(document).ready(function() {
        
        $('.modal-generate-invoice-from-presupuesto #business-id').change(function() {

           var selected = $(this).val() ;

           business_selected = null;

           $('.modal-generate-invoice-from-presupuesto #type').empty();

           $.each(sessionPHP.paraments.invoicing.business, function(i, business) {

                if (selected == business.id) {

                    business_selected = business;
                    
                    $('.modal-generate-invoice-from-presupuesto #type').append('<option value="">Seleccione Tipo</option>');

                    if (business.responsible == 1) { //ri

                        if (business.webservice_afip.crt != '') {
                            $('.modal-generate-invoice-from-presupuesto #type').append('<optgroup label="Facturas"><option value="XXX">PRESU X</option><option value="001">FACTURA A</option><option value="006">FACTURA B</option></optgroup>');
                        } else {
                            $('.modal-generate-invoice-from-presupuesto #type').append('<optgroup label="Facturas"><option value="XXX">PRESU X</option></optgroup>');
                        }
                    } else if (business.responsible == 6) { //monotributerou
                       
                        if (business.webservice_afip.crt != '') {
                            $('.modal-generate-invoice-from-presupuesto #type').append('<optgroup label="Facturas"><option value="XXX">PRESU X</option><option value="011">FACTURA C</option></optgroup>');
                        } else {
                            $('.modal-generate-invoice-from-presupuesto #type').append('<optgroup label="Facturas"><option value="XXX">PRESU X</option></optgroup>');
                        }
                   }
               }
           });
        });

        $('.modal-generate-invoice-from-presupuesto #business-id').change();
        
        // if(sessionPHP.paraments.accountant.account_enabled){
        //     $('#btn-delete-presupuesto').hide();
        // }

        $('.btn-hide-column-presupuestos').click(function() {
           $('.modal-hide-columns-presupuestos').modal('show');
        });

        $('.btn-search-column-presupuestos').click(function() {
           $('.modal-search-columns-presupuestos').modal('show');
        });

        $('#table-presupuestos').removeClass('display');

        $('#btns-tools-presupuestos').hide();

        loadPreferences('presupuestos-customer-view2', [2,6,7,9,10]);

        $('.btn-next-due-dates-presupuestos').click(function() {

            if ($(this).hasClass('btn-next-due-dates-selected')) {

                $(this).removeClass('btn-next-due-dates-selected');

                $('#label-connection-saldo-presupuesto').html('Deuda del mes');
            } else {

                $('#label-connection-saldo-presupuesto').html('Deuda total');

                $(this).addClass('btn-next-due-dates-selected');
            }

            table_presupuestos.draw();
        });
        
        var load = 0;

        table_presupuestos = $('#table-presupuestos').DataTable({
            "deferRender": false,
            "processing": true,
            "serverSide": true,
             "ajax": {
                "url": "/ispbrain/Customers/get_presupuestos.json",
                "data": function ( d ) {
                  return $.extend( {}, d, {
                    "where": {
                        "customer_code": customer.code,
                        "connection_id": parseInt($('#connecton-id-presupuesto').val())
                    },
                    "duedate": $('.btn-next-due-dates-presupuestos').hasClass('btn-next-due-dates-selected') ? 1 : 0,
                    "complete": 1,
                    'load': load,
                  });
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	},
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );

                    if (json.response.connection_saldo_month > 0) {
                        $('#connection-saldo-month-presupuesto').removeClass('input-debt-month-green');
                        $('#connection-saldo-month-presupuesto').addClass('input-debt-month-red');
                    } else {
                        $('#connection-saldo-month-presupuesto').removeClass('input-debt-month-red');
                        $('#connection-saldo-month-presupuesto').addClass('input-debt-month-green');
                    }

                    $('#connection-saldo-month-presupuesto').val(number_format(json.response.connection_saldo_month, true));

                    return JSON.stringify( json.response );
                }
            },
            "drawCallback": function( settings ) {
                var flag = true;
            	switch (settings.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
            	    if (table_presupuestos) {

                        var last_row = table_presupuestos.row(':first').data();

                        if (last_row) {
                            if (last_row.saldo > 0) {
                                $('#table-presupuestos tr#' + last_row.id).find('.column-saldo').addClass('last-cell-saldo-red');
                            } else {
                                $('#table-presupuestos tr#' + last_row.id).find('.column-saldo').addClass('last-cell-saldo-green');
                            }
                        }

                        var tableinfo = table_presupuestos.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column-presupuesto').addClass('search-apply');
                        } else {
                            $('.btn-search-column-presupuesto').removeClass('search-apply');
                        }
                    }
            	}
            },
		    "scrollY": true,
		    "scrollY": '260px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
		    "ordering": false,
		    "columns": [
                { 
                    "className": "",
                    "data": "date",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        var date = data.split('T')[0];
                        date = date.split('-');
                        return date[2] + '/' + date[1] + '/' + date[0];
                    }
                },
                { 
                    "className": " left",
                    "data": "user.name",
                    "type": "string"
                },
                { 
                    "data": "business_id",
                    "type": "options_obj",
                    "options": sessionPHP.paraments.invoicing.business,
                    "render": function ( data, type, row ) {
                        var business_name = '';
                        $.each(sessionPHP.paraments.invoicing.business, function (i, b) {
                            if (b.id == data) {
                                business_name = b.name;
                            }
                        });
                        return business_name;
                    }
                },
                { 
                    "className": "",
                    "data": "pto_vta",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return  pad(data, 4);
                    }
                },
                { 
                    "className": "",
                    "data": "num",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 8);
                    }
                },
                { 
                    "className": " left",
                    "data": "comments",
                    "type": "string"
                },
                { 
                    "className": " left",
                    "data": "customer_address",
                    "type": "string"
                },
                {
                    "className": " left",
                    "data": "customer_city",
                    "type": "string"
                },
                { 
                    "className": "",
                    "data": "duedate",
                    "type": "date",
                    "render": function ( data, type, row ) {

                        var duedate = data.split('T')[0];
                        duedate = duedate.split('-');
                        duedate =  duedate[2] + '/' + duedate[1] + '/' + duedate[0];
                        return duedate;
                    }
                },
                { 
                    "className": " right",
                    "data": "subtotal",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' )
                },
                { 
                    "className": " right",
                    "data": "sum_tax",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' )
                },
                { 
                    "className": " right text-info",
                    "data": "total",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' )
                },
                { 
                    "class": "column-saldo right",
                    "data": "saldo",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '$ ' )
                },
            ],
		    "columnDefs": [
                { 
                    "visible": false, targets: hide_columns
                },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
            "initComplete": function(settings, json) {},
		    "language": dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
        	"dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
		});

        $('#table-presupuestos_wrapper .tools').append($('#btns-tools-presupuestos').contents());

        $('#table-presupuestos').on( 'init.dt', function () {
            createModalHideColumn(table_presupuestos, '.modal-hide-columns-presupuestos');
            createModalSearchColumn(table_presupuestos, '.modal-search-columns-presupuestos');
        });

        $('#btns-tools-presupuestos').show();

        $('#connecton-id-presupuesto').change(function() {

            $(this).closest('div').addClass('mb-0');
            $('#connection-saldo-month-presupuesto').closest('div').addClass('mb-0');
            $('#connection-saldo-total-presupuesto').closest('div').addClass('mb-0');

            table_presupuestos.draw();
        });

        $('.btn-update-table-presupuesto').click(function() {
             if (table_presupuestos) {              
                table_presupuestos.draw();
            }
        });

        $('#connecton-id-presupuesto').change();

        $('#table-presupuestos tbody').on( 'click', 'tr', function () {

            if (!$(this).find('.dataTables_empty').length) {

                table_presupuestos.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');

                presupuesto_selected = table_presupuestos.row( this ).data();
                $('#btn-edit-administrative-movement').addClass('my-hidden');

                if (customer.billing_for_service) {
                    $('#btn-edit-administrative-movement').removeClass('my-hidden');
                }

                $('.modal-actions-presupuesto').modal('show');
            }
        });

        $('#btn-print-presupuesto').click(function() {
            
            var action = '/ispbrain/Presupuestos/showprint/' + presupuesto_selected.id;
            window.open(action, '_blank');
        });
        
        
        
         $('#btn-generate-invoice-from-presupuesto').click(function() {
             
             $('.modal-actions-presupuesto').modal('hide');
             
             $('.modal-generate-invoice-from-presupuesto').modal('show');
         });
         
         $(document).on("click", ".modal-generate-invoice-from-presupuesto .btn-generate", function(e) {
             
             var rows_selected_ids = [];
             rows_selected_ids.push(presupuesto_selected.id);
             
              if (rows_selected_ids) {
                
                var d = new Date(); 
                var created = $('.modal-generate-invoice-from-presupuesto #created').val().split('/');
                
                var h = addZero(d.getHours());
                var m = addZero(d.getMinutes());
                var s = addZero(d.getSeconds());
                
                created = created[2] + '-' + created[1] + '-' + created[0] + ' ' + h + ':' + m + ':' + s;
                
                var duedate = $('.modal-generate-invoice-from-presupuesto #duedate').val().split('/');
                duedate = duedate[2] + '-' + duedate[1] + '-' + duedate[0] + ' ' + h + ':'+ m + ':' + s;
                
                var date_start = $('.modal-generate-invoice-from-presupuesto #date_start').val().split('/');
                date_start = date_start[2] + '-' + date_start[1] + '-' + date_start[0] + ' ' + h + ':' + m + ':' + s;
                
                var date_end = $('.modal-generate-invoice-from-presupuesto #date_end').val().split('/');
                date_end = date_end[2] + '-' + date_end[1] + '-' + date_end[0] + ' ' + h + ':' + m + ':' + s;
  
                var data = {
                    ids: rows_selected_ids,
                    date: created,
                    duedate: duedate,
                    concept_type: parseInt($('.modal-generate-invoice-from-presupuesto #concept_type').val()),
                    date_start: date_start,
                    date_end: date_end,
                    type: $('.modal-generate-invoice-from-presupuesto #type').val(),
                    business_id: $('.modal-generate-invoice-from-presupuesto #business-id').val(),
                    from: 'customer-view'
                };
                
                created = new Date(data.date);
                duedate = new Date(data.duedate);
                date_start = new Date(data.date_start);
                date_end = new Date(data.date_end);
                
                var now = new Date();
                now.setHours(0);
                now.setMinutes(0);
                now.setSeconds(0);
              
                created.setHours(0);
                created.setMinutes(0);
                created.setSeconds(0);
                
                if (created > now) {
                    generateNoty('warning', 'La fecha de la factura no puede ser mayor a la fecha actual.');
                    return false;
                }
                
                if (duedate < created) {
                    generateNoty('warning', 'La fecha de vencimiento no puede ser menor a la fecha actual.');
                    return false;
                }
                
                if (data.concept_type != 1 && date_end < date_start) {
                    generateNoty('warning', 'Las fechas del período no son correctas.');
                    return false;
                }

                bootbox.confirm('Se generará la facturas del presupuesto seleccionado. ¿Desea continuar?', function(result) {
                    if (result) {
                
                        $('body')
                            .append( $('<form/>').attr({'action': '/ispbrain/invoices/generateIndiFromPresu/', 'method': 'post', 'id': 'form-block'})
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'data', 'value': JSON.stringify(data)}))
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                            .find('#form-block').submit();
                        
                        openModalPreloader("Generando Facturas. Espere Por favor ...");
                        
                    }
                });
                
            } else {
                bootbox.alert('Debe seleccionar al menos una deuda para continuar.');
            }
            
       
        });
        
        
        
        

        $('#btn-edit-administrative-movement').click(function() {
            $('.modal-actions-presupuesto').modal('hide');
            table_selected = table_presupuestos;
            $('.modal-edit-administrative-presupuestoment').modal('show');
        });

        $('#btn-delete-presupuesto').click(function() {
            
            $('.modal-actions-presupuesto').modal('hide');

            bootbox.confirm("¿Está Seguro que desea eliminar el presupuesto?", function(result) {
    
                if (result) {
                        
                    var request = $.ajax({
                        url: "<?= $this->Url->build(['controller' => 'Presupuestos', 'action' => 'delete']) ?>",  
                        type: 'POST',
                        dataType: "json",
                        data: JSON.stringify(presupuesto_selected),
                    });
                    
                    request.done(function( data ) {
                      table_presupuestos.draw();
                    });
                     
                    request.fail(function( jqXHR, textStatus ) {
                      generateNoty('error', 'Error al intenetar eliminar el presupuesto');
                    });
                }
            });
        });

        $(".btn-export").click(function() {

            bootbox.confirm('Se descargará un archivo Excel', function(result) {
                if (result) {
                    $('#table-presupuestos').tableExport({tableName: 'Resumen', type:'excel', escape:'false', columnNumber: [8, 9, 10, 13]});
                }
            }).find("div.modal-content").addClass("confirm-width-md");
        });

        $('a[id="presupuestos-tab"]').on('shown.bs.tab', function (e) {
             load = 1
            table_presupuestos.draw();
        });

    });

</script>

<?php $this->end(); ?>
