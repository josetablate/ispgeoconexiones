<?php $this->extend('/Customers/views/payment_commitment'); ?>

<?php $this->start('observations'); ?>

<style type="text/css">

    .my-hidden {
        display: none;
    }

</style>

<div id="btns-tools-observations">

    <div class="text-right btns-tools margin-bottom-5">

        <?php

            echo $this->Html->link(
                '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Agregar Observación',
                'class' => 'btn btn-default btn-add-observation ml-1 mt-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export-observations ml-1 mt-1',
                'escape' => false
            ]);

        ?>

    </div>
</div>

<div class="row">

    <div class="col-xl-12">
        <table class="table table-bordered table-hover" id="table-observations">
            <thead>
                <tr>
                    <th>Fecha</th>      <!--0-->
                    <th>Comentario</th> <!--1-->
                    <th>Usuario</th>    <!--2-->
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<?php
    echo $this->element('Observations/add', ['modal' => 'modal-add-observation', 'title' => 'Agregar Observación', 'records' => FALSE]);
    echo $this->element('Observations/edit', ['modal' => 'modal-edit-observation', 'title' => 'Observación']);
?>

<script type="text/javascript">

    var observation_selected = null;
    var table_observations = null;
    var users = null;
    var table_selected = null;
    var customer_code = null;

    $(document).ready(function() {

        users = <?= json_encode($users) ?>;

        $('#table-observations').removeClass('display');

        $('#btns-tools-observations').hide();

        table_observations = $('#table-observations').DataTable({
            "order": [[ 1, 'desc' ]],
            "autoWidth": false,
    	    "scrollY": '450px',
    	    "scrollCollapse": true,
    	    "scrollX": true,
            "processing": true,
            "serverSide": true,
		    "paging": true,
		    "ordering": true,
            "ajax": {
                "url": "/ispbrain/Observations/index.json",
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "customer_code": customer.code,
                        'main': 0
                    });
                },
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	},
            },
            "drawCallback": function( settings ) {
                var flag = true;
            	switch (settings.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            },
		    "columns": [
		        {
                    "data": "created",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var date = data.split('T')[0];
                            date = date.split('-');
                            var other = data.split('T')[1];
                            other = other.split('-')[0];
                            other = other.split(':');
                            return date[2] + '/' + date[1] + '/' + date[0] + ' ' + other[0] + ':' + other[1];
                        }
                    }
                },
                {
                   "className": " left",
                    "data": "comment",
                    "type": "string"
                },
                {
                    "data": "user_id",
                    "options": users,
                    "render": function ( data, type, row ) {
                        var u = "";
                        if (typeof data !== "undefined") {
                            $.each(users, function( index, value ) {
                                if (data == index) {
                                    u = value;
                                }
                            });
                        }
                        return u;
                    }
                },
            ],
		    "columnDefs": [
		        { "width": "10%", "targets": 0 },
		        { "width": "10%", "targets": 2 }
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
		    "language": dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
        	"dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
		});

        $('#table-observations_wrapper .tools').append($('#btns-tools-observations').contents());

        $('#btns-tools-observations').show();

        $('#table-observations tbody').on( 'click', 'tr', function () {

            if (!$(this).find('.dataTables_empty').length) {

                table_observations.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');

                observation_selected = table_observations.row( this ).data();

                table_selected = table_observations;
                $('#modal-edit-observation').modal('show');
            }
        });

        $('#btn-edit-observation').click(function() {
            table_selected = table_observations;
            $('#modal-edit-observation').modal('show');
        });

        $(".btn-export-observations").click(function() {

            bootbox.confirm('Se descargará un archivo Excel', function(result) {
                if (result) {
                    $('#table-observations').tableExport({tableName: 'Observaciones', type:'excel', escape:'false'});
                }
            }).find("div.modal-content").addClass("confirm-width-md");
        });

        $('.btn-add-observation').click(function() {
            table_selected = table_observations;
            customer_code = customer.code;
            $('#modal-add-observation').modal('show');
        });

        $('a[id="observations-tab"]').on('shown.bs.tab', function (e) {
            table_observations.draw();
        });

    });

</script>

<?php $this->end(); ?>
