<?php $this->extend('/Customers/views/transactions'); ?>

<?php $this->start('payment_commitment'); ?>

<style type="text/css">

    .my-hidden {
        display: none;
    }

</style>

<div id="btns-tools-payment_commitment">

    <div class="text-right btns-tools margin-bottom-5">

        <?php

            echo $this->Html->link(
                '<span class="fas fa-sync-alt" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Actualizar la tabla',
                'class' => 'btn btn-default btn-update-table-payment-commitment ml-1 mt-1',
                'escape' => false
            ]);

            $class_none = $customer->payment_commitment ? 'd-none' : '';
            echo $this->Html->link(
                '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Agregar Compromiso de Pago',
                'class' => 'btn btn-default btn-add-payment_commitment ml-1 mt-1 ' . $class_none,
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export-payment_commitment ml-1 mt-1',
                'escape' => false
            ]);

        ?>

    </div>
</div>

<div class="row">

    <div class="col-xl-12">
        <table class="table table-bordered table-hover" id="table-payment_commitment">
            <thead>
                <tr>
                    <th></th>            <!--0-->
                    <th>Fecha</th>       <!--1-->
                    <th>Detalle</th>     <!--2-->
                    <th>Vencimiento</th> <!--3-->
                    <th>Importe</th>     <!--4-->
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<?php
    echo $this->element('PaymentCommitment/add', ['modal' => 'modal-add-payment-commitment', 'title' => 'Agregar Compromiso de Pago']);
    echo $this->element('PaymentCommitment/edit', ['modal' => 'modal-edit-payment-commitment', 'title' => 'Editar Compromiso de Pago']);
?>

<script type="text/javascript">

    var payment_commitment_selected = null;
    var table_payment_commitment = null;
    var table_selected = null;

    $(document).ready(function() {

        $('#table-payment_commitment').removeClass('display');

        $('#btns-tools-payment_commitment').hide();

        table_payment_commitment = $('#table-payment_commitment').DataTable({
            "order": [[ 1, 'asc' ]],
            "autoWidth": true,
    	    "scrollY": '450px',
    	    "scrollCollapse": true,
    	    "scrollX": true,
            "processing": true,
            "serverSide": true,
		    "paging": true,
		    "ordering": true,
            "ajax": {
                "url": "/ispbrain/PaymentCommitment/getMovements.json",
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "where": {
                            "customer_code": customer_selected.code,
                        }
                    });
                },
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	},
            },
            "drawCallback": function( settings ) {
                var flag = true;
            	switch (settings.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            },
		    "columns": [
		        { 
                    "data": "created",
                    "render": function ( data, type, row ) {
                        var enabled = '<i class="far fa-money-bill-alt fa-2x" aria-hidden="true"></i>';
                        if (row.duedate) {
                            enabled = '<i class="far fa-handshake fa-2x" aria-hidden="true"></i>';
                        }
                        return enabled;
                    }
                },
		        { 
                    "data": "created",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                {
                   "className": " left",
                    "data": "detail",
                    "type": "string"
                },
                {
                    "data": "duedate",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        var created = "";
                        if (data) {
                            created = data.split('T')[0];
                            created = created.split('-');
                            created = created[2] + '/' + created[1] + '/' + created[0];
                        }
                        return created;
                    }
                },
                { 
                    "class": "right",
                    "data": "import",
                    "type": "decimal",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return number_format(data, true);
                        }
                        return "";
                    }
                },
            ],
		    "columnDefs": [
		        { "width": "5%", "targets": 0 },
		        { "width": "5%", "targets": 1 },
		        { "width": "70%", "targets": 2 },
		        { "width": "5%", "targets": 3 },
		        { "width": "5%", "targets": 4 },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
		    "language": dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
        	"dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
		});

        $('#table-payment_commitment_wrapper .tools').append($('#btns-tools-payment_commitment').contents());

        $('#btns-tools-payment_commitment').show();

        $('#table-payment_commitment tbody').on( 'click', 'tr', function () {

            if (!$(this).find('.dataTables_empty').length) {

                table_payment_commitment.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');

                payment_commitment_selected = table_payment_commitment.row( this ).data();

                table_selected = table_payment_commitment;

                if (payment_commitment_selected.duedate != null) {

                    if (new Date(payment_commitment_selected.duedate) > new Date()) {

                        $('#modal-edit-payment-commitment').modal('show');
                    }
                }
            }
        });

        $('#btn-edit-payment_commitment').click(function() {
            table_selected = table_payment_commitment;
            $('#modal-edit-payment-commitment').modal('show');
        });

        $('.btn-update-table-payment-commitment').click(function() {
            if (table_payment_commitment) {
                table_payment_commitment.draw();
            }
        });

        $(".btn-export-payment_commitment").click(function() {

            bootbox.confirm('Se descargará un archivo Excel', function(result) {
                if (result) {
                    $('#table-payment_commitment').tableExport({tableName: 'Compromiso de Pago', type:'excel', escape:'false'});
                }
            }).find("div.modal-content").addClass("confirm-width-md");
        });

        $('.btn-add-payment_commitment').click(function() {
            table_selected = table_payment_commitment;
            $('#modal-add-payment-commitment').modal('show');
        });

        $('a[id="payment_commitment-tab"]').on('shown.bs.tab', function (e) {
            table_payment_commitment.draw();
        });

    });

</script>

<?php $this->end(); ?>
