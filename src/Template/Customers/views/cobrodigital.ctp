<?php $this->extend('/Customers/views/tickets'); ?>

<?php $this->start('cobrodigital'); ?>
    <div class="row">
        <div class="col-md-4">
            <br>
            <table class="vertical-table">
                <tr class="d-none">
                    <th><?= __('Fecha') ?></th>
                    <td class="text-right"><?= $customer->cd_date ?></td>
                </tr>
                <tr>
                    <th><?= __('Habilitado') ?></th>
                    <td class="text-right"><?= $customer->cobrodigital ? 'SI' : 'NO' ?></td>
                </tr>
                <tr>
                    <th><?= __('Código de Barra') ?></th>
                    <td class="text-right">
                    <?= $customer->cd_barcode ?>
                    </td>
                </tr>
                <tr>
                    <th><?= __('Código Electrónico') ?></th>
                    <td class="text-right">
                    <?= $customer->cd_electronic_code ?>
                    </td>
                </tr>
                <tr>
                    <th><?= __('Adherido Débito Automático') ?></th>
                    <td class="text-right"><?= $customer->cd_auto_debit ? 'SI' : 'NO' ?></td>
                </tr>
                <tr>
                    <th><?= __('Nombre') ?></th>
                    <td class="text-right"><?= $customer->cd_firstname ?></td>
                </tr>
                <tr>
                    <th><?= __('Apellido') ?></th>
                    <td class="text-right"><?= $customer->cd_lastname ?></td>
                </tr>
                <tr>
                    <th><?= __('Correo') ?></th>
                    <td class="text-right"><?= $customer->cd_email ?></td>
                </tr>
                <tr>
                    <th><?= __('CUIT') ?></th>
                    <td class="text-right"><?= $customer->cd_cuit ?></td>
                </tr>
                <tr>
                    <th><?= __('CBU') ?></th>
                    <td class="text-right"><?= $customer->cd_cbu ?></td>
                </tr>
            </table>
        </div>
    </div> 
<?php $this->end(); ?>
