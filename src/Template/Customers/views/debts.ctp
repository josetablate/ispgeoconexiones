<?php $this->extend('/Customers/views/presupuestos'); ?>

<?php $this->start('debts'); ?>

<style type="text/css">

    .my-hidden {
        display: none;
    }

    .input-debt-month-red {
        font-size: 23px;
        padding: 0 15px 0 0;
        color: #f05f40;
    }

    .input-debt-month-green {
        font-size: 23px;
        padding: 0 15px 0 0;
        color: #28a745;
    }

    .last-cell-saldo-red {
        color: #f05f40;
        font-weight: bold;
    }

    .last-cell-saldo-green {
        color: #28a745;
        font-weight: bold;
    }

    .table-hover tbody tr:hover {
        background-color: white !important;
    }

    .table-hover tbody tr.selectable:hover {
        background-color: rgba(105, 104, 104, 0.28) !important;
    }

</style>

<div id="btns-tools-debts">

    <div class="text-right btns-tools margin-bottom-5">

        <?php 

            echo $this->Html->link(
                '<span class="fas fa-sync-alt" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Actualizar la tabla',
                'class' => 'btn btn-default btn-update-table-debt ml-1 mt-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon fas fa-search" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Buscar por Columnas',
                'class' => 'btn btn-default btn-search-column-debt ml-1 mt-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-eye" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Ocultar o Mostrar Columnas',
                'class' => 'btn btn-default btn-hide-column-debts ml-1 mt-1 d-none',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="far fa-calendar-plus" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Mostrar próximos vencimientos',
                'class' => 'btn btn-default btn-next-due-dates-debts ml-1 mt-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="fas fa-file-invoice aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Seleccionar y facturar',
                'class' => 'btn btn-default btn-generate-invoice-from-debt ml-1 mt-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export-debts ml-1 mt-1',
                'escape' => false
            ]);
        ?>

    </div>
</div>

<?php if ($customer->billing_for_service): ?>
    <div class="row">
        <div class="col-12">
            <div class="card border-secondary p-1 mb-2 mt-1 " id="card-customer-selector">
                <div class="card-body p-1">

                    <div class="row">
                        <div class="col-sm-12 col-md-9 col-lg-9 col-xl-9">
                            <?php echo $this->Form->input('connecton_id_debt', ['options' => $connectionsArray, 'label' => 'Servicios y productos', 'required' => true]); ?>
                        </div>
                        <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">

                            <div class="form-group text mb-0">
                                <label class="control-label" for="connection-saldo-month-debt" id="label-connection-saldo-debt">Deuda del mes</label>
                                <input type="text" name="connection_saldo_month_debt" class="text-right form-control input-debt-month-green" readonly="readonly" id="connection-saldo-month-debt">
                            </div>
                        </div>
                    </div>
    
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<div class="row">

    <div class="col-12">
         <small id="emailHelp" class="form-text text-muted">Movimientos no facturados. La siguiente tabla representa las lineas que posteriormente pasaran a ser conceptos en las facturas. <b>Destino</b> indica el tipo de comprobate que se generarán con dichos mivimientos.</small>
    </div>

    <div class="col-xl-12">
        <table class="table table-bordered table-hover" id="table-debts">
            <thead>
                <tr>
                    <th>Creado</th>         <!--0-->
                    <th>Usuario</th>        <!--1-->
                    <th>Perido</th>         <!--2-->
                    <th>Destino</th>        <!--3-->
                    <th>Cant.</th>          <!--4-->
                    <th>Descripción</th>    <!--5-->
                    <th>Subtotal</th>       <!--6-->
                    <th>Impuestos</th>      <!--7-->
                    <th>Venc.</th>          <!--8-->
                    <th>Total</th>          <!--9-->
                    <th>Saldo</th>          <!--10-->
                    <th>Nro asiento</th>    <!--11-->
                    
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade modal-generate-invoice-from-debt" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabel">Facturar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <div class="row">

                    <div class="col-6">
                        <?php
                            echo $this->Form->input('business_id', ['options' => $business, 'label' =>  'Empresa', 'value' => '']); 
                        ?>
                    </div>
                    <div class="col-6">
                        <?php
                            echo $this->Form->input('type', ['options' => [],  'label' => 'Tipo comprobante', 'required' => true]);
                        ?>
                    </div>
 
                </div>  

                <div class="row">

                    <div class="col-6">
                        <label class="control-label" for="">Fecha de emisión</label>
                        <div class='input-group date' id='created-datetimepicker'>
                            <input name="created" id="created"  type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar mr-0">
                                <span class="glyphicon icon-calendar mr-0"></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-6">
                        <?php
                            echo $this->Form->input('concept_type', ['label' => 'Tipo concepto', 'options' => $concept_types, 'value' => 2, 'id' => 'concept_type'])
                        ?>
                    </div>

                </div>

                <div class="row">
                     <div class="col-12">
                         <legend class="sub-title-sm">Período</legend>
                     </div>
                </div>

                <div class="row">
                     <div class="col-6">
                        <label class="control-label" for="">Desde</label>
                        <div class='input-group date' id='date_start-datetimepicker'>
                            <input name="date_start" id="date_start"  type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar mr-0">
                                <span class="glyphicon icon-calendar mr-0"></span>
                            </span>
                        </div>
                     </div>
                     <div class="col-6">
                        <label class="control-label" for="">Hasta</label>
                        <div class='input-group date' id='date_end-datetimepicker'>
                            <input name="date_end" id="date_end"  type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar mr-0">
                                <span class="glyphicon icon-calendar mr-0"></span>
                            </span>
                        </div>
                     </div>
                </div>

                <div class="row mt-2">
                     <div class="col-12">
                        <label class="control-label" for="">Venc. de Pago</label>
                        <div class='input-group date' id='duedate-datetimepicker'>
                            <input name="duedate" id="duedate"  type='text' class="form-control" required />
                            <span class="input-group-addon input-group-text calendar mr-0">
                                <span class="glyphicon icon-calendar mr-0"></span>
                            </span>
                        </div>
                     </div>
                </div>

                <div class="row mt-4">
                     <div class="col-12">
                         <button type="button" class="btn btn-default btn-cancel" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-danger btn-generate float-right">Confirmar</button>
                     </div>
                </div>

            </div>
        </div>
    </div>
</div>

<?php 

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-edit-debt',
        'name' =>  'Editar deuda',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-edit-discount',
        'name' =>  'Editar descuento',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-edit-administrative-movement-from-debt',
        'name' =>  'mover',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-success'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete-debt',
        'name' =>  'Eliminar',
        'icon' =>  'fa fa-times',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-debts', 'title' => 'Acciones', 'buttons' => $buttons ]);
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-debts']);
    
    echo $this->element('edit_debt_complete');
    echo $this->element('edit_discount');
?>

<script type="text/javascript">

    var table_debts = null;
    var move_selected = null;
    var table_selected = null;

    var table_debts_mode = null;
    var move_selected_list = [];

    $('.modal-generate-invoice-from-debt #created-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'DD/MM/YYYY'
    });

    $('.modal-generate-invoice-from-debt #date_start-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'DD/MM/YYYY'
    });

    $('.modal-generate-invoice-from-debt #date_end-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'DD/MM/YYYY'
    });

    var users = null;

    $('.modal-generate-invoice-from-debt #duedate-datetimepicker').datetimepicker({
        locale: 'es',
        defaultDate: new Date(),
        format: 'DD/MM/YYYY'
    });

    $(document).ready(function() {

        $('.modal-generate-invoice-from-debt #business-id').change(function() {

            var selected = $(this).val() ;

            business_selected = null;

            $('.modal-generate-invoice-from-debt #type').empty();

            $.each(sessionPHP.paraments.invoicing.business, function(i, business) {

                if (selected == business.id) {

                    business_selected = business;
 
                    $('.modal-generate-invoice-from-debt #type').append('<option value="">Seleccione Tipo</option>');

                    if (business.responsible == 1) { //ri

                        if (business.webservice_afip.crt != '') {
                            $('.modal-generate-invoice-from-debt #type').append('<optgroup label="Facturas"><option value="XXX">PRESU X</option><option value="001">FACTURA A</option><option value="006">FACTURA B</option></optgroup>');
                        } else {
                            $('.modal-generate-invoice-from-debt #type').append('<optgroup label="Facturas"><option value="XXX">PRESU X</option></optgroup>');
                        }
                    } else if (business.responsible == 6) { //monotributerou

                        if (business.webservice_afip.crt != '') {
                            $('.modal-generate-invoice-from-debt #type').append('<optgroup label="Facturas"><option value="XXX">PRESU X</option><option value="011">FACTURA C</option></optgroup>');
                        }  else {
                            $('.modal-generate-invoice-from-debt #type').append('<optgroup label="Facturas"><option value="XXX">PRESU X</option></optgroup>');
                        }
                   }
               }
           });
        });

        $('.modal-generate-invoice-from-debt #business-id').change();

        $('.btn-hide-column-debts').click(function() {
            $('.modal-hide-columns-debts').modal('show');
        });

        $('.btn-search-column-debt').click(function() {
            $('.modal-search-columns-debts').modal('show');
        });

        $('#table-debts').removeClass('display');

        $('#btns-tools').hide();

        loadPreferences('debts-cunstomer3-view', []);

        $('.btn-next-due-dates-debts').click(function() {

            if ($(this).hasClass('btn-next-due-dates-selected')) {

                $(this).removeClass('btn-next-due-dates-selected');

                $('#label-connection-saldo-debt').html('Deuda del mes');
            } else {

                $(this).addClass('btn-next-due-dates-selected');

                $('#label-connection-saldo-debt').html('Deuda total');
            }

            table_debts.draw();
        });

        var hide_forde_debts = [];
        var no_search_debts = [];

        if (!sessionPHP.paraments.accountant.account_enabled) {
            hide_forde_debts = [11];
            no_search_debts = [11];
        }

        table_debts = $('#table-debts').DataTable({
            "deferRender": false,
            "processing": true,
            "serverSide": true,
             "ajax": {
                "url": "/ispbrain/Customers/get_without_invoice_customer.json",
                "data": function ( d ) {
                  return $.extend( {}, d, {
                    "where": {
                        "customer_code": customer.code,
                        "connection_id": parseInt($('#connecton-id-debt').val())
                    },
                    "duedate": $('.btn-next-due-dates-debts').hasClass('btn-next-due-dates-selected') ? 1 : 0,
                    "complete": 1
                  });
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	},
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );

                    if (json.response.connection_saldo_month > 0) {
                        $('#connection-saldo-month-debt').removeClass('input-debt-month-green');
                        $('#connection-saldo-month-debt').addClass('input-debt-month-red');
                    } else {
                        $('#connection-saldo-month-debt').removeClass('input-debt-month-red');
                        $('#connection-saldo-month-debt').addClass('input-debt-month-green');
                    }

                    $('#connection-saldo-month-debt').val(number_format(json.response.connection_saldo_month, true));

                    return JSON.stringify( json.response );
                }
            },
            "drawCallback": function( settings ) {
                var flag = true;
            	switch (settings.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
            	    if (table_debts) {

                        var last_row = table_debts.row(':first').data();

                        if (last_row) {
                            if (last_row.saldo > 0) {
                                $('#table-debts tr#' + last_row.id).find('.column-saldo').addClass('last-cell-saldo-red');
                            } else {
                                $('#table-debts tr#' + last_row.id).find('.column-saldo').addClass('last-cell-saldo-green');
                            }
                        }

                        var tableinfo = table_debts.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column-debt').addClass('search-apply');
                        } else {
                            $('.btn-search-column-debt').removeClass('search-apply');
                        }
                    }
            	}
            },
		    "scrollY": true,
		    "scrollY": '260px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
		    "ordering": false,
		    "columns": [
		        { 
		            "class": "font-weight-bold",
                    "data": "date",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var date = data.split('T')[0];
                            date = date.split('-');
                            return date[2] + '/' + date[1] + '/' + date[0];
                        }
                    }
		        },
                { 
                    "class": "font-weight-bold",
                    "data": "username",
                    "type": "string"
                },
                { 
                    "class": "font-weight-bold",
                    "data": "period",
                },
                { 
                    "class": "font-weight-bold",
                    "data": "destino",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return sessionPHP.afip_codes.comprobantes[data];
                        }
                        return '';
                    }
                },
                { 
                    "class": "font-weight-bold",
                    "data": "quantity"
                },
                { 
                    "class": "font-weight-bold left",
                    "data": "description",
                    "type": "string"
                },
                { 
                    "class": "font-weight-bold right",
                    "data": "subtotal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '$ ' )
                },
                { 
                    "class": "font-weight-bold right",
                    "data": "sum_tax",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '$ ' )
                },
                { 
                    "class": "font-weight-bold",
                    "data": "duedate",
                    "type": "date",
                    "render": function ( data, type, row ) {

                        if (row.type_move == 'debt') {

                            var duedate = data.split('T')[0];
                            duedate = duedate.split('-');
                            return duedate[2] + '/' + duedate[1] + '/' + duedate[0];
                        }

                        return '';
                    }
                },
                { 
                    "class": "font-weight-bold right",
                    "data": "total",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '$ ' )
                },
                { 
                    "class": "font-weight-bold right column-saldo ",
                    "data": "saldo",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '$ ' )
                },
                {
                    "class": "font-weight-bold",
                    "data": "seating_number",
                    "type": "string",
                },
            ],
		    "columnDefs": [
                { 
                    "visible": false, targets: hide_forde_debts
                },
            ],
            "createdRow" : function( row, data, index ) {

                row.id = data.id;

                $(row).addClass('selectable');

                if (data.type_move == 'debt') {

                    row.id = 'debt_' +  data.id;

                    if (data.customers_has_discount) {
                        var row = table_debts.row($(row));
                        row.child( format( data ) ).show();
                    }

                } else {

                    row.id = 'discount_' +  data.id;
                }
            },
            "initComplete": function(settings, json) {
            },
		    "language": dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
        	"dom":
    	    	"<'row'<'col-xl-2'l><'col-xl-4 service'><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
		});

		function format ( d ) {

		    html = '<div class="row row p-0 mt-0 mr-0 mb-0 ml-1">';
                html += '<div class="col-xl-10 text-left " style="">';
                    html += '<span class="glyphicon icon-level-down"  aria-hidden="true"></span> &nbsp;&nbsp;';
                    html += d.customers_has_discount.description;
                    html += '&nbsp;&nbsp;&nbsp;&nbsp;';
                    html += number_format(-d.customers_has_discount.total, true);

                html += '</div>';
            html += '</div>';

            return html ;
        }

        $('#table-debts_wrapper .tools').append($('#btns-tools-debts').contents());

        $('#table-debts').on( 'init.dt', function () {
            createModalSearchColumn(table_debts, '.modal-search-columns-debts', no_search_debts);
        });

        $('#btns-tools-debts').show();

        $('#connecton-id-debt').change(function() {

            $(this).closest('div').addClass('mb-0');
            $('#connection-saldo-month-debt').closest('div').addClass('mb-0');
            $('#connection-saldo-total-debt').closest('div').addClass('mb-0');
            table_debts.draw();
        });

        $('#connecton-id-debt').change();

        $('#table-debts tbody').on( 'click', 'td', function () {

            if (!$(this).closest('tr').find('.dataTables_empty').length) {

                move_selected = table_debts.row( this).data();

                $('#btn-edit-administrative-movement-from-debt').hide();

                if (customer.billing_for_service) {
                    $('#btn-edit-administrative-movement-from-debt').show()
                }

                if (move_selected.type_move == 'debt') {

                    debt_selected = table_debts.row( this ).data();

		            $('.modal-debts #btn-edit-discount').hide();
		            $('.modal-debts #btn-edit-debt').show();

		        } else {

                    discount_selected = table_debts.row( this ).data();

		            $('.modal-debts #btn-edit-discount').show();
		            $('.modal-debts #btn-edit-debt').hide();
		        }

		        if (table_debts_mode == null) {

		             table_debts.$('tr.selected').removeClass('selected');
		             $(this).closest('tr').addClass('selected');
		             $('.modal-debts').modal('show');

		        } else if (table_debts_mode == 'generate-invoice') {

	               if ($.inArray($(this).closest('tr').attr('id'), move_selected_list) == -1) {

	                   move_selected_list.push($(this).closest('tr').attr('id'));
	                   $(this).closest('tr').addClass('selected');

	               } else {

                        var attr_id = $(this).closest('tr').attr('id');
                        move_selected_list = move_selected_list.filter(function(item) { 
                            return item !== attr_id 
                        });

	                   $(this).closest('tr').removeClass('selected');
	               }
		        }
            }
        });

        $('.btn-generate-invoice-from-debt').click(function() {

            if (table_debts_mode == null) {

                table_debts_mode = 'generate-invoice';

                move_selected_list = [];

                $('#table-debts_wrapper').addClass('border border-primary rounded p-2');
                $('.btn-generate-invoice-from-debt').addClass('border border-primary text-primary');
                table_debts.draw();

            } else if (table_debts_mode == 'generate-invoice') {

                 $('.modal-generate-invoice-from-debt').modal('show');
            }
        });

        $(document).on("click", ".modal-generate-invoice-from-debt .btn-cancel", function(e) {
            table_debts_mode =  null;
            $('#table-debts_wrapper').removeClass('border border-primary rounded p-2');
            $('.btn-generate-invoice-from-debt').removeClass('border border-primary text-primary');
            table_debts.draw();
        });

        $(document).on("click", ".modal-generate-invoice-from-debt .btn-generate", function(e) {

              if (move_selected_list.length > 0) {

                var d = new Date(); 
                var created = $('.modal-generate-invoice-from-debt #created').val().split('/');

                var h = addZero(d.getHours());
                var m = addZero(d.getMinutes());
                var s = addZero(d.getSeconds());

                created = created[2] + '-' + created[1] + '-' + created[0] + ' ' + h + ':' + m + ':' + s;

                var duedate = $('.modal-generate-invoice-from-debt #duedate').val().split('/');
                duedate = duedate[2] + '-' + duedate[1] + '-' + duedate[0] + ' ' + h + ':'+ m + ':' + s;

                var date_start = $('.modal-generate-invoice-from-debt #date_start').val().split('/');
                date_start = date_start[2] + '-' + date_start[1] + '-' + date_start[0] + ' ' + h + ':' + m + ':' + s;

                var date_end = $('.modal-generate-invoice-from-debt #date_end').val().split('/');
                date_end = date_end[2] + '-' + date_end[1] + '-' + date_end[0] + ' ' + h + ':' + m + ':' + s;
 
                var data = {
                    ids: move_selected_list,
                    date: created,
                    duedate: duedate,
                    concept_type: parseInt($('.modal-generate-invoice-from-debt #concept_type').val()),
                    date_start: date_start,
                    date_end: date_end,
                    type: $('.modal-generate-invoice-from-debt #type').val(),
                    business_id: $('.modal-generate-invoice-from-debt #business-id').val(),
                };

                created = new Date(data.date);
                duedate = new Date(data.duedate);
                date_start = new Date(data.date_start);
                date_end = new Date(data.date_end);

                var now = new Date();
                now.setHours(0);
                now.setMinutes(0);
                now.setSeconds(0);

                created.setHours(0);
                created.setMinutes(0);
                created.setSeconds(0);

                if (created > now) {
                    generateNoty('warning', 'La fecha de la factura no puede ser mayor a la fecha actual.');
                    return false;
                }

                if (duedate < created) {
                    generateNoty('warning', 'La fecha de vencimiento no puede ser menor a la fecha actual.');
                    return false;
                }

                if (data.concept_type != 1 && date_end < date_start) {
                    generateNoty('warning', 'Las fechas del período no son correctas.');
                    return false;
                }

                if ($('.modal-generate-invoice-from-debt #type').val() == "") {
                    generateNoty('warning', 'Debe seleccionar el Tipo comprobante.');
                    return false;
                }

                bootbox.confirm('Se generará la factura. ¿Desea continuar?', function(result) {
                    if (result) {

                        data.tab = "debts";
                        data.customer_code = customer_selected.code;

                        $('body')
                            .append( $('<form/>').attr({'action': '/ispbrain/invoices/generateIndiFromDebt/', 'method': 'post', 'id': 'form-block'})
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'data', 'value': JSON.stringify(data)}))
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                            .find('#form-block').submit();

                        openModalPreloader("Generando Factura. Espere Por favor ...");
                    }
                });

            } else {
                bootbox.alert('Debe seleccionar al menos una deuda para continuar.');
            }
        });

        $('.btn-update-table-debt').click(function() {
            if (table_debts) {
                table_debts.draw();
            }
        });

        $('a[id="debts-tab"]').on('shown.bs.tab', function (e) {
            if (table_debts) {
                table_debts.draw();
            }
        });

        $('#btn-edit-administrative-movement-from-debt').click(function() {
            $('.modal-debts').modal('hide');
            table_selected = table_debts;
            $('.modal-edit-administrative-movement').modal('show');
        });

        $('#modal-edit-administrative-movement').on('CHANGED_EDIT_ADMINISTRATIVE__MOVEMENT_SUCCESSFUL', function() {
            table_moves.draw();
        });

        $('#btn-delete-debt').click(function() {
            
            $('.modal-debts').modal('hide');

            switch (move_selected.type_move) {
                case 'debt':

                    bootbox.confirm("¿Está Seguro que desea eliminar la deuda?", function(result) {
        
                        if (result) {
                                
                            var request = $.ajax({
                                url: "<?= $this->Url->build(['controller' => 'Debts', 'action' => 'delete']) ?>",  
                                type: 'POST',
                                dataType: "json",
                                data: JSON.stringify(move_selected),
                            });
                            
                            request.done(function( data ) {
                              table_debts.draw();
                            });
                             
                            request.fail(function( jqXHR, textStatus ) {
                              generateNoty('error', 'Error al intenetar eliminar la deuda');
                            });
                        }
                    });
                    
                    break;
                    
                case 'customer_has_discount':
                   
                    bootbox.confirm("¿Está Seguro que desea eliminar el descuento?", function(result) {
        
                        if (result) {
                                
                            var request = $.ajax({
                                url: "<?= $this->Url->build(['controller' => 'CustomersHasDiscounts', 'action' => 'delete']) ?>",  
                                type: 'POST',
                                dataType: "json",
                                data: JSON.stringify(move_selected),
                            });
                            
                            request.done(function( data ) {
                              table_debts.draw();
                            });
                             
                            request.fail(function( jqXHR, textStatus ) {
                              generateNoty('error', 'Error al intenetar eliminar el descuento');
                            });
                        }
                    });
                    
                    break;
            }
        });

        $(".btn-export-debts").click(function() {

            bootbox.confirm('Se descargará un archivo Excel', function(result) {
                if (result) {
                    $('#table-debts').tableExport({tableName: 'Sin facturar', type:'excel', escape:'false'});
                }
            }).find("div.modal-content").addClass("confirm-width-md");
        });
    });

</script>

<?php $this->end(); ?>
