<?php $this->extend('/Customers/views/logs'); ?>

<?php $this->start('tickets'); ?>

<style type="text/css">

    #map {
		height: 350px;
	}

	.modal {
	    overflow-x: hidden;
	    overflow-y: auto;
	}

    .description {
        height: 145px;
        width: 100%;
        margin-bottom: 20px;
    }

    .my-hidden {
        display: none;
    }

    .actions-tickets {
        border: 1px solid #cccccc59;
        padding: 18px;
        font-family: sans-serif;
    }

    .view-tickets {
        border: 1px solid #cccccc59;
        padding: 18px;
        font-family: sans-serif;
    }

    .card-header {
        font-size: 12px;
    }

    .badge {
        font-size: 54%;
    }

    .btn-default.active {
        color: #f05f40;
        background-color: #e6e6e6;
        border-color: #f05f40;
    }

    .table-tickets-records th {
        background-color: #607D8B !important;
        color: white !important;
    }

</style>

<div class="row" id="container-tickets">
    <div class="col-xl-12">
        <table class="table table-hover table-bordered" id="table-tickets">
           <thead>
               <tr>
                    <th></th>           <!--0-->
                    <th>#</th>          <!--1-->
                    <th>Creado</th>     <!--2-->
                    <th>Modificado</th> <!--3-->
                    <th>Usuario</th>    <!--4-->
                    <th>Estado</th>     <!--5-->
                    <th>Categoría</th>  <!--6-->
                    <th>Cód.</th>       <!--7-->
                    <th>Cliente</th>    <!--8-->
                    <th>Dom.</th>       <!--9-->
                    <th>Inicio</th>     <!--10-->
                    <th>Asignado</th>   <!--11-->
                    <th>Título</th>     <!--12-->
                    <th>Área</th>       <!--13-->
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5">

        <?php
            echo $this->Html->link(
                '<span class="glyphicon fas fa-search" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Buscar por columnas',
                'class' => 'btn btn-default btn-search-column ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Agregar nuevo ticket',
                'class' => 'btn btn-default btn-add-ticket ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="fa fa-ticket-alt" aria-hidden="true"></span>',
                ['controller' => 'Tickets', 'action' => 'index_table'],
                [
                'title' => 'Ir a la lista de Tickets',
                'class' => 'btn btn-default btn-tickets ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-pdf" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar PDF',
                'class' => 'btn btn-default btn-export-pdf ml-1',
                'escape' => false
            ]);
        ?>
    </div>
</div>

<?php
    echo $this->element('search_columns', ['modal' => 'modal-search-columns-tickets']);
    echo $this->element('tickets/add', ['modal' => 'modal-add-ticket', 'title' => 'Agregar Ticket', 'categorias' => $categorias, 'customer' => $customer, 'controllers' => $controllers, 'services' => $services, 'connectionsArray' => $connectionsArray, 'areas' => $areas]);
?>

<script type="text/javascript">

    var table_tickets = null;
    var users;
    var status_ti = <?php echo json_encode($status_ti); ?>;
    var categories = <?php echo json_encode($categories); ?>;
    var curr_pos_scroll;
    var customer;
    var areas = null;

    function format(d) {

        var html = '<table class="table table-tickets-records">'
            + '<thead>'
                + '<tr>'
                  + '<th>Fecha</th>'
                  + '<th>Acción</th>'
                  + '<th>Descripción</th>'
                  + '<th>Imagen</th>'
                  + '<th>Usuario</th>'
                + '</tr>'
              + '</thead>'
            + '<tbody>';

        $.each(d.tickets_records, function(i, ticket_record) {
            var created = '';
            if (ticket_record.created) {
                created = ticket_record.created.split('T')[0];
                created = created.split('-');
                created = created[2] + '/' + created[1] + '/' + created[0];
            }
            var short_action = ticket_record.short_description ? ticket_record.short_description : '';

            var image = '';

            if (ticket_record.image) {
                image = '<img id="img-signature" src="/ispbrain/ticket_image/' + ticket_record.image + '" class="img-fluid" alt="Firma">';
            } else {
                image = '';
            }

            html +=
            '<tr>'
              + '<td>' + created + '</td>'
              + '<td>' + short_action + '</td>'
              + '<td>' + ticket_record.description + '</td>'
              + '<td>' + image + '</td>'
              + '<td>' + ticket_record.user.name + '</td>'
            + '</tr>';
        });

        html += '</tbody>'
            + '</table>';

        return html;
    }

    $(document).ready(function() {

        users = <?= json_encode($users) ?>;
        customer = <?= json_encode($customer) ?>;
        var code = customer.code;
        areas = <?= json_encode($areas) ?>;

        table_tickets = $('#table-tickets').DataTable({
    	    "order": [[ 1, 'desc' ]],
    	    "autoWidth": true,
    	    "scrollY": '350px',
    	    "scrollCollapse": true,
    	    "scrollX": true,
    	    "sWrapper":  "dataTables_wrapper dt-bootstrap4",
            "processing": true,
            "serverSide": true,
		    "ajax": {
                "url": "/ispbrain/tickets/get_tickets_from_table/" + code + ".json",
                "dataFilter": function(data) {
                    var json = $.parseJSON(data);
                    return JSON.stringify(json.response);
                },
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "status": 0,
                        "archived": 0
                    });
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "drawCallback": function(c) {
                var flag = true;
            	switch (c.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
            	    if (table_tickets) {

                        var tableinfo = table_tickets.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }

                        if (curr_pos_scroll) {
                            $(table_tickets.settings()[0].nScrollBody).scrollTop(curr_pos_scroll.top);
                            $(table_tickets.settings()[0].nScrollBody).scrollLeft(curr_pos_scroll.left);
                        }
                    }
            	}
            },
            "columns": [
                {
                    "className": 'details-control ',
                    "orderable": false,
                    "data":      'tickets_records',
                    "render": function ( data, type, full, meta ) {
                        var num = data.length;
                        if (num > 0) {
                            return "<a href='javascript:void(0)' class='grey'>"
                                + "<span class='icon-plus' aria-hidden='true'>"
                                + "</span>"
                                + "</a>";
                        }

                        return '';
                    },
                    "width": '3%'
                },
                { 
                    "data": "id",
                    "type": "integer"
                },
                { 
                    "data": "created",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                { 
                    "data": "modified",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                { 
                    "data": "user_id",
                    "type": "options",
                    "options": users,
                    "render": function ( data, type, row ) {
                        var u = "";
                        if (typeof data !== "undefined") {
                            $.each(users, function( index, value ) {
                                if (data == index) {
                                    u = value;
                                }
                            });
                        }
                        return u;
                    }
                },
                { 
                    "data": "status.name",
                    "type": "options",
                    "options": status_ti,
                },
                {
                    "data": "category.name",
                    "type": "options",
                    "options": categories,
                    "render": function ( data, type, row ) {
                        return '<span class="badge" id="category" style="font-size: 100%; color: ' + row.category.color_text + '; background-color: ' + row.category.color_back + '">' + data + '</span>';
                    }
                },
                { 
                    "data": "customer",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        var customer = "";
                        if (data) {
                            customer = pad(data.code, 5);
                        }
                        return customer;
                    }
                },
                { 
                    "data": "customer",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        var customer = "";
                        if (data) {
                            customer = data.name
                        }
                        return customer;
                    }
                },
                { 
                    "data": "customer",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        var customer = "";
                        if (data) {
                            customer = data.address
                        }
                        return customer;
                    }
                },
                { 
                    "data": "start_task",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            var created_2 = data.split('T')[1].split('-')[0].split(':');

                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0] + ' ' + created_2[0] + ':' + created_2[1];
                        }
                    }
                },
                { 
                    "data": "asigned_user_id",
                    "type": "options",
                    "options": users,
                    "render": function ( data, type, row ) {
                        var u = "";
                        if (typeof data !== "undefined") {
                            $.each(users, function( index, value ) {
                                if (data == index) {
                                    u = value;
                                }
                            });
                        }
                        return u;
                    }
                },
                {
                    "class": "col-description",
                    "data": "title",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        return data;
                    }
                },
                {
                    "data": "area_id",
                    "type": "options",
                    "options": areas,
                    "render": function ( data, type, row ) {
                        var u = "";
                        if (typeof data !== "undefined" && data != null) {
                            u = areas[data];
                        }
                        return u;
                    }
                },
            ],
            "columnDefs": [
                { 
                    "visible": true
                },
                { 
                    "class": "left", 
                    "width": "15%",
                    "targets": [8]
                },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
    	    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
    	});

		$('#table-tickets_filter').closest('div').before($('#btns-tools').contents());

		$('#table-tickets').on( 'init.dt', function () {
            createModalSearchColumn(table_tickets, '.modal-search-columns-tickets');
        });

        $('#btns-tools').show();

		$('#table-tickets tbody').on('click', 'td.details-control', function () {

            if (!$(this).closest('tr').find('.dataTables_empty').length) {
                table_tickets.$('tr.selected').removeClass('selected');
                $(this).closest('tr').addClass('selected');
            }

            var tr = $(this).closest('tr');
            var row = table_tickets.row( tr );

            if ( row.child.isShown() ) {
                row.child.hide();
                tr.removeClass('shown');
                $(this).find('span').removeClass('icon-minus');
                $(this).find('span').addClass('icon-plus');
            } else {
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
                $(this).find('span').removeClass('icon-plus');
                $(this).find('span').addClass('icon-minus');
            }
        });

        $(".btn-export-pdf").click(function() {

            var tickets = table_tickets.data().toArray();

            if (tickets.length > 0) {
		        var action = "/ispbrain/Tickets/ticketExportPDF/";
                $('body')
                    .append( $('<form/>').attr({'action': action, 'method': 'post', 'id': 'replacer'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'tickets', 'value': JSON.stringify(tickets)}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer').submit();
		    } else {
		        generateNoty('warning', 'Debe generar una búsqueda de Tickets para poder exportar los mismos');
		    }
        });

        $('.btn-search-column').click(function() {
            $('.modal-search-columns-tickets').modal('show');
        });

        $('.modal-search-columns-tickets').on('hidden.bs.modal', function () {
            $('.modal-backdrop').remove(); 
        });

        $('.btn-add-ticket').click(function() {
            $('.modal-add-ticket').modal('show');
        });
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

    $('a[id="tickets-tab"]').on('shown.bs.tab', function (e) {
        table_tickets.ajax.reload();
    });

</script>

<?php $this->end(); ?>
