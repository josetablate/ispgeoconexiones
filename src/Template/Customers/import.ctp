


<div class="col-md-8">
    <legend class="sub-title">Imporat CSV Clientes</legend>
        <?= $this->Form->create($customer, ['type' => 'file','class'=>'form-inline','role'=>'form']) ?>
        <div class="form-group">
            <label class="sr-only" for="csv"> CSV </label>
            <?php echo $this->Form->input('csv', ['type'=>'file', 'accept' => '.csv', 'class' => 'form-control', 'label' => false, 'placeholder' => 'csv upload',]); ?>
        </div>
        <button type="submit" class="btn btn-default"> Upload </button>
    <?= $this->Form->end() ?>
</div>