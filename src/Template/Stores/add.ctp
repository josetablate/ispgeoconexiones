<style type="text/css">

     textarea.form-control {
            height: 50px !important;
    }
    
</style>

    <div class="row">

        <div class="col-md-3">
            <?= $this->Form->create($store,  ['class' => 'form-load']) ?>
            <fieldset>
                <?php
                    echo $this->Form->input('name', ['label' => 'Nombre *']);
                    echo $this->Form->input('address', ['label' => 'Domicilio *']);
                    echo $this->Form->input('user_id', ['label' => 'Responsable *', 'options' => $users]);
                    echo $this->Form->input('comments', ['label' => 'Comentarios']);
                    echo $this->Form->input('enabled', ['label' => 'Habilitar/Deshabilitar', 'checked' => true, 'class' => 'input-checkbox' ]);
                ?>
            </fieldset>
            <?= $this->Html->link(__('Cancelar'),["controller" => "Stores", "action" => "index"], ['class' => 'btn btn-default']) ?>
                <?= $this->Form->button(__('Agregar'), ['class' => 'btn-submit btn-success']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
