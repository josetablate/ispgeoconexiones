<style type="text/css">

    #table-stores {
        width: 100% !important;
    }

    #table-stores td {
        margin: 0px 0px 0px 0px !important;
        padding: 0px 0px 0px 0px !important;
        vertical-align: middle;
    }

    textarea.form-control {
            height: 100px !important;
    }

    .modal-actions a.btn {
        width: 100%;
        margin-bottom: 5px;
    }

    tr.selected {
        background-color: #8eea99;
        color: #333 !important;
    }

    .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
        background-color: #d2d2d2;
    }

</style>

<div id="btns-tools">
    <div class="btns-tools margin-bottom-5">

        <?php 

            echo  $this->Html->link(
                '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                ["controller" => "stores", "action" => "add"],
                [
                'title' => 'Agregar nuevo depósito',
                'class' => 'btn btn-default btn-add ml-2',
                'escape' => false
            ]);

            echo  $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export ml-2',
                'escape' => false
            ]);
        ?>

    </div>
</div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-hover table-bordered" id="table-stores">
                <thead>
                    <tr>
                        <th>Activo</th>
                        <th>Nombre</th>
                        <th>Domicilio</th>
                        <th>Responsable</th>
                        <th>Creado</th>
                        <th>Modificado</th>
                        <th>Comentario</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($stores as $store): ?>
                    <tr data-id="<?=$store->id?>" data-name="<?=$store->name?>" data-userid="<?=$store->user_id?>" data-comments="<?=$store->comments?>" data-address="<?=$store->address?>" data-enabled="<?=($store->enabled == 0) ? '0' : '1' ?>" >
                        <?php if($store->enabled):?>
                            <td>
                                <span class="fa fa-check green" aria-hidden="true"></span>
                                <span style="display:none;" >1</span>
                            </td>
                        <?php else: ?>
                            <td>
                                <span class="fa fa-times red" aria-hidden="true"></span>
                                <span style="display:none;" >0</span>
                            </td>
                        <?php endif; ?>
                        <td><?= h($store->name) ?></td>
                        <td><?= h($store->address) ?></td>
                        <td><?= $store->user->username ?></td>
                        <td><?= date('d/m/Y H:i', strtotime($store->created))?></td>
                        <td><?= date('d/m/Y H:i', strtotime($store->modified))?></td>
                        <td><?= h($store->comments) ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade modal-actions" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel">Acciones</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <center>
                            <a class="btn btn-default " id="btn-edit" href="#" role="button">
                                <span class="icon-pencil2"  aria-hidden="true"></span>
                                Editar&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </a>
                            <a class="btn btn-danger btn-delete2" data-controller="Stores" data-text="¿Está Seguro que desea eliminar el depósito ?" href="#" role="button">
                                <span class="glyphicon icon-bin"  aria-hidden="true"></span>
                                Eliminar&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </a>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Editar Depósito</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-md-12">

                        <form method="post" accept-charset="utf-8" role="form" action="<?=$this->Url->build(["controller" => "Stores", "action" => "edit"])?>">
                            <div style="display:none;">
                                <input type="hidden" name="_method" value="POST">
                            </div>
                            <?php
                                echo $this->Form->input('name', ['label' => 'Nombre *']);
                                echo $this->Form->input('address', ['label' => 'Domicilio *']);
                                echo $this->Form->input('user_id', ['label' => 'Responsable *', 'options' => $users]);
                                echo $this->Form->input('comments', ['label' => 'Comentarios']);
                            ?>

                            <div class="checkbox">
                                <input type="hidden" name="enabled" value="0">
                                <label for="enabled" class="ui-checkboxradio-label ui-corner-all ui-button ui-widget ui-checkboxradio-checked ui-state-active text-white">
                                    <input type="checkbox" name="enabled" value="1" class="input-checkbox ui-checkboxradio ui-helper-hidden-accessible" id="enabled" checked="checked">
                                    Habilitar/Deshabilitar
                                </label>
                            </div>

                            <?php echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]); ?>

                            <input type="hidden" name="id" id="id"/>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-success" id="btn-confirm-edit">Guardar</button> 

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var table_stores = null;

    $(document).ready(function () {

        $('#table-stores').removeClass('display');

		table_stores = $('#table-stores').DataTable({
		    "order": [[ 0, 'desc' ]],
		    "autoWidth": true,
		    "scrollY": '350px',
		    "scrollCollapse": true,
		    "scrollX": true,
		      "columnDefs": [
             { "type": "datetime-custom", "targets": [4,5] }
             ],
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            "dom":
    	    	"<'row'<'col-md-6'l><'col-md-4'f><'col-md-2 tools'>>" +
        		"<'row'<'col-md-12'tr>>" +
        		"<'row'<'col-md-5'i><'col-md-7'p>>",
		});

	    $('#table-stores_wrapper .tools').append($('#btns-tools').contents());
        
    });

    $(".btn-export").click(function() {

        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if (result) {
                $('#table-stores').tableExport({tableName: 'Despósitos', type:'excel', escape:'false'});
            }
        });

    });

    $('#table-stores tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_stores.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');

            $('.modal-actions').modal('show');
        }
    });

    $('a#btn-edit').click(function() {

        $('.modal-actions').modal('hide');

        $('#modal-edit #id').val(table_stores.$('tr.selected').data('id'));
        $('#modal-edit #name').val(table_stores.$('tr.selected').data('name'));
        $('#modal-edit #address').val(table_stores.$('tr.selected').data('address'));
        $('#modal-edit #user-id').val(table_stores.$('tr.selected').data('userid'));
        $('#modal-edit #comments').val(table_stores.$('tr.selected').data('comments'));

        if (table_stores.$('tr.selected').data('enabled') == '1') {
            $('#modal-edit #enabled').closest('label').addClass('ui-state-active');
            $('#modal-edit #enabled').closest('label').addClass('ui-checkboxradio-checked');
            $('#modal-edit #enabled').attr('checked', true);
        } else {
            $('#modal-edit #enabled').attr('checked', false);
            $('#modal-edit #enabled').closest('label').removeClass('ui-state-active');
            $('#modal-edit #enabled').closest('label').removeClass('ui-checkboxradio-checked');
        }

        $('#modal-edit #enabled').val(table_stores.$('tr.selected').data('enabled'));
        $('#modal-edit').modal('show');
    });

    $('#modal-edit #enabled').click(function() {

      if ($('#modal-edit #enabled').val() == '1') {
          $('#modal-edit #enabled').val(0);
      } else {
          $('#modal-edit #enabled').val(1);
      }
    
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

	$('a.btn-delete2').click(function() {

        var text = $(this).data('text');
        var controller  = $(this).data('controller');
        var id  = table_stores.$('tr.selected').data('id');

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                    .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/delete/', 'method': 'post', 'id': 'replacer'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer').submit();
            }
        });
        
    });

</script>
