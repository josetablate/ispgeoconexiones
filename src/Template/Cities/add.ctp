
<div class="row">
    <div class="col-md-3">
        <?= $this->Form->create($city, ['class' => 'form-load']) ?>
        <fieldset>
            <?php
                echo $this->Form->input('name', ['label' => 'Nombre']);
                echo $this->Form->input('cp', ['label' => 'Código Postal', 'value' => '']);
                echo $this->Form->input('province_id', ['label' => 'Provincia', 'options' => $provinces, 'value' => $paraments->customer->province_default ? $paraments->customer->province_default : 1, 'class' => 'selectpicker', 'data-live-search' => "true"]);
                echo $this->Form->input('enabled', ['checked' => true, 'label' => 'Habilitado']);
            ?>
        </fieldset>
         <?= $this->Html->link(__('Cancelar'),["controller" => "Areas", "action" => "index"], ['title' => 'Ir a la lista de Áreas', 'class' => 'btn btn-default']) ?>
        <?= $this->Form->button(__('Agregar'), ['class' => 'btn-success' ]) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
