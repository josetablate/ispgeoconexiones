<style type="text/css">

    .intput-enabled {
        width: 100%;
    }

    .modal span {
        margin-right: 0 !important;
    }

</style>

<div id="btns-tools">

    <div class="text-right btns-tools margin-bottom-5">

        <?php

            echo $this->Html->link(
                '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                ['controller' => 'cities', 'action' => 'add'],
                [
                'title' => 'Agregar nueva ciudad',
                'class' => 'btn btn-default ',
                'escape' => false
            ]);

             echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar en formato excel el contenido de la tabla',
                'class' => 'btn btn-default btn-export ml-1',
                'escape' => false
            ]);
        ?>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-hover table-bordered" id="table-cities">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>CP</th>
                    <th>Provincia</th>
                    <th>Activo</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-edit',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-default'
    ];

    echo $this->element('actions', ['modal'=> 'modal-cities', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Editar Ciudad</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-md-12">

                        <form method="post" accept-charset="utf-8" role="form" action="<?= $this->Url->build(["controller" => "cities", "action" => "edit"]) ?>">
                            <div style="display:none;">
                                <input type="hidden" name="_method" value="POST">
                            </div>
                            <?php
                                echo $this->Form->input('name', ['label' => 'Nombre']);
                                echo $this->Form->input('cp', ['label' => 'CP']);
                                echo $this->Form->input('province_id', ['label' => 'Provincia', 'options' => $provinces, 'class' => 'selectpicker', 'data-live-search' => "true"]);
                                echo $this->Form->input('enabled', ['label' => 'Habilitado', 'type' => 'checkbox']);
                                echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]);
                            ?>
                            <input type="hidden" name="id" id="id"/>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-success" id="btn-confirm-edit">Guardar</button> 

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var table_cities = null;
    var city_selected = null;

    $(document).ready(function () {

        $('#table-cities').removeClass('display');

    	table_cities = $('#table-cities').DataTable({
    	    "order": [[ 0, 'desc' ]],
    	    "autoWidth": true,
    	    "scrollY": '350px',
    	    "scrollCollapse": true,
    	    "scrollX": true,
    	    "sWrapper":  "dataTables_wrapper dt-bootstrap4",
            "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/cities/index.json",
                "dataSrc": "cities",
                "error": function(c) {
                    switch (c.status) {
                        case 400:
                            flag = false;
                            generateNoty('warning', 'Ha ocurrido un error.');
                            break;
                        case 401:
                            flag = false;
                            generateNoty('warning', 'Error, no posee una autorización.');
                            break;
                        case 403:
                            flag = false;
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                        	setTimeout(function() { 
                        	  window.location.href ="/ispbrain";
                        	}, 3000);
                            break;
                    }
                }
            },
            "columns": [
                { 
                    "data": "id",
                },
                { 
                    "data": "name",
                },
                { 
                    "data": "cp",
                },
                { 
                    "data": "province.name"
                },
                { 
                    "data": "enabled",
                    "render": function ( data, type, row ) {
                        var enabled = '<i class="fa fa-times red" aria-hidden="true"></i>';
                        if (data) {
                            enabled = '<i class="fa fa-check green" aria-hidden="true"></i>';
                        }
                        return enabled;
                    }
                },
            ],
            "columnDefs": [
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
    	    "language": dataTable_lenguage,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Todas"]],
            "dom":
        		"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
    	});

		$('#table-cities_wrapper .tools').append($('#btns-tools').contents());

		$('#btns-tools').show();
    });

	$('#table-cities tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_cities.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            city_selected = table_cities.row( this ).data();

            $('.modal-cities').modal('show');
        }
    });

    $('#btn-edit').click(function() {

        $('.modal-cities').modal('hide');

        $('#modal-edit #id').val(city_selected.id);
        $('#modal-edit #cp').val(city_selected.cp);
        $('#modal-edit #name').val(city_selected.name);

        $('#modal-edit #province-id').val(city_selected.province_id);
        $('#modal-edit #province-id').closest('div').find('button').attr('title', city_selected.province.name);
        $('#modal-edit #province-id').closest('div').find('.filter-option').html(city_selected.province.name);

        $('#modal-edit #enabled').attr('checked', city_selected.enabled);

        $('#modal-edit').modal('show');
    });

    $('#btn-delete').click(function() {

        var text = "¿Está Seguro que desea eliminar el ciudad?";
        var id = city_selected.id;

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/cities/delete/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#replacer').submit();
            }
        });
    });

    $(".btn-export").click(function() {

        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if (result) {
                $('#table-cities').tableExport({tableName: 'Articulos', type:'excel', escape:'false'});
            }
        });
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
