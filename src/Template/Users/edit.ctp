<style type="text/css">

    select[multiple].actions-system, select[size].actions-system {
        height: 450px;
    }

    select.form-control {
            height: 450px !important;
    }

    .table {
        max-height: 450px;
        height: 450px;
        display: inline-block;
        width: 100%;
        overflow: auto;
   }

   .rol-system {
        font-size: 16px;
        font-weight: 500;
   }
 
</style>

<div class="row">
     <div class="col-md-10">
        <div class="text-right">
            <a class="btn btn-default" title="Agregar nuevo usuario" href="<?=$this->Url->build(["action" => "add"])?>" role="button">
                <span class="glyphicon icon-plus"  aria-hidden="true"></span>
            </a>
             <?= $this->Html->link(__("<span class='glyphicon icon-bin' title='Eliminar usuario' aria-hidden='true'></span>"), 
                '#',
                [
                    'class' => 'btn btn-default btn-delete', 
                    'role' => 'button',
                    'data-controller' => 'users',
                    'data-id' => $user->id,
                    'data-text' =>  __('¿Está Seguro que desea eliminar el usuario <strong>{0}</strong>?', $user->username),
                    'escape' => false
                ]) ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $this->Form->create($user,  ['class' => 'form-load']) ?>

        <fieldset>
            <div class="row">

                <div class="col-md-3">

                    <table style="width:100%">
                        <tr>
                            <td style="width:30%"><label class="control-label" for="">Nombre</label></td>
                            <td style="width:70%"><?php echo $this->Form->text('name'); ?></td>
                        </tr>
                        <tr>
                            <td style="width:30%"><label class="control-label" for="">Usuario</label></td>
                            <td style="width:70%"><?php echo $this->Form->text('username'); ?></td>
                        </tr>

                        <tr>
                            <td style="width:30%"><label class="control-label" for="">Clave</label></td>
                            <td style="width:70%"><?php echo $this->Form->text('password', ['type'=> 'password', 'required' => false, 'value' => '']); ?></td>
                        </tr>
                        <tr>
                            <td style="width:30%"><label class="control-label" for="">Teléfono</label></td>
                            <td style="width:70%"><?php echo $this->Form->text('phone'); ?></td>
                        </tr>

                        <tr>
                            <td style="width:30%"><label class="control-label" for="">Correo</label></td>
                            <td style="width:70%"><?php echo $this->Form->email('email'); ?></td>
                        </tr>

                        <tr>
                            <td style="width:30%"><label class="control-label" for="">Descripción</label></td>
                            <td style="width:70%"><?php echo $this->Form->text('description'); ?></td>
                        </tr>

                        <tr>
                            <td colspan="2" style="width:100%"><?php echo $this->Form->input('enabled', [
                                'label' => [
                                    'text' => "Habilitar/Deshabilitar <span class='fa fa-question-circle' title='Habilitar o deshabilitar el usuario.'  aria-hidden='true' ></span>",
                                    'escape' => false],
                                'class' => 'input-checkbox']); ?>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="col-md-3">
                    <label for="">Privilegios Seleccionados</label>
                    <table class="table" id="table-selected">
                    </table>
                </div>

                <div class="col-md-4">
                    <?php

                        $roles_array = [];

                        foreach ($roles as  $rol) {

                            if ($rol->enabled && !$rol->deleted) {
                                $roles_array[$rol->id] =  $rol->name;
                            } else {

                                foreach ($user->roles as $rol_user) {
                                    if ($rol->id == $rol_user->id) {
                                        $roles_array[$rol->id] =  $rol->name;
                                    }
                                }
                            }
                        }

                        echo $this->Form->input('roles._ids', [
                            'options' => $roles_array,
                            'label' => [
                                'text' => "Privilegios <span class='fa fa-question-circle' title='Los privilegios son un grupo de reglas que determina las acciones que el usuario tiene permitido realizar dentro del sistema..'  aria-hidden='true' ></span>",
                                'escape' => false
                            ],
                        ]);
                    ?>
                </div>

            </div>

        </fieldset>
        <?= $this->Html->link(__('Cancelar'), ["controller" => "Users", "action" => "index"], ['class' => 'btn btn-default']) ?>
        <?= $this->Form->button(__('Guardar'), ['class' => 'btn-submit btn-success']) ?>
        <?= $this->Form->end() ?>

    </div>
</div> <!-- end row -->

<script type="text/javascript">

    $(document).ready(function() {

        $('#roles-ids').change(function() {
            var tr = "";
            $( "#roles-ids option:selected" ).each(function() {
              tr += '<tr><td class="left rol-system">' + $( this ).html() + "</td></tr>";
            });
            $( "#table-selected" ).html( tr );
        }).trigger( "change" );
    });

    /**
     * confirmacion de eliminacion general 
     */
    $(document).on("click", ".btn-delete", function(e) {

        var text = $(this).data('text');
        var controller = $(this).data('controller');
        var id = $(this).data('id');

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                    .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/delete/', 'method': 'post', 'id': 'replacer'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer').submit();
            }
        });
    });

</script>
