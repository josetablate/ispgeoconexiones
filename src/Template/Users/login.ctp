
<br>
<br>

<div class="mx-auto text-right" style="width: 240px;">
  <?php echo $this->Html->image('logo.png', ['alt' => 'ISPGEO', 'width' => 240]);?>
  <br>
  <p class="text-muted text-secondary text-right"> <?='v'. $version?></p>
</div>

<div class="mx-auto" style="width: 200px;">
  <?= $this->Form->create() ?>
  <?= $this->Form->input('username', ['label' => __('Usuario'), 'autofocus' => true, 'value' => $cookie ? $cookie['username'] : '']) ?>
  <?= $this->Form->input('password', ['label' => __('Clave'), 'value' => $cookie ? $cookie['password'] : '']) ?>
  <?= $this->Form->input('remember_me', ['type' => 'checkbox', 'label' => 'Recordar contraseña', 'required' => false, 'checked' => $cookie ? $cookie['remember_me'] : false]); ?>
  <?= $this->Form->button(__('Entrar'), ['class' => 'btn btn-primary']) ?>
  <?= $this->Form->end() ?>
</div>
