<style type="text/css">

    select[multiple].actions-system, select[size].actions-system {
        height: 450px;
    }

    select.form-control {
        height: 450px !important;
    }

    .table {
        max-height: 450px;
        height: 450px;
        display: inline-block;
        width: 100%;
        overflow: auto;
    }

   .rol-system {
        font-size: 16px;
        font-weight: 500;
    }

</style>

<div class="row">
    <div class="col-md-12">
        <?= $this->Form->create($user, ['class' => 'form-load']) ?>

        <fieldset>
            <div class="row">

                <div class="col-md-3">

                    <table style="width:100%">
                        <tr>
                            <td style="width:30%"><label class="control-label" for="">Nombre</label></td>
                            <td style="width:70%"><?php echo $this->Form->text('name'); ?></td>
                        </tr>
                        <tr>
                            <td style="width:30%"><label class="control-label" for="">Usuario</label></td>
                            <td style="width:70%"><?php echo $this->Form->text('username'); ?></td>
                        </tr>
                        <tr>
                            <td style="width:30%"><label class="control-label" for="">Clave</label></td>
                            <td style="width:70%"><?php echo $this->Form->text('password', ['type'=> 'password']); ?></td>
                        </tr>
                        <tr>
                            <td style="width:30%"><label class="control-label" for="">Teléfono</label></td>
                            <td style="width:70%"><?php echo $this->Form->text('phone'); ?></td>
                        </tr>
                        <tr>
                            <td style="width:30%"><label class="control-label" for="">Correo</label></td>
                            <td style="width:70%"><?php echo $this->Form->email('email'); ?></td>
                        </tr>
                        <tr>
                            <td style="width:30%"><label class="control-label" for="">Descripción</label></td>
                            <td style="width:70%"><?php echo $this->Form->text('description'); ?></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width:100%"><?php echo $this->Form->input('enabled', [
                                'label' => [
                                    'text' => "Habilitar/Deshabilitar <span class='fa fa-question-circle' title='Habilitar o deshabilitar el usuario.' aria-hidden='true' ></span>",
                                    'escape' => false],
                                'class' => 'input-checkbox',  'checked' => true]); ?>
                            </td>
                        </tr>
                    </table>
                    <table style="width:100%">
                        <tr>
                            <td style="width:100%">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="store"> Crear Depósito
                                    </label>
                                </div> 
                            </td>
                        </tr>
                        <tr>
                            <td style="width:100%">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="cash"> Crear Caja
                                    </label>
                                </div> 
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="col-md-3">
                    <label for="">Privilegios Seleccionados</label>
                    <table class="table" id="table-selected">
                    </table>
                </div>

                <div class="col-md-4">
                    <?php
                        echo $this->Form->input('roles._ids', [
                            'options' => $roles,
                            'label' => [
                                'text' => "Privilegios <span class='fa fa-question-circle' title='Los privilegios son un grupo de reglas que determina las acciones que el usuario tiene permitido realizar dentro del sistema..'  aria-hidden='true' ></span>",
                                'escape' => false
                            ],
                        ]);
                        
                    ?>
                </div>

            </div>

        </fieldset>
        <?= $this->Html->link(__('Cancelar'), ["controller" => "Users", "action" => "index"], ['class' => 'btn btn-default']) ?>
        <?= $this->Form->button(__('Agregar'), ['class' => 'btn-submit btn-success']) ?>
        <?= $this->Form->end() ?>

    </div>
</div> <!-- end row -->

<script type="text/javascript">

    $(document).ready(function() {

        $('#roles-ids').change(function() {
            var tr = "";
            $( "#roles-ids option:selected" ).each(function() {
                tr += '<tr><td class="left rol-system">' + $( this ).html() + "</td></tr>";
            });
            $( "#table-selected" ).html( tr );
        }).trigger( "change" );
    });

</script>
