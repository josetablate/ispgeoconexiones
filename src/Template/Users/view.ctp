
<style type="text/css">

    legend {
        display: block;
        width: 100%;
        padding: 0;
        margin-bottom: 20px;
        font-size: 21px;
        line-height: inherit;
        color: #333;
        border: 0;
        border-bottom: 1px solid #e5e5e5;
    }

    .vertical-table {
        width: 100%;
        color: #696868;
    }

    table > tbody > tr {
        border-bottom: 1px solid #e5e5e5;
    }
 
    .green {
        font-weight: bold;
    }

    .red {
        font-weight: bold;
    }

    .rol-system {
        height: 40px;
        font-size: 16px;
        font-weight: 500;
    }
 
</style>

<div class="row">
     <div class="col-md-8">
        <div class="text-right">
          
            <a class="btn btn-default" title="Agregar nuevo usuario" href="<?= $this->Url->build(["action" => "add"]) ?>" role="button">
                <span class="glyphicon icon-plus"  aria-hidden="true"></span>
            </a>
            <a class="btn btn-default" title="Editar usuario" href="<?= $this->Url->build(["action" => "edit",  $user->id]) ?>" role="button">
                <span class="glyphicon icon-pencil2"  aria-hidden="true"></span>
            </a>
             <?= $this->Html->link(__("<span class='glyphicon icon-bin' title='Eliminar usuario' aria-hidden='true'></span>"), 
                '#',
                [
                    'class' => 'btn btn-default btn-delete', 
                    'role' => 'button',
                    'data-controller' => 'users',
                    'data-id' => $user->id,
                    'data-text' =>  __('¿Está Seguro que desea eliminar el usuario <strong>{0}</strong>?', $user->username),
                    'escape' => false
                ]) ?>
        </div>
    </div>
</div>

<div class="row">
 
    <div class="col-md-4">
        <legend class="sub-title">Usuario: <?= h($user->name) ?></legend>
        <table class="vertical-table" >
            <tr>
                <th><?= __('Nombre Completo') ?></th>
                <td class="text-right"><?= h($user->name) ?></td>
            </tr>
            <tr>
                <th><?= __('Usuario') ?></th>
                <td class="text-right"><?= h($user->username) ?></td>
            </tr>
            <tr>
                <th><?= __('Descripción') ?></th>
                <td class="text-right"><?= h($user->description) ?></td>
            </tr>
             <tr>
                <th><?= __('Rol') ?></th>
                <td class="text-right"><?= h($user->roles_names_list) ?></td>
            </tr>
           
            <tr>
                <th><?= __('Teléfono') ?></th>
                <td class="text-right"><?= h($user->phone) ?></td>
            </tr>

            <tr>
                <th><?= __('Correo') ?></th>
                <td class="text-right"><?= h($user->email ? $user->email : '') ?></td>
            </tr>

            <tr>
                <th><?= __('Id') ?></th>
                <td class="text-right"><?= $user->id ?></td>
            </tr>
            <tr>
                <th><?= __('Creado') ?></th>
                <td class="text-right"><?= date('d-m-Y H:i', strtotime($user->created)) ?></td>
            </tr>
            <tr>
                <th><?= __('Modificado') ?></th>
                <td class="text-right"><?= date('d-m-Y H:i', strtotime($user->modified)) ?></td>
            </tr>
            <tr>
                <th><?= __('Usuario Habilitado') ?></th>
                <?php if ($user->enabled): ?>
                    <td class="text-right green"><?=  __('Si') ?></td>
                <?php else:?>
                    <td class="text-right red"><?=  __('No') ?></td>
                <?php endif; ?>
            </tr>
        </table>
    </div>

    <div class="col-md-4">
        <legend class="sub-title">Privilegios</legend>
        <table class="table">

            <?php foreach ($user->roles as $role): ?>

                <tr>
                    <td class="rol-system"><?= $role->name ?></td>
                </tr>

            <?php endforeach; ?>
        </table>
    </div>
</div>

<script type="text/javascript">

    var token = <?= !empty($token) ? json_encode($token) : "[]" ?>;
    if (token == "[]") {
          token = false;
    };

    /**
     * confirmacion de eliminacion general 
     */
    $(document).on("click", ".btn-delete", function(e) {

        var text = $(this).data('text');
        var controller = $(this).data('controller');
        var id = $(this).data('id');

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/delete/', 'method': 'post', 'id': 'replacer'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#replacer').submit();
            }
        });
    });

</script>
