<?php

    $rowsCount = count($debts_lala);

    $this->PhpExcel->getActiveSheet()->setTitle('MasterCard Débito Automático');

    $this->PhpExcel->getActiveSheet()->fromArray($debts_lala, NULL, 'A1');

    $this->PhpExcel->getActiveSheet()->getStyle('A1:L1')->getFont()->setBold(true);

    $rowCount = 1;
    $customTitle = [
        'Fecha',        //0
        'Desde',        //1
        'Hasta',        //2
        'Nro Tarjeta',  //3
        'Nombre',       //4
        'Documento',    //5
        'Cód.',         //6
        'Ciudad',       //7
        'Nro Factura',  //8
        'Vencimiento',  //9
        'Total',        //10
        'Empresa'       //11
    ];

    $this->PhpExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $customTitle[0]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $customTitle[1]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $customTitle[2]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $customTitle[3]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $customTitle[4]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $customTitle[5]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $customTitle[6]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $customTitle[7]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $customTitle[8]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $customTitle[9]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $customTitle[10]);
    $this->PhpExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $customTitle[11]);

    $colsAttr = [
        'A' => [
            'setAutoSize' => true,
        ],
        'B' => [
            'setAutoSize' => true,
        ],
        'C' => [
            'setAutoSize' => true,
        ],
        'D' => [
            'setAutoSize' => true,
        ],
        'E' => [
            'setAutoSize' => true,
        ],
        'F' => [
            'setAutoSize' => true,
        ],
        'G' => [
            'setAutoSize' => true,
        ],
        'H' => [
            'setAutoSize' => true,
        ],
        'I' => [
            'setAutoSize' => true,
        ],
        'J' => [
            'setAutoSize' => true,
        ],
        'K' => [
            'setAutoSize' => true,
        ],
        'L' => [
            'setAutoSize' => true,
        ]
    ];
   $value = $this->PhpExcel->getActiveSheet()->getCell('B2')->getValue();

    foreach ($colsAttr as $col => $attr) {

        $activeSheet = $this->PhpExcel->getActiveSheet();

        //align center head columns
        $activeSheet->getStyle("$col"."1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $column = $activeSheet->getColumnDimension($col);

        $column->setAutoSize($attr['setAutoSize']);

        if (array_key_exists('setWidth', $attr)) {
            $column->setWidth($attr['setWidth']);
        }

        $alignment = $activeSheet->getStyle("$col" . "2:$col$rowsCount")->getAlignment();

        if (array_key_exists('horizontal', $attr)) {
            if ($attr['horizontal'] == 'right') {
                $alignment->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            } else if ($attr['horizontal'] == 'left') {
                $alignment->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            } else {
                $alignment->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }
        }
    }

?>
