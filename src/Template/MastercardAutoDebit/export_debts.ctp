<style type="text/css">

    legend {
        display: block;
        width: 100%;
        padding: 0;
        margin-bottom: 20px;
        font-size: 21px;
        line-height: inherit;
        color: #333;
        border: 0;
        border-bottom: 1px solid #e5e5e5;
    }

    .title {
        margin-left: 0px;
        margin-right: 0px;
        margin-bottom: 10px;
    }

    .title label.label-control {
        margin: 3px 0 3px 0;
    }

    .title label.label-control {
        margin: 3px 0 3px 0;
    }

    textarea.form-control {
        height: 80px !important;
    }

    span.red {
        color: red;
    }
    
    .lbl-check-tag {
        margin-left: 8px;
    }

    .textarea {
        margin-bottom: 0px;
    }

    #textarea_feedback {
        font-family: monospace;
        font-weight: bold;
        color: #696868;
        font-size: 15px;
    }

    #message-template {
        height: 149px !important;
    }

    .form-impuesto {
        border: 1px solid #ccc5c5;
        border-radius: 5px;
    }

</style>

<div class="row justify-content-center">
    <div class="col-3 form-impuesto">
        <?= $this->Form->create(null, ['url' => ['controller' => 'MastercardAutoDebit' , 'action' => 'exportDebts.xlsx'], 'class' => 'form-load', 'id' => 'main-form']) ?>
            <fieldset>
                <legend class="sub-title pt-2"><?=  __('Filtro para Exportar') ?></legend>

                <div class="col-md-12 col-lg-12 col-xl-12">
                    <label for="" class="label-control mt-3"><span class="red">*</span><?= __('Desde') ?></label>
                    <div class='input-group date' id='date_start-datetimepicker' required>
                        <input name='date_start'id="date-start" type='text' class="form-control" />
                        <span class="input-group-addon input-group-text calendar">
                            <span class="glyphicon icon-calendar"></span>
                        </span>
                    </div>
                </div>

                <div class="col-md-12 col-lg-12 col-xl-12">
                    <label for="" class="label-control mt-3"><span class="red">*</span><?= __('Hasta') ?></label>
                    <div class='input-group date' id='date_end-datetimepicker' required>
                        <input name='date_end'id="date-end" type='text' class="form-control" />
                        <span class="input-group-addon input-group-text calendar">
                            <span class="glyphicon icon-calendar"></span>
                        </span>
                    </div>
                </div>

                <div class="col-md-12 col-lg-12 col-xl-12 mt-3">
                    <?= $this->Form->button(__('Exportar'), ['class' => 'btn-success btn-block', 'type' => 'submit']) ?>
                </div>

            </fieldset>
        <?= $this->Form->end() ?>
    </div>
</div>

<form action="/ispbrain/MastercardAutoDebit/export_debts.xlsx" method="post" id="form-block" class="d-none">
    <input type="hidden" name="data" value="">
    <input type="hidden" name="hash" value="<?= time() ?>">
    <input type="hidden" name="_csrfToken" autocomplete="off" value="token">
</form>

<script type="text/javascript">

    $(document).ready(function() {

        $('#date_start-datetimepicker').datetimepicker({
            locale: 'es',
            defaultDate: new Date(),
            format: 'DD/MM/YYYY'
        });

        $('#date_end-datetimepicker').datetimepicker({
            locale: 'es',
            defaultDate: new Date(),
            format: 'DD/MM/YYYY'
        });

        $('#main-form').submit(function(e) {

            $('#main-form :input:visible[required="required"]').each(function()
            {
                if (!this.validity.valid)
                {
                    $(this).focus();
                    // break
                    return false;
                }
            });

            // var date_start =  $('#main-form #date-start').val().split('/');
            // date_start = date_start[2] + '-' + date_start[1] + '-' + date_start[0] + ' 00:00:00';

            // var date_end = $('#main-form #date-end').val().split('/');
            // date_end = date_end[2] + '-' + date_end[1] + '-' + date_end[0] + ' 00:00:00';

            // var data = {
            //     date_start: date_start,
            //     date_end: date_end
            // };

            // $("#form-block input[name=_csrfToken]").val(token);
            // $("#form-block input[name=data]").val(JSON.stringify(data));
            // $('#form-block').submit();
            // return false;
        });

    });

</script>
