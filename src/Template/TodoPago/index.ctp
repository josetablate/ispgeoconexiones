<style type="text/css">

    .my-hidden {
        display: none;
    }

     tr.title{
        color: #696868 !important;
        font-weight: bold;
    }

    .btn-small {
        padding: 2px;
        margin: 5px;
    }

    .contain-block {
        background-color: #ececec;
        border: 1px solid #c3c2c2;
        border-radius: 5px;
        margin-left: 15px;
        padding-top: 5px;
    }

    .lbl-legend {
        font-size: 20px;
        font-family: sans-serif;
    }

    .form-contain {
        border: 1px solid #d2cfcf;
        background: #f0f0f1;
        padding: 15px;
        border-radius: 5px;
    }

</style>

<?php
    $accountsArray = [];
    $accountsArray[''] = 'Raíz';
    foreach ($accounts as $a) {
        $accountsArray[$a->code] = $a->code . ' ' . $a->name;
    }
?>

<?= $this->Form->create($payment_getway,  ['class' => ['form-load', 'ml-3', 'form-contain']]) ?>
    <fieldset>
        <legend class="sub-title">Configuración</legend>
        <div class="row d-block">

            <?php
                $test = $payment_getway->config->todopago->MODO == 'test' ? '' : 'my-hidden';
                $prod = $payment_getway->config->todopago->MODO == 'prod' ? '' : 'my-hidden';
            ?>

            <div class="col-md-3 contain-block float-left">
                <legend class="lbl-legend">Visualización</legend>
                <?= $this->Form->input('enabled', ['label' => 'Habilitar', 'type' => 'checkbox', 'checked' => $payment_getway->config->todopago->enabled]) ?>
                <?= $this->Form->input('cash', ['label' => 'Ver Cobranza', 'type' => 'checkbox', 'checked' => $payment_getway->config->todopago->cash]) ?>
                <?= $this->Form->input('portal', ['label' => 'Ver Portal', 'type' => 'checkbox', 'checked' => $payment_getway->config->todopago->portal]) ?>
            </div>

            <div class="col-md-3 contain-block float-left">

                <div class="row">

                    <div class="col-md-12">
                        <legend class="lbl-legend">Cuenta Todo Pago</legend>
                    </div>

                    <div class="col-md-12">
                        <?= $this->Form->input('payment_getway.todopago.MODO', ['options' => ['prod' => 'Producción', 'test' => 'Prueba' ], 'value' => $payment_getway->config->todopago->MODO, 'label' => 'Modo']); ?>
                    </div>

                    <div class="col-md-12">

                        <div id="test-merchant-id-group" class="form-group text <?= $test ?>">
                            <label class="control-label" for="test-merchant-id">Nro de Comercio (Merchant ID)</label>
                            <input type="text" name="test-merchant-id" class="form-control" id="test-merchant-id" value="<?= $payment_getway->config->todopago->test->merchant_id ?>">
                        </div>

                        <div id="prod-merchant-id-group" class="form-group text <?= $prod ?>">
                            <label class="control-label" for="prod-merchant-id">Nro de Comercio (Merchant ID)</label>
                            <input type="text" name="prod-merchant-id" class="form-control" id="prod-merchant-id" value="<?= $payment_getway->config->todopago->prod->merchant_id ?>">
                        </div>

                    </div>

                    <div class="col-md-12">

                         <div id="test-api-keys-group" class="form-group text <?= $test ?>">
                            <label class="control-label" for="test-api-keys">Clave Seguridad (API Keys)</label>
                            <input type="text" name="test-api-keys" class="form-control" id="test-api-keys" value="<?= $payment_getway->config->todopago->test->api_keys ?>">
                        </div>

                        <div id="prod-api-keys-group" class="form-group text <?= $prod ?>">
                            <label class="control-label" for="prod-api-keys">Clave Seguridad (API Keys)</label>
                            <input type="text" name="prod-api-keys" class="form-control" id="prod-api-keys" value="<?= $payment_getway->config->todopago->prod->api_keys ?>">
                        </div>

                    </div>

                </div>
            </div>

            
            <div class="col-md-3 float-left">
                <div class="row">
                    <?php if ($account_enabled): ?>
                        <div class="col-md-12 contain-block">
                            <legend class="lbl-legend">Cuenta contable</legend>
                            <table class="mb-3">
                                 <tr>
                                    <td style="width:400px">
                                        <input type="hidden" name="account" id="account_code" value="<?= $payment_getway->config->todopago->account ?>" >
                                        <input type="text" id="account_code-show" class="form-control" readonly value="<?= $accountsArray[$payment_getway->config->todopago->account] ?>" placeholder="Cuenta">
                                    </td>
                                    <td> 
                                        <a class="btn btn-default btn-search-account btn-small" href="#" data-input="account_code" role="button">
                                            <span class="glyphicon icon-search" aria-hidden="true"></span>
                                        </a>
                                    </td>
                                    <td> 
                                        <a class="btn btn-default btn-bin-account btn-small" href="#" data-input="account_code" role="button">
                                            <span class="glyphicon icon-bin" aria-hidden="true"></span>
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    <?php endif; ?>

                    <div class="col-md-12 contain-block mt-3">
                        <div class="form-group required">
                            <label class="control-label" for="hours-execution">Ejecución para Consultar Pagos</label>
                            <div class='input-group date' id='hours-execution-datetimepicker'>
                                <span class="input-group-addon input-group-text mr-0">Horario</span>
                                <input  name="hours_execution" required="required" id="hours-execution"  type='text' class="form-control" />
                                <span class="input-group-addon input-group-text mr-0">
                                    <span class="glyphicon icon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <!--<?= $this->Form->input('automatic', ['label' => 'Automático', 'type' => 'checkbox', 'checked' => $payment_getway->config->todopago->automatic]); ?>-->
                    </div>
                </div>
            </div>

        </div>
    </fieldset>
    <?= $this->Html->link(__('Cancelar'), ["controller" => "PaymentMethods", "action" => "index"], ['class' => 'btn btn-default ']) ?>
    <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success' ]) ?>
<?= $this->Form->end() ?>

<div class="modal fade" id="modal-accounts" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Seleccionar Cuenta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">

                        <table class="table table-hover" id="table-accounts">
                            <thead>
                                <tr>
                                    <th>Cód.</th>
                                    <th>Nombre</th>
                                    <th>Descripción</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                foreach ($accounts as $acc): ?>
                                <tr class="<?= ($acc->title) ? 'title' : '' ?> <?= (!$acc->status) ? ' font-through' : '' ?>"
                                    data-code="<?= $acc->code ?>"
                                    data-name="<?= $acc->name ?>"
                                    >
                                    <td class="left"><?= $acc->code ?></td>
                                    <td class="left"><?php
        
                                        $hrchy = substr(strval($acc->code), 0, 4);
                                        $hrchy = str_split($hrchy);
                                        foreach ($hrchy as $h) {
                                            if ($h != '0') {
                                                echo '&nbsp;&nbsp;&nbsp;'; 
                                            }
                                        }
                                        if (!$acc->title) {
                                            echo '&nbsp;&nbsp;&nbsp;'; 
                                        }
                                        echo $acc->name; 
                                    ?>
                                    </td>
                                    <td class="left"><?= $acc->description ?></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <br>

                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" id="btn-account-selected" class="btn btn-success">Seleccionar</button> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var payment_getway;
    
    var table_acccounts = null;

    $(document).ready(function () {
        payment_getway = <?= json_encode($payment_getway) ?>;
        var year = new Date().getFullYear(); 
        var defaultDateCD = year + "-10-01T" + payment_getway.config.todopago.hours_execution;
        $('#hours-execution-datetimepicker').datetimepicker({
            locale: 'es',
            defaultDate: defaultDateCD,
            format: 'HH:mm',
        });

        $( "#payment-getway-todopago-modo" ).change(function() {
            var modo = $(this).val();
            if (modo == "test") {
                $('#prod-merchant-id-group').addClass('my-hidden');
                $('#prod-api-keys-group').addClass('my-hidden');
                $('#test-merchant-id-group').removeClass('my-hidden');
                $('#test-api-keys-group').removeClass('my-hidden');
            } else {
                $('#test-merchant-id-group').addClass('my-hidden');
                $('#test-api-keys-group').addClass('my-hidden');
                $('#prod-merchant-id-group').removeClass('my-hidden');
                $('#prod-api-keys-group').removeClass('my-hidden');
            }
        });

        $('#table-accounts').removeClass('display');

		table_acccounts = $('#table-accounts').DataTable({
		    "order": [[ 0, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '480px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
		});

		$('#table-accounts_filter').closest('div').before($('#btns-tools').contents());   

		var input_select = null;

		$('.btn-search-account').click(function() {

		    input_select = $(this).data('input');
		    $('#modal-accounts').modal('show');
		});

		$('.btn-bin-account').click(function(){

		    var input_select_temp = $(this).data('input');
		    $('#' + input_select_temp).val('');
		    $('#' + input_select_temp+'-show').val('');
		});

		$('#btn-account-selected').click(function() {

		    var code = table_acccounts.$('tr.selected').data('code');
		    var name = table_acccounts.$('tr.selected').data('name');

		    $('#' + input_select).val(code);
		    $('#' + input_select + '-show').val(code + ' ' + name);
		    $('#account_name').val(name);

		    $('#modal-accounts').modal('hide');
		});

		$('#modal-accounts').on('shown.bs.modal', function (e) {
		    table_acccounts.draw();
        });

        $('#table-accounts tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                table_acccounts.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });
    });

</script>
