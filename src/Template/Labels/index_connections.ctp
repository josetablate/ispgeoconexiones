

<style type="text/css">
    
    .modal #color-text{
        
        height: 35px;
    }
    
    .modal #color-back{
        
        height: 35px;
    }
    
     .my_label{
        font-size: 22px;
    }
    
    
</style>

<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5" >

        <?php

            echo $this->Html->link(
                '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Agregar nuevo etiqueta',
                'class' => 'btn btn-default btn-add',
                'escape' => false
                ]);

             echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export',
                'escape' => false
                ]);
        ?>

    </div>
</div>

<div class="row justify-content-center">
    <div class="col-md-8">
        <table class="table table-bordered" id="table-labels">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Texto</th>
                    <th>Activo</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-edit',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-labels', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Editar Etiqueta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <form method="post" accept-charset="utf-8" role="form" action="<?=$this->Url->build(["controller" => "Labels", "action" => "edit"])?>">
                    <div style="display:none;">
                        <input type="hidden" name="_method" value="POST">
                    </div>
                    
                    <div class="row">
                        
                        <div class="col-xl-12">
                             <?php
                                echo $this->Form->input('text', ['label' => 'Texto', 'required' => true]);
                            ?>
                        </div>
                        
                        <div class="col-xl-12">
                            <?php
                                echo $this->Form->control('color_text', ['label' => 'Color texto', 'type' => 'color' ]);
                            ?>
                        </div>
                        
                        <div class="col-xl-12">
                            <?php
                                echo $this->Form->input('entity', ['type' => 'hidden', 'value' => 1]);
                            ?>
                        </div>
                        
                        <div class="col-xl-12">
                            <?php
                                echo $this->Form->control('color_back', ['label' => 'Color fondo', 'type' => 'color']);
                            ?>
                        </div>
                        
                        <div class="col-xl-12">
                            <?php
                                echo $this->Form->control('enabled', ['label' => 'Habilitar/Deshabilitar', 'type' => 'checkbox' ]);
                            ?>
                        </div>
                     
                        <div class="col-xl-12">

                            <input type="hidden" name="id" id="id"/>
                            <?php echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]); ?>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-success float-right" id="btn-confirm-edit">Guardar</button> 
                        </div>
                   
                    </div>
                    
                </form>
           </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Agregar Etiqueta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                

                <form method="post" accept-charset="utf-8" role="form" action="<?=$this->Url->build(["controller" => "Labels", "action" => "add"])?>">
                    <div style="display:none;">
                        <input type="hidden" name="_method" value="POST">
                    </div>
                    
                    <div class="row">
                        
                        <div class="col-xl-12">
                             <?php
                                echo $this->Form->input('text', ['label' => 'Texto', 'required' => true]);
                            ?>
                        </div>
                        <div class="col-xl-12">
                            <?php
                                echo $this->Form->control('color_text', ['label' => 'Color texto', 'type' => 'color', 'value' => '#ffffff' ]);
                            ?>
                        </div>
                        
                        <div class="col-xl-12">
                            <?php
                                echo $this->Form->input('entity', ['type' => 'hidden', 'value' => 1]);
                            ?>
                        </div>
                        
                        <div class="col-xl-12">
                            <?php
                                echo $this->Form->control('color_back', ['label' => 'Color fondo', 'type' => 'color', 'value' => '#2196f3']);
                            ?>
                        </div>
                        
                        <div class="col-xl-12">
                            <?php
                                echo $this->Form->control('enabled', ['label' => 'Habilitar/Deshabilitar', 'checked' => true,  'type' => 'checkbox' ]);
                            ?>
                        </div>
                        
                        <?php echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]); ?>
                     
                        <div class="col-xl-12">
                            
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-success float-right" id="btn-confirm-edit">Guardar</button> 
                        </div>
                        
                        
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var table_labels = null;
    var label_selected = null;

    var codes = [];

    $(document).ready(function () {
     
        var selected = false;
        $('#table-labels').removeClass('display');
        
        $('#btns-tools').hide();

		table_labels = $('#table-labels').DataTable({
		    "order": [[ 1, 'asc' ]],
		    "autoWidth": true,
		    "scrollY": '350px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "sWrapper":  "dataTables_wrapper dt-bootstrap4",
            "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/Labels/index_connections.json",
                "dataSrc": "labels",
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
            				  window.location.href ="/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "columns": [
                { "data": "id" },
                { 
                    "data": "text",
                    "render": function ( data, type, row ) {
                        
                        return "<span class='my_label' style='background-color: "+row.color_back+";  color: "+row.color_text+";' > "+data+" </span>";
                    }
                
                },
                {
                    "data": "enabled",
                    "render": function ( data, type, row ) {
                        var enabled = '<i class="fa fa-times" aria-hidden="true"></i>';
                        if (data) {
                            enabled = '<i class="fa fa-check" aria-hidden="true"></i>';
                        }
                        return enabled;
                    }
                },
            ],
            "columnDefs": [
                {
                    "targets": [0,2],
                    "width": '15%'
                }, 
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
		    "language": dataTable_lenguage,
            "lengthMenu": [[50, 100, -1], [50, 100, "Todas"]],
            	"dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-4'f><'col-xl-2 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
		});
        
       
        

		$('#table-labels_wrapper .tools').append($('#btns-tools').contents());
		
		 $('#btns-tools').show();
    });

    $(".btn-export").click(function(){

        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if (result) {
                $('#table-labels').tableExport({tableName: 'Etiquetas', type:'excel', escape:'false'});
            }
        });
    });

    $('#table-labels tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_labels.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            label_selected = table_labels.row( this ).data();

            $('.modal-labels').modal('show');
        }
    });

    $('a#btn-edit').click(function() {

        $('.modal-actions').modal('hide');

        $('#modal-edit #id').val(label_selected.id);
        $('#modal-edit #text').val(label_selected.text);
        $('#modal-edit #entity').val(label_selected.entity);
        $('#modal-edit #color-back').val(label_selected.color_back);
        $('#modal-edit #color-text').val(label_selected.color_text);

        $('#modal-edit #enabled').attr('checked', label_selected.enabled);
        $('#modal-edit').modal('show');

    });

    $('#modal-edit #enabled').click(function() {

        if ($('#modal-edit #enabled').val() == '1') {
            $('#modal-edit #enabled').val(0);
        } else {
            $('#modal-edit #enabled').val(1);
        }
    });
    
    $('.btn-add').click(function(){
       
       $('#modal-add').modal('show');
        
    });
 
	$('a#btn-delete').click(function() {

        var text = "¿Está Seguro que desea eliminar la etiqueta?";
        var id  = label_selected.id;

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                    .append( $('<form/>').attr({'action': '/ispbrain/labels/delete/', 'method': 'post', 'id': 'replacer'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer').submit();
            }
        });
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
