


<div class="row">

    <div class="col-xl-3">

        <?= $this->Form->create($label,  ['class' => 'form-load']) ?>
        <fieldset>
    
        <?php
            echo $this->Form->control('text');
            echo $this->Form->control('table', ['options' => [0 => 'Clientes', 1 => 'Conexiones']]);
            echo $this->Form->control('enabled', ['label' => 'Habilitar/Deshabilitar', 'checked' => true, 'class' => 'input-checkbox' ]);
        ?>

        </fieldset>
        <?= $this->Form->button(__('Agregar')) ?>
        <?= $this->Form->end() ?>
    </div>
</div>


