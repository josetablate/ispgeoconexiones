<style type="text/css">

    .input-enabled {
        width: 100%;
    }

    .modal span{
        margin-right: 0 !important;
    }

</style>

<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5" >

        <?php

            echo $this->Html->link(
                '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                ['controller' => 'Areas', 'action' => 'add'],
                [
                'title' => 'Nueva Área',
                'class' => 'btn btn-default',
                'escape' => false
            ]);

             echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export ml-1',
                'escape' => false
            ]);
        ?>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-hover table-bordered" id="table-areas">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Ciudad</th>
                    <th>Empresa Fact.</th>
                    <th>Activo</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-edit',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-default'
    ];

    $buttons[] = [
        'id'   =>  'btn-delete',
        'name' =>  'Eliminar',
        'icon' =>  'icon-bin',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-areas', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="label-modal-edit">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Editar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div class="col-md-12">

                        <form method="post" accept-charset="utf-8" role="form" action="<?= $this->Url->build(["controller" => "Areas", "action" => "edit"]) ?>">
                            <div style="display:none;">
                                <input type="hidden" name="_method" value="POST">
                            </div>
                            <?php
                                echo $this->Form->input('name', ['label' => 'Nombre']);
                                echo $this->Form->input('description', ['label' => 'Descripción']);
                                echo $this->Form->input('city_id', ['label' => 'Ciudad', 'options' => $cities, 'class' => 'selectpicker', 'data-live-search' => "true"]);
                                echo $this->Form->input('business_billing_default', ['label' => 'Empresa Fact.', 'options' => $business]);
                                echo $this->Form->input('enabled', ['label' => 'Habilitado', 'type' => 'checkbox']);
                                echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]); 
                            ?>

                            <input type="hidden" name="id" id="id"/>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-success" id="btn-confirm-edit">Guardar</button> 

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var table_areas = null;
    var area_selected = null;
    var business = null;

    $(document).ready(function () {

        business = <?= json_encode($business) ?>;

        $('#table-areas').removeClass('display');

		table_areas = $('#table-areas').DataTable({
		    "autoWidth": true,
		    "scrollY": '350px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "sWrapper":  "dataTables_wrapper dt-bootstrap4",
            "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/areas/index.json",
                "dataSrc": "areas",
                "error": function(c) {
                    switch (c.status) {
                        case 400:
                            flag = false;
                            generateNoty('warning', 'Ha ocurrido un error.');
                            break;
                        case 401:
                            flag = false;
                            generateNoty('warning', 'Error, no posee una autorización.');
                            break;
                        case 403:
                            flag = false;
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                        	setTimeout(function() { 
                                window.location.href = "/ispbrain";
                        	}, 3000);
                            break;
                    }
                }
            },
            "columns": [
                { "data": "name" },
                { "data": "description" },
                { "data": "city.name" },
                {
                    "data": "business_billing_default",
                    "type": "options",
                    "options": business,
                    "render": function ( data, type, row ) {
                        return data ? business[data] : '';
                    }
                },
                { 
                    "data": "enabled",
                    "render": function ( data, type, row ) {
                        var enabled = '<i class="fa fa-times red" aria-hidden="true"></i>';
                        if (data) {
                            enabled = '<i class="fa fa-check green" aria-hidden="true"></i>';
                        }
                        return enabled;
                    }
                },
            ],
            "columnDefs": [
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
		    "language":  dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            "dom":
        		"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
		});

		$('#table-areas_wrapper .tools').append($('#btns-tools').contents());

        $('#btns-tools').show();
    });

    $(".btn-export").click(function(){
        bootbox.confirm('Se descargara un archivo Excel', function(result) {
            if (result) {
                $('#table-areas').tableExport({tableName: 'Áreas', type:'excel', escape:'false'});
            }
        });
    });

    $('#table-areas tbody').on( 'click', 'tr', function (e) {

        if (!$(this).find('.dataTables_empty').length) {

            table_areas.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            area_selected = table_areas.row( this ).data();

            $('.modal-areas').modal('show');
        }
    });

    $('a#btn-edit').click(function() {

        $('.modal-areas').modal('hide');

        $('#modal-edit #id').val(area_selected.id);
        $('#modal-edit #name').val(area_selected.name);
        $('#modal-edit #description').val(area_selected.description);

        $('#modal-edit #city-id').val(area_selected.city_id);
        $('#modal-edit #business-billing-default').val(area_selected.business_billing_default);
        $('#modal-edit #city-id').closest('div').find('button').attr('title', area_selected.city.name);
        $('#modal-edit #city-id').closest('div').find('.filter-option').html(area_selected.city.name);

        $('#modal-edit #enabled').attr('checked', area_selected.enabled);

        $('#modal-edit').modal('show');
    });

	$('#btn-delete').click(function() {

        var text = "¿Está Seguro que desea eliminar la Área?";
        var id = area_selected.id;

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                    .append( $('<form/>').attr({'action': '/ispbrain/Areas/delete/', 'method': 'post', 'id': 'replacer'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer').submit();
            }
        });
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
