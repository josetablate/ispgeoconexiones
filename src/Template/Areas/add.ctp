
<div class="row">
    <div class="col-md-3">
        <?= $this->Form->create($city, ['class' => 'form-load']) ?>
        <fieldset>
            <?php
                echo $this->Form->input('name', ['label' => 'Nombre']);
                echo $this->Form->input('description', ['label' => 'Descripción']);
                echo $this->Form->input('city_id', ['label' => 'Ciudad', 'options' => $cities,  'value' => $paraments->customer->city_default ? $paraments->customer->city_default : 1, 'class' => 'selectpicker', 'data-live-search' => "true"]);
                echo $this->Form->input('business_billing_default', ['label' => 'Empresa Fact.', 'options' => $business, 'value' => '']);
                echo $this->Form->input('enabled', ['label' => 'Habilitado', 'type' => 'checkbox', 'checked' => true]);
            ?>
        </fieldset>
         <?= $this->Html->link(__('Cancelar'),["controller" => "Areas", "action" => "index"], ['title' => 'Ir a la lista de Áreas', 'class' => 'btn btn-default']) ?>
        <?= $this->Form->button(__('Agregar'), ['class' => 'btn-success' ]) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
