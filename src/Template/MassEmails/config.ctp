<style type="text/css">

    legend {
        display: block;
        width: 100%;
        padding: 0;
        margin-bottom: 20px;
        font-size: 21px;
        line-height: inherit;
        color: #333;
        border: 0;
        border-bottom: 1px solid #e5e5e5;
    }

</style>

<div id="btns-tools-emails">
    <div class="pull-right btns-tools margin-bottom-5" >

        <?php

            echo $this->Html->link(
                '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                 ['controller' => 'MassEmails', 'action' => 'addEmail'],
                [
                'title' => 'Agregar nuevo Correo',
                'class' => 'btn btn-default btn-add-email',
                'escape' => false
            ]);
        ?>

    </div>
</div>

<div class="row">

    <div class="col-md-12">
        <table class="table table-bordered w-100" id="table-emails">
            <thead>
                <tr>
                    <th>ID</th>          <!--0-->
                    <th>Contacto</th>    <!--1-->
                    <th>Usuario</th>     <!--2-->
                    <th>Clave</th>       <!--3-->
                    <th>Class name</th>  <!--4-->
                    <th>Host</th>        <!--5-->
                    <th>Puerto</th>      <!--6-->
                    <th>Timeout</th>     <!--7-->
                    <th>Client</th>      <!--8-->
                    <th>TLS</th>         <!--9-->
                    <th>Copia</th>       <!--10-->
                    <th>Habilitado</th>  <!--11-->
                </tr>
            <thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <div class="col-md-12">
        <br>
        <legend>Configuración General</legend>
        <div class="row">
            <div class="col-md-2">
                <?= $this->Form->create(null, ['url' => ['controller' => 'MassEmails' , 'action' => 'config']]) ?>
                    <fieldset>
                        <label class="control-label" for="limit">Límite <a title="Límite hace referencia a la cantidad de clientes a los cuales se les enviará el correo en un envío." data-toggle="tooltip" class="btn-help-test" href="javascript:void(0)"><i class="fas fa-question-circle"></i></a></label>
                        <?= $this->Form->input('limit', ['label' => false, 'type' => 'number', 'min' => '1', 'value' => $paraments->mass_emails->limit, 'required' => TRUE]) ?>

                    </fieldset>
                    <br>
                    <?= $this->Html->link(__('Cancelar'),["action" => "indx"], ['class' => 'btn btn-default']) ?>
                    <?= $this->Form->button(__('Guardar'), ['class' => 'btn-success' ]) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-edit-email" tabindex="-1" role="dialog" aria-labelledby="label-modal-add">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Editar Cuenta de Correo</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">

                <form method="post" accept-charset="utf-8" enctype="multipart/form-data" role="form" action="<?= $this->Url->build(["controller" => "MassEmails", "action" => "editEmail"]) ?>">
                    <div style="display:none;">
                        <input type="hidden" name="_method" value="POST">
                    </div>

                    <div class="row">

                        <div class="col-md-4">
                            <?php

                                $required = FALSE;
                                $class = "d-none";

                                $user = "";
                                $class_name = "";
                                $pass = "";
                                $host = "";
                                $port = "";
                                $timeout = "";
                                $client = "";
                                $tls = "";

                                if ($paraments->emails_accounts->provider == "") {

                                    $required = TRUE;
                                    $class = "";

                                    $user = "Usuario";
                                    $class_name = "Class Name";
                                    $pass = "Clave";
                                    $host = "Host";
                                    $port = "Puerto";
                                    $timeout = "Timeout";
                                    $client = "Client";
                                    $tls = "TLS";
                                }
                            ?>
                            <?= $this->Form->input('contact', ['label' => 'Contacto', 'type' => 'email', 'required' => TRUE]) ?>
                            <?= $this->Form->input('copy', ['label' => 'Copia', 'type' => 'email']) ?>
                            <?= $this->Form->input('user', ['label' => $user, 'type' => 'text', 'required' => $required, 'class' => [$class]]) ?>
                            <?= $this->Form->input('class_name', ['label' => $class_name, 'type' => 'text', 'required' => $required, 'class' => [$class]]) ?>
                        </div>

                        <div class="col-md-4">
                            <?= $this->Form->input('pass', ['label' => $pass, 'type' => 'password', 'required' => $required, 'class' => [$class]]) ?>
                            <?= $this->Form->input('host', ['label' => $host, 'type' => 'text', 'required' => $required, 'class' => [$class]]) ?>
                            <?= $this->Form->input('port', ['label' => $port, 'type' => 'number', 'required' => $required, 'class' => [$class]]) ?>
                        </div>

                        <div class="col-md-4">
                            <?= $this->Form->input('timeout', ['label' => $timeout, 'type' => 'number', 'required' => $required, 'class' => [$class]]) ?>
                            <?= $this->Form->input('client', ['label' => $client, 'type' => 'text', 'class' => [$class]]) ?>
                            <?= $this->Form->input('tls', ['label' => $tls, 'type' => 'text', 'class' => [$class]]) ?>
                        </div>

                        <div class="col-md-4">
                            <?= $this->Form->input('enabled', ['label' => 'Habilitar', 'type' => 'checkbox']) ?>
                        </div>

                        <?php echo $this->Form->input('_csrfToken', ['type' => 'hidden', 'value' => $this->request->getParam('_csrfToken')]); ?>

                        <div class="col-xl-12 mt-1">
                            <input type="hidden" name="id" id="id"/>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-success" id="btn-confirm-edit">Guardar</button> 
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-edit-email',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2 mr-2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-disable-email',
        'name' =>  'Deshabilitar',
        'icon' =>  'fas fa-times',
        'type' =>  'btn-danger'
    ];

    echo $this->element('actions', ['modal'=> 'modal-email', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<script type="text/javascript">

    var parament;
    var table_emails = null;
    var email_selected = null;
    var no_check;

    $(document).ready(function () {

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

        paraments = <?= json_encode($paraments) ?>;

        no_check = [];

        if (sessionPHP.paraments.emails_accounts.provider != "") {
            no_check = [2, 3, 4, 5, 6, 7, 8, 9];
        }

        table_emails = $('#table-emails').DataTable({
		    "order": [[ 1, 'desc' ]],
		    "autoWidth": true,
		    "scrollY": '250px',
		    "scrollCollapse": true,
		    "scrollX": true,
		    "sWrapper":  "dataTables_wrapper dt-bootstrap4",
            "deferRender": true,
            "searching": false,
            "paging": false,
            "info": false,
            "ordering": false,
		    "ajax": {
                "url": "/ispbrain/MassEmails/get_emails.json",
                "dataSrc": "emails",
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "columns": [
                { 
                    "data": "id",
                },
                
                { 
                    "data": "contact",
                },
                { 
                    "data": "user",
                },
                { 
                    "data": "pass",
                    "render": function ( data, type, row ) {
                        return "";
                    }
                },
                { 
                    "data": "class_name",
                },
                { 
                    "data": "host",
                },
                { 
                    "data": "port",
                },
                { 
                    "data": "timeout",
                },
                { 
                    "data": "client",
                },
                { 
                    "data": "tls",
                },
                { 
                    "data": "copy",
                    "render": function ( data, type, row ) {
                        var copia = "";
                        if (data) {
                            copia = data;
                        }
                        return copia;
                    }
                },
                { 
                    "data": "enabled",
                    "render": function ( data, type, row ) {
                        var enabled = '<i class="fa fa-times red" aria-hidden="true"></i>';
                        if (data) {
                            enabled = '<i class="fa fa-check green" aria-hidden="true"></i>';
                        }
                        return enabled;
                    }
                },
            ],
            "columnDefs": [
                { 
                    "visible": false, targets: no_check
                },
            ],
            "createdRow" : function( row, data, index ) {
                row.contact = data.contact;
            },
		    "language": dataTable_lenguage,
            "lengthMenu": [[50, 100, -1], [50, 100, "Todas"]],
            "dom":
    	    	"<'row'<'col-xl-6 title'><'col-xl-6 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
		});

		$('#table-emails_wrapper .title').append('<h5>Cuentas</h5>');

		$('#table-emails_wrapper .tools').append($('#btns-tools-emails').contents());

		$('#btn-edit-email').click(function() {

            $('#modal-edit-email #id').val(email_selected.id);
            $('#modal-edit-email #contact').val(email_selected.contact);
            $('#modal-edit-email #copy').val(email_selected.copy);
            $('#modal-edit-email #user').val(email_selected.user);
            $('#modal-edit-email #class-name').val(email_selected.class_name);
            $('#modal-edit-email #pass').val(email_selected.pass);
            $('#modal-edit-email #host').val(email_selected.host);
            $('#modal-edit-email #port').val(email_selected.port);
            $('#modal-edit-email #timeout').val(email_selected.timeout);
            $('#modal-edit-email #client').val(email_selected.client);
            $('#modal-edit-email #tls').val(email_selected.tls);
            $('#modal-edit-email #enabled').attr('checked', email_selected.enabled);

            $('.modal-email').modal('hide');
            $('#modal-edit-email').modal('show');
        });

        $('#table-emails tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                table_emails.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
                email_selected = table_emails.row( this ).data();

                $('.modal-email').modal('show');
            }
        });

        $('#btn-disable-email').click(function() {

            if (email_selected) {

                var text = '¿Está Seguro que desea deshabilitar la cuenta de correo: ' + email_selected.contact + '?';
                bootbox.confirm(text, function(result) {
                    if (result) {
                        $('body')
                            .append( $('<form/>').attr({'action': '/ispbrain/MassEmails/disableEmail/', 'method': 'post', 'id': 'replacer'})
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': email_selected.id}))
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                            .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                            .find('#replacer').submit();
                    }
                });
            }

        });
    });

</script>
