<style type="text/css">

    legend {
        display: block;
        width: 100%;
        padding: 0;
        margin-bottom: 20px;
        font-size: 21px;
        line-height: inherit;
        color: #333;
        border: 0;
        border-bottom: 1px solid #e5e5e5;
    }

    .title {
        margin-left: 0px;
        margin-right: 0px;
        margin-bottom: 10px;
    }

    .title label.label-control {
        margin: 3px 0 3px 0;
    }

    .title label.label-control {
        margin: 3px 0 3px 0;
    }

    textarea.form-control {
        height: 80px !important;
    }

    span.red {
        color: red;
    }
    
    .lbl-check-tag {
        margin-left: 8px;
    }

    .textarea {
        margin-bottom: 0px;
    }

    #textarea_feedback {
        font-family: monospace;
        font-weight: bold;
        color: #696868;
        font-size: 15px;
    }

    #message-template {
        height: 149px !important;
    }

</style>

<div class="row">
   
    <div class="col-md-8">
        <?= $this->Form->create($template, ['id' => 'template-form']) ?>
        <fieldset>

            <div class="row">
                <div class="col-md-4">
                    <label for="name" class="label-control"><span class="red">*</span><?= __('Nombre') ?></label>
                    <?php 
                        echo $this->Form->text('name', ['required' => true] );
                    ?>
                    <label for="name" class="label-control"><span class="red">*</span><?= __('Asunto') ?></label>
                    <?php 
                        echo $this->Form->text('subject', ['required' => true] );
                    ?>
                    <?php 
                        echo $this->Form->input('description', ['type' => 'textarea', 'rows' => 2, 'label' => __('Descripción')]);
                    ?>
                    <label for="name" class="label-control mt-3"><span class="red">*</span><?= __('Empresa') ?></label>
                    <?php 
                        echo $this->Form->input('business_billing', ['options' => $business,  'label' =>  '', 'value' => '', 'required' => true]);
                    ?>
                    <label for="name" class="label-control mt-3"><span class="red">*</span><?= __('Cuenta de Correo') ?></label>
                    <?php 
                        echo $this->Form->input('email_id', ['options' => $emails,  'label' =>  '', 'value' => '', 'required' => true]);
                    ?>
                    <?php 
                        echo $this->Form->input('invoice_attach', ['label' => 'Adjuntar Factura', 'type' => 'checkbox']);
                    ?>
                    <?php 
                        echo $this->Form->input('receipt_attach', ['label' => 'Adjuntar Recibo', 'type' => 'checkbox']);
                    ?>
                    <?php 
                        echo $this->Form->input('account_summary_attach', ['label' => 'Adjuntar Resumen de Cuenta', 'type' => 'checkbox']);
                    ?>
                    <button type="submit" class="btn btn-success mt-3"><?= __('Agregar') ?></button>
                </div>
                <div class="col-md-8">
                    <?php
                        echo $this->Form->input('message', ['id' => 'message-template', 'label' => 'Mensaje', 'type' => 'textarea', 'rows' => 10, 'cols' => 80, 'required' => true]);
                    ?>
                    <div class="float-right" id="textarea_feedback"></div>
                </div>
            </div>

        </fieldset>
        <?= $this->Form->end() ?>
    </div>
    <div class="col-md-4">
        <div class="row">
            <div class="col-md-12">
                <legend class="lead sub-title"><?= __('Marcas para poner los') ?> <span class="text-info"><?= __('Valores') ?></span> <a class="btn-help-sms" href="javascript:void(0)"><i class="fas fa-question-circle"></i></a></legend>
                <ul class="list-unstyled" style="line-height: 2">
                    <li>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="customer-name">
                            <label class="form-check-label lbl-check-tag" for="customer-name">%cliente_nombre%</label>
                        </div>
                    </li>
                    <li>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="customer-code">
                            <label class="form-check-label lbl-check-tag" for="customer-code">%cliente_codigo%</label>
                        </div>
                    </li>
                    <li>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="customer-doc">
                            <label class="form-check-label lbl-check-tag" for="customer-doc">%cliente_documento%</label>
                        </div>
                    </li>
                    <li>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="customer-portal-user">
                            <label class="form-check-label lbl-check-tag" for="customer-portal-user">%cliente_usuario_portal%</label>
                        </div>
                    </li>
                    <li>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="customer-portal-pass">
                            <label class="form-check-label lbl-check-tag" for="customer-portal-pass">%cliente_clave_portal%</label>
                        </div>
                    </li>
                    <li>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="customer-balance">
                            <label class="form-check-label lbl-check-tag" for="customer-balance">%cliente_saldo_total%</label>
                        </div>
                    </li>
                    <li>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="customer-month-balance">
                            <label class="form-check-label lbl-check-tag" for="customer-month-balance">%cliente_saldo_mes%</label>
                        </div>
                    </li>
                    <li>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="enterprise-name">
                            <label class="form-check-label lbl-check-tag" for="enterprise-name">%empresa_nombre%</label>
                        </div>
                    </li>
                    <li>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="enterprise-address">
                            <label class="form-check-label lbl-check-tag" for="enterprise-address">%empresa_domicilio%</label>
                        </div>
                    </li>
                    <li>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="enterprise-web">
                            <label class="form-check-label lbl-check-tag" for="enterprise-web">%empresa_web%</label>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-md-12">
                <legend class="lead sub-title"><?= __('Prueba') ?> <a title="Arma la plantilla, luego puedes ver como luce ingresando tu correo y presiona enviar." data-toggle="tooltip" class="btn-help-test" href="javascript:void(0)"><i class="fas fa-question-circle"></i></a></legend>
                <form class="test-email-form" id="email-form">
                    <fieldset>
                        <label for="name" class="label-control"><?= __('Correo') ?></label>
                        <div class="input-group">
                           <input id="test-email" type="email" class="form-control" required>
                           <button id="btn-send-email" type="submit" class="btn btn-success" type="button"><?= __('Enviar') ?></button>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade help-sms-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabel">Consejos</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="list-unstyled" style="line-height: 2">
                            <li><span class="fa fa-check text-success"></span> <?= __('Para que en el mensaje aparezca el nombre del cliente se debe tildar: <strong>%cliente_nombre%</strong> dicha marca luego sera reemplazada por el nombre del cliente y así con cualquiera de las marcas que desee que aparezcan en el mensaje.') ?></li>
                        </ul>
                    	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    var editor  = null;

    $(document).ready(function() {

        $('[data-toggle="tooltip"]').tooltip();

        editor = CKEDITOR.replace( 'message-template' );

        CKEDITOR.config.height = 350;
        CKEDITOR.config.width = 'auto';
        CKEDITOR.config.title = false;
        CKEDITOR.config.toolbar = [
            { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Preview', 'Print' ] },
        	{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Undo', 'Redo' ] },
        	{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ] },
        	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent' ] },
        	{ name: 'insert', items: [ 'Image' ] },
        	{ name: 'styles', items: [ 'Format' ] },
        ];

        editor.on('contentDom', function() {
            var editable = editor.editable();
            editable.attachListener(editor.document, 'keyup', ck_jqx);
        });

        $('.form-check-input').click(function() {

            var id = $(this).attr('id');

            switch (id) {
                case 'customer-name':
                    if ($(this).is(':checked')) {
                        var text = $(this).next('label').text();
                        CKEDITOR.instances['message-template'].insertText(text);
                    } else {
                        var text = CKEDITOR.instances['message-template'].getData().replace($(this).next('label').text(), '');
                        CKEDITOR.instances['message-template'].setData(text);
                    }
                    break;

                case 'customer-code':
                    if ($(this).is(':checked')) {
                        var text = $(this).next('label').text();
                        CKEDITOR.instances['message-template'].insertText(text);
                    } else {
                        var text = CKEDITOR.instances['message-template'].getData().replace($(this).next('label').text(), '');
                        CKEDITOR.instances['message-template'].setData(text);
                    }
                    break;

                case 'customer-doc':
                    if ($(this).is(':checked')) {
                        var text = $(this).next('label').text();
                        CKEDITOR.instances['message-template'].insertText(text);
                    } else {
                        var text = CKEDITOR.instances['message-template'].getData().replace($(this).next('label').text(), '');
                        CKEDITOR.instances['message-template'].setData(text);
                    }
                    break;

                case 'customer-portal-user':
                    if ($(this).is(':checked')) {
                        var text = $(this).next('label').text();
                        CKEDITOR.instances['message-template'].insertText(text);
                    } else {
                        var text = CKEDITOR.instances['message-template'].getData().replace($(this).next('label').text(), '');
                        CKEDITOR.instances['message-template'].setData(text);
                    }
                    break;

                case 'customer-portal-pass':
                    if ($(this).is(':checked')) {
                        var text = $(this).next('label').text();
                        CKEDITOR.instances['message-template'].insertText(text);
                    } else {
                        var text = CKEDITOR.instances['message-template'].getData().replace($(this).next('label').text(), '');
                        CKEDITOR.instances['message-template'].setData(text);
                    }
                    break;

                case 'customer-balance':
                    if ($(this).is(':checked')) {
                        var text = $(this).next('label').text();
                        CKEDITOR.instances['message-template'].insertText(text);
                    } else {
                        var text = CKEDITOR.instances['message-template'].getData().replace($(this).next('label').text(), '');
                        CKEDITOR.instances['message-template'].setData(text);
                    }
                    break;

                case 'customer-month-balance':
                    if ($(this).is(':checked')) {
                        var text = $(this).next('label').text();
                        CKEDITOR.instances['message-template'].insertText(text);
                    } else {
                        var text = CKEDITOR.instances['message-template'].getData().replace($(this).next('label').text(), '');
                        CKEDITOR.instances['message-template'].setData(text);
                    }
                    break;

                case 'enterprise-name':
                    if ($(this).is(':checked')) {
                        var text = $(this).next('label').text();
                        CKEDITOR.instances['message-template'].insertText(text);
                    } else {
                        var text = CKEDITOR.instances['message-template'].getData().replace($(this).next('label').text(), '');
                        CKEDITOR.instances['message-template'].setData(text);
                    }
                    break;

                case 'enterprise-address':
                    if ($(this).is(':checked')) {
                        var text = $(this).next('label').text();
                        CKEDITOR.instances['message-template'].insertText(text);
                    } else {
                        var text = CKEDITOR.instances['message-template'].getData().replace($(this).next('label').text(), '');
                        CKEDITOR.instances['message-template'].setData(text);
                    }
                    break;

                case 'enterprise-web':
                    if ($(this).is(':checked')) {
                        var text = $(this).next('label').text();
                        CKEDITOR.instances['message-template'].insertText(text);
                    } else {
                        var text = CKEDITOR.instances['message-template'].getData().replace($(this).next('label').text(), '');
                        CKEDITOR.instances['message-template'].setData(text);
                    }
                    break;
            }
        });

        $('#template-form').submit(function(e) {
            var currentForm = this;

            if (!CKEDITOR.instances['message-template'].getData().replace(/<[^>]*>/gi, '').length) {
                e.preventDefault();
                generateNoty('warning' , 'Debe ingresar un mensaje.');
            } else {
                currentForm.submit();
            }
        });

        $('.btn-help-sms').click(function() {
            $('.help-sms-modal').modal('show');
        });
        
        $('#email-form').submit(function(e) {
            e.preventDefault();
            var test_email       = $('#test-email').val();
            var subject          = $("input[name=subject]").val();
            var business_billing = $( "#business-billing option:selected" ).val();
            var email_id         = $( "#email-id option:selected" ).val();

            if (editor.getData().length == 0) {
                generateNoty('warning', 'La Plantilla no tiene contenido.');
            } else {

                if (subject.length == 0) {
                    generateNoty('warning', 'Debe ingresar un Asunto.');
                } else {

                    if (business_billing == "") {
                        generateNoty('warning', 'Debe seleccionar la empresa.');
                    } else {
                        if (email_id == "") {
                            generateNoty('warning', 'Debe seleccionar una Cuenta de Correo.');
                        } else {
                            $.ajax({
                                url: "<?= $this->Url->build(['controller' => 'MassEmails', 'action' => 'sendEmailTest']) ?>",
                                type: 'POST',
                                dataType: "json",
                                data :  JSON.stringify({
                                    subject         : subject,
                                    business_billing: business_billing,
                                    email_id        : email_id,
                                    email           : test_email,
                                    html            : editor.getData()
                                }),
                                success: function(data) {
                                    generateNoty(data.type, data.message);
                                },
                                error: function (jqXHR) {
                                    if (jqXHR.status == 403) {

                                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                                    	setTimeout(function() { 
                                            window.location.href = "/ispbrain";
                                    	}, 3000);

                                    } else {
                                    	generateNoty('error', 'Error al enviar la Plantilla al Correo.');
                                    }
                                }
                            });
                        }
                    }
                }
            }
        });

    });

    function ck_jqx() {
        var texto = CKEDITOR.instances['message-template'].getData();

        if (texto.indexOf("%cliente_nombre%") > -1) {
            if (!$('#customer-name').is(':checked')) {
                $('#customer-name').prop('checked', true);
            }
        }

        if ($('#customer-name').is(':checked') && texto.indexOf("%cliente_nombre%") == -1) {
            $('#customer-name').prop('checked', false);
        }

        if (texto.indexOf("%cliente_codigo%") > -1) {
            if (!$('#customer-code').is(':checked')) {
                $('#customer-code').prop('checked', true);
            }
        }

        if ($('#customer-code').is(':checked') && texto.indexOf("%cliente_codigo%") == -1) {
            $('#customer-code').prop('checked', false);
        }

        if (texto.indexOf("%cliente_documento%") > -1) {
            if (!$('#customer-doc').is(':checked')) {
                $('#customer-doc').prop('checked', true);
            }
        }

        if ($('#customer-doc').is(':checked') && texto.indexOf("%cliente_documento%") == -1) {
            $('#customer-doc').prop('checked', false);
        }

        if (texto.indexOf("%cliente_usuario_portal%") > -1) {
            if (!$('#customer-portal-user').is(':checked')) {
                $('#customer-portal-user').prop('checked', true);
            }
        }

        if ($('#customer-portal-user').is(':checked') && texto.indexOf("%cliente_usuario_portal%") == -1) {
            $('#customer-portal-user').prop('checked', false);
        }

        if (texto.indexOf("%cliente_clave_portal%") > -1) {
            if (!$('#customer-portal-pass').is(':checked')) {
                $('#customer-portal-pass').prop('checked', true);
            }
        }

        if ($('#customer-portal-pass').is(':checked') && texto.indexOf("%cliente_clave_portal%") == -1) {
            $('#customer-portal-pass').prop('checked', false);
        }

        if (texto.indexOf("%cliente_saldo_total%") > -1) {
            if (!$('#customer-balance').is(':checked')) {
                $('#customer-balance').prop('checked', true);
            }
        }

        if ($('#customer-balance').is(':checked') && texto.indexOf("%cliente_saldo_total%") == -1) {
            $('#customer-balance').prop('checked', false);
        }

        if (texto.indexOf("%cliente_saldo_mes%") > -1) {
            if (!$('#customer-month-balance').is(':checked')) {
                $('#customer-month-balance').prop('checked', true);
            }
        }

        if ($('#customer-month-balance').is(':checked') && texto.indexOf("%cliente_saldo_mes%") == -1) {
            $('#customer-month-balance').prop('checked', false);
        }

        if (texto.indexOf("%empresa_nombre%") > -1) {
            if (!$('#enterprise-name').is(':checked')) {
                $('#enterprise-name').prop('checked', true);
            }
        }

        if ($('#enterprise-name').is(':checked') && texto.indexOf("%empresa_nombre%") == -1) {
            $('#enterprise-name').prop('checked', false);
        }

        if (texto.indexOf("%empresa_domicilio%") > -1) {
            if (!$('#enterprise-address').is(':checked')) {
                $('#enterprise-address').prop('checked', true);
            }
        }

        if ($('#enterprise-address').is(':checked') && texto.indexOf("%empresa_domicilio%") == -1) {
            $('#enterprise-address').prop('checked', false);
        }

        if (texto.indexOf("%empresa_web%") > -1) {
            if (!$('#enterprise-web').is(':checked')) {
                $('#enterprise-web').prop('checked', true);
            }
        }

        if ($('#enterprise-web').is(':checked') && texto.indexOf("%empresa_web%") == -1) {
            $('#enterprise-web').prop('checked', false);
        }
    };

</script>
