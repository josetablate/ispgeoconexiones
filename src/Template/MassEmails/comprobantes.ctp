<style type="text/css">

    .my-hidden {
        display: none;
    }

    tr.anulated {
        background-color: #aa1220;
        color: white;
    }

</style>

<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5">

        <?php

            echo  $this->Html->link(
                '<span class="glyphicon icon-checkbox-checked" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Seleccionar todos',
                'class' => 'btn btn-default btn-selected-all ml-2',
                'escape' => false
            ]);

            echo  $this->Html->link(
                '<span class="glyphicon icon-checkbox-unchecked" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Limpiar Selección',
                'class' => 'btn btn-default btn-clear-selected-all ml-2',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="fa fa-bullhorn" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Enviar',
                'class' => 'btn btn-success btn-send ml-2',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-eye" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Ocultar o Mostrar Columnas',
                'class' => 'btn btn-default btn-hide-column ml-2',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon fas fa-search" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Buscar por Columnas',
                'class' => 'btn btn-default btn-search-column ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar en formato excel el contenido de la Tabla',
                'class' => 'btn btn-default btn-export ml-2',
                'escape' => false
            ]);
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-hover" id="table-comprobantes" >
            <thead>
                <tr>
                    <th></th>                            <!--0-->
                    <th><?= ('Fecha') ?></th>            <!--1-->
                    <th><?= ('Desde') ?></th>            <!--2-->
                    <th><?= ('Hasta') ?></th>            <!--3-->
                    <th><?= ('Comprobante') ?></th>      <!--4-->
                    <th><?= ('Pto Vta') ?></th>          <!--5-->
                    <th><?= ('Número') ?></th>           <!--6-->
                    <th><?= ('Comentario') ?></th>       <!--7-->
                    <th><?= ('Venc.') ?></th>            <!--8-->
                    <th><?= ('Total') ?></th>            <!--9-->
                    <th><?= ('Usuario') ?></th>          <!--10-->
                    <th><?= ('Cód.') ?></th>             <!--11-->
                    <th><?= ('Tipo') ?></th>             <!--12-->
                    <th><?= ('Nro') ?></th>              <!--13-->
                    <th><?= ('Cliente') ?></th>          <!--14-->
                    <th><?= ('Correo Electr.') ?></th>   <!--15-->
                    <th><?= ('Dom.') ?></th>             <!--16-->
                    <th><?= ('Ciudad') ?></th>           <!--17-->
                    <th><?= ('Método Pago') ?></th>      <!--18-->
                    <th><?= ('Empresa') ?></th>          <!--19-->
                    <th><?= ('Anulado') ?></th>          <!--20-->
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade confirm-send-modal" id="modal-send-email" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridSystemModalLabel">Confirmar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-xl-12">
                        <?= $this->Form->create(NULL, ['id' => 'form-send-email']) ?>
                            <fieldset>

                                <div class="row">
                                    <div class="col-12">
                                        <label class="control-label mt-1" for="mass-emails-template-id" style="margin-bottom: 0;">Plantilla</label>
                                        <?php 
                                            echo $this->Form->input('template_id', ['class' => '', 'options' => $templates,  'label' =>  '', 'default' => '' , 'required' => TRUE]);
                                        ?>
                                    </div>
                                </div>

                            </fieldset>
                        <?= $this->Form->button(__('Enviar'), ['class' => 'btn-primary mt-1 ml-3 btn btn-confirm-send' ]) ?>
                        <?= $this->Form->end() ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-go-customer',
        'name' =>  'Ficha del Cliente',
        'icon' =>  'glyphicon icon-profile',
        'type' =>  'btn-primary'
    ];

    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-customers-mass-emails']);
    echo $this->element('actions', ['modal'=> 'modal-comprobantes', 'title' => 'Acciones', 'buttons' => $buttons ]);
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-customers-mass-emails']);
?>

<script type="text/javascript">

    var table_comprobantes = null;
    var comprobante_selected = null;
    var users = null;
    var templates = null;
    var ids_selected = [];
    var paymentMethods = null;
    var hash = null;

    $(document).ready(function () {

        users = <?= json_encode($users) ?>;
        templates = <?= json_encode($templates) ?>;
        paymentMethods = <?= json_encode($payment_methods) ?>;
        hash = <?= time() ?>;

        $('.btn-hide-column').click(function() {
            $('.modal-hide-columns-customers-mass-emails').modal('show');
        });

        $('.btn-search-column').click(function() {
           $('.modal-search-columns-customers-mass-emails').modal('show');
        });

        $('#table-comprobantes').removeClass('display');

        $('#btns-tools').hide();

		table_comprobantes = $('#table-comprobantes').DataTable({
		    "order": [[ 1, 'desc' ]],
            "deferRender": true,
            "processing": true,
            "serverSide": true,
		    "ajax": {
                "url": "/ispbrain/MassEmails/getComprobantes.json",
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "drawCallback": function( settings ) {
                var flag = true;
            	switch (settings.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
            	    if (table_comprobantes) {

                        var tableinfo = table_comprobantes.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }
                    }
            	}
            },
		    "scrollY": true,
		    "scrollY": '450px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
		    "columns": [
		        {
                    "className": 'checkbox-control',
                    "orderable": false,
                    "data": 'id',
                    "render": function ( data, type, full, meta ) {
                        return '<label class="custom-control custom-checkbox pl-3 pr-3 m-0 align-middle"><input type="checkbox" class="custom-control-input checkbox-select p-0 m-0 " id="row-checkbox-' + data + '"><span class="custom-control-indicator w-100 align-middle"></span></label>';
                    },
                    "width": '3%'
                },
                {
                    "className": "row-control",
                    "data": "date",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            var hours = data.split('T')[1].split('-')[0].split(':');
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0] + ' ' + hours[0] + ':' + hours[1];
                        }
                        return "";
                    }
                },
                {
                    "className": "row-control",
                    "data": "date_start",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            var hours = data.split('T')[1].split('-')[0].split(':');
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0] + ' ' + hours[0] + ':' + hours[1];
                        }
                        return "";
                    }
                },
                {
                    "className": "row-control",
                    "data": "date_end",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            var hours = data.split('T')[1].split('-')[0].split(':');
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0] + ' ' + hours[0] + ':' + hours[1];
                        }
                        return "";
                    }
                },
		        {
                    "className": "row-control",
                    "data": "tipo_comp",
                    "type": "options",
                    "options": {
                        "XXX": "PRESU X",
                        "001": "FACTURA A",
                        "006": "FACTURA B",
                        "011": "FACTURA C",
                        "NCX": "NOTA DE CRÉDITO X",
                        "003": "NOTA DE CRÉDITO A",
                        "008": "NOTA DE CRÉDITO B",
                        "013": "NOTA DE CRÉDITO C",
                        "NDX": "NOTA DE DÉBITO X",
                        "002": "NOTA DE DÉBITO A",
                        "007": "NOTA DE DÉBITO B",
                        "012": "NOTA DE DÉBITO C",
                        "XRX": "RX",
                        "FRX": "FRX",
                        "000": "PRESUPUESTO"
                    },
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.comprobantes[data];
                    }
                },
                {
                    "className": "row-control",
                    "data": "pto_vta",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return  pad(data, 4);
                    }
                },
                {
                    "className": "row-control",
                    "data": "num",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 8);
                    }
                },
                { 
                    "className": "row-control left",
                    "data": "comment",
                    "type": "string"
                },
                { 
                    "className": "row-control",
                    "data": "duedate",
                    "type": "date",
                    "render": function ( data, type, row ) {

                        var duedate = "";

                        if (data) {
                            duedate = data.split('T')[0];
                            duedate = duedate.split('-');

                            if (type == 'display') {

                                if (row.paid == null) {

                                    var date = new Date(duedate[1] + '/' + duedate[2] + '/' + duedate[0]);
                                    var todayDate = new Date();

                                    duedate =  duedate[2] + '/' + duedate[1] + '/' + duedate[0];

                                    if (date < todayDate) {
                                        return "<span class='text-danger'>" + duedate + "</span>";
                                    } else {
                                        return "<span class='text-info'>" + duedate + "</span>";
                                    }
                                }
                            }

                            duedate =  duedate[2] + '/' + duedate[1] + '/' + duedate[0];
                        }

                        return duedate;
                    }
                },
                { 
                    "className": "row-control right text-info",
                    "data": "total",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' )
                },
                { 
                    "className": "row-control left",
                    "data": "user_name",
                    "options": users,
                    "type": "options"
                },
                { 
                    "className": "row-control",
                    "data": "customer_code",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                {
                    "className": "row-control center",
                    "data": "customer_doc_type",
                    "type": "options",
                    "options": sessionPHP.afip_codes.doc_types,
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.doc_types[data];
                    }
                },
                {
                    "className": "row-control center",
                    "data": "customer_ident",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (data) {
                            ident = data;
                        }
                        return ident;
                    }
                },
                { 
                    "className": "row-control left",
                    "data": "customer_name",
                    "type": "string"
                },
                {
                    "className": "row-control",
                    "data": "customer_email",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        var email = "";
                        if (data) {
                            email = data;
                        }
                        return email;
                    }
                },
                { 
                    "className": "row-control left",
                    "data": "customer_address",
                    "type": "string"
                },
                {
                    "className": "row-control left",
                    "data": "customer_city",
                    "type": "string"
                },
                {
                    "className": "row-control",
                    "data": "payment_method",
                    "type": "options",
                    "options": paymentMethods,
                },
                {
                    "className": "row-control",
                    "data": "business_id",
                    "type": "options_obj",
                    "options": sessionPHP.paraments.invoicing.business,
                    "render": function ( data, type, row ) {
                        var business_name = '';
                        $.each(sessionPHP.paraments.invoicing.business, function (i, b) {
                            if (b.id == data) {
                                business_name = b.name + ' (' + b.address + ')';
                            }
                        });
                        return business_name;
                    }
                },
                {
                    "className": "row-control",
                    "data": "anulated",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            var hours = data.split('T')[1].split('-')[0].split(':');
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0] + ' ' + hours[0] + ':' + hours[1];
                        }
                        return "";
                    }
                },
            ],
		    "columnDefs": [
		        { "type": 'date-custom', targets: [5] },
		        { "type": 'numeric-comma', targets: [] },
                {
                    "targets": [],
                    "width": '8%'
                },
                { 
                    "class": "left", targets: []
                },
                { 
                    "visible": false, targets: hide_columns
                },
            ],
             "createdRow" : function( row, data, index ) {
                row.id = data.id + '-' + data.controller;
                $.each(ids_selected, function( index, value ) {
                    if (row.id == value) {
                        $(row).addClass('selected');
                        $(row).find('input:checkbox').attr('checked', 'checked')
                    }
                });
                if (data.anulated) {
                    $(row).addClass('anulated');
                }
            },
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
        	"dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
		});

		$('#table-comprobantes_wrapper .tools').append($('#btns-tools').contents());

		$('#btns-tools').show();
		
		$('#table-comprobantes').on( 'init.dt', function () {

		    createModalHideColumn(table_comprobantes, '.modal-hide-columns-customers-mass-emails');
		    createModalSearchColumn(table_comprobantes, '.modal-search-columns-customers-mass-emails');

		    $( "#table-comprobantes tbody" ).on( "click", ".checkbox-select", function() {

                if ($(this).is(':checked')) {
                    $(this).closest('tr').addClass('selected');
                    var comprobante = table_comprobantes.row( $(this).closest('tr') ).data().id + '-' + table_comprobantes.row( $(this).closest('tr') ).data().controller;
                    ids_selected.push(comprobante);
                } else {
                    $(this).closest('tr').removeClass('selected');
                    var comprobante = table_comprobantes.row( $(this).closest('tr') ).data().id + '-' + table_comprobantes.row( $(this).closest('tr') ).data().controller;
                    ids_selected.splice($.inArray(comprobante, ids_selected), 1);
                }
            });

            $("#table-comprobantes tbody").on( "click", ".row-control", function() {

                if (!$(this).find('.dataTables_empty').length) {
                    comprobante_selected = table_comprobantes.row( this ).data();
                    $('.modal-comprobantes').modal('show');
                }
            });
		});

		$('.btn-selected-all').click(function() {
            $('.checkbox-select:checkbox').prop('checked', false);
            $('.checkbox-select').click();
            ids_selected = [];
            table_comprobantes.rows().eq(0).each( function ( idx ) {
                var row = table_comprobantes.row( idx );
                if ($(row.node()).hasClass('selected')) {
                    var comprobante = table_comprobantes.row( idx ).data().id + '-' + table_comprobantes.row( idx ).data().controller;
                    ids_selected.push(comprobante);
                }
            });
        });

        $('.btn-clear-selected-all').click(function() {
            $('.checkbox-select:checkbox').prop('checked', true);
            $('.checkbox-select').click();
            ids_selected = [];
        });

        $('#btn-go-customer').click(function() {
            var action = '/ispbrain/customers/view/' + comprobante_selected.customer_code;
            window.open(action, '_blank');
        });

        $(".btn-export").click(function() {
            bootbox.confirm('Se descargará un archivo Excel', function(result) {
                if (result) {
                    $('#table-comprobantes').tableExport({tableName: 'Correos', type:'excel', escape:'false', columnNumber: [4]});
                }
            }).find("div.modal-content").addClass("confirm-width-md");
        });

        $(document).on("click", ".btn-send", function(e) {

            if (ids_selected.length > 0) {

                $('.confirm-send-modal').modal('show');

            } else {
                bootbox.alert('Debe seleccionar al menos 1 correo para continuar.');
            }
        });

        $('.btn-confirm-send').click(function(e) {

            e.preventDefault();

            var data = {
                ids: ids_selected,
                template_id: $( "#template-id option:selected" ).val()
            };

            $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/MassEmails/selectComprobantesSend', 'method': 'post', 'id': 'form-block'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'data', 'value': JSON.stringify(data)}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'hash', 'value': hash}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#form-block').submit();

            table_comprobantes.ajax.reload();
            $('.confirm-send-modal').modal('hide');
        });

    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
