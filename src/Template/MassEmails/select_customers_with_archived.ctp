<style type="text/css">

    .status {
        border-radius: 4px;
        display: inline-block;
        font-weight: bold;
        color: white;
        line-height: 1em;
        font-size: 10.5px;
        padding: 3px 0;
        margin: 0px 0px 0px 0px;
        width: 100%;
    }

    .cp {
        border: 1px solid #f38a73;
        background-color: #f38a73;
    }

    .cc {
        border: 1px solid gray;
        background-color: gray;
    }

    .i {
        border: 1px solid purple;
        background-color: purple;
    }

    .plus1M {
        border: 1px solid #f05f40;
        background-color: #f05f40;
        width: 80%;
    }

    .plus2M {
        border: 1px solid #f05f40;
        background-color: #f05f40;
        width: 80%;
    }

    .plus3M {
        border: 1px solid #f05f40;
        background-color: #f05f40;
        width: 80%;
    }

    .fz-13px {
        font-size: 13px;
    }

    .fz-16px{
        font-size: 16px;
    }

    .fz-26px {
        font-size: 26px;
    }

    .font-size-13px{
        font-size: 13px;
    }

    legend {
        display: block;
        width: 100%;
        padding: 0;
        margin-bottom: 20px;
        font-size: 18px;
        line-height: inherit;
        color: #333;
        border: 0;
        border-bottom: 1px solid #e5e5e5;
    }

</style>

    <input type="hidden" name="" id="template_id" value="<?= !empty($template) ? $template->id : '' ?>"/>

    <div id="btns-tools">
        <div class="text-right btns-tools margin-bottom-5">

            <?php 

                echo  $this->Html->link(
                    '<span class="glyphicon icon-checkbox-checked" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Seleccionar todos',
                    'class' => 'btn btn-default btn-selected-all ml-2',
                    'escape' => false
                ]);

                echo  $this->Html->link(
                    '<span class="glyphicon icon-checkbox-unchecked" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Limpiar Selección',
                    'class' => 'btn btn-default btn-clear-selected-all ml-2',
                    'escape' => false
                ]);

                echo $this->Html->link(
                    '<span class="glyphicon icon-eye" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Ocultar o Mostrar Columnas',
                    'class' => 'btn btn-default btn-hide-column ml-2',
                    'escape' => false
                ]);

                echo $this->Html->link(
                    '<span class="glyphicon fas fa-search"  aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Buscar por columnas',
                    'class' => 'btn btn-default btn-search-column ml-1',
                    'escape' => false
                ]);

                echo $this->Html->link(
                    '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                    'javascript:void(0)',
                    [
                    'title' => 'Exportar tabla',
                    'class' => 'btn btn-default btn-export ml-1',
                    'escape' => false
                ]);
            ?>

        </div>
    </div>

    <div class="row">

        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">

            <div class="row">

                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">

                    <div class="card border-secondary mb-3" style="height: 318px;">
                        <div class="card-header p-2">Datos de Plantilla</div>

                        <div class="card-body text-secondary p-2">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <label class="control-label mt-1" for="mass-emails-template-id" style="margin-bottom: 0;">Plantilla</label>
                                    <?php
                                        echo $this->Form->input('template_id', ['class' => '', 'options' => $templates, 'label' =>  '', 'default' => $template->id]);
                                    ?>
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <label class="control-label" for="mass-emails-subject" style="margin-bottom: 0;">Asunto</label>
                                    <?php
                                        echo $this->Form->text('subject', ['id' => 'mass-emails-subject', 'label' => 'Asunto', 'class' => 'mt-2', 'type' => 'text', 'value' => $template->subject]);
                                    ?>
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <label class="control-label mt-3" for="mass-emails-email-id" style="margin-bottom: 0;">Cuenta</label>
                                    <?php
                                        $eamil_default = $email_account_selected ? $email_account_selected->id : "";
                                        echo $this->Form->input('email_id', ['options' => $emails, 'label' => '', 'default' => $eamil_default]);
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">

            <div class="card border-secondary mb-3">
                <div class="card-header p-2">Configuración</div>
                <div class="card-body text-secondary p-2">
                    <div class="row" style="margin-bottom: 22px;">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <?php 
                                echo $this->Form->input('business_billing', ['options' => $business, 'label' => '', 'default' => $template->business_billing]);
                            ?>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                            <a class="btn btn-info btn-sm btn-collapInvoices" data-toggle="collapse" href="#collapInvoices" role="button" aria-expanded="false" aria-controls="collapInvoices">
                                Factura
                            </a>
                        </div>
                        <div class="collapse p-3" id="collapInvoices">
                            <div class="row">
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                    <?php
                                        echo $this->Form->input('invoice_attach', ['label' => 'Adjuntar Factura', 'class' =>  ['ml-2', 'mt-2'], 'type' => 'checkbox', 'checked' => $template->invoice_attach, 'title' => 'Tildar para poder adjuntar facturas y habilitar las opciones de que período de facturas y optar por facturas impagas', 'data-toggle'=>'tooltip']);
                                    ?>
                                </div>
                                <?php
                                    $disabled = $template->invoice_attach ? '' : 'disabled';
                                    $receipt_disabled = $template->receipt_attach ? '' : 'disabled';
                                ?>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12">
                                    <?php
                                        echo $this->Form->input('invoice_impagas', ['label' => 'Impagas', 'class' => 'mt-2', 'type' => 'checkbox', 'disabled' => $disabled]);
                                    ?>
                                </div>
                                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12">
                                    <div class="input-group">
                                        <div class="input-group date" id='invoice-periode-datetimepicker'>
                                            <span class="input-group-addon input-group-text mr-0">Período</span>
                                            <input name="from" id="invoice-periode" type='text' class="form-control" <?= $disabled ?>/>
                                            <span class="input-group-addon input-group-text calendar mr-0">
                                                <span class="glyphicon icon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                            <a class="btn btn-info btn-sm btn-collapseReceipts" data-toggle="collapse" href="#collapseReceipts" role="button" aria-expanded="false" aria-controls="collapseReceipts">
                                Recibo
                            </a>
                        </div>
                        <div class="collapse p-3" id="collapseReceipts">
                            <div class="row">
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                                    <?php
                                        echo $this->Form->input('receipt_attach', ['label' => 'Adjuntar Recibo', 'class' =>  ['ml-2', 'mt-2'], 'type' => 'checkbox', 'checked' => $template->receipt_attach, 'title' => 'Tildar para poder adjuntar recibos y habilitar las opciones de que período de recibo', 'data-toggle'=>'tooltip']);
                                    ?>
                                </div>
                                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12">
                                    <div class="input-group">
                                        <div class="input-group date" id='receipt-periode-datetimepicker'>
                                            <span class="input-group-addon input-group-text mr-0">Período</span>
                                            <input name="from" id="receipt-periode" type='text' class="form-control" <?= $receipt_disabled ?>/>
                                            <span class="input-group-addon input-group-text calendar mr-0">
                                                <span class="glyphicon icon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                            <a class="btn btn-info btn-sm btn-collapseSummary" data-toggle="collapse" href="#collapseSummary" role="button" aria-expanded="false" aria-controls="collapseSummary">
                                Resumen de Cuenta
                            </a>
                        </div>
                        <div class="collapse p-3 w-100" id="collapseSummary">

                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <?php
                                        echo $this->Form->input('account_summary_attach', ['label' => 'Adjuntar Resumen de Cuenta', 'class' =>  ['ml-2', 'mt-2'], 'type' => 'checkbox', 'checked' => $template->account_summary_attach, 'title' => 'Tildar para poder adjuntar resumen de cuenta', 'data-toggle'=>'tooltip']);
                                    ?>
                                </div>
                            </div>

                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3" style="visibility: <?= $payment_getway->config->mercadopago->enabled || $payment_getway->config->cuentadigital->enabled ? 'visible' : 'hidden' ?>">
                            <a class="btn btn-info btn-sm btn-collapsePaymentMethod" data-toggle="collapse" href="#collapsePaymentMethod" role="button" aria-expanded="false" aria-controls="collapsePaymentMethod">
                                Medios de Pagos
                            </a>
                        </div>
                        <div class="collapse p-3" id="collapsePaymentMethod">

                            <div class="alert alert-info alert-dismissible fade show" role="alert">
                                <i class="fas fa-info-circle mr-1"></i> Al tildar <b>Link de pago Mercado Pago</b> ó <b>Link de pago Cuenta Digital</b>, se enviará un resumen de cuenta en el correo con el link de pago.<br><br><b>Aclaración</b>: no se tendrá en cuenta la plantilla elegida, debido a que se utilizará una plantilla especial. Para mayor información consultar sobre dicha plantilla
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="visibility: <?= $payment_getway->config->mercadopago->enabled ? 'visible' : 'hidden' ?>">
                                    <?php
                                        echo $this->Form->input('payment_link_mercadopago', ['label' => 'Link de Pago Mercado Pago', 'class' =>  ['ml-2', 'mt-2'], 'type' => 'checkbox', 'checked' => FALSE, 'title' => 'Tildar para poder enviar link de pago junto con un resumen de cuenta', 'data-toggle'=>'tooltip']);
                                    ?>
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="visibility: <?= $payment_getway->config->cuentadigital->enabled ? 'visible' : 'hidden' ?>">
                                    <?php
                                        echo $this->Form->input('payment_link_cuentadigital', ['label' => 'Link de Pago Cuenta Digital', 'class' =>  ['ml-2', 'mt-2'], 'type' => 'checkbox', 'checked' => FALSE, 'title' => 'Tildar para poder enviar link de pago junto con un resumen de cuenta', 'data-toggle'=>'tooltip']);
                                    ?>
                                    <div class="row">
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                            <?php
                                                $venc = [
                                                    '' => 'Abierto',
                                                    1  => 'a 1 día',
                                                    2  => 'a 2 días',
                                                    3  => 'a 3 días',
                                                    4  => 'a 4 días',
                                                    5  => 'a 5 días',
                                                    6  => 'a 6 días',
                                                    7  => 'a 7 días',
                                                    8  => 'a 8 días',
                                                    9  => 'a 9 días',
                                                    10 => 'a 10 días',
                                                    11 => 'a 11 días',
                                                    12 => 'a 12 días',
                                                    13 => 'a 13 días',
                                                    14 => 'a 14 días',
                                                    15 => 'a 15 días',
                                                    16 => 'a 16 días',
                                                    17 => 'a 17 días',
                                                    18 => 'a 18 días',
                                                    19 => 'a 19 días',
                                                    20 => 'a 20 días',
                                                    21 => 'a 21 días',
                                                    22 => 'a 22 días',
                                                    23 => 'a 23 días',
                                                    24 => 'a 24 días',
                                                    25 => 'a 25 días',
                                                    26 => 'a 26 días',
                                                    27 => 'a 27 días',
                                                    28 => 'a 28 días',
                                                    29 => 'a 29 días',
                                                    30 => 'a 30 días',
                                                    31 => 'a 31 días',
                                                ];
                                                echo $this->Form->input('cd_venc', ['class' => '', 'options' => $venc, 'label' => 'Vencimiento', 'default' => '']);
                                            ?>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                            <?php
                                                echo $this->Form->input('cd_saldo', ['class' => '', 'options' => [0 => 'Abierto', 1 => 'Saldo mes', 2 => 'Saldo total'], 'label' => 'Monto a cobrar', 'default' => '']);
                                            ?>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                            <?php
                                                echo $this->Form->input('cd_efectivo', ['id' => 'cd-efectivo', 'label' => 'Efectivo', 'class' => ['ml-2', 'mt-2'], 'type' => 'checkbox', 'checked' => TRUE, 'title' => 'Tildar para tener la opción de pago en efectivo', 'data-toggle' => 'tooltip']);
                                            ?>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                            <?php
                                                echo $this->Form->input('cd_credit_card', ['id' => 'cd-credit-card', 'label' => 'Tarjetas de crédito y débito', 'class' => ['ml-2', 'mt-2'], 'type' => 'checkbox', 'checked' => FALSE, 'title' => 'Tildar para tener la opción pago con tarjetas de crédito y débito', 'data-toggle' => 'tooltip']);
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                    <div class="card border-secondary mb-3" style="height:128px;">
                        <div class="card-header p-2">Bloque Nº</div>
                        <div class="card-body text-secondary p-2 text-center">
                            <span class="badge badge-info fz-26px" style="margin-top: 13px;"><?= $block_next_id ?></span>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                    <div class="card border-secondary mb-3" style="height:129px;">
                        <div class="card-header p-2 text-center">Mensajes</div>
                        <div class="card-body text-secondary p-2 text-center">
                            <span class="badge badge-secondary fz-16px mt-1" style="margin-top: 20px !important;">
                                    <i class="fas fa-check mr-1" aria-hidden="true" title="Contador de seleccionados" ></i>
                                    <span class="customer-count"></span><span class="">/<?= $limit ?></span>
                                </span>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                    <div class="card border-secondary mb-3" style="height:129px;">
                        <div class="card-header p-2 text-center">Acción</div>
                        <div class="card-body text-secondary p-2 text-center">
                            <div id="btn-send">
                                <?php
                                    echo  $this->Html->link(
                                        '<span class="fa fa-bullhorn" aria-hidden="true"></span> Enviar',
                                        'javascript:void(0)',
                                        [
                                        'class'  => 'btn btn-default btn-send margin-bottom-5',
                                        'style'  => 'margin-top: 14px',
                                        'escape' => false
                                    ]);
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-hover" id="table-customers">
                <thead>
                    <tr>
                        <th></th>                     <!--0-->
                        <th >Creado</th>              <!--1-->
                        <th >Código</th>              <!--2-->
                        <th >Cuenta</th>              <!--3-->
                        <th >Etiquetas</th>           <!--4-->
                        <th >Impagas</th>             <!--5-->
                        <th >Saldo mes</th>           <!--6-->
                        <th >Nombre</th>              <!--7-->
                        <th >Documento</th>           <!--8-->
                        <th >Estado Servicios</th>    <!--9-->
                        <th >Domicilio</th>           <!--10-->
                        <th >Área</th>                <!--11-->
                        <th >Teléfono</th>            <!--12-->
                        <th >Vendedor</th>            <!--13-->
                        <th >Resp. Iva</th>           <!--14-->
                        <th >Comp.</th>               <!--15-->
                        <th >Deno.</th>               <!--16-->
                        <th >Correo electrónico</th>  <!--17-->
                        <th >Comentarios</th>         <!--18-->
                        <th >Día Venc.</th>           <!--19-->
                        <th >Clave Portal</th>        <!--20-->
                        <th >Empresa Facturación</th> <!--21-->
                        <th >Controladores</th>       <!--22-->
                        <th >Servicios</th>           <!--23-->
                        <th >Cuentas</th>             <!--24-->
                        <th >Archivado</th>           <!--25-->
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade confirm-send-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabel">Confirmar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label>Se enviará el correo a los clientes seleccionados. ¿Desea continuar?</label>

                        <div class="row">
                            <div class="col-md-4">
                        	    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        	</div>
                        	<div class="col-md-4">
                                <button type="button" class="btn btn-danger btn-confirm-send float-right" value="select_customers">Confirmar y Continuar</button>
                            </div>
                            <div class="col-md-4">
                                <button type="button" class="btn btn-info btn-confirm-send float-right" value="messages">Confirmar y Salir</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-go-customer',
        'name' =>  'Ficha del Cliente',
        'icon' =>  'glyphicon icon-profile',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-customers-mass-emails']);
    echo $this->element('actions', ['modal'=> 'modal-actions-customers', 'title' => 'Acciones', 'buttons' => $buttons]);
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-customers']);
    echo $this->element('modal_preloader');
?>

<script type="text/javascript">

    var template = <?php echo json_encode($template); ?>;
    var templates = <?php echo json_encode($templates_model); ?>;
    var emails = <?php echo json_encode($emails_model); ?>;

    var table_customers = null;
    var customer_selected = null;
    var labels = <?= json_encode($labels) ?>;
    var customer_count;
    var limit;
    var curr_pos_scroll = null;
    var ids_selected = [];
    var areas;
    var controllers;
    var services;
    var accounts = null;
    accounts = <?= json_encode($accounts) ?>;
    var hash = null;

    $(document).ready(function () {

        hash = <?= time() ?>;

        $("#cd-venc").attr("disabled", "disabled");
        $("#cd-saldo").attr("disabled", "disabled");
        $("#cd-efectivo").attr("disabled", "disabled");
        $("#cd-credit-card").attr("disabled", "disabled");

        $('[data-toggle="tooltip"]').tooltip();

        $('#invoice-periode-datetimepicker').datetimepicker({
        	locale: 'es',
        	defaultDate: new Date(),
        	format: 'MM/YYYY'
        });

        $('#receipt-periode-datetimepicker').datetimepicker({
        	locale: 'es',
        	defaultDate: new Date(),
        	format: 'MM/YYYY'
        });

        limit = <?php echo json_encode($limit); ?>;
        areas = <?php echo json_encode($areas); ?>;
        controllers = <?php echo json_encode($controllers); ?>;
        services = <?php echo json_encode($services); ?>;

        $('.btn-hide-column').click(function() {
            $('.modal-hide-columns-customers-mass-emails').modal('show');
        });

        $('.btn-search-column').click(function() {
            $('.modal-search-columns-customers').modal('show');
        });

        customer_count = 0;
        $('.customer-count').text(customer_count);

        $('#table-customers').removeClass('display');

        $('#btns-tools').hide();

        loadPreferences('customers-mass-email', [3, 10, 11, 12, 13, 14, 15, 18, 19, 20, 21]);

		table_customers = $('#table-customers').DataTable({
		    "order": [[ 1, 'desc' ]],
            "deferRender": true,
            "processing": true,
            "serverSide": true,
		    "ajax": {
                "url": "/ispbrain/MassEmails/get_customers_with_archived.json",
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "drawCallback": function( c ) {
                var flag = true;
            	switch (c.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
	            if (flag) {
	                if (table_customers) {

                        var tableinfo = table_customers.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }

                        if (curr_pos_scroll) {

                            $(table_customers.settings()[0].nScrollBody).scrollTop( curr_pos_scroll.top );
                            $(table_customers.settings()[0].nScrollBody).scrollLeft( curr_pos_scroll.left );
                        }
                    }
	            }
            },
            "scrollY": true,
		    "scrollY": '430px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
		    "columns": [
		        {
                    "className":      'checkbox-control',
                    "orderable":      false,
                    "data":           'id',
                    "render": function ( data, type, full, meta ) {
                        return '<label class="custom-control custom-checkbox pl-3 pr-3 m-0 align-middle"><input type="checkbox" class="custom-control-input checkbox-select p-0 m-0 " id="row-checkbox-' + data + '"><span class="custom-control-indicator w-100 align-middle"></span></label>';
                    },
                    "width": '3%'
                },
                {
                    "className":"row-control",
                    "data": "created",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0];
                        }
                    }
                },
                {
                    "className":"row-control",
                    "data": "code",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                {
                    "className":"row-control",
                    "data": "account_code",
                    "type": "string"
                },
                {
                    "className":"row-control",
                    "data": "status",
                    "type": "options_colors",
                    "options": labels,
                    "render": function ( data, type, row ) {
                        var spans = "";
                        $.each(row.labels, function(i, val) {
                            spans += "<span class='status' style='background-color: " + val.color_back + "' > </span>"; 
                        });
                        return spans;
                    }
                },
                {
                    "className":"row-control",
                    "data": "invoices_no_paid",
                    "type": "decimal",
                    "render": function ( data, type, row ) {
                       if (row.invoices_no_paid > 0) {
                           return "<span class='badge badge-danger font-size-13px' >" + row.invoices_no_paid + "</span>";
                       }
                       return "<span class='badge badge-success font-size-13px'>" + row.invoices_no_paid + "</span>";
                    }
                    
                },
                {
                    "className":"row-control",
                    "data": "debt_month",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' ),
                },
                {
                    "className":"row-control",
                    "data": "name",
                    "type": "string",
                },
                {
                    "className":"row-control",
                    "data": "ident",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (data) {
                            ident = data;
                        }
                        return sessionPHP.afip_codes.doc_types[row.doc_type] + ' ' + ident;
                    }
                },
                {
                    "className":"row-control",
                    "data": "services_active",
                    "type": "options",
                    "options": {  
                        "0":"Bloqueados",
                        "1":"Habilitados",
                    },
                    "render": function ( data, type, row ) {
                       return "<span class='font-size-13px'>" + row.services_active + '/' + row.services_total + "</span>";
                    }
                    
                },
                {
                    "className":"row-control",
                    "data": "address",
                    "type": "string",
                },
                {
                    "className":"row-control",
                    "data": "area_id",
                    "type": "options",
                    "options": areas,
                    "render": function ( data, type, row ) {
                        if (data) {
                            return areas[data];
                        }
                        return "";
                    }
                },
                {
                    "className":"row-control",
                    "data": "phone",
                    "type": "integer",
                },
                { 
                    "data": "seller",
                    "type": "string",
                },
                {
                    "className":"row-control",
                    "data": "responsible",
                    "type": "options",
                    "options": sessionPHP.afip_codes.responsibles,
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.responsibles[data];
                    }
                },
                {
                    "className":"row-control",
                    "data": "is_presupuesto",
                    "type": "options",
                    "options": {  
                        "0":"Factura",
                        "1":"Presupuesto",
                    },
                    "render": function ( data, type, row ) {
                        if (data) {
                            return 'PRESU';
                        }
                        return 'FACTURA';
                    }
                },
                {
                    "className":"row-control",
                    "data": "denomination",
                    "type": "options",
                    "options": {  
                        "0":"No Denominado",
                        "1":"Denominado",
                    },
                    "render": function ( data, type, row ) {
                        if (data) {
                            return 'SI';
                        }
                        return 'NO';
                    }
                },
                {
                    "className":"row-control",
                    "data": "email",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                {
                    "className":"row-control",
                    "data": "comments",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                {
                    "className":"row-control",
                    "data": "daydue",
                    "type": "integer",
                },
                {
                    "className":"row-control",
                    "data": "clave_portal",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                {
                    "className":"row-control",
                    "data": "business_billing",
                    "type": "options_obj",
                    "options": sessionPHP.paraments.invoicing.business,
                    "render": function ( data, type, row ) {
                        var business_name = '';
                        $.each(sessionPHP.paraments.invoicing.business, function (i, b) {
                            if (b.id == data) {
                                business_name = b.name;
                            }
                        });
                        return business_name;
                    }
                },
                {
                    "className":"row-control",
                    "data": "controller",
                    "type": "string",
                    "type": "options",
                    "options": controllers,
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                {
                    "className":"row-control",
                    "data": "service",
                    "type": "string",
                    "type": "options",
                    "options": services,
                    "render": function ( data, type, row ) {
                        if (data) {
                            return data;
                        }
                        return '';
                    }
                },
                {
                    "className":"row-control",
                    "data": "customers_accounts",
                    "type": "string",
                    "type": "options",
                    "options": accounts,
                    "render": function ( data, type, row ) {
                        if (row.customers_accounts.length > 0) {
                            var accounts_name = "";
                            $.each(row.customers_accounts, function( index, value ) {
                                accounts_name += accounts[value.payment_getway_id] + ' - ';
                            });
                            return accounts_name;
                        }
                        return '';
                    }
                },
                {
                    "className":"row-control",
                    "data": "deleted",
                    "type": "options",
                    "options": {  
                        "0":"NO",
                        "1":"SI",
                    },
                    "render": function ( data, type, row ) {
                        if (data) {
                            return 'SI';
                        }
                        return 'NO';
                    }
                },
            ],
		    "columnDefs": [
                {
                "targets": [0, 1, 2, 3, 4, 5, 6, 8, 9, 11, 12, 13, 14, 15, 16, 17, 19, 20],
                    "width": '5%'
                },
                {
                    "targets": [21],
                    "width": '8%'
                },
                {
                    "targets": [16, 18],
                    "width": '13%'
                },
                {
                    "targets": [7],
                    "width": '12%'
                },
                { 
                    "class": "left", targets: [7, 16]
                },
                { 
                    "visible": false, targets: hide_columns
                },
                { "orderable": false, "targets": [4, 9] }
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.code;
                $.each(ids_selected, function( index, value ) {
                    if (row.id == value) {
                        $(row).addClass('selected');
                        $(row).find('input:checkbox').attr('checked', 'checked')
                    }
                });
            },
		    "language": dataTable_lenguage,
		    "pagingType": "numbers",
            "lengthMenu": [[limit], [limit]],
        	"dom":
    	    	"<'row'<'col-xl-2'l><'col-xl-2 selected'><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
		});

	    $('#table-customers_wrapper .tools').append($('#btns-tools').contents());

	    $('#btns-tools').show();

		$('#table-customers').on( 'init.dt', function () {

		    createModalHideColumn(table_customers, '.modal-hide-columns-customers-mass-emails');
		    createModalSearchColumn(table_customers, '.modal-search-columns-customers');
		    loadSearchPreferences('customers-mass-email-search');

		    $( "#table-customers tbody" ).on( "click", ".checkbox-select", function() {

                if ($(this).is(':checked')) {
                    if (customer_count < limit) {
                        $(this).closest('tr').addClass('selected');
                        ids_selected.push(table_customers.row( $(this).closest('tr') ).data().code);
                        customer_count++;
                    } else {
                        $(this).val('FALSE');
                        $(this).prop('checked', false);
                        generateNoty('warning', "Llego al límite de correos para un bloque.");
                    }
                } else {
                    $(this).closest('tr').removeClass('selected');
                    ids_selected.splice($.inArray(table_customers.row( $(this).closest('tr') ).data().code, ids_selected), 1);
                    customer_count--;
                }
                $('.customer-count').text(customer_count);
            });

            $("#table-customers tbody").on( "click", ".row-control", function() {
                customer_selected = table_customers.row( $(this).closest('tr') ).data();
                $('.modal-actions-customers').modal('show');
            });
        });

        $('.btn-selected-all').click(function() {
            $('.checkbox-select:checkbox').prop('checked', false);
            $('.checkbox-select').click();
            customer_count = 0;
            ids_selected = [];
            table_customers.rows().eq(0).each( function ( idx ) {
                var row = table_customers.row( idx );
                if ($(row.node()).hasClass('selected')) {
                    ids_selected.push(table_customers.row( idx ).data().code);
                    customer_count++;
                }
            });
            $('.customer-count').text(customer_count);
        });

        $('.btn-clear-selected-all').click(function() {
            $('.checkbox-select:checkbox').prop('checked', true);
            $('.checkbox-select').click();
            customer_count = 0;
            ids_selected = [];
            table_customers.rows().eq(0).each( function ( idx ) {
                var row = table_customers.row( idx );
                if ($(row.node()).hasClass('selected')) {
                    customer_count++;
                }
            });
            $('.customer-count').text(customer_count);
        });

        $('#btn-go-customer').click(function() {
            var action = '/ispbrain/customers/view/' + customer_selected.code;
            window.open(action, '_blank');
        });

        $(".btn-export").click(function() {
            bootbox.confirm('Se descargará un archivo Excel', function(result) {
                if (result) {
                    $('#table-customers').tableExport({tableName: 'Avisos: Selección de Clientes', type:'excel', escape:'false'});
                }
            }).find("div.modal-content").addClass("confirm-width-md");
        });

        $(document).on("click", ".btn-send", function(e) {

            if (ids_selected.length > 0) {

                if ($('#business-billing :selected').val() != '') {

                    if ($('#email-id :selected').text() != '') {
                        $('.confirm-send-modal').modal('show');
                    } else {
                        bootbox.alert('Debe seleccionar una Cuenta de Correo.');
                    }
                } else {
                    bootbox.alert('Debe seleccionar la empresa.');
                }

            } else {
                bootbox.alert('Debe seleccionar al menos 1 cliente para continuar.');
            }
        });

        $('.btn-confirm-send').click(function() {

            var data = {
                ids: ids_selected,
                business_billing: $('#business-billing :selected').val(),
                email_id: $('#email-id :selected').val(),
                invoice_attach: $('#invoice-attach').is(':checked'),
                receipt_attach: $('#receipt-attach').is(':checked'),
                account_summary_attach: $('#account-summary-attach').is(':checked'),
                payment_link_mercadopago: $('#payment-link-mercadopago').is(':checked'),
                payment_link_cuentadigital: $('#payment-link-cuentadigital').is(':checked'),
                cd_venc: $('#cd-venc :selected').val(),
                cd_saldo: $('#cd-saldo :selected').val(),
                cd_efectivo: $('#cd-efectivo').is(':checked'),
                cd_credit_card: $('#cd-credit-card').is(':checked'),
                subject: $('#mass-emails-subject').val(),
                template_id: $('#template-id').val(),
                invoice_impagas: $('#invoice-impagas').is(':checked'),
                invoice_periode: $("#invoice-periode-datetimepicker").find("input").val(),
                receipt_periode: $("#receipt-periode-datetimepicker").find("input").val(),
                redirect: $(this).attr("value")
            };

            $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/MassEmails/selectCustomersWithArchived/' + template.id, 'method': 'post', 'id': 'form-block'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'data', 'value': JSON.stringify(data)}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'hash', 'value': hash}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#form-block').submit();

            table_customers.ajax.reload();
            $('.confirm-send-modal').modal('hide');
        });

        $('#payment-link-cuentadigital').change(function(event) {
           if ($(this).is(":checked")) {
               $("#cd-venc").removeAttr("disabled");
               $("#cd-saldo").removeAttr("disabled");
               $("#cd-efectivo").removeAttr("disabled");
               $("#cd-credit-card").removeAttr("disabled");
           } else {
               $("#cd-venc").attr("disabled", "disabled");
               $("#cd-saldo").attr("disabled", "disabled");
               $("#cd-efectivo").attr("disabled", "disabled");
               $("#cd-credit-card").attr("disabled", "disabled");
           }
        });

        $('#invoice-attach').change(function(event) {
           if ($(this).is(":checked")) {
               $("#invoice-impagas").removeAttr("disabled");
               $("#invoice-periode").removeAttr("disabled");
           } else {
               $("#invoice-impagas").attr("disabled", "disabled");
               $("#invoice-periode").attr("disabled", "disabled");
           }
        });

        $('#receipt-attach').change(function(event) {
           if ($(this).is(":checked")) {
               $("#receipt-periode").removeAttr("disabled");
           } else {
               $("#receipt-periode").attr("disabled", "disabled");
           }
        });

        $('#template-id').change(function(event) {

            var template_selected = $(this).val();

            $('#invoice-attach').prop('checked', false);
            $('#receipt-attach').prop('checked', false);
            $('#invoice-impagas').prop('checked', false);

            $.each(templates, function( index, value ) {

                if (template_selected == value.id) {

                    if (value.invoice_attach) {
                       $("#invoice-impagas").removeAttr("disabled");
                       $("#invoice-periode").removeAttr("disabled");
                    } else {
                       $("#invoice-impagas").attr("disabled", "disabled");
                       $("#invoice-periode").attr("disabled", "disabled");
                    }

                    $('#invoice-attach').prop('checked', value.invoice_attach);

                    if (value.receipt_attach) {
                       $("#receipt-periode").removeAttr("disabled");
                    } else {
                       $("#receipt-periode").attr("disabled", "disabled");
                    }

                    $('#receipt-attach').prop('checked', value.receipt_attach);

                    $('#account-summary-attach').prop('checked', value.account_summary_attach);

                    $('#mass-emails-subject').val(value.subject);
                    $("#business-billing").val(value.business_billing);

                    $('#email-id').val(value.email_id);
                }
            });
        });
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
