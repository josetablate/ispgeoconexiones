<style type="text/css">

    #table-mass-emails-templates {
        width: 100% !important;
    }

    #table-mass-emails-templates td {
        margin: 0px 0px 0px 0px !important;
        padding: 0px 5px 0px 5px !important;
        vertical-align: middle;
        max-width: 150px !important;
        word-wrap: break-word !important;
    }

    .modal-actions a.btn {
        width: 100%;
        margin-bottom: 5px;
    }

    tr.selected {
        background-color: #8eea99;
        color: #333 !important;
    }

    .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
        background-color: #d2d2d2;
    }

    .vertical-table {
        width: 100%;
    }

    .vertical-table td {
        max-width: 250px !important;
        word-wrap: break-word !important;
    }

    .my-hidden {
        display: none;
    }

    .info-messages {
        background: #607D8B;
        border-radius: 5px;
        padding: 7px;
        color: white;
    }

    .info-package {
        background: #00BCD4;
        border-radius: 5px;
        padding: 7px;
        color: white;
        margin-left: 10px;
    }

    .info-server {
        background: #8bc34a;
        border-radius: 5px;
        padding: 7px;
        color: white;
        margin-left: 10px;
    }

</style>

<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5">
        <?php 

            echo $this->Html->link(
                '<span class="glyphicon icon-plus" aria-hidden="true"></span>',
                ['controller' => 'MassEmails', 'action' => 'addTemplate'],
                [
                'title' => 'Nuevo Template',
                'class' => 'btn btn-default btn-add',
                'escape' => false
            ]);

        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-hover" id="table-mass-emails-templates" >
            <thead>
                <tr>
                    <th><?= ('#') ?></th>
                    <th><?= ('Nombre') ?></th>
                    <th><?= ('Descrip.') ?></th>
                    <th><?= ('Mensaje') ?></th>
                    <th><?= ('Empresa') ?></th>
                    <th><?= ('Adj. factura') ?></th>
                    <th><?= ('Adj. recibo') ?></th>
                    <th><?= ('Adj. Resumen de Cta') ?></th>
                    <th><?= ('Cta de Correo') ?></th>
                    <th><?= ('Habilitado') ?></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-select-customers',
        'name' =>  'Seleccionar Clientes',
        'icon' =>  'fa fa-users',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-edit',
        'name' =>  'Editar',
        'icon' =>  'icon-pencil2',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-enable',
        'name' =>  'Habilitar',
        'icon' =>  'fas fa-check',
        'type' =>  'btn-secondary'
    ];

    $buttons[] = [
        'id'   =>  'btn-disable',
        'name' =>  'Deshabilitar',
        'icon' =>  'fas fa-times',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-mass-emails-templates']);
    echo $this->element('actions', ['modal'=> 'modal-mass-emails-templates', 'title' => 'Acciones', 'buttons' => $buttons ]);
?>

<script type="text/javascript">

    var table_mass_emails_templates = null;
    var mass_emails_template_selected = null;

    $(document).ready(function () {

        $('#table-mass-emails-templates').removeClass('display');

        $('#btns-tools').hide();

		table_mass_emails_templates = $('#table-mass-emails-templates').DataTable({
		    "order": [[ 1, 'desc' ]],
            "deferRender": true,
		    "ajax": {
                "url": "/ispbrain/MassEmails/index.json",
                "dataSrc": "templates",
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
		    "scrollY": true,
		    "scrollY": '450px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
		    
		    "columns": [
                { 
                    "data": "id"
                },
                { 
                    "data": "name"
                },
                { 
                    "data": "description"
                },
                { 
                    "data": "message"
                },
                {
                    "data": "business_billing",
                    "render": function ( data, type, row ) {
                        var business_name = '';
                        $.each (sessionPHP.paraments.invoicing.business, function (i, b) {
                            if (b.id == data) {
                                business_name = b.name + ' (' + b.address + ')';
                            }
                        });
                        return business_name;
                    }
                },
                { 
                    "data": "invoice_attach",
                    "render": function ( data, type, row ) {
                        var enabled = '<i class="fa fa-times red" aria-hidden="true"></i>';
                        if (data) {
                            enabled = '<i class="fa fa-check green" aria-hidden="true"></i>';
                        }
                        return enabled;
                    }
                },
                { 
                    "data": "receipt_attach",
                    "render": function ( data, type, row ) {
                        var enabled = '<i class="fa fa-times red" aria-hidden="true"></i>';
                        if (data) {
                            enabled = '<i class="fa fa-check green" aria-hidden="true"></i>';
                        }
                        return enabled;
                    }
                },
                { 
                    "data": "account_summary_attach",
                    "render": function ( data, type, row ) {
                        var enabled = '<i class="fa fa-times red" aria-hidden="true"></i>';
                        if (data) {
                            enabled = '<i class="fa fa-check green" aria-hidden="true"></i>';
                        }
                        return enabled;
                    }
                },
                {
                    "data": "email_id",
                    "render": function ( data, type, row ) {
                        var email_contact = '';
                        var enabled = '<i class="fa fa-times red" aria-hidden="true"></i>';
                        $.each (sessionPHP.paraments.mass_emails.emails, function (i, email) {
                            if (email.id == data) {
                                email_contact = email.contact;
                                if (email.enabled) {
                                    enabled = '<i class="fa fa-check green" aria-hidden="true"></i>';
                                }
                            }
                        });
                        return email_contact + ' ' + enabled;
                    }
                },
                { 
                    "data": "enabled",
                    "render": function ( data, type, row ) {
                        var enabled = '<i class="fa fa-times red" aria-hidden="true"></i>';
                        if (data) {
                            enabled = '<i class="fa fa-check green" aria-hidden="true"></i>';
                        }
                        return enabled;
                    }
                }
            ],
		    "columnDefs": [
		        { "type": 'date-custom', targets: [] },
		        { "type": 'numeric-comma', targets: [] },
                {
                    "targets": [],
                    "width": '8%'
                },
                { 
                    "class": "left", targets: []
                },
                { 
                    "visible": false, targets: []
                },
            ],
             "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },

		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            
        	"dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-5'f><'col-xl-1 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
		});

		$('#table-mass-emails-templates_wrapper .tools').append($('#btns-tools').contents());
        $('#table-mass-emails-templates').on( 'init.dt', function () {
            createModalHideColumn(table_mass_emails_templates, '.modal-hide-columns-mass-emails-templates');
        });

		$('#btns-tools').show();

		$('#table-mass-emails-templates tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                table_mass_emails_templates.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
                mass_emails_template_selected = table_mass_emails_templates.row( this ).data();
                if (mass_emails_template_selected.enabled) {
                    $('#btn-enable').addClass('my-hidden');
                    $('#btn-disable').removeClass('my-hidden');
                } else {
                    $('#btn-enable').removeClass('my-hidden');
                    $('#btn-disable').addClass('my-hidden');
                }
                $('.modal-mass-emails-templates').modal('show');
            }
        });

    });

    $('#btn-select-customers').click(function() {
        var action = '/ispbrain/MassEmails/select-customers/' + mass_emails_template_selected.id;
        window.open(action, '_self');
    });

    $('#btn-edit').click(function() {
        var action = '/ispbrain/MassEmails/editTemplate/' + mass_emails_template_selected.id;
        window.open(action, '_self');
    });

    $('#btn-enable').click(function() {

        var controller  = 'MassEmails';
        var id  = mass_emails_template_selected.id;
        var name = mass_emails_template_selected.name;
        var text = '¿Está Seguro que desea habilitar el Template: ' + name + '?';

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                    .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/enableTemplate/', 'method': 'post', 'id': 'replacer'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                    .find('#replacer').submit();
            }
        });
    });

    $('#btn-disable').click(function() {

        var controller  = 'MassEmails';
        var id  = mass_emails_template_selected.id;
        var name = mass_emails_template_selected.name;
        var text = '¿Está Seguro que desea deshabilitar el Template: ' + name + '?';

        bootbox.confirm(text, function(result) {
            if (result) {
                $('body')
                    .append( $('<form/>').attr({'action': '/ispbrain/' + controller + '/disableTemplate/', 'method': 'post', 'id': 'replacer'})
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'id', 'value': id}))
                    .append( $('<input/>').attr( {'type': 'hidden', 'name': 'param2', 'value': "world"})))
                    .find('#replacer').submit();
            }
        });
    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
