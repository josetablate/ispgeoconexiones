<style type="text/css">

    .my-hidden {
        display: none;
    }

</style>

<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5">

        <?php

            echo  $this->Html->link(
                '<span class="glyphicon icon-checkbox-checked" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Seleccionar todos',
                'class' => 'btn btn-default btn-selected-all ml-2',
                'escape' => false
            ]);

            echo  $this->Html->link(
                '<span class="glyphicon icon-checkbox-unchecked" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Limpiar Selección',
                'class' => 'btn btn-default btn-clear-selected-all ml-2',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="fa fa-bullhorn" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Reenviar',
                'class' => 'btn btn-success btn-resend ml-2',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="fas fa-archive" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Archivar',
                'class' => 'btn btn-info btn-archive ml-2',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-eye" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Ocultar o Mostrar Columnas',
                'class' => 'btn btn-default btn-hide-column ml-2',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon fas fa-search" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Buscar por Columnas',
                'class' => 'btn btn-default btn-search-column ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar en formato excel el contenido de la Tabla',
                'class' => 'btn btn-default btn-export ml-2',
                'escape' => false
            ]);
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-hover" id="table-sms" >
            <thead>
                <tr>
                    <th></th>                               <!--0-->
                    <th><?= ('Plantilla') ?></th>           <!--1-->
                    <th><?= ('Blq.') ?></th>                <!--2-->
                    <th><?= ('Mensaje') ?></th>             <!--3-->
                    <th><?= ('Asunto') ?></th>              <!--4-->
                    <th><?= ('Estado') ?></th>              <!--5-->
                    <th><?= ('Envio') ?></th>               <!--6-->
                    <th><?= ('Cliente') ?></th>             <!--7-->
                    <th><?= ('Doc.') ?></th>                <!--8-->
                    <th><?= ('Cód.') ?></th>                <!--9-->
                    <th><?= ('Correo Electr.') ?></th>      <!--10-->
                    <th><?= ('Emp.') ?></th>                <!--11-->
                    <th><?= ('Usuario') ?></th>             <!--12-->
                    <th><?= ('Adj. Fact.') ?></th>          <!--13-->
                    <th><?= ('Impagas') ?></th>             <!--14-->
                    <th><?= ('Prdo Fact.') ?></th>          <!--15-->
                    <th><?= ('Adj. Rcbo.') ?></th>          <!--16-->
                    <th><?= ('Prdo Rcbo') ?></th>           <!--17-->
                    <th><?= ('Cta') ?></th>                 <!--18-->
                    <th><?= ('Adj. Res. Cta') ?></th>       <!--19-->
                    <th><?= ('Link MercadoPago') ?></th>    <!--20-->
                    <th><?= ('Link Cuenta Digital') ?></th> <!--21-->
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade confirm-send-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabel">Confirmar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label>Se archivarán los correos filtrados. ¿Desea continuar?</label>

                        <div class="row">
                            <div class="col-md-4">
                        	    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        	</div>
                        	<div class="col-md-4">
                                <button type="button" class="btn btn-danger btn-confirm-send float-right" value="messages">Confirmar y Continuar</button>
                            </div>
                            <div class="col-md-4">
                                <button type="button" class="btn btn-info btn-confirm-send float-right" value="archived_messages">Confirmar y Salir</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade confirm-resend-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabel">Confirmar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label>Se reenviarán los correos seleccionados. ¿Desea continuar?</label>

                        <div class="row">
                            <div class="col-md-4">
                        	    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        	</div>
                        	<div class="col-md-4">
                                <button type="button" class="btn btn-danger btn-confirm-resend float-right" value="messages">Confirmar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-go-customer',
        'name' =>  'Ficha del Cliente',
        'icon' =>  'glyphicon icon-profile',
        'type' =>  'btn-primary'
    ];

    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-customers-mass-emails']);
    echo $this->element('actions', ['modal'=> 'modal-sms', 'title' => 'Acciones', 'buttons' => $buttons ]);
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-customers-mass-emails']);
?>

<script type="text/javascript">

    var table_sms = null;
    var sms_selected = null;
    var emails_selected_ids = null;
    var users = null;
    var templates = null;
    var account_emails = null;
    var ids_selected = [];
    var hash = null;

    $(document).ready(function () {

        users = <?= json_encode($users) ?>;
        templates = <?= json_encode($templates) ?>;
        account_emails = <?= json_encode($account_emails) ?>;
        hash = <?= time() ?>;

        $('.btn-hide-column').click(function() {
            $('.modal-hide-columns-customers-mass-emails').modal('show');
        });

        $('.btn-search-column').click(function() {
           $('.modal-search-columns-customers-mass-emails').modal('show');
        });

        $('#table-sms').removeClass('display');

        $('#btns-tools').hide();

		table_sms = $('#table-sms').DataTable({
		    "order": [[ 1, 'desc' ]],
            "deferRender": true,
            "processing": true,
            "serverSide": true,
		    "ajax": {
                "url": "/ispbrain/MassEmails/getMessages.json",
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "archived": false
                    });
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "drawCallback": function( settings ) {
                var flag = true;
            	switch (settings.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
            	    if (table_sms) {

                        var tableinfo = table_sms.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }
                    }
            	}
            },
		    "scrollY": true,
		    "scrollY": '450px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
		    "columns": [
		        {
                    "className": 'checkbox-control',
                    "orderable": false,
                    "data": 'id',
                    "render": function ( data, type, full, meta ) {
                        return '<label class="custom-control custom-checkbox pl-3 pr-3 m-0 align-middle"><input type="checkbox" class="custom-control-input checkbox-select p-0 m-0 " id="row-checkbox-' + data + '"><span class="custom-control-indicator w-100 align-middle"></span></label>';
                    },
                    "width": '3%'
                },
		        { 
                    "className": "row-control",
                    "data": "mass_emails_block.template_name",
                    "type": "options",
                    "options": templates,
                    "render": function ( data, type, row ) {
                        return  data;
                    }
                },
                { 
                    "className": "row-control",
                    "data": "mass_emails_block.id",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return  data;
                    }
                },
                { 
                    "className": "row-control",
                    "data": "message",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        return  data;
                    }
                },
                { 
                    "className": "row-control",
                    "data": "subject",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        return  data;
                    }
                },
                { 
                    "className": "row-control",
                    "data": "status",
                    "type": "options",
                    "options": {
                        'Enviado': 'Enviado',
                        'No enviado': 'No enviado',
                    },
                    "render": function ( data, type, row ) {
                        return  data;
                    }
                },
                {
                    "className": "row-control text-info",
                    "data": "mass_emails_block.created",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            var hours = data.split('T')[1].split('-')[0].split(':');
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0] + ' ' + hours[0] + ':' + hours[1];
                        }
                    }
                },
                { 
                    "className": "row-control",
                    "data": "customer.name",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        return  data;
                    }
                },
                { 
                    "className": "row-control left",
                    "data": "customer.ident",
                    "type": "ident",
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (data) {
                            ident = data;
                        }
                        return sessionPHP.afip_codes.doc_types[row.customer.doc_type] + ' ' + ident;
                    }
                },
                { 
                    "className": "row-control",
                    "data": "customer.code",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                {
                    "className": "row-control",
                    "data": "email",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        var email = row.customer.hasOwnProperty('email') ? row.customer.email : '';
                        if (data) {
                            email = data;
                        }
                        return email;
                    }
                },
                {
                    "className": "row-control",
                    "data": "business_billing",
                    "type": "options_obj",
                    "options": sessionPHP.paraments.invoicing.business,
                    "render": function ( data, type, row ) {
                        var business_name = '';
                        $.each(sessionPHP.paraments.invoicing.business, function (i, b) {
                            if (b.id == data) {
                                business_name = b.name + ' (' + b.address + ')';
                            }
                        });
                        return business_name;
                    }
                },
                { 
                    "className": "row-control",
                    "data": "mass_emails_block.user_id",
                    "type": "options",
                    "options": users,
                    "render": function ( data, type, row ) {
                        return users[data];
                    }
                },
                {
                    "className": "row-control",
                    "data": "invoice_attach",
                    "type": "options",
                    "options": {
                        "0":"No",
                        "1":"Si",
                    },
                    "render": function ( data, type, row ) {
                        var enabled = 'No';
                        if (data) {
                            enabled = 'Si';
                        }
                        return enabled;
                    }
                },
                {
                    "className": "row-control",
                    "data": "invoice_impagas",
                    "type": "options",
                    "options": {
                        "0":"No",
                        "1":"Si",
                    },
                    "render": function ( data, type, row ) {
                        var enabled = 'No';
                        if (data) {
                            enabled = 'Si';
                        }
                        return enabled;
                    }
                },
                {
                    "className": "row-control text-info",
                    "data": "invoice_periode",
                    "type": "periode",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var data = data.split('-');
                            return data[1] + '/' + data[0];
                        }
                        return data;
                    }
                },
                {
                    "className": "row-control",
                    "data": "receipt_attach",
                    "type": "options",
                    "options": {
                        "0":"No",
                        "1":"Si",
                    },
                    "render": function ( data, type, row ) {
                        var enabled = 'No';
                        if (data) {
                            enabled = 'Si';
                        }
                        return enabled;
                    }
                },
                {
                    "className": "row-control text-info",
                    "data": "receipt_periode",
                    "type": "periode",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var data = data.split('-');
                            return data[1] + '/' + data[0];
                        }
                        return data;
                    }
                },
                {
                    "className": "row-control",
                    "data": "mass_emails_block.email_account",
                    "type": "options",
                    "options": account_emails,
                    "render": function ( data, type, row ) {
                        var email_account = "";
                        if (data) {
                            email_account = data;
                        }
                        return email_account;
                    }
                },
                {
                    "className": "row-control",
                    "data": "account_summary_attach",
                    "type": "options",
                    "options": {
                        "0":"No",
                        "1":"Si",
                    },
                    "render": function ( data, type, row ) {
                        var enabled = 'No';
                        if (data) {
                            enabled = 'Si';
                        }
                        return enabled;
                    }
                },
                {
                    "className": "row-control",
                    "data": "payment_link_mercadopago",
                    "type": "options",
                    "options": {
                        "0":"No",
                        "1":"Si",
                    },
                    "render": function ( data, type, row ) {
                        var enabled = 'No';
                        if (data) {
                            enabled = 'Si';
                        }
                        return enabled;
                    }
                },
                {
                    "className": "row-control",
                    "data": "payment_link_cuentadigital",
                    "type": "options",
                    "options": {
                        "0":"No",
                        "1":"Si",
                    },
                    "render": function ( data, type, row ) {
                        var enabled = 'No';
                        if (data) {
                            enabled = 'Si';
                        }
                        return enabled;
                    }
                },
            ],
		    "columnDefs": [
		        { "type": 'date-custom', targets: [5] },
		        { "type": 'numeric-comma', targets: [] },
                {
                    "targets": [],
                    "width": '8%'
                },
                { 
                    "class": "left", targets: []
                },
                { 
                    "visible": false, targets: hide_columns
                },
            ],
             "createdRow" : function( row, data, index ) {
                row.id = data.id;
                $.each(ids_selected, function( index, value ) {
                    if (row.id == value) {
                        $(row).addClass('selected');
                        $(row).find('input:checkbox').attr('checked', 'checked')
                    }
                });
            },
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
        	"dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
		});

		$('#table-sms_wrapper .tools').append($('#btns-tools').contents());

		$('#btns-tools').show();
		
		$('#table-sms').on( 'init.dt', function () {

		    createModalHideColumn(table_sms, '.modal-hide-columns-customers-mass-emails');
		    createModalSearchColumn(table_sms, '.modal-search-columns-customers-mass-emails');

		    $( "#table-sms tbody" ).on( "click", ".checkbox-select", function() {

                if ($(this).is(':checked')) {
                    $(this).closest('tr').addClass('selected');
                    ids_selected.push(table_sms.row( $(this).closest('tr') ).data().id);
                } else {
                    $(this).closest('tr').removeClass('selected');
                    ids_selected.splice($.inArray(table_sms.row( $(this).closest('tr') ).data().id, ids_selected), 1);
                }
            });

            $("#table-sms tbody").on( "click", ".row-control", function() {

                if (!$(this).find('.dataTables_empty').length) {
                    sms_selected = table_sms.row( this ).data();
                    $('.modal-sms').modal('show');
                }
            });
		});

		$('.btn-selected-all').click(function() {
            $('.checkbox-select:checkbox').prop('checked', false);
            $('.checkbox-select').click();
            ids_selected = [];
            table_sms.rows().eq(0).each( function ( idx ) {
                var row = table_sms.row( idx );
                if ($(row.node()).hasClass('selected')) {
                    ids_selected.push(table_sms.row( idx ).data().id);
                }
            });
        });

        $('.btn-clear-selected-all').click(function() {
            $('.checkbox-select:checkbox').prop('checked', true);
            $('.checkbox-select').click();
            ids_selected = [];
        });

        $('#btn-go-customer').click(function() {
            var action = '/ispbrain/customers/view/' + sms_selected.customer.code;
            window.open(action, '_blank');
        });

        $(".btn-export").click(function() {
            bootbox.confirm('Se descargará un archivo Excel', function(result) {
                if (result) {
                    $('#table-sms').tableExport({tableName: 'Correos', type:'excel', escape:'false', columnNumber: [4]});
                }
            }).find("div.modal-content").addClass("confirm-width-md");
        });

        $(".btn-archive").click(function() {

            if (ids_selected.length > 0) {

                $('.confirm-send-modal').modal('show');

            } else {
                bootbox.alert('Debe seleccionar al menos 1 correo para continuar.');
            }
        });

        $('.btn-confirm-send').click(function() {

            var data = {
                ids: ids_selected,
                redirect: $(this).attr("value")
            };

            $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/MassEmails/archive_messages', 'method': 'post', 'id': 'form-block'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'data', 'value': JSON.stringify(data)}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#form-block').submit();

            $('.confirm-send-modal').modal('hide');
        });

        $(document).on("click", ".btn-resend", function(e) {

            if (ids_selected.length > 0) {

                $('.confirm-resend-modal').modal('show');

            } else {
                bootbox.alert('Debe seleccionar al menos 1 correo para continuar.');
            }
        });

        $('.btn-confirm-resend').click(function() {

            var data = {
                ids: ids_selected,
                redirect: $(this).attr("value")
            };

            $('body')
                .append( $('<form/>').attr({'action': '/ispbrain/MassEmails/selectCustomersResend', 'method': 'post', 'id': 'form-block'})
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'data', 'value': JSON.stringify(data)}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': 'hash', 'value': hash}))
                .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                .find('#form-block').submit();

            table_sms.ajax.reload();
            $('.confirm-resend-modal').modal('hide');
        });

    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
