<style type="text/css">

    legend {
        display: block;
        width: 100%;
        padding: 0;
        margin-bottom: 20px;
        font-size: 21px;
        line-height: inherit;
        color: #333;
        border: 0;
        border-bottom: 1px solid #e5e5e5;
    }

</style>

<?= $this->Form->create(null, ['url' => ['controller' => 'MassEmails' , 'action' => 'addEmail']]) ?>
<fieldset>
    <div class="row">
        <div class="col-md-12">
            <legend class="sub-title"><?= __('Datos de Cuenta') ?></legend>
        </div>

        <div class="col-md-4">
            <?php

                $required = FALSE;
                $class = "d-none";

                $user = "";
                $class_name = "";
                $pass = "";
                $host = "";
                $port = "";
                $timeout = "";
                $client = "";
                $tls = "";

                if ($paraments->emails_accounts->provider == "") {

                    $required = TRUE;
                    $class = "";

                    $user = "Usuario";
                    $class_name = "Class Name";
                    $pass = "Clave";
                    $host = "Host";
                    $port = "Puerto";
                    $timeout = "Timeout";
                    $client = "Client";
                    $tls = "TLS";
                }
            ?>
            <?= $this->Form->input('contact', ['label' => 'Contacto', 'type' => 'email', 'required' => TRUE]) ?>
            <?= $this->Form->input('copy', ['label' => 'Copia', 'type' => 'email']) ?>
            <?= $this->Form->input('user', ['label' => $user, 'type' => 'text', 'required' => $required, 'class' => [$class]]) ?>
            <?= $this->Form->input('class_name', ['label' => $class_name, 'type' => 'text', 'required' => $required, 'class' => [$class]]) ?>
        </div>

        <div class="col-md-4">
            <?= $this->Form->input('pass', ['label' => $pass, 'type' => 'password', 'required' => $required, 'class' => [$class]]) ?>
            <?= $this->Form->input('host', ['label' => $host, 'type' => 'text', 'required' => $required, 'class' => [$class]]) ?>
            <?= $this->Form->input('port', ['label' => $port, 'type' => 'number', 'required' => $required, 'class' => [$class]]) ?>
        </div>

        <div class="col-md-4">
            <?= $this->Form->input('timeout', ['label' => $timeout, 'type' => 'number', 'required' => $required, 'class' => [$class]]) ?>
            <?= $this->Form->input('client', ['label' => $client, 'type' => 'text', 'class' => [$class]]) ?>
            <?= $this->Form->input('tls', ['label' => $tls, 'type' => 'text', 'class' => [$class]]) ?>
        </div>

        <div class="col-md-4">
            <?= $this->Form->input('enabled', ['label' => 'Habilitar', 'type' => 'checkbox']) ?>
        </div>

    </div>
</fieldset>
<?= $this->Form->button(__('Guardar'), ['class' => 'btn-success' ]) ?>
<?= $this->Form->end() ?>

<script type="text/javascript">

    var parament;

    $(document).ready(function () {
        paraments = <?= json_encode($paraments) ?>;
    });

</script>