<style type="text/css">

    .my-hidden {
        display: none;
    }

</style>

<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5">

        <?php

            echo $this->Html->link(
                '<span class="glyphicon fas fa-search" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Buscar por Columnas',
                'class' => 'btn btn-default btn-search-column ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-eye" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Ocultar o Mostrar Columnas',
                'class' => 'btn btn-default btn-hide-column ml-2',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar en formato excel el contenido de la Tabla',
                'class' => 'btn btn-default btn-export ml-2',
                'escape' => false
            ]);
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-hover" id="table-sms" >
            <thead>
                <tr>
                    <th><?= ('Plantilla') ?></th>      <!--0-->
                    <th><?= ('Blq.') ?></th>           <!--1-->
                    <th><?= ('Mensaje') ?></th>        <!--2-->
                    <th><?= ('Asunto') ?></th>         <!--3-->
                    <th><?= ('Estado') ?></th>         <!--4-->
                    <th><?= ('Envio') ?></th>          <!--5-->
                    <th><?= ('Cliente') ?></th>        <!--6-->
                    <th><?= ('Doc.') ?></th>           <!--7-->
                    <th><?= ('Cód.') ?></th>           <!--8-->
                    <th><?= ('Correo Electr.') ?></th> <!--9-->
                    <th><?= ('Emp.') ?></th>           <!--10-->
                    <th><?= ('Usuario') ?></th>        <!--11-->
                    <th><?= ('Adj. Fact.') ?></th>     <!--12-->
                    <th><?= ('Impagas') ?></th>        <!--13-->
                    <th><?= ('Prdo Fact.') ?></th>     <!--14-->
                    <th><?= ('Adj. Rcbo.') ?></th>     <!--15-->
                    <th><?= ('Prdo Rcbo') ?></th>      <!--16-->
                    <th><?= ('Cta') ?></th>            <!--17-->
                    <th><?= ('Adj. Res. Cta') ?></th>  <!--18-->
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-go-customer',
        'name' =>  'Ficha del Cliente',
        'icon' =>  'glyphicon icon-profile',
        'type' =>  'btn-primary'
    ];

    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-customers-mass-emails']);
    echo $this->element('actions', ['modal'=> 'modal-sms', 'title' => 'Acciones', 'buttons' => $buttons ]);
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-customers-mass-emails']);
?>

<script type="text/javascript">

    var table_sms = null;
    var sms_selected = null;
    var emails_selected_ids = null;
    var users = null;
    var templates = null;
    var account_emails = null;

    $(document).ready(function () {

        users = <?= json_encode($users) ?>;
        templates = <?= json_encode($templates) ?>;
        account_emails = <?= json_encode($account_emails) ?>;

        $('.btn-hide-column').click(function() {
            $('.modal-hide-columns-customers-mass-emails').modal('show');
        });

        $('.btn-search-column').click(function() {
           $('.modal-search-columns-customers-mass-emails').modal('show');
        });

        $('#table-sms').removeClass('display');

        $('#btns-tools').hide();

		table_sms = $('#table-sms').DataTable({
		     "order": [[ 1, 'desc' ]],
            "deferRender": true,
            "processing": true,
            "serverSide": true,
		    "ajax": {
                "url": "/ispbrain/MassEmails/getMessages.json",
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "archived": true
                    });
                },
            	"error": function(c) {
            		switch (c.status) {
            			case 400:
            				flag = false;
            				generateNoty('warning', 'Ha ocurrido un error.');
            				break;
            			case 401:
            				flag = false;
            				generateNoty('warning', 'Error, no posee una autorización.');
            				break;
            			case 403:
            				flag = false;
            				generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

            				setTimeout(function() { 
                                window.location.href = "/ispbrain";
            				}, 3000);
            				break;
            		}
            	}
            },
            "drawCallback": function( settings ) {
                var flag = true;
            	switch (settings.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
            	    if (table_sms) {

                        var tableinfo = table_sms.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }
                    }
            	}
            },
		    "scrollY": true,
		    "scrollY": '450px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
		    "columns": [
                { 
                    "className": "",
                    "data": "mass_emails_block.template_name",
                    "type": "options",
                    "options": templates,
                    "render": function ( data, type, row ) {
                        return  data;
                    }
                },
                { 
                    "className": "",
                    "data": "mass_emails_block.id",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return  data;
                    }
                },
                { 
                    "className": "",
                    "data": "message",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        return  data;
                    }
                },
                { 
                    "className": "",
                    "data": "subject",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        return  data;
                    }
                },
                { 
                    "className": "",
                    "data": "status",
                    "type": "options",
                    "options": {
                        'Enviado': 'Enviado',
                        'No enviado': 'No enviado',
                    },
                    "render": function ( data, type, row ) {
                        return  data;
                    }
                },
                {
                    "className": " text-info",
                    "data": "mass_emails_block.created",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var created = data.split('T')[0];
                            var hours = data.split('T')[1].split('-')[0].split(':');
                            created = created.split('-');
                            return created[2] + '/' + created[1] + '/' + created[0] + ' ' + hours[0] + ':' + hours[1];
                        }
                    }
                },
                { 
                    "className": "",
                    "data": "customer.name",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        return  data;
                    }
                },
                { 
                    "className": " left",
                    "data": "customer.ident",
                    "type": "ident",
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (data) {
                            ident = data;
                        }
                        return sessionPHP.afip_codes.doc_types[row.customer.doc_type] + ' ' + ident;
                    }
                },
                { 
                    "className": "",
                    "data": "customer.code",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                {
                    "className": "",
                    "data": "email",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        var email = row.customer.hasOwnProperty('email') ? row.customer.email : '';
                        if (data) {
                            email = data;
                        }
                        return email;
                    }
                },
                {
                    "className": "",
                    "data": "business_billing",
                    "type": "options_obj",
                    "options": sessionPHP.paraments.invoicing.business,
                    "render": function ( data, type, row ) {
                        var business_name = '';
                        $.each(sessionPHP.paraments.invoicing.business, function (i, b) {
                            if (b.id == data) {
                                business_name = b.name + ' (' + b.address + ')';
                            }
                        });
                        return business_name;
                    }
                },
                { 
                    "className": "",
                    "data": "mass_emails_block.user_id",
                    "type": "options",
                    "options": users,
                    "render": function ( data, type, row ) {
                        return users[data];
                    }
                },
                { 
                    "data": "invoice_attach",
                    "type": "options",
                    "options": {
                        "0":"No",
                        "1":"Si",
                    },
                    "render": function ( data, type, row ) {
                        var enabled = 'No';
                        if (data) {
                            enabled = 'Si';
                        }
                        return enabled;
                    }
                },
                { 
                    "data": "invoice_impagas",
                    "type": "options",
                    "options": {
                        "0":"No",
                        "1":"Si",
                    },
                    "render": function ( data, type, row ) {
                        var enabled = 'No';
                        if (data) {
                            enabled = 'Si';
                        }
                        return enabled;
                    }
                },
                {
                    "className": " text-info",
                    "data": "invoice_periode",
                    "type": "periode",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var data = data.split('-');
                            return data[1] + '/' + data[0];
                        }
                        return data;
                    }
                },
                { 
                    "data": "receipt_attach",
                    "type": "options",
                    "options": {
                        "0":"No",
                        "1":"Si",
                    },
                    "render": function ( data, type, row ) {
                        var enabled = 'No';
                        if (data) {
                            enabled = 'Si';
                        }
                        return enabled;
                    }
                },
                {
                    "className": " text-info",
                    "data": "receipt_periode",
                    "type": "periode",
                    "render": function ( data, type, row ) {
                        if (data) {
                            var data = data.split('-');
                            return data[1] + '/' + data[0];
                        }
                        return data;
                    }
                },
                {
                    "className": "",
                    "data": "mass_emails_block.email_account",
                    "type": "options",
                    "options": account_emails,
                    "render": function ( data, type, row ) {
                        var email_account = "";
                        if (data) {
                            email_account = data;
                        }
                        return email_account;
                    }
                },
                {
                    "className": "",
                    "data": "account_summary_attach",
                    "type": "options",
                    "options": {
                        "0":"No",
                        "1":"Si",
                    },
                    "render": function ( data, type, row ) {
                        var enabled = 'No';
                        if (data) {
                            enabled = 'Si';
                        }
                        return enabled;
                    }
                },
            ],
		    "columnDefs": [
		        { "type": 'date-custom', targets: [5] },
		        { "type": 'numeric-comma', targets: [] },
                {
                    "targets": [],
                    "width": '8%'
                },
                { 
                    "class": "left", targets: []
                },
                { 
                    "visible": false, targets: hide_columns
                },
            ],
             "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
		    "language": dataTable_lenguage,
            "pagingType": "numbers",
            "lengthMenu": [[100, 500, 1000, -1], [100, 500, 1000, "Todas"]],
            
        	"dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'p>>",
   
		});

		$('#table-sms_wrapper .tools').append($('#btns-tools').contents());

		$('#btns-tools').show();

        $('#table-sms').on( 'init.dt', function () {

            createModalHideColumn(table_sms, '.modal-hide-columns-customers-mass-emails');
		    createModalSearchColumn(table_sms, '.modal-search-columns-customers-mass-emails');

            $('#table-sms tbody').on( 'click', 'tr', function (e) {

                if (!$(this).find('.dataTables_empty').length) {

                    table_sms.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    sms_selected = table_sms.row( this ).data();

                    $('.modal-sms').modal('show');
                }
            });
        });

        $('#btn-go-customer').click(function() {
            var action = '/ispbrain/customers/view/' + sms_selected.customer.code;
            window.open(action, '_blank');
        });

        $(".btn-export").click(function() {
            bootbox.confirm('Se descargará un archivo Excel', function(result) {
                if (result) {
                    $('#table-sms').tableExport({tableName: 'Correos', type:'excel', escape:'false', columnNumber: [4]});
                }
            }).find("div.modal-content").addClass("confirm-width-md");
        });

    });

    // Jquery draggable modals
    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });

</script>
