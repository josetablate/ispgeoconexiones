<style type="text/css">

    .btn-export span {
        color: #fff !important;
    }

</style>

<div class="row">
    <div class="col-xl-6">

        <div class="card border-secondary mb-2">

            <div class="card-body p-2">

                <div class="row">
                    <div class="col-xl-2 pr-0">
                        <div class="text-secondary p-2">Período</div>
                    </div>
                    <div class="col-xl-5">
                        <div class="input-group mr-2">
                          <span class="input-group-addon input-group-text">DESDE</span>
                          <input type="text" id="date_from" class="form-control font-weight-bold" readonly>
                        </div>
                    </div>
                    <div class="col-xl-5">
                        <div class="input-group">
                          <span class="input-group-addon input-group-text">HASTA</span>
                          <input type="text" id="date_to" class="form-control font-weight-bold" readonly>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-xl-6 ">

        <div class="card border-secondary p-1 mb-2">

            <div class="card-body p-2 d-flex justify-content-around">

                <?php

                    echo  $this->Html->link(
                        '<span class="glyphicon icon-eye-plus" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => '',
                        'class' => 'btn btn-default btn-hide-column ml-2',
                        'escape' => false
                    ]);

                    echo  $this->Html->link(
                        '<span class="glyphicon icon-calendar" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Filtrar período',
                        'class' => 'btn btn-default btn-filter-period ml-2',
                        'data-toggle' => 'modal', 'data-target' => '#modal-filter',
                        'escape' => false
                    ]);

                    echo $this->Html->link(
                        '<span class="glyphicon fas fa-search" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Buscar por columnas',
                        'class' => 'btn btn-default btn-search-column ml-1',
                        'escape' => false
                    ]);

                    echo  $this->Html->link(
                        '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                        'javascript:void(0)',
                        [
                        'title' => 'Exportar en formato excel el contenido de la tabla',
                        'class' => 'btn btn-info btn-export ml-2',
                        'escape' => false
                    ]);
                ?>

            </div>

        </div>
    </div>
</div>

<div class="modal fade dont-fade bs-example-modal-sm" id="modal-filter" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Filtrar por Período</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <?= $this->Form->create() ?>

                    <div class="row">
                        <div class="col-md-12">

                            <div class="input-group">
                                <div class='input-group date' id='date-from-datetimepicker'>
                                    <span class="input-group-addon input-group-text mr-0">DESDE</span>
                                    <input name="from" id="date-from" type='text' class="column_search form-control datetimepicker" data-column="2" required />
                                    <span class="input-group-addon input-group-text mr-0">
                                        <span class="glyphicon icon-calendar"></span>
                                    </span>
                                </div>
                            </div>

                            <div class="input-group">
                              <div class='input-group date' id='date-to-datetimepicker'>
                                    <span class="input-group-addon input-group-text mr-0">HASTA</span>
                                    <input name="to" id="date-to" type='text' class="column_search form-control datetimepicker" data-column="3" required />
                                    <span class="input-group-addon input-group-text mr-0">
                                        <span class="glyphicon icon-calendar"></span>
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div>

                <?= $this->Form->end() ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-clear-filter" >Limpiar</button>
                <button type="button" class="btn btn-primary btn-periode-search">Buscar</button>
            </div>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-12">
       <table class="table table-bordered table-hover" id="table-debts">
            <thead>
                <tr>
                    <th>Estado</th>         <!--0-->
                    <th>Fecha</th>          <!--1-->
                    <th>Desde</th>          <!--2-->
                    <th>Hasta</th>          <!--3-->
                    <th>Nro Tarjeta</th>    <!--4-->
                    <th>Cliente</th>        <!--5-->
                    <th>Documento</th>      <!--6-->
                    <th>Cód.</th>           <!--7-->
                    <th>Ciudad</th>         <!--8-->
                    <th>Nro Factura</th>    <!--9-->
                    <th>Venc.</th>          <!--10-->
                    <th>Total</th>          <!--11-->
                    <th>Empresa</th>        <!--12-->
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

</div>

<div class="modal fade dont-fade bs-example-modal-sm" id="modal-confirm-accept" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Confirmar Acción: Aceptar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">

                        <div class="input-group">
                            <div class='input-group date' id='date-auto-debit-datetimepicker'>
                                <span class="input-group-addon input-group-text mr-0">Fecha</span>
                                <input name="date-auto-debit" id="date-auto-debit" type='text' class="column_search form-control datetimepicker" />
                                <span class="input-group-addon input-group-text mr-0">
                                    <span class="glyphicon icon-calendar"></span>
                                </span>
                                <small class="text-muted">Fecha de Impacto del Débito Automático</small>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="btn-confirm-accept">Confirmar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade dont-fade bs-example-modal-sm" id="modal-confirm-re-accept" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Confirmar Acción: Aceptar (edición)</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">

                        <div class="input-group">
                            <div class='input-group date' id='re-date-auto-debit-datetimepicker'>
                                <span class="input-group-addon input-group-text mr-0">Fecha</span>
                                <input name="re-date-auto-debit" id="re-date-auto-debit" type='text' class="column_search form-control datetimepicker" />
                                <span class="input-group-addon input-group-text mr-0">
                                    <span class="glyphicon icon-calendar"></span>
                                </span>
                                <small class="text-muted">Fecha de Impacto del Débito Automático</small>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="btn-confirm-re-accept">Confirmar</button>
            </div>
        </div>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-accept',
        'name' =>  'Aceptar',
        'icon' =>  'fas fa-check',
        'type' =>  'btn-success'
    ];

    $buttons[] = [
        'id'   =>  'btn-reject',
        'name' =>  'Rechazar',
        'icon' =>  'fas fa-times',
        'type' =>  'btn-danger'
    ];

    $buttons[] = [
        'id'   =>  'btn-re-accept',
        'name' =>  'Aceptar (edición)',
        'icon' =>  'fas fa-check',
        'type' =>  'btn-success'
    ];

    // $buttons[] = [
    //     'id'   =>  'btn-re-reject',
    //     'name' =>  'Rechazar (edición)',
    //     'icon' =>  'fas fa-times',
    //     'type' =>  'btn-danger'
    // ];

    $buttons[] = [
        'id'   =>  'btn-go-customer',
        'name' =>  'Ficha del Cliente',
        'icon' =>  'glyphicon icon-profile',
        'type' =>  'btn-secondary'
    ];

    echo $this->element('actions', ['modal'=> 'modal-actions-debts', 'title' => 'Acciones', 'buttons' => $buttons ]);
    echo $this->element('hide_columns', ['modal'=> 'modal-hide-columns-debts']);
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-debts']);
    echo $this->element('modal_preloader');

?>

<script type="text/javascript">

    var table_debts = null;
    var curr_pos_scroll = null;

    $(document).ready(function() {

        $('.btn-hide-column').click(function() {
            $('.modal-hide-columns-debts').modal('show');
        });

        $('.btn-search-column').click(function() {
            $('.modal-search-columns-debts').modal('show');
        });

        $('#table-debts').removeClass('display');

        $('#btns-tools').hide();

        loadPreferences('invoices-generate', [0, 1, 2, 3, 4, 5, 6, 12]);

        table_debts = $('#table-debts').DataTable({
		    "order": [[ 1, 'desc' ]],
            "deferRender": true,
            "processing": true,
            "serverSide": true,
		    "ajax": {
                "url": "/ispbrain/VisaAutoDebit/retrievesDebts.json",
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "payment_getway_id": 106
                    });
                },
                "error": function(c) {
                    switch (c.status) {
                        case 400:
                            flag = false;
                            generateNoty('warning', 'Ha ocurrido un error.');
                            break;
                        case 401:
                            flag = false;
                            generateNoty('warning', 'Error, no posee una autorización.');
                            break;
                        case 403:
                            flag = false;
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                        	setTimeout(function() { 
                                window.location.href = "/ispbrain";
                        	}, 3000);
                            break;
                    }
                }
            },
		    "drawCallback": function( settings ) {
                var flag = true;
            	switch (settings.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
            	if (flag) {
            	    if (table_debts) {

                        var tableinfo = table_debts.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }
                    }
            	}
            },
		    "scrollY": true,
		    "scrollY": '450px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
		    "columns": [
		        {
                    "data": "status",
                    "type": "options",
                    "options": {
                        "-1": "Sin procesar",
                        "1":"Aceptado",
                        "0":"Rechazado"
                    },
                    "render": function ( data, type, row ) {

                        switch (data) {
                            case -1:
                                return "Sin procesar";
                                break;
                            case 0:
                                return "Rechazado";
                                break;
                            case 1:
                                return "Aceptado";
                                break;
                        }
                    }
                },
                { 
                    "className": " row-control",
                    "data": "created",
                    "type": "date",
                    "render": function ( data, type, row ) {
                        var date = data.split('T')[0];
                        date = date.split('-');
                        return date[2] + '/' + date[1] + '/' + date[0];
                    }
                },
                { 
                    "className": " row-control",
                    "data": "date_start",
                    "render": function ( data, type, row ) {
                        var date_start = data.split('T')[0];
                        date_start = date_start.split('-');
                        return date_start[2] + '/' + date_start[1] + '/' + date_start[0];
                    }
                },
                { 
                    "className": " row-control",
                    "data": "date_end",
                    "render": function ( data, type, row ) {
                        var date_end = data.split('T')[0];
                        date_end = date_end.split('-');
                        return date_end[2] + '/' + date_end[1] + '/' + date_end[0];
                    }
                },
                { 
                    "className": " row-control left",
                    "data": "card_number",
                    "type": "string"
                },
                { 
                    "className": " row-control left",
                    "data": "customer_name",
                    "type": "string"
                },
                {
                    "orderable": false,
                    "data": "customer_ident",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        var ident = "";
                        if (row.customer_ident) {
                            ident = row.customer_ident;
                        }
                        return sessionPHP.afip_codes.doc_types[row.customer_doc_type] + ' ' + ident;
                    }
                },
                { 
                    "className": " row-control",
                    "data": "customer_code",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
                { 
                    "className": " row-control left",
                    "data": "customer_city",
                    "type": "string"
                },
                { 
                    "className": " row-control",
                    "data": "invoice_num",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return pad(data, 8);
                    }
                },
                { 
                    "className": " row-control text-info",
                    "data": "duedate",
                    "type": "date",
                    "render": function ( data, type, row ) {

                        var duedate = data.split('T')[0];
                        duedate = duedate.split('-');

                        if (type == 'display') {

                            if (row.paid == null) {

                                var date = new Date(duedate[1] + '/' + duedate[2] + '/' + duedate[0]);
                                var todayDate = new Date();

                                duedate =  duedate[2] + '/' + duedate[1] + '/' + duedate[0];

                                if (date < todayDate) {
                                    return "<span class='text-danger'>" + duedate + "</span>";
                                } else {
                                    return "<span class='text-info'>" + duedate + "</span>";
                                }
                            }
                        }

                        duedate =  duedate[2] + '/' + duedate[1] + '/' + duedate[0];
                        return duedate;
                    }
                },
                { 
                    "className": " row-control right text-info",
                    "data": "total",
                    "type": "decimal",
                    "render": $.fn.dataTable.render.number( '.', ',', 2, '' )
                },
                { 
                    "data": "business_id",
                    "type": "options_obj",
                    "options": sessionPHP.paraments.invoicing.business,
                    "render": function ( data, type, row ) {
                        var business_name = '';
                        $.each(sessionPHP.paraments.invoicing.business, function (i, b) {
                            if (b.id == data) {
                                business_name = b.name;
                            }
                        });
                        return business_name;
                    }
                }
            ],
		    "columnDefs": [
                { 
                    "type": 'date-custom',
                    "targets":  [1, 2, 3, 10]
                },
                { 
                    "type": 'numeric-comma', 
                    "targets": [11]
                }
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
		    "language": dataTable_lenguage,
            "lengthMenu": [[500, 1000, -1], [500, 1000, "Todas"]],
        	"dom":
    	    	"<'row'<'col-md-6'l><'col-md-6'f>>" +
        		"<'row'<'col-md-12'tr>>" +
        		"<'row'<'col-md-5'i><'col-md-7'p>>",
		});

        $('#table-debts_wrapper .tools').append($('#btns-tools').contents());

		$('#table-debts').on( 'init.dt', function () {

		    createModalHideColumn(table_debts, '.modal-hide-columns-debts');
            createModalSearchColumn(table_debts, '.modal-search-columns-debts');
        });

        $('#table-debts tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                debt_selected = table_debts.row( $(this).closest('tr') ).data();
                table_debts.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');

                switch (debt_selected.status) {

                    case -1:
                        $('#btn-accept').removeClass('d-none');
                        $('#btn-reject').removeClass('d-none');
                        $('#btn-re-accept').addClass('d-none');
                        //$('#btn-re-reject').addClass('d-none');
                        break;

                    case 0:
                        $('#btn-re-accept').removeClass('d-none');
                        $('#btn-accept').addClass('d-none');
                        $('#btn-reject').addClass('d-none');
                        //$('#btn-re-reject').addClass('d-none');
                        break;

                    case 1:
                        $('#btn-accept').addClass('d-none');
                        $('#btn-re-accept').addClass('d-none');
                        $('#btn-reject').addClass('d-none');
                        //$('#btn-re-reject').removeClass('d-none');
                        break;
                }

                $('.modal-actions-debts').modal('show');
            }
        });

        $('#btns-tools').show();

        $('#date-from-datetimepicker').datetimepicker({
            locale: 'es',
            format: 'DD/MM/YYYY'
        });

        $('#date-to-datetimepicker').datetimepicker({
            locale: 'es',
            format: 'DD/MM/YYYY'
        });

        $('#btn-go-customer').click(function() {
            var action = '/ispbrain/customers/view/' + debt_selected.customer_code;
            window.open(action, '_blank');
        });

        $("#date-from-datetimepicker").on("dp.change",function (e) {
            $('#date_from').val($('#date-from').val());
        });

        $("#date-to-datetimepicker").on("dp.change",function (e) {
            $('#date_to').val($('#date-to').val());
        });

        $('.btn-clear-filter').click(function() {
            table_debts.search( '' ).columns().search( '' ).draw();
            $('#date-from').val('');
            $('#date-to').val('');
            $('#date_from').val($('#date-from').val());
            $('#date_to').val($('#date-to').val());
        });

        $('.btn-periode-search').click(function() {
            if ($('#date-from').val() != '' && $('#date-to').val() != '') {
                table_debts.columns( 2 ).search( $('#date-from').val() );
                table_debts.columns( 3 ).search( $('#date-to').val() );
                table_debts.draw();
                $('#modal-filter').modal('hide');
            } else {
                generateNoty('warning', 'Debe cargar las fechas de período: Desde - Hasta para poder aplicar el filtro.');
            }
        });

        $('#modal-confirm-accept').on('shown.bs.modal', function () {
            $('#date-auto-debit').datetimepicker({
                locale: 'es',
                defaultDate: new Date(),
                format: 'DD/MM/YYYY'
            });
        });

        $('#btn-accept').click(function() {
            $('.modal-actions-debts').modal('hide');
            $('#modal-confirm-accept').modal('show');
        });

        $('#btn-confirm-accept').click(function() {

            if ($('#date-auto-debit').val() != "") {

                openModalPreloader('Procesar acción: aceptar ... ');

                var request = $.ajax({
                    url: "/ispbrain/VisaAutoDebit/acceptPayment.json",
                    method: "POST",
                    data: JSON.stringify({
                        id: debt_selected.id,
                        date: $('#date-auto-debit').val()
                    }),
                    dataType: "json"
                });

                request.done(function(response) {
                    closeModalPreloader();
                    $('#modal-confirm-accept').modal('hide');
                    generateNoty(response.response.typeMsg, response.response.msg);
                    table_debts.draw();
                });

                request.fail(function(jqXHR) {

                    closeModalPreloader();
                    $('#modal-confirm-accept').modal('hide');

                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {

                    	generateNoty('warning', 'Error al procesar acción: aceptar.');
                    }
                });
            } else {
                generateNoty('warning', 'Debe ingresar la Fecha de Impacto del Débito Automático');
            }
        })

        $('#modal-confirm-re-accept').on('shown.bs.modal', function () {
            $('#re-date-auto-debit').datetimepicker({
                locale: 'es',
                defaultDate: new Date(),
                format: 'DD/MM/YYYY'
            });
        });

        $('#btn-re-accept').click(function() {
            $('.modal-actions-debts').modal('hide');
            $('#modal-confirm-re-accept').modal('show');
        });

        $('#btn-confirm-re-accept').click(function() {
            
            if ($('#re-date-auto-debit').val() != "") {

                openModalPreloader('Procesar acción: aceptar (edición) ... ');

                var request = $.ajax({
                    url: "/ispbrain/VisaAutoDebit/editAcceptPayment.json",
                    method: "POST",
                    data: JSON.stringify({
                        id: debt_selected.id,
                        date: $('#re-date-auto-debit').val()
                    }),
                    dataType: "json"
                });

                request.done(function(response) {
                    closeModalPreloader();
                    $('#modal-confirm-re-accept').modal('hide');
                    generateNoty(response.response.typeMsg, response.response.msg);
                    table_debts.draw();
                });

                request.fail(function(jqXHR) {

                    closeModalPreloader();
                    $('#modal-confirm-re-accept').modal('hide');

                    if (jqXHR.status == 403) {

                    	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                    	setTimeout(function() { 
                            window.location.href = "/ispbrain";
                    	}, 3000);

                    } else {
                    	generateNoty('warning', 'Error al procesar acción: aceptar (edición)');
                    }
                });
            } else {
                generateNoty('warning', 'Debe ingresar la Fecha de Impacto del Débito Automático');
            }
        });

        $('#btn-reject').click(function() {

            $('.modal-actions-debts').modal('hide');

            var text = "¿Está Seguro que desea realizar la Acción: Rechazar?";

            bootbox.confirm(text, function(result) {

                if (result) {

                    openModalPreloader('Procesar acción: rechazar ... ');

                    var request = $.ajax({
                        url: "/ispbrain/VisaAutoDebit/rejectPayment.json",
                        method: "POST",
                        data: JSON.stringify({
                            id: debt_selected.id,
                        }),
                        dataType: "json"
                    });

                    request.done(function(response) {
                        closeModalPreloader();
                        $('.modal-actions-debts').modal('hide');
                        generateNoty(response.response.typeMsg, response.response.msg);
                        table_debts.draw();
                    });

                    request.fail(function(jqXHR) {

                        closeModalPreloader();

                        if (jqXHR.status == 403) {

                        	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                        	setTimeout(function() { 
                                window.location.href = "/ispbrain";
                        	}, 3000);

                        } else {
                        	generateNoty('warning', 'Error al procesar acción: rechazar.');
                        }
                    });
                }
            });
        });

        /*$('#btn-re-reject').click(function() {

            var text = "¿Está Seguro que desea realizar la Acción: Rechazar (edición)?";

            bootbox.confirm(text, function(result) {

                if (result) {

                    openModalPreloader('Procesar acción: rechazar (edición) ... ');

                    var request = $.ajax({
                        url: "/ispbrain/VisaAutoDebit/editRejectPayment.json",
                        method: "POST",
                        data: JSON.stringify({
                            id: debt_selected.id,
                        }),
                        dataType: "json"
                    });

                    request.done(function(response) {
                        $('.modal-actions-debts').modal('hide');
                        closeModalPreloader();
                        generateNoty(response.response.typeMsg, response.response.msg);
                        table_debts.draw();
                    });

                    request.fail(function(jqXHR) {

                        if (jqXHR.status == 403) {

                        	generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                        	setTimeout(function() {
                                window.location.href = "/ispbrain";
                        	}, 3000);

                        } else {
                        	generateNoty('warning', 'Error al procesar acción: rechazar (edición)');
                        }
                    });
                }
            });
        });*/

    });

</script>
