<div id="btns-tools">
    <div class="text-right btns-tools margin-bottom-5" >

        <?php

            echo $this->Html->link(
                '<span class="glyphicon fas fa-search" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Buscar por Columnas',
                'class' => 'btn btn-default btn-search-column ml-1',
                'escape' => false
            ]);

            echo $this->Html->link(
                '<span class="glyphicon icon-file-excel" aria-hidden="true"></span>',
                'javascript:void(0)',
                [
                'title' => 'Exportar tabla',
                'class' => 'btn btn-default btn-export ml-1',
                'escape' => false
            ]);

        ?>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered" id="table-accounts">
            <thead>
                <tr>
                    <th></th>             <!--0-->
                    <th>Nro Tarjeta</th>  <!--1-->
                    <th>Cliente</th>      <!--2-->
                    <th>Doc.</th>         <!--3-->
                    <th>Cód.</th>         <!--4-->
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<?php

    $buttons = [];

    $buttons[] = [
        'id'   =>  'btn-go-customer',
        'name' =>  'Ficha del Cliente',
        'icon' =>  'glyphicon icon-profile',
        'type' =>  'btn-info'
    ];

    $buttons[] = [
        'id'   =>  'btn-enable',
        'name' =>  'Haiblitar',
        'icon' =>  'fas fa-check',
        'type' =>  'btn-success'
    ];

    echo $this->element('actions', ['modal'=> 'modal-accounts', 'title' => 'Acciones', 'buttons' => $buttons ]);
    echo $this->element('search_columns', ['modal'=> 'modal-search-columns-accounts']);
    echo $this->element('modal_preloader');
?>

<script type="text/javascript">

    var table_cards = null;
    var account_selected = null;
    var curr_pos_scroll = null;

    $(document).ready(function () {

        $('.btn-search-column').click(function() {
            $('.modal-search-columns-accounts').modal('show');
        });

        $('.btn-hide-column').click(function() {
            $('.modal-hide-columns-accounts-deleted').modal('show');
        });

        $('#table-accounts').removeClass('display');

        $('#btns-tools').hide();

        $('#table-accounts').removeClass('display');

    	table_accounts = $('#table-accounts').DataTable({
    	    "order": [[ 1, 'desc' ]],
            "deferRender": true,
            "processing": true,
            "serverSide": true,
		    "ajax": {
                "url": "/ispbrain/VisaAutoDebit/retrieves.json",
                "dataFilter": function( data ) {
                    var json = $.parseJSON( data );
                    return JSON.stringify( json.response );
                },
                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "payment_getway_id": 106,
                        "deleted": 1
                    });
                },
                "error": function(c) {
                    switch (c.status) {
                        case 400:
                            flag = false;
                            generateNoty('warning', 'Ha ocurrido un error.');
                            break;
                        case 401:
                            flag = false;
                            generateNoty('warning', 'Error, no posee una autorización.');
                            break;
                        case 403:
                            flag = false;
                            generateNoty('warning', 'La sesión ha expirado. Por favor ingrese su credenciales.');

                        	setTimeout(function() { 
                                window.location.href = "/ispbrain";
                        	}, 3000);
                            break;
                    }
                }
            },
            "drawCallback": function( c ) {
                var flag = true;
            	switch (c.jqXHR.status) {
            		case 400:
            			flag = false;
            			break;
            		case 401:
            			flag = false;
            			break;
            		case 403:
            			flag = false;
            			break;
            	}
	            if (flag) {
                    if (table_accounts) {

                        var tableinfo = table_accounts.page.info();

                        if (tableinfo.recordsTotal != tableinfo.recordsDisplay) {
                            $('.btn-search-column').addClass('search-apply');
                        } else {
                            $('.btn-search-column').removeClass('search-apply');
                        }
                    }
            	}
            },
            "scrollY": true,
		    "scrollY": '430px',
		    "scrollX": true,
		    "scrollCollapse": true,
		    "paging": true,
            "columns": [
                { 
                    "data": "id",
                    "orderable": false,
                    "render": function ( data, type, row ) {
                        return "";
                    }
                },
                { 
                    "data": "card_number",
                    "type": "string"
                },
                { 
                    "data": "customer.name",
                    "type": "string"
                },
                { 
                    "data": "customer",
                    "type": "integer",
                    "render": function ( data, type, row ) {
                        return sessionPHP.afip_codes.doc_types[data.doc_type] + ' ' + data.ident;
                    }
                },
                { 
                    "data": "customer.code",
                    "type": "string",
                    "render": function ( data, type, row ) {
                        return pad(data, 5);
                    }
                },
            ],
    	    "columnDefs": [
    	        { "type": 'date-custom', targets: [] },
            ],
            "createdRow" : function( row, data, index ) {
                row.id = data.id;
            },
    	    "language": dataTable_lenguage,
    	    "pagingType": "numbers",
            "lengthMenu": [[ 50, 100, -1], [ 50, 100, "Todas"]],
            "dom":
    	    	"<'row'<'col-xl-6'l><'col-xl-3'f><'col-xl-3 tools'>>" +
        		"<'row'<'col-xl-12'tr>>" +
        		"<'row'<'col-xl-5'i><'col-xl-7'pb>>",
    	});

    	$('#table-accounts_wrapper .tools').append($('#btns-tools').contents());

    	$('#table-accounts').on( 'init.dt', function () {
            createModalSearchColumn(table_accounts, '.modal-search-columns-accounts');
        });

        $('#btns-tools').show();

        $('#table-accounts tbody').on( 'click', 'tr', function (e) {

            if (!$(this).find('.dataTables_empty').length) {

                table_accounts.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
                account_selected = table_accounts.row( this ).data();

                $('.modal-accounts').modal('show');
            }
        });

        $(".btn-export").click(function() {

            bootbox.confirm('Se descargará un archivo Excel', function(result) {
                if (result) {
                    $('#table-accounts').tableExport({tableName: 'Cuentas deshabilitadas', type:'excel', escape:'false'});
                }
            });
        });

        $('#btn-go-customer').click(function() {
            var action = '/ispbrain/customers/view/' + account_selected.customer.code;
            window.open(action, '_blank');
        });

        $('#btn-enable').click(function() {

            var text = "¿Está Seguro que desea habilitar la Cuenta?";
            var id  = account_selected.id;
            var customer_code  = account_selected.customer.code;
            var payment_getway_id = 106;

            bootbox.confirm(text, function(result) {
                if (result) {
                    $('body')
                        .append( $('<form/>').attr({'action': '/ispbrain/VisaAutoDebit/enableAccount', 'method': 'post', 'id': 'replacer'})
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'account_id', 'value': id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'customer_code', 'value': customer_code}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': 'payment_getway_id', 'value': payment_getway_id}))
                        .append( $('<input/>').attr( {'type': 'hidden', 'name': '_csrfToken', 'autocomplete': 'off', 'value': token})))
                        .find('#replacer').submit();
                }
            });
        });

        // Jquery draggable modals
        $('.modal-dialog').draggable({
            handle: ".modal-header"
        });

    });

</script>
