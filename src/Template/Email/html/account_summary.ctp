<!DOCTYPE html>
<html>
    <head>
        <title>ISPBrain - ¡Tu factura está lista!</title>
        <!--

            An email present from your friends at Litmus (@litmusapp)

            Email is surprisingly hard. While this has been thoroughly tested, your mileage may vary.
            It's highly recommended that you test using a service like Litmus (http://litmus.com) and your own devices.

            Enjoy!

         -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <style type="text/css">
            /* CLIENT-SPECIFIC STYLES */
            body, table, td, a{-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
            table, td{mso-table-lspace: 0pt; mso-table-rspace: 0pt;} /* Remove spacing between tables in Outlook 2007 and up */
            img{-ms-interpolation-mode: bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */

            /* RESET STYLES */
            img{border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;}
            table{border-collapse: collapse !important;}
            body{height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important;}

            /* iOS BLUE LINKS */
            a[x-apple-data-detectors] {
                color: inherit !important;
                text-decoration: none !important;
                font-size: inherit !important;
                font-family: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
            }

            /* MOBILE STYLES */
            @media screen and (max-width: 525px) {

                /* ALLOWS FOR FLUID TABLES */
                .wrapper {
                  width: 100% !important;
                    max-width: 100% !important;
                }

                /* ADJUSTS LAYOUT OF LOGO IMAGE */
                .logo img {
                  margin: 0 auto !important;
                }

                /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */
                .mobile-hide {
                  display: none !important;
                }

                .img-max {
                  max-width: 100% !important;
                  width: 100% !important;
                  height: auto !important;
                }

                /* FULL-WIDTH TABLES */
                .responsive-table {
                  width: 100% !important;
                }

                /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */
                .padding {
                  padding: 10px 5% 15px 5% !important;
                }

                .padding-meta {
                  padding: 30px 5% 0px 5% !important;
                  text-align: center;
                }

                .padding-copy {
                     padding: 10px 5% 10px 5% !important;
                  text-align: center;
                }

                .no-padding {
                  padding: 0 !important;
                }

                .section-padding {
                  padding: 50px 15px 50px 15px !important;
                }

                /* ADJUST BUTTONS ON MOBILE */
                .mobile-button-container {
                    margin: 0 auto;
                    width: 100% !important;
                }

                .mobile-button {
                    padding: 15px !important;
                    border: 0 !important;
                    font-size: 16px !important;
                    display: block !important;
                }
        
            }

            /* ANDROID CENTER FIX */
            div[style*="margin: 16px 0;"] { margin: 0 !important; }
        </style>
    </head>
    <body style="margin: 0 !important; padding: 0 !important;">
    
        <!-- HIDDEN PREHEADER TEXT -->
        <div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
            ¡Tu resumen de cuenta está lista!
        </div>
        
        <!-- HEADER -->
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td bgcolor="#ffffff" align="center">
                    <!--[if (gte mso 9)|(IE)]>
                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
                    <tr>
                    <td align="center" valign="top" width="500">
                    <![endif]-->
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;" class="wrapper">
                        <tr>
                            <td align="center" valign="top" style="padding: 15px 0;" class="logo">
                                <a href="http://litmus.com" target="_blank">
                                    <img alt="Logo" src="http://rec.ispbrain.io/img/logo-name-des-min.png" width="300" style="display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;" border="0">
                                </a>
                            </td>
                        </tr>
                    </table>
                    <!--[if (gte mso 9)|(IE)]>
                    </td>
                    </tr>
                    </table>
                    <![endif]-->
                </td>
            </tr>
            <tr>
                <td bgcolor="#ffffff" align="center" style="padding: 15px;">
                    <!--[if (gte mso 9)|(IE)]>
                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
                    <tr>
                    <td align="center" valign="top" width="500">
                    <![endif]-->
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;" class="responsive-table">
                        <tr>
                            <td>
                                <!-- COPY -->
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="center" style="font-size: 32px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 5px;" class="padding-copy">
                                            ¡Tu resumen de cuenta!
                                        </td>
                                    </tr>
        							<tr>
        								<td bgcolor="#ffffff" align="center" style="padding: 15px;">
        									<table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
        										<tr>
        											<td>
        												<table width="100%" border="0" cellspacing="0" cellpadding="4" style="border: 1px solid #686867; ">
        													
        													<tr>
        														<td valign="top" class="mobile-wrapper">
        															<!-- LEFT COLUMN -->
        															<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
        																<tr>
        																	<td style="padding: 0 0 5px 0;">
        																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
        																			<tr>
        																				<td align="left" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: bold;">
        																					Nombre:
        																				</td>
        																			</tr>
        																		</table>
        																	</td>
        																</tr>
        															</table>
        															<!-- RIGHT COLUMN -->
        															<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
        																<tr>
        																	<td>
        																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
        																			<tr>
        																				<td align="right" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: normal;">
        																					CAJA DE AHORRO
        																				</td>
        																			</tr>
        																		</table>
        																	</td>
        																</tr>
        															</table>
        														</td>
        													</tr>
        													
        													<tr>
        														<td valign="top" class="mobile-wrapper">
        															<!-- LEFT COLUMN -->
        															<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
        																<tr>
        																	<td style="padding: 0 0 5px 0;">
        																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
        																			<tr>
        																				<td align="left" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: bold;">
        																					Cód.:
        																				</td>
        																			</tr>
        																		</table>
        																	</td>
        																</tr>
        															</table>
        															<!-- RIGHT COLUMN -->
        															<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
        																<tr>
        																	<td>
        																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
        																			<tr>
        																				<td align="right" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: normal;">
        																					01505177/01000138598018
        																				</td>
        																			</tr>
        																		</table>
        																	</td>
        																</tr>
        															</table>
        														</td>
        													</tr>
        													
        													<tr>
        														<td valign="top" class="mobile-wrapper">
        															<!-- LEFT COLUMN -->
        															<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
        																<tr>
        																	<td style="padding: 0 0 5px 0;">
        																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
        																			<tr>
        																				<td align="left" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: bold;">
        																					DNI:
        																				</td>
        																			</tr>
        																		</table>
        																	</td>
        																</tr>
        															</table>
        															<!-- RIGHT COLUMN -->
        															<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
        																<tr>
        																	<td>
        																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
        																			<tr>
        																				<td align="right" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: normal;">
        																					0517/01138598/01
        																				</td>
        																			</tr>
        																		</table>
        																	</td>
        																</tr>
        															</table>
        														</td>
        													</tr>
        													
        													<tr>
        														<td valign="top" class="mobile-wrapper">
        															<!-- LEFT COLUMN -->
        															<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
        																<tr>
        																	<td style="padding: 0 0 5px 0;">
        																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
        																			<tr>
        																				<td align="left" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: bold;">
        																					Domicilio: 
        																				</td>
        																			</tr>
        																		</table>
        																	</td>
        																</tr>
        															</table>
        															<!-- RIGHT COLUMN -->
        															<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
        																<tr>
        																	<td>
        																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
        																			<tr>
        																				<td align="right" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: normal;">
        																					ALBERTO TABLATE AQUINO
        																				</td>
        																			</tr>
        																		</table>
        																	</td>
        																</tr>
        															</table>
        														</td>
        													</tr>
        													
        													<tr>
        														<td valign="top" class="mobile-wrapper">
        															<!-- LEFT COLUMN -->
        															<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
        																<tr>
        																	<td style="padding: 0 0 5px 0;">
        																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
        																			<tr>
        																				<td align="left" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: bold;">
        																					Tel.:  
        																				</td>
        																			</tr>
        																		</table>
        																	</td>
        																</tr>
        															</table>
        															<!-- RIGHT COLUMN -->
        															<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
        																<tr>
        																	<td>
        																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
        																			<tr>
        																				<td align="right" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: normal;">
        																					20-33732715-0
        																				</td>
        																			</tr>
        																		</table>
        																	</td>
        																</tr>
        															</table>
        														</td>
        													</tr>
        													
        													<tr>
        														<td valign="top" class="mobile-wrapper">
        															<!-- LEFT COLUMN -->
        															<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
        																<tr>
        																	<td style="padding: 0 0 5px 0;">
        																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
        																			<tr>
        																				<td align="left" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: bold;">
        																					Tipo:  
        																				</td>
        																			</tr>
        																		</table>
        																	</td>
        																</tr>
        															</table>
        															<!-- RIGHT COLUMN -->
        															<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
        																<tr>
        																	<td>
        																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
        																			<tr>
        																				<td align="right" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: normal;">
        																					20-33732715-0
        																				</td>
        																			</tr>
        																		</table>
        																	</td>
        																</tr>
        															</table>
        														</td>
        													</tr>
        												   
        												</table>
        											</td>
        										</tr>
        									</table>
        									<!--[if (gte mso 9)|(IE)]>
        									</td>
        									</tr>
        									</table>
        									<![endif]-->
        								</td>
        							</tr>
                                    <tr>
                                        <td align="left" style="padding: 20px 0 0 20px; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">
                                           A continuación describímos un resumen de tu cuenta.
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <!--[if (gte mso 9)|(IE)]>
                    </td>
                    </tr>
                    </table>
                    <![endif]-->
                </td>
            </tr>
            <tr>
                <td bgcolor="#ffffff" align="center" style="padding: 15px;" class="padding">
                    <!--[if (gte mso 9)|(IE)]>
                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
                    <tr>
                    <td align="center" valign="top" width="500">
                    <![endif]-->
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;" class="responsive-table">
        			
        				<tr>
                                        <td align="center" style="font-size: 22px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 5px;" class="padding-copy">
                                            Servicio
                                        </td>
                                    </tr>
        							<tr>
        								<td bgcolor="#ffffff" align="center" style="padding: 15px;">
        									<table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
        										<tr>
        											<td>
        												<table width="100%" border="0" cellspacing="0" cellpadding="4" style="border: 1px solid #686867; ">
        													
        													<tr>
        														<td valign="top" class="mobile-wrapper">
        															<!-- LEFT COLUMN -->
        															<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
        																<tr>
        																	<td style="padding: 0 0 5px 0;">
        																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
        																			<tr>
        																				<td align="left" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: bold;">
        																					Plan:
        																				</td>
        																			</tr>
        																		</table>
        																	</td>
        																</tr>
        															</table>
        															<!-- RIGHT COLUMN -->
        															<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
        																<tr>
        																	<td>
        																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
        																			<tr>
        																				<td align="right" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: normal;">
        																					CAJA DE AHORRO
        																				</td>
        																			</tr>
        																		</table>
        																	</td>
        																</tr>
        															</table>
        														</td>
        													</tr>
        													
        													<tr>
        														<td valign="top" class="mobile-wrapper">
        															<!-- LEFT COLUMN -->
        															<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
        																<tr>
        																	<td style="padding: 0 0 5px 0;">
        																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
        																			<tr>
        																				<td align="left" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: bold;">
        																					Domicilio.:
        																				</td>
        																			</tr>
        																		</table>
        																	</td>
        																</tr>
        															</table>
        															<!-- RIGHT COLUMN -->
        															<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
        																<tr>
        																	<td>
        																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
        																			<tr>
        																				<td align="right" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: normal;">
        																					01505177/01000138598018
        																				</td>
        																			</tr>
        																		</table>
        																	</td>
        																</tr>
        															</table>
        														</td>
        													</tr>
        													
        													<tr>
        														<td valign="top" class="mobile-wrapper">
        															<!-- LEFT COLUMN -->
        															<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
        																<tr>
        																	<td style="padding: 0 0 5px 0;">
        																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
        																			<tr>
        																				<td align="left" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: bold;">
        																					Saldo:
        																				</td>
        																			</tr>
        																		</table>
        																	</td>
        																</tr>
        															</table>
        															<!-- RIGHT COLUMN -->
        															<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
        																<tr>
        																	<td>
        																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
        																			<tr>
        																				<td align="right" style="font-family: Arial, sans-serif; color: #686867; font-size: 16px; font-weight: normal;">
        																					0517/01138598/01
        																				</td>
        																			</tr>
        																		</table>
        																	</td>
        																</tr>
        															</table>
        														</td>
        													</tr>
        												   
        												</table>
        											</td>
        										</tr>
        									</table>
        									<!--[if (gte mso 9)|(IE)]>
        									</td>
        									</tr>
        									</table>
        									<![endif]-->
        								</td>
        							</tr>
        			
                            <td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">
                            </td>
                        </tr>
                            <td>
                                <!-- TWO COLUMNS -->
                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                    <tr>
                                        <td valign="top" style="padding: 0;" class="mobile-wrapper">
                                            <!-- LEFT COLUMN -->
                                            <table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 80%;" align="left">
                                                <tr>
                                                    <td style="padding: 0 0 10px 0;">
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <tr>
                                                                <!--detalle-->
                                                                
                                                                <td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">
                                                                    Septiembre/2018
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- RIGHT COLUMN -->
                                            <table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 20%;" align="right">
                                                <tr>
                                                    <td style="padding: 0 0 10px 0;">
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <tr>
                                                                
                                                                <!--importe-->
                                                                
                                                                <td align="right" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">
                                                                    $ 2.800,00
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
        
                        <!------------------------------------------------------>
        
                        <!-------------TOTAL------------------------------------->
                        
                        <tr>
                            <td style="padding: 10px 0 0px 0; border-top: 1px solid #eaeaea; border-bottom: 1px dashed #aaaaaa;">
                                <!-- TWO COLUMNS -->
                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                    <tr>
                                        <td valign="top" class="mobile-wrapper">
                                            <!-- LEFT COLUMN -->
                                            <table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
                                                <tr>
                                                    <td style="padding: 0 0 10px 0;">
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <tr>
                                                                <td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px; font-weight: bold;">
                                                                    Saldo
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- RIGHT COLUMN -->
                                            <table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
                                                <tr>
                                                    <td>
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <tr>
                                                                <td align="right" style="font-family: Arial, sans-serif; color: #7ca230; font-size: 16px; font-weight: bold;">
                                                                    $ 5.600,00
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        
                        <!------------------------------------------------>
        				
        				<!-------------Link de pago------------------------------------->
                        
                        <tr>
                            <td style="padding: 10px 0 0px 0; border-top: 1px solid #eaeaea; border-bottom: 1px dashed #aaaaaa;">
                                <!-- TWO COLUMNS -->
                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                    <tr>
                                        <td valign="top" class="mobile-wrapper">
                                            <!-- LEFT COLUMN -->
                                            <table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
                                                <tr>
                                                    <td style="padding: 0 0 10px 0;">
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <tr>
                                                                <td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px; font-weight: bold;">
                                                                    Link de pago:
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- RIGHT COLUMN -->
                                            <table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
                                                <tr>
                                                    <td>
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <tr>
                                                                <td align="right" style="font-family: Arial, sans-serif; color: #7ca230; font-size: 16px; font-weight: bold;">
                                                                    link
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        
                        <!------------------------------------------------>
                        
                    </table>
                    <!--[if (gte mso 9)|(IE)]>
                    </td>
                    </tr>
                    </table>
                    <![endif]-->
                </td>
            </tr>
        
            <tr>
                <td bgcolor="#ffffff" align="center" style="padding: 7px;">
                    <table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <!-- COPY -->
                                        <td align="center" style="font-size: 32px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding-copy">
                                            ¿Cómo Pago?
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">
                                            El pago podés realizarlo haciendo click sobre el link de pago, te redireccionará a MercadoPago para que puedas elegir la opción de pago que te resulte más práctico.
                                        </td>
                                    </tr>
                                 
                                </table>
                            </td>
                        </tr>
                    </table>
                    <!--[if (gte mso 9)|(IE)]>
                    </td>
                    </tr>
                    </table>
                    <![endif]-->
                </td>
            </tr>
            
            <tr>
                <td bgcolor="#ffffff" align="center" style="padding: 7px;">
                    <table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <!-- COPY -->
                                        <td align="left" style="font-size: 16px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding-copy">
                                            Gracias por elegirnos. Por favor contáctanos con cualquier pregunta con respecto a tu resumen de cuenta.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style=" font-weight: bold; padding: 16px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">
                                           ISPBrain - Administración
                                        </td>
                                    </tr>
                                 
                                </table>
                            </td>
                        </tr>
                    </table>
                    <!--[if (gte mso 9)|(IE)]>
                    </td>
                    </tr>
                    </table>
                    <![endif]-->
                </td>
            </tr>
            
            
            
            <tr>
                <td bgcolor="#ffffff" align="center" style="padding: 20px 0px;">
                    <!--[if (gte mso 9)|(IE)]>
                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
                    <tr>
                    <td align="center" valign="top" width="500">
                    <![endif]-->
                    <!-- UNSUBSCRIBE COPY -->
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="max-width: 500px;" class="responsive-table">
                        <tr>
                            <td align="center" style="font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;">
                                © 2018 Todos los derechos reservados
                                <br>    
                            </td>
                        </tr>
                    </table>
                    <!--[if (gte mso 9)|(IE)]>
                    </td>
                    </tr>
                    </table>
                    <![endif]-->
                </td>
            </tr>
        </table>
    
    </body>
</html>