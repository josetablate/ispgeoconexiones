<?php

namespace App\Shell;

use Cake\Console\Shell;

use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\I18n\Time;

class SendEmailInvoicesShell extends Shell
{
    public function initialize()
    {
        parent::initialize();
    }

    public function main($invoices_ids)
    {
        Log::info('Send Invoices Emails ...', ['scope' => ['shell']]);

        $controllerClass = 'App\Controller\MassEmailsController';

        $mass_email = new $controllerClass;

        $mass_email->sendEmailPostFacturacion($invoices_ids);
    }
}
