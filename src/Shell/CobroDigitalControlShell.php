<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\File;
use Cake\I18n\Time;
use Cake\Log\Log;
use Cake\Http\Client;
use Cake\Routing\Router;

class CobroDigitalControlShell extends Shell
{
    private $payment_getway;

    public function initialize()
    {
        parent::initialize();

        $this->loadModel('PaymentGetwayParameters');
        $payemnt_getway_paraments_db = $this->PaymentGetwayParameters->find()->first();

        if ($payemnt_getway_paraments_db) {
            $payment_getway_cliente = unserialize($payemnt_getway_paraments_db->data);
            $this->payment_getway = json_decode(json_encode($payment_getway_cliente), true);
        }
    }

    private function inTime()
    {
        if (!$this->payment_getway) {
            return false;
        }

        $now = Time::now();

        $config_time = $this->payment_getway['config']['cobrodigital']['hours_execution'];

        $this->out('Config Time: ' . $config_time);

        $time_array = explode(":", $config_time);
        $config_hour = $time_array[0];
        $config_minute = $time_array[1];

        $config_now = Time::now();
        $config_now->hour($config_hour);
        $config_now->minute($config_minute);

        $this->out('Now: ' . $now);

        $result = date_diff($config_now, $now);

        $this->out('Date diff: ' . $result->h . ':' . $result->i);

        return ($result->h == 0 && $result->i == 0);
    }

    private function validateParament()
    {
        if ($this->payment_getway['config']['cobrodigital']['enabled']) {

            $count = sizeof($this->payment_getway['config']['cobrodigital']['credentials']);

            if ($count > 0) {

                if ($this->inTime()) {
                    return TRUE;
                } else {
                    Log::info('No está en horario.', ['scope' => ['cobro_digital']]);
                }

             } else {
                 Log::info('Credenciales vacias.', ['scope' => ['cobro_digital']]);
            }

        } else {
            Log::info('CobroDigital está deshabilitado.', ['scope' => ['cobro_digital']]);
        }
        return FALSE;
    }

    public function main()
    {
        if (!$this->payment_getway) {
            return FALSE;
        }

        Log::info('CobroDigital Control ...', ['scope' => ['shell']]);

        if ($this->validateParament() || $this->params['force']) {

            Log::info('validate Parament ok ...', ['scope' => ['shell']]);

            $controllerClass = 'App\Controller\CobroDigitalController';

            $cobrodigital = new $controllerClass;

            $data = json_decode($cobrodigital->ticketsControl());

            if ($data->code == 200) {
                Log::info('Completado. ' . $data->message, ['scope' => ['cobro_digital']]);
            } else {
                Log::error('Código: ' . $data->code . ' - Mensaje: ' . $data->message, ['scope' => ['cobro_digital']]);
                Log::error($data, ['scope' => ['cobro_digital']]);
            }

            return TRUE;
        }
    }

    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        $parser->addOption('force', [
            'short' => 'f',
            'help' => 'force in time ticket control from cobrodigital.',
            'boolean' => TRUE,
        ]);

        return $parser;
    }
}