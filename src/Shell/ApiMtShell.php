<?php

namespace App\Shell;

use Cake\Console\Shell;

class ApiMtShell extends Shell
{
    public function main()
    {
        $this->out('Hello world.');
        
        $command = escapeshellcmd("python3 " . WWW_ROOT . "api_mt_python_3/api.py 1.1.1.1 admin 123456");
        $output = shell_exec($command);
        $this->out($output);

    }
}