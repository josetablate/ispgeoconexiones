<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

use Cake\I18n\Time;

class WebServiceAFIPCSRShell extends Shell
{
    public function initialize()
    {
        parent::initialize();
    }
    
    public function main()
    {
        $this->hr();
        $this->out('Hello world.');
        $this->hr();
    }

    public function generate($business)
    {
        $company_name = str_replace('-', '', $business->name);
        $company_ident = str_replace('-', '', $business->cuit);

        shell_exec("openssl genrsa -out wsfe/$company_ident.key 2048");
        shell_exec("openssl req -new -key wsfe/$company_ident.key -subj '/C=AR/O=$company_name/CN=ISPBrain/serialNumber=CUIT $company_ident' -out wsfe/$company_ident.csr");

        shell_exec("tar -zcvf wsfe/$company_ident-certificado.tar.gz wsfe/$company_ident.key wsfe/$company_ident.csr");
    }
}