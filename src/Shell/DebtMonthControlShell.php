<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\File;
use Cake\I18n\Time;
use Cake\Log\Log;
use Cake\Http\Client;

class DebtMonthControlShell extends Shell
{
    private $paraments;

    public function initialize()
    {
        parent::initialize();

        $this->loadModel('GeneralParameters');
        $general_paraments_db = $this->GeneralParameters->find()->first();

        if ($general_paraments_db) {
            $parament_cliente = unserialize($general_paraments_db->data);
            $this->paraments = json_decode(json_encode($parament_cliente), true);
        }
    }

    public function main($verify_payment_commitment = FALSE)
    {
        if (!$this->paraments) {
            return false;
        }

        Log::info('Debt Month Control ...', ['scope' => ['shell']]);

        $controllerClass = 'App\Controller\DebtsController';

        $debts = new $controllerClass;

        $data = json_decode($debts->debtMonthControl($verify_payment_commitment));

        Log::info('Completed. ', ['scope' => ['debtMonthControl']]);

        return true;
    }
}