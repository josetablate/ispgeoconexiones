<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

use Cake\I18n\Time;
use Cake\Log\Log;

class DownloadShell extends Shell
{

    
    public function initialize()
    {
        parent::initialize();
    }
    
    public function main()
    {
        $this->hr();
        $this->out('Hello world.');
        $this->hr();
        
    }

    public function download($unix)
    {
        $cmd = "cd ".WWW_ROOT."temp_files_downloand/$unix &&  tar -zcvf  ../$unix.tar.gz .";
        
        // Log::info($cmd, ['scope' => ['shell']]);
        
        shell_exec($cmd);
        
        shell_exec("cd ".WWW_ROOT." &&  rm -R temp_files_downloand/$unix");
        
    }
    
}