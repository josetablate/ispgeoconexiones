<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\File;
use Cake\I18n\Time;
use Cake\Log\Log;
use Cake\Http\Client;
use Cake\Routing\Router;

class TodoPagoControlShell extends Shell
{
    private $paraments;

    public function initialize()
    {
        parent::initialize();
        
        $this->loadModel('GeneralParameters');
        $general_paraments_db = $this->GeneralParameters->find()->first();
        
        if($general_paraments_db){
            $parament_cliente = unserialize($general_paraments_db->data);
            $this->paraments = $parament_cliente;
        }
    }

    private function inTime()
    {
        $now = Time::now();

        $config_time = $this->paraments['payment_getway']['todopago']['hours_execution'];

        $this->out('Config Time: ' . $config_time);

        $time_array = explode(":", $config_time);
        $config_hour = $time_array[0];
        $config_minute = $time_array[1];

        $config_now = Time::now();
        $config_now->hour($config_hour);
        $config_now->minute($config_minute);

        $this->out('Now: ' . $now);

        $result = date_diff($config_now, $now);

        $this->out('Date diff: ' . $result->h . ':' . $result->i);

        return ($result->h == 0 && $result->i == 0);
    }

    private function validateParament()
    {
        if ($this->paraments['payment_getway']['todopago']['enabled']) {
            
            $modo = $this->paraments['payment_getway']['todopago']['MODO'];

            if ($this->paraments['payment_getway']['todopago'][$modo]['merchant_id'] != '' &&  $this->paraments['payment_getway']['todopago'][$modo]['api_keys'] != '') {

                if ($this->inTime()) {
                    return true;
                } else {
                    Log::info('No esta en horario.', ['scope' => ['TodoPago']]);
                }
                 
            }else{
                Log::info('Credenciales vacias.', ['scope' => ['TodoPago']]);
            }
           
        } else {
            Log::info('TodoPago está deshabilitado.', ['scope' => ['TodoPago']]);
        }
        return false;
    }

    public function main()
    {
        if(!$this->paraments){
            return false;
        }
        
        Log::info('TodoPago Control ...', ['scope' => ['shell']]);

        if ($this->validateParament() || $this->params['force']) {

            Log::info('validate Parament ok ...', ['scope' => ['shell']]);

            $controllerClass = 'App\Controller\TodoPagoController';

            $todopago = new $controllerClass;

            $data = json_decode($todopago->operationsControl());

            if ($data->code == 200) {
                $this->out(print_r($data));
                Log::info('Completado. ' . $data->message, ['scope' => ['Defaulter']]);
            } else {
                Log::error('Código: ' . $data->code . ' - Mensaje: ' . $data->message, ['scope' => ['Defaulter']]);
                Log::error($data, ['scope' => ['cobro_digital']]);
            }

            return true;
        }
    }

    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        $parser->addOption('force', [
            'short' => 'f',
            'help' => 'force in time control ticket from todopago.',
            'boolean' => true,
        ]);

        return $parser;
    }
}