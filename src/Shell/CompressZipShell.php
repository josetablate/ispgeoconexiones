<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

use Cake\I18n\Time;
use Cake\Log\Log;

class CompressZipShell extends Shell
{
    public function initialize()
    {
        parent::initialize();
    }

    public function main()
    {
        $this->hr();
        $this->out('Hello world.');
        $this->hr();
    }

    public function compress($filename, $zipname)
    {
        $cmd = "cd " . WWW_ROOT . "temp_files_downloand/ && zip $zipname $filename";

        shell_exec($cmd);

        shell_exec("cd " . WWW_ROOT . " &&  rm -R temp_files_downloand/$filename");
    }
}
