<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\File;
use Cake\I18n\Time;
use Cake\Log\Log;
use Cake\Http\Client;
use Cake\Routing\Router;

class PaymentCommitmentControlShell extends Shell
{
    private $paraments;

    public function initialize()
    {
        parent::initialize();

        $this->loadModel('GeneralParameters');
        $general_paraments_db = $this->GeneralParameters->find()->first();

        if ($general_paraments_db) {
            $parament_cliente = unserialize($general_paraments_db->data);
            $this->paraments = json_decode(json_encode($parament_cliente), true);
        }
    }

    private function inTime()
    {
        if (!$this->paraments) {
            return false;
        }

        $now = Time::now();

        $config_time = $this->paraments['customer']['payment_commitment'];

        $this->out('Config Time: ' . $config_time);

        $time_array = explode(":", $config_time);
        $config_hour = $time_array[0];
        $config_minute = $time_array[1];

        $config_now = Time::now();
        $config_now->hour($config_hour);
        $config_now->minute($config_minute);

        $this->out('Now: ' . $now);

        $result = date_diff($config_now, $now);

        $this->out('Date diff: ' . $result->h . ':' . $result->i);

        return ($result->h == 0 && $result->i == 0);
    }

    public function main()
    {
        if (!$this->paraments) {
            return false;
        }

        Log::info('Compromiso de Pago Control de Pagos ...', ['scope' => ['shell']]);

        if ($this->inTime() || $this->params['force']) {

            Log::info('validate Parament ok ...', ['scope' => ['shell']]);

            $controllerClass = 'App\Controller\PaymentCommitmentController';

            $payemnt_commitment = new $controllerClass;

            $data = json_decode($payemnt_commitment->paymentCommitmentControl());

            if ($data->code == 200) {
                Log::info('Mensaje: ' . $data->message . ' - Compromiso de pago completados: ' . $data->counter, ['scope' => ['PaymentCommitment']]);
            } else {
                Log::error('Código: ' . $data->code . ' - Mensaje: ' . $data->message, ['scope' => ['PaymentCommitment']]);
                Log::error($data, ['scope' => ['PaymentCommitment']]);
            }

            return true;
        }
    }

    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        $parser->addOption('force', [
            'short' => 'f',
            'help' => 'force in time Compromiso de Pago control de PaymentCommitment.',
            'boolean' => true,
        ]);

        return $parser;
    }
}