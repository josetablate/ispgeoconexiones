<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\Log\Log;
use App\Controller\IspControllerMikrotikPppoeTaController;
use App\Controller\IspControllerMikrotikDhcpTaController;
use App\Controller\IspControllerMikrotikIpfijaTaController;


class DiffControllersShell extends Shell
{
    private $ispControllerMikrotikPppoeTaController;
    private $ispControllerMikrotikDhcpTaController;

    public function initialize()
    {
        parent::initialize();
     
        $this->ispControllerMikrotikPppoeTaController =  new IspControllerMikrotikPppoeTaController();
        $this->ispControllerMikrotikDhcpTaController =  new IspControllerMikrotikDhcpTaController();
        $this->ispControllerMikrotikIpfijaTaController =  new IspControllerMikrotikIpfijaTaController();
    }

    public function main()
    {
        
        Log::info('check Sync ...', ['scope' => ['shell']]);
        
        $this->loadModel('Controllers');
        $controllers = $this->Controllers->find()->where(['deleted' => false, 'enabled' => true]);
        
        $controllers_unsynchronize = [];
         
        foreach($controllers as $controller){
             
            switch($controller->template){
          
                case'MikrotikPppoeTa':
                    $count = $this->ispControllerMikrotikPppoeTaController->checkSync($controller->id);
                    
                     if($count > 0){
                         Log::error($controller->name . ' (MikrotikPppoeTa)  cont: ' . $count, ['scope' => ['sync_controllers']]);
                     }else{
                         Log::info($controller->name . ' (MikrotikPppoeTa)  cont: ' . $count, ['scope' => ['sync_controllers']]);
                     }
                        
                    break;
               case'MikrotikDhcpTa':
                   
                    $count = $this->ispControllerMikrotikDhcpTaController->checkSync($controller->id);
                    
                    if($count > 0){
                        Log::error($controller->name . ' (MikrotikDhcpTa)  cont: ' . $count, ['scope' => ['sync_controllers']]);
                    }else{
                        Log::info($controller->name . ' (MikrotikDhcpTa)  cont: ' . $count, ['scope' => ['sync_controllers']]);
                    }
                       
                    break;
                case'MikrotikIpfijaTa':
                    $count = $this->ispControllerMikrotikIpfijaTaController->checkSync($controller->id);
                    
                    if($count > 0){
                        Log::error($controller->name . ' (MikrotikIpfijaTa)  cont: ' . $count, ['scope' => ['sync_controllers']]); 
                    }else{
                        Log::info($controller->name . ' (MikrotikIpfijaTa)  cont: ' . $count, ['scope' => ['sync_controllers']]); 
                    }
                    
                    
                       
                    break;
                 
             }
             
         }
         
         Log::info('check Sync Terminated', ['scope' => ['shell']]);
        
  
    }
}