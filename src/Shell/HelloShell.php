<?php

namespace App\Shell;

use Cake\Console\Shell;

class HelloShell extends Shell
{
    public function main()
    {
        $this->out('Hello world.');
        
        
        $color = $this->in('What color do you like?');
        
        // Get a choice from the user.
        
        $selection = $this->in('Red or Green?', ['R', 'G'], 'R');
    }
}