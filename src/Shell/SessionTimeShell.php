<?php
namespace App\Shell;

use Cake\Console\Shell;

/**
 * SessionTime shell command.
 */
class SessionTimeShell extends Shell
{

    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
    }

    public function change($session_time_before, $session_time_new)
    {
        shell_exec("sed -i '3s/$session_time_before/$session_time_new/' ../config/session.php");
    }
}
