<?php

namespace App\Shell;

use Cake\Console\Shell;

use PEAR2\Net\RouterOS;
use PEAR2\Net\RouterOS\SocketException;
use PEAR2\Net\RouterOS\DataFlowException;
use PEAR2\Net\RouterOS\Response;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

use Cake\I18n\Time;

class TrafficAccountingShell extends Shell
{
    private $_client;
    private $_util;
    private $paraments;
    private $_trafficAccountingRecord;
    
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Users');
        $this->_client = null;
        $this->_util = null;
        
        $this->loadModel('GeneralParameters');
        $general_paraments_db = $this->GeneralParameters->find()->first();
        
        if ($general_paraments_db) {
            $parament_cliente = unserialize($general_paraments_db->data);
            $this->paraments = $parament_cliente;
        }
        
    }
    
    public function main()
    {
        if (!$this->paraments) {
            return false;
        }
        
        $this->takeSnapshot();
        
    }

    private function connect($controller, $timeout = 5){
      
        if(!$controller->enabled){
            $this->abort(__('Controlador desabilitado'));
            return false;
        }
        
        if(!$this->_util){
            
            try{
                
                $this->_client = new RouterOS\Client(
                $controller->ip,
                $controller->username,
                $controller->password,
                $controller->port,
                false, $timeout);
                
            }catch(SocketException $e){
                
                if($e->getCode() == SocketException::CODE_CONNECTION_FAIL){
                    $this->abort(__('No se pudo conectar al controlador: {0}', $controller->name));
                    return false;
                }
            }catch(DataFlowException $e){
                
                if($e->getCode() == DataFlowException::CODE_INVALID_CREDENTIALS ){
                    $this->abort(__('Credenciales incorrectas, controlador: '. $controller->name));
                    return false;
                }
            }
            
            if(!$this->_client) return false;
            $this->_util = new RouterOS\Util($this->_client);
             
        }
        
        return true;
        
    }
    
    private function getTakeSnapshotTrafficAccounting($controller){
        
        $array = [];   
        
        if(!$this->connect($controller, 5)){
           return [];
        }
        
        $this->out(__('Connection to Controller: {0}', $controller->name));
        
        $this->_util->setMenu('/ip accounting snapshot');
        $this->_util->exec('take');
        
        foreach($this->_util->getAll() as $account){
            
            $src_user =  $account->getProperty('src-user');
            $dst_user =  $account->getProperty('dst-user');
            $src_address =  ip2long($account->getProperty('src-address'));
            $dst_address =  ip2long($account->getProperty('dst-address'));
            $packets =  intval($account->getProperty('packets'));
            $bytes =  intval($account->getProperty('bytes'));
            
           if($src_user){
               
               if(!array_key_exists($src_address,  $array)){
                   $array[$src_address] = [
                       'ip_address' => $src_address,
                       'packets_up' => 0,
                       'bytes_up' => 0,
                       'packets_down' => 0,
                       'bytes_down' => 0,
                       'user' => $src_user,
                       ];
               }
               
               $array[$src_address]['packets_up'] += $packets;
               $array[$src_address]['bytes_up'] += $bytes;
               
           }
           
           
           if($dst_user){
               
               if(!array_key_exists($dst_address,  $array)){
                   $array[$dst_address] = [
                       'ip_address' => $dst_address,
                       'packets_up' => 0,
                       'bytes_up' => 0,
                       'packets_down' => 0,
                       'bytes_down' => 0,
                       'user' => $dst_user,
                       ];
               }
               
               $array[$dst_address]['packets_down'] += $packets;
               $array[$dst_address]['bytes_down'] += $bytes ;
               
           }
        }
        
        return $array;
    
    }
    
    private function validate()
    {
        
        $file = new File(WWW_ROOT . '/trafficAccountingRecord.json', false, 0644);
        $json = $file->read(true, 'r');
        $this->_trafficAccountingRecord = json_decode($json, true);
        
        if(count($this->_trafficAccountingRecord) == 0){
            return true;
        }
        
        $last_record = end($this->_trafficAccountingRecord);
        
        $last_record_time = new Time($last_record['date']); 
        
        $this->out('last_record_time: ' . $last_record_time);
        
        $now = Time::now();
        
        $delay = intval($this->_parament['connection']['accounting_traffic']['delay']);
        
        $result = date_diff($last_record_time, $now);
        
        $this->out('diff: ' . $result->i  . ' delay: '. $delay);
        
        return ($result->i > $delay);
    }
    
    private function takeSnapshot()
    {
        $time = Time::now();
        
        $file = WWW_ROOT . 'crontab_log.txt';
        $fp = fopen($file, 'a+');
        fwrite($fp, $time->format('Y-m-d H:i:s') . " - TrafficAccounting" . "\r\n");
        fclose($fp);
        
        $this->hr();
        $this->out('Take Snapshot Traffic Accounting');
        $this->out(__('Time: {0}', $time->format('Y-m-d H:i:s')));
        $this->hr();
        
        if(!$this->validate()){
            $this->abort('No es tiempo aun');
            return false;
        }
     
     
        $record = new \stdClass;
        $record->date = Time::now();
     
        $this->_trafficAccountingRecord[] = $record;
        
        $json = json_encode($this->_trafficAccountingRecord, JSON_PRETTY_PRINT);
          
        $file = new File(WWW_ROOT . '/trafficAccountingRecord.json', false, 0644);
        $file->write($json, 'w', true);
       
        $file = WWW_ROOT . 'trafficAccountingRecord.txt';
        $fp = fopen($file, 'w');
        fwrite($fp, $time->format('Y-m-d H:i:s') . "\r\n");
        fclose($fp);
        
        if(!$this->_parament['system']['integration']){
             $this->abort('Integration Disabled');
        }
        
        $this->loadModel('Controllers');
        $controllers = $this->Controllers->find()->where(['deleted' => false]);
        
        foreach($controllers as $controller){
            
            $snapshots = $this->getTakeSnapshotTrafficAccounting($controller);
            
            if(count ($snapshots) > 0){
            
                $this->loadModel('ConnectionsTraffic');
                
                foreach ($snapshots as $snapshot) {
                    
                    $trafficAccount = $this->ConnectionsTraffic->newEntity();
                    $this->ConnectionsTraffic->patchEntity($trafficAccount, $snapshot);
                    $trafficAccount->time = $time;
                    $this->ConnectionsTraffic->save($trafficAccount);
                }
                
                $this->out(__('Update TrafficAccounting - Controller: {0}', $controller->name));
            }
        }
    }
    
}