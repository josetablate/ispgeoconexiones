<?php

namespace App\Shell;

use Cake\Log\Log;
use Cake\I18n\Time;

use Cake\Console\Shell;
use Cake\Filesystem\File;

class MigrationsFromJsonShell extends Shell
{
    
    public function main($action, $name_file)
    {
        $this->out('MigrationsFromJson main.');

        switch ($action) {
            case 'customers':
                $this->loadCustomers1($name_file);
                break;
            
            default:
                $this->out("$action  no implement.");
                break;
        }       
        
    }


    //Networksdigital
    function loadCustomers1($name_file){

        $file = new File(WWW_ROOT . "/Importers/$name_file.json", false, 0644);
        $json = $file->read(true, 'r');
        $customers_data = json_decode($json);

        $this->loadModel('Customers');
        $counter = 0;

        // Log::info('$customers_data:', ['scope' => ['shell']]);
        // Log::info($customers_data, ['scope' => ['shell']]);

        $customer_terminados = [];


        foreach($customers_data as $customer_data){

            // Log::info($customer_data, ['scope' => ['shell']]);
           
            $address = 'sin-especificar';
            $plan = 'sin-especificar';

            if($customer_data->servicio){
                $s = explode(' - ', $customer_data->servicio);
                $address = $s[0];
                $plan = $s[1];
            }

            $responsible = 5;
            // if(strpos($customer_data->tipo, 'Consumidor Final')){
            //     $responsible = 5;
            // }else{
            //     $responsible = 5;
            // } 

            $phone = 'sin-especificar';            
            if($customer_data->telefono){
                $phone = $customer_data->telefono;            
            }            


            $data = [
                'code'                      => $customer_data->nro_cliente,
                'name'                      => $customer_data->apellido . ' ' . $customer_data->nombre,
                'address'                   => $address,
                'doc_type'                  => '96',
                'ident'                     => $customer_data->dni,
                'responsible'               => $responsible,
                'debt_month'                => floatval($customer_data->saldo),
                'email'                     => $customer_data->mail,
                'is_presupuesto'            => '1',
                'phone'                     => $phone,
                'country_id'                => '1',
                'province_id'               => '4',
                'city_id'                   => '1',
                'area_id'                   => '1',
                'payment_method_default_id' => '1',
                'denomination'              => '1',
                'cond_venta'                => '96',
                'business_billing'          => 1526130279,
                'phone_alt'                 => '',              
                'seller'                    => '',
                'user_id'                   => 100,
                'daydue'                    => 10,
                'created'                   => new Time($customer_data->fecha_alta . '00:00:00'),
                'modified'                  => new Time($customer_data->fecha_alta . '00:00:00'),
                'comments'                  => "estado:$customer_data->estado,plan:$plan,servicio:".$customer_data->servicio
            ];

            
            $customer = $this->Customers->newEntity();
            $customer = $this->Customers->patchEntity($customer, $data);

            // Log::info('$customer', ['scope' => ['shell']]);
            // Log::info($customer, ['scope' => ['shell']]);

            if (!$this->Customers->save($customer)) {                    
                $this->out($customer->code . " error");
                break;
            }

            array_push($customer_terminados, $customer);

            $counter++;
        }

        $this->out("$counter cliente cargados");

        
    }
}