<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\File;
use Cake\I18n\Time;
use Cake\Log\Log;
use Cake\Http\Client;

class AutomaticConnectionBlockingShell extends Shell
{
    public $active_log;
    private $paraments;

    public function initialize()
    {
        parent::initialize();

        $this->loadModel('GeneralParameters');
        $general_paraments_db = $this->GeneralParameters->find()->first();

        if ($general_paraments_db) {
            $parament_cliente = unserialize($general_paraments_db->data);
            $this->paraments = json_decode(json_encode($parament_cliente), true);
        }

        $this->active_log = true;
    }

    private function inTime()
    {
        if (!$this->paraments) {
            return false;
        }

        $now = Time::now();

        $config_time = $this->paraments['automatic_connection_blocking']['hours_execution'];

        if ($this->active_log) {
            $this->out('Config Time: ' . $config_time);
        }

        $time_array = explode(":", $config_time);
        $config_hour = $time_array[0];
        $config_minute = $time_array[1];

        $config_now = Time::now();
        $config_now->hour($config_hour);
        $config_now->minute($config_minute);

        if ($this->active_log) {
            $this->out('Now: ' . $now);
        }

        $result = date_diff($config_now, $now);

        if ($this->active_log) {
            $this->out('Date diff: ' . $result->h . ':' . $result->i);
        }

        return ($result->h == 0 && $result->i == 0);
    }

    private function validateParament()
    {
        if (!$this->paraments) {
            return false;
        }

        if ($this->paraments['automatic_connection_blocking']['enabled']) {

            if ($this->inTime()) {

                if ($this->paraments['automatic_connection_blocking']['day'] == Time::now()->day) {

                    $this->loadModel('MessageTemplates');

                    $messageTemplate = $this->MessageTemplates->find()->where(['deleted' => 0, 'type' => 'Bloqueo', 'enabled' => 1])
                        ->order(['priority' => 'DESC'])->first();

                    if ($messageTemplate) {

                        return true;

                    } else {
                        if ($this->active_log) {
                            Log::warning('No existe un template de corte.', ['scope' => ['AutomaticConnectionBlocking']]);
                        }
                    }

                } else {
                    if ($this->active_log) {
                        Log::info('No esta fecha de corte.', ['scope' => ['AutomaticConnectionBlocking']]);
                    }
                }
            } else {
                Log::info('No está en horario.', ['scope' => ['cobro_digital']]);
            }
        } else {
            if ($this->active_log) {
                Log::info('El control de morosos esta desactivado.', ['scope' => ['AutomaticConnectionBlocking']]);
            }
        }
        return false;
    }

    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        $parser->addOption('force', [
            'short' => 'f',
            'help' => 'force time automatic connection blocking.',
            'boolean' => true,
        ]);

        return $parser;
    }

    public function main()
    {
        if (!$this->paraments) {
            return false;
        }

        if ($this->active_log) {
            Log::info('AutomaticConnectionBlocking Control ...', ['scope' => ['shell']]);
        }

        if ($this->validateParament() || $this->params['force']) {

            Log::info('validate Parament ok ...', ['scope' => ['shell']]);

            $controllerClass = 'App\Controller\ConnectionsController';

            $connection = new $controllerClass;

            $data = $connection->lockedMasiveFromCron();

            if ($data->ok) {
                Log::info('Completed. ' . $data->counter . ' Conexiones bloquedas', ['scope' => ['AutomaticConnectionBlocking']]);
            } else {
                if ($this->active_log) {
                    Log::error('no se completo el bloqueo: ' . $data->error_msg, ['scope' => ['AutomaticConnectionBlocking']]);
                }
            }

        } else {
            if ($this->active_log) {
                Log::warning('No es tiempo aun.', ['scope' => ['AutomaticConnectionBlocking']]);
            }
        }
    }
}
