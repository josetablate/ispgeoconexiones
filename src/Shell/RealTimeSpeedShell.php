<?php

namespace App\Shell;

use Cake\Console\Shell;

use PEAR2\Net\RouterOS;
use PEAR2\Net\RouterOS\SocketException;
use PEAR2\Net\RouterOS\DataFlowException;
use PEAR2\Net\RouterOS\RouterErrorException;
use PEAR2\Net\Transmitter\NetworkStream;
use PEAR2\Net\Transmitter\NetworkTransmitter;
use PEAR2\Net\RouterOS\Response;

use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\I18n\Time;

/*EvilFreelancer/routeros-api-php*/
use \RouterOS\Client;
use \RouterOS\Config;
use \RouterOS\Query;
use RouterOS\Exceptions\ClientException;
use RouterOS\Exceptions\QueryException;


class RealTimeSpeedShell extends Shell
{
    private $_client;
    private $_util;
    private $paraments;
    private $_trafficAccountingRecord;
    
    private $_scale = 1024;
    private $_iteration_limit = 50;
    
    private $_log = false;
    
    private $_sleep_time = 3;
    
    
    public function initialize()
    {
        parent::initialize();
   
        
        $this->_client = null;
        $this->_util = null;
        $this->_string_conn = null;
        
    }
    
    private function connectApi($controller, $timeout = 5){
        
        if($this->_log){
            $this->out('connectApi ...');
        }
       
        $this->loadModel('Controllers');

        $config = new Config([
            'host' => $controller->connect_to,
            'user' => $controller->username,
            'pass' => $controller->password,
            'port' => $controller->port,
            'ssl' => false,
            'timeout' => $timeout,
            'attempts' => 2          
        ]);

        $config_ssl = new Config([
            'host' => $controller->connect_to,
            'user' => $controller->username,
            'pass' => $controller->password,
            'port' => $controller->port_ssl,
            'ssl' => true,
            'timeout' => $timeout,
            'attempts' => 2
        ]); 

        if($controller->use_tls){
            $string_conn = $controller->connect_to.$controller->port_ssl;  
        }else{
            $string_conn = $controller->connect_to.$controller->port;
        }
        
        if(!$controller->enabled){
            return false;
        }
        
        if($controller->deleted){
           
            return false;
        }
        
        if($this->_string_conn != $string_conn){
            
            if($this->_log){
                $this->out('nueva conexion con el controlador ...');
            }
            
            $this->_client = null;
        }
        
        try{

            /**intenta conectar por ssl */
            $this->_client = new Client($config_ssl);

            $use_tls = true;
       
            
        }catch(ClientException $e){

            /**si no intentar conectar por api comun  */
            
            try{
                
                $this->_client = new Client($config);
                
                $use_tls = false;
                    
            }catch(ClientException $e){
           
                return false;                
                
            }              
        }
            
        if(!$this->_client) return false;
        
        $controller->use_tls = $use_tls;
        
        $this->Controllers->save($controller);
        
        if($this->_log){
            $this->out('conexion exitosa ...');
        }
         
        if($controller->use_tls){
            $this->_string_conn = $controller->connect_to.$controller->port_ssl;  
        }else{
            $this->_string_conn = $controller->connect_to.$controller->port;
        } 
        
        return true;
        
    }
    
    
    public function main($conneciton_id)
    {
       $this->getReteQueueApi($conneciton_id);
    }
    
    private function getReteQueueApi($conneciton_id){
    
        $this->loadModel('Connections');
        $this->loadModel('RealTimeSpeed');
        
        $connection = $this->Connections->get($conneciton_id, ['contain' => ['Controllers']]);
        
        if($connection->queue_name){
            
            if(!$this->connectApi($connection->controller, 5)){
               return 'no_connect';
            }   
     
            $this->RealTimeSpeed->deleteAll(['connection_id' => $connection->id]);
            
            for($i = 0; $i < $this->_iteration_limit; $i++){
                
                if($this->_log){
                    $this->out('iteracion: ' . $i);
                }
                
                $response = $this->_client->wr([
                    '/queue/simple/remove',
                    '=name='.$connection->queue_name
                    ]);
    
                if(count($response) > 0){

                    $rate = $response[0]['rate'];
                    
                    $this->out(' rate : '. $rate);
                    
                    $rate = explode('/', $rate);
                    $rate = ['up' => intval($rate[0]), 'down' => intval($rate[1]), 'connection_id' => $connection->id];
                    
                    $rate['up']     /= $this->_scale;
                    $rate['down']   /= $this->_scale;
                    
                    $rate['up']     /= $this->_scale;
                    $rate['down']   /= $this->_scale;
                    
                    $rate['up']     = round($rate['up'], 3);
                    $rate['down']   = round($rate['down'], 3);
                    
                    $realTimeSpeed = $this->RealTimeSpeed->newEntity();                    
                    $realTimeSpeed = $this->RealTimeSpeed->patchEntity($realTimeSpeed, $rate);                    
                    $this->RealTimeSpeed->save($realTimeSpeed);
                    
                    if($this->_log){
                        $this->out('save RealTimeSpeed : ' . $realTimeSpeed->up .'/'.$realTimeSpeed->down);
                    }                    
    
                }else{
                    return 'no_response';
                }
                
                sleep($this->_sleep_time); 
            }
            
            $this->RealTimeSpeed->deleteAll(['connection_id' => $connection->id]);
            
        }else{
            
            return 'no_queue_name';
        }
        
        return 'iteration_end';
       
    }
    
 
    
}