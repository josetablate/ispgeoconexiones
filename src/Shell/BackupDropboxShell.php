<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

use Cake\I18n\Time;
use Kunnu\Dropbox\DropboxApp; 
use Kunnu\Dropbox\Dropbox;
use Kunnu\Dropbox\DropboxFile;
use Cake\Log\Log;

class BackupDropboxShell extends Shell
{
    private $app;
    private $dropbox;
    private $paraments;
    private $files_limit;
    const BASE_URL = "https://dropbox.com";

    public function initialize()
    {
        parent::initialize();

        $this->loadModel('GeneralParameters');
        $general_paraments_db = $this->GeneralParameters->find()->first();

        if ($general_paraments_db) {
            $parament_cliente = unserialize($general_paraments_db->data);
            $this->paraments = $parament_cliente;
            $this->paraments = json_decode(json_encode($parament_cliente), true);

            $app_key =    $this->paraments['backup']['app_key'];
            $app_secret = $this->paraments['backup']['app_secret'];
            $token =      $this->paraments['backup']['token'];

            $this->app = new DropboxApp($app_key, $app_secret, $token);
            $this->dropbox = new Dropbox($this->app);

            $this->files_limit = $this->paraments['backup']['amount_files'];
        }
    }

    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        $parser->addOption('force', [
            'short' => 'f',
            'help' => 'force time backup.',
            'boolean' => true,
        ]);

        return $parser;
    }

    public function main()
    {
        if (!$this->paraments) {
            return false;
        }

        Log::info('Backup Dropbox ...', ['scope' => ['shell']]);

        if ($this->inTime() || $this->params['force']) {

            Log::info('En horario de backup.', ['scope' => ['shell']]);

            $this->out('Upload databases file.');

            if ($this->uploadDatabases()) {

                $this->verifyFilesAmount('databases');

                // if ($this->paraments['backup']['enabled_comprobantes']) {

                //     $this->uploadComprobantes();

                //     $this->verifyFilesAmount('comprobantes');
                // }

                $this->clearLocalBackupFile();
            }
        } 
    }

    private function inTime()
    {
        if (!$this->paraments) {
            return false;
        }

        $now = Time::now();

        $config_time = $this->paraments['backup']['hours_execution'];

        $this->out('Config Time: ' . $config_time);

        $time_array = explode(":", $config_time);
        $config_hour = $time_array[0];
        $config_minute = $time_array[1];

        $config_now = Time::now();
        $config_now->hour($config_hour);
        $config_now->minute($config_minute);

        $this->out('Now: ' . $now);

        $result = date_diff($config_now, $now);

        $this->out('Date diff: ' . $result->h . ':' . $result->i);

        return ($result->h == 0 && $result->i == 0);
    }

    private function uploadDatabases()
    {
        if ($this->isDomainAvailible(BackupDropboxShell::BASE_URL)) {

            $folder_name = $this->paraments['backup']['folder_name'];
            $dir = new Folder(WWW_ROOT . 'backup/', true);

            $user_mysql = "root";
            $pass_mysql = "";

            $database_path = WWW_ROOT . 'backup/';

            $query_get_databases = "mysql -u$user_mysql -p$pass_mysql -e 'SHOW DATABASES;' | grep -Ev '(Database|information_schema|performance_schema|mysql|phpmyadmin|c9)'";

            if ($GLOBALS['type'] == 'nube') {
                $query_get_databases = "mysql -u$user_mysql -p$pass_mysql -e 'SHOW DATABASES;' | grep '_$folder_name*'";
            }

            $databases_name = shell_exec($query_get_databases);

            $databases_name = explode("\n", $databases_name);
            array_pop($databases_name);

            foreach ($databases_name as $db_name) {
                $database_path_to_file = $database_path . $db_name . '.sql';
                shell_exec("mysqldump -u$user_mysql -p$pass_mysql $db_name > $database_path_to_file");
            }

            $file_db_name = 'db-'.Time::now()->format('Y-m-d_His'). ".tar.gz";
            $db_path_to_file = $database_path . $file_db_name;
            shell_exec("tar -czvf $db_path_to_file $database_path*.sql");

            $dropboxDatabaseFile = new DropboxFile($db_path_to_file);
            $fileDatabases = $this->dropbox->upload($dropboxDatabaseFile, "/$folder_name/databases/$file_db_name", ['autorename' => true]);

            if (method_exists($fileDatabases, 'getName')) {

                Log::info(' subió los backup de las base de datos correctamente.', ['scope' => ['backup']]);

                return true;

            } else {

                Log::error('No subió los backup de las base de datos.', ['scope' => ['backup']]);
            }
        } else {
            Log::error('No subió los backup de las base de datos. Sin acceso a Dropbox.', ['scope' => ['backup']]);
        }

        return false;
    }

    private function uploadComprobantes()
    {
        if ($this->isDomainAvailible(BackupDropboxShell::BASE_URL)) {

            $folder_name = $this->paraments['backup']['folder_name'];

            // comprimir carpeta de comprobantes
            $comprobantes_path = WWW_ROOT . 'comprobantes/';

            $file_comprobantes_name = 'co-'.Time::now()->format('Y-m-d_His') . ".tar.gz";
            $comprobantes_path_to_file = WWW_ROOT . $file_comprobantes_name;
            shell_exec("tar -zcvf $comprobantes_path_to_file $comprobantes_path");

            $dropboxComprobantesFile = new DropboxFile($comprobantes_path_to_file);
            $fileComprobantes = $this->dropbox->upload($dropboxComprobantesFile, "/$folder_name/comprobantes/$file_comprobantes_name", ['autorename' => true]);

            if (method_exists($fileComprobantes, 'getName')) {

                Log::info('subió las comprobantes correctamente.', ['scope' => ['backup']]);

                return true;

            } else {
                Log::error('No subió las comprobantes.', ['scope' => ['backup']]);
            }
        } else {
            Log::error('No subió las comprobantes. Sin acceso a Dropbox.', ['scope' => ['backup']]);
        }

        return false;
    }

    private function clearLocalBackupFile()
    {
         $remove_file_path = WWW_ROOT . 'backup/';
         shell_exec("rm -r $remove_file_path/*");
    }

    private function verifyFilesAmount($folder)
    {
        if ($this->isDomainAvailible(BackupDropboxShell::BASE_URL)) {

            $folder_name = $this->paraments['backup']['folder_name'];

            $listFolderContents = $this->dropbox->listFolder("/$folder_name/$folder/");

            // Fetch Items (Returns an instance of ModelCollection)
            $items = $listFolderContents->getItems();

            if (count($items) > $this->files_limit) {

                $path_file = $items->first()->path_lower;

                $deletedFile = $this->dropbox->delete($path_file);

                if (method_exists($deletedFile, 'getName')) {

                   Log::info("No elimino backupo mas viejo de la carpeta $folder.", ['scope' => ['backup']]);

                } else {

                    Log::error("No pudo eliminar el backup mas viejo  de la carpeta $folder.", ['scope' => ['backup']]);
                }
            }
        } else {

            Log::error("No pudo eliminar el backup mas viejo de la carpeta $folder. Sin acceso a Dropbox.", ['scope' => ['backup']]);
        }
    }

    //returns true, if domain is availible, false if not
    public function isDomainAvailible($domain)
    {
        //check, if a valid url is provided
        if (!filter_var($domain, FILTER_VALIDATE_URL)) {
            return false;
        }

        //initialize curl
        $curlInit = curl_init($domain);
        curl_setopt($curlInit, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curlInit, CURLOPT_HEADER, true);
        curl_setopt($curlInit, CURLOPT_NOBODY, true);
        curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, true);

        //get answer
        $response = curl_exec($curlInit);

        curl_close($curlInit);

        if ($response) return true;

        return false;
    }
}
