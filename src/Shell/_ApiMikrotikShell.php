<?php

namespace App\Shell;

use Cake\Console\Shell;

use PEAR2\Net\RouterOS;
use PEAR2\Net\RouterOS\SocketException;
use PEAR2\Net\RouterOS\DataFlowException;
use PEAR2\Net\RouterOS\RouterErrorException;
use PEAR2\Net\Transmitter\NetworkStream;
use PEAR2\Net\Transmitter\NetworkTransmitter;
use PEAR2\Net\RouterOS\Response;

use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Log\Log;
use Cake\I18n\Time;

/*EvilFreelancer/routeros-api-php*/
use \RouterOS\Client;
use \RouterOS\Config;
use \RouterOS\Query;
use RouterOS\Exceptions\ClientException;
use RouterOS\Exceptions\QueryException;


class ApiMikrotikShell extends Shell
{
    private $_client;
    
    public function initialize()
    {
        parent::initialize();  
        
        $this->_client = null;   
        
    }
    
    private function connectApiTest(){
        
    
        $this->loadModel('Controllers');

        $controllers = $this->Controllers->find()->where(['deleted' => false]);

        $data = [
            ['ID', 'Controlador', 'Habilitado', 'IP', 'Port', 'TLS', 'Status']              
        ];

        foreach($controllers as $controller){


            array_push($data, [$controller->name.'la', $controller->name, $controller->enable ? 'Si' : 'NO', $controller->connect_to, $controller->port.'', 'Si', 'OK']);

         

        }

        $this->out($data[0]);

 
        
    }
    
    
    public function main()
    {
  
        $this->connectApiTest();
      
    }
    
   
 
    
}