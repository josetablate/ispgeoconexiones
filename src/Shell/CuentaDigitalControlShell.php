<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\File;
use Cake\I18n\Time;
use Cake\Log\Log;
use Cake\Http\Client;
use Cake\Routing\Router;

class CuentaDigitalControlShell extends Shell
{
    private $payment_getway;

    public function initialize()
    {
        parent::initialize();

        $this->loadModel('PaymentGetwayParameters');
        $payemnt_getway_paraments_db = $this->PaymentGetwayParameters->find()->first();

        if ($payemnt_getway_paraments_db) {
            $payment_getway_cliente = unserialize($payemnt_getway_paraments_db->data);
            $this->payment_getway = json_decode(json_encode($payment_getway_cliente), true);
        }
    }

    private function inTime()
    {
        if (!$this->payment_getway) {
            return FALSE;
        }

        $now = Time::now();

        $config_time = $this->payment_getway['config']['cuentadigital']['hours_execution'];

        $this->out('Config Time: ' . $config_time);

        $time_array = explode(":", $config_time);
        $config_hour = $time_array[0];
        $config_minute = $time_array[1];

        $config_now = Time::now();
        $config_now->hour($config_hour);
        $config_now->minute($config_minute);

        $this->out('Now: ' . $now);

        $result = date_diff($config_now, $now);

        $this->out('Date diff: ' . $result->h . ':' . $result->i);

        return ($result->h == 0 && $result->i == 0);
    }

    private function validateParament()
    {
        if ($this->payment_getway['config']['cuentadigital']['enabled']) {

            $count = sizeof($this->payment_getway['config']['cuentadigital']['credentials']);

            if ($count > 0) {

                if ($this->inTime()) {
                    return TRUE;
                } else {
                    Log::info('No esta en horario.', ['scope' => ['cuentadigital']]);
                }

            } else {
                Log::info('Credenciales vacias.', ['scope' => ['cuentadigital']]);
            }
           
        } else {
            Log::info('Cuenta Digital está deshabilitado.', ['scope' => ['cuentadigital']]);
        }
        return FALSE;
    }

    public function main()
    {
        if (!$this->payment_getway) {
            return FALSE;
        }
        
        Log::info('CuentaDigital Control ...', ['scope' => ['shell']]);

        if ($this->validateParament() || $this->params['force']) {

            Log::info('validate Parament ok ...', ['scope' => ['shell']]);

            $controllerClass = 'App\Controller\CuentadigitalController';

            $cuentadigital = new $controllerClass;

            $data = json_decode($cuentadigital->ticketControl());

            if ($data->code == 200) {
                Log::info('Completado. ' . $data->message, ['scope' => ['cuentadigital']]);
            } else {
                Log::error('Código: ' . $data->code . ' - Mensaje: ' . $data->message, ['scope' => ['cuentadigital']]);
                Log::error($data, ['scope' => ['cuentadigital']]);
            }

            return TRUE;
        }
    }

    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        $parser->addOption('force', [
            'short' => 'f',
            'help' => 'force in time control ticket from cuentadigital.',
            'boolean' => TRUE,
        ]);

        return $parser;
    }
}