<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\Log\Log;

class CreateIPTableShell extends Shell
{
    
    public function initialize()
    {
        parent::initialize();
    }
    
    
    public function main()
    {
  
    }
    
    public function add($port, $ip, $port2)
    {
        
        Log::info('Create IPTable ...', ['scope' => ['shell']]);
        
        shell_exec("iptables -t nat -A PREROUTING -p tcp --dport $port -j DNAT --to-destination $ip:$port2");
        
        shell_exec("iptables-save > /etc/iptables/rules.v4");
         
        Log::info('Create IPTableTerminated', ['scope' => ['shell']]);
  
    }
    
    public function manual($port, $ip, $port2)
    {
        
        Log::info('Create IPTable ...', ['scope' => ['shell']]);
        
        shell_exec("iptables -t nat -A PREROUTING -p tcp --dport $port -j DNAT --to-destination $ip:$port2");
        
        shell_exec("iptables-save > /etc/iptables/rules.v4");
         
        Log::info('Create IPTableTerminated', ['scope' => ['shell']]);
  
    }
}