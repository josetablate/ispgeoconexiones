<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FreePoolIps Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Networks
 *
 * @method \App\Model\Entity\FreePoolIp get($primaryKey, $options = [])
 * @method \App\Model\Entity\FreePoolIp newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\FreePoolIp[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\FreePoolIp|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FreePoolIp patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\FreePoolIp[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\FreePoolIp findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class FreePoolIpsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('free_pool_ips');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Networks', [
            'foreignKey' => 'network_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('ip')
            ->requirePresence('ip', 'create')
            ->notEmpty('ip')
            ->add('ip', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['ip']));
        $rules->add($rules->existsIn(['network_id'], 'Networks'));

        return $rules;
    }
}
