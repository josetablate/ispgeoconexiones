<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PayuAccounts Model
 *
 * @property \App\Model\Table\PaymentGetwaysTable|\Cake\ORM\Association\BelongsTo $PaymentGetways
 *
 * @method \App\Model\Entity\PayuAccount get($primaryKey, $options = [])
 * @method \App\Model\Entity\PayuAccount newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PayuAccount[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PayuAccount|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PayuAccount|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PayuAccount patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PayuAccount[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PayuAccount findOrCreate($search, callable $callback = null, $options = [])
 */
class PayuAccountsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('payu_accounts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_code',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('barcode')
            ->maxLength('barcode', 50)
            ->allowEmpty('barcode');

        $validator
            ->scalar('id_comercio')
            ->maxLength('id_comercio', 10)
            ->allowEmpty('id_comercio');

        $validator
            ->integer('customer_code')
            ->allowEmpty('customer_code');

        $validator
            ->boolean('used')
            ->requirePresence('used', 'create')
            ->notEmpty('used');

        $validator
            ->boolean('deleted')
            ->requirePresence('deleted', 'create')
            ->notEmpty('deleted');

        $validator
            ->dateTime('date_exported')
            ->allowEmpty('date_exported');

        return $validator;
    }

    public function findServerSideData(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        if ($params['payment_getway_id'] == 7) {

            // tarjeta de cobranza
            $columns = [
                '',                             //0
                'PayuAccounts.barcode',         //1
                'PayuAccounts.id_comercio',     //2
                'PayuAccounts.date_exported',   //3
            ];

            $columns_select = [
                'PayuAccounts.barcode',         //0
                'PayuAccounts.id_comercio',     //1
                'Customers.name',               //2
                'Customers.ident',              //3
                'Customers.code',               //4
                'Customers.doc_type',           //5
                'PayuAccounts.date_exported',   //6
                'PayuAccounts.id',              //7
            ];

        }

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] =>  $o['dir']];
        }

        $query = $query->contain([
            'Customers'
        ])
        ->select($columns_select); 

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'PayuAccounts.date_exported':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2]  . '-' . $value[1][1] . '-' .  $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] .' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Customers.code':
                    case 'PayuAccounts.id_comercio':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'PayuAccounts.deleted':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? true : false];
                        break;

                    case 'PayuAccounts.barcode':
                    case 'Customers.ident':
                    case 'Customers.name':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {
                    case 'PayuAccounts.date_exported':
                        break;

                    case 'Customers.code': 
                    case 'PayuAccounts.id_comercio':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Customers.name': 
                    case 'Customers.ident':
                    case 'PayuAccounts.barcode':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order($order);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        return $query;
    }

    public function findRecordsFiltered(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        if ($params['payment_getway_id'] == 7) {

            // tarjeta de cobranza
            $columns = [
                '',                             //0
                'PayuAccounts.barcode',         //1
                'PayuAccounts.id_comercio',     //2
                'PayuAccounts.date_exported',   //3
            ];

            $columns_select = [
                'PayuAccounts.barcode',         //0
                'PayuAccounts.id_comercio',     //1
                'Customers.name',               //2
                'Customers.ident',              //3
                'Customers.code',               //4
                'Customers.doc_type',           //5
                'PayuAccounts.date_exported',   //6
                'PayuAccounts.id',              //7
            ];

        }

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] =>  $o['dir']];
        }

        $query = $query->contain([
            'Customers'
        ])->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'PayuAccounts.date_exported':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2]  . '-' . $value[1][1] . '-' .  $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] .' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Customers.code':
                    case 'PayuAccounts.id_comercio':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'PayuAccounts.deleted':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? true : false];
                        break;

                    case 'PayuAccounts.barcode':
                    case 'Customers.ident':
                    case 'Customers.name':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {
                    case 'PayuAccounts.date_exported':
                        break;

                    case 'Customers.code': 
                    case 'PayuAccounts.id_comercio':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Customers.name': 
                    case 'Customers.ident':
                    case 'PayuAccounts.barcode':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }


        if (count($between) > 0) {
            foreach ($between as $b) {
                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }
}
