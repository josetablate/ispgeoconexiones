<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DebitNotes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\DebitNote get($primaryKey, $options = [])
 * @method \App\Model\Entity\DebitNote newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DebitNote[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DebitNote|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DebitNote patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DebitNote[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DebitNote findOrCreate($search, callable $callback = null)
 */
class DebitNotesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('debit_notes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        
         $this->belongsTo('Customers', [
            'foreignKey' => 'customer_code',
            'joinType' => 'INNER'
        ]);
        
        $this->hasMany('ConceptsDebit', [
            'foreignKey' => 'debit_note_id'
        ]);
        
        $this->belongsTo('Connections', [
            'foreignKey' => 'connection_id',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('concept_type')
            ->requirePresence('concept_type', 'create')
            ->notEmpty('concept_type');

        $validator
            ->dateTime('date')
            ->requirePresence('date', 'create')
            ->notEmpty('date');

        $validator
            ->integer('pto_vta')
            ->requirePresence('pto_vta', 'create')
            ->notEmpty('pto_vta');

        $validator
            ->requirePresence('tipo_comp', 'create')
            ->notEmpty('tipo_comp');

        $validator
            ->allowEmpty('concept');

        $validator
            ->decimal('total')
            ->requirePresence('total', 'create')
            ->notEmpty('total');

        $validator
            ->requirePresence('company_name', 'create')
            ->notEmpty('company_name');

        $validator
            ->requirePresence('company_address', 'create')
            ->notEmpty('company_address');

        $validator
            ->requirePresence('company_cp', 'create')
            ->notEmpty('company_cp');

        $validator
            ->requirePresence('company_city', 'create')
            ->notEmpty('company_city');

        $validator
            ->requirePresence('company_phone', 'create')
            ->notEmpty('company_phone');

        $validator
            ->allowEmpty('company_fax');

        $validator
            ->requirePresence('company_ident', 'create')
            ->notEmpty('company_ident');

        $validator
            ->allowEmpty('company_email');

        $validator
            ->allowEmpty('company_web');

        $validator
            ->integer('customer_code')
            ->requirePresence('customer_code', 'create')
            ->notEmpty('customer_code');

        $validator
            ->requirePresence('customer_name', 'create')
            ->notEmpty('customer_name');

        $validator
            ->allowEmpty('customer_address');

        $validator
            ->allowEmpty('customer_cp');

        $validator
            ->allowEmpty('customer_city');

        $validator
            ->allowEmpty('customer_country');

        $validator
            ->integer('customer_doc_type')
            ->allowEmpty('customer_doc_type');

        $validator
            ->allowEmpty('customer_ident');

        $validator
            ->integer('customer_responsible')
            ->requirePresence('customer_responsible', 'create')
            ->notEmpty('customer_responsible');

        $validator
            ->allowEmpty('comments');

        $validator
            ->allowEmpty('cae');

        $validator
            ->dateTime('vto')
            ->allowEmpty('vto');

        $validator
            ->allowEmpty('barcode');

        $validator
            ->integer('seating_number')
            ->allowEmpty('seating_number');

        $validator
            ->dateTime('duedate')
            ->requirePresence('duedate', 'create')
            ->notEmpty('duedate');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
    
    public function findServerSideData(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;
        $columns = [
            'DebitNotes.date',              //0
            'DebitNotes.tipo_comp',         //1
            'DebitNotes.pto_vta',           //2
            'DebitNotes.num',               //3
            'DebitNotes.date_start',        //4
            'DebitNotes.date_end',          //5
            'DebitNotes.comments',          //6
            'DebitNotes.duedate',           //7
            'DebitNotes.subtotal',          //8
            'DebitNotes.sum_tax',           //9
            'DebitNotes.total',             //10
            'DebitNotes.cae',               //11
            'DebitNotes.vto',               //12
            'Users.username',               //13
            'DebitNotes.customer_code',     //14
            'DebitNotes.customer_doc_type', //15
            'DebitNotes.customer_ident',    //16 
            'DebitNotes.customer_name',     //17
            'DebitNotes.customer_address',  //18
            'DebitNotes.customer_city',     //19
            'DebitNotes.business_id',       //20
            'DebitNotes.seating_number',   //21
    
        ];

        $columns_select = [
            'DebitNotes.date',              //0
            'DebitNotes.tipo_comp',         //1
            'DebitNotes.pto_vta',           //2
            'DebitNotes.num',               //3
            'DebitNotes.date_start',        //4
            'DebitNotes.date_end',          //5
            'DebitNotes.comments',          //6
            'DebitNotes.duedate',           //7
            'DebitNotes.subtotal',          //8
            'DebitNotes.sum_tax',           //9
            'DebitNotes.total',             //10
            'DebitNotes.cae',               //11
            'DebitNotes.vto',               //12
            'Users.username',               //13
            'DebitNotes.customer_code',     //14
            'DebitNotes.customer_doc_type', //15
            'DebitNotes.customer_ident',    //16 
            'DebitNotes.customer_name',     //17
            'DebitNotes.customer_address',  //18
            'DebitNotes.customer_city',     //19
            'DebitNotes.business_id',       //20
            'DebitNotes.seating_number',   //21
            
            'DebitNotes.id',
            'Customers.email',
            'Customers.billing_for_service',
        ];

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] =>  $o['dir']];
        }

        $query = $query->contain([
            'Customers',
            'Users'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {
            
            $column['search']['value'] = trim($column['search']['value']);
            
            if ($column['search']['value'] != '') {
                
                switch ($columns[$index]) {

                    case 'DebitNotes.date': 
                    case 'DebitNotes.date_start':
                    case 'DebitNotes.date_end':
                    case 'DebitNotes.duedate':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'DebitNotes.tipo_comp':
                    case 'DebitNotes.pto_vta': 
                    case 'DebitNotes.num': 
                    case 'DebitNotes.customer_code':
                    case 'DebitNotes.business_id':    
                    case 'DebitNotes.customer_ident':
                    case 'DebitNotes.customer_doc_type':

                        $where += [$columns[$index] => $column['search']['value']];
                        break;
                    case 'DebitNotes.subtotal':
                    case 'DebitNotes.sum_tax':
                    case 'DebitNotes.total': 

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }

                        break;

                    case 'DebitNotes.cae':
                    case 'Users.name':
                    case 'DebitNotes.customer_name':
                    case 'DebitNotes.comments':
                    case 'DebitNotes.address':
                    case 'DebitNotes.city':
                    case 'DebitNotes.seating_number':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'DebitNotes.num':
                    case 'DebitNotes.cae':
                    case 'DebitNotes.customer_code':
                    case 'DebitNotes.customer_ident':
                    case 'DebitNotes.customer_doc_type':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'DebitNotes.customer_name':
                    case 'DebitNotes.comments':
                    case 'DebitNotes.address':
                    case 'DebitNotes.city':
                    case 'DebitNotes.seating_number':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if( $where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {
                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order($order);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        $query->toArray();

        return $query;
    }

    public function findRecordsFiltered(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;
        $columns = [
            'DebitNotes.date',              //0
            'DebitNotes.tipo_comp',         //1
            'DebitNotes.pto_vta',           //2
            'DebitNotes.num',               //3
            'DebitNotes.date_start',        //4
            'DebitNotes.date_end',          //5
            'DebitNotes.comments',          //6
            'DebitNotes.duedate',           //7
            'DebitNotes.subtotal',          //8
            'DebitNotes.sum_tax',           //9
            'DebitNotes.total',             //10
            'DebitNotes.cae',               //11
            'DebitNotes.vto',               //12
            'Users.username',               //13
            'DebitNotes.customer_code',     //14
            'DebitNotes.customer_doc_type', //15
            'DebitNotes.customer_ident',    //16 
            'DebitNotes.customer_name',     //17
            'DebitNotes.customer_address',  //18
            'DebitNotes.customer_city',     //19
            'DebitNotes.business_id',       //20
            'DebitNotes.seating_number',   //21
    
        ];

        $columns_select = [
            'DebitNotes.date',              //0
            'DebitNotes.tipo_comp',         //1
            'DebitNotes.pto_vta',           //2
            'DebitNotes.num',               //3
            'DebitNotes.date_start',        //4
            'DebitNotes.date_end',          //5
            'DebitNotes.comments',          //6
            'DebitNotes.duedate',           //7
            'DebitNotes.subtotal',          //8
            'DebitNotes.sum_tax',           //9
            'DebitNotes.total',             //10
            'DebitNotes.cae',               //11
            'DebitNotes.vto',               //12
            'Users.username',               //13
            'DebitNotes.customer_code',     //14
            'DebitNotes.customer_doc_type', //15
            'DebitNotes.customer_ident',    //16 
            'DebitNotes.customer_name',     //17
            'DebitNotes.customer_address',  //18
            'DebitNotes.customer_city',     //19
            'DebitNotes.business_id',       //20
            'DebitNotes.seating_number',   //21
            
            'DebitNotes.id',
            'Customers.email',
            'Customers.billing_for_service',
        ];

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] =>  $o['dir']];
        }

        $query = $query->contain([
            'Customers',
            'Users'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'DebitNotes.date': 
                    case 'DebitNotes.date_start':
                    case 'DebitNotes.date_end':
                    case 'DebitNotes.duedate':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'DebitNotes.tipo_comp':
                    case 'DebitNotes.pto_vta': 
                    case 'DebitNotes.num': 
                    case 'DebitNotes.customer_code':
                    case 'DebitNotes.business_id':
                    case 'DebitNotes.customer_ident':
                    case 'DebitNotes.customer_doc_type':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'DebitNotes.subtotal':
                    case 'DebitNotes.sum_tax':
                    case 'DebitNotes.total': 

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }

                        break;

                    case 'DebitNotes.cae':
                    case 'Users.name':
                    case 'DebitNotes.customer_name':
                    case 'DebitNotes.comments':
                    case 'DebitNotes.customer_address':
                    case 'DebitNotes.customer_city':
                    case 'DebitNotes.seating_number':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'DebitNotes.num':
                    case 'DebitNotes.cae':
                    case 'DebitNotes.customer_code':
                    case 'DebitNotes.customer_ident':
                    case 'DebitNotes.customer_doc_type':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'DebitNotes.customer_name':
                    case 'DebitNotes.comments':
                    case 'DebitNotes.customer_address':
                    case 'DebitNotes.customer_city':
                    case 'DebitNotes.seating_number':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {
                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }

    public function findServerSideDataMoves(Query $query, array $options)
    {
        $params = $options['params'];
     
        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;
        
        $columns = [
            'DebitNotes.date',
            'Users.username',
            '',
            'DebitNotes.tipo_comp',
            'DebitNotes.pto_vta',
            'DebitNotes.num',
            'DebitNotes.cae',
            'DebitNotes.comments',
            'DebitNotes.subtotal',
            'DebitNotes.sum_tax',
            'DebitNotes.total',
            '',
            'DebitNotes.duedate',
            '',
            'DebitNotes.seating_number'
        ];
            
        $Columns_select = [
            
            'DebitNotes.date',
            'Users.username',
            'DebitNotes.tipo_comp',
            'DebitNotes.pto_vta',
            'DebitNotes.num',
            'DebitNotes.cae',
            'DebitNotes.comments',
            'DebitNotes.subtotal',
            'DebitNotes.sum_tax',
            'DebitNotes.total',
            'DebitNotes.duedate',
            'DebitNotes.seating_number',
        
            'DebitNotes.id',
            'DebitNotes.connection_id',
            'DebitNotes.customer_code',
        ];

        //paginacion
        if ($params['length'] > 0) {
            
            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }
        
        $query = $query->contain([
            'Users'
        ])
        ->select($Columns_select);
        
        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {
            
            $column['search']['value'] = trim($column['search']['value']);
            
            if ($column['search']['value'] != '') {
                
                switch ($columns[$index]) {
                    
                    case 'DebitNotes.date': 
                    case 'DebitNotes.duedate':
                      
                        $value = explode('<>',$column['search']['value']); 
                        
                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);
                        
                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                            
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] .' -' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if( $value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] .  ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        
                        break;

                    case 'DebitNotes.tipo_comp':
                    case 'DebitNotes.pto_vta': 
                    case 'DebitNotes.num': 
                    case 'DebitNotes.cae':
             
                        $where += [$columns[$index] => $column['search']['value']];
                        break;
    
                    case 'DebitNotes.total': 
                        
                        $value = explode('<>', $column['search']['value']); 
                        
                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);
                        
                        if ($value[0] != '' && $value[1] != '' ) {
                            
                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                            
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        
                        break;
      
                    case 'Users.username':
                    case 'DebitNotes.comments':
                    case 'DebitNotes.seating_number':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }
        
        $params['search']['value'] = trim($params['search']['value']);
   
        //busqueda general
        if ($params['search']['value'] != '') {
            
            foreach ($columns as $index => $column) {
                
                switch ($columns[$index]) {

                    case 'DebitNotes.cae':
               
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;
                    
                    case 'user.username' :
                    case 'DebitNotes.comments' :
                    case 'DebitNotes.seating_number':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {
        
            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }
  
        $query->order(['DebitNotes.date' => 'desc']);
        
        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }
        
        $query->toArray();

        return $query;
    }
    
    public function findRecordsFilteredMoves(Query $query, array $options)
    {
        $params = $options['params'];
            
        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;
        
        $columns = [
            'DebitNotes.date',
            'Users.username',
            '',
            'DebitNotes.tipo_comp',
            'DebitNotes.pto_vta',
            'DebitNotes.num',
            'DebitNotes.cae',
            'DebitNotes.comments',
            'DebitNotes.subtotal',
            'DebitNotes.sum_tax',
            'DebitNotes.total',
            '',
            'DebitNotes.duedate',
            '',
            'DebitNotes.seating_number'
        ];
            
        $Columns_select = [
            
            'DebitNotes.date',
            'Users.username',
            'DebitNotes.tipo_comp',
            'DebitNotes.pto_vta',
            'DebitNotes.num',
            'DebitNotes.cae',
            'DebitNotes.comments',
            'DebitNotes.subtotal',
            'DebitNotes.sum_tax',
            'DebitNotes.total',
            'DebitNotes.duedate',
            'DebitNotes.seating_number',
        
            'DebitNotes.id',
            'DebitNotes.connection_id',
            'DebitNotes.customer_code',
        ];
        
        
        $query = $query->contain([
            'Users'
        ])
        ->select($Columns_select);
        
        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {
            
            $column['search']['value'] = trim($column['search']['value']);
            
            if ($column['search']['value'] != '') {
                
                switch ($columns[$index]) {
                    
                    case 'DebitNotes.date': 
                    case 'DebitNotes.duedate':
                      
                        $value = explode('<>', $column['search']['value']); 
                        
                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);
                        
                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                            
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0].' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        
                        break;
                    
                    case 'DebitNotes.tipo_comp':
                    case 'DebitNotes.pto_vta': 
                    case 'DebitNotes.num': 
                    case 'DebitNotes.cae':
                    
                        $where += [$columns[$index] => $column['search']['value']];
                        break;
          
                    case 'DebitNotes.total': 
                        
                        $value = explode('<>',$column['search']['value']); 
                        
                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);
                        
                        if ($value[0] != '' && $value[1] != '' ) {
                            
                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                            
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        
                        break;

                    case 'Users.username':
                    case 'DebitNotes.comments':
                    case 'DebitNotes.seating_number':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }
        
        $params['search']['value'] = trim($params['search']['value']);
   
        //busqueda general
        if ($params['search']['value'] != '') {
            
            foreach ($columns as $index => $column) {
                
                switch ($columns[$index]) {
                    
                    case 'DebitNotes.num':
                    case 'DebitNotes.cae':
                    
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;
                    
                    case 'Users.username' :
                    case 'DebitNotes.comments' :
                    case 'DebitNotes.seating_number':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }
       
        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }

    public function findServerSideDataComprobantes(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            '',                             //0
            'DebitNotes.date',              //1
            'DebitNotes.date_start',        //2
            'DebitNotes.date_end',          //3
            'DebitNotes.tipo_comp',         //4
            'DebitNotes.pto_vta',           //5
            'DebitNotes.num',               //6
            'DebitNotes.comments',          //7
            'DebitNotes.duedate',           //8
            'DebitNotes.total',             //9
            'Users.username',               //10
            'DebitNotes.customer_code',     //11
            'DebitNotes.customer_doc_type', //12
            'DebitNotes.customer_ident',    //13 
            'DebitNotes.customer_name',     //14
            'Customers.email',              //15
            'DebitNotes.customer_address',  //16
            'DebitNotes.customer_city',     //17
            '',                             //18
            'DebitNotes.business_id',       //19
        ];

        $columns_select = [
            'DebitNotes.id',                 //0
            'DebitNotes.date',               //1
            'DebitNotes.date_start',         //2
            'DebitNotes.date_end',           //3
            'DebitNotes.tipo_comp',          //4
            'DebitNotes.pto_vta',            //5
            'DebitNotes.num',                //6
            'DebitNotes.comments',           //7
            'DebitNotes.duedate',            //8
            'DebitNotes.total',              //9
            'Users.username',                //10
            'DebitNotes.customer_code',      //11
            'DebitNotes.customer_doc_type',  //12
            'DebitNotes.customer_ident',     //13 
            'DebitNotes.customer_name',      //14
            'Customers.email',               //15
            'DebitNotes.customer_address',   //16
            'DebitNotes.customer_city',      //17
            'DebitNotes.business_id',        //18
            'Customers.billing_for_service', //19
        ];

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] =>  $o['dir']];
        }

        $query = $query->contain([
            'Customers',
            'Users'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {
            
            $column['search']['value'] = trim($column['search']['value']);
            
            if ($column['search']['value'] != '') {
                
                switch ($columns[$index]) {

                    case 'DebitNotes.date': 
                    case 'DebitNotes.date_start':
                    case 'DebitNotes.date_end':
                    case 'DebitNotes.duedate':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'DebitNotes.tipo_comp':
                    case 'DebitNotes.pto_vta': 
                    case 'DebitNotes.num': 
                    case 'DebitNotes.customer_code':
                    case 'DebitNotes.business_id':    
                    case 'DebitNotes.customer_ident':
                    case 'DebitNotes.customer_doc_type':

                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'DebitNotes.total': 

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }

                        break;

                    case 'Users.name':
                    case 'DebitNotes.customer_name':
                    case 'DebitNotes.comments':
                    case 'DebitNotes.address':
                    case 'Customers.email':
                    case 'DebitNotes.city':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'DebitNotes.num':
                    case 'DebitNotes.customer_code':
                    case 'DebitNotes.customer_ident':
                    case 'DebitNotes.customer_doc_type':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'DebitNotes.customer_name':
                    case 'DebitNotes.comments':
                    case 'DebitNotes.address':
                    case 'DebitNotes.city':
                    case 'Customers.email':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if( $where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {
                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order($order);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        $query->toArray();

        return $query;
    }

    public function findRecordsFilteredComprobantes(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            '',                             //0
            'DebitNotes.date',              //1
            'DebitNotes.date_start',        //2
            'DebitNotes.date_end',          //3
            'DebitNotes.tipo_comp',         //4
            'DebitNotes.pto_vta',           //5
            'DebitNotes.num',               //6
            'DebitNotes.comments',          //7
            'DebitNotes.duedate',           //8
            'DebitNotes.total',             //9
            'Users.username',               //10
            'DebitNotes.customer_code',     //11
            'DebitNotes.customer_doc_type', //12
            'DebitNotes.customer_ident',    //13 
            'DebitNotes.customer_name',     //14
            'Customers.email',              //15
            'DebitNotes.customer_address',  //16
            'DebitNotes.customer_city',     //17
            '',                             //18
            'DebitNotes.business_id',       //19
        ];

        $columns_select = [
            'DebitNotes.id',                 //0
            'DebitNotes.date',               //1
            'DebitNotes.date_start',         //2
            'DebitNotes.date_end',           //3
            'DebitNotes.tipo_comp',          //4
            'DebitNotes.pto_vta',            //5
            'DebitNotes.num',                //6
            'DebitNotes.comments',           //7
            'DebitNotes.duedate',            //8
            'DebitNotes.total',              //9
            'Users.username',                //10
            'DebitNotes.customer_code',      //11
            'DebitNotes.customer_doc_type',  //12
            'DebitNotes.customer_ident',     //13 
            'DebitNotes.customer_name',      //14
            'Customers.email',               //15
            'DebitNotes.customer_address',   //16
            'DebitNotes.customer_city',      //17
            'DebitNotes.business_id',        //18
            'Customers.billing_for_service', //19
        ];

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] =>  $o['dir']];
        }

        $query = $query->contain([
            'Customers',
            'Users'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {
            
            $column['search']['value'] = trim($column['search']['value']);
            
            if ($column['search']['value'] != '') {
                
                switch ($columns[$index]) {

                    case 'DebitNotes.date': 
                    case 'DebitNotes.date_start':
                    case 'DebitNotes.date_end':
                    case 'DebitNotes.duedate':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'DebitNotes.tipo_comp':
                    case 'DebitNotes.pto_vta': 
                    case 'DebitNotes.num': 
                    case 'DebitNotes.customer_code':
                    case 'DebitNotes.business_id':    
                    case 'DebitNotes.customer_ident':
                    case 'DebitNotes.customer_doc_type':

                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'DebitNotes.total': 

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }

                        break;

                    case 'Users.name':
                    case 'DebitNotes.customer_name':
                    case 'DebitNotes.comments':
                    case 'DebitNotes.address':
                    case 'Customers.email':
                    case 'DebitNotes.city':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'DebitNotes.num':
                    case 'DebitNotes.customer_code':
                    case 'DebitNotes.customer_ident':
                    case 'DebitNotes.customer_doc_type':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'DebitNotes.customer_name':
                    case 'DebitNotes.comments':
                    case 'DebitNotes.address':
                    case 'DebitNotes.city':
                    case 'Customers.email':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {
                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }

    public function findServerSideDataMovesAll(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            'DebitNotes.date',
            'DebitNotes.customer_code',
            'DebitNotes.customer_name',
            'DebitNotes.customer_doc_type',
            'DebitNotes.customer_ident',
            'Users.username',
            '',
            'DebitNotes.tipo_comp',
            'DebitNotes.pto_vta',
            'DebitNotes.num',
            'DebitNotes.cae',
            'DebitNotes.comments',
            'DebitNotes.subtotal',
            'DebitNotes.sum_tax',
            'DebitNotes.total',
            '',
            'DebitNotes.duedate',
            '',
            'DebitNotes.seating_number'
        ];

        $Columns_select = [
            'DebitNotes.date',
            'DebitNotes.customer_code',
            'DebitNotes.customer_name',
            'DebitNotes.customer_doc_type',
            'DebitNotes.customer_ident',
            'Customers.billing_for_service',
            'Customers.email',
            'Users.username',
            'DebitNotes.tipo_comp',
            'DebitNotes.pto_vta',
            'DebitNotes.num',
            'DebitNotes.cae',
            'DebitNotes.comments',
            'DebitNotes.subtotal',
            'DebitNotes.sum_tax',
            'DebitNotes.total',
            'DebitNotes.duedate',
            'DebitNotes.seating_number',
            'DebitNotes.id',
            'DebitNotes.connection_id',
            'DebitNotes.customer_code'
        ];

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        $query = $query->contain([
            'Users',
            'Customers'
        ])
        ->select($Columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'DebitNotes.date': 
                    case 'DebitNotes.duedate':

                        $value = explode('<>',$column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] .' -' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if( $value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] .  ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }

                        break;

                    case 'DebitNotes.tipo_comp':
                    case 'DebitNotes.pto_vta': 
                    case 'DebitNotes.num': 
                    case 'DebitNotes.cae':
                    case 'DebitNotes.customer_code':
                    case 'DebitNotes.customer_doc_type':

                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'DebitNotes.total': 

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }

                        break;

                    case 'Users.username':
                    case 'DebitNotes.comments':
                    case 'DebitNotes.seating_number':
                    case 'DebitNotes.customer_name': 
                    case 'DebitNotes.customer_ident': 
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'DebitNotes.cae':
                    case 'DebitNotes.customer_code':
                    case 'DebitNotes.customer_doc_type':

                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'user.username' :
                    case 'DebitNotes.comments' :
                    case 'DebitNotes.seating_number':
                    case 'DebitNotes.customer_name': 
                    case 'DebitNotes.customer_ident': 
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order(['DebitNotes.date' => 'desc']);
        
        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        $query->toArray();

        return $query;
    }

    public function findRecordsFilteredMovesAll(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            'DebitNotes.date',
            'DebitNotes.customer_code',
            'DebitNotes.customer_name',
            'DebitNotes.customer_doc_type',
            'DebitNotes.customer_ident',
            'Users.username',
            '',
            'DebitNotes.tipo_comp',
            'DebitNotes.pto_vta',
            'DebitNotes.num',
            'DebitNotes.cae',
            'DebitNotes.comments',
            'DebitNotes.subtotal',
            'DebitNotes.sum_tax',
            'DebitNotes.total',
            '',
            'DebitNotes.duedate',
            '',
            'DebitNotes.seating_number'
        ];

        $Columns_select = [
            'DebitNotes.date',
            'DebitNotes.customer_code',
            'DebitNotes.customer_name',
            'DebitNotes.customer_doc_type',
            'DebitNotes.customer_ident',
            'Customers.billing_for_service',
            'Customers.email',
            'Users.username',
            'DebitNotes.tipo_comp',
            'DebitNotes.pto_vta',
            'DebitNotes.num',
            'DebitNotes.cae',
            'DebitNotes.comments',
            'DebitNotes.subtotal',
            'DebitNotes.sum_tax',
            'DebitNotes.total',
            'DebitNotes.duedate',
            'DebitNotes.seating_number',
            'DebitNotes.id',
            'DebitNotes.connection_id',
            'DebitNotes.customer_code'
        ];

        $query = $query->contain([
            'Users',
            'Customers'
        ])
        ->select($Columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'DebitNotes.date': 
                    case 'DebitNotes.duedate':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0].' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }

                        break;

                    case 'DebitNotes.tipo_comp':
                    case 'DebitNotes.pto_vta': 
                    case 'DebitNotes.num': 
                    case 'DebitNotes.cae':
                    case 'DebitNotes.customer_code':
                    case 'DebitNotes.customer_doc_type':

                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'DebitNotes.total': 

                        $value = explode('<>',$column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }

                        break;

                    case 'Users.username':
                    case 'DebitNotes.comments':
                    case 'DebitNotes.seating_number':
                    case 'DebitNotes.customer_name': 
                    case 'DebitNotes.customer_ident': 
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'DebitNotes.num':
                    case 'DebitNotes.cae':
                    case 'DebitNotes.customer_code':
                    case 'DebitNotes.customer_doc_type':

                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Users.username' :
                    case 'DebitNotes.comments' :
                    case 'DebitNotes.seating_number':
                    case 'DebitNotes.customer_name': 
                    case 'DebitNotes.customer_ident': 
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }

    // public function afterSave($event, $entity, $options)
    // {
    //     $detail = '';

    //     if ($entity->isNew()) {

    //         $paraments = $_SESSION['paraments'];

    //         foreach ($paraments->invoicing->business as $b) {
    //             if ($b->id == $entity->business_id) {
    //                  $business = $b->name . ' (' . $b->address . ')';
    //             }
    //         }

    //         $action = 'Creación de Nota de Débito';
    //         $detail .= 'Fecha: ' . $entity->date->format('d/m/Y') . PHP_EOL;
    //         $detail .= 'Desde: ' . $entity->date_start->format('d/m/Y') . PHP_EOL;
    //         $detail .= 'Hasta: ' . $entity->date_end->format('d/m/Y') . PHP_EOL;
    //         $detail .= 'Vencimiento: ' . $entity->duedate->format('d/m/Y') . PHP_EOL;
    //         $detail .= 'Número: ' . $entity->num . PHP_EOL;
    //         $detail .= 'Pto de Vta: ' . $entity->pto_vta . PHP_EOL;
    //         $detail .= 'Tipo de Comp.: ' . $_SESSION['afip_codes']['comprobantes'][$entity->tipo_comp] . PHP_EOL;
    //         $detail .= 'Comentarios: ' . $entity->comments . PHP_EOL;
    //         $detail .= 'Total: ' . number_format($entity->total, 2, ',', '.') . PHP_EOL;
    //         $detail .= 'Empresa: ' . $business . PHP_EOL;
    //         $detail .= 'Cond. de Vta: ' . $_SESSION['afip_codes']['cond_venta'][$entity->cond_vta] . PHP_EOL;

    //         $actionLog = TableRegistry::get('ActionLog');
    //         $query = $actionLog->query();
    //         $query->insert([
    //             'created', 
    //             'detail',
    //             'user_id',
    //             'action',
    //             'customer_code'
    //         ])
    //         ->values([
    //             'created' => Time::now(), 
    //             'detail' => $detail,
    //             'user_id' => $_SESSION['Auth']['User']['id'],
    //             'action' => $action,
    //             'customer_code' => isset($entity->customer_code) ? $entity->customer_code : null,
    //         ])
    //         ->execute();
    //     }
    // }
}
