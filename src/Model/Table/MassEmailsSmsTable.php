<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Log\Log;

/**
 * MassEmailsSms Model
 *
 * @property \App\Model\Table\ConnectionsTable|\Cake\ORM\Association\BelongsTo $Connections
 * @property \App\Model\Table\BloquesTable|\Cake\ORM\Association\BelongsTo $Bloques
 *
 * @method \App\Model\Entity\MassEmailsSm get($primaryKey, $options = [])
 * @method \App\Model\Entity\MassEmailsSm newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MassEmailsSm[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MassEmailsSm|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MassEmailsSm patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MassEmailsSm[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MassEmailsSm findOrCreate($search, callable $callback = null, $options = [])
 */
class MassEmailsSmsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('mass_emails_sms');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Connections', [
            'foreignKey' => 'connection_id'
        ]);

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_code'
        ]);

        $this->belongsTo('MassEmailsBlocks', [
            'foreignKey' => 'bloque_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('message')
            ->requirePresence('message', 'create')
            ->notEmpty('message');

        $validator
            ->integer('customer_code')
            ->allowEmpty('customer_code');

        $validator
            ->scalar('status')
            ->allowEmpty('status');

        $validator
            ->integer('business_billing')
            ->allowEmpty('business_billing');

        $validator
            ->boolean('invoice_attach')
            ->requirePresence('invoice_attach', 'create')
            ->notEmpty('invoice_attach');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['connection_id'], 'Connections'));
        $rules->add($rules->existsIn(['bloque_id'], 'MassEmailsBlocks'));

        return $rules;
    }

    public function findServerSideData(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            '',                                          //0
            'MassEmailsBlocks.template_name',            //1
            'MassEmailsBlocks.id',                       //2
            'MassEmailsSms.message',                     //3
            'MassEmailsSms.subject',                     //4
            'MassEmailsSms.status',                      //5
            'MassEmailsBlocks.created',                  //6
            'Customers.name',                            //7
            'Customers.ident',                           //8
            'Customers.code',                            //9
            'MassEmailsSms.email',                       //10
            'MassEmailsSms.business_billing',            //11
            'MassEmailsBlocks.user_id',                  //12
            'MassEmailsSms.invoice_attach',              //13
            'MassEmailsSms.invoice_impagas',             //14
            'MassEmailsSms.invoice_periode',             //15
            'MassEmailsSms.receipt_attach',              //16
            'MassEmailsSms.receipt_periode',             //17
            'MassEmailsBlocks.email_account',            //18
            'MassEmailsSms.account_summary_attach',      //19
            'MassEmailsSms.payment_link_mercadopago',    //20
            'MassEmailsSms.payment_link_cuentadigital',  //21
        ];

        $columns_select = [
            'MassEmailsSms.id',                          //0
            'MassEmailsBlocks.template_name',            //1
            'MassEmailsBlocks.id',                       //2
            'MassEmailsSms.message',                     //3
            'MassEmailsSms.subject',                     //4
            'MassEmailsSms.status',                      //5
            'MassEmailsBlocks.created',                  //6
            'Customers.name',                            //7
            'Customers.doc_type',                        //8
            'Customers.ident',                           //9
            'Customers.code',                            //10
            'MassEmailsSms.email',                       //11
            'MassEmailsSms.business_billing',            //12
            'MassEmailsBlocks.user_id',                  //13
            'MassEmailsSms.invoice_attach',              //14
            'MassEmailsSms.invoice_impagas',             //15
            'MassEmailsSms.invoice_periode',             //16
            'MassEmailsSms.receipt_attach',              //17
            'MassEmailsSms.receipt_periode',             //18
            'MassEmailsBlocks.email_account',            //19
            'MassEmailsSms.account_summary_attach',      //20
            'MassEmailsSms.payment_link_mercadopago',    //21
            'MassEmailsSms.payment_link_cuentadigital',  //22
        ];

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] =>  $o['dir']];
        }

        $query = $query->contain([
            'MassEmailsBlocks',
            'Customers',
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'MassEmailsBlocks.created':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2]  . '-' . $value[1][1] . '-' .  $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'MassEmailsSms.invoice_periode':
                    case 'MassEmailsSms.receipt_periode': 
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][1] . '-' . $value[0][0];
                            $value[1] = $value[1][1]  . '-' . $value[1][0];
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][1] . '-' . $value[0][0];
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][1] . '-' . $value[1][0];
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Customers.code':
                    case 'MassEmailsSms.status':
                    case 'MassEmailsBlocks.id':
                    case 'MassEmailsBlocks.user_id':
                    case 'MassEmailsSms.business_billing': 
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'MassEmailsSms.invoice_attach':
                    case 'MassEmailsSms.invoice_impagas':
                    case 'MassEmailsSms.receipt_attach':
                    case 'MassEmailsSms.account_summary_attach':
                    case 'MassEmailsSms.payment_link_mercadopago':
                    case 'MassEmailsSms.payment_link_cuentadigital':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? TRUE : FALSE];
                        break;

                    default: 
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {
                    case 'MassEmailsBlocks.created':
                    case 'MassEmailsSms.invoice_periode':
                    case 'MassEmailsSms.receipt_periode':
                        break;

                    case 'Customers.code': 
                    case 'MassEmailsBlocks.id':
                    case 'MassEmailsSms.business_billing':
                    case 'MassEmailsSms.status':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'MassEmailsBlocks.template_name':
                    case 'MassEmailsSms.message':
                    case 'Customers.name':
                    case 'Customers.ident':
                    case 'MassEmailsBlocks.email_account':
                    case 'MassEmailsSms.email':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order($order);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        return $query;
    }

    public function findRecordsFiltered(Query $query, array $options)
    {
        $params = $options['params'];

        $where = [];
        $between = [];
        $orWhere = [];

        $columns = [
            '',                                          //0
            'MassEmailsBlocks.template_name',            //1
            'MassEmailsBlocks.id',                       //2
            'MassEmailsSms.message',                     //3
            'MassEmailsSms.subject',                     //4
            'MassEmailsSms.status',                      //5
            'MassEmailsBlocks.created',                  //6
            'Customers.name',                            //7
            'Customers.ident',                           //8
            'Customers.code',                            //9
            'MassEmailsSms.email',                       //10
            'MassEmailsSms.business_billing',            //11
            'MassEmailsBlocks.user_id',                  //12
            'MassEmailsSms.invoice_attach',              //13
            'MassEmailsSms.invoice_impagas',             //14
            'MassEmailsSms.invoice_periode',             //15
            'MassEmailsSms.receipt_attach',              //16
            'MassEmailsSms.receipt_periode',             //17
            'MassEmailsBlocks.email_account',            //18
            'MassEmailsSms.account_summary_attach',      //19
            'MassEmailsSms.payment_link_mercadopago',    //20
            'MassEmailsSms.payment_link_cuentadigital',  //21
        ];

        $columns_select = [
            'MassEmailsSms.id',                          //0
            'MassEmailsBlocks.template_name',            //1
            'MassEmailsBlocks.id',                       //2
            'MassEmailsSms.message',                     //3
            'MassEmailsSms.subject',                     //4
            'MassEmailsSms.status',                      //5
            'MassEmailsBlocks.created',                  //6
            'Customers.name',                            //7
            'Customers.doc_type',                        //8
            'Customers.ident',                           //9
            'Customers.code',                            //10
            'MassEmailsSms.email',                       //11
            'MassEmailsSms.business_billing',            //12
            'MassEmailsBlocks.user_id',                  //13
            'MassEmailsSms.invoice_attach',              //14
            'MassEmailsSms.invoice_impagas',             //15
            'MassEmailsSms.invoice_periode',             //16
            'MassEmailsSms.receipt_attach',              //17
            'MassEmailsSms.receipt_periode',             //18
            'MassEmailsBlocks.email_account',            //19
            'MassEmailsSms.account_summary_attach',      //20
            'MassEmailsSms.payment_link_mercadopago',    //21
            'MassEmailsSms.payment_link_cuentadigital',  //22
        ];

        $query = $query->contain([
            'MassEmailsBlocks',
            'Customers',
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'MassEmailsBlocks.created':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2]  . '-' . $value[1][1] . '-' .  $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'MassEmailsSms.invoice_periode':
                    case 'MassEmailsSms.receipt_periode':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][1] . '-' . $value[0][0];
                            $value[1] = $value[1][1]  . '-' . $value[1][0];
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][1] . '-' . $value[0][0];
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][1] . '-' . $value[1][0];
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Customers.code':
                    case 'MassEmailsSms.status':
                    case 'MassEmailsBlocks.id':
                    case 'MassEmailsBlocks.user_id':
                    case 'MassEmailsSms.business_billing':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'MassEmailsSms.invoice_attach':
                    case 'MassEmailsSms.invoice_impagas':
                    case 'MassEmailsSms.receipt_attach':
                    case 'MassEmailsSms.account_summary_attach':
                    case 'MassEmailsSms.payment_link_mercadopago':
                    case 'MassEmailsSms.payment_link_cuentadigital':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? TRUE : FALSE];
                        break;

                    default: 
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {
                    case 'MassEmailsBlocks.created':
                    case 'MassEmailsSms.invoice_periode':
                    case 'MassEmailsSms.receipt_periode':
                        break;

                    case 'Customers.code': 
                    case 'MassEmailsBlocks.id':
                    case 'MassEmailsSms.business_billing':
                    case 'MassEmailsSms.status':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'MassEmailsBlocks.template_name':
                    case 'MassEmailsSms.message':
                    case 'Customers.name':
                    case 'Customers.ident':
                    case 'MassEmailsBlocks.email_account':
                    case 'MassEmailsSms.email':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {
            foreach ($between as $b) {
                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }
}
