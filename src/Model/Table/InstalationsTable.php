<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Instalations Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Connections
 * @property \Cake\ORM\Association\BelongsTo $Packages
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $PackagesSales
 * @property \Cake\ORM\Association\BelongsToMany $Articles
 *
 * @method \App\Model\Entity\Instalation get($primaryKey, $options = [])
 * @method \App\Model\Entity\Instalation newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Instalation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Instalation|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Instalation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Instalation[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Instalation findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class InstalationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('instalations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Connections', [
            'foreignKey' => 'connection_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Packages', [
            'foreignKey' => 'package_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_code',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('PackagesSales', [
            'foreignKey' => 'instalation_id'
        ]);
        $this->belongsToMany('Articles', [
            'foreignKey' => 'instalation_id',
            'targetForeignKey' => 'article_id',
            'joinTable' => 'instalations_articles'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('comments');

        // $validator
        //     ->boolean('deleted')
        //     ->requirePresence('deleted', 'create')
        //     ->notEmpty('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['connection_id'], 'Connections'));
        $rules->add($rules->existsIn(['package_id'], 'Packages'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
