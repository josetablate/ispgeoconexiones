<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Log\Log;

/**
 * CobrodigitalAccounts Model
 *
 * @property \App\Model\Table\PaymentGetwaysTable|\Cake\ORM\Association\BelongsTo $PaymentGetways
 *
 * @method \App\Model\Entity\CobrodigitalAccount get($primaryKey, $options = [])
 * @method \App\Model\Entity\CobrodigitalAccount newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CobrodigitalAccount[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CobrodigitalAccount|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CobrodigitalAccount|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CobrodigitalAccount patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CobrodigitalAccount[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CobrodigitalAccount findOrCreate($search, callable $callback = null, $options = [])
 */
class CobrodigitalAccountsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cobrodigital_accounts');
        $this->setDisplayField('customer_code');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_code',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('barcode')
            ->maxLength('barcode', 45)
            ->allowEmpty('barcode');

        $validator
            ->scalar('electronic_code')
            ->maxLength('electronic_code', 45)
            ->allowEmpty('electronic_code');

        $validator
            ->boolean('used')
            ->requirePresence('used', 'create')
            ->notEmpty('used');

        $validator
            ->boolean('deleted')
            ->requirePresence('deleted', 'create')
            ->notEmpty('deleted');

        $validator
            ->scalar('id_comercio')
            ->maxLength('id_comercio', 10)
            ->requirePresence('id_comercio', 'create')
            ->notEmpty('id_comercio');

        $validator
            ->integer('customer_code')
            ->allowEmpty('customer_code');

        $validator
            ->dateTime('date_exported')
            ->allowEmpty('date_exported');

        $validator
            ->scalar('firstname')
            ->maxLength('firstname', 150)
            ->allowEmpty('firstname');

        $validator
            ->scalar('lastname')
            ->maxLength('lastname', 50)
            ->allowEmpty('lastname');

        $validator
            ->scalar('cbu')
            ->maxLength('cbu', 45)
            ->allowEmpty('cbu');

        $validator
            ->scalar('cuit')
            ->maxLength('cuit', 45)
            ->allowEmpty('cuit');

        $validator
            ->email('email')
            ->allowEmpty('email');

        return $validator;
    }

    public function findServerSideData(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        if ($params['payment_getway_id'] == 99) {

            // tarjeta de cobranza
            $columns = [
                '',                                     //0
                'CobrodigitalAccounts.barcode',         //1
                'CobrodigitalAccounts.electronic_code', //2
                'CobrodigitalAccounts.id_comercio',     //3
                'Customers.name',                       //4
                'Customers.ident',                      //5
                'Customers.code',                       //6
                'CobrodigitalAccounts.date_exported',   //7
            ];

            $columns_select = [
                'CobrodigitalAccounts.barcode',         //0
                'CobrodigitalAccounts.electronic_code', //1
                'CobrodigitalAccounts.id_comercio',     //2
                'Customers.name',                       //3
                'Customers.ident',                      //4
                'Customers.code',                       //5
                'Customers.doc_type',                   //6
                'CobrodigitalAccounts.date_exported',   //7
                'CobrodigitalAccounts.id',              //8
            ];

        } else if ($params['payment_getway_id'] == 104) {
            // debito automatico
            $columns = [
                '',                                     //0
                'CobrodigitalAccounts.firstname',       //1
                'CobrodigitalAccounts.lastname',        //2
                'CobrodigitalAccounts.cbu',             //3
                'CobrodigitalAccounts.cuit',            //4
                'CobrodigitalAccounts.email',           //5
                'CobrodigitalAccounts.id_comercio',     //6
                'CobrodigitalAccounts.date_exported',   //7
            ];

            $columns_select = [
                'CobrodigitalAccounts.firstname',       //0
                'CobrodigitalAccounts.lastname',        //1
                'CobrodigitalAccounts.cbu',             //2
                'CobrodigitalAccounts.cuit',            //3
                'CobrodigitalAccounts.email',           //4
                'CobrodigitalAccounts.id_comercio',     //5
                'CobrodigitalAccounts.date_exported',   //6
                'CobrodigitalAccounts.id',              //7
            ];
        }

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] =>  $o['dir']];
        }

        $query = $query->contain([
            'Customers'
        ])
        ->select($columns_select); 

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'CobrodigitalAccounts.date_exported':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2]  . '-' . $value[1][1] . '-' .  $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] .' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Customers.code':
                    case 'CobrodigitalAccounts.id_comercio':
                    case 'CobrodigitalAccounts.cbu':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    //case 'CobrodigitalAccounts.used':
                    case 'CobrodigitalAccounts.deleted':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? true : false];
                        break;

                    case 'CobrodigitalAccounts.barcode':
                    case 'CobrodigitalAccounts.electronic_code':
                    case 'CobrodigitalAccounts.firstname':
                    case 'CobrodigitalAccounts.lastname':
                    case 'CobrodigitalAccounts.email':
                    case 'CobrodigitalAccounts.cuit':
                    case 'Customers.ident':
                    case 'Customers.name':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {
                    case 'CobrodigitalAccounts.date_exported':
                        break;

                    case 'Customers.code': 
                    case 'CobrodigitalAccounts.cbu':
                    case 'CobrodigitalAccounts.id_comercio':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Customers.name': 
                    case 'Customers.ident':
                    case 'CobrodigitalAccounts.email':
                    case 'CobrodigitalAccounts.barcode':
                    case 'CobrodigitalAccounts.electronic_code':
                    case 'CobrodigitalAccounts.firstname':
                    case 'CobrodigitalAccounts.lastname':
                    case 'CobrodigitalAccounts.cuit':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order($order);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        $payment_getway = $_SESSION['payment_getway'];

        $query->map(function ($row) use ($payment_getway) { // map() is a collection method, it executes the query

            $id_comercio_name = "";
            foreach ($payment_getway->config->cobrodigital->credentials as $credential) {
                if ($credential->idComercio == $row->id_comercio) {
                    $id_comercio_name = '(' . $credential->idComercio . ') ' . $credential->name;
                    break;
                }
            }
            $row->id_comercio = $id_comercio_name;
        })
        ->toArray();

        return $query;
    }

    public function findRecordsFiltered(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        if ($params['payment_getway_id'] == 99) {

            // tarjeta de cobranza
            $columns = [
                '',                                     //0
                'CobrodigitalAccounts.barcode',         //1
                'CobrodigitalAccounts.electronic_code', //2
                'CobrodigitalAccounts.id_comercio',     //3
                'Customers.name',                       //4
                'Customers.ident',                      //5
                'Customers.code',                       //6
                'CobrodigitalAccounts.date_exported',   //7
            ];

            $columns_select = [
                'CobrodigitalAccounts.barcode',         //0
                'CobrodigitalAccounts.electronic_code', //1
                'CobrodigitalAccounts.id_comercio',     //2
                'Customers.name',                       //3
                'Customers.ident',                      //4
                'Customers.code',                       //5
                'Customers.doc_type',                   //6
                'CobrodigitalAccounts.date_exported',   //7
                'CobrodigitalAccounts.id',              //8
            ];

        } else if ($params['payment_getway_id'] == 104) {
            // debito automatico
            $columns = [
                '',                                     //0
                'CobrodigitalAccounts.firstname',       //1
                'CobrodigitalAccounts.lastname',        //2
                'CobrodigitalAccounts.cbu',             //3
                'CobrodigitalAccounts.cuit',            //4
                'CobrodigitalAccounts.email',           //5
                'CobrodigitalAccounts.id_comercio',     //6
                'CobrodigitalAccounts.date_exported',   //7
            ];

            $columns_select = [
                'CobrodigitalAccounts.firstname',       //0
                'CobrodigitalAccounts.lastname',        //1
                'CobrodigitalAccounts.cbu',             //2
                'CobrodigitalAccounts.cuit',            //3
                'CobrodigitalAccounts.email',           //4
                'CobrodigitalAccounts.id_comercio',     //5
                'CobrodigitalAccounts.date_exported',   //6
                'CobrodigitalAccounts.id',              //7
            ];
        }

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] =>  $o['dir']];
        }

        $query = $query->contain([
            'Customers'
        ])->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'CobrodigitalAccounts.date_exported':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2]  . '-' . $value[1][1] . '-' .  $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] .' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Customers.code':
                    case 'CobrodigitalAccounts.id_comercio':
                    case 'CobrodigitalAccounts.cbu':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    //case 'CobrodigitalAccounts.used':
                    case 'CobrodigitalAccounts.deleted':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? true : false];
                        break;

                    case 'CobrodigitalAccounts.barcode':
                    case 'CobrodigitalAccounts.electronic_code':
                    case 'CobrodigitalAccounts.firstname':
                    case 'CobrodigitalAccounts.lastname':
                    case 'CobrodigitalAccounts.email':
                    case 'CobrodigitalAccounts.cuit':
                    case 'Customers.ident':
                    case 'Customers.name':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {
                    case 'CobrodigitalAccounts.date_exported':
                        break;

                    case 'Customers.code': 
                    case 'CobrodigitalAccounts.cbu':
                    case 'CobrodigitalAccounts.id_comercio':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Customers.name': 
                    case 'Customers.ident':
                    case 'CobrodigitalAccounts.email':
                    case 'CobrodigitalAccounts.barcode':
                    case 'CobrodigitalAccounts.electronic_code':
                    case 'CobrodigitalAccounts.firstname':
                    case 'CobrodigitalAccounts.lastname':
                    case 'CobrodigitalAccounts.cuit':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }


        if (count($between) > 0) {
            foreach ($between as $b) {
                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $payment_getway = $_SESSION['payment_getway'];

        $query->map(function ($row) use ($payment_getway) { // map() is a collection method, it executes the query

            $id_comercio_name = "";
            foreach ($payment_getway->config->cobrodigital->credentials as $credential) {
                if ($credential->idComercio == $row->id_comercio) {
                    $id_comercio_name = '(' . $credential->idComercio . ') ' . $credential->name;
                    break;
                }
            }
            $row->id_comercio = $id_comercio_name;
        })
        ->toArray();

        return $query->count();
    }
}
