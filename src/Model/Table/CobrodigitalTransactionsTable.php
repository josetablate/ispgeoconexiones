<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Log\Log;

/**
 * CobrodigitalTransactions Model
 *
 * @property \App\Model\Table\BloquesTable|\Cake\ORM\Association\BelongsTo $Bloques
 *
 * @method \App\Model\Entity\CobrodigitalTransaction get($primaryKey, $options = [])
 * @method \App\Model\Entity\CobrodigitalTransaction newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CobrodigitalTransaction[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CobrodigitalTransaction|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CobrodigitalTransaction|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CobrodigitalTransaction patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CobrodigitalTransaction[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CobrodigitalTransaction findOrCreate($search, callable $callback = null, $options = [])
 */
class CobrodigitalTransactionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cobrodigital_transactions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_code',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('id_transaccion')
            ->maxLength('id_transaccion', 100)
            ->requirePresence('id_transaccion', 'create')
            ->notEmpty('id_transaccion');

        $validator
            ->dateTime('fecha')
            ->requirePresence('fecha', 'create')
            ->notEmpty('fecha');

        $validator
            ->scalar('codigo_barras')
            ->maxLength('codigo_barras', 45)
            ->allowEmpty('codigo_barras');

        $validator
            ->scalar('identificador')
            ->maxLength('identificador', 255)
            ->allowEmpty('identificador');

        $validator
            ->integer('nro_boleta')
            ->allowEmpty('nro_boleta');

        $validator
            ->scalar('nombre')
            ->maxLength('nombre', 200)
            ->allowEmpty('nombre');

        $validator
            ->scalar('info')
            ->maxLength('info', 150)
            ->allowEmpty('info');

        $validator
            ->scalar('concepto')
            ->maxLength('concepto', 150)
            ->allowEmpty('concepto');

        $validator
            ->decimal('bruto')
            ->allowEmpty('bruto');

        $validator
            ->decimal('comision')
            ->allowEmpty('comision');

        $validator
            ->decimal('neto')
            ->allowEmpty('neto');

        $validator
            ->decimal('saldo_acumulado')
            ->allowEmpty('saldo_acumulado');

        $validator
            ->boolean('estado')
            ->requirePresence('estado', 'create')
            ->notEmpty('estado');

        $validator
            ->scalar('comments')
            ->maxLength('comments', 255)
            ->allowEmpty('comments');

        $validator
            ->integer('customer_code')
            ->allowEmpty('customer_code');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        return $rules;
    }

    public function findServerSideDataTransactions(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            'CobrodigitalTransactions.fecha',             //0
            'CobrodigitalTransactions.payment_getway_id', //1 metodo de pago
            'CobrodigitalTransactions.estado',            //2
            'Customers.code',                             //3
            'Customers.name',                             //4
            'Customers.ident',                            //5
            'CobrodigitalTransactions.id_transaccion',    //6
            'CobrodigitalTransactions.bruto',             //7
            'CobrodigitalTransactions.comments',          //8
            'CobrodigitalTransactions.codigo_barras',     //9
            'CobrodigitalTransactions.receipt_number',   //10
        ];

        $columns_select = [
            'CobrodigitalTransactions.id',                //0
            'CobrodigitalTransactions.fecha',             //1
            'CobrodigitalTransactions.payment_getway_id', //2
            'CobrodigitalTransactions.estado',            //3
            'Customers.code',                             //4
            'Customers.name',                             //5
            'Customers.ident',                            //6
            'Customers.doc_type',                         //7
            'CobrodigitalTransactions.id_transaccion',    //8
            'CobrodigitalTransactions.bruto',             //9
            'CobrodigitalTransactions.comments',          //10
            'CobrodigitalTransactions.codigo_barras',     //11
            'CobrodigitalTransactions.receipt_number',    //12
            'CobrodigitalTransactions.receipt_id',        //13
        ];

        //paginacion

        if ($params['length'] > 0) {
            
            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        $query = $query->contain([
            'Customers'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'CobrodigitalTransactions.fecha':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] .'-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Customers.code':
                    case 'CobrodigitalTransactions.payment_getway_id':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'CobrodigitalTransactions.bruto':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'CobrodigitalTransactions.estado':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? true : false];
                        break;

                    case 'CobrodigitalTransactions.comments':
                    case 'Customers.name':
                    case 'CobrodigitalTransactions.id_transaccion':
                    case 'CobrodigitalTransactions.codigo_barras':
                    case 'CobrodigitalTransactions.receipt_number':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);
 
        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Customers.code':
                    case 'CobrodigitalTransactions.payment_getway_id':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'CobrodigitalTransactions.comments':
                    case 'Customers.name':
                    case 'CobrodigitalTransactions.id_transaccion':
                    case 'CobrodigitalTransactions.codigo_barras':
                    case 'CobrodigitalTransactions.receipt_number':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order(['CobrodigitalTransactions.fecha' => 'desc']);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        $query->toArray();

        return $query;
    }

    public function findRecordsFilteredTransactions(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            'CobrodigitalTransactions.fecha',             //0
            'CobrodigitalTransactions.payment_getway_id', //1 metodo de pago
            'CobrodigitalTransactions.estado',            //2
            'Customers.code',                             //3
            'Customers.name',                             //4
            'Customers.ident',                            //5
            'CobrodigitalTransactions.id_transaccion',    //6
            'CobrodigitalTransactions.bruto',             //7
            'CobrodigitalTransactions.comments',          //8
            'CobrodigitalTransactions.codigo_barras',     //9
            'CobrodigitalTransactions.receipt_number',   //10
        ];

        $columns_select = [
            'CobrodigitalTransactions.id',                //0
            'CobrodigitalTransactions.fecha',             //1
            'CobrodigitalTransactions.payment_getway_id', //2
            'CobrodigitalTransactions.estado',            //3
            'Customers.code',                             //4
            'Customers.name',                             //5
            'Customers.ident',                            //6
            'Customers.doc_type',                         //7
            'CobrodigitalTransactions.id_transaccion',    //8
            'CobrodigitalTransactions.bruto',             //9
            'CobrodigitalTransactions.comments',          //10
            'CobrodigitalTransactions.codigo_barras',     //11
            'CobrodigitalTransactions.receipt_number',    //12
            'CobrodigitalTransactions.receipt_id',        //13
        ];

        $query = $query->contain([
            'Customers'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'CobrodigitalTransactions.fecha':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Customers.code':
                    case 'CobrodigitalTransactions.payment_getway_id':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'CobrodigitalTransactions.bruto':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'CobrodigitalTransactions.estado':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? true : false];
                        break;

                    case 'CobrodigitalTransactions.comments':
                    case 'CobrodigitalTransactions.id_transaccion':
                    case 'Customers.name':
                    case 'CobrodigitalTransactions.codigo_barras':
                    case 'CobrodigitalTransactions.receipt_number':

                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);
 
        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Customers.code':
                    case 'CobrodigitalTransactions.payment_getway_id':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'CobrodigitalTransactions.comments':
                    case 'Customers.name':
                    case 'CobrodigitalTransactions.id_transaccion':
                    case 'CobrodigitalTransactions.codigo_barras':
                    case 'CobrodigitalTransactions.receipt_number':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }
}
