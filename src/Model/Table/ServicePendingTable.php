<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ServicePending Model
 *
 * @property \App\Model\Table\PresalesTable|\Cake\ORM\Association\BelongsTo $Presales
 * @property \App\Model\Table\ServicesTable|\Cake\ORM\Association\BelongsTo $Services
 * @property \App\Model\Table\DiscuntsTable|\Cake\ORM\Association\BelongsTo $Discunts
 *
 * @method \App\Model\Entity\ServicePending get($primaryKey, $options = [])
 * @method \App\Model\Entity\ServicePending newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ServicePending[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ServicePending|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ServicePending patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ServicePending[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ServicePending findOrCreate($search, callable $callback = null, $options = [])
 */
class ServicePendingTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('service_pending');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Presales', [
            'foreignKey' => 'presale_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Services', [
            'foreignKey' => 'service_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Discounts', [
            'foreignKey' => 'discount_id',
            'joinType' => 'LEFT'
        ]);
        $this->hasMany('Tickets', [
            'foreignKey' => 'service_pending_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('customer_code')
            ->requirePresence('customer_code', 'create')
            ->notEmpty('customer_code');

        $validator
            ->dateTime('used')
            ->allowEmpty('used');

        $validator
            ->scalar('address')
            ->requirePresence('address', 'create')
            ->notEmpty('address');

        $validator
            ->numeric('lat')
            ->allowEmpty('lat');

        $validator
            ->numeric('lng')
            ->allowEmpty('lng');

        $validator
            ->dateTime('installation_date')
            ->requirePresence('installation_date', 'create')
            ->notEmpty('installation_date');

        $validator
            ->scalar('availability')
            ->allowEmpty('availability');

        $validator
            ->scalar('service_name')
            ->allowEmpty('service_name');

        $validator
            ->decimal('service_total')
            ->allowEmpty('service_total');

        $validator
            ->scalar('discount_name')
            ->allowEmpty('discount_name');

        $validator
            ->decimal('discount_total')
            ->allowEmpty('discount_total');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['presale_id'], 'Presales'));
        $rules->add($rules->existsIn(['service_id'], 'Services'));
        $rules->add($rules->existsIn(['discount_id'], 'Discounts'));

        return $rules;
    }
}
