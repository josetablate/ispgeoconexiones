<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ConnectionsDebtsMonth Model
 *
 * @property \App\Model\Table\ConnectionsTable|\Cake\ORM\Association\BelongsTo $Connections
 *
 * @method \App\Model\Entity\ConnectionsDebtsMonth get($primaryKey, $options = [])
 * @method \App\Model\Entity\ConnectionsDebtsMonth newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ConnectionsDebtsMonth[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ConnectionsDebtsMonth|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ConnectionsDebtsMonth|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ConnectionsDebtsMonth patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ConnectionsDebtsMonth[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ConnectionsDebtsMonth findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ConnectionsDebtsMonthTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('connections_debts_month');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Connections', [
            'foreignKey' => 'connection_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('business_billing')
            ->maxLength('business_billing', 45)
            ->requirePresence('business_billing', 'create')
            ->notEmpty('business_billing');

        $validator
            ->scalar('period')
            ->maxLength('period', 45)
            ->requirePresence('period', 'create')
            ->notEmpty('period');

        $validator
            ->decimal('total')
            ->allowEmpty('total');

        $validator
            ->decimal('totalx')
            ->allowEmpty('totalx');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['connection_id'], 'Connections'));

        return $rules;
    }
}
