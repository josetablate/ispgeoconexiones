<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Labels Model
 *
 * @property \App\Model\Table\ConnectionsTable|\Cake\ORM\Association\BelongsToMany $Connections
 * @property \App\Model\Table\CustomersTable|\Cake\ORM\Association\BelongsToMany $Customers
 *
 * @method \App\Model\Entity\Label get($primaryKey, $options = [])
 * @method \App\Model\Entity\Label newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Label[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Label|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Label patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Label[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Label findOrCreate($search, callable $callback = null, $options = [])
 */
class LabelsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('labels');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        
        // $this->belongsTo('Labels', ['foreignKey' => 'label_id',  'joinType' => 'INNER']);
        
        $this->belongsToMany('Customers',
        [
            'targetForeignKey' => 'customer_code',
            'foreignKey' => 'label_id',
            'joinTable' => 'customers_labels',
        ]);
        
        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('text')
            ->requirePresence('text', 'create')
            ->notEmpty('text');

        $validator
            ->integer('entity')
            ->requirePresence('entity', 'create')
            ->notEmpty('entity');

        return $validator;
    }
}
