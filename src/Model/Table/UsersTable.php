<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\HasMany $CashEntities
 * @property \Cake\ORM\Association\HasMany $Connections
 * @property \Cake\ORM\Association\HasMany $Debts
 * @property \Cake\ORM\Association\HasMany $Docs
 * @property \Cake\ORM\Association\HasMany $Instalations
 * @property \Cake\ORM\Association\HasMany $Logs
 * @property \Cake\ORM\Association\HasMany $Payments
 * @property \Cake\ORM\Association\HasMany $RoadMap
 * @property \Cake\ORM\Association\HasMany $Seating
 * @property \Cake\ORM\Association\HasMany $Stores
 * @property \Cake\ORM\Association\HasMany $Tickets
 * @property \Cake\ORM\Association\HasMany $TicketsRecords
 * @property \Cake\ORM\Association\BelongsToMany $Roles
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
    
      public $recursive = -1;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('username');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('Roles', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'role_id',
            'joinTable' => 'roles_users'
        ]);
        
        $this->hasOne('CashEntities', [
            'foreignKey' => 'user_id',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

         $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name','Este campo no puede estar vacio.');

        $validator
            ->requirePresence('username', 'create')
            ->notEmpty('username','Este campo no puede estar vacio.');

        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password','Este campo no puede estar vacio.', 'create');

        $validator
            ->allowEmpty('phone');

        $validator
            ->boolean('enabled')
            ->requirePresence('enabled', 'create')
            ->notEmpty('enabled');

        $validator
            ->allowEmpty('description');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username'], 'Este nombre de usuario ya existe.'));
        return $rules;
    }

    public function afterSave($event, $entity, $options)
    {
        $ok = false;

    	$detail = '';

    	if (!$entity->isNew() ) {

    		$action = 'Edición de Usuario';

    		$dirtyFields = $entity->getDirty();

    		$dirtyFields = array_diff($dirtyFields, ["modified"]);
    		$dirtyFields = array_diff($dirtyFields, ["password"]);

    		if (count($dirtyFields) == 0) {
    		    return true;
    		}

    		foreach ($dirtyFields as $dirtyField) {

    			switch ($dirtyField) {

    				case 'name': 
    					$detail .= 'Nombre: ' . $entity->getOriginal('name') . ' => ' . $entity->name . PHP_EOL; 
    					$ok = true;
    					break;
    				case 'username': 
    					$detail .= 'Usuario: ' . $entity->getOriginal('username') . ' => ' . $entity->username . PHP_EOL; 
    					$ok = true;
    					break;
    				case 'phone': 
    					$detail .= 'Teléfono: ' . $entity->getOriginal('phone') . ' => ' . $entity->phone . PHP_EOL; 
    					$ok = true;
                        break;
                    case 'email': 
    					$detail .= 'Correo: ' . $entity->getOriginal('email') . ' => ' . $entity->email . PHP_EOL; 
    					$ok = true;
    					break;
    				case 'enabled':
    					$detail .= 'Habilitado: ' . ($entity->getOriginal('enabled') ? 'Si' : 'No') . ' => ' . ($entity->enabled ? 'Si' : 'No') . PHP_EOL; 
    					$ok = true;
    					break;
    				case 'deleted':				
    					if ($entity->deleted) $action = 'Eliminación de usuario'; else $action = 'Restauración de usuario';
    					$ok = true;
    					break;
    				case 'description': 
    					$detail .= 'Descripción: ' . $entity->getOriginal('description') . ' => ' . $entity->description . PHP_EOL; 
    					$ok = true;
    					break;
    		   }
    		}
    	} else {

    		$action = 'Creación de Usuario';
    		$detail = 'Nombre: ' . $entity->name . PHP_EOL;
    		$detail .= 'Usuario: ' . $entity->username . PHP_EOL;
            $detail .= 'Teléfono: ' . $entity->phone . PHP_EOL;
            $detail .= 'Correo: ' . $entity->email . PHP_EOL;
    		$detail .= 'Habilitado: ' . ($entity->enabled ? 'Si' : 'No') . PHP_EOL;
    		$detail .= 'Descripción: ' . $entity->description . PHP_EOL; 
    		$ok = true;
    	}

        if ($ok) {

            if (!isset($_SESSION['Auth']['User']['id'])) {
    	        $user_id = 100;
    	    } else {
    	        $user_id = $_SESSION['Auth']['User']['id'];
    	    }

            $actionLog = TableRegistry::get('ActionLog');
        	$query = $actionLog->query();
        	$query->insert([
        		'created', 
        		'detail',
        		'user_id',
        		'action',
        		'customer_code'
        	])
        	->values([
        		'created' => Time::now(), 
        		'detail' => $detail,
        		'user_id' => $user_id,
        		'action' => $action,
        		'customer_code' => isset($entity->customer_code) ? $entity->customer_code : null,
        	])
        	->execute();
        }
    }
}
