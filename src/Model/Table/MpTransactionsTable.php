<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MpTransactions Model
 *
 * @property \App\Model\Table\PaymentsTable|\Cake\ORM\Association\BelongsTo $Payments
 * @property \App\Model\Table\CollectorsTable|\Cake\ORM\Association\BelongsTo $Collectors
 * @property \App\Model\Table\CurrenciesTable|\Cake\ORM\Association\BelongsTo $Currencies
 * @property \App\Model\Table\PaymentTypesTable|\Cake\ORM\Association\BelongsTo $PaymentTypes
 * @property \App\Model\Table\ConnectionsTable|\Cake\ORM\Association\BelongsTo $Connections
 * @property \App\Model\Table\ReceiptsTable|\Cake\ORM\Association\BelongsTo $Receipts
 * @property \App\Model\Table\BloquesTable|\Cake\ORM\Association\BelongsTo $Bloques
 *
 * @method \App\Model\Entity\MpTransaction get($primaryKey, $options = [])
 * @method \App\Model\Entity\MpTransaction newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MpTransaction[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MpTransaction|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MpTransaction|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MpTransaction patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MpTransaction[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MpTransaction findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MpTransactionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('mp_transactions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_code',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Connections', [
            'foreignKey' => 'connection_id'
        ]);
        $this->belongsTo('Receipts', [
            'foreignKey' => 'receipt_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('date_created')
            ->allowEmpty('date_created');

        $validator
            ->dateTime('date_approved')
            ->allowEmpty('date_approved');

        $validator
            ->dateTime('date_last_updated')
            ->allowEmpty('date_last_updated');

        $validator
            ->dateTime('money_release_date')
            ->allowEmpty('money_release_date');

        $validator
            ->scalar('operation_type')
            ->maxLength('operation_type', 20)
            ->allowEmpty('operation_type');

        $validator
            ->scalar('payer_email')
            ->maxLength('payer_email', 254)
            ->allowEmpty('payer_email');

        $validator
            ->scalar('description')
            ->maxLength('description', 100)
            ->allowEmpty('description');

        $validator
            ->decimal('transaction_amount')
            ->allowEmpty('transaction_amount');

        $validator
            ->decimal('total_paid_amount')
            ->allowEmpty('total_paid_amount');

        $validator
            ->scalar('status')
            ->maxLength('status', 20)
            ->allowEmpty('status');

        $validator
            ->scalar('status_detail')
            ->maxLength('status_detail', 20)
            ->allowEmpty('status_detail');

        $validator
            ->scalar('comment')
            ->maxLength('comment', 254)
            ->allowEmpty('comment');

        $validator
            ->boolean('local_status')
            ->requirePresence('local_status', 'create')
            ->notEmpty('local_status');

        $validator
            ->decimal('comision')
            ->allowEmpty('comision');

        $validator
            ->decimal('net_received_amount')
            ->allowEmpty('net_received_amount');

        $validator
            ->scalar('external_reference')
            ->maxLength('external_reference', 255)
            ->allowEmpty('external_reference');

        $validator
            ->integer('customer_code')
            ->allowEmpty('customer_code');

        $validator
            ->scalar('receipt_number')
            ->maxLength('receipt_number', 255)
            ->allowEmpty('receipt_number');

        return $validator;
    }

    public function findServerSideDataTransactions(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length'];
        $page = 1;

        $columns = [
            'MpTransactions.date_created',      //0
            'MpTransactions.payment_getway_id', //1 metodo de pago
            'MpTransactions.local_status',      //2
            'Customers.code',                   //3
            'Customers.name',                   //4
            'Customers.ident',                  //5
            'MpTransactions.payment_id',        //6
            'MpTransactions.total_paid_amount', //7
            'MpTransactions.comment',           //8
            '',                                 //9
            'MpTransactions.receipt_number',    //10
        ];

        $columns_select = [
            'MpTransactions.id',                //0
            'MpTransactions.date_created',      //1
            'MpTransactions.payment_getway_id', //2
            'MpTransactions.status',            //3
            'Customers.code',                   //4
            'Customers.name',                   //5
            'Customers.ident',                  //6
            'Customers.doc_type',               //7
            'MpTransactions.payment_id',        //8
            'MpTransactions.total_paid_amount', //9
            'MpTransactions.comment',           //10
            'MpTransactions.local_status',      //11
            'MpTransactions.receipt_number',    //12
            'MpTransactions.receipt_id',        //13
        ];

        //paginacion

        if ($params['length'] > 0) {
            
            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        $query = $query->contain([
            'Customers'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'MpTransactions.date_created':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] .'-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Customers.code':
                    case 'MpTransactions.payment_getway_id':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'MpTransactions.total_paid_amount':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'MpTransactions.local_status':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? true : false];
                        break;

                    case 'MpTransactions.comment':
                    case 'Customers.name':
                    case 'MpTransactions.payment_id':
                    case 'MpTransactions.receipt_number':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);
 
        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Customers.code':
                    case 'MpTransactions.payment_getway_id':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'MpTransactions.comment':
                    case 'Customers.name':
                    case 'MpTransactions.payment_id':
                    case 'MpTransactions.receipt_number':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order(['MpTransactions.date_created' => 'desc']);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        $query->toArray();

        return $query;
    }

    public function findRecordsFilteredTransactions(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length'];
        $page = 1;

        $columns = [
            'MpTransactions.date_created',      //0
            'MpTransactions.payment_getway_id', //1 metodo de pago
            'MpTransactions.local_status',      //2
            'Customers.code',                   //3
            'Customers.name',                   //4
            'Customers.ident',                  //5
            'MpTransactions.payment_id',        //6
            'MpTransactions.total_paid_amount', //7
            'MpTransactions.comment',           //8
            '',                                 //9
            'MpTransactions.receipt_number',    //10
        ];

        $columns_select = [
            'MpTransactions.id',                //0
            'MpTransactions.date_created',      //1
            'MpTransactions.payment_getway_id', //2
            'MpTransactions.status',            //3
            'Customers.code',                   //4
            'Customers.name',                   //5
            'Customers.ident',                  //6
            'Customers.doc_type',               //7
            'MpTransactions.payment_id',        //8
            'MpTransactions.total_paid_amount', //9
            'MpTransactions.comment',           //10
            'MpTransactions.local_status',      //11
            'MpTransactions.receipt_number',    //12
            'MpTransactions.receipt_id',        //13
        ];

        $query = $query->contain([
            'Customers'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'MpTransactions.date_created':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Customers.code':
                    case 'MpTransactions.payment_getway_id':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'MpTransactions.total_paid_amount':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'MpTransactions.local_status':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? true : false];
                        break;

                    case 'MpTransactions.comment':
                    case 'MpTransactions.payment_id':
                    case 'Customers.name':
                    case 'MpTransactions.receipt_number':

                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);
 
        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Customers.code':
                    case 'MpTransactions.payment_getway_id':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'MpTransactions.comment':
                    case 'Customers.name':
                    case 'MpTransactions.payment_id':
                    case 'MpTransactions.receipt_number':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }
}
