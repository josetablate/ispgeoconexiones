<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Log\Log;

/**
 * Invoices Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $Debts
 *
 * @method \App\Model\Entity\Invoice get($primaryKey, $options = [])
 * @method \App\Model\Entity\Invoice newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Invoice[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Invoice|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Invoice patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Invoice[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Invoice findOrCreate($search, callable $callback = null)
 */
class InvoicesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('invoices');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_code',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('Debts', [
            'foreignKey' => 'invoice_id'
        ]);
        
        $this->hasMany('CustomersHasDiscounts', [
            'foreignKey' => 'invoice_id'
        ]);

        $this->hasMany('Concepts', [
            'foreignKey' => 'comprobante_id'
        ]);

        $this->belongsTo('Connections', [
            'foreignKey' => 'connection_id',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('date')
            ->requirePresence('date', 'create')
            ->notEmpty('date');

        $validator
            ->dateTime('date_start')
            ->allowEmpty('date_start');

        $validator
            ->dateTime('date_end')
            ->allowEmpty('date_end');

        $validator
            ->dateTime('duedate')
            ->requirePresence('duedate', 'create')
            ->notEmpty('duedate');

        $validator
     
            ->requirePresence('tipo_comp', 'create')
            ->notEmpty('tipo_comp');

        $validator
            ->integer('pto_vta')
            ->requirePresence('pto_vta', 'create')
            ->notEmpty('pto_vta');

       
        $validator
            ->decimal('subtotal')
            ->requirePresence('subtotal', 'create')
            ->notEmpty('subtotal');

        $validator
            ->decimal('sum_tax')
            ->requirePresence('sum_tax', 'create')
            ->notEmpty('sum_tax');

        $validator
            ->decimal('discount')
            ->requirePresence('discount', 'create')
            ->notEmpty('discount');

        $validator
            ->decimal('total')
            ->requirePresence('total', 'create')
            ->notEmpty('total');

        $validator
            ->allowEmpty('cae');

        $validator
            ->dateTime('vto')
            ->allowEmpty('vto');

        $validator
            ->requirePresence('company_name', 'create')
            ->notEmpty('company_name');

        $validator
            ->requirePresence('company_address', 'create')
            ->notEmpty('company_address');

        $validator
            ->requirePresence('company_cp', 'create')
            ->notEmpty('company_cp');

        $validator
            ->requirePresence('company_city', 'create')
            ->notEmpty('company_city');

        $validator
            ->requirePresence('company_phone', 'create')
            ->notEmpty('company_phone');

        $validator
            ->allowEmpty('company_fax');

        $validator
            ->requirePresence('company_ident', 'create')
            ->notEmpty('company_ident');

        $validator
            ->allowEmpty('company_email');

        $validator
            ->allowEmpty('company_web');

        $validator
            ->integer('company_responsible')
            ->allowEmpty('company_responsible');

        $validator
            ->allowEmpty('concept_receipt');

        $validator
            ->integer('customer_code')
            ->allowEmpty('customer_code');

        $validator
            ->allowEmpty('customer_name');

        $validator
            ->allowEmpty('customer_address');

        $validator
            ->allowEmpty('customer_cp');

        $validator
            ->allowEmpty('customer_city');

        $validator
            ->allowEmpty('customer_country');

        $validator
            ->allowEmpty('customer_ident');

        $validator
            ->integer('customer_doc_type')
            ->allowEmpty('customer_doc_type');

        $validator
            ->integer('customer_responsible')
            ->allowEmpty('customer_responsible');

        $validator
            ->allowEmpty('comments');

        $validator
            ->integer('seating_number')
            ->allowEmpty('seating_number');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function beforeSave($event, $entity, $options)
    {
        if (!$entity->isDirty('portal_sync')) {
            $entity->portal_sync = false;
        }
    }

    public function findServerSideData(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;
        
        $columns = [
            'Invoices.date',             //0
            'Invoices.tipo_comp',        //1
            'Invoices.pto_vta',          //2
            'Invoices.num',              //3
            'Invoices.concept_type',     //4
            'Invoices.date_start',       //5
            'Invoices.date_end',         //6
            'Invoices.comments',         //7
            'Invoices.duedate',          //8
            'Invoices.subtotal',         //9
            'Invoices.sum_tax',          //10
            'Invoices.total',            //11
            'Invoices.paid',             //12
            'Invoices.cae',              //13
            'Invoices.vto',              //14
            'Users.name',                //15
            'Invoices.customer_code',    //16
            'Invoices.customer_doc_type',//17
            'Invoices.customer_ident',   //18
            'Invoices.customer_name',    //19
            'Invoices.customer_address', //20
            'Invoices.customer_city',    //21
            'Invoices.business_id',      //22
            'Invoices.seating_number',   //23
            'Invoices.anulated',         //24
        ];

        $columns_select = [
            'Invoices.date',             //0
            'Invoices.tipo_comp',        //1
            'Invoices.pto_vta',          //2
            'Invoices.num',              //3
            'Invoices.concept_type',     //4
            'Invoices.date_start',       //5
            'Invoices.date_end',         //6
            'Invoices.comments',         //7
            'Invoices.duedate',          //8
            'Invoices.subtotal',         //9
            'Invoices.sum_tax',          //10
            'Invoices.total',            //11
            'Invoices.paid',             //12
            'Invoices.cae',              //13
            'Invoices.vto',              //14
            'Users.name',                //15
            'Invoices.customer_code',    //16
            'Invoices.customer_doc_type',//17
            'Invoices.customer_ident',   //18
            'Invoices.customer_name',    //19
            'Invoices.customer_address', //20
            'Invoices.customer_city',    //21
            'Invoices.business_id',      //22
            'Invoices.seating_number',   //23 
            'Invoices.id',
            'Invoices.anulated',
            'Customers.billing_for_service',
            'Customers.email'
        ];

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] => $o['dir']];
        }

        $query = $query->contain([
            'Users',
            'Customers'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Invoices.date': 
                    case 'Invoices.date_start':
                    case 'Invoices.date_end':
                    case 'Invoices.duedate':
                    case 'Invoices.vto':
                    case 'Invoices.anulated':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Invoices.tipo_comp':
                    case 'Invoices.pto_vta': 
                    case 'Invoices.num': 
                    case 'Invoices.concept_type': 
                    case 'Invoices.customer_code':
                    case 'Invoices.business_id':    
                    case 'Invoices.customer_ident':
                    case 'Invoices.customer_doc_type':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Invoices.subtotal':
                    case 'Invoices.sum_tax':
                    case 'Invoices.total': 
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Invoices.cae':
                    case 'Users.name':
                    case 'Invoices.customer_name':
                    case 'Invoices.comments':
                    case 'Invoices.customer_address':
                    case 'Invoices.customer_city':
                    case 'Invoices.seating_number':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {
                    case 'Invoices.date':
                    case 'Invoices.tipo_comp':
                    case 'Invoices.pto_vta':
                    case 'Invoices.concept_type':
                    case 'Invoices.date_start':
                    case 'Invoices.date_end':
                    case 'Invoices.duedate':
                    case 'Invoices.subtotal':
                    case 'Invoices.sum_tax':
                    case 'Invoices.total':
                    case 'Invoices.vto':
                    case 'Users.name':
                    case 'Invoices.business_id':
                    case 'Invoices.anulated':
                        break;

                    case 'Invoices.num':
                    case 'Invoices.cae':
                    case 'Invoices.customer_code':
                    case 'Invoices.customer_ident':
                    case 'Invoices.customer_doc_type':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Invoices.customer_name':
                    case 'Invoices.comments' :
                    case 'Invoices.customer_address':
                    case 'Invoices.customer_city':
                    case 'Invoices.seating_number':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order($order);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        $query->toArray();

        return $query;
    }

    public function findRecordsFiltered(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;
        
        $columns = [
            'Invoices.date',             //0
            'Invoices.tipo_comp',        //1
            'Invoices.pto_vta',          //2
            'Invoices.num',              //3
            'Invoices.concept_type',     //4
            'Invoices.date_start',       //5
            'Invoices.date_end',         //6
            'Invoices.comments',         //7
            'Invoices.duedate',          //8
            'Invoices.subtotal',         //9
            'Invoices.sum_tax',          //10
            'Invoices.total',            //11
            'Invoices.paid',             //12
            'Invoices.cae',              //13
            'Invoices.vto',              //14
            'Users.name',                //15
            'Invoices.customer_code',    //16
            'Invoices.customer_doc_type',//17
            'Invoices.customer_ident',   //18
            'Invoices.customer_name',    //19
            'Invoices.customer_address', //20
            'Invoices.customer_city',    //21
            'Invoices.business_id',      //22
            'Invoices.seating_number',   //23
            'Invoices.anulated',         //24   
        ];

        $columns_select = [
            'Invoices.date',             //0
            'Invoices.tipo_comp',        //1
            'Invoices.pto_vta',          //2
            'Invoices.num',              //3
            'Invoices.concept_type',     //4
            'Invoices.date_start',       //5
            'Invoices.date_end',         //6
            'Invoices.comments',         //7
            'Invoices.duedate',          //8
            'Invoices.subtotal',         //9
            'Invoices.sum_tax',          //10
            'Invoices.total',            //11
            'Invoices.paid',             //12
            'Invoices.cae',              //13
            'Invoices.vto',              //14
            'Users.name',                //15
            'Invoices.customer_code',    //16
            'Invoices.customer_doc_type',//17
            'Invoices.customer_ident',   //18
            'Invoices.customer_name',    //19
            'Invoices.customer_address', //20
            'Invoices.customer_city',    //21
            'Invoices.business_id',      //22
            'Invoices.seating_number',   //23 
            'Invoices.id',
            'Invoices.anulated',
            'Customers.billing_for_service',
            'Customers.email'
        ];

        $query = $query->contain([
            'Users',
            'Customers'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Invoices.date': 
                    case 'Invoices.date_start':
                    case 'Invoices.date_end':
                    case 'Invoices.duedate':
                    case 'Invoices.vto':
                    case 'Invoices.paid':
                    case 'Invoices.anulated':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                            
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Invoices.tipo_comp':
                    case 'Invoices.pto_vta': 
                    case 'Invoices.num': 
                    case 'Invoices.concept_type': 
                    case 'Invoices.Customer_code':
                    case 'Invoices.business_id':
                    case 'Invoices.customer_ident':
                    case 'Invoices.customer_doc_type':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Invoices.subtotal':
                    case 'Invoices.sum_tax':
                    case 'Invoices.total':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;    

                    case 'Invoices.cae':
                    case 'Users.name':
                    case 'Invoices.customer_name':
                    case 'Invoices.comments':
                    case 'Invoices.customer_address':
                    case 'Invoices.customer_city':
                    case 'Invoices.seating_number':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);
 
        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Invoices.date':
                    case 'Invoices.tipo_comp':
                    case 'Invoices.pto_vta':
                    case 'Invoices.concept_type':
                    case 'Invoices.date_start':
                    case 'Invoices.date_end':
                    case 'Invoices.duedate':
                    case 'Invoices.subtotal':
                    case 'Invoices.sum_tax':
                    case 'Invoices.total':
                    case 'Invoices.vto':
                    case 'Invoices.paid':
                    case 'Users.name':
                    case 'Invoices.business_id':
                    case 'Invoices.anulated':
                        break;

                    case 'Invoices.num':
                    case 'Invoices.cae':
                    case 'Invoices.customer_code':
                    case 'Invoices.customer_ident':
                    case 'Invoices.customer_doc_type':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Invoices.customer_name':
                    case 'Invoices.comments':
                    case 'Invoices.customer_address':
                    case 'Invoices.customer_city':
                    case 'Invoices.seating_number':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }

    public function findServerSideDataMoves(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;
        
        $columns = [
            'Invoices.date',
            'Users.username',
            '',
            'Invoices.tipo_comp',
            'Invoices.pto_vta',
            'Invoices.num',
            'Invoices.cae',
            'Invoices.comments',
            'Invoices.subtotal',
            'Invoices.sum_tax',
            'Invoices.total',
            'Invoices.paid',
            'Invoices.duedate',
            '',
            'Invoices.seating_number',
            'Invoices.duedate',
            'Invoices.anulated',
        ];

        $Columns_select = [
            'Invoices.date',
            'Users.username',
            'Invoices.tipo_comp',
            'Invoices.pto_vta',
            'Invoices.num',
            'Invoices.cae',
            'Invoices.comments',
            'Invoices.subtotal',
            'Invoices.sum_tax',
            'Invoices.total',
            'Invoices.paid',
            'Invoices.duedate',
            'Invoices.seating_number',
            'Invoices.id',
            'Invoices.connection_id',
            'Invoices.customer_code',
            'Invoices.duedate',
            'Invoices.anulated',
        ];

        //paginacion

        if ($params['length'] > 0) {
            
            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        $query = $query->contain([
            'Users'
        ])
        ->select($Columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Invoices.date': 
                    case 'Invoices.duedate':
                    case 'Invoices.paid':
                    case 'Invoices.anulated':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] .'-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }

                        break;

                    case 'Invoices.tipo_comp':
                    case 'Invoices.pto_vta': 
                    case 'Invoices.num': 
                    case 'Invoices.cae':

                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Invoices.total': 

                        $value = explode('<>',$column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }

                        break;

                    case 'Users.username':
                    case 'Invoices.comments':
                    case 'Invoices.seating_number':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);
 
        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Invoices.cae':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'user.username' :
                    case 'Invoices.comments' :
                    case 'Invoices.seating_number':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order(['Invoices.date' => 'desc']);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        $query->toArray();

        return $query;
    }

    public function findRecordsFilteredMoves(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;
        
        $columns = [
            'Invoices.date',
            'Users.username',
            '',
            'Invoices.tipo_comp',
            'Invoices.pto_vta',
            'Invoices.num',
            'Invoices.cae',
            'Invoices.comments',
            'Invoices.subtotal',
            'Invoices.sum_tax',
            'Invoices.total',
            'Invoices.paid',
            'Invoices.duedate',
            '',
            'Invoices.seating_number',
            'Invoices.anulated',
        ];

        $Columns_select = [
            'Invoices.date',
            'Users.username',
            'Invoices.tipo_comp',
            'Invoices.pto_vta',
            'Invoices.num',
            'Invoices.cae',
            'Invoices.comments',
            'Invoices.subtotal',
            'Invoices.sum_tax',
            'Invoices.total',
            'Invoices.paid',
            'Invoices.duedate',
            'Invoices.seating_number',
            'Invoices.id',
            'Invoices.connection_id',
            'Invoices.customer_code',
            'Invoices.anulated',
        ];

        $query = $query->contain([
            'Users'
        ])
        ->select($Columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Invoices.date': 
                    case 'Invoices.duedate':
                    case 'Invoices.anulated':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                            
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }

                        break;

                    case 'Invoices.tipo_comp':
                    case 'Invoices.pto_vta': 
                    case 'Invoices.num': 
                    case 'Invoices.cae':

                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Invoices.total': 

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Users.username':
                    case 'Invoices.comments':
                    case 'Invoices.seating_number':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Invoices.num':
                    case 'Invoices.cae':
 
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Users.username' :
                    case 'Invoices.comments' :
                    case 'Invoices.seating_number':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }

    public function findServerSideDataPresuX(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;
        
        $columns = [
            '',                          //0
            'Invoices.date',             //1
            'Invoices.pto_vta',          //2
            'Invoices.num',              //3
            'Invoices.concept_type',     //4
            'Invoices.date_start',       //5
            'Invoices.date_end',         //6
            'Invoices.comments',         //7
            'Invoices.duedate',          //8
            'Invoices.subtotal',         //9
            'Invoices.sum_tax',          //10
            'Invoices.total',            //11
            'Invoices.paid',             //12
            'Users.name',                //13
            'Invoices.customer_code',    //14
            'Invoices.customer_doc_type',//15
            'Invoices.customer_ident',   //16
            'Invoices.customer_name',    //17
            'Invoices.customer_address', //18
            'Invoices.customer_city',    //19
            'Invoices.business_id',      //20
        ];

        $columns_select = [
            
            'Invoices.date',             //1
            'Invoices.pto_vta',          //2
            'Invoices.num',              //3
            'Invoices.concept_type',     //4
            'Invoices.date_start',       //5
            'Invoices.date_end',         //6
            'Invoices.comments',         //7
            'Invoices.duedate',          //8
            'Invoices.subtotal',         //9
            'Invoices.sum_tax',          //10
            'Invoices.total',            //11
            'Invoices.paid',             //12
            'Users.name',                //13
            'Invoices.customer_code',    //14
            'Invoices.customer_doc_type',//15
            'Invoices.customer_ident',   //16
            'Invoices.customer_name',    //17
            'Invoices.customer_address', //18
            'Invoices.customer_city',    //19
            'Invoices.business_id',      //20
            
            'Invoices.id',
            'Customers.billing_for_service',
            'Customers.email'
        ];

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] => $o['dir']];
        }

        $query = $query->contain([
            'Users',
            'Customers'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Invoices.date': 
                    case 'Invoices.date_start':
                    case 'Invoices.date_end':
                    case 'Invoices.duedate':
                    case 'Invoices.paid':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;
          
                    case 'Invoices.pto_vta': 
                    case 'Invoices.num': 
                    case 'Invoices.concept_type': 
                    case 'Invoices.customer_code':
                    case 'Invoices.business_id':    
                    case 'Invoices.customer_ident':
                    case 'Invoices.customer_doc_type':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Invoices.subtotal':
                    case 'Invoices.sum_tax':
                    case 'Invoices.total': 
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

      
                    case 'Users.name':
                    case 'Invoices.customer_name':
                    case 'Invoices.comments':
                    case 'Invoices.customer_address':
                    case 'Invoices.customer_city':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {
                    case 'Invoices.date':
                    case 'Invoices.pto_vta':
                    case 'Invoices.concept_type':
                    case 'Invoices.date_start':
                    case 'Invoices.date_end':
                    case 'Invoices.duedate':
                    case 'Invoices.subtotal':
                    case 'Invoices.sum_tax':
                    case 'Invoices.total':
                    case 'Users.name':
                    case 'Invoices.business_id':  
                        break;

                    case 'Invoices.num':
                    case 'Invoices.customer_code':
                    case 'Invoices.customer_ident':
                    case 'Invoices.customer_doc_type':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Invoices.customer_name':
                    case 'Invoices.comments' :
                    case 'Invoices.customer_address':
                    case 'Invoices.customer_city':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order($order);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        $query->toArray();

        return $query;
    }

    public function findRecordsFilteredPresuX(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;
       
        $columns = [
            '',                          //0
            'Invoices.date',             //1
            'Invoices.pto_vta',          //2
            'Invoices.num',              //3
            'Invoices.concept_type',     //4
            'Invoices.date_start',       //5
            'Invoices.date_end',         //6
            'Invoices.comments',         //7
            'Invoices.duedate',          //8
            'Invoices.subtotal',         //9
            'Invoices.sum_tax',          //10
            'Invoices.total',            //11
            'Invoices.paid',             //12
            'Users.name',                //13
            'Invoices.customer_code',    //14
            'Invoices.customer_doc_type',//15
            'Invoices.customer_ident',   //16
            'Invoices.customer_name',    //17
            'Invoices.customer_address', //18
            'Invoices.customer_city',    //19
            'Invoices.business_id',      //20
        ];

        $columns_select = [
            
            'Invoices.date',             //1
            'Invoices.pto_vta',          //2
            'Invoices.num',              //3
            'Invoices.concept_type',     //4
            'Invoices.date_start',       //5
            'Invoices.date_end',         //6
            'Invoices.comments',         //7
            'Invoices.duedate',          //8
            'Invoices.subtotal',         //9
            'Invoices.sum_tax',          //10
            'Invoices.total',            //11
            'Invoices.paid',             //12
            'Users.name',                //13
            'Invoices.customer_code',    //14
            'Invoices.customer_doc_type',//15
            'Invoices.customer_ident',   //16
            'Invoices.customer_name',    //17
            'Invoices.customer_address', //18
            'Invoices.customer_city',    //19
            'Invoices.business_id',      //20
            
            'Invoices.id',
            'Customers.billing_for_service',
            'Customers.email'
        ];

        $query = $query->contain([
            'Users',
            'Customers'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Invoices.date': 
                    case 'Invoices.date_start':
                    case 'Invoices.date_end':
                    case 'Invoices.duedate':
                    case 'Invoices.paid':
                       
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                            
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;
          
                    case 'Invoices.pto_vta': 
                    case 'Invoices.num': 
                    case 'Invoices.concept_type': 
                    case 'Invoices.Customer_code':
                    case 'Invoices.business_id':
                    case 'Invoices.customer_ident':
                    case 'Invoices.customer_doc_type':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Invoices.subtotal':
                    case 'Invoices.sum_tax':
                    case 'Invoices.total':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;    

                    case 'Users.name':
                    case 'Invoices.customer_name':
                    case 'Invoices.comments':
                    case 'Invoices.customer_address':
                    case 'Invoices.customer_city':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);
 
        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Invoices.date':
                    case 'Invoices.pto_vta':
                    case 'Invoices.concept_type':
                    case 'Invoices.date_start':
                    case 'Invoices.date_end':
                    case 'Invoices.duedate':
                    case 'Invoices.subtotal':
                    case 'Invoices.sum_tax':
                    case 'Invoices.total':
                    case 'Invoices.paid':
                    case 'Users.name':
                    case 'Invoices.business_id':
                        break;

                    case 'Invoices.num':
                    case 'Invoices.customer_code':
                    case 'Invoices.customer_ident':
                    case 'Invoices.customer_doc_type':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Invoices.customer_name':
                    case 'Invoices.comments':
                    case 'Invoices.customer_address':
                    case 'Invoices.customer_city':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }

    public function findServerSideDataCobrodigital(Query $query, array $options)
    {
        $params = $options['params'];
        $paraments = $_SESSION['paraments'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            '',                              //0
            'Invoices.date',                 //1
            'Invoices.tipo_comp',            //2
            'Invoices.num',                  //3
            'Invoices.date_start',           //4
            'Invoices.date_end',             //5
            'Invoices.duedate',              //6
            'Invoices.total',                //7
            'Invoices.customer_name',        //8
            'Invoices.customer_ident',       //9
            'Invoices.customer_code',        //10
            'Invoices.connection',           //11
            'Invoices.id_comercio',          //12
        ];

        if ($paraments->gral_config->billing_for_service) {
            $columns_select = [
                'Invoices.date',
                'Invoices.tipo_comp',
                'Invoices.num',
                'Invoices.date_start',
                'Invoices.date_end',
                'Invoices.duedate',
                'Invoices.total',
                'Invoices.customer_name',
                'Invoices.customer_ident',
                'Invoices.customer_doc_type',
                'Invoices.customer_code',
                'Connections.service_id',
                'Invoices.connection_id',
                'Connections.address',
                'Connections.created',
                'Invoices.id',
                'Connections.cobrodigital_card',
                'Connections.cobrodigital_auto_debit',
            ];
        } else {
            $columns_select = [
                'Invoices.date',
                'Invoices.tipo_comp',
                'Invoices.num',
                'Invoices.date_start',
                'Invoices.date_end',
                'Invoices.duedate',
                'Invoices.total',
                'Invoices.customer_name',
                'Invoices.customer_ident',
                'Invoices.customer_doc_type',
                'Invoices.customer_code',
                'Connections.service_id',
                'Invoices.connection_id',
                'Connections.address',
                'Connections.created',
                'Invoices.id',
            ];
        }

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] => $o['dir']];
        }

        $query = $query->contain([
            'Connections',
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    // case 'Invoices.date_start':
                    //     $value = [];

                    //     $value[0] = trim($params['columns'][3]['search']['value']);
                    //     $value[1] = trim($params['columns'][4]['search']['value']);

                    //     $value[0] = trim($value[0]);
                    //     $value[1] = trim($value[1]);

                    //     if ($value[0] != '' && $value[1] != '') {
                    //         $value[0] = explode('/', $value[0]);
                    //         $value[1] = explode('/', $value[1]);
                    //         $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                    //         $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                    //         $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                    //     }
                    //     break;

                    case 'Invoices.date_start':
                        $value = trim($column['search']['value']);
                        if ($value != '') {
                            $value = explode('/', $value);
                            $value = $value[2] . '-' . $value[1] . '-' . $value[0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >= ' => $value];
                        }
                        break;

                    case 'Invoices.date_end':
                        $value = trim($column['search']['value']);
                        if ($value != '') {
                            $value = explode('/', $value);
                            $value = $value[2] . '-' . $value[1] . '-' . $value[0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <= ' => $value];
                        }
                        break;

                    case 'Invoices.duedate':
                    case 'Invoices.date':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Invoices.tipo_comp':
                    case 'Invoices.num': 
                    case 'Invoices.customer_code':
                    case 'Invoices.customer_ident':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Invoices.total': 
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Invoices.customer_name':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {
                    case 'Invoices.tipo_comp':
                    case 'Invoices.date_start':
                    case 'Invoices.date_end':
                    case 'Invoices.date':
                    case 'Invoices.duedate':
                    case 'Invoices.total':
                        break;

                    case 'Invoices.num':
                    case 'Invoices.customer_code':
                    case 'Invoices.customer_ident':

                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Invoices.customer_name':

                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order($order);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        $payment_getway = $_SESSION['payment_getway'];

        $query->map(function ($row) use ($payment_getway) { // map() is a collection method, it executes the query

            $tableCobrodigitalAccount = TableRegistry::get('CobrodigitalAccounts');
            $cobrodigital_account = $tableCobrodigitalAccount
            	    ->find()
            	    ->select([
            	        'id_comercio'
        	        ])
        	        ->where([
        	            'customer_code' => $row->customer_code
    	            ])
        	        ->first();

            $row->id_comercio = "";

	        if ($cobrodigital_account) {
	            foreach ($payment_getway->config->cobrodigital->credentials as $credential) {
                    if ($credential->idComercio == $cobrodigital_account->id_comercio) {
                        $id_comercio_name = '(' . $credential->idComercio . ') ' . $credential->name;
                        $row->id_comercio = $id_comercio_name;
                        break;
                    }
                }
	        }

            if ($row->connection_id) {

                $tableService = TableRegistry::get('Services');
            	$service = $tableService
            	    ->find()
            	    ->select([
            	        'name'
        	        ])
        	        ->where([
        	            'id' => $row->connection->service_id
    	            ])
        	        ->first();

                if ($service) {
                    $created = $row->connection->created->format('d/m/Y');
                    $row->connection = $created . ' - ' . $service->name . ' - ' . $row->connection->address;
                }
            }
        })
        ->toArray();

        return $query;
    }

    public function findRecordsFilteredCobrodigital(Query $query, array $options)
    {
        $params = $options['params'];
        $paraments = $_SESSION['paraments'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            '',                              //0
            'Invoices.date',                 //1
            'Invoices.tipo_comp',            //2
            'Invoices.num',                  //3
            'Invoices.date_start',           //4
            'Invoices.date_end',             //5
            'Invoices.duedate',              //6
            'Invoices.total',                //7
            'Invoices.customer_name',        //8
            'Invoices.customer_ident',       //9
            'Invoices.customer_code',        //10
            'Invoices.connection',           //11
            'Invoices.id_comercio',          //12
        ];

        if ($paraments->gral_config->billing_for_service) {
            $columns_select = [
                'Invoices.date',
                'Invoices.tipo_comp',
                'Invoices.num',
                'Invoices.date_start',
                'Invoices.date_end',
                'Invoices.duedate',
                'Invoices.total',
                'Invoices.customer_name',
                'Invoices.customer_ident',
                'Invoices.customer_doc_type',
                'Invoices.customer_code',
                'Connections.service_id',
                'Invoices.connection_id',
                'Connections.address',
                'Connections.created',
                'Invoices.id',
                'Connections.cobrodigital_card',
                'Connections.cobrodigital_auto_debit',
            ];
        } else {
            $columns_select = [
                'Invoices.date',
                'Invoices.tipo_comp',
                'Invoices.num',
                'Invoices.date_start',
                'Invoices.date_end',
                'Invoices.duedate',
                'Invoices.total',
                'Invoices.customer_name',
                'Invoices.customer_ident',
                'Invoices.customer_doc_type',
                'Invoices.customer_code',
                'Connections.service_id',
                'Invoices.connection_id',
                'Connections.address',
                'Connections.created',
                'Invoices.id',
            ];
        }

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] => $o['dir']];
        }

        $query = $query->contain([
            'Connections',
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    // case 'Invoices.date_start':
                    //     $value = [];

                    //     $value[0] = trim($params['columns'][3]['search']['value']);
                    //     $value[1] = trim($params['columns'][4]['search']['value']);

                    //     $value[0] = trim($value[0]);
                    //     $value[1] = trim($value[1]);

                    //     if ($value[0] != '' && $value[1] != '') {
                    //         $value[0] = explode('/', $value[0]);
                    //         $value[1] = explode('/', $value[1]);
                    //         $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                    //         $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                    //         $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                    //     }
                    //     break;

                    case 'Invoices.date_start':
                        $value = trim($column['search']['value']);
                        if ($value != '') {
                            $value = explode('/', $value);
                            $value = $value[2] . '-' . $value[1] . '-' . $value[0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >= ' => $value];
                        }
                        break;

                    case 'Invoices.date_end':
                        $value = trim($column['search']['value']);
                        if ($value != '') {
                            $value = explode('/', $value);
                            $value = $value[2] . '-' . $value[1] . '-' . $value[0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <= ' => $value];
                        }
                        break;

                    case 'Invoices.duedate':
                    case 'Invoices.date':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Invoices.tipo_comp':
                    case 'Invoices.num': 
                    case 'Invoices.customer_code':
                    case 'Invoices.customer_ident':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Invoices.total': 
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Invoices.customer_name':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {
                    case 'Invoices.tipo_comp':
                    case 'Invoices.date_start':
                    case 'Invoices.date_end':
                    case 'Invoices.date':
                    case 'Invoices.duedate':
                    case 'Invoices.total':
                        break;

                    case 'Invoices.num':
                    case 'Invoices.customer_code':
                    case 'Invoices.customer_ident':

                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Invoices.customer_name':

                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }

    public function findServerSideDataComprobantes(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            '',                          //0
            'Invoices.date',             //1
            'Invoices.date_start',       //2
            'Invoices.date_end',         //3
            'Invoices.tipo_comp',        //4
            'Invoices.pto_vta',          //5
            'Invoices.num',              //6
            'Invoices.comments',         //7
            'Invoices.duedate',          //8
            'Invoices.total',            //9
            'Users.username',            //10
            'Invoices.customer_code',    //11
            'Invoices.customer_doc_type',//12
            'Invoices.customer_ident',   //13
            'Invoices.customer_name',    //14
            'Customers.email',           //15
            'Invoices.customer_address', //16
            'Invoices.customer_city',    //17
            '',                          //18
            'Invoices.business_id',      //19
            'Invoices.anulated',         //20
        ];

        $columns_select = [
            'Invoices.id',                   //0
            'Invoices.date',                 //1
            'Invoices.date_start',           //2
            'Invoices.date_end',             //3
            'Invoices.tipo_comp',            //4
            'Invoices.pto_vta',              //5
            'Invoices.num',                  //6
            'Invoices.comments',             //7
            'Invoices.duedate',              //8
            'Invoices.total',                //9
            'Users.username',                //10
            'Invoices.customer_code',        //11
            'Invoices.customer_doc_type',    //12
            'Invoices.customer_ident',       //13
            'Invoices.customer_name',        //14
            'Customers.email',               //15
            'Invoices.customer_address',     //16
            'Invoices.customer_city',        //17
            'Invoices.business_id',          //18
            'Customers.billing_for_service', //19
            'Invoices.anulated',             //20
        ];

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] => $o['dir']];
        }

        $query = $query->contain([
            'Users',
            'Customers'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Invoices.date': 
                    case 'Invoices.date_start':
                    case 'Invoices.date_end':
                    case 'Invoices.duedate':
                    case 'Invoices.anulated':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Invoices.tipo_comp':
                    case 'Invoices.pto_vta':
                    case 'Invoices.num':
                    case 'Invoices.customer_code':
                    case 'Invoices.business_id':
                    case 'Invoices.customer_ident':
                    case 'Invoices.customer_doc_type':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Invoices.total': 
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Users.username':
                    case 'Customers.email':
                    case 'Invoices.customer_name':
                    case 'Invoices.comments':
                    case 'Invoices.customer_address':
                    case 'Invoices.customer_city':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {
                    case 'Invoices.date':
                    case 'Invoices.tipo_comp':
                    case 'Invoices.pto_vta':
                    case 'Invoices.date_start':
                    case 'Invoices.date_end':
                    case 'Invoices.duedate':
                    case 'Invoices.total':
                    case 'Invoices.business_id':  
                    case 'Invoices.anulated':
                        break;

                    case 'Invoices.num':
                    case 'Invoices.customer_code':
                    case 'Invoices.customer_ident':
                    case 'Invoices.customer_doc_type':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Invoices.customer_name':
                    case 'Invoices.comments' :
                    case 'Invoices.customer_address':
                    case 'Invoices.customer_city':
                    case 'Customers.email':
                    case 'Users.username':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order($order);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        $query->toArray();

        return $query;
    }

    public function findRecordsFilteredComprobantes(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;
        
        $columns = [
            '',                          //0
            'Invoices.date',             //1
            'Invoices.date_start',       //2
            'Invoices.date_end',         //3
            'Invoices.tipo_comp',        //4
            'Invoices.pto_vta',          //5
            'Invoices.num',              //6
            'Invoices.comments',         //7
            'Invoices.duedate',          //8
            'Invoices.total',            //9
            'Users.username',            //10
            'Invoices.customer_code',    //11
            'Invoices.customer_doc_type',//12
            'Invoices.customer_ident',   //13
            'Invoices.customer_name',    //14
            'Customers.email',           //15
            'Invoices.customer_address', //16
            'Invoices.customer_city',    //17
            '',                          //18
            'Invoices.business_id',      //19
            'Invoices.anulated',         //20
        ];

        $columns_select = [
            'Invoices.id',                   //0
            'Invoices.date',                 //1
            'Invoices.date_start',           //2
            'Invoices.date_end',             //3
            'Invoices.tipo_comp',            //4
            'Invoices.pto_vta',              //5
            'Invoices.num',                  //6
            'Invoices.comments',             //7
            'Invoices.duedate',              //8
            'Invoices.total',                //9
            'Users.username',                //10
            'Invoices.customer_code',        //11
            'Invoices.customer_doc_type',    //12
            'Invoices.customer_ident',       //13
            'Invoices.customer_name',        //14
            'Customers.email',               //15
            'Invoices.customer_address',     //16
            'Invoices.customer_city',        //17
            'Invoices.business_id',          //18
            'Customers.billing_for_service', //19
            'Invoices.anulated',             //20
        ];

        $query = $query->contain([
            'Users',
            'Customers'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Invoices.date': 
                    case 'Invoices.date_start':
                    case 'Invoices.date_end':
                    case 'Invoices.duedate':
                    case 'Invoices.anulated':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Invoices.tipo_comp':
                    case 'Invoices.pto_vta':
                    case 'Invoices.num':
                    case 'Invoices.customer_code':
                    case 'Invoices.business_id':
                    case 'Invoices.customer_ident':
                    case 'Invoices.customer_doc_type':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Invoices.total': 
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Users.username':
                    case 'Customers.email':
                    case 'Invoices.customer_name':
                    case 'Invoices.comments':
                    case 'Invoices.customer_address':
                    case 'Invoices.customer_city':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {
                    case 'Invoices.date':
                    case 'Invoices.tipo_comp':
                    case 'Invoices.pto_vta':
                    case 'Invoices.date_start':
                    case 'Invoices.date_end':
                    case 'Invoices.duedate':
                    case 'Invoices.total':
                    case 'Invoices.business_id':
                    case 'Invoices.anulated':
                        break;

                    case 'Invoices.num':
                    case 'Invoices.customer_code':
                    case 'Invoices.customer_ident':
                    case 'Invoices.customer_doc_type':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Invoices.customer_name':
                    case 'Invoices.comments' :
                    case 'Invoices.customer_address':
                    case 'Invoices.customer_city':
                    case 'Customers.email':
                    case 'Users.username':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }

    public function findServerSideDataMovesAll(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            'Invoices.date',
            'Invoices.customer_code',
            'Invoices.customer_name',
            'Invoices.customer_doc_type',
            'Invoices.customer_ident',
            'Users.username',
            '',
            'Invoices.tipo_comp',
            'Invoices.pto_vta',
            'Invoices.num',
            'Invoices.cae',
            'Invoices.comments',
            'Invoices.subtotal',
            'Invoices.sum_tax',
            'Invoices.total',
            'Invoices.paid',
            'Invoices.duedate',
            '',
            'Invoices.seating_number'
        ];

        $Columns_select = [
            'Invoices.date',
            'Invoices.customer_code',
            'Invoices.customer_name',
            'Invoices.customer_doc_type',
            'Invoices.customer_ident',
            'Customers.billing_for_service',
            'Customers.email',
            'Users.username',
            'Invoices.tipo_comp',
            'Invoices.pto_vta',
            'Invoices.num',
            'Invoices.cae',
            'Invoices.comments',
            'Invoices.subtotal',
            'Invoices.sum_tax',
            'Invoices.total',
            'Invoices.paid',
            'Invoices.duedate',
            'Invoices.seating_number',
            'Invoices.id',
            'Invoices.connection_id'
        ];

        //paginacion

        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        $query = $query->contain([
            'Users',
            'Customers'
        ])->select($Columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Invoices.date': 
                    case 'Invoices.duedate':
                    case 'Invoices.paid':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] .'-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }

                        break;

                    case 'Invoices.tipo_comp':
                    case 'Invoices.pto_vta': 
                    case 'Invoices.num':
                    case 'Invoices.cae':
                    case 'Invoices.customer_code': 
                    case 'Invoices.customer_doc_type':

                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Invoices.total': 

                        $value = explode('<>',$column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }

                        break;

                    case 'Users.username':
                    case 'Invoices.comments':
                    case 'Invoices.seating_number':
                    case 'Invoices.customer_name': 
                    case 'Invoices.customer_ident':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);
 
        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Invoices.cae':
                    case 'Invoices.customer_code': 
                    case 'Invoices.customer_doc_type':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'user.username' :
                    case 'Invoices.comments' :
                    case 'Invoices.seating_number':
                    case 'Invoices.customer_name': 
                    case 'Invoices.customer_ident':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order(['Invoices.date' => 'desc']);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        $query->toArray();

        return $query;
    }

    public function findRecordsFilteredMovesAll(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;
        
        $columns = [
            'Invoices.date',
            'Invoices.customer_code',
            'Invoices.customer_name',
            'Invoices.customer_doc_type',
            'Invoices.customer_ident',
            'Users.username',
            '',
            'Invoices.tipo_comp',
            'Invoices.pto_vta',
            'Invoices.num',
            'Invoices.cae',
            'Invoices.comments',
            'Invoices.subtotal',
            'Invoices.sum_tax',
            'Invoices.total',
            'Invoices.paid',
            'Invoices.duedate',
            '',
            'Invoices.seating_number'
        ];

        $Columns_select = [
            'Invoices.date',
            'Invoices.customer_code',
            'Invoices.customer_name',
            'Invoices.customer_doc_type',
            'Invoices.customer_ident',
            'Customers.billing_for_service',
            'Customers.email',
            'Users.username',
            'Invoices.tipo_comp',
            'Invoices.pto_vta',
            'Invoices.num',
            'Invoices.cae',
            'Invoices.comments',
            'Invoices.subtotal',
            'Invoices.sum_tax',
            'Invoices.total',
            'Invoices.paid',
            'Invoices.duedate',
            'Invoices.seating_number',
            'Invoices.id',
            'Invoices.connection_id'
        ];

        $query = $query->contain([
            'Users',
            'Customers'
        ])
        ->select($Columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Invoices.date': 
                    case 'Invoices.duedate':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                            
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }

                        break;

                    case 'Invoices.tipo_comp':
                    case 'Invoices.pto_vta': 
                    case 'Invoices.num': 
                    case 'Invoices.cae':
                    case 'Invoices.customer_code':
                    case 'Invoices.customer_doc_type':

                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Invoices.total': 

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Users.username':
                    case 'Invoices.comments':
                    case 'Invoices.seating_number':
                    case 'Invoices.customer_name': 
                    case 'Invoices.customer_ident': 
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Invoices.num':
                    case 'Invoices.cae':
                    case 'Invoices.customer_code':
                    case 'Invoices.customer_doc_type':
 
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Users.username' :
                    case 'Invoices.comments' :
                    case 'Invoices.seating_number':
                    case 'Invoices.customer_name': 
                    case 'Invoices.customer_ident': 
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }

    // public function afterSave($event, $entity, $options)
    // {
    //     $detail = '';

    //     if ($entity->isNew()) {

    //         $paraments = $_SESSION['paraments'];

    //         foreach ($paraments->invoicing->business as $b) {
    //             if ($b->id == $entity->business_id) {
    //                  $business = $b->name . ' (' . $b->address . ')';
    //             }
    //         }

    //         $action = 'Creación de Factura';
    //         $detail .= 'Fecha: ' . $entity->date->format('d/m/Y') . PHP_EOL;
    //         $detail .= 'Desde: ' . $entity->date_start->format('d/m/Y') . PHP_EOL;
    //         $detail .= 'Hasta: ' . $entity->date_end->format('d/m/Y') . PHP_EOL;
    //         $detail .= 'Vencimiento: ' . $entity->duedate->format('d/m/Y') . PHP_EOL;
    //         $detail .= 'Número: ' . $entity->num . PHP_EOL;
    //         $detail .= 'Pto de Vta: ' . $entity->pto_vta . PHP_EOL;
    //         $detail .= 'Tipo de Comp.: ' . $_SESSION['afip_codes']['comprobantes'][$entity->tipo_comp] . PHP_EOL;
    //         $detail .= 'Comentarios: ' . $entity->comments . PHP_EOL;
    //         $detail .= 'Total: ' . number_format($entity->total, 2, ',', '.') . PHP_EOL;
    //         $detail .= 'Empresa: ' . $business . PHP_EOL;
    //         $detail .= 'Cond. de Vta: ' . $_SESSION['afip_codes']['cond_venta'][$entity->cond_vta] . PHP_EOL;

    //         $actionLog = TableRegistry::get('ActionLog');
    //         $query = $actionLog->query();
    //         $query->insert([
    //             'created', 
    //             'detail',
    //             'user_id',
    //             'action',
    //             'customer_code'
    //         ])
    //         ->values([
    //             'created' => Time::now(), 
    //             'detail' => $detail,
    //             'user_id' => $_SESSION['Auth']['User']['id'],
    //             'action' => $action,
    //             'customer_code' => isset($entity->customer_code) ? $entity->customer_code : null,
    //         ])
    //         ->execute();
    //     }
    // }
}
