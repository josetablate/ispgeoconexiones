<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PaymentsConcepts Model
 *
 * @method \App\Model\Entity\PaymentsConcept get($primaryKey, $options = [])
 * @method \App\Model\Entity\PaymentsConcept newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PaymentsConcept[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PaymentsConcept|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PaymentsConcept patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PaymentsConcept[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PaymentsConcept findOrCreate($search, callable $callback = null, $options = [])
 */
class PaymentsConceptsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('payments_concepts');
        $this->setDisplayField('text');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('text')
            ->requirePresence('text', 'create')
            ->notEmpty('text');

        return $validator;
    }
}
