<?php
namespace App\Model\Table\MikrotikDhcpTa;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TControllers Model
 *
 * @method \App\Model\Entity\TController get($primaryKey, $options = [])
 * @method \App\Model\Entity\TController newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TController[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TController|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TController patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TController[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TController findOrCreate($search, callable $callback = null)
 */
class ControllersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
         
        $this->setEntityClass('App\Model\Entity\MikrotikDhcpTa\Controller');  

        $this->setTable('controllers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('local_address');

        $validator
            ->allowEmpty('dns_server');
    

        // $validator
        //     ->boolean('deleted')
        //     ->requirePresence('deleted', 'create')
        //     ->notEmpty('deleted');

        $validator
            ->allowEmpty('queue_default');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'ispbrain_mikrotik_dhcp_ta';
    }
}
