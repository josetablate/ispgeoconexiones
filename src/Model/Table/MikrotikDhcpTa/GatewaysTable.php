<?php
namespace App\Model\Table\MikrotikDhcpTa;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TGateway Model
 *
 * @property \App\Model\Table\ControllersTable|\Cake\ORM\Association\BelongsTo $Controllers
 * @property \App\Model\Table\PoolsTable|\Cake\ORM\Association\BelongsTo $Pools
 *
 * @method \App\Model\Entity\TGateway get($primaryKey, $options = [])
 * @method \App\Model\Entity\TGateway newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TGateway[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TGateway|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TGateway|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TGateway patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TGateway[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TGateway findOrCreate($search, callable $callback = null, $options = [])
 */
class GatewaysTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
         
        $this->setEntityClass('App\Model\Entity\MikrotikDhcpTa\Gateway');  

        $this->setTable('gateways');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('MikrotikDhcpTa_Controllers', [
            'foreignKey' => 'controller_id',
            'joinType' => 'INNER'
        ]);
      
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('address')
            ->maxLength('address', 45)
            ->requirePresence('address', 'create')
            ->notEmpty('address');

        $validator
            ->scalar('interface')
            ->maxLength('interface', 45)
            ->requirePresence('interface', 'create')
            ->notEmpty('interface');

        $validator
            ->scalar('comment')
            ->maxLength('comment', 100)
            ->allowEmpty('comment');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['controller_id'], 'MikrotikDhcpTa_Controllers'));
 

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'ispbrain_mikrotik_dhcp_ta';
    }
}
