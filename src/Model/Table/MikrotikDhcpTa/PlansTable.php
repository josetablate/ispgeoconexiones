<?php
namespace App\Model\Table\MikrotikDhcpTa;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

/**
 * TaPlans Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Controllers
 * @property \Cake\ORM\Association\BelongsTo $Profiles
 * @property \Cake\ORM\Association\BelongsTo $Pools
 * @property \Cake\ORM\Association\BelongsTo $Plans
 * @property \Cake\ORM\Association\HasMany $Plans
 *
 * @method \App\Model\Entity\TaPlan get($primaryKey, $options = [])
 * @method \App\Model\Entity\TaPlan newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TaPlan[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TaPlan|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TaPlan patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TaPlan[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TaPlan findOrCreate($search, callable $callback = null)
 */
class PlansTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {  
        parent::initialize($config);
         
        $this->setEntityClass('App\Model\Entity\MikrotikDhcpTa\Plan');
         

        $this->setTable('plans');
        
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        
        $this->belongsTo('MikrotikDhcpTa_Profiles', [
            'foreignKey' => 'profile_id',
            'joinType' => 'INNER',
            'propertyName' => 'profile'
        ]);
        
        $this->belongsTo('MikrotikDhcpTa_Controllers', [
            'foreignKey' => 'controller_id',
            'joinType' => 'INNER',
            
        ]);
        
        $this->belongsTo('MikrotikDhcpTa_Networks', [
            'foreignKey' => 'network_id',
            'joinType' => 'LEFT',
            'propertyName' => 'network'
        ]);
        
        
        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->existsIn(['controller_id'], 'TControllers'));
        // $rules->add($rules->existsIn(['profile_id'], 'TProfiles'));
        // $rules->add($rules->existsIn(['pool_id'], 'TPools'));
  

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'ispbrain_mikrotik_dhcp_ta';
    }
   
}
