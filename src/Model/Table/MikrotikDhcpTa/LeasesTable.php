<?php
namespace App\Model\Table\MikrotikDhcpTa;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Rule\IsUnique;


class LeasesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
         
        $this->setEntityClass('App\Model\Entity\MikrotikDhcpTa\Lease');

        $this->setTable('leases');
        $this->setDisplayField('comment');
        $this->setPrimaryKey('id');

        
        $this->belongsTo('MikrotikDhcpTa_Controllers', [
            'foreignKey' => 'controller_id',
            'joinType' => 'INNER',
            'propertyName' => 'controller'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('address')
            ->requirePresence('address', 'create')
            ->notEmpty('address');
            
        $validator
            ->requirePresence('mac_address', 'create')
            ->notEmpty('mac_address');
            
        $validator
            ->requirePresence('server', 'create')
            ->notEmpty('server');
            
         $validator
            ->requirePresence('address_lists', 'create')
            ->notEmpty('address_lists');

        $validator
            ->notEmpty('comment');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules){

//         $rules->add($rules->isUnique(['address'], 'Esta IP ya existe en la base de datos'));
//         $rules->add($rules->isUnique(['mac_address'], 'Este MAC ya existe en la base de datos'));
        
        $rules->add($rules->existsIn(['controller_id'], 'MikrotikDhcpTa_Controllers'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'ispbrain_mikrotik_dhcp_ta';
    }
}
