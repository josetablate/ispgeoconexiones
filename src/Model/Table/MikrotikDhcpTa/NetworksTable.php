<?php
namespace App\Model\Table\MikrotikDhcpTa;


use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Table\Templates\A\TControllers;


class NetworksTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {     
        
        parent::initialize($config);
         
        $this->setEntityClass('App\Model\Entity\MikrotikDhcpTa\Network');

        $this->setTable('networks');
        $this->setDisplayField('nameAndAddresses');
        $this->setPrimaryKey('id');

       
        $this->belongsTo('MikrotikDhcpTa_Controllers', [
            'foreignKey' => 'controller_id',
            'joinType' => 'INNER'
        ]);
        
        $this->hasMany('MikrotikDhcpTa_Plans', [
            'foreignKey' => 'network_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('comment', 'create')
            ->notEmpty('comment');

        $validator
            ->requirePresence('address', 'create')
            ->notEmpty('address');

        $validator
            ->requirePresence('min_host', 'create')
            ->notEmpty('min_host');

        $validator
            ->requirePresence('max_host', 'create')
            ->notEmpty('max_host');
            
         $validator
            ->requirePresence('gateway', 'create')
            ->notEmpty('gateway');           
    

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->isUnique(['address'], 'Existe otro pool con este addresses.'));
        $rules->add($rules->existsIn(['controller_id'], 'MikrotikDhcpTa_Controllers'));
        // $rules->add($rules->isUnique(['name'], 'Existe otro pool con este nombre.'));
        // $rules->add($rules->existsIn(['next_pool_id'], 'Pools'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'ispbrain_mikrotik_dhcp_ta';
    }
}
