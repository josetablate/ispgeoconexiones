<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Log\Log;

/**
 * Tickets Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Connections
 * @property \Cake\ORM\Association\BelongsTo $AsignedUsers
 * @property \Cake\ORM\Association\BelongsTo $Customers
 * @property \Cake\ORM\Association\HasMany $TicketsRecords
 *
 * @method \App\Model\Entity\Ticket get($primaryKey, $options = [])
 * @method \App\Model\Entity\Ticket newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Ticket[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Ticket|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ticket patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Ticket[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Ticket findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TicketsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tickets');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Connections', [
            'foreignKey' => 'connection_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'asigned_user_id',
            'propertyName' => 'asigned_user_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id',
            'joinType' => 'LEFT'
        ]);
        $this->hasMany('TicketsRecords', [
            'foreignKey' => 'ticket_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('CategoriesTickets', [
            'foreignKey' => 'category',
            'propertyName' => 'category'
        ]);
        $this->belongsTo('StatusTickets', [
            'foreignKey' => 'status',
            'propertyName' => 'status'
        ]);
        $this->belongsTo('ServicePending', [
            'foreignKey' => 'service_pending_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Areas', [
            'foreignKey' => 'area_id',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        $validator
            ->integer('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->requirePresence('category', 'create')
            ->notEmpty('category');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['connection_id'], 'Connections'));
        $rules->add($rules->existsIn(['asigned_user_id'], 'Users'));
        $rules->add($rules->existsIn(['customer_id'], 'Customers'));

        return $rules;
    }

    public function findServerSideData(Query $query, array $options)
    {
        $params  = $options['params'];
        $role_id = array_key_exists('role_id', $options) ? $options['role_id'] : NULL;

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;
        $columns = [
            '',                           //0
            'Tickets.id',                 //1
            'Tickets.created',            //2
            'Tickets.modified',           //3
            'Tickets.user_id',            //4
            'StatusTickets.name',         //5
            'CategoriesTickets.name',     //6
            'Customers.code',             //7
            'Customers.name',             //8
            'Customers.address',          //9
            'Tickets.start_task',         //10
            'Tickets.asigned_user_id',    //11
            'Tickets.title',              //12
            'Tickets.area_id',            //13
            'Tickets.description',        //14
            'Tickets.service_pending_id', //15
        ];

        $columns_select = [
            'Tickets.id',
            'Tickets.created',
            'Tickets.modified',
            'Tickets.user_id',
            'Tickets.start_task',
            'Tickets.asigned_user_id',
            'Tickets.title',
            'Tickets.description',
            'CategoriesTickets.name',
            'CategoriesTickets.color_text',
            'CategoriesTickets.color_back',
            'StatusTickets.name',
            'Customers.code',
            'Customers.name',
            'Customers.doc_type',
            'Customers.ident',
            'Customers.phone',
            'Customers.address',
            'Customers.lat',
            'Customers.lng',
            'ServicePending.lat',
            'ServicePending.lng',
            'ServicePending.address',
            'ServicePending.service_name',
            'Connections.id',
            'Connections.lat',
            'Connections.lng',
            'Connections.ip',
            'Connections.address',
            'Tickets.area_id',
        ];

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] =>  $o['dir']];
        }

        $query = $query->contain([
            'StatusTickets',
            'Customers',
            'CategoriesTickets',
            'Users',
            'Connections',
            'TicketsRecords.Users',
            'ServicePending',
            'Areas'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        $labels_ids = []; 

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Tickets.created':
                    case 'Tickets.modified':
                    case 'Tickets.duedate':
                    case 'Tickets.start_task':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0].' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Tickets.id':
                    case 'Customers.code':
                    case 'Tickets.user_id':
                    case 'Tickets.asigned_user_id':
                    case 'StatusTickets.name':
                    case 'CategoriesTickets.name':
                    case 'Tickets.area_id':
                        if ($columns[$index] == 'Tickets.asigned_user_id' && $column['search']['value'] == 0) {
                            $where += [$columns[$index] . ' IS' => NULL];
                        } else {
                            $where += [$columns[$index] => $column['search']['value']];
                        }
                        break;

                    case 'Tickets.title':
                    case 'Tickets.description':
                    case 'Customers.name':
                    case 'Customers.address':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Tickets.id':
                    case 'Tickets.customer_id':
                    case 'Customers.code':
                    case 'Tickets.area_id':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Tickets.description':
                    case 'Tickets.title':
                    case 'Customers.name':
                    case 'Customers.address':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {
                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        // Usuarios puedan ver los tickets asigandos a sus usuarios y los que creo él.
        if ($role_id) {
            $orWhere += ['Tickets.asigned_user_id' => $_SESSION['Auth']['User']['id']];
            $orWhere += ['Tickets.user_id' => $_SESSION['Auth']['User']['id']];
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order($order);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        $query->map(function ($row) { // map() is a collection method, it executes the query

            $table = TableRegistry::get('Users');
            if ($row->asigned_user_id) {
                $row->asigned_user = $table->get($row->asigned_user_id);
            }

            return $row;
        })->toArray();

        return $query;
    }

    public function findRecordsFiltered(Query $query, array $options)
    {
        $params  = $options['params'];
        $role_id = array_key_exists('role_id', $options) ? $options['role_id'] : NULL;

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;
        $columns = [
            '',                           //0
            'Tickets.id',                 //1
            'Tickets.created',            //2
            'Tickets.modified',           //3
            'Tickets.user_id',            //4
            'StatusTickets.name',         //5
            'CategoriesTickets.name',     //6
            'Customers.code',             //7
            'Customers.name',             //8
            'Customers.address',          //9
            'Tickets.start_task',         //10
            'Tickets.asigned_user_id',    //11
            'Tickets.title',              //12
            'Tickets.area_id',            //13
            'Tickets.description',        //14
            'Tickets.service_pending_id', //15
        ];

        $columns_select = [
            'Tickets.id',
            'Tickets.created',
            'Tickets.modified',
            'Tickets.user_id',
            'StatusTickets.name',
            'CategoriesTickets.name',
            'Customers.code',
            'Customers.name',
            'Customers.doc_type',
            'Customers.ident',
            'Tickets.start_task',
            'Tickets.asigned_user_id',
            'Tickets.title',
            'Tickets.description',
            'CategoriesTickets.color_text',
            'CategoriesTickets.color_back',
            'Customers.phone',
            'Customers.address',
            'Tickets.service_pending_id',
            'ServicePending.address',
            'ServicePending.lat',
            'ServicePending.lng',
            'ServicePending.service_name',
            'Tickets.area_id',
        ];

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        $query = $query->contain([
            'StatusTickets',
            'Customers',
            'CategoriesTickets',
            'Users',
            'Connections',
            'TicketsRecords.Users',
            'ServicePending',
            'Areas'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        $labels_ids = []; 

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Tickets.created':
                    case 'Tickets.modified':
                    case 'Tickets.duedate':
                    case 'Tickets.start_task':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        }
                        else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0].' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        }
                        else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Tickets.id':
                    case 'Customers.code':
                    case 'Tickets.user_id':
                    case 'Tickets.asigned_user_id':
                    case 'StatusTickets.name':
                    case 'CategoriesTickets.name':
                    case 'Tickets.area_id':
                        if ($columns[$index] == 'Tickets.asigned_user_id' && $column['search']['value'] == 0) {
                            $where += [$columns[$index] . ' IS' => NULL];
                        } else {
                            $where += [$columns[$index] => $column['search']['value']];
                        }
                        break;

                    case 'Tickets.description':
                    case 'Tickets.title':
                    case 'Customers.name':
                    case 'Customers.address':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Tickets.id':
                    case 'Tickets.customer_id':
                    case 'Customers.code':
                    case 'Tickets.area_id':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Tickets.title':
                    case 'Tickets.description':
                    case 'Customers.name':
                    case 'Customers.address':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {
                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        // Usuarios puedan ver los tickets asigandos a sus usuarios y los que creo él.
        if ($role_id) {
            $orWhere += ['Tickets.asigned_user_id' => $_SESSION['Auth']['User']['id']];
            $orWhere += ['Tickets.user_id' => $_SESSION['Auth']['User']['id']];
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        return $query->count();
    }
}
