<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CashEntitiesTransactions Model
 *
 * @property \App\Model\Table\UserOriginsTable|\Cake\ORM\Association\BelongsTo $UserOrigins
 * @property \App\Model\Table\UserDestinationsTable|\Cake\ORM\Association\BelongsTo $UserDestinations
 *
 * @method \App\Model\Entity\CashEntitiesTransaction get($primaryKey, $options = [])
 * @method \App\Model\Entity\CashEntitiesTransaction newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CashEntitiesTransaction[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CashEntitiesTransaction|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CashEntitiesTransaction patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CashEntitiesTransaction[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CashEntitiesTransaction findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CashEntitiesTransactionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cash_entities_transactions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        
        // $this->belongsTo('Users', [
        //     'foreignKey' => 'user_destination_id',
        //     'joinType' => 'INNER',
        //     'propertyName' => 'destination'
        // ]);
        
         $this->belongsTo('Users', [
            'foreignKey' => 'user_origin_id',
            'joinType' => 'INNER',
            'propertyName' => 'origin'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->decimal('value')
            ->requirePresence('value', 'create')
            ->notEmpty('value');

        $validator
            ->scalar('type')
            ->allowEmpty('type');

        $validator
            ->scalar('concept')
            ->allowEmpty('concept');

        $validator
            ->dateTime('acepted')
            ->allowEmpty('acepted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->existsIn(['user_origin_id'], 'UserOrigins'));
        // $rules->add($rules->existsIn(['user_destination_id'], 'UserDestinations'));

        return $rules;
    }
}
