<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Log\Log;

/**
 * Sms Model
 *
 * @property |\Cake\ORM\Association\BelongsTo $Bloques
 *
 * @method \App\Model\Entity\Sm get($primaryKey, $options = [])
 * @method \App\Model\Entity\Sm newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Sm[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Sm|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Sm patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Sm[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Sm findOrCreate($search, callable $callback = null, $options = [])
 */
class SmsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('sms');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        
        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_code',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        
        $this->belongsTo('SmsBloques', [
            'foreignKey' => 'bloque_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('message')
            ->requirePresence('message', 'create')
            ->notEmpty('message');

        $validator
            ->integer('customer_code')
            ->requirePresence('customer_code', 'create')
            ->notEmpty('customer_code');

        $validator
            ->scalar('status')
            ->allowEmpty('status');

        return $validator;
    }

    public function findServerSideData(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            'SmsBloques.template_name',
            'SmsBloques.id',
            'Sms.message',
            'Sms.status',
            'SmsBloques.created',
            'Customers.name',
            'Customers.ident',
            'Customers.code',
            'Sms.phone',
            'Sms.business_billing',
            'SmsBloques.user_id',
            'SmsBloques.platform'
        ];

        $columns_select = [
            'Sms.id',
            'SmsBloques.template_name',
            'SmsBloques.id',
            'Sms.message',
            'Sms.status',
            'SmsBloques.created',
            'Customers.name',
            'Customers.doc_type',
            'Customers.ident',
            'Customers.code',
            'Sms.phone',
            'Sms.business_billing',
            'SmsBloques.user_id',
            'SmsBloques.platform',
        ];

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] =>  $o['dir']];
        }

        $query = $query->contain([
            'SmsBloques',
            'Customers',
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'SmsBloques.created': 

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2]  . '-' . $value[1][1] . '-' .  $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
 
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Customers.code':
                    case 'Sms.status':
                    case 'SmsBloques.id':
                    case 'SmsBloques.user_id':
                    case 'Sms.business_billing': 

                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    default: 
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {
                    case 'SmsBloques.created':
                        break;
                    case 'Customers.code': 
                    case 'SmsBloques.id':
                    case 'Sms.business_billing':
                    case 'Sms.status':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;
                    case 'SmsBloques.template_name':
                    case 'Sms.message':
                    case 'Customers.name':
                    case 'Customers.ident':
                    case 'SmsBloques.platform':
                    case 'Sms.phone':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order($order);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        return $query;
    }

    public function findRecordsFiltered(Query $query, array $options)
    {
        $params = $options['params'];

        $where = [];
        $between = [];
        $orWhere = [];

        $columns = [
            'SmsBloques.template_name',
            'SmsBloques.id',
            'Sms.message',
            'Sms.status',
            'SmsBloques.created',
            'Customers.name',
            'Customers.ident',
            'Customers.code',
            'Sms.phone',
            'Sms.business_billing',
            'SmsBloques.user_id',
            'SmsBloques.platform',
        ];

        $columns_select = [
            'SmsBloques.template_name',
            'SmsBloques.id',
            'Sms.message',
            'Sms.status',
            'SmsBloques.created',
            'Customers.name',
            'Customers.doc_type',
            'Customers.ident',
            'Customers.code',
            'Sms.phone',
            'Sms.business_billing',
            'SmsBloques.user_id',
            'SmsBloques.platform',
        ];

        $query = $query->contain([
            'SmsBloques',
            'Customers',
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'SmsBloques.created': 

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2]  . '-' . $value[1][1] . '-' .  $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                            
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Customers.code':
                    case 'Sms.status':
                    case 'SmsBloques.id':
                    case 'SmsBloques.user_id':
                    case 'Sms.business_billing': 

                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    default: 
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {
                    case 'SmsBloques.created':
                        break;
                    case 'Customers.code': 
                    case 'SmsBloques.id':
                    case 'Sms.business_billing':
                    case 'Sms.status':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;
                    case 'SmsBloques.template_name':
                    case 'Sms.message':
                    case 'Customers.name':
                    case 'Customers.ident':
                    case 'SmsBloques.platform':
                    case 'Sms.phone':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {
            foreach ($between as $b) {
                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }
}
