<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Log\Log;

/**
 * Customers Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Cities
 * @property \Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Customer get($primaryKey, $options = [])
 * @method \App\Model\Entity\Customer newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Customer[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Customer|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Customer patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Customer[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Customer findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CustomersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('customers');
        $this->setDisplayField('customer_code');
        $this->setPrimaryKey('code');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Countries', [
            'foreignKey' => 'country_id',
           'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Provinces', [
            'foreignKey' => 'province_id',
           'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Cities', [
            'foreignKey' => 'city_id',
           'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Areas', [
            'foreignKey' => 'area_id',
           'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Users');

        $this->hasMany('Debts', [
            'foreignKey' => 'customer_code',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('Comprobantes', [
            'foreignKey' => 'customer_code'
        ]);  

        $this->hasMany('Payments', [
            'foreignKey' => 'customer_code'
        ]);

        $this->hasMany('Receipts', [
            'foreignKey' => 'customer_code'
        ]);

        $this->hasMany('Invoices', [
            'foreignKey' => 'customer_code',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('DebitNotes', [
            'foreignKey' => 'customer_code',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('CreditNotes', [
            'foreignKey' => 'customer_code',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('CustomersHasDiscounts', [
            'foreignKey' => 'customer_code',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('Connections', [
            'foreignKey' => 'customer_code',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Accounts', [
            'foreignKey' => 'account_code',
            'joinType' => 'LEFT'
        ]);

        $this->belongsToMany('Labels', [
            'targetForeignKey' => 'label_id',
            'foreignKey' => 'customer_code',
            'joinTable' => 'customers_labels',
            'strategy' => 'subquery'
        ]);

        $this->hasMany('ServicePending', [
            'foreignKey' => 'customer_code',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('CobrodigitalAccounts', [
            'foreignKey' => 'customer_code',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('VisaAutoDebitAccounts', [
            'foreignKey' => 'customer_code',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('MastercardAutoDebitAccounts', [
            'foreignKey' => 'customer_code',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('CustomersAccounts', [
            'foreignKey' => 'customer_code',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('code')
            ->allowEmpty('code', 'create');

        $validator
            ->allowEmpty('status');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('address', 'create')
            ->notEmpty('address');

        $validator
            ->integer('responsible')
            ->requirePresence('responsible', 'create')
            ->notEmpty('responsible');

        $validator
            ->integer('doc_type')
            ->requirePresence('doc_type', 'create')
            ->notEmpty('doc_type');

        $validator
            ->requirePresence('phone', 'create')
            ->notEmpty('phone');

        $validator
            ->allowEmpty('phone_alt');

        $validator
            ->boolean('is_presupeusto')
            ->allowEmpty('is_presupeusto');

        $validator
            ->boolean('asked_router')
            ->allowEmpty('asked_router');

        $validator
            ->allowEmpty('availability');

        $validator
            ->allowEmpty('comments');

        $validator
            ->allowEmpty('clave_portal');

        $validator
            ->allowEmpty('seller');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['area_id'], 'Areas'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function findServerSideData(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length'];
        $page = 1;

        $columns = [
            '',                                     //0
            'Customers.created',                    //1
            'Customers.code',                       //2
            'Customers.account_code',               //3
            'Customers.etiquetas',                  //4
            'Customers.invoices_no_paid',           //5
            'Customers.debt_month',                 //6
            'Customers.name',                       //7
            'Customers.doc_type',                   //8
            'Customers.ident',                      //9
             '',                                    //10
            'Customers.address',                    //11
            'Customers.area_id',                    //12
            'Customers.phone',                      //13
            'Customers.seller',                     //14
            'Customers.responsible',                //15
            'Customers.is_presupuesto',             //16
            'Customers.denomination',               //17
            'Customers.email',                      //18
            'Customers.comments',                   //19
            'Customers.daydue',                     //20
            'Customers.clave_portal',               //21
            'Customers.business_billing',           //22
            'Customers.payment_commitment',         //23
            '',                                     //24
            '',                                     //25
            '',                                     //26
            'Customers.value_service',              //27
        ];

        $columns_select = [
            'Customers.created',
            'Customers.code',
            'Customers.account_code',
            'Customers.debt_month',
            'Customers.name',
            'Customers.doc_type',
            'Customers.ident',
            'Customers.address',
            'Customers.phone',
            'Customers.phone_alt',
            'Customers.seller',
            'Customers.responsible',
            'Customers.cond_venta',
            'Customers.is_presupuesto',
            'Customers.denomination',
            'Customers.email',
            'Customers.comments',
            'Customers.daydue',
            'Customers.clave_portal',
            'Customers.business_billing',
            'Customers.payment_method_default_id',
            'Customers.invoices_no_paid',
            'Customers.area_id',
            'Users.id',
            'Areas.name',
            'Customers.payment_commitment',
            'Customers.value_service',
        ];

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] => $o['dir']];
        }

        // para buscar conexiones habilitadas y bloqueadas
        if ($params['columns'][10]['search']['value'] != '') {

            $enabled = $params['columns'][10]['search']['value'];

            $query = $query->matching('Connections', function ($q) use ($enabled) {
                return $q->where([
                    'Connections.enabled' => $enabled,
                    'Connections.deleted' => FALSE
                ]);
            });
        }

        // cuentas metodos de pagos
        if (array_key_exists(24, $params['columns'])) {

            if ($params['columns'][24]['search']['value'] != '') {

                $payemnt_getway_id = $params['columns'][24]['search']['value'];

                $query = $query->matching('CustomersAccounts', function ($q) use ($payemnt_getway_id) {
                    return $q->where(['CustomersAccounts.payment_getway_id' => $payemnt_getway_id]);
                });
            }
        }

        $controller_id = NULL;

        // controlador
        if (array_key_exists(25, $params['columns'])) {

            if ($params['columns'][25]['search']['value'] != '') {

                $controller_id = $params['columns'][25]['search']['value'];

                $query = $query->matching('Connections', function ($q) use ($controller_id) {
                    return $q->where([
                        'Connections.controller_id' => $controller_id,
                        'Connections.deleted'       => FALSE
                    ]);
                });
            }
        }

        $service_id = NULL;

        // servicio
        if (array_key_exists(26, $params['columns'])) {

            if ($params['columns'][26]['search']['value'] != '') {

                $service_id = $params['columns'][26]['search']['value'];

                $query = $query->matching('Connections', function ($q) use ($service_id) {
                    return $q->where([
                        'Connections.service_id' => $service_id,
                        'Connections.deleted'    => FALSE
                    ]);
                });
            }
        }

        $query = $query->contain([
            'Areas',
            'Connections',
            'ServicePending.Services',
            'ServicePending.Presales.Users',
            'Labels',
            'Users',
            'CustomersAccounts'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case '':
                        break;

                    case 'Customers.created':
                    case 'Customers.payment_commitment':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2]  . '-' . $value[1][1] . '-' .  $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] .' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Customers.etiquetas':

                        $labels_ids_search = []; 
                        $customer_codes_labels = [];
                        $customer_labels = [];

                        $labels_ids_search = explode(',', $column['search']['value']);

                        if (in_array(0, $labels_ids_search)) { //buscada de sin etiquetas

                            $tableCustomersLabels = TableRegistry::get('CustomersLabels');
        				    $customer_codes_labels = $tableCustomersLabels->find('list')->toArray();

                            if (count($customer_codes_labels) > 0) {
                                $where += ['code NOT IN' => $customer_codes_labels];
                            }
                        } else { //con etiquetas

                            $tableCustomersLabels = TableRegistry::get('CustomersLabels');
        				    $tableCustomersLabels = $tableCustomersLabels->find()
                                ->where(['label_id IN' => $labels_ids_search]); 

                            foreach ($tableCustomersLabels as $tcl) {
                                if (!array_key_exists($tcl->customer_code, $customer_labels)) {
                                    $customer_labels[$tcl->customer_code] = [];
                                }
                                $customer_labels[$tcl->customer_code][] =  $tcl->label_id;
                            }

                            foreach ($customer_labels as $code => $labels) {
                                if (!array_diff($labels_ids_search, $labels)) {
                                    $customer_codes_labels[] = $code;
                                }
                            }

                            if (count($customer_codes_labels) > 0) {
                                $where += ['code IN' => $customer_codes_labels];
                            } else {
                                $where += ['code IN' => -1];
                            }
                        }
                        break;

                    case 'Customers.responsible':
                    case 'Customers.code':
                    case 'Customers.daydue':
                    case 'Customers.business_billing':
                    case 'Customers.area_id':
                    case 'Customers.doc_type':
                    case 'Customers.account_code':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Customers.debt_month':
                    case 'Customers.value_service':
                    case 'Customers.invoices_no_paid':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Customers.is_presupuesto':
                    case 'Customers.denomination':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? true : false];
                        break;

                    default: 
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case '':
                        break;

                    case 'Customers.created':
                    case 'Customers.payment_commitment':
                    case 'Customers.invoices_no_paid':
                    case 'Customers.debt_month':
                    case 'Customers.daydue':
                    case 'Customers.clave_portal':
                    case 'Customers.responsible':
                    case 'Customers.is_presupuesto':
                    case 'Customers.denomination':
                    case 'Customers.business_billing':
                    case 'Customers.value_service':
                        break;

                    case 'Customers.code':
                    case 'Customers.account_code':
                    case 'Customers.doc_type':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Customers.name':
                    case 'Customers.ident':
                    case 'Customers.address':
                    case 'Areas.name':
                    case 'Customers.phone':
                    case 'Customers.phone_alt':
                    case 'Customers.seller':
                    case 'Customers.email':
                    case 'Customers.comments':
                    case 'Customers.clave_portal':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order($order);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        $query->map(function ($row) use ($controller_id, $service_id) { // map() is a collection method, it executes the query

            //cargar historial de compromiso de pago
            $tablePaymentCommitment = TableRegistry::get('PaymentCommitment');
        	$paymentCommitment = $tablePaymentCommitment
        	    ->find()
        	    ->where([
        	        'duedate >'     => Time::now(),
        	        'customer_code' => $row->code,
        	        'ok IS'         => NULL
    	        ])
    	        ->first();

	        $tableControllers = TableRegistry::get('Controllers');
            $controllers_ids = $tableControllers->find('list')->toArray();

            $tableServices = TableRegistry::get('Services');
            $services_ids = $tableServices->find('list')->toArray();

            if ($paymentCommitment) {
                $row->payment_commitment = $paymentCommitment->duedate;
            }

            $row->services_active = 0;
            $row->services_total = 0;

            foreach ($row->connections as $conn) {

                if (!$conn->deleted) {

                    $row->services_total++;

                    if ($conn->enabled) {
                        $row->services_active++;
                    }

                    if ($controller_id) {
                        $row->controller = $controllers_ids[$controller_id] . ' - ';
                    } else {
                        $row->controller .= $controllers_ids[$conn->controller_id] . ' - ';
                    }

                    if ($service_id) {
                        $row->service = $services_ids[$service_id] . ' - ';
                    } else {
                        $row->service .= $services_ids[$conn->service_id] . ' - ';
                    }
                }
            }

            $service_pending_no_used = [];
            foreach ($row->service_pending as $sp) {

                if (!$sp->used) {
                    $service_pending_no_used[] = $sp;
                }
            }

            $row->service_pending = $service_pending_no_used;
            return $row;
        })
        ->toArray();

        return $query;
    }

    public function findRecordsFiltered(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length'];
        $page = 1;

        $columns = [
            '',                                     //0
            'Customers.created',                    //1
            'Customers.code',                       //2
            'Customers.account_code',               //3
            'Customers.etiquetas',                  //4
            'Customers.invoices_no_paid',           //5
            'Customers.debt_month',                 //6
            'Customers.name',                       //7
            'Customers.doc_type',                   //8
            'Customers.ident',                      //9
             '',                                    //10
            'Customers.address',                    //11
            'Customers.area_id',                    //12
            'Customers.phone',                      //13
            'Customers.seller',                     //14
            'Customers.responsible',                //15
            'Customers.is_presupuesto',             //16
            'Customers.denomination',               //17
            'Customers.email',                      //18
            'Customers.comments',                   //19
            'Customers.daydue',                     //20
            'Customers.clave_portal',               //21
            'Customers.business_billing',           //22
            'Customers.payment_commitment',         //23
            '',                                     //24
            '',                                     //25
            '',                                     //26
            'Customers.value_service',              //27
        ];

        $columns_select = [
            'Customers.created',
            'Customers.code',
            'Customers.account_code',
            'Customers.debt_month',
            'Customers.name',
            'Customers.doc_type',
            'Customers.ident',
            'Customers.address',
            'Customers.phone',
            'Customers.phone_alt',
            'Customers.seller',
            'Customers.responsible',
            'Customers.cond_venta',
            'Customers.is_presupuesto',
            'Customers.denomination',
            'Customers.email',
            'Customers.comments',
            'Customers.daydue',
            'Customers.clave_portal',
            'Customers.business_billing',
            'Customers.payment_method_default_id',
            'Customers.invoices_no_paid',
            'Customers.area_id',
            'Users.id',
            'Areas.name',
            'Customers.payment_commitment',
            'Customers.value_service',
        ];

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] =>  $o['dir']];
        }

        // para saber si la conexiones esta bloqueada o habilitada
        if (array_key_exists(10, $params['columns'])) {

            if ($params['columns'][10]['search']['value'] != '') {

                $enabled = $params['columns'][10]['search']['value'];

                $query = $query->matching('Connections', function ($q) use ($enabled) {
                    return $q->where([
                        'Connections.enabled' => $enabled,
                        'Connections.deleted' => FALSE
                    ]);
                });
            }
        }

        // cuentas de metodos de pagos
        if (array_key_exists(24, $params['columns'])) {

            if ($params['columns'][24]['search']['value'] != '') {

                $payemnt_getway_id = $params['columns'][24]['search']['value'];

                $query = $query->matching('CustomersAccounts', function ($q) use ($payemnt_getway_id) {
                    return $q->where([
                        'CustomersAccounts.payment_getway_id' => $payemnt_getway_id,
                        'CustomersAccounts.deleted'           => 0
                    ]);
                });
            }
        }

        // controlador
        if (array_key_exists(25, $params['columns'])) {

            if ($params['columns'][25]['search']['value'] != '') {

                $controller_id = $params['columns'][25]['search']['value'];

                $query = $query->matching('Connections', function ($q) use ($controller_id) {
                    return $q->where([
                        'Connections.controller_id' => $controller_id,
                        'Connections.deleted'       => FALSE
                    ]);
                });
            }
        }

        // servicio
        if (array_key_exists(26, $params['columns'])) {

            if ($params['columns'][26]['search']['value'] != '') {

                $service_id = $params['columns'][26]['search']['value'];

                $query = $query->matching('Connections', function ($q) use ($service_id) {
                    return $q->where([
                        'Connections.service_id' => $service_id,
                        'Connections.deleted'    => FALSE
                    ]);
                });
            }
        }

        $query = $query->contain([
            'Areas',
            'Connections',
            'ServicePending.Services',
            'ServicePending.Presales.Users',
            'Labels',
            'Users',
            'CustomersAccounts'
        ])->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case '':
                        break;

                    case 'Customers.created':
                    case 'Customers.payment_commitment':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Customers.etiquetas':

                        $labels_ids_search = []; 
                        $customer_codes_labels = [];
                        $customer_labels = [];

                        $labels_ids_search = explode(',', $column['search']['value']);

                        if (in_array(0, $labels_ids_search)) { //buscada de sin etiquetas
                            
                            $tableCustomersLabels = TableRegistry::get('CustomersLabels');
        				    $customer_codes_labels = $tableCustomersLabels->find('list')->toArray();

                            if (count($customer_codes_labels) > 0) {
                                $where += ['code NOT IN' => $customer_codes_labels];
                            }
                        } else { //con etiquetas

                            $tableCustomersLabels = TableRegistry::get('CustomersLabels');
        				    $tableCustomersLabels = $tableCustomersLabels->find()
                                ->where(['label_id IN' => $labels_ids_search]); 

                            foreach ($tableCustomersLabels as $tcl) {
                                if (!array_key_exists($tcl->customer_code,$customer_labels)) {
                                    $customer_labels[$tcl->customer_code] = [];
                                }
                                $customer_labels[$tcl->customer_code][] =  $tcl->label_id;
                            }

                            foreach ($customer_labels as $code => $labels) {
                                
                                if (!array_diff($labels_ids_search, $labels)) {
                                    $customer_codes_labels[] = $code;
                                }
                            }

                            if (count($customer_codes_labels) > 0) {
                                $where += ['code IN' => $customer_codes_labels];
                            } else {
                                $where += ['code IN' => -1];
                            }
                        }
                        break;

                    case 'Customers.responsible':
                    case 'Customers.code':
                    case 'Customers.daydue':
                    case 'Customers.business_billing':
                    case 'Customers.doc_type':
                    case 'Customers.area_id':
                    case 'Customers.account_code':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Customers.debt_month':
                    case 'Customers.invoices_no_paid':
                    case 'Customers.value_service':

                        $value = explode('<>', $column['search']['value']);

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Customers.is_presupuesto':
                    case 'Customers.denomination':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? true : false];
                        break;

                    default:
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {
            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case '':
                        break;

                    case 'Customers.created':
                    case 'Customers.payment_commitment':
                    case 'Customers.invoices_no_paid':
                    case 'Customers.debt_month':
                    case 'Customers.daydue':
                    case 'Customers.clave_portal':
                    case 'Customers.responsible':
                    case 'Customers.is_presupuesto':
                    case 'Customers.denomination':
                    case 'Customers.business_billing':
                    case 'Customers.value_service':
                        break;

                    case 'Customers.code':
                    case 'Customers.account_code':
                    case 'Customers.doc_type':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Customers.name':
                    case 'Customers.ident':
                    case 'Customers.address':
                    case 'Areas.name':
                    case 'Customers.phone':
                    case 'Customers.phone_alt':
                    case 'Customers.seller':
                    case 'Customers.email':
                    case 'Customers.comments':
                    case 'Customers.clave_portal':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {
            foreach ($between as $b) {
                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }

    public function findServerSideDataCheckSelection(Query $query, array $options)
    {
        $params = $options['params'];
        $type = $options['type'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length'];
        $page = 1;

        $columns = [
            '',                                     //0
            'Customers.created',                    //1
            'Customers.code',                       //2
            'Customers.account_code',               //3
            'Customers.etiquetas',                  //4
            'Customers.invoices_no_paid',           //5
            'Customers.debt_month',                 //6
            'Customers.name',                       //7
            'Customers.ident',                      //8
            '',                                     //9
            'Customers.address',                    //10
            'Customers.area_id',                    //11
            'Customers.phone',                      //12
            'Customers.seller',                     //13
            'Customers.responsible',                //14
            'Customers.is_presupuesto',             //15
            'Customers.denomination',               //16
            'Customers.email',                      //17
            'Customers.comments',                   //18
            'Customers.daydue',                     //19
            'Customers.clave_portal',               //20
            'Customers.business_billing',           //21
            '',                                     //22
            '',                                     //23
            '',                                     //24
            'Customers.last_emails',                //25
            'Customers.last_sms',                   //26
            'Customers.payment_commitment',         //27
        ];

        $columns_select = [
            'Customers.created',
            'Customers.code',
            'Customers.account_code',
            'Customers.debt_month',
            'Customers.name',
            'Customers.ident',
            'Customers.address',
            'Customers.phone',
            'Customers.seller',
            'Customers.responsible',
            'Customers.cond_venta',
            'Customers.is_presupuesto',
            'Customers.denomination',
            'Customers.email',
            'Customers.comments',
            'Customers.daydue',
            'Customers.clave_portal',
            'Customers.business_billing',
            'Customers.doc_type',
            'Customers.payment_method_default_id',
            'Customers.invoices_no_paid',
            'Users.id',
            'Areas.name',
            'Customers.area_id',
            'Customers.last_emails',
            'Customers.last_sms',
            'Customers.payment_commitment',
        ];

        //paginacion
        if ($params['length'] > 0) {
            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] =>  $o['dir']];
        }

        if ($params['columns'][24]['search']['value'] != '') {

            $payemnt_getway_id = $params['columns'][24]['search']['value'];

            $query = $query->matching('CustomersAccounts', function ($q) use ($payemnt_getway_id) {
                return $q->where([
                    'CustomersAccounts.payment_getway_id' => $payemnt_getway_id,
                    'CustomersAccounts.deleted'           => 0
                ]);
            });
        }

        $query = $query->contain([
            'Areas',
            'Connections',
            'ServicePending.Services',
            'ServicePending.Presales.Users',
            'Labels',
            'Users',
            'CustomersAccounts'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case '':
                        break;

                    case 'Customers.created':
                    case 'Customers.last_emails':
                    case 'Customers.last_sms':
                    case 'Customers.payment_commitment':

                        $value = explode('<>', $column['search']['value']);

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2]  . '-' . $value[1][1] . '-' .  $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                            
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Customers.etiquetas':

                        $labels_ids_search = []; 
                        $customer_codes_labels = [];
                        $customer_labels = [];
                        
                        $labels_ids_search = explode(',', $column['search']['value']);

                        if (in_array(0, $labels_ids_search)) { //buscada de sin etiquetas
                        	
                        	$tableCustomersLabels = TableRegistry::get('CustomersLabels');
                        	$customer_codes_labels = $tableCustomersLabels->find('list')->toArray();

                        	if (count($customer_codes_labels) > 0) {
                        		$where += ['code NOT IN' => $customer_codes_labels];
                        	}
                        } else { //con etiquetas
                        
                        	$tableCustomersLabels = TableRegistry::get('CustomersLabels');
                        	$tableCustomersLabels = $tableCustomersLabels->find()
                        		->where(['label_id IN' => $labels_ids_search]); 
                        		
                        	foreach ($tableCustomersLabels as $tcl) {
                        		if (!array_key_exists($tcl->customer_code,$customer_labels)) {
                        			$customer_labels[$tcl->customer_code] = [];
                        		}
                        		$customer_labels[$tcl->customer_code][] =  $tcl->label_id;
                        	}

                        	foreach ($customer_labels as $code => $labels) {
                        		
                        		if (!array_diff($labels_ids_search, $labels)) {
                        			$customer_codes_labels[] = $code;
                        		}
                        	}

                        	if (count($customer_codes_labels) > 0) {
                        		$where += ['code IN' => $customer_codes_labels];
                        	} else {
                                $where += ['code IN' => -1];
                            }
                        }
                        break;

                    case 'Customers.responsible':
                    case 'Customers.code':
                    case 'Customers.daydue':
                    case 'Customers.business_billing':
                    case 'Customers.area_id':

                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Customers.debt_month':
                    case 'Customers.invoices_no_paid':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }

                        break;

                    case 'Customers.is_presupuesto':
                    case 'Customers.denomination':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? TRUE : FALSE];
                        break;

                    default:
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Customers.created':
                    case '':
                    case 'Customers.invoices_no_paid':
                    case 'Customers.debt_month':
                    case 'Customers.daydue':
                    case 'Customers.clave_portal':
                    case 'Customers.responsible':
                    case 'Customers.is_presupuesto':
                    case 'Customers.denomination':
                    case 'Customers.business_billing':
                    case 'Customers.payment_commitment':
                        break;

                    case 'Customers.code':
                    case 'Customers.account_code':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Customers.name':
                    case 'Customers.ident':
                    case 'Customers.address':
                    case 'Areas.name':
                    case 'Customers.phone':
                    case 'Customers.seller':
                    case 'Customers.email':
                    case 'Customers.comments':
                    case 'Customers.clave_portal':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order($order);

        $query->map(function ($row) { // map() is a collection method, it executes the query

            $tableControllers = TableRegistry::get('Controllers');
            $controllers_ids = $tableControllers->find('list')->toArray();

            $tableServices = TableRegistry::get('Services');
            $services_ids = $tableServices->find('list')->toArray();

            $row->services_active = 0;
            $row->services_total = 0;

            foreach ($row->connections as $conn) {

                $row->controller = $controllers_ids[$conn->controller_id] . ' - ';
                $row->service = $services_ids[$conn->service_id] . ' - ';

                if (!$conn->deleted) {
                    $row->services_total++;
                    if ($conn->enabled) {
                        $row->services_active++;
                    }
                }
            }

            $service_pending_no_used = []; 
            foreach ($row->service_pending as $sp) {

                if (!$sp->used) {
                    $service_pending_no_used[] = $sp;
                }
            }

            $row->service_pending = $service_pending_no_used;
            return $row;
        })
        ->toArray();

        // Servicio bloqueado y habilitado
        if ($params['columns'][9]['search']['value'] != '') {

            $customers_ok = [];

            foreach ($query as $customer) {

                if (sizeof($customer->connections) > 0) {

                    foreach ($customer->connections as $connection) {

                        $enabled = $params['columns'][9]['search']['value'] ? TRUE : FALSE;

                        if ($connection->deleted == FALSE && $connection->enabled == $enabled) {

                            array_push($customers_ok, $customer);
                        }
                    }
                }
            }
            $query = $customers_ok;
        }

        // controlador
        if ($params['columns'][22]['search']['value'] != '') {

            $customers_ok = [];

            foreach ($query as $customer) {

                if (sizeof($customer->connections) > 0) {

                    foreach ($customer->connections as $connection) {

                        $controller_id = $params['columns'][22]['search']['value'];

                        if ($connection->deleted == FALSE && $connection->controller_id == $controller_id) {

                            array_push($customers_ok, $customer);
                        }
                    }
                }
            }
            $query = $customers_ok;
        }

        // servicio
        if ($params['columns'][23]['search']['value'] != '') {

            $customers_ok = [];

            foreach ($query as $customer) {

                if (sizeof($customer->connections) > 0) {

                    foreach ($customer->connections as $connection) {

                        $service_id = $params['columns'][23]['search']['value'];

                        if ($connection->deleted == FALSE && $connection->service_id == $service_id) {

                            array_push($customers_ok, $customer);
                        }
                    }
                }
            }
            $query = $customers_ok;
        }

        if ($type != '') {

            $customers_ok = [];

            switch ($type) {

                case 'sms_getway':
                    foreach ($query as $customer) {

                        array_push($customers_ok, $customer);
                    }
                    break;

                case 'smsmasivo':
                    foreach ($query as $customer) {

                        if (!$this->verifySMSMasivePhone($customer->phone)) {
                            array_push($customers_ok, $customer);
                        }
                    }
                    break;

                case 'mass_email':
                    foreach ($query as $customer) {

                        if (!$this->verifyMassEmails($customer->email)) {
                            array_push($customers_ok, $customer);
                        }
                    }
                    break;
            }

            $query = $customers_ok;
        }

        $query_return = $query;

        if (sizeof($query_return) > 0 && $limit != -1) {
            $query_return = array_chunk($query_return, $limit);
            $query_return = $query_return[--$page];
        }

        return $query_return;
    }

    public function findRecordsFilteredCheckSelection(Query $query, array $options)
    {
        $params = $options['params'];
        $type = $options['type'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length'];
        $page = 1;

        $columns = [
            '',                                     //0
            'Customers.created',                    //1
            'Customers.code',                       //2
            'Customers.account_code',               //3
            'Customers.etiquetas',                  //4
            'Customers.invoices_no_paid',           //5
            'Customers.debt_month',                 //6
            'Customers.name',                       //7
            'Customers.ident',                      //8
            '',                                     //9
            'Customers.address',                    //10
            'Customers.area_id',                    //11
            'Customers.phone',                      //12
            'Customers.seller',                     //13
            'Customers.responsible',                //14
            'Customers.is_presupuesto',             //15
            'Customers.denomination',               //16
            'Customers.email',                      //17
            'Customers.comments',                   //18
            'Customers.daydue',                     //19
            'Customers.clave_portal',               //20
            'Customers.business_billing',           //21
            '',                                     //22
            '',                                     //23
            '',                                     //24
            'Customers.last_emails',                //25
            'Customers.last_sms',                   //26
            'Customers.payment_commitment',         //27
        ];

        $columns_select = [
            'Customers.created',
            'Customers.code',
            'Customers.account_code',
            'Customers.debt_month',
            'Customers.name',
            'Customers.ident',
            'Customers.address',
            'Customers.phone',
            'Customers.seller',
            'Customers.responsible',
            'Customers.cond_venta',
            'Customers.is_presupuesto',
            'Customers.denomination',
            'Customers.email',
            'Customers.comments',
            'Customers.daydue',
            'Customers.clave_portal',
            'Customers.business_billing',
            'Customers.doc_type',
            'Customers.payment_method_default_id',
            'Customers.invoices_no_paid',
            'Users.id',
            'Areas.name',
            'Customers.area_id',
            'Customers.last_emails',
            'Customers.last_sms',
            'Customers.payment_commitment',
        ];

        //paginacion
        if ($params['length'] > 0) {
            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] =>  $o['dir']];
        }

        if ($params['columns'][24]['search']['value'] != '') {

            $payemnt_getway_id = $params['columns'][24]['search']['value'];

            $query = $query->matching('CustomersAccounts', function ($q) use ($payemnt_getway_id) {
                return $q->where([
                    'CustomersAccounts.payment_getway_id' => $payemnt_getway_id,
                    'CustomersAccounts.deleted'           => 0
                ]);
            });
        }

        $query = $query->contain([
            'Areas',
            'Connections.Services',
            'ServicePending.Presales.Users',
            'Labels',
            'Users',
            'CustomersAccounts'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case '':
                        break;

                    case 'Customers.created':
                    case 'Customers.last_emails':
                    case 'Customers.last_sms':
                    case 'Customers.payment_commitment':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Customers.etiquetas':

                        $labels_ids_search = []; 
                        $customer_codes_labels = [];
                        $customer_labels = [];
                        
                        $labels_ids_search = explode(',', $column['search']['value']);

                        if (in_array(0, $labels_ids_search)) { //buscada de sin etiquetas
                        	
                        	$tableCustomersLabels = TableRegistry::get('CustomersLabels');
                        	$customer_codes_labels = $tableCustomersLabels->find('list')->toArray();

                        	if (count($customer_codes_labels) > 0) {
                        		$where += ['code NOT IN' => $customer_codes_labels];
                        	}
                        } else { //con etiquetas
                        
                        	$tableCustomersLabels = TableRegistry::get('CustomersLabels');
                        	$tableCustomersLabels = $tableCustomersLabels->find()
                        		->where(['label_id IN' => $labels_ids_search]); 
                        		
                        	foreach ($tableCustomersLabels as $tcl) {
                        		if (!array_key_exists($tcl->customer_code,$customer_labels)) {
                        			$customer_labels[$tcl->customer_code] = [];
                        		}
                        		$customer_labels[$tcl->customer_code][] =  $tcl->label_id;
                        	}

                        	foreach($customer_labels as $code => $labels){
                        		
                        		if (!array_diff($labels_ids_search, $labels)) {
                        			$customer_codes_labels[] = $code;
                        		}
                        	}

                        	if (count($customer_codes_labels) > 0) {
                        		$where += ['code IN' => $customer_codes_labels];
                        	} else {
                                $where += ['code IN' => -1];
                            }
                        }
                        break;

                    case 'Customers.responsible':
                    case 'Customers.code':
                    case 'Customers.daydue':
                    case 'Customers.business_billing':
                    case 'Customers.doc_type':
                    case 'Customers.area_id':

                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Customers.debt_month':
                    case 'Customers.invoices_no_paid':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Customers.is_presupuesto':
                    case 'Customers.denomination':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? TRUE : FALSE];
                        break;

                    default: 
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {
            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Customers.created':
                    case '':
                    case 'Customers.invoices_no_paid':
                    case 'Customers.debt_month':
                    case 'Customers.daydue':
                    case 'Customers.clave_portal':
                    case 'Customers.responsible':
                    case 'Customers.is_presupuesto':
                    case 'Customers.denomination':
                    case 'Customers.business_billing':
                    case 'Customers.payment_commitment':
                        break;

                    case 'Customers.code':
                    case 'Customers.account_code':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Customers.name':
                    case 'Customers.ident':
                    case 'Customers.address':
                    case 'Areas.name':
                    case 'Customers.phone':
                    case 'Customers.seller':
                    case 'Customers.email':
                    case 'Customers.comments':
                    case 'Customers.clave_portal':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {
            foreach ($between as $b) {
                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        // Servicio habilitado y bloqueado
        if ($params['columns'][9]['search']['value'] != '') {

            $customers_ok = [];

            foreach ($query as $customer) {

                if (sizeof($customer->connections) > 0) {

                    foreach ($customer->connections as $connection) {

                        $enabled = $params['columns'][9]['search']['value'] ? TRUE : FALSE;

                        if ($connection->deleted == FALSE && $connection->enabled == $enabled) {

                            array_push($customers_ok, $customer);
                        }
                    }
                }
            }
            $query = $customers_ok;
        }

        // controlador
        if ($params['columns'][22]['search']['value'] != '') {

            $customers_ok = [];

            foreach ($query as $customer) {

                if (sizeof($customer->connections) > 0) {

                    foreach ($customer->connections as $connection) {

                        $controller_id = $params['columns'][22]['search']['value'];

                        if ($connection->deleted == FALSE && $connection->controller_id == $controller_id) {

                            array_push($customers_ok, $customer);
                        }
                    }
                }
            }
            $query = $customers_ok;
        }

        // servicio
        if ($params['columns'][23]['search']['value'] != '') {

            $customers_ok = [];

            foreach ($query as $customer) {

                if (sizeof($customer->connections) > 0) {

                    foreach ($customer->connections as $connection) {

                        $service_id = $params['columns'][23]['search']['value'];

                        if ($connection->deleted == FALSE && $connection->service_id == $service_id) {

                            array_push($customers_ok, $customer);
                        }
                    }
                }
            }
            $query = $customers_ok;
        }

        $customers_ok = [];

        if ($type != '') {
            switch ($type) {

                case 'sms_getway':
                    foreach ($query as $customer) {

                        array_push($customers_ok, $customer);
                    }
                    break;

                case 'smsmasivo':
                    foreach ($query as $customer) {

                        if (!$this->verifySMSMasivePhone($customer->phone)) {
                            array_push($customers_ok, $customer);
                        }
                    }
                    break;

                case 'mass_email':
                    foreach ($query as $customer) {

                        if (!$this->verifyMassEmails($customer->email)) {
                            array_push($customers_ok, $customer);
                        }
                    }
                    break;
            }

            $query = $customers_ok;
        }

        return count($query);
    }

    public function findServerSideDataSimple(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            'Customers.code',   //0
            'Customers.name',   //1
            'Customers.ident',  //2
            'Customers.address',//3
            'Customers.phone',  //4
        ];

        $columns_select = [
            'Customers.code',
            'Customers.name',
            'Customers.ident',
            'Customers.address',
            'Customers.phone',
            'Customers.doc_type',
            'debt_month',
            'business_billing',
            'Customers.billing_for_service',
            'Customers.comments',
        ];

        //paginacion

        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] =>  $o['dir']];
        }

        $query->contain([
            'Labels',
        ])->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Customers.code' : 
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Customers.name':
                    case 'Customers.ident':
                    case 'Customers.address':
                    case 'Customers.phone':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order($order);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        $query->toArray();

        $query_return = $query;

        return $query_return;
    }

    public function findRecordsFilteredSimple(Query $query, array $options)
    {
        $params = $options['params'];

        $where = [];
        $between = [];
        $orWhere = [];

        $columns = [
            'Customers.code',    //0
            'Customers.name',    //1
            'Customers.ident',   //2
            'Customers.address', //3
            'Customers.phone',   //4
        ];

        $columns_select = [
            'Customers.code',
            'Customers.name',
            'Customers.ident',
            'Customers.address',
            'Customers.doc_type',
            'debt_month',
            'business_billing',
            'Customers.billing_for_service',
            'Customers.comments',
            'Customers.phone',
        ];

        $query->contain([
            'Labels',
        ])->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Customers.code' :
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Customers.name':
                    case 'Customers.ident':
                    case 'Customers.address':
                    case 'Customers.phone':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }

    public function findServerSideDataCheckSelectionWithArchived(Query $query, array $options)
    {
        $params = $options['params'];
        $type = $options['type'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            '',                                     //0
            'Customers.created',                    //1
            'Customers.code',                       //2
            'Customers.account_code',               //3
            'Customers.etiquetas',                  //4
            'Customers.invoices_no_paid',           //5
            'Customers.debt_month',                 //6
            'Customers.name',                       //7
            'Customers.ident',                      //8
            '',                                     //9
            'Customers.address',                    //10
            'Customers.area_id',                    //11
            'Customers.phone',                      //12
            'Customers.seller',                     //13
            'Customers.responsible',                //14
            'Customers.is_presupuesto',             //15
            'Customers.denomination',               //16
            'Customers.email',                      //17
            'Customers.comments',                   //18
            'Customers.daydue',                     //19
            'Customers.clave_portal',               //20
            'Customers.business_billing',           //21
            '',                                     //22
            '',                                     //23
            '',                                     //24
            'Customers.deleted',                    //25
        ];

        $columns_select = [
            'Customers.created',
            'Customers.code',
            'Customers.account_code',
            'Customers.debt_month',
            'Customers.name',
            'Customers.ident',
            'Customers.address',
            'Customers.phone',
            'Customers.seller',
            'Customers.responsible',
            'Customers.cond_venta',
            'Customers.is_presupuesto',
            'Customers.denomination',
            'Customers.email',
            'Customers.comments',
            'Customers.daydue',
            'Customers.clave_portal',
            'Customers.business_billing',
            'Customers.doc_type',
            'Customers.payment_method_default_id',
            'Customers.invoices_no_paid',
            'Users.id',
            'Areas.name',
            'Customers.area_id',
            'Customers.deleted',
        ];

        //paginacion
        if ($params['length'] > 0) {
            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] =>  $o['dir']];
        }

        if ($params['columns'][24]['search']['value'] != '') {

            $payemnt_getway_id = $params['columns'][24]['search']['value'];

            $query = $query->matching('CustomersAccounts', function ($q) use ($payemnt_getway_id) {
                return $q->where([
                    'CustomersAccounts.payment_getway_id' => $payemnt_getway_id,
                    'CustomersAccounts.super_deleted'     => 0
                ]);
            });
        }

        $query = $query->contain([
            'Areas',
            'Connections',
            'ServicePending.Services',
            'ServicePending.Presales.Users',
            'Labels',
            'Users',
            'CustomersAccounts'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case '':
                        break;

                    case 'Customers.created':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2]  . '-' . $value[1][1] . '-' .  $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                            
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Customers.etiquetas':

                        $labels_ids_search = []; 
                        $customer_codes_labels = [];
                        $customer_labels = [];
                        
                        $labels_ids_search = explode(',', $column['search']['value']);

                        if (in_array(0, $labels_ids_search)) { //buscada de sin etiquetas
                        	
                        	$tableCustomersLabels = TableRegistry::get('CustomersLabels');
                        	$customer_codes_labels = $tableCustomersLabels->find('list')->toArray();

                        	if (count($customer_codes_labels) > 0) {
                        		$where += ['code NOT IN' => $customer_codes_labels];
                        	}
                        } else { //con etiquetas
                        
                        	$tableCustomersLabels = TableRegistry::get('CustomersLabels');
                        	$tableCustomersLabels = $tableCustomersLabels->find()
                        		->where(['label_id IN' => $labels_ids_search]); 
                        		
                        	foreach ($tableCustomersLabels as $tcl) {
                        		if (!array_key_exists($tcl->customer_code,$customer_labels)) {
                        			$customer_labels[$tcl->customer_code] = [];
                        		}
                        		$customer_labels[$tcl->customer_code][] =  $tcl->label_id;
                        	}

                        	foreach ($customer_labels as $code => $labels) {
                        		
                        		if (!array_diff($labels_ids_search, $labels)) {
                        			$customer_codes_labels[] = $code;
                        		}
                        	}

                        	if (count($customer_codes_labels) > 0) {
                        		$where += ['code IN' => $customer_codes_labels];
                        	} else {
                                $where += ['code IN' => -1];
                            }
                        }
                        break;

                    case 'Customers.responsible':
                    case 'Customers.code':
                    case 'Customers.daydue':
                    case 'Customers.business_billing':
                    case 'Customers.area_id':

                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Customers.debt_month':
                    case 'Customers.invoices_no_paid':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }

                        break;

                    case 'Customers.is_presupuesto':
                    case 'Customers.denomination':
                    case 'Customers.deleted':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? TRUE : FALSE];
                        break;

                    default: 
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Customers.created':
                    case '':
                    case 'Customers.invoices_no_paid':
                    case 'Customers.debt_month':
                    case 'Customers.daydue':
                    case 'Customers.clave_portal':
                    case 'Customers.responsible':
                    case 'Customers.is_presupuesto':
                    case 'Customers.denomination':
                    case 'Customers.business_billing':
                    case 'Customers.deleted':
                        break;

                    case 'Customers.code':
                    case 'Customers.account_code':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Customers.name':
                    case 'Customers.ident':
                    case 'Customers.address':
                    case 'Areas.name':
                    case 'Customers.phone':
                    case 'Customers.seller':
                    case 'Customers.email':
                    case 'Customers.comments':
                    case 'Customers.clave_portal':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order($order);

        $query->map(function ($row) { // map() is a collection method, it executes the query

            $tableControllers = TableRegistry::get('Controllers');
            $controllers_ids = $tableControllers->find('list')->toArray();

            $tableServices = TableRegistry::get('Services');
            $services_ids = $tableServices->find('list')->toArray();

            $row->services_active = 0;
            $row->services_total = 0;

            foreach ($row->connections as $conn) {

                $row->controller = $controllers_ids[$conn->controller_id] . ' - ';
                $row->service = $services_ids[$conn->service_id] . ' - ';

                if (!$conn->deleted) {
                    $row->services_total++;
                    if ($conn->enabled) {
                        $row->services_active++;
                    }
                }
            }

            $service_pending_no_used = []; 
            foreach ($row->service_pending as $sp) {

                if (!$sp->used) {
                    $service_pending_no_used[] = $sp;
                }
            }

            $row->service_pending = $service_pending_no_used;
            return $row;
        })
        ->toArray();

        // Servicio bloqueado y habilitado
        if ($params['columns'][9]['search']['value'] != '') {

            $customers_ok = [];

            foreach ($query as $customer) {

                if (sizeof($customer->connections) > 0) {

                    foreach ($customer->connections as $connection) {

                        $enabled = $params['columns'][9]['search']['value'] ? TRUE : FALSE;

                        if ($connection->deleted == FALSE && $connection->enabled == $enabled) {

                            array_push($customers_ok, $customer);
                        }
                    }
                }
            }
            $query = $customers_ok;
        }

        // controlador
        if ($params['columns'][22]['search']['value'] != '') {

            $customers_ok = [];

            foreach ($query as $customer) {

                if (sizeof($customer->connections) > 0) {

                    foreach ($customer->connections as $connection) {

                        $controller_id = $params['columns'][22]['search']['value'];

                        if ($connection->deleted == FALSE && $connection->controller_id == $controller_id) {

                            array_push($customers_ok, $customer);
                        }
                    }
                }
            }
            $query = $customers_ok;
        }

        // servicio
        if ($params['columns'][23]['search']['value'] != '') {

            $customers_ok = [];

            foreach ($query as $customer) {

                if (sizeof($customer->connections) > 0) {

                    foreach ($customer->connections as $connection) {

                        $service_id = $params['columns'][23]['search']['value'];

                        if ($connection->deleted == FALSE && $connection->service_id == $service_id) {

                            array_push($customers_ok, $customer);
                        }
                    }
                }
            }
            $query = $customers_ok;
        }

        if ($type != '') {

            $customers_ok = [];

            switch ($type) {

                case 'sms_getway':
                    foreach ($query as $customer) {

                        array_push($customers_ok, $customer);
                    }
                    break;

                case 'smsmasivo':
                    foreach ($query as $customer) {

                        if (!$this->verifySMSMasivePhone($customer->phone)) {
                            array_push($customers_ok, $customer);
                        }
                    }
                    break;

                case 'mass_email':
                    foreach ($query as $customer) {

                        if (!$this->verifyMassEmails($customer->email)) {
                            array_push($customers_ok, $customer);
                        }
                    }
                    break;
            }

            $query = $customers_ok;
        }

        $query_return = $query;

        if (sizeof($query_return) > 0 && $limit != -1) {
            $query_return = array_chunk($query_return, $limit);
            $query_return = $query_return[--$page];
        }

        return $query_return;
    }

    public function findRecordsFilteredCheckSelectionWithArchived(Query $query, array $options)
    {
        $params = $options['params'];
        $type = $options['type'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length'];
        $page = 1;

        $columns = [
            '',                                     //0
            'Customers.created',                    //1
            'Customers.code',                       //2
            'Customers.account_code',               //3
            'Customers.etiquetas',                  //4
            'Customers.invoices_no_paid',           //5
            'Customers.debt_month',                 //6
            'Customers.name',                       //7
            'Customers.ident',                      //8
            '',                                     //9
            'Customers.address',                    //10
            'Customers.area_id',                    //11
            'Customers.phone',                      //12
            'Customers.seller',                     //13
            'Customers.responsible',                //14
            'Customers.is_presupuesto',             //15
            'Customers.denomination',               //16
            'Customers.email',                      //17
            'Customers.comments',                   //18
            'Customers.daydue',                     //19
            'Customers.clave_portal',               //20
            'Customers.business_billing',           //21
            '',                                     //22
            '',                                     //23
            '',                                     //24
            'Customers.deleted',                    //25
        ];

        $columns_select = [
            'Customers.created',
            'Customers.code',
            'Customers.account_code',
            'Customers.debt_month',
            'Customers.name',
            'Customers.ident',
            'Customers.address',
            'Customers.phone',
            'Customers.seller',
            'Customers.responsible',
            'Customers.cond_venta',
            'Customers.is_presupuesto',
            'Customers.denomination',
            'Customers.email',
            'Customers.comments',
            'Customers.daydue',
            'Customers.clave_portal',
            'Customers.business_billing',
            'Customers.doc_type',
            'Customers.payment_method_default_id',
            'Customers.invoices_no_paid',
            'Users.id',
            'Areas.name',
            'Customers.area_id',
            'Customers.deleted',
        ];

        //paginacion
        if ($params['length'] > 0) {
            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length'];
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] =>  $o['dir']];
        }

        if ($params['columns'][24]['search']['value'] != '') {

            $payemnt_getway_id = $params['columns'][24]['search']['value'];

            $query = $query->matching('CustomersAccounts', function ($q) use ($payemnt_getway_id) {
                return $q->where([
                    'CustomersAccounts.payment_getway_id' => $payemnt_getway_id,
                    'CustomersAccounts.super_deleted'     => 0
                ]);
            });
        }

        $query = $query->contain([
            'Areas',
            'Connections.Services',
            'ServicePending.Presales.Users',
            'Labels',
            'Users',
            'CustomersAccounts'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case '':
                        break;

                    case 'Customers.created':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Customers.etiquetas':

                        $labels_ids_search = []; 
                        $customer_codes_labels = [];
                        $customer_labels = [];
                        
                        $labels_ids_search = explode(',', $column['search']['value']);

                        if (in_array(0, $labels_ids_search)) { //buscada de sin etiquetas
                        	
                        	$tableCustomersLabels = TableRegistry::get('CustomersLabels');
                        	$customer_codes_labels = $tableCustomersLabels->find('list')->toArray();

                        	if (count($customer_codes_labels) > 0) {
                        		$where += ['code NOT IN' => $customer_codes_labels];
                        	}
                        } else { //con etiquetas
                        
                        	$tableCustomersLabels = TableRegistry::get('CustomersLabels');
                        	$tableCustomersLabels = $tableCustomersLabels->find()
                        		->where(['label_id IN' => $labels_ids_search]); 
                        		
                        	foreach ($tableCustomersLabels as $tcl) {
                        		if (!array_key_exists($tcl->customer_code,$customer_labels)) {
                        			$customer_labels[$tcl->customer_code] = [];
                        		}
                        		$customer_labels[$tcl->customer_code][] =  $tcl->label_id;
                        	}

                        	foreach($customer_labels as $code => $labels){
                        		
                        		if (!array_diff($labels_ids_search, $labels)) {
                        			$customer_codes_labels[] = $code;
                        		}
                        	}

                        	if (count($customer_codes_labels) > 0) {
                        		$where += ['code IN' => $customer_codes_labels];
                        	} else {
                                $where += ['code IN' => -1];
                            }
                        }
                        break;

                    case 'Customers.responsible':
                    case 'Customers.code': 
                    case 'Customers.daydue':
                    case 'Customers.business_billing':
                    case 'Customers.doc_type':
                    case 'Customers.area_id':

                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Customers.debt_month': 
                    case 'Customers.invoices_no_paid':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Customers.is_presupuesto':
                    case 'Customers.denomination':
                    case 'Customers.deleted':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? TRUE : FALSE];
                        break;

                    default: 
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {
            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Customers.created':
                    case '':
                    case 'Customers.invoices_no_paid':
                    case 'Customers.debt_month':
                    case 'Customers.daydue':
                    case 'Customers.clave_portal':
                    case 'Customers.responsible':
                    case 'Customers.is_presupuesto':
                    case 'Customers.denomination':
                    case 'Customers.business_billing':
                    case 'Customers.deleted':
                        break;

                    case 'Customers.code':
                    case 'Customers.account_code':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Customers.name':
                    case 'Customers.ident':
                    case 'Customers.address':
                    case 'Areas.name':
                    case 'Customers.phone':
                    case 'Customers.seller':
                    case 'Customers.email':
                    case 'Customers.comments':
                    case 'Customers.clave_portal':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {
            foreach ($between as $b) {
                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        // Servicio habilitado y bloqueado
        if ($params['columns'][9]['search']['value'] != '') {

            $customers_ok = [];

            foreach ($query as $customer) {

                if (sizeof($customer->connections) > 0) {

                    foreach ($customer->connections as $connection) {

                        $enabled = $params['columns'][9]['search']['value'] ? TRUE : FALSE;

                        if ($connection->deleted == FALSE && $connection->enabled == $enabled) {

                            array_push($customers_ok, $customer);
                        }
                    }
                }
            }
            $query = $customers_ok;
        }

        // controlador
        if ($params['columns'][22]['search']['value'] != '') {

            $customers_ok = [];

            foreach ($query as $customer) {

                if (sizeof($customer->connections) > 0) {

                    foreach ($customer->connections as $connection) {

                        $controller_id = $params['columns'][22]['search']['value'];

                        if ($connection->deleted == FALSE && $connection->controller_id == $controller_id) {

                            array_push($customers_ok, $customer);
                        }
                    }
                }
            }
            $query = $customers_ok;
        }

        // servicio
        if ($params['columns'][23]['search']['value'] != '') {

            $customers_ok = [];

            foreach ($query as $customer) {

                if (sizeof($customer->connections) > 0) {

                    foreach ($customer->connections as $connection) {

                        $service_id = $params['columns'][23]['search']['value'];

                        if ($connection->deleted == FALSE && $connection->service_id == $service_id) {

                            array_push($customers_ok, $customer);
                        }
                    }
                }
            }
            $query = $customers_ok;
        }

        $customers_ok = [];

        if ($type != '') {
            switch ($type) {

                case 'sms_getway':
                    foreach ($query as $customer) {

                        array_push($customers_ok, $customer);
                    }
                    break;

                case 'smsmasivo':
                    foreach ($query as $customer) {

                        if (!$this->verifySMSMasivePhone($customer->phone)) {
                            array_push($customers_ok, $customer);
                        }
                    }
                    break;

                case 'mass_email':
                    foreach ($query as $customer) {

                        if (!$this->verifyMassEmails($customer->email)) {
                            array_push($customers_ok, $customer);
                        }
                    }
                    break;
            }

            $query = $customers_ok;
        }

        return count($query);
    }

    public function beforeSave($event, $entity, $options)
    {
        if (!$entity->isDirty('portal_sync')) {
            $entity->portal_sync = false;
        }
    }

    public function afterSave($event, $entity, $options)
    {
    	$detail = '';

    	$ok = false;

    	//if (!$entity->isNew() && !$entity->isDirty('account_code') && !$entity->isDirty('status')) {
	    if (!$entity->isNew()) {

    		$action = 'Edición de Cliente';

    		$dirtyFields = $entity->getDirty();

    		$dirtyFields = array_diff($dirtyFields, ["modified"]);

    		//cuando la edicion es solo de debt_month y se edita con el mismo valor
            // 		if (count($dirtyFields) == 1 && $entity->getOriginal('debt_month') == $entity->debt_month) {
            // 		    return true;
            // 		}

    		foreach ($dirtyFields as $dirtyField) {

    			switch ($dirtyField) {

    				case 'name': 
    					$detail .= 'Nombre: ' . $entity->getOriginal('name') . ' => ' . $entity->name . PHP_EOL;
    					$ok = true;
    					break;
    				case 'debt_month':
    					$detail .= 'Deuda mensual: ' .  number_format($entity->getOriginal('debt_month'), 2, ',', '.') . ' => ' . number_format($entity->debt_month, 2, ',', '.') . PHP_EOL; 
    					$ok = true;
    					break;
					case 'debt_total':
    					$detail .= 'Deuda Total: ' .  number_format($entity->getOriginal('debt_total'), 2, ',', '.') . ' => ' . number_format($entity->debt_total, 2, ',', '.') . PHP_EOL; 
    					$ok = true;
    					break;
    				case 'address': 
    					$detail .= 'Domicilio: ' . $entity->getOriginal('address') . ' => ' . $entity->address . PHP_EOL; 
    					$ok = true;
    					break;
    				case 'responsible': 
    					$detail .= 'Responsabilidad: ' . $_SESSION['afip_codes']['responsibles'][$entity->getOriginal('responsible')] . ' => ' . $_SESSION['afip_codes']['responsibles'][$entity->responsible] . PHP_EOL; 
    					$ok = true;
    					break;
    				case 'doc_type': 
    					$detail .= 'Tipo de Doc.: ' . $_SESSION['afip_codes']['doc_types'][$entity->getOriginal('doc_type')] . ' => ' . $_SESSION['afip_codes']['doc_types'][$entity->doc_type] . PHP_EOL; 
    					$ok = true;
    					break;
    				case 'ident': 
    					$detail .= 'Nro. de Doc: ' . $entity->getOriginal('ident') . ' => ' . $entity->ident . PHP_EOL; 
    					$ok = true;
    					break;
    				case 'phone': 
    					$ok = true;
    					$detail .= 'Teléfono: ' . $entity->getOriginal('phone') . ' => ' . $entity->phone . PHP_EOL; 
    					break;
    				case 'phone_alt': 
    					$detail .= 'Teléfono Alt.: ' . $entity->getOriginal('phone_alt') . ' => ' . $entity->phone_alt . PHP_EOL; 
    					$ok = true;
    					break;
    				case 'asked_router':
    					$detail .= 'Pidió Router: ' . ($entity->getOriginal('asked_router') ? 'Si' : 'No') . ' => ' . ($entity->asked_router ? 'Si' : 'No') . PHP_EOL; 
    					$ok = true;
    					break;
    				case 'availability': 
    					$detail .= 'Disponibilidad: ' . $entity->getOriginal('availability') . ' => ' . $entity->availability . PHP_EOL; 
    					$ok = true;
    					break;
    				case 'email': 
    					$detail .= 'Correo Electrónico: ' . $entity->getOriginal('email') . ' => ' . $entity->email . PHP_EOL; 
    					$ok = true;
    					break;
    				case 'comments': 
    					$detail .= 'Comentario: ' . $entity->getOriginal('comments') . ' => ' . $entity->comments . PHP_EOL; 
    					$ok = true;
    					break;
    				case 'daydue':
    					$detail .= 'Día de Venc.: ' . $entity->getOriginal('daydue') . ' => ' . $entity->daydue . PHP_EOL; 
    					$ok = true;
    					break;
    				case 'deleted':
    					if ($entity->deleted) $action = 'Archivado de Cliente'; else $action = 'Restauración de Cliente';
    					$ok = true;
    					break;
					case 'super_deleted':
    					if ($entity->deleted) $action = 'Eliminación de Cliente'; else $action = 'Restauración de Cliente';
    					$ok = true;
    					break;
    				case 'clave_portal': 
    					$detail .= 'Clave Portal: ' . $entity->getOriginal('clave_portal') . ' => ' . $entity->clave_portal . PHP_EOL; 
    					$ok = true;
    					break;
    				case 'seller':
    					$detail .= 'Vendedor: ' . $entity->getOriginal('seller') . ' => ' . $entity->seller . PHP_EOL; 
    					$ok = true;
    					break;
    				case 'area_id': 
    				    $table = TableRegistry::get('Areas');
    				    $area_original = $table->find()
                            ->where(['id' => $entity->getOriginal('area_id')])
                            ->first();
                        $area_new = $table->find()
                            ->where(['id' => $entity->area_id])
                            ->first();
    					$detail .= 'Área: ' . $area_original->name . ' => ' . $area_new->name . PHP_EOL; 
    					$ok = true;
    					break;
					case 'city_id': 
    				    $table = TableRegistry::get('Cities');
    				    $city_original = $table->find()
                            ->where(['id' => $entity->getOriginal('city_id')])
                            ->first();
                        $city_new = $table->find()
                            ->where(['id' => $entity->city_id])
                            ->first();
    					$detail .= 'Ciudad: ' . $city_original->name . ' => ' . $city_new->name . PHP_EOL; 
    					$ok = true;
    					break;
					case 'province_id':
    				    $table = TableRegistry::get('Provinces');
    				    $province_original = $table->find()
                            ->where(['id' => $entity->getOriginal('province_id')])
                            ->first();
                        $province_new = $table->find()
                            ->where(['id' => $entity->province_id])
                            ->first();
    					$detail .= 'Pronvincia: ' . $province_original->name . ' => ' . $province_new->name . PHP_EOL; 
    					$ok = true;
    					break;
					case 'country_id':
    				    $table = TableRegistry::get('Countries');
    				    $country_original = $table->find()
                            ->where(['id' => $entity->getOriginal('country_id')])
                            ->first();
                        $country_new = $table->find()
                            ->where(['id' => $entity->country_id])
                            ->first();
    					$detail .= 'País: ' . $country_original->name . ' => ' . $country_new->name . PHP_EOL; 
    					$ok = true;
    					break;
    			    case 'cond_venta': 
    					$detail .= 'Cond. de Venta: ' . $_SESSION['afip_codes']['cond_venta'][$entity->getOriginal('cond_venta')] . ' => ' . $_SESSION['afip_codes']['cond_venta'][$entity->cond_venta] . PHP_EOL; 
    					$ok = true;
    					break;
    				case 'is_presupuesto':
    					$detail .= 'Presupuesto: ' . ($entity->getOriginal('is_presupuesto') ? 'Si' : 'No' ) . ' => ' . ($entity->is_presupuesto ? 'Si' : 'No' ) . PHP_EOL; 
    					$ok = true;
    					break;
    				case 'denomination':
    					$detail .= 'Denominación: ' . ($entity->getOriginal('denomination') ? 'Si' : 'No' ) . ' => ' . ($entity->denomination ? 'Si' : 'No' ) . PHP_EOL;
    					$ok = true;
    					break;
    		   }
    		}

    	} else if ($entity->isNew()) {
    		$action = 'Creación de Cliente';
    		$detail = 'Código: ' . str_pad( $entity->code, 5, "0", STR_PAD_LEFT);  

    		$ok = true;
    	}

    	if ($ok) {

    	    $user_id = null;

    	    if (!isset($_SESSION['Auth']['User']['id'])) {
    	        $user_id = 100;
    	    } else {
    	        $user_id = $_SESSION['Auth']['User']['id'];
    	    }

    	    $actionLog = TableRegistry::get('ActionLog');
        	$query = $actionLog->query();
        	$query->insert([
        		'created', 
        		'detail',
        		'user_id',
        		'action',
        		'customer_code'
        	])
        	->values([
        		'created' => Time::now(), 
        		'detail' => $detail,
        		'user_id' => $user_id,
        		'action' => $action,
        		'customer_code' => $entity->code
        	])
        	->execute();
    	}
    }

    private function verifySMSMasivePhone($phone)
    {
        if ($phone == NULL || strlen($phone) < 1) {
            return true;
        }

        $white_space = strpos($phone, ' ');
        $white_space = is_int($white_space);

        $plus = strpos($phone, '+');
        $plus = is_int($plus);

        $dash = strpos($phone, '-');
        $dash = is_int($plus);

        $size = strlen($phone) != 10;

        $zero_initial = $phone[0] == 0;

        return ($white_space || $plus || $dash || !is_numeric($phone) || $size || $zero_initial);
    }

    private function verifyMassEmails($email)
    {
        $email_part = explode(",", $email);
        $validate = false;
        foreach ($email_part as $e) {
            $validate = !filter_var(trim($e), FILTER_VALIDATE_EMAIL);
        }
        return $validate;
    }
}
