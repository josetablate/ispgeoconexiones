<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

/**
 * Services Model
 *
 * @property \Cake\ORM\Association\HasMany $Connections
 *
 * @method \App\Model\Entity\Service get($primaryKey, $options = [])
 * @method \App\Model\Entity\Service newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Service[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Service|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Service patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Service[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Service findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ServicesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('services');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Connections', [
            'foreignKey' => 'service_id'
        ]);
        
        $this->belongsTo('Accounts', [
            'foreignKey' => 'account_code',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->allowEmpty('description');

        $validator
            ->decimal('price')
            ->requirePresence('price', 'create')
            ->notEmpty('price');

        $validator
            ->boolean('enabled')
            ->requirePresence('enabled', 'create')
            ->notEmpty('enabled');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['account_code']));

        return $rules;
    }

    public function afterSave($event, $entity, $options)
    {
        $ok = false;

        $detail = '';

        if (!$entity->isNew()) {

            $action = 'Edición de Servicio';

            $dirtyFields = $entity->getDirty();

            $dirtyFields = array_diff($dirtyFields, ["modified"]);

            foreach ($dirtyFields as $dirtyField) {

                switch ($dirtyField) {

                    case 'name': 
                        $detail .= 'Nombre: ' . $entity->getOriginal('name') . ' => ' . $entity->name . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'description': 
                        $detail .= 'Descripción: ' . $entity->getOriginal('description') . ' => ' . $entity->description . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'price':
                        $detail .= 'Precio: ' .  number_format($entity->getOriginal('price'), 2, ',', '.') . ' => ' . number_format($entity->price, 2, ',', '.') . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'enabled':
                        if ($entity->enabled) $action = 'Habilitación de Servicio'; else $action = 'Deshabilitación de Servicio';
                        $ok = true;
                        break;
                    case 'deleted':
                         if ($entity->deleted) $action = 'Eliminación de Servicio'; else $action = 'Restauración de Servicio';
                        break;
                    case 'account_code':
                         $detail .= 'Código de cuenta: ' . $entity->getOriginal('account_code') . ' => ' . $entity->account_code . PHP_EOL; 
                         $ok = true;
                         break;
                    case 'aliquot':
                        $detail .= 'Alícuota: ' . $_SESSION['afip_codes']['alicuotas_types'][$entity->getOriginal('aliquot')] . ' => ' . $_SESSION['afip_codes']['alicuotas_types'][$entity->aliquot] . PHP_EOL;
                        $ok = true;
                        break;
               }
            }
        } else {
            $ok = true;
            $action = 'Creación de Servicio';
            $detail .= 'Nombre: ' . $entity->name . PHP_EOL;
            $detail .= 'Descripción: ' . $entity->description . PHP_EOL;
            $detail .= 'Precio: ' . number_format($entity->price, 2, ',', '.') . PHP_EOL;
            $detail .= 'Habilitado: ' . ($entity->enabled ? 'Si' : 'No') . PHP_EOL; 
            $detail .= 'Alícuota: ' . $entity->aliquot . PHP_EOL;
        }

        if ($ok) {


            if (!isset($_SESSION['Auth']['User']['id'])) {
    	        $user_id = 100;
    	    } else {
    	        $user_id = $_SESSION['Auth']['User']['id'];
    	    }

            $actionLog = TableRegistry::get('ActionLog');
            $query = $actionLog->query();
            $query->insert([
                'created', 
                'detail',
                'user_id',
                'action',
                'customer_code'
            ])
            ->values([
                'created' => Time::now(), 
                'detail' => $detail,
                'user_id' => $user_id,
                'action' => $action,
                'customer_code' => isset($entity->customer_code) ? $entity->customer_code : null,
            ])
            ->execute();
        }
    }
}
