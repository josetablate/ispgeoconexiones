<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FirewallAddressLists Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Controllers
 * @property \Cake\ORM\Association\BelongsTo $Apis
 *
 * @method \App\Model\Entity\FirewallAddressList get($primaryKey, $options = [])
 * @method \App\Model\Entity\FirewallAddressList newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\FirewallAddressList[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\FirewallAddressList|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FirewallAddressList patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\FirewallAddressList[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\FirewallAddressList findOrCreate($search, callable $callback = null)
 */
class FirewallAddressListsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('firewall_address_lists');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Controllers', [
            'foreignKey' => 'controllers_id',
            'joinType' => 'INNER'
        ]);
        
        $this->belongsTo('Connections', [
            'foreignKey' => 'connection_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('list', 'create')
            ->notEmpty('list');

        $validator
            ->requirePresence('address', 'create')
            ->notEmpty('address');

        $validator
            ->requirePresence('comment', 'create')
            ->notEmpty('comment');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['controllers_id'], 'Controllers'));
        return $rules;
    }
}
