<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

/**
 * Products Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Articles
 * @property \Cake\ORM\Association\HasMany $Debts
 * @property \Cake\ORM\Association\HasMany $Logs
 *
 * @method \App\Model\Entity\Product get($primaryKey, $options = [])
 * @method \App\Model\Entity\Product newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Product[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Product|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Product patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Product[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Product findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProductsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('products');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Articles', [
            'foreignKey' => 'article_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Debts', [
            'foreignKey' => 'product_id'
        ]);
        $this->hasMany('Logs', [
            'foreignKey' => 'product_id'
        ]);
         $this->belongsTo('Accounts', [
            'foreignKey' => 'account_code',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->decimal('unit_price')
            ->requirePresence('unit_price', 'create')
            ->notEmpty('unit_price');

        $validator
            ->boolean('enabled')
            // ->requirePresence('enabled', 'create')
            ->allowEmpty('enabled');

        $validator
            ->boolean('deleted')
            // ->requirePresence('deleted', 'create')
            ->allowEmpty('deleted');

        

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['article_id'], 'Articles'));

        return $rules;
    }

    public function afterSave($event, $entity, $options)
    {
        $ok = false;

        $detail = '';

        if (!$entity->isNew()) {

            $action = 'Edición de Producto';

            $dirtyFields = $entity->getDirty();

            $dirtyFields = array_diff($dirtyFields, ["modified"]);

            foreach ($dirtyFields as $dirtyField) {

                switch ($dirtyField) {

                    case 'name': 
                        $detail .= 'Nombre: ' . $entity->getOriginal('name') . ' => ' . $entity->name . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'unit_price':
                        $detail .= 'Precio: ' .  number_format($entity->getOriginal('unit_price'), 2, ',', '.') . ' => ' . number_format($entity->unit_price, 2, ',', '.') . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'enabled':
                        if ($entity->enabled) $action = 'Habilitación de Producto'; else $action = 'Deshabilitación de Producto';
                        $ok = true;
                        break;
                    case 'article_id':
                        $table = TableRegistry::get('Articles');
    				    $original = $table->find()
                            ->where(['id' => $entity->getOriginal('article_id')])
                            ->first();
                        $new = $table->find()
                            ->where(['id' => $entity->article_id])
                            ->first();
                        $detail .= 'Código de Artículo: ' . $original->code . ' => ' . $new->code  . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'deleted':
                        if ($entity->deleted) $action = 'Eliminación de Producto'; else $action = 'Restauración de Producto';
                        $ok = true;
                        break;
                    case 'account_code':
                         $detail .= 'Código de cuenta: ' . $entity->getOriginal('account_code') . ' => ' . $entity->account_code . PHP_EOL; 
                         $ok = true;
                         break;
                    case 'aliquot':
                        $detail .= 'Alícuota: ' . $_SESSION['afip_codes']['alicuotas_types'][$entity->getOriginal('aliquot')] . ' => ' . $_SESSION['afip_codes']['alicuotas_types'][$entity->aliquot] . PHP_EOL;
                        $ok = true;
                        break;
               }
            }
        } else {
            $table = TableRegistry::get('Articles');
		    $article = $table->find()
                ->where(['id' => $entity->article_id])
                ->first();
            $action = 'Creación de Producto';
            $detail = 'Nombre: ' . $entity->name . PHP_EOL;
            $detail .= 'Precio: ' . number_format($entity->unit_price, 2, ',', '.') . PHP_EOL;
            $detail .= 'Habilitado: ' . ($entity->enabled ? 'Si' : 'No') . PHP_EOL;
            $detail .= 'Cód. artículo: ' . $article->code . PHP_EOL;
            $detail .= 'Alícuota: ' . $entity->aliquot . PHP_EOL;
            $ok = true;
        }

        if ($ok) {

            if (!isset($_SESSION['Auth']['User']['id'])) {
    	        $user_id = 100;
    	    } else {
    	        $user_id = $_SESSION['Auth']['User']['id'];
    	    }

            $actionLog = TableRegistry::get('ActionLog');
            $query = $actionLog->query();
            $query->insert([
                'created', 
                'detail',
                'user_id',
                'action',
                'customer_code'
            ])
            ->values([
                'created' => Time::now(), 
                'detail' => $detail,
                'user_id' => $user_id,
                'action' => $action,
                'customer_code' => isset($entity->customer_code) ? $entity->customer_code : null,
            ])
            ->execute();
        }
    }
}
