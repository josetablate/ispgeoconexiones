<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

/**
 * CustomersHasDiscounts Model
 *
 * @property \App\Model\Table\DebtsTable|\Cake\ORM\Association\BelongsTo $Debts
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\InvoicesTable|\Cake\ORM\Association\BelongsTo $Invoices
 * @property \App\Model\Table\ConnectionsTable|\Cake\ORM\Association\BelongsTo $Connections
 *
 * @method \App\Model\Entity\CustomersHasDiscount get($primaryKey, $options = [])
 * @method \App\Model\Entity\CustomersHasDiscount newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CustomersHasDiscount[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CustomersHasDiscount|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CustomersHasDiscount patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CustomersHasDiscount[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CustomersHasDiscount findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CustomersHasDiscountsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('customers_has_discounts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Debts', [
            'foreignKey' => 'debt_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Users', [ 
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Invoices', [
            'foreignKey' => 'invoice_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Connections', [
            'foreignKey' => 'connection_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_code'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('code')
            ->requirePresence('code', 'create')
            ->notEmpty('code');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        $validator
            ->decimal('price')
            ->requirePresence('price', 'create')
            ->notEmpty('price');

        $validator
            ->decimal('sum_price')
            ->requirePresence('sum_price', 'create')
            ->notEmpty('sum_price');

        $validator
            ->decimal('sum_tax')
            ->requirePresence('sum_tax', 'create')
            ->notEmpty('sum_tax');

        $validator
            ->decimal('total')
            ->requirePresence('total', 'create')
            ->notEmpty('total');

        $validator
            ->integer('customer_code')
            ->requirePresence('customer_code', 'create')
            ->notEmpty('customer_code');

        $validator
            ->integer('seating_number')
            ->allowEmpty('seating_number');

        $validator
            ->scalar('period')
            ->allowEmpty('period');

        $validator
            ->integer('tax')
            ->requirePresence('tax', 'create')
            ->notEmpty('tax');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['debt_id'], 'Debts'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function findServerSideData(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;
        $columns = [
            '',
            'CustomersHasDiscounts.created',
            '',
            'CustomersHasDiscounts.code',
            'CustomersHasDiscounts.description',
            '',
            '',
            'CustomersHasDiscounts.price',
            'CustomersHasDiscounts.sum_price',
            'CustomersHasDiscounts.tax',
            'CustomersHasDiscounts.sum_tax',
            '',
            'CustomersHasDiscounts.total',
            'CustomersHasDiscounts.tipo_comp',
            'Customers.code',
            'Customers.account_code',
            'Customers.ident',
            'Customers.name',
            'Customers.address',
            'Users.name',
            'CustomersHasDiscounts.seating_number',
            'Customers.business_billing',
        ];

        $columns_select = [
            'CustomersHasDiscounts.created',
            'CustomersHasDiscounts.code',
            'CustomersHasDiscounts.description',
            'CustomersHasDiscounts.price',
            'CustomersHasDiscounts.sum_price',
            'CustomersHasDiscounts.tax',
            'CustomersHasDiscounts.sum_tax',
            'CustomersHasDiscounts.total',
            'CustomersHasDiscounts.tipo_comp',
            'Customers.code',
            'Customers.account_code',
            'Customers.ident',
            'Customers.name',
            'Customers.address',
            'Users.name',
            'CustomersHasDiscounts.seating_number',
            'Customers.business_billing',
            'Connections.address',
            'Customers.doc_type',
            'CustomersHasDiscounts.id',
            'Customers.deleted',
            'CustomersHasDiscounts.period',
        ];

        //paginacion

        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        $query = $query->contain([
             'Customers.Areas', 
             'Connections',             
             'Users',
        ])
        ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        if ($params['period'] != '') {
            $where += ['CustomersHasDiscounts.period' => $params['period']];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'CustomersHasDiscounts.created': 

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'CustomersHasDiscounts.tipo_comp': 
                    case 'Customers.code':
                    case 'Customers.account_code':
                    case 'Customers.ident':
                    case 'CustomersHasDiscounts.seating_number':
                    case 'Customers.business_billing':

                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'CustomersHasDiscounts.total':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Users.name':
                    case 'Customers.name':
                    case 'CustomersHasDiscounts.description':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {
                    case 'CustomersHasDiscounts.tipo_comp':
                    case 'Customers.account_code':
                    case 'Customers.code':
                    case 'Customers.ident':
                    case 'CustomersHasDiscounts.seating_number':
                    case 'Customers.business_billing':

                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Customers.name':
                    case 'Users.name':
                    case 'CustomersHasDiscounts.description' :
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        $query->toArray();

        return $query;
    }

    public function findRecordsFiltered(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            '',
            'CustomersHasDiscounts.created',
            '',
            'CustomersHasDiscounts.code',
            'CustomersHasDiscounts.description',
            '',
            '',
            'CustomersHasDiscounts.price',
            'CustomersHasDiscounts.sum_price',
            'CustomersHasDiscounts.tax',
            'CustomersHasDiscounts.sum_tax',
            '',
            'CustomersHasDiscounts.total',
            'CustomersHasDiscounts.tipo_comp',
            'Customers.code',
            'Customers.account_code',
            'Customers.ident',
            'Customers.name',
            'Customers.address',
            'Users.name',
            'CustomersHasDiscounts.seating_number',
            'Customers.business_billing',
        ];

        $columns_select = [
            'CustomersHasDiscounts.created',
            'CustomersHasDiscounts.code',
            'CustomersHasDiscounts.description',
            'CustomersHasDiscounts.price',
            'CustomersHasDiscounts.sum_price',
            'CustomersHasDiscounts.tax',
            'CustomersHasDiscounts.sum_tax',
            'CustomersHasDiscounts.total',
            'CustomersHasDiscounts.tipo_comp',
            'Customers.code',
            'Customers.account_code',
            'Customers.ident',
            'Customers.name',
            'Customers.address',
            'Users.name',
            'CustomersHasDiscounts.seating_number',
            'Customers.business_billing',
            'Connections.address',
            'Customers.doc_type',
            'CustomersHasDiscounts.id',
            'Customers.deleted',
            'CustomersHasDiscounts.period',
        ];

        $query = $query->contain([
            'Customers.Areas',
            'Connections', 
            'Users',
        ])
        ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        if ( $params['period'] != '') {
            $where += ['CustomersHasDiscounts.period' => $params['period']];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'CustomersHasDiscounts.created': 

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'CustomersHasDiscounts.tipo_comp': 
                    case 'Customers.code':
                    case 'Customers.account_code':
                    case 'Customers.ident':
                    case 'CustomersHasDiscounts.seating_number':
                    case 'Customers.business_billing':

                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'CustomersHasDiscounts.total':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Users.name':
                    case 'Customers.name':
                    case 'CustomersHasDiscounts.description':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'CustomersHasDiscounts.tipo_comp':
                    case 'Customers.account_code':
                    case 'Customers.code':
                    case 'Customers.ident':
                    case 'CustomersHasDiscounts.seating_number':
                    case 'Customers.business_billing':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Customers.name':
                    case 'Users.name' :
                    case 'CustomersHasDiscounts.description':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }

    //MOVIMIENTOS (customer view)

    public function findServerSideDataMoves(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            'CustomersHasDiscounts.created',
            'Users.username',
            'CustomersHasDiscounts.period',
            'CustomersHasDiscounts.tipo_comp',
            '',
            'CustomersHasDiscounts.description',
            'CustomersHasDiscounts.sum_price',
            'CustomersHasDiscounts.sum_tax',
            '',
            'CustomersHasDiscounts.total',
            '',
            'CustomersHasDiscounts.seating_number',
        ];

        $Columns_select = [
            'CustomersHasDiscounts.created',
            'Users.username',
            'CustomersHasDiscounts.period',
            'CustomersHasDiscounts.tipo_comp',
            'CustomersHasDiscounts.description',
            'CustomersHasDiscounts.sum_price',
            'CustomersHasDiscounts.sum_tax',
            'CustomersHasDiscounts.total',
            'CustomersHasDiscounts.seating_number',
            'CustomersHasDiscounts.id',
            'CustomersHasDiscounts.connection_id',
            'CustomersHasDiscounts.customer_code',
            'CustomersHasDiscounts.debt_id',
            'CustomersHasDiscounts.invoice_id',
            'Customers.business_billing',
        ];

        //paginacion

        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        $query = $query->contain([
             'Debts', 
             'Users',
             'Customers'
             
        ])
        ->select($Columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'CustomersHasDiscounts.created': 

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-'.$value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0].' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Debts.tipo_comp': 
                    case 'CustomersHasDiscounts.seating_number':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'CustomersHasDiscounts.total':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Users.username':
                    case 'CustomersHasDiscounts.description':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {
                    case 'Debts.tipo_comp':
                    case 'CustomersHasDiscounts.seating_number':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Users.username':
                    case 'CustomersHasDiscounts.description' :
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach($between as $b){

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order(['CustomersHasDiscounts.created' => 'desc']);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        return $query->toArray();
    }

    public function findRecordsFilteredMoves(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            'CustomersHasDiscounts.created',
            'Users.username',
            'CustomersHasDiscounts.period',
            'CustomersHasDiscounts.tipo_comp',
            '',
            'CustomersHasDiscounts.description',
            'CustomersHasDiscounts.sum_price',
            'CustomersHasDiscounts.sum_tax',
            '',
            'CustomersHasDiscounts.total',
            '',
            'CustomersHasDiscounts.seating_number',
        ];

        $Columns_select = [
            'CustomersHasDiscounts.created',
            'Users.username',
            'CustomersHasDiscounts.period',
            'CustomersHasDiscounts.tipo_comp',
            'CustomersHasDiscounts.description',
            'CustomersHasDiscounts.sum_price',
            'CustomersHasDiscounts.sum_tax',
            'CustomersHasDiscounts.total',
            'CustomersHasDiscounts.seating_number',
            'CustomersHasDiscounts.id',
            'CustomersHasDiscounts.connection_id',
            'CustomersHasDiscounts.customer_code',
            'CustomersHasDiscounts.debt_id',
            'CustomersHasDiscounts.invoice_id',
            'Customers.business_billing',
        ];

        $query = $query->contain([
            'Debts', 
            'Users',
            'Customers'
        ])
        ->select($Columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'CustomersHasDiscounts.created': 

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Debts.tipo_comp': 
                    case 'CustomersHasDiscounts.seating_number':

                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'CustomersHasDiscounts.total':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Users.username':
                    case 'CustomersHasDiscounts.description':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Debts.tipo_comp':
                    case 'CustomersHasDiscounts.seating_number':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Users.username' :
                    case 'CustomersHasDiscounts.description' :
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }

    public function afterSave($event, $entity, $options)
    {
        $ok = false;

        $detail = '';

        if (!$entity->isNew()) {

            $action = 'Edición de Descuento';

            $dirtyFields = $entity->getDirty();

            $dirtyFields = array_diff($dirtyFields, ["modified"]);

            foreach ($dirtyFields as $dirtyField) {

                switch ($dirtyField) {
                    case 'tipo_comp': 
                        $detail .= 'Tipo de Comp.: ' . $_SESSION['afip_codes']['comprobantes'][$entity->tipo_comp] . PHP_EOL;
                        $ok = true;
                        break;
               }
            }
        } else {

            $fecha_creado = "";
            if ($entity->created) {
                $fecha_creado = $entity->created->format('d/m/Y');
            }

            $action = 'Creación de Descuento';
            $detail .= 'Fecha: ' . $fecha_creado . PHP_EOL;
            $detail .= 'Descripción: ' . $entity->description . PHP_EOL;
            $detail .= 'Destino: ' . $_SESSION['afip_codes']['comprobantes'][$entity->tipo_comp] . PHP_EOL;
            $detail .= 'Total: ' . number_format($entity->total, 2, ',', '.') . PHP_EOL;
            $detail .= 'Período: ' . $entity->period . PHP_EOL;
            $detail .= 'Tipo: ' . ($entity->debt_id ? 'Descuento relacionado a una deuda' : 'Descuento individual') . PHP_EOL;
        }

        if ($ok) {

            if (!isset($_SESSION['Auth']['User']['id'])) {
    	        $user_id = 100;
    	    } else {
    	        $user_id = $_SESSION['Auth']['User']['id'];
    	    }

            $actionLog = TableRegistry::get('ActionLog');
            $query = $actionLog->query();
            $query->insert([
                'created', 
                'detail',
                'user_id',
                'action',
                'customer_code'
            ])
            ->values([
                'created' => Time::now(), 
                'detail' => $detail,
                'user_id' => $user_id,
                'action' => $action,
                'customer_code' => isset($entity->customer_code) ? $entity->customer_code : null,
            ])
            ->execute();
        }
    }
}
