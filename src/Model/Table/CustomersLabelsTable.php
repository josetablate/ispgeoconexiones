<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CustomersLabels Model
 *
 * @property \App\Model\Table\LabelsTable|\Cake\ORM\Association\BelongsTo $Labels
 *
 * @method \App\Model\Entity\CustomersLabel get($primaryKey, $options = [])
 * @method \App\Model\Entity\CustomersLabel newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CustomersLabel[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CustomersLabel|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CustomersLabel|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CustomersLabel patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CustomersLabel[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CustomersLabel findOrCreate($search, callable $callback = null, $options = [])
 */
class CustomersLabelsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('customers_labels');
        $this->setDisplayField('customer_code');
        $this->setPrimaryKey('id');

        $this->belongsTo('Labels', [
            'foreignKey' => 'label_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('customer_code')
            ->requirePresence('customer_code', 'create')
            ->notEmpty('customer_code');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['label_id'], 'Labels'));

        return $rules;
    }
}
