<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SeatingDetail Model
 *
 * @method \App\Model\Entity\SeatingDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\SeatingDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SeatingDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SeatingDetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SeatingDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SeatingDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SeatingDetail findOrCreate($search, callable $callback = null)
 */
class SeatingDetailTable extends Table
{
    
      public $recursive = -1;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('seating_detail');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        
        $this->belongsTo('Seating', [
            'foreignKey' => 'seating_number',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Accounts', [
            'foreignKey' => 'account_code',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->decimal('debe')
            ->requirePresence('debe', 'create')
            ->notEmpty('debe');

        $validator
            ->decimal('haber')
            ->requirePresence('haber', 'create')
            ->notEmpty('haber');

        $validator
            ->integer('seating_number')
            ->requirePresence('seating_number', 'create')
            ->notEmpty('seating_number');

        $validator
            ->integer('account_code')
            ->requirePresence('account_code', 'create')
            ->notEmpty('account_code');

        return $validator;
    }
}
