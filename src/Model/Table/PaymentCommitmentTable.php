<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PaymentCommitment Model
 *
 * @property \App\Model\Table\ConnectionsTable|\Cake\ORM\Association\BelongsTo $Connections
 *
 * @method \App\Model\Entity\PaymentCommitment get($primaryKey, $options = [])
 * @method \App\Model\Entity\PaymentCommitment newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PaymentCommitment[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PaymentCommitment|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PaymentCommitment|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PaymentCommitment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PaymentCommitment[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PaymentCommitment findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PaymentCommitmentTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('payment_commitment');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Connections', [
            'foreignKey' => 'connection_id'
        ]);

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_code'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('customer_code')
            ->requirePresence('customer_code', 'create')
            ->notEmpty('customer_code');

        $validator
            ->dateTime('duedate')
            ->requirePresence('duedate', 'create')
            ->notEmpty('duedate');

        $validator
            ->scalar('comment')
            ->requirePresence('comment', 'create')
            ->notEmpty('comment');

        $validator
            ->scalar('promise')
            ->maxLength('promise', 20)
            ->allowEmpty('promise');

        return $validator;
    }

    public function findServerSideData(Query $query, array $options)
    {
        $params  = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            'PaymentCommitment.created',
            'PaymentCommitment.comment',
            'PaymentCommitment.duedate',
            'PaymentCommitment.promise',
        ];

        $columns_select = [
            'PaymentCommitment.id',
            'PaymentCommitment.created',
            'PaymentCommitment.customer_code',
            'PaymentCommitment.connection_id',
            'PaymentCommitment.comment',
            'PaymentCommitment.duedate',
            'PaymentCommitment.promise',
        ];

        if ($params['main']) {

            $columns = [
                'PaymentCommitment.created',
                'PaymentCommitment.comment',
                'PaymentCommitment.customer_code',
                'PaymentCommitment.ident',
                'PaymentCommitment.name',
                'PaymentCommitment.duedate',
                'PaymentCommitment.promise',
            ];

            $columns_select = [
                'PaymentCommitment.id',
                'PaymentCommitment.created',
                'PaymentCommitment.comment',
                'PaymentCommitment.customer_code',
                'PaymentCommitment.ident',
                'PaymentCommitment.name',
                'PaymentCommitment.duedate',
                'PaymentCommitment.promise',
            ];
        }

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] =>  $o['dir']];
        }

        $contain = [];

        if ($params['main']) {
            array_push($contain, 'Customers');
        }

        $query = $query
            ->contain($contain)
            ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        $labels_ids = []; 

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'PaymentCommitment.created':
                    case 'PaymentCommitment.duedate':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0].' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'PaymentCommitment.id':
                    case 'PaymentCommitment.customer_code':
                    case 'PaymentCommitment.connection_id':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'PaymentCommitment.promise':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? true : false];

                    case 'PaymentCommitment.comment':
                    case 'Customers.name':
                    case 'Customers.ident':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'PaymentCommitment.id':
                    case 'PaymentCommitment.customer_code':
                    case 'PaymentCommitment.connection_id':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'PaymentCommitment.comment':
                    case 'Customers.name':
                    case 'Customers.ident':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {
                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order($order);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        return $query;
    }

    public function findRecordsFiltered(Query $query, array $options)
    {
        $params  = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;
        $columns = [
            'PaymentCommitment.created',
            'PaymentCommitment.comment',
            'PaymentCommitment.duedate',
            'PaymentCommitment.promise',
        ];

        $columns_select = [
            'PaymentCommitment.id',
            'PaymentCommitment.created',
            'PaymentCommitment.customer_code',
            'PaymentCommitment.connection_id',
            'PaymentCommitment.comment',
            'PaymentCommitment.duedate',
            'PaymentCommitment.promise',
        ];

        if ($params['main']) {

            $columns = [
                'PaymentCommitment.created',
                'PaymentCommitment.comment',
                'PaymentCommitment.customer_code',
                'PaymentCommitment.ident',
                'PaymentCommitment.name',
                'PaymentCommitment.duedate',
                'PaymentCommitment.promise',
            ];

            $columns_select = [
                'PaymentCommitment.id',
                'PaymentCommitment.created',
                'PaymentCommitment.comment',
                'PaymentCommitment.customer_code',
                'PaymentCommitment.ident',
                'PaymentCommitment.name',
                'PaymentCommitment.duedate',
                'PaymentCommitment.promise',
            ];
        }

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        $contain = [];

        if ($params['main']) {
            array_push($contain, 'Customers');
        }

        $query = $query->contain($contain)
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'PaymentCommitment.created':
                    case 'PaymentCommitment.duedate':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0].' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'PaymentCommitment.id':
                    case 'PaymentCommitment.connection_id':
                    case 'PaymentCommitment.customer_code':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'PaymentCommitment.promise':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? true : false];

                    case 'PaymentCommitment.comment':
                    case 'Customers.name':
                    case 'Customers.ident':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'PaymentCommitment.id':
                    case 'PaymentCommitment.connection_id':
                    case 'PaymentCommitment.customer_code':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'PaymentCommitment.comment':
                    case 'Customers.name':
                    case 'Customers.ident':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {
                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        return $query->count();
    }

    public function findServerSideDataMovements(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            '',
            'PaymentCommitment.created',
            'PaymentCommitment.comment',
            'PaymentCommitment.duedate',
            'PaymentCommitment.import',
        ];

        $columns_select = [
            'PaymentCommitment.id',
            'PaymentCommitment.created',
            'PaymentCommitment.comment',
            'PaymentCommitment.duedate',
            'PaymentCommitment.import',
            'PaymentCommitment.customer_code',
        ];

        //paginacion

        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        $query = $query->contain([])
            ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'PaymentCommitment.created':
                    case 'PaymentCommitment.duedate': 
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'PaymentCommitment.import':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                            
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'PaymentCommitment.comment':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'PaymentCommitment.comment':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order(['PaymentCommitment.created' => 'desc']);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }
        return $query;
    }

    public function findRecordsFilteredMovements(Query $query, array $options)
    {
        $params = $options['params'];

        $where = [];
        $between = [];
        $orWhere = [];
        $columns = [
            '',
            'PaymentCommitment.created',
            'PaymentCommitment.comment',
            'PaymentCommitment.duedate',
            'PaymentCommitment.import',
        ];

        $columns_select = [
            'PaymentCommitment.id',
            'PaymentCommitment.created',
            'PaymentCommitment.comment',
            'PaymentCommitment.duedate',
            'PaymentCommitment.import',
            'PaymentCommitment.customer_code',
        ];

        $query = $query->contain([])
            ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'PaymentCommitment.created':
                    case 'PaymentCommitment.duedate':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'PaymentCommitment.import':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'PaymentCommitment.comment':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'PaymentCommitment.comment':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }
}
