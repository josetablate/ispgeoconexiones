<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Log\Log;

/**
 * ErrorLog Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\ErrorLog get($primaryKey, $options = [])
 * @method \App\Model\Entity\ErrorLog newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ErrorLog[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ErrorLog|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ErrorLog patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ErrorLog[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ErrorLog findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ErrorLogTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('error_log');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('msg')
            ->requirePresence('msg', 'create')
            ->notEmpty('msg');

        $validator
            ->scalar('data')
            ->allowEmpty('data');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function findServerSideData(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        // `id`, `created`, `msg`, `data`, `user_id`, `type`, `view`, `event`

        $columns = [
            'ErrorLog.id',      //0
            'ErrorLog.created', //1
            'ErrorLog.event',   //2
            'ErrorLog.msg',     //3
            'Users.username',   //4
            'ErrorLog.data',    //5
            'ErrorLog.type',    //6
            'ErrorLog.view',    //7
            'ErrorLog.event',   //8
        ];

        $columns_select = [
            'ErrorLog.id',      //0
            'ErrorLog.created', //1
            'ErrorLog.event',   //2
            'ErrorLog.msg',     //3
            'ErrorLog.user_id', //4
            'ErrorLog.data',    //5
            'ErrorLog.type',    //6
            'ErrorLog.view',    //7
            'Users.username',   //8
        ];

        //paginacion

        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] =>  $o['dir']];
        }

        $query = $query->contain([
            'Users'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {
                // Log::debug($columns);
                // Log::debug($index);
                // Log::debug($columns[$index]);

                // `id`, `created`, `msg`, `data`, `user_id`, `type`, `view`, `event`

                switch ($columns[$index]) {

                    case 'ErrorLog.created': 

                        $value = explode('<>',$column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2]  . '-' . $value[1][1] . '-' .  $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                            
                        }
                        else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        }
                        else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0].' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }

                        break;

                    case 'ErrorLog.user_id':

                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    default: 
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            // `id`, `created`, `msg`, `data`, `user_id`, `type`, `view`, `event`

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {
                    case 'ErrorLog.created': 
                        break;
                    case 'ErrorLog.user_id' :
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;
                    case 'ErrorLog.msg':
                    case 'ErrorLog.type':
                    case 'ErrorLog.view':
                    case 'ErrorLog.data':
                    case 'ErrorLog.event':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order($order);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        return $query;
    }

    public function findRecordsFiltered(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        // `id`, `created`, `msg`, `data`, `user_id`, `type`, `view`, `event`

        $columns = [
            'ErrorLog.id',      //0
            'ErrorLog.created', //1
            'ErrorLog.event',   //2
            'ErrorLog.msg',     //3
            'Users.username',   //4
            'ErrorLog.data',    //5
            'ErrorLog.type',    //6
            'ErrorLog.view',    //7
            'ErrorLog.event',   //8
        ];

        $columns_select = [
            'ErrorLog.id',      //0
            'ErrorLog.created', //1
            'ErrorLog.event',   //2
            'ErrorLog.msg',     //3
            'ErrorLog.user_id', //4
            'ErrorLog.data',    //5
            'ErrorLog.type',    //6
            'ErrorLog.view',    //7
            'Users.username',   //8
        ];

        //paginacion

        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] =>  $o['dir']];
        }

        $query = $query->contain([
            'Users'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {
                //Log::debug($columns);
                //Log::debug($index);
                //Log::debug($columns[$index]);

                // `id`, `created`, `msg`, `data`, `user_id`, `type`, `view`, `event`

                switch ($columns[$index]) {

                    case 'ErrorLog.created': 

                        $value = explode('<>',$column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2]  . '-' . $value[1][1] . '-' .  $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                            
                        }
                        else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        }
                        else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0].' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }

                        break;

                    case 'ErrorLog.user_id':     

                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    default: 
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            // `id`, `created`, `msg`, `data`, `user_id`, `type`, `view`, `event`

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {
                    case 'ErrorLog.created': 
                        break;
                    case 'ErrorLog.user_id' :
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;
                    case 'ErrorLog.msg':
                    case 'ErrorLog.type':
                    case 'ErrorLog.view':
                    case 'ErrorLog.data':
                    case 'ErrorLog.event':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order($order);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        return $query->count();
    }
    
    
    public function findServerSideDataQuickView(Query $query, array $options)
    {
        $params = $options['params'];
  
        $where = [];
     
        // `id`, `created`, `msg`, `data`, `user_id`, `type`, `view`, `event`

        $columns = [
            'ErrorLog.id',      //0
            'ErrorLog.created', //1
            'ErrorLog.event',   //2
            'ErrorLog.msg',     //3
            'Users.username',   //4
            'ErrorLog.data',    //5
            'ErrorLog.type',    //6
            'ErrorLog.view',    //7
            'ErrorLog.event',   //8
        ];

        $columns_select = [
            'ErrorLog.id',      //0
            'ErrorLog.created', //1
            'ErrorLog.event',   //2
            'ErrorLog.msg',     //3
            'ErrorLog.user_id', //4
            'ErrorLog.data',    //5
            'ErrorLog.type',    //6
            'ErrorLog.view',    //7
            'Users.username',   //8
        ];

        $query = $query->contain([
            'Users'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        if ($where) {
            $query->where($where);
        }

        $query->order(['ErrorLog.id' => 'DESC']);

        return $query;
    }

    public function findRecordsFilteredQuickView(Query $query, array $options)
    {
        $params = $options['params'];
    
        $where = [];

        // `id`, `created`, `msg`, `data`, `user_id`, `type`, `view`, `event`

        $columns = [
            'ErrorLog.id',      //0
            'ErrorLog.created', //1
            'ErrorLog.event',   //2
            'ErrorLog.msg',     //3
            'Users.username',   //4
            'ErrorLog.data',    //5
            'ErrorLog.type',    //6
            'ErrorLog.view',    //7
            'ErrorLog.event',   //8
        ];

        $columns_select = [
            'ErrorLog.id',      //0
            'ErrorLog.created', //1
            'ErrorLog.event',   //2
            'ErrorLog.msg',     //3
            'ErrorLog.user_id', //4
            'ErrorLog.data',    //5
            'ErrorLog.type',    //6
            'ErrorLog.view',    //7
            'Users.username',   //8
        ];


        $query = $query->contain([
            'Users'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        if ($where) {
            $query->where($where);
        }


        $query->order(['ErrorLog.id' => 'DESC']);

        return $query->count();
    }
}
