<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

/**
 * Controllers Model
 *
 * @property \Cake\ORM\Association\HasMany $IntegrationControllersHasPlans
 *
 * @method \App\Model\Entity\IntegrationController get($primaryKey, $options = [])
 * @method \App\Model\Entity\IntegrationController newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\IntegrationController[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\IntegrationController|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\IntegrationController patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\IntegrationController[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\IntegrationController findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ControllersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('controllers');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->notEmpty('name');

        $validator
            ->allowEmpty('local_anddress');

        $validator
            ->allowEmpty('server_dns');

        $validator
            ->allowEmpty('queue_type');

        $validator
            ->allowEmpty('template');

        $validator
            ->allowEmpty('description');

        $validator
            ->numeric('lat')
            ->allowEmpty('lat');

        $validator
            ->numeric('lng')
            ->allowEmpty('lng');

        $validator
            ->notEmpty('connect_to');

        $validator
            ->integer('port')
            ->allowEmpty('port');

        $validator
            ->notEmpty('username');

        $validator
            ->allowEmpty('password');

        $validator
            ->boolean('enabled')
            ->notEmpty('enabled');

        $validator
            ->notEmpty('trademark');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->isUnique(['name']));

        return $rules;
    }

    public function afterSave($event, $entity, $options)
    {
        $ok = false;

        $detail = '';

        if (!$entity->isNew()) {

            $action = 'Edición de Controlador';

            $dirtyFields = $entity->getDirty();

            $dirtyFields = array_diff($dirtyFields, ["modified"]);

            foreach ($dirtyFields as $dirtyField) {

                switch ($dirtyField) {

                    case 'name': 
                        $detail .= 'Nombre: ' . $entity->getOriginal('name') . ' => ' . $entity->name . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'template': 
                        $detail .= 'Tipo de conexión: ' . $entity->getOriginal('template') . ' => ' . $entity->template . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'description': 
                        $detail .= 'Descripción: ' . $entity->getOriginal('description') . ' => ' . $entity->description . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'connect_to': 
                        $detail .= 'Conectar a: ' . $entity->getOriginal('connect_to') . ' => ' . $entity->connect_to . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'port': 
                        $detail .= 'Puerto API: ' . $entity->getOriginal('port') . ' => ' . $entity->port . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'username': 
                        $detail .= 'Usuario: ' . $entity->getOriginal('username') . ' => ' . $entity->username . PHP_EOL; 
                        $ok = true;
                    case 'password': 
                        $detail .= 'Contraseña: ' . $entity->getOriginal('password') . ' => ' . $entity->password . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'trademark': 
                        $detail .= 'Marca: ' . $entity->getOriginal('trademark') . ' => ' . $entity->trademark . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'port_grap': 
                        $detail .= 'Puerto Gráfica: ' . $entity->getOriginal('port_grap') . ' => ' . $entity->port_grap . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'port_ssl': 
                        $detail .= 'Puerto SSL: ' . ($entity->getOriginal('port_ssl') ? 'Si' : 'No' ) . ' => ' . ($entity->port_ssl ? 'Si' : 'No' ) . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'use_tls': 
                        $detail .= 'Usa TLS: ' . $entity->getOriginal('use_tls') . ' => ' . $entity->use_tls . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'enabled':
                        $detail .= 'Habilitado: ' . ($entity->getOriginal('enabled') ? 'Si' : 'No' ) . ' => ' . ($entity->enabled ? 'Si' : 'No' ) . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'apply_config_avisos':
                        $detail .= 'APLICAR CONFIGURACIÓN AVISOS: ' . ($entity->getOriginal('apply_config_avisos') ? 'Si' : 'No' ) . ' => ' . ($entity->apply_config_avisos ? 'Si' : 'No' ) . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'apply_config_queue_graph':
                        $detail .= 'APLICAR CONFIGURACIÓN QUEUES GRAPH: ' . ($entity->getOriginal('apply_config_queue_graph') ? 'Si' : 'No' ) . ' => ' . ($entity->apply_config_queue_graph ? 'Si' : 'No' ) . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'apply_config_https':
                        $detail .= 'GENERAR CERTIFICADO: ' . ($entity->getOriginal('apply_config_https') ? 'Si' : 'No' ) . ' => ' . ($entity->apply_config_https ? 'Si' : 'No' ) . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'apply_config_temp_access':
                        $detail .= 'CONFIGURAR ACCESO NUEVO CLIENTE: ' . ($entity->getOriginal('apply_config_temp_access') ? 'Si' : 'No' ) . ' => ' . ($entity->apply_config_temp_access ? 'Si' : 'No' ) . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'apply_certificate_apissl':
                        $detail .= 'APLICAR CERTIFICADO API SSL: ' . ($entity->getOriginal('apply_certificate_apissl') ? 'Si' : 'No' ) . ' => ' . ($entity->apply_certificate_apissl ? 'Si' : 'No' ) . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'apply_certificate_apissl':
                        $detail .= 'APLICAR CERTIFICADO WWW SSL: ' . ($entity->getOriginal('apply_certificate_apissl') ? 'Si' : 'No' ) . ' => ' . ($entity->apply_certificate_apissl ? 'Si' : 'No' ) . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'deleted':
                        if ($entity->deleted) $action = 'Eliminación de Controlador'; else $action = 'Restauración de Controlador';
                        $ok = true;
                        break;
               }
            }
        } else {
            $action = 'Creación de Controlador';
            $detail = 'Nombre: ' . $entity->name . PHP_EOL;
            $detail .= 'Tipo de conexión: ' . $entity->template . PHP_EOL;
            $detail .= 'Descripción: ' . $entity->description . PHP_EOL;
            $detail .= 'Conectar a: ' . $entity->connect_to . PHP_EOL;
            $detail .= 'Puerto API: ' . $entity->port . PHP_EOL;
            $detail .= 'Puerto SSL: ' . $entity->port_ssl . PHP_EOL;
            $detail .= 'Puerto Gráfica: ' . $entity->port_grap . PHP_EOL;
            $detail .= 'Usuario: ' . $entity->username . PHP_EOL;
            $detail .= 'Contraseña: ' . $entity->password . PHP_EOL;
            $detail .= 'Marca: ' . $entity->trademark . PHP_EOL;
            $detail .= 'Habilitado: ' . ($entity->enabled ? 'Si' : 'No') . PHP_EOL;
            $ok = true;
        }

        if ($ok) {

            if (!isset($_SESSION['Auth']['User']['id'])) {
    	        $user_id = 100;
    	    } else {
    	        $user_id = $_SESSION['Auth']['User']['id'];
    	    }

            $actionLog = TableRegistry::get('ActionLog');
            $query = $actionLog->query();
            $query->insert([
                'created', 
                'detail',
                'user_id',
                'action',
                'customer_code'
            ])
            ->values([
                'created' => Time::now(), 
                'detail' => $detail,
                'user_id' => $user_id,
                'action' => $action,
                'customer_code' => isset($entity->customer_code) ? $entity->customer_code : null,
            ])
            ->execute();
        }
    }
}
