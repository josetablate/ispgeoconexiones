<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

/**
 * Discounts Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\ServicesTable|\Cake\ORM\Association\HasMany $Services
 * @property \App\Model\Table\ServicesHasDiscountsTable|\Cake\ORM\Association\HasMany $ServicesHasDiscounts
 *
 * @method \App\Model\Entity\Discount get($primaryKey, $options = [])
 * @method \App\Model\Entity\Discount newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Discount[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Discount|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Discount patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Discount[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Discount findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class DiscountsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('discounts');
        $this->setDisplayField('conceptAndCode');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Services', [
            'foreignKey' => 'discount_id'
        ]);
        $this->hasMany('ServicesHasDiscounts', [
            'foreignKey' => 'discount_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('concept')
            ->requirePresence('concept', 'create')
            ->notEmpty('concept');

        $validator
            ->allowEmpty('comments');



        $validator
            ->decimal('value')
            ->requirePresence('value', 'create')
            ->notEmpty('value');

        $validator
            ->integer('duration')
            ->requirePresence('duration', 'create')
            ->notEmpty('duration');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function afterSave($event, $entity, $options)
    {
        $ok = false;

        $detail = '';

        if (!$entity->isNew()) {

            $action = 'Edición de Regla de Descuento';

            $dirtyFields = $entity->getDirty();

            $dirtyFields = array_diff($dirtyFields, ["modified"]);

            foreach ($dirtyFields as $dirtyField) {

                switch ($dirtyField) {

                    case 'concept': 
                        $detail .= 'Concepto: ' . $entity->getOriginal('concept') . ' => ' . $entity->concept . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'comments': 
                        $detail .= 'Comentario: ' . $entity->getOriginal('comments') . ' => ' . $entity->comments . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'aliquot':
                        $detail .= 'Alícuota: ' . $_SESSION['afip_codes']['alicuotas_types'][$entity->getOriginal('aliquot')] . ' => ' . $_SESSION['afip_codes']['alicuotas_types'][$entity->aliquot] . PHP_EOL;
                        $ok = true;
                        break;
                    case 'value':
                        $detail .= 'Valor: ' .  number_format($entity->getOriginal('value'), 2, ',', '.') . ' => ' . number_format($entity->value, 2, ',', '.') . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'always':
                        $detail .= 'Siempre: ' . ($entity->getOriginal('always') ? 'Si' : 'No' ) . ' => ' . ($entity->always ? 'Si' : 'No' ) . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'duration': 
                        $detail .= 'Comentario: ' . $entity->getOriginal('duration') . ' => ' . $entity->duration . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'code': 
                        $detail .= 'Código: ' . $entity->getOriginal('code') . ' => ' . $entity->code . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'enabled':
                        if ($entity->enabled) $action = 'Habilitación de Regla de Descuento'; else $action = 'Deshabilitación de Regla de Descuento';
                        $ok = true;
                        break;
                    case 'deleted':
                        if ($entity->deleted) $action = 'Eliminación de Regla de Descuento'; else $action = 'Restauración de Regla de Descuento';
                        $ok = true;
                        break;
                    case 'type':
                        $type = [
                            'generic' => 'General',
                            'service' => 'Servicio',
                            'package' => 'Paquete',
                            'product' => 'Producto'
                        ];
                        $detail .= 'Tipo: ' . $type[$entity->getOriginal('type')] . ' => ' . $type[$entity->type] . PHP_EOL; 
                        $ok = true;
                        break;
               }
            }
        } else {
            $type = [
                'generic' => 'General',
                'service' => 'Servicio',
                'package' => 'Paquete',
                'product' => 'Producto'
            ];
            $action = 'Creación de Regla de Descuento';
            $detail = 'Concepto: ' . $entity->concept . PHP_EOL;
            $detail = 'Comentario: ' . $entity->comments . PHP_EOL;
            $detail .= 'Alícuota: ' . $entity->aliquot . PHP_EOL;
            $detail .= 'Valor: ' . number_format($entity->value, 2, ',', '.') . PHP_EOL;
            $detail .= 'Siempre: ' . ($entity->always ? 'Si' : 'No') . PHP_EOL;
            $detail .= 'Duración (meses): ' . $entity->duration . PHP_EOL;
            $detail .= 'Código: ' . $entity->code . PHP_EOL;
            $detail .= 'Habilitado: ' . ($entity->enabled ? 'Si' : 'No') . PHP_EOL;
            $detail .= 'Tipo: ' . $type[$entity->type] . PHP_EOL; 
            $ok = true;
        }

        if ($ok) {

            if (!isset($_SESSION['Auth']['User']['id'])) {
    	        $user_id = 100;
    	    } else {
    	        $user_id = $_SESSION['Auth']['User']['id'];
    	    }

            $actionLog = TableRegistry::get('ActionLog');
            $query = $actionLog->query();
            $query->insert([
                'created', 
                'detail',
                'user_id',
                'action',
                'customer_code'
            ])
            ->values([
                'created' => Time::now(), 
                'detail' => $detail,
                'user_id' => $user_id,
                'action' => $action,
                'customer_code' => isset($entity->customer_code) ? $entity->customer_code : null,
            ])
            ->execute();
        }
    }
}
