<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PayuTransactions Model
 *
 * @property \App\Model\Table\BloquesTable|\Cake\ORM\Association\BelongsTo $Bloques
 * @property \App\Model\Table\PaymentGetwaysTable|\Cake\ORM\Association\BelongsTo $PaymentGetways
 *
 * @method \App\Model\Entity\PayuTransaction get($primaryKey, $options = [])
 * @method \App\Model\Entity\PayuTransaction newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PayuTransaction[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PayuTransaction|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PayuTransaction|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PayuTransaction patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PayuTransaction[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PayuTransaction findOrCreate($search, callable $callback = null, $options = [])
 */
class PayuTransactionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('payu_transactions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_code',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('fecha_pago')
            ->requirePresence('fecha_pago', 'create')
            ->notEmpty('fecha_pago');

        $validator
            ->scalar('id_orden')
            ->maxLength('id_orden', 20)
            ->allowEmpty('id_orden');

        $validator
            ->decimal('valor')
            ->allowEmpty('valor');

        $validator
            ->scalar('pais')
            ->maxLength('pais', 20)
            ->allowEmpty('pais');

        $validator
            ->scalar('cuenta')
            ->maxLength('cuenta', 20)
            ->allowEmpty('cuenta');

        $validator
            ->scalar('numero_pago')
            ->maxLength('numero_pago', 20)
            ->allowEmpty('numero_pago');

        $validator
            ->scalar('id_cupon')
            ->maxLength('id_cupon', 20)
            ->allowEmpty('id_cupon');

        $validator
            ->scalar('referencia_venta')
            ->maxLength('referencia_venta', 20)
            ->allowEmpty('referencia_venta');

        $validator
            ->scalar('estado')
            ->maxLength('estado', 200)
            ->allowEmpty('estado');

        $validator
            ->scalar('medio_pago')
            ->maxLength('medio_pago', 20)
            ->allowEmpty('medio_pago');

        $validator
            ->scalar('tipo_creacion')
            ->maxLength('tipo_creacion', 50)
            ->allowEmpty('tipo_creacion');

        $validator
            ->boolean('multiple_pagos')
            ->requirePresence('multiple_pagos', 'create')
            ->notEmpty('multiple_pagos');

        $validator
            ->scalar('frecuencia_recordatorio')
            ->maxLength('frecuencia_recordatorio', 50)
            ->allowEmpty('frecuencia_recordatorio');

        $validator
            ->dateTime('fecha_recordatorio')
            ->requirePresence('fecha_recordatorio', 'create')
            ->allowEmpty('fecha_recordatorio');

        $validator
            ->scalar('nombre_cliente')
            ->maxLength('nombre_cliente', 200)
            ->allowEmpty('nombre_cliente');

        $validator
            ->scalar('concepto')
            ->maxLength('concepto', 100)
            ->allowEmpty('concepto');

        $validator
            ->dateTime('fecha_expiracion')
            ->requirePresence('fecha_expiracion', 'create')
            ->allowEmpty('fecha_expiracion');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->scalar('comments')
            ->maxLength('comments', 100)
            ->allowEmpty('comments');

        $validator
            ->integer('customer_code')
            ->allowEmpty('customer_code');

        $validator
            ->scalar('receipt_number')
            ->maxLength('receipt_number', 20)
            ->allowEmpty('receipt_number');

        return $validator;
    }

    public function findServerSideDataTransactions(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            'PayuTransactions.fecha_pago',        //0
            'PayuTransactions.payment_getway_id', //1 metodo de pago
            'PayuTransactions.estado',            //2
            'Customers.code',                     //3
            'Customers.name',                     //4
            'Customers.ident',                    //5
            'PayuTransactions.id_orden',          //6
            'PayuTransactions.valor',             //7
            'CobrodigitalTransactions.comments',  //8
            'PayuTransactions.numero_pago',       //9
            'PayuTransactions.receipt_number',    //10
        ];

        $columns_select = [
            'PayuTransactions.id',                //0
            'PayuTransactions.fecha_pago',        //1
            'PayuTransactions.payment_getway_id', //2
            'PayuTransactions.estado',            //3
            'Customers.code',                     //4
            'Customers.name',                     //5
            'Customers.ident',                    //6
            'Customers.doc_type',                 //7
            'PayuTransactions.id_orden',          //8
            'PayuTransactions.valor',             //9
            'PayuTransactions.comments',          //10
            'PayuTransactions.numero_pago',       //11
            'PayuTransactions.receipt_number',    //12
            'PayuTransactions.receipt_id',        //13
        ];

        //paginacion

        if ($params['length'] > 0) {
            
            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        $query = $query->contain([
            'Customers'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'PayuTransactions.fecha_pago':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] .'-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Customers.code':
                    case 'PayuTransactions.payment_getway_id':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'PayuTransactions.valor':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'PayuTransactions.estado':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? true : false];
                        break;

                    case 'PayuTransactions.comments':
                    case 'Customers.name':
                    case 'PayuTransactions.id_orden':
                    case 'PayuTransactions.numero_pago':
                    case 'PayuTransactions.receipt_number':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);
 
        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Customers.code':
                    case 'PayuTransactions.payment_getway_id':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'PayuTransactions.comments':
                    case 'Customers.name':
                    case 'PayuTransactions.id_orden':
                    case 'PayuTransactions.numero_pago':
                    case 'PayuTransactions.receipt_number':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order(['PayuTransactions.fecha_pago' => 'desc']);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        $query->toArray();

        return $query;
    }

    public function findRecordsFilteredTransactions(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            'PayuTransactions.fecha_pago',        //0
            'PayuTransactions.payment_getway_id', //1 metodo de pago
            'PayuTransactions.estado',            //2
            'Customers.code',                     //3
            'Customers.name',                     //4
            'Customers.ident',                    //5
            'PayuTransactions.id_orden',          //6
            'PayuTransactions.valor',             //7
            'CobrodigitalTransactions.comments',  //8
            'PayuTransactions.numero_pago',       //9
            'PayuTransactions.receipt_number',    //10
        ];

        $columns_select = [
            'PayuTransactions.id',                //0
            'PayuTransactions.fecha_pago',        //1
            'PayuTransactions.payment_getway_id', //2
            'PayuTransactions.estado',            //3
            'Customers.code',                     //4
            'Customers.name',                     //5
            'Customers.ident',                    //6
            'Customers.doc_type',                 //7
            'PayuTransactions.id_orden',          //8
            'PayuTransactions.valor',             //9
            'PayuTransactions.comments',          //10
            'PayuTransactions.numero_pago',       //11
            'PayuTransactions.receipt_number',    //12
            'PayuTransactions.receipt_id',        //13
        ];

        $query = $query->contain([
            'Customers'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'PayuTransactions.fecha_pago':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] .'-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Customers.code':
                    case 'PayuTransactions.payment_getway_id':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'PayuTransactions.valor':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'PayuTransactions.estado':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? true : false];
                        break;

                    case 'PayuTransactions.comments':
                    case 'Customers.name':
                    case 'PayuTransactions.id_orden':
                    case 'PayuTransactions.numero_pago':
                    case 'PayuTransactions.receipt_number':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);
 
        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Customers.code':
                    case 'PayuTransactions.payment_getway_id':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'PayuTransactions.comments':
                    case 'Customers.name':
                    case 'PayuTransactions.id_orden':
                    case 'PayuTransactions.numero_pago':
                    case 'PayuTransactions.receipt_number':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }
}
