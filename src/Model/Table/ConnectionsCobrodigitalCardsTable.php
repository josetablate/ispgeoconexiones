<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ConnectionsCobrodigitalCards Model
 *
 * @property \App\Model\Table\ConnectionsTable|\Cake\ORM\Association\BelongsTo $Connections
 * @property \App\Model\Table\CobrodigitalCardsTable|\Cake\ORM\Association\BelongsTo $CobrodigitalCards
 *
 * @method \App\Model\Entity\ConnectionsCobrodigitalCard get($primaryKey, $options = [])
 * @method \App\Model\Entity\ConnectionsCobrodigitalCard newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ConnectionsCobrodigitalCard[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ConnectionsCobrodigitalCard|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ConnectionsCobrodigitalCard patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ConnectionsCobrodigitalCard[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ConnectionsCobrodigitalCard findOrCreate($search, callable $callback = null, $options = [])
 */
class ConnectionsCobrodigitalCardsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('connections_cobrodigital_cards');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Connections', [
            'foreignKey' => 'connection_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('CobrodigitalCards', [
            'foreignKey' => 'cobrodigital_card_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['connection_id'], 'Connections'));
        $rules->add($rules->existsIn(['cobrodigital_card_id'], 'CobrodigitalCards'));

        return $rules;
    }
}
