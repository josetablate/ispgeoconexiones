<?php
namespace App\Model\Table\MikrotikIpfijaTa;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class ArpsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        
        $this->setEntityClass('App\Model\Entity\MikrotikIpfijaTa\Arp');

        $this->setTable('arp');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

       
        $this->belongsTo('MikrotikIpfijaTa_Controllers', [
            'foreignKey' => 'controller_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('MikrotikIpfijaTa_Pools', [
            'foreignKey' => 'pool_id'
        ]);
        $this->belongsTo('MikrotikIpfijaTa_Plans', [
            'foreignKey' => 'plan_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('address')
            ->requirePresence('address', 'create')
            ->notEmpty('address');

        $validator
            ->scalar('mac_address')
            ->maxLength('mac_address', 45)
            ->requirePresence('mac_address', 'create')
            ->notEmpty('mac_address');

        $validator
            ->scalar('interface')
            ->maxLength('interface', 45)
            ->requirePresence('interface', 'create')
            ->notEmpty('interface');

        $validator
            ->boolean('enabled')
            ->requirePresence('enabled', 'create')
            ->notEmpty('enabled');

        $validator
            ->scalar('comment')
            ->maxLength('comment', 100)
            ->allowEmpty('comment');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
    
        $rules->add($rules->existsIn(['controller_id'], 'MikrotikIpfijaTa_Controllers'));
        $rules->add($rules->existsIn(['pool_id'], 'MikrotikIpfijaTa_Pools'));
        $rules->add($rules->existsIn(['plan_id'], 'MikrotikIpfijaTa_Plans'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'ispbrain_mikrotik_ipfija_ta';
    }
}
