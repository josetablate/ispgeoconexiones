<?php
namespace App\Model\Table\MikrotikIpfijaTa;


use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;



class PoolsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        
        
        parent::initialize($config);
        
        $this->setEntityClass('App\Model\Entity\MikrotikIpfijaTa\Pool');
        
        $this->setTable('pools');
        $this->setDisplayField('nameAndAddresses');
        $this->setPrimaryKey('id');

  
        $this->belongsTo('MikrotikIpfijaTa_Controllers', [
            'foreignKey' => 'controller_id',
            'joinType' => 'INNER'
        ]);
        
        $this->hasMany('MikrotikIpfijaTa_Plans', [
            'foreignKey' => 'pool_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('addresses', 'create')
            ->notEmpty('addresses');

        $validator
            ->requirePresence('min_host', 'create')
            ->notEmpty('min_host');

        $validator
            ->requirePresence('max_host', 'create')
            ->notEmpty('max_host');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->isUnique(['addresses'], 'Existe otro pool con este addresses.'));
        // $rules->add($rules->isUnique(['name'], 'Existe otro pool con este nombre.'));
        // $rules->add($rules->existsIn(['next_pool_id'], 'Pools'));
        $rules->add($rules->existsIn(['controller_id'], 'MikrotikIpfijaTa_Controllers'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'ispbrain_mikrotik_ipfija_ta';
    }
}
