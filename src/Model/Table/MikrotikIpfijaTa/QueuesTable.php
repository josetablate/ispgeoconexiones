<?php
namespace App\Model\Table\MikrotikIpfijaTa;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Rule\IsUnique;


class QueuesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        
        $this->setEntityClass('App\Model\Entity\MikrotikIpfijaTa\Queue');

        $this->setTable('queues');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        
        $this->belongsTo('MikrotikIpfijaTa_Controllers', [
            'foreignKey' => 'controller_id',
            'joinType' => 'INNER',
            'propertyName' => 'controller'
        ]);
     
        $this->belongsTo('MikrotikIpfijaTa_Pools', [
            'foreignKey' => 'pool_id',
            'joinType' => 'LEFT',
            'propertyName' => 'pool'
        ]);
        $this->belongsTo('MikrotikIpfijaTa_Profiles', [
            'foreignKey' => 'profile_id',
            'joinType' => 'INNER',
            'propertyName' => 'profile'
        ]);
        $this->belongsTo('MikrotikIpfijaTa_Plans', [
            'foreignKey' => 'plan_id',
            'joinType' => 'INNER',
            'propertyName' => 'plan'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->integer('target')
            ->requirePresence('target', 'create')
            ->notEmpty('target');

        $validator
            ->allowEmpty('comment');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->isUnique(['name'], 'El nombre de queues ya existe en la base de datos'));
        // $rules->add($rules->isUnique(['target'], 'Esta IP ya existe en la base de datos'));
        
        $rules->add($rules->existsIn(['controller_id'], 'MikrotikIpfijaTa_Controllers'));
        $rules->add($rules->existsIn(['pool_id'], 'MikrotikIpfijaTa_Pools'));
        $rules->add($rules->existsIn(['profile_id'], 'MikrotikIpfijaTa_Profiles'));
        $rules->add($rules->existsIn(['plan_id'], 'MikrotikIpfijaTa_Plans'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'ispbrain_mikrotik_ipfija_ta';
    }
}
