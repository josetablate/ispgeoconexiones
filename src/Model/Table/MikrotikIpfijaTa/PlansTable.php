<?php
namespace App\Model\Table\MikrotikIpfijaTa;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;


class PlansTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {  
        parent::initialize($config);
        
        $this->setEntityClass('App\Model\Entity\MikrotikIpfijaTa\Plan');

        $this->setTable('plans');
        
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        
        $this->belongsTo('MikrotikIpfijaTa_Profiles', [
            'foreignKey' => 'profile_id',
            'joinType' => 'INNER',
            'propertyName' => 'profile'
        ]);
        
        $this->belongsTo('MikrotikIpfijaTa_Controllers', [
            'foreignKey' => 'controller_id',
            'joinType' => 'INNER',
        ]);
        
        
        $this->belongsTo('MikrotikIpfijaTa_Pools', [
            'foreignKey' => 'pool_id',
            'joinType' => 'LEFT',
            'propertyName' => 'pool'
        ]);
        
        
        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['controller_id'], 'MikrotikIpfijaTa_Controllers'));
        $rules->add($rules->existsIn(['profile_id'], 'MikrotikIpfijaTa_Profiles'));
        // $rules->add($rules->existsIn(['pool_id'], 'TPools'));
  

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'ispbrain_mikrotik_ipfija_ta';
    }
   
}
