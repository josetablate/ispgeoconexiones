<?php
namespace App\Model\Table\MikrotikIpfijaTa;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Rule\IsUnique;


class AddressListsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        
        $this->setEntityClass('App\Model\Entity\MikrotikIpfijaTa\AddressList');

        $this->setTable('address_lists');
        $this->setDisplayField('address');
        $this->setPrimaryKey('id');

        
        $this->belongsTo('MikrotikIpfijaTa_Controllers', [
            'foreignKey' => 'controller_id',
            'joinType' => 'INNER',
            'propertyName' => 'controller'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('list', 'create')
            ->notEmpty('list');
            
    
        $validator
            ->requirePresence('address', 'create')
            ->notEmpty('address');

        $validator
            ->allowEmpty('comment');
            
        $validator
            ->allowEmpty('api_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->isUnique(['address'], 'Esta IP ya existe en la base de datos'));
        $rules->add($rules->existsIn(['controller_id'], 'MikrotikIpfijaTa_Controllers'));
     

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'ispbrain_mikrotik_ipfija_ta';
    }
}
