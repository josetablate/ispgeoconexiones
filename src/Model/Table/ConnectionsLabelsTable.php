<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ConnectionsLabels Model
 *
 * @property \App\Model\Table\ConnectionsTable|\Cake\ORM\Association\BelongsTo $Connections
 * @property \App\Model\Table\LabelsTable|\Cake\ORM\Association\BelongsTo $Labels
 *
 * @method \App\Model\Entity\ConnectionsLabel get($primaryKey, $options = [])
 * @method \App\Model\Entity\ConnectionsLabel newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ConnectionsLabel[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ConnectionsLabel|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ConnectionsLabel|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ConnectionsLabel patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ConnectionsLabel[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ConnectionsLabel findOrCreate($search, callable $callback = null, $options = [])
 */
class ConnectionsLabelsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('connections_labels');
        $this->setDisplayField('connection_id');
        $this->setPrimaryKey('id');

        // $this->belongsTo('Connections', [
        //     'foreignKey' => 'connection_id',
        //     'joinType' => 'INNER'
        // ]);
        $this->belongsTo('Labels', [
            'foreignKey' => 'label_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->existsIn(['connection_id'], 'Connections'));
        $rules->add($rules->existsIn(['label_id'], 'Labels'));

        return $rules;
    }
}
