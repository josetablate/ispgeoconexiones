<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Log\Log;

/**
 * VisaAutoDebitRecords Model
 *
 * @property \App\Model\Table\InvoicesTable|\Cake\ORM\Association\BelongsTo $Invoices
 * @property \App\Model\Table\BusinessesTable|\Cake\ORM\Association\BelongsTo $Businesses
 * @property \App\Model\Table\PaymentGetwaysTable|\Cake\ORM\Association\BelongsTo $PaymentGetways
 *
 * @method \App\Model\Entity\VisaAutoDebitRecord get($primaryKey, $options = [])
 * @method \App\Model\Entity\VisaAutoDebitRecord newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\VisaAutoDebitRecord[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\VisaAutoDebitRecord|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\VisaAutoDebitRecord|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\VisaAutoDebitRecord patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\VisaAutoDebitRecord[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\VisaAutoDebitRecord findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class VisaAutoDebitRecordsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('visa_auto_debit_records');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Invoices', [
            'foreignKey' => 'invoice_id'
        ]);

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_code',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('date_start')
            ->requirePresence('date_start', 'create')
            ->notEmpty('date_start');

        $validator
            ->dateTime('date_end')
            ->requirePresence('date_end', 'create')
            ->notEmpty('date_end');

        $validator
            ->scalar('card_number')
            ->maxLength('card_number', 200)
            ->requirePresence('card_number', 'create')
            ->notEmpty('card_number');

        $validator
            ->scalar('customer_name')
            ->maxLength('customer_name', 200)
            ->requirePresence('customer_name', 'create')
            ->notEmpty('customer_name');

        $validator
            ->integer('customer_code')
            ->requirePresence('customer_code', 'create')
            ->notEmpty('customer_code');

        $validator
            ->integer('invoice_num')
            ->allowEmpty('invoice_num');

        $validator
            ->scalar('comment')
            ->maxLength('comment', 200)
            ->allowEmpty('comment');

        $validator
            ->dateTime('duedate')
            ->requirePresence('duedate', 'create')
            ->notEmpty('duedate');

        $validator
            ->decimal('total')
            ->requirePresence('total', 'create')
            ->notEmpty('total');

        $validator
            ->scalar('customer_city')
            ->maxLength('customer_city', 45)
            ->allowEmpty('customer_city');

        return $validator;
    }

    public function findServerSideData(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            'VisaAutoDebitRecords.status',        //0
            'VisaAutoDebitRecords.created',       //1
            'VisaAutoDebitRecords.date_start',    //2
            'VisaAutoDebitRecords.date_end',      //3
            'VisaAutoDebitRecords.card_number',   //4
            'VisaAutoDebitRecords.customer_name', //5
            'VisaAutoDebitRecords.customer_ident',//6
            'VisaAutoDebitRecords.customer_code', //7
            'VisaAutoDebitRecords.customer_city', //8
            'VisaAutoDebitRecords.invoice_num',   //9
            'VisaAutoDebitRecords.duedate',       //10
            'VisaAutoDebitRecords.total',         //11
            'VisaAutoDebitRecords.business_id',   //12
        ];

        $columns_select = [
            'VisaAutoDebitRecords.id',
            'VisaAutoDebitRecords.status',
            'VisaAutoDebitRecords.created',
            'VisaAutoDebitRecords.date_start',
            'VisaAutoDebitRecords.date_end',
            'VisaAutoDebitRecords.card_number',
            'VisaAutoDebitRecords.customer_name',
            'VisaAutoDebitRecords.customer_code',
            'VisaAutoDebitRecords.customer_doc_type',
            'VisaAutoDebitRecords.customer_ident',
            'VisaAutoDebitRecords.customer_city',
            'VisaAutoDebitRecords.invoice_num',
            'VisaAutoDebitRecords.duedate',
            'VisaAutoDebitRecords.total',
            'VisaAutoDebitRecords.business_id',
        ];

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] => $o['dir']];
        }

        $query = $query->contain([
            'Customers'
        ])
        ->select($columns_select); 

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    // case 'VisaAutoDebitRecords.date_start':
                    //     $value = [];

                    //     $value[0] = trim($params['columns'][2]['search']['value']);
                    //     $value[1] = trim($params['columns'][3]['search']['value']);

                    //     $value[0] = trim($value[0]);
                    //     $value[1] = trim($value[1]);

                    //     if ($value[0] != '' && $value[1] != '') {
                    //         $value[0] = explode('/', $value[0]);
                    //         $value[1] = explode('/', $value[1]);
                    //         $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                    //         $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                    //         $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                    //     }
                    //     break;

                    case 'VisaAutoDebitRecords.date_start':
                        $value = trim($column['search']['value']);
                        if ($value != '') {
                            $value = explode('/', $value);
                            $value = $value[2] . '-' . $value[1] . '-' . $value[0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >= ' => $value];
                        }
                        break;

                    case 'VisaAutoDebitRecords.date_end':
                        $value = trim($column['search']['value']);
                        if ($value != '') {
                            $value = explode('/', $value);
                            $value = $value[2] . '-' . $value[1] . '-' . $value[0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <= ' => $value];
                        }
                        break;

                    case 'VisaAutoDebitRecords.created':
                    case 'VisaAutoDebitRecords.duedate':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2]  . '-' . $value[1][1] . '-' .  $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] .' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'VisaAutoDebitRecords.customer_code':
                    case 'VisaAutoDebitRecords.business_id':
                    case 'VisaAutoDebitRecords.status':
                    case 'VisaAutoDebitRecords.invoice_num':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'VisaAutoDebitRecords.card_number':
                    case 'VisaAutoDebitRecords.customer_name':
                    case 'VisaAutoDebitRecords.customer_city':
                    case 'VisaAutoDebitRecords.customer_ident':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'VisaAutoDebitRecords.customer_code':
                    case 'VisaAutoDebitRecords.business_id':
                    case 'VisaAutoDebitRecords.status':
                    case 'VisaAutoDebitRecords.invoice_num':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'VisaAutoDebitRecords.card_number':
                    case 'VisaAutoDebitRecords.customer_name':
                    case 'VisaAutoDebitRecords.customer_city':
                    case 'VisaAutoDebitRecords.customer_ident':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order($order);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        return $query;
    }

    public function findRecordsFiltered(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            'VisaAutoDebitRecords.status',        //0
            'VisaAutoDebitRecords.created',       //1
            'VisaAutoDebitRecords.date_start',    //2
            'VisaAutoDebitRecords.date_end',      //3
            'VisaAutoDebitRecords.card_number',   //4
            'VisaAutoDebitRecords.customer_name', //5
            'VisaAutoDebitRecords.customer_ident',//6
            'VisaAutoDebitRecords.customer_code', //7
            'VisaAutoDebitRecords.customer_city', //8
            'VisaAutoDebitRecords.invoice_num',   //9
            'VisaAutoDebitRecords.duedate',       //10
            'VisaAutoDebitRecords.total',         //11
            'VisaAutoDebitRecords.business_id',   //12
        ];

        $columns_select = [
            'VisaAutoDebitRecords.id',
            'VisaAutoDebitRecords.status',
            'VisaAutoDebitRecords.created',
            'VisaAutoDebitRecords.date_start',
            'VisaAutoDebitRecords.date_end',
            'VisaAutoDebitRecords.card_number',
            'VisaAutoDebitRecords.customer_name',
            'VisaAutoDebitRecords.customer_code',
            'VisaAutoDebitRecords.customer_doc_type',
            'VisaAutoDebitRecords.customer_ident',
            'VisaAutoDebitRecords.customer_city',
            'VisaAutoDebitRecords.invoice_num',
            'VisaAutoDebitRecords.duedate',
            'VisaAutoDebitRecords.total',
            'VisaAutoDebitRecords.business_id',
        ];

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] =>  $o['dir']];
        }

        $query = $query->contain([
            'Customers'
        ])->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    // case 'VisaAutoDebitRecords.date_start':
                    //     $value = [];

                    //     $value[0] = trim($params['columns'][2]['search']['value']);
                    //     $value[1] = trim($params['columns'][3]['search']['value']);

                    //     $value[0] = trim($value[0]);
                    //     $value[1] = trim($value[1]);

                    //     if ($value[0] != '' && $value[1] != '') {
                    //         $value[0] = explode('/', $value[0]);
                    //         $value[1] = explode('/', $value[1]);
                    //         $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                    //         $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                    //         $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                    //     }
                    //     break;

                    case 'VisaAutoDebitRecords.date_start':
                        $value = trim($column['search']['value']);
                        if ($value != '') {
                            $value = explode('/', $value);
                            $value = $value[2] . '-' . $value[1] . '-' . $value[0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >= ' => $value];
                        }
                        break;

                    case 'VisaAutoDebitRecords.date_end':
                        $value = trim($column['search']['value']);
                        if ($value != '') {
                            $value = explode('/', $value);
                            $value = $value[2] . '-' . $value[1] . '-' . $value[0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <= ' => $value];
                        }
                        break;

                    case 'VisaAutoDebitRecords.created':
                    case 'VisaAutoDebitRecords.duedate':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2]  . '-' . $value[1][1] . '-' .  $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] .' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'VisaAutoDebitRecords.customer_code':
                    case 'VisaAutoDebitRecords.business_id':
                    case 'VisaAutoDebitRecords.status':
                    case 'VisaAutoDebitRecords.invoice_num':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'VisaAutoDebitRecords.card_number':
                    case 'VisaAutoDebitRecords.customer_name':
                    case 'VisaAutoDebitRecords.customer_city':
                    case 'VisaAutoDebitRecords.customer_ident':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'VisaAutoDebitRecords.customer_code':
                    case 'VisaAutoDebitRecords.business_id':
                    case 'VisaAutoDebitRecords.status':
                    case 'VisaAutoDebitRecords.invoice_num':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'VisaAutoDebitRecords.card_number':
                    case 'VisaAutoDebitRecords.customer_name':
                    case 'VisaAutoDebitRecords.customer_city':
                    case 'VisaAutoDebitRecords.customer_ident':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {
            foreach ($between as $b) {
                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }
}
