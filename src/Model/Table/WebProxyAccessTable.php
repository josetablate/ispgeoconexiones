<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WebProxyAccess Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Controllers
 * @property \Cake\ORM\Association\BelongsTo $Apis
 *
 * @method \App\Model\Entity\WebProxyAcces get($primaryKey, $options = [])
 * @method \App\Model\Entity\WebProxyAcces newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\WebProxyAcces[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\WebProxyAcces|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\WebProxyAcces patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\WebProxyAcces[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\WebProxyAcces findOrCreate($search, callable $callback = null)
 */
class WebProxyAccessTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('web_proxy_access');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Controllers', [
            'foreignKey' => 'controller_id',
            'joinType' => 'INNER'
        ]);
        
        $this->belongsTo('Connections', [
            'foreignKey' => 'connection_id',
            'joinType' => 'INNER'
        ]);
        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('src_address', 'create')
            ->notEmpty('src_address');

        $validator
            ->integer('dst_port')
            ->requirePresence('dst_port', 'create')
            ->notEmpty('dst_port');

        $validator
            ->allowEmpty('action');

        $validator
            ->allowEmpty('redirect_to');

        $validator
            ->allowEmpty('comment');

        $validator
            ->integer('order')
            ->allowEmpty('order');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['controller_id'], 'Controllers'));

        return $rules;
    }
}
