<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CustomersMessageTemplates Model
 *
 * @method \App\Model\Entity\CustomersMessageTemplate get($primaryKey, $options = [])
 * @method \App\Model\Entity\CustomersMessageTemplate newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CustomersMessageTemplate[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CustomersMessageTemplate|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CustomersMessageTemplate patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CustomersMessageTemplate[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CustomersMessageTemplate findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CustomersMessageTemplatesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('customers_message_templates');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('category')
            ->allowEmpty('category');

        $validator
            ->scalar('name')
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('description')
            ->allowEmpty('description');

        $validator
            ->scalar('html')
            ->allowEmpty('html');

        $validator
            ->dateTime('send_date')
            ->allowEmpty('send_date');

        $validator
            ->integer('priority')
            ->allowEmpty('priority');

        $validator
            ->boolean('portal_sync')
            ->allowEmpty('portal_sync');

        return $validator;
    }

    public function beforeSave($event, $entity, $options)
    {
        if (!$entity->isDirty('portal_sync')) {
            $entity->portal_sync = false;
        }
    }
}
