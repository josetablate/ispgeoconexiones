<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Presales Model
 *
 * @property \App\Model\Table\DebtProductsTable|\Cake\ORM\Association\BelongsTo $DebtProducts
 * @property \App\Model\Table\DebtPackagesTable|\Cake\ORM\Association\BelongsTo $DebtPackages
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Presale get($primaryKey, $options = [])
 * @method \App\Model\Entity\Presale newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Presale[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Presale|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Presale patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Presale[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Presale findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PresalesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('presales');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_code',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Debts', [
            'foreignKey' => 'presale_id'
        ]);
        $this->hasMany('ServicePending', [
            'foreignKey' => 'presale_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('comments')
            ->allowEmpty('comments');

        return $validator;
    }
}
