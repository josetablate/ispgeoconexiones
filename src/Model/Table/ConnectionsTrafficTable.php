<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ConnectionsTraffic Model
 *
 * @method \App\Model\Entity\ConnectionsTraffic get($primaryKey, $options = [])
 * @method \App\Model\Entity\ConnectionsTraffic newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ConnectionsTraffic[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ConnectionsTraffic|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ConnectionsTraffic patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ConnectionsTraffic[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ConnectionsTraffic findOrCreate($search, callable $callback = null, $options = [])
 */
class ConnectionsTrafficTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('connections_traffic');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('time')
            ->requirePresence('time', 'create')
            ->notEmpty('time');

        $validator
            ->integer('packets_up')
            ->requirePresence('packets_up', 'create')
            ->notEmpty('packets_up');

        $validator
            ->integer('bytes_up')
            ->requirePresence('bytes_up', 'create')
            ->notEmpty('bytes_up');

        $validator
            ->integer('packets_down')
            ->requirePresence('packets_down', 'create')
            ->notEmpty('packets_down');

        $validator
            ->integer('bytes_down')
            ->requirePresence('bytes_down', 'create')
            ->notEmpty('bytes_down');

        $validator
            ->integer('ip_address')
            ->requirePresence('ip_address', 'create')
            ->notEmpty('ip_address');

        $validator
            ->allowEmpty('user');

        return $validator;
    }
}
