<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Rule\IsUnique;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Filesystem\File;
use Cake\Log\Log;

/**
 * Connections Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Cities
 * @property \Cake\ORM\Association\BelongsTo $Controllers
 * @property \Cake\ORM\Association\HasMany $Instalations
 * @property \Cake\ORM\Association\HasMany $Logs
 * @property \Cake\ORM\Association\HasMany $Messages
 * @property \Cake\ORM\Association\HasMany $Tickets
 *
 * @method \App\Model\Entity\Connection get($primaryKey, $options = [])
 * @method \App\Model\Entity\Connection newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Connection[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Connection|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Connection patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Connection[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Connection findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ConnectionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('connections');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Areas', [
            'foreignKey' => 'area_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_code',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Controllers', [
            'foreignKey' => 'controller_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Services', [
            'foreignKey' => 'service_id',
            'joinType' => 'LEFT'
        ]);
        
        $this->hasMany('FirewallAddressLists', [
            'foreignKey' => 'connection_id',
            'joinType' => 'LEFT',
            'propertyName' => 'firewall_address_list',
        ]);

        $this->hasMany('WebProxyAccess', [
            'foreignKey' => 'connection_id',
            'joinType' => 'LEFT',
            'propertyName' => 'firewall_address_list',
        ]);

        $this->hasMany('Logs', [
            'foreignKey' => 'connection_id'
        ]);
       
        $this->hasMany('Tickets', [
            'foreignKey' => 'connection_id'
        ]);

        $this->hasMany('Debts', [
            'foreignKey' => 'connection_id',
            'joinType' => 'LEFT',
        ]);
        
        $this->hasMany('ConnectionsDebtsMonth', [
            'foreignKey' => 'connection_id',
            'joinType' => 'LEFT',
        ]);

        $this->hasMany('Invoices', [
            'foreignKey' => 'connection_id',
        ]);
        
        $this->belongsToMany('Labels', 
        [
            'targetForeignKey' => 'label_id',
            'foreignKey' => 'connection_id',
            'joinTable' => 'connections_labels',
            'strategy' => 'subquery'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('address', 'create')
            ->notEmpty('address');

        $validator
            ->numeric('lat')
            ->allowEmpty('lat');

        $validator
            ->numeric('lng')
            ->allowEmpty('lng');

        $validator
           ->notEmpty('ip');

        $validator
            ->integer('customer_code')
            ->requirePresence('customer_code', 'create')
            ->notEmpty('customer_code');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['ip'], 'Está IP ya existe en la base de datos'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['area_id'], 'Areas'));
        $rules->add($rules->existsIn(['controller_id'], 'Controllers'));

        return $rules;
    }

    public function findServerSideData(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;
        $columns = [
            'Connections.created',         //0
            'customer_code',               //1
            'Connections.etiquetas',       //2
            'Customers.name',              //3
            'Customers.ident',             //4
            'Connections.address',         //5
            'Connections.area_id',         //6
            'Customers.phone',             //7
            'Connections.controller_id',   //8
            'Connections.service_id',      //9
            'Connections.ip',              //10
            'Connections.mac',             //11
            'Connections.modified',        //12
            'Connections.user_id',         //13
            'Connections.enabled',         //14
            'Connections.comments',        //15
            'Connections.clave_wifi',      //16
            'Connections.ports',           //17
            'Connections.ip_alt',          //18
            'Connections.ing_traffic',     //19
            'Connections.before_ip',       //20
            'Connections.diff',            //21
            'Connections.queue_name',      //22
            'Connections.pppoe_name',      //23
            'Connections.pppoe_pass',      //24
            'Connections.blocking_date',   //25
            'Connections.enabling_date'
        ];

        $columns_select = [
            'Connections.created',
            'customer_code',
            'Customers.name',
            'Customers.ident',
            'Connections.address',
            'Connections.area_id',
            'Customers.phone',
            'Connections.controller_id',
            'Connections.service_id',
            'Connections.ip',
            'Connections.mac',
            'Connections.modified',
            'Connections.user_id',
            'Connections.enabled',
            'Connections.comments',
            'Connections.clave_wifi',
            'Connections.ports',
            'Connections.ip_alt',
            'Connections.ing_traffic',
            'Connections.before_ip',
            'Connections.diff',
            'Controllers.template',
            'Customers.doc_type',
            'Areas.id',
            'Connections.id',
            'Connections.deleted',
            'Areas.name',
            'Controllers.name',
            'Services.name',
            'Users.username',
            'Connections.queue_name',
            'Connections.pppoe_name',
            'Connections.pppoe_pass',
            'Connections.blocking_date',
            'Connections.enabling_date'
        ];

        //paginacion

        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach($params['order'] as $o) {
            $order += [$columns[$o['column']] =>  $o['dir']];
        }

        $query = $query->contain([
            'Customers',
            'Areas',
            'Controllers',
            'Services',
            'Users',
            'Labels'
        ])->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Connections.created': //date
                    case 'Connections.modified':
                    case 'Connections.blocking_date':
                    case 'Connections.enabling_date':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Connections.etiquetas':

                        $labels_ids_search = []; 
                        $connections_ids_labels = [];
                        $connection_labels = [];

                        $labels_ids_search = explode(',', $column['search']['value']);

                        if (in_array(0, $labels_ids_search)) { //buscada de sin etiquetas

                        	$tableConnectionsLabels = TableRegistry::get('ConnectionsLabels');
                        	$connections_ids_labels = $tableConnectionsLabels->find('list')->toArray();

                        	if (count($connections_ids_labels) > 0) {
                        		$where += ['Connections.id NOT IN' => $connections_ids_labels];
                        	}
                        } else { //con etiquetas

                        	$tableConnectionsLabels = TableRegistry::get('ConnectionsLabels');
                        	$tableConnectionsLabels = $tableConnectionsLabels->find()
                        		->where(['label_id IN' => $labels_ids_search]); 

                        	foreach ($tableConnectionsLabels as $tcl) {
                        		if (!array_key_exists($tcl->connection_id, $connection_labels)){
                        			$connection_labels[$tcl->connection_id] = [];
                        		}
                        		$connection_labels[$tcl->connection_id][] =  $tcl->label_id;
                        	}

                        	foreach ($connection_labels as $id => $labels) {

                        		if (!array_diff($labels_ids_search, $labels)) {
                        			$connections_ids_labels[] = $id;
                        		}
                        	}

                        	if (count($connections_ids_labels) > 0) {
                        		$where += ['Connections.id IN' => $connections_ids_labels];
                        	} else {
                                $where += ['Connections.id IN' => -1];
                            }
                        }
                        break;

                    case 'customer_code':   //integer
                    case 'Customers.ident':                     
                    case 'Connections.service_id':
                    case 'Connections.area_id':
                    case 'Connections.controller_id':
                    case 'Connections.user_id':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Connections.enabled':
                    case 'Connections.diff':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? true : false];
                        break;

                    case 'Connections.ip': 
                    case 'Connections.before_ip': 

                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;

                    case 'Connections.mac':                    
                        
                        $mac = $column['search']['value'];
                        
                        $mac = str_replace(':', '', $mac);
                        $mac = str_replace(' ', '', $mac);
                        $mac = str_replace('-', '', $mac);
                        
                        $where += [$columns[$index] . ' LIKE ' => '%' . $mac . '%'];
                        break;

                    case 'Customers.name': 
                    case 'Connections.address': 
                    case 'Controllers.name': 
                    case 'Services.name': 
                    case 'Areas.name':
                    case 'Users.username':
                    case 'Connections.comments':
                    case 'Connections.queue_name':
                    case 'Connections.pppoe_name':
                    case 'Connections.pppoe_pass':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);
 
        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {
                    case 'customer_code':

                        if(is_numeric($params['search']['value'])){
                            $orWhere += [$columns[$index] => $params['search']['value']];
                        }

                        break;

                    case 'Customers.ident':                   
                        
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Connections.ip': 
                    case 'Connections.before_ip':

                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;

                    case 'Connections.mac':                     

                        $mac = $params['search']['value'];

                        $mac = str_replace(':', '', $mac);
                        $mac = str_replace(' ', '', $mac);
                        $mac = str_replace('-', '', $mac);

                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $mac . '%'];

                        break;

                    case 'Customers.name': 
                    case 'Connections.address': 
                    case 'Controllers.name': 
                    case 'Services.name': 
                    case 'Areas.name':
                    case 'Users.username': 
                    case 'Connections.comments':
                    case 'Connections.queue_name':
                    case 'Connections.pppoe_name':
                    case 'Connections.pppoe_pass':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $customeOrder = false;

        if (array_key_exists('Connections.ip', $order)) {
           $query->order(['INET_ATON(Connections.ip)'  => $order['Connections.ip']]);
           $customeOrder = true; 
        }

        if (array_key_exists('Connections.before_ip', $order)) {
            $query->order(['INET_ATON(Connections.before_ip)'  => $order['Connections.before_ip']]);
            $customeOrder = true; 
        }

        if (!$customeOrder) {
            $query->order($order);
        }

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        return $query;
    }

    public function findRecordsFiltered(Query $query, array $options)
    {
        $params = $options['params'];
       
        $where = [];
        $between = [];
        $orWhere = [];
        $columns = [
            'Connections.created',         //0
            'customer_code',               //1
            'Connections.etiquetas',       //2
            'Customers.name',              //3
            'Customers.ident',             //4
            'Connections.address',         //5
            'Connections.area_id',         //6
            'Customers.phone',             //7
            'Connections.controller_id',   //8
            'Connections.service_id',      //9
            'Connections.ip',              //10
            'Connections.mac',             //11
            'Connections.modified',        //12
            'Connections.user_id',         //13
            'Connections.enabled',         //14
            'Connections.comments',        //15
            'Connections.clave_wifi',      //16
            'Connections.ports',           //17
            'Connections.ip_alt',          //18
            'Connections.ing_traffic',     //19
            'Connections.before_ip',       //20
            'Connections.diff',            //21
            'Connections.queue_name',      //22
            'Connections.pppoe_name',      //23
            'Connections.pppoe_pass',      //24
            'Connections.blocking_date',   //25
            'Connections.enabling_date'
        ];

        $columns_select = [
            'Connections.created',
            'customer_code',
            'Customers.name',
            'Customers.ident',
            'Connections.address',
            'Connections.area_id',
            'Customers.phone',
            'Connections.controller_id',
            'Connections.service_id',
            'Connections.ip',
            'Connections.mac',
            'Connections.modified',
            'Connections.user_id',
            'Connections.enabled',
            'Connections.comments',
            'Connections.clave_wifi',
            'Connections.ports',
            'Connections.ip_alt',
            'Connections.ing_traffic',
            'Connections.before_ip',
            'Connections.diff',
            'Controllers.template',
            'Customers.doc_type',
            'Areas.id',
            'Connections.id',
            'Connections.deleted',
            'Areas.name',
            'Controllers.name',
            'Services.name',
            'Users.username',
            'Connections.queue_name',
            'Connections.pppoe_name',
            'Connections.pppoe_pass',
            'Connections.blocking_date',
            'Connections.enabling_date'
        ];

        $query = $query->contain([
            'Customers',
            'Areas',
            'Controllers',
            'Services',
            'Users',
            'Labels'
        ])->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Connections.created': //date
                    case 'Connections.modified':
                    case 'Connections.blocking_date':
                    case 'Connections.enabling_date':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] .'-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index] , 'from' => $value[0], 'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Connections.etiquetas':

                        $labels_ids_search = []; 
                        $connections_ids_labels = [];
                        $connection_labels = [];

                        $labels_ids_search = explode(',', $column['search']['value']);

                        if (in_array(0, $labels_ids_search)) { //buscada de sin etiquetas

                        	$tableConnectionsLabels = TableRegistry::get('ConnectionsLabels');
                        	$connections_ids_labels = $tableConnectionsLabels->find('list')->toArray();

                        	if (count($connections_ids_labels) > 0) {
                        		$where += ['Connections.id NOT IN' => $connections_ids_labels];
                        	}
                        } else { //con etiquetas

                        	$tableConnectionsLabels = TableRegistry::get('ConnectionsLabels');
                        	$tableConnectionsLabels = $tableConnectionsLabels->find()
                        		->where(['label_id IN' => $labels_ids_search]); 

                        	foreach ($tableConnectionsLabels as $tcl) {
                        		if (!array_key_exists($tcl->connection_id, $connection_labels)){
                        			$connection_labels[$tcl->connection_id] = [];
                        		}
                        		$connection_labels[$tcl->connection_id][] =  $tcl->label_id;
                        	}

                        	foreach ($connection_labels as $id => $labels) {
                        		
                        		if (!array_diff($labels_ids_search, $labels)) {
                        			$connections_ids_labels[] = $id;
                        		}
                        	}

                        	if (count($connections_ids_labels) > 0) {
                        	 	$where += ['Connections.id IN' => $connections_ids_labels];
                        	} else {
                                $where += ['Connections.id IN' => -1];
                            }
                        }
                        break;

                    case 'customer_code':   //integer

                        if(is_numeric($column['search']['value'])){
                            $where += [$columns[$index] => $column['search']['value']];                      
                        }

                        break;

                    case 'Customers.ident':                     
                    case 'Connections.service_id':
                    case 'Connections.area_id':
                    case 'Connections.controller_id':
                    case 'Connections.user_id':

                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Connections.ip': 
                    case 'Connections.before_ip':

                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;

                    case 'Connections.mac':                   

                        $mac = $column['search']['value'];

                        $mac = str_replace(':', '', $mac);
                        $mac = str_replace(' ', '', $mac);
                        $mac = str_replace('-', '', $mac);

                        $where += [$columns[$index] . ' LIKE ' => '%' . $mac . '%'];
                        break;

                    case 'Connections.enabled':
                    case 'Connections.diff':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? true : false];
                        break;

                    case 'Customers.name': 
                    case 'Connections.address': 
                    case 'Controllers.name': 
                    case 'Services.name': 
                    case 'Areas.name': 
                    case 'Connections.comments':
                    case 'Users.username': 
                    case 'Connections.queue_name':
                    case 'Connections.pppoe_name':
                    case 'Connections.pppoe_pass':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'customer_code':

                        if(is_numeric($params['search']['value'])){
                            $orWhere += [$columns[$index] => $params['search']['value']];
                        }
                        break;
                        
                    case 'Customers.ident':                     
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Connections.ip': 
                    case 'Connections.before_ip':

                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;

                    case 'Connections.mac':                    

                        $mac = $params['search']['value'];

                        $mac = str_replace(':', '', $mac);
                        $mac = str_replace(' ', '', $mac);
                        $mac = str_replace('-', '', $mac);

                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $mac . '%'];
                        break;

                    case 'Customers.name': 
                    case 'Connections.address': 
                    case 'Controllers.name': 
                    case 'Services.name': 
                    case 'Areas.name':
                    case 'Users.username':
                    case 'Connections.comments':
                    case 'Connections.queue_name':
                    case 'Connections.pppoe_name':
                    case 'Connections.pppoe_pass':

                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {
            foreach ($between as $b) {
                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return  $query->count();
    }

    public function findServerSideDataAdministrative(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            'Connections.created',                 //0
            'Connections.service_id',              //1
            'Connections.controller_id',           //2
            'etiquetas',                           //3
            'Connections.address',                 //4
            'Connections.area_id',                 //5
            'customer_code',                       //6
            'Customers.name',                      //7
            'Customers.ident',                     //8
            'Customers.phone',                     //9
            'Customers.email',                     //10
            'Customers.daydue',                    //11
            'Connections.invoices_no_paid',        //12
            'Connections.debt_month',              //13
            'Connections.discount_description',    //14
            'Connections.force_debt',              //15
            'Connections.cobrodigital_card',       //16
            'Connections.cobrodigital_auto_debit', //17
            'Connections.chubut_auto_debit',       //18
            'Connections.chubut_auto_debit',       //19
            'Connections.comments',                //20
            'Connections.user_id',                 //21
            'Connections.enabled',                 //22
            'Connections.ip',                      //23
            'Connections.blocking_date',           //24
            'Connections.enabling_date'            //25
        ];

        $columns_select = [
            'Connections.ip',
            'Connections.created',
            'Connections.service_id',
            'Connections.controller_id',
            'Connections.address',
            'Connections.area_id',
            'customer_code',
            'Customers.name',
            'Customers.ident',
            'Customers.phone',
            'Customers.phone_alt',
            'Customers.email',
            'Customers.daydue',
            'Connections.invoices_no_paid',
            'Connections.debt_month',
            'Connections.force_debt',
            'Connections.cobrodigital_card',
            'Connections.cobrodigital_auto_debit',
            'Connections.chubut_auto_debit',
            'Connections.payu_card',
            'Connections.comments',
            'Connections.user_id',
            'Customers.doc_type',
            'Connections.enabled',
            'Connections.id',
            'Connections.discount_total_month',
            'Connections.discount_month',
            'Connections.discount_value',
            'Connections.discount_description',
            'Connections.discount_code',
            'Connections.discount_alicuot',
            'Connections.discount_always',
            'Customers.billing_for_service',
            'Services.name',
            'Controllers.name',
            'Areas.name',
            'Users.username',
            'Connections.blocking_date',
            'Connections.enabling_date'
        ];

        //paginacion

        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] => $o['dir']];
        }

        $query = $query->contain([
            'Customers',
            'Areas',
            'Services',
            'Users',
            'Labels',
            'Controllers',
        ])->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Connections.created': //date
                    case 'Connections.blocking_date':
                    case 'Connections.enabling_date':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'etiquetas':
                        $labels_ids_search = []; 
                        $connections_ids_labels = [];
                        $connection_labels = [];

                        $labels_ids_search = explode(',', $column['search']['value']);

                        if (in_array(0, $labels_ids_search)) { //buscada de sin etiquetas

                        	$tableConnectionsLabels = TableRegistry::get('ConnectionsLabels');
                        	$connections_ids_labels = $tableConnectionsLabels->find('list')->toArray();

                        	if (count($connections_ids_labels) > 0) {
                        		$where += ['Connections.id NOT IN' => $connections_ids_labels];
                        	}
                        } else { //con etiquetas

                        	$tableConnectionsLabels = TableRegistry::get('ConnectionsLabels');
                        	$tableConnectionsLabels = $tableConnectionsLabels->find()
                        		->where(['label_id IN' => $labels_ids_search]); 

                        	foreach ($tableConnectionsLabels as $tcl) {
                        		if (!array_key_exists($tcl->connection_id, $connection_labels)) {
                        			$connection_labels[$tcl->connection_id] = [];
                        		}
                        		$connection_labels[$tcl->connection_id][] =  $tcl->label_id;
                        	}

                        	foreach ($connection_labels as $id => $labels) {
                        		
                        		if (!array_diff($labels_ids_search, $labels)) {
                        			$connections_ids_labels[] = $id;
                        		}
                        	}

                        	if (count($connections_ids_labels) > 0) {
                        		$where += ['Connections.id IN' => $connections_ids_labels];
                        	} else {
                                $where += ['Connections.id IN' => -1];
                            }
                        }
                        break;

                    case 'Connections.debt_month': 
                    case 'Connections.invoices_no_paid':
                  
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'customer_code':   //integer

                        if(is_numeric($column['search']['value'])){
                            $where += [$columns[$index] => $column['search']['value']];
                        }
                        break;

                    case 'Customers.ident':
                    case 'Connections.area_id':
                    case 'Connections.service_id':
                    case 'Connections.controller_id':
                    case 'Connections.user_id':                  
                    case 'Customers.daydue':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Connections.enabled':
                    case 'Connections.force_debt':
                    case 'Connections.cobrodigital_card':
                    case 'Connections.cobrodigital_auto_debit':
                    case 'Connections.chubut_auto_debit':
                    case 'Connections.payu_card':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? true : false];
                        break;

                    case 'Customers.name':
                    case 'Controllers.name':
                    case 'Connections.address':
                    case 'Services.name':
                    case 'Areas.name':
                    case 'Connections.comments':
                    case 'Descuento':
                    case 'Customers.phone':
                    case 'Customers.phone_alt':
                    case 'Users.username':
                    case 'Customers.email':
                    case 'Connections.discount_description':
                    case 'Connections.ip': 
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {
                    case 'customer_code':

                        if(is_numeric($params['search']['value'])){
                            $orWhere += [$columns[$index] => $params['search']['value']];
                        }
                        break;

                    case 'Customers.ident':                    
                    case 'Customers.daydue':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Customers.name':
                    case 'Controllers.name':
                    case 'Connections.address':
                    case 'Services.name':
                    case 'Areas.name':
                    case 'Connections.comments':
                    case 'Users.username':
                    case 'Customers.phone':
                    case 'Customers.phone_alt':
                    case 'Customers.email':
                    case 'Connections.discount_description':
                    case 'Connections.ip':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order($order);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        return $query;
    }

    public function findRecordsFilteredAdministrative(Query $query, array $options)
    {
        $params = $options['params'];

        $where = [];
        $between = [];
        $orWhere = [];

        $columns = [
            'Connections.created',                 //0
            'Connections.service_id',              //1
            'Connections.controller_id',           //2
            'etiquetas',                           //3
            'Connections.address',                 //4
            'Connections.area_id',                 //5
            'customer_code',                       //6
            'Customers.name',                      //7
            'Customers.ident',                     //8
            'Customers.phone',                     //9
            'Customers.email',                     //10
            'Customers.daydue',                    //11
            'Connections.invoices_no_paid',        //12
            'Connections.debt_month',              //13
            'Connections.discount_description',    //14
            'Connections.force_debt',              //15
            'Connections.cobrodigital_card',       //16
            'Connections.cobrodigital_auto_debit', //17
            'Connections.chubut_auto_debit',       //18
            'Connections.chubut_auto_debit',       //19
            'Connections.comments',                //20
            'Connections.user_id',                 //21
            'Connections.enabled',                 //22
            'Connections.ip',                      //23
            'Connections.blocking_date',           //24
            'Connections.enabling_date'            //25
        ];

        $columns_select = [
            'Connections.ip',
            'Connections.created',
            'Connections.service_id',
            'Connections.controller_id',
            'Connections.address',
            'Connections.area_id',
            'customer_code',
            'Customers.name',
            'Customers.ident',
            'Customers.phone',
            'Customers.phone_alt',
            'Customers.email',
            'Customers.daydue',
            'Connections.invoices_no_paid',
            'Connections.debt_month',
            'Connections.force_debt',
            'Connections.cobrodigital_card',
            'Connections.cobrodigital_auto_debit',
            'Connections.chubut_auto_debit',
            'Connections.payu_card',
            'Connections.comments',
            'Connections.user_id',
            'Customers.doc_type',
            'Connections.enabled',
            'Connections.id',
            'Connections.discount_total_month',
            'Connections.discount_month',
            'Connections.discount_value',
            'Connections.discount_description',
            'Connections.discount_code',
            'Connections.discount_alicuot',
            'Connections.discount_always',
            'Customers.billing_for_service',
            'Services.name',
            'Controllers.name',
            'Areas.name',
            'Users.username',
            'Connections.blocking_date',
            'Connections.enabling_date'
        ];

        $query = $query->contain([
            'Customers',
            'Areas',
            'Services',
            'Users',
            'Labels',
            'Controllers',
        ])->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        $labels_ids = [];

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Connections.created': //date
                    case 'Connections.blocking_date':
                    case 'Connections.enabling_date':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0] ,  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'etiquetas':
                        $labels_ids_search = []; 
                        $connections_ids_labels = [];
                        $connection_labels = [];

                        $labels_ids_search = explode(',', $column['search']['value']);

                        if (in_array(0, $labels_ids_search)) { //buscada de sin etiquetas

                        	$tableConnectionsLabels = TableRegistry::get('ConnectionsLabels');
                        	$connections_ids_labels = $tableConnectionsLabels->find('list')->toArray();

                        	if (count($connections_ids_labels) > 0) {
                        		$where += ['Connections.id NOT IN' => $connections_ids_labels];
                        	}
                        } else { //con etiquetas

                        	$tableConnectionsLabels = TableRegistry::get('ConnectionsLabels');
                        	$tableConnectionsLabels = $tableConnectionsLabels->find()
                        		->where(['label_id IN' => $labels_ids_search]); 

                        	foreach ($tableConnectionsLabels as $tcl) {
                        		if (!array_key_exists($tcl->connection_id, $connection_labels)) {
                        			$connection_labels[$tcl->connection_id] = [];
                        		}
                        		$connection_labels[$tcl->connection_id][] =  $tcl->label_id;
                        	}

                        	foreach ($connection_labels as $id => $labels) {

                        		if (!array_diff($labels_ids_search, $labels)) {
                        			$connections_ids_labels[] = $id;
                        		}
                        	}

                        	if (count($connections_ids_labels) > 0) {
                        		$where += ['Connections.id IN' => $connections_ids_labels];
                        	} else {
                                $where += ['code IN' => -1];
                            }
                        }
                        break;

                    case 'Connections.debt_month': 
                    case 'Connections.invoices_no_paid':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'customer_code':   //integer

                        if(is_numeric($column['search']['value'])){
                            $where += [$columns[$index] => $column['search']['value']];
                        }
                        break;

                    case 'Customers.ident':
                    case 'Connections.area_id':
                    case 'Connections.service_id':
                    case 'Connections.controller_id':
                    case 'Connections.user_id':               
                    case 'Customers.daydue':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Connections.enabled':
                    case 'Connections.force_debt':
                    case 'Connections.cobrodigital_card':
                    case 'Connections.cobrodigital_auto_debit':
                    case 'Connections.chubut_auto_debit':
                    case 'Connections.payu_card':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? true : false];
                        break;

                    case 'Customers.name': 
                    case 'Connections.address': 
                    case 'Controllers.name': 
                    case 'Services.name': 
                    case 'Areas.name':
                    case 'Connections.comments':
                    case 'Descuento':
                    case 'Customers.phone':
                    case 'Customers.phone_alt':
                    case 'Users.username':
                    case 'Customers.email':
                    case 'Connections.discount_description':
                    case 'Connections.ip': 
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {
                    case 'customer_code':

                        if(is_numeric($params['search']['value'])){
                            $orWhere += [$columns[$index] => $params['search']['value']];
                        }
                        break;

                    case 'Customers.ident':                
                    case 'Customers.daydue':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Customers.name':
                    case 'Connections.address':
                    case 'Controllers.name':
                    case 'Services.name':
                    case 'Areas.name':
                    case 'Connections.comments':
                    case 'Users.username':
                    case 'Customers.phone':
                    case 'Customers.phone_alt':
                    case 'Customers.email':
                    case 'Connections.discount_description':
                    case 'Connections.ip': 
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {
            foreach ($between as $b) {
                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }
        return $query->count();
    }

    public function beforeSave($event, $entity, $options)
    {
        if (!$entity->isDirty('portal_sync')) {
            $entity->portal_sync = false;
        }
    }

    public function afterSave($event, $entity, $options)
    {
        $ok = false;

    	$detail = '';

    	if (!$entity->isNew() && !$entity->isDirty('diff')) {

    		$action = 'Edición de Conexión';

    		$dirtyFields = $entity->getDirty();

    		$dirtyFields = array_diff($dirtyFields, ["modified"]);

    		foreach ($dirtyFields as $dirtyField) {

    			switch ($dirtyField) {

    				case 'address': 
    					$detail .= 'Domicilio: ' . $entity->getOriginal('address') . ' => ' . $entity->address . PHP_EOL; 
    					$ok = true;
    					break;
    				case 'lat': 
    					$detail .= 'Lat: ' . $entity->getOriginal('lat') . ' => ' . $entity->lat . PHP_EOL; 
    					$ok = true;
    					break;
    				case 'lng': 
    					$detail .= 'Lng: ' . $entity->getOriginal('lng') . ' => ' . $entity->lng . PHP_EOL; 
    					$ok = true;
    					break;
    				case 'enabled':
    				    if ($entity->enabled) $action = 'Habilitación de Conexión'; else $action = 'Bloqueo de Conexión';
    					$ok = true;
    					break;
					case 'customer_code':
    				    $detail .= 'Cód. de Cliente: ' . $entity->getOriginal('customer_code') . ' => ' . $entity->customer_code . PHP_EOL; 
    					$ok = true;
    					break;
    				case 'area_id': 
    				    $table = TableRegistry::get('Areas');
    				    $area_original = $table->find()
                            ->where(['id' => $entity->getOriginal('area_id')])
                            ->first();
                        $area_new = $table->find()
                            ->where(['id' => $entity->area_id])
                            ->first();
    					$detail .= 'Área: ' . $area_original->name . ' => ' . $area_new->name . PHP_EOL; 
    					$ok = true;
    					break;
    			    case 'controller_id': 
    				    $table = TableRegistry::get('Controllers');
    				    $original = $table->find()
                            ->where(['id' => $entity->getOriginal('controller_id')])
                            ->first();
                        $new = $table->find()
                            ->where(['id' => $entity->controller_id])
                            ->first();
    					$detail .= 'Controlador: ' . $original->name . ' => ' . $new->name . PHP_EOL; 
    					$ok = true;
    					break;
    				case 'service_id': 
    				    $table = TableRegistry::get('Services');
    				    $original = $table->find()
                            ->where(['id' => $entity->getOriginal('service_id')])
                            ->first();
                        $new = $table->find()
                            ->where(['id' => $entity->service_id])
                            ->first();
    					$detail .= 'Servicio: ' . $original->name . ' => ' . $new->name . PHP_EOL; 
    					$ok = true;
    					break;
    				case 'ip': 
    					$detail .= 'IP: ' . $entity->getOriginal('ip') . ' => ' . $entity->ip . PHP_EOL; 
    					break;
    				case 'comments': 
    					$detail .= 'Comentario: ' . $entity->getOriginal('comments') . ' => ' . $entity->comments . PHP_EOL; 
    					$ok = true;
    					break;
    				case 'mac': 
    					$detail .= 'MAC: ' . $entity->getOriginal('mac') . ' => ' . $entity->mac . PHP_EOL; 
    					$ok = true;
    					break;
					case 'force_debt': 
    					$detail .= 'Generar Deuda: ' . ($entity->getOriginal('force_debt') ? 'Si' : 'No' ) . ' => ' . ($entity->force_debt ? 'Si' : 'No' ) . PHP_EOL; 
    					$ok = true;
    					break;
					case 'discount_total_month': 
    					$detail .= 'Descuento: total de meses: ' . $entity->getOriginal('discount_total_month') . ' => ' . $entity->discount_total_month . PHP_EOL; 
    					$ok = true;
    					break;
					case 'discount_month': 
    					$detail .= 'Descuento: meses restantes: ' . $entity->getOriginal('discount_month') . ' => ' . $entity->discount_month . PHP_EOL; 
    					$ok = true;
    					break;
					case 'discount_value': 
    					$detail .= 'Descuento: Porcentaje: ' . $entity->getOriginal('discount_value') . ' => ' . $entity->discount_value . PHP_EOL; 
    					$ok = true;
    					break;
					case 'discount_description': 
    					$detail .= 'Descuento: descripción: ' . $entity->getOriginal('discount_description') . ' => ' . $entity->discount_description . PHP_EOL; 
    					$ok = true;
    					break;
					case 'discount_code': 
    					$detail .= 'Descuento: código: ' . $entity->getOriginal('discount_code') . ' => ' . $entity->discount_code . PHP_EOL; 
    					$ok = true;
    					break;
					case 'discount_alicuot': 
    					$detail .= 'Descuento: alícuota: ' . $entity->getOriginal('discount_alicuot') . ' => ' . $entity->discount_alicuot . PHP_EOL; 
    					$ok = true;
    					break;
					case 'discount_always': 
    					$detail .= 'Descuento: alícuota: ' . ($entity->getOriginal('discount_always') ? 'Si' : 'No' ) . ' => ' . ($entity->discount_always ? 'Si' : 'No' ) . PHP_EOL;
    					$ok = true;
    					break;
					case 'queue_name': 
    					$detail .= 'Queue name: ' . $entity->getOriginal('queue_name') . ' => ' . $entity->queue_name . PHP_EOL; 
    					$ok = true;
    					break;
					case 'pppoe_name': 
    					$detail .= 'PPPoE name: ' . $entity->getOriginal('pppoe_name') . ' => ' . $entity->pppoe_name . PHP_EOL; 
    					$ok = true;
    					break;
					case 'pppoe_pass': 
    					$detail .= 'PPPoE pass: ' . $entity->getOriginal('pppoe_pass') . ' => ' . $entity->pppoe_pass . PHP_EOL; 
    					$ok = true;
    					break;
					case 'debt_period_generated': 
    					$detail .= 'Período de deuda generada: ' . $entity->getOriginal('debt_period_generated') . ' => ' . $entity->debt_period_generated . PHP_EOL; 
    					$ok = true;
    					break;
					case 'cobrodigital_card': 
    					$detail .= 'Utiliza tarjeta de Cobro Digital: ' . ($entity->getOriginal('cobrodigital_card') ? 'Si' : 'No' ) . ' => ' . ($entity->cobrodigital_card ? 'Si' : 'No' ) . PHP_EOL;
    					$ok = true;
    					break;
					case 'cobrodigital_auto_debit': 
    					$detail .= 'Utiliza débito automático Cobro Digital: ' . ($entity->getOriginal('cobrodigital_auto_debit') ? 'Si' : 'No' ) . ' => ' . ($entity->cobrodigital_auto_debit ? 'Si' : 'No' ) . PHP_EOL;
    					$ok = true;
    					break;
					case 'chubut_auto_debit': 
    					$detail .= 'Utiliza débito automático Banco Chubut: ' . ($entity->getOriginal('chubut_auto_debit') ? 'Si' : 'No' ) . ' => ' . ($entity->chubut_auto_debit ? 'Si' : 'No' ) . PHP_EOL;
    					$ok = true;
    					break;
					case 'payu_card': 
    					$detail .= 'Utiliza tarjeta de PayU: ' . ($entity->getOriginal('payu_card') ? 'Si' : 'No' ) . ' => ' . ($entity->payu_card ? 'Si' : 'No' ) . PHP_EOL;
    					$ok = true;
    					break;
					case 'visa_auto_debit': 
    					$detail .= 'Utiliza débito automático de Visa: ' . ($entity->getOriginal('visa_auto_debit') ? 'Si' : 'No' ) . ' => ' . ($entity->visa_auto_debit ? 'Si' : 'No' ) . PHP_EOL;
    					$ok = true;
    					break;
					case 'mastercard_auto_debit': 
    					$detail .= 'Utiliza débito automático de MasterCard: ' . ($entity->getOriginal('mastercard_auto_debit') ? 'Si' : 'No' ) . ' => ' . ($entity->mastercard_auto_debit ? 'Si' : 'No' ) . PHP_EOL;
    					$ok = true;
    					break;
					case 'cuentadigital': 
    					$detail .= 'Utiliza código abierto de Cuenta Digital: ' . ($entity->getOriginal('cuentadigital') ? 'Si' : 'No' ) . ' => ' . ($entity->cuentadigital ? 'Si' : 'No' ) . PHP_EOL;
    					$ok = true;
    					break;
					case 'debt_month': 
    					$detail .= 'Deuda Mes: ' . $entity->getOriginal('debt_month') . ' => ' . $entity->debt_month . PHP_EOL; 
    					$ok = true;
    					break;
					case 'debt_total': 
    					$detail .= 'Deuda total: ' . $entity->getOriginal('debt_total') . ' => ' . $entity->debt_total . PHP_EOL; 
    					$ok = true;
    					break;
					case 'blocking_date':
					    $fecha_bloqueo = "";
					    if ($entity->getOriginal('blocking_date')) {
					        $fecha_bloqueo = $entity->getOriginal('blocking_date')->format('d/m/Y');
					    }
					    $nueva_fecha_bloqueo = "";
					    if ($entity->blocking_date) {
					        $nueva_fecha_bloqueo = $entity->blocking_date->format('d/m/Y');
					    }
    					$detail .= 'Fecha de bloqueo: ' . $fecha_bloqueo . ' => ' . $nueva_fecha_bloqueo . PHP_EOL; 
    					$ok = true;
    					break;
					case 'enabling_date':
					    $fecha_habilitación = "";
					    if ($entity->getOriginal('enabling_date')) {
					        $fecha_habilitación = $entity->getOriginal('enabling_date')->format('d/m/Y');
					    }
					    $nueva_fecha_habilitación = "";
					    if ($entity->enabling_date) {
					        $nueva_fecha_habilitación = $entity->enabling_date->format('d/m/Y');
					    }
    					$detail .= 'Fecha de habilitación: ' . $fecha_habilitación . ' => ' . $nueva_fecha_habilitación . PHP_EOL; 
    					$ok = true;
    					break;
    		   }
    		}

    	} else if ($entity->isNew()) {

    		$action = 'Creación de Conexión';

			$detail = '';

		    $table = TableRegistry::get('Controllers');
            $new = $table->find()
                ->where(['id' => $entity->controller_id])
                ->first();
			$detail .= 'Controlador: ' . $new->name . PHP_EOL; 

		    $table = TableRegistry::get('Services');
            $new = $table->find()
                ->where(['id' => $entity->service_id])
                ->first();
			$detail .= 'Servicio: ' . $new->name . PHP_EOL; 

			$detail .= 'IP: ' . $entity->ip . PHP_EOL; 

			$ok = true;
    	}

    	if ($ok) {

    	    if (!isset($_SESSION['Auth']['User']['id'])) {
    	        $user_id = 100;
    	    } else {
    	        $user_id = $_SESSION['Auth']['User']['id'];
    	    }

    	    $actionLog = TableRegistry::get('ActionLog');
        	$query = $actionLog->query();
        	$query->insert([
        		'created', 
        		'detail',
        		'user_id',
        		'action',
        		'customer_code'
        	])
        	->values([
        		'created' => Time::now(), 
        		'detail' => $detail,
        		'user_id' => $user_id,
        		'action' => $action,
        		'customer_code' => $entity->customer_code
        	])
        	->execute();
    	}
    }

    public function afterDelete($event, $entity, $options)
    {
//         $detail = '';

// 	    $table = TableRegistry::get('Controllers');
//         $new = $table->find()
//             ->where(['id' => $entity->controller_id])
//             ->first();
// 		$detail .= 'Controlador: '. $new->name . PHP_EOL; 

// 	    $table = TableRegistry::get('Services');
//         $new = $table->find()
//             ->where(['id' => $entity->service_id])
//             ->first();
// 		$detail .= 'Servicio: ' . $new->name . PHP_EOL; 

// 		$detail .= 'IP: ' . $entity->ip . PHP_EOL; 

//         $actionLog = TableRegistry::get('ActionLog');

//     	$query = $actionLog->query();
//     	$query->insert([
//     		'created', 
//     		'detail',
//     		'user_id',
//     		'action',
//     		'customer_code'
//     	])
//     	->values([
//     		'created' => Time::now(), 
//     		'detail' => $detail,
//     		'user_id' => $_SESSION['Auth']['User']['id'],
//     		'action' => 'Eliminación de Conexión',
//     		'customer_code' => $entity->customer_code
//     	])
//     	->execute();
    }
}
