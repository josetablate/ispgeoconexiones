<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RealTimeSpeed Model
 *
 * @property \App\Model\Table\ConnectionsTable|\Cake\ORM\Association\BelongsTo $Connections
 *
 * @method \App\Model\Entity\RealTimeSpeed get($primaryKey, $options = [])
 * @method \App\Model\Entity\RealTimeSpeed newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\RealTimeSpeed[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RealTimeSpeed|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RealTimeSpeed|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RealTimeSpeed patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RealTimeSpeed[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\RealTimeSpeed findOrCreate($search, callable $callback = null, $options = [])
 */
class RealTimeSpeedTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('real_time_speed');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Connections', [
            'foreignKey' => 'connection_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->decimal('up')
            ->allowEmpty('up');

        $validator
            ->decimal('down')
            ->allowEmpty('down');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['connection_id'], 'Connections'));

        return $rules;
    }
}
