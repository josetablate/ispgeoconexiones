<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Log\Log;

/**
 * Payments Model
 *
 * @property \Cake\ORM\Association\BelongsTo $CashEntities
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $PaymentMethods
 * @property \Cake\ORM\Association\BelongsTo $Receipts
 *
 * @method \App\Model\Entity\Payment get($primaryKey, $options = [])
 * @method \App\Model\Entity\Payment newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Payment[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Payment|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Payment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Payment[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Payment findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PaymentsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('payments');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_code',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('CashEntities', [
            'foreignKey' => 'cash_entity_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Receipts', [
            'foreignKey' => 'receipt_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('concept', 'create')
            ->notEmpty('concept');

        $validator
            ->decimal('import')
            ->requirePresence('import', 'create')
            ->notEmpty('import');

        $validator
            ->integer('customer_code')
            ->requirePresence('customer_code', 'create')
            ->notEmpty('customer_code');

        $validator
            ->integer('seating_number')
            ->allowEmpty('seating_number');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['cash_entity_id'], 'CashEntities'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['receipt_id'], 'Receipts'));

        return $rules;
    }

    public function findServerSideData(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            'Payments.created',           //0
            'Receipts.tipo_comp',         //1
            'Receipts.pto_vta',           //2
            'Receipts.num',               //3
            'Payments.concept',           //4
            'Payments.import',            //5
            'Receipts.customer_code',     //6
            'Receipts.customer_doc_type', //7
            'Receipts.customer_ident',    //8
            'Receipts.customer_name',     //9
            'Receipts.customer_address',  //10
            'Receipts.customer_city',     //11
            'Payments.payment_method_id', //12
            'CashEntities.name',          //13
            'Users.name',                 //14
            'Payments.anulated',          //15
            'Payments.number_transaction',//16
            'Receipts.business_id',       //17
            'Payments.seating_number',    //18
            'Receipts.barcode_alt',       //19
        ];

        $columns_select = [
            'Payments.created',           //0
            'Receipts.tipo_comp',         //1
            'Receipts.pto_vta',           //2
            'Receipts.num',               //3
            'Payments.concept',           //4
            'Payments.import',            //5
            'Receipts.customer_code',     //6
            'Receipts.customer_doc_type', //7
            'Receipts.customer_ident',    //8
            'Receipts.customer_name',     //9
            'Receipts.customer_address',  //10
            'Receipts.customer_city',     //11
            'Payments.payment_method_id', //12
            'CashEntities.name',          //13
            'Users.name',                 //14
            'Payments.anulated',          //15
            'Payments.number_transaction',//16
            'Receipts.business_id',       //17
            'Payments.seating_number',    //18
            'Receipts.barcode_alt',       //19
            'Payments.id',
            'Payments.receipt_id',
            'Customers.billing_for_service',
            'Customers.email',
            'Receipts.id' 
        ];

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] =>  $o['dir']];
        }

        $query = $query->contain([
            'Customers',
            'Users',
            'CashEntities',
            'Receipts'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Payments.id':
                        break;

                    case 'Payments.created': 
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Receipts.tipo_comp':
                    case 'Receipts.pto_vta':
                    case 'Receipts.num':
                    case 'Receipts.customer_code':
                    case 'Payments.payment_method_id':
                    case 'Receipts.customer_doc_type':
                    case 'Receipts.business_id':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Payments.import':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Payments.cd_auto_debit':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? true : false];
                        break;

                    case 'Payments.anulated':
                        if ($column['search']['value'] == 1) {
                             $where += [$columns[$index] . ' IS '=> null];
                        } else {
                            $where += [$columns[$index] . ' IS NOT '=> null];
                        }
                        break;

                    case 'Payments.concept':
                    case 'CashEntities.name':
                    case 'Receipts.customer_ident':
                    case 'Receipts.customer_name':
                    case 'Users.name':
                    case 'Payments.number_transaction':
                    case 'Receipts.customer_address':
                    case 'Receipts.customer_city':
                    case 'Payments.seating_number':
                    case 'Receipts.barcode_alt':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {
                    case 'Payments.created':
                    case 'Receipts.tipo_comp':
                    case 'Receipts.pto_vta':
                    case 'Payments.import':
                    case 'Payments.payment_method_id':
                    case 'CashEntities.name':
                    case 'Users.name':
                    case 'Payments.anulated':
                    case 'Payments.id':
                        break;

                    case 'Receipts.num':
                    case 'Receipts.customer_code':
                    case 'Receipts.customer_ident':
                    case 'Receipts.customer_doc_type':
                    case 'Receipts.business_id':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Receipts.customer_name':
                    case 'Payments.concept':
                    case 'Payments.number_transaction':
                    case 'Receipts.customer_address':
                    case 'Receipts.customer_city':
                    case 'Payments.seating_number':
                    case 'Receipts.barcode_alt':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {
                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order($order);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        $payment_getways = $_SESSION['payment_getway'];

        $query->map(function ($row)  use ($payment_getways)  { // map() is a collection method, it executes the query

            foreach ($payment_getways->methods as $pg) {

                if ($pg->id == $row->payment_method_id) {
                    $payment_method = new \stdClass;
                    $payment_method->name = $pg->name;
                    $row->payment_method = $pg;
                }
            }

            return $row;
        })
        ->toArray();

        return $query;
    }

    public function findRecordsFiltered(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;
        
        $columns = [
            'Payments.created',           //0
            'Receipts.tipo_comp',         //1
            'Receipts.pto_vta',           //2
            'Receipts.num',               //3
            'Payments.concept',           //4
            'Payments.import',            //5
            'Receipts.customer_code',     //6
            'Receipts.customer_doc_type', //7
            'Receipts.customer_ident',    //8
            'Receipts.customer_name',     //9
            'Receipts.customer_address',  //10
            'Receipts.customer_city',     //11
            'Payments.payment_method_id', //12
            'CashEntities.name',          //13
            'Users.name',                 //14
            'Payments.anulated',          //15
            'Payments.number_transaction',//16
            'Receipts.business_id',       //17
            'Payments.seating_number',    //18
            'Receipts.barcode_alt',       //19
        ];

        $columns_select = [
            'Payments.created',           //0
            'Receipts.tipo_comp',         //1
            'Receipts.pto_vta',           //2
            'Receipts.num',               //3
            'Payments.concept',           //4
            'Payments.import',            //5
            'Receipts.customer_code',     //6
            'Receipts.customer_doc_type', //7
            'Receipts.customer_ident',    //8
            'Receipts.customer_name',     //9
            'Receipts.customer_address',  //10
            'Receipts.customer_city',     //11
            'Payments.payment_method_id', //12
            'CashEntities.name',          //13
            'Users.name',                 //14
            'Payments.anulated',          //15
            'Payments.number_transaction',//16
            'Receipts.business_id',       //17
            'Payments.seating_number',    //18
            'Receipts.barcode_alt',       //19
            'Payments.id',
            'Payments.receipt_id',
            'Customers.billing_for_service',
            'Customers.email',
            'Receipts.id' 
        ];

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] => $o['dir']];
        }

        $query = $query->contain([
            'Customers',
            'Users',
            'CashEntities',
            'Receipts'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Payments.id':
                        break;

                    case 'Payments.created':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Receipts.tipo_comp':
                    case 'Receipts.pto_vta': 
                    case 'Receipts.num': 
                    case 'Receipts.customer_code':
                    case 'Payments.payment_method_id':
                    case 'Receipts.customer_doc_type':
                    case 'Receipts.business_id':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Payments.import':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Payments.cd_auto_debit':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? true : false];
                        break;

                    case 'Payments.anulated':

                        if ($column['search']['value'] == 1) {
                             $where += [$columns[$index] . ' IS '=> null];
                        } else {
                            $where += [$columns[$index] . ' IS NOT '=> null];
                        }
                        break;

                    case 'Payments.concept':
                    case 'CashEntities.name':
                    case 'Receipts.customer_ident':
                    case 'Receipts.customer_name':
                    case 'Users.name':
                    case 'Payments.number_transaction':
                    case 'Receipts.customer_address':
                    case 'Receipts.customer_city':
                    case 'Payments.seating_number':
                    case 'Receipts.barcode_alt':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {
                    case 'Payments.created':
                    case 'Receipts.tipo_comp':
                    case 'Receipts.pto_vta':
                    case 'Payments.import':
                    case 'Payments.payment_method_id':
                    case 'CashEntities.name':
                    case 'Users.name':
                    case 'Payments.anulated':
                    case 'Payments.id':
                        break;

                    case 'Receipts.num':
                    case 'Receipts.customer_code':
                    case 'Receipts.customer_ident':
                    case 'Receipts.customer_doc_type':
                    case 'Receipts.business_id':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Receipts.customer_name':
                    case 'Payments.concept':
                    case 'Payments.number_transaction':
                    case 'Receipts.customer_address':
                    case 'Receipts.customer_city':
                    case 'Payments.seating_number':
                    case 'Receipts.barcode_alt':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {
                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        return $query->count();
    }

    public function findServerSideDataMoves(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            'Payments.created',
            'Users.username',
            'CashEntities.name',
            'Receipts.tipo_comp',
            'Receipts.pto_vta',
            'Receipts.num',
            '',
            'Payments.concept',
            '',
            '',
            'Payments.import',
            '',
            '',
            '',
            'Payments.seating_number'
        ];

        $columns_select = [
            'Payments.created',
            'Users.username',
            'CashEntities.name',
            'Receipts.tipo_comp',
            'Receipts.pto_vta',
            'Receipts.num',
            'Payments.concept',
            'Payments.import',
            'Payments.seating_number',
            'Payments.id',
            'Payments.customer_code',
            'Payments.connection_id',
            'Payments.payment_method_id',
            'Receipts.id',
        ];

        //paginacion

        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        $query = $query->contain([
            'Users',
            'CashEntities',
            'Receipts'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Payments.created':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Receipts.tipo_comp':
                    case 'Receipts.pto_vta':
                    case 'Receipts.num': 

                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Payments.import':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Payments.concept':
                    case 'CashEntities.name':
                    case 'Users.username':
                    case 'Payments.seating_number':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Receipts.num':

                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Users.username' :
                    case 'Payments.concept' :
                    case 'Payments.seating_number':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order(['Payments.created' => 'desc']);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        $payment_getways = $_SESSION['payment_getway'];

        $query->map(function ($row)  use ($payment_getways)  { // map() is a collection method, it executes the query

            foreach ($payment_getways->methods as $pg) {

                if ($pg->id == $row->payment_method_id) {
                    $payment_method = new \stdClass;
                    $payment_method->name = $pg->name;
                    $row->payment_method = $pg;
                }
            }

            return $row;

        })
        ->toArray();

        return $query;
    }

    public function findRecordsFilteredMoves(Query $query, array $options)
    {
        $params = $options['params'];

        $where = [];
        $between = [];
        $orWhere = [];

        $columns = [
            'Payments.created',
            'Users.username',
            'CashEntities.name',
            'Receipts.tipo_comp',
            'Receipts.pto_vta',
            'Receipts.num',
            '',
            'Payments.concept',
            '',
            '',
            'Payments.import',
            '',
            '',
            '',
            'Payments.seating_number'
        ];

        $columns_select = [
            'Payments.created',
            'Users.username',
            'CashEntities.name',
            'Receipts.tipo_comp',
            'Receipts.pto_vta',
            'Receipts.num',
            'Payments.concept',
            'Payments.import',
            'Payments.seating_number',
            'Payments.id',
            'Payments.customer_code',
            'Payments.connection_id',
            'Payments.payment_method_id',
            'Receipts.id',
        ];

        $query = $query->contain([
            'Users',
            'CashEntities',
            'Receipts'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Payments.created': 
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0].' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Receipts.tipo_comp':
                    case 'Receipts.pto_vta': 
                    case 'Receipts.num': 
             
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Payments.import':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Payments.concept':
                    case 'CashEntities.name':
                    case 'Users.username':
                    case 'Payments.seating_number':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Receipts.num':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Payments.concept':
                    case 'Users.username':
                    case 'Payments.seating_number':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }

    public function findServerSideDataMovements(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            'Payments.created',
            'Payments.concept',
            '',
            'Payments.import',
        ];

        $columns_select = [
            'Payments.id',
            'Payments.created',
            'Payments.concept',
            'Payments.import',
            'Payments.customer_code',
        ];

        //paginacion

        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        $query = $query->contain([])
            ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Payments.created': 
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Payments.import':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                            
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Payments.concept':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Payments.concept':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order(['Payments.created' => 'desc']);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }
        return $query;
    }

    public function findRecordsFilteredMovements(Query $query, array $options)
    {
        $params = $options['params'];

        $where = [];
        $between = [];
        $orWhere = [];
        $columns = [
            'Payments.created',
            'Payments.concept',
            '',
            'Payments.import',
        ];

        $columns_select = [
            'Payments.id',
            'Payments.created',
            'Payments.concept',
            'Payments.import',
            'Payments.customer_code',
        ];

        $query = $query->contain([])
            ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Payments.created': 
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0].' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Payments.import':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Payments.concept':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Payments.concept':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }

    public function findServerSideDataMovesAll(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            'Payments.created',
            'Customers.code',
            'Customers.name',
            'Customers.doc_type',
            'Customers.ident',
            'Users.username',
            'CashEntities.name',
            'Receipts.tipo_comp',
            'Receipts.pto_vta',
            'Receipts.num',
            '',
            'Payments.concept',
            '',
            '',
            'Payments.import',
            '',
            '',
            '',
            'Payments.seating_number'
        ];

        $columns_select = [
            'Payments.created',
            'Customers.code',
            'Customers.name',
            'Customers.doc_type',
            'Customers.ident',
            'Customers.billing_for_service',
            'Customers.email',
            'Users.username',
            'CashEntities.name',
            'Receipts.tipo_comp',
            'Receipts.pto_vta',
            'Receipts.num',
            'Payments.concept',
            'Payments.import',
            'Payments.seating_number',
            'Payments.id',
            'Payments.customer_code',
            'Payments.connection_id',
            'Payments.payment_method_id',
            'Receipts.id'
        ];

        //paginacion

        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        $query = $query->contain([
            'Users',
            'CashEntities',
            'Receipts',
            'Customers'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Payments.created': 
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Receipts.tipo_comp':
                    case 'Receipts.pto_vta':
                    case 'Receipts.num':
                    case 'Customers.name':
                    case 'Customers.ident':

                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Payments.import':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Payments.concept':
                    case 'CashEntities.name':
                    case 'Users.username':
                    case 'Payments.seating_number':
                    case 'Customers.doc_type': 
                    case 'Customers.code':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Receipts.num':
                    case 'Customers.doc_type': 
                    case 'Customers.code':
  
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Users.username' :
                    case 'Payments.concept' :
                    case 'Payments.seating_number':
                    case 'Customers.name': 
                    case 'Customers.ident':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order(['Payments.created' => 'desc']);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        $payment_getways = $_SESSION['payment_getway'];

        $query->map(function ($row) use ($payment_getways) { // map() is a collection method, it executes the query

            foreach ($payment_getways->methods as $pg) {

                if ($pg->id == $row->payment_method_id) {
                    $payment_method = new \stdClass;
                    $payment_method->name = $pg->name;
                    $row->payment_method = $pg;
                }
            }

            return $row;

        })
        ->toArray();

        return $query;
    }

    public function findRecordsFilteredMovesAll(Query $query, array $options)
    {
        $params = $options['params'];

        $where = [];
        $between = [];
        $orWhere = [];

        $columns = [
            'Payments.created',
            'Customers.code',
            'Customers.name',
            'Customers.doc_type',
            'Customers.ident',
            'Users.username',
            'CashEntities.name',
            'Receipts.tipo_comp',
            'Receipts.pto_vta',
            'Receipts.num',
            '',
            'Payments.concept',
            '',
            '',
            'Payments.import',
            '',
            '',
            '',
            'Payments.seating_number'
        ];

        $columns_select = [
            'Payments.created',
            'Customers.code',
            'Customers.name',
            'Customers.doc_type',
            'Customers.ident',
            'Customers.billing_for_service',
            'Customers.email',
            'Users.username',
            'CashEntities.name',
            'Receipts.tipo_comp',
            'Receipts.pto_vta',
            'Receipts.num',
            'Payments.concept',
            'Payments.import',
            'Payments.seating_number',
            'Payments.id',
            'Payments.customer_code',
            'Payments.connection_id',
            'Payments.payment_method_id',
            'Receipts.id'
        ];

        $query = $query->contain([
            'Users',
            'CashEntities',
            'Receipts',
            'Customers'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Payments.created': 
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0].' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Receipts.tipo_comp':
                    case 'Receipts.pto_vta': 
                    case 'Receipts.num':
                    case 'Customers.code': 
                    case 'Customers.doc_type':

                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Payments.import':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Payments.concept':
                    case 'CashEntities.name':
                    case 'Users.username':
                    case 'Payments.seating_number':
                    case 'Customers.name': 
                    case 'Customers.ident':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Receipts.num':
                    case 'Customers.code': 
                    case 'Customers.doc_type':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Payments.concept':
                    case 'Users.username':
                    case 'Payments.seating_number':
                    case 'Customers.name': 
                    case 'Customers.ident':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }
}
