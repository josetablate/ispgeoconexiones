<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * VisaAutoDebitAccounts Model
 *
 * @property \App\Model\Table\PaymentGetwaysTable|\Cake\ORM\Association\BelongsTo $PaymentGetways
 *
 * @method \App\Model\Entity\VisaAutoDebitAccount get($primaryKey, $options = [])
 * @method \App\Model\Entity\VisaAutoDebitAccount newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\VisaAutoDebitAccount[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\VisaAutoDebitAccount|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\VisaAutoDebitAccount|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\VisaAutoDebitAccount patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\VisaAutoDebitAccount[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\VisaAutoDebitAccount findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class VisaAutoDebitAccountsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('visa_auto_debit_accounts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_code',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('asigned')
            ->allowEmpty('asigned');

        $validator
            ->scalar('card_number')
            ->maxLength('card_number', 200)
            ->allowEmpty('card_number');

        $validator
            ->integer('customer_code')
            ->allowEmpty('customer_code');

        $validator
            ->boolean('deleted')
            ->requirePresence('deleted', 'create')
            ->notEmpty('deleted');

        return $validator;
    }

    public function findServerSideData(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            '',                                  //0
            'VisaAutoDebitAccounts.card_number', //1
            'Customers.name',                    //2
            'Customers.ident',                   //3
            'Customers.code',                    //4
        ];

        $columns_select = [
            'VisaAutoDebitAccounts.card_number', //1
            'Customers.name',                    //2
            'Customers.ident',                   //3
            'Customers.code',                    //4
            'Customers.doc_type',                //5
            'VisaAutoDebitAccounts.id',          //6
        ];

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] =>  $o['dir']];
        }

        $query = $query->contain([
            'Customers'
        ])
        ->select($columns_select); 

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Customers.code':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'VisaAutoDebitAccounts.deleted':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? TRUE : FALSE];
                        break;

                    case 'VisaAutoDebitAccounts.card_number':
                    case 'Customers.ident':
                    case 'Customers.name':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Customers.code':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Customers.name': 
                    case 'Customers.ident':
                    case 'VisaAutoDebitAccounts.card_number':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order($order);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        return $query;
    }

    public function findRecordsFiltered(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            '',                                  //0
            'VisaAutoDebitAccounts.card_number', //1
            'Customers.name',                    //2
            'Customers.ident',                   //3
            'Customers.code',                    //4
        ];

        $columns_select = [
            'VisaAutoDebitAccounts.card_number', //0
            'Customers.name',                    //1
            'Customers.ident',                   //2
            'Customers.code',                    //3
            'Customers.doc_type',                //4
            'VisaAutoDebitAccounts.id',          //5
        ];

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] =>  $o['dir']];
        }

        $query = $query->contain([
            'Customers'
        ])->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Customers.code':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'VisaAutoDebitAccounts.deleted':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? TRUE : FALSE];
                        break;

                    case 'VisaAutoDebitAccounts.card_number':
                    case 'Customers.ident':
                    case 'Customers.name':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Customers.code': 
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Customers.name': 
                    case 'Customers.ident':
                    case 'VisaAutoDebitAccounts.card_number':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }


        if (count($between) > 0) {
            foreach ($between as $b) {
                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }
}
