<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Receipts Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $Payments
 *
 * @method \App\Model\Entity\Receipt get($primaryKey, $options = [])
 * @method \App\Model\Entity\Receipt newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Receipt[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Receipt|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Receipt patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Receipt[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Receipt findOrCreate($search, callable $callback = null)
 */
class ReceiptsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('receipts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Receipts', [
            'foreignKey' => 'invoice_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Connections', [
            'foreignKey' => 'connection_id',
            'joinType' => 'LEFT'
        ]);

        $this->hasOne('Payments', [
            'foreignKey' => 'receipt_id'
        ]);

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_code',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('date')
            ->requirePresence('date', 'create')
            ->notEmpty('date');

        $validator
            ->integer('pto_vta')
            ->requirePresence('pto_vta', 'create')
            ->notEmpty('pto_vta');

        $validator
    
            ->requirePresence('tipo_comp', 'create')
            ->notEmpty('tipo_comp');

        $validator
            ->allowEmpty('concept');

        $validator
            ->decimal('total')
            ->requirePresence('total', 'create')
            ->notEmpty('total');

        $validator
            ->requirePresence('company_name', 'create')
            ->notEmpty('company_name');

        $validator
            ->requirePresence('company_address', 'create')
            ->notEmpty('company_address');

        $validator
            ->requirePresence('company_cp', 'create')
            ->notEmpty('company_cp');

        $validator
            ->requirePresence('company_city', 'create')
            ->notEmpty('company_city');

        $validator
            ->requirePresence('company_phone', 'create')
            ->notEmpty('company_phone');

        $validator
            ->allowEmpty('company_fax');

        $validator
            ->requirePresence('company_ident', 'create')
            ->notEmpty('company_ident');

        $validator
            ->allowEmpty('company_email');

        $validator
            ->allowEmpty('company_web');

        $validator
            ->integer('customer_code')
            ->requirePresence('customer_code', 'create')
            ->notEmpty('customer_code');

        $validator
            ->requirePresence('customer_name', 'create')
            ->notEmpty('customer_name');

        $validator
            ->allowEmpty('customer_address');

        $validator
            ->allowEmpty('customer_cp');

        $validator
            ->allowEmpty('customer_city');

        $validator
            ->allowEmpty('customer_country');

        $validator
            ->integer('customer_doc_type')
            ->allowEmpty('customer_doc_type');

        $validator
            ->allowEmpty('customer_ident');

        $validator
            ->allowEmpty('comments');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function beforeSave($event, $entity, $options)
    {
        if (!$entity->isDirty('portal_sync')) {
            $entity->portal_sync = false;
        }
    }

    public function findServerSideDataComprobantes(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            '',                           //0
            'Receipts.date',              //1
            'Receipts.date_start',        //2
            'Receipts.date_end',          //3
            'Receipts.tipo_comp',         //4
            'Receipts.pto_vta',           //5
            'Receipts.num',               //6
            'Receipts.concept',           //7
            'Receipts.duedate',           //8
            'Receipts.total',             //9
            'Users.username',             //10
            'Receipts.customer_code',     //11
            'Receipts.customer_doc_type', //12
            'Receipts.customer_ident',    //13
            'Receipts.customer_name',     //14
            'Customers.email',            //15
            'Receipts.customer_address',  //16
            'Receipts.customer_city',     //17
            'Receipts.payment_method',    //18
            'Receipts.business_id',       //19
        ];

        $columns_select = [
            'Receipts.id',                   //0
            'Receipts.date',                 //1
            'Receipts.date_start',           //2
            'Receipts.date_end',             //3
            'Receipts.tipo_comp',            //4
            'Receipts.pto_vta',              //5
            'Receipts.num',                  //6
            'Receipts.concept',              //7
            'Receipts.duedate',              //8
            'Receipts.total',                //9
            'Users.username',                //10
            'Receipts.customer_code',        //11
            'Receipts.customer_doc_type',    //12
            'Receipts.customer_ident',       //13
            'Receipts.customer_name',        //14
            'Customers.email',               //15
            'Receipts.customer_address',     //16
            'Receipts.customer_city',        //17
            'Receipts.payment_method',       //18
            'Receipts.business_id',          //19
            'Customers.billing_for_service', //20
        ];

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] => $o['dir']];
        }

        $query = $query->contain([
            'Users',
            'Customers'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Receipts.date': 
                    case 'Receipts.date_start':
                    case 'Receipts.date_end':
                    case 'Receipts.duedate':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Receipts.tipo_comp':
                    case 'Receipts.pto_vta':
                    case 'Receipts.num':
                    case 'Receipts.customer_code':
                    case 'Receipts.business_id':
                    case 'Receipts.customer_ident':
                    case 'Receipts.customer_doc_type':
                    case 'Receipts.payment_method':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Receipts.total': 
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Users.username':
                    case 'Customers.email':
                    case 'Receipts.customer_name':
                    case 'Receipts.comments':
                    case 'Receipts.customer_address':
                    case 'Receipts.customer_city':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {
                    case 'Receipts.date':
                    case 'Receipts.tipo_comp':
                    case 'Receipts.pto_vta':
                    case 'Receipts.date_start':
                    case 'Receipts.date_end':
                    case 'Receipts.duedate':
                    case 'Receipts.total':
                    case 'Users.name':
                    case 'Receipts.business_id':  
                        break;

                    case 'Receipts.num':
                    case 'Receipts.customer_code':
                    case 'Receipts.customer_ident':
                    case 'Receipts.customer_doc_type':
                    case 'Receipts.payment_method':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Receipts.customer_name':
                    case 'Receipts.comments' :
                    case 'Receipts.customer_address':
                    case 'Receipts.customer_city':
                    case 'Customers.email':
                    case 'Users.username':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order($order);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        $query->toArray();

        return $query;
    }

    public function findRecordsFilteredComprobantes(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;
        
        $columns = [
            '',                           //0
            'Receipts.date',              //1
            'Receipts.date_start',        //2
            'Receipts.date_end',          //3
            'Receipts.tipo_comp',         //4
            'Receipts.pto_vta',           //5
            'Receipts.num',               //6
            'Receipts.concept',           //7
            'Receipts.duedate',           //8
            'Receipts.total',             //9
            'Users.username',             //10
            'Receipts.customer_code',     //11
            'Receipts.customer_doc_type', //12
            'Receipts.customer_ident',    //13
            'Receipts.customer_name',     //14
            'Customers.email',            //15
            'Receipts.customer_address',  //16
            'Receipts.customer_city',     //17
            'Receipts.payment_method',    //18
            'Receipts.business_id',       //19
        ];

        $columns_select = [
            'Receipts.id',                   //0
            'Receipts.date',                 //1
            'Receipts.date_start',           //2
            'Receipts.date_end',             //3
            'Receipts.tipo_comp',            //4
            'Receipts.pto_vta',              //5
            'Receipts.num',                  //6
            'Receipts.concept',              //7
            'Receipts.duedate',              //8
            'Receipts.total',                //9
            'Users.username',                //10
            'Receipts.customer_code',        //11
            'Receipts.customer_doc_type',    //12
            'Receipts.customer_ident',       //13
            'Receipts.customer_name',        //14
            'Customers.email',               //15
            'Receipts.customer_address',     //16
            'Receipts.customer_city',        //17
            'Receipts.payment_method',       //18
            'Receipts.business_id',          //19
            'Customers.billing_for_service', //20
        ];

        $query = $query->contain([
            'Users',
            'Customers'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Receipts.date': 
                    case 'Receipts.date_start':
                    case 'Receipts.date_end':
                    case 'Receipts.duedate':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Receipts.tipo_comp':
                    case 'Receipts.pto_vta':
                    case 'Receipts.num':
                    case 'Receipts.customer_code':
                    case 'Receipts.business_id':
                    case 'Receipts.customer_ident':
                    case 'Receipts.customer_doc_type':
                    case 'Receipts.payment_method':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Receipts.total': 
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Users.username':
                    case 'Customers.email':
                    case 'Receipts.customer_name':
                    case 'Receipts.comments':
                    case 'Receipts.customer_address':
                    case 'Receipts.customer_city':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {
                    case 'Receipts.date':
                    case 'Receipts.tipo_comp':
                    case 'Receipts.pto_vta':
                    case 'Receipts.date_start':
                    case 'Receipts.date_end':
                    case 'Receipts.duedate':
                    case 'Receipts.total':
                    case 'Users.name':
                    case 'Receipts.business_id':  
                        break;

                    case 'Receipts.num':
                    case 'Receipts.customer_code':
                    case 'Receipts.customer_ident':
                    case 'Receipts.customer_doc_type':
                    case 'Receipts.payment_method':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Receipts.customer_name':
                    case 'Receipts.comments' :
                    case 'Receipts.customer_address':
                    case 'Receipts.customer_city':
                    case 'Customers.email':
                    case 'Users.username':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }

    // public function afterSave($event, $entity, $options)
    // {
    //     $detail = '';

    //     if ($entity->isNew()) {

    //         $paraments = $_SESSION['paraments'];

    //         foreach ($paraments->invoicing->business as $b) {
    //             if ($b->id == $entity->business_id) {
    //                  $business = $b->name . ' (' . $b->address . ')';
    //             }
    //         }

    //         $action = 'Creación de Recibo';
    //         $detail .= 'Fecha: ' . $entity->date->format('d/m/Y') . PHP_EOL;
    //         $detail .= 'Desde: ' . $entity->date_start->format('d/m/Y') . PHP_EOL;
    //         $detail .= 'Hasta: ' . $entity->date_end->format('d/m/Y') . PHP_EOL;
    //         $detail .= 'Vencimiento: ' . $entity->duedate->format('d/m/Y') . PHP_EOL;
    //         $detail .= 'Número: ' . $entity->num . PHP_EOL;
    //         $detail .= 'Pto de Vta: ' . $entity->pto_vta . PHP_EOL;
    //         $detail .= 'Comentarios: ' . $entity->comments . PHP_EOL;
    //         $detail .= 'Total: ' . number_format($entity->total, 2, ',', '.') . PHP_EOL;
    //         $detail .= 'Empresa: ' . $business . PHP_EOL;
    //         $detail .= 'Observación: ' . $entity->observations . PHP_EOL;
    //         $detail .= 'Método de Pago: ' . $entity->payment_method . PHP_EOL;

    //         $actionLog = TableRegistry::get('ActionLog');
    //         $query = $actionLog->query();
    //         $query->insert([
    //             'created', 
    //             'detail',
    //             'user_id',
    //             'action',
    //             'customer_code'
    //         ])
    //         ->values([
    //             'created' => Time::now(), 
    //             'detail' => $detail,
    //             'user_id' => $_SESSION['Auth']['User']['id'],
    //             'action' => $action,
    //             'customer_code' => isset($entity->customer_code) ? $entity->customer_code : null,
    //         ])
    //         ->execute();
    //     }
    // }
}
