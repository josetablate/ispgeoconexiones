<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * IntOutCashsEntities Model
 *
 * @property \Cake\ORM\Association\BelongsTo $CashEntities
 *
 * @method \App\Model\Entity\IntOutCashsEntity get($primaryKey, $options = [])
 * @method \App\Model\Entity\IntOutCashsEntity newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\IntOutCashsEntity[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\IntOutCashsEntity|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\IntOutCashsEntity patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\IntOutCashsEntity[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\IntOutCashsEntity findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class IntOutCashsEntitiesTable extends Table
{
    
      public $recursive = -1;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('int_out_cashs_entities');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('CashEntities', [
            'foreignKey' => 'cash_entity_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        $validator
            ->requirePresence('concept', 'create')
            ->notEmpty('concept');

        $validator
            ->decimal('in_value')
            ->requirePresence('in_value', 'create')
            ->notEmpty('in_value');

        $validator
            ->decimal('out_value')
            ->requirePresence('out_value', 'create')
            ->notEmpty('out_value');

        $validator
            ->boolean('deleted')
            ->requirePresence('deleted', 'create')
            ->notEmpty('deleted');

        $validator
            ->allowEmpty('data');

        $validator
            ->decimal('saldo')
            ->requirePresence('saldo', 'create')
            ->notEmpty('saldo');

        $validator
            ->integer('number_part')
            ->allowEmpty('number_part');

        $validator
            ->integer('seating_number')
            ->allowEmpty('seating_number');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['cash_entity_id'], 'CashEntities'));

        return $rules;
    }
}
