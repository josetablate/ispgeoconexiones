<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Articles Model
 *
 * @property \Cake\ORM\Association\HasMany $Products
 * @property \Cake\ORM\Association\HasMany $SnidStores
 * @property \Cake\ORM\Association\BelongsToMany $Stores
 * @property \Cake\ORM\Association\BelongsToMany $Instalations
 * @property \Cake\ORM\Association\BelongsToMany $Packages
 *
 * @method \App\Model\Entity\Article get($primaryKey, $options = [])
 * @method \App\Model\Entity\Article newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Article[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Article|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Article patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Article[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Article findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ArticlesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('articles');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Products', [
            'foreignKey' => 'article_id'
        ]);
        $this->hasMany('SnidStores', [
            'foreignKey' => 'article_id'
        ]);
        

        
        
        $this->belongsToMany('Stores', [
            'foreignKey' => 'article_id',
            'targetForeignKey' => 'store_id',
            'joinTable' => 'articles_stores'
        ]);
        $this->hasMany('ArticlesStores', [
            'foreignKey' => 'article_id'
        ]);
        $this->belongsToMany('Instalations', [
            'foreignKey' => 'article_id',
            'targetForeignKey' => 'instalation_id',
            'joinTable' => 'instalations_articles'
        ]);
        $this->belongsToMany('Packages', [
            'foreignKey' => 'article_id',
            'targetForeignKey' => 'package_id',
            'joinTable' => 'packages_articles'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('code', 'create')
            ->notEmpty('code');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->integer('total_amount')
            ->requirePresence('total_amount', 'create')
            ->notEmpty('total_amount');

        $validator
            ->boolean('exist_snid')
            // ->requirePresence('exist_snid', 'create')
            ->allowEmpty('exist_snid');

        $validator
            ->boolean('deleted')
            // ->requirePresence('deleted', 'create')
            ->allowEmpty('deleted');

        $validator
            ->boolean('enabled')
            // ->requirePresence('enabled', 'create')
            ->allowEmpty('enabled');

        return $validator;
    }
}
