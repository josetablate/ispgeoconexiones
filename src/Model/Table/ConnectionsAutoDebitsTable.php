<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ConnectionsAutoDebits Model
 *
 * @property \App\Model\Table\ConnectionsTable|\Cake\ORM\Association\BelongsTo $Connections
 * @property \App\Model\Table\AutoDebitsTable|\Cake\ORM\Association\BelongsTo $AutoDebits
 *
 * @method \App\Model\Entity\ConnectionsAutoDebit get($primaryKey, $options = [])
 * @method \App\Model\Entity\ConnectionsAutoDebit newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ConnectionsAutoDebit[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ConnectionsAutoDebit|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ConnectionsAutoDebit patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ConnectionsAutoDebit[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ConnectionsAutoDebit findOrCreate($search, callable $callback = null, $options = [])
 */
class ConnectionsAutoDebitsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('connections_auto_debits');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Connections', [
            'foreignKey' => 'connection_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('AutoDebits', [
            'foreignKey' => 'auto_debit_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['connection_id'], 'Connections'));
        $rules->add($rules->existsIn(['auto_debit_id'], 'AutoDebits'));

        return $rules;
    }
}
