<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MastercardAutoDebitTransactions Model
 *
 * @property \App\Model\Table\BloquesTable|\Cake\ORM\Association\BelongsTo $Bloques
 * @property \App\Model\Table\PaymentGetwaysTable|\Cake\ORM\Association\BelongsTo $PaymentGetways
 * @property \App\Model\Table\ReceiptsTable|\Cake\ORM\Association\BelongsTo $Receipts
 *
 * @method \App\Model\Entity\MastercardAutoDebitTransaction get($primaryKey, $options = [])
 * @method \App\Model\Entity\MastercardAutoDebitTransaction newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MastercardAutoDebitTransaction[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MastercardAutoDebitTransaction|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MastercardAutoDebitTransaction|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MastercardAutoDebitTransaction patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MastercardAutoDebitTransaction[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MastercardAutoDebitTransaction findOrCreate($search, callable $callback = null, $options = [])
 */
class MastercardAutoDebitTransactionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('mastercard_auto_debit_transactions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_code',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('id_transaction')
            ->maxLength('id_transaction', 100)
            ->requirePresence('id_transaction', 'create')
            ->notEmpty('id_transaction');

        $validator
            ->dateTime('date')
            ->requirePresence('date', 'create')
            ->notEmpty('date');

        $validator
            ->scalar('card_number')
            ->maxLength('card_number', 2000)
            ->allowEmpty('card_number');

        $validator
            ->decimal('total')
            ->allowEmpty('total');

        $validator
            ->boolean('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->scalar('comment')
            ->maxLength('comment', 255)
            ->allowEmpty('comment');

        $validator
            ->integer('customer_code')
            ->allowEmpty('customer_code');

        $validator
            ->scalar('receipt_number')
            ->maxLength('receipt_number', 255)
            ->allowEmpty('receipt_number');

        return $validator;
    }
    
    public function findServerSideDataTransactions(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            'MastercardAutoDebitTransactions.date',              //0
            'MastercardAutoDebitTransactions.payment_getway_id', //1 metodo de pago
            'MastercardAutoDebitTransactions.status',            //2
            'Customers.code',                                    //3
            'Customers.name',                                    //4
            'Customers.ident',                                   //5
            'MastercardAutoDebitTransactions.id',                //6
            'MastercardAutoDebitTransactions.total',             //7
            'MastercardAutoDebitTransactions.comment',           //8
            'MastercardAutoDebitTransactions.card_number',       //9
            'MastercardAutoDebitTransactions.receipt_number',    //10
        ];

        $columns_select = [
            'MastercardAutoDebitTransactions.id',                //0
            'MastercardAutoDebitTransactions.date',              //1
            'MastercardAutoDebitTransactions.payment_getway_id', //2
            'MastercardAutoDebitTransactions.status',            //3
            'Customers.code',                                    //4
            'Customers.name',                                    //5
            'Customers.ident',                                   //6
            'Customers.doc_type',                                //7
            'MastercardAutoDebitTransactions.total',             //8
            'MastercardAutoDebitTransactions.comment',           //9
            'MastercardAutoDebitTransactions.card_number',       //10
            'MastercardAutoDebitTransactions.receipt_number',    //11
            'MastercardAutoDebitTransactions.receipt_id',        //12
        ];

        //paginacion

        if ($params['length'] > 0) {
            
            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        $query = $query->contain([
            'Customers'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'MastercardAutoDebitTransactions.date':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] .'-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Customers.code':
                    case 'MastercardAutoDebitTransactions.payment_getway_id':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'MastercardAutoDebitTransactions.total':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'MastercardAutoDebitTransactions.status':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? true : false];
                        break;

                    case 'MastercardAutoDebitTransactions.comment':
                    case 'Customers.name':
                    case 'MastercardAutoDebitTransactions.id':
                    case 'MastercardAutoDebitTransactions.card_number':
                    case 'MastercardAutoDebitTransactions.receipt_number':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);
 
        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Customers.code':
                    case 'MastercardAutoDebitTransactions.payment_getway_id':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'MastercardAutoDebitTransactions.comment':
                    case 'Customers.name':
                    case 'MastercardAutoDebitTransactions.id':
                    case 'MastercardAutoDebitTransactions.card_number':
                    case 'MastercardAutoDebitTransactions.receipt_number':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order(['MastercardAutoDebitTransactions.date' => 'desc']);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        $query->toArray();

        return $query;
    }

    public function findRecordsFilteredTransactions(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            'MastercardAutoDebitTransactions.date',              //0
            'MastercardAutoDebitTransactions.payment_getway_id', //1 metodo de pago
            'MastercardAutoDebitTransactions.status',            //2
            'Customers.code',                                    //3
            'Customers.name',                                    //4
            'Customers.ident',                                   //5
            'MastercardAutoDebitTransactions.id',                //6
            'MastercardAutoDebitTransactions.total',             //7
            'MastercardAutoDebitTransactions.comment',           //8
            'MastercardAutoDebitTransactions.card_number',       //9
            'MastercardAutoDebitTransactions.receipt_number',    //10
        ];

        $columns_select = [
            'MastercardAutoDebitTransactions.id',                //0
            'MastercardAutoDebitTransactions.date',              //1
            'MastercardAutoDebitTransactions.payment_getway_id', //2
            'MastercardAutoDebitTransactions.status',            //3
            'Customers.code',                                    //4
            'Customers.name',                                    //5
            'Customers.ident',                                   //6
            'Customers.doc_type',                                //7
            'MastercardAutoDebitTransactions.total',             //8
            'MastercardAutoDebitTransactions.comment',           //9
            'MastercardAutoDebitTransactions.card_number',       //10
            'MastercardAutoDebitTransactions.receipt_number',    //11
            'MastercardAutoDebitTransactions.receipt_id',        //12
        ];

        $query = $query->contain([
            'Customers'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'MastercardAutoDebitTransactions.date':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] .'-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Customers.code':
                    case 'MastercardAutoDebitTransactions.payment_getway_id':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'MastercardAutoDebitTransactions.total':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'MastercardAutoDebitTransactions.status':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? true : false];
                        break;

                    case 'MastercardAutoDebitTransactions.comment':
                    case 'Customers.name':
                    case 'MastercardAutoDebitTransactions.id':
                    case 'MastercardAutoDebitTransactions.card_number':
                    case 'MastercardAutoDebitTransactions.receipt_number':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);
 
        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Customers.code':
                    case 'MastercardAutoDebitTransactions.payment_getway_id':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'MastercardAutoDebitTransactions.comment':
                    case 'Customers.name':
                    case 'MastercardAutoDebitTransactions.id':
                    case 'MastercardAutoDebitTransactions.card_number':
                    case 'MastercardAutoDebitTransactions.receipt_number':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }
}
