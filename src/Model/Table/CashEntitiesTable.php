<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

/**
 * CashEntities Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $IntOutCashsEntities
 * @property \Cake\ORM\Association\HasMany $Payments
 *
 * @method \App\Model\Entity\CashEntity get($primaryKey, $options = [])
 * @method \App\Model\Entity\CashEntity newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CashEntity[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CashEntity|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CashEntity patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CashEntity[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CashEntity findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CashEntitiesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cash_entities');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('IntOutCashsEntities', [
            'foreignKey' => 'cash_entity_id'
        ]);
        $this->hasMany('Payments', [
            'foreignKey' => 'cash_entity_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->decimal('cash')
            // ->requirePresence('cash', 'create')
            ->allowEmpty('cash');

        $validator
            ->allowEmpty('comments');

        $validator
            ->boolean('enabled')
            ->requirePresence('enabled', 'create')
            ->notEmpty('enabled');

        $validator
            ->boolean('deleted')
            // ->requirePresence('deleted', 'create')
            ->allowEmpty('deleted');

        $validator
            ->boolean('open')
            // ->requirePresence('open', 'create')
            ->allowEmpty('open');

        

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function afterSave($event, $entity, $options)
    {
        $ok = false;

        $detail = '';

        if (!$entity->isNew()) {

            $action = 'Edición de Caja';

            $dirtyFields = $entity->getDirty();

            $dirtyFields = array_diff($dirtyFields, ["modified"]);

            foreach ($dirtyFields as $dirtyField) {

                switch ($dirtyField) {

                    case 'name': 
                        $detail .= 'Nombre: ' . $entity->getOriginal('name') . ' => ' . $entity->name . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'comments': 
                        $detail .= 'Comentario: ' . $entity->getOriginal('comments') . ' => ' . $entity->comments . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'contado':
                        $detail .= 'Contado: ' .  number_format($entity->getOriginal('contado'), 2, ',', '.') . ' => ' . number_format($entity->contado, 2, ',', '.') . PHP_EOL; 
                        $ok = true;
                        break;
                     case 'cash_other':
                        $detail .= 'Otro Medio: ' .  number_format($entity->getOriginal('cash_other'), 2, ',', '.') . ' => ' . number_format($entity->cash_other, 2, ',', '.') . PHP_EOL; 
                        $ok = true;
                        break;
                    case 'enabled':
                        if ($entity->enabled) $action = 'Habilitación de Caja'; else $action = 'Deshabilitación de Caja';
                        $ok = true;
                        break;
                    case 'deleted':
                        if ($entity->deleted) $action = 'Eliminación de Caja'; else $action = 'Restauración de Caja';
                        $ok = true;
                        break;
               }
            }
        } else {
            $action = 'Creación de Caja';
            $detail = 'Nombre: ' . $entity->name . PHP_EOL;
            $detail = 'Comentario: ' . $entity->comments . PHP_EOL;
            $detail .= 'Habilitado: ' . ($entity->enabled ? 'Si' : 'No') . PHP_EOL;
            $ok = true;
        }

        if ($ok) {

            if (!isset($_SESSION['Auth']['User']['id'])) {
    	        $user_id = 100;
    	    } else {
    	        $user_id = $_SESSION['Auth']['User']['id'];
    	    }

            $actionLog = TableRegistry::get('ActionLog');
            $query = $actionLog->query();
            $query->insert([
                'created', 
                'detail',
                'user_id',
                'action',
                'customer_code'
            ])
            ->values([
                'created' => Time::now(), 
                'detail' => $detail,
                'user_id' => $user_id,
                'action' => $action,
                'customer_code' => isset($entity->customer_code) ? $entity->customer_code : null,
            ])
            ->execute();
        }
    }
}
