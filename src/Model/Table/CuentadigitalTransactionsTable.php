<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CuentadigitalTransactions Model
 *
 * @property \App\Model\Table\BloquesTable|\Cake\ORM\Association\BelongsTo $Bloques
 * @property \App\Model\Table\ReceiptsTable|\Cake\ORM\Association\BelongsTo $Receipts
 *
 * @method \App\Model\Entity\CuentadigitalTransaction get($primaryKey, $options = [])
 * @method \App\Model\Entity\CuentadigitalTransaction newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CuentadigitalTransaction[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CuentadigitalTransaction|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CuentadigitalTransaction|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CuentadigitalTransaction patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CuentadigitalTransaction[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CuentadigitalTransaction findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CuentadigitalTransactionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cuentadigital_transactions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_code',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('fecha')
            ->allowEmpty('fecha');

        $validator
            ->scalar('fecha_cobro')
            ->maxLength('fecha_cobro', 10)
            ->allowEmpty('fecha_cobro');

        $validator
            ->scalar('hhmmss')
            ->maxLength('hhmmss', 10)
            ->allowEmpty('hhmmss');

        $validator
            ->decimal('neto')
            ->allowEmpty('neto');

        $validator
            ->decimal('neto_original')
            ->allowEmpty('neto_original');

        $validator
            ->scalar('barcode')
            ->maxLength('barcode', 50)
            ->allowEmpty('barcode');

        $validator
            ->scalar('info')
            ->maxLength('info', 150)
            ->allowEmpty('info');

        $validator
            ->boolean('estado')
            ->requirePresence('estado', 'create')
            ->notEmpty('estado');

        $validator
            ->scalar('comentario')
            ->maxLength('comentario', 255)
            ->allowEmpty('comentario');

        $validator
            ->integer('customer_code')
            ->allowEmpty('customer_code');

        $validator
            ->scalar('receipt_number')
            ->maxLength('receipt_number', 255)
            ->allowEmpty('receipt_number');

        return $validator;
    }

    public function findServerSideDataTransactions(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            'CuentadigitalTransactions.fecha',             //0
            'CuentadigitalTransactions.payment_getway_id', //1 metodo de pago
            'CuentadigitalTransactions.estado',            //2
            'Customers.code',                              //3
            'Customers.name',                              //4
            'Customers.ident',                             //5
            'CuentadigitalTransactions.id',                //6
            'CuentadigitalTransactions.neto_original',     //7
            'CuentadigitalTransactions.comentario',        //8
            'CuentadigitalTransactions.barcode',           //9
            'CuentadigitalTransactions.receipt_number',    //10
        ];

        $columns_select = [
            'CuentadigitalTransactions.id',                //0
            'CuentadigitalTransactions.fecha',             //1
            'CuentadigitalTransactions.payment_getway_id', //2
            'CuentadigitalTransactions.estado',            //3
            'Customers.code',                              //4
            'Customers.name',                              //5
            'Customers.ident',                             //6
            'Customers.doc_type',                          //7
            'CuentadigitalTransactions.neto_original',     //8
            'CuentadigitalTransactions.comentario',        //9
            'CuentadigitalTransactions.barcode',           //10
            'CuentadigitalTransactions.receipt_number',    //11
            'CuentadigitalTransactions.receipt_id',        //12
        ];

        //paginacion

        if ($params['length'] > 0) {
            
            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        $query = $query->contain([
            'Customers'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'CuentadigitalTransactions.fecha':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] .'-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Customers.code':
                    case 'CuentadigitalTransactions.payment_getway_id':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'CuentadigitalTransactions.neto_original':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'CuentadigitalTransactions.estado':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? true : false];
                        break;

                    case 'CuentadigitalTransactions.comentario':
                    case 'Customers.name':
                    case 'CuentadigitalTransactions.id':
                    case 'CuentadigitalTransactions.barcode':
                    case 'CuentadigitalTransactions.receipt_number':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);
 
        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Customers.code':
                    case 'CuentadigitalTransactions.payment_getway_id':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'CuentadigitalTransactions.comentario':
                    case 'Customers.name':
                    case 'CuentadigitalTransactions.id':
                    case 'CuentadigitalTransactions.barcode':
                    case 'CuentadigitalTransactions.receipt_number':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order(['CuentadigitalTransactions.fecha' => 'desc']);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        $query->toArray();

        return $query;
    }

    public function findRecordsFilteredTransactions(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            'CuentadigitalTransactions.fecha',             //0
            'CuentadigitalTransactions.payment_getway_id', //1 metodo de pago
            'CuentadigitalTransactions.estado',            //2
            'Customers.code',                              //3
            'Customers.name',                              //4
            'Customers.ident',                             //5
            'CuentadigitalTransactions.id',                //6
            'CuentadigitalTransactions.neto_original',     //7
            'CuentadigitalTransactions.comentario',        //8
            'CuentadigitalTransactions.barcode',           //9
            'CuentadigitalTransactions.receipt_number',    //10
        ];

        $columns_select = [
            'CuentadigitalTransactions.id',                //0
            'CuentadigitalTransactions.fecha',             //1
            'CuentadigitalTransactions.payment_getway_id', //2
            'CuentadigitalTransactions.estado',            //3
            'Customers.code',                              //4
            'Customers.name',                              //5
            'Customers.ident',                             //6
            'Customers.doc_type',                          //7
            'CuentadigitalTransactions.neto_original',     //8
            'CuentadigitalTransactions.comentario',        //9
            'CuentadigitalTransactions.barcode',           //10
            'CuentadigitalTransactions.receipt_number',    //11
            'CuentadigitalTransactions.receipt_id',        //12
        ];

        $query = $query->contain([
            'Customers'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'CuentadigitalTransactions.fecha':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Customers.code':
                    case 'CuentadigitalTransactions.payment_getway_id':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'CuentadigitalTransactions.neto_original':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0], 'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'CuentadigitalTransactions.estado':
                        $where += [$columns[$index] => $column['search']['value'] == 1 ? true : false];
                        break;

                    case 'CuentadigitalTransactions.comentario':
                    case 'CuentadigitalTransactions.id':
                    case 'Customers.name':
                    case 'CuentadigitalTransactions.barcode':
                    case 'CuentadigitalTransactions.receipt_number':

                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);
 
        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Customers.code':
                    case 'CuentadigitalTransactions.payment_getway_id':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'CuentadigitalTransactions.comentario':
                    case 'Customers.name':
                    case 'CuentadigitalTransactions.id':
                    case 'CuentadigitalTransactions.barcode':
                    case 'CuentadigitalTransactions.receipt_number':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }
}
