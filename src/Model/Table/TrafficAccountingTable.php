<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TrafficAccounting Model
 *
 * @method \App\Model\Entity\TrafficAccounting get($primaryKey, $options = [])
 * @method \App\Model\Entity\TrafficAccounting newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TrafficAccounting[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TrafficAccounting|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TrafficAccounting patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TrafficAccounting[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TrafficAccounting findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TrafficAccountingTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('traffic_accounting');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('src_address')
            ->requirePresence('src_address', 'create')
            ->notEmpty('src_address');

        $validator
            ->integer('dst_address')
            ->requirePresence('dst_address', 'create')
            ->notEmpty('dst_address');

        $validator
            ->integer('packets')
            ->requirePresence('packets', 'create')
            ->notEmpty('packets');

        $validator
            ->requirePresence('bytes', 'create')
            ->notEmpty('bytes');

        return $validator;
    }
}
