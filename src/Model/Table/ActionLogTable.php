<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Log\Log;
use Cake\I18n\Time;

/**
 * ActionLog Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\ActionLog get($primaryKey, $options = [])
 * @method \App\Model\Entity\ActionLog newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ActionLog[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ActionLog|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ActionLog patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ActionLog[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ActionLog findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ActionLogTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('action_log');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_code',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('menu', 'create')
            ->notEmpty('menu');

        $validator
            ->requirePresence('msg', 'create')
            ->notEmpty('msg');

        $validator
            ->allowEmpty('data');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function findServerSideData(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        // `id`, `created`, `detail`, `user_id`, `action`, `customer_code`

        $columns = [
            'ActionLog.id',            //0
            'ActionLog.created',       //1
            'ActionLog.user_id',       //2
            'ActionLog.action',        //3
            'ActionLog.customer_code', //4
        ];

        $columns_select = [
            'ActionLog.id',            //0
            'ActionLog.created',       //1
            'ActionLog.detail',        //2
            'ActionLog.user_id',       //3
            'ActionLog.action',        //4
            'ActionLog.customer_code', //5
            'Customers.code',          //6
            'Customers.name',          //7
            'Customers.doc_type',      //8
            'Customers.ident',         //9
            'Users.id',                //10
            'Users.name',              //11
            'ActionLog.main',          //12
        ];

        // paginacion

        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        // ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] =>  $o['dir']];
        }

        $query = $query->contain([
            'Users', 
            'Customers'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                // `id`, `created`, `detail`, `user_id`, `action`, `customer_code`

                switch ($columns[$index]) {

                    case 'ActionLog.created': 

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2]  . '-' . $value[1][1] . '-' .  $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0].' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'ActionLog.user_id':
                    case 'ActionLog.id':
                    case 'ActionLog.customer_code':

                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    default: 
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            // `id`, `created`, `detail`, `user_id`, `action`, `customer_code`

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {
                    case 'ActionLog.created': 
                        break;
                    case 'ActionLog.user_id' :
                    case 'ActionLog.customer_code':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;
                    case 'ActionLog.detail':
                    case 'ActionLog.action':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order($order);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        return $query;
    }

    public function findRecordsFiltered(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        // `id`, `created`, `detail`, `user_id`, `action`, `customer_code`

        $columns = [
            'ActionLog.id',            //0
            'ActionLog.created',       //1
            'ActionLog.user_id',       //2
            'ActionLog.action',        //3
            'ActionLog.customer_code', //4
        ];

        $columns_select = [
            'ActionLog.id',            //0
            'ActionLog.created',       //1
            'ActionLog.detail',        //2
            'ActionLog.user_id',       //3
            'ActionLog.action',        //4
            'ActionLog.customer_code', //5
            'Customers.code',          //6
            'Customers.name',          //7
            'Customers.doc_type',      //8
            'Customers.ident',         //9
            'Users.id',                //10
            'Users.name',              //11
            'ActionLog.main',          //12
        ];

        //paginacion

        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] => $o['dir']];
        }

        $query = $query->contain([
            'Users', 
            'Customers'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if (isset($options['where'])) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                // `id`, `created`, `detail`, `user_id`, `action`, `customer_code`

                switch ($columns[$index]) {

                    case 'ActionLog.created': 

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2]  . '-' . $value[1][1] . '-' .  $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'ActionLog.user_id': 
                    case 'ActionLog.id':
                    case 'ActionLog.customer_code':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    default: 
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);

        //busqueda general
        if ($params['search']['value'] != '') {

            // `id`, `created`, `detail`, `user_id`, `action`, `customer_code`

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {
                    case 'ActionLog.created': 
                        break;

                    case 'ActionLog.user_id':
                    case 'ActionLog.customer_code':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'ActionLog.detail':
                    case 'ActionLog.action':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order($order);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        return $query->count();
    }
}
