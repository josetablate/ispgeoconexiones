<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Log\Log;

/**
 * Presupuestos Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\ConnectionsTable|\Cake\ORM\Association\BelongsTo $Connections
 * @property \App\Model\Table\BusinessesTable|\Cake\ORM\Association\BelongsTo $Businesses
 * @property \App\Model\Table\PresupuestoConceptsTable|\Cake\ORM\Association\HasMany $PresupuestoConcepts
 *
 * @method \App\Model\Entity\Presupuesto get($primaryKey, $options = [])
 * @method \App\Model\Entity\Presupuesto newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Presupuesto[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Presupuesto|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Presupuesto|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Presupuesto patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Presupuesto[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Presupuesto findOrCreate($search, callable $callback = null, $options = [])
 */
class PresupuestosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('presupuestos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Connections', [
            'foreignKey' => 'connection_id',
        ]);
        // $this->belongsTo('Businesses', [
        //     'foreignKey' => 'business_id',
        //     'joinType' => 'INNER'
        // ]);
        $this->hasMany('PresupuestoConcepts', [
            'foreignKey' => 'presupuesto_id'
        ]);
        
          $this->belongsTo('Customers', [
            'foreignKey' => 'customer_code',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('concept_type')
            ->requirePresence('concept_type', 'create')
            ->notEmpty('concept_type');

        $validator
            ->dateTime('date')
            ->requirePresence('date', 'create')
            ->notEmpty('date');

        $validator
            ->dateTime('date_start')
            ->allowEmpty('date_start');

        $validator
            ->dateTime('date_end')
            ->allowEmpty('date_end');

        $validator
            ->dateTime('duedate')
            ->requirePresence('duedate', 'create')
            ->notEmpty('duedate');

        $validator
            ->scalar('tipo_comp')
            ->maxLength('tipo_comp', 45)
            ->requirePresence('tipo_comp', 'create')
            ->notEmpty('tipo_comp');

        $validator
            ->integer('pto_vta')
            ->requirePresence('pto_vta', 'create')
            ->notEmpty('pto_vta');

        $validator
            ->integer('num')
            ->allowEmpty('num');

        $validator
            ->decimal('subtotal')
            ->requirePresence('subtotal', 'create')
            ->notEmpty('subtotal');

        $validator
            ->decimal('sum_tax')
            ->requirePresence('sum_tax', 'create')
            ->notEmpty('sum_tax');

        $validator
            ->decimal('discount')
            ->requirePresence('discount', 'create')
            ->notEmpty('discount');

        $validator
            ->decimal('total')
            ->requirePresence('total', 'create')
            ->notEmpty('total');

        $validator
            ->scalar('company_name')
            ->maxLength('company_name', 145)
            ->requirePresence('company_name', 'create')
            ->notEmpty('company_name');

        $validator
            ->scalar('company_address')
            ->maxLength('company_address', 200)
            ->requirePresence('company_address', 'create')
            ->notEmpty('company_address');

        $validator
            ->scalar('company_cp')
            ->maxLength('company_cp', 5)
            ->requirePresence('company_cp', 'create')
            ->notEmpty('company_cp');

        $validator
            ->scalar('company_city')
            ->maxLength('company_city', 145)
            ->requirePresence('company_city', 'create')
            ->notEmpty('company_city');

        $validator
            ->scalar('company_phone')
            ->maxLength('company_phone', 45)
            ->requirePresence('company_phone', 'create')
            ->notEmpty('company_phone');

        $validator
            ->scalar('company_fax')
            ->maxLength('company_fax', 45)
            ->allowEmpty('company_fax');

        $validator
            ->scalar('company_ident')
            ->maxLength('company_ident', 45)
            ->requirePresence('company_ident', 'create')
            ->notEmpty('company_ident');

        $validator
            ->scalar('company_email')
            ->maxLength('company_email', 45)
            ->allowEmpty('company_email');

        $validator
            ->scalar('company_web')
            ->maxLength('company_web', 45)
            ->allowEmpty('company_web');

        $validator
            ->integer('company_responsible')
            ->allowEmpty('company_responsible');

        $validator
            ->integer('customer_code')
            ->requirePresence('customer_code', 'create')
            ->notEmpty('customer_code');

        $validator
            ->scalar('customer_name')
            ->maxLength('customer_name', 145)
            ->allowEmpty('customer_name');

        $validator
            ->scalar('customer_address')
            ->maxLength('customer_address', 145)
            ->allowEmpty('customer_address');

        $validator
            ->scalar('customer_cp')
            ->maxLength('customer_cp', 45)
            ->allowEmpty('customer_cp');

        $validator
            ->scalar('customer_city')
            ->maxLength('customer_city', 45)
            ->allowEmpty('customer_city');

        $validator
            ->scalar('customer_country')
            ->maxLength('customer_country', 45)
            ->allowEmpty('customer_country');

        $validator
            ->scalar('customer_ident')
            ->maxLength('customer_ident', 45)
            ->allowEmpty('customer_ident');

        $validator
            ->integer('customer_doc_type')
            ->allowEmpty('customer_doc_type');

        $validator
            ->integer('customer_responsible')
            ->allowEmpty('customer_responsible');

        $validator
            ->scalar('cond_vta')
            ->maxLength('cond_vta', 45)
            ->requirePresence('cond_vta', 'create')
            ->notEmpty('cond_vta');

        $validator
            ->scalar('comments')
            ->maxLength('comments', 200)
            ->allowEmpty('comments');

        $validator
            ->scalar('printed')
            ->maxLength('printed', 225)
            ->allowEmpty('printed');

        $validator
            ->scalar('company_ing_bruto')
            ->maxLength('company_ing_bruto', 45)
            ->allowEmpty('company_ing_bruto');

        $validator
            ->scalar('company_init_act')
            ->maxLength('company_init_act', 45)
            ->allowEmpty('company_init_act');

        $validator
            ->scalar('period')
            ->maxLength('period', 45)
            ->allowEmpty('period');

        return $validator;
    }
    
    public function findServerSideData(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;
        
        $columns = [
            'Presupuestos.date',             //0
            'Presupuestos.pto_vta',          //1
            'Presupuestos.num',              //2
            'Presupuestos.comments',         //3
            'Presupuestos.duedate',          //4
            'Presupuestos.subtotal',         //5
            'Presupuestos.sum_tax',          //6
            'Presupuestos.total',            //7
            'Users.name',                    //8
            'Presupuestos.customer_code',    //9
            'Presupuestos.customer_doc_type',//10
            'Presupuestos.customer_ident',   //11
            'Presupuestos.customer_name',    //12
            'Presupuestos.customer_address', //13
            'Presupuestos.customer_city',    //14
            'Presupuestos.business_id',      //15
          
        ];

        $columns_select = [
            
            'Presupuestos.date',             //0
            'Presupuestos.pto_vta',          //1
            'Presupuestos.num',              //2
            'Presupuestos.comments',         //3
            'Presupuestos.duedate',          //4
            'Presupuestos.subtotal',         //5
            'Presupuestos.sum_tax',          //6
            'Presupuestos.total',            //7
            'Users.name',                    //8
            'Presupuestos.customer_code',    //9
            'Presupuestos.customer_doc_type',//10
            'Presupuestos.customer_ident',   //11
            'Presupuestos.customer_name',    //12
            'Presupuestos.customer_address', //13
            'Presupuestos.customer_city',    //14
            'Presupuestos.business_id',      //15
            
            'Presupuestos.id',
            'Customers.billing_for_service',
            'Customers.email',
            'Presupuestos.connection_id',
        ];

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] => $o['dir']];
        }

        $query = $query->contain([
            'Users',
            'Customers'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Presupuestos.date': 
                    case 'Presupuestos.duedate':
            
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                            
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

            
                    case 'Presupuestos.pto_vta': 
                    case 'Presupuestos.num': 
                    case 'Presupuestos.customer_code':
                    case 'Presupuestos.business_id':
                    case 'Presupuestos.customer_ident':
                    case 'Presupuestos.customer_doc_type':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Presupuestos.subtotal':
                    case 'Presupuestos.sum_tax':
                    case 'Presupuestos.total':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;    
    
                    case 'Users.name':
                    case 'Presupuestos.customer_name':
                    case 'Presupuestos.comments':
                    case 'Presupuestos.customer_address':
                    case 'Presupuestos.customer_city':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);
 
        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Presupuestos.num':
                    case 'Presupuestos.customer_code':
                    case 'Presupuestos.customer_ident':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Presupuestos.customer_name':
                    case 'Presupuestos.comments':
                    case 'Presupuestos.customer_address':
                    case 'Presupuestos.customer_city':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order($order);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        $query->toArray();

        return $query;
    }

    public function findRecordsFiltered(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;
        $columns = [
            'Presupuestos.date',             //0
            'Presupuestos.pto_vta',          //1
            'Presupuestos.num',              //2
            'Presupuestos.comments',         //3
            'Presupuestos.duedate',          //4
            'Presupuestos.subtotal',         //5
            'Presupuestos.sum_tax',          //6
            'Presupuestos.total',            //7
            'Users.name',                    //8
            'Presupuestos.customer_code',    //9
            'Presupuestos.customer_doc_type',//10
            'Presupuestos.customer_ident',   //11
            'Presupuestos.customer_name',    //12
            'Presupuestos.customer_address', //13
            'Presupuestos.customer_city',    //14
            'Presupuestos.business_id',      //15
          
        ];

        $columns_select = [
            
            'Presupuestos.date',             //0
            'Presupuestos.pto_vta',          //1
            'Presupuestos.num',              //2
            'Presupuestos.comments',         //3
            'Presupuestos.duedate',          //4
            'Presupuestos.subtotal',         //5
            'Presupuestos.sum_tax',          //6
            'Presupuestos.total',            //7
            'Users.name',                    //8
            'Presupuestos.customer_code',    //9
            'Presupuestos.customer_doc_type',//10
            'Presupuestos.customer_ident',   //11
            'Presupuestos.customer_name',    //12
            'Presupuestos.customer_address', //13
            'Presupuestos.customer_city',    //14
            'Presupuestos.business_id',      //15
            
            'Presupuestos.id',
            'Customers.billing_for_service',
            'Customers.email',
            'Presupuestos.connection_id',
        ];

        $query = $query->contain([
            'Users',
            'Customers'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Presupuestos.date': 
                    case 'Presupuestos.duedate':
            
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                            
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

            
                    case 'Presupuestos.pto_vta': 
                    case 'Presupuestos.num': 
                    case 'Presupuestos.customer_code':
                    case 'Presupuestos.business_id':
                    case 'Presupuestos.customer_ident':
                    case 'Presupuestos.customer_doc_type':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Presupuestos.subtotal':
                    case 'Presupuestos.sum_tax':
                    case 'Presupuestos.total':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;    
    
                    case 'Users.name':
                    case 'Presupuestos.customer_name':
                    case 'Presupuestos.comments':
                    case 'Presupuestos.customer_address':
                    case 'Presupuestos.customer_city':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);
 
        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Presupuestos.num':
                    case 'Presupuestos.customer_code':
                    case 'Presupuestos.customer_ident':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Presupuestos.customer_name':
                    case 'Presupuestos.comments':
                    case 'Presupuestos.customer_address':
                    case 'Presupuestos.customer_city':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }
    
    public function findServerSideDataCustomer(Query $query, array $options)
    {
        $params = $options['params'];
        
        // Log::debug($params);

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;
        
        $columns = [
            'Presupuestos.date',             //0
            'Users.name',                    //1
            'Presupuestos.business_id',      //2
            'Presupuestos.pto_vta',          //3
            'Presupuestos.num',              //4
            'Presupuestos.comments',         //5
            'Presupuestos.customer_address', //6
            'Presupuestos.customer_city',    //7
            'Presupuestos.duedate',          //8
            'Presupuestos.subtotal',         //9
            'Presupuestos.sum_tax',          //10
            'Presupuestos.total',            //11
            
          
        ];

        $columns_select = [
            
            'Presupuestos.date',             //0
            'Users.name',                    //1
            'Presupuestos.business_id',      //2
            'Presupuestos.pto_vta',          //3
            'Presupuestos.num',              //4
            'Presupuestos.comments',         //5
            'Presupuestos.customer_address', //6
            'Presupuestos.customer_city',    //7
            'Presupuestos.duedate',          //8
            'Presupuestos.subtotal',         //9
            'Presupuestos.sum_tax',          //10
            'Presupuestos.total',            //11
            
            'Presupuestos.id',
            'Customers.billing_for_service',
            'Customers.email',
            'Presupuestos.connection_id',
        ];

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        // foreach ($params['order'] as $o) {
        //     $order += [$columns[$o['column']] => $o['dir']];
        // }

        $query = $query->contain([
            'Users',
            'Customers'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Presupuestos.date': 
                    case 'Presupuestos.duedate':
            
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                            
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

            
                    case 'Presupuestos.pto_vta': 
                    case 'Presupuestos.num': 
                    case 'Presupuestos.business_id':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Presupuestos.subtotal':
                    case 'Presupuestos.sum_tax':
                    case 'Presupuestos.total':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;    
    
                    case 'Users.name':
                    case 'Presupuestos.comments':
                    case 'Presupuestos.customer_address':
                    case 'Presupuestos.customer_city':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);
 
        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Presupuestos.num':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;
  
                    case 'Presupuestos.comments':
                    case 'Presupuestos.customer_address':
                    case 'Presupuestos.customer_city':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order(['Presupuestos.date' => 'DESC']);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        $query->toArray();

        return $query;
    }

    public function findRecordsFilteredCustomer(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;
        
        $columns = [
            'Presupuestos.date',             //0
            'Users.name',                    //1
            'Presupuestos.business_id',      //2
            'Presupuestos.pto_vta',          //3
            'Presupuestos.num',              //4
            'Presupuestos.comments',         //5
            'Presupuestos.customer_address', //6
            'Presupuestos.customer_city',    //7
            'Presupuestos.duedate',          //8
            'Presupuestos.subtotal',         //9
            'Presupuestos.sum_tax',          //10
            'Presupuestos.total',            //11
            
          
        ];

        $columns_select = [
            
            'Presupuestos.date',             //0
            'Users.name',                    //1
            'Presupuestos.business_id',      //2
            'Presupuestos.pto_vta',          //3
            'Presupuestos.num',              //4
            'Presupuestos.comments',         //5
            'Presupuestos.customer_address', //6
            'Presupuestos.customer_city',    //7
            'Presupuestos.duedate',          //8
            'Presupuestos.subtotal',         //9
            'Presupuestos.sum_tax',          //10
            'Presupuestos.total',            //11
            
            'Presupuestos.id',
            'Customers.billing_for_service',
            'Customers.email',
            'Presupuestos.connection_id',
        ];

        $query = $query->contain([
            'Users',
            'Customers'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Presupuestos.date': 
                    case 'Presupuestos.duedate':
            
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                            
                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

            
                    case 'Presupuestos.pto_vta': 
                    case 'Presupuestos.num': 
                    case 'Presupuestos.business_id':
    
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Presupuestos.subtotal':
                    case 'Presupuestos.sum_tax':
                    case 'Presupuestos.total':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;    
    
                    case 'Users.name':
                    case 'Presupuestos.comments':
                    case 'Presupuestos.customer_address':
                    case 'Presupuestos.customer_city':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);
 
        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Presupuestos.num':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;
    
                    case 'Presupuestos.comments':
                    case 'Presupuestos.customer_address':
                    case 'Presupuestos.customer_city':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        return $rules;
    }

    public function findServerSideDataComprobantes(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            '',                              //0
            'Presupuestos.date',             //1
            'Presupuestos.date_Start',       //2
            'Presupuestos.date_end',         //3
            'Presupuestos.tipo_comp',        //4
            'Presupuestos.pto_vta',          //5
            'Presupuestos.num',              //6
            'Presupuestos.comments',         //7
            'Presupuestos.duedate',          //8
            'Presupuestos.total',            //9
            'Users.name',                    //10
            'Presupuestos.customer_code',    //11
            'Presupuestos.customer_doc_type',//12
            'Presupuestos.customer_ident',   //13
            'Presupuestos.customer_name',    //14
            'Customers.email',               //15
            'Presupuestos.customer_address', //16
            'Presupuestos.customer_city',    //17
            '',                              //18
            'Presupuestos.business_id',      //19
        ];

        $columns_select = [
            'Presupuestos.id',               //0
            'Presupuestos.date',             //1
            'Presupuestos.date_Start',       //2
            'Presupuestos.date_end',         //3
            'Presupuestos.tipo_comp',        //4
            'Presupuestos.pto_vta',          //5
            'Presupuestos.num',              //6
            'Presupuestos.comments',         //7
            'Presupuestos.duedate',          //8
            'Presupuestos.total',            //9
            'Users.name',                    //10
            'Presupuestos.customer_code',    //11
            'Presupuestos.customer_doc_type',//12
            'Presupuestos.customer_ident',   //13
            'Presupuestos.customer_name',    //14
            'Customers.email',               //15
            'Presupuestos.customer_address', //16
            'Presupuestos.customer_city',    //17
            'Presupuestos.business_id',      //18
            'Customers.billing_for_service', //19
            'Presupuestos.connection_id',    //20
        ];

        //paginacion
        if ($params['length'] > 0) {

            if ($params['start'] > 0) {
                $page = $params['start'] / $params['length']; 
                $page++;
            }
        }

        //ordenamiento
        foreach ($params['order'] as $o) {
            $order += [$columns[$o['column']] => $o['dir']];
        }

        $query = $query->contain([
            'Users',
            'Customers'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Presupuestos.date':
                    case 'Presupuestos.date_start':
                    case 'Presupuestos.date_end':
                    case 'Presupuestos.duedate':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Presupuestos.pto_vta':
                    case 'Presupuestos.num':
                    case 'Presupuestos.customer_code':
                    case 'Presupuestos.business_id':
                    case 'Presupuestos.customer_ident':
                    case 'Presupuestos.customer_doc_type':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Presupuestos.total':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;    

                    case 'Users.name':
                    case 'Presupuestos.customer_name':
                    case 'Presupuestos.comments':
                    case 'Presupuestos.customer_address':
                    case 'Presupuestos.customer_city':
                    case 'Customers.email':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);
 
        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Presupuestos.num':
                    case 'Presupuestos.customer_code':
                    case 'Presupuestos.customer_ident':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Presupuestos.customer_name':
                    case 'Presupuestos.comments':
                    case 'Presupuestos.customer_address':
                    case 'Presupuestos.customer_city':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        $query->order($order);

        if ($limit > 0) {
            $query->limit($limit)->page($page);
        }

        $query->toArray();

        return $query;
    }

    public function findRecordsFilteredComprobantes(Query $query, array $options)
    {
        $params = $options['params'];

        $order = [];
        $where = [];
        $between = [];
        $orWhere = [];
        $limit = $params['length']; 
        $page = 1;

        $columns = [
            '',                              //0
            'Presupuestos.date',             //1
            'Presupuestos.date_Start',       //2
            'Presupuestos.date_end',         //3
            'Presupuestos.tipo_comp',        //4
            'Presupuestos.pto_vta',          //5
            'Presupuestos.num',              //6
            'Presupuestos.comments',         //7
            'Presupuestos.duedate',          //8
            'Presupuestos.total',            //9
            'Users.name',                    //10
            'Presupuestos.customer_code',    //11
            'Presupuestos.customer_doc_type',//12
            'Presupuestos.customer_ident',   //13
            'Presupuestos.customer_name',    //14
            'Customers.email',               //15
            'Presupuestos.customer_address', //16
            'Presupuestos.customer_city',    //17
            '',                              //18
            'Presupuestos.business_id',      //19
        ];

        $columns_select = [
            'Presupuestos.id',               //0
            'Presupuestos.date',             //1
            'Presupuestos.date_Start',       //2
            'Presupuestos.date_end',         //3
            'Presupuestos.tipo_comp',        //4
            'Presupuestos.pto_vta',          //5
            'Presupuestos.num',              //6
            'Presupuestos.comments',         //7
            'Presupuestos.duedate',          //8
            'Presupuestos.total',            //9
            'Users.name',                    //10
            'Presupuestos.customer_code',    //11
            'Presupuestos.customer_doc_type',//12
            'Presupuestos.customer_ident',   //13
            'Presupuestos.customer_name',    //14
            'Customers.email',               //15
            'Presupuestos.customer_address', //16
            'Presupuestos.customer_city',    //17
            'Presupuestos.business_id',      //18
            'Customers.billing_for_service', //19
            'Presupuestos.connection_id',    //20
        ];

        $query = $query->contain([
            'Users',
            'Customers'
        ])
        ->select($columns_select);

        //where extra o por defecto
        if ($options['where']) {
            $where += $options['where'];
        }

        //busqueda por columna
        foreach ($params['columns'] as $index => $column) {

            $column['search']['value'] = trim($column['search']['value']);

            if ($column['search']['value'] != '') {

                switch ($columns[$index]) {

                    case 'Presupuestos.date':
                    case 'Presupuestos.date_start':
                    case 'Presupuestos.date_end':
                    case 'Presupuestos.duedate':

                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {
                            $value[0] = explode('/', $value[0]);
                            $value[1] = explode('/', $value[1]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];

                        } else if ($value[0] != '') {
                            $value[0] = explode('/', $value[0]);
                            $value[0] = $value[0][2] . '-' . $value[0][1] . '-' . $value[0][0] . ' 00:00:00';
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = explode('/', $value[1]);
                            $value[1] = $value[1][2] . '-' . $value[1][1] . '-' . $value[1][0] . ' 23:59:59';
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;

                    case 'Presupuestos.pto_vta':
                    case 'Presupuestos.num':
                    case 'Presupuestos.customer_code':
                    case 'Presupuestos.business_id':
                    case 'Presupuestos.customer_ident':
                    case 'Presupuestos.customer_doc_type':
                        $where += [$columns[$index] => $column['search']['value']];
                        break;

                    case 'Presupuestos.total':
                        $value = explode('<>', $column['search']['value']); 

                        $value[0] = trim($value[0]);
                        $value[1] = trim($value[1]);

                        if ($value[0] != '' && $value[1] != '' ) {

                            $value[0] = floatval($value[0]);
                            $value[1] = floatval($value[1]);
                            $between[] = ['field' => $columns[$index], 'from' => $value[0],  'to' => $value[1]];
                        } else if ($value[0] != '') {
                            $value[0] = floatval($value[0]);
                            $where += [$columns[$index] . ' >=' => $value[0]];
                        } else if ($value[1] != '') {
                            $value[1] = floatval($value[1]);
                            $where += [$columns[$index] . ' <=' => $value[1]];
                        }
                        break;    

                    case 'Users.name':
                    case 'Presupuestos.customer_name':
                    case 'Presupuestos.comments':
                    case 'Presupuestos.customer_address':
                    case 'Presupuestos.customer_city':
                    case 'Customers.email':
                        $where += [$columns[$index] . ' LIKE ' => '%' . $column['search']['value'] . '%'];
                        break;
                }
            }
        }

        $params['search']['value'] = trim($params['search']['value']);
 
        //busqueda general
        if ($params['search']['value'] != '') {

            foreach ($columns as $index => $column) {

                switch ($columns[$index]) {

                    case 'Presupuestos.num':
                    case 'Presupuestos.customer_code':
                    case 'Presupuestos.customer_ident':
                        $orWhere += [$columns[$index] => $params['search']['value']];
                        break;

                    case 'Presupuestos.customer_name':
                    case 'Presupuestos.comments':
                    case 'Presupuestos.customer_address':
                    case 'Presupuestos.customer_city':
                        $orWhere += [$columns[$index] . ' LIKE ' => '%' . $params['search']['value'] . '%'];
                        break;
                }
            }
        }

        if ($where) {
            $query->where($where);
        }

        if (count($between) > 0) {

            foreach ($between as $b) {

                $query->where(function ($exp) use ($b) {
                    return $exp->between($b['field'], $b['from'], $b['to']);
                });
            }
        }

        if ($orWhere) {
            $query->andWhere(['OR' => $orWhere]);
        }

        return $query->count();
    }    
}
