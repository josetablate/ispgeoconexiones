<?php
namespace App\Model\Table\MikrotikPppoeTa;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TPppServers Model
 *
 * @property \Cake\ORM\Association\BelongsTo $TIntegrationControllers
 *
 * @method \App\Model\Entity\TPppServer get($primaryKey, $options = [])
 * @method \App\Model\Entity\TPppServer newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TPppServer[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TPppServer|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TPppServer patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TPppServer[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TPppServer findOrCreate($search, callable $callback = null)
 */
class PppoeServersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
         
        $this->setEntityClass('App\Model\Entity\MikrotikPppoeTa\PppoeServer');    

        $this->setTable('pppoe_servers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('MikrotikPppoeTa_Controllers', [
            'foreignKey' => 'controller_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('interface', 'create')
            ->notEmpty('interface');

        $validator
            ->requirePresence('one_session_per_host', 'create')
            ->notEmpty('one_session_per_host');

        $validator
            ->requirePresence('authentication', 'create')
            ->notEmpty('authentication');

        $validator
            ->requirePresence('service_name', 'create')
            ->notEmpty('service_name');

        $validator
            ->requirePresence('disabled', 'create')
            ->notEmpty('disabled');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['controller_id'], 'MikrotikPppoeTa_Controllers'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'ispbrain_mikrotik_pppoe_ta';
    }
}
