<?php
namespace App\Model\Table\MikrotikPppoeTa;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Profiles Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Controllers
 * @property \Cake\ORM\Association\HasMany $Plans
 *
 * @method \App\Model\Entity\Profile get($primaryKey, $options = [])
 * @method \App\Model\Entity\Profile newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Profile[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Profile|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Profile patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Profile[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Profile findOrCreate($search, callable $callback = null)
 */
class ProfilesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
         
        $this->setEntityClass('App\Model\Entity\MikrotikPppoeTa\Profile');  

        $this->setTable('profiles');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('MikrotikPppoeTa_Controllers', [
            'foreignKey' => 'controller_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('MikrotikPppoeTa_Plans', [
            'foreignKey' => 'profile_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->notEmpty('local_address');

        $validator
            ->allowEmpty('dns_server');

       
        $validator
            ->allowEmpty('down');

        $validator
            ->allowEmpty('up');

        $validator
            ->allowEmpty('down_burst');

        $validator
            ->allowEmpty('up_burst');

        $validator
            ->allowEmpty('down_threshold');

        $validator
            ->allowEmpty('up_threshold');

        $validator
            ->allowEmpty('down_time');

        $validator
            ->allowEmpty('up_time');

        $validator
            ->allowEmpty('priority');

        $validator
            ->allowEmpty('down_at_limit');

        $validator
            ->allowEmpty('up_at_limit');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['controller_id'], 'MikrotikPppoeTa_Controllers'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'ispbrain_mikrotik_pppoe_ta';
    }
}
