<?php
namespace App\Model\Table\MikrotikPppoeTa;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Rule\IsUnique;

/**
 * TSecrets Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Connections
 * @property \Cake\ORM\Association\BelongsTo $TControllers
 * @property \Cake\ORM\Association\BelongsTo $Apis
 * @property \Cake\ORM\Association\BelongsTo $TPools
 * @property \Cake\ORM\Association\BelongsTo $TProfiles
 * @property \Cake\ORM\Association\BelongsTo $TPlans
 *
 * @method \App\Model\Entity\TSecret get($primaryKey, $options = [])
 * @method \App\Model\Entity\TSecret newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TSecret[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TSecret|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TSecret patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TSecret[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TSecret findOrCreate($search, callable $callback = null)
 */
class SecretsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
         
        $this->setEntityClass('App\Model\Entity\MikrotikPppoeTa\Secret'); 

        $this->setTable('secrets');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
        
        $this->belongsTo('MikrotikPppoeTa_Controllers', [
            'foreignKey' => 'controller_id',
            'joinType' => 'INNER',
            'propertyName' => 'controller'
        ]);
    
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        $validator
            ->requirePresence('service', 'create')
            ->notEmpty('service');

        $validator
            ->integer('local_address')
            ->allowEmpty('local_address');

        $validator
            ->integer('remote_address')
            ->requirePresence('remote_address', 'create')
            ->notEmpty('remote_address');

        $validator
            ->allowEmpty('last_logged_out');

        $validator
            ->allowEmpty('comment');
   

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        //$rules->add($rules->isUnique(['name'], 'El nombre de secrets ya existe en la base de datos'));
        // $rules->add($rules->isUnique(['remote_address'], 'Esta IP ya existe en la base de datos'));
        
        $rules->add($rules->existsIn(['controller_id'], 'MikrotikPppoeTa_Controllers'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'ispbrain_mikrotik_pppoe_ta';
    }
}
