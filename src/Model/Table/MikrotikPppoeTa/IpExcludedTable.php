<?php
namespace App\Model\Table\MikrotikPppoeTa;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;


class IpExcludedTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);
         
        $this->setEntityClass('App\Model\Entity\MikrotikPppoeTa\IpExcluded');

        $this->setTable('ip_excluded');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        
        $this->belongsTo('MikrotikPppoeTa_Controllers', [
            'foreignKey' => 'controller_id',
            'joinType' => 'INNER'
        ]);

       
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('ip', 'create')
            ->notEmpty('ip');
            
        $validator
            ->allowEmpty('comments');
            

        return $validator;
    }

    
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['controller_id'], 'MikrotikPppoeTa_Controllers'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'ispbrain_mikrotik_pppoe_ta';
    }
}
