<?php
namespace App\Model\Table\MikrotikPppoeTa;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;


class PlansTable extends Table
{
     
    public function initialize(array $config)
    {  
        parent::initialize($config);
         
        $this->setEntityClass('App\Model\Entity\MikrotikPppoeTa\Plan');

        $this->setTable('plans');
        
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        
        $this->belongsTo('MikrotikPppoeTa_Profiles', [
            'foreignKey' => 'profile_id',
            'joinType' => 'INNER',
            'propertyName' => 'profile'
        ]);
        
        $this->belongsTo('MikrotikPppoeTa_Controllers', [
            'foreignKey' => 'controller_id',
            'joinType' => 'INNER',            
        ]);
        
        $this->belongsTo('MikrotikPppoeTa_Pools', [
            'foreignKey' => 'pool_id',
            'joinType' => 'LEFT',
            'propertyName' => 'pool'
        ]);
        
        
        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['controller_id'], 'MikrotikPppoeTa_Controllers'));
        $rules->add($rules->existsIn(['profile_id'], 'MikrotikPppoeTa_Profiles'));
        // $rules->add($rules->existsIn(['pool_id'], 'TPools'));
  

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'ispbrain_mikrotik_pppoe_ta';
    }
   
}
