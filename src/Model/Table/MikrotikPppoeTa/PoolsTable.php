<?php
namespace App\Model\Table\MikrotikPppoeTa;


use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;


/**

 * Pools Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Pools
 * @property \Cake\ORM\Association\BelongsTo $Controllers
 * @property \Cake\ORM\Association\HasMany $Plans
 *
 * @method \App\Model\Entity\Pool get($primaryKey, $options = [])
 * @method \App\Model\Entity\Pool newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Pool[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Pool|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Pool patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Pool[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Pool findOrCreate($search, callable $callback = null)
 */
class PoolsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {       
        
        parent::initialize($config);
         
        $this->setEntityClass('App\Model\Entity\MikrotikPppoeTa\Pool');        
         
        $this->setTable('pools');
         
        $this->setDisplayField('nameAndAddresses');
        $this->setPrimaryKey('id');

  
        $this->belongsTo('MikrotikPppoeTa_Controllers', [
            'foreignKey' => 'controller_id',
            'joinType' => 'INNER'
        ]);
        
        $this->hasMany('MikrotikPppoeTa_Plans', [
            'foreignKey' => 'pool_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('addresses', 'create')
            ->notEmpty('addresses');

        $validator
            ->requirePresence('min_host', 'create')
            ->notEmpty('min_host');

        $validator
            ->requirePresence('max_host', 'create')
            ->notEmpty('max_host');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
//         $rules->add($rules->isUnique(['addresses'], 'Existe otro pool con este addresses.'));
        // $rules->add($rules->isUnique(['name'], 'Existe otro pool con este nombre.'));
        // $rules->add($rules->existsIn(['next_pool_id'], 'Pools'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'ispbrain_mikrotik_pppoe_ta';
    }
}
