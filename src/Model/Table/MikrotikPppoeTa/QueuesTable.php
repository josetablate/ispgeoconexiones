<?php
namespace App\Model\Table\MikrotikPppoeTa;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Rule\IsUnique;

/**
 * TSecrets Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Connections
 * @property \Cake\ORM\Association\BelongsTo $TControllers
 * @property \Cake\ORM\Association\BelongsTo $Apis
 * @property \Cake\ORM\Association\BelongsTo $TPools
 * @property \Cake\ORM\Association\BelongsTo $TProfiles
 * @property \Cake\ORM\Association\BelongsTo $TPlans
 *
 * @method \App\Model\Entity\TSecret get($primaryKey, $options = [])
 * @method \App\Model\Entity\TSecret newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TSecret[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TSecret|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TSecret patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TSecret[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TSecret findOrCreate($search, callable $callback = null)
 */
class QueuesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
         
        $this->setEntityClass('App\Model\Entity\MikrotikPppoeTa\Queue');  

        $this->setTable('queues');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        
        $this->belongsTo('MikrotikPppoeTa_Controllers', [
            'foreignKey' => 'controller_id',
            'joinType' => 'INNER',
            'propertyName' => 'controller'
        ]);
     
        $this->belongsTo('MikrotikPppoeTa_Pools', [
            'foreignKey' => 'pool_id',
            'joinType' => 'LEFT',
            'propertyName' => 'pool'
        ]);
        $this->belongsTo('MikrotikPppoeTa_Profiles', [
            'foreignKey' => 'profile_id',
            'joinType' => 'INNER',
            'propertyName' => 'profile'
        ]);
        $this->belongsTo('MikrotikPppoeTa_Plans', [
            'foreignKey' => 'plan_id',
            'joinType' => 'INNER',
            'propertyName' => 'plan'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->integer('target')
            ->requirePresence('target', 'create')
            ->notEmpty('target');

        $validator
            ->allowEmpty('comment');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
//         $rules->add($rules->isUnique(['name'], 'El nombre de queues ya existe en la base de datos'));
//         $rules->add($rules->isUnique(['target'], 'Esta IP ya existe en la base de datos'));
        
        $rules->add($rules->existsIn(['controller_id'], 'MikrotikPppoeTa_Controllers'));
        $rules->add($rules->existsIn(['pool_id'], 'MikrotikPppoeTa_Pools'));
        $rules->add($rules->existsIn(['profile_id'], 'MikrotikPppoeTa_Profiles'));
        $rules->add($rules->existsIn(['plan_id'], 'MikrotikPppoeTa_Plans'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'ispbrain_mikrotik_pppoe_ta';
    }
}
