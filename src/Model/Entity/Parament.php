<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Parament Entity
 *
 * @property int $id
 * @property bool $integration_enable
 * @property float $lat_center
 * @property float $lng_center
 * @property string $cutomer_type_tax_default
 * @property string $customer_type_bill_default
 * @property bool $customer_validate_dni_enable
 * @property bool $ppp_user_pass_auto
 * @property int $conecction_locker_date
 * @property string $business_type_tax
 * @property int $business_cp
 * @property string $business_name
 * @property string $business_address
 * @property string $business_phone
 * @property string $business_type
 * @property string $business_cuit
 * @property string $business_ing_bruto
 * @property \Cake\I18n\Time $business_ini_act
 * @property string $business_state
 * @property string $bill_due_type
 * @property int $bill_due_date
 * @property int $point_sale
 * @property int $bill_init_number_fx
 * @property int $bill_init_number_fa
 * @property int $bill_init_number_fb
 * @property int $bill_init_number_fc
 * @property int $bill_init_number_r
 * @property bool $calculate_proportional
 * @property bool $fix_saldo_change_plan
 * @property float $interest_moroso_day
 * @property float $interest_due_2
 * @property float $interest_due_3
 * @property float $interest_due_4
 * @property float $interest_due_5
 * @property float $interest_due_6
 * @property int $send_forewarning_date
 * @property bool $avisos_auto
 * @property float $debt_cut
 * @property string $ip_server
 * @property string $message_corte_title
 * @property string $message_corte_des
 * @property string $message_venc_title
 * @property string $message_venc_des
 * @property string $message_info_title
 * @property string $message_info_des
 * @property string $cashs_account_parent
 * @property string $customer_account_parent
 * @property string $service_internet_account_parent
 * @property string $bonif_account_code
 * @property string $recarg_account_code
 * @property string $packages_account_parent
 * @property string $products_account_parent
 * @property string $bonif_services_account_code
 */
class Parament extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
