<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MpTransaction Entity
 *
 * @property int $id
 * @property int $payment_id
 * @property \Cake\I18n\FrozenTime $date_created
 * @property \Cake\I18n\FrozenTime $date_approved
 * @property \Cake\I18n\FrozenTime $date_last_updated
 * @property \Cake\I18n\FrozenTime $money_release_date
 * @property string $operation_type
 * @property string $collector_id
 * @property string $payer_email
 * @property string $description
 * @property float $transaction_amount
 * @property float $total_paid_amount
 * @property string $currency_id
 * @property string $status
 * @property string $status_detail
 * @property string $payment_type_id
 * @property string $comment
 * @property bool $local_status
 * @property float $comision
 * @property float $net_received_amount
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property string $external_reference
 * @property int $customer_code
 * @property int $connection_id
 * @property string $receipt_number
 * @property int $receipt_id
 * @property int $bloque_id
 *
 * @property \App\Model\Entity\Payment $payment
 * @property \App\Model\Entity\Collector $collector
 * @property \App\Model\Entity\Currency $currency
 * @property \App\Model\Entity\PaymentType $payment_type
 * @property \App\Model\Entity\Connection $connection
 * @property \App\Model\Entity\Receipt $receipt
 * @property \App\Model\Entity\Bloque $bloque
 */
class MpTransaction extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
