<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CuentadigitalTransaction Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime $fecha
 * @property string $fecha_cobro
 * @property string $hhmmss
 * @property float $neto
 * @property float $neto_original
 * @property string $barcode
 * @property string $info
 * @property bool $estado
 * @property string $comentario
 * @property int $bloque_id
 * @property int $customer_code
 * @property string $receipt_number
 * @property int $receipt_id
 *
 * @property \App\Model\Entity\Bloque $bloque
 * @property \App\Model\Entity\Receipt $receipt
 */
class CuentadigitalTransaction extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
