<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Presale Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $installations_date
 * @property int $customer_code
 * @property int $debt_product_id
 * @property int $debt_package_id
 * @property string $availability
 * @property string $comments
 * @property int $user_id
 * @property float $lat
 * @property float $lng
 *
 * @property \App\Model\Entity\DebtProduct $debt_product
 * @property \App\Model\Entity\DebtPackage $debt_package
 * @property \App\Model\Entity\User $user
 */
class Presale extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
