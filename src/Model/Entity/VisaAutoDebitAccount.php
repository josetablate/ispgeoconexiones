<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * VisaAutoDebitAccount Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime $asigned
 * @property string $card_number
 * @property int $customer_code
 * @property int $payment_getway_id
 * @property bool $deleted
 *
 * @property \App\Model\Entity\PaymentGetway $payment_getway
 */
class VisaAutoDebitAccount extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => true
    ];
}
