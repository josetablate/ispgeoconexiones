<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CashEntitiesTransaction Entity
 *
 * @property int $id
 * @property int $user_origin_id
 * @property int $user_destination_id
 * @property float $value
 * @property string $type
 * @property string $concept
 * @property \Cake\I18n\FrozenTime $acepted
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\UserOrigin $user_origin
 * @property \App\Model\Entity\UserDestination $user_destination
 */
class CashEntitiesTransaction extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_origin_id' => true,
        'user_destination_id' => true,
        'value' => true,
        'type' => true,
        'concept' => true,
        'acepted' => true,
        'created' => true,
        'modified' => true,
        'user_origin' => true,
        'user_destination' => true
    ];
}
