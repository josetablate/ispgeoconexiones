<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CustomersLabel Entity
 *
 * @property int $id
 * @property int $customer_code
 * @property int $label_id
 *
 * @property \App\Model\Entity\Label $label
 */
class CustomersLabel extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'customer_code' => true,
        'label_id' => true,
        'label' => true
    ];
}
