<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ConnectionsHasMessageTemplate Entity
 *
 * @property int $id
 * @property int $connections_id
 * @property int $message_template_id
 * @property string $ip
 *
 * @property \App\Model\Entity\Connection $connection
 * @property \App\Model\Entity\MessageTemplate $message_template
 */
class ConnectionsHasMessageTemplate extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
