<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * IntOutCashsEntity Entity
 *
 * @property int $id
 * @property \Cake\I18n\Time $created
 * @property string $type
 * @property string $concept
 * @property float $in_value
 * @property float $out_value
 * @property bool $deleted
 * @property string $data
 * @property float $saldo
 * @property int $number_part
 * @property int $cash_entity_id
 * @property int $seating_number
 *
 * @property \App\Model\Entity\CashEntity $cash_entity
 */
class IntOutCashsEntity extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
