<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Connection Entity
 *
 * @property int $id
 * @property string $address
 * @property float $lat
 * @property float $lng
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property bool $deleted
 * @property int $customer_code
 * @property int $user_id
 * @property int $zone_id
 * @property int $controller_id
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\City $city
 * @property \App\Model\Entity\Controller $controller
 * @property \App\Model\Entity\Instalation[] $instalations
 * @property \App\Model\Entity\Log[] $logs
 * @property \App\Model\Entity\Message[] $messages
 * @property \App\Model\Entity\Ticket[] $tickets
 */
class Connection extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        // 'id' => false
    ];
}
