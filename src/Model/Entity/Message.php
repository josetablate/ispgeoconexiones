<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Message Entity
 *
 * @property int $id
 * @property int $ip
 * @property string $type
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $delete_date
 * @property bool $deleted
 * @property int $customer_code
 * @property int $connection_id
 * @property int $view
 *
 * @property \App\Model\Entity\Connection $connection
 */
class Message extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
