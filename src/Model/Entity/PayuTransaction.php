<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PayuTransaction Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $fecha_pago
 * @property string $id_orden
 * @property float $valor
 * @property string $pais
 * @property string $cuenta
 * @property string $numero_pago
 * @property string $id_cupon
 * @property string $referencia_venta
 * @property string $estado
 * @property string $medio_pago
 * @property string $tipo_creacion
 * @property bool $multiple_pagos
 * @property string $frecuencia_recordatorio
 * @property \Cake\I18n\FrozenTime $fecha_recordatorio
 * @property string $nombre_cliente
 * @property string $concepto
 * @property \Cake\I18n\FrozenTime $fecha_expiracion
 * @property string $email
 * @property string $comments
 * @property int $bloque_id
 * @property int $customer_code
 * @property string $receipt_number
 * @property int $payment_getway_id
 *
 * @property \App\Model\Entity\Bloque $bloque
 * @property \App\Model\Entity\PaymentGetway $payment_getway
 */
class PayuTransaction extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'fecha_pago' => true,
        'id_orden' => true,
        'valor' => true,
        'pais' => true,
        'cuenta' => true,
        'numero_pago' => true,
        'id_cupon' => true,
        'referencia_venta' => true,
        'estado' => true,
        'medio_pago' => true,
        'tipo_creacion' => true,
        'multiple_pagos' => true,
        'frecuencia_recordatorio' => true,
        'fecha_recordatorio' => true,
        'nombre_cliente' => true,
        'concepto' => true,
        'fecha_expiracion' => true,
        'email' => true,
        'comments' => true,
        'bloque_id' => true,
        'customer_code' => true,
        'receipt_number' => true,
        'payment_getway_id' => true,
        'bloque' => true,
        'payment_getway' => true
    ];
}
