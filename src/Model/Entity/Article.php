<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Article Entity
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $total_amount
 * @property bool $exist_snid
 * @property bool $deleted
 * @property bool $enabled
 *
 * @property \App\Model\Entity\Product[] $products
 * @property \App\Model\Entity\SnidStore[] $snid_stores
 * @property \App\Model\Entity\Store[] $stores
 * @property \App\Model\Entity\Instalation[] $instalations
 * @property \App\Model\Entity\Package[] $packages
 */
class Article extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
