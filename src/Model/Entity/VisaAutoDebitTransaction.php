<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * VisaAutoDebitTransaction Entity
 *
 * @property int $id
 * @property string $id_transaction
 * @property \Cake\I18n\FrozenTime $date
 * @property string $card_number
 * @property float $total
 * @property bool $status
 * @property string $comment
 * @property int $bloque_id
 * @property int $customer_code
 * @property int $payment_getway_id
 * @property string $receipt_number
 * @property string $receipt_id
 *
 * @property \App\Model\Entity\Bloque $bloque
 * @property \App\Model\Entity\PaymentGetway $payment_getway
 * @property \App\Model\Entity\Receipt $receipt
 */
class VisaAutoDebitTransaction extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => true
    ];
}
