<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Customer Entity
 *
 * @property int $code
 * @property string $status
 * @property string $name
 * @property string $address
 * @property int $responsible
 * @property int $doc_type
 * @property string $ident
 * @property string $phone
 * @property string $phone_alt
 * @property bool $asked_router
 * @property string $availability
 * @property string $email
 * @property string $comments
 * @property string $tipo_comp
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property bool $deleted
 * @property string $clave_portal
 * @property string $seller
 * @property string $plan_ask
 * @property int $zone_id
 * @property int $user_id
 * @property int $account_code
 *
 * @property \App\Model\Entity\City $city
 * @property \App\Model\Entity\User $user
 */
class Customer extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'code' => true
    ];
}
