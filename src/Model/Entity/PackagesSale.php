<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PackagesSale Entity
 *
 * @property int $id
 * @property \Cake\I18n\Time $created
 * @property bool $deleted
 * @property int $customer_code
 * @property int $package_id
 * @property int $instalation_id
 *
 * @property \App\Model\Entity\Package $package
 * @property \App\Model\Entity\Instalation $instalation
 */
class PackagesSale extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
