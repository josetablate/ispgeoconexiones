<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CashEntity Entity
 *
 * @property int $id
 * @property string $name
 * @property float $cash
 * @property string $comments
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property bool $enabled
 * @property bool $deleted
 * @property bool $open
 * @property int $user_id
 * @property int $account_code
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\IntOutCashsEntity[] $int_out_cashs_entities
 * @property \App\Model\Entity\Payment[] $payments
 */
class CashEntity extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
