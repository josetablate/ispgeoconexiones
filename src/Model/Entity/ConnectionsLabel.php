<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ConnectionsLabel Entity
 *
 * @property int $id
 * @property int $connection_id
 * @property int $label_id
 *
 * @property \App\Model\Entity\Connection $connection
 * @property \App\Model\Entity\Label $label
 */
class ConnectionsLabel extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'connection_id' => true,
        'label_id' => true,
        'connection' => true,
        'label' => true
    ];
}
