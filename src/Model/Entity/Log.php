<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Log Entity
 *
 * @property int $id
 * @property string $action
 * @property \Cake\I18n\Time $created
 * @property string $menu
 * @property int $user_id
 * @property int $connection_id
 * @property int $customer_code
 * @property int $service_id
 * @property int $product_id
 * @property int $package_id
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Connection $connection
 * @property \App\Model\Entity\Service $service
 * @property \App\Model\Entity\Product $product
 * @property \App\Model\Entity\Package $package
 */
class Log extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
