<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Discount Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $created
 * @property string $concept
 * @property string $comments
 * @property int $alicuot
 * @property string $type_value
 * @property float $value
 * @property bool $always
 * @property int $duration
 * @property int $user_id
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Service[] $services
 * @property \App\Model\Entity\ServicesHasDiscount[] $services_has_discounts
 */
class Discount extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
    
    
    protected function _getConceptAndCode()
    {
        return '(' . $this->_properties['code'] . ') ' .
            $this->_properties['concept'];
    }
}
