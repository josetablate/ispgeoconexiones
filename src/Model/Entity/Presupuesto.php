<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Presupuesto Entity
 *
 * @property int $id
 * @property int $concept_type
 * @property \Cake\I18n\FrozenTime $date
 * @property \Cake\I18n\FrozenTime $date_start
 * @property \Cake\I18n\FrozenTime $date_end
 * @property \Cake\I18n\FrozenTime $duedate
 * @property string $tipo_comp
 * @property int $pto_vta
 * @property int $num
 * @property float $subtotal
 * @property float $sum_tax
 * @property float $discount
 * @property float $total
 * @property string $company_name
 * @property string $company_address
 * @property string $company_cp
 * @property string $company_city
 * @property string $company_phone
 * @property string $company_fax
 * @property string $company_ident
 * @property string $company_email
 * @property string $company_web
 * @property int $company_responsible
 * @property int $customer_code
 * @property string $customer_name
 * @property string $customer_address
 * @property string $customer_cp
 * @property string $customer_city
 * @property string $customer_country
 * @property string $customer_ident
 * @property int $customer_doc_type
 * @property int $customer_responsible
 * @property string $cond_vta
 * @property string $comments
 * @property int $user_id
 * @property string $printed
 * @property float $resto
 * @property bool $portal_sync
 * @property int $connection_id
 * @property int $business_id
 * @property string $company_ing_bruto
 * @property string $company_init_act
 * @property string $period
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Connection $connection
 * @property \App\Model\Entity\Business $business
 * @property \App\Model\Entity\PresupuestoConcept[] $presupuesto_concepts
 */
class Presupuesto extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
