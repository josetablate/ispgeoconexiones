<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Debt Entity
 *
 * @property int $id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \Cake\I18n\Time $duedate
 * @property string $type
 * @property string $code
 * @property string $description
 * @property float $quantity
 * @property string $unit
 * @property float $price
 * @property float $sum_price
 * @property float $sum_tax
 * @property float $discount
 * @property float $total
 * @property int $dues
 * @property string $tipo_comp
 * @property int $user_id
 * @property int $customer_code
 * @property int $seating_number
 * @property int $invoice_id
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Invoice $invoice
 */
class Debt extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
