<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Router Entity
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property float $lat
 * @property float $lng
 * @property string $ip
 * @property int $port
 * @property string $username
 * @property string $password
 * @property bool $enabled
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property bool $deleted
 *
 * @property \App\Model\Entity\Network[] $networks
 */
class Router extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
}
