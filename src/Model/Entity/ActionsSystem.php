<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ActionsSystem Entity
 *
 * @property int $id
 * @property string $action
 * @property string $display_name
 * @property int $controller_id
 *
 * @property \App\Model\Entity\Controller $controller
 * @property \App\Model\Entity\Role[] $roles
 */
class ActionsSystem extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
