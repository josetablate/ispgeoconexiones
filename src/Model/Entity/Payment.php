<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Payment Entity
 *
 * @property int $id
 * @property \Cake\I18n\Time $created
 * @property string $concept
 * @property float $import
 * @property int $customer_code
 * @property int $cash_entity_id
 * @property int $user_id
 * @property int $payment_method_id
 * @property int $seating_number
 * @property int $receipt_id
 *
 * @property \App\Model\Entity\CashEntity $cash_entity
 * @property \App\Model\Entity\Customer $customer
 * @property \App\Model\Entity\Doc $doc
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\PaymentMethod $payment_method
 * @property \App\Model\Entity\Seating $seating
 */
class Payment extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
