<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CustomersHasDiscount Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property string $code
 * @property string $description
 * @property float $price
 * @property float $sum_price
 * @property float $sum_tax
 * @property float $total
 * @property int $debt_id
 * @property int $user_id
 * @property int $customer_code
 * @property int $seating_number
 * @property int $invoice_id
 * @property int $connection_id
 * @property string $period
 * @property int $tax
 *
 * @property \App\Model\Entity\Debt $debt
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Invoice $invoice
 * @property \App\Model\Entity\Connection $connection
 */
class CustomersHasDiscount extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'created' => true,
        'modified' => true,
        'code' => true,
        'description' => true,
        'price' => true,
        'sum_price' => true,
        'sum_tax' => true,
        'total' => true,
        'debt_id' => true,
        'user_id' => true,
        'customer_code' => true,
        'seating_number' => true,
        'invoice_id' => true,
        'connection_id' => true,
        'period' => true,
        'tax' => true,
        'debt' => true,
        'user' => true,
        'invoice' => true,
        'connection' => true
    ];
}
