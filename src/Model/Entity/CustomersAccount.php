<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CustomersAccount Entity
 *
 * @property int $id
 * @property int $customer_code
 * @property int $account_id
 * @property int $payment_getway_id
 * @property bool $deleted
 *
 * @property \App\Model\Entity\Account $account
 * @property \App\Model\Entity\PaymentGetway $payment_getway
 */
class CustomersAccount extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'code' => false
    ];
}
