<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CobrodigitalAccount Entity
 *
 * @property int $id
 * @property string $barcode
 * @property string $electronic_code
 * @property bool $used
 * @property bool $deleted
 * @property string $id_comercio
 * @property int $customer_code
 * @property \Cake\I18n\FrozenTime $date_exported
 * @property string $firstname
 * @property string $lastname
 * @property string $cbu
 * @property string $cuit
 * @property string $email
 * @property int $payment_getway_id
 *
 * @property \App\Model\Entity\PaymentGetway $payment_getway
 */
class CobrodigitalAccount extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
