<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * WebProxyAcces Entity
 *
 * @property int $id
 * @property string $src_address
 * @property int $dst_port
 * @property string $action
 * @property string $redirect_to
 * @property string $comment
 * @property int $order
 * @property int $controller_id
 * @property string $api_id
 *
 * @property \App\Model\Entity\Controller $controller
 * @property \App\Model\Entity\Api $api
 */
class WebProxyAcces extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
