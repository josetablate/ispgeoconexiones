<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ConnectionsDebtsMonth Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $created
 * @property string $business_billing
 * @property string $period
 * @property float $total
 * @property float $totalx
 * @property int $connection_id
 *
 * @property \App\Model\Entity\Connection $connection
 */
class ConnectionsDebtsMonth extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
