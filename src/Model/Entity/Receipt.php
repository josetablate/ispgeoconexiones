<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Receipt Entity
 *
 * @property int $id
 * @property \Cake\I18n\Time $date
 * @property int $pto_vta
 * @property string $num
 * @property int $tipo_comp
 * @property string $concept
 * @property float $import
 * @property string $company_name
 * @property string $company_address
 * @property string $company_cp
 * @property string $company_city
 * @property string $company_phone
 * @property string $company_fax
 * @property string $company_ident
 * @property string $company_email
 * @property string $company_web
 * @property int $customer_code
 * @property string $customer_name
 * @property string $customer_address
 * @property string $customer_cp
 * @property string $customer_city
 * @property string $customer_country
 * @property int $customer_doc_type
 * @property string $customer_ident
 * @property string $comments
 * @property int $user_id
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Payment[] $payments
 */
class Receipt extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
