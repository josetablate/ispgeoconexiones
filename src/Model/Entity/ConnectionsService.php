<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ConnectionsService Entity
 *
 * @property int $id
 * @property int $counter_month
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property bool $deleted
 * @property int $service_id
 * @property int $connection_id
 *
 * @property \App\Model\Entity\Service $service
 * @property \App\Model\Entity\Connection $connection
 */
class ConnectionsService extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
