<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CuentadigitalAccount Entity
 *
 * @property int $id
 * @property string $barcode
 * @property int $payment_getway_id
 * @property bool $deleted
 * @property int $customer_code
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property string $image_src
 *
 * @property \App\Model\Entity\PaymentGetway $payment_getway
 */
class CuentadigitalAccount extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
