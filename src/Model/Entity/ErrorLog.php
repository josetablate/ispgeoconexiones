<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ErrorLog Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $created
 * @property string $msg
 * @property string $data
 * @property int $user_id
 *
 * @property \App\Model\Entity\User $user
 */
class ErrorLog extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'created' => true,
        'msg' => true,
        'data' => true,
        'user_id' => true,
        'user' => true
    ];
}
