<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CobrodigitalTransaction Entity
 *
 * @property int $id
 * @property string $id_transaccion
 * @property \Cake\I18n\FrozenTime $fecha
 * @property string $codigo_barras
 * @property string $identificador
 * @property int $nro_boleta
 * @property string $nombre
 * @property string $info
 * @property string $concepto
 * @property float $bruto
 * @property float $comision
 * @property float $neto
 * @property float $saldo_acumulado
 * @property bool $estado
 * @property string $comments
 * @property int $bloque_id
 * @property int $customer_code
 *
 * @property \App\Model\Entity\Bloque $bloque
 */
class CobrodigitalTransaction extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id_transaccion' => true,
        'fecha' => true,
        'codigo_barras' => true,
        'identificador' => true,
        'nro_boleta' => true,
        'nombre' => true,
        'info' => true,
        'concepto' => true,
        'bruto' => true,
        'comision' => true,
        'neto' => true,
        'saldo_acumulado' => true,
        'estado' => true,
        'comments' => true,
        'bloque_id' => true,
        'customer_code' => true,
        'bloque' => true
    ];
}
