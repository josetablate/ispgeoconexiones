<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MessageTemplate Entity
 *
 * @property int $id
 * @property string $type
 * @property string $name
 * @property string $description
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property string $html
 * @property bool $deleted
 * @property \Cake\I18n\Time $send_date
 * @property int $user_id
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\ConnectionsHasMessageTemplate[] $connections_has_message_templates
 */
class MessageTemplate extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
