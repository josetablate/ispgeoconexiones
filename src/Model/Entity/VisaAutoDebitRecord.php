<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * VisaAutoDebitRecord Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $date_start
 * @property \Cake\I18n\FrozenTime $date_end
 * @property string $card_number
 * @property string $customer_name
 * @property int $customer_code
 * @property int $invoice_num
 * @property int $invoice_id
 * @property string $comment
 * @property \Cake\I18n\FrozenTime $duedate
 * @property float $total
 * @property string $customer_city
 * @property int $business_id
 * @property int $payment_getway_id
 * @property bool $status
 *
 * @property \App\Model\Entity\Invoice $invoice
 * @property \App\Model\Entity\Business $business
 * @property \App\Model\Entity\PaymentGetway $payment_getway
 */
class VisaAutoDebitRecord extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => true
    ];
}
