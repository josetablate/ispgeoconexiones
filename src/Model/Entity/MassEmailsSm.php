<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MassEmailsSm Entity
 *
 * @property int $id
 * @property string $message
 * @property int $customer_code
 * @property int $connection_id
 * @property string $status
 * @property int $bloque_id
 * @property int $business_billing
 * @property bool $invoice_attach
 *
 * @property \App\Model\Entity\Connection $connection
 * @property \App\Model\Entity\Bloque $bloque
 */
class MassEmailsSm extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
