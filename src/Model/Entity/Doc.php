<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Doc Entity
 *
 * @property int $id
 * @property int $point_sale
 * @property int $number
 * @property \Cake\I18n\Time $created
 * @property string $type
 * @property string $concept
 * @property string $type_comp
 * @property \Cake\I18n\Time $date_start
 * @property \Cake\I18n\Time $date_end
 * @property string $cae
 * @property float $total
 * @property bool $deleted
 * @property \Cake\I18n\Time $duedate
 * @property int $user_id
 * @property int $customer_code
 * @property int $seating_number
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Debt[] $debts
 * @property \App\Model\Entity\Payment[] $payments
 */
class Doc extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
