<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MigrationFromExcel Entity
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property string $address
 * @property string $tipo_doc
 * @property string $ident
 * @property string $responsable
 * @property string $phone
 * @property string $phone_alt
 * @property string $area
 * @property string $mail
 * @property string $is_presupuesto
 * @property string $cod_barra_co_di
 * @property string $cod_cu_di
 * @property string $nro_cu_di
 * @property string $cod_payu
 * @property string $saldo
 * @property string $empresa
 * @property string $dueday
 * @property string $valor_servicio
 * @property string $fecha_inst
 * @property string $nodo
 * @property string $plan
 * @property string $ip
 * @property string $mac
 * @property string $pppoe_name
 * @property string $pppoe_pass
 * @property string $free
 * @property string $comments
 */
class MigrationFromExcel extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'code' => true,
        'name' => true,
        'address' => true,
        'tipo_doc' => true,
        'ident' => true,
        'responsable' => true,
        'phone' => true,
        'phone_alt' => true,
        'area' => true,
        'mail' => true,
        'is_presupuesto' => true,
        'cod_barra_co_di' => true,
        'cod_cu_di' => true,
        'nro_cu_di' => true,
        'cod_payu' => true,
        'saldo' => true,
        'empresa' => true,
        'dueday' => true,
        'valor_servicio' => true,
        'fecha_inst' => true,
        'nodo' => true,
        'plan' => true,
        'ip' => true,
        'mac' => true,
        'pppoe_name' => true,
        'pppoe_pass' => true,
        'free' => true,
        'comments' => true
    ];
}
