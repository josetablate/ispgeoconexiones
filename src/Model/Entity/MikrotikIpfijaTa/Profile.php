<?php
namespace App\Model\Entity\MikrotikIpfijaTa;

use Cake\ORM\Entity;

/**
 * Profile Entity
 *
 * @property int $id
 * @property string $name
 * @property string $local_addres
 * @property string $dns_server
 * @property string $rate_limit_string
 * @property string $queue_type
 * @property int $down
 * @property int $up
 * @property int $down_burst
 * @property int $up_burst
 * @property int $down_threshold
 * @property int $up_threshold
 * @property int $down_time
 * @property int $up_time
 * @property int $priority
 * @property int $down_at_limit
 * @property int $up_at_limit
 * @property int $controller_id
 *
 * @property \App\Model\Entity\IntegrationControllersHasPlan[] $integration_controllers_has_plans
 */
class Profile extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
