<?php
namespace App\Model\Entity\MikrotikIpfijaTa;

use Cake\ORM\Entity;

/**
 * TController Entity
 *
 * @property int $id
 * @property string $local_address
 * @property string $dns_server
 * @property bool $deleted
 * @property string $queue_default
 */
class Controller extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
