<?php
namespace App\Model\Entity\MikrotikIpfijaTa;

use Cake\ORM\Entity;

/**
 * TaPlan Entity
 *
 * @property int $id
 * @property int $controller_id
 * @property int $profile_id
 * @property int $pool_id
 * @property int $plan_id
 *
 * @property \App\Model\Entity\Controller $controller
 * @property \App\Model\Entity\Profile $profile
 * @property \App\Model\Entity\Pool $pool
 * @property \App\Model\Entity\Plan[] $plans
 */
class Plan extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
