<?php
namespace App\Model\Entity\MikrotikIpfijaTa;

use Cake\ORM\Entity;

/**
 * TSecret Entity
 *
 * @property int $id
 * @property string $name
 * @property string $password
 * @property string $service
 * @property int $local_address
 * @property int $remote_address
 * @property string $last_logged_out
 * @property string $comments
 * @property bool $enabled
 * @property int $connection_id
 * @property int $controller_id
 * @property string $api_id
 * @property int $pool_id
 * @property bool $deleted
 * @property int $profile_id
 * @property int $plan_id
 *
 * @property \App\Model\Entity\Connection $connection
 * @property \App\Model\Entity\TController $t_controller
 * @property \App\Model\Entity\Api $api
 * @property \App\Model\Entity\TPool $t_pool
 * @property \App\Model\Entity\TProfile $t_profile
 * @property \App\Model\Entity\TPlan $t_plan
 */
class Queue extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => true
    ];
   
}
