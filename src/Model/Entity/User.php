<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;

/**
 * User Entity
 *
 * @property int $id
 * @property string $last_name
 * @property string $first_name
 * @property string $username
 * @property string $dni_cuil
 * @property string $password
 * @property string $password_salt
 * @property string $phone
 * @property string $email
 * @property bool $enabled
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property bool $deleted
 * @property string $description
 *
 * @property \App\Model\Entity\CashEntity[] $cash_entities
 * @property \App\Model\Entity\Connection[] $connections
 * @property \App\Model\Entity\Debt[] $debts
 * @property \App\Model\Entity\Doc[] $docs
 * @property \App\Model\Entity\Instalation[] $instalations
 * @property \App\Model\Entity\Log[] $logs
 * @property \App\Model\Entity\Payment[] $payments
 * @property \App\Model\Entity\RoadMap[] $road_map
 * @property \App\Model\Entity\Seating[] $seating
 * @property \App\Model\Entity\Store[] $stores
 * @property \App\Model\Entity\Ticket[] $tickets
 * @property \App\Model\Entity\TicketsRecord[] $tickets_records
 * @property \App\Model\Entity\Role[] $roles
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
    ];
    
    
     
    protected function _setPassword($value){
        if(empty($value)) return $this->_properties['password'];
        return (new DefaultPasswordHasher)->hash($value);
    }
    
  
}
