<?php
namespace App\Model\Entity\MikrotikDhcpTa;

use Cake\ORM\Entity;

/**
 * Pool Entity
 *
 * @property int $id
 * @property string $name
 * @property string $addresses
 * @property string $min_host
 * @property string $max_host
 * @property int $next_pool_id
 * @property int $controller_id
 *
 * @property \App\Model\Entity\FreePoolIp[] $free_pool_ips
 * @property \App\Model\Entity\IntegrationControllersHasPlan[] $integration_controllers_has_plans
 */
class Pool extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
    
}
