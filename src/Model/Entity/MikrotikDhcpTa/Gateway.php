<?php
namespace App\Model\Entity\MikrotikDhcpTa;

use Cake\ORM\Entity;

/**
 * TGateway Entity
 *
 * @property int $id
 * @property string $address
 * @property string $interface
 * @property bool $enabled
 * @property string $comment
 * @property int $controller_id
 * @property int $pool_id
 *
 * @property \App\Model\Entity\Controller $controller
 * @property \App\Model\Entity\Pool $pool
 */
class Gateway extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
