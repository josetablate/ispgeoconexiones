<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SeatingDetail Entity
 *
 * @property int $id
 * @property float $debe
 * @property float $haber
 * @property int $seating_number
 * @property int $account_code
 */
class SeatingDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
