<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Ticket Entity
 *
 * @property int $id
 * @property string $description
 * @property int $status
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $user_id
 * @property int $connection_id
 * @property int $asigned_user_id
 * @property int $customer_id
 * @property string $category
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Connection $connection
 * @property \App\Model\Entity\AsignedUser $asigned_user
 * @property \App\Model\Entity\Customer $customer
 * @property \App\Model\Entity\TicketsRecord[] $tickets_records
 */
class Ticket extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
