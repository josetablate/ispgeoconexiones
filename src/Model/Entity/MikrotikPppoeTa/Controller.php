<?php
namespace App\Model\Entity\MikrotikPppoeTa;

use Cake\ORM\Entity;

class Controller extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
