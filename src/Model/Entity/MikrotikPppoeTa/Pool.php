<?php
namespace App\Model\Entity\MikrotikPppoeTa;

use Cake\ORM\Entity;

class Pool extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
    
    protected function _getNameAndAddresses()
    {
        return $this->_properties['name'] . ' ' .
            $this->_properties['addresses'];
    }
}
