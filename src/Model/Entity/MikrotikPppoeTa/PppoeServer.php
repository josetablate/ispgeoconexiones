<?php
namespace App\Model\Entity\MikrotikPppoeTa;

use Cake\ORM\Entity;

class PppoeServer extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
