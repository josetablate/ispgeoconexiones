<?php
namespace App\Model\Entity\MikrotikPppoeTa;

use Cake\ORM\Entity;

class IpExcluded extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
