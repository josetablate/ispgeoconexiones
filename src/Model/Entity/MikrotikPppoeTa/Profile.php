<?php
namespace App\Model\Entity\MikrotikPppoeTa;

use Cake\ORM\Entity;

class Profile extends Entity
{

    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
