<?php
namespace App\Model\Entity\MikrotikPppoeTa;

use Cake\ORM\Entity;

class Secret extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => true
    ];
}
