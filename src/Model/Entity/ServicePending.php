<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ServicePending Entity
 *
 * @property int $id
 * @property int $presale_id
 * @property int $service_id
 * @property int $discunt_id
 * @property int $customer_code
 * @property \Cake\I18n\FrozenTime $used
 * @property string $address
 * @property float $lat
 * @property float $lng
 * @property \Cake\I18n\FrozenTime $installation_date
 * @property string $availability
 * @property string $service_name
 * @property float $service_total
 * @property string $discount_name
 * @property float $discount_total
 *
 * @property \App\Model\Entity\Presale $presale
 * @property \App\Model\Entity\Service $service
 * @property \App\Model\Entity\Discunt $discunt
 */
class ServicePending extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
