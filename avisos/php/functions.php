<?php

include '../../config/bootstrap.php';
include '../../config/database.php';

$servername = "localhost";
$database = "ispbraindb".$suffix;
$username = $user;
$password = $pass;




$data = [];
$data['error'] =  false;

function connectDatabase(){
	
	global $servername, $database, $username, $password, $data, $pdo;
	
	try {
        $link = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
        // $conn the PDO error mode to exception
        $link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
	}
    catch(PDOException $e) {
        $data['error'] = "Connection failed: " . $e->getMessage();
        return false;
    }
    
    return $link;
} 

function getAviso($ip){
	
	global $data;
	
	$link = connectDatabase();
	
	try {
		
		if($link){
			
			$query = "SELECT connections.customer_code,  connections_has_message_templates.id as connections_has_message_templates_id, message_templates.*
				FROM connections_has_message_templates 
				INNER JOIN  message_templates ON message_templates.id = connections_has_message_templates.message_template_id 
				INNER JOIN  connections ON connections.id = connections_has_message_templates.connections_id 
				WHERE connections_has_message_templates.ip = '$ip' ORDER BY message_templates.priority DESC LIMIT 1";
				   
		    $stmt = $link->prepare($query); 
		    $stmt->execute();
		
		    // set the resulting array to associative
		    $aviso =  $stmt->fetchObject();
		}
	}
    catch(PDOException $e) {
        $data['error'] = "Failed: " . $e->getMessage();
    }
    
    return $aviso;
} 

function getCustomers($customer_code){
	
	global $data;
	
	$link = connectDatabase();
	
	try {
		
		if($link){
			
			$query = "SELECT * FROM customers WHERE code = $customer_code LIMIT 1";
				   
		    $stmt = $link->prepare($query); 
		    $stmt->execute();
		
		    // set the resulting array to associative
		    $results =  $stmt->fetchObject();
		}
	}
    catch(PDOException $e) {
        $data['error'] = "Failed: " . $e->getMessage();
    }
    
    return $results;
} 

function parseTags($aviso){
	
	// $json = file_get_contents('../../webroot/paraments.json');
	// $paraments = json_decode($json, true);
	
	$customer = getCustomers($aviso->customer_code);
	
	$html = $aviso->html;
	
	if($aviso->type == 'Aviso'){
		if (strpos($html, '[boton_continuar]') !== false) {
			
			$html = str_replace('[boton_continuar]', '<a href="#" class="btn-continue">Seguir Navegando</a>', $html);
		}
	}
	
	
	if (strpos($html, '[cliente_nombre]') !== false) {
		
		$html = str_replace('[cliente_nombre]', $customer->name, $html);
	}
	
	if (strpos($html, '[cliente_code]') !== false) {
		
		$html = str_replace('[cliente_code]', str_pad($customer->code, 5, '0', STR_PAD_LEFT), $html);
	}
	
	if (strpos($html, '[cliente_usuario_portal]') !== false) {
		
		$html = str_replace('[cliente_usuario_portal]', $customer->ident, $html);
	}
	
	if (strpos($html, '[cliente_clave_portal]') !== false) {
		
		$html = str_replace('[cliente_clave_portal]', $customer->clave_portal, $html);
	}
	
	if (strpos($html, '[cliente_saldo_mes]') !== false) {
		
		$html = str_replace('[cliente_saldo_mes]', $customer->debt_month, $html);
	}
	
	//https:\/\/10.199.0.18\/avisos\/resources\/1503434132-robot-noti.gif\"
	
	// $url = "https://".$paraments['system']['server']['ip']."/avisos/";
	
	// if (strpos($html, $url) !== false) {
		
	// 	$html = str_replace($url, '', $html);
	// }
	
	// //http://10.199.0.18/avisos/resources/1503434132-robot-noti.gif
	
	// $url = "http://".$paraments['system']['server']['ip']."/avisos/";
	
	// if (strpos($html, $url) !== false) {
		
	// 	$html = str_replace($url, '', $html);
	// }
	
	return $html;
	
}


switch($_POST['func']){
		
	case 'getHtml': 

        $link = connectDatabase();

        if($link){

                $aviso = getAviso($_POST['ip']);

                if($aviso){
                        $data['html'] = parseTags($aviso);
                        $data['connections_has_message_templates_id'] = $aviso->connections_has_message_templates_id;
                        echo json_encode($data);
                }else{
                        $data['error'] = 'No existe avisos relacionadados a la IP: ' . $_POST['ip'];
                        echo json_encode($data);
                }
        }else{

                echo json_encode($data);
        }

        break;
}



