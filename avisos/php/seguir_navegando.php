<?php

include '../../config/bootstrap.php';
include '../../config/database.php';

// require_once 'routeros/PEAR2_Net_RouterOS-1.0.0b5.phar';
// use PEAR2\Net\RouterOS;
// use PEAR2\Net\RouterOS\SocketException;
// use PEAR2\Net\RouterOS\DataFlowException;
// use PEAR2\Net\RouterOS\Response;

/*EvilFreelancer/routeros-api-php*/
use \RouterOS\Client;
use \RouterOS\Config;
use \RouterOS\Query;
use RouterOS\Exceptions\ClientException;
use RouterOS\Exceptions\QueryException;
use App\Controller\Component\Integrations\QueryHelper;


$servername = "localhost";
$database = "ispbraindb".$suffix;
$username = $user;
$password = $pass;

const DEBUG = true;


log_file("--------------------------------------------INICIO----------------------------------------------------------------");

if(!array_key_exists('ip', $_POST)){
    $log =  "Error IP no existe en el POST." . "\r\n";
    log_file($log);
    return false;
}

if(!array_key_exists('connections_has_message_templates_id', $_POST)){
    $log =  "Error connections_has_message_templates_id no existe en el POST." . "\r\n";
    log_file($log);
    return false;
}

$ip = $_POST['ip'];
$chmt_id = $_POST['connections_has_message_templates_id'];

$log =  '"************* START IP: ' . $ip. "\r\n";

log_file($log);

removeAviso($ip, $chmt_id);

function connectDatabase(){
	
	global $servername, $database, $username, $password;
	
	try {
        $link = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
        // $conn the PDO error mode to exception
        $link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
	}
    catch(PDOException $e) {
        $error = "Connection failed: " . $e->getMessage();
        log_file($error);
        return false;
    }
    
    return $link;
} 

function log_file($text){
    
    if(DEBUG){
        $flog = fopen("accept_avisos_log.txt","a");
        fwrite($flog, "\r\n" . date('Y-m-d H:i:s') . ' -- ' . $text); 
        fclose($flog);
    }
}

function removeAviso($ip, $chmt_id){
    
    $link = connectDatabase();
    
    //verifico si existen mas de 1 aviso
    
    $rowCount = countAvisos($ip);
    
    if($rowCount == 1){
        
        log_file("*************  TIENE UN SOLO AVISO IP: $ip \r\n");
        
        $firewall_address_list = getFirewallAddressList($ip);
        
        //  log_file($firewall_address_list);
        
        if($firewall_address_list->api_id){
            
            $controller = getController($ip);
            
            if(removeIpFirewallInConttroller($controller, $firewall_address_list)){
                
                log_file("************* SE ELIMINO ADDRESS LIST EN EL CONTROLADOR IP: $ip \r\n");
                
                // elimina el registo de firewall_address_lists en la base de datos
                if(deleteFirewallAddressLists($firewall_address_list)){
                    
                    log_file("************* SE ELIMINO ADDRESS LIST DE LA BASE DE DATOS IP: $ip \r\n");
                    
                    
                    //elimina el el aviso de la base
                    if(deleteConnectionsHasMessageTemplate($ip, $chmt_id)){
                        
                        log_file("************* SE ELIMINO AVISO DE LA BASE DE DATOS IP: $ip \r\n");
                        
                        header("Location: https://www.google.com.ar");
                        
                    }
                }
                    
            }
        }
    }
    else{
        
        log_file("************* TIENE 2 ó MAS AVISOS IP: $ip \r\n");
         
         
        //elimina el el aviso de la base
        if(deleteConnectionsHasMessageTemplate($ip, $chmt_id)){
            
            log_file("************* SE ELIMINO AVISO DE LA BASE DE DATOS IP: $ip \r\n");
            
        }
        
        header("Location: http://httpbin.org/");
        
        //header("Location: https://www.google.com.ar");
         
    }
}

function deleteConnectionsHasMessageTemplate($ip, $chmt_id){
    
    $link = connectDatabase();
	
	//elimino el aviso
	
	try {
			
		$query = "DELETE FROM `connections_has_message_templates` WHERE id = $chmt_id";
	    $stmt = $link->prepare($query); 
	    $stmt->execute();
		
	}
    catch(PDOException $e) {
        $error = "Failed: " . $e->getMessage();
        log_file($error);
        return false;
    }
    
    $log = "$query \r\n";
    log_file($log);
    
    log_file("************* SE ELIMINO connections_has_message_templates IP: $ip \r\n");
    
    return true;
    
}

function countAvisos($ip){
    
    $link = connectDatabase();
    
    try {
			
		$query = "SELECT id FROM `connections_has_message_templates` WHERE `ip` = '$ip' and `type` = 'Aviso'";
	    $stmt = $link->query($query); 
	   // $res = $stmt->execute();
	    $rowCount = $stmt->rowCount(); 
		
	}
    catch(PDOException $e) {
        $error = "Failed: " . $e->getMessage();
        log_file($error);
        return false;
    }
    
    $log = "$query \r\n";
    log_file($log);
    
    return $rowCount;
    
}

function getFirewallAddressList($ip){
    
    $link = connectDatabase();
    
    try {
    			
		$query = "SELECT * FROM `firewall_address_lists` WHERE `address` = '$ip' AND `list` = 'Avisos' LIMIT 1";
	    $stmt = $link->prepare($query); 
	    $stmt->execute();
	    $firewall_address_list =  $stmt->fetchObject();
	
	}
    catch(PDOException $e) {
        $error = "Failed: " . $e->getMessage();
        log_file($error);
        return false;
    }
    
    $log = "$query \r\n";
    log_file($log);
    
    return $firewall_address_list;

}

function getController($ip){
    
    $link = connectDatabase();
    
    try {
    			
		$query = "SELECT controllers.name, controllers.connect_to, controllers.port, controllers.username,controllers.password FROM connections
			INNER JOIN controllers ON connections.controller_id = controllers.id
			WHERE connections.ip = '$ip' LIMIT 1";
	    $stmt = $link->prepare($query); 
	    $stmt->execute();
	    $controller =  $stmt->fetchObject();
	
	}
    catch(PDOException $e) {
        $error = "Failed: " . $e->getMessage();
        log_file($error);
        return false;
    }
    
    $log = "$query \r\n";
    log_file($log);
    
    
    return $controller;
}

function deleteFirewallAddressLists($firewall_address_list){
    
    $link = connectDatabase();

    try {
    		
    	$query = "DELETE FROM `firewall_address_lists` WHERE `id` = " . $firewall_address_list->id;
        $stmt = $link->prepare($query); 
        $stmt->execute();
    }
    catch(PDOException $e) {
        $error = "Failed: " . $e->getMessage();
        log_file($error);
        return false;
    }
    
    $log = "$query \r\n";
    log_file($log);
    
    return true;
}



function connectRouterMikrotik($controller, $timeout = 5){

    $client = null;

    $config = new Config([
        'host' => $controller->connect_to,
        'user' => $controller->username,
        'pass' => $controller->password,
        'port' => intval($controller->port),
        'ssl' => false,
        'timeout' => $timeout,
        'attempts' => 2          
    ]);
    
    try{

        $client = new Client($config);   
        
    }catch(ClientException $e){        
  
        $log =  'No se pudo conectar al Router. Controller: '. $controller->name . "\r\n";
        $log .=  'getMessage: '. $e->getMessage() . "\r\n";
        log_file($log);   
    
        return false;        
    }    

    return $client;
}

function removeIpFirewallInConttroller($controller, $firewallAddressList){
    
    $client = connectRouterMikrotik($controller);
    if(!$client) return false;

    $response = $client->wr([
        '/ip/firewall/address-list/remove',
        '=.id='.$firewallAddressList->api_id
        ]);

    if(array_key_exists('after', $response)){
        if(array_key_exists('message', $response['after'])){

            $log =  "Error al remover el ip firewall del mikrotik IP: ".$firewallAddressList->address. " \r\n";
            $log .=  "message: ".$response['after']['message']. " \r\n";
            log_file($log);
            return false;
        }
    }

 
    //elimino el proxy access


    $response = $client->wr([
        '/ip/proxy/access/print', 
        '?src-address='.$firewallAddressList->address
        ]);
 
    $web_proxy_access_api_id =  count($response) > 0 ? $response[0]['.id'] : false;

    if(!$web_proxy_access_api_id){
        $log =  "El proxy access ya estaba eliminado IP: ".$firewallAddressList->address. " \r\n";
        log_file($log);
	    return true; 
    }

    $response = $client->wr([
        '/ip/proxy/access/remove',
         '=.id='.$web_proxy_access_api_id
         ]);

    if(array_key_exists('after', $response)){
        if(array_key_exists('message', $response['after'])){

            $log =  "Error al remover proxy access del mikrotik IP: ".$firewallAddressList->address. " \r\n";
            $log .=  "message: ".$response['after']['message']. " \r\n";
            log_file($log);

            return false;
        }
    }    

    return true;
}

log_file("---------------------------------------------------FIN---------------------------------------------------------");

?>

