<?php

include 'database.php';
include 'session.php';
include 'debug.php';

return [

    'debug' => filter_var(env('DEBUG', $debug), FILTER_VALIDATE_BOOLEAN),

    'EmailTransport' => [
        'default' => [
            'className' => 'Mail',
            // The following keys are used in SMTP transports
            'host' => 'localhost',
            'port' => 25,
            'timeout' => 30,
            'username' => 'user',
            'password' => 'secret',
            'client' => null,
            'tls' => null,
            'url' => env('EMAIL_TRANSPORT_DEFAULT_URL', null),
        ],
    ],

    'Email' => [
        'default' => [
            'transport' => 'default',
            'from' => 'you@localhost',
            //'charset' => 'utf-8',
            //'headerCharset' => 'utf-8',
        ],
    ],

    'Datasources' => [
        'default' => [
            'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Mysql',
            'persistent' => false,
            'host' => 'localhost',
            //'port' => 'non_standard_port_number',
            'username' => $user,
            'password' => $pass,
            'database' => '',
            'encoding' => 'utf8',
            'timezone' => 'UTC',
            'flags' => [],
            'cacheMetadata' => true,
            'log' => false,
            'quoteIdentifiers' => false,
            'url' => env('DATABASE_URL', null),
        ],              
        'ispbrain_mikrotik_ipfija_ta' => [
            'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Mysql',
            'persistent' => false,
            'host' => 'localhost',
            //'port' => 'non_standard_port_number',
            'username' => $user,
            'password' => $pass,
            'database' => '',
            'encoding' => 'utf8',
            'timezone' => 'UTC',
            'flags' => [],
            'cacheMetadata' => true,
            'log' => false,
            'quoteIdentifiers' => false,
            'url' => env('DATABASE_URL', null),
        ],
        'ispbrain_mikrotik_pppoe_ta' => [
            'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Mysql',
            'persistent' => false,
            'host' => 'localhost',
            //'port' => 'non_standard_port_number',
            'username' => $user,
            'password' => $pass,
            'database' => '',
            'encoding' => 'utf8',
            'timezone' => 'UTC',
            'flags' => [],
            'cacheMetadata' => true,
            'log' => false,
            'quoteIdentifiers' => false,
            'url' => env('DATABASE_URL', null),
        ],
        'ispbrain_mikrotik_dhcp_ta' => [
            'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Mysql',
            'persistent' => false,
            'host' => 'localhost',
            //'port' => 'non_standard_port_number',
            'username' => $user,
            'password' => $pass,
            'database' => '',
            'encoding' => 'utf8',
            'timezone' => 'UTC',
            'flags' => [],
            'cacheMetadata' => true,
            'log' => false,
            'quoteIdentifiers' => false,
            'url' => env('DATABASE_URL', null),
        ],
        // 'debug_kit' => [
        //     'className' => 'Cake\Database\Connection',
        //     'driver' => 'Cake\Database\Driver\Mysql',
        //     'persistent' => false,
        //     'host' => 'localhost',
        //     //'port' => 'nonstandard_port_number',
        //     'username' => $user,    // Your DB username here
        //     'password' => $pass,    // Your DB password here
        //     'database' => '',
        //     'encoding' => 'utf8',
        //     'timezone' => 'UTC',
        //     'cacheMetadata' => true,
        //     'quoteIdentifiers' => false,
        //     //'init' => ['SET GLOBAL innodb_stats_on_metadata = 0'],
        // ], 
    ],

    'Session' => [
        'defaults' => 'database',
        'timeout'  => $timeout
    ],
];
