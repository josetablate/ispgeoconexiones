<?php
use Migrations\AbstractSeed;

/**
 * TPools seed.
 */
class TNetworksSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'comment' => 'pool3mb',
                'address' => '10.10.40.0/24',
                'min_host' => '10.10.40.1',
                'max_host' => '10.10.40.254',
                'next_network_id' => NULL,
                'gateway' => '10.10.40.1',
                'dns_server' => '8.8.8.8',
                'controller_id' => '2',
            ],
            [
                'id' => '2',
                'comment' => 'pool6mb',
                'address' => '10.10.45.0/24',
                'min_host' => '10.10.45.1',
                'max_host' => '10.10.45.254',
                'next_network_id' => NULL,
                'gateway' => '10.10.45.1',
                'dns_server' => '8.8.8.8',
                'controller_id' => '2',
            ],
        ];

        $table = $this->table('t_networks');
        $table->insert($data)->save();
    }
}
