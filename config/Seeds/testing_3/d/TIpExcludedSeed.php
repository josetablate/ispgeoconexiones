<?php
use Migrations\AbstractSeed;

/**
 * TPools seed.
 */
class TIpExcludedSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'ip' => ip2long('10.10.40.1'),
                'controller_id' => '2',
                'comments' => 'gateway de (dhcp network) pool3mb',
            ],
            [
                'id' => '2',
                'ip' => ip2long('10.10.45.1'),
                'controller_id' => '2',
                'comments' => 'gateway de (dhcp network) pool6mb',
            ],
            
        ];

        $table = $this->table('t_ip_excluded');
        $table->insert($data)->save();
    }
}
