<?php
use Migrations\AbstractSeed;

/**
 * TPools seed.
 */
class TDhcpServers extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'DHCP-Server',
                'interface' => 'ether1',
                'disabled' => 'no',
                'controller_id' => '2',
                'api_id' => NULL,
            ]
        ];

        $table = $this->table('t_dhcp_servers');
        $table->insert($data)->save();
    }
}
