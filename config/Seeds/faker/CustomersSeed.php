<?php
use Migrations\AbstractSeed;

/**
 * Customers seed.
 */
class CustomersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {

        $account_code = 120000001;

        $faker = Faker\Factory::create();
        $customers = [];
        for ($i = 0; $i < 100; $i++) {

            $email = $faker->email;
            $email_array = explode('@', $email);
            $email = $email_array[0] . '@yopmail.com';

            $customers[] = [
                'code'                      => $i + 1,
                'status'                    => 'CP',
                'name'                      => $faker->name,
                'address'                   => $faker->address,
                'responsible'               => $faker->randomElement([5, 6]),
                'doc_type'                  => 96,
                'ident'                     => $faker->randomNumber(8),
                'phone'                     => $faker->isbn10,
                'asked_router'              => $faker->randomElement([0,1]),
                'email'                     => $email,
                'comments'                  => $faker->sentence(6),
                'daydue'                    => 10,
                'created'                   => date('Y-m-d H:i:s'),
                'modified'                  => date('Y-m-d H:i:s'),
                'deleted'                   => $faker->randomElement([0, 1, 0, 0, 0, 0, 0]),
                'clave_portal'              => $faker->word,
                'seller'                    => $faker->firstName,
                'country_id'                => 1,
                'province_id'               => 4,
                'city_id'                   => 1,
                'area_id'                   => 1,
                'user_id'                   => 101,
                'account_code'              => $account_code++,
                'cond_venta'                => 1,
                'is_presupuesto'            => $faker->randomElement([0, 1]),
                'payment_method_default_id' => 1,
                'business_billing'          => 1526130279
            ];
        }

        $this->insert('customers', $customers);

        $accounts = [];

        foreach ($customers as $customer) {

            $accounts[] = [
                'code'        => $customer['account_code'],
                'name'        => $customer['code'] . ' ' . $customer['ident'] . ' ' . $customer['name'],
                'description' => '',
                'status'      => 1,
                'saldo'       => 0,
                'title'       => 0,
            ];
        }

        $this->insert('accounts', $accounts);
    }
}
