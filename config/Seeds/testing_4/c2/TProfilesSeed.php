<?php
use Migrations\AbstractSeed;

/**
 * TProfiles seed.
 */
class TProfilesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
       $data = [
            [
                'id' => '3',
                'name' => 'profile3mb',
                'local_address' => '1.2.3.4',
                'up' => '1024',
                'down' => '3072',
                'up_at_limit' => '',
                'down_at_limit' => '',
                'up_burst' => '',
                'down_burst' => '',
                'up_threshold' => '',
                'down_threshold' => '',
                'up_time' => NULL,
                'down_time' => NULL,
                'priority' => NULL,
                'up_queue_type' => 'wireless-default',
                'down_queue_type' => 'wireless-default',
                'controller_id' => '2'
            ],
            [
                'id' => '4',
                'name' => 'profile6mb',
                'local_address' => '1.2.3.4',
                'up' => '1024',
                'down' => '6144',
                'up_at_limit' => '',
                'down_at_limit' => '',
                'up_burst' => '',
                'down_burst' => '',
                'up_threshold' => '',
                'down_threshold' => '',
                'up_time' => NULL,
                'down_time' => NULL,
                'priority' => NULL,
                'up_queue_type' => 'wireless-default',
                'down_queue_type' => 'wireless-default',
                'controller_id' => '2'
            ]
        ];

        $table = $this->table('t_profiles');
        $table->insert($data)->save();
    }
}
