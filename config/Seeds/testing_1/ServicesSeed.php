<?php
use Migrations\AbstractSeed;

/**
 * Controllers seed.
 */
class ServicesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'Plan 3MB',
                'description' => '',
                'price' => 500,
                'created' => '2017-11-18 10:59:03',
                'modified' => '2017-11-18 10:59:03',
                'enabled' => '1',
                'account_code' => '311000002',
                'deleted' => '0',
                'aliquot' => 5
            ],
            [
                'id' => '2',
                'name' => 'Plan 6MB',
                'description' => '',
                'price' => 1000,
                'created' => '2017-11-18 10:59:03',
                'modified' => '2017-11-18 10:59:03',
                'enabled' => '1',
                'account_code' => '311000003',
                'deleted' => '0',
                'aliquot' => 5
            ],
        ];

        $table = $this->table('services');
        $table->insert($data)->save();
    }
}
