<?php
use Migrations\AbstractSeed;

/**
 * TPools seed.
 */
class TPoolsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'pool3mb',
                'addresses' => '10.10.10.0/24',
                'min_host' => '10.10.10.1',
                'max_host' => '10.10.10.254',
                'next_pool_id' => NULL,
                'controller_id' => '1',
            ],
            [
                'id' => '2',
                'name' => 'pool6mb',
                'addresses' => '10.10.15.0/24',
                'min_host' => '10.10.15.1',
                'max_host' => '10.10.15.254',
                'next_pool_id' => NULL,
                'controller_id' => '1',
            ],
        ];

        $table = $this->table('t_pools');
        $table->insert($data)->save();
    }
}
