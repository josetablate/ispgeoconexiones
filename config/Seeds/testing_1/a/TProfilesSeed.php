<?php
use Migrations\AbstractSeed;

/**
 * TProfiles seed.
 */
class TProfilesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'profile3mb',
                'local_address' => '10.20.30.40',
                'dns_server' => '',
                'rate_limit_string' => '1024k/3072k',
                'queue_type' => 'default-small',
                'down' => '3072',
                'up' => '1024',
                'down_burst' => NULL,
                'up_burst' => NULL,
                'down_threshold' => NULL,
                'up_threshold' => NULL,
                'down_time' => NULL,
                'up_time' => NULL,
                'priority' => NULL,
                'down_at_limit' => NULL,
                'up_at_limit' => NULL,
                'controller_id' => '1',
                'api_id' => NULL,
            ],
            [
                'id' => '2',
                'name' => 'profile6mb',
                'local_address' => '10.20.30.40',
                'dns_server' => '',
                'rate_limit_string' => '1024k/6144k',
                'queue_type' => 'default-small',
                'down' => '6144',
                'up' => '1024',
                'down_burst' => NULL,
                'up_burst' => NULL,
                'down_threshold' => NULL,
                'up_threshold' => NULL,
                'down_time' => NULL,
                'up_time' => NULL,
                'priority' => NULL,
                'down_at_limit' => NULL,
                'up_at_limit' => NULL,
                'controller_id' => '1',
                'api_id' => NULL,
            ],
        ];

        $table = $this->table('t_profiles');
        $table->insert($data)->save();
    }
}
