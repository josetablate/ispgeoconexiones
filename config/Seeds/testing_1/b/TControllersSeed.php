<?php
use Migrations\AbstractSeed;

/**
 * TControllers seed.
 */
class TControllersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '2',
                'local_address' => '1.2.3.5',
                'dns_server' => NULL,
                'deleted' => '0',
                'queue_default' => NULL,
                'address_list_name' => 'IPFIJA2-Clientes-ISPBrain'
            ],
        ];

        $table = $this->table('t_controllers');
        $table->insert($data)->save();
    }
}
