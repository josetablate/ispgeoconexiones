<?php
use Migrations\AbstractSeed;

/**
 * TPools seed.
 */
class TPoolsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'pool3mb',
                'addresses' => '10.10.20.0/24',
                'min_host' => '10.10.20.1',
                'max_host' => '10.10.20.254',
                'next_pool_id' => NULL,
                'controller_id' => '2',
            ],
            [
                'id' => '2',
                'name' => 'pool6mb',
                'addresses' => '10.10.25.0/24',
                'min_host' => '10.10.25.1',
                'max_host' => '10.10.25.254',
                'next_pool_id' => NULL,
                'controller_id' => '2',
            ],
        ];

        $table = $this->table('t_pools');
        $table->insert($data)->save();
    }
}
