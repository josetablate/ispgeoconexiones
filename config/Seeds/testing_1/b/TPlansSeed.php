<?php
use Migrations\AbstractSeed;

/**
 * TPlans seed.
 */
class TPlansSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'controller_id' => '2',
                'profile_id' => '1',
                'pool_id' => '1',
                'service_id' => '1',
            ],
            [
                'id' => '2',
                'controller_id' => '2',
                'profile_id' => '2',
                'pool_id' => '2',
                'service_id' => '2',
            ],
        ];

        $table = $this->table('t_plans');
        $table->insert($data)->save();
    }
}
