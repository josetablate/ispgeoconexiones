<?php
use Migrations\AbstractSeed;

/**
 * PaymentsConcepts seed.
 */
class PaymentsConceptsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'text' => 'Pago varios',
            ],
        ];

        $table = $this->table('payments_concepts');
        $table->insert($data)->save();
    }
}
