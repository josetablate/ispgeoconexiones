<?php
use Migrations\AbstractSeed;

/**
 * Clases seed.
 */
class ClasesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $table = $this->table('clases');

        $table->truncate();

        $data = [
            [
                'id' => '1',
                'clase' => 'Accounts',
                'display_name' => 'Plan de Cuentas',
            ],
            [
                'id' => '2',
                'clase' => 'ActionLog',
                'display_name' => 'Registro de Acciones',
            ],
            [
                'id' => '3',
                'clase' => 'Articles',
                'display_name' => 'Artículos',
            ],
            [
                'id' => '4',
                'clase' => 'ArticlesStores',
                'display_name' => 'Stock',
            ],
            [
                'id' => '5',
                'clase' => 'CashEntities',
                'display_name' => 'Cajas',
            ],
            [
                'id' => '6',
                'clase' => 'Connections',
                'display_name' => 'Conexiones',
            ],
            [
                'id' => '7',
                'clase' => 'IspController',
                'display_name' => 'Isp Controller',
            ],
            [
                'id' => '8',
                'clase' => 'CreditNotes',
                'display_name' => 'Notas de Crédito',
            ],
            [
                'id' => '9',
                'clase' => 'DebitNotes',
                'display_name' => 'Notas de Débito',
            ],
            [
                'id' => '10',
                'clase' => 'Customers',
                'display_name' => 'Clientes',
            ],
            [
                'id' => '11',
                'clase' => 'Debts',
                'display_name' => 'Deudas',
            ],
            [
                'id' => '12',
                'clase' => 'DiaryBook',
                'display_name' => 'Contable',
            ],
            [
                'id' => '13',
                'clase' => 'ErrorLog',
                'display_name' => 'Registro de Errores',
            ],
            [
                'id' => '14',
                'clase' => 'Home',
                'display_name' => 'Tablero',
            ],
            [
                'id' => '15',
                'clase' => 'Instalations',
                'display_name' => 'Instalaciones',
            ],
            [
                'id' => '16',
                'clase' => 'Invoices',
                'display_name' => 'Facturas',
            ],
            [
                'id' => '17',
                'clase' => 'Packages',
                'display_name' => 'Paquetes',
            ],
            [
                'id' => '18',
                'clase' => 'PaymentMethods',
                'display_name' => 'Forma de Pagos',
            ],
            [
                'id' => '19',
                'clase' => 'Payments',
                'display_name' => 'Pagos',
            ],
            [
                'id' => '20',
                'clase' => 'Products',
                'display_name' => 'Productos',
            ],
            [
                'id' => '22',
                'clase' => 'Roles',
                'display_name' => 'Privilegios',
            ],
            [
                'id' => '23',
                'clase' => 'Seating',
                'display_name' => 'Asientos',
            ],
            [
                'id' => '24',
                'clase' => 'Services',
                'display_name' => 'Servicos',
            ],
            [
                'id' => '25',
                'clase' => 'Stores',
                'display_name' => 'Depósitos',
            ],
            [
                'id' => '26',
                'clase' => 'Users',
                'display_name' => 'Usuarios',
            ],
            [
                'id' => '27',
                'clase' => 'Areas',
                'display_name' => 'Áreas',
            ],
            [
                'id' => '28',
                'clase' => 'Settings',
                'display_name' => 'Configuraciones',
            ],
            [
                'id' => '29',
                'clase' => 'AvisoHttp',
                'display_name' => 'Avisos HTTP',
            ],
            [
                'id' => '33',
                'clase' => 'Tickets',
                'display_name' => 'Tickets',
            ],
            [
                'id' => '34',
                'clase' => 'TicketsRecords',
                'display_name' => 'Registro de Tickets',
            ],
            [
                'id' => '36',
                'clase' => 'IntOutCashsEntities',
                'display_name' => 'Movimientos de Cajas',
            ],
            [
                'id' => '37',
                'clase' => 'Comprobante',
                'display_name' => 'Facturas',
            ],
            [
                'id' => '38',
                'clase' => 'CobroDigital',
                'display_name' => 'Cobro Digital - Método de Pago',
            ],
            [
                'id' => '39',
                'clase' => 'Notifications',
                'display_name' => 'Notificaciones',
            ],
            [
                'id' => '40',
                'clase' => 'SmsMasivo',
                'display_name' => 'SMS Masivo',
            ],
            [
                'id' => '41',
                'clase' => 'Presales',
                'display_name' => 'Ventas',
            ],
            [
                'id' => '42',
                'clase' => 'Discounts',
                'display_name' => 'Descuentos',
            ],
            [
                'id' => '43',
                'clase' => 'CashEntitiesTransactions',
                'display_name' => 'Transferencias entre Cajas',
            ],
			[
                'id' => '44',
                'clase' => 'ChequePaymentMethod',
                'display_name' => 'Cheque - Método de Pago',
            ],
			[
                'id' => '45',
                'clase' => 'Cities',
                'display_name' => 'Ciudades',
            ],
			[
                'id' => '46',
                'clase' => 'ConnectionsGraphic',
                'display_name' => 'Gráfica de Conexiones',
            ],
			[
                'id' => '47',
                'clase' => 'ConnectionsHasMessageTemplates',
                'display_name' => 'Mensajes enviados - Avisos HTTP',
            ],
			[
                'id' => '48',
                'clase' => 'ConnectionsTraffic',
                'display_name' => 'Registro de Tráfico',
            ],
			[
                'id' => '49',
                'clase' => 'CreditCards',
                'display_name' => 'Tarjeta de Crédito - Método de Pago',
            ],
			[
                'id' => '50',
                'clase' => 'Cuentadigital',
                'display_name' => 'Cuenta Digital - Método de Pago',
            ],
			[
                'id' => '51',
                'clase' => 'CustomersAccounts',
                'display_name' => 'Cuentas de Clientes - Método de Pago',
            ],
			[
                'id' => '52',
                'clase' => 'CustomersHasDiscounts',
                'display_name' => 'Concepto de Descuento',
            ],
			[
                'id' => '53',
                'clase' => 'CustomersMessageTemplates',
                'display_name' => 'Plantilla - Avisos Portal',
            ],
			[
                'id' => '54',
                'clase' => 'DebitAutoChubut',
                'display_name' => 'Débito Automatico Banco Chubut',
            ],
			[
                'id' => '55',
                'clase' => 'DebitCards',
                'display_name' => 'Tarjeta de Débito',
            ],
			[
                'id' => '56',
                'clase' => 'Download',
                'display_name' => 'Descarga de Comprobante',
            ],
			[
                'id' => '57',
                'clase' => 'Importers',
                'display_name' => 'Importadores',
            ],
			[
                'id' => '58',
                'clase' => 'Impuestos',
                'display_name' => 'Impuestos',
            ],
			[
                'id' => '59',
                'clase' => 'IspController',
                'display_name' => 'Controladores',
            ],
			[
                'id' => '60',
                'clase' => 'IspControllerMikrotikDhcpTa',
                'display_name' => 'Controlador Mikrotik DHCP Ta',
            ],
			[
                'id' => '61',
                'clase' => 'IspControllerMikrotikIpfijaTa',
                'display_name' => 'Controlador Mikrotik IP Fija Ta',
            ],
			[
                'id' => '62',
                'clase' => 'IspControllerMikrotikPppoeTa',
                'display_name' => 'Controlador Mikrotik PPPoE Ta',
            ],
			[
                'id' => '63',
                'clase' => 'Labels',
                'display_name' => 'Etiquetas',
            ],
			[
                'id' => '64',
                'clase' => 'MassEmails',
                'display_name' => 'Correo Masivo',
            ],
			[
                'id' => '65',
                'clase' => 'MastercardAutoDebit',
                'display_name' => 'MasterCard Débito Automático - Método de Pago',
            ],
			[
                'id' => '66',
                'clase' => 'MercadoPago',
                'display_name' => 'Mercado Pago - Método de Pago',
            ],
			[
                'id' => '67',
                'clase' => 'MessageTemplates',
                'display_name' => 'Plantilla HTTP',
            ],
			[
                'id' => '68',
                'clase' => 'Observations',
                'display_name' => 'Observaciones',
            ],
			[
                'id' => '69',
                'clase' => 'PaymentCommitment',
                'display_name' => 'Compromiso de Pago',
            ],
			[
                'id' => '70',
                'clase' => 'PaymentsConcepts',
                'display_name' => 'Concepto de Pagos',
            ],
			[
                'id' => '71',
                'clase' => 'Payu',
                'display_name' => 'PayU - Métodos de Pagos',
            ],
			[
                'id' => '72',
                'clase' => 'Pim',
                'display_name' => 'Pim - Métodos de Pagos',
            ],
			[
                'id' => '73',
                'clase' => 'Portal',
                'display_name' => 'Portal',
            ],
			[
                'id' => '74',
                'clase' => 'Presupuesto',
                'display_name' => 'Presupuesto',
            ],
			[
                'id' => '75',
                'clase' => 'SeatingDetail',
                'display_name' => 'Detalle de Asiento Contable',
            ],
			[
                'id' => '76',
                'clase' => 'Telegram',
                'display_name' => 'Telegram',
            ],
			[
                'id' => '77',
                'clase' => 'TemplateResources',
                'display_name' => 'Recuersos para plantilla aviso HTTP',
            ],
			[
                'id' => '78',
                'clase' => 'TodoPago',
                'display_name' => 'TodoPago - Método de Pago',
            ],
			[
                'id' => '79',
                'clase' => 'TransferencesPaymentMethod',
                'display_name' => 'Transferencias de Método de Pago',
            ],
			[
                'id' => '80',
                'clase' => 'VisaAutoDebit',
                'display_name' => 'Visa Débito Automático - Método de Pago',
            ],
            [
                'id' => '81',
                'clase' => 'Presupuestos',
                'display_name' => 'Presupuestos',
            ],
            [
                'id' => '82',
                'clase' => 'Rapipago',
                'display_name' => 'Rapipago',
            ],
        ];

        $table->insert($data)->save();
    }
}
