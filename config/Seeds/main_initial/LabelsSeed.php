<?php
use Migrations\AbstractSeed;

/**
 * Labels seed.
 */
class LabelsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'text' => 'Faltan Datos',
                'entity' => '0',
                'color_back' => '#2196f3',
                'color_text' => '#ffffff',
                'enabled' => '1',
            ],
            [
                'id' => '2',
                'text' => 'Retirar Antena',
                'entity' => '1',
                'color_back' => '#ff0000',
                'color_text' => '#ffffff',
                'enabled' => '1',
            ],
        ];

        $table = $this->table('labels');
        $table->insert($data)->save();
    }
}
