<?php
use Migrations\AbstractSeed;

/**
 * CashEntities seed.
 */
class CashEntitiesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'Caja Admin',
                'cash' => '0.00',
                'comments' => '',
                'created' => '2018-05-05 11:18:01',
                'modified' => '2018-05-05 11:18:01',
                'enabled' => '1',
                'deleted' => '0',
                'open' => '0',
                'user_id' => '101',
                'account_code' => '110000003',
            ],
        ];

        $table = $this->table('cash_entities');
        $table->insert($data)->save();
        
        $data = [
            [
                'code' => '110000003',
                'name' => 'Caja Admin',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
        ];

        $table = $this->table('accounts');
        $table->insert($data)->save();
    }
}
