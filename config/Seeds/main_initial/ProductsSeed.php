<?php
use Migrations\AbstractSeed;

/**
 * Products seed.
 */
class ProductsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'TP-Link 750mbps',
                'unit_price' => '500.00',
                'created' => '2018-05-05 10:02:36',
                'modified' => '2018-05-05 10:02:36',
                'enabled' => '1',
                'deleted' => '0',
                'article_id' => '1',
                'account_code' => '312000001',
                'aliquot' => 5,
            ],
        ];

        $table = $this->table('products');
        $table->insert($data)->save();
        
        $data = [
            [
                'code' => '312000001',
                'name' => 'TP-Link 750mbps',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
        ];

        $table = $this->table('accounts');
        $table->insert($data)->save();
    }
}
