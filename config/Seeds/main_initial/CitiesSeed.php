<?php
use Migrations\AbstractSeed;

/**
 * Cities seed.
 */
class CitiesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'Ciudad Ejemplo 1',
                'province_id' => '4',
                'cp' => '1234',
                'enabled' => '1',
            ],
            [
                'id' => '2',
                'name' => 'Ciudad Ejemplo 2',
                'province_id' => '4',
                'cp' => '4321',
                'enabled' => '1',
            ],
        ];

        $table = $this->table('cities');
        $table->insert($data)->save();
    }
}
