<?php
use Migrations\AbstractSeed;

/**
 * Stores seed.
 */
class StoresSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'Depósito Central',
                'address' => 'Calle 123',
                'lat' => NULL,
                'lng' => NULL,
                'comments' => '',
                'created' => '2018-05-05 10:00:54',
                'modified' => '2018-05-05 10:00:54',
                'deleted' => '0',
                'enabled' => '1',
                'user_id' => '101',
            ],
        ];

        $table = $this->table('stores');
        $table->insert($data)->save();
    }
}
