<?php
use Migrations\AbstractSeed;

/**
 * ActionsSystemRoles seed.
 */
class ActionsSystemRolesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {

        $table = $this->table('actions_system_roles');

        $table->truncate();

        $data = [
            [
                'id' => '184',
                'role_id' => '102',
                'action_system_id' => '1',
            ],
            [
                'id' => '185',
                'role_id' => '102',
                'action_system_id' => '2',
            ],
            [
                'id' => '186',
                'role_id' => '102',
                'action_system_id' => '3',
            ],
            [
                'id' => '187',
                'role_id' => '102',
                'action_system_id' => '4',
            ],
            [
                'id' => '189',
                'role_id' => '102',
                'action_system_id' => '10',
            ],
            [
                'id' => '190',
                'role_id' => '102',
                'action_system_id' => '11',
            ],
            [
                'id' => '191',
                'role_id' => '102',
                'action_system_id' => '12',
            ],
            [
                'id' => '192',
                'role_id' => '102',
                'action_system_id' => '13',
            ],
            [
                'id' => '193',
                'role_id' => '102',
                'action_system_id' => '14',
            ],
            [
                'id' => '194',
                'role_id' => '102',
                'action_system_id' => '15',
            ],
            [
                'id' => '195',
                'role_id' => '102',
                'action_system_id' => '16',
            ],
            [
                'id' => '196',
                'role_id' => '102',
                'action_system_id' => '17',
            ],
            [
                'id' => '197',
                'role_id' => '102',
                'action_system_id' => '18',
            ],
            [
                'id' => '198',
                'role_id' => '102',
                'action_system_id' => '19',
            ],
            [
                'id' => '199',
                'role_id' => '102',
                'action_system_id' => '20',
            ],
            [
                'id' => '200',
                'role_id' => '102',
                'action_system_id' => '21',
            ],
            [
                'id' => '201',
                'role_id' => '102',
                'action_system_id' => '22',
            ],
            [
                'id' => '202',
                'role_id' => '102',
                'action_system_id' => '23',
            ],
            [
                'id' => '203',
                'role_id' => '102',
                'action_system_id' => '24',
            ],
            [
                'id' => '204',
                'role_id' => '102',
                'action_system_id' => '25',
            ],
            [
                'id' => '205',
                'role_id' => '102',
                'action_system_id' => '26',
            ],
            [
                'id' => '206',
                'role_id' => '102',
                'action_system_id' => '27',
            ],
            [
                'id' => '207',
                'role_id' => '102',
                'action_system_id' => '28',
            ],
            [
                'id' => '208',
                'role_id' => '102',
                'action_system_id' => '29',
            ],
            [
                'id' => '209',
                'role_id' => '102',
                'action_system_id' => '30',
            ],
            [
                'id' => '210',
                'role_id' => '102',
                'action_system_id' => '31',
            ],
            [
                'id' => '211',
                'role_id' => '102',
                'action_system_id' => '32',
            ],
            [
                'id' => '212',
                'role_id' => '102',
                'action_system_id' => '33',
            ],
            [
                'id' => '213',
                'role_id' => '102',
                'action_system_id' => '34',
            ],
            [
                'id' => '214',
                'role_id' => '102',
                'action_system_id' => '35',
            ],
            [
                'id' => '215',
                'role_id' => '102',
                'action_system_id' => '36',
            ],
            [
                'id' => '216',
                'role_id' => '102',
                'action_system_id' => '39',
            ],
            [
                'id' => '217',
                'role_id' => '102',
                'action_system_id' => '40',
            ],
            [
                'id' => '218',
                'role_id' => '102',
                'action_system_id' => '41',
            ],
            [
                'id' => '219',
                'role_id' => '102',
                'action_system_id' => '42',
            ],
            [
                'id' => '221',
                'role_id' => '102',
                'action_system_id' => '44',
            ],
            [
                'id' => '222',
                'role_id' => '102',
                'action_system_id' => '45',
            ],
            [
                'id' => '223',
                'role_id' => '102',
                'action_system_id' => '46',
            ],
            [
                'id' => '224',
                'role_id' => '102',
                'action_system_id' => '47',
            ],
            [
                'id' => '225',
                'role_id' => '102',
                'action_system_id' => '48',
            ],
            [
                'id' => '226',
                'role_id' => '102',
                'action_system_id' => '49',
            ],
            [
                'id' => '228',
                'role_id' => '102',
                'action_system_id' => '51',
            ],
            [
                'id' => '229',
                'role_id' => '102',
                'action_system_id' => '52',
            ],
            [
                'id' => '230',
                'role_id' => '102',
                'action_system_id' => '53',
            ],
            [
                'id' => '231',
                'role_id' => '102',
                'action_system_id' => '54',
            ],
            [
                'id' => '232',
                'role_id' => '102',
                'action_system_id' => '55',
            ],
            [
                'id' => '234',
                'role_id' => '102',
                'action_system_id' => '57',
            ],
            [
                'id' => '235',
                'role_id' => '102',
                'action_system_id' => '58',
            ],
            [
                'id' => '236',
                'role_id' => '102',
                'action_system_id' => '59',
            ],
            [
                'id' => '237',
                'role_id' => '102',
                'action_system_id' => '60',
            ],
            [
                'id' => '238',
                'role_id' => '102',
                'action_system_id' => '61',
            ],
            [
                'id' => '239',
                'role_id' => '102',
                'action_system_id' => '62',
            ],
            [
                'id' => '240',
                'role_id' => '102',
                'action_system_id' => '63',
            ],
            [
                'id' => '241',
                'role_id' => '102',
                'action_system_id' => '64',
            ],
            [
                'id' => '242',
                'role_id' => '102',
                'action_system_id' => '70',
            ],
            [
                'id' => '243',
                'role_id' => '102',
                'action_system_id' => '71',
            ],
            [
                'id' => '244',
                'role_id' => '102',
                'action_system_id' => '72',
            ],
            [
                'id' => '245',
                'role_id' => '102',
                'action_system_id' => '73',
            ],
            [
                'id' => '246',
                'role_id' => '102',
                'action_system_id' => '74',
            ],
            [
                'id' => '247',
                'role_id' => '102',
                'action_system_id' => '75',
            ],
            [
                'id' => '248',
                'role_id' => '102',
                'action_system_id' => '76',
            ],
            [
                'id' => '249',
                'role_id' => '102',
                'action_system_id' => '81',
            ],
            [
                'id' => '250',
                'role_id' => '102',
                'action_system_id' => '82',
            ],
            [
                'id' => '251',
                'role_id' => '102',
                'action_system_id' => '83',
            ],
            [
                'id' => '252',
                'role_id' => '102',
                'action_system_id' => '84',
            ],
            [
                'id' => '253',
                'role_id' => '102',
                'action_system_id' => '85',
            ],
            [
                'id' => '254',
                'role_id' => '102',
                'action_system_id' => '86',
            ],
            [
                'id' => '258',
                'role_id' => '102',
                'action_system_id' => '90',
            ],
            [
                'id' => '259',
                'role_id' => '102',
                'action_system_id' => '91',
            ],
            [
                'id' => '260',
                'role_id' => '102',
                'action_system_id' => '92',
            ],
            [
                'id' => '261',
                'role_id' => '102',
                'action_system_id' => '93',
            ],
            [
                'id' => '262',
                'role_id' => '102',
                'action_system_id' => '94',
            ],
            [
                'id' => '263',
                'role_id' => '102',
                'action_system_id' => '95',
            ],
            [
                'id' => '264',
                'role_id' => '102',
                'action_system_id' => '96',
            ],
            [
                'id' => '265',
                'role_id' => '102',
                'action_system_id' => '99',
            ],
            [
                'id' => '267',
                'role_id' => '102',
                'action_system_id' => '101',
            ],
            [
                'id' => '268',
                'role_id' => '102',
                'action_system_id' => '102',
            ],
            [
                'id' => '269',
                'role_id' => '102',
                'action_system_id' => '103',
            ],
            [
                'id' => '270',
                'role_id' => '102',
                'action_system_id' => '105',
            ],
            [
                'id' => '271',
                'role_id' => '102',
                'action_system_id' => '106',
            ],
            [
                'id' => '272',
                'role_id' => '102',
                'action_system_id' => '107',
            ],
            [
                'id' => '273',
                'role_id' => '102',
                'action_system_id' => '108',
            ],
            [
                'id' => '274',
                'role_id' => '102',
                'action_system_id' => '109',
            ],
            [
                'id' => '275',
                'role_id' => '102',
                'action_system_id' => '110',
            ],
            [
                'id' => '277',
                'role_id' => '102',
                'action_system_id' => '112',
            ],
            [
                'id' => '278',
                'role_id' => '102',
                'action_system_id' => '113',
            ],
            [
                'id' => '279',
                'role_id' => '102',
                'action_system_id' => '114',
            ],
            [
                'id' => '280',
                'role_id' => '102',
                'action_system_id' => '115',
            ],
            [
                'id' => '281',
                'role_id' => '102',
                'action_system_id' => '116',
            ],
            [
                'id' => '282',
                'role_id' => '102',
                'action_system_id' => '117',
            ],
            [
                'id' => '283',
                'role_id' => '102',
                'action_system_id' => '118',
            ],
            [
                'id' => '284',
                'role_id' => '102',
                'action_system_id' => '119',
            ],
            [
                'id' => '285',
                'role_id' => '102',
                'action_system_id' => '122',
            ],
            [
                'id' => '287',
                'role_id' => '102',
                'action_system_id' => '124',
            ],
            [
                'id' => '288',
                'role_id' => '102',
                'action_system_id' => '125',
            ],
            [
                'id' => '289',
                'role_id' => '102',
                'action_system_id' => '126',
            ],
            [
                'id' => '295',
                'role_id' => '102',
                'action_system_id' => '132',
            ],
            [
                'id' => '297',
                'role_id' => '102',
                'action_system_id' => '134',
            ],
            [
                'id' => '298',
                'role_id' => '102',
                'action_system_id' => '135',
            ],
            [
                'id' => '310',
                'role_id' => '102',
                'action_system_id' => '149',
            ],
            [
                'id' => '311',
                'role_id' => '102',
                'action_system_id' => '157',
            ],
            [
                'id' => '312',
                'role_id' => '102',
                'action_system_id' => '158',
            ],
            [
                'id' => '313',
                'role_id' => '102',
                'action_system_id' => '159',
            ],
            [
                'id' => '314',
                'role_id' => '102',
                'action_system_id' => '160',
            ],
            [
                'id' => '315',
                'role_id' => '102',
                'action_system_id' => '161',
            ],
            [
                'id' => '320',
                'role_id' => '102',
                'action_system_id' => '166',
            ],
            [
                'id' => '322',
                'role_id' => '102',
                'action_system_id' => '168',
            ],
            [
                'id' => '323',
                'role_id' => '102',
                'action_system_id' => '169',
            ],
            [
                'id' => '324',
                'role_id' => '102',
                'action_system_id' => '170',
            ],
            [
                'id' => '327',
                'role_id' => '102',
                'action_system_id' => '173',
            ],
            [
                'id' => '490',
                'role_id' => '104',
                'action_system_id' => '44',
            ],
            [
                'id' => '491',
                'role_id' => '104',
                'action_system_id' => '45',
            ],
            [
                'id' => '492',
                'role_id' => '104',
                'action_system_id' => '46',
            ],
            [
                'id' => '493',
                'role_id' => '104',
                'action_system_id' => '47',
            ],
            [
                'id' => '494',
                'role_id' => '104',
                'action_system_id' => '49',
            ],
            [
                'id' => '495',
                'role_id' => '104',
                'action_system_id' => '134',
            ],
            [
                'id' => '496',
                'role_id' => '104',
                'action_system_id' => '135',
            ],
            [
                'id' => '502',
                'role_id' => '104',
                'action_system_id' => '52',
            ],
            [
                'id' => '503',
                'role_id' => '104',
                'action_system_id' => '53',
            ],
            [
                'id' => '504',
                'role_id' => '104',
                'action_system_id' => '54',
            ],
            [
                'id' => '505',
                'role_id' => '104',
                'action_system_id' => '74',
            ],
            [
                'id' => '506',
                'role_id' => '104',
                'action_system_id' => '75',
            ],
            [
                'id' => '507',
                'role_id' => '104',
                'action_system_id' => '76',
            ],
            [
                'id' => '508',
                'role_id' => '104',
                'action_system_id' => '81',
            ],
            [
                'id' => '509',
                'role_id' => '104',
                'action_system_id' => '82',
            ],
            [
                'id' => '510',
                'role_id' => '104',
                'action_system_id' => '92',
            ],
            [
                'id' => '511',
                'role_id' => '104',
                'action_system_id' => '93',
            ],
            [
                'id' => '512',
                'role_id' => '104',
                'action_system_id' => '106',
            ],
            [
                'id' => '513',
                'role_id' => '104',
                'action_system_id' => '174',
            ],
            [
                'id' => '514',
                'role_id' => '105',
                'action_system_id' => '29',
            ],
            [
                'id' => '515',
                'role_id' => '105',
                'action_system_id' => '30',
            ],
            [
                'id' => '516',
                'role_id' => '105',
                'action_system_id' => '31',
            ],
            [
                'id' => '517',
                'role_id' => '105',
                'action_system_id' => '32',
            ],
            [
                'id' => '518',
                'role_id' => '105',
                'action_system_id' => '33',
            ],
            [
                'id' => '519',
                'role_id' => '105',
                'action_system_id' => '34',
            ],
            [
                'id' => '520',
                'role_id' => '105',
                'action_system_id' => '35',
            ],
            [
                'id' => '521',
                'role_id' => '105',
                'action_system_id' => '44',
            ],
            [
                'id' => '522',
                'role_id' => '105',
                'action_system_id' => '70',
            ],
            [
                'id' => '523',
                'role_id' => '105',
                'action_system_id' => '71',
            ],
            [
                'id' => '524',
                'role_id' => '105',
                'action_system_id' => '72',
            ],
            [
                'id' => '525',
                'role_id' => '105',
                'action_system_id' => '73',
            ],
            [
                'id' => '526',
                'role_id' => '105',
                'action_system_id' => '81',
            ],
            [
                'id' => '527',
                'role_id' => '105',
                'action_system_id' => '92',
            ],
            [
                'id' => '528',
                'role_id' => '105',
                'action_system_id' => '106',
            ],
            [
                'id' => '529',
                'role_id' => '105',
                'action_system_id' => '128',
            ],
            [
                'id' => '531',
                'role_id' => '105',
                'action_system_id' => '133',
            ],
            [
                'id' => '537',
                'role_id' => '105',
                'action_system_id' => '166',
            ],
            [
                'id' => '538',
                'role_id' => '105',
                'action_system_id' => '170',
            ],
            [
                'id' => '539',
                'role_id' => '106',
                'action_system_id' => '24',
            ],
            [
                'id' => '540',
                'role_id' => '106',
                'action_system_id' => '25',
            ],
            [
                'id' => '541',
                'role_id' => '106',
                'action_system_id' => '26',
            ],
            [
                'id' => '542',
                'role_id' => '106',
                'action_system_id' => '27',
            ],
            [
                'id' => '543',
                'role_id' => '106',
                'action_system_id' => '28',
            ],
            [
                'id' => '544',
                'role_id' => '106',
                'action_system_id' => '29',
            ],
            [
                'id' => '545',
                'role_id' => '106',
                'action_system_id' => '34',
            ],
            [
                'id' => '546',
                'role_id' => '106',
                'action_system_id' => '35',
            ],
            [
                'id' => '547',
                'role_id' => '106',
                'action_system_id' => '39',
            ],
            [
                'id' => '548',
                'role_id' => '106',
                'action_system_id' => '40',
            ],
            [
                'id' => '549',
                'role_id' => '106',
                'action_system_id' => '41',
            ],
            [
                'id' => '550',
                'role_id' => '106',
                'action_system_id' => '42',
            ],
            [
                'id' => '551',
                'role_id' => '106',
                'action_system_id' => '44',
            ],
            [
                'id' => '552',
                'role_id' => '106',
                'action_system_id' => '46',
            ],
            [
                'id' => '553',
                'role_id' => '106',
                'action_system_id' => '47',
            ],
            [
                'id' => '554',
                'role_id' => '106',
                'action_system_id' => '48',
            ],
            [
                'id' => '556',
                'role_id' => '106',
                'action_system_id' => '70',
            ],
            [
                'id' => '557',
                'role_id' => '106',
                'action_system_id' => '74',
            ],
            [
                'id' => '559',
                'role_id' => '106',
                'action_system_id' => '76',
            ],
            [
                'id' => '560',
                'role_id' => '106',
                'action_system_id' => '81',
            ],
            [
                'id' => '561',
                'role_id' => '106',
                'action_system_id' => '86',
            ],
            [
                'id' => '562',
                'role_id' => '106',
                'action_system_id' => '90',
            ],
            [
                'id' => '563',
                'role_id' => '106',
                'action_system_id' => '91',
            ],
            [
                'id' => '564',
                'role_id' => '106',
                'action_system_id' => '92',
            ],
            [
                'id' => '565',
                'role_id' => '106',
                'action_system_id' => '106',
            ],
            [
                'id' => '566',
                'role_id' => '106',
                'action_system_id' => '130',
            ],
            [
                'id' => '567',
                'role_id' => '106',
                'action_system_id' => '149',
            ],
            [
                'id' => '568',
                'role_id' => '106',
                'action_system_id' => '168',
            ],
            [
                'id' => '569',
                'role_id' => '106',
                'action_system_id' => '169',
            ],
            [
                'id' => '571',
                'role_id' => '107',
                'action_system_id' => '10',
            ],
            [
                'id' => '572',
                'role_id' => '107',
                'action_system_id' => '11',
            ],
            [
                'id' => '573',
                'role_id' => '107',
                'action_system_id' => '12',
            ],
            [
                'id' => '574',
                'role_id' => '107',
                'action_system_id' => '13',
            ],
            [
                'id' => '575',
                'role_id' => '107',
                'action_system_id' => '14',
            ],
            [
                'id' => '576',
                'role_id' => '107',
                'action_system_id' => '15',
            ],
            [
                'id' => '577',
                'role_id' => '107',
                'action_system_id' => '16',
            ],
            [
                'id' => '578',
                'role_id' => '107',
                'action_system_id' => '17',
            ],
            [
                'id' => '579',
                'role_id' => '107',
                'action_system_id' => '110',
            ],
            [
                'id' => '580',
                'role_id' => '107',
                'action_system_id' => '112',
            ],
            [
                'id' => '581',
                'role_id' => '107',
                'action_system_id' => '113',
            ],
            [
                'id' => '582',
                'role_id' => '107',
                'action_system_id' => '114',
            ],
            [
                'id' => '583',
                'role_id' => '108',
                'action_system_id' => '29',
            ],
            [
                'id' => '584',
                'role_id' => '108',
                'action_system_id' => '32',
            ],
            [
                'id' => '585',
                'role_id' => '108',
                'action_system_id' => '44',
            ],
            [
                'id' => '586',
                'role_id' => '108',
                'action_system_id' => '47',
            ],
            [
                'id' => '587',
                'role_id' => '108',
                'action_system_id' => '157',
            ],
            [
                'id' => '588',
                'role_id' => '108',
                'action_system_id' => '158',
            ],
            [
                'id' => '589',
                'role_id' => '108',
                'action_system_id' => '159',
            ],
            [
                'id' => '590',
                'role_id' => '108',
                'action_system_id' => '160',
            ],
            [
                'id' => '591',
                'role_id' => '108',
                'action_system_id' => '161',
            ],
            [
                'id' => '592',
                'role_id' => '108',
                'action_system_id' => '129',
            ],
            [
                'id' => '593',
                'role_id' => '109',
                'action_system_id' => '18',
            ],
            [
                'id' => '594',
                'role_id' => '109',
                'action_system_id' => '19',
            ],
            [
                'id' => '595',
                'role_id' => '109',
                'action_system_id' => '20',
            ],
            [
                'id' => '596',
                'role_id' => '109',
                'action_system_id' => '21',
            ],
            [
                'id' => '597',
                'role_id' => '109',
                'action_system_id' => '23',
            ],
            [
                'id' => '598',
                'role_id' => '109',
                'action_system_id' => '39',
            ],
            [
                'id' => '599',
                'role_id' => '109',
                'action_system_id' => '40',
            ],
            [
                'id' => '600',
                'role_id' => '109',
                'action_system_id' => '41',
            ],
            [
                'id' => '601',
                'role_id' => '109',
                'action_system_id' => '42',
            ],
            [
                'id' => '602',
                'role_id' => '109',
                'action_system_id' => '44',
            ],
            [
                'id' => '603',
                'role_id' => '109',
                'action_system_id' => '46',
            ],
            [
                'id' => '604',
                'role_id' => '109',
                'action_system_id' => '47',
            ],
            [
                'id' => '605',
                'role_id' => '109',
                'action_system_id' => '48',
            ],
            [
                'id' => '606',
                'role_id' => '109',
                'action_system_id' => '51',
            ],
            [
                'id' => '607',
                'role_id' => '109',
                'action_system_id' => '54',
            ],
            [
                'id' => '608',
                'role_id' => '109',
                'action_system_id' => '55',
            ],
            [
                'id' => '609',
                'role_id' => '109',
                'action_system_id' => '64',
            ],
            [
                'id' => '610',
                'role_id' => '109',
                'action_system_id' => '70',
            ],
            [
                'id' => '611',
                'role_id' => '109',
                'action_system_id' => '73',
            ],
            [
                'id' => '612',
                'role_id' => '109',
                'action_system_id' => '74',
            ],
            [
                'id' => '613',
                'role_id' => '109',
                'action_system_id' => '75',
            ],
            [
                'id' => '614',
                'role_id' => '109',
                'action_system_id' => '76',
            ],
            [
                'id' => '615',
                'role_id' => '109',
                'action_system_id' => '81',
            ],
            [
                'id' => '616',
                'role_id' => '109',
                'action_system_id' => '82',
            ],
            [
                'id' => '617',
                'role_id' => '109',
                'action_system_id' => '84',
            ],
            [
                'id' => '618',
                'role_id' => '109',
                'action_system_id' => '86',
            ],
            [
                'id' => '619',
                'role_id' => '109',
                'action_system_id' => '91',
            ],
            [
                'id' => '620',
                'role_id' => '109',
                'action_system_id' => '92',
            ],
            [
                'id' => '621',
                'role_id' => '109',
                'action_system_id' => '93',
            ],
            [
                'id' => '622',
                'role_id' => '109',
                'action_system_id' => '95',
            ],
            [
                'id' => '623',
                'role_id' => '109',
                'action_system_id' => '106',
            ],
            [
                'id' => '624',
                'role_id' => '109',
                'action_system_id' => '108',
            ],
            [
                'id' => '625',
                'role_id' => '109',
                'action_system_id' => '115',
            ],
            [
                'id' => '626',
                'role_id' => '109',
                'action_system_id' => '116',
            ],
            [
                'id' => '627',
                'role_id' => '109',
                'action_system_id' => '127',
            ],
            [
                'id' => '628',
                'role_id' => '109',
                'action_system_id' => '128',
            ],
            [
                'id' => '629',
                'role_id' => '109',
                'action_system_id' => '130',
            ],
            [
                'id' => '630',
                'role_id' => '109',
                'action_system_id' => '168',
            ],
            [
                'id' => '631',
                'role_id' => '109',
                'action_system_id' => '169',
            ],
            [
                'id' => '632',
                'role_id' => '109',
                'action_system_id' => '174',
            ],
            [
                'id' => '633',
                'role_id' => '106',
                'action_system_id' => '64',
            ],
            [
                'id' => '634',
                'role_id' => '107',
                'action_system_id' => '64',
            ],
            [
                'id' => '635',
                'role_id' => '108',
                'action_system_id' => '64',
            ],
            [
                'id' => '637',
                'role_id' => '102',
                'action_system_id' => '174',
            ],
            [
                'id' => '638',
                'role_id' => '105',
                'action_system_id' => '64',
            ],
            [
                'id' => '639',
                'role_id' => '104',
                'action_system_id' => '64',
            ],
            [
                'id' => '667',
                'role_id' => '102',
                'action_system_id' => '175',
            ],
            [
                'id' => '668',
                'role_id' => '102',
                'action_system_id' => '176',
            ],
            [
                'id' => '669',
                'role_id' => '102',
                'action_system_id' => '177',
            ],
            [
                'id' => '670',
                'role_id' => '102',
                'action_system_id' => '178',
            ],
            [
                'id' => '671',
                'role_id' => '109',
                'action_system_id' => '175',
            ],
            [
                'id' => '672',
                'role_id' => '109',
                'action_system_id' => '176',
            ],
            [
                'id' => '673',
                'role_id' => '109',
                'action_system_id' => '177',
            ],
            [
                'id' => '674',
                'role_id' => '109',
                'action_system_id' => '178',
            ],           
            [
                'id' => '1237',
                'role_id' => '108',
                'action_system_id' => '177',
            ],
            [
                'id' => '1238',
                'role_id' => '108',
                'action_system_id' => '178',
            ],
            [
                'id' => '1239',
                'role_id' => '102',
                'action_system_id' => '5',
            ],
            [
                'id' => '1244',
                'role_id' => '112',
                'action_system_id' => '179',
            ],
            [
                'id' => '1270',
                'role_id' => '102',
                'action_system_id' => '179',
            ],
            [
                'id' => '1271',
                'role_id' => '102',
                'action_system_id' => '138',
            ],
            [
                'id' => '1272',
                'role_id' => '111',
                'action_system_id' => '138',
            ],
            [
                'id' => '1274',
                'role_id' => '102',
                'action_system_id' => '127',
            ],
            [
                'id' => '1275',
                'role_id' => '102',
                'action_system_id' => '128',
            ],
            [
                'id' => '1276',
                'role_id' => '102',
                'action_system_id' => '129',
            ],
            [
                'id' => '1277',
                'role_id' => '102',
                'action_system_id' => '130',
            ],
            [
                'id' => '1278',
                'role_id' => '102',
                'action_system_id' => '133',
            ],
            [
                'id' => '1281',
                'role_id' => '102',
                'action_system_id' => '187',
            ],
            [
                'id' => '1282',
                'role_id' => '104',
                'action_system_id' => '186',
            ],
            [
                'id' => '1283',
                'role_id' => '104',
                'action_system_id' => '187',
            ],
            [
                'id' => '1285',
                'role_id' => '102',
                'action_system_id' => '188',
            ],           
            [
                'id' => '1293',
                'role_id' => '102',
                'action_system_id' => '189',
            ],
            [
                'id' => '1294',
                'role_id' => '102',
                'action_system_id' => '190',
            ],
            [
                'id' => '1295',
                'role_id' => '102',
                'action_system_id' => '191',
            ],
            [
                'id' => '1296',
                'role_id' => '102',
                'action_system_id' => '192',
            ],
            [
                'id' => '1297',
                'role_id' => '102',
                'action_system_id' => '193',
            ],
            [
                'id' => '1299',
                'role_id' => '102',
                'action_system_id' => '194',
            ],
            [
                'id' => '1300',
                'role_id' => '102',
                'action_system_id' => '195',
            ],
            [
                'id' => '1301',
                'role_id' => '102',
                'action_system_id' => '196',
            ],
            [
                'id' => '1302',
                'role_id' => '102',
                'action_system_id' => '197',
            ],
            [
                'id' => '1303',
                'role_id' => '102',
                'action_system_id' => '198',
            ],
            [
                'id' => '1304',
                'role_id' => '102',
                'action_system_id' => '199',
            ],
            [
                'id' => '1305',
                'role_id' => '102',
                'action_system_id' => '200',
            ],
            [
                'id' => '1306',
                'role_id' => '102',
                'action_system_id' => '201',
            ],
            [
                'id' => '1307',
                'role_id' => '102',
                'action_system_id' => '202',
            ],
            [
                'id' => '1308',
                'role_id' => '102',
                'action_system_id' => '203',
            ],
            [
                'id' => '1309',
                'role_id' => '102',
                'action_system_id' => '204',
            ],
            [
                'id' => '1310',
                'role_id' => '102',
                'action_system_id' => '205',
            ],
            [
                'id' => '1311',
                'role_id' => '102',
                'action_system_id' => '206',
            ],
            [
                'id' => '1312',
                'role_id' => '102',
                'action_system_id' => '207',
            ],
            [
                'id' => '1313',
                'role_id' => '102',
                'action_system_id' => '208',
            ],
            [
                'id' => '1314',
                'role_id' => '102',
                'action_system_id' => '209',
            ],
            [
                'id' => '1315',
                'role_id' => '102',
                'action_system_id' => '210',
            ],
            [
                'id' => '1316',
                'role_id' => '102',
                'action_system_id' => '211',
            ],
            [
                'id' => '1317',
                'role_id' => '102',
                'action_system_id' => '212',
            ],
            [
                'id' => '1318',
                'role_id' => '102',
                'action_system_id' => '213',
            ],
            [
                'id' => '1319',
                'role_id' => '102',
                'action_system_id' => '214',
            ],
            [
                'id' => '1320',
                'role_id' => '102',
                'action_system_id' => '215',
            ],
            [
                'id' => '1321',
                'role_id' => '102',
                'action_system_id' => '216',
            ],
            [
                'id' => '1322',
                'role_id' => '102',
                'action_system_id' => '217',
            ],
            [
                'id' => '1323',
                'role_id' => '102',
                'action_system_id' => '218',
            ],
            [
                'id' => '1324',
                'role_id' => '102',
                'action_system_id' => '219',
            ],
            [
                'id' => '1325',
                'role_id' => '102',
                'action_system_id' => '220',
            ],
            [
                'id' => '1326',
                'role_id' => '102',
                'action_system_id' => '221',
            ],
            [
                'id' => '1327',
                'role_id' => '102',
                'action_system_id' => '222',
            ],
            [
                'id' => '1328',
                'role_id' => '102',
                'action_system_id' => '223',
            ],
            [
                'id' => '1329',
                'role_id' => '102',
                'action_system_id' => '224',
            ],
            [
                'id' => '1330',
                'role_id' => '102',
                'action_system_id' => '226',
            ],
            [
                'id' => '1331',
                'role_id' => '102',
                'action_system_id' => '227',
            ],
            [
                'id' => '1332',
                'role_id' => '102',
                'action_system_id' => '228',
            ],
            [
                'id' => '1333',
                'role_id' => '102',
                'action_system_id' => '229',
            ],
            [
                'id' => '1334',
                'role_id' => '102',
                'action_system_id' => '230',
            ],
            [
                'id' => '1335',
                'role_id' => '102',
                'action_system_id' => '231',
            ],
            [
                'id' => '1336',
                'role_id' => '102',
                'action_system_id' => '232',
            ],
            [
                'id' => '1337',
                'role_id' => '102',
                'action_system_id' => '233',
            ],
            [
                'id' => '1338',
                'role_id' => '102',
                'action_system_id' => '234',
            ],
            [
                'id' => '1339',
                'role_id' => '102',
                'action_system_id' => '235',
            ],
            [
                'id' => '1340',
                'role_id' => '102',
                'action_system_id' => '236',
            ],
            [
                'id' => '1341',
                'role_id' => '102',
                'action_system_id' => '237',
            ],
            [
                'id' => '1342',
                'role_id' => '102',
                'action_system_id' => '238',
            ],
            [
                'id' => '1343',
                'role_id' => '102',
                'action_system_id' => '239',
            ],
            [
                'id' => '1344',
                'role_id' => '102',
                'action_system_id' => '240',
            ],
            [
                'id' => '1345',
                'role_id' => '102',
                'action_system_id' => '241',
            ],
            [
                'id' => '1346',
                'role_id' => '102',
                'action_system_id' => '242',
            ],
            [
                'id' => '1347',
                'role_id' => '102',
                'action_system_id' => '243',
            ],
            [
                'id' => '1348',
                'role_id' => '102',
                'action_system_id' => '244',
            ],
            [
                'id' => '1349',
                'role_id' => '102',
                'action_system_id' => '245',
            ],
            [
                'id' => '1350',
                'role_id' => '102',
                'action_system_id' => '246',
            ],
            [
                'id' => '1351',
                'role_id' => '102',
                'action_system_id' => '247',
            ],
            [
                'id' => '1352',
                'role_id' => '102',
                'action_system_id' => '248',
            ],
            [
                'id' => '1353',
                'role_id' => '102',
                'action_system_id' => '249',
            ],
            [
                'id' => '1354',
                'role_id' => '102',
                'action_system_id' => '250',
            ],
            [
                'id' => '1355',
                'role_id' => '102',
                'action_system_id' => '251',
            ],
            [
                'id' => '1356',
                'role_id' => '102',
                'action_system_id' => '252',
            ],
            [
                'id' => '1357',
                'role_id' => '102',
                'action_system_id' => '253',
            ],
            [
                'id' => '1358',
                'role_id' => '102',
                'action_system_id' => '254',
            ],
            [
                'id' => '1359',
                'role_id' => '102',
                'action_system_id' => '255',
            ],
            [
                'id' => '1360',
                'role_id' => '102',
                'action_system_id' => '256',
            ],
            [
                'id' => '1361',
                'role_id' => '102',
                'action_system_id' => '257',
            ],
            [
                'id' => '1362',
                'role_id' => '102',
                'action_system_id' => '258',
            ],
            [
                'id' => '1363',
                'role_id' => '102',
                'action_system_id' => '259',
            ],
            [
                'id' => '1364',
                'role_id' => '102',
                'action_system_id' => '260',
            ],
            [
                'id' => '1365',
                'role_id' => '102',
                'action_system_id' => '261',
            ],
            [
                'id' => '1366',
                'role_id' => '102',
                'action_system_id' => '262',
            ],
            [
                'id' => '1367',
                'role_id' => '102',
                'action_system_id' => '263',
            ],
            [
                'id' => '1368',
                'role_id' => '102',
                'action_system_id' => '264',
            ],
            [
                'id' => '1369',
                'role_id' => '102',
                'action_system_id' => '265',
            ],
            [
                'id' => '1370',
                'role_id' => '102',
                'action_system_id' => '266',
            ],
            [
                'id' => '1371',
                'role_id' => '102',
                'action_system_id' => '267',
            ],
            [
                'id' => '1372',
                'role_id' => '102',
                'action_system_id' => '268',
            ],
            [
                'id' => '1373',
                'role_id' => '102',
                'action_system_id' => '269',
            ],
            [
                'id' => '1374',
                'role_id' => '102',
                'action_system_id' => '270',
            ],
            [
                'id' => '1375',
                'role_id' => '102',
                'action_system_id' => '271',
            ],
            [
                'id' => '1376',
                'role_id' => '102',
                'action_system_id' => '272',
            ],
            [
                'id' => '1377',
                'role_id' => '102',
                'action_system_id' => '273',
            ],
            [
                'id' => '1378',
                'role_id' => '102',
                'action_system_id' => '274',
            ],
            [
                'id' => '1379',
                'role_id' => '102',
                'action_system_id' => '275',
            ],
            [
                'id' => '1380',
                'role_id' => '102',
                'action_system_id' => '276',
            ],
            [
                'id' => '1381',
                'role_id' => '102',
                'action_system_id' => '277',
            ],
            [
                'id' => '1382',
                'role_id' => '102',
                'action_system_id' => '278',
            ],
            [
                'id' => '1383',
                'role_id' => '102',
                'action_system_id' => '279',
            ],
            [
                'id' => '1384',
                'role_id' => '102',
                'action_system_id' => '280',
            ],
            [
                'id' => '1385',
                'role_id' => '102',
                'action_system_id' => '281',
            ],
            [
                'id' => '1386',
                'role_id' => '102',
                'action_system_id' => '282',
            ],
            [
                'id' => '1387',
                'role_id' => '102',
                'action_system_id' => '283',
            ],
            [
                'id' => '1388',
                'role_id' => '102',
                'action_system_id' => '284',
            ],
            [
                'id' => '1389',
                'role_id' => '102',
                'action_system_id' => '285',
            ],
            [
                'id' => '1390',
                'role_id' => '102',
                'action_system_id' => '286',
            ],
            [
                'id' => '1391',
                'role_id' => '102',
                'action_system_id' => '287',
            ],
            [
                'id' => '1392',
                'role_id' => '102',
                'action_system_id' => '288',
            ],
            [
                'id' => '1393',
                'role_id' => '102',
                'action_system_id' => '289',
            ],
            [
                'id' => '1394',
                'role_id' => '102',
                'action_system_id' => '290',
            ],
            [
                'id' => '1395',
                'role_id' => '102',
                'action_system_id' => '291',
            ],
            [
                'id' => '1396',
                'role_id' => '102',
                'action_system_id' => '292',
            ],
            [
                'id' => '1397',
                'role_id' => '102',
                'action_system_id' => '293',
            ],
            [
                'id' => '1398',
                'role_id' => '102',
                'action_system_id' => '294',
            ],
            [
                'id' => '1399',
                'role_id' => '102',
                'action_system_id' => '295',
            ],
            [
                'id' => '1400',
                'role_id' => '102',
                'action_system_id' => '296',
            ],
            [
                'id' => '1401',
                'role_id' => '102',
                'action_system_id' => '297',
            ],
            [
                'id' => '1402',
                'role_id' => '102',
                'action_system_id' => '298',
            ],
            [
                'id' => '1403',
                'role_id' => '102',
                'action_system_id' => '299',
            ],
            [
                'id' => '1404',
                'role_id' => '102',
                'action_system_id' => '300',
            ],
            [
                'id' => '1405',
                'role_id' => '102',
                'action_system_id' => '301',
            ],
            [
                'id' => '1406',
                'role_id' => '102',
                'action_system_id' => '302',
            ],
            [
                'id' => '1407',
                'role_id' => '102',
                'action_system_id' => '303',
            ],
            [
                'id' => '1408',
                'role_id' => '102',
                'action_system_id' => '304',
            ],
            [
                'id' => '1409',
                'role_id' => '102',
                'action_system_id' => '306',
            ],
            [
                'id' => '1410',
                'role_id' => '102',
                'action_system_id' => '307',
            ],
            [
                'id' => '1411',
                'role_id' => '102',
                'action_system_id' => '308',
            ],
            [
                'id' => '1412',
                'role_id' => '102',
                'action_system_id' => '309',
            ],
            [
                'id' => '1413',
                'role_id' => '102',
                'action_system_id' => '310',
            ],
            [
                'id' => '1414',
                'role_id' => '102',
                'action_system_id' => '311',
            ],
            [
                'id' => '1415',
                'role_id' => '102',
                'action_system_id' => '312',
            ],
            [
                'id' => '1416',
                'role_id' => '102',
                'action_system_id' => '313',
            ],
            [
                'id' => '1417',
                'role_id' => '102',
                'action_system_id' => '314',
            ],
            [
                'id' => '1418',
                'role_id' => '102',
                'action_system_id' => '315',
            ],
            [
                'id' => '1419',
                'role_id' => '102',
                'action_system_id' => '316',
            ],
            [
                'id' => '1420',
                'role_id' => '102',
                'action_system_id' => '317',
            ],
            [
                'id' => '1421',
                'role_id' => '102',
                'action_system_id' => '318',
            ],
            [
                'id' => '1422',
                'role_id' => '102',
                'action_system_id' => '319',
            ],
            [
                'id' => '1423',
                'role_id' => '102',
                'action_system_id' => '320',
            ],
            [
                'id' => '1424',
                'role_id' => '102',
                'action_system_id' => '321',
            ],
            [
                'id' => '1425',
                'role_id' => '102',
                'action_system_id' => '322',
            ],
            [
                'id' => '1426',
                'role_id' => '102',
                'action_system_id' => '323',
            ],
            [
                'id' => '1427',
                'role_id' => '102',
                'action_system_id' => '324',
            ],
            [
                'id' => '1428',
                'role_id' => '102',
                'action_system_id' => '325',
            ],
            [
                'id' => '1429',
                'role_id' => '102',
                'action_system_id' => '326',
            ],
            [
                'id' => '1430',
                'role_id' => '102',
                'action_system_id' => '327',
            ],
            [
                'id' => '1431',
                'role_id' => '102',
                'action_system_id' => '328',
            ],
            [
                'id' => '1432',
                'role_id' => '102',
                'action_system_id' => '329',
            ],
            [
                'id' => '1433',
                'role_id' => '102',
                'action_system_id' => '330',
            ],
            [
                'id' => '1434',
                'role_id' => '102',
                'action_system_id' => '331',
            ],
            [
                'id' => '1435',
                'role_id' => '102',
                'action_system_id' => '332',
            ],
            [
                'id' => '1436',
                'role_id' => '102',
                'action_system_id' => '333',
            ],
            [
                'id' => '1437',
                'role_id' => '102',
                'action_system_id' => '334',
            ],
            [
                'id' => '1438',
                'role_id' => '102',
                'action_system_id' => '335',
            ],
            [
                'id' => '1439',
                'role_id' => '102',
                'action_system_id' => '336',
            ],
            [
                'id' => '1440',
                'role_id' => '102',
                'action_system_id' => '337',
            ],
            [
                'id' => '1441',
                'role_id' => '102',
                'action_system_id' => '338',
            ],
            [
                'id' => '1442',
                'role_id' => '102',
                'action_system_id' => '339',
            ],
            [
                'id' => '1443',
                'role_id' => '102',
                'action_system_id' => '340',
            ],
            [
                'id' => '1444',
                'role_id' => '102',
                'action_system_id' => '341',
            ],
            [
                'id' => '1445',
                'role_id' => '102',
                'action_system_id' => '342',
            ],
            [
                'id' => '1446',
                'role_id' => '102',
                'action_system_id' => '343',
            ],
            [
                'id' => '1447',
                'role_id' => '102',
                'action_system_id' => '344',
            ],
            [
                'id' => '1448',
                'role_id' => '102',
                'action_system_id' => '345',
            ],
            [
                'id' => '1449',
                'role_id' => '102',
                'action_system_id' => '346',
            ],
            [
                'id' => '1450',
                'role_id' => '102',
                'action_system_id' => '347',
            ],
            [
                'id' => '1451',
                'role_id' => '102',
                'action_system_id' => '348',
            ],
            [
                'id' => '1452',
                'role_id' => '102',
                'action_system_id' => '349',
            ],
            [
                'id' => '1453',
                'role_id' => '102',
                'action_system_id' => '350',
            ],
            [
                'id' => '1454',
                'role_id' => '102',
                'action_system_id' => '351',
            ],
            [
                'id' => '1455',
                'role_id' => '102',
                'action_system_id' => '352',
            ],
            [
                'id' => '1456',
                'role_id' => '102',
                'action_system_id' => '353',
            ],
            [
                'id' => '1457',
                'role_id' => '102',
                'action_system_id' => '354',
            ],
            [
                'id' => '1458',
                'role_id' => '102',
                'action_system_id' => '355',
            ],
            [
                'id' => '1459',
                'role_id' => '102',
                'action_system_id' => '356',
            ],
            [
                'id' => '1460',
                'role_id' => '102',
                'action_system_id' => '357',
            ],
            [
                'id' => '1461',
                'role_id' => '102',
                'action_system_id' => '358',
            ],
            [
                'id' => '1462',
                'role_id' => '102',
                'action_system_id' => '359',
            ],
            [
                'id' => '1463',
                'role_id' => '102',
                'action_system_id' => '360',
            ],
            [
                'id' => '1464',
                'role_id' => '102',
                'action_system_id' => '361',
            ],
            [
                'id' => '1465',
                'role_id' => '102',
                'action_system_id' => '362',
            ],
            [
                'id' => '1466',
                'role_id' => '102',
                'action_system_id' => '363',
            ],
            [
                'id' => '1467',
                'role_id' => '102',
                'action_system_id' => '364',
            ],
            [
                'id' => '1468',
                'role_id' => '102',
                'action_system_id' => '365',
            ],
            [
                'id' => '1469',
                'role_id' => '102',
                'action_system_id' => '366',
            ],
            [
                'id' => '1470',
                'role_id' => '102',
                'action_system_id' => '367',
            ],
            [
                'id' => '1471',
                'role_id' => '102',
                'action_system_id' => '368',
            ],
            [
                'id' => '1472',
                'role_id' => '102',
                'action_system_id' => '369',
            ],
            [
                'id' => '1473',
                'role_id' => '102',
                'action_system_id' => '370',
            ],
            [
                'id' => '1474',
                'role_id' => '102',
                'action_system_id' => '371',
            ],
            [
                'id' => '1475',
                'role_id' => '102',
                'action_system_id' => '372',
            ],
            [
                'id' => '1476',
                'role_id' => '102',
                'action_system_id' => '373',
            ],
            [
                'id' => '1477',
                'role_id' => '102',
                'action_system_id' => '374',
            ],
            [
                'id' => '1478',
                'role_id' => '102',
                'action_system_id' => '375',
            ],
            [
                'id' => '1479',
                'role_id' => '102',
                'action_system_id' => '376',
            ],
            [
                'id' => '1480',
                'role_id' => '102',
                'action_system_id' => '377',
            ],
            [
                'id' => '1481',
                'role_id' => '102',
                'action_system_id' => '378',
            ],
            [
                'id' => '1482',
                'role_id' => '102',
                'action_system_id' => '379',
            ],
            [
                'id' => '1483',
                'role_id' => '102',
                'action_system_id' => '380',
            ],
            [
                'id' => '1484',
                'role_id' => '102',
                'action_system_id' => '381',
            ],
            [
                'id' => '1485',
                'role_id' => '102',
                'action_system_id' => '382',
            ],
            [
                'id' => '1486',
                'role_id' => '102',
                'action_system_id' => '383',
            ],
            [
                'id' => '1487',
                'role_id' => '102',
                'action_system_id' => '384',
            ],
            [
                'id' => '1488',
                'role_id' => '102',
                'action_system_id' => '385',
            ],
            [
                'id' => '1489',
                'role_id' => '102',
                'action_system_id' => '386',
            ],
            [
                'id' => '1490',
                'role_id' => '102',
                'action_system_id' => '387',
            ],
            [
                'id' => '1491',
                'role_id' => '102',
                'action_system_id' => '388',
            ],
            [
                'id' => '1492',
                'role_id' => '102',
                'action_system_id' => '389',
            ],
            [
                'id' => '1493',
                'role_id' => '113',
                'action_system_id' => '396',
            ],
        ];

        $table->insert($data)->save();
    }
}
