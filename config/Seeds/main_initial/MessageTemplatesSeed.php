<?php
use Migrations\AbstractSeed;

/**
 * MessageTemplates seed.
 */
class MessageTemplatesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'type' => 'Bloqueo',
                'name' => 'Pantalla de Bloqueo',
                'description' => 'lala',
                'created' => '2017-10-13 19:39:15',
                'modified' => '2017-10-13 19:39:34',
                'html' => '<h1 style="text-align:center"><strong>servicio bloqueado</strong></h1>',
                'deleted' => '0',
                'send_date' => NULL,
                'user_id' => '101',
                'priority' => '1',
                'enabled' => '1',
            ],
        ];

        $table = $this->table('message_templates');
        $table->insert($data)->save();
    }
}
