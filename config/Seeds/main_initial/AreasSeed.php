<?php
use Migrations\AbstractSeed;

/**
 * Zone seed.
 */
class AreasSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'Área A',
                'city_id' => '1',
                'enabled' => '1',
                'deleted' => '0',
            ],
            [
                'id' => '2',
                'name' => 'Área B',
                'city_id' => '1',
                'enabled' => '1',
                'deleted' => '0',
            ],
        ];

        $table = $this->table('areas');
        $table->insert($data)->save();
    }
}
