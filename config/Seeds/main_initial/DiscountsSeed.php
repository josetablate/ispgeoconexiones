<?php
use Migrations\AbstractSeed;

/**
 * Discounts seed.
 */
class DiscountsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'created' => '2018-05-11 02:16:41',
                'concept' => 'DESCUENTO DE 10%',
                'comments' => '',
                'value' => '0.10',
                'always' => '0',
                'duration' => '1',
                'user_id' => '101',
                'code' => 'DES1',
                'deleted' => '0',
                'enabled' => '1',
                'type' => 'package',
            ],
            [
                'id' => '2',
                'created' => '2018-05-11 02:32:31',
                'concept' => 'PRIMEROS 3 MESES BONIFICADO 15%',
                'comments' => '',
                'value' => '0.15',
                'always' => '0',
                'duration' => '3',
                'user_id' => '101',
                'code' => 'DES2',
                'deleted' => '0',
                'enabled' => '1',
                'type' => 'service',
            ],
            [
                'id' => '3',
                'created' => '2018-05-11 16:34:02',
                'concept' => 'DESCUENTO DE 15%',
                'comments' => '',
                'value' => '0.15',
                'always' => '0',
                'duration' => '1',
                'user_id' => '101',
                'code' => 'DES3',
                'deleted' => '0',
                'enabled' => '1',
                'type' => 'product',
            ],
        ];

        $table = $this->table('discounts');
        $table->insert($data)->save();
    }
}
