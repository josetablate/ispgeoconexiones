<?php
use Migrations\AbstractSeed;

/**
 * Users seed.
 */
class UsersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '100',
                'name' => 'Sistema',
                'username' => 'sistema',
                'password' => '$2y$10$TOXoDlCAAA7cfrMYfxaESe5vzkEe9Tzfbx/mqQ1sF/sEkp/Me5Krm',
                'phone' => '',
                'enabled' => '1',
                'created' => '2018-01-28 14:26:30',
                'modified' => '2018-01-28 14:50:43',
                'deleted' => '0',
                'description' => '',
            ],
            [
                'id' => '101',
                'name' => 'Administrador',
                'username' => 'admin',
                'password' => '$2y$10$fubdEIm3WDYwGDxisbGQI.wlSRUZIUZlx3IgKu9B5ML4wt9bMvoJm',
                'phone' => '',
                'enabled' => '1',
                'created' => '2018-01-29 12:55:25',
                'modified' => '2018-01-29 17:08:45',
                'deleted' => '0',
                'description' => '',
            ],
        ];

        $table = $this->table('users');
        $table->insert($data)->save();
    }
}
