<?php
use Migrations\AbstractSeed;

/**
 * RolesUsers seed.
 */
class RolesUsersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '3',
                'user_id' => '101',
                'role_id' => '102',
            ],
        ];

        $table = $this->table('roles_users');
        $table->insert($data)->save();
    }
}
