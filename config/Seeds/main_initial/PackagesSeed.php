<?php
use Migrations\AbstractSeed;

/**
 * Packages seed.
 */
class PackagesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'Instalación Básica',
                'description' => '',
                'created' => '2018-05-02 15:23:19',
                'modified' => '2018-05-02 15:23:19',
                'price' => '800.00',
                'enabled' => '1',
                'deleted' => '0',
                'account_code' => '313000001',
                'aliquot' => 5,
            ],
        ];

        $table = $this->table('packages');
        $table->insert($data)->save();
        
        
         $data = [
            [
                'code' => '313000001',
                'name' => 'Instalación Básica',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
        ];

        $table = $this->table('accounts');
        $table->insert($data)->save();
    }
}
