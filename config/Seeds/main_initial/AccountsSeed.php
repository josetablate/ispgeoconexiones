<?php
use Migrations\AbstractSeed;

/**
 * Accounts seed.
 */
class AccountsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'code' => '100000000',
                'name' => 'ACTIVO',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '1',
            ],
            [
                'code' => '110000000',
                'name' => 'Cajas y Bancos',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '1',
            ],
            [
                'code' => '110000001',
                'name' => 'Banco Chubut (Caja Ahorro 1)',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
             [
                'code' => '110000002',
                'name' => 'Banco Chubut (Caja Ahorro 2)',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
            [
                'code' => '120000000',
                'name' => 'Cuentas x Cobrar',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '1',
            ],
            [
                'code' => '130000000',
                'name' => 'Otras Cuentas x Cobrar',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '1',
            ],
            [
                'code' => '130000001',
                'name' => 'IVA Credito fiscal',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
            [
                'code' => '130000002',
                'name' => 'Retenciones ATP',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
            [
                'code' => '130000003',
                'name' => 'Cobro Digital',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
            [
                'code' => '130000004',
                'name' => 'Todo Pago',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
            [
                'code' => '130000005',
                'name' => 'Mercado Pago',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
            [
                'code' => '130000006',
                'name' => 'Fondos sin Depositar (cheques)',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
            [
                'code' => '140000000',
                'name' => 'Bienes de Cambio',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '1',
            ],
            [
                'code' => '141000000',
                'name' => 'Mercadería Reventa',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '1',
            ],
            [
                'code' => '142000000',
                'name' => 'Materiales Varios',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '1',
            ],
            [
                'code' => '150000000',
                'name' => 'Inversiones',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '1',
            ],
            [
                'code' => '150000001',
                'name' => 'Plazo fijo Banco',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
            [
                'code' => '160000000',
                'name' => 'Bienes de Uso',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '1',
            ],
            [
                'code' => '161000000',
                'name' => 'Rodados',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '1',
            ],
            [
                'code' => '200000000',
                'name' => 'PASIVO',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '1',
            ],
            [
                'code' => '210000000',
                'name' => 'Cuentas a Pagar',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '1',
            ],
            [
                'code' => '220000000',
                'name' => 'Sueldos y L.Sociales por Pagar',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '1',
            ],
            [
                'code' => '220000001',
                'name' => 'Sueldos y Jornales por pagar',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
            [
                'code' => '220000002',
                'name' => 'Leyes Sociales por Pagar',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
            [
                'code' => '230000000',
                'name' => 'Cargar Fiscales por Pagar',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '1',
            ],
            [
                'code' => '230000001',
                'name' => 'Iva por pagar',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
            [
                'code' => '230000002',
                'name' => 'Imp. A los ing. Brutos por pagar',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
            [
                'code' => '230000003',
                'name' => 'Imp. A las Ganancias por pagar',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
            [
                'code' => '240000000',
                'name' => 'Otras Cuentas a Pagar ',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '1',
            ],
            [
                'code' => '300000000',
                'name' => 'RESULTADO',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '1',
            ],
            [
                'code' => '310000000',
                'name' => 'Resultado Positivo',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '1',
            ],
            [
                'code' => '311000000',
                'name' => 'Venta Servicios',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '1',
            ],
            [
                'code' => '311000001',
                'name' => 'Bonificación de Servicios',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
            [
                'code' => '312000000',
                'name' => 'Venta de Mercadería',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '1',
            ],
            [
                'code' => '313000000',
                'name' => 'Venta de Instalaciones',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '1',
            ],
            [
                'code' => '320000000',
                'name' => 'Resultado Negativo',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '1',
            ],
            [
                'code' => '320000001',
                'name' => 'Alquiler Nodo 1',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
            [
                'code' => '320000002',
                'name' => 'Gastos Generales',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
            [
                'code' => '320000003',
                'name' => 'Bonificaciones Concedidas',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
            [
                'code' => '320000004',
                'name' => 'Compras',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
            [
                'code' => '320000005',
                'name' => 'Recargos',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
            [
                'code' => '320000006',
                'name' => 'Instaladores',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
            [
                'code' => '320000007',
                'name' => 'Comisiones',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
            [
                'code' => '320000008',
                'name' => 'Sueldo Tecnico',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
            [
                'code' => '400000000',
                'name' => 'PATRIMONIO NETO',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '1',
            ],
            [
                'code' => '400000001',
                'name' => 'Capital',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
            [
                'code' => '400000003',
                'name' => 'Resultado del ejercicio',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
            [
                'code' => '400000004',
                'name' => 'Resultado ejercicios anteriores',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
            [
                'code' => '400000005',
                'name' => 'Reservas',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
        ];

        $table = $this->table('accounts');
        $table->insert($data)->save();
    }
}
