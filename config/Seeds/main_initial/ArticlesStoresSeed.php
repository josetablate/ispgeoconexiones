<?php
use Migrations\AbstractSeed;

/**
 * ArticlesStores seed.
 */
class ArticlesStoresSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'current_amount' => '100',
                'created' => '2018-05-05 10:01:52',
                'modified' => '2018-05-05 10:01:52',
                'store_id' => '1',
                'article_id' => '1',
            ],
        ];

        $table = $this->table('articles_stores');
        $table->insert($data)->save();
    }
}
