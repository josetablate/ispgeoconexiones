<?php
use Migrations\AbstractSeed;

/**
 * Articles seed.
 */
class ArticlesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'code' => 'TPLINK750',
                'name' => 'TP-Link 750mbps',
                'created' => '2018-05-05 10:00:11',
                'modified' => '2018-05-05 10:01:52',
                'total_amount' => '100',
                'exist_snid' => '0',
                'deleted' => '0',
                'enabled' => '1',
            ],
        ];

        $table = $this->table('articles');
        $table->insert($data)->save();
    }
}
