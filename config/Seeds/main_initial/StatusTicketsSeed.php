<?php
use Migrations\AbstractSeed;

/**
 * StatusTickets seed.
 */
class StatusTicketsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'Abierto',
                'deleted' => '0',
                'ordering' => '0',
            ],
            [
                'id' => '2',
                'name' => 'Cerrado',
                'deleted' => '0',
                'ordering' => '1',
            ],
            [
                'id' => '3',
                'name' => 'Asignado',
                'deleted' => '0',
                'ordering' => '1',
            ],
        ];

        $table = $this->table('status_tickets');
        $table->insert($data)->save();
    }
}
