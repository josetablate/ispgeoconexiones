<?php
use Migrations\AbstractSeed;

/**
 * Provinces seed.
 */
class ProvincesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'CABA',
                'country_id' => '1',
                'enabled' => '1',
            ],
            [
                'id' => '2',
                'name' => 'Buenos Aires',
                'country_id' => '1',
                'enabled' => '1',
            ],
            [
                'id' => '3',
                'name' => 'Catamarca',
                'country_id' => '1',
                'enabled' => '1',
            ],
            [
                'id' => '4',
                'name' => 'Chaco',
                'country_id' => '1',
                'enabled' => '1',
            ],
            [
                'id' => '5',
                'name' => 'Chubut',
                'country_id' => '1',
                'enabled' => '1',
            ],
            [
                'id' => '6',
                'name' => 'Córdoba',
                'country_id' => '1',
                'enabled' => '1',
            ],
            [
                'id' => '7',
                'name' => 'Corrientes',
                'country_id' => '1',
                'enabled' => '1',
            ],
            [
                'id' => '8',
                'name' => 'Entre Ríos',
                'country_id' => '1',
                'enabled' => '1',
            ],
            [
                'id' => '9',
                'name' => 'Formosa',
                'country_id' => '1',
                'enabled' => '1',
            ],
            [
                'id' => '10',
                'name' => 'Jujuy',
                'country_id' => '1',
                'enabled' => '1',
            ],
            [
                'id' => '11',
                'name' => 'La Pampa',
                'country_id' => '1',
                'enabled' => '1',
            ],
            [
                'id' => '12',
                'name' => 'La Rioja',
                'country_id' => '1',
                'enabled' => '1',
            ],
            [
                'id' => '13',
                'name' => 'Mendoza',
                'country_id' => '1',
                'enabled' => '1',
            ],
            [
                'id' => '14',
                'name' => 'Misiones',
                'country_id' => '1',
                'enabled' => '1',
            ],
            [
                'id' => '15',
                'name' => 'Neuquén',
                'country_id' => '1',
                'enabled' => '1',
            ],
            [
                'id' => '16',
                'name' => 'Río Negro',
                'country_id' => '1',
                'enabled' => '1',
            ],
            [
                'id' => '17',
                'name' => 'Salta',
                'country_id' => '1',
                'enabled' => '1',
            ],
            [
                'id' => '18',
                'name' => 'San Juan',
                'country_id' => '1',
                'enabled' => '1',
            ],
            [
                'id' => '19',
                'name' => 'San Luis',
                'country_id' => '1',
                'enabled' => '1',
            ],
            [
                'id' => '20',
                'name' => 'Santa Cruz',
                'country_id' => '1',
                'enabled' => '1',
            ],
            [
                'id' => '21',
                'name' => 'Santa Fe',
                'country_id' => '1',
                'enabled' => '1',
            ],
            [
                'id' => '22',
                'name' => 'Santiago del Estero',
                'country_id' => '1',
                'enabled' => '1',
            ],
            [
                'id' => '23',
                'name' => 'Tierra del Fuego',
                'country_id' => '1',
                'enabled' => '1',
            ],
            [
                'id' => '24',
                'name' => 'Tucumán',
                'country_id' => '1',
                'enabled' => '1',
            ],
        ];

        $table = $this->table('provinces');
        $table->insert($data)->save();
    }
}
