<?php
use Migrations\AbstractSeed;

/**
 * Roles seed.
 */
class RolesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '100',
                'name' => 'Sistema',
                'created' => '2018-01-28 14:26:51',
                'modified' => '2018-02-13 20:51:30',
                'enabled' => '1',
                'deleted' => '0',
            ],
            [
                'id' => '102',
                'name' => 'Todos los permisos',
                'created' => '2018-01-28 14:42:34',
                'modified' => '2019-07-13 18:04:34',
                'enabled' => '1',
                'deleted' => '0',
            ],
            [
                'id' => '104',
                'name' => 'Vendedor',
                'created' => '2018-01-29 15:48:26',
                'modified' => '2018-01-29 16:32:32',
                'enabled' => '1',
                'deleted' => '0',
            ],
            [
                'id' => '105',
                'name' => 'Técnico',
                'created' => '2018-01-29 16:03:05',
                'modified' => '2018-01-29 16:32:10',
                'enabled' => '1',
                'deleted' => '0',
            ],
            [
                'id' => '106',
                'name' => 'Cobrador',
                'created' => '2018-01-29 16:10:08',
                'modified' => '2018-01-29 16:36:54',
                'enabled' => '1',
                'deleted' => '0',
            ],
            [
                'id' => '107',
                'name' => 'Stock',
                'created' => '2018-01-29 16:11:53',
                'modified' => '2018-01-29 16:26:54',
                'enabled' => '1',
                'deleted' => '0',
            ],
            [
                'id' => '108',
                'name' => 'Tickets',
                'created' => '2018-01-29 16:12:46',
                'modified' => '2018-02-13 16:13:46',
                'enabled' => '1',
                'deleted' => '0',
            ],
            [
                'id' => '109',
                'name' => 'Administrativo',
                'created' => '2018-01-29 16:26:15',
                'modified' => '2018-01-29 23:47:26',
                'enabled' => '1',
                'deleted' => '0',
            ],
            [
                'id' => '111',
                'name' => 'Avisos',
                'created' => '2018-01-29 16:41:36',
                'modified' => '2018-02-13 15:59:06',
                'enabled' => '1',
                'deleted' => '0',
            ],
            [
                'id' => '112',
                'name' => 'SMS Masivo',
                'created' => '2018-02-08 18:23:35',
                'modified' => '2018-02-08 18:23:35',
                'enabled' => '1',
                'deleted' => '0',
            ],
            [
                'id' => '113',
                'name' => 'Licencia',
                'created' => '2018-02-08 18:23:35',
                'modified' => '2018-02-08 18:23:35',
                'enabled' => '1',
                'deleted' => '0',
            ],
        ];

        $table = $this->table('roles');
        $table->insert($data)->save();
    }
}
