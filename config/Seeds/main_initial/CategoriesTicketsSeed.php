<?php
use Migrations\AbstractSeed;

/**
 * CategoriesTickets seed.
 */
class CategoriesTicketsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'Soporte Técnico',
                'deleted' => '0',
                'ordering' => '1',
                'color_back' => '#2196f3',
                'color_text' => '#ffffff',
            ],
            [
                'id' => '2',
                'name' => 'Instalación',
                'deleted' => '0',
                'ordering' => '2',
                'color_back' => '#008000',
                'color_text' => '#ffffff',
            ],
            [
                'id' => '3',
                'name' => 'Facturación',
                'deleted' => '0',
                'ordering' => '3',
                'color_back' => '#f2f200',
                'color_text' => '#000000',
            ],
            [
                'id' => '4',
                'name' => 'Venta',
                'deleted' => '0',
                'ordering' => '4',
                'color_back' => '#ff0000',
                'color_text' => '#ffffff',
            ],
        ];

        $table = $this->table('categories_tickets');
        $table->insert($data)->save();
    }
}
