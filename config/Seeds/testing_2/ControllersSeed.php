<?php
use Migrations\AbstractSeed;

/**
 * Controllers seed.
 */
class ControllersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'N1 (IP Fija)',
                'template' => 'template-b',
                'description' => '',
                'lat' => NULL,
                'lng' => NULL,
                'ip' => '190.106.130.81',
                'port' => '2010',
                'username' => 'admin',
                'password' => '123456',
                'enabled' => '1',
                'created' => '2017-11-18 10:58:31',
                'modified' => '2017-11-18 10:58:31',
                'trademark' => 'mikrotik',
                'message_method' => 'met_a',
                'deleted' => '0',
            ],
            [
                'id' => '2',
                'name' => 'N2 (PPPoe + Grafica)',
                'template' => 'template-c',
                'description' => '',
                'lat' => NULL,
                'lng' => NULL,
                'ip' => '190.106.130.66',
                'port' => NULL,
                'username' => 'admin',
                'password' => '123456',
                'enabled' => '1',
                'created' => '2017-11-18 10:59:03',
                'modified' => '2017-11-18 10:59:03',
                'trademark' => 'mikrotik',
                'message_method' => 'met_a',
                'deleted' => '0',
            ],
        ];

        $table = $this->table('controllers');
        $table->insert($data)->save();
    }
}
