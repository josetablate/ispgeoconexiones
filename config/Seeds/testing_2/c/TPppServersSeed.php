<?php
use Migrations\AbstractSeed;

/**
 * TPppServers seed.
 */
class TPppServersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'interface' => 'ether1',
                'one_session_per_host' => 'yes',
                'authentication' => 'chap,mschap1,mschap2',
                'service_name' => 'ISPBrain-PPPoEServer',
                'disabled' => 'no',
                'controller_id' => '2',
                'api_id' => NULL,
            ],
        ];

        $table = $this->table('t_ppp_servers');
        $table->insert($data)->save();
    }
}
