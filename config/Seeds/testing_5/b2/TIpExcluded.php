<?php
use Migrations\AbstractSeed;

/**
 * TProfiles seed.
 */
class TIpExcluded extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '3',
                'ip' => ip2long('10.10.30.1'),
                'comments' => 'Gateway pool3mb',
                'controller_id' => '2'
            ],
            [
                'id' => '4',
                'ip' => ip2long('10.10.35.1'),
                'comments' => 'Gateway pool6mb',
                'controller_id' => '2'
            ],
        ];

        $table = $this->table('t_ip_excluded');
        $table->insert($data)->save();
    }
}
