<?php
use Migrations\AbstractSeed;

/**
 * Accounts seed.
 */
class AccountsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'code' => '311000002',
                'name' => 'Plan 3MB',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
            [
                'code' => '311000003',
                'name' => 'Plan 6MB',
                'description' => NULL,
                'status' => '1',
                'saldo' => '0.00',
                'title' => '0',
            ],
        ];

        $table = $this->table('accounts');
        $table->insert($data)->save();
    }
}
