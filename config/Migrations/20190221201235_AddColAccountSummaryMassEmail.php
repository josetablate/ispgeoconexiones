<?php
use Migrations\AbstractMigration;

class AddColAccountSummaryMassEmail extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('mass_emails_templates')
            ->addColumn('account_summary_attach', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->save();

        $this->table('mass_emails_sms')
            ->addColumn('account_summary_attach', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->save();
    }
}
