<?php
use Migrations\AbstractMigration;

class AddColBillingForServiceCustomer extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('customers');
        $table->addColumn('billing_for_service', 'boolean', [
            'default' => false,
            'limit' => null,
            'null' => false,
        ])->save();
    }
}
                