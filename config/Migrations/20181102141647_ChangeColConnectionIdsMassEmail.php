<?php
use Migrations\AbstractMigration;

class ChangeColConnectionIdsMassEmail extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('mass_emails_sms')
            ->changeColumn('connections_id', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->changeColumn('invoices_id', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->changeColumn('receipts_id', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->save();
    }
}
