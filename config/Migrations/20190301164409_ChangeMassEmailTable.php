<?php
use Migrations\AbstractMigration;

class ChangeMassEmailTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('mass_emails_sms')
            ->changeColumn('receipt_attach', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->save();

        $this->table('mass_emails_templates')
            ->changeColumn('receipt_attach', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->save();
    }
}
