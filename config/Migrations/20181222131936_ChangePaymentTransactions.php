<?php
use Migrations\AbstractMigration;

class ChangePaymentTransactions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('payu_transactions')
            ->changeColumn('receipt_number', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->save();

        $this->table('cobrodigital_transactions')
            ->changeColumn('receipt_number', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->save();
    }
}
