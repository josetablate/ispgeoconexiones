<?php
use Migrations\AbstractMigration;

class AddColumSeatingNumber extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
         $this->table('cash_entities_transactions')
            ->addColumn('seating_number', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->save();
    }
}
