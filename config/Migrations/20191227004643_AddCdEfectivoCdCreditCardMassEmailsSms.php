<?php
use Migrations\AbstractMigration;

class AddCdEfectivoCdCreditCardMassEmailsSms extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('mass_emails_sms')
            ->addColumn('cd_efectivo', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('cd_credit_card', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->save();
    }
}
