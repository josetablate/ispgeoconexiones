<?php
use Migrations\AbstractMigration;

class AddColumnControllers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('controllers');

        $table->addColumn('integration', 'boolean', [
            'default' => true,
            'limit' => null,
            'null' => false,
        ])->update();

    
    }
}
