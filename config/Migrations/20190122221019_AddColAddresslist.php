<?php
use Migrations\AbstractMigration;

class AddColAddresslist extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('firewall_address_lists')
            ->addColumn('before_address', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => true,
            ])
            ->save();
    }
}
