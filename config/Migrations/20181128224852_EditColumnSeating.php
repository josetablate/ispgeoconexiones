<?php
use Migrations\AbstractMigration;

class EditColumnSeating extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        
        $table = $this->table('seating');
        $table->changeColumn('concept', 'string', [
            'default' => null,
            'limit' => 200,
            'null' => true,
        ])->save();
    }
}
