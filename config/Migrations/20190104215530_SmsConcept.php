<?php
use Migrations\AbstractMigration;

class SmsConcept extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('concepts')
            ->changeColumn('description', 'text', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])->save();

        $this->table('concepts_debit')
            ->changeColumn('description', 'text', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])->save();

        $this->table('concepts_credit')
            ->changeColumn('description', 'text', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])->save();
    }
}
