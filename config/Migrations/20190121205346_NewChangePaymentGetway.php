<?php
use Migrations\AbstractMigration;

class NewChangePaymentGetway extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('payu_transactions')
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('receipt_id', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->save();

        $this->table('cobrodigital_transactions')
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('receipt_id', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->save();

        $this->table('connections')
            ->addColumn('visa_auto_debit', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('mastercard_auto_debit', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->save();
    }
}
