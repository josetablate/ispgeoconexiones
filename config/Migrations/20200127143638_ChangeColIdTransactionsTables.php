<?php
use Migrations\AbstractMigration;

class ChangeColIdTransactionsTables extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('mp_transactions')
            ->changeColumn('payment_id', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->save();

        $this->table('cobrodigital_transactions')
            ->changeColumn('id_transaccion', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->save();

        $this->table('payu_transactions')
            ->changeColumn('id_orden', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->save();

        $this->table('mastercard_auto_debit_transactions')
            ->changeColumn('id_transaction', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->save();

        $this->table('visa_auto_debit_transactions')
            ->changeColumn('id_transaction', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->save();
    }
}
