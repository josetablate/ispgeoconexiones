<?php
use Migrations\AbstractMigration;

class ChangeMessageColMassEmail extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('mass_emails_templates');
        $table->changeColumn('message', 'text', [
            'default' => '',
            'limit' => null,
            'null' => false,
        ])->save();
    }
}
