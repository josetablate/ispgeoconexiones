<?php
use Migrations\AbstractMigration;

class ChangeObservations extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('observations');
        $table->renameColumn('model_id', 'customer_code');
        $table->removeColumn('model')
            ->save();
    }
}
