<?php
use Migrations\AbstractMigration;

class AddColumnsConnections extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
         $this->table('connections')
            ->addColumn('queue_name', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ]) 
            ->addColumn('pppoe_name', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ])
            ->addColumn('pppoe_pass', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ])
            ->save();
    }
}
