<?php
use Migrations\AbstractMigration;

class AddBusinessDefaultArea extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('areas')
            ->addColumn('business_billing_default', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->save();
    }
}
