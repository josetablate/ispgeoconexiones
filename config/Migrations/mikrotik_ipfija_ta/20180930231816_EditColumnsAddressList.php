<?php
use Migrations\AbstractMigration;

class EditColumnsAddressList extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('address_lists');
        $table->changeColumn('address', 'biginteger', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
              ->save();
    }
}
