<?php
use Migrations\AbstractMigration;

class EditColumnIpExluded extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $users = $this->table('ip_excluded');
        $users->changeColumn('ip', 'string', ['limit' => 45])
              ->save();
    }
}
