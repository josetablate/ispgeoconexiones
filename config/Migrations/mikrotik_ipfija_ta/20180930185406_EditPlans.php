<?php
use Migrations\AbstractMigration;

class EditPlans extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $users = $this->table('plans');
        $users->changeColumn('pool_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
              ->save();
    }
}
