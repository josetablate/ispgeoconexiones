<?php
use Migrations\AbstractMigration;

class AddIdTransactionsCuentaDigital extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('cuentadigital_transactions')
            ->removeIndex([
                'fecha_cobro',
                'hhmmss',
                'barcode'
            ])
            ->addColumn('id_transaccion', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addIndex(
                [
                    'id_transaccion',
                ],
                [
                    'unique' => true
                ]
            )->save();
    }
}
