<?php
use Migrations\AbstractMigration;

class ChangeCodeConnections extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('connections')
            ->changeColumn('discount_code', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ])
            ->save();
    }
}
