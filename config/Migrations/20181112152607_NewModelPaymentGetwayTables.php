<?php
use Migrations\AbstractMigration;

class NewModelPaymentGetwayTables extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('receipts')
            ->addColumn('barcode_alt', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => true,
            ])
            ->addColumn('electronic_code', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => true,
            ])
            ->addColumn('id_comercio', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('firstname', 'string', [
                'default' => null,
                'limit' => 150,
                'null' => true,
            ])
            ->addColumn('lastname', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => true,
            ])
            ->addColumn('cbu', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ])
            ->addColumn('cuit', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 80,
                'null' => true,
            ])->save();

        $this->table('customers_accounts')
            ->addColumn('customer_code', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('account_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])->addColumn('payment_getway_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])->addColumn('deleted', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])->save();

        $this->table('auto_debits')
            ->changeColumn('payment_method_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->removeColumn('id_comercio')
            ->renameColumn('payment_method_id', 'payment_getway_id')
            ->removeColumn('enabled')
            ->addColumn('deleted', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])->save();

        $this->table('auto_debits')
            ->rename('auto_debits_accounts');

        $this->table('cobrodigital_cards')
            ->changeColumn('barcode', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ])
            ->changeColumn('electronic_code', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ])
            ->addColumn('firstname', 'string', [
                'default' => null,
                'limit' => 150,
                'null' => true,
            ])
            ->addColumn('lastname', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => true,
            ])
            ->addColumn('cbu', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ])
            ->addColumn('cuit', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 80,
                'null' => true,
            ])
            ->addColumn('payment_getway_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])->save();

        $this->table('cobrodigital_cards')
            ->rename('cobrodigital_accounts');

        $this->table('service_pending')
            ->removeColumn('auto_debit_id')
            ->removeColumn('cobrodigital_card_id')
            ->removeColumn('payment_connection_id')
            ->addColumn('account_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('pay_with_account', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => true,
            ])->save();
    }
}
