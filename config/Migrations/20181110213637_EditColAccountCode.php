<?php
use Migrations\AbstractMigration;

class EditColAccountCode extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('services');
        $table->changeColumn('account_code', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
        ])->save();
        
        $table = $this->table('products');
        $table->changeColumn('account_code', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
        ])->save();
        
        $table = $this->table('packages');
        $table->changeColumn('account_code', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
        ])->save();
        
        $table = $this->table('customers');
        $table->changeColumn('account_code', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
        ])->save();
        
        $table = $this->table('cash_entities');
        $table->changeColumn('account_code', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
        ])->save();
    }
}
