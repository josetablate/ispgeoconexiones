<?php
use Migrations\AbstractMigration;

class AddEditDnsServerProfiles extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $users = $this->table('profiles');
        $users->changeColumn('dns_server', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ])
              ->save();
    }
}
