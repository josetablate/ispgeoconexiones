<?php
use Migrations\AbstractMigration;

class AddAccountNumberCuentaDigitalAccount extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('cuentadigital_accounts')
            ->addColumn('account_number', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => true,
            ])
            ->save();
    }
}
