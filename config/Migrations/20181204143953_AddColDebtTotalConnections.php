<?php
use Migrations\AbstractMigration;

class AddColDebtTotalConnections extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
         $this->table('connections')
            ->addColumn('debt_total', 'decimal', [
                'default' => 0,
                'null' => false,
                'precision' => 10,
                'scale' => 2,
            ])
            ->save();
    }
}
