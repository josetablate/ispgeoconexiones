<?php
use Migrations\AbstractMigration;

class AddColVencSaldoMassEmailsSms extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('mass_emails_sms')
            ->addColumn('cd_venc', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('cd_saldo', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->save();
    }
}
