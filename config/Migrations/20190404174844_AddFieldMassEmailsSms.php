<?php
use Migrations\AbstractMigration;

class AddFieldMassEmailsSms extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('mass_emails_sms')
            ->addColumn('email_id', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('payment_link_mercadopago', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('template_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])->save();
    }
}
