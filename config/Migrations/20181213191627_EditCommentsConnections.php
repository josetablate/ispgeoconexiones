<?php
use Migrations\AbstractMigration;

class EditCommentsConnections extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('connections')
            ->changeColumn('comments', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])->save();
    }
}
