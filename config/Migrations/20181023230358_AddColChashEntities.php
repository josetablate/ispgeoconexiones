<?php
use Migrations\AbstractMigration;

class AddColChashEntities extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('cash_entities');
        
        $table->addColumn('cash_other', 'decimal', [
                'default' => 0,
                'null' => false,
                'precision' => 10,
                'scale' => 2,
        ])
        ->addColumn('contado', 'decimal', [
                'default' => 0,
                'null' => false,
                'precision' => 10,
                'scale' => 2,
        ])
        ->save();
    
    }
}
