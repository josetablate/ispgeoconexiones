<?php
use Migrations\AbstractMigration;

class NewTableRealtimeSpeed extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
  
    public function change()
    {
        $this->table('real_time_speed')
            ->addColumn('up', 'decimal', [
               'default' => '0.00',
                'null' => true,
                'precision' => 10,
                'scale' => 2,
            ])
            ->addColumn('down', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 10,
                'scale' => 2,
            ])
            ->addColumn('connection_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->create();
    }
   
}
