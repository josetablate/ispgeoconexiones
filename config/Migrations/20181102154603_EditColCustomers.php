<?php
use Migrations\AbstractMigration;

class EditColCustomers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('customers');
        $table->changeColumn('ident', 'string', [
             'default' => null,
                'limit' => 45,
                'null' => true,
        ])->save();
    }
}
