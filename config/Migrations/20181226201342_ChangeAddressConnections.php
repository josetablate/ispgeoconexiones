<?php
use Migrations\AbstractMigration;

class ChangeAddressConnections extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('connections')
            ->changeColumn('address', 'string', [
                'default' => null,
                'limit' => 150,
                'null' => false,
            ])
            ->save();
    }
}
