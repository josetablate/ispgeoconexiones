<?php
use Migrations\AbstractMigration;

class AddColMoveChash2 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('int_out_cashs_entities');
        
        $table->addColumn('is_table', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ])
        ->save();
    }
}
