<?php
use Migrations\AbstractMigration;

class AddDebtIdConnectionDebtMonth extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('connections_debts_month')
             ->addColumn('debt_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->save();
    }
}
