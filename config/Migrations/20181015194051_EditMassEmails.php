<?php
use Migrations\AbstractMigration;

class EditMassEmails extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('mass_emails_blocks')
            ->changeColumn('status', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ])
            ->save();

        $this->table('mass_emails_sms')
            ->changeColumn('status', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ])
            ->save();
    }
}
