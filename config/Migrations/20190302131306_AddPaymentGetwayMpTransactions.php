<?php
use Migrations\AbstractMigration;

class AddPaymentGetwayMpTransactions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('mp_transactions')
            ->addColumn('payment_getway_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])->save();
    }
}
