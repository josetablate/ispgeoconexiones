<?php
use Migrations\AbstractMigration;

class EditColumnSeatingNumber extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
          
        $table = $this->table('invoices');
        $table->changeColumn('seating_number', 'string', [
                'default' => null,
                'limit' =>  100,
                'null' => true,
        ])->save();
        
        $table = $this->table('payments');
        $table->changeColumn('seating_number', 'string', [
                'default' => null,
                'limit' =>  100,
                'null' => true,
        ])->save();
        
        $table = $this->table('credit_notes');
        $table->changeColumn('seating_number', 'string', [
                'default' => null,
                'limit' =>  100,
                'null' => true,
        ])->save();
        
        $table = $this->table('debit_notes');
        $table->changeColumn('seating_number', 'string', [
                'default' => null,
                'limit' =>  100,
                'null' => true,
        ])->save();
    }
}
