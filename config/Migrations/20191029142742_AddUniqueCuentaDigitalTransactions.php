<?php
use Migrations\AbstractMigration;

class AddUniqueCuentaDigitalTransactions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('cuentadigital_transactions')
            ->addIndex(
                [
                    'fecha_cobro',
                    'hhmmss',
                    'barcode'
                ], 
                [
                    'unique' => true
                ]
            )->save();
    }
}
