<?php
use Migrations\AbstractMigration;

class ChangeColSeatingNumberPayments extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('payments')
            ->changeColumn('seating_number', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])->save();

        $this->table('customers_has_discounts')
            ->changeColumn('seating_number', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])->save();

        $this->table('products')
            ->changeColumn('account_code', 'integer', [
                'default' => null,
                'limit' => 9,
                'null' => true,
            ])->save();

        $this->table('packages')
            ->changeColumn('account_code', 'integer', [
                'default' => null,
                'limit' => 9,
                'null' => true,
            ])->save();

        $this->table('cash_entities')
            ->changeColumn('account_code', 'integer', [
                'default' => null,
                'limit' => 9,
                'null' => true,
            ])->save();

        $this->table('services')
            ->changeColumn('account_code', 'integer', [
                'default' => null,
                'limit' => 9,
                'null' => true,
            ])->save();
    }
}
