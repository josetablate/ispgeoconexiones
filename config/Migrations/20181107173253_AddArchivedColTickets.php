<?php
use Migrations\AbstractMigration;

class AddArchivedColTickets extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('tickets')
            ->addColumn('archived', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])->save();
    }
}
