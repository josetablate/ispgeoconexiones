<?php
use Migrations\AbstractMigration;

class ChangeDecimalPackageProduct extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('packages')
            ->changeColumn('price', 'decimal', [
                'default' => 0,
                'null' => false,
                'precision' => 10,
                'scale' => 2
            ])
            ->save();

        $this->table('products')
            ->changeColumn('unit_price', 'decimal', [
                'default' => 0,
                'null' => false,
                'precision' => 10,
                'scale' => 2
            ])
            ->save();
    }
}
