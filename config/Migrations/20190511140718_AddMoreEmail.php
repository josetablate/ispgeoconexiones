<?php
use Migrations\AbstractMigration;

class AddMoreEmail extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('customers')
            ->changeColumn('email', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->save();

        $this->table('tp_transactions')
            ->changeColumn('CUSTOMEREMAIL', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->save();

        $this->table('credit_notes')
            ->changeColumn('company_email', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->save();

        $this->table('debit_notes')
            ->changeColumn('company_email', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->save();

        $this->table('invoices')
            ->changeColumn('company_email', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->save();

        $this->table('receipts')
            ->changeColumn('company_email', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->save();
    }
}
