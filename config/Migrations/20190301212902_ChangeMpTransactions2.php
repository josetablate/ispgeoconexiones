<?php
use Migrations\AbstractMigration;

class ChangeMpTransactions2 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('mp_transactions')
            ->changeColumn('status_detail', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])->save();
    }
}
