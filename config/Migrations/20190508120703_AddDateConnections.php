<?php
use Migrations\AbstractMigration;

class AddDateConnections extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('connections')
            ->addColumn('blocking_date', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('enabling_date', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->save();
    }
}
