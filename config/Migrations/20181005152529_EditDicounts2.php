<?php
use Migrations\AbstractMigration;

class EditDicounts2 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        
        $table = $this->table('concepts');
        $table->changeColumn('discount', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 10,
                'scale' => 2,
        ])->save();
        $table = $this->table('concepts_credit');
        $table->changeColumn('discount', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 10,
                'scale' => 2,
        ])->save();
        $table = $this->table('concepts_debit');
        $table->changeColumn('discount', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 10,
                'scale' => 2,
        ])->save();
    }
}
