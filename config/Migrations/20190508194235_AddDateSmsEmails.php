<?php
use Migrations\AbstractMigration;

class AddDateSmsEmails extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('customers')
            ->addColumn('last_emails', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('last_sms', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->save();
    }
}
