<?php
use Migrations\AbstractMigration;

class AddFechaPagoCobroDigitalTransactions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('cobrodigital_transactions')
            ->addColumn('fecha_pago', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->save();
    }
}
