<?php
use Migrations\AbstractMigration;

class AddAnulatedInvoices extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('invoices')
            ->addColumn('anulated', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])->save();
    }
}
