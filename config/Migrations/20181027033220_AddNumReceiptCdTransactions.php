<?php
use Migrations\AbstractMigration;

class AddNumReceiptCdTransactions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('cobrodigital_transactions')
            ->addColumn('receipt_number', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => true,
            ])
            ->save();
    }
}
