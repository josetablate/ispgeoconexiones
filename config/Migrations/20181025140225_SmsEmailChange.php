<?php
use Migrations\AbstractMigration;

class SmsEmailChange extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('sms_templates')
            ->changeColumn('message', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => false,
            ])->save();

        $this->table('sms')
            ->changeColumn('message', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => false,
            ])->save();

        $this->table('mass_emails_sms')
            ->changeColumn('message', 'text', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])->save();

        $this->table('mass_emails_templates')
            ->changeColumn('message', 'text', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])->save();
    }
}
