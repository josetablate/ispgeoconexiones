<?php
use Migrations\AbstractMigration;

class AddPayuTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->table('payu_accounts')
            ->addColumn('card_number', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => true,
            ])
            ->addColumn('barcode', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => true,
            ])
            ->addColumn('id_comercio', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('customer_code', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('payment_getway_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])->addColumn('used', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])->addColumn('deleted', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])->create();
    }

    public function down()
    {
        $this->dropTable('payu_accounts');
    }
}
