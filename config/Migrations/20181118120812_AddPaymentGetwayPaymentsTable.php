<?php
use Migrations\AbstractMigration;

class AddPaymentGetwayPaymentsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('payments')
            ->addColumn('account_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->save();

        $this->table('service_pending')
            ->changeColumn('payment_method_id', 'integer', [
                'default' => null,
                'limit' => 2,
                'null' => true,
            ])
            ->save();

        $this->table('cobrodigital_transactions')
            ->addColumn('payment_getway_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->save();

        $this->table('invoices')
            ->addColumn('cd_auto_debit_date_exported', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->save();

        $this->table('connections')
            ->addColumn('cobrodigital_card', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('cobrodigital_auto_debit', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('chubut_auto_debit', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->save();
    }
}
