<?php
use Migrations\AbstractMigration;

class AddColumMoveCash3 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('int_out_cashs_entities');
        
        $table->renameColumn('saldo', 'saldo_contado');
        
        $table->addColumn('saldo_other', 'decimal', [
                'default' => 0,
                'null' => false,
                'precision' => 10,
                'scale' => 2,
        ])
        ->save();
    }
}
