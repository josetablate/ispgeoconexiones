<?php
use Migrations\AbstractMigration;

class EditNameServices extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('services')
            ->changeColumn('name', 'string', [
                'default' => null,
                'limit' => 60,
                'null' => false,
            ])->save();
    }
}
