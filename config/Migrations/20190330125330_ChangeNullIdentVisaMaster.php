<?php
use Migrations\AbstractMigration;

class ChangeNullIdentVisaMaster extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('visa_auto_debit_records')
            ->changeColumn('customer_ident', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ])->save();

        $this->table('mastercard_auto_debit_records')
            ->changeColumn('customer_ident', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ])->save();
    }
}
