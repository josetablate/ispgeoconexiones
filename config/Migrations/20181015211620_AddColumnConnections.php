<?php
use Migrations\AbstractMigration;

class AddColumnConnections extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('connections');
        $table->addColumn('debt_period_generated', 'text', [
            'default' => null,
            'limit' => null,
            'null' => true,
        ])->save();
    }
}
