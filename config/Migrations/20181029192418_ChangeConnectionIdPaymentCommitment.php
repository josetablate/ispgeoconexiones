<?php
use Migrations\AbstractMigration;

class ChangeConnectionIdPaymentCommitment extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('payment_commitment')
            ->changeColumn('connection_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('import', 'decimal', [
                'default' => null,
                'null' => true,
                'precision' => 10,
                'scale' => 2,
            ])
            ->save();

        $this->table('customers')
            ->addColumn('payment_commitment', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->save();
    }
}
