<?php
use Migrations\AbstractMigration;

class AddColumnDateDiffControllers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
          $this->table('controllers')
            ->addColumn('last_control_diff', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ]) 
            ->addColumn('last_status', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->save();
    }
}
