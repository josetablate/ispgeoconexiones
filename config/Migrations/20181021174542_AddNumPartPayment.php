<?php
use Migrations\AbstractMigration;

class AddNumPartPayment extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('payments')
            ->addColumn('number_part', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ])
            ->save();
    }
}
