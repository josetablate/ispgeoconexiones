<?php
use Migrations\AbstractMigration;

class AddPaymentLinkCuentaDigital extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('mass_emails_sms')
            ->addColumn('payment_link_cuentadigital', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])->save();
    }
}
