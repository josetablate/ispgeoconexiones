<?php
use Migrations\AbstractMigration;

class AddIndexConnectionsDebtMonth extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('connections_debts_month')
            ->addIndex(
                [
                    'connection_id',
                    'period'
                ], 
                [
                    'unique' => true
                ]
            )->save();
    }
}
