<?php
use Migrations\AbstractMigration;

class EditColComnet extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('queues');
        $table->changeColumn('comment', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ])->save();
        
        $table = $this->table('leases');
        $table->changeColumn('comment', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ])->save();
    }
}
