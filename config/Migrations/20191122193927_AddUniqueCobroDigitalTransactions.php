<?php
use Migrations\AbstractMigration;

class AddUniqueCobroDigitalTransactions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('cobrodigital_transactions')
            ->addIndex([
                'id_transaccion',
            ],
            [
                'unique' => true
            ])
            ->save();
    }
}
