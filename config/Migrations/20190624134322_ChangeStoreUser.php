<?php
use Migrations\AbstractMigration;

class ChangeStoreUser extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('stores')
            ->changeColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 4,
                'null' => true,
            ])
            ->save();
    }
}
