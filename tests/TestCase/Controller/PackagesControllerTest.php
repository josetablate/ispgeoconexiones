<?php
namespace App\Test\TestCase\Controller;

use App\Controller\PackagesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\PackagesController Test Case
 */
class PackagesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.packages',
        'app.instalations',
        'app.products',
        'app.stock',
        'app.stores',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.controllers',
        'app.actions_system_roles',
        'app.roles_users',
        'app.stock_stores',
        'app.packages_products',
        'app.services',
        'app.current_account_services',
        'app.connections',
        'app.customers',
        'app.cities',
        'app.networks',
        'app.nodes',
        'app.speeds',
        'app.free_pool_ips',
        'app.suports',
        'app.transactions',
        'app.connections_services',
        'app.packages_services'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
