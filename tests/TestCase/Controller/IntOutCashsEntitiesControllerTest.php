<?php
namespace App\Test\TestCase\Controller;

use App\Controller\IntOutCashsEntitiesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\IntOutCashsEntitiesController Test Case
 */
class IntOutCashsEntitiesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.int_out_cashs_entities',
        'app.cash_entities',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.clases',
        'app.actions_system_roles',
        'app.roles_users',
        'app.payments',
        'app.customers',
        'app.zone',
        'app.road_map',
        'app.debts',
        'app.invoices',
        'app.concepts',
        'app.comprobantes',
        'app.receipts',
        'app.services',
        'app.connections',
        'app.controllers',
        'app.firewall_address_lists',
        'app.web_proxy_access',
        'app.instalations',
        'app.packages',
        'app.logs',
        'app.products',
        'app.articles',
        'app.snid_stores',
        'app.stores',
        'app.articles_stores',
        'app.instalations_articles',
        'app.packages_articles',
        'app.accounts',
        'app.packages_sales',
        'app.tickets',
        'app.tickets_records',
        'app.payment_methods'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
