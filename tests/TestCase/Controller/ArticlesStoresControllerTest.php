<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ArticlesStoresController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ArticlesStoresController Test Case
 */
class ArticlesStoresControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.articles_stores',
        'app.stores',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.controllers',
        'app.actions_system_roles',
        'app.roles_users',
        'app.snid_stores',
        'app.articles',
        'app.products',
        'app.debts',
        'app.connections_services',
        'app.services',
        'app.profiles',
        'app.networks',
        'app.routers',
        'app.connections',
        'app.cities',
        'app.customers',
        'app.road_map',
        'app.payments',
        'app.cash_entities',
        'app.int_out_cashs_entities',
        'app.docs',
        'app.payment_methods',
        'app.instalations',
        'app.packages',
        'app.logs',
        'app.packages_sales',
        'app.packages_articles',
        'app.instalations_articles',
        'app.messages',
        'app.tickets',
        'app.tickets_records',
        'app.free_pool_ips'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
