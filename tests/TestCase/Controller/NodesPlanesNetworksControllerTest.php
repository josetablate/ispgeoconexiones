<?php
namespace App\Test\TestCase\Controller;

use App\Controller\NodesPlanesNetworksController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\NodesPlanesNetworksController Test Case
 */
class NodesPlanesNetworksControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.nodes_planes_networks',
        'app.nodes_plans',
        'app.plans',
        'app.connections',
        'app.users',
        'app.nodes',
        'app.networks'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
