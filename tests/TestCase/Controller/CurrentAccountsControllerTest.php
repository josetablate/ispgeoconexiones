<?php
namespace App\Test\TestCase\Controller;

use App\Controller\CurrentAccountsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\CurrentAccountsController Test Case
 */
class CurrentAccountsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.current_accounts',
        'app.products',
        'app.stock',
        'app.stores',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.controllers',
        'app.actions_system_roles',
        'app.roles_users',
        'app.stock_stores',
        'app.packages_products',
        'app.packages',
        'app.instalations',
        'app.connections',
        'app.customers',
        'app.road_map',
        'app.cities',
        'app.networks',
        'app.nodes',
        'app.speeds',
        'app.free_pool_ips',
        'app.suports',
        'app.transactions',
        'app.connections_services',
        'app.services',
        'app.current_account_services',
        'app.packages_services',
        'app.instalations_products',
        'app.bills',
        'app.bills_current_accounts'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
