<?php
namespace App\Test\TestCase\Controller;

use App\Controller\CustomersHasDiscountsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\CustomersHasDiscountsController Test Case
 */
class CustomersHasDiscountsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.customers_has_discounts',
        'app.debts',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.clases',
        'app.actions_system_roles',
        'app.roles_users',
        'app.invoices',
        'app.customers',
        'app.countries',
        'app.provinces',
        'app.cities',
        'app.areas',
        'app.connections',
        'app.controllers',
        'app.services',
        'app.accounts',
        'app.discounts',
        'app.services_has_discounts',
        'app.firewall_address_lists',
        'app.web_proxy_access',
        'app.instalations',
        'app.packages',
        'app.packages_sales',
        'app.articles',
        'app.products',
        'app.logs',
        'app.snid_stores',
        'app.stores',
        'app.articles_stores',
        'app.instalations_articles',
        'app.packages_articles',
        'app.tickets',
        'app.tickets_records',
        'app.labels',
        'app.connections_labels',
        'app.customers_labels',
        'app.road_map',
        'app.comprobantes',
        'app.payments',
        'app.cash_entities',
        'app.int_out_cashs_entities',
        'app.receipts',
        'app.concepts'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
