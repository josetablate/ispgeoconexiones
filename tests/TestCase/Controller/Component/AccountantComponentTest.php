<?php
namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\AccountantComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\AccountantComponent Test Case
 */
class AccountantComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Controller\Component\AccountantComponent
     */
    public $Accountant;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->Accountant = new AccountantComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Accountant);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
