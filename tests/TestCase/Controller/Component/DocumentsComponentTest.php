<?php
namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\DocumentsComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\DocumentsComponent Test Case
 */
class DocumentsComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Controller\Component\DocumentsComponent
     */
    public $Documents;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->Documents = new DocumentsComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Documents);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
