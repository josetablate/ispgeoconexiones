<?php
namespace App\Test\TestCase\Controller;

use App\Controller\PackagesUsersController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\PackagesUsersController Test Case
 */
class PackagesUsersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.packages_users',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.controllers',
        'app.actions_system_roles',
        'app.roles_users',
        'app.packages',
        'app.instalations',
        'app.connections',
        'app.customers',
        'app.road_map',
        'app.current_accounts',
        'app.products',
        'app.stock',
        'app.stores',
        'app.stock_stores',
        'app.packages_products',
        'app.services',
        'app.speeds',
        'app.networks',
        'app.nodes',
        'app.free_pool_ips',
        'app.connections_services',
        'app.bills',
        'app.payments',
        'app.payment_methods',
        'app.cash_entities',
        'app.bills_current_accounts',
        'app.cities',
        'app.suports',
        'app.transactions',
        'app.instalations_products'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
