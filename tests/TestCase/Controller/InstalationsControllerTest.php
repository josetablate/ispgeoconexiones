<?php
namespace App\Test\TestCase\Controller;

use App\Controller\InstalationsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\InstalationsController Test Case
 */
class InstalationsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.instalations',
        'app.connections',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.controllers',
        'app.actions_system_roles',
        'app.roles_users',
        'app.customers',
        'app.road_map',
        'app.cities',
        'app.networks',
        'app.nodes',
        'app.speeds',
        'app.free_pool_ips',
        'app.suports',
        'app.transactions',
        'app.packages',
        'app.packages_products',
        'app.products',
        'app.stock',
        'app.stores',
        'app.stock_stores',
        'app.additional_products'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
