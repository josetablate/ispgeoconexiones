<?php
namespace App\Test\TestCase\Controller;

use App\Controller\InstalationsTypesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\InstalationsTypesController Test Case
 */
class InstalationsTypesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.instalations_types',
        'app.bailment',
        'app.instalations_types_bailment',
        'app.materials',
        'app.installer_materials',
        'app.instalations_types_materials',
        'app.products',
        'app.installer_products',
        'app.instalations_types_products'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
