<?php
namespace App\Test\TestCase\Controller;

use App\Controller\LabelsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\LabelsController Test Case
 */
class LabelsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.labels',
        'app.connections',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.clases',
        'app.actions_system_roles',
        'app.roles_users',
        'app.zone',
        'app.customers',
        'app.road_map',
        'app.debts',
        'app.invoices',
        'app.concepts',
        'app.comprobantes',
        'app.payments',
        'app.cash_entities',
        'app.int_out_cashs_entities',
        'app.receipts',
        'app.services',
        'app.accounts',
        'app.instalations',
        'app.packages',
        'app.packages_sales',
        'app.articles',
        'app.products',
        'app.logs',
        'app.snid_stores',
        'app.stores',
        'app.articles_stores',
        'app.instalations_articles',
        'app.packages_articles',
        'app.customers_labels',
        'app.controllers',
        'app.firewall_address_lists',
        'app.web_proxy_access',
        'app.tickets',
        'app.tickets_records',
        'app.connections_labels'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
