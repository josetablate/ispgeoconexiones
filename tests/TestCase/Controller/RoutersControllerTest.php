<?php
namespace App\Test\TestCase\Controller;

use App\Controller\RoutersController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\RoutersController Test Case
 */
class RoutersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.routers',
        'app.networks',
        'app.profiles',
        'app.services',
        'app.logs',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.controllers',
        'app.actions_system_roles',
        'app.roles_users',
        'app.connections',
        'app.cities',
        'app.customers',
        'app.road_map',
        'app.debts',
        'app.connections_services',
        'app.packages_sales',
        'app.packages',
        'app.instalations',
        'app.articles',
        'app.products',
        'app.accounts',
        'app.snid_stores',
        'app.stores',
        'app.articles_stores',
        'app.instalations_articles',
        'app.packages_articles',
        'app.docs',
        'app.payments',
        'app.cash_entities',
        'app.int_out_cashs_entities',
        'app.payment_methods',
        'app.messages',
        'app.tickets',
        'app.tickets_records',
        'app.free_pool_ips'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
