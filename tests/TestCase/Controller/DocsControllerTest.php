<?php
namespace App\Test\TestCase\Controller;

use App\Controller\DocsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\DocsController Test Case
 */
class DocsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.docs',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.controllers',
        'app.actions_system_roles',
        'app.roles_users',
        'app.debts',
        'app.connections_services',
        'app.services',
        'app.speeds',
        'app.networks',
        'app.nodes',
        'app.free_pool_ips',
        'app.current_accounts',
        'app.products',
        'app.stock',
        'app.stores',
        'app.stock_stores',
        'app.instalations',
        'app.connections',
        'app.customers',
        'app.road_map',
        'app.payments',
        'app.cash_entities',
        'app.movements',
        'app.payment_methods',
        'app.commercial_docs_concepts',
        'app.commercial_docs',
        'app.packages_sales',
        'app.packages',
        'app.packages_products',
        'app.cities',
        'app.suports',
        'app.transactions',
        'app.instalations_products',
        'app.bills',
        'app.bills_current_accounts'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
