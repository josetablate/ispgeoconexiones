<?php
namespace App\Test\TestCase\Controller;

use App\Controller\MessagesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\MessagesController Test Case
 */
class MessagesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.messages',
        'app.customers',
        'app.road_map',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.controllers',
        'app.actions_system_roles',
        'app.roles_users',
        'app.current_accounts',
        'app.products',
        'app.stock',
        'app.stores',
        'app.stock_stores',
        'app.services',
        'app.speeds',
        'app.networks',
        'app.nodes',
        'app.free_pool_ips',
        'app.connections',
        'app.cities',
        'app.connections_services',
        'app.movements',
        'app.cash_entities',
        'app.payment_methods',
        'app.payments',
        'app.bills',
        'app.bills_current_accounts',
        'app.commercial_docs_concepts',
        'app.commercial_docs',
        'app.packages_sales',
        'app.packages',
        'app.instalations',
        'app.instalations_products',
        'app.packages_products',
        'app.suports',
        'app.transactions'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
