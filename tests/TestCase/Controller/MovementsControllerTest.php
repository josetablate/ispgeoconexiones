<?php
namespace App\Test\TestCase\Controller;

use App\Controller\MovementsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\MovementsController Test Case
 */
class MovementsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.movements',
        'app.cash_entities',
        'app.payments',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.controllers',
        'app.actions_system_roles',
        'app.roles_users',
        'app.bills',
        'app.current_accounts',
        'app.products',
        'app.stock',
        'app.stores',
        'app.stock_stores',
        'app.services',
        'app.speeds',
        'app.networks',
        'app.nodes',
        'app.free_pool_ips',
        'app.connections',
        'app.customers',
        'app.road_map',
        'app.cities',
        'app.connections_services',
        'app.instalations',
        'app.packages',
        'app.packages_products',
        'app.instalations_products',
        'app.suports',
        'app.transactions',
        'app.bills_current_accounts',
        'app.payment_methods',
        'app.commercial_docs_concepts',
        'app.commercial_docs'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
