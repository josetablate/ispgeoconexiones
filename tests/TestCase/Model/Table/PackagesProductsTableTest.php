<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PackagesProductsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PackagesProductsTable Test Case
 */
class PackagesProductsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PackagesProductsTable
     */
    public $PackagesProducts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.packages_products',
        'app.products',
        'app.stock',
        'app.stores',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.controllers',
        'app.actions_system_roles',
        'app.roles_users',
        'app.stock_stores',
        'app.packages',
        'app.instalations'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PackagesProducts') ? [] : ['className' => 'App\Model\Table\PackagesProductsTable'];
        $this->PackagesProducts = TableRegistry::get('PackagesProducts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PackagesProducts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
