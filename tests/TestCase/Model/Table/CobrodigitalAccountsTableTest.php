<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CobrodigitalAccountsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CobrodigitalAccountsTable Test Case
 */
class CobrodigitalAccountsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CobrodigitalAccountsTable
     */
    public $CobrodigitalAccounts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cobrodigital_accounts',
        'app.payment_getways'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CobrodigitalAccounts') ? [] : ['className' => CobrodigitalAccountsTable::class];
        $this->CobrodigitalAccounts = TableRegistry::getTableLocator()->get('CobrodigitalAccounts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CobrodigitalAccounts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
