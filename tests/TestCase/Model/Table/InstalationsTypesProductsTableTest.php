<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InstalationsTypesProductsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InstalationsTypesProductsTable Test Case
 */
class InstalationsTypesProductsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InstalationsTypesProductsTable
     */
    public $InstalationsTypesProducts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.instalations_types_products',
        'app.instalations_types',
        'app.bailment',
        'app.bailment_snid',
        'app.instalations',
        'app.connections',
        'app.users',
        'app.plans',
        'app.nodes',
        'app.instalations_bailment',
        'app.instalations_types_bailment',
        'app.materials',
        'app.instalations_types_materials',
        'app.products'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('InstalationsTypesProducts') ? [] : ['className' => 'App\Model\Table\InstalationsTypesProductsTable'];
        $this->InstalationsTypesProducts = TableRegistry::get('InstalationsTypesProducts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InstalationsTypesProducts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
