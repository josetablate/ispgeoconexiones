<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CommercialDocsConceptsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CommercialDocsConceptsTable Test Case
 */
class CommercialDocsConceptsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CommercialDocsConceptsTable
     */
    public $CommercialDocsConcepts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.commercial_docs_concepts',
        'app.movements',
        'app.cash_entities',
        'app.payments',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.controllers',
        'app.actions_system_roles',
        'app.roles_users',
        'app.bills',
        'app.current_accounts',
        'app.products',
        'app.stock',
        'app.stores',
        'app.stock_stores',
        'app.services',
        'app.speeds',
        'app.networks',
        'app.nodes',
        'app.free_pool_ips',
        'app.connections',
        'app.customers',
        'app.road_map',
        'app.cities',
        'app.connections_services',
        'app.instalations',
        'app.packages',
        'app.packages_products',
        'app.instalations_products',
        'app.suports',
        'app.transactions',
        'app.bills_current_accounts',
        'app.payment_methods',
        'app.commercial_docs'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CommercialDocsConcepts') ? [] : ['className' => 'App\Model\Table\CommercialDocsConceptsTable'];
        $this->CommercialDocsConcepts = TableRegistry::get('CommercialDocsConcepts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CommercialDocsConcepts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
