<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CuentadigitalAccountsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CuentadigitalAccountsTable Test Case
 */
class CuentadigitalAccountsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CuentadigitalAccountsTable
     */
    public $CuentadigitalAccounts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cuentadigital_accounts',
        'app.payment_getways'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CuentadigitalAccounts') ? [] : ['className' => CuentadigitalAccountsTable::class];
        $this->CuentadigitalAccounts = TableRegistry::getTableLocator()->get('CuentadigitalAccounts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CuentadigitalAccounts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
