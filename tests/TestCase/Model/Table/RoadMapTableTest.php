<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RoadMapTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RoadMapTable Test Case
 */
class RoadMapTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RoadMapTable
     */
    public $RoadMap;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.road_map',
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('RoadMap') ? [] : ['className' => 'App\Model\Table\RoadMapTable'];
        $this->RoadMap = TableRegistry::get('RoadMap', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RoadMap);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
