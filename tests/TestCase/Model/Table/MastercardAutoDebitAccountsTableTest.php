<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MastercardAutoDebitAccountsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MastercardAutoDebitAccountsTable Test Case
 */
class MastercardAutoDebitAccountsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MastercardAutoDebitAccountsTable
     */
    public $MastercardAutoDebitAccounts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.mastercard_auto_debit_accounts',
        'app.payment_getways'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('MastercardAutoDebitAccounts') ? [] : ['className' => MastercardAutoDebitAccountsTable::class];
        $this->MastercardAutoDebitAccounts = TableRegistry::getTableLocator()->get('MastercardAutoDebitAccounts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MastercardAutoDebitAccounts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
