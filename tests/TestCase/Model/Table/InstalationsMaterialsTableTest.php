<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InstalationsMaterialsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InstalationsMaterialsTable Test Case
 */
class InstalationsMaterialsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InstalationsMaterialsTable
     */
    public $InstalationsMaterials;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.instalations_materials',
        'app.materials',
        'app.instalations',
        'app.instalations_types',
        'app.connections',
        'app.users',
        'app.plans',
        'app.nodes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('InstalationsMaterials') ? [] : ['className' => 'App\Model\Table\InstalationsMaterialsTable'];
        $this->InstalationsMaterials = TableRegistry::get('InstalationsMaterials', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InstalationsMaterials);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
