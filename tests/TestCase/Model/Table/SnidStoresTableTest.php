<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SnidStoresTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SnidStoresTable Test Case
 */
class SnidStoresTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SnidStoresTable
     */
    public $SnidStores;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.snid_stores',
        'app.articles',
        'app.products',
        'app.debts',
        'app.connections_services',
        'app.services',
        'app.profiles',
        'app.networks',
        'app.routers',
        'app.connections',
        'app.users',
        'app.cities',
        'app.customers',
        'app.instalations',
        'app.packages',
        'app.logs',
        'app.packages_sales',
        'app.packages_articles',
        'app.instalations_articles',
        'app.messages',
        'app.tickets',
        'app.free_pool_ips',
        'app.docs',
        'app.payments',
        'app.cash_entities',
        'app.int_out_cashs_entities',
        'app.payment_methods',
        'app.stores',
        'app.articles_stores'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SnidStores') ? [] : ['className' => 'App\Model\Table\SnidStoresTable'];
        $this->SnidStores = TableRegistry::get('SnidStores', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SnidStores);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
