<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TicketsRecordsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TicketsRecordsTable Test Case
 */
class TicketsRecordsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TicketsRecordsTable
     */
    public $TicketsRecords;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tickets_records',
        'app.tickets',
        'app.users',
        'app.connections',
        'app.networks',
        'app.routers',
        'app.profiles',
        'app.services',
        'app.logs',
        'app.products',
        'app.articles',
        'app.snid_stores',
        'app.stores',
        'app.articles_stores',
        'app.instalations',
        'app.packages',
        'app.packages_sales',
        'app.packages_articles',
        'app.instalations_articles',
        'app.debts',
        'app.connections_services',
        'app.docs',
        'app.payments',
        'app.cash_entities',
        'app.int_out_cashs_entities',
        'app.payment_methods',
        'app.free_pool_ips',
        'app.cities',
        'app.customers',
        'app.messages'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TicketsRecords') ? [] : ['className' => 'App\Model\Table\TicketsRecordsTable'];
        $this->TicketsRecords = TableRegistry::get('TicketsRecords', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TicketsRecords);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
