<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PresupuestosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PresupuestosTable Test Case
 */
class PresupuestosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PresupuestosTable
     */
    public $Presupuestos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.presupuestos',
        'app.users',
        'app.connections',
        'app.businesses',
        'app.presupuesto_concepts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Presupuestos') ? [] : ['className' => PresupuestosTable::class];
        $this->Presupuestos = TableRegistry::getTableLocator()->get('Presupuestos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Presupuestos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
