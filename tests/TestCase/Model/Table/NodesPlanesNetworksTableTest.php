<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NodesPlanesNetworksTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NodesPlanesNetworksTable Test Case
 */
class NodesPlanesNetworksTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NodesPlanesNetworksTable
     */
    public $NodesPlanesNetworks;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.nodes_planes_networks',
        'app.nodes_plans',
        'app.plans',
        'app.connections',
        'app.users',
        'app.nodes',
        'app.networks'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('NodesPlanesNetworks') ? [] : ['className' => 'App\Model\Table\NodesPlanesNetworksTable'];
        $this->NodesPlanesNetworks = TableRegistry::get('NodesPlanesNetworks', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NodesPlanesNetworks);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
