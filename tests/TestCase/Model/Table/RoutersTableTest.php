<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RoutersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RoutersTable Test Case
 */
class RoutersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RoutersTable
     */
    public $Routers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.routers',
        'app.networks',
        'app.profiles',
        'app.services',
        'app.connections',
        'app.users',
        'app.cities',
        'app.customers',
        'app.connections_services',
        'app.instalations',
        'app.packages',
        'app.logs',
        'app.products',
        'app.articles',
        'app.snid_stores',
        'app.stores',
        'app.articles_stores',
        'app.instalations_articles',
        'app.packages_articles',
        'app.debts',
        'app.packages_sales',
        'app.docs',
        'app.payments',
        'app.cash_entities',
        'app.int_out_cashs_entities',
        'app.payment_methods',
        'app.messages',
        'app.tickets',
        'app.free_pool_ips'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Routers') ? [] : ['className' => 'App\Model\Table\RoutersTable'];
        $this->Routers = TableRegistry::get('Routers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Routers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
