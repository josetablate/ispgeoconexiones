<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NodesPlansTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NodesPlansTable Test Case
 */
class NodesPlansTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NodesPlansTable
     */
    public $NodesPlans;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.nodes_plans',
        'app.plans',
        'app.connections',
        'app.users',
        'app.nodes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('NodesPlans') ? [] : ['className' => 'App\Model\Table\NodesPlansTable'];
        $this->NodesPlans = TableRegistry::get('NodesPlans', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NodesPlans);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
