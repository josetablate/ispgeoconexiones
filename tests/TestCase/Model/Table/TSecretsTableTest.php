<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TSecretsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TSecretsTable Test Case
 */
class TSecretsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TSecretsTable
     */
    public $TSecrets;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.t_secrets',
        'app.connections',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.clases',
        'app.actions_system_roles',
        'app.roles_users',
        'app.cities',
        'app.customers',
        'app.road_map',
        'app.debts',
        'app.connections_services',
        'app.packages_sales',
        'app.packages',
        'app.instalations',
        'app.articles',
        'app.products',
        'app.logs',
        'app.plans',
        'app.accounts',
        'app.snid_stores',
        'app.stores',
        'app.articles_stores',
        'app.instalations_articles',
        'app.packages_articles',
        'app.docs',
        'app.payments',
        'app.cash_entities',
        'app.int_out_cashs_entities',
        'app.payment_methods',
        'app.seating',
        'app.seating_detail',
        'app.services',
        'app.controllers',
        'app.messages',
        'app.tickets',
        'app.tickets_records',
        'app.t_controllers',
        'app.apis',
        'app.t_pools',
        'app.t_profiles',
        'app.t_plans'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TSecrets') ? [] : ['className' => 'App\Model\Table\TSecretsTable'];
        $this->TSecrets = TableRegistry::get('TSecrets', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TSecrets);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
