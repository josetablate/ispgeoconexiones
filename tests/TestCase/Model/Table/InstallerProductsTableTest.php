<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InstallerProductsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InstallerProductsTable Test Case
 */
class InstallerProductsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InstallerProductsTable
     */
    public $InstallerProducts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.installer_products',
        'app.users',
        'app.products'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('InstallerProducts') ? [] : ['className' => 'App\Model\Table\InstallerProductsTable'];
        $this->InstallerProducts = TableRegistry::get('InstallerProducts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InstallerProducts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
