<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StockStoresTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StockStoresTable Test Case
 */
class StockStoresTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StockStoresTable
     */
    public $StockStores;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.stock_stores',
        'app.stores',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.controllers',
        'app.actions_system_roles',
        'app.roles_users',
        'app.stock',
        'app.products'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('StockStores') ? [] : ['className' => 'App\Model\Table\StockStoresTable'];
        $this->StockStores = TableRegistry::get('StockStores', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->StockStores);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
