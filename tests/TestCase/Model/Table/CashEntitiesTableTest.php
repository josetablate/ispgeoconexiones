<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CashEntitiesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CashEntitiesTable Test Case
 */
class CashEntitiesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CashEntitiesTable
     */
    public $CashEntities;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cash_entities',
        'app.users',
        'app.int_out_cashs_entities',
        'app.payments'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CashEntities') ? [] : ['className' => 'App\Model\Table\CashEntitiesTable'];
        $this->CashEntities = TableRegistry::get('CashEntities', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CashEntities);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
