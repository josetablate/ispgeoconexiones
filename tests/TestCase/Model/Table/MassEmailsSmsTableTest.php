<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MassEmailsSmsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MassEmailsSmsTable Test Case
 */
class MassEmailsSmsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MassEmailsSmsTable
     */
    public $MassEmailsSms;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.mass_emails_sms',
        'app.connections',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.clases',
        'app.actions_system_roles',
        'app.roles_users',
        'app.cash_entities',
        'app.int_out_cashs_entities',
        'app.payments',
        'app.customers',
        'app.countries',
        'app.provinces',
        'app.cities',
        'app.areas',
        'app.debts',
        'app.invoices',
        'app.concepts',
        'app.customers_has_discounts',
        'app.comprobantes',
        'app.receipts',
        'app.debit_notes',
        'app.concepts_debit',
        'app.credit_notes',
        'app.concepts_credit',
        'app.instalations',
        'app.packages',
        'app.packages_sales',
        'app.articles',
        'app.products',
        'app.logs',
        'app.accounts',
        'app.snid_stores',
        'app.stores',
        'app.articles_stores',
        'app.instalations_articles',
        'app.packages_articles',
        'app.labels',
        'app.customers_labels',
        'app.service_pending',
        'app.presales',
        'app.services',
        'app.discounts',
        'app.services_has_discounts',
        'app.tickets',
        'app.tickets_records',
        'app.categories_tickets',
        'app.status_tickets',
        'app.controllers',
        'app.firewall_address_lists',
        'app.web_proxy_access',
        'app.connections_labels',
        'app.connections_cobrodigital_cards',
        'app.cobrodigital_cards',
        'app.connections_auto_debits',
        'app.auto_debits',
        'app.bloques'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('MassEmailsSms') ? [] : ['className' => MassEmailsSmsTable::class];
        $this->MassEmailsSms = TableRegistry::get('MassEmailsSms', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MassEmailsSms);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
