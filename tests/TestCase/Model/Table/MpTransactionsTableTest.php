<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MpTransactionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MpTransactionsTable Test Case
 */
class MpTransactionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MpTransactionsTable
     */
    public $MpTransactions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.mp_transactions',
        'app.payments',
        'app.collectors',
        'app.currencies',
        'app.payment_types',
        'app.connections',
        'app.receipts',
        'app.bloques'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('MpTransactions') ? [] : ['className' => MpTransactionsTable::class];
        $this->MpTransactions = TableRegistry::getTableLocator()->get('MpTransactions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MpTransactions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
