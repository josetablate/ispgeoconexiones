<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MigrationFromExcelTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MigrationFromExcelTable Test Case
 */
class MigrationFromExcelTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MigrationFromExcelTable
     */
    public $MigrationFromExcel;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.migration_from_excel'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('MigrationFromExcel') ? [] : ['className' => MigrationFromExcelTable::class];
        $this->MigrationFromExcel = TableRegistry::getTableLocator()->get('MigrationFromExcel', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MigrationFromExcel);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
