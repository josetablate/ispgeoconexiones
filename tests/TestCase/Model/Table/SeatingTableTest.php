<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SeatingTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SeatingTable Test Case
 */
class SeatingTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SeatingTable
     */
    public $Seating;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.seating',
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Seating') ? [] : ['className' => 'App\Model\Table\SeatingTable'];
        $this->Seating = TableRegistry::get('Seating', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Seating);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
