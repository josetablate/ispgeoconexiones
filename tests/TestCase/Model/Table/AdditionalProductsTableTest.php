<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AdditionalProductsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AdditionalProductsTable Test Case
 */
class AdditionalProductsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AdditionalProductsTable
     */
    public $AdditionalProducts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.additional_products',
        'app.products',
        'app.stock',
        'app.stores',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.controllers',
        'app.actions_system_roles',
        'app.roles_users',
        'app.stock_stores',
        'app.instalations',
        'app.connections',
        'app.customers',
        'app.road_map',
        'app.cities',
        'app.networks',
        'app.nodes',
        'app.speeds',
        'app.free_pool_ips',
        'app.suports',
        'app.transactions',
        'app.packages',
        'app.packages_products'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('AdditionalProducts') ? [] : ['className' => 'App\Model\Table\AdditionalProductsTable'];
        $this->AdditionalProducts = TableRegistry::get('AdditionalProducts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AdditionalProducts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
