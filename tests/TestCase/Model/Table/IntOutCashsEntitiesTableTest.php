<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\IntOutCashsEntitiesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\IntOutCashsEntitiesTable Test Case
 */
class IntOutCashsEntitiesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\IntOutCashsEntitiesTable
     */
    public $IntOutCashsEntities;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.int_out_cashs_entities',
        'app.cash_entities',
        'app.users',
        'app.payments'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('IntOutCashsEntities') ? [] : ['className' => 'App\Model\Table\IntOutCashsEntitiesTable'];
        $this->IntOutCashsEntities = TableRegistry::get('IntOutCashsEntities', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->IntOutCashsEntities);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
