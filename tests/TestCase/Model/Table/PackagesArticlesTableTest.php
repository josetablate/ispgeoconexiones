<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PackagesArticlesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PackagesArticlesTable Test Case
 */
class PackagesArticlesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PackagesArticlesTable
     */
    public $PackagesArticles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.packages_articles',
        'app.articles',
        'app.products',
        'app.snid_stores',
        'app.stores',
        'app.articles_stores',
        'app.instalations',
        'app.connections',
        'app.networks',
        'app.routers',
        'app.profiles',
        'app.free_pool_ips',
        'app.users',
        'app.cities',
        'app.customers',
        'app.services',
        'app.connections_services',
        'app.logs',
        'app.packages',
        'app.packages_sales',
        'app.messages',
        'app.tickets',
        'app.instalations_articles'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PackagesArticles') ? [] : ['className' => 'App\Model\Table\PackagesArticlesTable'];
        $this->PackagesArticles = TableRegistry::get('PackagesArticles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PackagesArticles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
