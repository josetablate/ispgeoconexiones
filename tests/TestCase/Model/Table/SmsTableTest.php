<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SmsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SmsTable Test Case
 */
class SmsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SmsTable
     */
    public $Sms;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sms',
        'app.customers',
        'app.zone',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.clases',
        'app.actions_system_roles',
        'app.roles_users',
        'app.road_map',
        'app.debts',
        'app.invoices',
        'app.concepts',
        'app.comprobantes',
        'app.payments',
        'app.cash_entities',
        'app.int_out_cashs_entities',
        'app.receipts',
        'app.services',
        'app.connections',
        'app.controllers',
        'app.firewall_address_lists',
        'app.web_proxy_access',
        'app.instalations',
        'app.packages',
        'app.packages_sales',
        'app.articles',
        'app.products',
        'app.logs',
        'app.accounts',
        'app.snid_stores',
        'app.stores',
        'app.articles_stores',
        'app.instalations_articles',
        'app.packages_articles',
        'app.tickets',
        'app.tickets_records',
        'app.sms_bloques'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Sms') ? [] : ['className' => SmsTable::class];
        $this->Sms = TableRegistry::get('Sms', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Sms);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
