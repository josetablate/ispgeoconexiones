<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CobrodigitalTransactionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CobrodigitalTransactionsTable Test Case
 */
class CobrodigitalTransactionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CobrodigitalTransactionsTable
     */
    public $CobrodigitalTransactions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cobrodigital_transactions',
        'app.bloques'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CobrodigitalTransactions') ? [] : ['className' => CobrodigitalTransactionsTable::class];
        $this->CobrodigitalTransactions = TableRegistry::getTableLocator()->get('CobrodigitalTransactions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CobrodigitalTransactions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
