<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PackagesSalesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PackagesSalesTable Test Case
 */
class PackagesSalesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PackagesSalesTable
     */
    public $PackagesSales;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.packages_sales',
        'app.packages',
        'app.instalations',
        'app.connections',
        'app.networks',
        'app.routers',
        'app.profiles',
        'app.free_pool_ips',
        'app.users',
        'app.cities',
        'app.customers',
        'app.services',
        'app.connections_services',
        'app.logs',
        'app.products',
        'app.messages',
        'app.tickets',
        'app.articles',
        'app.snid_stores',
        'app.stores',
        'app.articles_stores',
        'app.instalations_articles',
        'app.packages_articles'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PackagesSales') ? [] : ['className' => 'App\Model\Table\PackagesSalesTable'];
        $this->PackagesSales = TableRegistry::get('PackagesSales', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PackagesSales);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
