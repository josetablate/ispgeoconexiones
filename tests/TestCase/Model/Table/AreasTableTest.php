<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AreasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AreasTable Test Case
 */
class AreasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AreasTable
     */
    public $Areas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.areas',
        'app.cities',
        'app.provinces',
        'app.customers',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.clases',
        'app.actions_system_roles',
        'app.roles_users',
        'app.road_map',
        'app.debts',
        'app.invoices',
        'app.concepts',
        'app.connections',
        'app.controllers',
        'app.services',
        'app.accounts',
        'app.firewall_address_lists',
        'app.web_proxy_access',
        'app.instalations',
        'app.packages',
        'app.packages_sales',
        'app.articles',
        'app.products',
        'app.logs',
        'app.snid_stores',
        'app.stores',
        'app.articles_stores',
        'app.instalations_articles',
        'app.packages_articles',
        'app.tickets',
        'app.tickets_records',
        'app.labels',
        'app.connections_labels',
        'app.comprobantes',
        'app.payments',
        'app.cash_entities',
        'app.int_out_cashs_entities',
        'app.receipts',
        'app.customers_labels'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Areas') ? [] : ['className' => AreasTable::class];
        $this->Areas = TableRegistry::get('Areas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Areas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
