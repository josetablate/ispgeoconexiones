<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TaPlansTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TaPlansTable Test Case
 */
class TaPlansTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TaPlansTable
     */
    public $TaPlans;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ta_plans',
        'app.controllers',
        'app.actions_system',
        'app.roles',
        'app.actions_system_roles',
        'app.users',
        'app.roles_users',
        'app.profiles',
        'app.plans',
        'app.connections',
        'app.customers',
        'app.road_map',
        'app.debts',
        'app.connections_services',
        'app.services',
        'app.packages_sales',
        'app.packages',
        'app.instalations',
        'app.articles',
        'app.products',
        'app.logs',
        'app.plans',
        'app.integration_controllers_has_plans',
        'app.integration_controllers',
        'app.pools',
        'app.accounts',
        'app.snid_stores',
        'app.stores',
        'app.articles_stores',
        'app.instalations_articles',
        'app.packages_articles',
        'app.docs',
        'app.payments',
        'app.cash_entities',
        'app.int_out_cashs_entities',
        'app.payment_methods',
        'app.seating',
        'app.seating_detail',
        'app.cities',
        'app.networks'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TaPlans') ? [] : ['className' => 'App\Model\Table\TaPlansTable'];
        $this->TaPlans = TableRegistry::get('TaPlans', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TaPlans);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
