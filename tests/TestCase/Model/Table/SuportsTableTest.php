<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SuportsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SuportsTable Test Case
 */
class SuportsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SuportsTable
     */
    public $Suports;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.suports',
        'app.users',
        'app.connections',
        'app.plans',
        'app.nets',
        'app.nodes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Suports') ? [] : ['className' => 'App\Model\Table\SuportsTable'];
        $this->Suports = TableRegistry::get('Suports', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Suports);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
