<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InstalationsTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InstalationsTypesTable Test Case
 */
class InstalationsTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InstalationsTypesTable
     */
    public $InstalationsTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.instalations_types',
        'app.bailment',
        'app.bailment_snid',
        'app.instalations',
        'app.connections',
        'app.users',
        'app.plans',
        'app.nodes',
        'app.instalations_bailment',
        'app.instalations_types_bailment',
        'app.materials',
        'app.instalations_types_materials',
        'app.products',
        'app.instalations_types_products'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('InstalationsTypes') ? [] : ['className' => 'App\Model\Table\InstalationsTypesTable'];
        $this->InstalationsTypes = TableRegistry::get('InstalationsTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InstalationsTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
