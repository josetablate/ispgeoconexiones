<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\IpsNetworksTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\IpsNetworksTable Test Case
 */
class IpsNetworksTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\IpsNetworksTable
     */
    public $IpsNetworks;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ips_networks',
        'app.networks',
        'app.nodes',
        'app.plans',
        'app.free_pool_ips'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('IpsNetworks') ? [] : ['className' => 'App\Model\Table\IpsNetworksTable'];
        $this->IpsNetworks = TableRegistry::get('IpsNetworks', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->IpsNetworks);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
