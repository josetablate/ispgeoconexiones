<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MessageTemplatesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MessageTemplatesTable Test Case
 */
class MessageTemplatesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MessageTemplatesTable
     */
    public $MessageTemplates;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.message_templates',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.clases',
        'app.actions_system_roles',
        'app.roles_users',
        'app.connections_has_message_templates'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('MessageTemplates') ? [] : ['className' => 'App\Model\Table\MessageTemplatesTable'];
        $this->MessageTemplates = TableRegistry::get('MessageTemplates', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MessageTemplates);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
