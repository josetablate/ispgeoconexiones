<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SmsBloquesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SmsBloquesTable Test Case
 */
class SmsBloquesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SmsBloquesTable
     */
    public $SmsBloques;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sms_bloques',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.clases',
        'app.actions_system_roles',
        'app.roles_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SmsBloques') ? [] : ['className' => SmsBloquesTable::class];
        $this->SmsBloques = TableRegistry::get('SmsBloques', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SmsBloques);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
