<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FirewallAddressListsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FirewallAddressListsTable Test Case
 */
class FirewallAddressListsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FirewallAddressListsTable
     */
    public $FirewallAddressLists;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.firewall_address_lists',
        'app.controllers',
        'app.apis'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FirewallAddressLists') ? [] : ['className' => 'App\Model\Table\FirewallAddressListsTable'];
        $this->FirewallAddressLists = TableRegistry::get('FirewallAddressLists', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FirewallAddressLists);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
