<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\IntegrationControllersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\IntegrationControllersTable Test Case
 */
class IntegrationControllersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\IntegrationControllersTable
     */
    public $IntegrationControllers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.integration_controllers',
        'app.integration_controllers_has_plans'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('IntegrationControllers') ? [] : ['className' => 'App\Model\Table\IntegrationControllersTable'];
        $this->IntegrationControllers = TableRegistry::get('IntegrationControllers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->IntegrationControllers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
