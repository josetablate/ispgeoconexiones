<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ActionsSystemRolesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ActionsSystemRolesTable Test Case
 */
class ActionsSystemRolesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ActionsSystemRolesTable
     */
    public $ActionsSystemRoles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.actions_system_roles',
        'app.roles',
        'app.actions_system',
        'app.controllers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ActionsSystemRoles') ? [] : ['className' => 'App\Model\Table\ActionsSystemRolesTable'];
        $this->ActionsSystemRoles = TableRegistry::get('ActionsSystemRoles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ActionsSystemRoles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
