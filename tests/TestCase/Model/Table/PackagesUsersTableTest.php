<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PackagesUsersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PackagesUsersTable Test Case
 */
class PackagesUsersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PackagesUsersTable
     */
    public $PackagesUsers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.packages_users',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.controllers',
        'app.actions_system_roles',
        'app.roles_users',
        'app.packages',
        'app.instalations',
        'app.connections',
        'app.customers',
        'app.road_map',
        'app.current_accounts',
        'app.products',
        'app.stock',
        'app.stores',
        'app.stock_stores',
        'app.services',
        'app.speeds',
        'app.networks',
        'app.nodes',
        'app.free_pool_ips',
        'app.connections_services',
        'app.bills',
        'app.payments',
        'app.payment_methods',
        'app.cash_entities',
        'app.bills_current_accounts',
        'app.cities',
        'app.suports',
        'app.transactions',
        'app.instalations_products',
        'app.packages_products'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PackagesUsers') ? [] : ['className' => 'App\Model\Table\PackagesUsersTable'];
        $this->PackagesUsers = TableRegistry::get('PackagesUsers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PackagesUsers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
