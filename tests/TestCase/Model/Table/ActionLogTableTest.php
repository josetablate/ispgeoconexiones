<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ActionLogTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ActionLogTable Test Case
 */
class ActionLogTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ActionLogTable
     */
    public $ActionLog;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.action_log',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.clases',
        'app.actions_system_roles',
        'app.roles_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ActionLog') ? [] : ['className' => 'App\Model\Table\ActionLogTable'];
        $this->ActionLog = TableRegistry::get('ActionLog', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ActionLog);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
