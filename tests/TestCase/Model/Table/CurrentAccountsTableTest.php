<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CurrentAccountsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CurrentAccountsTable Test Case
 */
class CurrentAccountsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CurrentAccountsTable
     */
    public $CurrentAccounts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.current_accounts',
        'app.products',
        'app.stock',
        'app.stores',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.controllers',
        'app.actions_system_roles',
        'app.roles_users',
        'app.stock_stores',
        'app.services',
        'app.current_account_services',
        'app.connections',
        'app.customers',
        'app.road_map',
        'app.cities',
        'app.networks',
        'app.nodes',
        'app.speeds',
        'app.free_pool_ips',
        'app.instalations',
        'app.packages',
        'app.packages_products',
        'app.instalations_products',
        'app.suports',
        'app.transactions',
        'app.connections_services',
        'app.packages_services',
        'app.bills',
        'app.bills_current_accounts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CurrentAccounts') ? [] : ['className' => 'App\Model\Table\CurrentAccountsTable'];
        $this->CurrentAccounts = TableRegistry::get('CurrentAccounts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CurrentAccounts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
