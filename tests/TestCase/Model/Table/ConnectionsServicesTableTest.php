<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ConnectionsServicesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ConnectionsServicesTable Test Case
 */
class ConnectionsServicesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ConnectionsServicesTable
     */
    public $ConnectionsServices;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.connections_services',
        'app.services',
        'app.connections',
        'app.networks',
        'app.users',
        'app.cities',
        'app.customers',
        'app.instalations',
        'app.logs',
        'app.messages',
        'app.tickets'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ConnectionsServices') ? [] : ['className' => 'App\Model\Table\ConnectionsServicesTable'];
        $this->ConnectionsServices = TableRegistry::get('ConnectionsServices', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ConnectionsServices);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
