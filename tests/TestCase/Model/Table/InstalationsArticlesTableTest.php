<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InstalationsArticlesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InstalationsArticlesTable Test Case
 */
class InstalationsArticlesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InstalationsArticlesTable
     */
    public $InstalationsArticles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.instalations_articles',
        'app.articles',
        'app.products',
        'app.snid_stores',
        'app.stores',
        'app.articles_stores',
        'app.instalations',
        'app.connections',
        'app.networks',
        'app.users',
        'app.cities',
        'app.customers',
        'app.services',
        'app.connections_services',
        'app.logs',
        'app.messages',
        'app.tickets',
        'app.packages',
        'app.packages_sales',
        'app.packages_articles'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('InstalationsArticles') ? [] : ['className' => 'App\Model\Table\InstalationsArticlesTable'];
        $this->InstalationsArticles = TableRegistry::get('InstalationsArticles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InstalationsArticles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
