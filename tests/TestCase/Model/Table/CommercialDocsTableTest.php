<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CommercialDocsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CommercialDocsTable Test Case
 */
class CommercialDocsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CommercialDocsTable
     */
    public $CommercialDocs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.commercial_docs',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.controllers',
        'app.actions_system_roles',
        'app.roles_users',
        'app.commercial_docs_concepts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CommercialDocs') ? [] : ['className' => 'App\Model\Table\CommercialDocsTable'];
        $this->CommercialDocs = TableRegistry::get('CommercialDocs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CommercialDocs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
