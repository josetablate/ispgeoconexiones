<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SpeedsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SpeedsTable Test Case
 */
class SpeedsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SpeedsTable
     */
    public $Speeds;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.speeds',
        'app.networks',
        'app.nodes',
        'app.free_pool_ips'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Speeds') ? [] : ['className' => 'App\Model\Table\SpeedsTable'];
        $this->Speeds = TableRegistry::get('Speeds', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Speeds);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
