<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InstallerBailmentTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InstallerBailmentTable Test Case
 */
class InstallerBailmentTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InstallerBailmentTable
     */
    public $InstallerBailment;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.installer_bailment',
        'app.users',
        'app.bailment_snid',
        'app.bailment',
        'app.instalations',
        'app.instalations_types',
        'app.instalations_types_bailment',
        'app.materials',
        'app.instalations_types_materials',
        'app.products',
        'app.instalations_types_products',
        'app.connections',
        'app.plans',
        'app.nodes',
        'app.instalations_bailment'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('InstallerBailment') ? [] : ['className' => 'App\Model\Table\InstallerBailmentTable'];
        $this->InstallerBailment = TableRegistry::get('InstallerBailment', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InstallerBailment);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
