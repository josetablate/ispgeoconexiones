<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TArpTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TArpTable Test Case
 */
class TArpTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TArpTable
     */
    public $TArp;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.t_arp',
        'app.connections',
        'app.controllers',
        'app.pools',
        'app.plans'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TArp') ? [] : ['className' => TArpTable::class];
        $this->TArp = TableRegistry::getTableLocator()->get('TArp', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TArp);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
