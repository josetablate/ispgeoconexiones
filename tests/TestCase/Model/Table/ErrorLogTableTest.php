<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ErrorLogTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ErrorLogTable Test Case
 */
class ErrorLogTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ErrorLogTable
     */
    public $ErrorLog;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.error_log',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.clases',
        'app.actions_system_roles',
        'app.roles_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ErrorLog') ? [] : ['className' => ErrorLogTable::class];
        $this->ErrorLog = TableRegistry::get('ErrorLog', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ErrorLog);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
