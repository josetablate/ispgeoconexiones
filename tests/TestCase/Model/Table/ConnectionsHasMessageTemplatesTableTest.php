<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ConnectionsHasMessageTemplatesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ConnectionsHasMessageTemplatesTable Test Case
 */
class ConnectionsHasMessageTemplatesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ConnectionsHasMessageTemplatesTable
     */
    public $ConnectionsHasMessageTemplates;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.connections_has_message_templates',
        'app.connections',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.clases',
        'app.actions_system_roles',
        'app.roles_users',
        'app.zone',
        'app.customers',
        'app.road_map',
        'app.debts',
        'app.invoices',
        'app.concepts',
        'app.comprobantes',
        'app.payments',
        'app.cash_entities',
        'app.int_out_cashs_entities',
        'app.payment_methods',
        'app.receipts',
        'app.services',
        'app.accounts',
        'app.instalations',
        'app.packages',
        'app.logs',
        'app.products',
        'app.articles',
        'app.snid_stores',
        'app.stores',
        'app.articles_stores',
        'app.instalations_articles',
        'app.packages_articles',
        'app.packages_sales',
        'app.controllers',
        'app.firewall_address_lists',
        'app.web_proxy_access',
        'app.tickets',
        'app.tickets_records',
        'app.message_templates'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ConnectionsHasMessageTemplates') ? [] : ['className' => 'App\Model\Table\ConnectionsHasMessageTemplatesTable'];
        $this->ConnectionsHasMessageTemplates = TableRegistry::get('ConnectionsHasMessageTemplates', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ConnectionsHasMessageTemplates);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
