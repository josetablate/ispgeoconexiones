<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PresalesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PresalesTable Test Case
 */
class PresalesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PresalesTable
     */
    public $Presales;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.presales',
        'app.debt_products',
        'app.debt_packages',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.clases',
        'app.actions_system_roles',
        'app.roles_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Presales') ? [] : ['className' => PresalesTable::class];
        $this->Presales = TableRegistry::get('Presales', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Presales);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
