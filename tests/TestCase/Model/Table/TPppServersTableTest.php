<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TPppServersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TPppServersTable Test Case
 */
class TPppServersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TPppServersTable
     */
    public $TPppServers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.t_ppp_servers',
        'app.t_integration_controllers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TPppServers') ? [] : ['className' => 'App\Model\Table\TPppServersTable'];
        $this->TPppServers = TableRegistry::get('TPppServers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TPppServers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
