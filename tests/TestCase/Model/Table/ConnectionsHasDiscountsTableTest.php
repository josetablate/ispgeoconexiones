<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ConnectionsHasDiscountsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ConnectionsHasDiscountsTable Test Case
 */
class ConnectionsHasDiscountsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ConnectionsHasDiscountsTable
     */
    public $ConnectionsHasDiscounts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.connections_has_discounts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ConnectionsHasDiscounts') ? [] : ['className' => ConnectionsHasDiscountsTable::class];
        $this->ConnectionsHasDiscounts = TableRegistry::get('ConnectionsHasDiscounts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ConnectionsHasDiscounts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
