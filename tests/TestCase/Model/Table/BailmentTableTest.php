<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BailmentTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BailmentTable Test Case
 */
class BailmentTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BailmentTable
     */
    public $Bailment;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.bailment',
        'app.bailment_snid',
        'app.instalations',
        'app.instalations_bailment',
        'app.instalations_types',
        'app.instalations_types_bailment'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Bailment') ? [] : ['className' => 'App\Model\Table\BailmentTable'];
        $this->Bailment = TableRegistry::get('Bailment', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Bailment);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
