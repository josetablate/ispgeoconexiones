<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CashEntitiesTransactionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CashEntitiesTransactionsTable Test Case
 */
class CashEntitiesTransactionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CashEntitiesTransactionsTable
     */
    public $CashEntitiesTransactions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cash_entities_transactions',
        'app.user_origins',
        'app.user_destinations'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CashEntitiesTransactions') ? [] : ['className' => CashEntitiesTransactionsTable::class];
        $this->CashEntitiesTransactions = TableRegistry::get('CashEntitiesTransactions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CashEntitiesTransactions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
