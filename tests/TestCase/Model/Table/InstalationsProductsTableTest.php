<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InstalationsProductsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InstalationsProductsTable Test Case
 */
class InstalationsProductsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InstalationsProductsTable
     */
    public $InstalationsProducts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.instalations_products',
        'app.products',
        'app.stock',
        'app.stores',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.controllers',
        'app.actions_system_roles',
        'app.roles_users',
        'app.stock_stores',
        'app.instalations',
        'app.connections',
        'app.customers',
        'app.road_map',
        'app.cities',
        'app.networks',
        'app.nodes',
        'app.speeds',
        'app.free_pool_ips',
        'app.suports',
        'app.transactions',
        'app.packages',
        'app.packages_products',
        'app.additional_products'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('InstalationsProducts') ? [] : ['className' => 'App\Model\Table\InstalationsProductsTable'];
        $this->InstalationsProducts = TableRegistry::get('InstalationsProducts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InstalationsProducts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
