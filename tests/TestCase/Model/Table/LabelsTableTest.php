<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LabelsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LabelsTable Test Case
 */
class LabelsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LabelsTable
     */
    public $Labels;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.labels',
        'app.connections',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.clases',
        'app.actions_system_roles',
        'app.roles_users',
        'app.zone',
        'app.customers',
        'app.road_map',
        'app.debts',
        'app.invoices',
        'app.concepts',
        'app.comprobantes',
        'app.payments',
        'app.cash_entities',
        'app.int_out_cashs_entities',
        'app.receipts',
        'app.services',
        'app.accounts',
        'app.instalations',
        'app.packages',
        'app.packages_sales',
        'app.articles',
        'app.products',
        'app.logs',
        'app.snid_stores',
        'app.stores',
        'app.articles_stores',
        'app.instalations_articles',
        'app.packages_articles',
        'app.controllers',
        'app.firewall_address_lists',
        'app.web_proxy_access',
        'app.tickets',
        'app.tickets_records',
        'app.connections_labels',
        'app.customers_labels'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Labels') ? [] : ['className' => LabelsTable::class];
        $this->Labels = TableRegistry::get('Labels', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Labels);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
