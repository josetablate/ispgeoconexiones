<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BailmentSnidTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BailmentSnidTable Test Case
 */
class BailmentSnidTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BailmentSnidTable
     */
    public $BailmentSnid;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.bailment_snid',
        'app.bailment',
        'app.instalations',
        'app.instalations_bailment',
        'app.instalations_types',
        'app.instalations_types_bailment'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('BailmentSnid') ? [] : ['className' => 'App\Model\Table\BailmentSnidTable'];
        $this->BailmentSnid = TableRegistry::get('BailmentSnid', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BailmentSnid);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
