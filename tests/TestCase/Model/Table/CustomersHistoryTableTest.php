<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CustomersHistoryTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CustomersHistoryTable Test Case
 */
class CustomersHistoryTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CustomersHistoryTable
     */
    public $CustomersHistory;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.customers_history',
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CustomersHistory') ? [] : ['className' => 'App\Model\Table\CustomersHistoryTable'];
        $this->CustomersHistory = TableRegistry::get('CustomersHistory', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CustomersHistory);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
