<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MastercardAutoDebitRecordsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MastercardAutoDebitRecordsTable Test Case
 */
class MastercardAutoDebitRecordsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MastercardAutoDebitRecordsTable
     */
    public $MastercardAutoDebitRecords;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.mastercard_auto_debit_records',
        'app.invoices',
        'app.businesses',
        'app.payment_getways'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('MastercardAutoDebitRecords') ? [] : ['className' => MastercardAutoDebitRecordsTable::class];
        $this->MastercardAutoDebitRecords = TableRegistry::getTableLocator()->get('MastercardAutoDebitRecords', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MastercardAutoDebitRecords);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
