<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TGatewayTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TGatewayTable Test Case
 */
class TGatewayTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TGatewayTable
     */
    public $TGateway;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.t_gateway',
        'app.controllers',
        'app.pools'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TGateway') ? [] : ['className' => TGatewayTable::class];
        $this->TGateway = TableRegistry::getTableLocator()->get('TGateway', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TGateway);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
