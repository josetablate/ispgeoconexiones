<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TemplateResourcesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TemplateResourcesTable Test Case
 */
class TemplateResourcesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TemplateResourcesTable
     */
    public $TemplateResources;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.template_resources'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TemplateResources') ? [] : ['className' => 'App\Model\Table\TemplateResourcesTable'];
        $this->TemplateResources = TableRegistry::get('TemplateResources', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TemplateResources);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
