<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ConnectionsDebtsMonthTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ConnectionsDebtsMonthTable Test Case
 */
class ConnectionsDebtsMonthTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ConnectionsDebtsMonthTable
     */
    public $ConnectionsDebtsMonth;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.connections_debts_month',
        'app.connections'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ConnectionsDebtsMonth') ? [] : ['className' => ConnectionsDebtsMonthTable::class];
        $this->ConnectionsDebtsMonth = TableRegistry::getTableLocator()->get('ConnectionsDebtsMonth', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ConnectionsDebtsMonth);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
