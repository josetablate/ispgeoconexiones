<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\IntegrationControllersHasPlansTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\IntegrationControllersHasPlansTable Test Case
 */
class IntegrationControllersHasPlansTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\IntegrationControllersHasPlansTable
     */
    public $IntegrationControllersHasPlans;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.integration_controllers_has_plans',
        'app.integration_controllers',
        'app.plans',
        'app.connections',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.controllers',
        'app.actions_system_roles',
        'app.roles_users',
        'app.customers',
        'app.road_map',
        'app.debts',
        'app.connections_services',
        'app.services',
        'app.packages_sales',
        'app.packages',
        'app.instalations',
        'app.articles',
        'app.products',
        'app.logs',
        'app.plans',
        'app.accounts',
        'app.snid_stores',
        'app.stores',
        'app.articles_stores',
        'app.instalations_articles',
        'app.packages_articles',
        'app.docs',
        'app.payments',
        'app.cash_entities',
        'app.int_out_cashs_entities',
        'app.payment_methods',
        'app.seating',
        'app.seating_detail',
        'app.cities',
        'app.networks',
        'app.profiles',
        'app.pools',
        'app.free_pool_ips'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('IntegrationControllersHasPlans') ? [] : ['className' => 'App\Model\Table\IntegrationControllersHasPlansTable'];
        $this->IntegrationControllersHasPlans = TableRegistry::get('IntegrationControllersHasPlans', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->IntegrationControllersHasPlans);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
