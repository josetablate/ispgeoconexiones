<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InstalationsTypesBailmentTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InstalationsTypesBailmentTable Test Case
 */
class InstalationsTypesBailmentTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InstalationsTypesBailmentTable
     */
    public $InstalationsTypesBailment;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.instalations_types_bailment',
        'app.instalations_types',
        'app.bailment',
        'app.bailment_snid',
        'app.instalations',
        'app.connections',
        'app.users',
        'app.plans',
        'app.nodes',
        'app.instalations_bailment',
        'app.materials',
        'app.instalations_types_materials',
        'app.products',
        'app.instalations_types_products'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('InstalationsTypesBailment') ? [] : ['className' => 'App\Model\Table\InstalationsTypesBailmentTable'];
        $this->InstalationsTypesBailment = TableRegistry::get('InstalationsTypesBailment', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InstalationsTypesBailment);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
