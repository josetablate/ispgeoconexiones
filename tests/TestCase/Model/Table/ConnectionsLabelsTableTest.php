<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ConnectionsLabelsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ConnectionsLabelsTable Test Case
 */
class ConnectionsLabelsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ConnectionsLabelsTable
     */
    public $ConnectionsLabels;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.connections_labels',
        'app.connections',
        'app.labels'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ConnectionsLabels') ? [] : ['className' => ConnectionsLabelsTable::class];
        $this->ConnectionsLabels = TableRegistry::getTableLocator()->get('ConnectionsLabels', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ConnectionsLabels);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
