<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CustomersMessageTemplatesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CustomersMessageTemplatesTable Test Case
 */
class CustomersMessageTemplatesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CustomersMessageTemplatesTable
     */
    public $CustomersMessageTemplates;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.customers_message_templates'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CustomersMessageTemplates') ? [] : ['className' => CustomersMessageTemplatesTable::class];
        $this->CustomersMessageTemplates = TableRegistry::get('CustomersMessageTemplates', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CustomersMessageTemplates);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
