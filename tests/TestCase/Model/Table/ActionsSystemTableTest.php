<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ActionsSystemTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ActionsSystemTable Test Case
 */
class ActionsSystemTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ActionsSystemTable
     */
    public $ActionsSystem;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.actions_system',
        'app.controllers',
        'app.roles',
        'app.actions_system_roles'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ActionsSystem') ? [] : ['className' => 'App\Model\Table\ActionsSystemTable'];
        $this->ActionsSystem = TableRegistry::get('ActionsSystem', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ActionsSystem);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
