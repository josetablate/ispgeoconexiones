<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VisaAutoDebitTransactionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VisaAutoDebitTransactionsTable Test Case
 */
class VisaAutoDebitTransactionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VisaAutoDebitTransactionsTable
     */
    public $VisaAutoDebitTransactions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.visa_auto_debit_transactions',
        'app.bloques',
        'app.payment_getways',
        'app.receipts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('VisaAutoDebitTransactions') ? [] : ['className' => VisaAutoDebitTransactionsTable::class];
        $this->VisaAutoDebitTransactions = TableRegistry::getTableLocator()->get('VisaAutoDebitTransactions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->VisaAutoDebitTransactions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
