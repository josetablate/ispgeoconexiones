<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SeatingDetailTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SeatingDetailTable Test Case
 */
class SeatingDetailTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SeatingDetailTable
     */
    public $SeatingDetail;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.seating_detail'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SeatingDetail') ? [] : ['className' => 'App\Model\Table\SeatingDetailTable'];
        $this->SeatingDetail = TableRegistry::get('SeatingDetail', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SeatingDetail);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
