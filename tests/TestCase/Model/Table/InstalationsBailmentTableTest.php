<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InstalationsBailmentTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InstalationsBailmentTable Test Case
 */
class InstalationsBailmentTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InstalationsBailmentTable
     */
    public $InstalationsBailment;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.instalations_bailment',
        'app.bailment_snid',
        'app.bailment',
        'app.instalations',
        'app.instalations_types',
        'app.connections',
        'app.users',
        'app.plans',
        'app.nodes',
        'app.instalations_types_bailment'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('InstalationsBailment') ? [] : ['className' => 'App\Model\Table\InstalationsBailmentTable'];
        $this->InstalationsBailment = TableRegistry::get('InstalationsBailment', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InstalationsBailment);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
