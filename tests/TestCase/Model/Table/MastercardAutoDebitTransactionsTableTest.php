<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MastercardAutoDebitTransactionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MastercardAutoDebitTransactionsTable Test Case
 */
class MastercardAutoDebitTransactionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MastercardAutoDebitTransactionsTable
     */
    public $MastercardAutoDebitTransactions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.mastercard_auto_debit_transactions',
        'app.bloques',
        'app.payment_getways',
        'app.receipts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('MastercardAutoDebitTransactions') ? [] : ['className' => MastercardAutoDebitTransactionsTable::class];
        $this->MastercardAutoDebitTransactions = TableRegistry::getTableLocator()->get('MastercardAutoDebitTransactions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MastercardAutoDebitTransactions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
