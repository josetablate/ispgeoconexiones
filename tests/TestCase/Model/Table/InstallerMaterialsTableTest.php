<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InstallerMaterialsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InstallerMaterialsTable Test Case
 */
class InstallerMaterialsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InstallerMaterialsTable
     */
    public $InstallerMaterials;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.installer_materials',
        'app.users',
        'app.materials'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('InstallerMaterials') ? [] : ['className' => 'App\Model\Table\InstallerMaterialsTable'];
        $this->InstallerMaterials = TableRegistry::get('InstallerMaterials', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InstallerMaterials);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
