<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WebProxyAccessTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WebProxyAccessTable Test Case
 */
class WebProxyAccessTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\WebProxyAccessTable
     */
    public $WebProxyAccess;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.web_proxy_access',
        'app.controllers',
        'app.apis'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('WebProxyAccess') ? [] : ['className' => 'App\Model\Table\WebProxyAccessTable'];
        $this->WebProxyAccess = TableRegistry::get('WebProxyAccess', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WebProxyAccess);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
