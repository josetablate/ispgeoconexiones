<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ConnectionsDebtsMonthRegisterTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ConnectionsDebtsMonthRegisterTable Test Case
 */
class ConnectionsDebtsMonthRegisterTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ConnectionsDebtsMonthRegisterTable
     */
    public $ConnectionsDebtsMonthRegister;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.connections_debts_month_register'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ConnectionsDebtsMonthRegister') ? [] : ['className' => ConnectionsDebtsMonthRegisterTable::class];
        $this->ConnectionsDebtsMonthRegister = TableRegistry::getTableLocator()->get('ConnectionsDebtsMonthRegister', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ConnectionsDebtsMonthRegister);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
