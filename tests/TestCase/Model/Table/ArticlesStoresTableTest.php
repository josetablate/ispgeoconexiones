<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ArticlesStoresTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ArticlesStoresTable Test Case
 */
class ArticlesStoresTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ArticlesStoresTable
     */
    public $ArticlesStores;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.articles_stores',
        'app.stores',
        'app.articles',
        'app.products',
        'app.snid_stores',
        'app.instalations',
        'app.instalations_articles',
        'app.packages',
        'app.packages_articles'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ArticlesStores') ? [] : ['className' => 'App\Model\Table\ArticlesStoresTable'];
        $this->ArticlesStores = TableRegistry::get('ArticlesStores', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ArticlesStores);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
