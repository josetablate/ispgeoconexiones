<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TrafficAccountingTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TrafficAccountingTable Test Case
 */
class TrafficAccountingTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TrafficAccountingTable
     */
    public $TrafficAccounting;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.traffic_accounting'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TrafficAccounting') ? [] : ['className' => 'App\Model\Table\TrafficAccountingTable'];
        $this->TrafficAccounting = TableRegistry::get('TrafficAccounting', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TrafficAccounting);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
