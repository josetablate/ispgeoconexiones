<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PaymentsConceptsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PaymentsConceptsTable Test Case
 */
class PaymentsConceptsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PaymentsConceptsTable
     */
    public $PaymentsConcepts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.payments_concepts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PaymentsConcepts') ? [] : ['className' => PaymentsConceptsTable::class];
        $this->PaymentsConcepts = TableRegistry::get('PaymentsConcepts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PaymentsConcepts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
