<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VisaAutoDebitRecordsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VisaAutoDebitRecordsTable Test Case
 */
class VisaAutoDebitRecordsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VisaAutoDebitRecordsTable
     */
    public $VisaAutoDebitRecords;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.visa_auto_debit_records',
        'app.invoices',
        'app.businesses',
        'app.payment_getways'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('VisaAutoDebitRecords') ? [] : ['className' => VisaAutoDebitRecordsTable::class];
        $this->VisaAutoDebitRecords = TableRegistry::getTableLocator()->get('VisaAutoDebitRecords', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->VisaAutoDebitRecords);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
