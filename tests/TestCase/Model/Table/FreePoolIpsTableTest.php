<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FreePoolIpsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FreePoolIpsTable Test Case
 */
class FreePoolIpsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FreePoolIpsTable
     */
    public $FreePoolIps;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.free_pool_ips',
        'app.networks'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FreePoolIps') ? [] : ['className' => 'App\Model\Table\FreePoolIpsTable'];
        $this->FreePoolIps = TableRegistry::get('FreePoolIps', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FreePoolIps);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
