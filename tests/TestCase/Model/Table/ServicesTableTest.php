<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ServicesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ServicesTable Test Case
 */
class ServicesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ServicesTable
     */
    public $Services;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.services',
        'app.connections',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.clases',
        'app.actions_system_roles',
        'app.roles_users',
        'app.customers',
        'app.road_map',
        'app.debts',
        'app.connections_services',
        'app.packages_sales',
        'app.packages',
        'app.instalations',
        'app.articles',
        'app.products',
        'app.logs',
        'app.plans',
        'app.accounts',
        'app.snid_stores',
        'app.stores',
        'app.articles_stores',
        'app.instalations_articles',
        'app.packages_articles',
        'app.docs',
        'app.payments',
        'app.cash_entities',
        'app.int_out_cashs_entities',
        'app.payment_methods',
        'app.seating',
        'app.seating_detail',
        'app.cities',
        'app.networks'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Services') ? [] : ['className' => 'App\Model\Table\ServicesTable'];
        $this->Services = TableRegistry::get('Services', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Services);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
