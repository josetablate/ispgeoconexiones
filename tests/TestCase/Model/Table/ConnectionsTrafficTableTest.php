<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ConnectionsTrafficTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ConnectionsTrafficTable Test Case
 */
class ConnectionsTrafficTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ConnectionsTrafficTable
     */
    public $ConnectionsTraffic;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.connections_traffic'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ConnectionsTraffic') ? [] : ['className' => 'App\Model\Table\ConnectionsTrafficTable'];
        $this->ConnectionsTraffic = TableRegistry::get('ConnectionsTraffic', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ConnectionsTraffic);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
