<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DebitNotesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DebitNotesTable Test Case
 */
class DebitNotesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DebitNotesTable
     */
    public $DebitNotes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.debit_notes',
        'app.users',
        'app.roles',
        'app.actions_system',
        'app.clases',
        'app.actions_system_roles',
        'app.roles_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DebitNotes') ? [] : ['className' => 'App\Model\Table\DebitNotesTable'];
        $this->DebitNotes = TableRegistry::get('DebitNotes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DebitNotes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
