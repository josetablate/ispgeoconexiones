<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PayuAccountsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PayuAccountsTable Test Case
 */
class PayuAccountsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PayuAccountsTable
     */
    public $PayuAccounts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.payu_accounts',
        'app.payment_getways'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PayuAccounts') ? [] : ['className' => PayuAccountsTable::class];
        $this->PayuAccounts = TableRegistry::getTableLocator()->get('PayuAccounts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PayuAccounts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
