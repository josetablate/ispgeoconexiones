<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ParamentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ParamentsTable Test Case
 */
class ParamentsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ParamentsTable
     */
    public $Paraments;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.paraments'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Paraments') ? [] : ['className' => 'App\Model\Table\ParamentsTable'];
        $this->Paraments = TableRegistry::get('Paraments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Paraments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
