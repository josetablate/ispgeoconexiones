<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CuentadigitalTransactionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CuentadigitalTransactionsTable Test Case
 */
class CuentadigitalTransactionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CuentadigitalTransactionsTable
     */
    public $CuentadigitalTransactions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cuentadigital_transactions',
        'app.bloques',
        'app.receipts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CuentadigitalTransactions') ? [] : ['className' => CuentadigitalTransactionsTable::class];
        $this->CuentadigitalTransactions = TableRegistry::getTableLocator()->get('CuentadigitalTransactions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CuentadigitalTransactions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
