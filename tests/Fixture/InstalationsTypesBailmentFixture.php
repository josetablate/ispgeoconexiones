<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * InstalationsTypesBailmentFixture
 *
 */
class InstalationsTypesBailmentFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'instalations_types_bailment';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'amount' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'instalacionType_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'bailment_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_instalacionsTypes_bailment_instalacionsTypes1_idx' => ['type' => 'index', 'columns' => ['instalacionType_id'], 'length' => []],
            'fk_instalacionsTypes_bailment_bailment1_idx' => ['type' => 'index', 'columns' => ['bailment_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id', 'instalacionType_id', 'bailment_id'], 'length' => []],
            'fk_instalacionsTypes_bailment_instalacionsTypes1' => ['type' => 'foreign', 'columns' => ['instalacionType_id'], 'references' => ['instalations_types', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_instalacionsTypes_bailment_bailment1' => ['type' => 'foreign', 'columns' => ['bailment_id'], 'references' => ['bailment', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'amount' => 1,
            'instalacionType_id' => 1,
            'bailment_id' => 1,
            'created' => '2016-07-15 18:41:33',
            'modified' => '2016-07-15 18:41:33'
        ],
    ];
}
