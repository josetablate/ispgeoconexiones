<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ParamentsFixture
 *
 */
class ParamentsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 1, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'integration_enable' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '1', 'comment' => '', 'precision' => null],
        'lat_center' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => ''],
        'lng_center' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => ''],
        'cutomer_type_tax_default' => ['type' => 'string', 'length' => 2, 'null' => false, 'default' => 'cf', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'customer_type_bill_default' => ['type' => 'string', 'length' => 2, 'null' => false, 'default' => 'x', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'customer_validate_dni_enable' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '1', 'comment' => '', 'precision' => null],
        'ppp_user_pass_auto' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
        'conecction_locker_date' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => '15', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'business_type_tax' => ['type' => 'string', 'length' => 1, 'null' => false, 'default' => 'm', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'business_cp' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'business_name' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => 'NOMBRE EMPRESA', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'business_address' => ['type' => 'string', 'length' => 100, 'null' => false, 'default' => 'Calle 123', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'business_phone' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => '000000000000', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'business_type' => ['type' => 'string', 'length' => 100, 'null' => false, 'default' => 'Responsable Monotributista', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'business_cuit' => ['type' => 'string', 'length' => 13, 'null' => false, 'default' => '00-00000000-0', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'business_ing_bruto' => ['type' => 'string', 'length' => 13, 'null' => false, 'default' => '00-00000000-0', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'business_ini_act' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => '2016-01-01 00:00:00', 'comment' => '', 'precision' => null],
        'business_state' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => 'PROVINCIA', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'bill_due_type' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => 'mv', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'bill_due_date' => ['type' => 'integer', 'length' => 2, 'unsigned' => false, 'null' => false, 'default' => '10', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'point_sale' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => true, 'default' => '1', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'bill_init_number_fx' => ['type' => 'integer', 'length' => 8, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'bill_init_number_fa' => ['type' => 'integer', 'length' => 8, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'bill_init_number_fb' => ['type' => 'integer', 'length' => 8, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'bill_init_number_fc' => ['type' => 'integer', 'length' => 8, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'bill_init_number_r' => ['type' => 'integer', 'length' => 8, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'calculate_proportional' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
        'fix_saldo_change_plan' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
        'interest_moroso_day' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => false, 'default' => '1', 'comment' => ''],
        'interest_due_2' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => '1', 'comment' => ''],
        'interest_due_3' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => '1', 'comment' => ''],
        'interest_due_4' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => '1', 'comment' => ''],
        'interest_due_5' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => '1', 'comment' => ''],
        'interest_due_6' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => '1', 'comment' => ''],
        'send_forewarning_date' => ['type' => 'integer', 'length' => 2, 'unsigned' => false, 'null' => false, 'default' => '8', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'avisos_auto' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
        'debt_cut' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => false, 'default' => '1', 'comment' => ''],
        'ip_server' => ['type' => 'string', 'length' => 15, 'null' => false, 'default' => '10.0.0.1', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'message_corte_title' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'message_corte_des' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null],
        'message_venc_title' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'message_venc_des' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null],
        'message_info_title' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'message_info_des' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null],
        'cashs_account_parent' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'customer_account_parent' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'service_internet_account_parent' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'bonif_account_code' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'recarg_account_code' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'packages_account_parent' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'products_account_parent' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'bonif_services_account_code' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'integration_enable' => 1,
            'lat_center' => 1,
            'lng_center' => 1,
            'cutomer_type_tax_default' => '',
            'customer_type_bill_default' => '',
            'customer_validate_dni_enable' => 1,
            'ppp_user_pass_auto' => 1,
            'conecction_locker_date' => 1,
            'business_type_tax' => 'Lorem ipsum dolor sit ame',
            'business_cp' => 1,
            'business_name' => 'Lorem ipsum dolor sit amet',
            'business_address' => 'Lorem ipsum dolor sit amet',
            'business_phone' => 'Lorem ipsum dolor sit amet',
            'business_type' => 'Lorem ipsum dolor sit amet',
            'business_cuit' => 'Lorem ipsum',
            'business_ing_bruto' => 'Lorem ipsum',
            'business_ini_act' => '2017-03-11 20:11:53',
            'business_state' => 'Lorem ipsum dolor sit amet',
            'bill_due_type' => 'Lorem ipsum dolor sit amet',
            'bill_due_date' => 1,
            'point_sale' => 1,
            'bill_init_number_fx' => 1,
            'bill_init_number_fa' => 1,
            'bill_init_number_fb' => 1,
            'bill_init_number_fc' => 1,
            'bill_init_number_r' => 1,
            'calculate_proportional' => 1,
            'fix_saldo_change_plan' => 1,
            'interest_moroso_day' => 1,
            'interest_due_2' => 1,
            'interest_due_3' => 1,
            'interest_due_4' => 1,
            'interest_due_5' => 1,
            'interest_due_6' => 1,
            'send_forewarning_date' => 1,
            'avisos_auto' => 1,
            'debt_cut' => 1,
            'ip_server' => 'Lorem ipsum d',
            'message_corte_title' => 'Lorem ipsum dolor sit amet',
            'message_corte_des' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
            'message_venc_title' => 'Lorem ipsum dolor sit amet',
            'message_venc_des' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
            'message_info_title' => 'Lorem ipsum dolor sit amet',
            'message_info_des' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
            'cashs_account_parent' => 'Lorem ipsum dolor ',
            'customer_account_parent' => 'Lorem ipsum dolor ',
            'service_internet_account_parent' => 'Lorem ipsum dolor ',
            'bonif_account_code' => 'Lorem ipsum dolor ',
            'recarg_account_code' => 'Lorem ipsum dolor ',
            'packages_account_parent' => 'Lorem ipsum dolor ',
            'products_account_parent' => 'Lorem ipsum dolor ',
            'bonif_services_account_code' => 'Lorem ipsum dolor '
        ],
    ];
}
