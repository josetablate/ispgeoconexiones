<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * NodesPlansFixture
 *
 */
class NodesPlansFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'plans_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'nodes_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'enabled' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_plans_nodes1_idx' => ['type' => 'index', 'columns' => ['nodes_id'], 'length' => []],
            'fk_nodes_plans_Plans1_idx' => ['type' => 'index', 'columns' => ['plans_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_plans_nodes1' => ['type' => 'foreign', 'columns' => ['nodes_id'], 'references' => ['nodes', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_nodes_plans_Plans1' => ['type' => 'foreign', 'columns' => ['plans_id'], 'references' => ['plans', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'plans_id' => 1,
            'nodes_id' => 1,
            'enabled' => 1,
            'created' => '2016-07-20 12:31:23',
            'modified' => '2016-07-20 12:31:23'
        ],
    ];
}
