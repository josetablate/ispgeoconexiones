<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * BailmentSnidFixture
 *
 */
class BailmentSnidFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'bailment_snid';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'bailment_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'snid' => ['type' => 'string', 'length' => 100, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_bailment_snid_bailment1_idx' => ['type' => 'index', 'columns' => ['bailment_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id', 'bailment_id'], 'length' => []],
            'snid_UNIQUE' => ['type' => 'unique', 'columns' => ['snid'], 'length' => []],
            'fk_bailment_snid_bailment1' => ['type' => 'foreign', 'columns' => ['bailment_id'], 'references' => ['bailment', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'bailment_id' => 1,
            'snid' => 'Lorem ipsum dolor sit amet',
            'created' => '2016-07-15 18:41:32',
            'modified' => '2016-07-15 18:41:32'
        ],
    ];
}
