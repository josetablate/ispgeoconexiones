<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * InstalationsBailmentFixture
 *
 */
class InstalationsBailmentFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'instalations_bailment';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'amount' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'bailment_snid_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'instalation_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_instalations_bailment_bailment_snid1_idx' => ['type' => 'index', 'columns' => ['bailment_snid_id'], 'length' => []],
            'fk_instalations_bailment_instalations1_idx' => ['type' => 'index', 'columns' => ['instalation_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id', 'bailment_snid_id', 'instalation_id'], 'length' => []],
            'fk_instalations_bailment_bailment_snid1' => ['type' => 'foreign', 'columns' => ['bailment_snid_id'], 'references' => ['bailment_snid', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_instalations_bailment_instalations1' => ['type' => 'foreign', 'columns' => ['instalation_id'], 'references' => ['instalations', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'amount' => 1,
            'bailment_snid_id' => 1,
            'instalation_id' => 1,
            'created' => '2016-07-15 18:41:32',
            'modified' => '2016-07-15 18:41:32'
        ],
    ];
}
