<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CommercialDocsConceptsFixture
 *
 */
class CommercialDocsConceptsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'movement_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'commercial_doc_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_commercial_docs_concepts_movements1_idx' => ['type' => 'index', 'columns' => ['movement_id'], 'length' => []],
            'fk_commercial_docs_concepts_commercial_docs1_idx' => ['type' => 'index', 'columns' => ['commercial_doc_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_commercial_docs_concepts_movements1' => ['type' => 'foreign', 'columns' => ['movement_id'], 'references' => ['movements', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_commercial_docs_concepts_commercial_docs1' => ['type' => 'foreign', 'columns' => ['commercial_doc_id'], 'references' => ['commercial_docs', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'movement_id' => 1,
            'commercial_doc_id' => 1
        ],
    ];
}
