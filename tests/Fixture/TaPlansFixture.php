<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TaPlansFixture
 *
 */
class TaPlansFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'plans';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'controller_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'profile_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'pool_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'plan_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_plans_controllers1_idx' => ['type' => 'index', 'columns' => ['controller_id'], 'length' => []],
            'fk_plans_profiles1_idx' => ['type' => 'index', 'columns' => ['profile_id'], 'length' => []],
            'fk_plans_pools1_idx' => ['type' => 'index', 'columns' => ['pool_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_plans_controllers1' => ['type' => 'foreign', 'columns' => ['controller_id'], 'references' => ['controllers', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_plans_profiles1' => ['type' => 'foreign', 'columns' => ['profile_id'], 'references' => ['profiles', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_plans_pools1' => ['type' => 'foreign', 'columns' => ['pool_id'], 'references' => ['pools', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'controller_id' => 1,
            'profile_id' => 1,
            'pool_id' => 1,
            'plan_id' => 1
        ],
    ];
}
