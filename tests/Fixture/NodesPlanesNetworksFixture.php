<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * NodesPlanesNetworksFixture
 *
 */
class NodesPlanesNetworksFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'nodes_plans_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'networks_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_nodes_planes_networks_nodes_plans1_idx' => ['type' => 'index', 'columns' => ['nodes_plans_id'], 'length' => []],
            'fk_nodes_planes_networks_networks1_idx' => ['type' => 'index', 'columns' => ['networks_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id', 'nodes_plans_id', 'networks_id'], 'length' => []],
            'fk_nodes_planes_networks_nodes_plans1' => ['type' => 'foreign', 'columns' => ['nodes_plans_id'], 'references' => ['nodes_plans', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_nodes_planes_networks_networks1' => ['type' => 'foreign', 'columns' => ['networks_id'], 'references' => ['networks', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'nodes_plans_id' => 1,
            'networks_id' => 1,
            'created' => '2016-07-20 12:32:50',
            'modified' => '2016-07-20 12:32:50'
        ],
    ];
}
