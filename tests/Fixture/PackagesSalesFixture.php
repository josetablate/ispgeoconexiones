<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PackagesSalesFixture
 *
 */
class PackagesSalesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 6, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'deleted' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
        'customer_code' => ['type' => 'integer', 'length' => 5, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'package_id' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'instalation_id' => ['type' => 'integer', 'length' => 6, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_packages_sales_customers1_idx' => ['type' => 'index', 'columns' => ['customer_code'], 'length' => []],
            'fk_packages_sales_packages1_idx' => ['type' => 'index', 'columns' => ['package_id'], 'length' => []],
            'fk_packages_sales_instalations1_idx' => ['type' => 'index', 'columns' => ['instalation_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_packages_sales_customers1' => ['type' => 'foreign', 'columns' => ['customer_code'], 'references' => ['customers', 'code'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_packages_sales_packages1' => ['type' => 'foreign', 'columns' => ['package_id'], 'references' => ['packages', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_packages_sales_instalations1' => ['type' => 'foreign', 'columns' => ['instalation_id'], 'references' => ['instalations', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'created' => '2017-03-11 20:11:50',
            'deleted' => 1,
            'customer_code' => 1,
            'package_id' => 1,
            'instalation_id' => 1
        ],
    ];
}
