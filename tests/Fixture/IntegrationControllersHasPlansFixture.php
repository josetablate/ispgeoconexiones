<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * IntegrationControllersHasPlansFixture
 *
 */
class IntegrationControllersHasPlansFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 4, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'integration_controller_id' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'plan_id' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'profile_id' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'pool_id' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_integration_controllers_has_plans_plans1_idx' => ['type' => 'index', 'columns' => ['plan_id'], 'length' => []],
            'fk_integration_controllers_has_plans_integration_controller_idx' => ['type' => 'index', 'columns' => ['integration_controller_id'], 'length' => []],
            'fk_integration_controllers_has_plans_profiles1_idx' => ['type' => 'index', 'columns' => ['profile_id'], 'length' => []],
            'fk_integration_controllers_has_plans_pools1_idx' => ['type' => 'index', 'columns' => ['pool_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_integration_controllers_has_plans_integration_controllers1' => ['type' => 'foreign', 'columns' => ['integration_controller_id'], 'references' => ['integration_controllers', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_integration_controllers_has_plans_plans1' => ['type' => 'foreign', 'columns' => ['plan_id'], 'references' => ['plans', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_integration_controllers_has_plans_pools1' => ['type' => 'foreign', 'columns' => ['pool_id'], 'references' => ['pools', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_integration_controllers_has_plans_profiles1' => ['type' => 'foreign', 'columns' => ['profile_id'], 'references' => ['profiles', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'integration_controller_id' => 1,
            'plan_id' => 1,
            'profile_id' => 1,
            'pool_id' => 1
        ],
    ];
}
